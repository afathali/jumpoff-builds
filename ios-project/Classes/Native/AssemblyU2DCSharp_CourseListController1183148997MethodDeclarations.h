﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CourseListController
struct CourseListController_t1183148997;
// DeleteCourseEventArgs
struct DeleteCourseEventArgs_t2630278065;
// DeleteCourseResponse
struct DeleteCourseResponse_t1898856809;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DeleteCourseEventArgs2630278065.h"
#include "AssemblyU2DCSharp_DeleteCourseResponse1898856809.h"

// System.Void CourseListController::.ctor()
extern "C"  void CourseListController__ctor_m2177838640 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::AwakeOverride()
extern "C"  void CourseListController_AwakeOverride_m1710449961 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::DestroyOverride()
extern "C"  void CourseListController_DestroyOverride_m3357447926 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::HandleDeleteCourseEventArgs(DeleteCourseEventArgs)
extern "C"  void CourseListController_HandleDeleteCourseEventArgs_m444718928 (CourseListController_t1183148997 * __this, DeleteCourseEventArgs_t2630278065 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::HideAll()
extern "C"  void CourseListController_HideAll_m1213509717 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::Start()
extern "C"  void CourseListController_Start_m1056877808 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::Update()
extern "C"  void CourseListController_Update_m2388028465 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::SkipSave()
extern "C"  void CourseListController_SkipSave_m4119546080 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::CloseDeleteMessage()
extern "C"  void CourseListController_CloseDeleteMessage_m1218038880 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::ConfirmDeleteCourse()
extern "C"  void CourseListController_ConfirmDeleteCourse_m3703010678 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::HandleDeleteCourseResponse(System.Boolean,DeleteCourseResponse)
extern "C"  void CourseListController_HandleDeleteCourseResponse_m3999337657 (CourseListController_t1183148997 * __this, bool ___success0, DeleteCourseResponse_t1898856809 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::BuildCourseList()
extern "C"  void CourseListController_BuildCourseList_m3751986693 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::Close()
extern "C"  void CourseListController_Close_m670124412 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::GetNewImage()
extern "C"  void CourseListController_GetNewImage_m1515942957 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::AddNewCourse()
extern "C"  void CourseListController_AddNewCourse_m2434924892 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::EditSelectedImageDetails()
extern "C"  void CourseListController_EditSelectedImageDetails_m3523982954 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::SubmitCourse()
extern "C"  void CourseListController_SubmitCourse_m189062681 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::<Start>m__10C()
extern "C"  void CourseListController_U3CStartU3Em__10C_m1746229369 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::<Start>m__10D()
extern "C"  void CourseListController_U3CStartU3Em__10D_m1746229266 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::<Start>m__10E()
extern "C"  void CourseListController_U3CStartU3Em__10E_m1746229171 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::<Start>m__10F()
extern "C"  void CourseListController_U3CStartU3Em__10F_m1746229332 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::<Start>m__110()
extern "C"  void CourseListController_U3CStartU3Em__110_m3332513611 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::<Start>m__111()
extern "C"  void CourseListController_U3CStartU3Em__111_m3332513644 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::<Start>m__112()
extern "C"  void CourseListController_U3CStartU3Em__112_m3332513677 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::<Start>m__113()
extern "C"  void CourseListController_U3CStartU3Em__113_m3332513710 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseListController::<Start>m__114()
extern "C"  void CourseListController_U3CStartU3Em__114_m3332513479 (CourseListController_t1183148997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
