﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct ShimEnumerator_t2463690899;
// System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct Dictionary_2_t2358566078;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m951601814_gshared (ShimEnumerator_t2463690899 * __this, Dictionary_2_t2358566078 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m951601814(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2463690899 *, Dictionary_2_t2358566078 *, const MethodInfo*))ShimEnumerator__ctor_m951601814_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2051636185_gshared (ShimEnumerator_t2463690899 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2051636185(__this, method) ((  bool (*) (ShimEnumerator_t2463690899 *, const MethodInfo*))ShimEnumerator_MoveNext_m2051636185_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2321025869_gshared (ShimEnumerator_t2463690899 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2321025869(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2463690899 *, const MethodInfo*))ShimEnumerator_get_Entry_m2321025869_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1655108688_gshared (ShimEnumerator_t2463690899 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1655108688(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2463690899 *, const MethodInfo*))ShimEnumerator_get_Key_m1655108688_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m456005610_gshared (ShimEnumerator_t2463690899 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m456005610(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2463690899 *, const MethodInfo*))ShimEnumerator_get_Value_m456005610_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3788940230_gshared (ShimEnumerator_t2463690899 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3788940230(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2463690899 *, const MethodInfo*))ShimEnumerator_get_Current_m3788940230_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m1423171536_gshared (ShimEnumerator_t2463690899 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1423171536(__this, method) ((  void (*) (ShimEnumerator_t2463690899 *, const MethodInfo*))ShimEnumerator_Reset_m1423171536_gshared)(__this, method)
