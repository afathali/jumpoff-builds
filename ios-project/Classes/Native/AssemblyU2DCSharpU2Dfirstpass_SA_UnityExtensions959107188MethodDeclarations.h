﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Action
struct Action_t3226471752;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Animation_2123337604.h"
#include "System_Core_System_Action3226471752.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void SA_UnityExtensions::MoveTo(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,SA.Common.Animation.EaseType,System.Action)
extern "C"  void SA_UnityExtensions_MoveTo_m1338073887 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, Vector3_t2243707580  ___position1, float ___time2, int32_t ___easeType3, Action_t3226471752 * ___OnCompleteAction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_UnityExtensions::ScaleTo(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,SA.Common.Animation.EaseType,System.Action)
extern "C"  void SA_UnityExtensions_ScaleTo_m2112209700 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, Vector3_t2243707580  ___scale1, float ___time2, int32_t ___easeType3, Action_t3226471752 * ___OnCompleteAction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite SA_UnityExtensions::ToSprite(UnityEngine.Texture2D)
extern "C"  Sprite_t309593783 * SA_UnityExtensions_ToSprite_m2215582518 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
