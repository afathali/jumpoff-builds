﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CropHints
struct  CropHints_t3208752360  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject CropHints::hintsUI
	GameObject_t1756533147 * ___hintsUI_2;
	// UnityEngine.GameObject CropHints::hint1
	GameObject_t1756533147 * ___hint1_3;
	// UnityEngine.GameObject CropHints::hint2
	GameObject_t1756533147 * ___hint2_4;
	// UnityEngine.UI.Button CropHints::btn_continue
	Button_t2872111280 * ___btn_continue_5;
	// UnityEngine.UI.Button CropHints::btn_info
	Button_t2872111280 * ___btn_info_6;

public:
	inline static int32_t get_offset_of_hintsUI_2() { return static_cast<int32_t>(offsetof(CropHints_t3208752360, ___hintsUI_2)); }
	inline GameObject_t1756533147 * get_hintsUI_2() const { return ___hintsUI_2; }
	inline GameObject_t1756533147 ** get_address_of_hintsUI_2() { return &___hintsUI_2; }
	inline void set_hintsUI_2(GameObject_t1756533147 * value)
	{
		___hintsUI_2 = value;
		Il2CppCodeGenWriteBarrier(&___hintsUI_2, value);
	}

	inline static int32_t get_offset_of_hint1_3() { return static_cast<int32_t>(offsetof(CropHints_t3208752360, ___hint1_3)); }
	inline GameObject_t1756533147 * get_hint1_3() const { return ___hint1_3; }
	inline GameObject_t1756533147 ** get_address_of_hint1_3() { return &___hint1_3; }
	inline void set_hint1_3(GameObject_t1756533147 * value)
	{
		___hint1_3 = value;
		Il2CppCodeGenWriteBarrier(&___hint1_3, value);
	}

	inline static int32_t get_offset_of_hint2_4() { return static_cast<int32_t>(offsetof(CropHints_t3208752360, ___hint2_4)); }
	inline GameObject_t1756533147 * get_hint2_4() const { return ___hint2_4; }
	inline GameObject_t1756533147 ** get_address_of_hint2_4() { return &___hint2_4; }
	inline void set_hint2_4(GameObject_t1756533147 * value)
	{
		___hint2_4 = value;
		Il2CppCodeGenWriteBarrier(&___hint2_4, value);
	}

	inline static int32_t get_offset_of_btn_continue_5() { return static_cast<int32_t>(offsetof(CropHints_t3208752360, ___btn_continue_5)); }
	inline Button_t2872111280 * get_btn_continue_5() const { return ___btn_continue_5; }
	inline Button_t2872111280 ** get_address_of_btn_continue_5() { return &___btn_continue_5; }
	inline void set_btn_continue_5(Button_t2872111280 * value)
	{
		___btn_continue_5 = value;
		Il2CppCodeGenWriteBarrier(&___btn_continue_5, value);
	}

	inline static int32_t get_offset_of_btn_info_6() { return static_cast<int32_t>(offsetof(CropHints_t3208752360, ___btn_info_6)); }
	inline Button_t2872111280 * get_btn_info_6() const { return ___btn_info_6; }
	inline Button_t2872111280 ** get_address_of_btn_info_6() { return &___btn_info_6; }
	inline void set_btn_info_6(Button_t2872111280 * value)
	{
		___btn_info_6 = value;
		Il2CppCodeGenWriteBarrier(&___btn_info_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
