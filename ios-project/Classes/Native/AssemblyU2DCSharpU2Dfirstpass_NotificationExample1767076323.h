﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<ISN_RemoteNotificationsRegistrationResult>
struct Action_1_t3137674533;

#include "AssemblyU2DCSharpU2Dfirstpass_BaseIOSFeaturePrevie3055692840.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationExample
struct  NotificationExample_t1767076323  : public BaseIOSFeaturePreview_t3055692840
{
public:
	// System.Int32 NotificationExample::lastNotificationId
	int32_t ___lastNotificationId_12;

public:
	inline static int32_t get_offset_of_lastNotificationId_12() { return static_cast<int32_t>(offsetof(NotificationExample_t1767076323, ___lastNotificationId_12)); }
	inline int32_t get_lastNotificationId_12() const { return ___lastNotificationId_12; }
	inline int32_t* get_address_of_lastNotificationId_12() { return &___lastNotificationId_12; }
	inline void set_lastNotificationId_12(int32_t value)
	{
		___lastNotificationId_12 = value;
	}
};

struct NotificationExample_t1767076323_StaticFields
{
public:
	// System.Action`1<ISN_RemoteNotificationsRegistrationResult> NotificationExample::<>f__am$cache1
	Action_1_t3137674533 * ___U3CU3Ef__amU24cache1_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_13() { return static_cast<int32_t>(offsetof(NotificationExample_t1767076323_StaticFields, ___U3CU3Ef__amU24cache1_13)); }
	inline Action_1_t3137674533 * get_U3CU3Ef__amU24cache1_13() const { return ___U3CU3Ef__amU24cache1_13; }
	inline Action_1_t3137674533 ** get_address_of_U3CU3Ef__amU24cache1_13() { return &___U3CU3Ef__amU24cache1_13; }
	inline void set_U3CU3Ef__amU24cache1_13(Action_1_t3137674533 * value)
	{
		___U3CU3Ef__amU24cache1_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
