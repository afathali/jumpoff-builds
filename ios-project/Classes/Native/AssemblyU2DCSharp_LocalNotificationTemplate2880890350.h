﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalNotificationTemplate
struct  LocalNotificationTemplate_t2880890350  : public Il2CppObject
{
public:
	// System.Int32 LocalNotificationTemplate::_id
	int32_t ____id_1;
	// System.String LocalNotificationTemplate::_title
	String_t* ____title_2;
	// System.String LocalNotificationTemplate::_message
	String_t* ____message_3;
	// System.DateTime LocalNotificationTemplate::_fireDate
	DateTime_t693205669  ____fireDate_4;

public:
	inline static int32_t get_offset_of__id_1() { return static_cast<int32_t>(offsetof(LocalNotificationTemplate_t2880890350, ____id_1)); }
	inline int32_t get__id_1() const { return ____id_1; }
	inline int32_t* get_address_of__id_1() { return &____id_1; }
	inline void set__id_1(int32_t value)
	{
		____id_1 = value;
	}

	inline static int32_t get_offset_of__title_2() { return static_cast<int32_t>(offsetof(LocalNotificationTemplate_t2880890350, ____title_2)); }
	inline String_t* get__title_2() const { return ____title_2; }
	inline String_t** get_address_of__title_2() { return &____title_2; }
	inline void set__title_2(String_t* value)
	{
		____title_2 = value;
		Il2CppCodeGenWriteBarrier(&____title_2, value);
	}

	inline static int32_t get_offset_of__message_3() { return static_cast<int32_t>(offsetof(LocalNotificationTemplate_t2880890350, ____message_3)); }
	inline String_t* get__message_3() const { return ____message_3; }
	inline String_t** get_address_of__message_3() { return &____message_3; }
	inline void set__message_3(String_t* value)
	{
		____message_3 = value;
		Il2CppCodeGenWriteBarrier(&____message_3, value);
	}

	inline static int32_t get_offset_of__fireDate_4() { return static_cast<int32_t>(offsetof(LocalNotificationTemplate_t2880890350, ____fireDate_4)); }
	inline DateTime_t693205669  get__fireDate_4() const { return ____fireDate_4; }
	inline DateTime_t693205669 * get_address_of__fireDate_4() { return &____fireDate_4; }
	inline void set__fireDate_4(DateTime_t693205669  value)
	{
		____fireDate_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
