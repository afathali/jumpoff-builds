﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonConverter
struct JsonConverter_t1964060750;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t3772113849;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.JsonConverter::.ctor()
extern "C"  void JsonConverter__ctor_m3599769528 (JsonConverter_t1964060750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.JsonConverter::GetSchema()
extern "C"  JsonSchema_t3772113849 * JsonConverter_GetSchema_m1518774069 (JsonConverter_t1964060750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonConverter::get_CanRead()
extern "C"  bool JsonConverter_get_CanRead_m2493037983 (JsonConverter_t1964060750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonConverter::get_CanWrite()
extern "C"  bool JsonConverter_get_CanWrite_m3803561624 (JsonConverter_t1964060750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
