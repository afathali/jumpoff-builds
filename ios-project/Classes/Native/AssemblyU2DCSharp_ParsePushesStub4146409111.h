﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct Action_2_t2514582953;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParsePushesStub
struct  ParsePushesStub_t4146409111  : public Il2CppObject
{
public:

public:
};

struct ParsePushesStub_t4146409111_StaticFields
{
public:
	// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>> ParsePushesStub::OnPushReceived
	Action_2_t2514582953 * ___OnPushReceived_0;
	// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>> ParsePushesStub::<>f__am$cache1
	Action_2_t2514582953 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_OnPushReceived_0() { return static_cast<int32_t>(offsetof(ParsePushesStub_t4146409111_StaticFields, ___OnPushReceived_0)); }
	inline Action_2_t2514582953 * get_OnPushReceived_0() const { return ___OnPushReceived_0; }
	inline Action_2_t2514582953 ** get_address_of_OnPushReceived_0() { return &___OnPushReceived_0; }
	inline void set_OnPushReceived_0(Action_2_t2514582953 * value)
	{
		___OnPushReceived_0 = value;
		Il2CppCodeGenWriteBarrier(&___OnPushReceived_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(ParsePushesStub_t4146409111_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Action_2_t2514582953 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Action_2_t2514582953 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Action_2_t2514582953 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
