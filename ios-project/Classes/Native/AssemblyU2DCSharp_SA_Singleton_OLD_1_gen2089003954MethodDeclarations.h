﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3980293213MethodDeclarations.h"

// System.Void SA_Singleton_OLD`1<AndroidNativeUtility>::.ctor()
#define SA_Singleton_OLD_1__ctor_m3998523892(__this, method) ((  void (*) (SA_Singleton_OLD_1_t2089003954 *, const MethodInfo*))SA_Singleton_OLD_1__ctor_m2643683696_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<AndroidNativeUtility>::.cctor()
#define SA_Singleton_OLD_1__cctor_m3987558215(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1__cctor_m1355192463_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<AndroidNativeUtility>::get_instance()
#define SA_Singleton_OLD_1_get_instance_m1972122551(__this /* static, unused */, method) ((  AndroidNativeUtility_t798160036 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_instance_m1963118739_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<AndroidNativeUtility>::get_Instance()
#define SA_Singleton_OLD_1_get_Instance_m2016760791(__this /* static, unused */, method) ((  AndroidNativeUtility_t798160036 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_Instance_m2967800051_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<AndroidNativeUtility>::get_HasInstance()
#define SA_Singleton_OLD_1_get_HasInstance_m743083970(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_HasInstance_m4184706798_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<AndroidNativeUtility>::get_IsDestroyed()
#define SA_Singleton_OLD_1_get_IsDestroyed_m2689114790(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_IsDestroyed_m1648274602_gshared)(__this /* static, unused */, method)
// System.Void SA_Singleton_OLD`1<AndroidNativeUtility>::OnDestroy()
#define SA_Singleton_OLD_1_OnDestroy_m3124164275(__this, method) ((  void (*) (SA_Singleton_OLD_1_t2089003954 *, const MethodInfo*))SA_Singleton_OLD_1_OnDestroy_m1680414419_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<AndroidNativeUtility>::OnApplicationQuit()
#define SA_Singleton_OLD_1_OnApplicationQuit_m2021504666(__this, method) ((  void (*) (SA_Singleton_OLD_1_t2089003954 *, const MethodInfo*))SA_Singleton_OLD_1_OnApplicationQuit_m1864960902_gshared)(__this, method)
