﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;

#include "AssemblyU2DCSharp_MCGBehaviour3307770884.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Login_Controller
struct  Login_Controller_t1381689646  : public MCGBehaviour_t3307770884
{
public:
	// UnityEngine.GameObject Login_Controller::LoginWait
	GameObject_t1756533147 * ___LoginWait_4;
	// UnityEngine.Camera Login_Controller::MainCamera
	Camera_t189460977 * ___MainCamera_5;
	// UnityEngine.GameObject Login_Controller::ErrorMessage
	GameObject_t1756533147 * ___ErrorMessage_6;
	// UnityEngine.UI.Button Login_Controller::btn_signIn
	Button_t2872111280 * ___btn_signIn_7;
	// UnityEngine.UI.Button Login_Controller::btn_signUp
	Button_t2872111280 * ___btn_signUp_8;
	// UnityEngine.UI.Button Login_Controller::btn_signCancel
	Button_t2872111280 * ___btn_signCancel_9;
	// UnityEngine.UI.Button Login_Controller::btn_signSubmit
	Button_t2872111280 * ___btn_signSubmit_10;
	// UnityEngine.UI.Button Login_Controller::btn_close1
	Button_t2872111280 * ___btn_close1_11;
	// UnityEngine.UI.Button Login_Controller::btn_close2
	Button_t2872111280 * ___btn_close2_12;
	// UnityEngine.UI.Button Login_Controller::btn_close3
	Button_t2872111280 * ___btn_close3_13;
	// UnityEngine.UI.Button Login_Controller::btn_close4
	Button_t2872111280 * ___btn_close4_14;
	// UnityEngine.UI.Button Login_Controller::btn_close5
	Button_t2872111280 * ___btn_close5_15;
	// UnityEngine.UI.InputField Login_Controller::UserEmail
	InputField_t1631627530 * ___UserEmail_16;
	// UnityEngine.UI.InputField Login_Controller::UserPassword
	InputField_t1631627530 * ___UserPassword_17;
	// UnityEngine.UI.Button Login_Controller::btn_forgotPassword
	Button_t2872111280 * ___btn_forgotPassword_18;
	// UnityEngine.GameObject Login_Controller::SignInPage
	GameObject_t1756533147 * ___SignInPage_19;
	// UnityEngine.GameObject Login_Controller::SignUpPage
	GameObject_t1756533147 * ___SignUpPage_20;
	// UnityEngine.GameObject Login_Controller::MainPage
	GameObject_t1756533147 * ___MainPage_21;
	// UnityEngine.GameObject Login_Controller::ForgotPasswordPage
	GameObject_t1756533147 * ___ForgotPasswordPage_22;
	// UnityEngine.GameObject Login_Controller::ChangePasswordPage
	GameObject_t1756533147 * ___ChangePasswordPage_23;
	// UnityEngine.UI.InputField Login_Controller::forgotEmail
	InputField_t1631627530 * ___forgotEmail_24;
	// UnityEngine.UI.Button Login_Controller::btn_forgotCancel
	Button_t2872111280 * ___btn_forgotCancel_25;
	// UnityEngine.UI.Button Login_Controller::btn_forgotSubmit
	Button_t2872111280 * ___btn_forgotSubmit_26;
	// UnityEngine.UI.InputField Login_Controller::changePassword
	InputField_t1631627530 * ___changePassword_27;
	// UnityEngine.UI.InputField Login_Controller::changeReenterPassword
	InputField_t1631627530 * ___changeReenterPassword_28;
	// UnityEngine.UI.Button Login_Controller::btn_changeCancel
	Button_t2872111280 * ___btn_changeCancel_29;
	// UnityEngine.UI.Button Login_Controller::btn_changeSubmit
	Button_t2872111280 * ___btn_changeSubmit_30;
	// UnityEngine.UI.InputField Login_Controller::FullName
	InputField_t1631627530 * ___FullName_31;
	// UnityEngine.UI.InputField Login_Controller::Email
	InputField_t1631627530 * ___Email_32;
	// UnityEngine.UI.InputField Login_Controller::Password
	InputField_t1631627530 * ___Password_33;
	// UnityEngine.UI.InputField Login_Controller::ConfirmPassword
	InputField_t1631627530 * ___ConfirmPassword_34;
	// UnityEngine.GameObject Login_Controller::Zone1
	GameObject_t1756533147 * ___Zone1_35;
	// UnityEngine.GameObject Login_Controller::Zone2
	GameObject_t1756533147 * ___Zone2_36;
	// UnityEngine.GameObject Login_Controller::Zone3
	GameObject_t1756533147 * ___Zone3_37;
	// UnityEngine.GameObject Login_Controller::Zone4
	GameObject_t1756533147 * ___Zone4_38;
	// UnityEngine.GameObject Login_Controller::Zone5
	GameObject_t1756533147 * ___Zone5_39;
	// UnityEngine.GameObject Login_Controller::Zone6
	GameObject_t1756533147 * ___Zone6_40;
	// UnityEngine.GameObject Login_Controller::Zone7
	GameObject_t1756533147 * ___Zone7_41;
	// UnityEngine.GameObject Login_Controller::Zone8
	GameObject_t1756533147 * ___Zone8_42;
	// UnityEngine.GameObject Login_Controller::Zone9
	GameObject_t1756533147 * ___Zone9_43;
	// UnityEngine.GameObject Login_Controller::Zone10
	GameObject_t1756533147 * ___Zone10_44;
	// UnityEngine.GameObject Login_Controller::Zone11
	GameObject_t1756533147 * ___Zone11_45;
	// UnityEngine.GameObject Login_Controller::Zone12
	GameObject_t1756533147 * ___Zone12_46;
	// UnityEngine.GameObject Login_Controller::Discipline1
	GameObject_t1756533147 * ___Discipline1_47;
	// UnityEngine.GameObject Login_Controller::Discipline2
	GameObject_t1756533147 * ___Discipline2_48;
	// UnityEngine.GameObject Login_Controller::Discipline3
	GameObject_t1756533147 * ___Discipline3_49;
	// System.Collections.Generic.List`1<System.Int32> Login_Controller::selectedZones
	List_1_t1440998580 * ___selectedZones_50;
	// System.Collections.Generic.List`1<System.String> Login_Controller::selectedDisciplines
	List_1_t1398341365 * ___selectedDisciplines_51;
	// UnityEngine.UI.Button Login_Controller::btn_cancel
	Button_t2872111280 * ___btn_cancel_52;
	// UnityEngine.UI.Button Login_Controller::btn_join
	Button_t2872111280 * ___btn_join_53;
	// UnityEngine.GameObject Login_Controller::Terms
	GameObject_t1756533147 * ___Terms_54;
	// UnityEngine.UI.Button Login_Controller::btn_terms
	Button_t2872111280 * ___btn_terms_55;
	// System.Int32 Login_Controller::CurrentScreen
	int32_t ___CurrentScreen_56;

public:
	inline static int32_t get_offset_of_LoginWait_4() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___LoginWait_4)); }
	inline GameObject_t1756533147 * get_LoginWait_4() const { return ___LoginWait_4; }
	inline GameObject_t1756533147 ** get_address_of_LoginWait_4() { return &___LoginWait_4; }
	inline void set_LoginWait_4(GameObject_t1756533147 * value)
	{
		___LoginWait_4 = value;
		Il2CppCodeGenWriteBarrier(&___LoginWait_4, value);
	}

	inline static int32_t get_offset_of_MainCamera_5() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___MainCamera_5)); }
	inline Camera_t189460977 * get_MainCamera_5() const { return ___MainCamera_5; }
	inline Camera_t189460977 ** get_address_of_MainCamera_5() { return &___MainCamera_5; }
	inline void set_MainCamera_5(Camera_t189460977 * value)
	{
		___MainCamera_5 = value;
		Il2CppCodeGenWriteBarrier(&___MainCamera_5, value);
	}

	inline static int32_t get_offset_of_ErrorMessage_6() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___ErrorMessage_6)); }
	inline GameObject_t1756533147 * get_ErrorMessage_6() const { return ___ErrorMessage_6; }
	inline GameObject_t1756533147 ** get_address_of_ErrorMessage_6() { return &___ErrorMessage_6; }
	inline void set_ErrorMessage_6(GameObject_t1756533147 * value)
	{
		___ErrorMessage_6 = value;
		Il2CppCodeGenWriteBarrier(&___ErrorMessage_6, value);
	}

	inline static int32_t get_offset_of_btn_signIn_7() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_signIn_7)); }
	inline Button_t2872111280 * get_btn_signIn_7() const { return ___btn_signIn_7; }
	inline Button_t2872111280 ** get_address_of_btn_signIn_7() { return &___btn_signIn_7; }
	inline void set_btn_signIn_7(Button_t2872111280 * value)
	{
		___btn_signIn_7 = value;
		Il2CppCodeGenWriteBarrier(&___btn_signIn_7, value);
	}

	inline static int32_t get_offset_of_btn_signUp_8() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_signUp_8)); }
	inline Button_t2872111280 * get_btn_signUp_8() const { return ___btn_signUp_8; }
	inline Button_t2872111280 ** get_address_of_btn_signUp_8() { return &___btn_signUp_8; }
	inline void set_btn_signUp_8(Button_t2872111280 * value)
	{
		___btn_signUp_8 = value;
		Il2CppCodeGenWriteBarrier(&___btn_signUp_8, value);
	}

	inline static int32_t get_offset_of_btn_signCancel_9() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_signCancel_9)); }
	inline Button_t2872111280 * get_btn_signCancel_9() const { return ___btn_signCancel_9; }
	inline Button_t2872111280 ** get_address_of_btn_signCancel_9() { return &___btn_signCancel_9; }
	inline void set_btn_signCancel_9(Button_t2872111280 * value)
	{
		___btn_signCancel_9 = value;
		Il2CppCodeGenWriteBarrier(&___btn_signCancel_9, value);
	}

	inline static int32_t get_offset_of_btn_signSubmit_10() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_signSubmit_10)); }
	inline Button_t2872111280 * get_btn_signSubmit_10() const { return ___btn_signSubmit_10; }
	inline Button_t2872111280 ** get_address_of_btn_signSubmit_10() { return &___btn_signSubmit_10; }
	inline void set_btn_signSubmit_10(Button_t2872111280 * value)
	{
		___btn_signSubmit_10 = value;
		Il2CppCodeGenWriteBarrier(&___btn_signSubmit_10, value);
	}

	inline static int32_t get_offset_of_btn_close1_11() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_close1_11)); }
	inline Button_t2872111280 * get_btn_close1_11() const { return ___btn_close1_11; }
	inline Button_t2872111280 ** get_address_of_btn_close1_11() { return &___btn_close1_11; }
	inline void set_btn_close1_11(Button_t2872111280 * value)
	{
		___btn_close1_11 = value;
		Il2CppCodeGenWriteBarrier(&___btn_close1_11, value);
	}

	inline static int32_t get_offset_of_btn_close2_12() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_close2_12)); }
	inline Button_t2872111280 * get_btn_close2_12() const { return ___btn_close2_12; }
	inline Button_t2872111280 ** get_address_of_btn_close2_12() { return &___btn_close2_12; }
	inline void set_btn_close2_12(Button_t2872111280 * value)
	{
		___btn_close2_12 = value;
		Il2CppCodeGenWriteBarrier(&___btn_close2_12, value);
	}

	inline static int32_t get_offset_of_btn_close3_13() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_close3_13)); }
	inline Button_t2872111280 * get_btn_close3_13() const { return ___btn_close3_13; }
	inline Button_t2872111280 ** get_address_of_btn_close3_13() { return &___btn_close3_13; }
	inline void set_btn_close3_13(Button_t2872111280 * value)
	{
		___btn_close3_13 = value;
		Il2CppCodeGenWriteBarrier(&___btn_close3_13, value);
	}

	inline static int32_t get_offset_of_btn_close4_14() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_close4_14)); }
	inline Button_t2872111280 * get_btn_close4_14() const { return ___btn_close4_14; }
	inline Button_t2872111280 ** get_address_of_btn_close4_14() { return &___btn_close4_14; }
	inline void set_btn_close4_14(Button_t2872111280 * value)
	{
		___btn_close4_14 = value;
		Il2CppCodeGenWriteBarrier(&___btn_close4_14, value);
	}

	inline static int32_t get_offset_of_btn_close5_15() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_close5_15)); }
	inline Button_t2872111280 * get_btn_close5_15() const { return ___btn_close5_15; }
	inline Button_t2872111280 ** get_address_of_btn_close5_15() { return &___btn_close5_15; }
	inline void set_btn_close5_15(Button_t2872111280 * value)
	{
		___btn_close5_15 = value;
		Il2CppCodeGenWriteBarrier(&___btn_close5_15, value);
	}

	inline static int32_t get_offset_of_UserEmail_16() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___UserEmail_16)); }
	inline InputField_t1631627530 * get_UserEmail_16() const { return ___UserEmail_16; }
	inline InputField_t1631627530 ** get_address_of_UserEmail_16() { return &___UserEmail_16; }
	inline void set_UserEmail_16(InputField_t1631627530 * value)
	{
		___UserEmail_16 = value;
		Il2CppCodeGenWriteBarrier(&___UserEmail_16, value);
	}

	inline static int32_t get_offset_of_UserPassword_17() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___UserPassword_17)); }
	inline InputField_t1631627530 * get_UserPassword_17() const { return ___UserPassword_17; }
	inline InputField_t1631627530 ** get_address_of_UserPassword_17() { return &___UserPassword_17; }
	inline void set_UserPassword_17(InputField_t1631627530 * value)
	{
		___UserPassword_17 = value;
		Il2CppCodeGenWriteBarrier(&___UserPassword_17, value);
	}

	inline static int32_t get_offset_of_btn_forgotPassword_18() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_forgotPassword_18)); }
	inline Button_t2872111280 * get_btn_forgotPassword_18() const { return ___btn_forgotPassword_18; }
	inline Button_t2872111280 ** get_address_of_btn_forgotPassword_18() { return &___btn_forgotPassword_18; }
	inline void set_btn_forgotPassword_18(Button_t2872111280 * value)
	{
		___btn_forgotPassword_18 = value;
		Il2CppCodeGenWriteBarrier(&___btn_forgotPassword_18, value);
	}

	inline static int32_t get_offset_of_SignInPage_19() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___SignInPage_19)); }
	inline GameObject_t1756533147 * get_SignInPage_19() const { return ___SignInPage_19; }
	inline GameObject_t1756533147 ** get_address_of_SignInPage_19() { return &___SignInPage_19; }
	inline void set_SignInPage_19(GameObject_t1756533147 * value)
	{
		___SignInPage_19 = value;
		Il2CppCodeGenWriteBarrier(&___SignInPage_19, value);
	}

	inline static int32_t get_offset_of_SignUpPage_20() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___SignUpPage_20)); }
	inline GameObject_t1756533147 * get_SignUpPage_20() const { return ___SignUpPage_20; }
	inline GameObject_t1756533147 ** get_address_of_SignUpPage_20() { return &___SignUpPage_20; }
	inline void set_SignUpPage_20(GameObject_t1756533147 * value)
	{
		___SignUpPage_20 = value;
		Il2CppCodeGenWriteBarrier(&___SignUpPage_20, value);
	}

	inline static int32_t get_offset_of_MainPage_21() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___MainPage_21)); }
	inline GameObject_t1756533147 * get_MainPage_21() const { return ___MainPage_21; }
	inline GameObject_t1756533147 ** get_address_of_MainPage_21() { return &___MainPage_21; }
	inline void set_MainPage_21(GameObject_t1756533147 * value)
	{
		___MainPage_21 = value;
		Il2CppCodeGenWriteBarrier(&___MainPage_21, value);
	}

	inline static int32_t get_offset_of_ForgotPasswordPage_22() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___ForgotPasswordPage_22)); }
	inline GameObject_t1756533147 * get_ForgotPasswordPage_22() const { return ___ForgotPasswordPage_22; }
	inline GameObject_t1756533147 ** get_address_of_ForgotPasswordPage_22() { return &___ForgotPasswordPage_22; }
	inline void set_ForgotPasswordPage_22(GameObject_t1756533147 * value)
	{
		___ForgotPasswordPage_22 = value;
		Il2CppCodeGenWriteBarrier(&___ForgotPasswordPage_22, value);
	}

	inline static int32_t get_offset_of_ChangePasswordPage_23() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___ChangePasswordPage_23)); }
	inline GameObject_t1756533147 * get_ChangePasswordPage_23() const { return ___ChangePasswordPage_23; }
	inline GameObject_t1756533147 ** get_address_of_ChangePasswordPage_23() { return &___ChangePasswordPage_23; }
	inline void set_ChangePasswordPage_23(GameObject_t1756533147 * value)
	{
		___ChangePasswordPage_23 = value;
		Il2CppCodeGenWriteBarrier(&___ChangePasswordPage_23, value);
	}

	inline static int32_t get_offset_of_forgotEmail_24() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___forgotEmail_24)); }
	inline InputField_t1631627530 * get_forgotEmail_24() const { return ___forgotEmail_24; }
	inline InputField_t1631627530 ** get_address_of_forgotEmail_24() { return &___forgotEmail_24; }
	inline void set_forgotEmail_24(InputField_t1631627530 * value)
	{
		___forgotEmail_24 = value;
		Il2CppCodeGenWriteBarrier(&___forgotEmail_24, value);
	}

	inline static int32_t get_offset_of_btn_forgotCancel_25() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_forgotCancel_25)); }
	inline Button_t2872111280 * get_btn_forgotCancel_25() const { return ___btn_forgotCancel_25; }
	inline Button_t2872111280 ** get_address_of_btn_forgotCancel_25() { return &___btn_forgotCancel_25; }
	inline void set_btn_forgotCancel_25(Button_t2872111280 * value)
	{
		___btn_forgotCancel_25 = value;
		Il2CppCodeGenWriteBarrier(&___btn_forgotCancel_25, value);
	}

	inline static int32_t get_offset_of_btn_forgotSubmit_26() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_forgotSubmit_26)); }
	inline Button_t2872111280 * get_btn_forgotSubmit_26() const { return ___btn_forgotSubmit_26; }
	inline Button_t2872111280 ** get_address_of_btn_forgotSubmit_26() { return &___btn_forgotSubmit_26; }
	inline void set_btn_forgotSubmit_26(Button_t2872111280 * value)
	{
		___btn_forgotSubmit_26 = value;
		Il2CppCodeGenWriteBarrier(&___btn_forgotSubmit_26, value);
	}

	inline static int32_t get_offset_of_changePassword_27() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___changePassword_27)); }
	inline InputField_t1631627530 * get_changePassword_27() const { return ___changePassword_27; }
	inline InputField_t1631627530 ** get_address_of_changePassword_27() { return &___changePassword_27; }
	inline void set_changePassword_27(InputField_t1631627530 * value)
	{
		___changePassword_27 = value;
		Il2CppCodeGenWriteBarrier(&___changePassword_27, value);
	}

	inline static int32_t get_offset_of_changeReenterPassword_28() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___changeReenterPassword_28)); }
	inline InputField_t1631627530 * get_changeReenterPassword_28() const { return ___changeReenterPassword_28; }
	inline InputField_t1631627530 ** get_address_of_changeReenterPassword_28() { return &___changeReenterPassword_28; }
	inline void set_changeReenterPassword_28(InputField_t1631627530 * value)
	{
		___changeReenterPassword_28 = value;
		Il2CppCodeGenWriteBarrier(&___changeReenterPassword_28, value);
	}

	inline static int32_t get_offset_of_btn_changeCancel_29() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_changeCancel_29)); }
	inline Button_t2872111280 * get_btn_changeCancel_29() const { return ___btn_changeCancel_29; }
	inline Button_t2872111280 ** get_address_of_btn_changeCancel_29() { return &___btn_changeCancel_29; }
	inline void set_btn_changeCancel_29(Button_t2872111280 * value)
	{
		___btn_changeCancel_29 = value;
		Il2CppCodeGenWriteBarrier(&___btn_changeCancel_29, value);
	}

	inline static int32_t get_offset_of_btn_changeSubmit_30() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_changeSubmit_30)); }
	inline Button_t2872111280 * get_btn_changeSubmit_30() const { return ___btn_changeSubmit_30; }
	inline Button_t2872111280 ** get_address_of_btn_changeSubmit_30() { return &___btn_changeSubmit_30; }
	inline void set_btn_changeSubmit_30(Button_t2872111280 * value)
	{
		___btn_changeSubmit_30 = value;
		Il2CppCodeGenWriteBarrier(&___btn_changeSubmit_30, value);
	}

	inline static int32_t get_offset_of_FullName_31() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___FullName_31)); }
	inline InputField_t1631627530 * get_FullName_31() const { return ___FullName_31; }
	inline InputField_t1631627530 ** get_address_of_FullName_31() { return &___FullName_31; }
	inline void set_FullName_31(InputField_t1631627530 * value)
	{
		___FullName_31 = value;
		Il2CppCodeGenWriteBarrier(&___FullName_31, value);
	}

	inline static int32_t get_offset_of_Email_32() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Email_32)); }
	inline InputField_t1631627530 * get_Email_32() const { return ___Email_32; }
	inline InputField_t1631627530 ** get_address_of_Email_32() { return &___Email_32; }
	inline void set_Email_32(InputField_t1631627530 * value)
	{
		___Email_32 = value;
		Il2CppCodeGenWriteBarrier(&___Email_32, value);
	}

	inline static int32_t get_offset_of_Password_33() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Password_33)); }
	inline InputField_t1631627530 * get_Password_33() const { return ___Password_33; }
	inline InputField_t1631627530 ** get_address_of_Password_33() { return &___Password_33; }
	inline void set_Password_33(InputField_t1631627530 * value)
	{
		___Password_33 = value;
		Il2CppCodeGenWriteBarrier(&___Password_33, value);
	}

	inline static int32_t get_offset_of_ConfirmPassword_34() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___ConfirmPassword_34)); }
	inline InputField_t1631627530 * get_ConfirmPassword_34() const { return ___ConfirmPassword_34; }
	inline InputField_t1631627530 ** get_address_of_ConfirmPassword_34() { return &___ConfirmPassword_34; }
	inline void set_ConfirmPassword_34(InputField_t1631627530 * value)
	{
		___ConfirmPassword_34 = value;
		Il2CppCodeGenWriteBarrier(&___ConfirmPassword_34, value);
	}

	inline static int32_t get_offset_of_Zone1_35() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Zone1_35)); }
	inline GameObject_t1756533147 * get_Zone1_35() const { return ___Zone1_35; }
	inline GameObject_t1756533147 ** get_address_of_Zone1_35() { return &___Zone1_35; }
	inline void set_Zone1_35(GameObject_t1756533147 * value)
	{
		___Zone1_35 = value;
		Il2CppCodeGenWriteBarrier(&___Zone1_35, value);
	}

	inline static int32_t get_offset_of_Zone2_36() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Zone2_36)); }
	inline GameObject_t1756533147 * get_Zone2_36() const { return ___Zone2_36; }
	inline GameObject_t1756533147 ** get_address_of_Zone2_36() { return &___Zone2_36; }
	inline void set_Zone2_36(GameObject_t1756533147 * value)
	{
		___Zone2_36 = value;
		Il2CppCodeGenWriteBarrier(&___Zone2_36, value);
	}

	inline static int32_t get_offset_of_Zone3_37() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Zone3_37)); }
	inline GameObject_t1756533147 * get_Zone3_37() const { return ___Zone3_37; }
	inline GameObject_t1756533147 ** get_address_of_Zone3_37() { return &___Zone3_37; }
	inline void set_Zone3_37(GameObject_t1756533147 * value)
	{
		___Zone3_37 = value;
		Il2CppCodeGenWriteBarrier(&___Zone3_37, value);
	}

	inline static int32_t get_offset_of_Zone4_38() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Zone4_38)); }
	inline GameObject_t1756533147 * get_Zone4_38() const { return ___Zone4_38; }
	inline GameObject_t1756533147 ** get_address_of_Zone4_38() { return &___Zone4_38; }
	inline void set_Zone4_38(GameObject_t1756533147 * value)
	{
		___Zone4_38 = value;
		Il2CppCodeGenWriteBarrier(&___Zone4_38, value);
	}

	inline static int32_t get_offset_of_Zone5_39() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Zone5_39)); }
	inline GameObject_t1756533147 * get_Zone5_39() const { return ___Zone5_39; }
	inline GameObject_t1756533147 ** get_address_of_Zone5_39() { return &___Zone5_39; }
	inline void set_Zone5_39(GameObject_t1756533147 * value)
	{
		___Zone5_39 = value;
		Il2CppCodeGenWriteBarrier(&___Zone5_39, value);
	}

	inline static int32_t get_offset_of_Zone6_40() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Zone6_40)); }
	inline GameObject_t1756533147 * get_Zone6_40() const { return ___Zone6_40; }
	inline GameObject_t1756533147 ** get_address_of_Zone6_40() { return &___Zone6_40; }
	inline void set_Zone6_40(GameObject_t1756533147 * value)
	{
		___Zone6_40 = value;
		Il2CppCodeGenWriteBarrier(&___Zone6_40, value);
	}

	inline static int32_t get_offset_of_Zone7_41() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Zone7_41)); }
	inline GameObject_t1756533147 * get_Zone7_41() const { return ___Zone7_41; }
	inline GameObject_t1756533147 ** get_address_of_Zone7_41() { return &___Zone7_41; }
	inline void set_Zone7_41(GameObject_t1756533147 * value)
	{
		___Zone7_41 = value;
		Il2CppCodeGenWriteBarrier(&___Zone7_41, value);
	}

	inline static int32_t get_offset_of_Zone8_42() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Zone8_42)); }
	inline GameObject_t1756533147 * get_Zone8_42() const { return ___Zone8_42; }
	inline GameObject_t1756533147 ** get_address_of_Zone8_42() { return &___Zone8_42; }
	inline void set_Zone8_42(GameObject_t1756533147 * value)
	{
		___Zone8_42 = value;
		Il2CppCodeGenWriteBarrier(&___Zone8_42, value);
	}

	inline static int32_t get_offset_of_Zone9_43() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Zone9_43)); }
	inline GameObject_t1756533147 * get_Zone9_43() const { return ___Zone9_43; }
	inline GameObject_t1756533147 ** get_address_of_Zone9_43() { return &___Zone9_43; }
	inline void set_Zone9_43(GameObject_t1756533147 * value)
	{
		___Zone9_43 = value;
		Il2CppCodeGenWriteBarrier(&___Zone9_43, value);
	}

	inline static int32_t get_offset_of_Zone10_44() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Zone10_44)); }
	inline GameObject_t1756533147 * get_Zone10_44() const { return ___Zone10_44; }
	inline GameObject_t1756533147 ** get_address_of_Zone10_44() { return &___Zone10_44; }
	inline void set_Zone10_44(GameObject_t1756533147 * value)
	{
		___Zone10_44 = value;
		Il2CppCodeGenWriteBarrier(&___Zone10_44, value);
	}

	inline static int32_t get_offset_of_Zone11_45() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Zone11_45)); }
	inline GameObject_t1756533147 * get_Zone11_45() const { return ___Zone11_45; }
	inline GameObject_t1756533147 ** get_address_of_Zone11_45() { return &___Zone11_45; }
	inline void set_Zone11_45(GameObject_t1756533147 * value)
	{
		___Zone11_45 = value;
		Il2CppCodeGenWriteBarrier(&___Zone11_45, value);
	}

	inline static int32_t get_offset_of_Zone12_46() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Zone12_46)); }
	inline GameObject_t1756533147 * get_Zone12_46() const { return ___Zone12_46; }
	inline GameObject_t1756533147 ** get_address_of_Zone12_46() { return &___Zone12_46; }
	inline void set_Zone12_46(GameObject_t1756533147 * value)
	{
		___Zone12_46 = value;
		Il2CppCodeGenWriteBarrier(&___Zone12_46, value);
	}

	inline static int32_t get_offset_of_Discipline1_47() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Discipline1_47)); }
	inline GameObject_t1756533147 * get_Discipline1_47() const { return ___Discipline1_47; }
	inline GameObject_t1756533147 ** get_address_of_Discipline1_47() { return &___Discipline1_47; }
	inline void set_Discipline1_47(GameObject_t1756533147 * value)
	{
		___Discipline1_47 = value;
		Il2CppCodeGenWriteBarrier(&___Discipline1_47, value);
	}

	inline static int32_t get_offset_of_Discipline2_48() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Discipline2_48)); }
	inline GameObject_t1756533147 * get_Discipline2_48() const { return ___Discipline2_48; }
	inline GameObject_t1756533147 ** get_address_of_Discipline2_48() { return &___Discipline2_48; }
	inline void set_Discipline2_48(GameObject_t1756533147 * value)
	{
		___Discipline2_48 = value;
		Il2CppCodeGenWriteBarrier(&___Discipline2_48, value);
	}

	inline static int32_t get_offset_of_Discipline3_49() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Discipline3_49)); }
	inline GameObject_t1756533147 * get_Discipline3_49() const { return ___Discipline3_49; }
	inline GameObject_t1756533147 ** get_address_of_Discipline3_49() { return &___Discipline3_49; }
	inline void set_Discipline3_49(GameObject_t1756533147 * value)
	{
		___Discipline3_49 = value;
		Il2CppCodeGenWriteBarrier(&___Discipline3_49, value);
	}

	inline static int32_t get_offset_of_selectedZones_50() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___selectedZones_50)); }
	inline List_1_t1440998580 * get_selectedZones_50() const { return ___selectedZones_50; }
	inline List_1_t1440998580 ** get_address_of_selectedZones_50() { return &___selectedZones_50; }
	inline void set_selectedZones_50(List_1_t1440998580 * value)
	{
		___selectedZones_50 = value;
		Il2CppCodeGenWriteBarrier(&___selectedZones_50, value);
	}

	inline static int32_t get_offset_of_selectedDisciplines_51() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___selectedDisciplines_51)); }
	inline List_1_t1398341365 * get_selectedDisciplines_51() const { return ___selectedDisciplines_51; }
	inline List_1_t1398341365 ** get_address_of_selectedDisciplines_51() { return &___selectedDisciplines_51; }
	inline void set_selectedDisciplines_51(List_1_t1398341365 * value)
	{
		___selectedDisciplines_51 = value;
		Il2CppCodeGenWriteBarrier(&___selectedDisciplines_51, value);
	}

	inline static int32_t get_offset_of_btn_cancel_52() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_cancel_52)); }
	inline Button_t2872111280 * get_btn_cancel_52() const { return ___btn_cancel_52; }
	inline Button_t2872111280 ** get_address_of_btn_cancel_52() { return &___btn_cancel_52; }
	inline void set_btn_cancel_52(Button_t2872111280 * value)
	{
		___btn_cancel_52 = value;
		Il2CppCodeGenWriteBarrier(&___btn_cancel_52, value);
	}

	inline static int32_t get_offset_of_btn_join_53() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_join_53)); }
	inline Button_t2872111280 * get_btn_join_53() const { return ___btn_join_53; }
	inline Button_t2872111280 ** get_address_of_btn_join_53() { return &___btn_join_53; }
	inline void set_btn_join_53(Button_t2872111280 * value)
	{
		___btn_join_53 = value;
		Il2CppCodeGenWriteBarrier(&___btn_join_53, value);
	}

	inline static int32_t get_offset_of_Terms_54() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___Terms_54)); }
	inline GameObject_t1756533147 * get_Terms_54() const { return ___Terms_54; }
	inline GameObject_t1756533147 ** get_address_of_Terms_54() { return &___Terms_54; }
	inline void set_Terms_54(GameObject_t1756533147 * value)
	{
		___Terms_54 = value;
		Il2CppCodeGenWriteBarrier(&___Terms_54, value);
	}

	inline static int32_t get_offset_of_btn_terms_55() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___btn_terms_55)); }
	inline Button_t2872111280 * get_btn_terms_55() const { return ___btn_terms_55; }
	inline Button_t2872111280 ** get_address_of_btn_terms_55() { return &___btn_terms_55; }
	inline void set_btn_terms_55(Button_t2872111280 * value)
	{
		___btn_terms_55 = value;
		Il2CppCodeGenWriteBarrier(&___btn_terms_55, value);
	}

	inline static int32_t get_offset_of_CurrentScreen_56() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646, ___CurrentScreen_56)); }
	inline int32_t get_CurrentScreen_56() const { return ___CurrentScreen_56; }
	inline int32_t* get_address_of_CurrentScreen_56() { return &___CurrentScreen_56; }
	inline void set_CurrentScreen_56(int32_t value)
	{
		___CurrentScreen_56 = value;
	}
};

struct Login_Controller_t1381689646_StaticFields
{
public:
	// UnityEngine.Events.UnityAction Login_Controller::<>f__am$cache35
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache35_57;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache35_57() { return static_cast<int32_t>(offsetof(Login_Controller_t1381689646_StaticFields, ___U3CU3Ef__amU24cache35_57)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache35_57() const { return ___U3CU3Ef__amU24cache35_57; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache35_57() { return &___U3CU3Ef__amU24cache35_57; }
	inline void set_U3CU3Ef__amU24cache35_57(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache35_57 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache35_57, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
