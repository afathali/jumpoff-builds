﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaGenerator/<GenerateInternal>c__AnonStorey1E
struct U3CGenerateInternalU3Ec__AnonStorey1E_t3389842206;
// Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema
struct TypeSchema_t460462092;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460462092.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator/<GenerateInternal>c__AnonStorey1E::.ctor()
extern "C"  void U3CGenerateInternalU3Ec__AnonStorey1E__ctor_m1626351179 (U3CGenerateInternalU3Ec__AnonStorey1E_t3389842206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaGenerator/<GenerateInternal>c__AnonStorey1E::<>m__D2(Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema)
extern "C"  bool U3CGenerateInternalU3Ec__AnonStorey1E_U3CU3Em__D2_m4032699000 (U3CGenerateInternalU3Ec__AnonStorey1E_t3389842206 * __this, TypeSchema_t460462092 * ___tc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
