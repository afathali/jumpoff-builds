﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vector4Converter
struct Vector4Converter_t3044633495;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t1973729997;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t1719617802;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonReader
struct JsonReader_t3154730733;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter1973729997.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer1719617802.h"
#include "mscorlib_System_Type1303803226.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader3154730733.h"

// System.Void Vector4Converter::.ctor()
extern "C"  void Vector4Converter__ctor_m2449945418 (Vector4Converter_t3044633495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vector4Converter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void Vector4Converter_WriteJson_m3794964100 (Vector4Converter_t3044633495 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t1719617802 * ___serializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vector4Converter::CanConvert(System.Type)
extern "C"  bool Vector4Converter_CanConvert_m2683792996 (Vector4Converter_t3044633495 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Vector4Converter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * Vector4Converter_ReadJson_m791286017 (Vector4Converter_t3044633495 * __this, JsonReader_t3154730733 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t1719617802 * ___serializer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vector4Converter::get_CanRead()
extern "C"  bool Vector4Converter_get_CanRead_m3085923085 (Vector4Converter_t3044633495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
