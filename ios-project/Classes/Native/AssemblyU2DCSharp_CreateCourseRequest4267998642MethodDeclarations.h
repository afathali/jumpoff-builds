﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CreateCourseRequest
struct CreateCourseRequest_t4267998642;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// JumpPoint[]
struct JumpPointU5BU5D_t4004280907;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// System.Void CreateCourseRequest::.ctor()
extern "C"  void CreateCourseRequest__ctor_m3977756611 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CreateCourseRequest::get_Type()
extern "C"  String_t* CreateCourseRequest_get_Type_m974855911 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseRequest::set_Type(System.String)
extern "C"  void CreateCourseRequest_set_Type_m3742680262 (CreateCourseRequest_t4267998642 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 CreateCourseRequest::get_Id()
extern "C"  int64_t CreateCourseRequest_get_Id_m3776124520 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseRequest::set_Id(System.Int64)
extern "C"  void CreateCourseRequest_set_Id_m2231139681 (CreateCourseRequest_t4267998642 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 CreateCourseRequest::get_UserId()
extern "C"  int64_t CreateCourseRequest_get_UserId_m1476555699 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseRequest::set_UserId(System.Int64)
extern "C"  void CreateCourseRequest_set_UserId_m2805658194 (CreateCourseRequest_t4267998642 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CreateCourseRequest::get_Name()
extern "C"  String_t* CreateCourseRequest_get_Name_m3744825700 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseRequest::set_Name(System.String)
extern "C"  void CreateCourseRequest_set_Name_m2849739367 (CreateCourseRequest_t4267998642 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CreateCourseRequest::get_Show()
extern "C"  String_t* CreateCourseRequest_get_Show_m2003131570 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseRequest::set_Show(System.String)
extern "C"  void CreateCourseRequest_set_Show_m4243652239 (CreateCourseRequest_t4267998642 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CreateCourseRequest::get_Date()
extern "C"  String_t* CreateCourseRequest_get_Date_m242356035 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseRequest::set_Date(System.String)
extern "C"  void CreateCourseRequest_set_Date_m3406281874 (CreateCourseRequest_t4267998642 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CreateCourseRequest::get_Notes()
extern "C"  String_t* CreateCourseRequest_get_Notes_m172987498 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseRequest::set_Notes(System.String)
extern "C"  void CreateCourseRequest_set_Notes_m3139647959 (CreateCourseRequest_t4267998642 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] CreateCourseRequest::get_Image()
extern "C"  ByteU5BU5D_t3397334013* CreateCourseRequest_get_Image_m1974429313 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseRequest::set_Image(System.Byte[])
extern "C"  void CreateCourseRequest_set_Image_m624438438 (CreateCourseRequest_t4267998642 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CreateCourseRequest::get_ImagePosition()
extern "C"  Vector3_t2243707580  CreateCourseRequest_get_ImagePosition_m126217422 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseRequest::set_ImagePosition(UnityEngine.Vector3)
extern "C"  void CreateCourseRequest_set_ImagePosition_m1273661601 (CreateCourseRequest_t4267998642 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion CreateCourseRequest::get_ImageRotation()
extern "C"  Quaternion_t4030073918  CreateCourseRequest_get_ImageRotation_m4070961709 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseRequest::set_ImageRotation(UnityEngine.Quaternion)
extern "C"  void CreateCourseRequest_set_ImageRotation_m3278001848 (CreateCourseRequest_t4267998642 * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CreateCourseRequest::get_ImageScale()
extern "C"  Vector3_t2243707580  CreateCourseRequest_get_ImageScale_m1732159371 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseRequest::set_ImageScale(UnityEngine.Vector3)
extern "C"  void CreateCourseRequest_set_ImageScale_m4014463758 (CreateCourseRequest_t4267998642 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JumpPoint[] CreateCourseRequest::get_Jumps()
extern "C"  JumpPointU5BU5D_t4004280907* CreateCourseRequest_get_Jumps_m22474336 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseRequest::set_Jumps(JumpPoint[])
extern "C"  void CreateCourseRequest_set_Jumps_m1426303471 (CreateCourseRequest_t4267998642 * __this, JumpPointU5BU5D_t4004280907* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CreateCourseRequest::get_Scale()
extern "C"  String_t* CreateCourseRequest_get_Scale_m1218926207 (CreateCourseRequest_t4267998642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseRequest::set_Scale(System.String)
extern "C"  void CreateCourseRequest_set_Scale_m4050148942 (CreateCourseRequest_t4267998642 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
