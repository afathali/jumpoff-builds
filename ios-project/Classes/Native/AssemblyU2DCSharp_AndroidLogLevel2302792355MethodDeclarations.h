﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidLogLevel
struct AndroidLogLevel_t2302792355;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidLogLevel::.ctor()
extern "C"  void AndroidLogLevel__ctor_m841596038 (AndroidLogLevel_t2302792355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
