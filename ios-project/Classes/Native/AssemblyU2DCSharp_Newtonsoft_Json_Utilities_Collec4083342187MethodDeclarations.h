﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.CollectionUtils/<CreateDictionaryWrapper>c__AnonStorey28
struct U3CCreateDictionaryWrapperU3Ec__AnonStorey28_t4083342187;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void Newtonsoft.Json.Utilities.CollectionUtils/<CreateDictionaryWrapper>c__AnonStorey28::.ctor()
extern "C"  void U3CCreateDictionaryWrapperU3Ec__AnonStorey28__ctor_m509117610 (U3CCreateDictionaryWrapperU3Ec__AnonStorey28_t4083342187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.CollectionUtils/<CreateDictionaryWrapper>c__AnonStorey28::<>m__EA(System.Type,System.Collections.Generic.IList`1<System.Object>)
extern "C"  Il2CppObject * U3CCreateDictionaryWrapperU3Ec__AnonStorey28_U3CU3Em__EA_m2492086936 (U3CCreateDictionaryWrapperU3Ec__AnonStorey28_t4083342187 * __this, Type_t * ___t0, Il2CppObject* ___a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
