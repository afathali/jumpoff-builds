﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Action`1<ISN_SwipeDirection>
struct Action_1_t570721078;
// System.Action`1<SA.IOSNative.Contacts.ContactsResult>
struct Action_1_t847530666;

#include "AssemblyU2DCSharpU2Dfirstpass_BaseIOSFeaturePrevie3055692840.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeIOSActionsExample
struct  NativeIOSActionsExample_t1779049155  : public BaseIOSFeaturePreview_t3055692840
{
public:
	// UnityEngine.Texture2D NativeIOSActionsExample::hello_texture
	Texture2D_t3542995729 * ___hello_texture_12;
	// UnityEngine.Texture2D NativeIOSActionsExample::drawTexture
	Texture2D_t3542995729 * ___drawTexture_13;
	// System.DateTime NativeIOSActionsExample::time
	DateTime_t693205669  ___time_14;

public:
	inline static int32_t get_offset_of_hello_texture_12() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t1779049155, ___hello_texture_12)); }
	inline Texture2D_t3542995729 * get_hello_texture_12() const { return ___hello_texture_12; }
	inline Texture2D_t3542995729 ** get_address_of_hello_texture_12() { return &___hello_texture_12; }
	inline void set_hello_texture_12(Texture2D_t3542995729 * value)
	{
		___hello_texture_12 = value;
		Il2CppCodeGenWriteBarrier(&___hello_texture_12, value);
	}

	inline static int32_t get_offset_of_drawTexture_13() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t1779049155, ___drawTexture_13)); }
	inline Texture2D_t3542995729 * get_drawTexture_13() const { return ___drawTexture_13; }
	inline Texture2D_t3542995729 ** get_address_of_drawTexture_13() { return &___drawTexture_13; }
	inline void set_drawTexture_13(Texture2D_t3542995729 * value)
	{
		___drawTexture_13 = value;
		Il2CppCodeGenWriteBarrier(&___drawTexture_13, value);
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t1779049155, ___time_14)); }
	inline DateTime_t693205669  get_time_14() const { return ___time_14; }
	inline DateTime_t693205669 * get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(DateTime_t693205669  value)
	{
		___time_14 = value;
	}
};

struct NativeIOSActionsExample_t1779049155_StaticFields
{
public:
	// System.Action`1<ISN_SwipeDirection> NativeIOSActionsExample::<>f__am$cache3
	Action_1_t570721078 * ___U3CU3Ef__amU24cache3_15;
	// System.Action`1<SA.IOSNative.Contacts.ContactsResult> NativeIOSActionsExample::<>f__am$cache4
	Action_1_t847530666 * ___U3CU3Ef__amU24cache4_16;
	// System.Action`1<SA.IOSNative.Contacts.ContactsResult> NativeIOSActionsExample::<>f__am$cache5
	Action_1_t847530666 * ___U3CU3Ef__amU24cache5_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_15() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t1779049155_StaticFields, ___U3CU3Ef__amU24cache3_15)); }
	inline Action_1_t570721078 * get_U3CU3Ef__amU24cache3_15() const { return ___U3CU3Ef__amU24cache3_15; }
	inline Action_1_t570721078 ** get_address_of_U3CU3Ef__amU24cache3_15() { return &___U3CU3Ef__amU24cache3_15; }
	inline void set_U3CU3Ef__amU24cache3_15(Action_1_t570721078 * value)
	{
		___U3CU3Ef__amU24cache3_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_16() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t1779049155_StaticFields, ___U3CU3Ef__amU24cache4_16)); }
	inline Action_1_t847530666 * get_U3CU3Ef__amU24cache4_16() const { return ___U3CU3Ef__amU24cache4_16; }
	inline Action_1_t847530666 ** get_address_of_U3CU3Ef__amU24cache4_16() { return &___U3CU3Ef__amU24cache4_16; }
	inline void set_U3CU3Ef__amU24cache4_16(Action_1_t847530666 * value)
	{
		___U3CU3Ef__amU24cache4_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_17() { return static_cast<int32_t>(offsetof(NativeIOSActionsExample_t1779049155_StaticFields, ___U3CU3Ef__amU24cache5_17)); }
	inline Action_1_t847530666 * get_U3CU3Ef__amU24cache5_17() const { return ___U3CU3Ef__amU24cache5_17; }
	inline Action_1_t847530666 ** get_address_of_U3CU3Ef__amU24cache5_17() { return &___U3CU3Ef__amU24cache5_17; }
	inline void set_U3CU3Ef__amU24cache5_17(Action_1_t847530666 * value)
	{
		___U3CU3Ef__amU24cache5_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
