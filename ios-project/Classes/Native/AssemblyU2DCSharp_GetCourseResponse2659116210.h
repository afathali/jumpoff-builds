﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// DeleteCourseRequest
struct DeleteCourseRequest_t1683798751;
// System.Collections.Generic.List`1<Course>
struct List_1_t2852233831;

#include "AssemblyU2DCSharp_Shared_Response_1_gen3540059715.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetCourseResponse
struct  GetCourseResponse_t2659116210  : public Response_1_t3540059715
{
public:
	// System.Boolean GetCourseResponse::<Success>k__BackingField
	bool ___U3CSuccessU3Ek__BackingField_0;
	// System.String GetCourseResponse::<ErrorMessage>k__BackingField
	String_t* ___U3CErrorMessageU3Ek__BackingField_1;
	// DeleteCourseRequest GetCourseResponse::<Request>k__BackingField
	DeleteCourseRequest_t1683798751 * ___U3CRequestU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<Course> GetCourseResponse::<Courses>k__BackingField
	List_1_t2852233831 * ___U3CCoursesU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CSuccessU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetCourseResponse_t2659116210, ___U3CSuccessU3Ek__BackingField_0)); }
	inline bool get_U3CSuccessU3Ek__BackingField_0() const { return ___U3CSuccessU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CSuccessU3Ek__BackingField_0() { return &___U3CSuccessU3Ek__BackingField_0; }
	inline void set_U3CSuccessU3Ek__BackingField_0(bool value)
	{
		___U3CSuccessU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CErrorMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetCourseResponse_t2659116210, ___U3CErrorMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CErrorMessageU3Ek__BackingField_1() const { return ___U3CErrorMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CErrorMessageU3Ek__BackingField_1() { return &___U3CErrorMessageU3Ek__BackingField_1; }
	inline void set_U3CErrorMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CErrorMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CErrorMessageU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetCourseResponse_t2659116210, ___U3CRequestU3Ek__BackingField_2)); }
	inline DeleteCourseRequest_t1683798751 * get_U3CRequestU3Ek__BackingField_2() const { return ___U3CRequestU3Ek__BackingField_2; }
	inline DeleteCourseRequest_t1683798751 ** get_address_of_U3CRequestU3Ek__BackingField_2() { return &___U3CRequestU3Ek__BackingField_2; }
	inline void set_U3CRequestU3Ek__BackingField_2(DeleteCourseRequest_t1683798751 * value)
	{
		___U3CRequestU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CCoursesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetCourseResponse_t2659116210, ___U3CCoursesU3Ek__BackingField_3)); }
	inline List_1_t2852233831 * get_U3CCoursesU3Ek__BackingField_3() const { return ___U3CCoursesU3Ek__BackingField_3; }
	inline List_1_t2852233831 ** get_address_of_U3CCoursesU3Ek__BackingField_3() { return &___U3CCoursesU3Ek__BackingField_3; }
	inline void set_U3CCoursesU3Ek__BackingField_3(List_1_t2852233831 * value)
	{
		___U3CCoursesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCoursesU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
