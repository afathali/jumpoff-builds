﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidContactInfo
struct AndroidContactInfo_t2118672179;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidContactInfo::.ctor()
extern "C"  void AndroidContactInfo__ctor_m2134188024 (AndroidContactInfo_t2118672179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
