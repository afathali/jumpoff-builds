﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen3536498748MethodDeclarations.h"

// System.Void System.Action`3<GK_MatchType,System.String[],GK_Player[]>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m3363872687(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t1065438871 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m2995706635_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<GK_MatchType,System.String[],GK_Player[]>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m2861387557(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t1065438871 *, int32_t, StringU5BU5D_t1642385972*, GK_PlayerU5BU5D_t1642762691*, const MethodInfo*))Action_3_Invoke_m76985707_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<GK_MatchType,System.String[],GK_Player[]>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m989091824(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t1065438871 *, int32_t, StringU5BU5D_t1642385972*, GK_PlayerU5BU5D_t1642762691*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m2213772654_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<GK_MatchType,System.String[],GK_Player[]>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m953325655(__this, ___result0, method) ((  void (*) (Action_3_t1065438871 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m887229305_gshared)(__this, ___result0, method)
