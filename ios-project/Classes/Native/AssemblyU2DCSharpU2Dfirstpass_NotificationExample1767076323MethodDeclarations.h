﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationExample
struct NotificationExample_t1767076323;
// ISN_LocalNotification
struct ISN_LocalNotification_t273186689;
// SA.Common.Models.Result
struct Result_t4287219743;
// ISN_RemoteNotificationsRegistrationResult
struct ISN_RemoteNotificationsRegistrationResult_t3335875151;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_LocalNotification273186689.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_RemoteNotificati3335875151.h"

// System.Void NotificationExample::.ctor()
extern "C"  void NotificationExample__ctor_m641775332 (NotificationExample_t1767076323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationExample::Awake()
extern "C"  void NotificationExample_Awake_m2490153981 (NotificationExample_t1767076323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationExample::OnGUI()
extern "C"  void NotificationExample_OnGUI_m3485453880 (NotificationExample_t1767076323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationExample::CheckNotificationSettings()
extern "C"  void NotificationExample_CheckNotificationSettings_m2039323902 (NotificationExample_t1767076323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationExample::HandleOnLocalNotificationReceived(ISN_LocalNotification)
extern "C"  void NotificationExample_HandleOnLocalNotificationReceived_m3149127331 (NotificationExample_t1767076323 * __this, ISN_LocalNotification_t273186689 * ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationExample::OnNotificationScheduleResult(SA.Common.Models.Result)
extern "C"  void NotificationExample_OnNotificationScheduleResult_m792490614 (NotificationExample_t1767076323 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationExample::<OnGUI>m__77(ISN_RemoteNotificationsRegistrationResult)
extern "C"  void NotificationExample_U3COnGUIU3Em__77_m3788913682 (Il2CppObject * __this /* static, unused */, ISN_RemoteNotificationsRegistrationResult_t3335875151 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
