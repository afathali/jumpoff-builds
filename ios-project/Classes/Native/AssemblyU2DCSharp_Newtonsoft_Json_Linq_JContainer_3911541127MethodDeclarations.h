﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator10
struct U3CDescendantsU3Ec__Iterator10_t3911541127;
// Newtonsoft.Json.Linq.JToken
struct JToken_t2552644013;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t28167840;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator10::.ctor()
extern "C"  void U3CDescendantsU3Ec__Iterator10__ctor_m1655535840 (U3CDescendantsU3Ec__Iterator10_t3911541127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator10::System.Collections.Generic.IEnumerator<Newtonsoft.Json.Linq.JToken>.get_Current()
extern "C"  JToken_t2552644013 * U3CDescendantsU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m1111179907 (U3CDescendantsU3Ec__Iterator10_t3911541127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDescendantsU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m2981621060 (U3CDescendantsU3Ec__Iterator10_t3911541127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator10::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CDescendantsU3Ec__Iterator10_System_Collections_IEnumerable_GetEnumerator_m1595281907 (U3CDescendantsU3Ec__Iterator10_t3911541127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator10::System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>.GetEnumerator()
extern "C"  Il2CppObject* U3CDescendantsU3Ec__Iterator10_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_m2039082182 (U3CDescendantsU3Ec__Iterator10_t3911541127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator10::MoveNext()
extern "C"  bool U3CDescendantsU3Ec__Iterator10_MoveNext_m2833796804 (U3CDescendantsU3Ec__Iterator10_t3911541127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator10::Dispose()
extern "C"  void U3CDescendantsU3Ec__Iterator10_Dispose_m2724006413 (U3CDescendantsU3Ec__Iterator10_t3911541127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator10::Reset()
extern "C"  void U3CDescendantsU3Ec__Iterator10_Reset_m855792375 (U3CDescendantsU3Ec__Iterator10_t3911541127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
