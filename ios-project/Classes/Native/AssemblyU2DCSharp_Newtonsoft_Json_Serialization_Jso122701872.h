﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_t1997612730;

#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js1566984540.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonISerializableContract
struct  JsonISerializableContract_t122701872  : public JsonContract_t1566984540
{
public:
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonISerializableContract::<ISerializableCreator>k__BackingField
	ObjectConstructor_1_t1997612730 * ___U3CISerializableCreatorU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CISerializableCreatorU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonISerializableContract_t122701872, ___U3CISerializableCreatorU3Ek__BackingField_12)); }
	inline ObjectConstructor_1_t1997612730 * get_U3CISerializableCreatorU3Ek__BackingField_12() const { return ___U3CISerializableCreatorU3Ek__BackingField_12; }
	inline ObjectConstructor_1_t1997612730 ** get_address_of_U3CISerializableCreatorU3Ek__BackingField_12() { return &___U3CISerializableCreatorU3Ek__BackingField_12; }
	inline void set_U3CISerializableCreatorU3Ek__BackingField_12(ObjectConstructor_1_t1997612730 * value)
	{
		___U3CISerializableCreatorU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CISerializableCreatorU3Ek__BackingField_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
