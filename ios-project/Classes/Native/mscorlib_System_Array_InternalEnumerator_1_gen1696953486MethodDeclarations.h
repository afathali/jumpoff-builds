﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1696953486.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_AN_PermissionState838201224.h"

// System.Void System.Array/InternalEnumerator`1<AN_PermissionState>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m768548935_gshared (InternalEnumerator_1_t1696953486 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m768548935(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1696953486 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m768548935_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AN_PermissionState>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4103474431_gshared (InternalEnumerator_1_t1696953486 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4103474431(__this, method) ((  void (*) (InternalEnumerator_1_t1696953486 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4103474431_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AN_PermissionState>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m793817675_gshared (InternalEnumerator_1_t1696953486 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m793817675(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1696953486 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m793817675_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AN_PermissionState>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m373497536_gshared (InternalEnumerator_1_t1696953486 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m373497536(__this, method) ((  void (*) (InternalEnumerator_1_t1696953486 *, const MethodInfo*))InternalEnumerator_1_Dispose_m373497536_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AN_PermissionState>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1683395259_gshared (InternalEnumerator_1_t1696953486 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1683395259(__this, method) ((  bool (*) (InternalEnumerator_1_t1696953486 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1683395259_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AN_PermissionState>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m242532934_gshared (InternalEnumerator_1_t1696953486 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m242532934(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1696953486 *, const MethodInfo*))InternalEnumerator_1_get_Current_m242532934_gshared)(__this, method)
