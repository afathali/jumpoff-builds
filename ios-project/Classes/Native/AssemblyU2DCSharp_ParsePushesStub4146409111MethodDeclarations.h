﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct Action_2_t2514582953;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ParsePushesStub::.cctor()
extern "C"  void ParsePushesStub__cctor_m1470061325 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParsePushesStub::add_OnPushReceived(System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern "C"  void ParsePushesStub_add_OnPushReceived_m148247484 (Il2CppObject * __this /* static, unused */, Action_2_t2514582953 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParsePushesStub::remove_OnPushReceived(System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern "C"  void ParsePushesStub_remove_OnPushReceived_m2769759603 (Il2CppObject * __this /* static, unused */, Action_2_t2514582953 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParsePushesStub::InitParse()
extern "C"  void ParsePushesStub_InitParse_m973610909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParsePushesStub::<OnPushReceived>m__26(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void ParsePushesStub_U3COnPushReceivedU3Em__26_m1896247416 (Il2CppObject * __this /* static, unused */, String_t* p0, Dictionary_2_t309261261 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
