﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TwitterAndroidUseExample/<PostScreenshot>c__Iterator7
struct U3CPostScreenshotU3Ec__Iterator7_t3015627852;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TwitterAndroidUseExample/<PostScreenshot>c__Iterator7::.ctor()
extern "C"  void U3CPostScreenshotU3Ec__Iterator7__ctor_m927970665 (U3CPostScreenshotU3Ec__Iterator7_t3015627852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TwitterAndroidUseExample/<PostScreenshot>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPostScreenshotU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1363810375 (U3CPostScreenshotU3Ec__Iterator7_t3015627852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TwitterAndroidUseExample/<PostScreenshot>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPostScreenshotU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1320430191 (U3CPostScreenshotU3Ec__Iterator7_t3015627852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TwitterAndroidUseExample/<PostScreenshot>c__Iterator7::MoveNext()
extern "C"  bool U3CPostScreenshotU3Ec__Iterator7_MoveNext_m3897507451 (U3CPostScreenshotU3Ec__Iterator7_t3015627852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample/<PostScreenshot>c__Iterator7::Dispose()
extern "C"  void U3CPostScreenshotU3Ec__Iterator7_Dispose_m3273449222 (U3CPostScreenshotU3Ec__Iterator7_t3015627852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample/<PostScreenshot>c__Iterator7::Reset()
extern "C"  void U3CPostScreenshotU3Ec__Iterator7_Reset_m3474374580 (U3CPostScreenshotU3Ec__Iterator7_t3015627852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
