﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_UserInfoLoadResult
struct GK_UserInfoLoadResult_t1177841233;
// GK_Player
struct GK_Player_t2782008294;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Player2782008294.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GK_UserInfoLoadResult::.ctor(GK_Player)
extern "C"  void GK_UserInfoLoadResult__ctor_m3320579474 (GK_UserInfoLoadResult_t1177841233 * __this, GK_Player_t2782008294 * ___tpl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_UserInfoLoadResult::.ctor(System.String)
extern "C"  void GK_UserInfoLoadResult__ctor_m186532156 (GK_UserInfoLoadResult_t1177841233 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_UserInfoLoadResult::get_playerId()
extern "C"  String_t* GK_UserInfoLoadResult_get_playerId_m771586346 (GK_UserInfoLoadResult_t1177841233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_Player GK_UserInfoLoadResult::get_playerTemplate()
extern "C"  GK_Player_t2782008294 * GK_UserInfoLoadResult_get_playerTemplate_m1989524311 (GK_UserInfoLoadResult_t1177841233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
