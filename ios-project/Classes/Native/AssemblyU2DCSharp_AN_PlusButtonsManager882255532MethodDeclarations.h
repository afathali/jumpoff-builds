﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_PlusButtonsManager
struct AN_PlusButtonsManager_t882255532;
// AN_PlusButton
struct AN_PlusButton_t1370758440;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AN_PlusButton1370758440.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_PlusButtonsManager::.ctor()
extern "C"  void AN_PlusButtonsManager__ctor_m30558995 (AN_PlusButtonsManager_t882255532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButtonsManager::Awake()
extern "C"  void AN_PlusButtonsManager_Awake_m1878911244 (AN_PlusButtonsManager_t882255532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButtonsManager::RegisterButton(AN_PlusButton)
extern "C"  void AN_PlusButtonsManager_RegisterButton_m2312891020 (AN_PlusButtonsManager_t882255532 * __this, AN_PlusButton_t1370758440 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButtonsManager::OnApplicationPause(System.Boolean)
extern "C"  void AN_PlusButtonsManager_OnApplicationPause_m4230285785 (AN_PlusButtonsManager_t882255532 * __this, bool ___IsPaused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButtonsManager::OnPlusClicked(System.String)
extern "C"  void AN_PlusButtonsManager_OnPlusClicked_m1166898565 (AN_PlusButtonsManager_t882255532 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
