﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"

// System.Void System.Action`1<SK_RequestCapabilitieResult>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m3431867717(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t2311963226 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m584977596_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<SK_RequestCapabilitieResult>::Invoke(T)
#define Action_1_Invoke_m1898924496(__this, ___obj0, method) ((  void (*) (Action_1_t2311963226 *, SK_RequestCapabilitieResult_t2510163844 *, const MethodInfo*))Action_1_Invoke_m101203496_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<SK_RequestCapabilitieResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m70679227(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t2311963226 *, SK_RequestCapabilitieResult_t2510163844 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1305519803_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<SK_RequestCapabilitieResult>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m1924637136(__this, ___result0, method) ((  void (*) (Action_1_t2311963226 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m2057605070_gshared)(__this, ___result0, method)
