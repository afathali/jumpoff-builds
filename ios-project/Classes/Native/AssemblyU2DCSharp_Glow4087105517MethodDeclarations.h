﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Glow
struct Glow_t4087105517;

#include "codegen/il2cpp-codegen.h"

// System.Void Glow::.ctor()
extern "C"  void Glow__ctor_m3669851472 (Glow_t4087105517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Glow::Start()
extern "C"  void Glow_Start_m3287855640 (Glow_t4087105517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Glow::Update()
extern "C"  void Glow_Update_m1080829369 (Glow_t4087105517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Glow::FixedUpdate()
extern "C"  void Glow_FixedUpdate_m3820461315 (Glow_t4087105517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
