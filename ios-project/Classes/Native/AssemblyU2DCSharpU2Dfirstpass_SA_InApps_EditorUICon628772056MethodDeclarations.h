﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_InApps_EditorUIController
struct SA_InApps_EditorUIController_t628772056;
// System.String
struct String_t;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Animation_2123337604.h"

// System.Void SA_InApps_EditorUIController::.ctor()
extern "C"  void SA_InApps_EditorUIController__ctor_m3019146543 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_InApps_EditorUIController::Awake()
extern "C"  void SA_InApps_EditorUIController_Awake_m2392438458 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_InApps_EditorUIController::ShowInAppPopup(System.String,System.String,System.String,System.Action`1<System.Boolean>)
extern "C"  void SA_InApps_EditorUIController_ShowInAppPopup_m1587370083 (SA_InApps_EditorUIController_t628772056 * __this, String_t* ___title0, String_t* ___describtion1, String_t* ___price2, Action_1_t3627374100 * ___OnComplete3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_InApps_EditorUIController::Close()
extern "C"  void SA_InApps_EditorUIController_Close_m3806835033 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_InApps_EditorUIController::HandleOnInTweenComplete()
extern "C"  void SA_InApps_EditorUIController_HandleOnInTweenComplete_m2010508043 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_InApps_EditorUIController::HandleOnOutTweenComplete()
extern "C"  void SA_InApps_EditorUIController_HandleOnOutTweenComplete_m760268178 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_InApps_EditorUIController::HandleOnValueChanged(System.Single)
extern "C"  void SA_InApps_EditorUIController_HandleOnValueChanged_m2963842966 (SA_InApps_EditorUIController_t628772056 * __this, float ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_InApps_EditorUIController::HandleOnFadeValueChanged(System.Single)
extern "C"  void SA_InApps_EditorUIController_HandleOnFadeValueChanged_m3561002028 (SA_InApps_EditorUIController_t628772056 * __this, float ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_InApps_EditorUIController::HandleFadeComplete()
extern "C"  void SA_InApps_EditorUIController_HandleFadeComplete_m1732026102 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_InApps_EditorUIController::FadeIn()
extern "C"  void SA_InApps_EditorUIController_FadeIn_m978052472 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_InApps_EditorUIController::FadeOut()
extern "C"  void SA_InApps_EditorUIController_FadeOut_m1069073623 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_InApps_EditorUIController::Animate(System.Single,System.Single,SA.Common.Animation.EaseType)
extern "C"  void SA_InApps_EditorUIController_Animate_m1489603625 (SA_InApps_EditorUIController_t628772056 * __this, float ___from0, float ___to1, int32_t ___easeType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
