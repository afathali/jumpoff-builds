﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver
struct CamelCasePropertyNamesContractResolver_t2702496155;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver::.ctor()
extern "C"  void CamelCasePropertyNamesContractResolver__ctor_m1657595757 (CamelCasePropertyNamesContractResolver_t2702496155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver::ResolvePropertyName(System.String)
extern "C"  String_t* CamelCasePropertyNamesContractResolver_ResolvePropertyName_m1648102282 (CamelCasePropertyNamesContractResolver_t2702496155 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
