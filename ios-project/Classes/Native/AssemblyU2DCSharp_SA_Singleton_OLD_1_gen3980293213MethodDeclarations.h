﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_Singleton_OLD`1<System.Object>
struct SA_Singleton_OLD_1_t3980293213;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SA_Singleton_OLD`1<System.Object>::.ctor()
extern "C"  void SA_Singleton_OLD_1__ctor_m2643683696_gshared (SA_Singleton_OLD_1_t3980293213 * __this, const MethodInfo* method);
#define SA_Singleton_OLD_1__ctor_m2643683696(__this, method) ((  void (*) (SA_Singleton_OLD_1_t3980293213 *, const MethodInfo*))SA_Singleton_OLD_1__ctor_m2643683696_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<System.Object>::.cctor()
extern "C"  void SA_Singleton_OLD_1__cctor_m1355192463_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define SA_Singleton_OLD_1__cctor_m1355192463(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1__cctor_m1355192463_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<System.Object>::get_instance()
extern "C"  Il2CppObject * SA_Singleton_OLD_1_get_instance_m1963118739_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define SA_Singleton_OLD_1_get_instance_m1963118739(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_instance_m1963118739_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * SA_Singleton_OLD_1_get_Instance_m2967800051_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define SA_Singleton_OLD_1_get_Instance_m2967800051(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_Instance_m2967800051_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<System.Object>::get_HasInstance()
extern "C"  bool SA_Singleton_OLD_1_get_HasInstance_m4184706798_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define SA_Singleton_OLD_1_get_HasInstance_m4184706798(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_HasInstance_m4184706798_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<System.Object>::get_IsDestroyed()
extern "C"  bool SA_Singleton_OLD_1_get_IsDestroyed_m1648274602_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define SA_Singleton_OLD_1_get_IsDestroyed_m1648274602(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_IsDestroyed_m1648274602_gshared)(__this /* static, unused */, method)
// System.Void SA_Singleton_OLD`1<System.Object>::OnDestroy()
extern "C"  void SA_Singleton_OLD_1_OnDestroy_m1680414419_gshared (SA_Singleton_OLD_1_t3980293213 * __this, const MethodInfo* method);
#define SA_Singleton_OLD_1_OnDestroy_m1680414419(__this, method) ((  void (*) (SA_Singleton_OLD_1_t3980293213 *, const MethodInfo*))SA_Singleton_OLD_1_OnDestroy_m1680414419_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<System.Object>::OnApplicationQuit()
extern "C"  void SA_Singleton_OLD_1_OnApplicationQuit_m1864960902_gshared (SA_Singleton_OLD_1_t3980293213 * __this, const MethodInfo* method);
#define SA_Singleton_OLD_1_OnApplicationQuit_m1864960902(__this, method) ((  void (*) (SA_Singleton_OLD_1_t3980293213 *, const MethodInfo*))SA_Singleton_OLD_1_OnApplicationQuit_m1864960902_gshared)(__this, method)
