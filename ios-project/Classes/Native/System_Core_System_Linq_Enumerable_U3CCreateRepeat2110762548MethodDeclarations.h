﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>
struct U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::.ctor()
extern "C"  void U3CCreateRepeatIteratorU3Ec__IteratorE_1__ctor_m181366867_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1__ctor_m181366867(__this, method) ((  void (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1__ctor_m181366867_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2551204196_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2551204196(__this, method) ((  Il2CppObject * (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2551204196_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerator_get_Current_m1095994885_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerator_get_Current_m1095994885(__this, method) ((  Il2CppObject * (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerator_get_Current_m1095994885_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerable_GetEnumerator_m2771816396_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerable_GetEnumerator_m2771816396(__this, method) ((  Il2CppObject * (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerable_GetEnumerator_m2771816396_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2824030563_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2824030563(__this, method) ((  Il2CppObject* (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2824030563_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::MoveNext()
extern "C"  bool U3CCreateRepeatIteratorU3Ec__IteratorE_1_MoveNext_m302587737_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1_MoveNext_m302587737(__this, method) ((  bool (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1_MoveNext_m302587737_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::Dispose()
extern "C"  void U3CCreateRepeatIteratorU3Ec__IteratorE_1_Dispose_m1026478484_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1_Dispose_m1026478484(__this, method) ((  void (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1_Dispose_m1026478484_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::Reset()
extern "C"  void U3CCreateRepeatIteratorU3Ec__IteratorE_1_Reset_m3405375562_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1_Reset_m3405375562(__this, method) ((  void (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1_Reset_m3405375562_gshared)(__this, method)
