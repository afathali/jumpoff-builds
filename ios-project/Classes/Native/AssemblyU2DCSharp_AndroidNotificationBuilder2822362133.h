﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidNotificationBuilder
struct  AndroidNotificationBuilder_t2822362133  : public Il2CppObject
{
public:
	// System.Int32 AndroidNotificationBuilder::_id
	int32_t ____id_1;
	// System.String AndroidNotificationBuilder::_title
	String_t* ____title_2;
	// System.String AndroidNotificationBuilder::_message
	String_t* ____message_3;
	// System.Int32 AndroidNotificationBuilder::_time
	int32_t ____time_4;
	// System.String AndroidNotificationBuilder::_sound
	String_t* ____sound_5;
	// System.String AndroidNotificationBuilder::_smallIcon
	String_t* ____smallIcon_6;
	// System.Boolean AndroidNotificationBuilder::_vibration
	bool ____vibration_7;
	// System.Boolean AndroidNotificationBuilder::_showIfAppForeground
	bool ____showIfAppForeground_8;
	// System.String AndroidNotificationBuilder::_largeIcon
	String_t* ____largeIcon_9;
	// UnityEngine.Texture2D AndroidNotificationBuilder::_bigPicture
	Texture2D_t3542995729 * ____bigPicture_10;

public:
	inline static int32_t get_offset_of__id_1() { return static_cast<int32_t>(offsetof(AndroidNotificationBuilder_t2822362133, ____id_1)); }
	inline int32_t get__id_1() const { return ____id_1; }
	inline int32_t* get_address_of__id_1() { return &____id_1; }
	inline void set__id_1(int32_t value)
	{
		____id_1 = value;
	}

	inline static int32_t get_offset_of__title_2() { return static_cast<int32_t>(offsetof(AndroidNotificationBuilder_t2822362133, ____title_2)); }
	inline String_t* get__title_2() const { return ____title_2; }
	inline String_t** get_address_of__title_2() { return &____title_2; }
	inline void set__title_2(String_t* value)
	{
		____title_2 = value;
		Il2CppCodeGenWriteBarrier(&____title_2, value);
	}

	inline static int32_t get_offset_of__message_3() { return static_cast<int32_t>(offsetof(AndroidNotificationBuilder_t2822362133, ____message_3)); }
	inline String_t* get__message_3() const { return ____message_3; }
	inline String_t** get_address_of__message_3() { return &____message_3; }
	inline void set__message_3(String_t* value)
	{
		____message_3 = value;
		Il2CppCodeGenWriteBarrier(&____message_3, value);
	}

	inline static int32_t get_offset_of__time_4() { return static_cast<int32_t>(offsetof(AndroidNotificationBuilder_t2822362133, ____time_4)); }
	inline int32_t get__time_4() const { return ____time_4; }
	inline int32_t* get_address_of__time_4() { return &____time_4; }
	inline void set__time_4(int32_t value)
	{
		____time_4 = value;
	}

	inline static int32_t get_offset_of__sound_5() { return static_cast<int32_t>(offsetof(AndroidNotificationBuilder_t2822362133, ____sound_5)); }
	inline String_t* get__sound_5() const { return ____sound_5; }
	inline String_t** get_address_of__sound_5() { return &____sound_5; }
	inline void set__sound_5(String_t* value)
	{
		____sound_5 = value;
		Il2CppCodeGenWriteBarrier(&____sound_5, value);
	}

	inline static int32_t get_offset_of__smallIcon_6() { return static_cast<int32_t>(offsetof(AndroidNotificationBuilder_t2822362133, ____smallIcon_6)); }
	inline String_t* get__smallIcon_6() const { return ____smallIcon_6; }
	inline String_t** get_address_of__smallIcon_6() { return &____smallIcon_6; }
	inline void set__smallIcon_6(String_t* value)
	{
		____smallIcon_6 = value;
		Il2CppCodeGenWriteBarrier(&____smallIcon_6, value);
	}

	inline static int32_t get_offset_of__vibration_7() { return static_cast<int32_t>(offsetof(AndroidNotificationBuilder_t2822362133, ____vibration_7)); }
	inline bool get__vibration_7() const { return ____vibration_7; }
	inline bool* get_address_of__vibration_7() { return &____vibration_7; }
	inline void set__vibration_7(bool value)
	{
		____vibration_7 = value;
	}

	inline static int32_t get_offset_of__showIfAppForeground_8() { return static_cast<int32_t>(offsetof(AndroidNotificationBuilder_t2822362133, ____showIfAppForeground_8)); }
	inline bool get__showIfAppForeground_8() const { return ____showIfAppForeground_8; }
	inline bool* get_address_of__showIfAppForeground_8() { return &____showIfAppForeground_8; }
	inline void set__showIfAppForeground_8(bool value)
	{
		____showIfAppForeground_8 = value;
	}

	inline static int32_t get_offset_of__largeIcon_9() { return static_cast<int32_t>(offsetof(AndroidNotificationBuilder_t2822362133, ____largeIcon_9)); }
	inline String_t* get__largeIcon_9() const { return ____largeIcon_9; }
	inline String_t** get_address_of__largeIcon_9() { return &____largeIcon_9; }
	inline void set__largeIcon_9(String_t* value)
	{
		____largeIcon_9 = value;
		Il2CppCodeGenWriteBarrier(&____largeIcon_9, value);
	}

	inline static int32_t get_offset_of__bigPicture_10() { return static_cast<int32_t>(offsetof(AndroidNotificationBuilder_t2822362133, ____bigPicture_10)); }
	inline Texture2D_t3542995729 * get__bigPicture_10() const { return ____bigPicture_10; }
	inline Texture2D_t3542995729 ** get_address_of__bigPicture_10() { return &____bigPicture_10; }
	inline void set__bigPicture_10(Texture2D_t3542995729 * value)
	{
		____bigPicture_10 = value;
		Il2CppCodeGenWriteBarrier(&____bigPicture_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
