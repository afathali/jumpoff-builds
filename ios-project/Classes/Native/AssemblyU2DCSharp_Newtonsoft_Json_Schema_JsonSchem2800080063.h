﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_ObjectModel_KeyedCollec499193797.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Schema.JsonSchemaNodeCollection
struct  JsonSchemaNodeCollection_t2800080063  : public KeyedCollection_2_t499193797
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
