﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameCenterTvOSExample
struct GameCenterTvOSExample_t913629997;
// SA.Common.Models.Result
struct Result_t4287219743;
// GK_LeaderboardResult
struct GK_LeaderboardResult_t866080833;
// GK_AchievementProgressResult
struct GK_AchievementProgressResult_t3539574352;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_LeaderboardResult866080833.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_AchievementProgre3539574352.h"

// System.Void GameCenterTvOSExample::.ctor()
extern "C"  void GameCenterTvOSExample__ctor_m988765912 (GameCenterTvOSExample_t913629997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::Start()
extern "C"  void GameCenterTvOSExample_Start_m1850553996 (GameCenterTvOSExample_t913629997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::OnAuthFinished(SA.Common.Models.Result)
extern "C"  void GameCenterTvOSExample_OnAuthFinished_m293666189 (GameCenterTvOSExample_t913629997 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::ShowAchivemnets()
extern "C"  void GameCenterTvOSExample_ShowAchivemnets_m3910456966 (GameCenterTvOSExample_t913629997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::SubmitAchievement()
extern "C"  void GameCenterTvOSExample_SubmitAchievement_m4124659445 (GameCenterTvOSExample_t913629997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::ResetAchievements()
extern "C"  void GameCenterTvOSExample_ResetAchievements_m1990205423 (GameCenterTvOSExample_t913629997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::ShowLeaderboards()
extern "C"  void GameCenterTvOSExample_ShowLeaderboards_m2160810027 (GameCenterTvOSExample_t913629997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::ShowLeaderboardByID()
extern "C"  void GameCenterTvOSExample_ShowLeaderboardByID_m2502065480 (GameCenterTvOSExample_t913629997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::ReportScore()
extern "C"  void GameCenterTvOSExample_ReportScore_m3008911124 (GameCenterTvOSExample_t913629997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::OnScoreSubmitted(GK_LeaderboardResult)
extern "C"  void GameCenterTvOSExample_OnScoreSubmitted_m4037005641 (GameCenterTvOSExample_t913629997 * __this, GK_LeaderboardResult_t866080833 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::OnAchievementsLoaded(SA.Common.Models.Result)
extern "C"  void GameCenterTvOSExample_OnAchievementsLoaded_m1441956652 (GameCenterTvOSExample_t913629997 * __this, Result_t4287219743 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::HandleOnAchievementsReset(SA.Common.Models.Result)
extern "C"  void GameCenterTvOSExample_HandleOnAchievementsReset_m681350808 (GameCenterTvOSExample_t913629997 * __this, Result_t4287219743 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::HandleOnAchievementsProgress(GK_AchievementProgressResult)
extern "C"  void GameCenterTvOSExample_HandleOnAchievementsProgress_m4261335498 (GameCenterTvOSExample_t913629997 * __this, GK_AchievementProgressResult_t3539574352 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::GameCenterManager_OnGameCenterViewDismissed()
extern "C"  void GameCenterTvOSExample_GameCenterManager_OnGameCenterViewDismissed_m4215493373 (GameCenterTvOSExample_t913629997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::<OnAuthFinished>m__78()
extern "C"  void GameCenterTvOSExample_U3COnAuthFinishedU3Em__78_m1077022121 (GameCenterTvOSExample_t913629997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterTvOSExample::<ShowLeaderboardByID>m__79(SA.Common.Models.Result)
extern "C"  void GameCenterTvOSExample_U3CShowLeaderboardByIDU3Em__79_m2194500619 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
