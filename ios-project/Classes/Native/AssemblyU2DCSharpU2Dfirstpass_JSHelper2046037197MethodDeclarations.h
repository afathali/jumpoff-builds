﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSHelper
struct JSHelper_t2046037197;
// System.String
struct String_t;
// SA.Common.Models.Result
struct Result_t4287219743;
// GK_AchievementProgressResult
struct GK_AchievementProgressResult_t3539574352;
// GK_LeaderboardResult
struct GK_LeaderboardResult_t866080833;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_AchievementProgre3539574352.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_LeaderboardResult866080833.h"

// System.Void JSHelper::.ctor()
extern "C"  void JSHelper__ctor_m1265971444 (JSHelper_t2046037197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::InitGameCneter()
extern "C"  void JSHelper_InitGameCneter_m257049697 (JSHelper_t2046037197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::SubmitScore(System.Int32)
extern "C"  void JSHelper_SubmitScore_m3387936747 (JSHelper_t2046037197 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::SubmitAchievement(System.String)
extern "C"  void JSHelper_SubmitAchievement_m2339124711 (JSHelper_t2046037197 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::HandleOnAchievementsLoaded(SA.Common.Models.Result)
extern "C"  void JSHelper_HandleOnAchievementsLoaded_m1850534526 (JSHelper_t2046037197 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::HandleOnAchievementsProgress(GK_AchievementProgressResult)
extern "C"  void JSHelper_HandleOnAchievementsProgress_m782365878 (JSHelper_t2046037197 * __this, GK_AchievementProgressResult_t3539574352 * ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::HandleOnAchievementsReset(SA.Common.Models.Result)
extern "C"  void JSHelper_HandleOnAchievementsReset_m2029546708 (JSHelper_t2046037197 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::OnScoreSubmitted(GK_LeaderboardResult)
extern "C"  void JSHelper_OnScoreSubmitted_m3658851009 (JSHelper_t2046037197 * __this, GK_LeaderboardResult_t866080833 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSHelper::HandleOnAuthFinished(SA.Common.Models.Result)
extern "C"  void JSHelper_HandleOnAuthFinished_m4134314063 (JSHelper_t2046037197 * __this, Result_t4287219743 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
