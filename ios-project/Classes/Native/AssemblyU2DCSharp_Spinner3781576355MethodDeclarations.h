﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Spinner
struct Spinner_t3781576355;

#include "codegen/il2cpp-codegen.h"

// System.Void Spinner::.ctor()
extern "C"  void Spinner__ctor_m289912328 (Spinner_t3781576355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spinner::.cctor()
extern "C"  void Spinner__cctor_m2287885017 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spinner::setVal(System.Int32)
extern "C"  void Spinner_setVal_m1944984044 (Spinner_t3781576355 * __this, int32_t ___newVal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spinner::init()
extern "C"  void Spinner_init_m2345105664 (Spinner_t3781576355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spinner::Start()
extern "C"  void Spinner_Start_m3608410196 (Spinner_t3781576355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Spinner::intToChar(System.Int32)
extern "C"  Il2CppChar Spinner_intToChar_m1712853147 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spinner::updateSpinner()
extern "C"  void Spinner_updateSpinner_m1190349826 (Spinner_t3781576355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spinner::Update()
extern "C"  void Spinner_Update_m293469643 (Spinner_t3781576355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spinner::<init>m__157()
extern "C"  void Spinner_U3CinitU3Em__157_m513112202 (Spinner_t3781576355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spinner::<init>m__158()
extern "C"  void Spinner_U3CinitU3Em__158_m513112115 (Spinner_t3781576355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
