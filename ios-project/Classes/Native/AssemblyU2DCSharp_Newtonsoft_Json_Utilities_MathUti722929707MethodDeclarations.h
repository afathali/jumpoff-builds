﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.MathUtils
struct MathUtils_t722929707;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen2341081996.h"

// System.Void Newtonsoft.Json.Utilities.MathUtils::.ctor()
extern "C"  void MathUtils__ctor_m3927409723 (MathUtils_t722929707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Utilities.MathUtils::IntLength(System.Int32)
extern "C"  int32_t MathUtils_IntLength_m1607178455 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Utilities.MathUtils::HexToInt(System.Char)
extern "C"  int32_t MathUtils_HexToInt_m1629087511 (Il2CppObject * __this /* static, unused */, Il2CppChar ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.Utilities.MathUtils::IntToHex(System.Int32)
extern "C"  Il2CppChar MathUtils_IntToHex_m3771652151 (Il2CppObject * __this /* static, unused */, int32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Utilities.MathUtils::GetDecimalPlaces(System.Double)
extern "C"  int32_t MathUtils_GetDecimalPlaces_m1147755822 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Utilities.MathUtils::Min(System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>)
extern "C"  Nullable_1_t334943763  MathUtils_Min_m2363402842 (Il2CppObject * __this /* static, unused */, Nullable_1_t334943763  ___val10, Nullable_1_t334943763  ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Utilities.MathUtils::Max(System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>)
extern "C"  Nullable_1_t334943763  MathUtils_Max_m1700175456 (Il2CppObject * __this /* static, unused */, Nullable_1_t334943763  ___val10, Nullable_1_t334943763  ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.Utilities.MathUtils::Min(System.Nullable`1<System.Double>,System.Nullable`1<System.Double>)
extern "C"  Nullable_1_t2341081996  MathUtils_Min_m627978589 (Il2CppObject * __this /* static, unused */, Nullable_1_t2341081996  ___val10, Nullable_1_t2341081996  ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.Utilities.MathUtils::Max(System.Nullable`1<System.Double>,System.Nullable`1<System.Double>)
extern "C"  Nullable_1_t2341081996  MathUtils_Max_m1415781971 (Il2CppObject * __this /* static, unused */, Nullable_1_t2341081996  ___val10, Nullable_1_t2341081996  ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.MathUtils::ApproxEquals(System.Double,System.Double)
extern "C"  bool MathUtils_ApproxEquals_m1065492404 (Il2CppObject * __this /* static, unused */, double ___d10, double ___d21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
