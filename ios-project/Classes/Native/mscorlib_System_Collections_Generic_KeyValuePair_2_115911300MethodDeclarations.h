﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_115911300.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1576491477_gshared (KeyValuePair_2_t115911300 * __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1576491477(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t115911300 *, KeyValuePair_2_t3132015601 , int32_t, const MethodInfo*))KeyValuePair_2__ctor_m1576491477_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Key()
extern "C"  KeyValuePair_2_t3132015601  KeyValuePair_2_get_Key_m3310904759_gshared (KeyValuePair_2_t115911300 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m3310904759(__this, method) ((  KeyValuePair_2_t3132015601  (*) (KeyValuePair_2_t115911300 *, const MethodInfo*))KeyValuePair_2_get_Key_m3310904759_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2320304504_gshared (KeyValuePair_2_t115911300 * __this, KeyValuePair_2_t3132015601  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2320304504(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t115911300 *, KeyValuePair_2_t3132015601 , const MethodInfo*))KeyValuePair_2_set_Key_m2320304504_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2441171031_gshared (KeyValuePair_2_t115911300 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2441171031(__this, method) ((  int32_t (*) (KeyValuePair_2_t115911300 *, const MethodInfo*))KeyValuePair_2_get_Value_m2441171031_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2305444424_gshared (KeyValuePair_2_t115911300 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2305444424(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t115911300 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m2305444424_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2468585952_gshared (KeyValuePair_2_t115911300 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2468585952(__this, method) ((  String_t* (*) (KeyValuePair_2_t115911300 *, const MethodInfo*))KeyValuePair_2_ToString_m2468585952_gshared)(__this, method)
