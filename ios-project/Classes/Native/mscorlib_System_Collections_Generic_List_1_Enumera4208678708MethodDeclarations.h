﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<JumpPoint>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3085835741(__this, ___l0, method) ((  void (*) (Enumerator_t4208678708 *, List_1_t378981738 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JumpPoint>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m473492629(__this, method) ((  void (*) (Enumerator_t4208678708 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<JumpPoint>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1355038709(__this, method) ((  Il2CppObject * (*) (Enumerator_t4208678708 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JumpPoint>::Dispose()
#define Enumerator_Dispose_m2171997994(__this, method) ((  void (*) (Enumerator_t4208678708 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JumpPoint>::VerifyState()
#define Enumerator_VerifyState_m399571455(__this, method) ((  void (*) (Enumerator_t4208678708 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<JumpPoint>::MoveNext()
#define Enumerator_MoveNext_m305300510(__this, method) ((  bool (*) (Enumerator_t4208678708 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<JumpPoint>::get_Current()
#define Enumerator_get_Current_m2134868468(__this, method) ((  JumpPoint_t1009860606 * (*) (Enumerator_t4208678708 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
