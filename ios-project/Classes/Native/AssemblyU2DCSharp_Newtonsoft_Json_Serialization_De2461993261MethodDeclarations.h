﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultSerializationBinder
struct DefaultSerializationBinder_t2461993261;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De3055062677.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::.ctor()
extern "C"  void DefaultSerializationBinder__ctor_m3246991559 (DefaultSerializationBinder_t2461993261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::.cctor()
extern "C"  void DefaultSerializationBinder__cctor_m3364220358 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::GetTypeFromTypeNameKey(Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey)
extern "C"  Type_t * DefaultSerializationBinder_GetTypeFromTypeNameKey_m1478349144 (Il2CppObject * __this /* static, unused */, TypeNameKey_t3055062677  ___typeNameKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::BindToType(System.String,System.String)
extern "C"  Type_t * DefaultSerializationBinder_BindToType_m1040878909 (DefaultSerializationBinder_t2461993261 * __this, String_t* ___assemblyName0, String_t* ___typeName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
