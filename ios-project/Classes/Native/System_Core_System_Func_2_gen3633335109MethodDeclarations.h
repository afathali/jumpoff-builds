﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3155315995MethodDeclarations.h"

// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1484960465(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3633335109 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3625226049_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m2078813219(__this, ___arg10, method) ((  bool (*) (Func_2_t3633335109 *, KeyValuePair_2_t3497699202 , const MethodInfo*))Func_2_Invoke_m2007717759_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m299088770(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3633335109 *, KeyValuePair_2_t3497699202 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4076199064_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1711190277(__this, ___result0, method) ((  bool (*) (Func_2_t3633335109 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1410626241_gshared)(__this, ___result0, method)
