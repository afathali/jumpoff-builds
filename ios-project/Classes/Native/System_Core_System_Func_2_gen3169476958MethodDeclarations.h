﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2691457844MethodDeclarations.h"

// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3130007923(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3169476958 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m932920331_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m1902298885(__this, ___arg10, method) ((  bool (*) (Func_2_t3169476958 *, KeyValuePair_2_t1414869661 , const MethodInfo*))Func_2_Invoke_m2632170457_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m942597816(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3169476958 *, KeyValuePair_2_t1414869661 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3215041470_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m187671687(__this, ___result0, method) ((  bool (*) (Func_2_t3169476958 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m2540981435_gshared)(__this, ___result0, method)
