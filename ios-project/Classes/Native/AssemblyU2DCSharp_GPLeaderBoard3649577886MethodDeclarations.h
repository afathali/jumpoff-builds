﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPLeaderBoard
struct GPLeaderBoard_t3649577886;
// System.String
struct String_t;
// System.Collections.Generic.List`1<GPScore>
struct List_1_t2588610021;
// GPScore
struct GPScore_t3219488889;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Collections.Generic.Dictionary`2<System.Int32,GP_LocalPlayerScoreUpdateListener>
struct Dictionary_2_t3460919322;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GPBoardTimeSpan42003024.h"
#include "AssemblyU2DCSharp_GPCollectionType2617299399.h"
#include "AssemblyU2DCSharp_GPScore3219488889.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void GPLeaderBoard::.ctor(System.String,System.String)
extern "C"  void GPLeaderBoard__ctor_m2451529085 (GPLeaderBoard_t3649577886 * __this, String_t* ___lId0, String_t* ___lName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPLeaderBoard::UpdateName(System.String)
extern "C"  void GPLeaderBoard_UpdateName_m3457519421 (GPLeaderBoard_t3649577886 * __this, String_t* ___lName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GPScore> GPLeaderBoard::GetScoresList(GPBoardTimeSpan,GPCollectionType)
extern "C"  List_1_t2588610021 * GPLeaderBoard_GetScoresList_m1399999169 (GPLeaderBoard_t3649577886 * __this, int32_t ___timeSpan0, int32_t ___collection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPScore GPLeaderBoard::GetScoreByPlayerId(System.String,GPBoardTimeSpan,GPCollectionType)
extern "C"  GPScore_t3219488889 * GPLeaderBoard_GetScoreByPlayerId_m4033957241 (GPLeaderBoard_t3649577886 * __this, String_t* ___playerId0, int32_t ___timeSpan1, int32_t ___collection2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPScore GPLeaderBoard::GetScore(System.Int32,GPBoardTimeSpan,GPCollectionType)
extern "C"  GPScore_t3219488889 * GPLeaderBoard_GetScore_m2309824915 (GPLeaderBoard_t3649577886 * __this, int32_t ___rank0, int32_t ___timeSpan1, int32_t ___collection2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPScore GPLeaderBoard::GetCurrentPlayerScore(GPBoardTimeSpan,GPCollectionType)
extern "C"  GPScore_t3219488889 * GPLeaderBoard_GetCurrentPlayerScore_m2450221146 (GPLeaderBoard_t3649577886 * __this, int32_t ___timeSpan0, int32_t ___collection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPLeaderBoard::CreateScoreListener(System.Int32)
extern "C"  void GPLeaderBoard_CreateScoreListener_m380614438 (GPLeaderBoard_t3649577886 * __this, int32_t ___requestId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPLeaderBoard::ReportLocalPlayerScoreUpdate(GPScore,System.Int32)
extern "C"  void GPLeaderBoard_ReportLocalPlayerScoreUpdate_m1868193116 (GPLeaderBoard_t3649577886 * __this, GPScore_t3219488889 * ___score0, int32_t ___requestId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPLeaderBoard::ReportLocalPlayerScoreUpdateFail(System.String,System.Int32)
extern "C"  void GPLeaderBoard_ReportLocalPlayerScoreUpdateFail_m2035478005 (GPLeaderBoard_t3649577886 * __this, String_t* ___errorData0, int32_t ___requestId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPLeaderBoard::UpdateCurrentPlayerScore(System.Collections.Generic.List`1<GPScore>)
extern "C"  void GPLeaderBoard_UpdateCurrentPlayerScore_m1562302399 (GPLeaderBoard_t3649577886 * __this, List_1_t2588610021 * ___newScores0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPLeaderBoard::UpdateCurrentPlayerScore(GPScore)
extern "C"  void GPLeaderBoard_UpdateCurrentPlayerScore_m846415745 (GPLeaderBoard_t3649577886 * __this, GPScore_t3219488889 * ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPLeaderBoard::UpdateScore(GPScore)
extern "C"  void GPLeaderBoard_UpdateScore_m3353269679 (GPLeaderBoard_t3649577886 * __this, GPScore_t3219488889 * ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPLeaderBoard::get_Id()
extern "C"  String_t* GPLeaderBoard_get_Id_m3089493774 (GPLeaderBoard_t3649577886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPLeaderBoard::set_Id(System.String)
extern "C"  void GPLeaderBoard_set_Id_m1616848191 (GPLeaderBoard_t3649577886 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPLeaderBoard::get_Name()
extern "C"  String_t* GPLeaderBoard_get_Name_m1003522976 (GPLeaderBoard_t3649577886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPLeaderBoard::set_Name(System.String)
extern "C"  void GPLeaderBoard_set_Name_m2111601373 (GPLeaderBoard_t3649577886 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPLeaderBoard::get_Description()
extern "C"  String_t* GPLeaderBoard_get_Description_m2120074239 (GPLeaderBoard_t3649577886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPLeaderBoard::set_Description(System.String)
extern "C"  void GPLeaderBoard_set_Description_m2606587298 (GPLeaderBoard_t3649577886 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GPLeaderBoard::get_Texture()
extern "C"  Texture2D_t3542995729 * GPLeaderBoard_get_Texture_m237625648 (GPLeaderBoard_t3649577886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPLeaderBoard::set_Texture(UnityEngine.Texture2D)
extern "C"  void GPLeaderBoard_set_Texture_m3171329521 (GPLeaderBoard_t3649577886 * __this, Texture2D_t3542995729 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,GP_LocalPlayerScoreUpdateListener> GPLeaderBoard::get_ScoreUpdateListners()
extern "C"  Dictionary_2_t3460919322 * GPLeaderBoard_get_ScoreUpdateListners_m2811690469 (GPLeaderBoard_t3649577886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
