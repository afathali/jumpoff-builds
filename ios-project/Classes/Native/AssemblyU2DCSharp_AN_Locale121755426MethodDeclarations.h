﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_Locale
struct AN_Locale_t121755426;

#include "codegen/il2cpp-codegen.h"

// System.Void AN_Locale::.ctor()
extern "C"  void AN_Locale__ctor_m3387698995 (AN_Locale_t121755426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
