﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GP_Event>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m111906247(__this, ___l0, method) ((  void (*) (Enumerator_t3521961770 *, List_1_t3987232096 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GP_Event>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1192798971(__this, method) ((  void (*) (Enumerator_t3521961770 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GP_Event>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1440561979(__this, method) ((  Il2CppObject * (*) (Enumerator_t3521961770 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GP_Event>::Dispose()
#define Enumerator_Dispose_m2284366096(__this, method) ((  void (*) (Enumerator_t3521961770 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GP_Event>::VerifyState()
#define Enumerator_VerifyState_m537254613(__this, method) ((  void (*) (Enumerator_t3521961770 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GP_Event>::MoveNext()
#define Enumerator_MoveNext_m1750644750(__this, method) ((  bool (*) (Enumerator_t3521961770 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GP_Event>::get_Current()
#define Enumerator_get_Current_m2303512416(__this, method) ((  GP_Event_t323143668 * (*) (Enumerator_t3521961770 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
