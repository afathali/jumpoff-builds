﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_Snapshot
struct GP_Snapshot_t1489095664;

#include "codegen/il2cpp-codegen.h"

// System.Void GP_Snapshot::.ctor()
extern "C"  void GP_Snapshot__ctor_m49923843 (GP_Snapshot_t1489095664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
