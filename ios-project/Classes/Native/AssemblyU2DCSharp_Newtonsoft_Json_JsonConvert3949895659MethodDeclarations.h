﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.Enum
struct Enum_t2459695545;
// System.Uri
struct Uri_t19570940;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t3128012475;
// Newtonsoft.Json.JsonSerializerSettings
struct JsonSerializerSettings_t842388167;
// System.Xml.XmlNode
struct XmlNode_t616554813;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_DateTimeOffset1362988906.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_IO_TextWriter4027217640.h"
#include "mscorlib_System_DateTimeKind2186819611.h"
#include "mscorlib_System_Enum2459695545.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Guid2533601593.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_TypeCode2536926201.h"
#include "mscorlib_System_Type1303803226.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Formatting4009318759.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializerSet842388167.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"

// System.Void Newtonsoft.Json.JsonConvert::.cctor()
extern "C"  void JsonConvert__cctor_m3451089456 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.DateTime)
extern "C"  String_t* JsonConvert_ToString_m4083164292 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.DateTimeOffset)
extern "C"  String_t* JsonConvert_ToString_m4184408799 (Il2CppObject * __this /* static, unused */, DateTimeOffset_t1362988906  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan Newtonsoft.Json.JsonConvert::GetUtcOffset(System.DateTime)
extern "C"  TimeSpan_t3430258949  JsonConvert_GetUtcOffset_m2298188781 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::WriteDateTimeString(System.IO.TextWriter,System.DateTime)
extern "C"  void JsonConvert_WriteDateTimeString_m3943429897 (Il2CppObject * __this /* static, unused */, TextWriter_t4027217640 * ___writer0, DateTime_t693205669  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::WriteDateTimeString(System.IO.TextWriter,System.DateTime,System.TimeSpan,System.DateTimeKind)
extern "C"  void JsonConvert_WriteDateTimeString_m2878042529 (Il2CppObject * __this /* static, unused */, TextWriter_t4027217640 * ___writer0, DateTime_t693205669  ___value1, TimeSpan_t3430258949  ___offset2, int32_t ___kind3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::ToUniversalTicks(System.DateTime)
extern "C"  int64_t JsonConvert_ToUniversalTicks_m154658882 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::ToUniversalTicks(System.DateTime,System.TimeSpan)
extern "C"  int64_t JsonConvert_ToUniversalTicks_m3510287098 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___dateTime0, TimeSpan_t3430258949  ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::ConvertDateTimeToJavaScriptTicks(System.DateTime,System.TimeSpan)
extern "C"  int64_t JsonConvert_ConvertDateTimeToJavaScriptTicks_m2367486536 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___dateTime0, TimeSpan_t3430258949  ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::ConvertDateTimeToJavaScriptTicks(System.DateTime)
extern "C"  int64_t JsonConvert_ConvertDateTimeToJavaScriptTicks_m3725525504 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::ConvertDateTimeToJavaScriptTicks(System.DateTime,System.Boolean)
extern "C"  int64_t JsonConvert_ConvertDateTimeToJavaScriptTicks_m2287407327 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___dateTime0, bool ___convertToUtc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::UniversialTicksToJavaScriptTicks(System.Int64)
extern "C"  int64_t JsonConvert_UniversialTicksToJavaScriptTicks_m3561108232 (Il2CppObject * __this /* static, unused */, int64_t ___universialTicks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Newtonsoft.Json.JsonConvert::ConvertJavaScriptTicksToDateTime(System.Int64)
extern "C"  DateTime_t693205669  JsonConvert_ConvertJavaScriptTicksToDateTime_m2567476000 (Il2CppObject * __this /* static, unused */, int64_t ___javaScriptTicks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Boolean)
extern "C"  String_t* JsonConvert_ToString_m378517807 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Char)
extern "C"  String_t* JsonConvert_ToString_m2029005487 (Il2CppObject * __this /* static, unused */, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Enum)
extern "C"  String_t* JsonConvert_ToString_m497195744 (Il2CppObject * __this /* static, unused */, Enum_t2459695545 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Int32)
extern "C"  String_t* JsonConvert_ToString_m3337727173 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Int16)
extern "C"  String_t* JsonConvert_ToString_m1368358771 (Il2CppObject * __this /* static, unused */, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.UInt16)
extern "C"  String_t* JsonConvert_ToString_m3186047926 (Il2CppObject * __this /* static, unused */, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.UInt32)
extern "C"  String_t* JsonConvert_ToString_m3468372924 (Il2CppObject * __this /* static, unused */, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Int64)
extern "C"  String_t* JsonConvert_ToString_m205559518 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.UInt64)
extern "C"  String_t* JsonConvert_ToString_m1283936599 (Il2CppObject * __this /* static, unused */, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Single)
extern "C"  String_t* JsonConvert_ToString_m909328605 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Double)
extern "C"  String_t* JsonConvert_ToString_m308021000 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::EnsureDecimalPlace(System.Double,System.String)
extern "C"  String_t* JsonConvert_EnsureDecimalPlace_m3959687584 (Il2CppObject * __this /* static, unused */, double ___value0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::EnsureDecimalPlace(System.String)
extern "C"  String_t* JsonConvert_EnsureDecimalPlace_m3104964532 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Byte)
extern "C"  String_t* JsonConvert_ToString_m1557947437 (Il2CppObject * __this /* static, unused */, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.SByte)
extern "C"  String_t* JsonConvert_ToString_m1359036958 (Il2CppObject * __this /* static, unused */, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Decimal)
extern "C"  String_t* JsonConvert_ToString_m4218694386 (Il2CppObject * __this /* static, unused */, Decimal_t724701077  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Guid)
extern "C"  String_t* JsonConvert_ToString_m1029380288 (Il2CppObject * __this /* static, unused */, Guid_t2533601593  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.TimeSpan)
extern "C"  String_t* JsonConvert_ToString_m781736212 (Il2CppObject * __this /* static, unused */, TimeSpan_t3430258949  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Uri)
extern "C"  String_t* JsonConvert_ToString_m3514332257 (Il2CppObject * __this /* static, unused */, Uri_t19570940 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.String)
extern "C"  String_t* JsonConvert_ToString_m1886006944 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.String,System.Char)
extern "C"  String_t* JsonConvert_ToString_m2968147597 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Il2CppChar ___delimter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Object)
extern "C"  String_t* JsonConvert_ToString_m2866866378 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonConvert::IsJsonPrimitiveTypeCode(System.TypeCode)
extern "C"  bool JsonConvert_IsJsonPrimitiveTypeCode_m1336846167 (Il2CppObject * __this /* static, unused */, int32_t ___typeCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonConvert::IsJsonPrimitiveType(System.Type)
extern "C"  bool JsonConvert_IsJsonPrimitiveType_m288566679 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonConvert::IsJsonPrimitive(System.Object)
extern "C"  bool JsonConvert_IsJsonPrimitive_m100951668 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object)
extern "C"  String_t* JsonConvert_SerializeObject_m3256232875 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,Newtonsoft.Json.Formatting)
extern "C"  String_t* JsonConvert_SerializeObject_m3860727631 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___formatting1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,Newtonsoft.Json.JsonConverter[])
extern "C"  String_t* JsonConvert_SerializeObject_m1579294568 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, JsonConverterU5BU5D_t3128012475* ___converters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,Newtonsoft.Json.Formatting,Newtonsoft.Json.JsonConverter[])
extern "C"  String_t* JsonConvert_SerializeObject_m1447043134 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___formatting1, JsonConverterU5BU5D_t3128012475* ___converters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,Newtonsoft.Json.Formatting,Newtonsoft.Json.JsonSerializerSettings)
extern "C"  String_t* JsonConvert_SerializeObject_m1472570157 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___formatting1, JsonSerializerSettings_t842388167 * ___settings2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String)
extern "C"  Il2CppObject * JsonConvert_DeserializeObject_m2958357244 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,Newtonsoft.Json.JsonSerializerSettings)
extern "C"  Il2CppObject * JsonConvert_DeserializeObject_m172616000 (Il2CppObject * __this /* static, unused */, String_t* ___value0, JsonSerializerSettings_t842388167 * ___settings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,System.Type)
extern "C"  Il2CppObject * JsonConvert_DeserializeObject_m2749120137 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,System.Type,Newtonsoft.Json.JsonConverter[])
extern "C"  Il2CppObject * JsonConvert_DeserializeObject_m2011931944 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Type_t * ___type1, JsonConverterU5BU5D_t3128012475* ___converters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,System.Type,Newtonsoft.Json.JsonSerializerSettings)
extern "C"  Il2CppObject * JsonConvert_DeserializeObject_m2325759671 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Type_t * ___type1, JsonSerializerSettings_t842388167 * ___settings2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::PopulateObject(System.String,System.Object)
extern "C"  void JsonConvert_PopulateObject_m3452096312 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::PopulateObject(System.String,System.Object,Newtonsoft.Json.JsonSerializerSettings)
extern "C"  void JsonConvert_PopulateObject_m3337616636 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Il2CppObject * ___target1, JsonSerializerSettings_t842388167 * ___settings2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeXmlNode(System.Xml.XmlNode)
extern "C"  String_t* JsonConvert_SerializeXmlNode_m774326974 (Il2CppObject * __this /* static, unused */, XmlNode_t616554813 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeXmlNode(System.Xml.XmlNode,Newtonsoft.Json.Formatting)
extern "C"  String_t* JsonConvert_SerializeXmlNode_m3180193118 (Il2CppObject * __this /* static, unused */, XmlNode_t616554813 * ___node0, int32_t ___formatting1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeXmlNode(System.Xml.XmlNode,Newtonsoft.Json.Formatting,System.Boolean)
extern "C"  String_t* JsonConvert_SerializeXmlNode_m455247279 (Il2CppObject * __this /* static, unused */, XmlNode_t616554813 * ___node0, int32_t ___formatting1, bool ___omitRootObject2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlDocument Newtonsoft.Json.JsonConvert::DeserializeXmlNode(System.String)
extern "C"  XmlDocument_t3649534162 * JsonConvert_DeserializeXmlNode_m876356742 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlDocument Newtonsoft.Json.JsonConvert::DeserializeXmlNode(System.String,System.String)
extern "C"  XmlDocument_t3649534162 * JsonConvert_DeserializeXmlNode_m3339872442 (Il2CppObject * __this /* static, unused */, String_t* ___value0, String_t* ___deserializeRootElementName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlDocument Newtonsoft.Json.JsonConvert::DeserializeXmlNode(System.String,System.String,System.Boolean)
extern "C"  XmlDocument_t3649534162 * JsonConvert_DeserializeXmlNode_m4176978683 (Il2CppObject * __this /* static, unused */, String_t* ___value0, String_t* ___deserializeRootElementName1, bool ___writeArrayAttribute2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
