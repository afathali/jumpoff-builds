﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlNodeConverter/<ReadArrayElements>c__AnonStorey1B
struct U3CReadArrayElementsU3Ec__AnonStorey1B_t913630666;
// Newtonsoft.Json.Converters.IXmlElement
struct IXmlElement_t2005722770;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Converters.XmlNodeConverter/<ReadArrayElements>c__AnonStorey1B::.ctor()
extern "C"  void U3CReadArrayElementsU3Ec__AnonStorey1B__ctor_m2468273695 (U3CReadArrayElementsU3Ec__AnonStorey1B_t913630666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter/<ReadArrayElements>c__AnonStorey1B::<>m__C3(Newtonsoft.Json.Converters.IXmlElement)
extern "C"  bool U3CReadArrayElementsU3Ec__AnonStorey1B_U3CU3Em__C3_m4158826380 (U3CReadArrayElementsU3Ec__AnonStorey1B_t913630666 * __this, Il2CppObject * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
