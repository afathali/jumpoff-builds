﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidABOrganization
struct AndroidABOrganization_t1539209357;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidABOrganization::.ctor()
extern "C"  void AndroidABOrganization__ctor_m1628004230 (AndroidABOrganization_t1539209357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
