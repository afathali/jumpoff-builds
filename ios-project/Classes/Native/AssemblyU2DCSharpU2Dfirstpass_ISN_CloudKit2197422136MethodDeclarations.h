﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_CloudKit
struct ISN_CloudKit_t2197422136;
// CK_Database
struct CK_Database_t243306482;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ISN_CloudKit::.ctor()
extern "C"  void ISN_CloudKit__ctor_m4116253693 (ISN_CloudKit_t2197422136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::Awake()
extern "C"  void ISN_CloudKit_Awake_m1711901058 (ISN_CloudKit_t2197422136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CK_Database ISN_CloudKit::get_PrivateDB()
extern "C"  CK_Database_t243306482 * ISN_CloudKit_get_PrivateDB_m364089878 (ISN_CloudKit_t2197422136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CK_Database ISN_CloudKit::get_PublicDB()
extern "C"  CK_Database_t243306482 * ISN_CloudKit_get_PublicDB_m769951248 (ISN_CloudKit_t2197422136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::OnSaveRecordSuccess(System.String)
extern "C"  void ISN_CloudKit_OnSaveRecordSuccess_m1613519257 (ISN_CloudKit_t2197422136 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::OnSaveRecordFailed(System.String)
extern "C"  void ISN_CloudKit_OnSaveRecordFailed_m426125417 (ISN_CloudKit_t2197422136 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::OnDeleteRecordSuccess(System.String)
extern "C"  void ISN_CloudKit_OnDeleteRecordSuccess_m1275453077 (ISN_CloudKit_t2197422136 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::OnDeleteRecordFailed(System.String)
extern "C"  void ISN_CloudKit_OnDeleteRecordFailed_m1233124893 (ISN_CloudKit_t2197422136 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::OnPerformQuerySuccess(System.String)
extern "C"  void ISN_CloudKit_OnPerformQuerySuccess_m2534295238 (ISN_CloudKit_t2197422136 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::OnPerformQueryFailed(System.String)
extern "C"  void ISN_CloudKit_OnPerformQueryFailed_m2086289252 (ISN_CloudKit_t2197422136 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::OnFetchRecordSuccess(System.String)
extern "C"  void ISN_CloudKit_OnFetchRecordSuccess_m941331398 (ISN_CloudKit_t2197422136 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::OnFetchRecordFailed(System.String)
extern "C"  void ISN_CloudKit_OnFetchRecordFailed_m2284584436 (ISN_CloudKit_t2197422136 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::CreateRecordId_Object(System.Int32,System.String)
extern "C"  void ISN_CloudKit_CreateRecordId_Object_m1453825022 (Il2CppObject * __this /* static, unused */, int32_t ___recordId0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::UpdateRecord_Object(System.Int32,System.String,System.String,System.String,System.Int32)
extern "C"  void ISN_CloudKit_UpdateRecord_Object_m3890012173 (Il2CppObject * __this /* static, unused */, int32_t ___ID0, String_t* ___type1, String_t* ___keys2, String_t* ___values3, int32_t ___recordId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::SaveRecord(System.Int32,System.Int32)
extern "C"  void ISN_CloudKit_SaveRecord_m1575989481 (Il2CppObject * __this /* static, unused */, int32_t ___dbId0, int32_t ___recordId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::DeleteRecord(System.Int32,System.Int32)
extern "C"  void ISN_CloudKit_DeleteRecord_m2565319973 (Il2CppObject * __this /* static, unused */, int32_t ___dbId0, int32_t ___recordId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::FetchRecord(System.Int32,System.Int32)
extern "C"  void ISN_CloudKit_FetchRecord_m1712777808 (Il2CppObject * __this /* static, unused */, int32_t ___dbId0, int32_t ___recordId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CloudKit::PerformQuery(System.Int32,System.String,System.String)
extern "C"  void ISN_CloudKit_PerformQuery_m804482595 (Il2CppObject * __this /* static, unused */, int32_t ___dbId0, String_t* ___query1, String_t* ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
