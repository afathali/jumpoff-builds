﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSSocialManager
struct IOSSocialManager_t2957403963;
// System.Action
struct Action_t3226471752;
// System.Action`1<SA.Common.Models.Result>
struct Action_1_t4089019125;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// SA.Common.Models.Result
struct Result_t4287219743;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"

// System.Void IOSSocialManager::.ctor()
extern "C"  void IOSSocialManager__ctor_m1850707842 (IOSSocialManager_t2957403963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::.cctor()
extern "C"  void IOSSocialManager__cctor_m1765628291 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::add_OnFacebookPostStart(System.Action)
extern "C"  void IOSSocialManager_add_OnFacebookPostStart_m356920418 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::remove_OnFacebookPostStart(System.Action)
extern "C"  void IOSSocialManager_remove_OnFacebookPostStart_m2504296581 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::add_OnTwitterPostStart(System.Action)
extern "C"  void IOSSocialManager_add_OnTwitterPostStart_m3659300599 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::remove_OnTwitterPostStart(System.Action)
extern "C"  void IOSSocialManager_remove_OnTwitterPostStart_m247767744 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::add_OnInstagramPostStart(System.Action)
extern "C"  void IOSSocialManager_add_OnInstagramPostStart_m3472089234 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::remove_OnInstagramPostStart(System.Action)
extern "C"  void IOSSocialManager_remove_OnInstagramPostStart_m2748981941 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::add_OnFacebookPostResult(System.Action`1<SA.Common.Models.Result>)
extern "C"  void IOSSocialManager_add_OnFacebookPostResult_m4202682424 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::remove_OnFacebookPostResult(System.Action`1<SA.Common.Models.Result>)
extern "C"  void IOSSocialManager_remove_OnFacebookPostResult_m2174053823 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::add_OnTwitterPostResult(System.Action`1<SA.Common.Models.Result>)
extern "C"  void IOSSocialManager_add_OnTwitterPostResult_m1846786549 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::remove_OnTwitterPostResult(System.Action`1<SA.Common.Models.Result>)
extern "C"  void IOSSocialManager_remove_OnTwitterPostResult_m2167237006 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::add_OnInstagramPostResult(System.Action`1<SA.Common.Models.Result>)
extern "C"  void IOSSocialManager_add_OnInstagramPostResult_m2065660588 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::remove_OnInstagramPostResult(System.Action`1<SA.Common.Models.Result>)
extern "C"  void IOSSocialManager_remove_OnInstagramPostResult_m873841199 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::add_OnMailResult(System.Action`1<SA.Common.Models.Result>)
extern "C"  void IOSSocialManager_add_OnMailResult_m843903749 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::remove_OnMailResult(System.Action`1<SA.Common.Models.Result>)
extern "C"  void IOSSocialManager_remove_OnMailResult_m2585317538 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::Awake()
extern "C"  void IOSSocialManager_Awake_m3741349893 (IOSSocialManager_t2957403963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::ShareMedia(System.String,UnityEngine.Texture2D)
extern "C"  void IOSSocialManager_ShareMedia_m1196552713 (IOSSocialManager_t2957403963 * __this, String_t* ___text0, Texture2D_t3542995729 * ___texture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::TwitterPost(System.String,System.String,UnityEngine.Texture2D)
extern "C"  void IOSSocialManager_TwitterPost_m3680658987 (IOSSocialManager_t2957403963 * __this, String_t* ___text0, String_t* ___url1, Texture2D_t3542995729 * ___texture2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::TwitterPostGif(System.String,System.String)
extern "C"  void IOSSocialManager_TwitterPostGif_m1894286749 (IOSSocialManager_t2957403963 * __this, String_t* ___text0, String_t* ___url1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::FacebookPost(System.String,System.String,UnityEngine.Texture2D)
extern "C"  void IOSSocialManager_FacebookPost_m1557350116 (IOSSocialManager_t2957403963 * __this, String_t* ___text0, String_t* ___url1, Texture2D_t3542995729 * ___texture2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::InstagramPost(UnityEngine.Texture2D)
extern "C"  void IOSSocialManager_InstagramPost_m780605942 (IOSSocialManager_t2957403963 * __this, Texture2D_t3542995729 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::InstagramPost(UnityEngine.Texture2D,System.String)
extern "C"  void IOSSocialManager_InstagramPost_m3885280290 (IOSSocialManager_t2957403963 * __this, Texture2D_t3542995729 * ___texture0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::WhatsAppShareText(System.String)
extern "C"  void IOSSocialManager_WhatsAppShareText_m1521018350 (IOSSocialManager_t2957403963 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::WhatsAppShareImage(UnityEngine.Texture2D)
extern "C"  void IOSSocialManager_WhatsAppShareImage_m3004926556 (IOSSocialManager_t2957403963 * __this, Texture2D_t3542995729 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::SendMail(System.String,System.String,System.String)
extern "C"  void IOSSocialManager_SendMail_m495141507 (IOSSocialManager_t2957403963 * __this, String_t* ___subject0, String_t* ___body1, String_t* ___recipients2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::SendMail(System.String,System.String,System.String,UnityEngine.Texture2D)
extern "C"  void IOSSocialManager_SendMail_m3444779057 (IOSSocialManager_t2957403963 * __this, String_t* ___subject0, String_t* ___body1, String_t* ___recipients2, Texture2D_t3542995729 * ___texture3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnTwitterPostFailed()
extern "C"  void IOSSocialManager_OnTwitterPostFailed_m1401965359 (IOSSocialManager_t2957403963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnTwitterPostSuccess()
extern "C"  void IOSSocialManager_OnTwitterPostSuccess_m1931197891 (IOSSocialManager_t2957403963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnFacebookPostFailed()
extern "C"  void IOSSocialManager_OnFacebookPostFailed_m228273322 (IOSSocialManager_t2957403963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnFacebookPostSuccess()
extern "C"  void IOSSocialManager_OnFacebookPostSuccess_m109639208 (IOSSocialManager_t2957403963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnMailFailed()
extern "C"  void IOSSocialManager_OnMailFailed_m1607877843 (IOSSocialManager_t2957403963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnMailSuccess()
extern "C"  void IOSSocialManager_OnMailSuccess_m3967689231 (IOSSocialManager_t2957403963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnInstaPostSuccess()
extern "C"  void IOSSocialManager_OnInstaPostSuccess_m2331569423 (IOSSocialManager_t2957403963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::OnInstaPostFailed(System.String)
extern "C"  void IOSSocialManager_OnInstaPostFailed_m3829751033 (IOSSocialManager_t2957403963 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::<OnFacebookPostStart>m__43()
extern "C"  void IOSSocialManager_U3COnFacebookPostStartU3Em__43_m2766948409 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::<OnTwitterPostStart>m__44()
extern "C"  void IOSSocialManager_U3COnTwitterPostStartU3Em__44_m1036533783 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::<OnInstagramPostStart>m__45()
extern "C"  void IOSSocialManager_U3COnInstagramPostStartU3Em__45_m3641792863 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::<OnFacebookPostResult>m__46(SA.Common.Models.Result)
extern "C"  void IOSSocialManager_U3COnFacebookPostResultU3Em__46_m3300152245 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::<OnTwitterPostResult>m__47(SA.Common.Models.Result)
extern "C"  void IOSSocialManager_U3COnTwitterPostResultU3Em__47_m3484304753 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::<OnInstagramPostResult>m__48(SA.Common.Models.Result)
extern "C"  void IOSSocialManager_U3COnInstagramPostResultU3Em__48_m3089599403 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialManager::<OnMailResult>m__49(SA.Common.Models.Result)
extern "C"  void IOSSocialManager_U3COnMailResultU3Em__49_m1939488403 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
