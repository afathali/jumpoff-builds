﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TwitterAndroidUseExample
struct TwitterAndroidUseExample_t3716461687;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// TW_APIRequstResult
struct TW_APIRequstResult_t455151055;
// TWResult
struct TWResult_t1480791060;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TW_APIRequstResult455151055.h"
#include "AssemblyU2DCSharp_TWResult1480791060.h"

// System.Void TwitterAndroidUseExample::.ctor()
extern "C"  void TwitterAndroidUseExample__ctor_m3028684496 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::.cctor()
extern "C"  void TwitterAndroidUseExample__cctor_m1583921361 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::Awake()
extern "C"  void TwitterAndroidUseExample_Awake_m1138923443 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::FixedUpdate()
extern "C"  void TwitterAndroidUseExample_FixedUpdate_m834922037 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::Connect()
extern "C"  void TwitterAndroidUseExample_Connect_m1940673030 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::PostWithAuthCheck()
extern "C"  void TwitterAndroidUseExample_PostWithAuthCheck_m3030743264 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::PostNativeScreenshot()
extern "C"  void TwitterAndroidUseExample_PostNativeScreenshot_m3656328347 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::PostMSG()
extern "C"  void TwitterAndroidUseExample_PostMSG_m3902714939 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::PostImage()
extern "C"  void TwitterAndroidUseExample_PostImage_m686919629 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TwitterAndroidUseExample::PostTWScreenshot()
extern "C"  Il2CppObject * TwitterAndroidUseExample_PostTWScreenshot_m3376347515 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::LoadUserData()
extern "C"  void TwitterAndroidUseExample_LoadUserData_m4005831689 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::Test()
extern "C"  void TwitterAndroidUseExample_Test_m1081943210 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::OnResult(TW_APIRequstResult)
extern "C"  void TwitterAndroidUseExample_OnResult_m3017830839 (TwitterAndroidUseExample_t3716461687 * __this, TW_APIRequstResult_t455151055 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::PostMessage()
extern "C"  void TwitterAndroidUseExample_PostMessage_m2960928897 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::PostScreehShot()
extern "C"  void TwitterAndroidUseExample_PostScreehShot_m3214950638 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::OnUserDataRequestCompleteAction(TWResult)
extern "C"  void TwitterAndroidUseExample_OnUserDataRequestCompleteAction_m4052876722 (TwitterAndroidUseExample_t3716461687 * __this, TWResult_t1480791060 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::OnPostingCompleteAction(TWResult)
extern "C"  void TwitterAndroidUseExample_OnPostingCompleteAction_m1263164080 (TwitterAndroidUseExample_t3716461687 * __this, TWResult_t1480791060 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::OnAuthCompleteAction(TWResult)
extern "C"  void TwitterAndroidUseExample_OnAuthCompleteAction_m3501154056 (TwitterAndroidUseExample_t3716461687 * __this, TWResult_t1480791060 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::OnTwitterInitedAction(TWResult)
extern "C"  void TwitterAndroidUseExample_OnTwitterInitedAction_m2738949181 (TwitterAndroidUseExample_t3716461687 * __this, TWResult_t1480791060 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::OnAuth()
extern "C"  void TwitterAndroidUseExample_OnAuth_m1598691707 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::RetrieveTimeLine()
extern "C"  void TwitterAndroidUseExample_RetrieveTimeLine_m1860969047 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::UserLookUpRequest()
extern "C"  void TwitterAndroidUseExample_UserLookUpRequest_m1011515106 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::FriedsidsRequest()
extern "C"  void TwitterAndroidUseExample_FriedsidsRequest_m721485134 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::FollowersidsRequest()
extern "C"  void TwitterAndroidUseExample_FollowersidsRequest_m4126401476 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::TweetSearch()
extern "C"  void TwitterAndroidUseExample_TweetSearch_m2097130215 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::OnIdsLoaded(TW_APIRequstResult)
extern "C"  void TwitterAndroidUseExample_OnIdsLoaded_m2592330663 (TwitterAndroidUseExample_t3716461687 * __this, TW_APIRequstResult_t455151055 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::OnLookUpRequestComplete(TW_APIRequstResult)
extern "C"  void TwitterAndroidUseExample_OnLookUpRequestComplete_m3832901118 (TwitterAndroidUseExample_t3716461687 * __this, TW_APIRequstResult_t455151055 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::OnSearchRequestComplete(TW_APIRequstResult)
extern "C"  void TwitterAndroidUseExample_OnSearchRequestComplete_m724304128 (TwitterAndroidUseExample_t3716461687 * __this, TW_APIRequstResult_t455151055 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::OnTimeLineRequestComplete(TW_APIRequstResult)
extern "C"  void TwitterAndroidUseExample_OnTimeLineRequestComplete_m482990533 (TwitterAndroidUseExample_t3716461687 * __this, TW_APIRequstResult_t455151055 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TwitterAndroidUseExample::PostScreenshot()
extern "C"  Il2CppObject * TwitterAndroidUseExample_PostScreenshot_m2830238868 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample::LogOut()
extern "C"  void TwitterAndroidUseExample_LogOut_m3658773008 (TwitterAndroidUseExample_t3716461687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
