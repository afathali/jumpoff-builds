﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GetCourseRequest
struct GetCourseRequest_t2656580458;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GetCourseRequest::.ctor()
extern "C"  void GetCourseRequest__ctor_m142224029 (GetCourseRequest_t2656580458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GetCourseRequest::get_Type()
extern "C"  String_t* GetCourseRequest_get_Type_m415311969 (GetCourseRequest_t2656580458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetCourseRequest::set_Type(System.String)
extern "C"  void GetCourseRequest_set_Type_m1689717514 (GetCourseRequest_t2656580458 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GetCourseRequest::get_UserId()
extern "C"  int64_t GetCourseRequest_get_UserId_m4180839461 (GetCourseRequest_t2656580458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetCourseRequest::set_UserId(System.Int64)
extern "C"  void GetCourseRequest_set_UserId_m3201715090 (GetCourseRequest_t2656580458 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GetCourseRequest::get_Id()
extern "C"  int64_t GetCourseRequest_get_Id_m2384183670 (GetCourseRequest_t2656580458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetCourseRequest::set_Id(System.Int64)
extern "C"  void GetCourseRequest_set_Id_m1714939015 (GetCourseRequest_t2656580458 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
