﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ImageUploader
struct ImageUploader_t3573860837;
// IOSImagePickResult
struct IOSImagePickResult_t1671334394;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSImagePickResult1671334394.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void ImageUploader::.ctor()
extern "C"  void ImageUploader__ctor_m3124635910 (ImageUploader_t3573860837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploader::Start()
extern "C"  void ImageUploader_Start_m3877816278 (ImageUploader_t3573860837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploader::OnImagePickedCallback(IOSImagePickResult)
extern "C"  void ImageUploader_OnImagePickedCallback_m2113778017 (ImageUploader_t3573860837 * __this, IOSImagePickResult_t1671334394 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D ImageUploader::applyRotation(UnityEngine.Texture2D,System.Int32)
extern "C"  Texture2D_t3542995729 * ImageUploader_applyRotation_m468878784 (ImageUploader_t3573860837 * __this, Texture2D_t3542995729 * ___texture0, int32_t ___rotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ImageUploader::ExifOrientationToRotation(System.Int32)
extern "C"  int32_t ImageUploader_ExifOrientationToRotation_m4184055770 (ImageUploader_t3573860837 * __this, int32_t ___exifOrientation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploader::OnPickSuccess(UnityEngine.Texture2D,System.Int32)
extern "C"  void ImageUploader_OnPickSuccess_m78892824 (ImageUploader_t3573860837 * __this, Texture2D_t3542995729 * ___Image0, int32_t ___exifOrientation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploader::OnPickFail()
extern "C"  void ImageUploader_OnPickFail_m3236927040 (ImageUploader_t3573860837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploader::CloseImageFail()
extern "C"  void ImageUploader_CloseImageFail_m1384174363 (ImageUploader_t3573860837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploader::CropImage()
extern "C"  void ImageUploader_CropImage_m1777571037 (ImageUploader_t3573860837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploader::<Start>m__135()
extern "C"  void ImageUploader_U3CStartU3Em__135_m1929195720 (ImageUploader_t3573860837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploader::<Start>m__136()
extern "C"  void ImageUploader_U3CStartU3Em__136_m2352683223 (ImageUploader_t3573860837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploader::<Start>m__137()
extern "C"  void ImageUploader_U3CStartU3Em__137_m2211520722 (ImageUploader_t3573860837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
