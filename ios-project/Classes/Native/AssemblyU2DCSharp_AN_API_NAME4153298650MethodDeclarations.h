﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_API_NAME
struct AN_API_NAME_t4153298650;

#include "codegen/il2cpp-codegen.h"

// System.Void AN_API_NAME::.ctor()
extern "C"  void AN_API_NAME__ctor_m2434452283 (AN_API_NAME_t4153298650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
