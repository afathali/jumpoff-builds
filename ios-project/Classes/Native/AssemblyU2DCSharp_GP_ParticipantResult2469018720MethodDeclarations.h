﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_ParticipantResult
struct GP_ParticipantResult_t2469018720;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_TBM_ParticipantResult1952842762.h"

// System.Void GP_ParticipantResult::.ctor(System.String,GP_TBM_ParticipantResult,System.Int32)
extern "C"  void GP_ParticipantResult__ctor_m2176233556 (GP_ParticipantResult_t2469018720 * __this, String_t* ___participantId0, int32_t ___result1, int32_t ___placing2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_ParticipantResult::.ctor(System.Int32,System.String,GP_TBM_ParticipantResult,System.Int32)
extern "C"  void GP_ParticipantResult__ctor_m3475861505 (GP_ParticipantResult_t2469018720 * __this, int32_t ___versionCode0, String_t* ___participantId1, int32_t ___result2, int32_t ___placing3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_ParticipantResult::.ctor(System.String[],System.Int32)
extern "C"  void GP_ParticipantResult__ctor_m111543818 (GP_ParticipantResult_t2469018720 * __this, StringU5BU5D_t1642385972* ___data0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GP_ParticipantResult::get_Placing()
extern "C"  int32_t GP_ParticipantResult_get_Placing_m2868797842 (GP_ParticipantResult_t2469018720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_TBM_ParticipantResult GP_ParticipantResult::get_Result()
extern "C"  int32_t GP_ParticipantResult_get_Result_m177518274 (GP_ParticipantResult_t2469018720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GP_ParticipantResult::get_VersionCode()
extern "C"  int32_t GP_ParticipantResult_get_VersionCode_m4264571205 (GP_ParticipantResult_t2469018720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GP_ParticipantResult::get_ParticipantId()
extern "C"  String_t* GP_ParticipantResult_get_ParticipantId_m1029682171 (GP_ParticipantResult_t2469018720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
