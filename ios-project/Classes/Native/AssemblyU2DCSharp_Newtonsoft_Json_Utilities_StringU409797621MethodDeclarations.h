﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.StringUtils/<Indent>c__AnonStorey37
struct U3CIndentU3Ec__AnonStorey37_t409797621;
// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_TextWriter4027217640.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Newtonsoft.Json.Utilities.StringUtils/<Indent>c__AnonStorey37::.ctor()
extern "C"  void U3CIndentU3Ec__AnonStorey37__ctor_m3451771742 (U3CIndentU3Ec__AnonStorey37_t409797621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.StringUtils/<Indent>c__AnonStorey37::<>m__104(System.IO.TextWriter,System.String)
extern "C"  void U3CIndentU3Ec__AnonStorey37_U3CU3Em__104_m918632105 (U3CIndentU3Ec__AnonStorey37_t409797621 * __this, TextWriter_t4027217640 * ___tw0, String_t* ___line1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
