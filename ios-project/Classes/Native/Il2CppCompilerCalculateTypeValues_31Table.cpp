﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ForgotPasswordRequest1886688323.h"
#include "AssemblyU2DCSharp_ForgotPasswordResponse4062332037.h"
#include "AssemblyU2DCSharp_ChangePasswordRequest4102061450.h"
#include "AssemblyU2DCSharp_ChangePasswordResponse3897893034.h"
#include "AssemblyU2DCSharp_Toggle3489774764.h"
#include "AssemblyU2DCSharp_MainPage_Controller1135564517.h"
#include "AssemblyU2DCSharp_MainPage_Controller_U3CGetBanner1818337539.h"
#include "AssemblyU2DCSharp_MainPage_Controller_U3CGetBannerU453977480.h"
#include "AssemblyU2DCSharp_MessageSender204794706.h"
#include "AssemblyU2DCSharp_MessageSenderBridge908748265.h"
#include "AssemblyU2DCSharp_Shared_Request3213492751.h"
#include "AssemblyU2DCSharp_Texture2DExtensions_Texture2DExt1413084901.h"
#include "AssemblyU2DCSharp_Spinner3781576355.h"
#include "AssemblyU2DCSharp_Switch1718632940.h"
#include "AssemblyU2DCSharp_Switch_ChangeCallback1172037850.h"
#include "AssemblyU2DCSharp_Terms510630971.h"
#include "AssemblyU2DCSharp_HorseAnimation2036550335.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObservableSupport_A6322105.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObservableSupport894287291.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObservableSuppor3936316282.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_Validatio1731902491.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_D13662249053.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3672778798.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3672778802.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_JSUseExample4150672372.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_JSCall1337227025.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (ForgotPasswordRequest_t1886688323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3100[2] = 
{
	ForgotPasswordRequest_t1886688323::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	ForgotPasswordRequest_t1886688323::get_offset_of_U3CEmailU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (ForgotPasswordResponse_t4062332037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3101[3] = 
{
	ForgotPasswordResponse_t4062332037::get_offset_of_U3CSuccessU3Ek__BackingField_0(),
	ForgotPasswordResponse_t4062332037::get_offset_of_U3CErrorMessageU3Ek__BackingField_1(),
	ForgotPasswordResponse_t4062332037::get_offset_of_U3CRequestU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (ChangePasswordRequest_t4102061450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3102[3] = 
{
	ChangePasswordRequest_t4102061450::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	ChangePasswordRequest_t4102061450::get_offset_of_U3CUserIdU3Ek__BackingField_3(),
	ChangePasswordRequest_t4102061450::get_offset_of_U3CNewPasswordU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (ChangePasswordResponse_t3897893034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3103[3] = 
{
	ChangePasswordResponse_t3897893034::get_offset_of_U3CSuccessU3Ek__BackingField_0(),
	ChangePasswordResponse_t3897893034::get_offset_of_U3CErrorMessageU3Ek__BackingField_1(),
	ChangePasswordResponse_t3897893034::get_offset_of_U3CRequestU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (Toggle_t3489774764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3104[4] = 
{
	Toggle_t3489774764::get_offset_of_UnCheckedImage_2(),
	Toggle_t3489774764::get_offset_of_CheckedImage_3(),
	Toggle_t3489774764::get_offset_of_btn_toggle_4(),
	Toggle_t3489774764::get_offset_of_IsItemChecked_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (MainPage_Controller_t1135564517), -1, sizeof(MainPage_Controller_t1135564517_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3105[25] = 
{
	MainPage_Controller_t1135564517::get_offset_of_btn_uploadNew_4(),
	MainPage_Controller_t1135564517::get_offset_of_btn_createBlank_5(),
	MainPage_Controller_t1135564517::get_offset_of_btn_viewCourses_6(),
	MainPage_Controller_t1135564517::get_offset_of_MainCamera_7(),
	MainPage_Controller_t1135564517::get_offset_of_SignUp_8(),
	MainPage_Controller_t1135564517::get_offset_of_btn_signUp_9(),
	MainPage_Controller_t1135564517::get_offset_of_btn_closeLogin_10(),
	MainPage_Controller_t1135564517::get_offset_of_AdObject_11(),
	MainPage_Controller_t1135564517::get_offset_of_currentAdNumber_12(),
	MainPage_Controller_t1135564517::get_offset_of_bannerImageUrl_13(),
	MainPage_Controller_t1135564517::get_offset_of_textFileUrl_14(),
	MainPage_Controller_t1135564517::get_offset_of_bannerUrl_15(),
	MainPage_Controller_t1135564517::get_offset_of_btn_banner_16(),
	MainPage_Controller_t1135564517::get_offset_of_btn_logout_17(),
	MainPage_Controller_t1135564517::get_offset_of_logo_18(),
	MainPage_Controller_t1135564517::get_offset_of_dlg_logout_19(),
	MainPage_Controller_t1135564517::get_offset_of_btn_logout_yes_20(),
	MainPage_Controller_t1135564517::get_offset_of_btn_logout_no_21(),
	MainPage_Controller_t1135564517::get_offset_of_waiting_22(),
	MainPage_Controller_t1135564517::get_offset_of_fail_23(),
	MainPage_Controller_t1135564517::get_offset_of_terms_24(),
	MainPage_Controller_t1135564517::get_offset_of_destroying_25(),
	MainPage_Controller_t1135564517_StaticFields::get_offset_of_U3CU3Ef__amU24cache16_26(),
	MainPage_Controller_t1135564517_StaticFields::get_offset_of_U3CU3Ef__amU24cache17_27(),
	MainPage_Controller_t1135564517_StaticFields::get_offset_of_U3CU3Ef__amU24cache18_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (U3CGetBannerImageU3Ec__Iterator16_t1818337539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3106[5] = 
{
	U3CGetBannerImageU3Ec__Iterator16_t1818337539::get_offset_of_U3CwwwU3E__0_0(),
	U3CGetBannerImageU3Ec__Iterator16_t1818337539::get_offset_of_U3CtexU3E__1_1(),
	U3CGetBannerImageU3Ec__Iterator16_t1818337539::get_offset_of_U24PC_2(),
	U3CGetBannerImageU3Ec__Iterator16_t1818337539::get_offset_of_U24current_3(),
	U3CGetBannerImageU3Ec__Iterator16_t1818337539::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (U3CGetBannerUrlU3Ec__Iterator17_t453977480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3107[4] = 
{
	U3CGetBannerUrlU3Ec__Iterator17_t453977480::get_offset_of_U3CwwwU3E__0_0(),
	U3CGetBannerUrlU3Ec__Iterator17_t453977480::get_offset_of_U24PC_1(),
	U3CGetBannerUrlU3Ec__Iterator17_t453977480::get_offset_of_U24current_2(),
	U3CGetBannerUrlU3Ec__Iterator17_t453977480::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (MessageSender_t204794706), -1, sizeof(MessageSender_t204794706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3108[4] = 
{
	0,
	MessageSender_t204794706::get_offset_of__postUrl_1(),
	MessageSender_t204794706_StaticFields::get_offset_of__bridge_2(),
	MessageSender_t204794706_StaticFields::get_offset_of_setCookie_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3109[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (MessageSenderBridge_t908748265), -1, sizeof(MessageSenderBridge_t908748265_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3110[1] = 
{
	MessageSenderBridge_t908748265_StaticFields::get_offset_of__instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (Request_t3213492751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3111[2] = 
{
	Request_t3213492751::get_offset_of_U3Capi_versionU3Ek__BackingField_0(),
	Request_t3213492751::get_offset_of_U3CBaseTypeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (Texture2DExtensions_t1413084901), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (Spinner_t3781576355), -1, sizeof(Spinner_t3781576355_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3114[9] = 
{
	Spinner_t3781576355::get_offset_of_btn_up_2(),
	Spinner_t3781576355::get_offset_of_btn_down_3(),
	Spinner_t3781576355::get_offset_of_text_4(),
	Spinner_t3781576355::get_offset_of_alphabet_5(),
	Spinner_t3781576355::get_offset_of_val_6(),
	Spinner_t3781576355::get_offset_of_minVal_7(),
	Spinner_t3781576355::get_offset_of_maxVal_8(),
	Spinner_t3781576355::get_offset_of_inited_9(),
	Spinner_t3781576355_StaticFields::get_offset_of_alpha_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (Switch_t1718632940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3115[4] = 
{
	Switch_t1718632940::get_offset_of_onImage_2(),
	Switch_t1718632940::get_offset_of_button_3(),
	Switch_t1718632940::get_offset_of_on_4(),
	Switch_t1718632940::get_offset_of_listeners_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (ChangeCallback_t1172037850), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (Terms_t510630971), -1, sizeof(Terms_t510630971_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3117[3] = 
{
	Terms_t510630971::get_offset_of_agree_2(),
	Terms_t510630971::get_offset_of_disagree_3(),
	Terms_t510630971_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (HorseAnimation_t2036550335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3118[18] = 
{
	0,
	HorseAnimation_t2036550335::get_offset_of_animate_3(),
	HorseAnimation_t2036550335::get_offset_of_headTurn_4(),
	HorseAnimation_t2036550335::get_offset_of_bodyMovementScale_5(),
	HorseAnimation_t2036550335::get_offset_of_headMovementScale_6(),
	HorseAnimation_t2036550335::get_offset_of_movementSpeed_7(),
	HorseAnimation_t2036550335::get_offset_of_yMovementScale_8(),
	HorseAnimation_t2036550335::get_offset_of_bodyBone_9(),
	HorseAnimation_t2036550335::get_offset_of_neckBone_10(),
	HorseAnimation_t2036550335::get_offset_of_headBone_11(),
	HorseAnimation_t2036550335::get_offset_of_startNeck_12(),
	HorseAnimation_t2036550335::get_offset_of_startHead_13(),
	HorseAnimation_t2036550335::get_offset_of_startBody_14(),
	HorseAnimation_t2036550335::get_offset_of_startBodyBonePos_15(),
	HorseAnimation_t2036550335::get_offset_of_delta_16(),
	HorseAnimation_t2036550335::get_offset_of_currentNeckRot_17(),
	HorseAnimation_t2036550335::get_offset_of_jumping_18(),
	HorseAnimation_t2036550335::get_offset_of_jumpTime_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (AddingNewEventHandler_t6322105), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (NotifyCollectionChangedEventHandler_t894287291), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (PropertyChangingEventHandler_t3936316282), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (ValidationEventHandler_t1731902491), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (D1_t3662249053), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3127[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3128[2] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (U24ArrayTypeU2452_t3672778799)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2452_t3672778799_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (U24ArrayTypeU2412_t3672778809)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778809_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (U3CModuleU3E_t3783534233), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (JSUseExample_t4150672372), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (U3CModuleU3E_t3783534234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (JSCall_t1337227025), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
