﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Login_Controller
struct Login_Controller_t1381689646;
// ForgotPasswordResponse
struct ForgotPasswordResponse_t4062332037;
// ChangePasswordResponse
struct ChangePasswordResponse_t3897893034;
// LoginResponse
struct LoginResponse_t1319408382;
// CreateCourseResponse
struct CreateCourseResponse_t3800683746;
// RegisterResponse
struct RegisterResponse_t410466074;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ForgotPasswordResponse4062332037.h"
#include "AssemblyU2DCSharp_ChangePasswordResponse3897893034.h"
#include "AssemblyU2DCSharp_LoginResponse1319408382.h"
#include "AssemblyU2DCSharp_CreateCourseResponse3800683746.h"
#include "AssemblyU2DCSharp_RegisterResponse410466074.h"

// System.Void Login_Controller::.ctor()
extern "C"  void Login_Controller__ctor_m2346814031 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::Start()
extern "C"  void Login_Controller_Start_m2465424123 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::SendForgotPassword()
extern "C"  void Login_Controller_SendForgotPassword_m1922854307 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::HandleForgotPasswordResponse(System.Boolean,ForgotPasswordResponse)
extern "C"  void Login_Controller_HandleForgotPasswordResponse_m3513413968 (Login_Controller_t1381689646 * __this, bool ___success0, ForgotPasswordResponse_t4062332037 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::SendChangePassword()
extern "C"  void Login_Controller_SendChangePassword_m2199807766 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::HandleChangePasswordResponse(System.Boolean,ChangePasswordResponse)
extern "C"  void Login_Controller_HandleChangePasswordResponse_m2512043504 (Login_Controller_t1381689646 * __this, bool ___success0, ChangePasswordResponse_t3897893034 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::MoveScreen(System.Int32)
extern "C"  void Login_Controller_MoveScreen_m1079269379 (Login_Controller_t1381689646 * __this, int32_t ____screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::SignIn()
extern "C"  void Login_Controller_SignIn_m3877860943 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::HandleLoginResponse(System.Boolean,LoginResponse)
extern "C"  void Login_Controller_HandleLoginResponse_m4172821634 (Login_Controller_t1381689646 * __this, bool ___success0, LoginResponse_t1319408382 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::SaveCourseThroughLogin()
extern "C"  void Login_Controller_SaveCourseThroughLogin_m1255844831 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::HandleCreateCourseResponse(System.Boolean,CreateCourseResponse)
extern "C"  void Login_Controller_HandleCreateCourseResponse_m3385799568 (Login_Controller_t1381689646 * __this, bool ___success0, CreateCourseResponse_t3800683746 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::CancelLogin()
extern "C"  void Login_Controller_CancelLogin_m1676200478 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::Join()
extern "C"  void Login_Controller_Join_m1174927565 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::AgreeToTerms()
extern "C"  void Login_Controller_AgreeToTerms_m2553888337 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::DisagreeToTerms()
extern "C"  void Login_Controller_DisagreeToTerms_m179062039 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::HandleRegisterResponse(System.Boolean,RegisterResponse)
extern "C"  void Login_Controller_HandleRegisterResponse_m2710831248 (Login_Controller_t1381689646 * __this, bool ___success0, RegisterResponse_t410466074 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__13B()
extern "C"  void Login_Controller_U3CStartU3Em__13B_m566391970 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__13C()
extern "C"  void Login_Controller_U3CStartU3Em__13C_m566392065 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__13D()
extern "C"  void Login_Controller_U3CStartU3Em__13D_m566392168 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__13E()
extern "C"  void Login_Controller_U3CStartU3Em__13E_m566392263 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__13F()
extern "C"  void Login_Controller_U3CStartU3Em__13F_m566392102 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__140()
extern "C"  void Login_Controller_U3CStartU3Em__140_m1588029541 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__141()
extern "C"  void Login_Controller_U3CStartU3Em__141_m1588029508 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__142()
extern "C"  void Login_Controller_U3CStartU3Em__142_m1588029475 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__143()
extern "C"  void Login_Controller_U3CStartU3Em__143_m1588029442 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__144()
extern "C"  void Login_Controller_U3CStartU3Em__144_m1588029673 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__145()
extern "C"  void Login_Controller_U3CStartU3Em__145_m1588029640 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__146()
extern "C"  void Login_Controller_U3CStartU3Em__146_m1588029607 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__147()
extern "C"  void Login_Controller_U3CStartU3Em__147_m1588029574 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__148()
extern "C"  void Login_Controller_U3CStartU3Em__148_m1588029789 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__149()
extern "C"  void Login_Controller_U3CStartU3Em__149_m1588029756 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__14A()
extern "C"  void Login_Controller_U3CStartU3Em__14A_m1588029044 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<Start>m__14B()
extern "C"  void Login_Controller_U3CStartU3Em__14B_m1588028883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login_Controller::<HandleRegisterResponse>m__14C()
extern "C"  void Login_Controller_U3CHandleRegisterResponseU3Em__14C_m1650471298 (Login_Controller_t1381689646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
