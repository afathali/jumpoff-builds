﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_EditorTestingSceneController
struct SA_EditorTestingSceneController_t2020162592;

#include "codegen/il2cpp-codegen.h"

// System.Void SA_EditorTestingSceneController::.ctor()
extern "C"  void SA_EditorTestingSceneController__ctor_m3020214257 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorTestingSceneController::LoadInterstitial()
extern "C"  void SA_EditorTestingSceneController_LoadInterstitial_m1662098405 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorTestingSceneController::ShowInterstitial()
extern "C"  void SA_EditorTestingSceneController_ShowInterstitial_m3136271942 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorTestingSceneController::LoadVideo()
extern "C"  void SA_EditorTestingSceneController_LoadVideo_m2371520130 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorTestingSceneController::ShowVideo()
extern "C"  void SA_EditorTestingSceneController_ShowVideo_m4077472515 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorTestingSceneController::Show_Notifications()
extern "C"  void SA_EditorTestingSceneController_Show_Notifications_m302334907 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorTestingSceneController::Show_A_Notifications()
extern "C"  void SA_EditorTestingSceneController_Show_A_Notifications_m3294551617 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorTestingSceneController::Show_L_Notifications()
extern "C"  void SA_EditorTestingSceneController_Show_L_Notifications_m1764659148 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorTestingSceneController::Show_E_Notifications()
extern "C"  void SA_EditorTestingSceneController_Show_E_Notifications_m4283359045 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorTestingSceneController::Show_InApp_Popup()
extern "C"  void SA_EditorTestingSceneController_Show_InApp_Popup_m224412576 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorTestingSceneController::FixedUpdate()
extern "C"  void SA_EditorTestingSceneController_FixedUpdate_m3521350690 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
