﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JToken/<Ancestors>c__IteratorD
struct U3CAncestorsU3Ec__IteratorD_t445779108;
// Newtonsoft.Json.Linq.JToken
struct JToken_t2552644013;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t28167840;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.JToken/<Ancestors>c__IteratorD::.ctor()
extern "C"  void U3CAncestorsU3Ec__IteratorD__ctor_m63438649 (U3CAncestorsU3Ec__IteratorD_t445779108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<Ancestors>c__IteratorD::System.Collections.Generic.IEnumerator<Newtonsoft.Json.Linq.JToken>.get_Current()
extern "C"  JToken_t2552644013 * U3CAncestorsU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m3711707678 (U3CAncestorsU3Ec__IteratorD_t445779108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JToken/<Ancestors>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAncestorsU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3599507319 (U3CAncestorsU3Ec__IteratorD_t445779108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JToken/<Ancestors>c__IteratorD::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CAncestorsU3Ec__IteratorD_System_Collections_IEnumerable_GetEnumerator_m1322890590 (U3CAncestorsU3Ec__IteratorD_t445779108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken/<Ancestors>c__IteratorD::System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>.GetEnumerator()
extern "C"  Il2CppObject* U3CAncestorsU3Ec__IteratorD_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_m3863253685 (U3CAncestorsU3Ec__IteratorD_t445779108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken/<Ancestors>c__IteratorD::MoveNext()
extern "C"  bool U3CAncestorsU3Ec__IteratorD_MoveNext_m2541627115 (U3CAncestorsU3Ec__IteratorD_t445779108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<Ancestors>c__IteratorD::Dispose()
extern "C"  void U3CAncestorsU3Ec__IteratorD_Dispose_m2498497334 (U3CAncestorsU3Ec__IteratorD_t445779108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<Ancestors>c__IteratorD::Reset()
extern "C"  void U3CAncestorsU3Ec__IteratorD_Reset_m942764100 (U3CAncestorsU3Ec__IteratorD_t445779108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
