﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_Event
struct GP_Event_t323143668;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void GP_Event::.ctor()
extern "C"  void GP_Event__ctor_m1855104567 (GP_Event_t323143668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Event::LoadIcon()
extern "C"  void GP_Event_LoadIcon_m1968066386 (GP_Event_t323143668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GP_Event::get_icon()
extern "C"  Texture2D_t3542995729 * GP_Event_get_icon_m2231868930 (GP_Event_t323143668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Event::OnTextureLoaded(UnityEngine.Texture2D)
extern "C"  void GP_Event_OnTextureLoaded_m2555361640 (GP_Event_t323143668 * __this, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
