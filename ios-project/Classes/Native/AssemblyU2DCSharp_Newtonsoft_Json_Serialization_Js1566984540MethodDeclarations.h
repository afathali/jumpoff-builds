﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1566984540;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t1964060750;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Func`1<System.Object>
struct Func_1_t348874681;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Serialization.ErrorContext
struct ErrorContext_t615697659;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter1964060750.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Err615697659.h"

// System.Void Newtonsoft.Json.Serialization.JsonContract::.ctor(System.Type)
extern "C"  void JsonContract__ctor_m917491473 (JsonContract_t1566984540 * __this, Type_t * ___underlyingType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonContract::get_UnderlyingType()
extern "C"  Type_t * JsonContract_get_UnderlyingType_m3628691220 (JsonContract_t1566984540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::set_UnderlyingType(System.Type)
extern "C"  void JsonContract_set_UnderlyingType_m2016964841 (JsonContract_t1566984540 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonContract::get_CreatedType()
extern "C"  Type_t * JsonContract_get_CreatedType_m3017496495 (JsonContract_t1566984540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::set_CreatedType(System.Type)
extern "C"  void JsonContract_set_CreatedType_m177173866 (JsonContract_t1566984540 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContract::get_IsReference()
extern "C"  Nullable_1_t2088641033  JsonContract_get_IsReference_m1592964083 (JsonContract_t1566984540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::set_IsReference(System.Nullable`1<System.Boolean>)
extern "C"  void JsonContract_set_IsReference_m1593989474 (JsonContract_t1566984540 * __this, Nullable_1_t2088641033  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::get_Converter()
extern "C"  JsonConverter_t1964060750 * JsonContract_get_Converter_m2971439803 (JsonContract_t1566984540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::set_Converter(Newtonsoft.Json.JsonConverter)
extern "C"  void JsonContract_set_Converter_m2763177982 (JsonContract_t1566984540 * __this, JsonConverter_t1964060750 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::get_InternalConverter()
extern "C"  JsonConverter_t1964060750 * JsonContract_get_InternalConverter_m2324177064 (JsonContract_t1566984540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::set_InternalConverter(Newtonsoft.Json.JsonConverter)
extern "C"  void JsonContract_set_InternalConverter_m2193962355 (JsonContract_t1566984540 * __this, JsonConverter_t1964060750 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Newtonsoft.Json.Serialization.JsonContract::get_OnDeserialized()
extern "C"  MethodInfo_t * JsonContract_get_OnDeserialized_m2075190625 (JsonContract_t1566984540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::set_OnDeserialized(System.Reflection.MethodInfo)
extern "C"  void JsonContract_set_OnDeserialized_m473297624 (JsonContract_t1566984540 * __this, MethodInfo_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Newtonsoft.Json.Serialization.JsonContract::get_OnDeserializing()
extern "C"  MethodInfo_t * JsonContract_get_OnDeserializing_m4000907612 (JsonContract_t1566984540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::set_OnDeserializing(System.Reflection.MethodInfo)
extern "C"  void JsonContract_set_OnDeserializing_m3119036087 (JsonContract_t1566984540 * __this, MethodInfo_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Newtonsoft.Json.Serialization.JsonContract::get_OnSerialized()
extern "C"  MethodInfo_t * JsonContract_get_OnSerialized_m4160116184 (JsonContract_t1566984540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::set_OnSerialized(System.Reflection.MethodInfo)
extern "C"  void JsonContract_set_OnSerialized_m2489372907 (JsonContract_t1566984540 * __this, MethodInfo_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Newtonsoft.Json.Serialization.JsonContract::get_OnSerializing()
extern "C"  MethodInfo_t * JsonContract_get_OnSerializing_m258581 (JsonContract_t1566984540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::set_OnSerializing(System.Reflection.MethodInfo)
extern "C"  void JsonContract_set_OnSerializing_m269052426 (JsonContract_t1566984540 * __this, MethodInfo_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonContract::get_DefaultCreator()
extern "C"  Func_1_t348874681 * JsonContract_get_DefaultCreator_m22103951 (JsonContract_t1566984540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::set_DefaultCreator(System.Func`1<System.Object>)
extern "C"  void JsonContract_set_DefaultCreator_m3993148058 (JsonContract_t1566984540 * __this, Func_1_t348874681 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonContract::get_DefaultCreatorNonPublic()
extern "C"  bool JsonContract_get_DefaultCreatorNonPublic_m3155504812 (JsonContract_t1566984540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::set_DefaultCreatorNonPublic(System.Boolean)
extern "C"  void JsonContract_set_DefaultCreatorNonPublic_m3102575625 (JsonContract_t1566984540 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Newtonsoft.Json.Serialization.JsonContract::get_OnError()
extern "C"  MethodInfo_t * JsonContract_get_OnError_m1566992450 (JsonContract_t1566984540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::set_OnError(System.Reflection.MethodInfo)
extern "C"  void JsonContract_set_OnError_m1696808521 (JsonContract_t1566984540 * __this, MethodInfo_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnSerializing(System.Object,System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonContract_InvokeOnSerializing_m3053789874 (JsonContract_t1566984540 * __this, Il2CppObject * ___o0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnSerialized(System.Object,System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonContract_InvokeOnSerialized_m1436612217 (JsonContract_t1566984540 * __this, Il2CppObject * ___o0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnDeserializing(System.Object,System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonContract_InvokeOnDeserializing_m565678333 (JsonContract_t1566984540 * __this, Il2CppObject * ___o0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnDeserialized(System.Object,System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonContract_InvokeOnDeserialized_m2913609966 (JsonContract_t1566984540 * __this, Il2CppObject * ___o0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnError(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext)
extern "C"  void JsonContract_InvokeOnError_m3837208643 (JsonContract_t1566984540 * __this, Il2CppObject * ___o0, StreamingContext_t1417235061  ___context1, ErrorContext_t615697659 * ___errorContext2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
