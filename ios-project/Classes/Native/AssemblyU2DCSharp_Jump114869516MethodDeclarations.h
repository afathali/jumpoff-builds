﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Jump
struct Jump_t114869516;

#include "codegen/il2cpp-codegen.h"

// System.Void Jump::.ctor()
extern "C"  void Jump__ctor_m2873503733 (Jump_t114869516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Jump::get_jumpOrder()
extern "C"  int32_t Jump_get_jumpOrder_m3837753466 (Jump_t114869516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jump::set_jumpOrder(System.Int32)
extern "C"  void Jump_set_jumpOrder_m2053692633 (Jump_t114869516 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Jump::get_jumpType()
extern "C"  int32_t Jump_get_jumpType_m2213058256 (Jump_t114869516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jump::set_jumpType(System.Int32)
extern "C"  void Jump_set_jumpType_m2508789695 (Jump_t114869516 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jump::SetRotation(System.Single)
extern "C"  void Jump_SetRotation_m285611798 (Jump_t114869516 * __this, float ____rotation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jump::UpdateSprite()
extern "C"  void Jump_UpdateSprite_m269975637 (Jump_t114869516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jump::SetJumpType(System.Int32)
extern "C"  void Jump_SetJumpType_m2466627406 (Jump_t114869516 * __this, int32_t ____jumpType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jump::updateText(System.Boolean)
extern "C"  void Jump_updateText_m1590926360 (Jump_t114869516 * __this, bool ___showCombination0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
