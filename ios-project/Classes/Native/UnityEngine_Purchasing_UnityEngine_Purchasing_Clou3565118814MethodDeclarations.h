﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.CloudCatalogManager/<FetchProducts>c__AnonStorey2
struct U3CFetchProductsU3Ec__AnonStorey2_t3565118814;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.CloudCatalogManager/<FetchProducts>c__AnonStorey2::.ctor()
extern "C"  void U3CFetchProductsU3Ec__AnonStorey2__ctor_m383879158 (U3CFetchProductsU3Ec__AnonStorey2_t3565118814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.CloudCatalogManager/<FetchProducts>c__AnonStorey2::<>m__0(System.String)
extern "C"  void U3CFetchProductsU3Ec__AnonStorey2_U3CU3Em__0_m742209237 (U3CFetchProductsU3Ec__AnonStorey2_t3565118814 * __this, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.CloudCatalogManager/<FetchProducts>c__AnonStorey2::<>m__1(System.String)
extern "C"  void U3CFetchProductsU3Ec__AnonStorey2_U3CU3Em__1_m230995316 (U3CFetchProductsU3Ec__AnonStorey2_t3565118814 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.CloudCatalogManager/<FetchProducts>c__AnonStorey2::<>m__4()
extern "C"  void U3CFetchProductsU3Ec__AnonStorey2_U3CU3Em__4_m1409723799 (U3CFetchProductsU3Ec__AnonStorey2_t3565118814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
