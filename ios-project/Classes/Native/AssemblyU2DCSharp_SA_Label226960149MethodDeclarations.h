﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_Label
struct SA_Label_t226960149;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SA_Label::.ctor()
extern "C"  void SA_Label__ctor_m3249699272 (SA_Label_t226960149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA_Label::get_text()
extern "C"  String_t* SA_Label_get_text_m207265117 (SA_Label_t226960149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Label::set_text(System.String)
extern "C"  void SA_Label_set_text_m1606271094 (SA_Label_t226960149 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
