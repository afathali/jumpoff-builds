﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MCGBehaviour/InjectionContainer
struct InjectionContainer_t3223711125;
// MCGBehaviour/Component
struct Component_t1531646212;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.String
struct String_t;
// MCGBehaviour/InjectionContainer/TypeData
struct TypeData_t212254090;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t2851816542;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MCGBehaviour_Component1531646212.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_MCGBehaviour_InjectionContainer_T212254090.h"
#include "mscorlib_System_Reflection_ConstructorInfo2851816542.h"

// System.Void MCGBehaviour/InjectionContainer::.ctor()
extern "C"  void InjectionContainer__ctor_m3889672852 (InjectionContainer_t3223711125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour/InjectionContainer::.cctor()
extern "C"  void InjectionContainer__cctor_m3193880015 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MCGBehaviour/InjectionContainer MCGBehaviour/InjectionContainer::Register(MCGBehaviour/Component)
extern "C"  InjectionContainer_t3223711125 * InjectionContainer_Register_m1936959541 (InjectionContainer_t3223711125 * __this, Component_t1531646212 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MCGBehaviour/InjectionContainer::Resolve(System.Type,System.String)
extern "C"  Il2CppObject * InjectionContainer_Resolve_m1778779216 (InjectionContainer_t3223711125 * __this, Type_t * ___type0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MCGBehaviour/InjectionContainer::ResolveInstance(System.Type,MCGBehaviour/InjectionContainer/TypeData)
extern "C"  Il2CppObject * InjectionContainer_ResolveInstance_m2015924807 (InjectionContainer_t3223711125 * __this, Type_t * ___type0, TypeData_t212254090 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MCGBehaviour/InjectionContainer::CreateInstance(System.Type)
extern "C"  Il2CppObject * InjectionContainer_CreateInstance_m2714446303 (InjectionContainer_t3223711125 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MCGBehaviour/InjectionContainer::<CreateInstance>m__10B(System.Reflection.ConstructorInfo)
extern "C"  int32_t InjectionContainer_U3CCreateInstanceU3Em__10B_m889280691 (Il2CppObject * __this /* static, unused */, ConstructorInfo_t2851816542 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
