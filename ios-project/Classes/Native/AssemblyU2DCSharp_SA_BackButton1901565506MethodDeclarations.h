﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_BackButton
struct SA_BackButton_t1901565506;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void SA_BackButton::.ctor()
extern "C"  void SA_BackButton__ctor_m1800514331 (SA_BackButton_t1901565506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_BackButton::.cctor()
extern "C"  void SA_BackButton__cctor_m871647344 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_BackButton::Start()
extern "C"  void SA_BackButton_Start_m2538225031 (SA_BackButton_t1901565506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_BackButton::FixedUpdate()
extern "C"  void SA_BackButton_FixedUpdate_m2429153846 (SA_BackButton_t1901565506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_BackButton::OnClick()
extern "C"  void SA_BackButton_OnClick_m1386455614 (SA_BackButton_t1901565506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_BackButton::GoBack()
extern "C"  void SA_BackButton_GoBack_m2445920324 (SA_BackButton_t1901565506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA_BackButton::get_LevelName()
extern "C"  String_t* SA_BackButton_get_LevelName_m2307553226 (SA_BackButton_t1901565506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
