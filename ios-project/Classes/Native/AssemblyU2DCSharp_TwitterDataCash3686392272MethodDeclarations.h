﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweetTemplate
struct TweetTemplate_t3444491657;
// System.String
struct String_t;
// TwitterUserInfo
struct TwitterUserInfo_t87370740;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TweetTemplate3444491657.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_TwitterUserInfo87370740.h"

// System.Void TwitterDataCash::.cctor()
extern "C"  void TwitterDataCash__cctor_m3828643790 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterDataCash::AddTweet(TweetTemplate)
extern "C"  void TwitterDataCash_AddTweet_m2045636830 (Il2CppObject * __this /* static, unused */, TweetTemplate_t3444491657 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweetTemplate TwitterDataCash::GetTweetsById(System.String)
extern "C"  TweetTemplate_t3444491657 * TwitterDataCash_GetTweetsById_m2766316665 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterDataCash::AddUser(TwitterUserInfo)
extern "C"  void TwitterDataCash_AddUser_m3780404453 (Il2CppObject * __this /* static, unused */, TwitterUserInfo_t87370740 * ___u0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TwitterUserInfo TwitterDataCash::GetUserById(System.String)
extern "C"  TwitterUserInfo_t87370740 * TwitterDataCash_GetUserById_m4041735877 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
