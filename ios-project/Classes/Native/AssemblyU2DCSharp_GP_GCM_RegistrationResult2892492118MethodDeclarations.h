﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_GCM_RegistrationResult
struct GP_GCM_RegistrationResult_t2892492118;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_GCM_RegistrationResult::.ctor()
extern "C"  void GP_GCM_RegistrationResult__ctor_m3218152193 (GP_GCM_RegistrationResult_t2892492118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_GCM_RegistrationResult::.ctor(System.String)
extern "C"  void GP_GCM_RegistrationResult__ctor_m2366284031 (GP_GCM_RegistrationResult_t2892492118 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GP_GCM_RegistrationResult::get_RegistrationDeviceId()
extern "C"  String_t* GP_GCM_RegistrationResult_get_RegistrationDeviceId_m463985701 (GP_GCM_RegistrationResult_t2892492118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
