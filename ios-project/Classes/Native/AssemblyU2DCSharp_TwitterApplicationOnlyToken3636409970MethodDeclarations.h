﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TwitterApplicationOnlyToken
struct TwitterApplicationOnlyToken_t3636409970;
// System.Action
struct Action_t3226471752;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"

// System.Void TwitterApplicationOnlyToken::.ctor()
extern "C"  void TwitterApplicationOnlyToken__ctor_m2444686339 (TwitterApplicationOnlyToken_t3636409970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterApplicationOnlyToken::add_ActionComplete(System.Action)
extern "C"  void TwitterApplicationOnlyToken_add_ActionComplete_m3317892447 (TwitterApplicationOnlyToken_t3636409970 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterApplicationOnlyToken::remove_ActionComplete(System.Action)
extern "C"  void TwitterApplicationOnlyToken_remove_ActionComplete_m105879940 (TwitterApplicationOnlyToken_t3636409970 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterApplicationOnlyToken::Awake()
extern "C"  void TwitterApplicationOnlyToken_Awake_m1401918354 (TwitterApplicationOnlyToken_t3636409970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterApplicationOnlyToken::RetrieveToken()
extern "C"  void TwitterApplicationOnlyToken_RetrieveToken_m697273556 (TwitterApplicationOnlyToken_t3636409970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterApplicationOnlyToken::get_currentToken()
extern "C"  String_t* TwitterApplicationOnlyToken_get_currentToken_m3821405405 (TwitterApplicationOnlyToken_t3636409970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TwitterApplicationOnlyToken::Load()
extern "C"  Il2CppObject * TwitterApplicationOnlyToken_Load_m3183196075 (TwitterApplicationOnlyToken_t3636409970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterApplicationOnlyToken::<ActionComplete>m__AC()
extern "C"  void TwitterApplicationOnlyToken_U3CActionCompleteU3Em__AC_m819065889 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
