﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// DefaultPreviewButton
struct DefaultPreviewButton_t12674677;
// SA_Texture
struct SA_Texture_t3361108684;
// SA_Label
struct SA_Label_t226960149;
// DefaultPreviewButton[]
struct DefaultPreviewButtonU5BU5D_t3800739864;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwitterAndroidUseExample
struct  TwitterAndroidUseExample_t3716461687  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture2D TwitterAndroidUseExample::ImageToShare
	Texture2D_t3542995729 * ___ImageToShare_4;
	// DefaultPreviewButton TwitterAndroidUseExample::connectButton
	DefaultPreviewButton_t12674677 * ___connectButton_5;
	// SA_Texture TwitterAndroidUseExample::avatar
	SA_Texture_t3361108684 * ___avatar_6;
	// SA_Label TwitterAndroidUseExample::Location
	SA_Label_t226960149 * ___Location_7;
	// SA_Label TwitterAndroidUseExample::Language
	SA_Label_t226960149 * ___Language_8;
	// SA_Label TwitterAndroidUseExample::Status
	SA_Label_t226960149 * ___Status_9;
	// SA_Label TwitterAndroidUseExample::Name
	SA_Label_t226960149 * ___Name_10;
	// DefaultPreviewButton[] TwitterAndroidUseExample::AuthDependedButtons
	DefaultPreviewButtonU5BU5D_t3800739864* ___AuthDependedButtons_11;

public:
	inline static int32_t get_offset_of_ImageToShare_4() { return static_cast<int32_t>(offsetof(TwitterAndroidUseExample_t3716461687, ___ImageToShare_4)); }
	inline Texture2D_t3542995729 * get_ImageToShare_4() const { return ___ImageToShare_4; }
	inline Texture2D_t3542995729 ** get_address_of_ImageToShare_4() { return &___ImageToShare_4; }
	inline void set_ImageToShare_4(Texture2D_t3542995729 * value)
	{
		___ImageToShare_4 = value;
		Il2CppCodeGenWriteBarrier(&___ImageToShare_4, value);
	}

	inline static int32_t get_offset_of_connectButton_5() { return static_cast<int32_t>(offsetof(TwitterAndroidUseExample_t3716461687, ___connectButton_5)); }
	inline DefaultPreviewButton_t12674677 * get_connectButton_5() const { return ___connectButton_5; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_connectButton_5() { return &___connectButton_5; }
	inline void set_connectButton_5(DefaultPreviewButton_t12674677 * value)
	{
		___connectButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___connectButton_5, value);
	}

	inline static int32_t get_offset_of_avatar_6() { return static_cast<int32_t>(offsetof(TwitterAndroidUseExample_t3716461687, ___avatar_6)); }
	inline SA_Texture_t3361108684 * get_avatar_6() const { return ___avatar_6; }
	inline SA_Texture_t3361108684 ** get_address_of_avatar_6() { return &___avatar_6; }
	inline void set_avatar_6(SA_Texture_t3361108684 * value)
	{
		___avatar_6 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_6, value);
	}

	inline static int32_t get_offset_of_Location_7() { return static_cast<int32_t>(offsetof(TwitterAndroidUseExample_t3716461687, ___Location_7)); }
	inline SA_Label_t226960149 * get_Location_7() const { return ___Location_7; }
	inline SA_Label_t226960149 ** get_address_of_Location_7() { return &___Location_7; }
	inline void set_Location_7(SA_Label_t226960149 * value)
	{
		___Location_7 = value;
		Il2CppCodeGenWriteBarrier(&___Location_7, value);
	}

	inline static int32_t get_offset_of_Language_8() { return static_cast<int32_t>(offsetof(TwitterAndroidUseExample_t3716461687, ___Language_8)); }
	inline SA_Label_t226960149 * get_Language_8() const { return ___Language_8; }
	inline SA_Label_t226960149 ** get_address_of_Language_8() { return &___Language_8; }
	inline void set_Language_8(SA_Label_t226960149 * value)
	{
		___Language_8 = value;
		Il2CppCodeGenWriteBarrier(&___Language_8, value);
	}

	inline static int32_t get_offset_of_Status_9() { return static_cast<int32_t>(offsetof(TwitterAndroidUseExample_t3716461687, ___Status_9)); }
	inline SA_Label_t226960149 * get_Status_9() const { return ___Status_9; }
	inline SA_Label_t226960149 ** get_address_of_Status_9() { return &___Status_9; }
	inline void set_Status_9(SA_Label_t226960149 * value)
	{
		___Status_9 = value;
		Il2CppCodeGenWriteBarrier(&___Status_9, value);
	}

	inline static int32_t get_offset_of_Name_10() { return static_cast<int32_t>(offsetof(TwitterAndroidUseExample_t3716461687, ___Name_10)); }
	inline SA_Label_t226960149 * get_Name_10() const { return ___Name_10; }
	inline SA_Label_t226960149 ** get_address_of_Name_10() { return &___Name_10; }
	inline void set_Name_10(SA_Label_t226960149 * value)
	{
		___Name_10 = value;
		Il2CppCodeGenWriteBarrier(&___Name_10, value);
	}

	inline static int32_t get_offset_of_AuthDependedButtons_11() { return static_cast<int32_t>(offsetof(TwitterAndroidUseExample_t3716461687, ___AuthDependedButtons_11)); }
	inline DefaultPreviewButtonU5BU5D_t3800739864* get_AuthDependedButtons_11() const { return ___AuthDependedButtons_11; }
	inline DefaultPreviewButtonU5BU5D_t3800739864** get_address_of_AuthDependedButtons_11() { return &___AuthDependedButtons_11; }
	inline void set_AuthDependedButtons_11(DefaultPreviewButtonU5BU5D_t3800739864* value)
	{
		___AuthDependedButtons_11 = value;
		Il2CppCodeGenWriteBarrier(&___AuthDependedButtons_11, value);
	}
};

struct TwitterAndroidUseExample_t3716461687_StaticFields
{
public:
	// System.Boolean TwitterAndroidUseExample::IsUserInfoLoaded
	bool ___IsUserInfoLoaded_2;
	// System.Boolean TwitterAndroidUseExample::IsAuthenticated
	bool ___IsAuthenticated_3;

public:
	inline static int32_t get_offset_of_IsUserInfoLoaded_2() { return static_cast<int32_t>(offsetof(TwitterAndroidUseExample_t3716461687_StaticFields, ___IsUserInfoLoaded_2)); }
	inline bool get_IsUserInfoLoaded_2() const { return ___IsUserInfoLoaded_2; }
	inline bool* get_address_of_IsUserInfoLoaded_2() { return &___IsUserInfoLoaded_2; }
	inline void set_IsUserInfoLoaded_2(bool value)
	{
		___IsUserInfoLoaded_2 = value;
	}

	inline static int32_t get_offset_of_IsAuthenticated_3() { return static_cast<int32_t>(offsetof(TwitterAndroidUseExample_t3716461687_StaticFields, ___IsAuthenticated_3)); }
	inline bool get_IsAuthenticated_3() const { return ___IsAuthenticated_3; }
	inline bool* get_address_of_IsAuthenticated_3() { return &___IsAuthenticated_3; }
	inline void set_IsAuthenticated_3(bool value)
	{
		___IsAuthenticated_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
