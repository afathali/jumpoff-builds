﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweetTemplate
struct TweetTemplate_t3444491657;

#include "codegen/il2cpp-codegen.h"

// System.Void TweetTemplate::.ctor()
extern "C"  void TweetTemplate__ctor_m1797600386 (TweetTemplate_t3444491657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
