﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Shared.Response`1<System.Object>
struct Response_1_t3572928552;

#include "codegen/il2cpp-codegen.h"

// System.Void Shared.Response`1<System.Object>::.ctor()
extern "C"  void Response_1__ctor_m920360982_gshared (Response_1_t3572928552 * __this, const MethodInfo* method);
#define Response_1__ctor_m920360982(__this, method) ((  void (*) (Response_1_t3572928552 *, const MethodInfo*))Response_1__ctor_m920360982_gshared)(__this, method)
