﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShowErrorEventArgs
struct ShowErrorEventArgs_t2135289798;
// System.String
struct String_t;
// ShowErrorEventArgs/del
struct del_t1625342804;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ShowErrorEventArgs_del1625342804.h"

// System.Void ShowErrorEventArgs::.ctor()
extern "C"  void ShowErrorEventArgs__ctor_m3923682159 (ShowErrorEventArgs_t2135289798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ShowErrorEventArgs::get_Message()
extern "C"  String_t* ShowErrorEventArgs_get_Message_m3911740030 (ShowErrorEventArgs_t2135289798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowErrorEventArgs::set_Message(System.String)
extern "C"  void ShowErrorEventArgs_set_Message_m3454019047 (ShowErrorEventArgs_t2135289798 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ShowErrorEventArgs/del ShowErrorEventArgs::get_onDismiss()
extern "C"  del_t1625342804 * ShowErrorEventArgs_get_onDismiss_m236821064 (ShowErrorEventArgs_t2135289798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowErrorEventArgs::set_onDismiss(ShowErrorEventArgs/del)
extern "C"  void ShowErrorEventArgs_set_onDismiss_m1105192191 (ShowErrorEventArgs_t2135289798 * __this, del_t1625342804 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
