﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen3369346583MethodDeclarations.h"

// System.Void System.Func`3<System.Type,System.Collections.Generic.IList`1<System.Object>,System.Object>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m3348447403(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t2880746669 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m3917225629_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Type,System.Collections.Generic.IList`1<System.Object>,System.Object>::Invoke(T1,T2)
#define Func_3_Invoke_m3416510136(__this, ___arg10, ___arg21, method) ((  Il2CppObject * (*) (Func_3_t2880746669 *, Type_t *, Il2CppObject*, const MethodInfo*))Func_3_Invoke_m1588676556_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Type,System.Collections.Generic.IList`1<System.Object>,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m2337749179(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t2880746669 *, Type_t *, Il2CppObject*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m3007306193_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Type,System.Collections.Generic.IList`1<System.Object>,System.Object>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m436068039(__this, ___result0, method) ((  Il2CppObject * (*) (Func_3_t2880746669 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m917241105_gshared)(__this, ___result0, method)
