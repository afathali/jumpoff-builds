﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,FB_ProfileImageSize>
struct Transform_1_t1576166847;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_FB_ProfileImageSize3003328130.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,FB_ProfileImageSize>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1740663031_gshared (Transform_1_t1576166847 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m1740663031(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1576166847 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1740663031_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,FB_ProfileImageSize>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m302575779_gshared (Transform_1_t1576166847 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m302575779(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t1576166847 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m302575779_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,FB_ProfileImageSize>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4193502274_gshared (Transform_1_t1576166847 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m4193502274(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1576166847 *, int32_t, Il2CppObject *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m4193502274_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,FB_ProfileImageSize>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1459725537_gshared (Transform_1_t1576166847 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m1459725537(__this, ___result0, method) ((  int32_t (*) (Transform_1_t1576166847 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1459725537_gshared)(__this, ___result0, method)
