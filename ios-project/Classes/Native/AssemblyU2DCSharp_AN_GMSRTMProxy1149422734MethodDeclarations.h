﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_GMSRTMProxy
struct AN_GMSRTMProxy_t1149422734;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_GMSRTMProxy::.ctor()
extern "C"  void AN_GMSRTMProxy__ctor_m3484640581 (AN_GMSRTMProxy_t1149422734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_GMSRTMProxy_CallActivityFunction_m591372280 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::RTMFindMatch(System.Int32,System.Int32,System.String[])
extern "C"  void AN_GMSRTMProxy_RTMFindMatch_m1529710566 (Il2CppObject * __this /* static, unused */, int32_t ___minPlayers0, int32_t ___maxPlayers1, StringU5BU5D_t1642385972* ___pIds2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::RTMFindMatch(System.String[])
extern "C"  void AN_GMSRTMProxy_RTMFindMatch_m3818667144 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___pIds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::sendDataToAll(System.String,System.Int32,System.Int32)
extern "C"  void AN_GMSRTMProxy_sendDataToAll_m214479189 (Il2CppObject * __this /* static, unused */, String_t* ___data0, int32_t ___sendType1, int32_t ___dataId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::sendDataToPlayers(System.String,System.String,System.Int32,System.Int32)
extern "C"  void AN_GMSRTMProxy_sendDataToPlayers_m3795991958 (Il2CppObject * __this /* static, unused */, String_t* ___data0, String_t* ___players1, int32_t ___sendType2, int32_t ___dataId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::ShowWaitingRoomIntent()
extern "C"  void AN_GMSRTMProxy_ShowWaitingRoomIntent_m2144470116 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::InvitePlayers(System.Int32,System.Int32)
extern "C"  void AN_GMSRTMProxy_InvitePlayers_m2338888320 (Il2CppObject * __this /* static, unused */, int32_t ___minPlayers0, int32_t ___maxPlayers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::RTM_SetVariant(System.Int32)
extern "C"  void AN_GMSRTMProxy_RTM_SetVariant_m4217399417 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::RTM_SetExclusiveBitMask(System.Int32)
extern "C"  void AN_GMSRTMProxy_RTM_SetExclusiveBitMask_m3062817031 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::RTM_AcceptInvitation(System.String)
extern "C"  void AN_GMSRTMProxy_RTM_AcceptInvitation_m1222038854 (Il2CppObject * __this /* static, unused */, String_t* ___invitationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::RTM_DeclineInvitation(System.String)
extern "C"  void AN_GMSRTMProxy_RTM_DeclineInvitation_m2955913960 (Il2CppObject * __this /* static, unused */, String_t* ___invitationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::RTM_DismissInvitation(System.String)
extern "C"  void AN_GMSRTMProxy_RTM_DismissInvitation_m2910192638 (Il2CppObject * __this /* static, unused */, String_t* ___invitationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_AcceptInvitation(System.String)
extern "C"  void AN_GMSRTMProxy_TBM_AcceptInvitation_m2148759726 (Il2CppObject * __this /* static, unused */, String_t* ___invitationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_DeclineInvitation(System.String)
extern "C"  void AN_GMSRTMProxy_TBM_DeclineInvitation_m1853381352 (Il2CppObject * __this /* static, unused */, String_t* ___invitationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_DismissInvitation(System.String)
extern "C"  void AN_GMSRTMProxy_TBM_DismissInvitation_m2373305746 (Il2CppObject * __this /* static, unused */, String_t* ___invitationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_CreateMatch(System.Int32,System.Int32,System.String[])
extern "C"  void AN_GMSRTMProxy_TBM_CreateMatch_m3926457782 (Il2CppObject * __this /* static, unused */, int32_t ___minPlayers0, int32_t ___maxPlayers1, StringU5BU5D_t1642385972* ___playersIds2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::CancelMatch(System.String)
extern "C"  void AN_GMSRTMProxy_CancelMatch_m3906857172 (Il2CppObject * __this /* static, unused */, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::DismissMatch(System.String)
extern "C"  void AN_GMSRTMProxy_DismissMatch_m4068888442 (Il2CppObject * __this /* static, unused */, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_FinishMatch(System.String,System.String,System.String[],System.Int32[],System.Int32[],System.Int32[])
extern "C"  void AN_GMSRTMProxy_TBM_FinishMatch_m3870973864 (Il2CppObject * __this /* static, unused */, String_t* ___matchId0, String_t* ___matchData1, StringU5BU5D_t1642385972* ___pIds2, Int32U5BU5D_t3030399641* ___results3, Int32U5BU5D_t3030399641* ___placing4, Int32U5BU5D_t3030399641* ___versions5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_FinishMatchWithId(System.String)
extern "C"  void AN_GMSRTMProxy_TBM_FinishMatchWithId_m3904872548 (Il2CppObject * __this /* static, unused */, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_LeaveMatch(System.String)
extern "C"  void AN_GMSRTMProxy_TBM_LeaveMatch_m1073560091 (Il2CppObject * __this /* static, unused */, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_LeaveMatchDuringTurn(System.String,System.String)
extern "C"  void AN_GMSRTMProxy_TBM_LeaveMatchDuringTurn_m598649635 (Il2CppObject * __this /* static, unused */, String_t* ___matchId0, String_t* ___pendingParticipantId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_LoadMatchInfo(System.String)
extern "C"  void AN_GMSRTMProxy_TBM_LoadMatchInfo_m1664198524 (Il2CppObject * __this /* static, unused */, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_LoadMatchesInfo(System.Int32,System.Int32[])
extern "C"  void AN_GMSRTMProxy_TBM_LoadMatchesInfo_m444590238 (Il2CppObject * __this /* static, unused */, int32_t ___invitationSortOrder0, Int32U5BU5D_t3030399641* ___matchTurnStatuses1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_LoadAllMatchesInfo(System.Int32)
extern "C"  void AN_GMSRTMProxy_TBM_LoadAllMatchesInfo_m2050164414 (Il2CppObject * __this /* static, unused */, int32_t ___invitationSortOrder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_Rematch(System.String)
extern "C"  void AN_GMSRTMProxy_TBM_Rematch_m1939725039 (Il2CppObject * __this /* static, unused */, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_RegisterMatchUpdateListener()
extern "C"  void AN_GMSRTMProxy_TBM_RegisterMatchUpdateListener_m2288852664 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_UnregisterMatchUpdateListener()
extern "C"  void AN_GMSRTMProxy_TBM_UnregisterMatchUpdateListener_m3921583511 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_TakeTrun(System.String,System.String,System.String,System.String[],System.Int32[],System.Int32[],System.Int32[])
extern "C"  void AN_GMSRTMProxy_TBM_TakeTrun_m3076291486 (Il2CppObject * __this /* static, unused */, String_t* ___matchId0, String_t* ___matchData1, String_t* ___pendingParticipantId2, StringU5BU5D_t1642385972* ___pIds3, Int32U5BU5D_t3030399641* ___results4, Int32U5BU5D_t3030399641* ___placing5, Int32U5BU5D_t3030399641* ___versions6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::StartSelectOpponentsView(System.Int32,System.Int32,System.Boolean)
extern "C"  void AN_GMSRTMProxy_StartSelectOpponentsView_m3530484485 (Il2CppObject * __this /* static, unused */, int32_t ___minPlayers0, int32_t ___maxPlayers1, bool ___allowAutomatch2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_ShowInbox()
extern "C"  void AN_GMSRTMProxy_TBM_ShowInbox_m3581998724 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_SetVariant(System.Int32)
extern "C"  void AN_GMSRTMProxy_TBM_SetVariant_m797803429 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSRTMProxy::TBM_SetExclusiveBitMask(System.Int32)
extern "C"  void AN_GMSRTMProxy_TBM_SetExclusiveBitMask_m3830786159 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
