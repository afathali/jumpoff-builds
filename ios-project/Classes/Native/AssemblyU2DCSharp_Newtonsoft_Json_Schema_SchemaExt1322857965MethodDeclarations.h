﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.SchemaExtensions/<IsValid>c__AnonStorey20
struct U3CIsValidU3Ec__AnonStorey20_t1322857965;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Schema.ValidationEventArgs
struct ValidationEventArgs_t130261338;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_Validation130261338.h"

// System.Void Newtonsoft.Json.Schema.SchemaExtensions/<IsValid>c__AnonStorey20::.ctor()
extern "C"  void U3CIsValidU3Ec__AnonStorey20__ctor_m2487479508 (U3CIsValidU3Ec__AnonStorey20_t1322857965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.SchemaExtensions/<IsValid>c__AnonStorey20::<>m__D7(System.Object,Newtonsoft.Json.Schema.ValidationEventArgs)
extern "C"  void U3CIsValidU3Ec__AnonStorey20_U3CU3Em__D7_m1295234348 (U3CIsValidU3Ec__AnonStorey20_t1322857965 * __this, Il2CppObject * ___sender0, ValidationEventArgs_t130261338 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
