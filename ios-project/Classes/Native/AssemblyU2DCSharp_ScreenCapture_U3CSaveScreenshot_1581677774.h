﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14
struct  U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774  : public Il2CppObject
{
public:
	// UnityEngine.Texture2D ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14::<texture>__0
	Texture2D_t3542995729 * ___U3CtextureU3E__0_0;
	// System.Byte[] ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14::<bytes>__1
	ByteU5BU5D_t3397334013* ___U3CbytesU3E__1_1;
	// System.String ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14::filePath
	String_t* ___filePath_2;
	// System.Int32 ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14::$PC
	int32_t ___U24PC_3;
	// System.Object ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14::$current
	Il2CppObject * ___U24current_4;
	// System.String ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14::<$>filePath
	String_t* ___U3CU24U3EfilePath_5;

public:
	inline static int32_t get_offset_of_U3CtextureU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774, ___U3CtextureU3E__0_0)); }
	inline Texture2D_t3542995729 * get_U3CtextureU3E__0_0() const { return ___U3CtextureU3E__0_0; }
	inline Texture2D_t3542995729 ** get_address_of_U3CtextureU3E__0_0() { return &___U3CtextureU3E__0_0; }
	inline void set_U3CtextureU3E__0_0(Texture2D_t3542995729 * value)
	{
		___U3CtextureU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtextureU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__1_1() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774, ___U3CbytesU3E__1_1)); }
	inline ByteU5BU5D_t3397334013* get_U3CbytesU3E__1_1() const { return ___U3CbytesU3E__1_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CbytesU3E__1_1() { return &___U3CbytesU3E__1_1; }
	inline void set_U3CbytesU3E__1_1(ByteU5BU5D_t3397334013* value)
	{
		___U3CbytesU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbytesU3E__1_1, value);
	}

	inline static int32_t get_offset_of_filePath_2() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774, ___filePath_2)); }
	inline String_t* get_filePath_2() const { return ___filePath_2; }
	inline String_t** get_address_of_filePath_2() { return &___filePath_2; }
	inline void set_filePath_2(String_t* value)
	{
		___filePath_2 = value;
		Il2CppCodeGenWriteBarrier(&___filePath_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EfilePath_5() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774, ___U3CU24U3EfilePath_5)); }
	inline String_t* get_U3CU24U3EfilePath_5() const { return ___U3CU24U3EfilePath_5; }
	inline String_t** get_address_of_U3CU24U3EfilePath_5() { return &___U3CU24U3EfilePath_5; }
	inline void set_U3CU24U3EfilePath_5(String_t* value)
	{
		___U3CU24U3EfilePath_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EfilePath_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
