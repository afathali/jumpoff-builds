﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// RegisterRequest
struct RegisterRequest_t698226714;
// User
struct User_t719925459;

#include "AssemblyU2DCSharp_Shared_Response_1_gen1581705971.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegisterResponse
struct  RegisterResponse_t410466074  : public Response_1_t1581705971
{
public:
	// System.Boolean RegisterResponse::<Success>k__BackingField
	bool ___U3CSuccessU3Ek__BackingField_0;
	// System.String RegisterResponse::<ErrorMessage>k__BackingField
	String_t* ___U3CErrorMessageU3Ek__BackingField_1;
	// RegisterRequest RegisterResponse::<Request>k__BackingField
	RegisterRequest_t698226714 * ___U3CRequestU3Ek__BackingField_2;
	// System.String RegisterResponse::<AutoLogin>k__BackingField
	String_t* ___U3CAutoLoginU3Ek__BackingField_3;
	// User RegisterResponse::<User>k__BackingField
	User_t719925459 * ___U3CUserU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CSuccessU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RegisterResponse_t410466074, ___U3CSuccessU3Ek__BackingField_0)); }
	inline bool get_U3CSuccessU3Ek__BackingField_0() const { return ___U3CSuccessU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CSuccessU3Ek__BackingField_0() { return &___U3CSuccessU3Ek__BackingField_0; }
	inline void set_U3CSuccessU3Ek__BackingField_0(bool value)
	{
		___U3CSuccessU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CErrorMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RegisterResponse_t410466074, ___U3CErrorMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CErrorMessageU3Ek__BackingField_1() const { return ___U3CErrorMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CErrorMessageU3Ek__BackingField_1() { return &___U3CErrorMessageU3Ek__BackingField_1; }
	inline void set_U3CErrorMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CErrorMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CErrorMessageU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RegisterResponse_t410466074, ___U3CRequestU3Ek__BackingField_2)); }
	inline RegisterRequest_t698226714 * get_U3CRequestU3Ek__BackingField_2() const { return ___U3CRequestU3Ek__BackingField_2; }
	inline RegisterRequest_t698226714 ** get_address_of_U3CRequestU3Ek__BackingField_2() { return &___U3CRequestU3Ek__BackingField_2; }
	inline void set_U3CRequestU3Ek__BackingField_2(RegisterRequest_t698226714 * value)
	{
		___U3CRequestU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CAutoLoginU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RegisterResponse_t410466074, ___U3CAutoLoginU3Ek__BackingField_3)); }
	inline String_t* get_U3CAutoLoginU3Ek__BackingField_3() const { return ___U3CAutoLoginU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CAutoLoginU3Ek__BackingField_3() { return &___U3CAutoLoginU3Ek__BackingField_3; }
	inline void set_U3CAutoLoginU3Ek__BackingField_3(String_t* value)
	{
		___U3CAutoLoginU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAutoLoginU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CUserU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RegisterResponse_t410466074, ___U3CUserU3Ek__BackingField_4)); }
	inline User_t719925459 * get_U3CUserU3Ek__BackingField_4() const { return ___U3CUserU3Ek__BackingField_4; }
	inline User_t719925459 ** get_address_of_U3CUserU3Ek__BackingField_4() { return &___U3CUserU3Ek__BackingField_4; }
	inline void set_U3CUserU3Ek__BackingField_4(User_t719925459 * value)
	{
		___U3CUserU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUserU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
