﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// System.Collections.Generic.List`1<JumpPoint>
struct List_1_t378981738;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen506773895.h"
#include "mscorlib_System_Nullable_1_gen2293140233.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Course
struct  Course_t3483112699  : public Il2CppObject
{
public:
	// System.Nullable`1<UnityEngine.Vector3> Course::ImagePosition
	Nullable_1_t506773895  ___ImagePosition_0;
	// System.Nullable`1<UnityEngine.Quaternion> Course::ImageRotation
	Nullable_1_t2293140233  ___ImageRotation_1;
	// System.Nullable`1<UnityEngine.Vector3> Course::ImageScale
	Nullable_1_t506773895  ___ImageScale_2;
	// System.Boolean Course::course_show_cheats
	bool ___course_show_cheats_3;
	// System.Boolean Course::course_show_image
	bool ___course_show_image_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Course::linePoints
	List_1_t1612828712 * ___linePoints_5;
	// System.Int64 Course::<Id>k__BackingField
	int64_t ___U3CIdU3Ek__BackingField_6;
	// System.Byte[] Course::<Image>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CImageU3Ek__BackingField_7;
	// System.String Course::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_8;
	// System.String Course::<Show>k__BackingField
	String_t* ___U3CShowU3Ek__BackingField_9;
	// System.String Course::<Date>k__BackingField
	String_t* ___U3CDateU3Ek__BackingField_10;
	// System.String Course::<Notes>k__BackingField
	String_t* ___U3CNotesU3Ek__BackingField_11;
	// System.Collections.Generic.List`1<JumpPoint> Course::<Jumps>k__BackingField
	List_1_t378981738 * ___U3CJumpsU3Ek__BackingField_12;
	// System.String Course::<Scale>k__BackingField
	String_t* ___U3CScaleU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_ImagePosition_0() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___ImagePosition_0)); }
	inline Nullable_1_t506773895  get_ImagePosition_0() const { return ___ImagePosition_0; }
	inline Nullable_1_t506773895 * get_address_of_ImagePosition_0() { return &___ImagePosition_0; }
	inline void set_ImagePosition_0(Nullable_1_t506773895  value)
	{
		___ImagePosition_0 = value;
	}

	inline static int32_t get_offset_of_ImageRotation_1() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___ImageRotation_1)); }
	inline Nullable_1_t2293140233  get_ImageRotation_1() const { return ___ImageRotation_1; }
	inline Nullable_1_t2293140233 * get_address_of_ImageRotation_1() { return &___ImageRotation_1; }
	inline void set_ImageRotation_1(Nullable_1_t2293140233  value)
	{
		___ImageRotation_1 = value;
	}

	inline static int32_t get_offset_of_ImageScale_2() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___ImageScale_2)); }
	inline Nullable_1_t506773895  get_ImageScale_2() const { return ___ImageScale_2; }
	inline Nullable_1_t506773895 * get_address_of_ImageScale_2() { return &___ImageScale_2; }
	inline void set_ImageScale_2(Nullable_1_t506773895  value)
	{
		___ImageScale_2 = value;
	}

	inline static int32_t get_offset_of_course_show_cheats_3() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___course_show_cheats_3)); }
	inline bool get_course_show_cheats_3() const { return ___course_show_cheats_3; }
	inline bool* get_address_of_course_show_cheats_3() { return &___course_show_cheats_3; }
	inline void set_course_show_cheats_3(bool value)
	{
		___course_show_cheats_3 = value;
	}

	inline static int32_t get_offset_of_course_show_image_4() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___course_show_image_4)); }
	inline bool get_course_show_image_4() const { return ___course_show_image_4; }
	inline bool* get_address_of_course_show_image_4() { return &___course_show_image_4; }
	inline void set_course_show_image_4(bool value)
	{
		___course_show_image_4 = value;
	}

	inline static int32_t get_offset_of_linePoints_5() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___linePoints_5)); }
	inline List_1_t1612828712 * get_linePoints_5() const { return ___linePoints_5; }
	inline List_1_t1612828712 ** get_address_of_linePoints_5() { return &___linePoints_5; }
	inline void set_linePoints_5(List_1_t1612828712 * value)
	{
		___linePoints_5 = value;
		Il2CppCodeGenWriteBarrier(&___linePoints_5, value);
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___U3CIdU3Ek__BackingField_6)); }
	inline int64_t get_U3CIdU3Ek__BackingField_6() const { return ___U3CIdU3Ek__BackingField_6; }
	inline int64_t* get_address_of_U3CIdU3Ek__BackingField_6() { return &___U3CIdU3Ek__BackingField_6; }
	inline void set_U3CIdU3Ek__BackingField_6(int64_t value)
	{
		___U3CIdU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CImageU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___U3CImageU3Ek__BackingField_7)); }
	inline ByteU5BU5D_t3397334013* get_U3CImageU3Ek__BackingField_7() const { return ___U3CImageU3Ek__BackingField_7; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CImageU3Ek__BackingField_7() { return &___U3CImageU3Ek__BackingField_7; }
	inline void set_U3CImageU3Ek__BackingField_7(ByteU5BU5D_t3397334013* value)
	{
		___U3CImageU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CImageU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___U3CNameU3Ek__BackingField_8)); }
	inline String_t* get_U3CNameU3Ek__BackingField_8() const { return ___U3CNameU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_8() { return &___U3CNameU3Ek__BackingField_8; }
	inline void set_U3CNameU3Ek__BackingField_8(String_t* value)
	{
		___U3CNameU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNameU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CShowU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___U3CShowU3Ek__BackingField_9)); }
	inline String_t* get_U3CShowU3Ek__BackingField_9() const { return ___U3CShowU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CShowU3Ek__BackingField_9() { return &___U3CShowU3Ek__BackingField_9; }
	inline void set_U3CShowU3Ek__BackingField_9(String_t* value)
	{
		___U3CShowU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CShowU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CDateU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___U3CDateU3Ek__BackingField_10)); }
	inline String_t* get_U3CDateU3Ek__BackingField_10() const { return ___U3CDateU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CDateU3Ek__BackingField_10() { return &___U3CDateU3Ek__BackingField_10; }
	inline void set_U3CDateU3Ek__BackingField_10(String_t* value)
	{
		___U3CDateU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDateU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CNotesU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___U3CNotesU3Ek__BackingField_11)); }
	inline String_t* get_U3CNotesU3Ek__BackingField_11() const { return ___U3CNotesU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CNotesU3Ek__BackingField_11() { return &___U3CNotesU3Ek__BackingField_11; }
	inline void set_U3CNotesU3Ek__BackingField_11(String_t* value)
	{
		___U3CNotesU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNotesU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CJumpsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___U3CJumpsU3Ek__BackingField_12)); }
	inline List_1_t378981738 * get_U3CJumpsU3Ek__BackingField_12() const { return ___U3CJumpsU3Ek__BackingField_12; }
	inline List_1_t378981738 ** get_address_of_U3CJumpsU3Ek__BackingField_12() { return &___U3CJumpsU3Ek__BackingField_12; }
	inline void set_U3CJumpsU3Ek__BackingField_12(List_1_t378981738 * value)
	{
		___U3CJumpsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CJumpsU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CScaleU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Course_t3483112699, ___U3CScaleU3Ek__BackingField_13)); }
	inline String_t* get_U3CScaleU3Ek__BackingField_13() const { return ___U3CScaleU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CScaleU3Ek__BackingField_13() { return &___U3CScaleU3Ek__BackingField_13; }
	inline void set_U3CScaleU3Ek__BackingField_13(String_t* value)
	{
		___U3CScaleU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CScaleU3Ek__BackingField_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
