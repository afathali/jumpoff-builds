﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.EnumMemberAttribute
struct EnumMemberAttribute_t187433993;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Runtime.Serialization.EnumMemberAttribute::.ctor()
extern "C"  void EnumMemberAttribute__ctor_m228733601 (EnumMemberAttribute_t187433993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.EnumMemberAttribute::get_Value()
extern "C"  String_t* EnumMemberAttribute_get_Value_m2788748028 (EnumMemberAttribute_t187433993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.EnumMemberAttribute::set_Value(System.String)
extern "C"  void EnumMemberAttribute_set_Value_m1939598701 (EnumMemberAttribute_t187433993 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
