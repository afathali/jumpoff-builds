﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookAndroidUseExample/<PostScreenshot>c__Iterator5
struct U3CPostScreenshotU3Ec__Iterator5_t1609060767;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FacebookAndroidUseExample/<PostScreenshot>c__Iterator5::.ctor()
extern "C"  void U3CPostScreenshotU3Ec__Iterator5__ctor_m1231824160 (U3CPostScreenshotU3Ec__Iterator5_t1609060767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FacebookAndroidUseExample/<PostScreenshot>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPostScreenshotU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2148717860 (U3CPostScreenshotU3Ec__Iterator5_t1609060767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FacebookAndroidUseExample/<PostScreenshot>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPostScreenshotU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m238071068 (U3CPostScreenshotU3Ec__Iterator5_t1609060767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FacebookAndroidUseExample/<PostScreenshot>c__Iterator5::MoveNext()
extern "C"  bool U3CPostScreenshotU3Ec__Iterator5_MoveNext_m721932324 (U3CPostScreenshotU3Ec__Iterator5_t1609060767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample/<PostScreenshot>c__Iterator5::Dispose()
extern "C"  void U3CPostScreenshotU3Ec__Iterator5_Dispose_m1920351901 (U3CPostScreenshotU3Ec__Iterator5_t1609060767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample/<PostScreenshot>c__Iterator5::Reset()
extern "C"  void U3CPostScreenshotU3Ec__Iterator5_Reset_m400655703 (U3CPostScreenshotU3Ec__Iterator5_t1609060767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
