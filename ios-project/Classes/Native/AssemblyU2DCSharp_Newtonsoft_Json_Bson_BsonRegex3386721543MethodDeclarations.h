﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonRegex
struct BsonRegex_t3386721543;
// System.String
struct String_t;
// Newtonsoft.Json.Bson.BsonString
struct BsonString_t3501425379;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonString3501425379.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonType2055433366.h"

// System.Void Newtonsoft.Json.Bson.BsonRegex::.ctor(System.String,System.String)
extern "C"  void BsonRegex__ctor_m241179919 (BsonRegex_t3386721543 * __this, String_t* ___pattern0, String_t* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::get_Pattern()
extern "C"  BsonString_t3501425379 * BsonRegex_get_Pattern_m3912991305 (BsonRegex_t3386721543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonRegex::set_Pattern(Newtonsoft.Json.Bson.BsonString)
extern "C"  void BsonRegex_set_Pattern_m2902897932 (BsonRegex_t3386721543 * __this, BsonString_t3501425379 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::get_Options()
extern "C"  BsonString_t3501425379 * BsonRegex_get_Options_m3881961249 (BsonRegex_t3386721543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonRegex::set_Options(Newtonsoft.Json.Bson.BsonString)
extern "C"  void BsonRegex_set_Options_m1123295220 (BsonRegex_t3386721543 * __this, BsonString_t3501425379 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonRegex::get_Type()
extern "C"  int8_t BsonRegex_get_Type_m2728316696 (BsonRegex_t3386721543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
