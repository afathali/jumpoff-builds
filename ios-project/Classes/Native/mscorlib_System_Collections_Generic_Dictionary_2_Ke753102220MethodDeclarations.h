﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct Dictionary_2_t2358566078;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke753102220.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3725287957_gshared (Enumerator_t753102220 * __this, Dictionary_2_t2358566078 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3725287957(__this, ___host0, method) ((  void (*) (Enumerator_t753102220 *, Dictionary_2_t2358566078 *, const MethodInfo*))Enumerator__ctor_m3725287957_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2429470792_gshared (Enumerator_t753102220 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2429470792(__this, method) ((  Il2CppObject * (*) (Enumerator_t753102220 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2429470792_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1948070592_gshared (Enumerator_t753102220 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1948070592(__this, method) ((  void (*) (Enumerator_t753102220 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1948070592_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1696555509_gshared (Enumerator_t753102220 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1696555509(__this, method) ((  void (*) (Enumerator_t753102220 *, const MethodInfo*))Enumerator_Dispose_m1696555509_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m614304652_gshared (Enumerator_t753102220 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m614304652(__this, method) ((  bool (*) (Enumerator_t753102220 *, const MethodInfo*))Enumerator_MoveNext_m614304652_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t3132015601  Enumerator_get_Current_m530044104_gshared (Enumerator_t753102220 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m530044104(__this, method) ((  KeyValuePair_2_t3132015601  (*) (Enumerator_t753102220 *, const MethodInfo*))Enumerator_get_Current_m530044104_gshared)(__this, method)
