﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::.ctor()
#define Dictionary_2__ctor_m1584300536(__this, method) ((  void (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1169495424(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t309026718 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m406310120_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m1758418869(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t309026718 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2602799901_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::.ctor(System.Int32)
#define Dictionary_2__ctor_m1601464600(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t309026718 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m206582704_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m2636877064(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t309026718 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3143729840_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m3637121669(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t309026718 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2391180541_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m2936372550(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t309026718 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1206668798_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3714628307(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m853262843_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1377459555(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2954370043_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3619621869(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m673000885_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m103864253(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1552474645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3901281076(__this, method) ((  bool (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m286716188_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m221410697(__this, method) ((  bool (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m114053137_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m393225983(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t309026718 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m237963271_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1036353082(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t309026718 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3775521570_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m586911021(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t309026718 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m984276885_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m3559095273(__this, ___key0, method) ((  bool (*) (Dictionary_2_t309026718 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2868006769_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m2166748190(__this, ___key0, method) ((  void (*) (Dictionary_2_t309026718 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2017099222_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m145650011(__this, method) ((  bool (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m960517203_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m227750163(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1900166091_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2810929661(__this, method) ((  bool (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4094240197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m450112492(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t309026718 *, KeyValuePair_2_t2361339236 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m990341268_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m119842376(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t309026718 *, KeyValuePair_2_t2361339236 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1058501024_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1267298472(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t309026718 *, KeyValuePair_2U5BU5D_t2783779981*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m976354816_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2950138975(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t309026718 *, KeyValuePair_2_t2361339236 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1705959559_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2589460499(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t309026718 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3578539931_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1676998398(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3100111910_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4166753701(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2925090477_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2200649168(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2684932776_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::get_Count()
#define Dictionary_2_get_Count_m449836954(__this, method) ((  int32_t (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_get_Count_m3636113691_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::get_Item(TKey)
#define Dictionary_2_get_Item_m2399619498(__this, ___key0, method) ((  List_1_t2689214752 * (*) (Dictionary_2_t309026718 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m2413909512_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m363531463(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t309026718 *, String_t*, List_1_t2689214752 *, const MethodInfo*))Dictionary_2_set_Item_m458653679_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m2319175151(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t309026718 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1045257495_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m1913651532(__this, ___size0, method) ((  void (*) (Dictionary_2_t309026718 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2270022740_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m603079670(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t309026718 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2147716750_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m836291700(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2361339236  (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t2689214752 *, const MethodInfo*))Dictionary_2_make_pair_m2631942124_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m2180059722(__this /* static, unused */, ___key0, ___value1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t2689214752 *, const MethodInfo*))Dictionary_2_pick_key_m2840829442_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m3166690498(__this /* static, unused */, ___key0, ___value1, method) ((  List_1_t2689214752 * (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t2689214752 *, const MethodInfo*))Dictionary_2_pick_value_m1872663242_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m1901332283(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t309026718 *, KeyValuePair_2U5BU5D_t2783779981*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1495142643_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::Resize()
#define Dictionary_2_Resize_m462245533(__this, method) ((  void (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_Resize_m2672264133_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::Add(TKey,TValue)
#define Dictionary_2_Add_m1705925376(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t309026718 *, String_t*, List_1_t2689214752 *, const MethodInfo*))Dictionary_2_Add_m1708621268_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::Clear()
#define Dictionary_2_Clear_m929475975(__this, method) ((  void (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m3114932039(__this, ___key0, method) ((  bool (*) (Dictionary_2_t309026718 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m3553426152_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m2326029144(__this, ___value0, method) ((  bool (*) (Dictionary_2_t309026718 *, List_1_t2689214752 *, const MethodInfo*))Dictionary_2_ContainsValue_m2375979648_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m2429810023(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t309026718 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2864531407_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m2890216031(__this, ___sender0, method) ((  void (*) (Dictionary_2_t309026718 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2160537783_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::Remove(TKey)
#define Dictionary_2_Remove_m1102993185(__this, ___key0, method) ((  bool (*) (Dictionary_2_t309026718 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m1366616528_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m2741405991(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t309026718 *, String_t*, List_1_t2689214752 **, const MethodInfo*))Dictionary_2_TryGetValue_m1120370623_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::get_Keys()
#define Dictionary_2_get_Keys_m3801345396(__this, method) ((  KeyCollection_t2792524489 * (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_get_Keys_m1635778172_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::get_Values()
#define Dictionary_2_get_Values_m507892652(__this, method) ((  ValueCollection_t3307053857 * (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_get_Values_m825860460_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m3462748853(__this, ___key0, method) ((  String_t* (*) (Dictionary_2_t309026718 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4209561517_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m1062792853(__this, ___value0, method) ((  List_1_t2689214752 * (*) (Dictionary_2_t309026718 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1381983709_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m3724676647(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t309026718 *, KeyValuePair_2_t2361339236 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m663697471_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m466775946(__this, method) ((  Enumerator_t1629051420  (*) (Dictionary_2_t309026718 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1752238884_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m1699725837(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t2689214752 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2061238213_gshared)(__this /* static, unused */, ___key0, ___value1, method)
