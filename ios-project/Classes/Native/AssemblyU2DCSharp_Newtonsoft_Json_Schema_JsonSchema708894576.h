﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2570160834;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel>
struct IList_1_t1249835177;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>
struct IDictionary_2_t622757259;
// Newtonsoft.Json.Schema.JsonSchemaModel
struct JsonSchemaModel_t708894576;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t3093584614;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen2341081996.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Schema.JsonSchemaModel
struct  JsonSchemaModel_t708894576  : public Il2CppObject
{
public:
	// System.Boolean Newtonsoft.Json.Schema.JsonSchemaModel::<Required>k__BackingField
	bool ___U3CRequiredU3Ek__BackingField_0;
	// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.Schema.JsonSchemaModel::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_1;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Schema.JsonSchemaModel::<MinimumLength>k__BackingField
	Nullable_1_t334943763  ___U3CMinimumLengthU3Ek__BackingField_2;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Schema.JsonSchemaModel::<MaximumLength>k__BackingField
	Nullable_1_t334943763  ___U3CMaximumLengthU3Ek__BackingField_3;
	// System.Nullable`1<System.Double> Newtonsoft.Json.Schema.JsonSchemaModel::<DivisibleBy>k__BackingField
	Nullable_1_t2341081996  ___U3CDivisibleByU3Ek__BackingField_4;
	// System.Nullable`1<System.Double> Newtonsoft.Json.Schema.JsonSchemaModel::<Minimum>k__BackingField
	Nullable_1_t2341081996  ___U3CMinimumU3Ek__BackingField_5;
	// System.Nullable`1<System.Double> Newtonsoft.Json.Schema.JsonSchemaModel::<Maximum>k__BackingField
	Nullable_1_t2341081996  ___U3CMaximumU3Ek__BackingField_6;
	// System.Boolean Newtonsoft.Json.Schema.JsonSchemaModel::<ExclusiveMinimum>k__BackingField
	bool ___U3CExclusiveMinimumU3Ek__BackingField_7;
	// System.Boolean Newtonsoft.Json.Schema.JsonSchemaModel::<ExclusiveMaximum>k__BackingField
	bool ___U3CExclusiveMaximumU3Ek__BackingField_8;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Schema.JsonSchemaModel::<MinimumItems>k__BackingField
	Nullable_1_t334943763  ___U3CMinimumItemsU3Ek__BackingField_9;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Schema.JsonSchemaModel::<MaximumItems>k__BackingField
	Nullable_1_t334943763  ___U3CMaximumItemsU3Ek__BackingField_10;
	// System.Collections.Generic.IList`1<System.String> Newtonsoft.Json.Schema.JsonSchemaModel::<Patterns>k__BackingField
	Il2CppObject* ___U3CPatternsU3Ek__BackingField_11;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.Schema.JsonSchemaModel::<Items>k__BackingField
	Il2CppObject* ___U3CItemsU3Ek__BackingField_12;
	// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.Schema.JsonSchemaModel::<Properties>k__BackingField
	Il2CppObject* ___U3CPropertiesU3Ek__BackingField_13;
	// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.Schema.JsonSchemaModel::<PatternProperties>k__BackingField
	Il2CppObject* ___U3CPatternPropertiesU3Ek__BackingField_14;
	// Newtonsoft.Json.Schema.JsonSchemaModel Newtonsoft.Json.Schema.JsonSchemaModel::<AdditionalProperties>k__BackingField
	JsonSchemaModel_t708894576 * ___U3CAdditionalPropertiesU3Ek__BackingField_15;
	// System.Boolean Newtonsoft.Json.Schema.JsonSchemaModel::<AllowAdditionalProperties>k__BackingField
	bool ___U3CAllowAdditionalPropertiesU3Ek__BackingField_16;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Schema.JsonSchemaModel::<Enum>k__BackingField
	Il2CppObject* ___U3CEnumU3Ek__BackingField_17;
	// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.Schema.JsonSchemaModel::<Disallow>k__BackingField
	int32_t ___U3CDisallowU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_U3CRequiredU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CRequiredU3Ek__BackingField_0)); }
	inline bool get_U3CRequiredU3Ek__BackingField_0() const { return ___U3CRequiredU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CRequiredU3Ek__BackingField_0() { return &___U3CRequiredU3Ek__BackingField_0; }
	inline void set_U3CRequiredU3Ek__BackingField_0(bool value)
	{
		___U3CRequiredU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumLengthU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CMinimumLengthU3Ek__BackingField_2)); }
	inline Nullable_1_t334943763  get_U3CMinimumLengthU3Ek__BackingField_2() const { return ___U3CMinimumLengthU3Ek__BackingField_2; }
	inline Nullable_1_t334943763 * get_address_of_U3CMinimumLengthU3Ek__BackingField_2() { return &___U3CMinimumLengthU3Ek__BackingField_2; }
	inline void set_U3CMinimumLengthU3Ek__BackingField_2(Nullable_1_t334943763  value)
	{
		___U3CMinimumLengthU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CMaximumLengthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CMaximumLengthU3Ek__BackingField_3)); }
	inline Nullable_1_t334943763  get_U3CMaximumLengthU3Ek__BackingField_3() const { return ___U3CMaximumLengthU3Ek__BackingField_3; }
	inline Nullable_1_t334943763 * get_address_of_U3CMaximumLengthU3Ek__BackingField_3() { return &___U3CMaximumLengthU3Ek__BackingField_3; }
	inline void set_U3CMaximumLengthU3Ek__BackingField_3(Nullable_1_t334943763  value)
	{
		___U3CMaximumLengthU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDivisibleByU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CDivisibleByU3Ek__BackingField_4)); }
	inline Nullable_1_t2341081996  get_U3CDivisibleByU3Ek__BackingField_4() const { return ___U3CDivisibleByU3Ek__BackingField_4; }
	inline Nullable_1_t2341081996 * get_address_of_U3CDivisibleByU3Ek__BackingField_4() { return &___U3CDivisibleByU3Ek__BackingField_4; }
	inline void set_U3CDivisibleByU3Ek__BackingField_4(Nullable_1_t2341081996  value)
	{
		___U3CDivisibleByU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CMinimumU3Ek__BackingField_5)); }
	inline Nullable_1_t2341081996  get_U3CMinimumU3Ek__BackingField_5() const { return ___U3CMinimumU3Ek__BackingField_5; }
	inline Nullable_1_t2341081996 * get_address_of_U3CMinimumU3Ek__BackingField_5() { return &___U3CMinimumU3Ek__BackingField_5; }
	inline void set_U3CMinimumU3Ek__BackingField_5(Nullable_1_t2341081996  value)
	{
		___U3CMinimumU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMaximumU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CMaximumU3Ek__BackingField_6)); }
	inline Nullable_1_t2341081996  get_U3CMaximumU3Ek__BackingField_6() const { return ___U3CMaximumU3Ek__BackingField_6; }
	inline Nullable_1_t2341081996 * get_address_of_U3CMaximumU3Ek__BackingField_6() { return &___U3CMaximumU3Ek__BackingField_6; }
	inline void set_U3CMaximumU3Ek__BackingField_6(Nullable_1_t2341081996  value)
	{
		___U3CMaximumU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CExclusiveMinimumU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CExclusiveMinimumU3Ek__BackingField_7)); }
	inline bool get_U3CExclusiveMinimumU3Ek__BackingField_7() const { return ___U3CExclusiveMinimumU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CExclusiveMinimumU3Ek__BackingField_7() { return &___U3CExclusiveMinimumU3Ek__BackingField_7; }
	inline void set_U3CExclusiveMinimumU3Ek__BackingField_7(bool value)
	{
		___U3CExclusiveMinimumU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CExclusiveMaximumU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CExclusiveMaximumU3Ek__BackingField_8)); }
	inline bool get_U3CExclusiveMaximumU3Ek__BackingField_8() const { return ___U3CExclusiveMaximumU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CExclusiveMaximumU3Ek__BackingField_8() { return &___U3CExclusiveMaximumU3Ek__BackingField_8; }
	inline void set_U3CExclusiveMaximumU3Ek__BackingField_8(bool value)
	{
		___U3CExclusiveMaximumU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumItemsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CMinimumItemsU3Ek__BackingField_9)); }
	inline Nullable_1_t334943763  get_U3CMinimumItemsU3Ek__BackingField_9() const { return ___U3CMinimumItemsU3Ek__BackingField_9; }
	inline Nullable_1_t334943763 * get_address_of_U3CMinimumItemsU3Ek__BackingField_9() { return &___U3CMinimumItemsU3Ek__BackingField_9; }
	inline void set_U3CMinimumItemsU3Ek__BackingField_9(Nullable_1_t334943763  value)
	{
		___U3CMinimumItemsU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CMaximumItemsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CMaximumItemsU3Ek__BackingField_10)); }
	inline Nullable_1_t334943763  get_U3CMaximumItemsU3Ek__BackingField_10() const { return ___U3CMaximumItemsU3Ek__BackingField_10; }
	inline Nullable_1_t334943763 * get_address_of_U3CMaximumItemsU3Ek__BackingField_10() { return &___U3CMaximumItemsU3Ek__BackingField_10; }
	inline void set_U3CMaximumItemsU3Ek__BackingField_10(Nullable_1_t334943763  value)
	{
		___U3CMaximumItemsU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CPatternsU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CPatternsU3Ek__BackingField_11)); }
	inline Il2CppObject* get_U3CPatternsU3Ek__BackingField_11() const { return ___U3CPatternsU3Ek__BackingField_11; }
	inline Il2CppObject** get_address_of_U3CPatternsU3Ek__BackingField_11() { return &___U3CPatternsU3Ek__BackingField_11; }
	inline void set_U3CPatternsU3Ek__BackingField_11(Il2CppObject* value)
	{
		___U3CPatternsU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPatternsU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CItemsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CItemsU3Ek__BackingField_12)); }
	inline Il2CppObject* get_U3CItemsU3Ek__BackingField_12() const { return ___U3CItemsU3Ek__BackingField_12; }
	inline Il2CppObject** get_address_of_U3CItemsU3Ek__BackingField_12() { return &___U3CItemsU3Ek__BackingField_12; }
	inline void set_U3CItemsU3Ek__BackingField_12(Il2CppObject* value)
	{
		___U3CItemsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemsU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CPropertiesU3Ek__BackingField_13)); }
	inline Il2CppObject* get_U3CPropertiesU3Ek__BackingField_13() const { return ___U3CPropertiesU3Ek__BackingField_13; }
	inline Il2CppObject** get_address_of_U3CPropertiesU3Ek__BackingField_13() { return &___U3CPropertiesU3Ek__BackingField_13; }
	inline void set_U3CPropertiesU3Ek__BackingField_13(Il2CppObject* value)
	{
		___U3CPropertiesU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPropertiesU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CPatternPropertiesU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CPatternPropertiesU3Ek__BackingField_14)); }
	inline Il2CppObject* get_U3CPatternPropertiesU3Ek__BackingField_14() const { return ___U3CPatternPropertiesU3Ek__BackingField_14; }
	inline Il2CppObject** get_address_of_U3CPatternPropertiesU3Ek__BackingField_14() { return &___U3CPatternPropertiesU3Ek__BackingField_14; }
	inline void set_U3CPatternPropertiesU3Ek__BackingField_14(Il2CppObject* value)
	{
		___U3CPatternPropertiesU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPatternPropertiesU3Ek__BackingField_14, value);
	}

	inline static int32_t get_offset_of_U3CAdditionalPropertiesU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CAdditionalPropertiesU3Ek__BackingField_15)); }
	inline JsonSchemaModel_t708894576 * get_U3CAdditionalPropertiesU3Ek__BackingField_15() const { return ___U3CAdditionalPropertiesU3Ek__BackingField_15; }
	inline JsonSchemaModel_t708894576 ** get_address_of_U3CAdditionalPropertiesU3Ek__BackingField_15() { return &___U3CAdditionalPropertiesU3Ek__BackingField_15; }
	inline void set_U3CAdditionalPropertiesU3Ek__BackingField_15(JsonSchemaModel_t708894576 * value)
	{
		___U3CAdditionalPropertiesU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAdditionalPropertiesU3Ek__BackingField_15, value);
	}

	inline static int32_t get_offset_of_U3CAllowAdditionalPropertiesU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CAllowAdditionalPropertiesU3Ek__BackingField_16)); }
	inline bool get_U3CAllowAdditionalPropertiesU3Ek__BackingField_16() const { return ___U3CAllowAdditionalPropertiesU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CAllowAdditionalPropertiesU3Ek__BackingField_16() { return &___U3CAllowAdditionalPropertiesU3Ek__BackingField_16; }
	inline void set_U3CAllowAdditionalPropertiesU3Ek__BackingField_16(bool value)
	{
		___U3CAllowAdditionalPropertiesU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CEnumU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CEnumU3Ek__BackingField_17)); }
	inline Il2CppObject* get_U3CEnumU3Ek__BackingField_17() const { return ___U3CEnumU3Ek__BackingField_17; }
	inline Il2CppObject** get_address_of_U3CEnumU3Ek__BackingField_17() { return &___U3CEnumU3Ek__BackingField_17; }
	inline void set_U3CEnumU3Ek__BackingField_17(Il2CppObject* value)
	{
		___U3CEnumU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEnumU3Ek__BackingField_17, value);
	}

	inline static int32_t get_offset_of_U3CDisallowU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonSchemaModel_t708894576, ___U3CDisallowU3Ek__BackingField_18)); }
	inline int32_t get_U3CDisallowU3Ek__BackingField_18() const { return ___U3CDisallowU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CDisallowU3Ek__BackingField_18() { return &___U3CDisallowU3Ek__BackingField_18; }
	inline void set_U3CDisallowU3Ek__BackingField_18(int32_t value)
	{
		___U3CDisallowU3Ek__BackingField_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
