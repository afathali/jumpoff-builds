﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_SnapshotConflict
struct GP_SnapshotConflict_t4002586770;
// GP_Snapshot
struct GP_Snapshot_t1489095664;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GP_Snapshot1489095664.h"

// System.Void GP_SnapshotConflict::.ctor(GP_Snapshot,GP_Snapshot)
extern "C"  void GP_SnapshotConflict__ctor_m2361633973 (GP_SnapshotConflict_t4002586770 * __this, GP_Snapshot_t1489095664 * ___s10, GP_Snapshot_t1489095664 * ___s21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_Snapshot GP_SnapshotConflict::get_Snapshot()
extern "C"  GP_Snapshot_t1489095664 * GP_SnapshotConflict_get_Snapshot_m2670539635 (GP_SnapshotConflict_t4002586770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_Snapshot GP_SnapshotConflict::get_ConflictingSnapshot()
extern "C"  GP_Snapshot_t1489095664 * GP_SnapshotConflict_get_ConflictingSnapshot_m3554866297 (GP_SnapshotConflict_t4002586770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_SnapshotConflict::Resolve(GP_Snapshot)
extern "C"  void GP_SnapshotConflict_Resolve_m1033641673 (GP_SnapshotConflict_t4002586770 * __this, GP_Snapshot_t1489095664 * ___snapshot0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
