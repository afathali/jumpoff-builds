﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_LicenseManagerProxy
struct AN_LicenseManagerProxy_t1907271486;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_LicenseManagerProxy::.ctor()
extern "C"  void AN_LicenseManagerProxy__ctor_m2700123851 (AN_LicenseManagerProxy_t1907271486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_LicenseManagerProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_LicenseManagerProxy_CallActivityFunction_m2928052892 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_LicenseManagerProxy::StartLicenseRequest(System.String)
extern "C"  void AN_LicenseManagerProxy_StartLicenseRequest_m3595847479 (Il2CppObject * __this /* static, unused */, String_t* ___base64PublicKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
