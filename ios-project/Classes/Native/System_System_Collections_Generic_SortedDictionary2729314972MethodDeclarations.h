﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>
struct SortedDictionary_2_t2729314972;
// System.Collections.Generic.IComparer`1<System.Int32>
struct IComparer_1_t26340570;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3023952753;
// System.Object
struct Il2CppObject;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEnumerator_1_t607539428;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t14295340;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_Collections_Generic_SortedDictionary4097876472.h"

// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::.ctor()
extern "C"  void SortedDictionary_2__ctor_m2249479999_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2__ctor_m2249479999(__this, method) ((  void (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2__ctor_m2249479999_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
extern "C"  void SortedDictionary_2__ctor_m3797745209_gshared (SortedDictionary_2_t2729314972 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define SortedDictionary_2__ctor_m3797745209(__this, ___comparer0, method) ((  void (*) (SortedDictionary_2_t2729314972 *, Il2CppObject*, const MethodInfo*))SortedDictionary_2__ctor_m3797745209_gshared)(__this, ___comparer0, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4192330182_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4192330182(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4192330182_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1790959638_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1790959638(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1790959638_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3587439085_gshared (SortedDictionary_2_t2729314972 * __this, KeyValuePair_2_t3132015601  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3587439085(__this, ___item0, method) ((  void (*) (SortedDictionary_2_t2729314972 *, KeyValuePair_2_t3132015601 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3587439085_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2899855627_gshared (SortedDictionary_2_t2729314972 * __this, KeyValuePair_2_t3132015601  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2899855627(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t2729314972 *, KeyValuePair_2_t3132015601 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2899855627_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2776079166_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2776079166(__this, method) ((  bool (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2776079166_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1189261532_gshared (SortedDictionary_2_t2729314972 * __this, KeyValuePair_2_t3132015601  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1189261532(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t2729314972 *, KeyValuePair_2_t3132015601 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1189261532_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_Add_m1927463518_gshared (SortedDictionary_2_t2729314972 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Add_m1927463518(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t2729314972 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Add_m1927463518_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_Contains_m3264183090_gshared (SortedDictionary_2_t2729314972 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Contains_m3264183090(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t2729314972 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Contains_m3264183090_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m2065988401_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m2065988401(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m2065988401_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3376003629_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3376003629(__this, method) ((  bool (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3376003629_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m88279344_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m88279344(__this, method) ((  bool (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m88279344_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Keys_m645489870_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Keys_m645489870(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Keys_m645489870_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_Remove_m4221730733_gshared (SortedDictionary_2_t2729314972 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Remove_m4221730733(__this, ___key0, method) ((  void (*) (SortedDictionary_2_t2729314972 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Remove_m4221730733_gshared)(__this, ___key0, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Values_m24953444_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Values_m24953444(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Values_m24953444_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Item_m484297376_gshared (SortedDictionary_2_t2729314972 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Item_m484297376(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t2729314972 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Item_m484297376_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_set_Item_m2616710169_gshared (SortedDictionary_2_t2729314972 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_set_Item_m2616710169(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t2729314972 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_set_Item_m2616710169_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void SortedDictionary_2_System_Collections_ICollection_CopyTo_m3250936852_gshared (SortedDictionary_2_t2729314972 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_CopyTo_m3250936852(__this, ___array0, ___index1, method) ((  void (*) (SortedDictionary_2_t2729314972 *, Il2CppArray *, int32_t, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_CopyTo_m3250936852_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m309837336_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m309837336(__this, method) ((  bool (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m309837336_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m1462669236_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m1462669236(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m1462669236_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1798345371_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1798345371(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1798345371_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4161878230_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4161878230(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4161878230_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::get_Count()
extern "C"  int32_t SortedDictionary_2_get_Count_m2469646656_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_get_Count_m2469646656(__this, method) ((  int32_t (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_get_Count_m2469646656_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::get_Item(TKey)
extern "C"  int32_t SortedDictionary_2_get_Item_m440566121_gshared (SortedDictionary_2_t2729314972 * __this, int32_t ___key0, const MethodInfo* method);
#define SortedDictionary_2_get_Item_m440566121(__this, ___key0, method) ((  int32_t (*) (SortedDictionary_2_t2729314972 *, int32_t, const MethodInfo*))SortedDictionary_2_get_Item_m440566121_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::set_Item(TKey,TValue)
extern "C"  void SortedDictionary_2_set_Item_m3144817932_gshared (SortedDictionary_2_t2729314972 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define SortedDictionary_2_set_Item_m3144817932(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t2729314972 *, int32_t, int32_t, const MethodInfo*))SortedDictionary_2_set_Item_m3144817932_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::Add(TKey,TValue)
extern "C"  void SortedDictionary_2_Add_m3714918295_gshared (SortedDictionary_2_t2729314972 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define SortedDictionary_2_Add_m3714918295(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t2729314972 *, int32_t, int32_t, const MethodInfo*))SortedDictionary_2_Add_m3714918295_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::Clear()
extern "C"  void SortedDictionary_2_Clear_m618002715_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_Clear_m618002715(__this, method) ((  void (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_Clear_m618002715_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::ContainsKey(TKey)
extern "C"  bool SortedDictionary_2_ContainsKey_m645900996_gshared (SortedDictionary_2_t2729314972 * __this, int32_t ___key0, const MethodInfo* method);
#define SortedDictionary_2_ContainsKey_m645900996(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t2729314972 *, int32_t, const MethodInfo*))SortedDictionary_2_ContainsKey_m645900996_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::ContainsValue(TValue)
extern "C"  bool SortedDictionary_2_ContainsValue_m2387963161_gshared (SortedDictionary_2_t2729314972 * __this, int32_t ___value0, const MethodInfo* method);
#define SortedDictionary_2_ContainsValue_m2387963161(__this, ___value0, method) ((  bool (*) (SortedDictionary_2_t2729314972 *, int32_t, const MethodInfo*))SortedDictionary_2_ContainsValue_m2387963161_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SortedDictionary_2_CopyTo_m3999230570_gshared (SortedDictionary_2_t2729314972 * __this, KeyValuePair_2U5BU5D_t14295340* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define SortedDictionary_2_CopyTo_m3999230570(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedDictionary_2_t2729314972 *, KeyValuePair_2U5BU5D_t14295340*, int32_t, const MethodInfo*))SortedDictionary_2_CopyTo_m3999230570_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.Generic.SortedDictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t4097876472  SortedDictionary_2_GetEnumerator_m1065794710_gshared (SortedDictionary_2_t2729314972 * __this, const MethodInfo* method);
#define SortedDictionary_2_GetEnumerator_m1065794710(__this, method) ((  Enumerator_t4097876472  (*) (SortedDictionary_2_t2729314972 *, const MethodInfo*))SortedDictionary_2_GetEnumerator_m1065794710_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::Remove(TKey)
extern "C"  bool SortedDictionary_2_Remove_m2704128987_gshared (SortedDictionary_2_t2729314972 * __this, int32_t ___key0, const MethodInfo* method);
#define SortedDictionary_2_Remove_m2704128987(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t2729314972 *, int32_t, const MethodInfo*))SortedDictionary_2_Remove_m2704128987_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::TryGetValue(TKey,TValue&)
extern "C"  bool SortedDictionary_2_TryGetValue_m968918282_gshared (SortedDictionary_2_t2729314972 * __this, int32_t ___key0, int32_t* ___value1, const MethodInfo* method);
#define SortedDictionary_2_TryGetValue_m968918282(__this, ___key0, ___value1, method) ((  bool (*) (SortedDictionary_2_t2729314972 *, int32_t, int32_t*, const MethodInfo*))SortedDictionary_2_TryGetValue_m968918282_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::ToKey(System.Object)
extern "C"  int32_t SortedDictionary_2_ToKey_m1817743592_gshared (SortedDictionary_2_t2729314972 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_ToKey_m1817743592(__this, ___key0, method) ((  int32_t (*) (SortedDictionary_2_t2729314972 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToKey_m1817743592_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>::ToValue(System.Object)
extern "C"  int32_t SortedDictionary_2_ToValue_m69841896_gshared (SortedDictionary_2_t2729314972 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SortedDictionary_2_ToValue_m69841896(__this, ___value0, method) ((  int32_t (*) (SortedDictionary_2_t2729314972 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToValue_m69841896_gshared)(__this, ___value0, method)
