﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "AssemblyU2DCSharp_MCGBehaviour3307770884.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jump
struct  Jump_t114869516  : public MCGBehaviour_t3307770884
{
public:
	// UnityEngine.GameObject Jump::img_arrow
	GameObject_t1756533147 * ___img_arrow_4;
	// UnityEngine.GameObject Jump::img_x
	GameObject_t1756533147 * ___img_x_5;
	// System.Single Jump::rotation
	float ___rotation_6;
	// System.Int32 Jump::_jumpNumber
	int32_t ____jumpNumber_7;
	// System.Int32 Jump::_jumpCombination
	int32_t ____jumpCombination_8;
	// UnityEngine.GameObject Jump::Collider1
	GameObject_t1756533147 * ___Collider1_9;
	// UnityEngine.GameObject Jump::Collider2
	GameObject_t1756533147 * ___Collider2_10;
	// UnityEngine.GameObject Jump::Circle
	GameObject_t1756533147 * ___Circle_11;
	// UnityEngine.GameObject Jump::Select
	GameObject_t1756533147 * ___Select_12;
	// UnityEngine.UI.Text Jump::number
	Text_t356221433 * ___number_13;
	// UnityEngine.GameObject Jump::jumpNumberText
	GameObject_t1756533147 * ___jumpNumberText_14;
	// UnityEngine.GameObject Jump::underline
	GameObject_t1756533147 * ___underline_15;
	// UnityEngine.GameObject Jump::RotataableItems
	GameObject_t1756533147 * ___RotataableItems_16;
	// UnityEngine.Sprite Jump::Jump_1
	Sprite_t309593783 * ___Jump_1_17;
	// UnityEngine.Sprite Jump::Jump_2
	Sprite_t309593783 * ___Jump_2_18;
	// UnityEngine.Sprite Jump::Jump_3
	Sprite_t309593783 * ___Jump_3_19;
	// UnityEngine.Sprite Jump::Jump_6
	Sprite_t309593783 * ___Jump_6_20;
	// UnityEngine.Sprite Jump::Jump_7
	Sprite_t309593783 * ___Jump_7_21;
	// UnityEngine.Sprite Jump::Jump_8
	Sprite_t309593783 * ___Jump_8_22;
	// UnityEngine.UI.Image Jump::JumpTypeImage
	Image_t2042527209 * ___JumpTypeImage_23;
	// UnityEngine.GameObject Jump::numberPanel
	GameObject_t1756533147 * ___numberPanel_24;
	// System.Int32 Jump::<jumpOrder>k__BackingField
	int32_t ___U3CjumpOrderU3Ek__BackingField_25;
	// System.Int32 Jump::<jumpType>k__BackingField
	int32_t ___U3CjumpTypeU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of_img_arrow_4() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___img_arrow_4)); }
	inline GameObject_t1756533147 * get_img_arrow_4() const { return ___img_arrow_4; }
	inline GameObject_t1756533147 ** get_address_of_img_arrow_4() { return &___img_arrow_4; }
	inline void set_img_arrow_4(GameObject_t1756533147 * value)
	{
		___img_arrow_4 = value;
		Il2CppCodeGenWriteBarrier(&___img_arrow_4, value);
	}

	inline static int32_t get_offset_of_img_x_5() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___img_x_5)); }
	inline GameObject_t1756533147 * get_img_x_5() const { return ___img_x_5; }
	inline GameObject_t1756533147 ** get_address_of_img_x_5() { return &___img_x_5; }
	inline void set_img_x_5(GameObject_t1756533147 * value)
	{
		___img_x_5 = value;
		Il2CppCodeGenWriteBarrier(&___img_x_5, value);
	}

	inline static int32_t get_offset_of_rotation_6() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___rotation_6)); }
	inline float get_rotation_6() const { return ___rotation_6; }
	inline float* get_address_of_rotation_6() { return &___rotation_6; }
	inline void set_rotation_6(float value)
	{
		___rotation_6 = value;
	}

	inline static int32_t get_offset_of__jumpNumber_7() { return static_cast<int32_t>(offsetof(Jump_t114869516, ____jumpNumber_7)); }
	inline int32_t get__jumpNumber_7() const { return ____jumpNumber_7; }
	inline int32_t* get_address_of__jumpNumber_7() { return &____jumpNumber_7; }
	inline void set__jumpNumber_7(int32_t value)
	{
		____jumpNumber_7 = value;
	}

	inline static int32_t get_offset_of__jumpCombination_8() { return static_cast<int32_t>(offsetof(Jump_t114869516, ____jumpCombination_8)); }
	inline int32_t get__jumpCombination_8() const { return ____jumpCombination_8; }
	inline int32_t* get_address_of__jumpCombination_8() { return &____jumpCombination_8; }
	inline void set__jumpCombination_8(int32_t value)
	{
		____jumpCombination_8 = value;
	}

	inline static int32_t get_offset_of_Collider1_9() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___Collider1_9)); }
	inline GameObject_t1756533147 * get_Collider1_9() const { return ___Collider1_9; }
	inline GameObject_t1756533147 ** get_address_of_Collider1_9() { return &___Collider1_9; }
	inline void set_Collider1_9(GameObject_t1756533147 * value)
	{
		___Collider1_9 = value;
		Il2CppCodeGenWriteBarrier(&___Collider1_9, value);
	}

	inline static int32_t get_offset_of_Collider2_10() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___Collider2_10)); }
	inline GameObject_t1756533147 * get_Collider2_10() const { return ___Collider2_10; }
	inline GameObject_t1756533147 ** get_address_of_Collider2_10() { return &___Collider2_10; }
	inline void set_Collider2_10(GameObject_t1756533147 * value)
	{
		___Collider2_10 = value;
		Il2CppCodeGenWriteBarrier(&___Collider2_10, value);
	}

	inline static int32_t get_offset_of_Circle_11() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___Circle_11)); }
	inline GameObject_t1756533147 * get_Circle_11() const { return ___Circle_11; }
	inline GameObject_t1756533147 ** get_address_of_Circle_11() { return &___Circle_11; }
	inline void set_Circle_11(GameObject_t1756533147 * value)
	{
		___Circle_11 = value;
		Il2CppCodeGenWriteBarrier(&___Circle_11, value);
	}

	inline static int32_t get_offset_of_Select_12() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___Select_12)); }
	inline GameObject_t1756533147 * get_Select_12() const { return ___Select_12; }
	inline GameObject_t1756533147 ** get_address_of_Select_12() { return &___Select_12; }
	inline void set_Select_12(GameObject_t1756533147 * value)
	{
		___Select_12 = value;
		Il2CppCodeGenWriteBarrier(&___Select_12, value);
	}

	inline static int32_t get_offset_of_number_13() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___number_13)); }
	inline Text_t356221433 * get_number_13() const { return ___number_13; }
	inline Text_t356221433 ** get_address_of_number_13() { return &___number_13; }
	inline void set_number_13(Text_t356221433 * value)
	{
		___number_13 = value;
		Il2CppCodeGenWriteBarrier(&___number_13, value);
	}

	inline static int32_t get_offset_of_jumpNumberText_14() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___jumpNumberText_14)); }
	inline GameObject_t1756533147 * get_jumpNumberText_14() const { return ___jumpNumberText_14; }
	inline GameObject_t1756533147 ** get_address_of_jumpNumberText_14() { return &___jumpNumberText_14; }
	inline void set_jumpNumberText_14(GameObject_t1756533147 * value)
	{
		___jumpNumberText_14 = value;
		Il2CppCodeGenWriteBarrier(&___jumpNumberText_14, value);
	}

	inline static int32_t get_offset_of_underline_15() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___underline_15)); }
	inline GameObject_t1756533147 * get_underline_15() const { return ___underline_15; }
	inline GameObject_t1756533147 ** get_address_of_underline_15() { return &___underline_15; }
	inline void set_underline_15(GameObject_t1756533147 * value)
	{
		___underline_15 = value;
		Il2CppCodeGenWriteBarrier(&___underline_15, value);
	}

	inline static int32_t get_offset_of_RotataableItems_16() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___RotataableItems_16)); }
	inline GameObject_t1756533147 * get_RotataableItems_16() const { return ___RotataableItems_16; }
	inline GameObject_t1756533147 ** get_address_of_RotataableItems_16() { return &___RotataableItems_16; }
	inline void set_RotataableItems_16(GameObject_t1756533147 * value)
	{
		___RotataableItems_16 = value;
		Il2CppCodeGenWriteBarrier(&___RotataableItems_16, value);
	}

	inline static int32_t get_offset_of_Jump_1_17() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___Jump_1_17)); }
	inline Sprite_t309593783 * get_Jump_1_17() const { return ___Jump_1_17; }
	inline Sprite_t309593783 ** get_address_of_Jump_1_17() { return &___Jump_1_17; }
	inline void set_Jump_1_17(Sprite_t309593783 * value)
	{
		___Jump_1_17 = value;
		Il2CppCodeGenWriteBarrier(&___Jump_1_17, value);
	}

	inline static int32_t get_offset_of_Jump_2_18() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___Jump_2_18)); }
	inline Sprite_t309593783 * get_Jump_2_18() const { return ___Jump_2_18; }
	inline Sprite_t309593783 ** get_address_of_Jump_2_18() { return &___Jump_2_18; }
	inline void set_Jump_2_18(Sprite_t309593783 * value)
	{
		___Jump_2_18 = value;
		Il2CppCodeGenWriteBarrier(&___Jump_2_18, value);
	}

	inline static int32_t get_offset_of_Jump_3_19() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___Jump_3_19)); }
	inline Sprite_t309593783 * get_Jump_3_19() const { return ___Jump_3_19; }
	inline Sprite_t309593783 ** get_address_of_Jump_3_19() { return &___Jump_3_19; }
	inline void set_Jump_3_19(Sprite_t309593783 * value)
	{
		___Jump_3_19 = value;
		Il2CppCodeGenWriteBarrier(&___Jump_3_19, value);
	}

	inline static int32_t get_offset_of_Jump_6_20() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___Jump_6_20)); }
	inline Sprite_t309593783 * get_Jump_6_20() const { return ___Jump_6_20; }
	inline Sprite_t309593783 ** get_address_of_Jump_6_20() { return &___Jump_6_20; }
	inline void set_Jump_6_20(Sprite_t309593783 * value)
	{
		___Jump_6_20 = value;
		Il2CppCodeGenWriteBarrier(&___Jump_6_20, value);
	}

	inline static int32_t get_offset_of_Jump_7_21() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___Jump_7_21)); }
	inline Sprite_t309593783 * get_Jump_7_21() const { return ___Jump_7_21; }
	inline Sprite_t309593783 ** get_address_of_Jump_7_21() { return &___Jump_7_21; }
	inline void set_Jump_7_21(Sprite_t309593783 * value)
	{
		___Jump_7_21 = value;
		Il2CppCodeGenWriteBarrier(&___Jump_7_21, value);
	}

	inline static int32_t get_offset_of_Jump_8_22() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___Jump_8_22)); }
	inline Sprite_t309593783 * get_Jump_8_22() const { return ___Jump_8_22; }
	inline Sprite_t309593783 ** get_address_of_Jump_8_22() { return &___Jump_8_22; }
	inline void set_Jump_8_22(Sprite_t309593783 * value)
	{
		___Jump_8_22 = value;
		Il2CppCodeGenWriteBarrier(&___Jump_8_22, value);
	}

	inline static int32_t get_offset_of_JumpTypeImage_23() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___JumpTypeImage_23)); }
	inline Image_t2042527209 * get_JumpTypeImage_23() const { return ___JumpTypeImage_23; }
	inline Image_t2042527209 ** get_address_of_JumpTypeImage_23() { return &___JumpTypeImage_23; }
	inline void set_JumpTypeImage_23(Image_t2042527209 * value)
	{
		___JumpTypeImage_23 = value;
		Il2CppCodeGenWriteBarrier(&___JumpTypeImage_23, value);
	}

	inline static int32_t get_offset_of_numberPanel_24() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___numberPanel_24)); }
	inline GameObject_t1756533147 * get_numberPanel_24() const { return ___numberPanel_24; }
	inline GameObject_t1756533147 ** get_address_of_numberPanel_24() { return &___numberPanel_24; }
	inline void set_numberPanel_24(GameObject_t1756533147 * value)
	{
		___numberPanel_24 = value;
		Il2CppCodeGenWriteBarrier(&___numberPanel_24, value);
	}

	inline static int32_t get_offset_of_U3CjumpOrderU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___U3CjumpOrderU3Ek__BackingField_25)); }
	inline int32_t get_U3CjumpOrderU3Ek__BackingField_25() const { return ___U3CjumpOrderU3Ek__BackingField_25; }
	inline int32_t* get_address_of_U3CjumpOrderU3Ek__BackingField_25() { return &___U3CjumpOrderU3Ek__BackingField_25; }
	inline void set_U3CjumpOrderU3Ek__BackingField_25(int32_t value)
	{
		___U3CjumpOrderU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CjumpTypeU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(Jump_t114869516, ___U3CjumpTypeU3Ek__BackingField_26)); }
	inline int32_t get_U3CjumpTypeU3Ek__BackingField_26() const { return ___U3CjumpTypeU3Ek__BackingField_26; }
	inline int32_t* get_address_of_U3CjumpTypeU3Ek__BackingField_26() { return &___U3CjumpTypeU3Ek__BackingField_26; }
	inline void set_U3CjumpTypeU3Ek__BackingField_26(int32_t value)
	{
		___U3CjumpTypeU3Ek__BackingField_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
