﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayEvents
struct GooglePlayEvents_t1613959644;
// System.String
struct String_t;
// System.Collections.Generic.List`1<GP_Event>
struct List_1_t3987232096;
// GooglePlayResult
struct GooglePlayResult_t3097469636;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GooglePlayResult3097469636.h"

// System.Void GooglePlayEvents::.ctor()
extern "C"  void GooglePlayEvents__ctor_m2504973633 (GooglePlayEvents_t1613959644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayEvents::Awake()
extern "C"  void GooglePlayEvents_Awake_m1878096588 (GooglePlayEvents_t1613959644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayEvents::LoadEvents()
extern "C"  void GooglePlayEvents_LoadEvents_m1953736794 (GooglePlayEvents_t1613959644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayEvents::SumbitEvent(System.String)
extern "C"  void GooglePlayEvents_SumbitEvent_m4290350659 (GooglePlayEvents_t1613959644 * __this, String_t* ___eventId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayEvents::SumbitEvent(System.String,System.Int32)
extern "C"  void GooglePlayEvents_SumbitEvent_m4046026464 (GooglePlayEvents_t1613959644 * __this, String_t* ___eventId0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GP_Event> GooglePlayEvents::get_Events()
extern "C"  List_1_t3987232096 * GooglePlayEvents_get_Events_m2983454130 (GooglePlayEvents_t1613959644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayEvents::OnGPEventsLoaded(System.String)
extern "C"  void GooglePlayEvents_OnGPEventsLoaded_m1494797509 (GooglePlayEvents_t1613959644 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayEvents::<OnEventsLoaded>m__3B(GooglePlayResult)
extern "C"  void GooglePlayEvents_U3COnEventsLoadedU3Em__3B_m544517160 (Il2CppObject * __this /* static, unused */, GooglePlayResult_t3097469636 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
