﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameDataExample
struct GameDataExample_t175804752;

#include "codegen/il2cpp-codegen.h"

// System.Void GameDataExample::.ctor()
extern "C"  void GameDataExample__ctor_m748053133 (GameDataExample_t175804752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameDataExample::get_coins()
extern "C"  int32_t GameDataExample_get_coins_m3126471336 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameDataExample::AddCoins(System.Int32)
extern "C"  void GameDataExample_AddCoins_m3965600725 (Il2CppObject * __this /* static, unused */, int32_t ___amount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameDataExample::EnableCoinsBoost()
extern "C"  void GameDataExample_EnableCoinsBoost_m2828907021 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameDataExample::get_IsBoostPurchased()
extern "C"  bool GameDataExample_get_IsBoostPurchased_m2810165032 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
