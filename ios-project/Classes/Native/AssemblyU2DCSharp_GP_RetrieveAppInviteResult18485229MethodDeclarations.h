﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_RetrieveAppInviteResult
struct GP_RetrieveAppInviteResult_t18485229;
// System.String
struct String_t;
// GP_AppInvite
struct GP_AppInvite_t1121244750;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_AppInvite1121244750.h"

// System.Void GP_RetrieveAppInviteResult::.ctor(System.String)
extern "C"  void GP_RetrieveAppInviteResult__ctor_m4190328980 (GP_RetrieveAppInviteResult_t18485229 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_RetrieveAppInviteResult::.ctor(GP_AppInvite)
extern "C"  void GP_RetrieveAppInviteResult__ctor_m2184964352 (GP_RetrieveAppInviteResult_t18485229 * __this, GP_AppInvite_t1121244750 * ___invite0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_AppInvite GP_RetrieveAppInviteResult::get_AppInvite()
extern "C"  GP_AppInvite_t1121244750 * GP_RetrieveAppInviteResult_get_AppInvite_m3905777912 (GP_RetrieveAppInviteResult_t18485229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
