﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3462960326(__this, ___dictionary0, method) ((  void (*) (Enumerator_t134503849 *, Dictionary_2_t3109446443 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m202758215(__this, method) ((  Il2CppObject * (*) (Enumerator_t134503849 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3358470307(__this, method) ((  void (*) (Enumerator_t134503849 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3497304240(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t134503849 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3930657489(__this, method) ((  Il2CppObject * (*) (Enumerator_t134503849 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1420704513(__this, method) ((  Il2CppObject * (*) (Enumerator_t134503849 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::MoveNext()
#define Enumerator_MoveNext_m959412039(__this, method) ((  bool (*) (Enumerator_t134503849 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::get_Current()
#define Enumerator_get_Current_m2330326799(__this, method) ((  KeyValuePair_2_t866791665  (*) (Enumerator_t134503849 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2098530790(__this, method) ((  int32_t (*) (Enumerator_t134503849 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3867472742(__this, method) ((  GK_FriendRequest_t4101620808 * (*) (Enumerator_t134503849 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::Reset()
#define Enumerator_Reset_m2550407948(__this, method) ((  void (*) (Enumerator_t134503849 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::VerifyState()
#define Enumerator_VerifyState_m352610989(__this, method) ((  void (*) (Enumerator_t134503849 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1265252523(__this, method) ((  void (*) (Enumerator_t134503849 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GK_FriendRequest>::Dispose()
#define Enumerator_Dispose_m4046940802(__this, method) ((  void (*) (Enumerator_t134503849 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
