﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3961629604MethodDeclarations.h"

// System.Void System.Func`2<Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2621256441(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2386093395 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1354888807_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m1033370219(__this, ___arg10, method) ((  bool (*) (Func_2_t2386093395 *, TypeSchema_t460462092 *, const MethodInfo*))Func_2_Invoke_m2968608789_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2394306954(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2386093395 *, TypeSchema_t460462092 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1429757044_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2650471533(__this, ___result0, method) ((  bool (*) (Func_2_t2386093395 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m924416567_gshared)(__this, ___result0, method)
