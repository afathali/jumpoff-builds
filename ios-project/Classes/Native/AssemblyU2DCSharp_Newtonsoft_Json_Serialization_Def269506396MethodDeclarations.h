﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultContractResolver/<SetIsSpecifiedActions>c__AnonStorey23
struct U3CSetIsSpecifiedActionsU3Ec__AnonStorey23_t269506396;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<SetIsSpecifiedActions>c__AnonStorey23::.ctor()
extern "C"  void U3CSetIsSpecifiedActionsU3Ec__AnonStorey23__ctor_m2977550467 (U3CSetIsSpecifiedActionsU3Ec__AnonStorey23_t269506396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<SetIsSpecifiedActions>c__AnonStorey23::<>m__DF(System.Object)
extern "C"  bool U3CSetIsSpecifiedActionsU3Ec__AnonStorey23_U3CU3Em__DF_m2248724228 (U3CSetIsSpecifiedActionsU3Ec__AnonStorey23_t269506396 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
