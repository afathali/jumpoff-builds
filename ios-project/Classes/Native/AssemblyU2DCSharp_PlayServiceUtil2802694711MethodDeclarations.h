﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GPAchievementState819112501.h"
#include "AssemblyU2DCSharp_GPAchievementType785407574.h"

// GPAchievementState PlayServiceUtil::GetAchievementStateById(System.Int32)
extern "C"  int32_t PlayServiceUtil_GetAchievementStateById_m3779318971 (Il2CppObject * __this /* static, unused */, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPAchievementType PlayServiceUtil::GetAchievementTypeById(System.Int32)
extern "C"  int32_t PlayServiceUtil_GetAchievementTypeById_m886382479 (Il2CppObject * __this /* static, unused */, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
