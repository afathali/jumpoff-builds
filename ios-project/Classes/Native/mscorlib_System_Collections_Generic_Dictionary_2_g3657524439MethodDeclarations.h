﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1334805305MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor()
#define Dictionary_2__ctor_m689849583(__this, method) ((  void (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2__ctor_m2836016318_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1001370547(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3657524439 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1886025631_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m705407810(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3657524439 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m316258200_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Int32)
#define Dictionary_2__ctor_m3546963749(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3657524439 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m553319225_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1370286741(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3657524439 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1679806537_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1191086040(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t3657524439 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2632888642_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m288651603(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3657524439 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m3470823591_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m582511278(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2784388552_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3412288422(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2859450176_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1574462378(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m3611003760_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m4226765516(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1160993878_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1341506271(__this, method) ((  bool (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3256011059_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1479892024(__this, method) ((  bool (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3135632802_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2203099840(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3657524439 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m470283286_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1905176827(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3657524439 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m952915023_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m4094186826(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3657524439 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m4025116688_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m2204696170(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3657524439 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m4243953040_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m2950795383(__this, ___key0, method) ((  void (*) (Dictionary_2_t3657524439 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2364651291_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3817896496(__this, method) ((  bool (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m407061894_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2499058848(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3190773558_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1422141570(__this, method) ((  bool (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m372383048_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3284954059(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3657524439 *, KeyValuePair_2_t1414869661 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2516271191_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3323841529(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3657524439 *, KeyValuePair_2_t1414869661 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m120964101_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4213434799(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3657524439 *, KeyValuePair_2U5BU5D_t2822481488*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3992195595_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3250085836(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3657524439 *, KeyValuePair_2_t1414869661 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3493717890_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m553562560(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3657524439 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m211818838_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2579799949(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2575233185_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2392353786(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m998185808_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4185047759(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3782673907_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::get_Count()
#define Dictionary_2_get_Count_m1081547024(__this, method) ((  int32_t (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_get_Count_m2651810726_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::get_Item(TKey)
#define Dictionary_2_get_Item_m1141480919(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3657524439 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m3264376043_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m224215386(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3657524439 *, String_t*, int32_t, const MethodInfo*))Dictionary_2_set_Item_m2024412100_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m3823728588(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3657524439 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1989482002_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m4272523277(__this, ___size0, method) ((  void (*) (Dictionary_2_t3657524439 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2404839201_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m3598106079(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3657524439 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2383218387_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m4263898861(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1414869661  (*) (Il2CppObject * /* static, unused */, String_t*, int32_t, const MethodInfo*))Dictionary_2_make_pair_m2824014537_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m803551373(__this /* static, unused */, ___key0, ___value1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, int32_t, const MethodInfo*))Dictionary_2_pick_key_m1703565137_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m2219042845(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, String_t*, int32_t, const MethodInfo*))Dictionary_2_pick_value_m1820997985_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m2934015550(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3657524439 *, KeyValuePair_2U5BU5D_t2822481488*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3539403768_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::Resize()
#define Dictionary_2_Resize_m1822713122(__this, method) ((  void (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_Resize_m3513975816_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::Add(TKey,TValue)
#define Dictionary_2_Add_m3251871495(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3657524439 *, String_t*, int32_t, const MethodInfo*))Dictionary_2_Add_m1354283437_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::Clear()
#define Dictionary_2_Clear_m1646357661(__this, method) ((  void (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_Clear_m443406673_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m3157360207(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3657524439 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m2393545939_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m3439110879(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3657524439 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m3846688547_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m874130452(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3657524439 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m3862838986_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m3602463706(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3657524439 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3822893684_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::Remove(TKey)
#define Dictionary_2_Remove_m667541473(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3657524439 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m3154834493_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m3025530414(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3657524439 *, String_t*, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m568772648_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::get_Keys()
#define Dictionary_2_get_Keys_m229668427(__this, method) ((  KeyCollection_t1846054914 * (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_get_Keys_m3168816223_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::get_Values()
#define Dictionary_2_get_Values_m4173209795(__this, method) ((  ValueCollection_t2360584282 * (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_get_Values_m2184765879_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m3179044072(__this, ___key0, method) ((  String_t* (*) (Dictionary_2_t3657524439 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2107972530_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m3935287168(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t3657524439 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1531438698_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m1011626806(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3657524439 *, KeyValuePair_2_t1414869661 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3962305184_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m1499121(__this, method) ((  Enumerator_t682581845  (*) (Dictionary_2_t3657524439 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3021808117_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m1857200424(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, String_t*, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3775762914_gshared)(__this /* static, unused */, ___key0, ___value1, method)
