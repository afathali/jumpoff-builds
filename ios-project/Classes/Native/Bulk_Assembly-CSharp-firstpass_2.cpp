﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// SA.IOSDeploy.Variable
struct Variable_t1157765046;
// SA.IOSDeploy.VariableListed
struct VariableListed_t1912381035;
// System.Collections.Generic.Dictionary`2<System.String,SA.IOSDeploy.VariableListed>
struct Dictionary_2_t3827160297;
// SA.IOSNative.Contacts.Contact
struct Contact_t4178394798;
// SA.IOSNative.Contacts.ContactsResult
struct ContactsResult_t1045731284;
// System.Collections.Generic.List`1<SA.IOSNative.Contacts.Contact>
struct List_1_t3547515930;
// SA.Common.Models.Error
struct Error_t445207774;
// SA.IOSNative.Contacts.ContactStore
struct ContactStore_t160827595;
// System.Action`1<SA.IOSNative.Contacts.ContactsResult>
struct Action_1_t847530666;
// System.String
struct String_t;
// SA.IOSNative.Contacts.PhoneNumber
struct PhoneNumber_t113354641;
// SA_Ad_EditorUIController
struct SA_Ad_EditorUIController_t4080214878;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action
struct Action_t3226471752;
// UnityEngine.Canvas
struct Canvas_t209405766;
// System.Object
struct Il2CppObject;
// SA_EditorAd
struct SA_EditorAd_t1410159287;
// SA_EditorInApps
struct SA_EditorInApps_t4145855819;
// SA_InApps_EditorUIController
struct SA_InApps_EditorUIController_t628772056;
// SA_EditorNotifications
struct SA_EditorNotifications_t2173147004;
// SA_Notifications_EditorUIController
struct SA_Notifications_EditorUIController_t749267495;
// SA_EditorTestingSceneController
struct SA_EditorTestingSceneController_t2020162592;
// SA_UIHightDependence
struct SA_UIHightDependence_t2303649696;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// SA_UIWidthDependence
struct SA_UIWidthDependence_t1527301732;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// SA.Common.Animation.ValuesTween
struct ValuesTween_t4181447589;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// SK_AuthorizationResult
struct SK_AuthorizationResult_t2047753871;
// SK_CloudService
struct SK_CloudService_t588287925;
// System.Action`1<SK_AuthorizationResult>
struct Action_1_t1849553253;
// System.Action`1<SK_RequestCapabilitieResult>
struct Action_1_t2311963226;
// System.Action`1<SK_RequestStorefrontIdentifierResult>
struct Action_1_t914276544;
// SK_RequestCapabilitieResult
struct SK_RequestCapabilitieResult_t2510163844;
// SK_RequestStorefrontIdentifierResult
struct SK_RequestStorefrontIdentifierResult_t1112477162;
// TBM_Multiplayer_Example
struct TBM_Multiplayer_Example_t1159631235;
// GK_Player
struct GK_Player_t2782008294;
// GK_Invite
struct GK_Invite_t22070530;
// System.String[]
struct StringU5BU5D_t1642385972;
// GK_Player[]
struct GK_PlayerU5BU5D_t1642762691;
// GK_RTM_MatchStartedResult
struct GK_RTM_MatchStartedResult_t833698690;
// GK_UserPhotoLoadResult
struct GK_UserPhotoLoadResult_t1614198031;
// SA.Common.Models.Result
struct Result_t4287219743;
// GK_TBM_LoadMatchesResult
struct GK_TBM_LoadMatchesResult_t370491735;
// GK_TBM_MatchDataUpdateResult
struct GK_TBM_MatchDataUpdateResult_t1356006034;
// GK_TBM_EndTrunResult
struct GK_TBM_EndTrunResult_t1517380690;
// GK_TBM_MatchEndResult
struct GK_TBM_MatchEndResult_t3461768810;
// GK_TBM_MatchRemovedResult
struct GK_TBM_MatchRemovedResult_t909126313;
// GK_TBM_Match
struct GK_TBM_Match_t132033130;
// GK_TBM_MatchInitResult
struct GK_TBM_MatchInitResult_t3847830897;
// TvOsCloudExample
struct TvOsCloudExample_t1625417505;
// iCloudData
struct iCloudData_t3080637488;
// TvOSInAppsExample
struct TvOSInAppsExample_t752347109;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_PlistVa3782075076.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_PlistVa3782075076MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_Variabl1157765046.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_Variabl1157765046MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1281502167MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1281502167.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_Variabl1912381035.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat816231841.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat816231841MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3827160297.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3827160297MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_Variabl1912381035MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contact4178394798.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contact4178394798MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3777443069MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3777443069.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contact1045731284.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contact1045731284MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3547515930.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3547515930MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contacts160827595.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contacts160827595MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen847530666MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Sin643392700MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen847530666.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Data_Conver243910518MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contacts113354641MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contacts113354641.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_StringSplitOptions2996162939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_Logger85856405MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Ad_EditorUIContro4080214878.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Ad_EditorUIContro4080214878MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "System_Core_System_Action3226471752MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorTesting2941573636MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas209405766MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorAd1410159287.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorAd1410159287MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si1892724392MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorInApps4145855819.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorInApps4145855819MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_InApps_EditorUICon628772056MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_InApps_EditorUICon628772056.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorNotificatio2173147004.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorNotificatio2173147004MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorNotificatio2256397131.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Notifications_Edit749267495MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Notifications_Edit749267495.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorNotificatio2256397131MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorTesting2941573636.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorTestingScen2020162592.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorTestingScen2020162592MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Animation_4181447589MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Animation_4181447589.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Animation_2123337604.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_UIHightDependence2303649696.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1878309314MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1878309314.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_UIHightDependence2303649696MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_UIWidthDependence1527301732.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_UIWidthDependence1527301732MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_UnityExtensions959107188.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_UnityExtensions959107188MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "UnityEngine_UnityEngine_Sprite309593783MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_AuthorizationResu2047753871.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_AuthorizationResu2047753871MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_CloudServiceAutho3247563438.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_CloudService588287925.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_CloudService588287925MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si1070853030MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1849553253MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2311963226MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen914276544MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1849553253.h"
#include "mscorlib_System_Action_1_gen2311963226.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_RequestCapabiliti2510163844.h"
#include "mscorlib_System_Action_1_gen914276544.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_RequestStorefront1112477162.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSNativeMarketBridg1553898649MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_RequestCapabiliti2510163844MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_CloudServiceCapabi714629801.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_RequestStorefront1112477162MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_CloudServiceAutho3247563438MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_CloudServiceCapabi714629801MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TBM_Multiplayer_Exam1159631235.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TBM_Multiplayer_Exam1159631235MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BaseIOSFeaturePrevie3055692840MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GameCenterManager1487113918MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen4089019125MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si1332195736MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GameCenter_RTM849630631MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1415997413MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Player2782008294MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen635498072MethodDeclarations.h"
#include "System_Core_System_Action_3_gen1065438871MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GameCenterInvitation2643374653MethodDeclarations.h"
#include "System_Core_System_Action_2_gen884171879MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1736305833MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Player2782008294.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "mscorlib_System_Action_1_gen4089019125.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GameCenter_RTM849630631.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_UserPhotoLoadResu1614198031.h"
#include "mscorlib_System_Action_1_gen1415997413.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_PhotoSize3124681388.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_RTM_MatchStartedRe833698690.h"
#include "mscorlib_System_Action_1_gen635498072.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_MatchType1493351924.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "System_Core_System_Action_3_gen1065438871.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Invite22070530.h"
#include "System_Core_System_Action_2_gen884171879.h"
#include "System_Core_System_Action_2_gen1736305833.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge401820260MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge401820260.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_RTM_MatchStartedRe833698690MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_RTM_Match873568990MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_RTM_Match873568990.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_UserPhotoLoadResu1614198031MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si3940119580MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GameCenter_TBM3457554475MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen172291117MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3649630279MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_Match132033130MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1157805416MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_Participant3803955090MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3173076222MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1319180072MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3263568192MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen710925695MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_Participant3803955090.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2707805896.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BaseIOSFeaturePrevie3055692840.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GameCenter_TBM3457554475.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_LoadMatchesRes370491735.h"
#include "mscorlib_System_Action_1_gen172291117.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchInitResu3847830897.h"
#include "mscorlib_System_Action_1_gen3649630279.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_Match132033130.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchDataUpda1356006034.h"
#include "mscorlib_System_Action_1_gen1157805416.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TurnBasedMatchOut2242380984.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3173076222.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2707805896MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_EndTrunResult1517380690.h"
#include "mscorlib_System_Action_1_gen1319180072.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchEndResul3461768810.h"
#include "mscorlib_System_Action_1_gen3263568192.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchRemovedRe909126313.h"
#include "mscorlib_System_Action_1_gen710925695.h"
#include "mscorlib_System_Boolean3825574718MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2046812392MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24099124910.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3366837094.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2046812392.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3366837094MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24099124910MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchDataUpda1356006034MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSMessage2569463336MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_EndTrunResult1517380690MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSMessage2569463336.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchEndResul3461768810MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchRemovedRe909126313MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3796121558MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3796121558.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchInitResu3847830897MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TvOsCloudExample1625417505.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TvOsCloudExample1625417505MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2882436870MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iCloudManager2506189173MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si2988754278MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iCloudData3080637488.h"
#include "mscorlib_System_Action_1_gen2882436870.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iCloudManager2506189173.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iCloudData3080637488MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TvOSInAppsExample752347109.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TvOSInAppsExample752347109MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PaymentManagerExampl3283960187MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m2721246802_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m2721246802(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Canvas>()
#define Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, method) ((  Canvas_t209405766 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t3349966182_m1310250299(__this, method) ((  RectTransform_t3349966182 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m4099813343_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m4099813343(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m4099813343_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<SA.Common.Animation.ValuesTween>()
#define GameObject_AddComponent_TisValuesTween_t4181447589_m1522737034(__this, method) ((  ValuesTween_t4181447589 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m4099813343_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SA.IOSDeploy.Variable::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1281502167_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m194429470_MethodInfo_var;
extern const uint32_t Variable__ctor_m1260310667_MetadataUsageId;
extern "C"  void Variable__ctor_m1260310667 (Variable_t1157765046 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Variable__ctor_m1260310667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_IsOpen_0((bool)1);
		__this->set_IsListOpen_1((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_StringValue_5(L_0);
		__this->set_BooleanValue_8((bool)1);
		List_1_t1281502167 * L_1 = (List_1_t1281502167 *)il2cpp_codegen_object_new(List_1_t1281502167_il2cpp_TypeInfo_var);
		List_1__ctor_m194429470(L_1, /*hidden argument*/List_1__ctor_m194429470_MethodInfo_var);
		__this->set_ArrayValue_9(L_1);
		List_1_t1281502167 * L_2 = (List_1_t1281502167 *)il2cpp_codegen_object_new(List_1_t1281502167_il2cpp_TypeInfo_var);
		List_1__ctor_m194429470(L_2, /*hidden argument*/List_1__ctor_m194429470_MethodInfo_var);
		__this->set_DictValues_10(L_2);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA.IOSDeploy.Variable::AddVarToDictionary(SA.IOSDeploy.VariableListed)
extern Il2CppClass* Enumerator_t816231841_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m207587607_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1435761395_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1249235375_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1684933602_MethodInfo_var;
extern const uint32_t Variable_AddVarToDictionary_m3256281913_MetadataUsageId;
extern "C"  void Variable_AddVarToDictionary_m3256281913 (Variable_t1157765046 * __this, VariableListed_t1912381035 * ___v0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Variable_AddVarToDictionary_m3256281913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	VariableListed_t1912381035 * V_1 = NULL;
	Enumerator_t816231841  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		List_1_t1281502167 * L_0 = __this->get_DictValues_10();
		NullCheck(L_0);
		Enumerator_t816231841  L_1 = List_1_GetEnumerator_m207587607(L_0, /*hidden argument*/List_1_GetEnumerator_m207587607_MethodInfo_var);
		V_2 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0038;
		}

IL_0013:
		{
			VariableListed_t1912381035 * L_2 = Enumerator_get_Current_m1435761395((&V_2), /*hidden argument*/Enumerator_get_Current_m1435761395_MethodInfo_var);
			V_1 = L_2;
			VariableListed_t1912381035 * L_3 = V_1;
			NullCheck(L_3);
			String_t* L_4 = L_3->get_DictKey_1();
			VariableListed_t1912381035 * L_5 = ___v0;
			NullCheck(L_5);
			String_t* L_6 = L_5->get_DictKey_1();
			NullCheck(L_4);
			bool L_7 = String_Equals_m2633592423(L_4, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0038;
			}
		}

IL_0031:
		{
			V_0 = (bool)0;
			goto IL_0044;
		}

IL_0038:
		{
			bool L_8 = Enumerator_MoveNext_m1249235375((&V_2), /*hidden argument*/Enumerator_MoveNext_m1249235375_MethodInfo_var);
			if (L_8)
			{
				goto IL_0013;
			}
		}

IL_0044:
		{
			IL2CPP_LEAVE(0x55, FINALLY_0049);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		Enumerator_t816231841  L_9 = V_2;
		Enumerator_t816231841  L_10 = L_9;
		Il2CppObject * L_11 = Box(Enumerator_t816231841_il2cpp_TypeInfo_var, &L_10);
		NullCheck((Il2CppObject *)L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
		IL2CPP_END_FINALLY(73)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0055:
	{
		bool L_12 = V_0;
		if (!L_12)
		{
			goto IL_0067;
		}
	}
	{
		List_1_t1281502167 * L_13 = __this->get_DictValues_10();
		VariableListed_t1912381035 * L_14 = ___v0;
		NullCheck(L_13);
		List_1_Add_m1684933602(L_13, L_14, /*hidden argument*/List_1_Add_m1684933602_MethodInfo_var);
	}

IL_0067:
	{
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,SA.IOSDeploy.VariableListed> SA.IOSDeploy.Variable::get_DictionaryValue()
extern Il2CppClass* Dictionary_2_t3827160297_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t816231841_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4204953261_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m207587607_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1435761395_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1433354965_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1249235375_MethodInfo_var;
extern const uint32_t Variable_get_DictionaryValue_m3058259748_MetadataUsageId;
extern "C"  Dictionary_2_t3827160297 * Variable_get_DictionaryValue_m3058259748 (Variable_t1157765046 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Variable_get_DictionaryValue_m3058259748_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t3827160297 * V_0 = NULL;
	VariableListed_t1912381035 * V_1 = NULL;
	Enumerator_t816231841  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3827160297 * L_0 = (Dictionary_2_t3827160297 *)il2cpp_codegen_object_new(Dictionary_2_t3827160297_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4204953261(L_0, /*hidden argument*/Dictionary_2__ctor_m4204953261_MethodInfo_var);
		V_0 = L_0;
		List_1_t1281502167 * L_1 = __this->get_DictValues_10();
		NullCheck(L_1);
		Enumerator_t816231841  L_2 = List_1_GetEnumerator_m207587607(L_1, /*hidden argument*/List_1_GetEnumerator_m207587607_MethodInfo_var);
		V_2 = L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002c;
		}

IL_0017:
		{
			VariableListed_t1912381035 * L_3 = Enumerator_get_Current_m1435761395((&V_2), /*hidden argument*/Enumerator_get_Current_m1435761395_MethodInfo_var);
			V_1 = L_3;
			Dictionary_2_t3827160297 * L_4 = V_0;
			VariableListed_t1912381035 * L_5 = V_1;
			NullCheck(L_5);
			String_t* L_6 = L_5->get_DictKey_1();
			VariableListed_t1912381035 * L_7 = V_1;
			NullCheck(L_4);
			Dictionary_2_Add_m1433354965(L_4, L_6, L_7, /*hidden argument*/Dictionary_2_Add_m1433354965_MethodInfo_var);
		}

IL_002c:
		{
			bool L_8 = Enumerator_MoveNext_m1249235375((&V_2), /*hidden argument*/Enumerator_MoveNext_m1249235375_MethodInfo_var);
			if (L_8)
			{
				goto IL_0017;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		Enumerator_t816231841  L_9 = V_2;
		Enumerator_t816231841  L_10 = L_9;
		Il2CppObject * L_11 = Box(Enumerator_t816231841_il2cpp_TypeInfo_var, &L_10);
		NullCheck((Il2CppObject *)L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
		IL2CPP_END_FINALLY(61)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0049:
	{
		Dictionary_2_t3827160297 * L_12 = V_0;
		return L_12;
	}
}
// System.Void SA.IOSDeploy.VariableListed::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t VariableListed__ctor_m2185474432_MetadataUsageId;
extern "C"  void VariableListed__ctor_m2185474432 (VariableListed_t1912381035 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VariableListed__ctor_m2185474432_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_IsOpen_0((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_DictKey_1(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_StringValue_2(L_1);
		__this->set_BooleanValue_5((bool)1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA.IOSNative.Contacts.Contact::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3777443069_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m860136075_MethodInfo_var;
extern const uint32_t Contact__ctor_m1566119026_MetadataUsageId;
extern "C"  void Contact__ctor_m1566119026 (Contact_t4178394798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Contact__ctor_m1566119026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_GivenName_0(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_FamilyName_1(L_1);
		List_1_t1398341365 * L_2 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_2, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_Emails_2(L_2);
		List_1_t3777443069 * L_3 = (List_1_t3777443069 *)il2cpp_codegen_object_new(List_1_t3777443069_il2cpp_TypeInfo_var);
		List_1__ctor_m860136075(L_3, /*hidden argument*/List_1__ctor_m860136075_MethodInfo_var);
		__this->set_PhoneNumbers_3(L_3);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactsResult::.ctor(System.Collections.Generic.List`1<SA.IOSNative.Contacts.Contact>)
extern Il2CppClass* List_1_t3547515930_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1336269712_MethodInfo_var;
extern const uint32_t ContactsResult__ctor_m266668425_MetadataUsageId;
extern "C"  void ContactsResult__ctor_m266668425 (ContactsResult_t1045731284 * __this, List_1_t3547515930 * ___contacts0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactsResult__ctor_m266668425_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3547515930 * L_0 = (List_1_t3547515930 *)il2cpp_codegen_object_new(List_1_t3547515930_il2cpp_TypeInfo_var);
		List_1__ctor_m1336269712(L_0, /*hidden argument*/List_1__ctor_m1336269712_MethodInfo_var);
		__this->set__Contacts_1(L_0);
		Result__ctor_m114131617(__this, /*hidden argument*/NULL);
		List_1_t3547515930 * L_1 = ___contacts0;
		__this->set__Contacts_1(L_1);
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactsResult::.ctor(SA.Common.Models.Error)
extern Il2CppClass* List_1_t3547515930_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1336269712_MethodInfo_var;
extern const uint32_t ContactsResult__ctor_m669686757_MetadataUsageId;
extern "C"  void ContactsResult__ctor_m669686757 (ContactsResult_t1045731284 * __this, Error_t445207774 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactsResult__ctor_m669686757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3547515930 * L_0 = (List_1_t3547515930 *)il2cpp_codegen_object_new(List_1_t3547515930_il2cpp_TypeInfo_var);
		List_1__ctor_m1336269712(L_0, /*hidden argument*/List_1__ctor_m1336269712_MethodInfo_var);
		__this->set__Contacts_1(L_0);
		Error_t445207774 * L_1 = ___error0;
		Result__ctor_m67150262(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<SA.IOSNative.Contacts.Contact> SA.IOSNative.Contacts.ContactsResult::get_Contacts()
extern "C"  List_1_t3547515930 * ContactsResult_get_Contacts_m368706388 (ContactsResult_t1045731284 * __this, const MethodInfo* method)
{
	{
		List_1_t3547515930 * L_0 = __this->get__Contacts_1();
		return L_0;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::.ctor()
extern Il2CppClass* ContactStore_t160827595_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t847530666_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t643392700_il2cpp_TypeInfo_var;
extern const MethodInfo* ContactStore_U3CContactsLoadResultU3Em__5D_m1939839080_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3929150440_MethodInfo_var;
extern const MethodInfo* ContactStore_U3CContactsPickResultU3Em__5E_m1450293290_MethodInfo_var;
extern const MethodInfo* Singleton_1__ctor_m1490556780_MethodInfo_var;
extern const uint32_t ContactStore__ctor_m3006704629_MetadataUsageId;
extern "C"  void ContactStore__ctor_m3006704629 (ContactStore_t160827595 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore__ctor_m3006704629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ContactStore_t160827595 * G_B2_0 = NULL;
	ContactStore_t160827595 * G_B1_0 = NULL;
	ContactStore_t160827595 * G_B4_0 = NULL;
	ContactStore_t160827595 * G_B3_0 = NULL;
	{
		Action_1_t847530666 * L_0 = ((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_6();
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)ContactStore_U3CContactsLoadResultU3Em__5D_m1939839080_MethodInfo_var);
		Action_1_t847530666 * L_2 = (Action_1_t847530666 *)il2cpp_codegen_object_new(Action_1_t847530666_il2cpp_TypeInfo_var);
		Action_1__ctor_m3929150440(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m3929150440_MethodInfo_var);
		((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_6(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Action_1_t847530666 * L_3 = ((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_6();
		NullCheck(G_B2_0);
		G_B2_0->set_ContactsLoadResult_4(L_3);
		Action_1_t847530666 * L_4 = ((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_7();
		G_B3_0 = __this;
		if (L_4)
		{
			G_B4_0 = __this;
			goto IL_003c;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)ContactStore_U3CContactsPickResultU3Em__5E_m1450293290_MethodInfo_var);
		Action_1_t847530666 * L_6 = (Action_1_t847530666 *)il2cpp_codegen_object_new(Action_1_t847530666_il2cpp_TypeInfo_var);
		Action_1__ctor_m3929150440(L_6, NULL, L_5, /*hidden argument*/Action_1__ctor_m3929150440_MethodInfo_var);
		((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_7(L_6);
		G_B4_0 = G_B3_0;
	}

IL_003c:
	{
		Action_1_t847530666 * L_7 = ((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_7();
		NullCheck(G_B4_0);
		G_B4_0->set_ContactsPickResult_5(L_7);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t643392700_il2cpp_TypeInfo_var);
		Singleton_1__ctor_m1490556780(__this, /*hidden argument*/Singleton_1__ctor_m1490556780_MethodInfo_var);
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::add_ContactsLoadResult(System.Action`1<SA.IOSNative.Contacts.ContactsResult>)
extern Il2CppClass* Action_1_t847530666_il2cpp_TypeInfo_var;
extern const uint32_t ContactStore_add_ContactsLoadResult_m3108040362_MetadataUsageId;
extern "C"  void ContactStore_add_ContactsLoadResult_m3108040362 (ContactStore_t160827595 * __this, Action_1_t847530666 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore_add_ContactsLoadResult_m3108040362_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t847530666 * L_0 = __this->get_ContactsLoadResult_4();
		Action_1_t847530666 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ContactsLoadResult_4(((Action_1_t847530666 *)CastclassSealed(L_2, Action_1_t847530666_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::remove_ContactsLoadResult(System.Action`1<SA.IOSNative.Contacts.ContactsResult>)
extern Il2CppClass* Action_1_t847530666_il2cpp_TypeInfo_var;
extern const uint32_t ContactStore_remove_ContactsLoadResult_m3252485327_MetadataUsageId;
extern "C"  void ContactStore_remove_ContactsLoadResult_m3252485327 (ContactStore_t160827595 * __this, Action_1_t847530666 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore_remove_ContactsLoadResult_m3252485327_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t847530666 * L_0 = __this->get_ContactsLoadResult_4();
		Action_1_t847530666 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ContactsLoadResult_4(((Action_1_t847530666 *)CastclassSealed(L_2, Action_1_t847530666_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::add_ContactsPickResult(System.Action`1<SA.IOSNative.Contacts.ContactsResult>)
extern Il2CppClass* Action_1_t847530666_il2cpp_TypeInfo_var;
extern const uint32_t ContactStore_add_ContactsPickResult_m904606315_MetadataUsageId;
extern "C"  void ContactStore_add_ContactsPickResult_m904606315 (ContactStore_t160827595 * __this, Action_1_t847530666 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore_add_ContactsPickResult_m904606315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t847530666 * L_0 = __this->get_ContactsPickResult_5();
		Action_1_t847530666 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ContactsPickResult_5(((Action_1_t847530666 *)CastclassSealed(L_2, Action_1_t847530666_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::remove_ContactsPickResult(System.Action`1<SA.IOSNative.Contacts.ContactsResult>)
extern Il2CppClass* Action_1_t847530666_il2cpp_TypeInfo_var;
extern const uint32_t ContactStore_remove_ContactsPickResult_m2678985818_MetadataUsageId;
extern "C"  void ContactStore_remove_ContactsPickResult_m2678985818 (ContactStore_t160827595 * __this, Action_1_t847530666 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore_remove_ContactsPickResult_m2678985818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t847530666 * L_0 = __this->get_ContactsPickResult_5();
		Action_1_t847530666 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ContactsPickResult_5(((Action_1_t847530666 *)CastclassSealed(L_2, Action_1_t847530666_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ContactStore_Awake_m599978168_MetadataUsageId;
extern "C"  void ContactStore_Awake_m599978168 (ContactStore_t160827595 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore_Awake_m599978168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::ShowContactsPickerUI(System.Action`1<SA.IOSNative.Contacts.ContactsResult>)
extern Il2CppClass* Action_1_t847530666_il2cpp_TypeInfo_var;
extern const uint32_t ContactStore_ShowContactsPickerUI_m3091324270_MetadataUsageId;
extern "C"  void ContactStore_ShowContactsPickerUI_m3091324270 (ContactStore_t160827595 * __this, Action_1_t847530666 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore_ShowContactsPickerUI_m3091324270_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t847530666 * L_0 = __this->get_ContactsPickResult_5();
		Action_1_t847530666 * L_1 = ___callback0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ContactsPickResult_5(((Action_1_t847530666 *)CastclassSealed(L_2, Action_1_t847530666_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::RetrievePhoneContacts(System.Action`1<SA.IOSNative.Contacts.ContactsResult>)
extern Il2CppClass* Action_1_t847530666_il2cpp_TypeInfo_var;
extern const uint32_t ContactStore_RetrievePhoneContacts_m1324477995_MetadataUsageId;
extern "C"  void ContactStore_RetrievePhoneContacts_m1324477995 (ContactStore_t160827595 * __this, Action_1_t847530666 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore_RetrievePhoneContacts_m1324477995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t847530666 * L_0 = __this->get_ContactsLoadResult_4();
		Action_1_t847530666 * L_1 = ___callback0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ContactsLoadResult_4(((Action_1_t847530666 *)CastclassSealed(L_2, Action_1_t847530666_il2cpp_TypeInfo_var)));
		return;
	}
}
// SA.IOSNative.Contacts.Contact SA.IOSNative.Contacts.ContactStore::ParseContactData(System.String)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Contact_t4178394798_il2cpp_TypeInfo_var;
extern Il2CppClass* PhoneNumber_t113354641_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_AddRange_m2513889489_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2480171799_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1576501541;
extern const uint32_t ContactStore_ParseContactData_m2938065374_MetadataUsageId;
extern "C"  Contact_t4178394798 * ContactStore_ParseContactData_m2938065374 (ContactStore_t160827595 * __this, String_t* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore_ParseContactData_m2938065374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	Contact_t4178394798 * V_1 = NULL;
	StringU5BU5D_t1642385972* V_2 = NULL;
	StringU5BU5D_t1642385972* V_3 = NULL;
	StringU5BU5D_t1642385972* V_4 = NULL;
	int32_t V_5 = 0;
	PhoneNumber_t113354641 * V_6 = NULL;
	{
		String_t* L_0 = ___data0;
		CharU5BU5D_t1328083999* L_1 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)124));
		NullCheck(L_0);
		StringU5BU5D_t1642385972* L_2 = String_Split_m3326265864(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Contact_t4178394798 * L_3 = (Contact_t4178394798 *)il2cpp_codegen_object_new(Contact_t4178394798_il2cpp_TypeInfo_var);
		Contact__ctor_m1566119026(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		Contact_t4178394798 * L_4 = V_1;
		StringU5BU5D_t1642385972* L_5 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		int32_t L_6 = 0;
		String_t* L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_4);
		L_4->set_GivenName_0(L_7);
		Contact_t4178394798 * L_8 = V_1;
		StringU5BU5D_t1642385972* L_9 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		int32_t L_10 = 1;
		String_t* L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_8);
		L_8->set_FamilyName_1(L_11);
		StringU5BU5D_t1642385972* L_12 = V_0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		int32_t L_13 = 2;
		String_t* L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		StringU5BU5D_t1642385972* L_15 = Converter_ParseArray_m783528499(NULL /*static, unused*/, L_14, _stringLiteral1576501541, /*hidden argument*/NULL);
		V_2 = L_15;
		Contact_t4178394798 * L_16 = V_1;
		NullCheck(L_16);
		List_1_t1398341365 * L_17 = L_16->get_Emails_2();
		StringU5BU5D_t1642385972* L_18 = V_2;
		NullCheck(L_17);
		List_1_AddRange_m2513889489(L_17, (Il2CppObject*)(Il2CppObject*)L_18, /*hidden argument*/List_1_AddRange_m2513889489_MethodInfo_var);
		StringU5BU5D_t1642385972* L_19 = V_0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		int32_t L_20 = 3;
		String_t* L_21 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		StringU5BU5D_t1642385972* L_22 = Converter_ParseArray_m783528499(NULL /*static, unused*/, L_21, _stringLiteral1576501541, /*hidden argument*/NULL);
		V_3 = L_22;
		StringU5BU5D_t1642385972* L_23 = V_0;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 4);
		int32_t L_24 = 4;
		String_t* L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		StringU5BU5D_t1642385972* L_26 = Converter_ParseArray_m783528499(NULL /*static, unused*/, L_25, _stringLiteral1576501541, /*hidden argument*/NULL);
		V_4 = L_26;
		V_5 = 0;
		goto IL_009a;
	}

IL_0069:
	{
		PhoneNumber_t113354641 * L_27 = (PhoneNumber_t113354641 *)il2cpp_codegen_object_new(PhoneNumber_t113354641_il2cpp_TypeInfo_var);
		PhoneNumber__ctor_m564836165(L_27, /*hidden argument*/NULL);
		V_6 = L_27;
		PhoneNumber_t113354641 * L_28 = V_6;
		StringU5BU5D_t1642385972* L_29 = V_3;
		int32_t L_30 = V_5;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = L_30;
		String_t* L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_28);
		L_28->set_CountryCode_0(L_32);
		PhoneNumber_t113354641 * L_33 = V_6;
		StringU5BU5D_t1642385972* L_34 = V_4;
		int32_t L_35 = V_5;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		int32_t L_36 = L_35;
		String_t* L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_33);
		L_33->set_Digits_1(L_37);
		Contact_t4178394798 * L_38 = V_1;
		NullCheck(L_38);
		List_1_t3777443069 * L_39 = L_38->get_PhoneNumbers_3();
		PhoneNumber_t113354641 * L_40 = V_6;
		NullCheck(L_39);
		List_1_Add_m2480171799(L_39, L_40, /*hidden argument*/List_1_Add_m2480171799_MethodInfo_var);
		int32_t L_41 = V_5;
		V_5 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_42 = V_5;
		StringU5BU5D_t1642385972* L_43 = V_3;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_43)->max_length)))))))
		{
			goto IL_0069;
		}
	}
	{
		Contact_t4178394798 * L_44 = V_1;
		return L_44;
	}
}
// System.Collections.Generic.List`1<SA.IOSNative.Contacts.Contact> SA.IOSNative.Contacts.ContactStore::ParseContactArray(System.String)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3547515930_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1336269712_MethodInfo_var;
extern const MethodInfo* List_1_Add_m825633364_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1576504325;
extern Il2CppCodeGenString* _stringLiteral2226164812;
extern const uint32_t ContactStore_ParseContactArray_m1185376369_MetadataUsageId;
extern "C"  List_1_t3547515930 * ContactStore_ParseContactArray_m1185376369 (ContactStore_t160827595 * __this, String_t* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore_ParseContactArray_m1185376369_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	List_1_t3547515930 * V_1 = NULL;
	int32_t V_2 = 0;
	Contact_t4178394798 * V_3 = NULL;
	{
		String_t* L_0 = ___data0;
		StringU5BU5D_t1642385972* L_1 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, _stringLiteral1576504325);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1576504325);
		NullCheck(L_0);
		StringU5BU5D_t1642385972* L_2 = String_Split_m3927740091(L_0, L_1, 0, /*hidden argument*/NULL);
		V_0 = L_2;
		List_1_t3547515930 * L_3 = (List_1_t3547515930 *)il2cpp_codegen_object_new(List_1_t3547515930_il2cpp_TypeInfo_var);
		List_1__ctor_m1336269712(L_3, /*hidden argument*/List_1__ctor_m1336269712_MethodInfo_var);
		V_1 = L_3;
		V_2 = 0;
		goto IL_004f;
	}

IL_0023:
	{
		StringU5BU5D_t1642385972* L_4 = V_0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		String_t* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, _stringLiteral2226164812, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0058;
	}

IL_003a:
	{
		StringU5BU5D_t1642385972* L_9 = V_0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		String_t* L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Contact_t4178394798 * L_13 = ContactStore_ParseContactData_m2938065374(__this, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		List_1_t3547515930 * L_14 = V_1;
		Contact_t4178394798 * L_15 = V_3;
		NullCheck(L_14);
		List_1_Add_m825633364(L_14, L_15, /*hidden argument*/List_1_Add_m825633364_MethodInfo_var);
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_004f:
	{
		int32_t L_17 = V_2;
		StringU5BU5D_t1642385972* L_18 = V_0;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0023;
		}
	}

IL_0058:
	{
		List_1_t3547515930 * L_19 = V_1;
		return L_19;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::OnContactPickerDidCancel(System.String)
extern Il2CppClass* Error_t445207774_il2cpp_TypeInfo_var;
extern Il2CppClass* ContactsResult_t1045731284_il2cpp_TypeInfo_var;
extern Il2CppClass* ContactStore_t160827595_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t847530666_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m4072936787_MethodInfo_var;
extern const MethodInfo* ContactStore_U3COnContactPickerDidCancelU3Em__5F_m3804620746_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3929150440_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral925872460;
extern const uint32_t ContactStore_OnContactPickerDidCancel_m2845941731_MetadataUsageId;
extern "C"  void ContactStore_OnContactPickerDidCancel_m2845941731 (ContactStore_t160827595 * __this, String_t* ___errorData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore_OnContactPickerDidCancel_m2845941731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Error_t445207774 * V_0 = NULL;
	ContactsResult_t1045731284 * V_1 = NULL;
	ContactStore_t160827595 * G_B2_0 = NULL;
	ContactStore_t160827595 * G_B1_0 = NULL;
	{
		Error_t445207774 * L_0 = (Error_t445207774 *)il2cpp_codegen_object_new(Error_t445207774_il2cpp_TypeInfo_var);
		Error__ctor_m3167320083(L_0, 0, _stringLiteral925872460, /*hidden argument*/NULL);
		V_0 = L_0;
		Error_t445207774 * L_1 = V_0;
		ContactsResult_t1045731284 * L_2 = (ContactsResult_t1045731284 *)il2cpp_codegen_object_new(ContactsResult_t1045731284_il2cpp_TypeInfo_var);
		ContactsResult__ctor_m669686757(L_2, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		Action_1_t847530666 * L_3 = __this->get_ContactsPickResult_5();
		ContactsResult_t1045731284 * L_4 = V_1;
		NullCheck(L_3);
		Action_1_Invoke_m4072936787(L_3, L_4, /*hidden argument*/Action_1_Invoke_m4072936787_MethodInfo_var);
		Action_1_t847530666 * L_5 = ((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_8();
		G_B1_0 = __this;
		if (L_5)
		{
			G_B2_0 = __this;
			goto IL_0038;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)ContactStore_U3COnContactPickerDidCancelU3Em__5F_m3804620746_MethodInfo_var);
		Action_1_t847530666 * L_7 = (Action_1_t847530666 *)il2cpp_codegen_object_new(Action_1_t847530666_il2cpp_TypeInfo_var);
		Action_1__ctor_m3929150440(L_7, NULL, L_6, /*hidden argument*/Action_1__ctor_m3929150440_MethodInfo_var);
		((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_8(L_7);
		G_B2_0 = G_B1_0;
	}

IL_0038:
	{
		Action_1_t847530666 * L_8 = ((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_8();
		NullCheck(G_B2_0);
		G_B2_0->set_ContactsPickResult_5(L_8);
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::OnPickerDidSelectContacts(System.String)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ContactsResult_t1045731284_il2cpp_TypeInfo_var;
extern Il2CppClass* ContactStore_t160827595_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t847530666_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m3037429982_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m4072936787_MethodInfo_var;
extern const MethodInfo* ContactStore_U3COnPickerDidSelectContactsU3Em__60_m1357108638_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3929150440_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1951889900;
extern Il2CppCodeGenString* _stringLiteral1645509673;
extern Il2CppCodeGenString* _stringLiteral2312154431;
extern const uint32_t ContactStore_OnPickerDidSelectContacts_m1476770768_MetadataUsageId;
extern "C"  void ContactStore_OnPickerDidSelectContacts_m1476770768 (ContactStore_t160827595 * __this, String_t* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore_OnPickerDidSelectContacts_m1476770768_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3547515930 * V_0 = NULL;
	ContactsResult_t1045731284 * V_1 = NULL;
	ContactStore_t160827595 * G_B2_0 = NULL;
	ContactStore_t160827595 * G_B1_0 = NULL;
	{
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, _stringLiteral1951889900, 3, /*hidden argument*/NULL);
		String_t* L_0 = ___data0;
		List_1_t3547515930 * L_1 = ContactStore_ParseContactArray_m1185376369(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		List_1_t3547515930 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m3037429982(L_2, /*hidden argument*/List_1_get_Count_m3037429982_MethodInfo_var);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral1645509673, L_5, _stringLiteral2312154431, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_6, 3, /*hidden argument*/NULL);
		List_1_t3547515930 * L_7 = V_0;
		ContactsResult_t1045731284 * L_8 = (ContactsResult_t1045731284 *)il2cpp_codegen_object_new(ContactsResult_t1045731284_il2cpp_TypeInfo_var);
		ContactsResult__ctor_m266668425(L_8, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		Action_1_t847530666 * L_9 = __this->get_ContactsPickResult_5();
		ContactsResult_t1045731284 * L_10 = V_1;
		NullCheck(L_9);
		Action_1_Invoke_m4072936787(L_9, L_10, /*hidden argument*/Action_1_Invoke_m4072936787_MethodInfo_var);
		Action_1_t847530666 * L_11 = ((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_9();
		G_B1_0 = __this;
		if (L_11)
		{
			G_B2_0 = __this;
			goto IL_005f;
		}
	}
	{
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)ContactStore_U3COnPickerDidSelectContactsU3Em__60_m1357108638_MethodInfo_var);
		Action_1_t847530666 * L_13 = (Action_1_t847530666 *)il2cpp_codegen_object_new(Action_1_t847530666_il2cpp_TypeInfo_var);
		Action_1__ctor_m3929150440(L_13, NULL, L_12, /*hidden argument*/Action_1__ctor_m3929150440_MethodInfo_var);
		((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache5_9(L_13);
		G_B2_0 = G_B1_0;
	}

IL_005f:
	{
		Action_1_t847530666 * L_14 = ((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_9();
		NullCheck(G_B2_0);
		G_B2_0->set_ContactsPickResult_5(L_14);
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::OnContactsRetrieveFailed(System.String)
extern Il2CppClass* Error_t445207774_il2cpp_TypeInfo_var;
extern Il2CppClass* ContactsResult_t1045731284_il2cpp_TypeInfo_var;
extern Il2CppClass* ContactStore_t160827595_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t847530666_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m4072936787_MethodInfo_var;
extern const MethodInfo* ContactStore_U3COnContactsRetrieveFailedU3Em__61_m3095485511_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3929150440_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3539838638;
extern const uint32_t ContactStore_OnContactsRetrieveFailed_m107372548_MetadataUsageId;
extern "C"  void ContactStore_OnContactsRetrieveFailed_m107372548 (ContactStore_t160827595 * __this, String_t* ___errorData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore_OnContactsRetrieveFailed_m107372548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Error_t445207774 * V_0 = NULL;
	ContactsResult_t1045731284 * V_1 = NULL;
	ContactStore_t160827595 * G_B2_0 = NULL;
	ContactStore_t160827595 * G_B1_0 = NULL;
	{
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, _stringLiteral3539838638, 3, /*hidden argument*/NULL);
		String_t* L_0 = ___errorData0;
		Error_t445207774 * L_1 = (Error_t445207774 *)il2cpp_codegen_object_new(Error_t445207774_il2cpp_TypeInfo_var);
		Error__ctor_m994168232(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Error_t445207774 * L_2 = V_0;
		ContactsResult_t1045731284 * L_3 = (ContactsResult_t1045731284 *)il2cpp_codegen_object_new(ContactsResult_t1045731284_il2cpp_TypeInfo_var);
		ContactsResult__ctor_m669686757(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Action_1_t847530666 * L_4 = __this->get_ContactsLoadResult_4();
		ContactsResult_t1045731284 * L_5 = V_1;
		NullCheck(L_4);
		Action_1_Invoke_m4072936787(L_4, L_5, /*hidden argument*/Action_1_Invoke_m4072936787_MethodInfo_var);
		Action_1_t847530666 * L_6 = ((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_10();
		G_B1_0 = __this;
		if (L_6)
		{
			G_B2_0 = __this;
			goto IL_003e;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)ContactStore_U3COnContactsRetrieveFailedU3Em__61_m3095485511_MethodInfo_var);
		Action_1_t847530666 * L_8 = (Action_1_t847530666 *)il2cpp_codegen_object_new(Action_1_t847530666_il2cpp_TypeInfo_var);
		Action_1__ctor_m3929150440(L_8, NULL, L_7, /*hidden argument*/Action_1__ctor_m3929150440_MethodInfo_var);
		((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache6_10(L_8);
		G_B2_0 = G_B1_0;
	}

IL_003e:
	{
		Action_1_t847530666 * L_9 = ((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_10();
		NullCheck(G_B2_0);
		G_B2_0->set_ContactsLoadResult_4(L_9);
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::OnContactsRetrieveFinished(System.String)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ContactsResult_t1045731284_il2cpp_TypeInfo_var;
extern Il2CppClass* ContactStore_t160827595_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t847530666_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m3037429982_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m4072936787_MethodInfo_var;
extern const MethodInfo* ContactStore_U3COnContactsRetrieveFinishedU3Em__62_m3905779813_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3929150440_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral561059279;
extern Il2CppCodeGenString* _stringLiteral442065462;
extern Il2CppCodeGenString* _stringLiteral2312154431;
extern const uint32_t ContactStore_OnContactsRetrieveFinished_m3883776513_MetadataUsageId;
extern "C"  void ContactStore_OnContactsRetrieveFinished_m3883776513 (ContactStore_t160827595 * __this, String_t* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContactStore_OnContactsRetrieveFinished_m3883776513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3547515930 * V_0 = NULL;
	ContactsResult_t1045731284 * V_1 = NULL;
	ContactStore_t160827595 * G_B2_0 = NULL;
	ContactStore_t160827595 * G_B1_0 = NULL;
	{
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, _stringLiteral561059279, 3, /*hidden argument*/NULL);
		String_t* L_0 = ___data0;
		List_1_t3547515930 * L_1 = ContactStore_ParseContactArray_m1185376369(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		List_1_t3547515930 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m3037429982(L_2, /*hidden argument*/List_1_get_Count_m3037429982_MethodInfo_var);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral442065462, L_5, _stringLiteral2312154431, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_6, 3, /*hidden argument*/NULL);
		List_1_t3547515930 * L_7 = V_0;
		ContactsResult_t1045731284 * L_8 = (ContactsResult_t1045731284 *)il2cpp_codegen_object_new(ContactsResult_t1045731284_il2cpp_TypeInfo_var);
		ContactsResult__ctor_m266668425(L_8, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		Action_1_t847530666 * L_9 = __this->get_ContactsLoadResult_4();
		ContactsResult_t1045731284 * L_10 = V_1;
		NullCheck(L_9);
		Action_1_Invoke_m4072936787(L_9, L_10, /*hidden argument*/Action_1_Invoke_m4072936787_MethodInfo_var);
		Action_1_t847530666 * L_11 = ((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_11();
		G_B1_0 = __this;
		if (L_11)
		{
			G_B2_0 = __this;
			goto IL_005f;
		}
	}
	{
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)ContactStore_U3COnContactsRetrieveFinishedU3Em__62_m3905779813_MethodInfo_var);
		Action_1_t847530666 * L_13 = (Action_1_t847530666 *)il2cpp_codegen_object_new(Action_1_t847530666_il2cpp_TypeInfo_var);
		Action_1__ctor_m3929150440(L_13, NULL, L_12, /*hidden argument*/Action_1__ctor_m3929150440_MethodInfo_var);
		((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache7_11(L_13);
		G_B2_0 = G_B1_0;
	}

IL_005f:
	{
		Action_1_t847530666 * L_14 = ((ContactStore_t160827595_StaticFields*)ContactStore_t160827595_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_11();
		NullCheck(G_B2_0);
		G_B2_0->set_ContactsLoadResult_4(L_14);
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::<ContactsLoadResult>m__5D(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void ContactStore_U3CContactsLoadResultU3Em__5D_m1939839080 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::<ContactsPickResult>m__5E(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void ContactStore_U3CContactsPickResultU3Em__5E_m1450293290 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::<OnContactPickerDidCancel>m__5F(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void ContactStore_U3COnContactPickerDidCancelU3Em__5F_m3804620746 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::<OnPickerDidSelectContacts>m__60(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void ContactStore_U3COnPickerDidSelectContactsU3Em__60_m1357108638 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::<OnContactsRetrieveFailed>m__61(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void ContactStore_U3COnContactsRetrieveFailedU3Em__61_m3095485511 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA.IOSNative.Contacts.ContactStore::<OnContactsRetrieveFinished>m__62(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void ContactStore_U3COnContactsRetrieveFinishedU3Em__62_m3905779813 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA.IOSNative.Contacts.PhoneNumber::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PhoneNumber__ctor_m564836165_MetadataUsageId;
extern "C"  void PhoneNumber__ctor_m564836165 (PhoneNumber_t113354641 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PhoneNumber__ctor_m564836165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_CountryCode_0(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_Digits_1(L_1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Ad_EditorUIController::.ctor()
extern Il2CppClass* SA_Ad_EditorUIController_t4080214878_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const MethodInfo* SA_Ad_EditorUIController_U3COnCloseVideoU3Em__81_m54967096_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m309821356_MethodInfo_var;
extern const MethodInfo* SA_Ad_EditorUIController_U3COnVideoLeftApplicationU3Em__82_m1798040571_MethodInfo_var;
extern const MethodInfo* SA_Ad_EditorUIController_U3COnCloseInterstitialU3Em__83_m3395544757_MethodInfo_var;
extern const MethodInfo* SA_Ad_EditorUIController_U3COnInterstitialLeftApplicationU3Em__84_m164768560_MethodInfo_var;
extern const uint32_t SA_Ad_EditorUIController__ctor_m2754299091_MetadataUsageId;
extern "C"  void SA_Ad_EditorUIController__ctor_m2754299091 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Ad_EditorUIController__ctor_m2754299091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SA_Ad_EditorUIController_t4080214878 * G_B2_0 = NULL;
	SA_Ad_EditorUIController_t4080214878 * G_B1_0 = NULL;
	SA_Ad_EditorUIController_t4080214878 * G_B4_0 = NULL;
	SA_Ad_EditorUIController_t4080214878 * G_B3_0 = NULL;
	SA_Ad_EditorUIController_t4080214878 * G_B6_0 = NULL;
	SA_Ad_EditorUIController_t4080214878 * G_B5_0 = NULL;
	SA_Ad_EditorUIController_t4080214878 * G_B8_0 = NULL;
	SA_Ad_EditorUIController_t4080214878 * G_B7_0 = NULL;
	{
		Action_1_t3627374100 * L_0 = ((SA_Ad_EditorUIController_t4080214878_StaticFields*)SA_Ad_EditorUIController_t4080214878_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache8_10();
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)SA_Ad_EditorUIController_U3COnCloseVideoU3Em__81_m54967096_MethodInfo_var);
		Action_1_t3627374100 * L_2 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		((SA_Ad_EditorUIController_t4080214878_StaticFields*)SA_Ad_EditorUIController_t4080214878_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache8_10(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Action_1_t3627374100 * L_3 = ((SA_Ad_EditorUIController_t4080214878_StaticFields*)SA_Ad_EditorUIController_t4080214878_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache8_10();
		NullCheck(G_B2_0);
		G_B2_0->set_OnCloseVideo_6(L_3);
		Action_t3226471752 * L_4 = ((SA_Ad_EditorUIController_t4080214878_StaticFields*)SA_Ad_EditorUIController_t4080214878_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache9_11();
		G_B3_0 = __this;
		if (L_4)
		{
			G_B4_0 = __this;
			goto IL_003c;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)SA_Ad_EditorUIController_U3COnVideoLeftApplicationU3Em__82_m1798040571_MethodInfo_var);
		Action_t3226471752 * L_6 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_6, NULL, L_5, /*hidden argument*/NULL);
		((SA_Ad_EditorUIController_t4080214878_StaticFields*)SA_Ad_EditorUIController_t4080214878_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache9_11(L_6);
		G_B4_0 = G_B3_0;
	}

IL_003c:
	{
		Action_t3226471752 * L_7 = ((SA_Ad_EditorUIController_t4080214878_StaticFields*)SA_Ad_EditorUIController_t4080214878_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache9_11();
		NullCheck(G_B4_0);
		G_B4_0->set_OnVideoLeftApplication_7(L_7);
		Action_1_t3627374100 * L_8 = ((SA_Ad_EditorUIController_t4080214878_StaticFields*)SA_Ad_EditorUIController_t4080214878_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheA_12();
		G_B5_0 = __this;
		if (L_8)
		{
			G_B6_0 = __this;
			goto IL_005f;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)SA_Ad_EditorUIController_U3COnCloseInterstitialU3Em__83_m3395544757_MethodInfo_var);
		Action_1_t3627374100 * L_10 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_10, NULL, L_9, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		((SA_Ad_EditorUIController_t4080214878_StaticFields*)SA_Ad_EditorUIController_t4080214878_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheA_12(L_10);
		G_B6_0 = G_B5_0;
	}

IL_005f:
	{
		Action_1_t3627374100 * L_11 = ((SA_Ad_EditorUIController_t4080214878_StaticFields*)SA_Ad_EditorUIController_t4080214878_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheA_12();
		NullCheck(G_B6_0);
		G_B6_0->set_OnCloseInterstitial_8(L_11);
		Action_t3226471752 * L_12 = ((SA_Ad_EditorUIController_t4080214878_StaticFields*)SA_Ad_EditorUIController_t4080214878_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheB_13();
		G_B7_0 = __this;
		if (L_12)
		{
			G_B8_0 = __this;
			goto IL_0082;
		}
	}
	{
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)SA_Ad_EditorUIController_U3COnInterstitialLeftApplicationU3Em__84_m164768560_MethodInfo_var);
		Action_t3226471752 * L_14 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_14, NULL, L_13, /*hidden argument*/NULL);
		((SA_Ad_EditorUIController_t4080214878_StaticFields*)SA_Ad_EditorUIController_t4080214878_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheB_13(L_14);
		G_B8_0 = G_B7_0;
	}

IL_0082:
	{
		Action_t3226471752 * L_15 = ((SA_Ad_EditorUIController_t4080214878_StaticFields*)SA_Ad_EditorUIController_t4080214878_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheB_13();
		NullCheck(G_B8_0);
		G_B8_0->set_OnInterstitialLeftApplication_9(L_15);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Ad_EditorUIController::add_OnCloseVideo(System.Action`1<System.Boolean>)
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern const uint32_t SA_Ad_EditorUIController_add_OnCloseVideo_m2668537072_MetadataUsageId;
extern "C"  void SA_Ad_EditorUIController_add_OnCloseVideo_m2668537072 (SA_Ad_EditorUIController_t4080214878 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Ad_EditorUIController_add_OnCloseVideo_m2668537072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = __this->get_OnCloseVideo_6();
		Action_1_t3627374100 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnCloseVideo_6(((Action_1_t3627374100 *)CastclassSealed(L_2, Action_1_t3627374100_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_Ad_EditorUIController::remove_OnCloseVideo(System.Action`1<System.Boolean>)
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern const uint32_t SA_Ad_EditorUIController_remove_OnCloseVideo_m1838040549_MetadataUsageId;
extern "C"  void SA_Ad_EditorUIController_remove_OnCloseVideo_m1838040549 (SA_Ad_EditorUIController_t4080214878 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Ad_EditorUIController_remove_OnCloseVideo_m1838040549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = __this->get_OnCloseVideo_6();
		Action_1_t3627374100 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnCloseVideo_6(((Action_1_t3627374100 *)CastclassSealed(L_2, Action_1_t3627374100_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_Ad_EditorUIController::add_OnVideoLeftApplication(System.Action)
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const uint32_t SA_Ad_EditorUIController_add_OnVideoLeftApplication_m4048021005_MetadataUsageId;
extern "C"  void SA_Ad_EditorUIController_add_OnVideoLeftApplication_m4048021005 (SA_Ad_EditorUIController_t4080214878 * __this, Action_t3226471752 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Ad_EditorUIController_add_OnVideoLeftApplication_m4048021005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t3226471752 * L_0 = __this->get_OnVideoLeftApplication_7();
		Action_t3226471752 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnVideoLeftApplication_7(((Action_t3226471752 *)CastclassSealed(L_2, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_Ad_EditorUIController::remove_OnVideoLeftApplication(System.Action)
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const uint32_t SA_Ad_EditorUIController_remove_OnVideoLeftApplication_m3273645198_MetadataUsageId;
extern "C"  void SA_Ad_EditorUIController_remove_OnVideoLeftApplication_m3273645198 (SA_Ad_EditorUIController_t4080214878 * __this, Action_t3226471752 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Ad_EditorUIController_remove_OnVideoLeftApplication_m3273645198_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t3226471752 * L_0 = __this->get_OnVideoLeftApplication_7();
		Action_t3226471752 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnVideoLeftApplication_7(((Action_t3226471752 *)CastclassSealed(L_2, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_Ad_EditorUIController::add_OnCloseInterstitial(System.Action`1<System.Boolean>)
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern const uint32_t SA_Ad_EditorUIController_add_OnCloseInterstitial_m114112109_MetadataUsageId;
extern "C"  void SA_Ad_EditorUIController_add_OnCloseInterstitial_m114112109 (SA_Ad_EditorUIController_t4080214878 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Ad_EditorUIController_add_OnCloseInterstitial_m114112109_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = __this->get_OnCloseInterstitial_8();
		Action_1_t3627374100 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnCloseInterstitial_8(((Action_1_t3627374100 *)CastclassSealed(L_2, Action_1_t3627374100_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_Ad_EditorUIController::remove_OnCloseInterstitial(System.Action`1<System.Boolean>)
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern const uint32_t SA_Ad_EditorUIController_remove_OnCloseInterstitial_m1246761324_MetadataUsageId;
extern "C"  void SA_Ad_EditorUIController_remove_OnCloseInterstitial_m1246761324 (SA_Ad_EditorUIController_t4080214878 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Ad_EditorUIController_remove_OnCloseInterstitial_m1246761324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = __this->get_OnCloseInterstitial_8();
		Action_1_t3627374100 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnCloseInterstitial_8(((Action_1_t3627374100 *)CastclassSealed(L_2, Action_1_t3627374100_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_Ad_EditorUIController::add_OnInterstitialLeftApplication(System.Action)
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const uint32_t SA_Ad_EditorUIController_add_OnInterstitialLeftApplication_m578268554_MetadataUsageId;
extern "C"  void SA_Ad_EditorUIController_add_OnInterstitialLeftApplication_m578268554 (SA_Ad_EditorUIController_t4080214878 * __this, Action_t3226471752 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Ad_EditorUIController_add_OnInterstitialLeftApplication_m578268554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t3226471752 * L_0 = __this->get_OnInterstitialLeftApplication_9();
		Action_t3226471752 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnInterstitialLeftApplication_9(((Action_t3226471752 *)CastclassSealed(L_2, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_Ad_EditorUIController::remove_OnInterstitialLeftApplication(System.Action)
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const uint32_t SA_Ad_EditorUIController_remove_OnInterstitialLeftApplication_m3223399041_MetadataUsageId;
extern "C"  void SA_Ad_EditorUIController_remove_OnInterstitialLeftApplication_m3223399041 (SA_Ad_EditorUIController_t4080214878 * __this, Action_t3226471752 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Ad_EditorUIController_remove_OnInterstitialLeftApplication_m3223399041_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t3226471752 * L_0 = __this->get_OnInterstitialLeftApplication_9();
		Action_t3226471752 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnInterstitialLeftApplication_9(((Action_t3226471752 *)CastclassSealed(L_2, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_Ad_EditorUIController::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var;
extern const uint32_t SA_Ad_EditorUIController_Awake_m863792080_MetadataUsageId;
extern "C"  void SA_Ad_EditorUIController_Awake_m863792080 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Ad_EditorUIController_Awake_m863792080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Canvas_t209405766 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SA_EditorTesting_CheckForEventSystem_m1303346307(NULL /*static, unused*/, /*hidden argument*/NULL);
		Canvas_t209405766 * L_1 = Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var);
		V_0 = L_1;
		Canvas_t209405766 * L_2 = V_0;
		NullCheck(L_2);
		Canvas_set_sortingOrder_m2922819993(L_2, ((int32_t)10001), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Ad_EditorUIController::Start()
extern "C"  void SA_Ad_EditorUIController_Start_m2958576415 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_Ad_EditorUIController::InterstitialClick()
extern "C"  void SA_Ad_EditorUIController_InterstitialClick_m3665011207 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method)
{
	{
		Action_t3226471752 * L_0 = __this->get_OnInterstitialLeftApplication_9();
		NullCheck(L_0);
		Action_Invoke_m3801112262(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Ad_EditorUIController::VideoClick()
extern "C"  void SA_Ad_EditorUIController_VideoClick_m2145169358 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method)
{
	{
		Action_t3226471752 * L_0 = __this->get_OnVideoLeftApplication_7();
		NullCheck(L_0);
		Action_Invoke_m3801112262(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Ad_EditorUIController::ShowInterstitialAd()
extern "C"  void SA_Ad_EditorUIController_ShowInterstitialAd_m142409635 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_InterstitialPanel_3();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Ad_EditorUIController::ShowVideoAd()
extern "C"  void SA_Ad_EditorUIController_ShowVideoAd_m1736833306 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_VideoPanel_2();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Ad_EditorUIController::CloseInterstitial()
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t SA_Ad_EditorUIController_CloseInterstitial_m1695380903_MetadataUsageId;
extern "C"  void SA_Ad_EditorUIController_CloseInterstitial_m1695380903 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Ad_EditorUIController_CloseInterstitial_m1695380903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_InterstitialPanel_3();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		Action_1_t3627374100 * L_2 = __this->get_OnCloseInterstitial_8();
		NullCheck(L_2);
		Action_1_Invoke_m3662000152(L_2, (bool)1, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		return;
	}
}
// System.Void SA_Ad_EditorUIController::CloseVideo()
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t SA_Ad_EditorUIController_CloseVideo_m962096164_MetadataUsageId;
extern "C"  void SA_Ad_EditorUIController_CloseVideo_m962096164 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Ad_EditorUIController_CloseVideo_m962096164_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_VideoPanel_2();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		Action_1_t3627374100 * L_2 = __this->get_OnCloseVideo_6();
		NullCheck(L_2);
		Action_1_Invoke_m3662000152(L_2, (bool)1, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		return;
	}
}
// System.Void SA_Ad_EditorUIController::<OnCloseVideo>m__81(System.Boolean)
extern "C"  void SA_Ad_EditorUIController_U3COnCloseVideoU3Em__81_m54967096 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_Ad_EditorUIController::<OnVideoLeftApplication>m__82()
extern "C"  void SA_Ad_EditorUIController_U3COnVideoLeftApplicationU3Em__82_m1798040571 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_Ad_EditorUIController::<OnCloseInterstitial>m__83(System.Boolean)
extern "C"  void SA_Ad_EditorUIController_U3COnCloseInterstitialU3Em__83_m3395544757 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_Ad_EditorUIController::<OnInterstitialLeftApplication>m__84()
extern "C"  void SA_Ad_EditorUIController_U3COnInterstitialLeftApplicationU3Em__84_m164768560 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_EditorAd::.ctor()
extern Il2CppClass* Singleton_1_t1892724392_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1__ctor_m445423581_MethodInfo_var;
extern const uint32_t SA_EditorAd__ctor_m221181330_MetadataUsageId;
extern "C"  void SA_EditorAd__ctor_m221181330 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd__ctor_m221181330_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set__FillRate_10(((int32_t)100));
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1892724392_il2cpp_TypeInfo_var);
		Singleton_1__ctor_m445423581(__this, /*hidden argument*/Singleton_1__ctor_m445423581_MethodInfo_var);
		return;
	}
}
// System.Void SA_EditorAd::.cctor()
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const MethodInfo* SA_EditorAd_U3COnInterstitialFinishedU3Em__85_m1726536410_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m309821356_MethodInfo_var;
extern const MethodInfo* SA_EditorAd_U3COnInterstitialLoadCompleteU3Em__86_m3467906502_MethodInfo_var;
extern const MethodInfo* SA_EditorAd_U3COnInterstitialLeftApplicationU3Em__87_m2918659926_MethodInfo_var;
extern const MethodInfo* SA_EditorAd_U3COnVideoFinishedU3Em__88_m143744916_MethodInfo_var;
extern const MethodInfo* SA_EditorAd_U3COnVideoLoadCompleteU3Em__89_m3518109402_MethodInfo_var;
extern const MethodInfo* SA_EditorAd_U3COnVideoLeftApplicationU3Em__8A_m1595797995_MethodInfo_var;
extern const uint32_t SA_EditorAd__cctor_m1704305763_MetadataUsageId;
extern "C"  void SA_EditorAd__cctor_m1704305763 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd__cctor_m1704305763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheC_18();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)SA_EditorAd_U3COnInterstitialFinishedU3Em__85_m1726536410_MethodInfo_var);
		Action_1_t3627374100 * L_2 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheC_18(L_2);
	}

IL_0018:
	{
		Action_1_t3627374100 * L_3 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheC_18();
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnInterstitialFinished_12(L_3);
		Action_1_t3627374100 * L_4 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheD_19();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)SA_EditorAd_U3COnInterstitialLoadCompleteU3Em__86_m3467906502_MethodInfo_var);
		Action_1_t3627374100 * L_6 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_6, NULL, L_5, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheD_19(L_6);
	}

IL_003a:
	{
		Action_1_t3627374100 * L_7 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheD_19();
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnInterstitialLoadComplete_13(L_7);
		Action_t3226471752 * L_8 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheE_20();
		if (L_8)
		{
			goto IL_005c;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)SA_EditorAd_U3COnInterstitialLeftApplicationU3Em__87_m2918659926_MethodInfo_var);
		Action_t3226471752 * L_10 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_10, NULL, L_9, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheE_20(L_10);
	}

IL_005c:
	{
		Action_t3226471752 * L_11 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheE_20();
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnInterstitialLeftApplication_14(L_11);
		Action_1_t3627374100 * L_12 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheF_21();
		if (L_12)
		{
			goto IL_007e;
		}
	}
	{
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)SA_EditorAd_U3COnVideoFinishedU3Em__88_m143744916_MethodInfo_var);
		Action_1_t3627374100 * L_14 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_14, NULL, L_13, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheF_21(L_14);
	}

IL_007e:
	{
		Action_1_t3627374100 * L_15 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheF_21();
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnVideoFinished_15(L_15);
		Action_1_t3627374100 * L_16 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache10_22();
		if (L_16)
		{
			goto IL_00a0;
		}
	}
	{
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)SA_EditorAd_U3COnVideoLoadCompleteU3Em__89_m3518109402_MethodInfo_var);
		Action_1_t3627374100 * L_18 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_18, NULL, L_17, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache10_22(L_18);
	}

IL_00a0:
	{
		Action_1_t3627374100 * L_19 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache10_22();
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnVideoLoadComplete_16(L_19);
		Action_t3226471752 * L_20 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache11_23();
		if (L_20)
		{
			goto IL_00c2;
		}
	}
	{
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)SA_EditorAd_U3COnVideoLeftApplicationU3Em__8A_m1595797995_MethodInfo_var);
		Action_t3226471752 * L_22 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_22, NULL, L_21, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache11_23(L_22);
	}

IL_00c2:
	{
		Action_t3226471752 * L_23 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache11_23();
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnVideoLeftApplication_17(L_23);
		return;
	}
}
// System.Void SA_EditorAd::add_OnInterstitialFinished(System.Action`1<System.Boolean>)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_add_OnInterstitialFinished_m3371953136_MetadataUsageId;
extern "C"  void SA_EditorAd_add_OnInterstitialFinished_m3371953136 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_add_OnInterstitialFinished_m3371953136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnInterstitialFinished_12();
		Action_1_t3627374100 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnInterstitialFinished_12(((Action_1_t3627374100 *)CastclassSealed(L_2, Action_1_t3627374100_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_EditorAd::remove_OnInterstitialFinished(System.Action`1<System.Boolean>)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_remove_OnInterstitialFinished_m3646045237_MetadataUsageId;
extern "C"  void SA_EditorAd_remove_OnInterstitialFinished_m3646045237 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_remove_OnInterstitialFinished_m3646045237_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnInterstitialFinished_12();
		Action_1_t3627374100 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnInterstitialFinished_12(((Action_1_t3627374100 *)CastclassSealed(L_2, Action_1_t3627374100_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_EditorAd::add_OnInterstitialLoadComplete(System.Action`1<System.Boolean>)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_add_OnInterstitialLoadComplete_m3568679983_MetadataUsageId;
extern "C"  void SA_EditorAd_add_OnInterstitialLoadComplete_m3568679983 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_add_OnInterstitialLoadComplete_m3568679983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnInterstitialLoadComplete_13();
		Action_1_t3627374100 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnInterstitialLoadComplete_13(((Action_1_t3627374100 *)CastclassSealed(L_2, Action_1_t3627374100_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_EditorAd::remove_OnInterstitialLoadComplete(System.Action`1<System.Boolean>)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_remove_OnInterstitialLoadComplete_m3690804946_MetadataUsageId;
extern "C"  void SA_EditorAd_remove_OnInterstitialLoadComplete_m3690804946 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_remove_OnInterstitialLoadComplete_m3690804946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnInterstitialLoadComplete_13();
		Action_1_t3627374100 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnInterstitialLoadComplete_13(((Action_1_t3627374100 *)CastclassSealed(L_2, Action_1_t3627374100_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_EditorAd::add_OnInterstitialLeftApplication(System.Action)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_add_OnInterstitialLeftApplication_m455497281_MetadataUsageId;
extern "C"  void SA_EditorAd_add_OnInterstitialLeftApplication_m455497281 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_add_OnInterstitialLeftApplication_m455497281_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_t3226471752 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnInterstitialLeftApplication_14();
		Action_t3226471752 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnInterstitialLeftApplication_14(((Action_t3226471752 *)CastclassSealed(L_2, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_EditorAd::remove_OnInterstitialLeftApplication(System.Action)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_remove_OnInterstitialLeftApplication_m4089595218_MetadataUsageId;
extern "C"  void SA_EditorAd_remove_OnInterstitialLeftApplication_m4089595218 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_remove_OnInterstitialLeftApplication_m4089595218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_t3226471752 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnInterstitialLeftApplication_14();
		Action_t3226471752 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnInterstitialLeftApplication_14(((Action_t3226471752 *)CastclassSealed(L_2, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_EditorAd::add_OnVideoFinished(System.Action`1<System.Boolean>)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_add_OnVideoFinished_m1331794961_MetadataUsageId;
extern "C"  void SA_EditorAd_add_OnVideoFinished_m1331794961 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_add_OnVideoFinished_m1331794961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnVideoFinished_15();
		Action_1_t3627374100 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnVideoFinished_15(((Action_1_t3627374100 *)CastclassSealed(L_2, Action_1_t3627374100_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_EditorAd::remove_OnVideoFinished(System.Action`1<System.Boolean>)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_remove_OnVideoFinished_m3221228756_MetadataUsageId;
extern "C"  void SA_EditorAd_remove_OnVideoFinished_m3221228756 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_remove_OnVideoFinished_m3221228756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnVideoFinished_15();
		Action_1_t3627374100 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnVideoFinished_15(((Action_1_t3627374100 *)CastclassSealed(L_2, Action_1_t3627374100_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_EditorAd::add_OnVideoLoadComplete(System.Action`1<System.Boolean>)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_add_OnVideoLoadComplete_m3949273972_MetadataUsageId;
extern "C"  void SA_EditorAd_add_OnVideoLoadComplete_m3949273972 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_add_OnVideoLoadComplete_m3949273972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnVideoLoadComplete_16();
		Action_1_t3627374100 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnVideoLoadComplete_16(((Action_1_t3627374100 *)CastclassSealed(L_2, Action_1_t3627374100_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_EditorAd::remove_OnVideoLoadComplete(System.Action`1<System.Boolean>)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_remove_OnVideoLoadComplete_m4259709539_MetadataUsageId;
extern "C"  void SA_EditorAd_remove_OnVideoLoadComplete_m4259709539 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_remove_OnVideoLoadComplete_m4259709539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnVideoLoadComplete_16();
		Action_1_t3627374100 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnVideoLoadComplete_16(((Action_1_t3627374100 *)CastclassSealed(L_2, Action_1_t3627374100_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_EditorAd::add_OnVideoLeftApplication(System.Action)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_add_OnVideoLeftApplication_m1976118804_MetadataUsageId;
extern "C"  void SA_EditorAd_add_OnVideoLeftApplication_m1976118804 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_add_OnVideoLeftApplication_m1976118804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_t3226471752 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnVideoLeftApplication_17();
		Action_t3226471752 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnVideoLeftApplication_17(((Action_t3226471752 *)CastclassSealed(L_2, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_EditorAd::remove_OnVideoLeftApplication(System.Action)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_remove_OnVideoLeftApplication_m292313749_MetadataUsageId;
extern "C"  void SA_EditorAd_remove_OnVideoLeftApplication_m292313749 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_remove_OnVideoLeftApplication_m292313749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_t3226471752 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnVideoLeftApplication_17();
		Action_t3226471752 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->set_OnVideoLeftApplication_17(((Action_t3226471752 *)CastclassSealed(L_2, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SA_EditorAd::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_Awake_m2091180225_MetadataUsageId;
extern "C"  void SA_EditorAd_Awake_m2091180225 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_Awake_m2091180225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorAd::SetFillRate(System.Int32)
extern "C"  void SA_EditorAd_SetFillRate_m841461312 (SA_EditorAd_t1410159287 * __this, int32_t ___fillRate0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___fillRate0;
		__this->set__FillRate_10(L_0);
		return;
	}
}
// System.Void SA_EditorAd::LoadInterstitial()
extern Il2CppCodeGenString* _stringLiteral334316091;
extern const uint32_t SA_EditorAd_LoadInterstitial_m92056142_MetadataUsageId;
extern "C"  void SA_EditorAd_LoadInterstitial_m92056142 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_LoadInterstitial_m92056142_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		bool L_0 = __this->get__IsInterstitialLoading_6();
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		bool L_1 = SA_EditorAd_get_IsInterstitialReady_m4069673862(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0039;
		}
	}
	{
		__this->set__IsInterstitialLoading_6((bool)1);
		float L_2 = Random_Range_m2884721203(NULL /*static, unused*/, (1.0f), (3.0f), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral334316091, L_3, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void SA_EditorAd::ShowInterstitial()
extern "C"  void SA_EditorAd_ShowInterstitial_m668797125 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	{
		bool L_0 = SA_EditorAd_get_IsInterstitialReady_m4069673862(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000b;
		}
	}

IL_000b:
	{
		return;
	}
}
// System.Void SA_EditorAd::LoadVideo()
extern Il2CppCodeGenString* _stringLiteral3390038804;
extern const uint32_t SA_EditorAd_LoadVideo_m534603967_MetadataUsageId;
extern "C"  void SA_EditorAd_LoadVideo_m534603967 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_LoadVideo_m534603967_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		bool L_0 = __this->get__IsVideoLoading_7();
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		bool L_1 = SA_EditorAd_get_IsVideoReady_m2967159589(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0039;
		}
	}
	{
		__this->set__IsVideoLoading_7((bool)1);
		float L_2 = Random_Range_m2884721203(NULL /*static, unused*/, (1.0f), (3.0f), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral3390038804, L_3, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void SA_EditorAd::ShowVideo()
extern "C"  void SA_EditorAd_ShowVideo_m434293846 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	{
		bool L_0 = SA_EditorAd_get_IsVideoReady_m2967159589(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000b;
		}
	}

IL_000b:
	{
		return;
	}
}
// System.Boolean SA_EditorAd::get_IsVideoReady()
extern "C"  bool SA_EditorAd_get_IsVideoReady_m2967159589 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__IsVideoReady_9();
		return L_0;
	}
}
// System.Boolean SA_EditorAd::get_IsVideoLoading()
extern "C"  bool SA_EditorAd_get_IsVideoLoading_m597033854 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__IsVideoLoading_7();
		return L_0;
	}
}
// System.Boolean SA_EditorAd::get_IsInterstitialReady()
extern "C"  bool SA_EditorAd_get_IsInterstitialReady_m4069673862 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__IsInterstitialReady_8();
		return L_0;
	}
}
// System.Boolean SA_EditorAd::get_IsInterstitialLoading()
extern "C"  bool SA_EditorAd_get_IsInterstitialLoading_m917384547 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__IsInterstitialLoading_6();
		return L_0;
	}
}
// System.Boolean SA_EditorAd::get_HasFill()
extern "C"  bool SA_EditorAd_get_HasFill_m4283363370 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Random_Range_m694320887(NULL /*static, unused*/, 1, ((int32_t)100), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		int32_t L_2 = __this->get__FillRate_10();
		if ((((int32_t)L_1) > ((int32_t)L_2)))
		{
			goto IL_0017;
		}
	}
	{
		return (bool)1;
	}

IL_0017:
	{
		return (bool)0;
	}
}
// System.Int32 SA_EditorAd::get_FillRate()
extern "C"  int32_t SA_EditorAd_get_FillRate_m2541661572 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__FillRate_10();
		return L_0;
	}
}
// SA_Ad_EditorUIController SA_EditorAd::get_EditorUI()
extern "C"  SA_Ad_EditorUIController_t4080214878 * SA_EditorAd_get_EditorUI_m1488010437 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	{
		SA_Ad_EditorUIController_t4080214878 * L_0 = __this->get__EditorUI_11();
		return L_0;
	}
}
// System.Void SA_EditorAd::OnVideoRequestComplete()
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t SA_EditorAd_OnVideoRequestComplete_m2394915786_MetadataUsageId;
extern "C"  void SA_EditorAd_OnVideoRequestComplete_m2394915786 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_OnVideoRequestComplete_m2394915786_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set__IsVideoLoading_7((bool)0);
		bool L_0 = SA_EditorAd_get_HasFill_m4283363370(__this, /*hidden argument*/NULL);
		__this->set__IsVideoReady_9(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_1 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnVideoLoadComplete_16();
		bool L_2 = __this->get__IsVideoReady_9();
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		return;
	}
}
// System.Void SA_EditorAd::OnInterstitialRequestComplete()
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t SA_EditorAd_OnInterstitialRequestComplete_m829538427_MetadataUsageId;
extern "C"  void SA_EditorAd_OnInterstitialRequestComplete_m829538427 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_OnInterstitialRequestComplete_m829538427_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set__IsInterstitialLoading_6((bool)0);
		bool L_0 = SA_EditorAd_get_HasFill_m4283363370(__this, /*hidden argument*/NULL);
		__this->set__IsInterstitialReady_8(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_1 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnInterstitialLoadComplete_13();
		bool L_2 = __this->get__IsInterstitialReady_8();
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		return;
	}
}
// System.Void SA_EditorAd::OnInterstitialFinished_UIEvent(System.Boolean)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t SA_EditorAd_OnInterstitialFinished_UIEvent_m3974037417_MetadataUsageId;
extern "C"  void SA_EditorAd_OnInterstitialFinished_UIEvent_m3974037417 (SA_EditorAd_t1410159287 * __this, bool ___IsRewarded0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_OnInterstitialFinished_UIEvent_m3974037417_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set__IsInterstitialReady_8((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnInterstitialFinished_12();
		bool L_1 = ___IsRewarded0;
		NullCheck(L_0);
		Action_1_Invoke_m3662000152(L_0, L_1, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		return;
	}
}
// System.Void SA_EditorAd::OnVideoFinished_UIEvent(System.Boolean)
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t SA_EditorAd_OnVideoFinished_UIEvent_m3652767796_MetadataUsageId;
extern "C"  void SA_EditorAd_OnVideoFinished_UIEvent_m3652767796 (SA_EditorAd_t1410159287 * __this, bool ___IsRewarded0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_OnVideoFinished_UIEvent_m3652767796_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set__IsVideoReady_9((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnVideoFinished_15();
		bool L_1 = ___IsRewarded0;
		NullCheck(L_0);
		Action_1_Invoke_m3662000152(L_0, L_1, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		return;
	}
}
// System.Void SA_EditorAd::OnInterstitialLeftApplication_UIEvent()
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_OnInterstitialLeftApplication_UIEvent_m582665065_MetadataUsageId;
extern "C"  void SA_EditorAd_OnInterstitialLeftApplication_UIEvent_m582665065 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_OnInterstitialLeftApplication_UIEvent_m582665065_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_t3226471752 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnInterstitialLeftApplication_14();
		NullCheck(L_0);
		Action_Invoke_m3801112262(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorAd::OnVideoLeftApplication_UIEvent()
extern Il2CppClass* SA_EditorAd_t1410159287_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorAd_OnVideoLeftApplication_UIEvent_m3753138856_MetadataUsageId;
extern "C"  void SA_EditorAd_OnVideoLeftApplication_UIEvent_m3753138856 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorAd_OnVideoLeftApplication_UIEvent_m3753138856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorAd_t1410159287_il2cpp_TypeInfo_var);
		Action_t3226471752 * L_0 = ((SA_EditorAd_t1410159287_StaticFields*)SA_EditorAd_t1410159287_il2cpp_TypeInfo_var->static_fields)->get_OnVideoLeftApplication_17();
		NullCheck(L_0);
		Action_Invoke_m3801112262(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorAd::<OnInterstitialFinished>m__85(System.Boolean)
extern "C"  void SA_EditorAd_U3COnInterstitialFinishedU3Em__85_m1726536410 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_EditorAd::<OnInterstitialLoadComplete>m__86(System.Boolean)
extern "C"  void SA_EditorAd_U3COnInterstitialLoadCompleteU3Em__86_m3467906502 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_EditorAd::<OnInterstitialLeftApplication>m__87()
extern "C"  void SA_EditorAd_U3COnInterstitialLeftApplicationU3Em__87_m2918659926 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_EditorAd::<OnVideoFinished>m__88(System.Boolean)
extern "C"  void SA_EditorAd_U3COnVideoFinishedU3Em__88_m143744916 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_EditorAd::<OnVideoLoadComplete>m__89(System.Boolean)
extern "C"  void SA_EditorAd_U3COnVideoLoadCompleteU3Em__89_m3518109402 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_EditorAd::<OnVideoLeftApplication>m__8A()
extern "C"  void SA_EditorAd_U3COnVideoLeftApplicationU3Em__8A_m1595797995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_EditorInApps::.ctor()
extern "C"  void SA_EditorInApps__ctor_m2551493084 (SA_EditorInApps_t4145855819 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorInApps::.cctor()
extern "C"  void SA_EditorInApps__cctor_m2469562007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_EditorInApps::ShowInAppPopup(System.String,System.String,System.String,System.Action`1<System.Boolean>)
extern Il2CppClass* SA_EditorInApps_t4145855819_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorInApps_ShowInAppPopup_m3255254618_MetadataUsageId;
extern "C"  void SA_EditorInApps_ShowInAppPopup_m3255254618 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___describtion1, String_t* ___price2, Action_1_t3627374100 * ___OnComplete3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorInApps_ShowInAppPopup_m3255254618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorInApps_t4145855819_il2cpp_TypeInfo_var);
		SA_InApps_EditorUIController_t628772056 * L_0 = SA_EditorInApps_get_EditorUI_m2498527461(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___title0;
		String_t* L_2 = ___describtion1;
		String_t* L_3 = ___price2;
		Action_1_t3627374100 * L_4 = ___OnComplete3;
		NullCheck(L_0);
		SA_InApps_EditorUIController_ShowInAppPopup_m1587370083(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// SA_InApps_EditorUIController SA_EditorInApps::get_EditorUI()
extern Il2CppClass* SA_EditorInApps_t4145855819_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorInApps_get_EditorUI_m2498527461_MetadataUsageId;
extern "C"  SA_InApps_EditorUIController_t628772056 * SA_EditorInApps_get_EditorUI_m2498527461 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorInApps_get_EditorUI_m2498527461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorInApps_t4145855819_il2cpp_TypeInfo_var);
		SA_InApps_EditorUIController_t628772056 * L_0 = ((SA_EditorInApps_t4145855819_StaticFields*)SA_EditorInApps_t4145855819_il2cpp_TypeInfo_var->static_fields)->get__EditorUI_0();
		return L_0;
	}
}
// System.Void SA_EditorNotifications::.ctor()
extern "C"  void SA_EditorNotifications__ctor_m992071291 (SA_EditorNotifications_t2173147004 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorNotifications::.cctor()
extern "C"  void SA_EditorNotifications__cctor_m4219903688 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_EditorNotifications::ShowNotification(System.String,System.String,SA_EditorNotificationType)
extern Il2CppClass* SA_EditorNotifications_t2173147004_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorNotifications_ShowNotification_m3379309726_MetadataUsageId;
extern "C"  void SA_EditorNotifications_ShowNotification_m3379309726 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, int32_t ___type2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorNotifications_ShowNotification_m3379309726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = SA_EditorTesting_get_IsInsideEditor_m216790077(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorNotifications_t2173147004_il2cpp_TypeInfo_var);
		SA_Notifications_EditorUIController_t749267495 * L_1 = SA_EditorNotifications_get_EditorUI_m428117509(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = ___title0;
		String_t* L_3 = ___message1;
		int32_t L_4 = ___type2;
		NullCheck(L_1);
		SA_Notifications_EditorUIController_ShowNotification_m2600411583(L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// SA_Notifications_EditorUIController SA_EditorNotifications::get_EditorUI()
extern Il2CppClass* SA_EditorNotifications_t2173147004_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorNotifications_get_EditorUI_m428117509_MetadataUsageId;
extern "C"  SA_Notifications_EditorUIController_t749267495 * SA_EditorNotifications_get_EditorUI_m428117509 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorNotifications_get_EditorUI_m428117509_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorNotifications_t2173147004_il2cpp_TypeInfo_var);
		SA_Notifications_EditorUIController_t749267495 * L_0 = ((SA_EditorNotifications_t2173147004_StaticFields*)SA_EditorNotifications_t2173147004_il2cpp_TypeInfo_var->static_fields)->get__EditorUI_0();
		return L_0;
	}
}
// System.Boolean SA_EditorTesting::get_IsInsideEditor()
extern "C"  bool SA_EditorTesting_get_IsInsideEditor_m216790077 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)7)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}

IL_0017:
	{
		V_0 = (bool)1;
	}

IL_0019:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean SA_EditorTesting::HasFill(System.Single)
extern "C"  bool SA_EditorTesting_HasFill_m1763252407 (Il2CppObject * __this /* static, unused */, float ___fillRate0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Random_Range_m694320887(NULL /*static, unused*/, 1, ((int32_t)100), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		float L_2 = ___fillRate0;
		if ((!(((float)(((float)((float)L_1)))) <= ((float)L_2))))
		{
			goto IL_0013;
		}
	}
	{
		return (bool)1;
	}

IL_0013:
	{
		return (bool)0;
	}
}
// System.Void SA_EditorTesting::CheckForEventSystem()
extern const Il2CppType* EventSystem_t3466835263_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* EventSystem_t3466835263_il2cpp_TypeInfo_var;
extern const uint32_t SA_EditorTesting_CheckForEventSystem_m1303346307_MetadataUsageId;
extern "C"  void SA_EditorTesting_CheckForEventSystem_m1303346307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorTesting_CheckForEventSystem_m1303346307_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EventSystem_t3466835263 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(EventSystem_t3466835263_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_1 = Object_FindObjectOfType_m2330404063(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((EventSystem_t3466835263 *)CastclassClass(L_1, EventSystem_t3466835263_il2cpp_TypeInfo_var));
		EventSystem_t3466835263 * L_2 = V_0;
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}

IL_0021:
	{
		return;
	}
}
// System.Void SA_EditorTestingSceneController::.ctor()
extern "C"  void SA_EditorTestingSceneController__ctor_m3020214257 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorTestingSceneController::LoadInterstitial()
extern Il2CppClass* Singleton_1_t1892724392_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_Instance_m74131242_MethodInfo_var;
extern const uint32_t SA_EditorTestingSceneController_LoadInterstitial_m1662098405_MetadataUsageId;
extern "C"  void SA_EditorTestingSceneController_LoadInterstitial_m1662098405 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorTestingSceneController_LoadInterstitial_m1662098405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1892724392_il2cpp_TypeInfo_var);
		SA_EditorAd_t1410159287 * L_0 = Singleton_1_get_Instance_m74131242(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m74131242_MethodInfo_var);
		NullCheck(L_0);
		SA_EditorAd_LoadInterstitial_m92056142(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorTestingSceneController::ShowInterstitial()
extern Il2CppClass* Singleton_1_t1892724392_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_Instance_m74131242_MethodInfo_var;
extern const uint32_t SA_EditorTestingSceneController_ShowInterstitial_m3136271942_MetadataUsageId;
extern "C"  void SA_EditorTestingSceneController_ShowInterstitial_m3136271942 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorTestingSceneController_ShowInterstitial_m3136271942_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1892724392_il2cpp_TypeInfo_var);
		SA_EditorAd_t1410159287 * L_0 = Singleton_1_get_Instance_m74131242(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m74131242_MethodInfo_var);
		NullCheck(L_0);
		SA_EditorAd_ShowInterstitial_m668797125(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorTestingSceneController::LoadVideo()
extern Il2CppClass* Singleton_1_t1892724392_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_Instance_m74131242_MethodInfo_var;
extern const uint32_t SA_EditorTestingSceneController_LoadVideo_m2371520130_MetadataUsageId;
extern "C"  void SA_EditorTestingSceneController_LoadVideo_m2371520130 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorTestingSceneController_LoadVideo_m2371520130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1892724392_il2cpp_TypeInfo_var);
		SA_EditorAd_t1410159287 * L_0 = Singleton_1_get_Instance_m74131242(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m74131242_MethodInfo_var);
		NullCheck(L_0);
		SA_EditorAd_LoadVideo_m534603967(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorTestingSceneController::ShowVideo()
extern Il2CppClass* Singleton_1_t1892724392_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_Instance_m74131242_MethodInfo_var;
extern const uint32_t SA_EditorTestingSceneController_ShowVideo_m4077472515_MetadataUsageId;
extern "C"  void SA_EditorTestingSceneController_ShowVideo_m4077472515 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorTestingSceneController_ShowVideo_m4077472515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1892724392_il2cpp_TypeInfo_var);
		SA_EditorAd_t1410159287 * L_0 = Singleton_1_get_Instance_m74131242(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m74131242_MethodInfo_var);
		NullCheck(L_0);
		SA_EditorAd_ShowVideo_m434293846(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorTestingSceneController::Show_Notifications()
extern Il2CppClass* SA_EditorNotifications_t2173147004_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3423762534;
extern Il2CppCodeGenString* _stringLiteral2546486763;
extern const uint32_t SA_EditorTestingSceneController_Show_Notifications_m302334907_MetadataUsageId;
extern "C"  void SA_EditorTestingSceneController_Show_Notifications_m302334907 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorTestingSceneController_Show_Notifications_m302334907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorNotifications_t2173147004_il2cpp_TypeInfo_var);
		SA_EditorNotifications_ShowNotification_m3379309726(NULL /*static, unused*/, _stringLiteral3423762534, _stringLiteral2546486763, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorTestingSceneController::Show_A_Notifications()
extern Il2CppClass* SA_EditorNotifications_t2173147004_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1066644319;
extern Il2CppCodeGenString* _stringLiteral2546486763;
extern const uint32_t SA_EditorTestingSceneController_Show_A_Notifications_m3294551617_MetadataUsageId;
extern "C"  void SA_EditorTestingSceneController_Show_A_Notifications_m3294551617 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorTestingSceneController_Show_A_Notifications_m3294551617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorNotifications_t2173147004_il2cpp_TypeInfo_var);
		SA_EditorNotifications_ShowNotification_m3379309726(NULL /*static, unused*/, _stringLiteral1066644319, _stringLiteral2546486763, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorTestingSceneController::Show_L_Notifications()
extern Il2CppClass* SA_EditorNotifications_t2173147004_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3257374625;
extern Il2CppCodeGenString* _stringLiteral2546486763;
extern const uint32_t SA_EditorTestingSceneController_Show_L_Notifications_m1764659148_MetadataUsageId;
extern "C"  void SA_EditorTestingSceneController_Show_L_Notifications_m1764659148 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorTestingSceneController_Show_L_Notifications_m1764659148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorNotifications_t2173147004_il2cpp_TypeInfo_var);
		SA_EditorNotifications_ShowNotification_m3379309726(NULL /*static, unused*/, _stringLiteral3257374625, _stringLiteral2546486763, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorTestingSceneController::Show_E_Notifications()
extern Il2CppClass* SA_EditorNotifications_t2173147004_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral22442200;
extern Il2CppCodeGenString* _stringLiteral2546486763;
extern const uint32_t SA_EditorTestingSceneController_Show_E_Notifications_m4283359045_MetadataUsageId;
extern "C"  void SA_EditorTestingSceneController_Show_E_Notifications_m4283359045 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorTestingSceneController_Show_E_Notifications_m4283359045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorNotifications_t2173147004_il2cpp_TypeInfo_var);
		SA_EditorNotifications_ShowNotification_m3379309726(NULL /*static, unused*/, _stringLiteral22442200, _stringLiteral2546486763, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorTestingSceneController::Show_InApp_Popup()
extern Il2CppClass* SA_EditorInApps_t4145855819_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral34031111;
extern Il2CppCodeGenString* _stringLiteral3915845327;
extern Il2CppCodeGenString* _stringLiteral1540360452;
extern const uint32_t SA_EditorTestingSceneController_Show_InApp_Popup_m224412576_MetadataUsageId;
extern "C"  void SA_EditorTestingSceneController_Show_InApp_Popup_m224412576 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorTestingSceneController_Show_InApp_Popup_m224412576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_EditorInApps_t4145855819_il2cpp_TypeInfo_var);
		SA_EditorInApps_ShowInAppPopup_m3255254618(NULL /*static, unused*/, _stringLiteral34031111, _stringLiteral3915845327, _stringLiteral1540360452, (Action_1_t3627374100 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_EditorTestingSceneController::FixedUpdate()
extern Il2CppClass* Singleton_1_t1892724392_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_Instance_m74131242_MethodInfo_var;
extern const uint32_t SA_EditorTestingSceneController_FixedUpdate_m3521350690_MetadataUsageId;
extern "C"  void SA_EditorTestingSceneController_FixedUpdate_m3521350690 (SA_EditorTestingSceneController_t2020162592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_EditorTestingSceneController_FixedUpdate_m3521350690_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1892724392_il2cpp_TypeInfo_var);
		SA_EditorAd_t1410159287 * L_0 = Singleton_1_get_Instance_m74131242(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m74131242_MethodInfo_var);
		NullCheck(L_0);
		bool L_1 = SA_EditorAd_get_IsInterstitialReady_m4069673862(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		Button_t2872111280 * L_2 = __this->get_ShowInterstitial_Button_2();
		NullCheck(L_2);
		Selectable_set_interactable_m63718297(L_2, (bool)1, /*hidden argument*/NULL);
		goto IL_002c;
	}

IL_0020:
	{
		Button_t2872111280 * L_3 = __this->get_ShowInterstitial_Button_2();
		NullCheck(L_3);
		Selectable_set_interactable_m63718297(L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1892724392_il2cpp_TypeInfo_var);
		SA_EditorAd_t1410159287 * L_4 = Singleton_1_get_Instance_m74131242(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m74131242_MethodInfo_var);
		NullCheck(L_4);
		bool L_5 = SA_EditorAd_get_IsVideoReady_m2967159589(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004c;
		}
	}
	{
		Button_t2872111280 * L_6 = __this->get_ShowInterstitial_Video_3();
		NullCheck(L_6);
		Selectable_set_interactable_m63718297(L_6, (bool)1, /*hidden argument*/NULL);
		goto IL_0058;
	}

IL_004c:
	{
		Button_t2872111280 * L_7 = __this->get_ShowInterstitial_Video_3();
		NullCheck(L_7);
		Selectable_set_interactable_m63718297(L_7, (bool)0, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void SA_InApps_EditorUIController::.ctor()
extern "C"  void SA_InApps_EditorUIController__ctor_m3019146543 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_InApps_EditorUIController::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var;
extern const uint32_t SA_InApps_EditorUIController_Awake_m2392438458_MetadataUsageId;
extern "C"  void SA_InApps_EditorUIController_Awake_m2392438458 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_InApps_EditorUIController_Awake_m2392438458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Canvas_t209405766 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Canvas_t209405766 * L_1 = Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var);
		V_0 = L_1;
		Canvas_t209405766 * L_2 = V_0;
		NullCheck(L_2);
		Canvas_set_sortingOrder_m2922819993(L_2, ((int32_t)10003), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_InApps_EditorUIController::ShowInAppPopup(System.String,System.String,System.String,System.Action`1<System.Boolean>)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const MethodInfo* SA_InApps_EditorUIController_HandleOnInTweenComplete_m2010508043_MethodInfo_var;
extern const uint32_t SA_InApps_EditorUIController_ShowInAppPopup_m1587370083_MetadataUsageId;
extern "C"  void SA_InApps_EditorUIController_ShowInAppPopup_m1587370083 (SA_InApps_EditorUIController_t628772056 * __this, String_t* ___title0, String_t* ___describtion1, String_t* ___price2, Action_1_t3627374100 * ___OnComplete3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_InApps_EditorUIController_ShowInAppPopup_m1587370083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ValuesTween_t4181447589 * L_0 = __this->get__CurrentTween_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		ValuesTween_t4181447589 * L_2 = __this->get__CurrentTween_8();
		NullCheck(L_2);
		ValuesTween_Stop_m1272304863(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		ValuesTween_t4181447589 * L_3 = __this->get__FaderTween_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		ValuesTween_t4181447589 * L_5 = __this->get__FaderTween_9();
		NullCheck(L_5);
		ValuesTween_Stop_m1272304863(L_5, /*hidden argument*/NULL);
	}

IL_0038:
	{
		Action_1_t3627374100 * L_6 = ___OnComplete3;
		__this->set__OnComplete_10(L_6);
		Text_t356221433 * L_7 = __this->get_Title_2();
		String_t* L_8 = ___title0;
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_8);
		Text_t356221433 * L_9 = __this->get_Describtion_3();
		String_t* L_10 = ___describtion1;
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, L_10);
		Text_t356221433 * L_11 = __this->get_Price_4();
		String_t* L_12 = ___price2;
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_12);
		SA_InApps_EditorUIController_Animate_m1489603625(__this, (150.0f), (-300.0f), ((int32_t)27), /*hidden argument*/NULL);
		ValuesTween_t4181447589 * L_13 = __this->get__CurrentTween_8();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)SA_InApps_EditorUIController_HandleOnInTweenComplete_m2010508043_MethodInfo_var);
		Action_t3226471752 * L_15 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		ValuesTween_add_OnComplete_m205323862(L_13, L_15, /*hidden argument*/NULL);
		SA_InApps_EditorUIController_FadeIn_m978052472(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_InApps_EditorUIController::Close()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const MethodInfo* SA_InApps_EditorUIController_HandleOnOutTweenComplete_m760268178_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t SA_InApps_EditorUIController_Close_m3806835033_MetadataUsageId;
extern "C"  void SA_InApps_EditorUIController_Close_m3806835033 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_InApps_EditorUIController_Close_m3806835033_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ValuesTween_t4181447589 * L_0 = __this->get__CurrentTween_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		ValuesTween_t4181447589 * L_2 = __this->get__CurrentTween_8();
		NullCheck(L_2);
		ValuesTween_Stop_m1272304863(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		ValuesTween_t4181447589 * L_3 = __this->get__FaderTween_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		ValuesTween_t4181447589 * L_5 = __this->get__FaderTween_9();
		NullCheck(L_5);
		ValuesTween_Stop_m1272304863(L_5, /*hidden argument*/NULL);
	}

IL_0038:
	{
		SA_InApps_EditorUIController_Animate_m1489603625(__this, (-300.0f), (150.0f), ((int32_t)26), /*hidden argument*/NULL);
		ValuesTween_t4181447589 * L_6 = __this->get__CurrentTween_8();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)SA_InApps_EditorUIController_HandleOnOutTweenComplete_m760268178_MethodInfo_var);
		Action_t3226471752 * L_8 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_8, __this, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		ValuesTween_add_OnComplete_m205323862(L_6, L_8, /*hidden argument*/NULL);
		SA_InApps_EditorUIController_FadeOut_m1069073623(__this, /*hidden argument*/NULL);
		Action_1_t3627374100 * L_9 = __this->get__OnComplete_10();
		if (!L_9)
		{
			goto IL_008f;
		}
	}
	{
		Action_1_t3627374100 * L_10 = __this->get__OnComplete_10();
		Toggle_t3976754468 * L_11 = __this->get_IsSuccsesPurchase_5();
		NullCheck(L_11);
		bool L_12 = Toggle_get_isOn_m366838229(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Action_1_Invoke_m3662000152(L_10, L_12, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		__this->set__OnComplete_10((Action_1_t3627374100 *)NULL);
	}

IL_008f:
	{
		return;
	}
}
// System.Void SA_InApps_EditorUIController::HandleOnInTweenComplete()
extern "C"  void SA_InApps_EditorUIController_HandleOnInTweenComplete_m2010508043 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method)
{
	{
		__this->set__CurrentTween_8((ValuesTween_t4181447589 *)NULL);
		return;
	}
}
// System.Void SA_InApps_EditorUIController::HandleOnOutTweenComplete()
extern "C"  void SA_InApps_EditorUIController_HandleOnOutTweenComplete_m760268178 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method)
{
	{
		__this->set__CurrentTween_8((ValuesTween_t4181447589 *)NULL);
		return;
	}
}
// System.Void SA_InApps_EditorUIController::HandleOnValueChanged(System.Single)
extern "C"  void SA_InApps_EditorUIController_HandleOnValueChanged_m2963842966 (SA_InApps_EditorUIController_t628772056 * __this, float ___pos0, const MethodInfo* method)
{
	{
		SA_UIHightDependence_t2303649696 * L_0 = __this->get_HightDependence_7();
		NullCheck(L_0);
		Rect_t3681755626 * L_1 = L_0->get_address_of_InitialRect_5();
		float L_2 = ___pos0;
		Rect_set_y_m4294916608(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_InApps_EditorUIController::HandleOnFadeValueChanged(System.Single)
extern "C"  void SA_InApps_EditorUIController_HandleOnFadeValueChanged_m3561002028 (SA_InApps_EditorUIController_t628772056 * __this, float ___a0, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t2020392075  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Image_t2042527209 * L_0 = __this->get_Fader_6();
		Image_t2042527209 * L_1 = __this->get_Fader_6();
		NullCheck(L_1);
		Color_t2020392075  L_2 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_1);
		V_0 = L_2;
		float L_3 = (&V_0)->get_r_0();
		Image_t2042527209 * L_4 = __this->get_Fader_6();
		NullCheck(L_4);
		Color_t2020392075  L_5 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_4);
		V_1 = L_5;
		float L_6 = (&V_1)->get_g_1();
		Image_t2042527209 * L_7 = __this->get_Fader_6();
		NullCheck(L_7);
		Color_t2020392075  L_8 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_7);
		V_2 = L_8;
		float L_9 = (&V_2)->get_b_2();
		float L_10 = ___a0;
		Color_t2020392075  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m1909920690(&L_11, L_3, L_6, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_11);
		return;
	}
}
// System.Void SA_InApps_EditorUIController::HandleFadeComplete()
extern "C"  void SA_InApps_EditorUIController_HandleFadeComplete_m1732026102 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_Fader_6();
		NullCheck(L_0);
		Behaviour_set_enabled_m1796096907(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_InApps_EditorUIController::FadeIn()
extern Il2CppClass* Action_1_t1878309314_il2cpp_TypeInfo_var;
extern const MethodInfo* SA_InApps_EditorUIController_HandleOnFadeValueChanged_m3561002028_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1373098066_MethodInfo_var;
extern const uint32_t SA_InApps_EditorUIController_FadeIn_m978052472_MetadataUsageId;
extern "C"  void SA_InApps_EditorUIController_FadeIn_m978052472 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_InApps_EditorUIController_FadeIn_m978052472_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Image_t2042527209 * L_0 = __this->get_Fader_6();
		NullCheck(L_0);
		Behaviour_set_enabled_m1796096907(L_0, (bool)1, /*hidden argument*/NULL);
		ValuesTween_t4181447589 * L_1 = ValuesTween_Create_m3355431774(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__FaderTween_9(L_1);
		ValuesTween_t4181447589 * L_2 = __this->get__FaderTween_9();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)SA_InApps_EditorUIController_HandleOnFadeValueChanged_m3561002028_MethodInfo_var);
		Action_1_t1878309314 * L_4 = (Action_1_t1878309314 *)il2cpp_codegen_object_new(Action_1_t1878309314_il2cpp_TypeInfo_var);
		Action_1__ctor_m1373098066(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m1373098066_MethodInfo_var);
		NullCheck(L_2);
		ValuesTween_add_OnValueChanged_m3920362804(L_2, L_4, /*hidden argument*/NULL);
		ValuesTween_t4181447589 * L_5 = __this->get__FaderTween_9();
		NullCheck(L_5);
		ValuesTween_ValueTo_m3388519367(L_5, (0.0f), (0.5f), (0.5f), ((int32_t)21), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_InApps_EditorUIController::FadeOut()
extern Il2CppClass* Action_1_t1878309314_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const MethodInfo* SA_InApps_EditorUIController_HandleOnFadeValueChanged_m3561002028_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1373098066_MethodInfo_var;
extern const MethodInfo* SA_InApps_EditorUIController_HandleFadeComplete_m1732026102_MethodInfo_var;
extern const uint32_t SA_InApps_EditorUIController_FadeOut_m1069073623_MetadataUsageId;
extern "C"  void SA_InApps_EditorUIController_FadeOut_m1069073623 (SA_InApps_EditorUIController_t628772056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_InApps_EditorUIController_FadeOut_m1069073623_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ValuesTween_t4181447589 * L_0 = ValuesTween_Create_m3355431774(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__FaderTween_9(L_0);
		ValuesTween_t4181447589 * L_1 = __this->get__FaderTween_9();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)SA_InApps_EditorUIController_HandleOnFadeValueChanged_m3561002028_MethodInfo_var);
		Action_1_t1878309314 * L_3 = (Action_1_t1878309314 *)il2cpp_codegen_object_new(Action_1_t1878309314_il2cpp_TypeInfo_var);
		Action_1__ctor_m1373098066(L_3, __this, L_2, /*hidden argument*/Action_1__ctor_m1373098066_MethodInfo_var);
		NullCheck(L_1);
		ValuesTween_add_OnValueChanged_m3920362804(L_1, L_3, /*hidden argument*/NULL);
		ValuesTween_t4181447589 * L_4 = __this->get__FaderTween_9();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)SA_InApps_EditorUIController_HandleFadeComplete_m1732026102_MethodInfo_var);
		Action_t3226471752 * L_6 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		ValuesTween_add_OnComplete_m205323862(L_4, L_6, /*hidden argument*/NULL);
		ValuesTween_t4181447589 * L_7 = __this->get__FaderTween_9();
		NullCheck(L_7);
		ValuesTween_ValueTo_m3388519367(L_7, (0.5f), (0.0f), (0.5f), ((int32_t)21), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_InApps_EditorUIController::Animate(System.Single,System.Single,SA.Common.Animation.EaseType)
extern Il2CppClass* Action_1_t1878309314_il2cpp_TypeInfo_var;
extern const MethodInfo* SA_InApps_EditorUIController_HandleOnValueChanged_m2963842966_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1373098066_MethodInfo_var;
extern const uint32_t SA_InApps_EditorUIController_Animate_m1489603625_MetadataUsageId;
extern "C"  void SA_InApps_EditorUIController_Animate_m1489603625 (SA_InApps_EditorUIController_t628772056 * __this, float ___from0, float ___to1, int32_t ___easeType2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_InApps_EditorUIController_Animate_m1489603625_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ValuesTween_t4181447589 * L_0 = ValuesTween_Create_m3355431774(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__CurrentTween_8(L_0);
		ValuesTween_t4181447589 * L_1 = __this->get__CurrentTween_8();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)SA_InApps_EditorUIController_HandleOnValueChanged_m2963842966_MethodInfo_var);
		Action_1_t1878309314 * L_3 = (Action_1_t1878309314 *)il2cpp_codegen_object_new(Action_1_t1878309314_il2cpp_TypeInfo_var);
		Action_1__ctor_m1373098066(L_3, __this, L_2, /*hidden argument*/Action_1__ctor_m1373098066_MethodInfo_var);
		NullCheck(L_1);
		ValuesTween_add_OnValueChanged_m3920362804(L_1, L_3, /*hidden argument*/NULL);
		ValuesTween_t4181447589 * L_4 = __this->get__CurrentTween_8();
		float L_5 = ___from0;
		float L_6 = ___to1;
		int32_t L_7 = ___easeType2;
		NullCheck(L_4);
		ValuesTween_ValueTo_m3388519367(L_4, L_5, L_6, (0.5f), L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Notifications_EditorUIController::.ctor()
extern "C"  void SA_Notifications_EditorUIController__ctor_m943839576 (SA_Notifications_EditorUIController_t749267495 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Notifications_EditorUIController::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var;
extern const uint32_t SA_Notifications_EditorUIController_Awake_m2339683289_MetadataUsageId;
extern "C"  void SA_Notifications_EditorUIController_Awake_m2339683289 (SA_Notifications_EditorUIController_t749267495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Notifications_EditorUIController_Awake_m2339683289_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Canvas_t209405766 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SA_EditorTesting_CheckForEventSystem_m1303346307(NULL /*static, unused*/, /*hidden argument*/NULL);
		Canvas_t209405766 * L_1 = Component_GetComponent_TisCanvas_t209405766_m3588040191(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t209405766_m3588040191_MethodInfo_var);
		V_0 = L_1;
		Canvas_t209405766 * L_2 = V_0;
		NullCheck(L_2);
		Canvas_set_sortingOrder_m2922819993(L_2, ((int32_t)10001), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Notifications_EditorUIController::ShowNotification(System.String,System.String,SA_EditorNotificationType)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const MethodInfo* SA_Notifications_EditorUIController_HandleOnInTweenComplete_m1988608916_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3842761509;
extern const uint32_t SA_Notifications_EditorUIController_ShowNotification_m2600411583_MetadataUsageId;
extern "C"  void SA_Notifications_EditorUIController_ShowNotification_m2600411583 (SA_Notifications_EditorUIController_t749267495 * __this, String_t* ___title0, String_t* ___message1, int32_t ___type2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Notifications_EditorUIController_ShowNotification_m2600411583_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Image_t2042527209 * V_0 = NULL;
	ImageU5BU5D_t590162004* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		ValuesTween_t4181447589 * L_0 = __this->get__CurrentTween_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		ValuesTween_t4181447589 * L_2 = __this->get__CurrentTween_6();
		NullCheck(L_2);
		ValuesTween_Stop_m1272304863(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		MonoBehaviour_CancelInvoke_m2508161963(__this, _stringLiteral3842761509, /*hidden argument*/NULL);
		Text_t356221433 * L_3 = __this->get_Title_2();
		String_t* L_4 = ___title0;
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_4);
		Text_t356221433 * L_5 = __this->get_Message_3();
		String_t* L_6 = ___message1;
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_6);
		ImageU5BU5D_t590162004* L_7 = __this->get_Icons_4();
		V_1 = L_7;
		V_2 = 0;
		goto IL_0061;
	}

IL_004d:
	{
		ImageU5BU5D_t590162004* L_8 = V_1;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		Image_t2042527209 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_0 = L_11;
		Image_t2042527209 * L_12 = V_0;
		NullCheck(L_12);
		GameObject_t1756533147 * L_13 = Component_get_gameObject_m3105766835(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		GameObject_SetActive_m2887581199(L_13, (bool)0, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_15 = V_2;
		ImageU5BU5D_t590162004* L_16 = V_1;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_17 = ___type2;
		V_3 = L_17;
		int32_t L_18 = V_3;
		if (L_18 == 0)
		{
			goto IL_00cf;
		}
		if (L_18 == 1)
		{
			goto IL_0087;
		}
		if (L_18 == 2)
		{
			goto IL_00b7;
		}
		if (L_18 == 3)
		{
			goto IL_009f;
		}
	}
	{
		goto IL_00e7;
	}

IL_0087:
	{
		ImageU5BU5D_t590162004* L_19 = __this->get_Icons_4();
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 0);
		int32_t L_20 = 0;
		Image_t2042527209 * L_21 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		GameObject_t1756533147 * L_22 = Component_get_gameObject_m3105766835(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		GameObject_SetActive_m2887581199(L_22, (bool)1, /*hidden argument*/NULL);
		goto IL_00e7;
	}

IL_009f:
	{
		ImageU5BU5D_t590162004* L_23 = __this->get_Icons_4();
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 1);
		int32_t L_24 = 1;
		Image_t2042527209 * L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		GameObject_t1756533147 * L_26 = Component_get_gameObject_m3105766835(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		GameObject_SetActive_m2887581199(L_26, (bool)1, /*hidden argument*/NULL);
		goto IL_00e7;
	}

IL_00b7:
	{
		ImageU5BU5D_t590162004* L_27 = __this->get_Icons_4();
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 2);
		int32_t L_28 = 2;
		Image_t2042527209 * L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		GameObject_t1756533147 * L_30 = Component_get_gameObject_m3105766835(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		GameObject_SetActive_m2887581199(L_30, (bool)1, /*hidden argument*/NULL);
		goto IL_00e7;
	}

IL_00cf:
	{
		ImageU5BU5D_t590162004* L_31 = __this->get_Icons_4();
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 3);
		int32_t L_32 = 3;
		Image_t2042527209 * L_33 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_33);
		GameObject_t1756533147 * L_34 = Component_get_gameObject_m3105766835(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		GameObject_SetActive_m2887581199(L_34, (bool)1, /*hidden argument*/NULL);
		goto IL_00e7;
	}

IL_00e7:
	{
		SA_Notifications_EditorUIController_Animate_m2937568428(__this, (52.0f), (-52.0f), ((int32_t)27), /*hidden argument*/NULL);
		ValuesTween_t4181447589 * L_35 = __this->get__CurrentTween_6();
		IntPtr_t L_36;
		L_36.set_m_value_0((void*)(void*)SA_Notifications_EditorUIController_HandleOnInTweenComplete_m1988608916_MethodInfo_var);
		Action_t3226471752 * L_37 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_37, __this, L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		ValuesTween_add_OnComplete_m205323862(L_35, L_37, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Notifications_EditorUIController::HandleOnInTweenComplete()
extern Il2CppCodeGenString* _stringLiteral3842761509;
extern const uint32_t SA_Notifications_EditorUIController_HandleOnInTweenComplete_m1988608916_MetadataUsageId;
extern "C"  void SA_Notifications_EditorUIController_HandleOnInTweenComplete_m1988608916 (SA_Notifications_EditorUIController_t749267495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Notifications_EditorUIController_HandleOnInTweenComplete_m1988608916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set__CurrentTween_6((ValuesTween_t4181447589 *)NULL);
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral3842761509, (2.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Notifications_EditorUIController::NotificationDelayComplete()
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const MethodInfo* SA_Notifications_EditorUIController_HandleOnOutTweenComplete_m471538893_MethodInfo_var;
extern const uint32_t SA_Notifications_EditorUIController_NotificationDelayComplete_m884402905_MetadataUsageId;
extern "C"  void SA_Notifications_EditorUIController_NotificationDelayComplete_m884402905 (SA_Notifications_EditorUIController_t749267495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Notifications_EditorUIController_NotificationDelayComplete_m884402905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SA_Notifications_EditorUIController_Animate_m2937568428(__this, (-52.0f), (58.0f), ((int32_t)26), /*hidden argument*/NULL);
		ValuesTween_t4181447589 * L_0 = __this->get__CurrentTween_6();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)SA_Notifications_EditorUIController_HandleOnOutTweenComplete_m471538893_MethodInfo_var);
		Action_t3226471752 * L_2 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_2, __this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		ValuesTween_add_OnComplete_m205323862(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Notifications_EditorUIController::HandleOnOutTweenComplete()
extern "C"  void SA_Notifications_EditorUIController_HandleOnOutTweenComplete_m471538893 (SA_Notifications_EditorUIController_t749267495 * __this, const MethodInfo* method)
{
	{
		__this->set__CurrentTween_6((ValuesTween_t4181447589 *)NULL);
		return;
	}
}
// System.Void SA_Notifications_EditorUIController::HandleOnValueChanged(System.Single)
extern "C"  void SA_Notifications_EditorUIController_HandleOnValueChanged_m2929388259 (SA_Notifications_EditorUIController_t749267495 * __this, float ___pos0, const MethodInfo* method)
{
	{
		SA_UIHightDependence_t2303649696 * L_0 = __this->get_HightDependence_5();
		NullCheck(L_0);
		Rect_t3681755626 * L_1 = L_0->get_address_of_InitialRect_5();
		float L_2 = ___pos0;
		Rect_set_y_m4294916608(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Notifications_EditorUIController::Animate(System.Single,System.Single,SA.Common.Animation.EaseType)
extern Il2CppClass* Action_1_t1878309314_il2cpp_TypeInfo_var;
extern const MethodInfo* SA_Notifications_EditorUIController_HandleOnValueChanged_m2929388259_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1373098066_MethodInfo_var;
extern const uint32_t SA_Notifications_EditorUIController_Animate_m2937568428_MetadataUsageId;
extern "C"  void SA_Notifications_EditorUIController_Animate_m2937568428 (SA_Notifications_EditorUIController_t749267495 * __this, float ___from0, float ___to1, int32_t ___easeType2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Notifications_EditorUIController_Animate_m2937568428_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ValuesTween_t4181447589 * L_0 = ValuesTween_Create_m3355431774(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__CurrentTween_6(L_0);
		ValuesTween_t4181447589 * L_1 = __this->get__CurrentTween_6();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)SA_Notifications_EditorUIController_HandleOnValueChanged_m2929388259_MethodInfo_var);
		Action_1_t1878309314 * L_3 = (Action_1_t1878309314 *)il2cpp_codegen_object_new(Action_1_t1878309314_il2cpp_TypeInfo_var);
		Action_1__ctor_m1373098066(L_3, __this, L_2, /*hidden argument*/Action_1__ctor_m1373098066_MethodInfo_var);
		NullCheck(L_1);
		ValuesTween_add_OnValueChanged_m3920362804(L_1, L_3, /*hidden argument*/NULL);
		ValuesTween_t4181447589 * L_4 = __this->get__CurrentTween_6();
		float L_5 = ___from0;
		float L_6 = ___to1;
		int32_t L_7 = ___easeType2;
		NullCheck(L_4);
		ValuesTween_ValueTo_m3388519367(L_4, L_5, L_6, (0.5f), L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_UIHightDependence::.ctor()
extern Il2CppClass* Rect_t3681755626_il2cpp_TypeInfo_var;
extern const uint32_t SA_UIHightDependence__ctor_m878015997_MetadataUsageId;
extern "C"  void SA_UIHightDependence__ctor_m878015997 (SA_UIHightDependence_t2303649696 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_UIHightDependence__ctor_m878015997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (Rect_t3681755626_il2cpp_TypeInfo_var, (&V_0));
		Rect_t3681755626  L_0 = V_0;
		__this->set_InitialRect_5(L_0);
		Initobj (Rect_t3681755626_il2cpp_TypeInfo_var, (&V_1));
		Rect_t3681755626  L_1 = V_1;
		__this->set_InitialScreen_6(L_1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_UIHightDependence::Awake()
extern "C"  void SA_UIHightDependence_Awake_m2762991386 (SA_UIHightDependence_t2303649696 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		SA_UIHightDependence_ApplyTransformation_m76535612(__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		return;
	}
}
// System.Void SA_UIHightDependence::Update()
extern "C"  void SA_UIHightDependence_Update_m533303276 (SA_UIHightDependence_t2303649696 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_00b5;
		}
	}
	{
		bool L_1 = __this->get_KeepRatioInEdiotr_3();
		if (L_1)
		{
			goto IL_009e;
		}
	}
	{
		RectTransform_t3349966182 * L_2 = SA_UIHightDependence_get_rect_m631336958(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector2_t2243707579  L_3 = RectTransform_get_anchoredPosition_m3570822376(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&V_0)->get_x_1();
		RectTransform_t3349966182 * L_5 = SA_UIHightDependence_get_rect_m631336958(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector2_t2243707579  L_6 = RectTransform_get_anchoredPosition_m3570822376(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_y_2();
		RectTransform_t3349966182 * L_8 = SA_UIHightDependence_get_rect_m631336958(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Rect_t3681755626  L_9 = RectTransform_get_rect_m73954734(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = Rect_get_width_m1138015702((&V_2), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_11 = SA_UIHightDependence_get_rect_m631336958(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Rect_t3681755626  L_12 = RectTransform_get_rect_m73954734(L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		float L_13 = Rect_get_height_m3128694305((&V_3), /*hidden argument*/NULL);
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, L_4, L_7, L_10, L_13, /*hidden argument*/NULL);
		__this->set_InitialRect_5(L_14);
		int32_t L_15 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_16 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (0.0f), (0.0f), (((float)((float)L_15))), (((float)((float)L_16))), /*hidden argument*/NULL);
		__this->set_InitialScreen_6(L_17);
		RectTransform_t3349966182 * L_18 = SA_UIHightDependence_get_rect_m631336958(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Object_set_hideFlags_m2204253440(L_18, 0, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_009e:
	{
		SA_UIHightDependence_ApplyTransformation_m76535612(__this, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_19 = SA_UIHightDependence_get_rect_m631336958(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Object_set_hideFlags_m2204253440(L_19, 8, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		goto IL_00c6;
	}

IL_00b5:
	{
		bool L_20 = __this->get_CaclulcateOnlyOntStart_4();
		if (L_20)
		{
			goto IL_00c6;
		}
	}
	{
		SA_UIHightDependence_ApplyTransformation_m76535612(__this, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void SA_UIHightDependence::ApplyTransformation()
extern "C"  void SA_UIHightDependence_ApplyTransformation_m76535612 (SA_UIHightDependence_t2303649696 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		Rect_t3681755626 * L_0 = __this->get_address_of_InitialScreen_6();
		float L_1 = Rect_get_height_m3128694305(L_0, /*hidden argument*/NULL);
		Rect_t3681755626 * L_2 = __this->get_address_of_InitialRect_5();
		float L_3 = Rect_get_height_m3128694305(L_2, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_1/(float)L_3));
		Rect_t3681755626 * L_4 = __this->get_address_of_InitialRect_5();
		float L_5 = Rect_get_height_m3128694305(L_4, /*hidden argument*/NULL);
		Rect_t3681755626 * L_6 = __this->get_address_of_InitialRect_5();
		float L_7 = Rect_get_width_m1138015702(L_6, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_5/(float)L_7));
		int32_t L_8 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		V_2 = ((float)((float)(((float)((float)L_8)))/(float)L_9));
		float L_10 = V_2;
		float L_11 = V_1;
		V_3 = ((float)((float)L_10/(float)L_11));
		Rect_t3681755626 * L_12 = __this->get_address_of_InitialRect_5();
		float L_13 = Rect_get_y_m1393582395(L_12, /*hidden argument*/NULL);
		Rect_t3681755626 * L_14 = __this->get_address_of_InitialRect_5();
		float L_15 = Rect_get_height_m3128694305(L_14, /*hidden argument*/NULL);
		V_4 = ((float)((float)L_13/(float)L_15));
		Rect_t3681755626 * L_16 = __this->get_address_of_InitialRect_5();
		float L_17 = Rect_get_x_m1393582490(L_16, /*hidden argument*/NULL);
		Rect_t3681755626 * L_18 = __this->get_address_of_InitialRect_5();
		float L_19 = Rect_get_width_m1138015702(L_18, /*hidden argument*/NULL);
		V_5 = ((float)((float)L_17/(float)L_19));
		float L_20 = V_2;
		float L_21 = V_4;
		V_6 = ((float)((float)L_20*(float)L_21));
		float L_22 = V_3;
		float L_23 = V_5;
		V_7 = ((float)((float)L_22*(float)L_23));
		RectTransform_t3349966182 * L_24 = SA_UIHightDependence_get_rect_m631336958(__this, /*hidden argument*/NULL);
		float L_25 = V_7;
		float L_26 = V_6;
		Vector2_t2243707579  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector2__ctor_m3067419446(&L_27, L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		RectTransform_set_anchoredPosition_m2077229449(L_24, L_27, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_28 = SA_UIHightDependence_get_rect_m631336958(__this, /*hidden argument*/NULL);
		float L_29 = V_3;
		float L_30 = V_2;
		Vector2_t2243707579  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector2__ctor_m3067419446(&L_31, L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		RectTransform_set_sizeDelta_m2319668137(L_28, L_31, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectTransform SA_UIHightDependence::get_rect()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRectTransform_t3349966182_m1310250299_MethodInfo_var;
extern const uint32_t SA_UIHightDependence_get_rect_m631336958_MetadataUsageId;
extern "C"  RectTransform_t3349966182 * SA_UIHightDependence_get_rect_m631336958 (SA_UIHightDependence_t2303649696 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_UIHightDependence_get_rect_m631336958_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t3349966182 * L_0 = __this->get__rect_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RectTransform_t3349966182 * L_2 = Component_GetComponent_TisRectTransform_t3349966182_m1310250299(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t3349966182_m1310250299_MethodInfo_var);
		__this->set__rect_2(L_2);
	}

IL_001d:
	{
		RectTransform_t3349966182 * L_3 = __this->get__rect_2();
		return L_3;
	}
}
// System.Void SA_UIHightDependence::OnDetroy()
extern "C"  void SA_UIHightDependence_OnDetroy_m2620108061 (SA_UIHightDependence_t2303649696 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = SA_UIHightDependence_get_rect_m631336958(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Object_set_hideFlags_m2204253440(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_UIWidthDependence::.ctor()
extern Il2CppClass* Rect_t3681755626_il2cpp_TypeInfo_var;
extern const uint32_t SA_UIWidthDependence__ctor_m2486248185_MetadataUsageId;
extern "C"  void SA_UIWidthDependence__ctor_m2486248185 (SA_UIWidthDependence_t1527301732 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_UIWidthDependence__ctor_m2486248185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (Rect_t3681755626_il2cpp_TypeInfo_var, (&V_0));
		Rect_t3681755626  L_0 = V_0;
		__this->set_InitialRect_5(L_0);
		Initobj (Rect_t3681755626_il2cpp_TypeInfo_var, (&V_1));
		Rect_t3681755626  L_1 = V_1;
		__this->set_InitialScreen_6(L_1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_UIWidthDependence::Awake()
extern "C"  void SA_UIWidthDependence_Awake_m77143550 (SA_UIWidthDependence_t1527301732 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		SA_UIWidthDependence_ApplyTransformation_m2761767072(__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		return;
	}
}
// System.Void SA_UIWidthDependence::Update()
extern "C"  void SA_UIWidthDependence_Update_m79544464 (SA_UIWidthDependence_t1527301732 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_00b5;
		}
	}
	{
		bool L_1 = __this->get_KeepRatioInEdiotr_3();
		if (L_1)
		{
			goto IL_009e;
		}
	}
	{
		RectTransform_t3349966182 * L_2 = SA_UIWidthDependence_get_rect_m3387637258(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector2_t2243707579  L_3 = RectTransform_get_anchoredPosition_m3570822376(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&V_0)->get_x_1();
		RectTransform_t3349966182 * L_5 = SA_UIWidthDependence_get_rect_m3387637258(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector2_t2243707579  L_6 = RectTransform_get_anchoredPosition_m3570822376(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_y_2();
		RectTransform_t3349966182 * L_8 = SA_UIWidthDependence_get_rect_m3387637258(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Rect_t3681755626  L_9 = RectTransform_get_rect_m73954734(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = Rect_get_width_m1138015702((&V_2), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_11 = SA_UIWidthDependence_get_rect_m3387637258(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Rect_t3681755626  L_12 = RectTransform_get_rect_m73954734(L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		float L_13 = Rect_get_height_m3128694305((&V_3), /*hidden argument*/NULL);
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, L_4, L_7, L_10, L_13, /*hidden argument*/NULL);
		__this->set_InitialRect_5(L_14);
		int32_t L_15 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_16 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (0.0f), (0.0f), (((float)((float)L_15))), (((float)((float)L_16))), /*hidden argument*/NULL);
		__this->set_InitialScreen_6(L_17);
		RectTransform_t3349966182 * L_18 = SA_UIWidthDependence_get_rect_m3387637258(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Object_set_hideFlags_m2204253440(L_18, 0, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_009e:
	{
		SA_UIWidthDependence_ApplyTransformation_m2761767072(__this, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_19 = SA_UIWidthDependence_get_rect_m3387637258(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Object_set_hideFlags_m2204253440(L_19, 8, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		goto IL_00c6;
	}

IL_00b5:
	{
		bool L_20 = __this->get_CaclulcateOnlyOntStart_4();
		if (L_20)
		{
			goto IL_00c6;
		}
	}
	{
		SA_UIWidthDependence_ApplyTransformation_m2761767072(__this, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void SA_UIWidthDependence::ApplyTransformation()
extern "C"  void SA_UIWidthDependence_ApplyTransformation_m2761767072 (SA_UIWidthDependence_t1527301732 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		Rect_t3681755626 * L_0 = __this->get_address_of_InitialScreen_6();
		float L_1 = Rect_get_width_m1138015702(L_0, /*hidden argument*/NULL);
		Rect_t3681755626 * L_2 = __this->get_address_of_InitialRect_5();
		float L_3 = Rect_get_width_m1138015702(L_2, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_1/(float)L_3));
		Rect_t3681755626 * L_4 = __this->get_address_of_InitialRect_5();
		float L_5 = Rect_get_height_m3128694305(L_4, /*hidden argument*/NULL);
		Rect_t3681755626 * L_6 = __this->get_address_of_InitialRect_5();
		float L_7 = Rect_get_width_m1138015702(L_6, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_5/(float)L_7));
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		V_2 = ((float)((float)(((float)((float)L_8)))/(float)L_9));
		float L_10 = V_2;
		float L_11 = V_1;
		V_3 = ((float)((float)L_10*(float)L_11));
		Rect_t3681755626 * L_12 = __this->get_address_of_InitialRect_5();
		float L_13 = Rect_get_y_m1393582395(L_12, /*hidden argument*/NULL);
		Rect_t3681755626 * L_14 = __this->get_address_of_InitialRect_5();
		float L_15 = Rect_get_height_m3128694305(L_14, /*hidden argument*/NULL);
		V_4 = ((float)((float)L_13/(float)L_15));
		Rect_t3681755626 * L_16 = __this->get_address_of_InitialRect_5();
		float L_17 = Rect_get_x_m1393582490(L_16, /*hidden argument*/NULL);
		Rect_t3681755626 * L_18 = __this->get_address_of_InitialRect_5();
		float L_19 = Rect_get_width_m1138015702(L_18, /*hidden argument*/NULL);
		V_5 = ((float)((float)L_17/(float)L_19));
		float L_20 = V_3;
		float L_21 = V_4;
		V_6 = ((float)((float)L_20*(float)L_21));
		float L_22 = V_2;
		float L_23 = V_5;
		V_7 = ((float)((float)L_22*(float)L_23));
		RectTransform_t3349966182 * L_24 = SA_UIWidthDependence_get_rect_m3387637258(__this, /*hidden argument*/NULL);
		float L_25 = V_7;
		float L_26 = V_6;
		Vector2_t2243707579  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector2__ctor_m3067419446(&L_27, L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		RectTransform_set_anchoredPosition_m2077229449(L_24, L_27, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_28 = SA_UIWidthDependence_get_rect_m3387637258(__this, /*hidden argument*/NULL);
		float L_29 = V_2;
		float L_30 = V_3;
		Vector2_t2243707579  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector2__ctor_m3067419446(&L_31, L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		RectTransform_set_sizeDelta_m2319668137(L_28, L_31, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectTransform SA_UIWidthDependence::get_rect()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRectTransform_t3349966182_m1310250299_MethodInfo_var;
extern const uint32_t SA_UIWidthDependence_get_rect_m3387637258_MetadataUsageId;
extern "C"  RectTransform_t3349966182 * SA_UIWidthDependence_get_rect_m3387637258 (SA_UIWidthDependence_t1527301732 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_UIWidthDependence_get_rect_m3387637258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t3349966182 * L_0 = __this->get__rect_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RectTransform_t3349966182 * L_2 = Component_GetComponent_TisRectTransform_t3349966182_m1310250299(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t3349966182_m1310250299_MethodInfo_var);
		__this->set__rect_2(L_2);
	}

IL_001d:
	{
		RectTransform_t3349966182 * L_3 = __this->get__rect_2();
		return L_3;
	}
}
// System.Void SA_UIWidthDependence::OnDetroy()
extern "C"  void SA_UIWidthDependence_OnDetroy_m2883930857 (SA_UIWidthDependence_t1527301732 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = SA_UIWidthDependence_get_rect_m3387637258(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Object_set_hideFlags_m2204253440(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_UnityExtensions::MoveTo(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,SA.Common.Animation.EaseType,System.Action)
extern const MethodInfo* GameObject_AddComponent_TisValuesTween_t4181447589_m1522737034_MethodInfo_var;
extern const uint32_t SA_UnityExtensions_MoveTo_m1338073887_MetadataUsageId;
extern "C"  void SA_UnityExtensions_MoveTo_m1338073887 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, Vector3_t2243707580  ___position1, float ___time2, int32_t ___easeType3, Action_t3226471752 * ___OnCompleteAction4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_UnityExtensions_MoveTo_m1338073887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ValuesTween_t4181447589 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___go0;
		NullCheck(L_0);
		ValuesTween_t4181447589 * L_1 = GameObject_AddComponent_TisValuesTween_t4181447589_m1522737034(L_0, /*hidden argument*/GameObject_AddComponent_TisValuesTween_t4181447589_m1522737034_MethodInfo_var);
		V_0 = L_1;
		ValuesTween_t4181447589 * L_2 = V_0;
		NullCheck(L_2);
		L_2->set_DestoryGameObjectOnComplete_2((bool)0);
		ValuesTween_t4181447589 * L_3 = V_0;
		GameObject_t1756533147 * L_4 = ___go0;
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_position_m1104419803(L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = ___position1;
		float L_8 = ___time2;
		int32_t L_9 = ___easeType3;
		NullCheck(L_3);
		ValuesTween_VectorTo_m538993157(L_3, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		ValuesTween_t4181447589 * L_10 = V_0;
		Action_t3226471752 * L_11 = ___OnCompleteAction4;
		NullCheck(L_10);
		ValuesTween_add_OnComplete_m205323862(L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_UnityExtensions::ScaleTo(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,SA.Common.Animation.EaseType,System.Action)
extern const MethodInfo* GameObject_AddComponent_TisValuesTween_t4181447589_m1522737034_MethodInfo_var;
extern const uint32_t SA_UnityExtensions_ScaleTo_m2112209700_MetadataUsageId;
extern "C"  void SA_UnityExtensions_ScaleTo_m2112209700 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, Vector3_t2243707580  ___scale1, float ___time2, int32_t ___easeType3, Action_t3226471752 * ___OnCompleteAction4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_UnityExtensions_ScaleTo_m2112209700_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ValuesTween_t4181447589 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___go0;
		NullCheck(L_0);
		ValuesTween_t4181447589 * L_1 = GameObject_AddComponent_TisValuesTween_t4181447589_m1522737034(L_0, /*hidden argument*/GameObject_AddComponent_TisValuesTween_t4181447589_m1522737034_MethodInfo_var);
		V_0 = L_1;
		ValuesTween_t4181447589 * L_2 = V_0;
		NullCheck(L_2);
		L_2->set_DestoryGameObjectOnComplete_2((bool)0);
		ValuesTween_t4181447589 * L_3 = V_0;
		GameObject_t1756533147 * L_4 = ___go0;
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_localScale_m3074381503(L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = ___scale1;
		float L_8 = ___time2;
		int32_t L_9 = ___easeType3;
		NullCheck(L_3);
		ValuesTween_ScaleTo_m3693444612(L_3, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		ValuesTween_t4181447589 * L_10 = V_0;
		Action_t3226471752 * L_11 = ___OnCompleteAction4;
		NullCheck(L_10);
		ValuesTween_add_OnComplete_m205323862(L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Sprite SA_UnityExtensions::ToSprite(UnityEngine.Texture2D)
extern "C"  Sprite_t309593783 * SA_UnityExtensions_ToSprite_m2215582518 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, const MethodInfo* method)
{
	{
		Texture2D_t3542995729 * L_0 = ___texture0;
		Texture2D_t3542995729 * L_1 = ___texture0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_1);
		Texture2D_t3542995729 * L_3 = ___texture0;
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_3);
		Rect_t3681755626  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Rect__ctor_m1220545469(&L_5, (0.0f), (0.0f), (((float)((float)L_2))), (((float)((float)L_4))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m3067419446(&L_6, (0.5f), (0.5f), /*hidden argument*/NULL);
		Sprite_t309593783 * L_7 = Sprite_Create_m3262956430(NULL /*static, unused*/, L_0, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void SK_AuthorizationResult::.ctor(SK_CloudServiceAuthorizationStatus)
extern "C"  void SK_AuthorizationResult__ctor_m1369504372 (SK_AuthorizationResult_t2047753871 * __this, int32_t ___status0, const MethodInfo* method)
{
	{
		Result__ctor_m114131617(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___status0;
		__this->set__AuthorizationStatus_1(L_0);
		return;
	}
}
// SK_CloudServiceAuthorizationStatus SK_AuthorizationResult::get_AuthorizationStatus()
extern "C"  int32_t SK_AuthorizationResult_get_AuthorizationStatus_m4090126557 (SK_AuthorizationResult_t2047753871 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__AuthorizationStatus_1();
		return L_0;
	}
}
// System.Void SK_CloudService::.ctor()
extern Il2CppClass* Singleton_1_t1070853030_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1__ctor_m2480424923_MethodInfo_var;
extern const uint32_t SK_CloudService__ctor_m2360440912_MetadataUsageId;
extern "C"  void SK_CloudService__ctor_m2360440912 (SK_CloudService_t588287925 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService__ctor_m2360440912_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1070853030_il2cpp_TypeInfo_var);
		Singleton_1__ctor_m2480424923(__this, /*hidden argument*/Singleton_1__ctor_m2480424923_MethodInfo_var);
		return;
	}
}
// System.Void SK_CloudService::.cctor()
extern Il2CppClass* SK_CloudService_t588287925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1849553253_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2311963226_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t914276544_il2cpp_TypeInfo_var;
extern const MethodInfo* SK_CloudService_U3COnAuthorizationFinishedU3Em__55_m641725672_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m729650118_MethodInfo_var;
extern const MethodInfo* SK_CloudService_U3COnCapabilitiesRequestFinishedU3Em__56_m2689700734_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3431867717_MethodInfo_var;
extern const MethodInfo* SK_CloudService_U3COnStorefrontIdentifierRequestFinishedU3Em__57_m232922430_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2695184357_MethodInfo_var;
extern const uint32_t SK_CloudService__cctor_m1424263585_MetadataUsageId;
extern "C"  void SK_CloudService__cctor_m1424263585 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService__cctor_m1424263585_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1849553253 * L_0 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_7();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)SK_CloudService_U3COnAuthorizationFinishedU3Em__55_m641725672_MethodInfo_var);
		Action_1_t1849553253 * L_2 = (Action_1_t1849553253 *)il2cpp_codegen_object_new(Action_1_t1849553253_il2cpp_TypeInfo_var);
		Action_1__ctor_m729650118(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m729650118_MethodInfo_var);
		((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_7(L_2);
	}

IL_0018:
	{
		Action_1_t1849553253 * L_3 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_7();
		((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->set_OnAuthorizationFinished_4(L_3);
		Action_1_t2311963226 * L_4 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_8();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)SK_CloudService_U3COnCapabilitiesRequestFinishedU3Em__56_m2689700734_MethodInfo_var);
		Action_1_t2311963226 * L_6 = (Action_1_t2311963226 *)il2cpp_codegen_object_new(Action_1_t2311963226_il2cpp_TypeInfo_var);
		Action_1__ctor_m3431867717(L_6, NULL, L_5, /*hidden argument*/Action_1__ctor_m3431867717_MethodInfo_var);
		((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_8(L_6);
	}

IL_003a:
	{
		Action_1_t2311963226 * L_7 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_8();
		((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->set_OnCapabilitiesRequestFinished_5(L_7);
		Action_1_t914276544 * L_8 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_9();
		if (L_8)
		{
			goto IL_005c;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)SK_CloudService_U3COnStorefrontIdentifierRequestFinishedU3Em__57_m232922430_MethodInfo_var);
		Action_1_t914276544 * L_10 = (Action_1_t914276544 *)il2cpp_codegen_object_new(Action_1_t914276544_il2cpp_TypeInfo_var);
		Action_1__ctor_m2695184357(L_10, NULL, L_9, /*hidden argument*/Action_1__ctor_m2695184357_MethodInfo_var);
		((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache5_9(L_10);
	}

IL_005c:
	{
		Action_1_t914276544 * L_11 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_9();
		((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->set_OnStorefrontIdentifierRequestFinished_6(L_11);
		return;
	}
}
// System.Void SK_CloudService::add_OnAuthorizationFinished(System.Action`1<SK_AuthorizationResult>)
extern Il2CppClass* SK_CloudService_t588287925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1849553253_il2cpp_TypeInfo_var;
extern const uint32_t SK_CloudService_add_OnAuthorizationFinished_m427846867_MetadataUsageId;
extern "C"  void SK_CloudService_add_OnAuthorizationFinished_m427846867 (Il2CppObject * __this /* static, unused */, Action_1_t1849553253 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService_add_OnAuthorizationFinished_m427846867_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SK_CloudService_t588287925_il2cpp_TypeInfo_var);
		Action_1_t1849553253 * L_0 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_OnAuthorizationFinished_4();
		Action_1_t1849553253 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->set_OnAuthorizationFinished_4(((Action_1_t1849553253 *)CastclassSealed(L_2, Action_1_t1849553253_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SK_CloudService::remove_OnAuthorizationFinished(System.Action`1<SK_AuthorizationResult>)
extern Il2CppClass* SK_CloudService_t588287925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1849553253_il2cpp_TypeInfo_var;
extern const uint32_t SK_CloudService_remove_OnAuthorizationFinished_m474800222_MetadataUsageId;
extern "C"  void SK_CloudService_remove_OnAuthorizationFinished_m474800222 (Il2CppObject * __this /* static, unused */, Action_1_t1849553253 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService_remove_OnAuthorizationFinished_m474800222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SK_CloudService_t588287925_il2cpp_TypeInfo_var);
		Action_1_t1849553253 * L_0 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_OnAuthorizationFinished_4();
		Action_1_t1849553253 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->set_OnAuthorizationFinished_4(((Action_1_t1849553253 *)CastclassSealed(L_2, Action_1_t1849553253_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SK_CloudService::add_OnCapabilitiesRequestFinished(System.Action`1<SK_RequestCapabilitieResult>)
extern Il2CppClass* SK_CloudService_t588287925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2311963226_il2cpp_TypeInfo_var;
extern const uint32_t SK_CloudService_add_OnCapabilitiesRequestFinished_m1336743292_MetadataUsageId;
extern "C"  void SK_CloudService_add_OnCapabilitiesRequestFinished_m1336743292 (Il2CppObject * __this /* static, unused */, Action_1_t2311963226 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService_add_OnCapabilitiesRequestFinished_m1336743292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SK_CloudService_t588287925_il2cpp_TypeInfo_var);
		Action_1_t2311963226 * L_0 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_OnCapabilitiesRequestFinished_5();
		Action_1_t2311963226 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->set_OnCapabilitiesRequestFinished_5(((Action_1_t2311963226 *)CastclassSealed(L_2, Action_1_t2311963226_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SK_CloudService::remove_OnCapabilitiesRequestFinished(System.Action`1<SK_RequestCapabilitieResult>)
extern Il2CppClass* SK_CloudService_t588287925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2311963226_il2cpp_TypeInfo_var;
extern const uint32_t SK_CloudService_remove_OnCapabilitiesRequestFinished_m1749211023_MetadataUsageId;
extern "C"  void SK_CloudService_remove_OnCapabilitiesRequestFinished_m1749211023 (Il2CppObject * __this /* static, unused */, Action_1_t2311963226 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService_remove_OnCapabilitiesRequestFinished_m1749211023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SK_CloudService_t588287925_il2cpp_TypeInfo_var);
		Action_1_t2311963226 * L_0 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_OnCapabilitiesRequestFinished_5();
		Action_1_t2311963226 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->set_OnCapabilitiesRequestFinished_5(((Action_1_t2311963226 *)CastclassSealed(L_2, Action_1_t2311963226_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SK_CloudService::add_OnStorefrontIdentifierRequestFinished(System.Action`1<SK_RequestStorefrontIdentifierResult>)
extern Il2CppClass* SK_CloudService_t588287925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t914276544_il2cpp_TypeInfo_var;
extern const uint32_t SK_CloudService_add_OnStorefrontIdentifierRequestFinished_m3166541867_MetadataUsageId;
extern "C"  void SK_CloudService_add_OnStorefrontIdentifierRequestFinished_m3166541867 (Il2CppObject * __this /* static, unused */, Action_1_t914276544 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService_add_OnStorefrontIdentifierRequestFinished_m3166541867_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SK_CloudService_t588287925_il2cpp_TypeInfo_var);
		Action_1_t914276544 * L_0 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_OnStorefrontIdentifierRequestFinished_6();
		Action_1_t914276544 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->set_OnStorefrontIdentifierRequestFinished_6(((Action_1_t914276544 *)CastclassSealed(L_2, Action_1_t914276544_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SK_CloudService::remove_OnStorefrontIdentifierRequestFinished(System.Action`1<SK_RequestStorefrontIdentifierResult>)
extern Il2CppClass* SK_CloudService_t588287925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t914276544_il2cpp_TypeInfo_var;
extern const uint32_t SK_CloudService_remove_OnStorefrontIdentifierRequestFinished_m3563341042_MetadataUsageId;
extern "C"  void SK_CloudService_remove_OnStorefrontIdentifierRequestFinished_m3563341042 (Il2CppObject * __this /* static, unused */, Action_1_t914276544 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService_remove_OnStorefrontIdentifierRequestFinished_m3563341042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SK_CloudService_t588287925_il2cpp_TypeInfo_var);
		Action_1_t914276544 * L_0 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_OnStorefrontIdentifierRequestFinished_6();
		Action_1_t914276544 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->set_OnStorefrontIdentifierRequestFinished_6(((Action_1_t914276544 *)CastclassSealed(L_2, Action_1_t914276544_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SK_CloudService::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t SK_CloudService_Awake_m1413994399_MetadataUsageId;
extern "C"  void SK_CloudService_Awake_m1413994399 (SK_CloudService_t588287925 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService_Awake_m1413994399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SK_CloudService::RequestAuthorization()
extern "C"  void SK_CloudService_RequestAuthorization_m3901679232 (SK_CloudService_t588287925 * __this, const MethodInfo* method)
{
	{
		IOSNativeMarketBridge_CloudService_RequestAuthorization_m4114280947(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SK_CloudService::RequestCapabilities()
extern "C"  void SK_CloudService_RequestCapabilities_m3908666353 (SK_CloudService_t588287925 * __this, const MethodInfo* method)
{
	{
		IOSNativeMarketBridge_CloudService_RequestCapabilities_m1052414014(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SK_CloudService::RequestStorefrontIdentifier()
extern "C"  void SK_CloudService_RequestStorefrontIdentifier_m2230795202 (SK_CloudService_t588287925 * __this, const MethodInfo* method)
{
	{
		IOSNativeMarketBridge_CloudService_RequestStorefrontIdentifier_m2269887697(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 SK_CloudService::get_AuthorizationStatus()
extern "C"  int32_t SK_CloudService_get_AuthorizationStatus_m3074601638 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = IOSNativeMarketBridge_CloudService_AuthorizationStatus_m408060408(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void SK_CloudService::Event_AuthorizationFinished(System.String)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* SK_AuthorizationResult_t2047753871_il2cpp_TypeInfo_var;
extern Il2CppClass* SK_CloudService_t588287925_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3806981723_MethodInfo_var;
extern const uint32_t SK_CloudService_Event_AuthorizationFinished_m2104971666_MetadataUsageId;
extern "C"  void SK_CloudService_Event_AuthorizationFinished_m2104971666 (SK_CloudService_t588287925 * __this, String_t* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService_Event_AuthorizationFinished_m2104971666_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	SK_AuthorizationResult_t2047753871 * V_1 = NULL;
	{
		String_t* L_0 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_1 = Convert_ToInt32_m2593311249(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		SK_AuthorizationResult_t2047753871 * L_3 = (SK_AuthorizationResult_t2047753871 *)il2cpp_codegen_object_new(SK_AuthorizationResult_t2047753871_il2cpp_TypeInfo_var);
		SK_AuthorizationResult__ctor_m1369504372(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(SK_CloudService_t588287925_il2cpp_TypeInfo_var);
		Action_1_t1849553253 * L_4 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_OnAuthorizationFinished_4();
		SK_AuthorizationResult_t2047753871 * L_5 = V_1;
		NullCheck(L_4);
		Action_1_Invoke_m3806981723(L_4, L_5, /*hidden argument*/Action_1_Invoke_m3806981723_MethodInfo_var);
		return;
	}
}
// System.Void SK_CloudService::Event_RequestCapabilitieSsuccess(System.String)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* SK_RequestCapabilitieResult_t2510163844_il2cpp_TypeInfo_var;
extern Il2CppClass* SK_CloudService_t588287925_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1898924496_MethodInfo_var;
extern const uint32_t SK_CloudService_Event_RequestCapabilitieSsuccess_m182284955_MetadataUsageId;
extern "C"  void SK_CloudService_Event_RequestCapabilitieSsuccess_m182284955 (SK_CloudService_t588287925 * __this, String_t* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService_Event_RequestCapabilitieSsuccess_m182284955_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	SK_RequestCapabilitieResult_t2510163844 * V_1 = NULL;
	{
		String_t* L_0 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_1 = Convert_ToInt32_m2593311249(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		SK_RequestCapabilitieResult_t2510163844 * L_3 = (SK_RequestCapabilitieResult_t2510163844 *)il2cpp_codegen_object_new(SK_RequestCapabilitieResult_t2510163844_il2cpp_TypeInfo_var);
		SK_RequestCapabilitieResult__ctor_m2861536598(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(SK_CloudService_t588287925_il2cpp_TypeInfo_var);
		Action_1_t2311963226 * L_4 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_OnCapabilitiesRequestFinished_5();
		SK_RequestCapabilitieResult_t2510163844 * L_5 = V_1;
		NullCheck(L_4);
		Action_1_Invoke_m1898924496(L_4, L_5, /*hidden argument*/Action_1_Invoke_m1898924496_MethodInfo_var);
		return;
	}
}
// System.Void SK_CloudService::Event_RequestCapabilitiesFailed(System.String)
extern Il2CppClass* SK_RequestCapabilitieResult_t2510163844_il2cpp_TypeInfo_var;
extern Il2CppClass* SK_CloudService_t588287925_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1898924496_MethodInfo_var;
extern const uint32_t SK_CloudService_Event_RequestCapabilitiesFailed_m149570975_MetadataUsageId;
extern "C"  void SK_CloudService_Event_RequestCapabilitiesFailed_m149570975 (SK_CloudService_t588287925 * __this, String_t* ___errorData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService_Event_RequestCapabilitiesFailed_m149570975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SK_RequestCapabilitieResult_t2510163844 * V_0 = NULL;
	{
		String_t* L_0 = ___errorData0;
		SK_RequestCapabilitieResult_t2510163844 * L_1 = (SK_RequestCapabilitieResult_t2510163844 *)il2cpp_codegen_object_new(SK_RequestCapabilitieResult_t2510163844_il2cpp_TypeInfo_var);
		SK_RequestCapabilitieResult__ctor_m3528146553(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SK_CloudService_t588287925_il2cpp_TypeInfo_var);
		Action_1_t2311963226 * L_2 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_OnCapabilitiesRequestFinished_5();
		SK_RequestCapabilitieResult_t2510163844 * L_3 = V_0;
		NullCheck(L_2);
		Action_1_Invoke_m1898924496(L_2, L_3, /*hidden argument*/Action_1_Invoke_m1898924496_MethodInfo_var);
		return;
	}
}
// System.Void SK_CloudService::Event_RequestStorefrontIdentifierSsuccess(System.String)
extern Il2CppClass* SK_RequestStorefrontIdentifierResult_t1112477162_il2cpp_TypeInfo_var;
extern Il2CppClass* SK_CloudService_t588287925_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3140662838_MethodInfo_var;
extern const uint32_t SK_CloudService_Event_RequestStorefrontIdentifierSsuccess_m2080645007_MetadataUsageId;
extern "C"  void SK_CloudService_Event_RequestStorefrontIdentifierSsuccess_m2080645007 (SK_CloudService_t588287925 * __this, String_t* ___storefrontIdentifier0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService_Event_RequestStorefrontIdentifierSsuccess_m2080645007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SK_RequestStorefrontIdentifierResult_t1112477162 * V_0 = NULL;
	{
		SK_RequestStorefrontIdentifierResult_t1112477162 * L_0 = (SK_RequestStorefrontIdentifierResult_t1112477162 *)il2cpp_codegen_object_new(SK_RequestStorefrontIdentifierResult_t1112477162_il2cpp_TypeInfo_var);
		SK_RequestStorefrontIdentifierResult__ctor_m2409072583(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		SK_RequestStorefrontIdentifierResult_t1112477162 * L_1 = V_0;
		String_t* L_2 = ___storefrontIdentifier0;
		NullCheck(L_1);
		SK_RequestStorefrontIdentifierResult_set_StorefrontIdentifier_m2867142185(L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SK_CloudService_t588287925_il2cpp_TypeInfo_var);
		Action_1_t914276544 * L_3 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_OnStorefrontIdentifierRequestFinished_6();
		SK_RequestStorefrontIdentifierResult_t1112477162 * L_4 = V_0;
		NullCheck(L_3);
		Action_1_Invoke_m3140662838(L_3, L_4, /*hidden argument*/Action_1_Invoke_m3140662838_MethodInfo_var);
		return;
	}
}
// System.Void SK_CloudService::Event_RequestStorefrontIdentifierFailed(System.String)
extern Il2CppClass* SK_RequestStorefrontIdentifierResult_t1112477162_il2cpp_TypeInfo_var;
extern Il2CppClass* SK_CloudService_t588287925_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3140662838_MethodInfo_var;
extern const uint32_t SK_CloudService_Event_RequestStorefrontIdentifierFailed_m3271524126_MetadataUsageId;
extern "C"  void SK_CloudService_Event_RequestStorefrontIdentifierFailed_m3271524126 (SK_CloudService_t588287925 * __this, String_t* ___errorData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_CloudService_Event_RequestStorefrontIdentifierFailed_m3271524126_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SK_RequestStorefrontIdentifierResult_t1112477162 * V_0 = NULL;
	{
		String_t* L_0 = ___errorData0;
		SK_RequestStorefrontIdentifierResult_t1112477162 * L_1 = (SK_RequestStorefrontIdentifierResult_t1112477162 *)il2cpp_codegen_object_new(SK_RequestStorefrontIdentifierResult_t1112477162_il2cpp_TypeInfo_var);
		SK_RequestStorefrontIdentifierResult__ctor_m3372218593(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SK_CloudService_t588287925_il2cpp_TypeInfo_var);
		Action_1_t914276544 * L_2 = ((SK_CloudService_t588287925_StaticFields*)SK_CloudService_t588287925_il2cpp_TypeInfo_var->static_fields)->get_OnStorefrontIdentifierRequestFinished_6();
		SK_RequestStorefrontIdentifierResult_t1112477162 * L_3 = V_0;
		NullCheck(L_2);
		Action_1_Invoke_m3140662838(L_2, L_3, /*hidden argument*/Action_1_Invoke_m3140662838_MethodInfo_var);
		return;
	}
}
// System.Void SK_CloudService::<OnAuthorizationFinished>m__55(SK_AuthorizationResult)
extern "C"  void SK_CloudService_U3COnAuthorizationFinishedU3Em__55_m641725672 (Il2CppObject * __this /* static, unused */, SK_AuthorizationResult_t2047753871 * p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SK_CloudService::<OnCapabilitiesRequestFinished>m__56(SK_RequestCapabilitieResult)
extern "C"  void SK_CloudService_U3COnCapabilitiesRequestFinishedU3Em__56_m2689700734 (Il2CppObject * __this /* static, unused */, SK_RequestCapabilitieResult_t2510163844 * p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SK_CloudService::<OnStorefrontIdentifierRequestFinished>m__57(SK_RequestStorefrontIdentifierResult)
extern "C"  void SK_CloudService_U3COnStorefrontIdentifierRequestFinishedU3Em__57_m232922430 (Il2CppObject * __this /* static, unused */, SK_RequestStorefrontIdentifierResult_t1112477162 * p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SK_RequestCapabilitieResult::.ctor(SK_CloudServiceCapability)
extern "C"  void SK_RequestCapabilitieResult__ctor_m2861536598 (SK_RequestCapabilitieResult_t2510163844 * __this, int32_t ___capability0, const MethodInfo* method)
{
	{
		Result__ctor_m114131617(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capability0;
		__this->set__Capability_1(L_0);
		return;
	}
}
// System.Void SK_RequestCapabilitieResult::.ctor(System.String)
extern Il2CppClass* Error_t445207774_il2cpp_TypeInfo_var;
extern const uint32_t SK_RequestCapabilitieResult__ctor_m3528146553_MetadataUsageId;
extern "C"  void SK_RequestCapabilitieResult__ctor_m3528146553 (SK_RequestCapabilitieResult_t2510163844 * __this, String_t* ___errorData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_RequestCapabilitieResult__ctor_m3528146553_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___errorData0;
		Error_t445207774 * L_1 = (Error_t445207774 *)il2cpp_codegen_object_new(Error_t445207774_il2cpp_TypeInfo_var);
		Error__ctor_m994168232(L_1, L_0, /*hidden argument*/NULL);
		Result__ctor_m67150262(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// SK_CloudServiceCapability SK_RequestCapabilitieResult::get_Capability()
extern "C"  int32_t SK_RequestCapabilitieResult_get_Capability_m2135374280 (SK_RequestCapabilitieResult_t2510163844 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__Capability_1();
		return L_0;
	}
}
// System.Void SK_RequestStorefrontIdentifierResult::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SK_RequestStorefrontIdentifierResult__ctor_m2409072583_MetadataUsageId;
extern "C"  void SK_RequestStorefrontIdentifierResult__ctor_m2409072583 (SK_RequestStorefrontIdentifierResult_t1112477162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_RequestStorefrontIdentifierResult__ctor_m2409072583_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__StorefrontIdentifier_1(L_0);
		Result__ctor_m114131617(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SK_RequestStorefrontIdentifierResult::.ctor(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Error_t445207774_il2cpp_TypeInfo_var;
extern const uint32_t SK_RequestStorefrontIdentifierResult__ctor_m3372218593_MetadataUsageId;
extern "C"  void SK_RequestStorefrontIdentifierResult__ctor_m3372218593 (SK_RequestStorefrontIdentifierResult_t1112477162 * __this, String_t* ___errorData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SK_RequestStorefrontIdentifierResult__ctor_m3372218593_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__StorefrontIdentifier_1(L_0);
		String_t* L_1 = ___errorData0;
		Error_t445207774 * L_2 = (Error_t445207774 *)il2cpp_codegen_object_new(Error_t445207774_il2cpp_TypeInfo_var);
		Error__ctor_m994168232(L_2, L_1, /*hidden argument*/NULL);
		Result__ctor_m67150262(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String SK_RequestStorefrontIdentifierResult::get_StorefrontIdentifier()
extern "C"  String_t* SK_RequestStorefrontIdentifierResult_get_StorefrontIdentifier_m2139624576 (SK_RequestStorefrontIdentifierResult_t1112477162 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__StorefrontIdentifier_1();
		return L_0;
	}
}
// System.Void SK_RequestStorefrontIdentifierResult::set_StorefrontIdentifier(System.String)
extern "C"  void SK_RequestStorefrontIdentifierResult_set_StorefrontIdentifier_m2867142185 (SK_RequestStorefrontIdentifierResult_t1112477162 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__StorefrontIdentifier_1(L_0);
		return;
	}
}
// System.Void TBM_Multiplayer_Example::.ctor()
extern "C"  void TBM_Multiplayer_Example__ctor_m2298897268 (TBM_Multiplayer_Example_t1159631235 * __this, const MethodInfo* method)
{
	{
		BaseIOSFeaturePreview__ctor_m3771582115(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TBM_Multiplayer_Example::.cctor()
extern "C"  void TBM_Multiplayer_Example__cctor_m3316973679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TBM_Multiplayer_Example::Awake()
extern Il2CppClass* TBM_Multiplayer_Example_t1159631235_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterManager_t1487113918_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t4089019125_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t1332195736_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1415997413_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t635498072_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenter_RTM_t849630631_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t1065438871_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterInvitations_t2643374653_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t884171879_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t1736305833_il2cpp_TypeInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_OnAuthFinished_m469706619_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3970753795_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_Instance_m4000186170_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m566484697_MethodInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_HandleOnPlayerPhotoLoaded_m4232714084_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m978151514_MethodInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_HandleActionMatchStarted_m4249849620_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3283389991_MethodInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_HandleActionPlayerRequestedMatchWithRecipients_m3843375748_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m3363872687_MethodInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_HandleActionPlayerAcceptedInvitation_m1361775653_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m942121334_MethodInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_HandleActionNearbyPlayerStateUpdated_m3661636353_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m1101796991_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2585082435;
extern const uint32_t TBM_Multiplayer_Example_Awake_m1936938341_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_Awake_m1936938341 (TBM_Multiplayer_Example_t1159631235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_Awake_m1936938341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	StringU5BU5D_t1642385972* V_4 = NULL;
	GK_Player_t2782008294 * V_5 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(TBM_Multiplayer_Example_t1159631235_il2cpp_TypeInfo_var);
		bool L_0 = ((TBM_Multiplayer_Example_t1159631235_StaticFields*)TBM_Multiplayer_Example_t1159631235_il2cpp_TypeInfo_var->static_fields)->get_IsInitialized_12();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterManager_t1487113918_il2cpp_TypeInfo_var);
		GameCenterManager_Init_m2787824017(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_OnAuthFinished_m469706619_MethodInfo_var);
		Action_1_t4089019125 * L_2 = (Action_1_t4089019125 *)il2cpp_codegen_object_new(Action_1_t4089019125_il2cpp_TypeInfo_var);
		Action_1__ctor_m3970753795(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m3970753795_MethodInfo_var);
		GameCenterManager_add_OnAuthFinished_m1732264708(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TBM_Multiplayer_Example_t1159631235_il2cpp_TypeInfo_var);
		((TBM_Multiplayer_Example_t1159631235_StaticFields*)TBM_Multiplayer_Example_t1159631235_il2cpp_TypeInfo_var->static_fields)->set_IsInitialized_12((bool)1);
	}

IL_0026:
	{
		V_0 = 4;
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1332195736_il2cpp_TypeInfo_var);
		GameCenter_RTM_t849630631 * L_3 = Singleton_1_get_Instance_m4000186170(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m4000186170_MethodInfo_var);
		int32_t L_4 = V_0;
		NullCheck(L_3);
		GameCenter_RTM_SetPlayerAttributes_m481404903(L_3, L_4, /*hidden argument*/NULL);
		V_1 = 2;
		V_2 = 2;
		V_3 = _stringLiteral2585082435;
		StringU5BU5D_t1642385972* L_5 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterManager_t1487113918_il2cpp_TypeInfo_var);
		List_1_t1398341365 * L_6 = GameCenterManager_get_FriendsList_m1765526358(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = List_1_get_Item_m566484697(L_6, 0, /*hidden argument*/List_1_get_Item_m566484697_MethodInfo_var);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_7);
		V_4 = L_5;
		GameCenter_RTM_t849630631 * L_8 = Singleton_1_get_Instance_m4000186170(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m4000186170_MethodInfo_var);
		int32_t L_9 = V_1;
		int32_t L_10 = V_2;
		String_t* L_11 = V_3;
		StringU5BU5D_t1642385972* L_12 = V_4;
		NullCheck(L_8);
		GameCenter_RTM_FindMatchWithNativeUI_m1019797187(L_8, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		GK_Player_t2782008294 * L_13 = GameCenterManager_get_Player_m3574869014(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_13;
		GK_Player_t2782008294 * L_14 = V_5;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_HandleOnPlayerPhotoLoaded_m4232714084_MethodInfo_var);
		Action_1_t1415997413 * L_16 = (Action_1_t1415997413 *)il2cpp_codegen_object_new(Action_1_t1415997413_il2cpp_TypeInfo_var);
		Action_1__ctor_m978151514(L_16, __this, L_15, /*hidden argument*/Action_1__ctor_m978151514_MethodInfo_var);
		NullCheck(L_14);
		GK_Player_add_OnPlayerPhotoLoaded_m3924483489(L_14, L_16, /*hidden argument*/NULL);
		GK_Player_t2782008294 * L_17 = V_5;
		NullCheck(L_17);
		GK_Player_LoadPhoto_m783847573(L_17, 1, /*hidden argument*/NULL);
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_HandleActionMatchStarted_m4249849620_MethodInfo_var);
		Action_1_t635498072 * L_19 = (Action_1_t635498072 *)il2cpp_codegen_object_new(Action_1_t635498072_il2cpp_TypeInfo_var);
		Action_1__ctor_m3283389991(L_19, __this, L_18, /*hidden argument*/Action_1__ctor_m3283389991_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_RTM_t849630631_il2cpp_TypeInfo_var);
		GameCenter_RTM_add_ActionMatchStarted_m3211150216(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_HandleActionPlayerRequestedMatchWithRecipients_m3843375748_MethodInfo_var);
		Action_3_t1065438871 * L_21 = (Action_3_t1065438871 *)il2cpp_codegen_object_new(Action_3_t1065438871_il2cpp_TypeInfo_var);
		Action_3__ctor_m3363872687(L_21, __this, L_20, /*hidden argument*/Action_3__ctor_m3363872687_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterInvitations_t2643374653_il2cpp_TypeInfo_var);
		GameCenterInvitations_add_ActionPlayerRequestedMatchWithRecipients_m458734746(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_HandleActionPlayerAcceptedInvitation_m1361775653_MethodInfo_var);
		Action_2_t884171879 * L_23 = (Action_2_t884171879 *)il2cpp_codegen_object_new(Action_2_t884171879_il2cpp_TypeInfo_var);
		Action_2__ctor_m942121334(L_23, __this, L_22, /*hidden argument*/Action_2__ctor_m942121334_MethodInfo_var);
		GameCenterInvitations_add_ActionPlayerAcceptedInvitation_m2598178882(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_HandleActionNearbyPlayerStateUpdated_m3661636353_MethodInfo_var);
		Action_2_t1736305833 * L_25 = (Action_2_t1736305833 *)il2cpp_codegen_object_new(Action_2_t1736305833_il2cpp_TypeInfo_var);
		Action_2__ctor_m1101796991(L_25, __this, L_24, /*hidden argument*/Action_2__ctor_m1101796991_MethodInfo_var);
		GameCenter_RTM_add_ActionNearbyPlayerStateUpdated_m2327195658(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		GameCenter_RTM_t849630631 * L_26 = Singleton_1_get_Instance_m4000186170(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m4000186170_MethodInfo_var);
		NullCheck(L_26);
		GameCenter_RTM_StartBrowsingForNearbyPlayers_m2173112015(L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TBM_Multiplayer_Example::HandleActionNearbyPlayerStateUpdated(GK_Player,System.Boolean)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t1332195736_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_Instance_m4000186170_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m1901183144_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3198827733;
extern Il2CppCodeGenString* _stringLiteral684692009;
extern Il2CppCodeGenString* _stringLiteral835016634;
extern const uint32_t TBM_Multiplayer_Example_HandleActionNearbyPlayerStateUpdated_m3661636353_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_HandleActionNearbyPlayerStateUpdated_m3661636353 (TBM_Multiplayer_Example_t1159631235 * __this, GK_Player_t2782008294 * ___player0, bool ___IsAvaliable1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_HandleActionNearbyPlayerStateUpdated_m3661636353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3198827733);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3198827733);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		GK_Player_t2782008294 * L_2 = ___player0;
		NullCheck(L_2);
		String_t* L_3 = GK_Player_get_DisplayName_m186231108(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral684692009);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral684692009);
		ObjectU5BU5D_t3614634134* L_5 = L_4;
		bool L_6 = ___IsAvaliable1;
		bool L_7 = L_6;
		Il2CppObject * L_8 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3881798623(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_9, 3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1332195736_il2cpp_TypeInfo_var);
		GameCenter_RTM_t849630631 * L_10 = Singleton_1_get_Instance_m4000186170(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m4000186170_MethodInfo_var);
		NullCheck(L_10);
		Dictionary_2_t401820260 * L_11 = GameCenter_RTM_get_NearbyPlayers_m3254451274(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = Dictionary_2_get_Count_m1901183144(L_11, /*hidden argument*/Dictionary_2_get_Count_m1901183144_MethodInfo_var);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_13);
		String_t* L_15 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral835016634, L_14, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_15, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TBM_Multiplayer_Example::HandleActionPlayerAcceptedInvitation(GK_MatchType,GK_Invite)
extern Il2CppClass* Singleton_1_t1332195736_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_Instance_m4000186170_MethodInfo_var;
extern const uint32_t TBM_Multiplayer_Example_HandleActionPlayerAcceptedInvitation_m1361775653_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_HandleActionPlayerAcceptedInvitation_m1361775653 (TBM_Multiplayer_Example_t1159631235 * __this, int32_t ___matchType0, GK_Invite_t22070530 * ___invite1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_HandleActionPlayerAcceptedInvitation_m1361775653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = ___matchType0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0015;
		}
	}
	{
		V_0 = (bool)1;
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1332195736_il2cpp_TypeInfo_var);
		GameCenter_RTM_t849630631 * L_1 = Singleton_1_get_Instance_m4000186170(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m4000186170_MethodInfo_var);
		GK_Invite_t22070530 * L_2 = ___invite1;
		bool L_3 = V_0;
		NullCheck(L_1);
		GameCenter_RTM_StartMatchWithInvite_m1779370069(L_1, L_2, L_3, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void TBM_Multiplayer_Example::HandleActionPlayerRequestedMatchWithRecipients(GK_MatchType,System.String[],GK_Player[])
extern Il2CppClass* Singleton_1_t1332195736_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_Instance_m4000186170_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2585082435;
extern const uint32_t TBM_Multiplayer_Example_HandleActionPlayerRequestedMatchWithRecipients_m3843375748_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_HandleActionPlayerRequestedMatchWithRecipients_m3843375748 (TBM_Multiplayer_Example_t1159631235 * __this, int32_t ___matchType0, StringU5BU5D_t1642385972* ___recepientIds1, GK_PlayerU5BU5D_t1642762691* ___recepients2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_HandleActionPlayerRequestedMatchWithRecipients_m3843375748_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t L_0 = ___matchType0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_001f;
		}
	}
	{
		V_0 = _stringLiteral2585082435;
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1332195736_il2cpp_TypeInfo_var);
		GameCenter_RTM_t849630631 * L_1 = Singleton_1_get_Instance_m4000186170(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m4000186170_MethodInfo_var);
		StringU5BU5D_t1642385972* L_2 = ___recepientIds1;
		NullCheck(L_2);
		StringU5BU5D_t1642385972* L_3 = ___recepientIds1;
		NullCheck(L_3);
		String_t* L_4 = V_0;
		StringU5BU5D_t1642385972* L_5 = ___recepientIds1;
		NullCheck(L_1);
		GameCenter_RTM_FindMatchWithNativeUI_m1019797187(L_1, (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))), L_4, L_5, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void TBM_Multiplayer_Example::HandleActionMatchStarted(GK_RTM_MatchStartedResult)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3554282894;
extern Il2CppCodeGenString* _stringLiteral2649529179;
extern const uint32_t TBM_Multiplayer_Example_HandleActionMatchStarted_m4249849620_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_HandleActionMatchStarted_m4249849620 (TBM_Multiplayer_Example_t1159631235 * __this, GK_RTM_MatchStartedResult_t833698690 * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_HandleActionMatchStarted_m4249849620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GK_RTM_MatchStartedResult_t833698690 * L_0 = ___result0;
		NullCheck(L_0);
		bool L_1 = Result_get_IsSucceeded_m955312237(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, _stringLiteral3554282894, 3, /*hidden argument*/NULL);
		GK_RTM_MatchStartedResult_t833698690 * L_2 = ___result0;
		NullCheck(L_2);
		GK_RTM_Match_t873568990 * L_3 = GK_RTM_MatchStartedResult_get_Match_m1358040904(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = GK_RTM_Match_get_ExpectedPlayerCount_m2952370914(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0026;
		}
	}

IL_0026:
	{
		goto IL_0046;
	}

IL_002b:
	{
		GK_RTM_MatchStartedResult_t833698690 * L_5 = ___result0;
		NullCheck(L_5);
		Error_t445207774 * L_6 = Result_get_Error_m3741808192(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = Error_get_Message_m419911227(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2649529179, L_7, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_8, 3, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Void TBM_Multiplayer_Example::HandleOnPlayerPhotoLoaded(GK_UserPhotoLoadResult)
extern Il2CppClass* GameCenterManager_t1487113918_il2cpp_TypeInfo_var;
extern const uint32_t TBM_Multiplayer_Example_HandleOnPlayerPhotoLoaded_m4232714084_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_HandleOnPlayerPhotoLoaded_m4232714084 (TBM_Multiplayer_Example_t1159631235 * __this, GK_UserPhotoLoadResult_t1614198031 * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_HandleOnPlayerPhotoLoaded_m4232714084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GK_UserPhotoLoadResult_t1614198031 * L_0 = ___result0;
		NullCheck(L_0);
		bool L_1 = Result_get_IsSucceeded_m955312237(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		GK_UserPhotoLoadResult_t1614198031 * L_2 = ___result0;
		NullCheck(L_2);
		Texture2D_t3542995729 * L_3 = GK_UserPhotoLoadResult_get_Photo_m3984735534(L_2, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_3, 3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterManager_t1487113918_il2cpp_TypeInfo_var);
		GK_Player_t2782008294 * L_4 = GameCenterManager_get_Player_m3574869014(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Texture2D_t3542995729 * L_5 = GK_Player_get_BigPhoto_m2222963713(L_4, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_5, 3, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void TBM_Multiplayer_Example::OnGUI()
extern Il2CppClass* GameCenterManager_t1487113918_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t3940119580_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t172291117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3649630279_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1157805416_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1319180072_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2707805896_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3263568192_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t710925695_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_Instance_m85090890_MethodInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_ActionMatchesResultLoaded_m2973444938_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2617219004_MethodInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_ActionMatchFound_m2262552008_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1880827218_MethodInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_ActionMatchDataUpdated_m72619858_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2637091011_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m694581298_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1433138044_MethodInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_ActionTrunEnded_m1257330973_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2764520851_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3208953854_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m4160213088_MethodInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_ActionMacthEnded_m2135114273_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1524944681_MethodInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_ActionMacthRemoved_m1026773452_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2133134992_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2207751291;
extern Il2CppCodeGenString* _stringLiteral4083190521;
extern Il2CppCodeGenString* _stringLiteral717880733;
extern Il2CppCodeGenString* _stringLiteral2510855807;
extern Il2CppCodeGenString* _stringLiteral2683019182;
extern Il2CppCodeGenString* _stringLiteral2410366290;
extern Il2CppCodeGenString* _stringLiteral372029430;
extern Il2CppCodeGenString* _stringLiteral2215163754;
extern Il2CppCodeGenString* _stringLiteral2765270333;
extern Il2CppCodeGenString* _stringLiteral1993840386;
extern Il2CppCodeGenString* _stringLiteral813573746;
extern Il2CppCodeGenString* _stringLiteral3705768981;
extern const uint32_t TBM_Multiplayer_Example_OnGUI_m1708448308_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_OnGUI_m1708448308 (TBM_Multiplayer_Example_t1159631235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_OnGUI_m1708448308_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	GK_TBM_Participant_t3803955090 * V_2 = NULL;
	Enumerator_t2707805896  V_3;
	memset(&V_3, 0, sizeof(V_3));
	ByteU5BU5D_t3397334013* V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		BaseIOSFeaturePreview_UpdateToStartPos_m848793309(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterManager_t1487113918_il2cpp_TypeInfo_var);
		bool L_0 = GameCenterManager_get_IsPlayerAuthenticated_m1875190954(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0047;
		}
	}
	{
		float L_1 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_2 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartY_5();
		int32_t L_3 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Rect__ctor_m1220545469(&L_4, L_1, L_2, (((float)((float)L_3))), (40.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_5 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_style_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_4, _stringLiteral2207751291, L_5, /*hidden argument*/NULL);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		goto IL_0079;
	}

IL_0047:
	{
		float L_6 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_7 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartY_5();
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Rect__ctor_m1220545469(&L_9, L_6, L_7, (((float)((float)L_8))), (40.0f), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_10 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_style_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_9, _stringLiteral4083190521, L_10, /*hidden argument*/NULL);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
	}

IL_0079:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		float L_11 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartY_5();
		float L_12 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_YLableStep_11();
		((BaseIOSFeaturePreview_t3055692840 *)__this)->set_StartY_5(((float)((float)L_11+(float)L_12)));
		float L_13 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_14 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartY_5();
		int32_t L_15 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonWidth_3();
		int32_t L_16 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonHeight_4();
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, L_13, L_14, (((float)((float)L_15))), (((float)((float)L_16))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral717880733, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00db;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t3940119580_il2cpp_TypeInfo_var);
		GameCenter_TBM_t3457554475 * L_19 = Singleton_1_get_Instance_m85090890(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m85090890_MethodInfo_var);
		NullCheck(L_19);
		GameCenter_TBM_LoadMatchesInfo_m1825241001(L_19, /*hidden argument*/NULL);
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_ActionMatchesResultLoaded_m2973444938_MethodInfo_var);
		Action_1_t172291117 * L_21 = (Action_1_t172291117 *)il2cpp_codegen_object_new(Action_1_t172291117_il2cpp_TypeInfo_var);
		Action_1__ctor_m2617219004(L_21, __this, L_20, /*hidden argument*/Action_1__ctor_m2617219004_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_add_ActionMatchesInfoLoaded_m3018774395(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
	}

IL_00db:
	{
		float L_22 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_23 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_XButtonStep_9();
		((BaseIOSFeaturePreview_t3055692840 *)__this)->set_StartX_6(((float)((float)L_22+(float)L_23)));
		float L_24 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_25 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartY_5();
		int32_t L_26 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonWidth_3();
		int32_t L_27 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonHeight_4();
		Rect_t3681755626  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Rect__ctor_m1220545469(&L_28, L_24, L_25, (((float)((float)L_26))), (((float)((float)L_27))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_29 = GUI_Button_m3054448581(NULL /*static, unused*/, L_28, _stringLiteral2510855807, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_013f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t3940119580_il2cpp_TypeInfo_var);
		GameCenter_TBM_t3457554475 * L_30 = Singleton_1_get_Instance_m85090890(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m85090890_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_30);
		GameCenter_TBM_FindMatch_m895359874(L_30, 2, 2, L_31, (StringU5BU5D_t1642385972*)(StringU5BU5D_t1642385972*)NULL, /*hidden argument*/NULL);
		IntPtr_t L_32;
		L_32.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_ActionMatchFound_m2262552008_MethodInfo_var);
		Action_1_t3649630279 * L_33 = (Action_1_t3649630279 *)il2cpp_codegen_object_new(Action_1_t3649630279_il2cpp_TypeInfo_var);
		Action_1__ctor_m1880827218(L_33, __this, L_32, /*hidden argument*/Action_1__ctor_m1880827218_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_add_ActionMatchFound_m2449499778(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_013f:
	{
		float L_34 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_35 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_XButtonStep_9();
		((BaseIOSFeaturePreview_t3055692840 *)__this)->set_StartX_6(((float)((float)L_34+(float)L_35)));
		float L_36 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_37 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartY_5();
		int32_t L_38 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonWidth_3();
		int32_t L_39 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonHeight_4();
		Rect_t3681755626  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Rect__ctor_m1220545469(&L_40, L_36, L_37, (((float)((float)L_38))), (((float)((float)L_39))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_41 = GUI_Button_m3054448581(NULL /*static, unused*/, L_40, _stringLiteral2683019182, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_01a3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t3940119580_il2cpp_TypeInfo_var);
		GameCenter_TBM_t3457554475 * L_42 = Singleton_1_get_Instance_m85090890(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m85090890_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_42);
		GameCenter_TBM_FindMatchWithNativeUI_m3228567259(L_42, 2, 2, L_43, (StringU5BU5D_t1642385972*)(StringU5BU5D_t1642385972*)NULL, /*hidden argument*/NULL);
		IntPtr_t L_44;
		L_44.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_ActionMatchFound_m2262552008_MethodInfo_var);
		Action_1_t3649630279 * L_45 = (Action_1_t3649630279 *)il2cpp_codegen_object_new(Action_1_t3649630279_il2cpp_TypeInfo_var);
		Action_1__ctor_m1880827218(L_45, __this, L_44, /*hidden argument*/Action_1__ctor_m1880827218_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_add_ActionMatchFound_m2449499778(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
	}

IL_01a3:
	{
		GK_TBM_Match_t132033130 * L_46 = TBM_Multiplayer_Example_get_CurrentMatch_m2767251338(__this, /*hidden argument*/NULL);
		if (L_46)
		{
			goto IL_01b9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		goto IL_01bf;
	}

IL_01b9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
	}

IL_01bf:
	{
		float L_47 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_XStartPos_7();
		((BaseIOSFeaturePreview_t3055692840 *)__this)->set_StartX_6(L_47);
		float L_48 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartY_5();
		float L_49 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_YButtonStep_10();
		((BaseIOSFeaturePreview_t3055692840 *)__this)->set_StartY_5(((float)((float)L_48+(float)L_49)));
		float L_50 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_51 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartY_5();
		int32_t L_52 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonWidth_3();
		int32_t L_53 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonHeight_4();
		Rect_t3681755626  L_54;
		memset(&L_54, 0, sizeof(L_54));
		Rect__ctor_m1220545469(&L_54, L_50, L_51, (((float)((float)L_52))), (((float)((float)L_53))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_55 = GUI_Button_m3054448581(NULL /*static, unused*/, L_54, _stringLiteral2410366290, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0253;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_56 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		GK_TBM_Match_t132033130 * L_57 = TBM_Multiplayer_Example_get_CurrentMatch_m2767251338(__this, /*hidden argument*/NULL);
		NullCheck(L_57);
		String_t* L_58 = GK_TBM_Match_get_UTF8StringData_m69722767(L_57, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_59 = String_Concat_m2596409543(NULL /*static, unused*/, L_58, _stringLiteral372029430, /*hidden argument*/NULL);
		NullCheck(L_56);
		ByteU5BU5D_t3397334013* L_60 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_56, L_59);
		V_0 = L_60;
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t3940119580_il2cpp_TypeInfo_var);
		GameCenter_TBM_t3457554475 * L_61 = Singleton_1_get_Instance_m85090890(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m85090890_MethodInfo_var);
		GK_TBM_Match_t132033130 * L_62 = TBM_Multiplayer_Example_get_CurrentMatch_m2767251338(__this, /*hidden argument*/NULL);
		NullCheck(L_62);
		String_t* L_63 = L_62->get_Id_0();
		ByteU5BU5D_t3397334013* L_64 = V_0;
		NullCheck(L_61);
		GameCenter_TBM_SaveCurrentTurn_m1797122022(L_61, L_63, L_64, /*hidden argument*/NULL);
		IntPtr_t L_65;
		L_65.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_ActionMatchDataUpdated_m72619858_MethodInfo_var);
		Action_1_t1157805416 * L_66 = (Action_1_t1157805416 *)il2cpp_codegen_object_new(Action_1_t1157805416_il2cpp_TypeInfo_var);
		Action_1__ctor_m2637091011(L_66, __this, L_65, /*hidden argument*/Action_1__ctor_m2637091011_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_add_ActionMatchDataUpdated_m4255370712(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
	}

IL_0253:
	{
		float L_67 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_68 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_XButtonStep_9();
		((BaseIOSFeaturePreview_t3055692840 *)__this)->set_StartX_6(((float)((float)L_67+(float)L_68)));
		float L_69 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_70 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartY_5();
		int32_t L_71 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonWidth_3();
		int32_t L_72 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonHeight_4();
		Rect_t3681755626  L_73;
		memset(&L_73, 0, sizeof(L_73));
		Rect__ctor_m1220545469(&L_73, L_69, L_70, (((float)((float)L_71))), (((float)((float)L_72))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_74 = GUI_Button_m3054448581(NULL /*static, unused*/, L_73, _stringLiteral2215163754, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_0342;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_75 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_75);
		ByteU5BU5D_t3397334013* L_76 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_75, _stringLiteral2765270333);
		V_1 = L_76;
		GK_TBM_Match_t132033130 * L_77 = TBM_Multiplayer_Example_get_CurrentMatch_m2767251338(__this, /*hidden argument*/NULL);
		NullCheck(L_77);
		GK_TBM_Participant_t3803955090 * L_78 = L_77->get_CurrentParticipant_2();
		NullCheck(L_78);
		GK_TBM_Participant_SetOutcome_m992397011(L_78, 6, /*hidden argument*/NULL);
		GK_TBM_Match_t132033130 * L_79 = TBM_Multiplayer_Example_get_CurrentMatch_m2767251338(__this, /*hidden argument*/NULL);
		NullCheck(L_79);
		List_1_t3173076222 * L_80 = L_79->get_Participants_6();
		NullCheck(L_80);
		Enumerator_t2707805896  L_81 = List_1_GetEnumerator_m694581298(L_80, /*hidden argument*/List_1_GetEnumerator_m694581298_MethodInfo_var);
		V_3 = L_81;
	}

IL_02c6:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0325;
		}

IL_02cb:
		{
			GK_TBM_Participant_t3803955090 * L_82 = Enumerator_get_Current_m1433138044((&V_3), /*hidden argument*/Enumerator_get_Current_m1433138044_MethodInfo_var);
			V_2 = L_82;
			GK_TBM_Participant_t3803955090 * L_83 = V_2;
			NullCheck(L_83);
			String_t* L_84 = GK_TBM_Participant_get_PlayerId_m3280637819(L_83, /*hidden argument*/NULL);
			GK_TBM_Match_t132033130 * L_85 = TBM_Multiplayer_Example_get_CurrentMatch_m2767251338(__this, /*hidden argument*/NULL);
			NullCheck(L_85);
			GK_TBM_Participant_t3803955090 * L_86 = L_85->get_CurrentParticipant_2();
			NullCheck(L_86);
			String_t* L_87 = GK_TBM_Participant_get_PlayerId_m3280637819(L_86, /*hidden argument*/NULL);
			NullCheck(L_84);
			bool L_88 = String_Equals_m2633592423(L_84, L_87, /*hidden argument*/NULL);
			if (L_88)
			{
				goto IL_0325;
			}
		}

IL_02f3:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t3940119580_il2cpp_TypeInfo_var);
			GameCenter_TBM_t3457554475 * L_89 = Singleton_1_get_Instance_m85090890(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m85090890_MethodInfo_var);
			GK_TBM_Match_t132033130 * L_90 = TBM_Multiplayer_Example_get_CurrentMatch_m2767251338(__this, /*hidden argument*/NULL);
			NullCheck(L_90);
			String_t* L_91 = L_90->get_Id_0();
			ByteU5BU5D_t3397334013* L_92 = V_1;
			GK_TBM_Participant_t3803955090 * L_93 = V_2;
			NullCheck(L_93);
			String_t* L_94 = GK_TBM_Participant_get_PlayerId_m3280637819(L_93, /*hidden argument*/NULL);
			NullCheck(L_89);
			GameCenter_TBM_EndTurn_m851226489(L_89, L_91, L_92, L_94, /*hidden argument*/NULL);
			IntPtr_t L_95;
			L_95.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_ActionTrunEnded_m1257330973_MethodInfo_var);
			Action_1_t1319180072 * L_96 = (Action_1_t1319180072 *)il2cpp_codegen_object_new(Action_1_t1319180072_il2cpp_TypeInfo_var);
			Action_1__ctor_m2764520851(L_96, __this, L_95, /*hidden argument*/Action_1__ctor_m2764520851_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
			GameCenter_TBM_add_ActionTrunEnded_m2225327805(NULL /*static, unused*/, L_96, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x451, FINALLY_0336);
		}

IL_0325:
		{
			bool L_97 = Enumerator_MoveNext_m3208953854((&V_3), /*hidden argument*/Enumerator_MoveNext_m3208953854_MethodInfo_var);
			if (L_97)
			{
				goto IL_02cb;
			}
		}

IL_0331:
		{
			IL2CPP_LEAVE(0x342, FINALLY_0336);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0336;
	}

FINALLY_0336:
	{ // begin finally (depth: 1)
		Enumerator_t2707805896  L_98 = V_3;
		Enumerator_t2707805896  L_99 = L_98;
		Il2CppObject * L_100 = Box(Enumerator_t2707805896_il2cpp_TypeInfo_var, &L_99);
		NullCheck((Il2CppObject *)L_100);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_100);
		IL2CPP_END_FINALLY(822)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(822)
	{
		IL2CPP_JUMP_TBL(0x451, IL_0451)
		IL2CPP_JUMP_TBL(0x342, IL_0342)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0342:
	{
		float L_101 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_102 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_XButtonStep_9();
		((BaseIOSFeaturePreview_t3055692840 *)__this)->set_StartX_6(((float)((float)L_101+(float)L_102)));
		float L_103 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_104 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartY_5();
		int32_t L_105 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonWidth_3();
		int32_t L_106 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonHeight_4();
		Rect_t3681755626  L_107;
		memset(&L_107, 0, sizeof(L_107));
		Rect__ctor_m1220545469(&L_107, L_103, L_104, (((float)((float)L_105))), (((float)((float)L_106))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_108 = GUI_Button_m3054448581(NULL /*static, unused*/, L_107, _stringLiteral1993840386, /*hidden argument*/NULL);
		if (!L_108)
		{
			goto IL_03ea;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_109 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_109);
		ByteU5BU5D_t3397334013* L_110 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_109, _stringLiteral813573746);
		V_4 = L_110;
		GK_TBM_Match_t132033130 * L_111 = TBM_Multiplayer_Example_get_CurrentMatch_m2767251338(__this, /*hidden argument*/NULL);
		NullCheck(L_111);
		List_1_t3173076222 * L_112 = L_111->get_Participants_6();
		NullCheck(L_112);
		GK_TBM_Participant_t3803955090 * L_113 = List_1_get_Item_m4160213088(L_112, 0, /*hidden argument*/List_1_get_Item_m4160213088_MethodInfo_var);
		NullCheck(L_113);
		GK_TBM_Participant_SetOutcome_m992397011(L_113, 2, /*hidden argument*/NULL);
		GK_TBM_Match_t132033130 * L_114 = TBM_Multiplayer_Example_get_CurrentMatch_m2767251338(__this, /*hidden argument*/NULL);
		NullCheck(L_114);
		List_1_t3173076222 * L_115 = L_114->get_Participants_6();
		NullCheck(L_115);
		GK_TBM_Participant_t3803955090 * L_116 = List_1_get_Item_m4160213088(L_115, 1, /*hidden argument*/List_1_get_Item_m4160213088_MethodInfo_var);
		NullCheck(L_116);
		GK_TBM_Participant_SetOutcome_m992397011(L_116, 3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t3940119580_il2cpp_TypeInfo_var);
		GameCenter_TBM_t3457554475 * L_117 = Singleton_1_get_Instance_m85090890(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m85090890_MethodInfo_var);
		GK_TBM_Match_t132033130 * L_118 = TBM_Multiplayer_Example_get_CurrentMatch_m2767251338(__this, /*hidden argument*/NULL);
		NullCheck(L_118);
		String_t* L_119 = L_118->get_Id_0();
		ByteU5BU5D_t3397334013* L_120 = V_4;
		NullCheck(L_117);
		GameCenter_TBM_EndMatch_m3469373837(L_117, L_119, L_120, /*hidden argument*/NULL);
		IntPtr_t L_121;
		L_121.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_ActionMacthEnded_m2135114273_MethodInfo_var);
		Action_1_t3263568192 * L_122 = (Action_1_t3263568192 *)il2cpp_codegen_object_new(Action_1_t3263568192_il2cpp_TypeInfo_var);
		Action_1__ctor_m1524944681(L_122, __this, L_121, /*hidden argument*/Action_1__ctor_m1524944681_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_add_ActionMacthEnded_m1847574209(NULL /*static, unused*/, L_122, /*hidden argument*/NULL);
	}

IL_03ea:
	{
		float L_123 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_124 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_XButtonStep_9();
		((BaseIOSFeaturePreview_t3055692840 *)__this)->set_StartX_6(((float)((float)L_123+(float)L_124)));
		float L_125 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartX_6();
		float L_126 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_StartY_5();
		int32_t L_127 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonWidth_3();
		int32_t L_128 = ((BaseIOSFeaturePreview_t3055692840 *)__this)->get_buttonHeight_4();
		Rect_t3681755626  L_129;
		memset(&L_129, 0, sizeof(L_129));
		Rect__ctor_m1220545469(&L_129, L_125, L_126, (((float)((float)L_127))), (((float)((float)L_128))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_130 = GUI_Button_m3054448581(NULL /*static, unused*/, L_129, _stringLiteral3705768981, /*hidden argument*/NULL);
		if (!L_130)
		{
			goto IL_0451;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t3940119580_il2cpp_TypeInfo_var);
		GameCenter_TBM_t3457554475 * L_131 = Singleton_1_get_Instance_m85090890(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m85090890_MethodInfo_var);
		GK_TBM_Match_t132033130 * L_132 = TBM_Multiplayer_Example_get_CurrentMatch_m2767251338(__this, /*hidden argument*/NULL);
		NullCheck(L_132);
		String_t* L_133 = L_132->get_Id_0();
		NullCheck(L_131);
		GameCenter_TBM_RemoveMatch_m2792781327(L_131, L_133, /*hidden argument*/NULL);
		IntPtr_t L_134;
		L_134.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_ActionMacthRemoved_m1026773452_MethodInfo_var);
		Action_1_t710925695 * L_135 = (Action_1_t710925695 *)il2cpp_codegen_object_new(Action_1_t710925695_il2cpp_TypeInfo_var);
		Action_1__ctor_m2133134992(L_135, __this, L_134, /*hidden argument*/Action_1__ctor_m2133134992_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_add_ActionMatchRemoved_m1720421448(NULL /*static, unused*/, L_135, /*hidden argument*/NULL);
	}

IL_0451:
	{
		return;
	}
}
// System.Void TBM_Multiplayer_Example::OnAuthFinished(SA.Common.Models.Result)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2874532579;
extern const uint32_t TBM_Multiplayer_Example_OnAuthFinished_m469706619_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_OnAuthFinished_m469706619 (TBM_Multiplayer_Example_t1159631235 * __this, Result_t4287219743 * ___res0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_OnAuthFinished_m469706619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Result_t4287219743 * L_0 = ___res0;
		NullCheck(L_0);
		bool L_1 = Result_get_IsSucceeded_m955312237(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = Boolean_ToString_m1253164328((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2874532579, L_2, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_3, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TBM_Multiplayer_Example::ActionMatchesResultLoaded(GK_TBM_LoadMatchesResult)
extern Il2CppClass* Action_1_t172291117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t3366837094_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_ActionMatchesResultLoaded_m2973444938_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2617219004_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m3026227290_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m3820473346_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2660140860_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1743401971_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m464999297_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2199162803;
extern const uint32_t TBM_Multiplayer_Example_ActionMatchesResultLoaded_m2973444938_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_ActionMatchesResultLoaded_m2973444938 (TBM_Multiplayer_Example_t1159631235 * __this, GK_TBM_LoadMatchesResult_t370491735 * ___res0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_ActionMatchesResultLoaded_m2973444938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t4099124910  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3366837094  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GK_TBM_Match_t132033130 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_ActionMatchesResultLoaded_m2973444938_MethodInfo_var);
		Action_1_t172291117 * L_1 = (Action_1_t172291117 *)il2cpp_codegen_object_new(Action_1_t172291117_il2cpp_TypeInfo_var);
		Action_1__ctor_m2617219004(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m2617219004_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_remove_ActionMatchesInfoLoaded_m3664428832(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GK_TBM_LoadMatchesResult_t370491735 * L_2 = ___res0;
		NullCheck(L_2);
		bool L_3 = Result_get_IsSucceeded_m955312237(L_2, /*hidden argument*/NULL);
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2199162803, L_5, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_6, 3, /*hidden argument*/NULL);
		GK_TBM_LoadMatchesResult_t370491735 * L_7 = ___res0;
		NullCheck(L_7);
		bool L_8 = Result_get_IsFailed_m2142044049(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0038;
		}
	}
	{
		return;
	}

IL_0038:
	{
		GK_TBM_LoadMatchesResult_t370491735 * L_9 = ___res0;
		NullCheck(L_9);
		Dictionary_2_t2046812392 * L_10 = L_9->get_LoadedMatches_1();
		NullCheck(L_10);
		int32_t L_11 = Dictionary_2_get_Count_m3026227290(L_10, /*hidden argument*/Dictionary_2_get_Count_m3026227290_MethodInfo_var);
		if (L_11)
		{
			goto IL_0049;
		}
	}
	{
		return;
	}

IL_0049:
	{
		GK_TBM_LoadMatchesResult_t370491735 * L_12 = ___res0;
		NullCheck(L_12);
		Dictionary_2_t2046812392 * L_13 = L_12->get_LoadedMatches_1();
		NullCheck(L_13);
		Enumerator_t3366837094  L_14 = Dictionary_2_GetEnumerator_m3820473346(L_13, /*hidden argument*/Dictionary_2_GetEnumerator_m3820473346_MethodInfo_var);
		V_1 = L_14;
	}

IL_0055:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0070;
		}

IL_005a:
		{
			KeyValuePair_2_t4099124910  L_15 = Enumerator_get_Current_m2660140860((&V_1), /*hidden argument*/Enumerator_get_Current_m2660140860_MethodInfo_var);
			V_0 = L_15;
			GK_TBM_Match_t132033130 * L_16 = KeyValuePair_2_get_Value_m1743401971((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1743401971_MethodInfo_var);
			V_2 = L_16;
			GK_TBM_Match_t132033130 * L_17 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
			GameCenter_TBM_PrintMatchInfo_m1940379666(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		}

IL_0070:
		{
			bool L_18 = Enumerator_MoveNext_m464999297((&V_1), /*hidden argument*/Enumerator_MoveNext_m464999297_MethodInfo_var);
			if (L_18)
			{
				goto IL_005a;
			}
		}

IL_007c:
		{
			IL2CPP_LEAVE(0x8D, FINALLY_0081);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0081;
	}

FINALLY_0081:
	{ // begin finally (depth: 1)
		Enumerator_t3366837094  L_19 = V_1;
		Enumerator_t3366837094  L_20 = L_19;
		Il2CppObject * L_21 = Box(Enumerator_t3366837094_il2cpp_TypeInfo_var, &L_20);
		NullCheck((Il2CppObject *)L_21);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_21);
		IL2CPP_END_FINALLY(129)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(129)
	{
		IL2CPP_JUMP_TBL(0x8D, IL_008d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008d:
	{
		return;
	}
}
// System.Void TBM_Multiplayer_Example::ActionMatchDataUpdated(GK_TBM_MatchDataUpdateResult)
extern Il2CppClass* Action_1_t1157805416_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_ActionMatchDataUpdated_m72619858_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2637091011_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1249115610;
extern const uint32_t TBM_Multiplayer_Example_ActionMatchDataUpdated_m72619858_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_ActionMatchDataUpdated_m72619858 (TBM_Multiplayer_Example_t1159631235 * __this, GK_TBM_MatchDataUpdateResult_t1356006034 * ___res0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_ActionMatchDataUpdated_m72619858_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_ActionMatchDataUpdated_m72619858_MethodInfo_var);
		Action_1_t1157805416 * L_1 = (Action_1_t1157805416 *)il2cpp_codegen_object_new(Action_1_t1157805416_il2cpp_TypeInfo_var);
		Action_1__ctor_m2637091011(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m2637091011_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_remove_ActionMatchDataUpdated_m287121403(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GK_TBM_MatchDataUpdateResult_t1356006034 * L_2 = ___res0;
		NullCheck(L_2);
		bool L_3 = Result_get_IsSucceeded_m955312237(L_2, /*hidden argument*/NULL);
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1249115610, L_5, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_6, 3, /*hidden argument*/NULL);
		GK_TBM_MatchDataUpdateResult_t1356006034 * L_7 = ___res0;
		NullCheck(L_7);
		bool L_8 = Result_get_IsFailed_m2142044049(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		GK_TBM_MatchDataUpdateResult_t1356006034 * L_9 = ___res0;
		NullCheck(L_9);
		Error_t445207774 * L_10 = Result_get_Error_m3741808192(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = Error_get_Message_m419911227(L_10, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_11, 3, /*hidden argument*/NULL);
		goto IL_0058;
	}

IL_004d:
	{
		GK_TBM_MatchDataUpdateResult_t1356006034 * L_12 = ___res0;
		NullCheck(L_12);
		GK_TBM_Match_t132033130 * L_13 = GK_TBM_MatchDataUpdateResult_get_Match_m1666389990(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_PrintMatchInfo_m1940379666(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void TBM_Multiplayer_Example::ActionTrunEnded(GK_TBM_EndTrunResult)
extern Il2CppClass* Action_1_t1319180072_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_ActionTrunEnded_m1257330973_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2764520851_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3549943156;
extern Il2CppCodeGenString* _stringLiteral2731676137;
extern const uint32_t TBM_Multiplayer_Example_ActionTrunEnded_m1257330973_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_ActionTrunEnded_m1257330973 (TBM_Multiplayer_Example_t1159631235 * __this, GK_TBM_EndTrunResult_t1517380690 * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_ActionTrunEnded_m1257330973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_ActionTrunEnded_m1257330973_MethodInfo_var);
		Action_1_t1319180072 * L_1 = (Action_1_t1319180072 *)il2cpp_codegen_object_new(Action_1_t1319180072_il2cpp_TypeInfo_var);
		Action_1__ctor_m2764520851(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m2764520851_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_remove_ActionTrunEnded_m1803575896(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GK_TBM_EndTrunResult_t1517380690 * L_2 = ___result0;
		NullCheck(L_2);
		bool L_3 = Result_get_IsSucceeded_m955312237(L_2, /*hidden argument*/NULL);
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3549943156, L_5, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_6, 3, /*hidden argument*/NULL);
		GK_TBM_EndTrunResult_t1517380690 * L_7 = ___result0;
		NullCheck(L_7);
		bool L_8 = Result_get_IsFailed_m2142044049(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0063;
		}
	}
	{
		GK_TBM_EndTrunResult_t1517380690 * L_9 = ___result0;
		NullCheck(L_9);
		Error_t445207774 * L_10 = Result_get_Error_m3741808192(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = Error_get_Message_m419911227(L_10, /*hidden argument*/NULL);
		IOSMessage_Create_m3899886368(NULL /*static, unused*/, _stringLiteral2731676137, L_11, /*hidden argument*/NULL);
		GK_TBM_EndTrunResult_t1517380690 * L_12 = ___result0;
		NullCheck(L_12);
		Error_t445207774 * L_13 = Result_get_Error_m3741808192(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		String_t* L_14 = Error_get_Message_m419911227(L_13, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_14, 3, /*hidden argument*/NULL);
		goto IL_006e;
	}

IL_0063:
	{
		GK_TBM_EndTrunResult_t1517380690 * L_15 = ___result0;
		NullCheck(L_15);
		GK_TBM_Match_t132033130 * L_16 = GK_TBM_EndTrunResult_get_Match_m1400348774(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_PrintMatchInfo_m1940379666(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
	}

IL_006e:
	{
		return;
	}
}
// System.Void TBM_Multiplayer_Example::ActionMacthEnded(GK_TBM_MatchEndResult)
extern Il2CppClass* Action_1_t3263568192_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_ActionMacthEnded_m2135114273_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1524944681_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral978883854;
extern const uint32_t TBM_Multiplayer_Example_ActionMacthEnded_m2135114273_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_ActionMacthEnded_m2135114273 (TBM_Multiplayer_Example_t1159631235 * __this, GK_TBM_MatchEndResult_t3461768810 * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_ActionMacthEnded_m2135114273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_ActionMacthEnded_m2135114273_MethodInfo_var);
		Action_1_t3263568192 * L_1 = (Action_1_t3263568192 *)il2cpp_codegen_object_new(Action_1_t3263568192_il2cpp_TypeInfo_var);
		Action_1__ctor_m1524944681(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m1524944681_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_remove_ActionMacthEnded_m1318723784(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GK_TBM_MatchEndResult_t3461768810 * L_2 = ___result0;
		NullCheck(L_2);
		bool L_3 = Result_get_IsSucceeded_m955312237(L_2, /*hidden argument*/NULL);
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral978883854, L_5, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_6, 3, /*hidden argument*/NULL);
		GK_TBM_MatchEndResult_t3461768810 * L_7 = ___result0;
		NullCheck(L_7);
		bool L_8 = Result_get_IsFailed_m2142044049(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		GK_TBM_MatchEndResult_t3461768810 * L_9 = ___result0;
		NullCheck(L_9);
		Error_t445207774 * L_10 = Result_get_Error_m3741808192(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = Error_get_Message_m419911227(L_10, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_11, 3, /*hidden argument*/NULL);
		goto IL_0058;
	}

IL_004d:
	{
		GK_TBM_MatchEndResult_t3461768810 * L_12 = ___result0;
		NullCheck(L_12);
		GK_TBM_Match_t132033130 * L_13 = GK_TBM_MatchEndResult_get_Match_m698835188(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_PrintMatchInfo_m1940379666(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void TBM_Multiplayer_Example::ActionMacthRemoved(GK_TBM_MatchRemovedResult)
extern Il2CppClass* Action_1_t710925695_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_ActionMacthRemoved_m1026773452_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2133134992_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4287446946;
extern Il2CppCodeGenString* _stringLiteral3926096630;
extern const uint32_t TBM_Multiplayer_Example_ActionMacthRemoved_m1026773452_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_ActionMacthRemoved_m1026773452 (TBM_Multiplayer_Example_t1159631235 * __this, GK_TBM_MatchRemovedResult_t909126313 * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_ActionMacthRemoved_m1026773452_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_ActionMacthRemoved_m1026773452_MethodInfo_var);
		Action_1_t710925695 * L_1 = (Action_1_t710925695 *)il2cpp_codegen_object_new(Action_1_t710925695_il2cpp_TypeInfo_var);
		Action_1__ctor_m2133134992(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m2133134992_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_remove_ActionMatchRemoved_m4248501503(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GK_TBM_MatchRemovedResult_t909126313 * L_2 = ___result0;
		NullCheck(L_2);
		bool L_3 = Result_get_IsSucceeded_m955312237(L_2, /*hidden argument*/NULL);
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral4287446946, L_5, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_6, 3, /*hidden argument*/NULL);
		GK_TBM_MatchRemovedResult_t909126313 * L_7 = ___result0;
		NullCheck(L_7);
		bool L_8 = Result_get_IsFailed_m2142044049(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		GK_TBM_MatchRemovedResult_t909126313 * L_9 = ___result0;
		NullCheck(L_9);
		Error_t445207774 * L_10 = Result_get_Error_m3741808192(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = Error_get_Message_m419911227(L_10, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_11, 3, /*hidden argument*/NULL);
		goto IL_0063;
	}

IL_004d:
	{
		GK_TBM_MatchRemovedResult_t909126313 * L_12 = ___result0;
		NullCheck(L_12);
		String_t* L_13 = GK_TBM_MatchRemovedResult_get_MatchId_m3559544014(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3926096630, L_13, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_14, 3, /*hidden argument*/NULL);
	}

IL_0063:
	{
		return;
	}
}
// GK_TBM_Match TBM_Multiplayer_Example::get_CurrentMatch()
extern Il2CppClass* Singleton_1_t3940119580_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_Instance_m85090890_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m3026227290_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m539466384_MethodInfo_var;
extern const uint32_t TBM_Multiplayer_Example_get_CurrentMatch_m2767251338_MetadataUsageId;
extern "C"  GK_TBM_Match_t132033130 * TBM_Multiplayer_Example_get_CurrentMatch_m2767251338 (TBM_Multiplayer_Example_t1159631235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_get_CurrentMatch_m2767251338_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t3940119580_il2cpp_TypeInfo_var);
		GameCenter_TBM_t3457554475 * L_0 = Singleton_1_get_Instance_m85090890(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m85090890_MethodInfo_var);
		NullCheck(L_0);
		Dictionary_2_t2046812392 * L_1 = GameCenter_TBM_get_Matches_m1703533532(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Dictionary_2_get_Count_m3026227290(L_1, /*hidden argument*/Dictionary_2_get_Count_m3026227290_MethodInfo_var);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t3940119580_il2cpp_TypeInfo_var);
		GameCenter_TBM_t3457554475 * L_3 = Singleton_1_get_Instance_m85090890(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m85090890_MethodInfo_var);
		NullCheck(L_3);
		List_1_t3796121558 * L_4 = GameCenter_TBM_get_MatchesList_m2702417999(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GK_TBM_Match_t132033130 * L_5 = List_1_get_Item_m539466384(L_4, 0, /*hidden argument*/List_1_get_Item_m539466384_MethodInfo_var);
		return L_5;
	}

IL_0026:
	{
		return (GK_TBM_Match_t132033130 *)NULL;
	}
}
// System.Void TBM_Multiplayer_Example::ActionMatchFound(GK_TBM_MatchInitResult)
extern Il2CppClass* Action_1_t3649630279_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* TBM_Multiplayer_Example_ActionMatchFound_m2262552008_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1880827218_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral696385752;
extern const uint32_t TBM_Multiplayer_Example_ActionMatchFound_m2262552008_MetadataUsageId;
extern "C"  void TBM_Multiplayer_Example_ActionMatchFound_m2262552008 (TBM_Multiplayer_Example_t1159631235 * __this, GK_TBM_MatchInitResult_t3847830897 * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TBM_Multiplayer_Example_ActionMatchFound_m2262552008_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TBM_Multiplayer_Example_ActionMatchFound_m2262552008_MethodInfo_var);
		Action_1_t3649630279 * L_1 = (Action_1_t3649630279 *)il2cpp_codegen_object_new(Action_1_t3649630279_il2cpp_TypeInfo_var);
		Action_1__ctor_m1880827218(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m1880827218_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_remove_ActionMatchFound_m1195516151(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GK_TBM_MatchInitResult_t3847830897 * L_2 = ___result0;
		NullCheck(L_2);
		bool L_3 = Result_get_IsSucceeded_m955312237(L_2, /*hidden argument*/NULL);
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral696385752, L_5, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_6, 3, /*hidden argument*/NULL);
		GK_TBM_MatchInitResult_t3847830897 * L_7 = ___result0;
		NullCheck(L_7);
		bool L_8 = Result_get_IsFailed_m2142044049(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		GK_TBM_MatchInitResult_t3847830897 * L_9 = ___result0;
		NullCheck(L_9);
		Error_t445207774 * L_10 = Result_get_Error_m3741808192(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = Error_get_Message_m419911227(L_10, /*hidden argument*/NULL);
		ISN_Logger_Log_m3550720543(NULL /*static, unused*/, L_11, 3, /*hidden argument*/NULL);
		goto IL_0058;
	}

IL_004d:
	{
		GK_TBM_MatchInitResult_t3847830897 * L_12 = ___result0;
		NullCheck(L_12);
		GK_TBM_Match_t132033130 * L_13 = GK_TBM_MatchInitResult_get_Match_m2586326451(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenter_TBM_t3457554475_il2cpp_TypeInfo_var);
		GameCenter_TBM_PrintMatchInfo_m1940379666(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void TvOsCloudExample::.ctor()
extern "C"  void TvOsCloudExample__ctor_m1384387766 (TvOsCloudExample_t1625417505 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TvOsCloudExample::Start()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2882436870_il2cpp_TypeInfo_var;
extern Il2CppClass* iCloudManager_t2506189173_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2988754278_il2cpp_TypeInfo_var;
extern const MethodInfo* TvOsCloudExample_OnCloudDataReceivedAction_m3322197275_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3601282379_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_Instance_m1107454056_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral320574433;
extern Il2CppCodeGenString* _stringLiteral3423762534;
extern Il2CppCodeGenString* _stringLiteral3423761286;
extern const uint32_t TvOsCloudExample_Start_m1785053202_MetadataUsageId;
extern "C"  void TvOsCloudExample_Start_m1785053202 (TvOsCloudExample_t1625417505 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TvOsCloudExample_Start_m1785053202_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral320574433, /*hidden argument*/NULL);
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TvOsCloudExample_OnCloudDataReceivedAction_m3322197275_MethodInfo_var);
		Action_1_t2882436870 * L_1 = (Action_1_t2882436870 *)il2cpp_codegen_object_new(Action_1_t2882436870_il2cpp_TypeInfo_var);
		Action_1__ctor_m3601282379(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m3601282379_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(iCloudManager_t2506189173_il2cpp_TypeInfo_var);
		iCloudManager_add_OnCloudDataReceivedAction_m2337728703(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2988754278_il2cpp_TypeInfo_var);
		iCloudManager_t2506189173 * L_2 = Singleton_1_get_Instance_m1107454056(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m1107454056_MethodInfo_var);
		NullCheck(L_2);
		iCloudManager_setString_m892730585(L_2, _stringLiteral3423762534, _stringLiteral3423761286, /*hidden argument*/NULL);
		iCloudManager_t2506189173 * L_3 = Singleton_1_get_Instance_m1107454056(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m1107454056_MethodInfo_var);
		NullCheck(L_3);
		iCloudManager_requestDataForKey_m1824626745(L_3, _stringLiteral3423762534, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TvOsCloudExample::OnCloudDataReceivedAction(iCloudData)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral916666509;
extern Il2CppCodeGenString* _stringLiteral377719534;
extern Il2CppCodeGenString* _stringLiteral57471863;
extern const uint32_t TvOsCloudExample_OnCloudDataReceivedAction_m3322197275_MetadataUsageId;
extern "C"  void TvOsCloudExample_OnCloudDataReceivedAction_m3322197275 (TvOsCloudExample_t1625417505 * __this, iCloudData_t3080637488 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TvOsCloudExample_OnCloudDataReceivedAction_m3322197275_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral916666509, /*hidden argument*/NULL);
		iCloudData_t3080637488 * L_0 = ___data0;
		NullCheck(L_0);
		bool L_1 = iCloudData_get_IsEmpty_m3971846247(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		iCloudData_t3080637488 * L_2 = ___data0;
		NullCheck(L_2);
		String_t* L_3 = iCloudData_get_key_m3393039034(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, L_3, _stringLiteral377719534, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_004a;
	}

IL_002f:
	{
		iCloudData_t3080637488 * L_5 = ___data0;
		NullCheck(L_5);
		String_t* L_6 = iCloudData_get_key_m3393039034(L_5, /*hidden argument*/NULL);
		iCloudData_t3080637488 * L_7 = ___data0;
		NullCheck(L_7);
		String_t* L_8 = iCloudData_get_stringValue_m413251455(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m612901809(NULL /*static, unused*/, L_6, _stringLiteral57471863, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void TvOSInAppsExample::.ctor()
extern "C"  void TvOSInAppsExample__ctor_m1961606928 (TvOSInAppsExample_t752347109 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TvOSInAppsExample::Init()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* PaymentManagerExample_t3283960187_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2126706816;
extern const uint32_t TvOSInAppsExample_Init_m403571852_MetadataUsageId;
extern "C"  void TvOSInAppsExample_Init_m403571852 (TvOSInAppsExample_t752347109 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TvOSInAppsExample_Init_m403571852_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2126706816, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PaymentManagerExample_t3283960187_il2cpp_TypeInfo_var);
		PaymentManagerExample_init_m1458338142(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TvOSInAppsExample::Buy()
extern Il2CppClass* PaymentManagerExample_t3283960187_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3268315178;
extern const uint32_t TvOSInAppsExample_Buy_m1752436844_MetadataUsageId;
extern "C"  void TvOSInAppsExample_Buy_m1752436844 (TvOSInAppsExample_t752347109 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TvOSInAppsExample_Buy_m1752436844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PaymentManagerExample_t3283960187_il2cpp_TypeInfo_var);
		PaymentManagerExample_buyItem_m2963797393(NULL /*static, unused*/, _stringLiteral3268315178, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
