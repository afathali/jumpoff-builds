﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1
struct U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::.ctor()
extern "C"  void U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1__ctor_m267654728 (U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3419169766 (U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m155824254 (U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::MoveNext()
extern "C"  bool U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_MoveNext_m3251078568 (U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::Dispose()
extern "C"  void U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_Dispose_m107912071 (U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::Reset()
extern "C"  void U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_Reset_m97897717 (U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
