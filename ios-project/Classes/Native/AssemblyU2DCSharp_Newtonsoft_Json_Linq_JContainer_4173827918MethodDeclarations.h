﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer
struct JTokenReferenceEqualityComparer_t4173827918;
// Newtonsoft.Json.Linq.JToken
struct JToken_t2552644013;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken2552644013.h"

// System.Void Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer::.ctor()
extern "C"  void JTokenReferenceEqualityComparer__ctor_m1638203977 (JTokenReferenceEqualityComparer_t4173827918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer::.cctor()
extern "C"  void JTokenReferenceEqualityComparer__cctor_m1158126264 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer::Equals(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  bool JTokenReferenceEqualityComparer_Equals_m947685732 (JTokenReferenceEqualityComparer_t4173827918 * __this, JToken_t2552644013 * ___x0, JToken_t2552644013 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer::GetHashCode(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JTokenReferenceEqualityComparer_GetHashCode_m2757445912 (JTokenReferenceEqualityComparer_t4173827918 * __this, JToken_t2552644013 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
