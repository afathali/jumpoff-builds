﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidAppInfoLoader
struct AndroidAppInfoLoader_t2209520163;
// System.Action`1<PackageAppInfo>
struct Action_1_t62711973;
// System.String
struct String_t;
// PackageAppInfo
struct PackageAppInfo_t260912591;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PackageAppInfo260912591.h"

// System.Void AndroidAppInfoLoader::.ctor()
extern "C"  void AndroidAppInfoLoader__ctor_m739909736 (AndroidAppInfoLoader_t2209520163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAppInfoLoader::.cctor()
extern "C"  void AndroidAppInfoLoader__cctor_m3953580725 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAppInfoLoader::add_ActionPacakgeInfoLoaded(System.Action`1<PackageAppInfo>)
extern "C"  void AndroidAppInfoLoader_add_ActionPacakgeInfoLoaded_m3527847174 (Il2CppObject * __this /* static, unused */, Action_1_t62711973 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAppInfoLoader::remove_ActionPacakgeInfoLoaded(System.Action`1<PackageAppInfo>)
extern "C"  void AndroidAppInfoLoader_remove_ActionPacakgeInfoLoaded_m3998589345 (Il2CppObject * __this /* static, unused */, Action_1_t62711973 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAppInfoLoader::LoadPackageInfo()
extern "C"  void AndroidAppInfoLoader_LoadPackageInfo_m3479998212 (AndroidAppInfoLoader_t2209520163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAppInfoLoader::OnPackageInfoLoaded(System.String)
extern "C"  void AndroidAppInfoLoader_OnPackageInfoLoaded_m2351233712 (AndroidAppInfoLoader_t2209520163 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAppInfoLoader::<ActionPacakgeInfoLoaded>m__18(PackageAppInfo)
extern "C"  void AndroidAppInfoLoader_U3CActionPacakgeInfoLoadedU3Em__18_m182848962 (Il2CppObject * __this /* static, unused */, PackageAppInfo_t260912591 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
