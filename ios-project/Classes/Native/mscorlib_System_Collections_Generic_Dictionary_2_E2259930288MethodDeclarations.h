﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m217613849(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2259930288 *, Dictionary_2_t939905586 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3924595452(__this, method) ((  Il2CppObject * (*) (Enumerator_t2259930288 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m234295202(__this, method) ((  void (*) (Enumerator_t2259930288 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2298601823(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2259930288 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3978156766(__this, method) ((  Il2CppObject * (*) (Enumerator_t2259930288 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1585042808(__this, method) ((  Il2CppObject * (*) (Enumerator_t2259930288 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::MoveNext()
#define Enumerator_MoveNext_m1504900534(__this, method) ((  bool (*) (Enumerator_t2259930288 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::get_Current()
#define Enumerator_get_Current_m482332966(__this, method) ((  KeyValuePair_2_t2992218104  (*) (Enumerator_t2259930288 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3703276629(__this, method) ((  String_t* (*) (Enumerator_t2259930288 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1608368349(__this, method) ((  GK_SavedGame_t3320093620 * (*) (Enumerator_t2259930288 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::Reset()
#define Enumerator_Reset_m764111567(__this, method) ((  void (*) (Enumerator_t2259930288 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::VerifyState()
#define Enumerator_VerifyState_m2193728130(__this, method) ((  void (*) (Enumerator_t2259930288 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m399950130(__this, method) ((  void (*) (Enumerator_t2259930288 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GK_SavedGame>::Dispose()
#define Enumerator_Dispose_m1047259745(__this, method) ((  void (*) (Enumerator_t2259930288 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
