﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GP_RTM_Network_Package
struct  GP_RTM_Network_Package_t2050307869  : public Il2CppObject
{
public:
	// System.String GP_RTM_Network_Package::_playerId
	String_t* ____playerId_1;
	// System.Byte[] GP_RTM_Network_Package::_buffer
	ByteU5BU5D_t3397334013* ____buffer_2;

public:
	inline static int32_t get_offset_of__playerId_1() { return static_cast<int32_t>(offsetof(GP_RTM_Network_Package_t2050307869, ____playerId_1)); }
	inline String_t* get__playerId_1() const { return ____playerId_1; }
	inline String_t** get_address_of__playerId_1() { return &____playerId_1; }
	inline void set__playerId_1(String_t* value)
	{
		____playerId_1 = value;
		Il2CppCodeGenWriteBarrier(&____playerId_1, value);
	}

	inline static int32_t get_offset_of__buffer_2() { return static_cast<int32_t>(offsetof(GP_RTM_Network_Package_t2050307869, ____buffer_2)); }
	inline ByteU5BU5D_t3397334013* get__buffer_2() const { return ____buffer_2; }
	inline ByteU5BU5D_t3397334013** get_address_of__buffer_2() { return &____buffer_2; }
	inline void set__buffer_2(ByteU5BU5D_t3397334013* value)
	{
		____buffer_2 = value;
		Il2CppCodeGenWriteBarrier(&____buffer_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
