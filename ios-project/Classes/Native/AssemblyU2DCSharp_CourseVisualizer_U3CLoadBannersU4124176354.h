﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t2919945039;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Object
struct Il2CppObject;
// CourseVisualizer
struct CourseVisualizer_t3597553323;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat660383953.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CourseVisualizer/<LoadBanners>c__Iterator0
struct  U3CLoadBannersU3Ec__Iterator0_t4124176354  : public Il2CppObject
{
public:
	// UnityEngine.WWW CourseVisualizer/<LoadBanners>c__Iterator0::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_0;
	// System.String[] CourseVisualizer/<LoadBanners>c__Iterator0::<lines>__1
	StringU5BU5D_t1642385972* ___U3ClinesU3E__1_1;
	// System.String[] CourseVisualizer/<LoadBanners>c__Iterator0::<$s_5>__2
	StringU5BU5D_t1642385972* ___U3CU24s_5U3E__2_2;
	// System.Int32 CourseVisualizer/<LoadBanners>c__Iterator0::<$s_6>__3
	int32_t ___U3CU24s_6U3E__3_3;
	// System.String CourseVisualizer/<LoadBanners>c__Iterator0::<s>__4
	String_t* ___U3CsU3E__4_4;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> CourseVisualizer/<LoadBanners>c__Iterator0::<$s_7>__5
	Enumerator_t660383953  ___U3CU24s_7U3E__5_5;
	// UnityEngine.GameObject CourseVisualizer/<LoadBanners>c__Iterator0::<e>__6
	GameObject_t1756533147 * ___U3CeU3E__6_6;
	// UnityEngine.Texture2D CourseVisualizer/<LoadBanners>c__Iterator0::<tex>__7
	Texture2D_t3542995729 * ___U3CtexU3E__7_7;
	// System.Int32 CourseVisualizer/<LoadBanners>c__Iterator0::$PC
	int32_t ___U24PC_8;
	// System.Object CourseVisualizer/<LoadBanners>c__Iterator0::$current
	Il2CppObject * ___U24current_9;
	// CourseVisualizer CourseVisualizer/<LoadBanners>c__Iterator0::<>f__this
	CourseVisualizer_t3597553323 * ___U3CU3Ef__this_10;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadBannersU3Ec__Iterator0_t4124176354, ___U3CwwwU3E__0_0)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3ClinesU3E__1_1() { return static_cast<int32_t>(offsetof(U3CLoadBannersU3Ec__Iterator0_t4124176354, ___U3ClinesU3E__1_1)); }
	inline StringU5BU5D_t1642385972* get_U3ClinesU3E__1_1() const { return ___U3ClinesU3E__1_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3ClinesU3E__1_1() { return &___U3ClinesU3E__1_1; }
	inline void set_U3ClinesU3E__1_1(StringU5BU5D_t1642385972* value)
	{
		___U3ClinesU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClinesU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_5U3E__2_2() { return static_cast<int32_t>(offsetof(U3CLoadBannersU3Ec__Iterator0_t4124176354, ___U3CU24s_5U3E__2_2)); }
	inline StringU5BU5D_t1642385972* get_U3CU24s_5U3E__2_2() const { return ___U3CU24s_5U3E__2_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CU24s_5U3E__2_2() { return &___U3CU24s_5U3E__2_2; }
	inline void set_U3CU24s_5U3E__2_2(StringU5BU5D_t1642385972* value)
	{
		___U3CU24s_5U3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_5U3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_6U3E__3_3() { return static_cast<int32_t>(offsetof(U3CLoadBannersU3Ec__Iterator0_t4124176354, ___U3CU24s_6U3E__3_3)); }
	inline int32_t get_U3CU24s_6U3E__3_3() const { return ___U3CU24s_6U3E__3_3; }
	inline int32_t* get_address_of_U3CU24s_6U3E__3_3() { return &___U3CU24s_6U3E__3_3; }
	inline void set_U3CU24s_6U3E__3_3(int32_t value)
	{
		___U3CU24s_6U3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CsU3E__4_4() { return static_cast<int32_t>(offsetof(U3CLoadBannersU3Ec__Iterator0_t4124176354, ___U3CsU3E__4_4)); }
	inline String_t* get_U3CsU3E__4_4() const { return ___U3CsU3E__4_4; }
	inline String_t** get_address_of_U3CsU3E__4_4() { return &___U3CsU3E__4_4; }
	inline void set_U3CsU3E__4_4(String_t* value)
	{
		___U3CsU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CU24s_7U3E__5_5() { return static_cast<int32_t>(offsetof(U3CLoadBannersU3Ec__Iterator0_t4124176354, ___U3CU24s_7U3E__5_5)); }
	inline Enumerator_t660383953  get_U3CU24s_7U3E__5_5() const { return ___U3CU24s_7U3E__5_5; }
	inline Enumerator_t660383953 * get_address_of_U3CU24s_7U3E__5_5() { return &___U3CU24s_7U3E__5_5; }
	inline void set_U3CU24s_7U3E__5_5(Enumerator_t660383953  value)
	{
		___U3CU24s_7U3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U3CeU3E__6_6() { return static_cast<int32_t>(offsetof(U3CLoadBannersU3Ec__Iterator0_t4124176354, ___U3CeU3E__6_6)); }
	inline GameObject_t1756533147 * get_U3CeU3E__6_6() const { return ___U3CeU3E__6_6; }
	inline GameObject_t1756533147 ** get_address_of_U3CeU3E__6_6() { return &___U3CeU3E__6_6; }
	inline void set_U3CeU3E__6_6(GameObject_t1756533147 * value)
	{
		___U3CeU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CtexU3E__7_7() { return static_cast<int32_t>(offsetof(U3CLoadBannersU3Ec__Iterator0_t4124176354, ___U3CtexU3E__7_7)); }
	inline Texture2D_t3542995729 * get_U3CtexU3E__7_7() const { return ___U3CtexU3E__7_7; }
	inline Texture2D_t3542995729 ** get_address_of_U3CtexU3E__7_7() { return &___U3CtexU3E__7_7; }
	inline void set_U3CtexU3E__7_7(Texture2D_t3542995729 * value)
	{
		___U3CtexU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtexU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CLoadBannersU3Ec__Iterator0_t4124176354, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CLoadBannersU3Ec__Iterator0_t4124176354, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_10() { return static_cast<int32_t>(offsetof(U3CLoadBannersU3Ec__Iterator0_t4124176354, ___U3CU3Ef__this_10)); }
	inline CourseVisualizer_t3597553323 * get_U3CU3Ef__this_10() const { return ___U3CU3Ef__this_10; }
	inline CourseVisualizer_t3597553323 ** get_address_of_U3CU3Ef__this_10() { return &___U3CU3Ef__this_10; }
	inline void set_U3CU3Ef__this_10(CourseVisualizer_t3597553323 * value)
	{
		___U3CU3Ef__this_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
