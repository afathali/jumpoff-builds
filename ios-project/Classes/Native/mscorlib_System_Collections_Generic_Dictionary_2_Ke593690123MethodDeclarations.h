﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4180772701MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3888443188(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t593690123 *, Dictionary_2_t2405159648 *, const MethodInfo*))KeyCollection__ctor_m3555561037_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1333791826(__this, ___item0, method) ((  void (*) (KeyCollection_t593690123 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3743129415_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3890639037(__this, method) ((  void (*) (KeyCollection_t593690123 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m522538414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3379214666(__this, ___item0, method) ((  bool (*) (KeyCollection_t593690123 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3840423669_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1499686961(__this, ___item0, method) ((  bool (*) (KeyCollection_t593690123 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2545700230_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m692113839(__this, method) ((  Il2CppObject* (*) (KeyCollection_t593690123 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3763330846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m4049359799(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t593690123 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m4142454252_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3242881502(__this, method) ((  Il2CppObject * (*) (KeyCollection_t593690123 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1589406383_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1496750173(__this, method) ((  bool (*) (KeyCollection_t593690123 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1119857328_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2249451847(__this, method) ((  bool (*) (KeyCollection_t593690123 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1423289640_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2252372971(__this, method) ((  Il2CppObject * (*) (KeyCollection_t593690123 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3802319686_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3620861377(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t593690123 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4056130090_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2948024494(__this, method) ((  Enumerator_t799695790  (*) (KeyCollection_t593690123 *, const MethodInfo*))KeyCollection_GetEnumerator_m627495629_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte[]>::get_Count()
#define KeyCollection_get_Count_m854287215(__this, method) ((  int32_t (*) (KeyCollection_t593690123 *, const MethodInfo*))KeyCollection_get_Count_m2942903136_gshared)(__this, method)
