﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.IOSDeploy.Framework
struct Framework_t4022948262;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.IOSDeploy.Framework::.ctor()
extern "C"  void Framework__ctor_m4086854825 (Framework_t4022948262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
