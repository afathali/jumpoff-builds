﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ExifLib_ExifTagFormat3078035447.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExifLib.ExifTag
struct  ExifTag_t1511511074  : public Il2CppObject
{
public:
	// System.Int32 ExifLib.ExifTag::<Tag>k__BackingField
	int32_t ___U3CTagU3Ek__BackingField_1;
	// ExifLib.ExifTagFormat ExifLib.ExifTag::<Format>k__BackingField
	int32_t ___U3CFormatU3Ek__BackingField_2;
	// System.Int32 ExifLib.ExifTag::<Components>k__BackingField
	int32_t ___U3CComponentsU3Ek__BackingField_3;
	// System.Byte[] ExifLib.ExifTag::<Data>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CDataU3Ek__BackingField_4;
	// System.Boolean ExifLib.ExifTag::<LittleEndian>k__BackingField
	bool ___U3CLittleEndianU3Ek__BackingField_5;
	// System.Boolean ExifLib.ExifTag::<IsValid>k__BackingField
	bool ___U3CIsValidU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTagU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExifTag_t1511511074, ___U3CTagU3Ek__BackingField_1)); }
	inline int32_t get_U3CTagU3Ek__BackingField_1() const { return ___U3CTagU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTagU3Ek__BackingField_1() { return &___U3CTagU3Ek__BackingField_1; }
	inline void set_U3CTagU3Ek__BackingField_1(int32_t value)
	{
		___U3CTagU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CFormatU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ExifTag_t1511511074, ___U3CFormatU3Ek__BackingField_2)); }
	inline int32_t get_U3CFormatU3Ek__BackingField_2() const { return ___U3CFormatU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CFormatU3Ek__BackingField_2() { return &___U3CFormatU3Ek__BackingField_2; }
	inline void set_U3CFormatU3Ek__BackingField_2(int32_t value)
	{
		___U3CFormatU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CComponentsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ExifTag_t1511511074, ___U3CComponentsU3Ek__BackingField_3)); }
	inline int32_t get_U3CComponentsU3Ek__BackingField_3() const { return ___U3CComponentsU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CComponentsU3Ek__BackingField_3() { return &___U3CComponentsU3Ek__BackingField_3; }
	inline void set_U3CComponentsU3Ek__BackingField_3(int32_t value)
	{
		___U3CComponentsU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ExifTag_t1511511074, ___U3CDataU3Ek__BackingField_4)); }
	inline ByteU5BU5D_t3397334013* get_U3CDataU3Ek__BackingField_4() const { return ___U3CDataU3Ek__BackingField_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CDataU3Ek__BackingField_4() { return &___U3CDataU3Ek__BackingField_4; }
	inline void set_U3CDataU3Ek__BackingField_4(ByteU5BU5D_t3397334013* value)
	{
		___U3CDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDataU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CLittleEndianU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ExifTag_t1511511074, ___U3CLittleEndianU3Ek__BackingField_5)); }
	inline bool get_U3CLittleEndianU3Ek__BackingField_5() const { return ___U3CLittleEndianU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CLittleEndianU3Ek__BackingField_5() { return &___U3CLittleEndianU3Ek__BackingField_5; }
	inline void set_U3CLittleEndianU3Ek__BackingField_5(bool value)
	{
		___U3CLittleEndianU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsValidU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ExifTag_t1511511074, ___U3CIsValidU3Ek__BackingField_6)); }
	inline bool get_U3CIsValidU3Ek__BackingField_6() const { return ___U3CIsValidU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsValidU3Ek__BackingField_6() { return &___U3CIsValidU3Ek__BackingField_6; }
	inline void set_U3CIsValidU3Ek__BackingField_6(bool value)
	{
		___U3CIsValidU3Ek__BackingField_6 = value;
	}
};

struct ExifTag_t1511511074_StaticFields
{
public:
	// System.Int32[] ExifLib.ExifTag::BytesPerFormat
	Int32U5BU5D_t3030399641* ___BytesPerFormat_0;

public:
	inline static int32_t get_offset_of_BytesPerFormat_0() { return static_cast<int32_t>(offsetof(ExifTag_t1511511074_StaticFields, ___BytesPerFormat_0)); }
	inline Int32U5BU5D_t3030399641* get_BytesPerFormat_0() const { return ___BytesPerFormat_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_BytesPerFormat_0() { return &___BytesPerFormat_0; }
	inline void set_BytesPerFormat_0(Int32U5BU5D_t3030399641* value)
	{
		___BytesPerFormat_0 = value;
		Il2CppCodeGenWriteBarrier(&___BytesPerFormat_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
