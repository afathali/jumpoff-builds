﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"

// System.Void System.Predicate`1<SA.IOSNative.Contacts.PhoneNumber>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m1900473025(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t2851292052 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2910895094_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<SA.IOSNative.Contacts.PhoneNumber>::Invoke(T)
#define Predicate_1_Invoke_m1963186413(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2851292052 *, PhoneNumber_t113354641 *, const MethodInfo*))Predicate_1_Invoke_m921875257_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<SA.IOSNative.Contacts.PhoneNumber>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m545102056(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t2851292052 *, PhoneNumber_t113354641 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3556950370_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<SA.IOSNative.Contacts.PhoneNumber>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m1990633643(__this, ___result0, method) ((  bool (*) (Predicate_1_t2851292052 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3656575065_gshared)(__this, ___result0, method)
