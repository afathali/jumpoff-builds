﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::.ctor()
#define List_1__ctor_m99093911(__this, method) ((  void (*) (List_1_t2582320210 *, const MethodInfo*))List_1__ctor_m1864370736_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m4027193427(__this, ___collection0, method) ((  void (*) (List_1_t2582320210 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2396561940_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::.ctor(System.Int32)
#define List_1__ctor_m1760621081(__this, ___capacity0, method) ((  void (*) (List_1_t2582320210 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::.cctor()
#define List_1__cctor_m1138400163(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3951275328(__this, method) ((  Il2CppObject* (*) (List_1_t2582320210 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1986378518(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2582320210 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2127454649(__this, method) ((  Il2CppObject * (*) (List_1_t2582320210 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m745631262(__this, ___item0, method) ((  int32_t (*) (List_1_t2582320210 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m923357120(__this, ___item0, method) ((  bool (*) (List_1_t2582320210 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m394082436(__this, ___item0, method) ((  int32_t (*) (List_1_t2582320210 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m2387091945(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2582320210 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3661743145(__this, ___item0, method) ((  void (*) (List_1_t2582320210 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2259367829(__this, method) ((  bool (*) (List_1_t2582320210 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2541816246(__this, method) ((  bool (*) (List_1_t2582320210 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m520729270(__this, method) ((  Il2CppObject * (*) (List_1_t2582320210 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3954307093(__this, method) ((  bool (*) (List_1_t2582320210 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2108240178(__this, method) ((  bool (*) (List_1_t2582320210 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1757549629(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2582320210 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3247320824(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2582320210 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::Add(T)
#define List_1_Add_m3211383443(__this, ___item0, method) ((  void (*) (List_1_t2582320210 *, FB_LikeInfo_t3213199078 *, const MethodInfo*))List_1_Add_m2488140228_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2514144556(__this, ___newCount0, method) ((  void (*) (List_1_t2582320210 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3332351484(__this, ___collection0, method) ((  void (*) (List_1_t2582320210 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m754697308(__this, ___enumerable0, method) ((  void (*) (List_1_t2582320210 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1213202345(__this, ___collection0, method) ((  void (*) (List_1_t2582320210 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<FB_LikeInfo>::AsReadOnly()
#define List_1_AsReadOnly_m3606488010(__this, method) ((  ReadOnlyCollection_1_t3398984770 * (*) (List_1_t2582320210 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::Clear()
#define List_1_Clear_m1898056793(__this, method) ((  void (*) (List_1_t2582320210 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FB_LikeInfo>::Contains(T)
#define List_1_Contains_m4024787443(__this, ___item0, method) ((  bool (*) (List_1_t2582320210 *, FB_LikeInfo_t3213199078 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::CopyTo(T[])
#define List_1_CopyTo_m1092488270(__this, ___array0, method) ((  void (*) (List_1_t2582320210 *, FB_LikeInfoU5BU5D_t993260227*, const MethodInfo*))List_1_CopyTo_m626830950_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m2608796725(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2582320210 *, FB_LikeInfoU5BU5D_t993260227*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<FB_LikeInfo>::Find(System.Predicate`1<T>)
#define List_1_Find_m1707723959(__this, ___match0, method) ((  FB_LikeInfo_t3213199078 * (*) (List_1_t2582320210 *, Predicate_1_t1656169193 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m126654958(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1656169193 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<FB_LikeInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3792780723(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2582320210 *, int32_t, int32_t, Predicate_1_t1656169193 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<FB_LikeInfo>::GetEnumerator()
#define List_1_GetEnumerator_m4017292318(__this, method) ((  Enumerator_t2117049884  (*) (List_1_t2582320210 *, const MethodInfo*))List_1_GetEnumerator_m1854899495_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FB_LikeInfo>::IndexOf(T)
#define List_1_IndexOf_m1288908937(__this, ___item0, method) ((  int32_t (*) (List_1_t2582320210 *, FB_LikeInfo_t3213199078 *, const MethodInfo*))List_1_IndexOf_m1888505567_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m2787790818(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2582320210 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m4183811809(__this, ___index0, method) ((  void (*) (List_1_t2582320210 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::Insert(System.Int32,T)
#define List_1_Insert_m411967416(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2582320210 *, int32_t, FB_LikeInfo_t3213199078 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1749203231(__this, ___collection0, method) ((  void (*) (List_1_t2582320210 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<FB_LikeInfo>::Remove(T)
#define List_1_Remove_m3337861798(__this, ___item0, method) ((  bool (*) (List_1_t2582320210 *, FB_LikeInfo_t3213199078 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<FB_LikeInfo>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m140366254(__this, ___match0, method) ((  int32_t (*) (List_1_t2582320210 *, Predicate_1_t1656169193 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m174203604(__this, ___index0, method) ((  void (*) (List_1_t2582320210 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2639322385_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::Reverse()
#define List_1_Reverse_m2199008968(__this, method) ((  void (*) (List_1_t2582320210 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::Sort()
#define List_1_Sort_m797239628(__this, method) ((  void (*) (List_1_t2582320210 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1499221256(__this, ___comparer0, method) ((  void (*) (List_1_t2582320210 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3437917387(__this, ___comparison0, method) ((  void (*) (List_1_t2582320210 *, Comparison_1_t179970633 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<FB_LikeInfo>::ToArray()
#define List_1_ToArray_m3015428595(__this, method) ((  FB_LikeInfoU5BU5D_t993260227* (*) (List_1_t2582320210 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::TrimExcess()
#define List_1_TrimExcess_m984892789(__this, method) ((  void (*) (List_1_t2582320210 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FB_LikeInfo>::get_Capacity()
#define List_1_get_Capacity_m1550995211(__this, method) ((  int32_t (*) (List_1_t2582320210 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m3316367644(__this, ___value0, method) ((  void (*) (List_1_t2582320210 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<FB_LikeInfo>::get_Count()
#define List_1_get_Count_m3583201214(__this, method) ((  int32_t (*) (List_1_t2582320210 *, const MethodInfo*))List_1_get_Count_m4253763168_gshared)(__this, method)
// T System.Collections.Generic.List`1<FB_LikeInfo>::get_Item(System.Int32)
#define List_1_get_Item_m3275946730(__this, ___index0, method) ((  FB_LikeInfo_t3213199078 * (*) (List_1_t2582320210 *, int32_t, const MethodInfo*))List_1_get_Item_m905507485_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FB_LikeInfo>::set_Item(System.Int32,T)
#define List_1_set_Item_m3631543541(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2582320210 *, int32_t, FB_LikeInfo_t3213199078 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
