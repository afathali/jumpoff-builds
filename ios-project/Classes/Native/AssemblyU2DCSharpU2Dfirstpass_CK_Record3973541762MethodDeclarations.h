﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CK_Record
struct CK_Record_t3973541762;
// CK_RecordID
struct CK_RecordID_t41838833;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_RecordID41838833.h"
#include "mscorlib_System_String2029220233.h"

// System.Void CK_Record::.ctor(CK_RecordID,System.String)
extern "C"  void CK_Record__ctor_m3511663942 (CK_Record_t3973541762 * __this, CK_RecordID_t41838833 * ___id0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Record::.ctor(System.String,System.String)
extern "C"  void CK_Record__ctor_m3309656539 (CK_Record_t3973541762 * __this, String_t* ___name0, String_t* ___template1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Record::.cctor()
extern "C"  void CK_Record__cctor_m2799746370 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Record::SetObject(System.String,System.String)
extern "C"  void CK_Record_SetObject_m3260356362 (CK_Record_t3973541762 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CK_Record::GetObject(System.String)
extern "C"  String_t* CK_Record_GetObject_m2142979419 (CK_Record_t3973541762 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CK_RecordID CK_Record::get_Id()
extern "C"  CK_RecordID_t41838833 * CK_Record_get_Id_m1430839399 (CK_Record_t3973541762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CK_Record::get_Type()
extern "C"  String_t* CK_Record_get_Type_m2867585591 (CK_Record_t3973541762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Record::IndexRecord()
extern "C"  void CK_Record_IndexRecord_m1470701294 (CK_Record_t3973541762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Record::UpdateRecord()
extern "C"  void CK_Record_UpdateRecord_m2403414911 (CK_Record_t3973541762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CK_Record::get_Internal_Id()
extern "C"  int32_t CK_Record_get_Internal_Id_m482171849 (CK_Record_t3973541762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CK_Record CK_Record::GetRecordByInternalId(System.Int32)
extern "C"  CK_Record_t3973541762 * CK_Record_GetRecordByInternalId_m1430321889 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
