﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonDotNet.Extras.CustomConverters.Matrix4x4Converter
struct Matrix4x4Converter_t4108782939;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t1973729997;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t1719617802;
// Newtonsoft.Json.JsonReader
struct JsonReader_t3154730733;
// System.Type
struct Type_t;
// Newtonsoft.Json.Linq.JProperty
struct JProperty_t2956441399;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter1973729997.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer1719617802.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader3154730733.h"
#include "mscorlib_System_Type1303803226.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JProperty2956441399.h"

// System.Void JsonDotNet.Extras.CustomConverters.Matrix4x4Converter::.ctor()
extern "C"  void Matrix4x4Converter__ctor_m3900260479 (Matrix4x4Converter_t4108782939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonDotNet.Extras.CustomConverters.Matrix4x4Converter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void Matrix4x4Converter_WriteJson_m3897961087 (Matrix4x4Converter_t4108782939 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t1719617802 * ___serializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JsonDotNet.Extras.CustomConverters.Matrix4x4Converter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * Matrix4x4Converter_ReadJson_m1660348266 (Matrix4x4Converter_t4108782939 * __this, JsonReader_t3154730733 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t1719617802 * ___serializer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsonDotNet.Extras.CustomConverters.Matrix4x4Converter::get_CanRead()
extern "C"  bool Matrix4x4Converter_get_CanRead_m1501312438 (Matrix4x4Converter_t4108782939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsonDotNet.Extras.CustomConverters.Matrix4x4Converter::CanConvert(System.Type)
extern "C"  bool Matrix4x4Converter_CanConvert_m3085631059 (Matrix4x4Converter_t4108782939 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsonDotNet.Extras.CustomConverters.Matrix4x4Converter::<WriteJson>m__BC(Newtonsoft.Json.Linq.JProperty)
extern "C"  bool Matrix4x4Converter_U3CWriteJsonU3Em__BC_m104395512 (Il2CppObject * __this /* static, unused */, JProperty_t2956441399 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JsonDotNet.Extras.CustomConverters.Matrix4x4Converter::<WriteJson>m__BD(Newtonsoft.Json.Linq.JProperty)
extern "C"  String_t* Matrix4x4Converter_U3CWriteJsonU3Em__BD_m2534480052 (Il2CppObject * __this /* static, unused */, JProperty_t2956441399 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
