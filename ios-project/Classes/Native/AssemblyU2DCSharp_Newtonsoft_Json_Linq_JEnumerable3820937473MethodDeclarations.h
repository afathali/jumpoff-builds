﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JEnumerable3957742755MethodDeclarations.h"

// System.Void Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define JEnumerable_1__ctor_m1623461924(__this, ___enumerable0, method) ((  void (*) (JEnumerable_1_t3820937473 *, Il2CppObject*, const MethodInfo*))JEnumerable_1__ctor_m1860534610_gshared)(__this, ___enumerable0, method)
// System.Void Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::.cctor()
#define JEnumerable_1__cctor_m3736172794(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))JEnumerable_1__cctor_m3633367596_gshared)(__this /* static, unused */, method)
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::System.Collections.IEnumerable.GetEnumerator()
#define JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m2242563902(__this, method) ((  Il2CppObject * (*) (JEnumerable_1_t3820937473 *, const MethodInfo*))JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m2922461236_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::GetEnumerator()
#define JEnumerable_1_GetEnumerator_m2701238769(__this, method) ((  Il2CppObject* (*) (JEnumerable_1_t3820937473 *, const MethodInfo*))JEnumerable_1_GetEnumerator_m1984457945_gshared)(__this, method)
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::get_Item(System.Object)
#define JEnumerable_1_get_Item_m2731416323(__this, ___key0, method) ((  Il2CppObject* (*) (JEnumerable_1_t3820937473 *, Il2CppObject *, const MethodInfo*))JEnumerable_1_get_Item_m2121380803_gshared)(__this, ___key0, method)
// System.Boolean Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::Equals(System.Object)
#define JEnumerable_1_Equals_m315476016(__this, ___obj0, method) ((  bool (*) (JEnumerable_1_t3820937473 *, Il2CppObject *, const MethodInfo*))JEnumerable_1_Equals_m1085474098_gshared)(__this, ___obj0, method)
// System.Int32 Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::GetHashCode()
#define JEnumerable_1_GetHashCode_m4248329424(__this, method) ((  int32_t (*) (JEnumerable_1_t3820937473 *, const MethodInfo*))JEnumerable_1_GetHashCode_m1140639406_gshared)(__this, method)
