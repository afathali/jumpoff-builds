﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Terms
struct  Terms_t510630971  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button Terms::agree
	Button_t2872111280 * ___agree_2;
	// UnityEngine.UI.Button Terms::disagree
	Button_t2872111280 * ___disagree_3;

public:
	inline static int32_t get_offset_of_agree_2() { return static_cast<int32_t>(offsetof(Terms_t510630971, ___agree_2)); }
	inline Button_t2872111280 * get_agree_2() const { return ___agree_2; }
	inline Button_t2872111280 ** get_address_of_agree_2() { return &___agree_2; }
	inline void set_agree_2(Button_t2872111280 * value)
	{
		___agree_2 = value;
		Il2CppCodeGenWriteBarrier(&___agree_2, value);
	}

	inline static int32_t get_offset_of_disagree_3() { return static_cast<int32_t>(offsetof(Terms_t510630971, ___disagree_3)); }
	inline Button_t2872111280 * get_disagree_3() const { return ___disagree_3; }
	inline Button_t2872111280 ** get_address_of_disagree_3() { return &___disagree_3; }
	inline void set_disagree_3(Button_t2872111280 * value)
	{
		___disagree_3 = value;
		Il2CppCodeGenWriteBarrier(&___disagree_3, value);
	}
};

struct Terms_t510630971_StaticFields
{
public:
	// UnityEngine.Events.UnityAction Terms::<>f__am$cache2
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache2_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(Terms_t510630971_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
