﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,GP_TBM_Match>
struct Dictionary_2_t3189857243;
// System.Action`1<GP_TBM_LoadMatchesResult>
struct Action_1_t643572420;
// System.Action`1<GP_TBM_MatchInitiatedResult>
struct Action_1_t3945860229;
// System.Action`1<GP_TBM_CancelMatchResult>
struct Action_1_t3681092534;
// System.Action`1<GP_TBM_LeaveMatchResult>
struct Action_1_t3459603101;
// System.Action`1<GP_TBM_LoadMatchResult>
struct Action_1_t1159218312;
// System.Action`1<GP_TBM_UpdateMatchResult>
struct Action_1_t3744805351;
// System.Action`1<GP_TBM_MatchReceivedResult>
struct Action_1_t2196472297;
// System.Action`1<GP_TBM_MatchRemovedResult>
struct Action_1_t488154502;
// System.Action`1<AndroidActivityResult>
struct Action_1_t3559310183;
// System.Action`2<System.String,GP_TBM_MatchInitiatedResult>
struct Action_2_t2054415243;
// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen1931975228.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayTBM
struct  GooglePlayTBM_t641131310  : public SA_Singleton_OLD_1_t1931975228
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,GP_TBM_Match> GooglePlayTBM::_Matches
	Dictionary_2_t3189857243 * ____Matches_4;

public:
	inline static int32_t get_offset_of__Matches_4() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310, ____Matches_4)); }
	inline Dictionary_2_t3189857243 * get__Matches_4() const { return ____Matches_4; }
	inline Dictionary_2_t3189857243 ** get_address_of__Matches_4() { return &____Matches_4; }
	inline void set__Matches_4(Dictionary_2_t3189857243 * value)
	{
		____Matches_4 = value;
		Il2CppCodeGenWriteBarrier(&____Matches_4, value);
	}
};

struct GooglePlayTBM_t641131310_StaticFields
{
public:
	// System.Action`1<GP_TBM_LoadMatchesResult> GooglePlayTBM::ActionMatchesResultLoaded
	Action_1_t643572420 * ___ActionMatchesResultLoaded_5;
	// System.Action`1<GP_TBM_MatchInitiatedResult> GooglePlayTBM::ActionMatchInitiated
	Action_1_t3945860229 * ___ActionMatchInitiated_6;
	// System.Action`1<GP_TBM_CancelMatchResult> GooglePlayTBM::ActionMatchCanceled
	Action_1_t3681092534 * ___ActionMatchCanceled_7;
	// System.Action`1<GP_TBM_LeaveMatchResult> GooglePlayTBM::ActionMatchLeaved
	Action_1_t3459603101 * ___ActionMatchLeaved_8;
	// System.Action`1<GP_TBM_LoadMatchResult> GooglePlayTBM::ActionMatchDataLoaded
	Action_1_t1159218312 * ___ActionMatchDataLoaded_9;
	// System.Action`1<GP_TBM_UpdateMatchResult> GooglePlayTBM::ActionMatchUpdated
	Action_1_t3744805351 * ___ActionMatchUpdated_10;
	// System.Action`1<GP_TBM_UpdateMatchResult> GooglePlayTBM::ActionMatchTurnFinished
	Action_1_t3744805351 * ___ActionMatchTurnFinished_11;
	// System.Action`1<GP_TBM_MatchReceivedResult> GooglePlayTBM::ActionMatchReceived
	Action_1_t2196472297 * ___ActionMatchReceived_12;
	// System.Action`1<GP_TBM_MatchRemovedResult> GooglePlayTBM::ActionMatchRemoved
	Action_1_t488154502 * ___ActionMatchRemoved_13;
	// System.Action`1<AndroidActivityResult> GooglePlayTBM::ActionMatchCreationCanceled
	Action_1_t3559310183 * ___ActionMatchCreationCanceled_14;
	// System.Action`2<System.String,GP_TBM_MatchInitiatedResult> GooglePlayTBM::ActionMatchInvitationAccepted
	Action_2_t2054415243 * ___ActionMatchInvitationAccepted_15;
	// System.Action`1<System.String> GooglePlayTBM::ActionMatchInvitationDeclined
	Action_1_t1831019615 * ___ActionMatchInvitationDeclined_16;
	// System.Action`1<GP_TBM_LoadMatchesResult> GooglePlayTBM::<>f__am$cacheD
	Action_1_t643572420 * ___U3CU3Ef__amU24cacheD_17;
	// System.Action`1<GP_TBM_MatchInitiatedResult> GooglePlayTBM::<>f__am$cacheE
	Action_1_t3945860229 * ___U3CU3Ef__amU24cacheE_18;
	// System.Action`1<GP_TBM_CancelMatchResult> GooglePlayTBM::<>f__am$cacheF
	Action_1_t3681092534 * ___U3CU3Ef__amU24cacheF_19;
	// System.Action`1<GP_TBM_LeaveMatchResult> GooglePlayTBM::<>f__am$cache10
	Action_1_t3459603101 * ___U3CU3Ef__amU24cache10_20;
	// System.Action`1<GP_TBM_LoadMatchResult> GooglePlayTBM::<>f__am$cache11
	Action_1_t1159218312 * ___U3CU3Ef__amU24cache11_21;
	// System.Action`1<GP_TBM_UpdateMatchResult> GooglePlayTBM::<>f__am$cache12
	Action_1_t3744805351 * ___U3CU3Ef__amU24cache12_22;
	// System.Action`1<GP_TBM_UpdateMatchResult> GooglePlayTBM::<>f__am$cache13
	Action_1_t3744805351 * ___U3CU3Ef__amU24cache13_23;
	// System.Action`1<GP_TBM_MatchReceivedResult> GooglePlayTBM::<>f__am$cache14
	Action_1_t2196472297 * ___U3CU3Ef__amU24cache14_24;
	// System.Action`1<GP_TBM_MatchRemovedResult> GooglePlayTBM::<>f__am$cache15
	Action_1_t488154502 * ___U3CU3Ef__amU24cache15_25;
	// System.Action`1<AndroidActivityResult> GooglePlayTBM::<>f__am$cache16
	Action_1_t3559310183 * ___U3CU3Ef__amU24cache16_26;
	// System.Action`2<System.String,GP_TBM_MatchInitiatedResult> GooglePlayTBM::<>f__am$cache17
	Action_2_t2054415243 * ___U3CU3Ef__amU24cache17_27;
	// System.Action`1<System.String> GooglePlayTBM::<>f__am$cache18
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache18_28;

public:
	inline static int32_t get_offset_of_ActionMatchesResultLoaded_5() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___ActionMatchesResultLoaded_5)); }
	inline Action_1_t643572420 * get_ActionMatchesResultLoaded_5() const { return ___ActionMatchesResultLoaded_5; }
	inline Action_1_t643572420 ** get_address_of_ActionMatchesResultLoaded_5() { return &___ActionMatchesResultLoaded_5; }
	inline void set_ActionMatchesResultLoaded_5(Action_1_t643572420 * value)
	{
		___ActionMatchesResultLoaded_5 = value;
		Il2CppCodeGenWriteBarrier(&___ActionMatchesResultLoaded_5, value);
	}

	inline static int32_t get_offset_of_ActionMatchInitiated_6() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___ActionMatchInitiated_6)); }
	inline Action_1_t3945860229 * get_ActionMatchInitiated_6() const { return ___ActionMatchInitiated_6; }
	inline Action_1_t3945860229 ** get_address_of_ActionMatchInitiated_6() { return &___ActionMatchInitiated_6; }
	inline void set_ActionMatchInitiated_6(Action_1_t3945860229 * value)
	{
		___ActionMatchInitiated_6 = value;
		Il2CppCodeGenWriteBarrier(&___ActionMatchInitiated_6, value);
	}

	inline static int32_t get_offset_of_ActionMatchCanceled_7() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___ActionMatchCanceled_7)); }
	inline Action_1_t3681092534 * get_ActionMatchCanceled_7() const { return ___ActionMatchCanceled_7; }
	inline Action_1_t3681092534 ** get_address_of_ActionMatchCanceled_7() { return &___ActionMatchCanceled_7; }
	inline void set_ActionMatchCanceled_7(Action_1_t3681092534 * value)
	{
		___ActionMatchCanceled_7 = value;
		Il2CppCodeGenWriteBarrier(&___ActionMatchCanceled_7, value);
	}

	inline static int32_t get_offset_of_ActionMatchLeaved_8() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___ActionMatchLeaved_8)); }
	inline Action_1_t3459603101 * get_ActionMatchLeaved_8() const { return ___ActionMatchLeaved_8; }
	inline Action_1_t3459603101 ** get_address_of_ActionMatchLeaved_8() { return &___ActionMatchLeaved_8; }
	inline void set_ActionMatchLeaved_8(Action_1_t3459603101 * value)
	{
		___ActionMatchLeaved_8 = value;
		Il2CppCodeGenWriteBarrier(&___ActionMatchLeaved_8, value);
	}

	inline static int32_t get_offset_of_ActionMatchDataLoaded_9() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___ActionMatchDataLoaded_9)); }
	inline Action_1_t1159218312 * get_ActionMatchDataLoaded_9() const { return ___ActionMatchDataLoaded_9; }
	inline Action_1_t1159218312 ** get_address_of_ActionMatchDataLoaded_9() { return &___ActionMatchDataLoaded_9; }
	inline void set_ActionMatchDataLoaded_9(Action_1_t1159218312 * value)
	{
		___ActionMatchDataLoaded_9 = value;
		Il2CppCodeGenWriteBarrier(&___ActionMatchDataLoaded_9, value);
	}

	inline static int32_t get_offset_of_ActionMatchUpdated_10() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___ActionMatchUpdated_10)); }
	inline Action_1_t3744805351 * get_ActionMatchUpdated_10() const { return ___ActionMatchUpdated_10; }
	inline Action_1_t3744805351 ** get_address_of_ActionMatchUpdated_10() { return &___ActionMatchUpdated_10; }
	inline void set_ActionMatchUpdated_10(Action_1_t3744805351 * value)
	{
		___ActionMatchUpdated_10 = value;
		Il2CppCodeGenWriteBarrier(&___ActionMatchUpdated_10, value);
	}

	inline static int32_t get_offset_of_ActionMatchTurnFinished_11() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___ActionMatchTurnFinished_11)); }
	inline Action_1_t3744805351 * get_ActionMatchTurnFinished_11() const { return ___ActionMatchTurnFinished_11; }
	inline Action_1_t3744805351 ** get_address_of_ActionMatchTurnFinished_11() { return &___ActionMatchTurnFinished_11; }
	inline void set_ActionMatchTurnFinished_11(Action_1_t3744805351 * value)
	{
		___ActionMatchTurnFinished_11 = value;
		Il2CppCodeGenWriteBarrier(&___ActionMatchTurnFinished_11, value);
	}

	inline static int32_t get_offset_of_ActionMatchReceived_12() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___ActionMatchReceived_12)); }
	inline Action_1_t2196472297 * get_ActionMatchReceived_12() const { return ___ActionMatchReceived_12; }
	inline Action_1_t2196472297 ** get_address_of_ActionMatchReceived_12() { return &___ActionMatchReceived_12; }
	inline void set_ActionMatchReceived_12(Action_1_t2196472297 * value)
	{
		___ActionMatchReceived_12 = value;
		Il2CppCodeGenWriteBarrier(&___ActionMatchReceived_12, value);
	}

	inline static int32_t get_offset_of_ActionMatchRemoved_13() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___ActionMatchRemoved_13)); }
	inline Action_1_t488154502 * get_ActionMatchRemoved_13() const { return ___ActionMatchRemoved_13; }
	inline Action_1_t488154502 ** get_address_of_ActionMatchRemoved_13() { return &___ActionMatchRemoved_13; }
	inline void set_ActionMatchRemoved_13(Action_1_t488154502 * value)
	{
		___ActionMatchRemoved_13 = value;
		Il2CppCodeGenWriteBarrier(&___ActionMatchRemoved_13, value);
	}

	inline static int32_t get_offset_of_ActionMatchCreationCanceled_14() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___ActionMatchCreationCanceled_14)); }
	inline Action_1_t3559310183 * get_ActionMatchCreationCanceled_14() const { return ___ActionMatchCreationCanceled_14; }
	inline Action_1_t3559310183 ** get_address_of_ActionMatchCreationCanceled_14() { return &___ActionMatchCreationCanceled_14; }
	inline void set_ActionMatchCreationCanceled_14(Action_1_t3559310183 * value)
	{
		___ActionMatchCreationCanceled_14 = value;
		Il2CppCodeGenWriteBarrier(&___ActionMatchCreationCanceled_14, value);
	}

	inline static int32_t get_offset_of_ActionMatchInvitationAccepted_15() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___ActionMatchInvitationAccepted_15)); }
	inline Action_2_t2054415243 * get_ActionMatchInvitationAccepted_15() const { return ___ActionMatchInvitationAccepted_15; }
	inline Action_2_t2054415243 ** get_address_of_ActionMatchInvitationAccepted_15() { return &___ActionMatchInvitationAccepted_15; }
	inline void set_ActionMatchInvitationAccepted_15(Action_2_t2054415243 * value)
	{
		___ActionMatchInvitationAccepted_15 = value;
		Il2CppCodeGenWriteBarrier(&___ActionMatchInvitationAccepted_15, value);
	}

	inline static int32_t get_offset_of_ActionMatchInvitationDeclined_16() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___ActionMatchInvitationDeclined_16)); }
	inline Action_1_t1831019615 * get_ActionMatchInvitationDeclined_16() const { return ___ActionMatchInvitationDeclined_16; }
	inline Action_1_t1831019615 ** get_address_of_ActionMatchInvitationDeclined_16() { return &___ActionMatchInvitationDeclined_16; }
	inline void set_ActionMatchInvitationDeclined_16(Action_1_t1831019615 * value)
	{
		___ActionMatchInvitationDeclined_16 = value;
		Il2CppCodeGenWriteBarrier(&___ActionMatchInvitationDeclined_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_17() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___U3CU3Ef__amU24cacheD_17)); }
	inline Action_1_t643572420 * get_U3CU3Ef__amU24cacheD_17() const { return ___U3CU3Ef__amU24cacheD_17; }
	inline Action_1_t643572420 ** get_address_of_U3CU3Ef__amU24cacheD_17() { return &___U3CU3Ef__amU24cacheD_17; }
	inline void set_U3CU3Ef__amU24cacheD_17(Action_1_t643572420 * value)
	{
		___U3CU3Ef__amU24cacheD_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_18() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___U3CU3Ef__amU24cacheE_18)); }
	inline Action_1_t3945860229 * get_U3CU3Ef__amU24cacheE_18() const { return ___U3CU3Ef__amU24cacheE_18; }
	inline Action_1_t3945860229 ** get_address_of_U3CU3Ef__amU24cacheE_18() { return &___U3CU3Ef__amU24cacheE_18; }
	inline void set_U3CU3Ef__amU24cacheE_18(Action_1_t3945860229 * value)
	{
		___U3CU3Ef__amU24cacheE_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_19() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___U3CU3Ef__amU24cacheF_19)); }
	inline Action_1_t3681092534 * get_U3CU3Ef__amU24cacheF_19() const { return ___U3CU3Ef__amU24cacheF_19; }
	inline Action_1_t3681092534 ** get_address_of_U3CU3Ef__amU24cacheF_19() { return &___U3CU3Ef__amU24cacheF_19; }
	inline void set_U3CU3Ef__amU24cacheF_19(Action_1_t3681092534 * value)
	{
		___U3CU3Ef__amU24cacheF_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_20() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___U3CU3Ef__amU24cache10_20)); }
	inline Action_1_t3459603101 * get_U3CU3Ef__amU24cache10_20() const { return ___U3CU3Ef__amU24cache10_20; }
	inline Action_1_t3459603101 ** get_address_of_U3CU3Ef__amU24cache10_20() { return &___U3CU3Ef__amU24cache10_20; }
	inline void set_U3CU3Ef__amU24cache10_20(Action_1_t3459603101 * value)
	{
		___U3CU3Ef__amU24cache10_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_21() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___U3CU3Ef__amU24cache11_21)); }
	inline Action_1_t1159218312 * get_U3CU3Ef__amU24cache11_21() const { return ___U3CU3Ef__amU24cache11_21; }
	inline Action_1_t1159218312 ** get_address_of_U3CU3Ef__amU24cache11_21() { return &___U3CU3Ef__amU24cache11_21; }
	inline void set_U3CU3Ef__amU24cache11_21(Action_1_t1159218312 * value)
	{
		___U3CU3Ef__amU24cache11_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache12_22() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___U3CU3Ef__amU24cache12_22)); }
	inline Action_1_t3744805351 * get_U3CU3Ef__amU24cache12_22() const { return ___U3CU3Ef__amU24cache12_22; }
	inline Action_1_t3744805351 ** get_address_of_U3CU3Ef__amU24cache12_22() { return &___U3CU3Ef__amU24cache12_22; }
	inline void set_U3CU3Ef__amU24cache12_22(Action_1_t3744805351 * value)
	{
		___U3CU3Ef__amU24cache12_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache12_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache13_23() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___U3CU3Ef__amU24cache13_23)); }
	inline Action_1_t3744805351 * get_U3CU3Ef__amU24cache13_23() const { return ___U3CU3Ef__amU24cache13_23; }
	inline Action_1_t3744805351 ** get_address_of_U3CU3Ef__amU24cache13_23() { return &___U3CU3Ef__amU24cache13_23; }
	inline void set_U3CU3Ef__amU24cache13_23(Action_1_t3744805351 * value)
	{
		___U3CU3Ef__amU24cache13_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache13_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_24() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___U3CU3Ef__amU24cache14_24)); }
	inline Action_1_t2196472297 * get_U3CU3Ef__amU24cache14_24() const { return ___U3CU3Ef__amU24cache14_24; }
	inline Action_1_t2196472297 ** get_address_of_U3CU3Ef__amU24cache14_24() { return &___U3CU3Ef__amU24cache14_24; }
	inline void set_U3CU3Ef__amU24cache14_24(Action_1_t2196472297 * value)
	{
		___U3CU3Ef__amU24cache14_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache14_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_25() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___U3CU3Ef__amU24cache15_25)); }
	inline Action_1_t488154502 * get_U3CU3Ef__amU24cache15_25() const { return ___U3CU3Ef__amU24cache15_25; }
	inline Action_1_t488154502 ** get_address_of_U3CU3Ef__amU24cache15_25() { return &___U3CU3Ef__amU24cache15_25; }
	inline void set_U3CU3Ef__amU24cache15_25(Action_1_t488154502 * value)
	{
		___U3CU3Ef__amU24cache15_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache15_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache16_26() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___U3CU3Ef__amU24cache16_26)); }
	inline Action_1_t3559310183 * get_U3CU3Ef__amU24cache16_26() const { return ___U3CU3Ef__amU24cache16_26; }
	inline Action_1_t3559310183 ** get_address_of_U3CU3Ef__amU24cache16_26() { return &___U3CU3Ef__amU24cache16_26; }
	inline void set_U3CU3Ef__amU24cache16_26(Action_1_t3559310183 * value)
	{
		___U3CU3Ef__amU24cache16_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache16_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache17_27() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___U3CU3Ef__amU24cache17_27)); }
	inline Action_2_t2054415243 * get_U3CU3Ef__amU24cache17_27() const { return ___U3CU3Ef__amU24cache17_27; }
	inline Action_2_t2054415243 ** get_address_of_U3CU3Ef__amU24cache17_27() { return &___U3CU3Ef__amU24cache17_27; }
	inline void set_U3CU3Ef__amU24cache17_27(Action_2_t2054415243 * value)
	{
		___U3CU3Ef__amU24cache17_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache17_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache18_28() { return static_cast<int32_t>(offsetof(GooglePlayTBM_t641131310_StaticFields, ___U3CU3Ef__amU24cache18_28)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache18_28() const { return ___U3CU3Ef__amU24cache18_28; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache18_28() { return &___U3CU3Ef__amU24cache18_28; }
	inline void set_U3CU3Ef__amU24cache18_28(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache18_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache18_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
