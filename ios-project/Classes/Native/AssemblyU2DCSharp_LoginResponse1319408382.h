﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// LoginRequest
struct LoginRequest_t2273172322;
// User
struct User_t719925459;
// System.Collections.Generic.List`1<Course>
struct List_1_t2852233831;

#include "AssemblyU2DCSharp_Shared_Response_1_gen3156651579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginResponse
struct  LoginResponse_t1319408382  : public Response_1_t3156651579
{
public:
	// System.Boolean LoginResponse::<Success>k__BackingField
	bool ___U3CSuccessU3Ek__BackingField_0;
	// System.String LoginResponse::<ErrorMessage>k__BackingField
	String_t* ___U3CErrorMessageU3Ek__BackingField_1;
	// LoginRequest LoginResponse::<Request>k__BackingField
	LoginRequest_t2273172322 * ___U3CRequestU3Ek__BackingField_2;
	// System.Boolean LoginResponse::<IsNewPassword>k__BackingField
	bool ___U3CIsNewPasswordU3Ek__BackingField_3;
	// System.String LoginResponse::<AutoLogin>k__BackingField
	String_t* ___U3CAutoLoginU3Ek__BackingField_4;
	// User LoginResponse::<User>k__BackingField
	User_t719925459 * ___U3CUserU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<Course> LoginResponse::<Courses>k__BackingField
	List_1_t2852233831 * ___U3CCoursesU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CSuccessU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LoginResponse_t1319408382, ___U3CSuccessU3Ek__BackingField_0)); }
	inline bool get_U3CSuccessU3Ek__BackingField_0() const { return ___U3CSuccessU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CSuccessU3Ek__BackingField_0() { return &___U3CSuccessU3Ek__BackingField_0; }
	inline void set_U3CSuccessU3Ek__BackingField_0(bool value)
	{
		___U3CSuccessU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CErrorMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LoginResponse_t1319408382, ___U3CErrorMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CErrorMessageU3Ek__BackingField_1() const { return ___U3CErrorMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CErrorMessageU3Ek__BackingField_1() { return &___U3CErrorMessageU3Ek__BackingField_1; }
	inline void set_U3CErrorMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CErrorMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CErrorMessageU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LoginResponse_t1319408382, ___U3CRequestU3Ek__BackingField_2)); }
	inline LoginRequest_t2273172322 * get_U3CRequestU3Ek__BackingField_2() const { return ___U3CRequestU3Ek__BackingField_2; }
	inline LoginRequest_t2273172322 ** get_address_of_U3CRequestU3Ek__BackingField_2() { return &___U3CRequestU3Ek__BackingField_2; }
	inline void set_U3CRequestU3Ek__BackingField_2(LoginRequest_t2273172322 * value)
	{
		___U3CRequestU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CIsNewPasswordU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LoginResponse_t1319408382, ___U3CIsNewPasswordU3Ek__BackingField_3)); }
	inline bool get_U3CIsNewPasswordU3Ek__BackingField_3() const { return ___U3CIsNewPasswordU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsNewPasswordU3Ek__BackingField_3() { return &___U3CIsNewPasswordU3Ek__BackingField_3; }
	inline void set_U3CIsNewPasswordU3Ek__BackingField_3(bool value)
	{
		___U3CIsNewPasswordU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CAutoLoginU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LoginResponse_t1319408382, ___U3CAutoLoginU3Ek__BackingField_4)); }
	inline String_t* get_U3CAutoLoginU3Ek__BackingField_4() const { return ___U3CAutoLoginU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CAutoLoginU3Ek__BackingField_4() { return &___U3CAutoLoginU3Ek__BackingField_4; }
	inline void set_U3CAutoLoginU3Ek__BackingField_4(String_t* value)
	{
		___U3CAutoLoginU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAutoLoginU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CUserU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LoginResponse_t1319408382, ___U3CUserU3Ek__BackingField_5)); }
	inline User_t719925459 * get_U3CUserU3Ek__BackingField_5() const { return ___U3CUserU3Ek__BackingField_5; }
	inline User_t719925459 ** get_address_of_U3CUserU3Ek__BackingField_5() { return &___U3CUserU3Ek__BackingField_5; }
	inline void set_U3CUserU3Ek__BackingField_5(User_t719925459 * value)
	{
		___U3CUserU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUserU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CCoursesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LoginResponse_t1319408382, ___U3CCoursesU3Ek__BackingField_6)); }
	inline List_1_t2852233831 * get_U3CCoursesU3Ek__BackingField_6() const { return ___U3CCoursesU3Ek__BackingField_6; }
	inline List_1_t2852233831 ** get_address_of_U3CCoursesU3Ek__BackingField_6() { return &___U3CCoursesU3Ek__BackingField_6; }
	inline void set_U3CCoursesU3Ek__BackingField_6(List_1_t2852233831 * value)
	{
		___U3CCoursesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCoursesU3Ek__BackingField_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
