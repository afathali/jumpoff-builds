﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RegisterRequest
struct RegisterRequest_t698226714;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void RegisterRequest::.ctor()
extern "C"  void RegisterRequest__ctor_m78630787 (RegisterRequest_t698226714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RegisterRequest::get_Type()
extern "C"  String_t* RegisterRequest_get_Type_m4192950191 (RegisterRequest_t698226714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterRequest::set_Type(System.String)
extern "C"  void RegisterRequest_set_Type_m485513614 (RegisterRequest_t698226714 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RegisterRequest::get_FullName()
extern "C"  String_t* RegisterRequest_get_FullName_m4139688335 (RegisterRequest_t698226714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterRequest::set_FullName(System.String)
extern "C"  void RegisterRequest_set_FullName_m3738463884 (RegisterRequest_t698226714 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RegisterRequest::get_Email()
extern "C"  String_t* RegisterRequest_get_Email_m1839805383 (RegisterRequest_t698226714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterRequest::set_Email(System.String)
extern "C"  void RegisterRequest_set_Email_m1758155648 (RegisterRequest_t698226714 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RegisterRequest::get_Password()
extern "C"  String_t* RegisterRequest_get_Password_m1034774594 (RegisterRequest_t698226714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterRequest::set_Password(System.String)
extern "C"  void RegisterRequest_set_Password_m136279411 (RegisterRequest_t698226714 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> RegisterRequest::get_Zones()
extern "C"  List_1_t1440998580 * RegisterRequest_get_Zones_m2118050567 (RegisterRequest_t698226714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterRequest::set_Zones(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void RegisterRequest_set_Zones_m794325698 (RegisterRequest_t698226714 * __this, List_1_t1440998580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> RegisterRequest::get_Disipline()
extern "C"  List_1_t1398341365 * RegisterRequest_get_Disipline_m693468134 (RegisterRequest_t698226714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterRequest::set_Disipline(System.Collections.Generic.List`1<System.String>)
extern "C"  void RegisterRequest_set_Disipline_m140808723 (RegisterRequest_t698226714 * __this, List_1_t1398341365 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
