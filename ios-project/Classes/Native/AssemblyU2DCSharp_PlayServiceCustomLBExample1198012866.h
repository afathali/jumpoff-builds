﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Texture
struct Texture_t2243626319;
// DefaultPreviewButton
struct DefaultPreviewButton_t12674677;
// SA_Label
struct SA_Label_t226960149;
// DefaultPreviewButton[]
struct DefaultPreviewButtonU5BU5D_t3800739864;
// CustomLeaderboardFiledsHolder[]
struct CustomLeaderboardFiledsHolderU5BU5D_t960860040;
// GPLeaderBoard
struct GPLeaderBoard_t3649577886;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_GPCollectionType2617299399.h"
#include "AssemblyU2DCSharp_GPBoardTimeSpan42003024.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayServiceCustomLBExample
struct  PlayServiceCustomLBExample_t1198012866  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PlayServiceCustomLBExample::avatar
	GameObject_t1756533147 * ___avatar_3;
	// UnityEngine.Texture PlayServiceCustomLBExample::defaulttexture
	Texture_t2243626319 * ___defaulttexture_4;
	// DefaultPreviewButton PlayServiceCustomLBExample::connectButton
	DefaultPreviewButton_t12674677 * ___connectButton_5;
	// SA_Label PlayServiceCustomLBExample::playerLabel
	SA_Label_t226960149 * ___playerLabel_6;
	// DefaultPreviewButton PlayServiceCustomLBExample::GlobalButton
	DefaultPreviewButton_t12674677 * ___GlobalButton_7;
	// DefaultPreviewButton PlayServiceCustomLBExample::LocalButton
	DefaultPreviewButton_t12674677 * ___LocalButton_8;
	// DefaultPreviewButton PlayServiceCustomLBExample::AllTimeButton
	DefaultPreviewButton_t12674677 * ___AllTimeButton_9;
	// DefaultPreviewButton PlayServiceCustomLBExample::WeekButton
	DefaultPreviewButton_t12674677 * ___WeekButton_10;
	// DefaultPreviewButton PlayServiceCustomLBExample::TodayButton
	DefaultPreviewButton_t12674677 * ___TodayButton_11;
	// DefaultPreviewButton PlayServiceCustomLBExample::SubmitScoreButton
	DefaultPreviewButton_t12674677 * ___SubmitScoreButton_12;
	// DefaultPreviewButton[] PlayServiceCustomLBExample::ConnectionDependedntButtons
	DefaultPreviewButtonU5BU5D_t3800739864* ___ConnectionDependedntButtons_13;
	// CustomLeaderboardFiledsHolder[] PlayServiceCustomLBExample::lines
	CustomLeaderboardFiledsHolderU5BU5D_t960860040* ___lines_14;
	// GPLeaderBoard PlayServiceCustomLBExample::loadedLeaderBoard
	GPLeaderBoard_t3649577886 * ___loadedLeaderBoard_15;
	// GPCollectionType PlayServiceCustomLBExample::displayCollection
	int32_t ___displayCollection_16;
	// GPBoardTimeSpan PlayServiceCustomLBExample::displayTime
	int32_t ___displayTime_17;
	// System.Int32 PlayServiceCustomLBExample::score
	int32_t ___score_18;

public:
	inline static int32_t get_offset_of_avatar_3() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___avatar_3)); }
	inline GameObject_t1756533147 * get_avatar_3() const { return ___avatar_3; }
	inline GameObject_t1756533147 ** get_address_of_avatar_3() { return &___avatar_3; }
	inline void set_avatar_3(GameObject_t1756533147 * value)
	{
		___avatar_3 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_3, value);
	}

	inline static int32_t get_offset_of_defaulttexture_4() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___defaulttexture_4)); }
	inline Texture_t2243626319 * get_defaulttexture_4() const { return ___defaulttexture_4; }
	inline Texture_t2243626319 ** get_address_of_defaulttexture_4() { return &___defaulttexture_4; }
	inline void set_defaulttexture_4(Texture_t2243626319 * value)
	{
		___defaulttexture_4 = value;
		Il2CppCodeGenWriteBarrier(&___defaulttexture_4, value);
	}

	inline static int32_t get_offset_of_connectButton_5() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___connectButton_5)); }
	inline DefaultPreviewButton_t12674677 * get_connectButton_5() const { return ___connectButton_5; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_connectButton_5() { return &___connectButton_5; }
	inline void set_connectButton_5(DefaultPreviewButton_t12674677 * value)
	{
		___connectButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___connectButton_5, value);
	}

	inline static int32_t get_offset_of_playerLabel_6() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___playerLabel_6)); }
	inline SA_Label_t226960149 * get_playerLabel_6() const { return ___playerLabel_6; }
	inline SA_Label_t226960149 ** get_address_of_playerLabel_6() { return &___playerLabel_6; }
	inline void set_playerLabel_6(SA_Label_t226960149 * value)
	{
		___playerLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___playerLabel_6, value);
	}

	inline static int32_t get_offset_of_GlobalButton_7() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___GlobalButton_7)); }
	inline DefaultPreviewButton_t12674677 * get_GlobalButton_7() const { return ___GlobalButton_7; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_GlobalButton_7() { return &___GlobalButton_7; }
	inline void set_GlobalButton_7(DefaultPreviewButton_t12674677 * value)
	{
		___GlobalButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalButton_7, value);
	}

	inline static int32_t get_offset_of_LocalButton_8() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___LocalButton_8)); }
	inline DefaultPreviewButton_t12674677 * get_LocalButton_8() const { return ___LocalButton_8; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_LocalButton_8() { return &___LocalButton_8; }
	inline void set_LocalButton_8(DefaultPreviewButton_t12674677 * value)
	{
		___LocalButton_8 = value;
		Il2CppCodeGenWriteBarrier(&___LocalButton_8, value);
	}

	inline static int32_t get_offset_of_AllTimeButton_9() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___AllTimeButton_9)); }
	inline DefaultPreviewButton_t12674677 * get_AllTimeButton_9() const { return ___AllTimeButton_9; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_AllTimeButton_9() { return &___AllTimeButton_9; }
	inline void set_AllTimeButton_9(DefaultPreviewButton_t12674677 * value)
	{
		___AllTimeButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___AllTimeButton_9, value);
	}

	inline static int32_t get_offset_of_WeekButton_10() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___WeekButton_10)); }
	inline DefaultPreviewButton_t12674677 * get_WeekButton_10() const { return ___WeekButton_10; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_WeekButton_10() { return &___WeekButton_10; }
	inline void set_WeekButton_10(DefaultPreviewButton_t12674677 * value)
	{
		___WeekButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___WeekButton_10, value);
	}

	inline static int32_t get_offset_of_TodayButton_11() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___TodayButton_11)); }
	inline DefaultPreviewButton_t12674677 * get_TodayButton_11() const { return ___TodayButton_11; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_TodayButton_11() { return &___TodayButton_11; }
	inline void set_TodayButton_11(DefaultPreviewButton_t12674677 * value)
	{
		___TodayButton_11 = value;
		Il2CppCodeGenWriteBarrier(&___TodayButton_11, value);
	}

	inline static int32_t get_offset_of_SubmitScoreButton_12() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___SubmitScoreButton_12)); }
	inline DefaultPreviewButton_t12674677 * get_SubmitScoreButton_12() const { return ___SubmitScoreButton_12; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_SubmitScoreButton_12() { return &___SubmitScoreButton_12; }
	inline void set_SubmitScoreButton_12(DefaultPreviewButton_t12674677 * value)
	{
		___SubmitScoreButton_12 = value;
		Il2CppCodeGenWriteBarrier(&___SubmitScoreButton_12, value);
	}

	inline static int32_t get_offset_of_ConnectionDependedntButtons_13() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___ConnectionDependedntButtons_13)); }
	inline DefaultPreviewButtonU5BU5D_t3800739864* get_ConnectionDependedntButtons_13() const { return ___ConnectionDependedntButtons_13; }
	inline DefaultPreviewButtonU5BU5D_t3800739864** get_address_of_ConnectionDependedntButtons_13() { return &___ConnectionDependedntButtons_13; }
	inline void set_ConnectionDependedntButtons_13(DefaultPreviewButtonU5BU5D_t3800739864* value)
	{
		___ConnectionDependedntButtons_13 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionDependedntButtons_13, value);
	}

	inline static int32_t get_offset_of_lines_14() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___lines_14)); }
	inline CustomLeaderboardFiledsHolderU5BU5D_t960860040* get_lines_14() const { return ___lines_14; }
	inline CustomLeaderboardFiledsHolderU5BU5D_t960860040** get_address_of_lines_14() { return &___lines_14; }
	inline void set_lines_14(CustomLeaderboardFiledsHolderU5BU5D_t960860040* value)
	{
		___lines_14 = value;
		Il2CppCodeGenWriteBarrier(&___lines_14, value);
	}

	inline static int32_t get_offset_of_loadedLeaderBoard_15() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___loadedLeaderBoard_15)); }
	inline GPLeaderBoard_t3649577886 * get_loadedLeaderBoard_15() const { return ___loadedLeaderBoard_15; }
	inline GPLeaderBoard_t3649577886 ** get_address_of_loadedLeaderBoard_15() { return &___loadedLeaderBoard_15; }
	inline void set_loadedLeaderBoard_15(GPLeaderBoard_t3649577886 * value)
	{
		___loadedLeaderBoard_15 = value;
		Il2CppCodeGenWriteBarrier(&___loadedLeaderBoard_15, value);
	}

	inline static int32_t get_offset_of_displayCollection_16() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___displayCollection_16)); }
	inline int32_t get_displayCollection_16() const { return ___displayCollection_16; }
	inline int32_t* get_address_of_displayCollection_16() { return &___displayCollection_16; }
	inline void set_displayCollection_16(int32_t value)
	{
		___displayCollection_16 = value;
	}

	inline static int32_t get_offset_of_displayTime_17() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___displayTime_17)); }
	inline int32_t get_displayTime_17() const { return ___displayTime_17; }
	inline int32_t* get_address_of_displayTime_17() { return &___displayTime_17; }
	inline void set_displayTime_17(int32_t value)
	{
		___displayTime_17 = value;
	}

	inline static int32_t get_offset_of_score_18() { return static_cast<int32_t>(offsetof(PlayServiceCustomLBExample_t1198012866, ___score_18)); }
	inline int32_t get_score_18() const { return ___score_18; }
	inline int32_t* get_address_of_score_18() { return &___score_18; }
	inline void set_score_18(int32_t value)
	{
		___score_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
