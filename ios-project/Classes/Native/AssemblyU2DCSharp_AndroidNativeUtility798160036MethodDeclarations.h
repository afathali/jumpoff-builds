﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidNativeUtility
struct AndroidNativeUtility_t798160036;
// System.Action`1<AN_PackageCheckResult>
struct Action_1_t3497215137;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<AN_Locale>
struct Action_1_t4218522104;
// System.Action`1<System.String[]>
struct Action_1_t1444185354;
// System.Action`1<AN_NetworkInfo>
struct Action_1_t49592952;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// AN_PackageCheckResult
struct AN_PackageCheckResult_t3695415755;
// AN_Locale
struct AN_Locale_t121755426;
// System.String[]
struct StringU5BU5D_t1642385972;
// AN_NetworkInfo
struct AN_NetworkInfo_t247793570;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AN_PackageCheckResult3695415755.h"
#include "AssemblyU2DCSharp_AN_Locale121755426.h"
#include "AssemblyU2DCSharp_AN_NetworkInfo247793570.h"

// System.Void AndroidNativeUtility::.ctor()
extern "C"  void AndroidNativeUtility__ctor_m2809437437 (AndroidNativeUtility_t798160036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::.cctor()
extern "C"  void AndroidNativeUtility__cctor_m3165227678 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::add_OnPackageCheckResult(System.Action`1<AN_PackageCheckResult>)
extern "C"  void AndroidNativeUtility_add_OnPackageCheckResult_m4169895504 (Il2CppObject * __this /* static, unused */, Action_1_t3497215137 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::remove_OnPackageCheckResult(System.Action`1<AN_PackageCheckResult>)
extern "C"  void AndroidNativeUtility_remove_OnPackageCheckResult_m821150719 (Il2CppObject * __this /* static, unused */, Action_1_t3497215137 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::add_OnAndroidIdLoaded(System.Action`1<System.String>)
extern "C"  void AndroidNativeUtility_add_OnAndroidIdLoaded_m3476932343 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::remove_OnAndroidIdLoaded(System.Action`1<System.String>)
extern "C"  void AndroidNativeUtility_remove_OnAndroidIdLoaded_m2018308690 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::add_InternalStoragePathLoaded(System.Action`1<System.String>)
extern "C"  void AndroidNativeUtility_add_InternalStoragePathLoaded_m3046268067 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::remove_InternalStoragePathLoaded(System.Action`1<System.String>)
extern "C"  void AndroidNativeUtility_remove_InternalStoragePathLoaded_m2393460898 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::add_ExternalStoragePathLoaded(System.Action`1<System.String>)
extern "C"  void AndroidNativeUtility_add_ExternalStoragePathLoaded_m175423949 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::remove_ExternalStoragePathLoaded(System.Action`1<System.String>)
extern "C"  void AndroidNativeUtility_remove_ExternalStoragePathLoaded_m470167692 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::add_LocaleInfoLoaded(System.Action`1<AN_Locale>)
extern "C"  void AndroidNativeUtility_add_LocaleInfoLoaded_m3440298296 (Il2CppObject * __this /* static, unused */, Action_1_t4218522104 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::remove_LocaleInfoLoaded(System.Action`1<AN_Locale>)
extern "C"  void AndroidNativeUtility_remove_LocaleInfoLoaded_m2374730567 (Il2CppObject * __this /* static, unused */, Action_1_t4218522104 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::add_ActionDevicePackagesListLoaded(System.Action`1<System.String[]>)
extern "C"  void AndroidNativeUtility_add_ActionDevicePackagesListLoaded_m308820415 (Il2CppObject * __this /* static, unused */, Action_1_t1444185354 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::remove_ActionDevicePackagesListLoaded(System.Action`1<System.String[]>)
extern "C"  void AndroidNativeUtility_remove_ActionDevicePackagesListLoaded_m1969350900 (Il2CppObject * __this /* static, unused */, Action_1_t1444185354 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::add_ActionNetworkInfoLoaded(System.Action`1<AN_NetworkInfo>)
extern "C"  void AndroidNativeUtility_add_ActionNetworkInfoLoaded_m3487612020 (Il2CppObject * __this /* static, unused */, Action_1_t49592952 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::remove_ActionNetworkInfoLoaded(System.Action`1<AN_NetworkInfo>)
extern "C"  void AndroidNativeUtility_remove_ActionNetworkInfoLoaded_m1994749071 (Il2CppObject * __this /* static, unused */, Action_1_t49592952 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::Awake()
extern "C"  void AndroidNativeUtility_Awake_m402102224 (AndroidNativeUtility_t798160036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::CheckIsPackageInstalled(System.String)
extern "C"  void AndroidNativeUtility_CheckIsPackageInstalled_m1449979449 (AndroidNativeUtility_t798160036 * __this, String_t* ___packageName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::RunPackage(System.String)
extern "C"  void AndroidNativeUtility_RunPackage_m2737725446 (AndroidNativeUtility_t798160036 * __this, String_t* ___packageName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::StartApplication(System.String)
extern "C"  void AndroidNativeUtility_StartApplication_m850178065 (AndroidNativeUtility_t798160036 * __this, String_t* ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::StartApplication(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void AndroidNativeUtility_StartApplication_m645285712 (AndroidNativeUtility_t798160036 * __this, String_t* ___packageName0, Dictionary_2_t3943999495 * ___extras1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::LoadAndroidId()
extern "C"  void AndroidNativeUtility_LoadAndroidId_m3197663129 (AndroidNativeUtility_t798160036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::GetInternalStoragePath()
extern "C"  void AndroidNativeUtility_GetInternalStoragePath_m644154046 (AndroidNativeUtility_t798160036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::GetExternalStoragePath()
extern "C"  void AndroidNativeUtility_GetExternalStoragePath_m4143816372 (AndroidNativeUtility_t798160036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::LoadLocaleInfo()
extern "C"  void AndroidNativeUtility_LoadLocaleInfo_m2533955665 (AndroidNativeUtility_t798160036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::LoadPackagesList()
extern "C"  void AndroidNativeUtility_LoadPackagesList_m2736495066 (AndroidNativeUtility_t798160036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::LoadNetworkInfo()
extern "C"  void AndroidNativeUtility_LoadNetworkInfo_m2707780303 (AndroidNativeUtility_t798160036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::OpenSettingsPage(System.String)
extern "C"  void AndroidNativeUtility_OpenSettingsPage_m489645829 (Il2CppObject * __this /* static, unused */, String_t* ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::ShowPreloader(System.String,System.String)
extern "C"  void AndroidNativeUtility_ShowPreloader_m3064990136 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::HidePreloader()
extern "C"  void AndroidNativeUtility_HidePreloader_m3033225367 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::OpenAppRatingPage(System.String)
extern "C"  void AndroidNativeUtility_OpenAppRatingPage_m863655924 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::HideCurrentPopup()
extern "C"  void AndroidNativeUtility_HideCurrentPopup_m3762541306 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AndroidNativeUtility::get_SDKLevel()
extern "C"  int32_t AndroidNativeUtility_get_SDKLevel_m3307350162 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::OnAndroidIdLoadedEvent(System.String)
extern "C"  void AndroidNativeUtility_OnAndroidIdLoadedEvent_m1219340871 (AndroidNativeUtility_t798160036 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::OnPacakgeFound(System.String)
extern "C"  void AndroidNativeUtility_OnPacakgeFound_m678092568 (AndroidNativeUtility_t798160036 * __this, String_t* ___packageName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::OnPacakgeNotFound(System.String)
extern "C"  void AndroidNativeUtility_OnPacakgeNotFound_m1088859487 (AndroidNativeUtility_t798160036 * __this, String_t* ___packageName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::OnExternalStoragePathLoaded(System.String)
extern "C"  void AndroidNativeUtility_OnExternalStoragePathLoaded_m2586076844 (AndroidNativeUtility_t798160036 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::OnInternalStoragePathLoaded(System.String)
extern "C"  void AndroidNativeUtility_OnInternalStoragePathLoaded_m1853422178 (AndroidNativeUtility_t798160036 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::OnLocaleInfoLoaded(System.String)
extern "C"  void AndroidNativeUtility_OnLocaleInfoLoaded_m258470335 (AndroidNativeUtility_t798160036 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::OnPackagesListLoaded(System.String)
extern "C"  void AndroidNativeUtility_OnPackagesListLoaded_m2536322960 (AndroidNativeUtility_t798160036 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::OnNetworkInfoLoaded(System.String)
extern "C"  void AndroidNativeUtility_OnNetworkInfoLoaded_m2447352529 (AndroidNativeUtility_t798160036 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::<OnPackageCheckResult>m__1B(AN_PackageCheckResult)
extern "C"  void AndroidNativeUtility_U3COnPackageCheckResultU3Em__1B_m4222876564 (Il2CppObject * __this /* static, unused */, AN_PackageCheckResult_t3695415755 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::<OnAndroidIdLoaded>m__1C(System.String)
extern "C"  void AndroidNativeUtility_U3COnAndroidIdLoadedU3Em__1C_m3933256266 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::<InternalStoragePathLoaded>m__1D(System.String)
extern "C"  void AndroidNativeUtility_U3CInternalStoragePathLoadedU3Em__1D_m3358781721 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::<ExternalStoragePathLoaded>m__1E(System.String)
extern "C"  void AndroidNativeUtility_U3CExternalStoragePathLoadedU3Em__1E_m1232187986 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::<LocaleInfoLoaded>m__1F(AN_Locale)
extern "C"  void AndroidNativeUtility_U3CLocaleInfoLoadedU3Em__1F_m4091735792 (Il2CppObject * __this /* static, unused */, AN_Locale_t121755426 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::<ActionDevicePackagesListLoaded>m__20(System.String[])
extern "C"  void AndroidNativeUtility_U3CActionDevicePackagesListLoadedU3Em__20_m4271619692 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeUtility::<ActionNetworkInfoLoaded>m__21(AN_NetworkInfo)
extern "C"  void AndroidNativeUtility_U3CActionNetworkInfoLoadedU3Em__21_m1831188914 (Il2CppObject * __this /* static, unused */, AN_NetworkInfo_t247793570 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
