﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GallerySaveResult
struct GallerySaveResult_t1643856950;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GallerySaveResult::.ctor(System.String,System.Boolean)
extern "C"  void GallerySaveResult__ctor_m2594979648 (GallerySaveResult_t1643856950 * __this, String_t* ___path0, bool ___res1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GallerySaveResult::get_imagePath()
extern "C"  String_t* GallerySaveResult_get_imagePath_m3142343489 (GallerySaveResult_t1643856950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
