﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1480628819MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3661115912(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2334175253 *, Dictionary_2_t3631115410 *, const MethodInfo*))ValueCollection__ctor_m360886506_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2560275074(__this, ___item0, method) ((  void (*) (ValueCollection_t2334175253 *, Texture2D_t3542995729 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2371726736_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2544922071(__this, method) ((  void (*) (ValueCollection_t2334175253 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m31724987_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1431807854(__this, ___item0, method) ((  bool (*) (ValueCollection_t2334175253 *, Texture2D_t3542995729 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m542925012_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3371577247(__this, ___item0, method) ((  bool (*) (ValueCollection_t2334175253 *, Texture2D_t3542995729 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2399896467_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2344397245(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2334175253 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2977964245_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m539039145(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2334175253 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2352087081_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1727512186(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2334175253 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1200661560_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m876936771(__this, method) ((  bool (*) (ValueCollection_t2334175253 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m670572463_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m211697613(__this, method) ((  bool (*) (ValueCollection_t2334175253 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1165288885_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3470390841(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2334175253 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1191576105_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m871532031(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2334175253 *, Texture2DU5BU5D_t2724090252*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2654148259_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1620441754(__this, method) ((  Enumerator_t1022680878  (*) (ValueCollection_t2334175253 *, const MethodInfo*))ValueCollection_GetEnumerator_m2927199816_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::get_Count()
#define ValueCollection_get_Count_m29326349(__this, method) ((  int32_t (*) (ValueCollection_t2334175253 *, const MethodInfo*))ValueCollection_get_Count_m217140693_gshared)(__this, method)
