﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FB_Score
struct FB_Score_t1450841581;
// System.Action`1<FB_Score>
struct Action_1_t1252640963;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FB_ProfileImageSize3003328130.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharp_FB_Score1450841581.h"

// System.Void FB_Score::.ctor()
extern "C"  void FB_Score__ctor_m2446538128 (FB_Score_t1450841581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_Score::add_OnProfileImageLoaded(System.Action`1<FB_Score>)
extern "C"  void FB_Score_add_OnProfileImageLoaded_m3287562317 (FB_Score_t1450841581 * __this, Action_1_t1252640963 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_Score::remove_OnProfileImageLoaded(System.Action`1<FB_Score>)
extern "C"  void FB_Score_remove_OnProfileImageLoaded_m4256801316 (FB_Score_t1450841581 * __this, Action_1_t1252640963 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_Score::GetProfileUrl(FB_ProfileImageSize)
extern "C"  String_t* FB_Score_GetProfileUrl_m421239753 (FB_Score_t1450841581 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D FB_Score::GetProfileImage(FB_ProfileImageSize)
extern "C"  Texture2D_t3542995729 * FB_Score_GetProfileImage_m3890021265 (FB_Score_t1450841581 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_Score::LoadProfileImage(FB_ProfileImageSize)
extern "C"  void FB_Score_LoadProfileImage_m2823572640 (FB_Score_t1450841581 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_Score::OnSquareImageLoaded(UnityEngine.Texture2D)
extern "C"  void FB_Score_OnSquareImageLoaded_m1275599060 (FB_Score_t1450841581 * __this, Texture2D_t3542995729 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_Score::OnLargeImageLoaded(UnityEngine.Texture2D)
extern "C"  void FB_Score_OnLargeImageLoaded_m523602112 (FB_Score_t1450841581 * __this, Texture2D_t3542995729 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_Score::OnNormalImageLoaded(UnityEngine.Texture2D)
extern "C"  void FB_Score_OnNormalImageLoaded_m2466187716 (FB_Score_t1450841581 * __this, Texture2D_t3542995729 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_Score::OnSmallImageLoaded(UnityEngine.Texture2D)
extern "C"  void FB_Score_OnSmallImageLoaded_m3102675288 (FB_Score_t1450841581 * __this, Texture2D_t3542995729 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_Score::<OnProfileImageLoaded>m__A1(FB_Score)
extern "C"  void FB_Score_U3COnProfileImageLoadedU3Em__A1_m1353749572 (Il2CppObject * __this /* static, unused */, FB_Score_t1450841581 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
