﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GP_SnapshotMeta>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3563528556(__this, ___l0, method) ((  void (*) (Enumerator_t258630245 *, List_1_t723900571 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GP_SnapshotMeta>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2600534126(__this, method) ((  void (*) (Enumerator_t258630245 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GP_SnapshotMeta>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3086333242(__this, method) ((  Il2CppObject * (*) (Enumerator_t258630245 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GP_SnapshotMeta>::Dispose()
#define Enumerator_Dispose_m1755325775(__this, method) ((  void (*) (Enumerator_t258630245 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GP_SnapshotMeta>::VerifyState()
#define Enumerator_VerifyState_m1580980610(__this, method) ((  void (*) (Enumerator_t258630245 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GP_SnapshotMeta>::MoveNext()
#define Enumerator_MoveNext_m2664781293(__this, method) ((  bool (*) (Enumerator_t258630245 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GP_SnapshotMeta>::get_Current()
#define Enumerator_get_Current_m898158213(__this, method) ((  GP_SnapshotMeta_t1354779439 * (*) (Enumerator_t258630245 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
