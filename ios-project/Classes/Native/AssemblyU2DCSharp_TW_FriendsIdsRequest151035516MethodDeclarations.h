﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TW_FriendsIdsRequest
struct TW_FriendsIdsRequest_t151035516;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TW_FriendsIdsRequest::.ctor()
extern "C"  void TW_FriendsIdsRequest__ctor_m4256127265 (TW_FriendsIdsRequest_t151035516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TW_FriendsIdsRequest TW_FriendsIdsRequest::Create()
extern "C"  TW_FriendsIdsRequest_t151035516 * TW_FriendsIdsRequest_Create_m3301839676 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_FriendsIdsRequest::Awake()
extern "C"  void TW_FriendsIdsRequest_Awake_m3622485612 (TW_FriendsIdsRequest_t151035516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_FriendsIdsRequest::OnResult(System.String)
extern "C"  void TW_FriendsIdsRequest_OnResult_m1127385675 (TW_FriendsIdsRequest_t151035516 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
