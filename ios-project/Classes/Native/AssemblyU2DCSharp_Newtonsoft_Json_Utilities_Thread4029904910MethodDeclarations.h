﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread3893850024MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m2936927542(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t4029904910 *, Func_2_t2832056028 *, const MethodInfo*))ThreadSafeStore_2__ctor_m1485686387_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>::Get(TKey)
#define ThreadSafeStore_2_Get_m297922516(__this, ___key0, method) ((  Func_2_t2825504181 * (*) (ThreadSafeStore_2_t4029904910 *, TypeConvertKey_t1788482786 , const MethodInfo*))ThreadSafeStore_2_Get_m1536546351_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m1124758784(__this, ___key0, method) ((  Func_2_t2825504181 * (*) (ThreadSafeStore_2_t4029904910 *, TypeConvertKey_t1788482786 , const MethodInfo*))ThreadSafeStore_2_AddValue_m3015471795_gshared)(__this, ___key0, method)
