﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BillingResult
struct BillingResult_t3511841850;
// System.String
struct String_t;
// GooglePurchaseTemplate
struct GooglePurchaseTemplate_t2609331866;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GooglePurchaseTemplate2609331866.h"

// System.Void BillingResult::.ctor(System.Int32,System.String,GooglePurchaseTemplate)
extern "C"  void BillingResult__ctor_m1087984048 (BillingResult_t3511841850 * __this, int32_t ___code0, String_t* ___msg1, GooglePurchaseTemplate_t2609331866 * ___p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BillingResult::.ctor(System.Int32,System.String)
extern "C"  void BillingResult__ctor_m3704378460 (BillingResult_t3511841850 * __this, int32_t ___code0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePurchaseTemplate BillingResult::get_purchase()
extern "C"  GooglePurchaseTemplate_t2609331866 * BillingResult_get_purchase_m4004840708 (BillingResult_t3511841850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BillingResult::get_response()
extern "C"  int32_t BillingResult_get_response_m3363962061 (BillingResult_t3511841850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String BillingResult::get_message()
extern "C"  String_t* BillingResult_get_message_m2209750360 (BillingResult_t3511841850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BillingResult::get_isSuccess()
extern "C"  bool BillingResult_get_isSuccess_m4109223105 (BillingResult_t3511841850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BillingResult::get_isFailure()
extern "C"  bool BillingResult_get_isFailure_m172522938 (BillingResult_t3511841850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
