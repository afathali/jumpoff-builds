﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PermissionsManager
struct PermissionsManager_t4266031513;
// System.Action`1<AN_GrantPermissionsResult>
struct Action_1_t52289039;
// System.String
struct String_t;
// AN_ManifestPermission[]
struct AN_ManifestPermissionU5BU5D_t246101855;
// System.String[]
struct StringU5BU5D_t1642385972;
// AN_GrantPermissionsResult
struct AN_GrantPermissionsResult_t250489657;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AN_ManifestPermission1772110202.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AN_GrantPermissionsResult250489657.h"

// System.Void PermissionsManager::.ctor()
extern "C"  void PermissionsManager__ctor_m3396652334 (PermissionsManager_t4266031513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PermissionsManager::.cctor()
extern "C"  void PermissionsManager__cctor_m2050031147 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PermissionsManager::add_ActionPermissionsRequestCompleted(System.Action`1<AN_GrantPermissionsResult>)
extern "C"  void PermissionsManager_add_ActionPermissionsRequestCompleted_m804982405 (Il2CppObject * __this /* static, unused */, Action_1_t52289039 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PermissionsManager::remove_ActionPermissionsRequestCompleted(System.Action`1<AN_GrantPermissionsResult>)
extern "C"  void PermissionsManager_remove_ActionPermissionsRequestCompleted_m3273904002 (Il2CppObject * __this /* static, unused */, Action_1_t52289039 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PermissionsManager::Awake()
extern "C"  void PermissionsManager_Awake_m1513520985 (PermissionsManager_t4266031513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PermissionsManager::IsPermissionGranted(AN_ManifestPermission)
extern "C"  bool PermissionsManager_IsPermissionGranted_m1078303162 (Il2CppObject * __this /* static, unused */, int32_t ___permission0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PermissionsManager::IsPermissionGranted(System.String)
extern "C"  bool PermissionsManager_IsPermissionGranted_m1492941176 (Il2CppObject * __this /* static, unused */, String_t* ___permission0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PermissionsManager::RequestPermissions(AN_ManifestPermission[])
extern "C"  void PermissionsManager_RequestPermissions_m401094395 (PermissionsManager_t4266031513 * __this, AN_ManifestPermissionU5BU5D_t246101855* ___permissions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PermissionsManager::RequestPermissions(System.String[])
extern "C"  void PermissionsManager_RequestPermissions_m1992532921 (PermissionsManager_t4266031513 * __this, StringU5BU5D_t1642385972* ___permissions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PermissionsManager::OnPermissionsResult(System.String)
extern "C"  void PermissionsManager_OnPermissionsResult_m3607339790 (PermissionsManager_t4266031513 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AN_ManifestPermission PermissionsManager::GetPermissionByName(System.String)
extern "C"  int32_t PermissionsManager_GetPermissionByName_m744578584 (Il2CppObject * __this /* static, unused */, String_t* ___fullName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PermissionsManager::<ActionPermissionsRequestCompleted>m__22(AN_GrantPermissionsResult)
extern "C"  void PermissionsManager_U3CActionPermissionsRequestCompletedU3Em__22_m117329986 (Il2CppObject * __this /* static, unused */, AN_GrantPermissionsResult_t250489657 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
