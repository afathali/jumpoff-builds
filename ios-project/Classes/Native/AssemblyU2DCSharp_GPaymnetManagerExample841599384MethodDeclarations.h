﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPaymnetManagerExample
struct GPaymnetManagerExample_t841599384;
// System.String
struct String_t;
// GooglePurchaseTemplate
struct GooglePurchaseTemplate_t2609331866;
// BillingResult
struct BillingResult_t3511841850;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GooglePurchaseTemplate2609331866.h"
#include "AssemblyU2DCSharp_BillingResult3511841850.h"

// System.Void GPaymnetManagerExample::.ctor()
extern "C"  void GPaymnetManagerExample__ctor_m2122808061 (GPaymnetManagerExample_t841599384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPaymnetManagerExample::.cctor()
extern "C"  void GPaymnetManagerExample__cctor_m2132294474 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPaymnetManagerExample::init()
extern "C"  void GPaymnetManagerExample_init_m116644627 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPaymnetManagerExample::purchase(System.String)
extern "C"  void GPaymnetManagerExample_purchase_m3597475454 (Il2CppObject * __this /* static, unused */, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPaymnetManagerExample::consume(System.String)
extern "C"  void GPaymnetManagerExample_consume_m3714213211 (Il2CppObject * __this /* static, unused */, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GPaymnetManagerExample::get_isInited()
extern "C"  bool GPaymnetManagerExample_get_isInited_m3363220569 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPaymnetManagerExample::OnProcessingPurchasedProduct(GooglePurchaseTemplate)
extern "C"  void GPaymnetManagerExample_OnProcessingPurchasedProduct_m1192751861 (Il2CppObject * __this /* static, unused */, GooglePurchaseTemplate_t2609331866 * ___purchase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPaymnetManagerExample::OnProcessingConsumeProduct(GooglePurchaseTemplate)
extern "C"  void GPaymnetManagerExample_OnProcessingConsumeProduct_m2780249114 (Il2CppObject * __this /* static, unused */, GooglePurchaseTemplate_t2609331866 * ___purchase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPaymnetManagerExample::OnProductPurchased(BillingResult)
extern "C"  void GPaymnetManagerExample_OnProductPurchased_m1999632618 (Il2CppObject * __this /* static, unused */, BillingResult_t3511841850 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPaymnetManagerExample::OnProductConsumed(BillingResult)
extern "C"  void GPaymnetManagerExample_OnProductConsumed_m4232625845 (Il2CppObject * __this /* static, unused */, BillingResult_t3511841850 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPaymnetManagerExample::OnBillingConnected(BillingResult)
extern "C"  void GPaymnetManagerExample_OnBillingConnected_m1455983352 (Il2CppObject * __this /* static, unused */, BillingResult_t3511841850 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPaymnetManagerExample::OnRetrieveProductsFinised(BillingResult)
extern "C"  void GPaymnetManagerExample_OnRetrieveProductsFinised_m576664056 (Il2CppObject * __this /* static, unused */, BillingResult_t3511841850 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
