﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Jump_Collider
struct Jump_Collider_t2378236225;

#include "codegen/il2cpp-codegen.h"

// System.Void Jump_Collider::.ctor()
extern "C"  void Jump_Collider__ctor_m2254453544 (Jump_Collider_t2378236225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
