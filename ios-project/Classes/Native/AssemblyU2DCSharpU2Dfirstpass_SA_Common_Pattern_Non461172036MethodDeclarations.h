﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Pattern.NonMonoSingleton`1<System.Object>
struct NonMonoSingleton_1_t461172036;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.Common.Pattern.NonMonoSingleton`1<System.Object>::.ctor()
extern "C"  void NonMonoSingleton_1__ctor_m3099661804_gshared (NonMonoSingleton_1_t461172036 * __this, const MethodInfo* method);
#define NonMonoSingleton_1__ctor_m3099661804(__this, method) ((  void (*) (NonMonoSingleton_1_t461172036 *, const MethodInfo*))NonMonoSingleton_1__ctor_m3099661804_gshared)(__this, method)
// System.Void SA.Common.Pattern.NonMonoSingleton`1<System.Object>::.cctor()
extern "C"  void NonMonoSingleton_1__cctor_m3625282977_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define NonMonoSingleton_1__cctor_m3625282977(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))NonMonoSingleton_1__cctor_m3625282977_gshared)(__this /* static, unused */, method)
// T SA.Common.Pattern.NonMonoSingleton`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * NonMonoSingleton_1_get_Instance_m2157798457_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define NonMonoSingleton_1_get_Instance_m2157798457(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))NonMonoSingleton_1_get_Instance_m2157798457_gshared)(__this /* static, unused */, method)
