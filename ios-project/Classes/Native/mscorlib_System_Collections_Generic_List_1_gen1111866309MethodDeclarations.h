﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct List_1_t1111866309;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct IEnumerable_1_t2034872222;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct IEnumerator_1_t3513236300;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct ICollection_1_t2694820482;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct ReadOnlyCollection_1_t1928530869;
// Newtonsoft.Json.Schema.JsonSchemaType[]
struct JsonSchemaTypeU5BU5D_t3104176164;
// System.Predicate`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct Predicate_1_t185715292;
// System.Collections.Generic.IComparer`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct IComparer_1_t3992175595;
// System.Comparison`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct Comparison_1_t3004484028;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat646595983.h"

// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor()
extern "C"  void List_1__ctor_m148626358_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1__ctor_m148626358(__this, method) ((  void (*) (List_1_t1111866309 *, const MethodInfo*))List_1__ctor_m148626358_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1243639046_gshared (List_1_t1111866309 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1243639046(__this, ___collection0, method) ((  void (*) (List_1_t1111866309 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1243639046_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m537215620_gshared (List_1_t1111866309 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m537215620(__this, ___capacity0, method) ((  void (*) (List_1_t1111866309 *, int32_t, const MethodInfo*))List_1__ctor_m537215620_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::.cctor()
extern "C"  void List_1__cctor_m3445819108_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3445819108(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3445819108_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4281793379_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4281793379(__this, method) ((  Il2CppObject* (*) (List_1_t1111866309 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4281793379_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1812565655_gshared (List_1_t1111866309 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1812565655(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1111866309 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1812565655_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m4128463602_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m4128463602(__this, method) ((  Il2CppObject * (*) (List_1_t1111866309 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m4128463602_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m857464591_gshared (List_1_t1111866309 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m857464591(__this, ___item0, method) ((  int32_t (*) (List_1_t1111866309 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m857464591_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m271221599_gshared (List_1_t1111866309 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m271221599(__this, ___item0, method) ((  bool (*) (List_1_t1111866309 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m271221599_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m136404349_gshared (List_1_t1111866309 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m136404349(__this, ___item0, method) ((  int32_t (*) (List_1_t1111866309 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m136404349_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m195290808_gshared (List_1_t1111866309 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m195290808(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1111866309 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m195290808_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3210918246_gshared (List_1_t1111866309 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3210918246(__this, ___item0, method) ((  void (*) (List_1_t1111866309 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3210918246_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4042457246_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4042457246(__this, method) ((  bool (*) (List_1_t1111866309 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4042457246_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3601377279_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3601377279(__this, method) ((  bool (*) (List_1_t1111866309 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3601377279_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2441036391_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2441036391(__this, method) ((  Il2CppObject * (*) (List_1_t1111866309 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2441036391_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m636545572_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m636545572(__this, method) ((  bool (*) (List_1_t1111866309 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m636545572_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2327127635_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2327127635(__this, method) ((  bool (*) (List_1_t1111866309 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2327127635_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m4239867312_gshared (List_1_t1111866309 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m4239867312(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1111866309 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m4239867312_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m159520501_gshared (List_1_t1111866309 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m159520501(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1111866309 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m159520501_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::Add(T)
extern "C"  void List_1_Add_m4022917570_gshared (List_1_t1111866309 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m4022917570(__this, ___item0, method) ((  void (*) (List_1_t1111866309 *, int32_t, const MethodInfo*))List_1_Add_m4022917570_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1239672123_gshared (List_1_t1111866309 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1239672123(__this, ___newCount0, method) ((  void (*) (List_1_t1111866309 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1239672123_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1861786051_gshared (List_1_t1111866309 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1861786051(__this, ___collection0, method) ((  void (*) (List_1_t1111866309 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1861786051_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2269133619_gshared (List_1_t1111866309 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2269133619(__this, ___enumerable0, method) ((  void (*) (List_1_t1111866309 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2269133619_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m691505362_gshared (List_1_t1111866309 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m691505362(__this, ___collection0, method) ((  void (*) (List_1_t1111866309 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m691505362_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1928530869 * List_1_AsReadOnly_m3559009979_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3559009979(__this, method) ((  ReadOnlyCollection_1_t1928530869 * (*) (List_1_t1111866309 *, const MethodInfo*))List_1_AsReadOnly_m3559009979_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::Clear()
extern "C"  void List_1_Clear_m2103423280_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_Clear_m2103423280(__this, method) ((  void (*) (List_1_t1111866309 *, const MethodInfo*))List_1_Clear_m2103423280_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::Contains(T)
extern "C"  bool List_1_Contains_m1456130682_gshared (List_1_t1111866309 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m1456130682(__this, ___item0, method) ((  bool (*) (List_1_t1111866309 *, int32_t, const MethodInfo*))List_1_Contains_m1456130682_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m3665878075_gshared (List_1_t1111866309 * __this, JsonSchemaTypeU5BU5D_t3104176164* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m3665878075(__this, ___array0, method) ((  void (*) (List_1_t1111866309 *, JsonSchemaTypeU5BU5D_t3104176164*, const MethodInfo*))List_1_CopyTo_m3665878075_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2692091208_gshared (List_1_t1111866309 * __this, JsonSchemaTypeU5BU5D_t3104176164* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2692091208(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1111866309 *, JsonSchemaTypeU5BU5D_t3104176164*, int32_t, const MethodInfo*))List_1_CopyTo_m2692091208_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m2918263444_gshared (List_1_t1111866309 * __this, Predicate_1_t185715292 * ___match0, const MethodInfo* method);
#define List_1_Find_m2918263444(__this, ___match0, method) ((  int32_t (*) (List_1_t1111866309 *, Predicate_1_t185715292 *, const MethodInfo*))List_1_Find_m2918263444_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1710299031_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t185715292 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1710299031(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t185715292 *, const MethodInfo*))List_1_CheckMatch_m1710299031_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1320793004_gshared (List_1_t1111866309 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t185715292 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1320793004(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1111866309 *, int32_t, int32_t, Predicate_1_t185715292 *, const MethodInfo*))List_1_GetIndex_m1320793004_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::GetEnumerator()
extern "C"  Enumerator_t646595983  List_1_GetEnumerator_m1827319403_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1827319403(__this, method) ((  Enumerator_t646595983  (*) (List_1_t1111866309 *, const MethodInfo*))List_1_GetEnumerator_m1827319403_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1685647358_gshared (List_1_t1111866309 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1685647358(__this, ___item0, method) ((  int32_t (*) (List_1_t1111866309 *, int32_t, const MethodInfo*))List_1_IndexOf_m1685647358_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3855083875_gshared (List_1_t1111866309 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3855083875(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1111866309 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3855083875_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1161413536_gshared (List_1_t1111866309 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1161413536(__this, ___index0, method) ((  void (*) (List_1_t1111866309 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1161413536_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2988011385_gshared (List_1_t1111866309 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m2988011385(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1111866309 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m2988011385_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m75182238_gshared (List_1_t1111866309 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m75182238(__this, ___collection0, method) ((  void (*) (List_1_t1111866309 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m75182238_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::Remove(T)
extern "C"  bool List_1_Remove_m1349644489_gshared (List_1_t1111866309 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m1349644489(__this, ___item0, method) ((  bool (*) (List_1_t1111866309 *, int32_t, const MethodInfo*))List_1_Remove_m1349644489_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m829879831_gshared (List_1_t1111866309 * __this, Predicate_1_t185715292 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m829879831(__this, ___match0, method) ((  int32_t (*) (List_1_t1111866309 *, Predicate_1_t185715292 *, const MethodInfo*))List_1_RemoveAll_m829879831_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3084531509_gshared (List_1_t1111866309 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3084531509(__this, ___index0, method) ((  void (*) (List_1_t1111866309 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3084531509_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::Reverse()
extern "C"  void List_1_Reverse_m66990723_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_Reverse_m66990723(__this, method) ((  void (*) (List_1_t1111866309 *, const MethodInfo*))List_1_Reverse_m66990723_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::Sort()
extern "C"  void List_1_Sort_m439980761_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_Sort_m439980761(__this, method) ((  void (*) (List_1_t1111866309 *, const MethodInfo*))List_1_Sort_m439980761_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m4076903197_gshared (List_1_t1111866309 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m4076903197(__this, ___comparer0, method) ((  void (*) (List_1_t1111866309 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m4076903197_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2450932476_gshared (List_1_t1111866309 * __this, Comparison_1_t3004484028 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2450932476(__this, ___comparison0, method) ((  void (*) (List_1_t1111866309 *, Comparison_1_t3004484028 *, const MethodInfo*))List_1_Sort_m2450932476_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::ToArray()
extern "C"  JsonSchemaTypeU5BU5D_t3104176164* List_1_ToArray_m3039117260_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_ToArray_m3039117260(__this, method) ((  JsonSchemaTypeU5BU5D_t3104176164* (*) (List_1_t1111866309 *, const MethodInfo*))List_1_ToArray_m3039117260_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::TrimExcess()
extern "C"  void List_1_TrimExcess_m903132114_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m903132114(__this, method) ((  void (*) (List_1_t1111866309 *, const MethodInfo*))List_1_TrimExcess_m903132114_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3502283048_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3502283048(__this, method) ((  int32_t (*) (List_1_t1111866309 *, const MethodInfo*))List_1_get_Capacity_m3502283048_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2156608739_gshared (List_1_t1111866309 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2156608739(__this, ___value0, method) ((  void (*) (List_1_t1111866309 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2156608739_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Count()
extern "C"  int32_t List_1_get_Count_m131439359_gshared (List_1_t1111866309 * __this, const MethodInfo* method);
#define List_1_get_Count_m131439359(__this, method) ((  int32_t (*) (List_1_t1111866309 *, const MethodInfo*))List_1_get_Count_m131439359_gshared)(__this, method)
// T System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m220789433_gshared (List_1_t1111866309 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m220789433(__this, ___index0, method) ((  int32_t (*) (List_1_t1111866309 *, int32_t, const MethodInfo*))List_1_get_Item_m220789433_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2235881346_gshared (List_1_t1111866309 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m2235881346(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1111866309 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m2235881346_gshared)(__this, ___index0, ___value1, method)
