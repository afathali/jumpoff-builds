﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketExample/<SendRequest>c__Iterator2
struct U3CSendRequestU3Ec__Iterator2_t714520321;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MarketExample/<SendRequest>c__Iterator2::.ctor()
extern "C"  void U3CSendRequestU3Ec__Iterator2__ctor_m2402304764 (U3CSendRequestU3Ec__Iterator2_t714520321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MarketExample/<SendRequest>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendRequestU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3218401866 (U3CSendRequestU3Ec__Iterator2_t714520321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MarketExample/<SendRequest>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendRequestU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1218021090 (U3CSendRequestU3Ec__Iterator2_t714520321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MarketExample/<SendRequest>c__Iterator2::MoveNext()
extern "C"  bool U3CSendRequestU3Ec__Iterator2_MoveNext_m4012205320 (U3CSendRequestU3Ec__Iterator2_t714520321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample/<SendRequest>c__Iterator2::Dispose()
extern "C"  void U3CSendRequestU3Ec__Iterator2_Dispose_m1699039545 (U3CSendRequestU3Ec__Iterator2_t714520321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample/<SendRequest>c__Iterator2::Reset()
extern "C"  void U3CSendRequestU3Ec__Iterator2_Reset_m4135404951 (U3CSendRequestU3Ec__Iterator2_t714520321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
