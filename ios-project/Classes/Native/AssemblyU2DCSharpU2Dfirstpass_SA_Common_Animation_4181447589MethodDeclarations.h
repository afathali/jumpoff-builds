﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Animation.ValuesTween
struct ValuesTween_t4181447589;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.Single>
struct Action_1_t1878309314;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_t2045506962;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Animation_2123337604.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void SA.Common.Animation.ValuesTween::.ctor()
extern "C"  void ValuesTween__ctor_m1494584311 (ValuesTween_t4181447589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::add_OnComplete(System.Action)
extern "C"  void ValuesTween_add_OnComplete_m205323862 (ValuesTween_t4181447589 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::remove_OnComplete(System.Action)
extern "C"  void ValuesTween_remove_OnComplete_m7989923 (ValuesTween_t4181447589 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::add_OnValueChanged(System.Action`1<System.Single>)
extern "C"  void ValuesTween_add_OnValueChanged_m3920362804 (ValuesTween_t4181447589 * __this, Action_1_t1878309314 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::remove_OnValueChanged(System.Action`1<System.Single>)
extern "C"  void ValuesTween_remove_OnValueChanged_m3897741613 (ValuesTween_t4181447589 * __this, Action_1_t1878309314 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::add_OnVectorValueChanged(System.Action`1<UnityEngine.Vector3>)
extern "C"  void ValuesTween_add_OnVectorValueChanged_m3131916367 (ValuesTween_t4181447589 * __this, Action_1_t2045506962 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::remove_OnVectorValueChanged(System.Action`1<UnityEngine.Vector3>)
extern "C"  void ValuesTween_remove_OnVectorValueChanged_m3151833966 (ValuesTween_t4181447589 * __this, Action_1_t2045506962 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SA.Common.Animation.ValuesTween SA.Common.Animation.ValuesTween::Create()
extern "C"  ValuesTween_t4181447589 * ValuesTween_Create_m3355431774 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::Update()
extern "C"  void ValuesTween_Update_m223920684 (ValuesTween_t4181447589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::ValueTo(System.Single,System.Single,System.Single,SA.Common.Animation.EaseType)
extern "C"  void ValuesTween_ValueTo_m3388519367 (ValuesTween_t4181447589 * __this, float ___from0, float ___to1, float ___time2, int32_t ___easeType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::VectorTo(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,SA.Common.Animation.EaseType)
extern "C"  void ValuesTween_VectorTo_m538993157 (ValuesTween_t4181447589 * __this, Vector3_t2243707580  ___from0, Vector3_t2243707580  ___to1, float ___time2, int32_t ___easeType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::ScaleTo(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,SA.Common.Animation.EaseType)
extern "C"  void ValuesTween_ScaleTo_m3693444612 (ValuesTween_t4181447589 * __this, Vector3_t2243707580  ___from0, Vector3_t2243707580  ___to1, float ___time2, int32_t ___easeType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::VectorToS(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,SA.Common.Animation.EaseType)
extern "C"  void ValuesTween_VectorToS_m2984789324 (ValuesTween_t4181447589 * __this, Vector3_t2243707580  ___from0, Vector3_t2243707580  ___to1, float ___speed2, int32_t ___easeType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::Stop()
extern "C"  void ValuesTween_Stop_m1272304863 (ValuesTween_t4181447589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::onTweenComplete()
extern "C"  void ValuesTween_onTweenComplete_m3852257970 (ValuesTween_t4181447589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::<OnComplete>m__7A()
extern "C"  void ValuesTween_U3COnCompleteU3Em__7A_m4128660100 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::<OnValueChanged>m__7B(System.Single)
extern "C"  void ValuesTween_U3COnValueChangedU3Em__7B_m2740858198 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.ValuesTween::<OnVectorValueChanged>m__7C(UnityEngine.Vector3)
extern "C"  void ValuesTween_U3COnVectorValueChangedU3Em__7C_m3911287770 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
