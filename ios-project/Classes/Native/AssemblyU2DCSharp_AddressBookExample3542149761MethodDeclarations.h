﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AddressBookExample
struct AddressBookExample_t3542149761;

#include "codegen/il2cpp-codegen.h"

// System.Void AddressBookExample::.ctor()
extern "C"  void AddressBookExample__ctor_m4168925032 (AddressBookExample_t3542149761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBookExample::LoadAdressBook()
extern "C"  void AddressBookExample_LoadAdressBook_m1247088833 (AddressBookExample_t3542149761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBookExample::OnContactsLoaded()
extern "C"  void AddressBookExample_OnContactsLoaded_m4075865361 (AddressBookExample_t3542149761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
