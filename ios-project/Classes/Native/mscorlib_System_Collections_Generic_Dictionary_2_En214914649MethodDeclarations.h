﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m582984910(__this, ___dictionary0, method) ((  void (*) (Enumerator_t214914649 *, Dictionary_2_t3189857243 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3572439807(__this, method) ((  Il2CppObject * (*) (Enumerator_t214914649 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3235764919(__this, method) ((  void (*) (Enumerator_t214914649 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3982037510(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t214914649 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1515292221(__this, method) ((  Il2CppObject * (*) (Enumerator_t214914649 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1805292757(__this, method) ((  Il2CppObject * (*) (Enumerator_t214914649 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::MoveNext()
#define Enumerator_MoveNext_m1732019283(__this, method) ((  bool (*) (Enumerator_t214914649 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::get_Current()
#define Enumerator_get_Current_m285235003(__this, method) ((  KeyValuePair_2_t947202465  (*) (Enumerator_t214914649 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3042029006(__this, method) ((  String_t* (*) (Enumerator_t214914649 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m385483838(__this, method) ((  GP_TBM_Match_t1275077981 * (*) (Enumerator_t214914649 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::Reset()
#define Enumerator_Reset_m200256400(__this, method) ((  void (*) (Enumerator_t214914649 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::VerifyState()
#define Enumerator_VerifyState_m790189573(__this, method) ((  void (*) (Enumerator_t214914649 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1514228079(__this, method) ((  void (*) (Enumerator_t214914649 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_TBM_Match>::Dispose()
#define Enumerator_Dispose_m747633730(__this, method) ((  void (*) (Enumerator_t214914649 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
