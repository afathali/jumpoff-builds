﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>
struct List_1_t676948345;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JTokenType>
struct IEnumerable_1_t1599954258;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JTokenType>
struct IEnumerator_1_t3078318336;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Newtonsoft.Json.Linq.JTokenType>
struct ICollection_1_t2259902518;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>
struct ReadOnlyCollection_1_t1493612905;
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t1368357152;
// System.Predicate`1<Newtonsoft.Json.Linq.JTokenType>
struct Predicate_1_t4045764624;
// System.Collections.Generic.IComparer`1<Newtonsoft.Json.Linq.JTokenType>
struct IComparer_1_t3557257631;
// System.Comparison`1<Newtonsoft.Json.Linq.JTokenType>
struct Comparison_1_t2569566064;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType1307827213.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat211678019.h"

// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::.ctor()
extern "C"  void List_1__ctor_m293058543_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1__ctor_m293058543(__this, method) ((  void (*) (List_1_t676948345 *, const MethodInfo*))List_1__ctor_m293058543_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m224414339_gshared (List_1_t676948345 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m224414339(__this, ___collection0, method) ((  void (*) (List_1_t676948345 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m224414339_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1514409906_gshared (List_1_t676948345 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1514409906(__this, ___capacity0, method) ((  void (*) (List_1_t676948345 *, int32_t, const MethodInfo*))List_1__ctor_m1514409906_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::.cctor()
extern "C"  void List_1__cctor_m967487939_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m967487939(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m967487939_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m743937674_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m743937674(__this, method) ((  Il2CppObject* (*) (List_1_t676948345 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m743937674_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3026333044_gshared (List_1_t676948345 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3026333044(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t676948345 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3026333044_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1450811881_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1450811881(__this, method) ((  Il2CppObject * (*) (List_1_t676948345 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1450811881_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2416825420_gshared (List_1_t676948345 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2416825420(__this, ___item0, method) ((  int32_t (*) (List_1_t676948345 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2416825420_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m490839706_gshared (List_1_t676948345 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m490839706(__this, ___item0, method) ((  bool (*) (List_1_t676948345 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m490839706_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1088982994_gshared (List_1_t676948345 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1088982994(__this, ___item0, method) ((  int32_t (*) (List_1_t676948345 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1088982994_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3083304881_gshared (List_1_t676948345 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3083304881(__this, ___index0, ___item1, method) ((  void (*) (List_1_t676948345 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3083304881_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m980781825_gshared (List_1_t676948345 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m980781825(__this, ___item0, method) ((  void (*) (List_1_t676948345 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m980781825_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m756812253_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m756812253(__this, method) ((  bool (*) (List_1_t676948345 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m756812253_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m438010620_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m438010620(__this, method) ((  bool (*) (List_1_t676948345 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m438010620_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1538477316_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1538477316(__this, method) ((  Il2CppObject * (*) (List_1_t676948345 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1538477316_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1885562421_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1885562421(__this, method) ((  bool (*) (List_1_t676948345 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1885562421_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1995059688_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1995059688(__this, method) ((  bool (*) (List_1_t676948345 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1995059688_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m341939069_gshared (List_1_t676948345 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m341939069(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t676948345 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m341939069_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m353390614_gshared (List_1_t676948345 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m353390614(__this, ___index0, ___value1, method) ((  void (*) (List_1_t676948345 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m353390614_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Add(T)
extern "C"  void List_1_Add_m3101348419_gshared (List_1_t676948345 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m3101348419(__this, ___item0, method) ((  void (*) (List_1_t676948345 *, int32_t, const MethodInfo*))List_1_Add_m3101348419_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1700123118_gshared (List_1_t676948345 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1700123118(__this, ___newCount0, method) ((  void (*) (List_1_t676948345 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1700123118_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1601362374_gshared (List_1_t676948345 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1601362374(__this, ___collection0, method) ((  void (*) (List_1_t676948345 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1601362374_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1912989494_gshared (List_1_t676948345 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1912989494(__this, ___enumerable0, method) ((  void (*) (List_1_t676948345 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1912989494_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3717319993_gshared (List_1_t676948345 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3717319993(__this, ___collection0, method) ((  void (*) (List_1_t676948345 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3717319993_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1493612905 * List_1_AsReadOnly_m2957239488_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2957239488(__this, method) ((  ReadOnlyCollection_1_t1493612905 * (*) (List_1_t676948345 *, const MethodInfo*))List_1_AsReadOnly_m2957239488_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Clear()
extern "C"  void List_1_Clear_m3523035449_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_Clear_m3523035449(__this, method) ((  void (*) (List_1_t676948345 *, const MethodInfo*))List_1_Clear_m3523035449_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Contains(T)
extern "C"  bool List_1_Contains_m2371812587_gshared (List_1_t676948345 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m2371812587(__this, ___item0, method) ((  bool (*) (List_1_t676948345 *, int32_t, const MethodInfo*))List_1_Contains_m2371812587_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m243278988_gshared (List_1_t676948345 * __this, JTokenTypeU5BU5D_t1368357152* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m243278988(__this, ___array0, method) ((  void (*) (List_1_t676948345 *, JTokenTypeU5BU5D_t1368357152*, const MethodInfo*))List_1_CopyTo_m243278988_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3677821221_gshared (List_1_t676948345 * __this, JTokenTypeU5BU5D_t1368357152* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3677821221(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t676948345 *, JTokenTypeU5BU5D_t1368357152*, int32_t, const MethodInfo*))List_1_CopyTo_m3677821221_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m39668183_gshared (List_1_t676948345 * __this, Predicate_1_t4045764624 * ___match0, const MethodInfo* method);
#define List_1_Find_m39668183(__this, ___match0, method) ((  int32_t (*) (List_1_t676948345 *, Predicate_1_t4045764624 *, const MethodInfo*))List_1_Find_m39668183_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3966594068_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t4045764624 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3966594068(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t4045764624 *, const MethodInfo*))List_1_CheckMatch_m3966594068_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2358877219_gshared (List_1_t676948345 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t4045764624 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2358877219(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t676948345 *, int32_t, int32_t, Predicate_1_t4045764624 *, const MethodInfo*))List_1_GetIndex_m2358877219_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::GetEnumerator()
extern "C"  Enumerator_t211678019  List_1_GetEnumerator_m2132204028_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2132204028(__this, method) ((  Enumerator_t211678019  (*) (List_1_t676948345 *, const MethodInfo*))List_1_GetEnumerator_m2132204028_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2474268625_gshared (List_1_t676948345 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2474268625(__this, ___item0, method) ((  int32_t (*) (List_1_t676948345 *, int32_t, const MethodInfo*))List_1_IndexOf_m2474268625_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m808223720_gshared (List_1_t676948345 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m808223720(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t676948345 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m808223720_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3546569161_gshared (List_1_t676948345 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3546569161(__this, ___index0, method) ((  void (*) (List_1_t676948345 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3546569161_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3858269262_gshared (List_1_t676948345 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m3858269262(__this, ___index0, ___item1, method) ((  void (*) (List_1_t676948345 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m3858269262_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m44359023_gshared (List_1_t676948345 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m44359023(__this, ___collection0, method) ((  void (*) (List_1_t676948345 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m44359023_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Remove(T)
extern "C"  bool List_1_Remove_m2432911120_gshared (List_1_t676948345 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m2432911120(__this, ___item0, method) ((  bool (*) (List_1_t676948345 *, int32_t, const MethodInfo*))List_1_Remove_m2432911120_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2420569444_gshared (List_1_t676948345 * __this, Predicate_1_t4045764624 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2420569444(__this, ___match0, method) ((  int32_t (*) (List_1_t676948345 *, Predicate_1_t4045764624 *, const MethodInfo*))List_1_RemoveAll_m2420569444_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1512268857_gshared (List_1_t676948345 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1512268857(__this, ___index0, method) ((  void (*) (List_1_t676948345 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1512268857_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Reverse()
extern "C"  void List_1_Reverse_m3772327978_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_Reverse_m3772327978(__this, method) ((  void (*) (List_1_t676948345 *, const MethodInfo*))List_1_Reverse_m3772327978_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Sort()
extern "C"  void List_1_Sort_m3509562834_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_Sort_m3509562834(__this, method) ((  void (*) (List_1_t676948345 *, const MethodInfo*))List_1_Sort_m3509562834_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1912729902_gshared (List_1_t676948345 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1912729902(__this, ___comparer0, method) ((  void (*) (List_1_t676948345 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1912729902_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2317359723_gshared (List_1_t676948345 * __this, Comparison_1_t2569566064 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2317359723(__this, ___comparison0, method) ((  void (*) (List_1_t676948345 *, Comparison_1_t2569566064 *, const MethodInfo*))List_1_Sort_m2317359723_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::ToArray()
extern "C"  JTokenTypeU5BU5D_t1368357152* List_1_ToArray_m3011088635_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_ToArray_m3011088635(__this, method) ((  JTokenTypeU5BU5D_t1368357152* (*) (List_1_t676948345 *, const MethodInfo*))List_1_ToArray_m3011088635_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::TrimExcess()
extern "C"  void List_1_TrimExcess_m826149941_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m826149941(__this, method) ((  void (*) (List_1_t676948345 *, const MethodInfo*))List_1_TrimExcess_m826149941_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3775075219_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3775075219(__this, method) ((  int32_t (*) (List_1_t676948345 *, const MethodInfo*))List_1_get_Capacity_m3775075219_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2818964294_gshared (List_1_t676948345 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2818964294(__this, ___value0, method) ((  void (*) (List_1_t676948345 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2818964294_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::get_Count()
extern "C"  int32_t List_1_get_Count_m2529903351_gshared (List_1_t676948345 * __this, const MethodInfo* method);
#define List_1_get_Count_m2529903351(__this, method) ((  int32_t (*) (List_1_t676948345 *, const MethodInfo*))List_1_get_Count_m2529903351_gshared)(__this, method)
// T System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m1399647136_gshared (List_1_t676948345 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1399647136(__this, ___index0, method) ((  int32_t (*) (List_1_t676948345 *, int32_t, const MethodInfo*))List_1_get_Item_m1399647136_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3792972923_gshared (List_1_t676948345 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m3792972923(__this, ___index0, ___value1, method) ((  void (*) (List_1_t676948345 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m3792972923_gshared)(__this, ___index0, ___value1, method)
