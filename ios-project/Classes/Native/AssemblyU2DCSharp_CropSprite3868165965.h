﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CropSprite
struct  CropSprite_t3868165965  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject CropSprite::spriteToCrop
	GameObject_t1756533147 * ___spriteToCrop_2;
	// UnityEngine.Vector3 CropSprite::startPoint
	Vector3_t2243707580  ___startPoint_3;
	// UnityEngine.Vector3 CropSprite::endPoint
	Vector3_t2243707580  ___endPoint_4;
	// System.Boolean CropSprite::isMousePressed
	bool ___isMousePressed_5;
	// UnityEngine.LineRenderer CropSprite::leftLine
	LineRenderer_t849157671 * ___leftLine_6;
	// UnityEngine.LineRenderer CropSprite::rightLine
	LineRenderer_t849157671 * ___rightLine_7;
	// UnityEngine.LineRenderer CropSprite::topLine
	LineRenderer_t849157671 * ___topLine_8;
	// UnityEngine.LineRenderer CropSprite::bottomLine
	LineRenderer_t849157671 * ___bottomLine_9;

public:
	inline static int32_t get_offset_of_spriteToCrop_2() { return static_cast<int32_t>(offsetof(CropSprite_t3868165965, ___spriteToCrop_2)); }
	inline GameObject_t1756533147 * get_spriteToCrop_2() const { return ___spriteToCrop_2; }
	inline GameObject_t1756533147 ** get_address_of_spriteToCrop_2() { return &___spriteToCrop_2; }
	inline void set_spriteToCrop_2(GameObject_t1756533147 * value)
	{
		___spriteToCrop_2 = value;
		Il2CppCodeGenWriteBarrier(&___spriteToCrop_2, value);
	}

	inline static int32_t get_offset_of_startPoint_3() { return static_cast<int32_t>(offsetof(CropSprite_t3868165965, ___startPoint_3)); }
	inline Vector3_t2243707580  get_startPoint_3() const { return ___startPoint_3; }
	inline Vector3_t2243707580 * get_address_of_startPoint_3() { return &___startPoint_3; }
	inline void set_startPoint_3(Vector3_t2243707580  value)
	{
		___startPoint_3 = value;
	}

	inline static int32_t get_offset_of_endPoint_4() { return static_cast<int32_t>(offsetof(CropSprite_t3868165965, ___endPoint_4)); }
	inline Vector3_t2243707580  get_endPoint_4() const { return ___endPoint_4; }
	inline Vector3_t2243707580 * get_address_of_endPoint_4() { return &___endPoint_4; }
	inline void set_endPoint_4(Vector3_t2243707580  value)
	{
		___endPoint_4 = value;
	}

	inline static int32_t get_offset_of_isMousePressed_5() { return static_cast<int32_t>(offsetof(CropSprite_t3868165965, ___isMousePressed_5)); }
	inline bool get_isMousePressed_5() const { return ___isMousePressed_5; }
	inline bool* get_address_of_isMousePressed_5() { return &___isMousePressed_5; }
	inline void set_isMousePressed_5(bool value)
	{
		___isMousePressed_5 = value;
	}

	inline static int32_t get_offset_of_leftLine_6() { return static_cast<int32_t>(offsetof(CropSprite_t3868165965, ___leftLine_6)); }
	inline LineRenderer_t849157671 * get_leftLine_6() const { return ___leftLine_6; }
	inline LineRenderer_t849157671 ** get_address_of_leftLine_6() { return &___leftLine_6; }
	inline void set_leftLine_6(LineRenderer_t849157671 * value)
	{
		___leftLine_6 = value;
		Il2CppCodeGenWriteBarrier(&___leftLine_6, value);
	}

	inline static int32_t get_offset_of_rightLine_7() { return static_cast<int32_t>(offsetof(CropSprite_t3868165965, ___rightLine_7)); }
	inline LineRenderer_t849157671 * get_rightLine_7() const { return ___rightLine_7; }
	inline LineRenderer_t849157671 ** get_address_of_rightLine_7() { return &___rightLine_7; }
	inline void set_rightLine_7(LineRenderer_t849157671 * value)
	{
		___rightLine_7 = value;
		Il2CppCodeGenWriteBarrier(&___rightLine_7, value);
	}

	inline static int32_t get_offset_of_topLine_8() { return static_cast<int32_t>(offsetof(CropSprite_t3868165965, ___topLine_8)); }
	inline LineRenderer_t849157671 * get_topLine_8() const { return ___topLine_8; }
	inline LineRenderer_t849157671 ** get_address_of_topLine_8() { return &___topLine_8; }
	inline void set_topLine_8(LineRenderer_t849157671 * value)
	{
		___topLine_8 = value;
		Il2CppCodeGenWriteBarrier(&___topLine_8, value);
	}

	inline static int32_t get_offset_of_bottomLine_9() { return static_cast<int32_t>(offsetof(CropSprite_t3868165965, ___bottomLine_9)); }
	inline LineRenderer_t849157671 * get_bottomLine_9() const { return ___bottomLine_9; }
	inline LineRenderer_t849157671 ** get_address_of_bottomLine_9() { return &___bottomLine_9; }
	inline void set_bottomLine_9(LineRenderer_t849157671 * value)
	{
		___bottomLine_9 = value;
		Il2CppCodeGenWriteBarrier(&___bottomLine_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
