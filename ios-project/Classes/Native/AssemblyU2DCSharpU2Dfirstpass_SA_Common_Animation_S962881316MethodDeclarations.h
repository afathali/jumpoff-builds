﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Animation.SA_iTween/<TweenRestart>c__Iterator8
struct U3CTweenRestartU3Ec__Iterator8_t962881316;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.Common.Animation.SA_iTween/<TweenRestart>c__Iterator8::.ctor()
extern "C"  void U3CTweenRestartU3Ec__Iterator8__ctor_m1351269265 (U3CTweenRestartU3Ec__Iterator8_t962881316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Animation.SA_iTween/<TweenRestart>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTweenRestartU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m397940887 (U3CTweenRestartU3Ec__Iterator8_t962881316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Animation.SA_iTween/<TweenRestart>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTweenRestartU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m284727567 (U3CTweenRestartU3Ec__Iterator8_t962881316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA.Common.Animation.SA_iTween/<TweenRestart>c__Iterator8::MoveNext()
extern "C"  bool U3CTweenRestartU3Ec__Iterator8_MoveNext_m46813663 (U3CTweenRestartU3Ec__Iterator8_t962881316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween/<TweenRestart>c__Iterator8::Dispose()
extern "C"  void U3CTweenRestartU3Ec__Iterator8_Dispose_m110247696 (U3CTweenRestartU3Ec__Iterator8_t962881316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween/<TweenRestart>c__Iterator8::Reset()
extern "C"  void U3CTweenRestartU3Ec__Iterator8_Reset_m3033678058 (U3CTweenRestartU3Ec__Iterator8_t962881316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
