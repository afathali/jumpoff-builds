﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_AN_InAppType2513116246.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleProductTemplate
struct  GoogleProductTemplate_t1112616324  : public Il2CppObject
{
public:
	// System.Boolean GoogleProductTemplate::IsOpen
	bool ___IsOpen_0;
	// System.String GoogleProductTemplate::SKU
	String_t* ___SKU_1;
	// System.String GoogleProductTemplate::_OriginalJson
	String_t* ____OriginalJson_2;
	// System.String GoogleProductTemplate::_LocalizedPrice
	String_t* ____LocalizedPrice_3;
	// System.Int64 GoogleProductTemplate::_PriceAmountMicros
	int64_t ____PriceAmountMicros_4;
	// System.String GoogleProductTemplate::_PriceCurrencyCode
	String_t* ____PriceCurrencyCode_5;
	// System.String GoogleProductTemplate::_Description
	String_t* ____Description_6;
	// System.String GoogleProductTemplate::_Title
	String_t* ____Title_7;
	// UnityEngine.Texture2D GoogleProductTemplate::_Texture
	Texture2D_t3542995729 * ____Texture_8;
	// AN_InAppType GoogleProductTemplate::_ProductType
	int32_t ____ProductType_9;

public:
	inline static int32_t get_offset_of_IsOpen_0() { return static_cast<int32_t>(offsetof(GoogleProductTemplate_t1112616324, ___IsOpen_0)); }
	inline bool get_IsOpen_0() const { return ___IsOpen_0; }
	inline bool* get_address_of_IsOpen_0() { return &___IsOpen_0; }
	inline void set_IsOpen_0(bool value)
	{
		___IsOpen_0 = value;
	}

	inline static int32_t get_offset_of_SKU_1() { return static_cast<int32_t>(offsetof(GoogleProductTemplate_t1112616324, ___SKU_1)); }
	inline String_t* get_SKU_1() const { return ___SKU_1; }
	inline String_t** get_address_of_SKU_1() { return &___SKU_1; }
	inline void set_SKU_1(String_t* value)
	{
		___SKU_1 = value;
		Il2CppCodeGenWriteBarrier(&___SKU_1, value);
	}

	inline static int32_t get_offset_of__OriginalJson_2() { return static_cast<int32_t>(offsetof(GoogleProductTemplate_t1112616324, ____OriginalJson_2)); }
	inline String_t* get__OriginalJson_2() const { return ____OriginalJson_2; }
	inline String_t** get_address_of__OriginalJson_2() { return &____OriginalJson_2; }
	inline void set__OriginalJson_2(String_t* value)
	{
		____OriginalJson_2 = value;
		Il2CppCodeGenWriteBarrier(&____OriginalJson_2, value);
	}

	inline static int32_t get_offset_of__LocalizedPrice_3() { return static_cast<int32_t>(offsetof(GoogleProductTemplate_t1112616324, ____LocalizedPrice_3)); }
	inline String_t* get__LocalizedPrice_3() const { return ____LocalizedPrice_3; }
	inline String_t** get_address_of__LocalizedPrice_3() { return &____LocalizedPrice_3; }
	inline void set__LocalizedPrice_3(String_t* value)
	{
		____LocalizedPrice_3 = value;
		Il2CppCodeGenWriteBarrier(&____LocalizedPrice_3, value);
	}

	inline static int32_t get_offset_of__PriceAmountMicros_4() { return static_cast<int32_t>(offsetof(GoogleProductTemplate_t1112616324, ____PriceAmountMicros_4)); }
	inline int64_t get__PriceAmountMicros_4() const { return ____PriceAmountMicros_4; }
	inline int64_t* get_address_of__PriceAmountMicros_4() { return &____PriceAmountMicros_4; }
	inline void set__PriceAmountMicros_4(int64_t value)
	{
		____PriceAmountMicros_4 = value;
	}

	inline static int32_t get_offset_of__PriceCurrencyCode_5() { return static_cast<int32_t>(offsetof(GoogleProductTemplate_t1112616324, ____PriceCurrencyCode_5)); }
	inline String_t* get__PriceCurrencyCode_5() const { return ____PriceCurrencyCode_5; }
	inline String_t** get_address_of__PriceCurrencyCode_5() { return &____PriceCurrencyCode_5; }
	inline void set__PriceCurrencyCode_5(String_t* value)
	{
		____PriceCurrencyCode_5 = value;
		Il2CppCodeGenWriteBarrier(&____PriceCurrencyCode_5, value);
	}

	inline static int32_t get_offset_of__Description_6() { return static_cast<int32_t>(offsetof(GoogleProductTemplate_t1112616324, ____Description_6)); }
	inline String_t* get__Description_6() const { return ____Description_6; }
	inline String_t** get_address_of__Description_6() { return &____Description_6; }
	inline void set__Description_6(String_t* value)
	{
		____Description_6 = value;
		Il2CppCodeGenWriteBarrier(&____Description_6, value);
	}

	inline static int32_t get_offset_of__Title_7() { return static_cast<int32_t>(offsetof(GoogleProductTemplate_t1112616324, ____Title_7)); }
	inline String_t* get__Title_7() const { return ____Title_7; }
	inline String_t** get_address_of__Title_7() { return &____Title_7; }
	inline void set__Title_7(String_t* value)
	{
		____Title_7 = value;
		Il2CppCodeGenWriteBarrier(&____Title_7, value);
	}

	inline static int32_t get_offset_of__Texture_8() { return static_cast<int32_t>(offsetof(GoogleProductTemplate_t1112616324, ____Texture_8)); }
	inline Texture2D_t3542995729 * get__Texture_8() const { return ____Texture_8; }
	inline Texture2D_t3542995729 ** get_address_of__Texture_8() { return &____Texture_8; }
	inline void set__Texture_8(Texture2D_t3542995729 * value)
	{
		____Texture_8 = value;
		Il2CppCodeGenWriteBarrier(&____Texture_8, value);
	}

	inline static int32_t get_offset_of__ProductType_9() { return static_cast<int32_t>(offsetof(GoogleProductTemplate_t1112616324, ____ProductType_9)); }
	inline int32_t get__ProductType_9() const { return ____ProductType_9; }
	inline int32_t* get_address_of__ProductType_9() { return &____ProductType_9; }
	inline void set__ProductType_9(int32_t value)
	{
		____ProductType_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
