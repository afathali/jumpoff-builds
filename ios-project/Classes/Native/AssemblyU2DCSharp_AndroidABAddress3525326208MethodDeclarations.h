﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidABAddress
struct AndroidABAddress_t3525326208;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidABAddress::.ctor()
extern "C"  void AndroidABAddress__ctor_m1551325013 (AndroidABAddress_t3525326208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
