﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase
struct SampleBase_t2925764113;
// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleChild
struct SampleChild_t2736175310;
// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject
struct SimpleClassObject_t1988087395;

#include "codegen/il2cpp-codegen.h"

// System.Void Assets.DustinHorne.JsonDotNetUnity.TestCases.TestCaseUtils::.cctor()
extern "C"  void TestCaseUtils__cctor_m3379786443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase Assets.DustinHorne.JsonDotNetUnity.TestCases.TestCaseUtils::GetSampleBase()
extern "C"  SampleBase_t2925764113 * TestCaseUtils_GetSampleBase_m1894883999 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleChild Assets.DustinHorne.JsonDotNetUnity.TestCases.TestCaseUtils::GetSampleChid()
extern "C"  SampleChild_t2736175310 * TestCaseUtils_GetSampleChid_m1674068947 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject Assets.DustinHorne.JsonDotNetUnity.TestCases.TestCaseUtils::GetSimpleClassObject()
extern "C"  SimpleClassObject_t1988087395 * TestCaseUtils_GetSimpleClassObject_m2281124519 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
