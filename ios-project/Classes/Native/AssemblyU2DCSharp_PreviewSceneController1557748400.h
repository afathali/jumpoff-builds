﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SA_Label
struct SA_Label_t226960149;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PreviewSceneController
struct  PreviewSceneController_t1557748400  : public MonoBehaviour_t1158329972
{
public:
	// SA_Label PreviewSceneController::title
	SA_Label_t226960149 * ___title_2;

public:
	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(PreviewSceneController_t1557748400, ___title_2)); }
	inline SA_Label_t226960149 * get_title_2() const { return ___title_2; }
	inline SA_Label_t226960149 ** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(SA_Label_t226960149 * value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier(&___title_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
