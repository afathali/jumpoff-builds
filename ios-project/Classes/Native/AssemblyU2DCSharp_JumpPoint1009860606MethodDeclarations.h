﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JumpPoint
struct JumpPoint_t1009860606;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void JumpPoint::.ctor()
extern "C"  void JumpPoint__ctor_m842382689 (JumpPoint_t1009860606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JumpPoint::get_xPos()
extern "C"  float JumpPoint_get_xPos_m1223918236 (JumpPoint_t1009860606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpPoint::set_xPos(System.Single)
extern "C"  void JumpPoint_set_xPos_m2424910905 (JumpPoint_t1009860606 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JumpPoint::get_yPos()
extern "C"  float JumpPoint_get_yPos_m2638384887 (JumpPoint_t1009860606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpPoint::set_yPos(System.Single)
extern "C"  void JumpPoint_set_yPos_m1855712756 (JumpPoint_t1009860606 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JumpPoint::get_rotation()
extern "C"  float JumpPoint_get_rotation_m3492114068 (JumpPoint_t1009860606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpPoint::set_rotation(System.Single)
extern "C"  void JumpPoint_set_rotation_m3229471221 (JumpPoint_t1009860606 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JumpPoint::get_jumpNumber()
extern "C"  int32_t JumpPoint_get_jumpNumber_m781662051 (JumpPoint_t1009860606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpPoint::set_jumpNumber(System.Int32)
extern "C"  void JumpPoint_set_jumpNumber_m2641195424 (JumpPoint_t1009860606 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JumpPoint::get_jumpType()
extern "C"  int32_t JumpPoint_get_jumpType_m4154701054 (JumpPoint_t1009860606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpPoint::set_jumpType(System.Int32)
extern "C"  void JumpPoint_set_jumpType_m3422297987 (JumpPoint_t1009860606 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JumpPoint::get_tempNumberText()
extern "C"  String_t* JumpPoint_get_tempNumberText_m4248595467 (JumpPoint_t1009860606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpPoint::set_tempNumberText(System.String)
extern "C"  void JumpPoint_set_tempNumberText_m612648894 (JumpPoint_t1009860606 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
