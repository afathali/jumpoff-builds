﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3980293213MethodDeclarations.h"

// System.Void SA_Singleton_OLD`1<GP_AppInvitesController>::.ctor()
#define SA_Singleton_OLD_1__ctor_m2324220973(__this, method) ((  void (*) (SA_Singleton_OLD_1_t3715273415 *, const MethodInfo*))SA_Singleton_OLD_1__ctor_m2643683696_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<GP_AppInvitesController>::.cctor()
#define SA_Singleton_OLD_1__cctor_m3546954968(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1__cctor_m1355192463_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<GP_AppInvitesController>::get_instance()
#define SA_Singleton_OLD_1_get_instance_m3066090764(__this /* static, unused */, method) ((  GP_AppInvitesController_t2424429497 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_instance_m1963118739_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<GP_AppInvitesController>::get_Instance()
#define SA_Singleton_OLD_1_get_Instance_m3839271340(__this /* static, unused */, method) ((  GP_AppInvitesController_t2424429497 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_Instance_m2967800051_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<GP_AppInvitesController>::get_HasInstance()
#define SA_Singleton_OLD_1_get_HasInstance_m1522769099(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_HasInstance_m4184706798_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<GP_AppInvitesController>::get_IsDestroyed()
#define SA_Singleton_OLD_1_get_IsDestroyed_m1694165665(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_IsDestroyed_m1648274602_gshared)(__this /* static, unused */, method)
// System.Void SA_Singleton_OLD`1<GP_AppInvitesController>::OnDestroy()
#define SA_Singleton_OLD_1_OnDestroy_m545447260(__this, method) ((  void (*) (SA_Singleton_OLD_1_t3715273415 *, const MethodInfo*))SA_Singleton_OLD_1_OnDestroy_m1680414419_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<GP_AppInvitesController>::OnApplicationQuit()
#define SA_Singleton_OLD_1_OnApplicationQuit_m1099737183(__this, method) ((  void (*) (SA_Singleton_OLD_1_t3715273415 *, const MethodInfo*))SA_Singleton_OLD_1_OnApplicationQuit_m1864960902_gshared)(__this, method)
