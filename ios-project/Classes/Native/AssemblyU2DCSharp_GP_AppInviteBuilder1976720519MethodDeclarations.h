﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_AppInviteBuilder
struct GP_AppInviteBuilder_t1976720519;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_AppInviteBuilder::.ctor(System.String)
extern "C"  void GP_AppInviteBuilder__ctor_m1012180098 (GP_AppInviteBuilder_t1976720519 * __this, String_t* ___title0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInviteBuilder::SetMessage(System.String)
extern "C"  void GP_AppInviteBuilder_SetMessage_m2224435809 (GP_AppInviteBuilder_t1976720519 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInviteBuilder::SetDeepLink(System.String)
extern "C"  void GP_AppInviteBuilder_SetDeepLink_m855378736 (GP_AppInviteBuilder_t1976720519 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInviteBuilder::SetCallToActionText(System.String)
extern "C"  void GP_AppInviteBuilder_SetCallToActionText_m3576020550 (GP_AppInviteBuilder_t1976720519 * __this, String_t* ___actionText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInviteBuilder::SetGoogleAnalyticsTrackingId(System.String)
extern "C"  void GP_AppInviteBuilder_SetGoogleAnalyticsTrackingId_m2261263891 (GP_AppInviteBuilder_t1976720519 * __this, String_t* ___trackingId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInviteBuilder::SetAndroidMinimumVersionCode(System.Int32)
extern "C"  void GP_AppInviteBuilder_SetAndroidMinimumVersionCode_m964576787 (GP_AppInviteBuilder_t1976720519 * __this, int32_t ___versionCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInviteBuilder::SetAdditionalReferralParameters(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void GP_AppInviteBuilder_SetAdditionalReferralParameters_m2229578965 (GP_AppInviteBuilder_t1976720519 * __this, Dictionary_2_t3943999495 * ___referralParameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GP_AppInviteBuilder::get_Id()
extern "C"  int32_t GP_AppInviteBuilder_get_Id_m3370829632 (GP_AppInviteBuilder_t1976720519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
