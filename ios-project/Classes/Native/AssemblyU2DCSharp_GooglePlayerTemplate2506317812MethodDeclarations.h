﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayerTemplate
struct GooglePlayerTemplate_t2506317812;
// System.String
struct String_t;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void GooglePlayerTemplate::.ctor(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void GooglePlayerTemplate__ctor_m1356420145 (GooglePlayerTemplate_t2506317812 * __this, String_t* ___pId0, String_t* ___pName1, String_t* ___iconUrl2, String_t* ___imageUrl3, String_t* ___pHasIconImage4, String_t* ___pHasHiResImage5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayerTemplate::add_BigPhotoLoaded(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void GooglePlayerTemplate_add_BigPhotoLoaded_m3009312106 (GooglePlayerTemplate_t2506317812 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayerTemplate::remove_BigPhotoLoaded(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void GooglePlayerTemplate_remove_BigPhotoLoaded_m1844204763 (GooglePlayerTemplate_t2506317812 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayerTemplate::add_SmallPhotoLoaded(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void GooglePlayerTemplate_add_SmallPhotoLoaded_m1807229991 (GooglePlayerTemplate_t2506317812 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayerTemplate::remove_SmallPhotoLoaded(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void GooglePlayerTemplate_remove_SmallPhotoLoaded_m2441879456 (GooglePlayerTemplate_t2506317812 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayerTemplate::LoadImage()
extern "C"  void GooglePlayerTemplate_LoadImage_m2984479034 (GooglePlayerTemplate_t2506317812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayerTemplate::LoadIcon()
extern "C"  void GooglePlayerTemplate_LoadIcon_m1705563658 (GooglePlayerTemplate_t2506317812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayerTemplate::get_playerId()
extern "C"  String_t* GooglePlayerTemplate_get_playerId_m3596248039 (GooglePlayerTemplate_t2506317812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayerTemplate::get_name()
extern "C"  String_t* GooglePlayerTemplate_get_name_m2315233820 (GooglePlayerTemplate_t2506317812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayerTemplate::get_hasIconImage()
extern "C"  bool GooglePlayerTemplate_get_hasIconImage_m2405763070 (GooglePlayerTemplate_t2506317812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayerTemplate::get_hasHiResImage()
extern "C"  bool GooglePlayerTemplate_get_hasHiResImage_m3189902478 (GooglePlayerTemplate_t2506317812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayerTemplate::get_iconImageUrl()
extern "C"  String_t* GooglePlayerTemplate_get_iconImageUrl_m2524220692 (GooglePlayerTemplate_t2506317812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayerTemplate::get_hiResImageUrl()
extern "C"  String_t* GooglePlayerTemplate_get_hiResImageUrl_m860475898 (GooglePlayerTemplate_t2506317812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GooglePlayerTemplate::get_icon()
extern "C"  Texture2D_t3542995729 * GooglePlayerTemplate_get_icon_m4011738138 (GooglePlayerTemplate_t2506317812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GooglePlayerTemplate::get_image()
extern "C"  Texture2D_t3542995729 * GooglePlayerTemplate_get_image_m4037563710 (GooglePlayerTemplate_t2506317812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayerTemplate::OnProfileImageLoaded(UnityEngine.Texture2D)
extern "C"  void GooglePlayerTemplate_OnProfileImageLoaded_m4008838569 (GooglePlayerTemplate_t2506317812 * __this, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayerTemplate::OnProfileIconLoaded(UnityEngine.Texture2D)
extern "C"  void GooglePlayerTemplate_OnProfileIconLoaded_m4074426757 (GooglePlayerTemplate_t2506317812 * __this, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayerTemplate::<BigPhotoLoaded>m__B2(UnityEngine.Texture2D)
extern "C"  void GooglePlayerTemplate_U3CBigPhotoLoadedU3Em__B2_m2073949943 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayerTemplate::<SmallPhotoLoaded>m__B3(UnityEngine.Texture2D)
extern "C"  void GooglePlayerTemplate_U3CSmallPhotoLoadedU3Em__B3_m3548930341 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
