﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ForgotPasswordResponse
struct ForgotPasswordResponse_t4062332037;
// System.String
struct String_t;
// ForgotPasswordRequest
struct ForgotPasswordRequest_t1886688323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ForgotPasswordRequest1886688323.h"

// System.Void ForgotPasswordResponse::.ctor()
extern "C"  void ForgotPasswordResponse__ctor_m1785243256 (ForgotPasswordResponse_t4062332037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ForgotPasswordResponse::get_Success()
extern "C"  bool ForgotPasswordResponse_get_Success_m3513523722 (ForgotPasswordResponse_t4062332037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ForgotPasswordResponse::set_Success(System.Boolean)
extern "C"  void ForgotPasswordResponse_set_Success_m1772748313 (ForgotPasswordResponse_t4062332037 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ForgotPasswordResponse::get_ErrorMessage()
extern "C"  String_t* ForgotPasswordResponse_get_ErrorMessage_m2251962595 (ForgotPasswordResponse_t4062332037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ForgotPasswordResponse::set_ErrorMessage(System.String)
extern "C"  void ForgotPasswordResponse_set_ErrorMessage_m196877548 (ForgotPasswordResponse_t4062332037 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ForgotPasswordRequest ForgotPasswordResponse::get_Request()
extern "C"  ForgotPasswordRequest_t1886688323 * ForgotPasswordResponse_get_Request_m3396782606 (ForgotPasswordResponse_t4062332037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ForgotPasswordResponse::set_Request(ForgotPasswordRequest)
extern "C"  void ForgotPasswordResponse_set_Request_m757590925 (ForgotPasswordResponse_t4062332037 * __this, ForgotPasswordRequest_t1886688323 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
