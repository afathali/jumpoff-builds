﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si3172014400MethodDeclarations.h"

// System.Void SA.Common.Pattern.Singleton`1<IOSCamera>::.ctor()
#define Singleton_1__ctor_m2623668410(__this, method) ((  void (*) (Singleton_1_t3327673795 *, const MethodInfo*))Singleton_1__ctor_m4152044218_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<IOSCamera>::.cctor()
#define Singleton_1__cctor_m3760625719(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1__cctor_m982417053_gshared)(__this /* static, unused */, method)
// T SA.Common.Pattern.Singleton`1<IOSCamera>::get_Instance()
#define Singleton_1_get_Instance_m3514657807(__this /* static, unused */, method) ((  IOSCamera_t2845108690 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m3228489301_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<IOSCamera>::get_HasInstance()
#define Singleton_1_get_HasInstance_m2044499284(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_HasInstance_m2551508260_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<IOSCamera>::get_IsDestroyed()
#define Singleton_1_get_IsDestroyed_m1188857532(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_IsDestroyed_m4170993228_gshared)(__this /* static, unused */, method)
// System.Void SA.Common.Pattern.Singleton`1<IOSCamera>::OnDestroy()
#define Singleton_1_OnDestroy_m2436997643(__this, method) ((  void (*) (Singleton_1_t3327673795 *, const MethodInfo*))Singleton_1_OnDestroy_m3790554761_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<IOSCamera>::OnApplicationQuit()
#define Singleton_1_OnApplicationQuit_m1172541220(__this, method) ((  void (*) (Singleton_1_t3327673795 *, const MethodInfo*))Singleton_1_OnApplicationQuit_m1956476828_gshared)(__this, method)
