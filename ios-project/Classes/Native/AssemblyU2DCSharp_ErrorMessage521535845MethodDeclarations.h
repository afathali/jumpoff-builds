﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ErrorMessage
struct ErrorMessage_t521535845;
// ShowErrorEventArgs
struct ShowErrorEventArgs_t2135289798;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ShowErrorEventArgs2135289798.h"

// System.Void ErrorMessage::.ctor()
extern "C"  void ErrorMessage__ctor_m563190128 (ErrorMessage_t521535845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessage::Start()
extern "C"  void ErrorMessage_Start_m1713727376 (ErrorMessage_t521535845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessage::AwakeOverride()
extern "C"  void ErrorMessage_AwakeOverride_m1652336665 (ErrorMessage_t521535845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessage::DestroyOverride()
extern "C"  void ErrorMessage_DestroyOverride_m288035638 (ErrorMessage_t521535845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessage::HandleShowErrorEventArgs(ShowErrorEventArgs)
extern "C"  void ErrorMessage_HandleShowErrorEventArgs_m356313344 (ErrorMessage_t521535845 * __this, ShowErrorEventArgs_t2135289798 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessage::<Start>m__134()
extern "C"  void ErrorMessage_U3CStartU3Em__134_m1569327181 (ErrorMessage_t521535845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
