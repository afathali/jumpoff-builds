﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultContractResolver/<CreateShouldSerializeTest>c__AnonStorey22
struct U3CCreateShouldSerializeTestU3Ec__AnonStorey22_t1797502741;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<CreateShouldSerializeTest>c__AnonStorey22::.ctor()
extern "C"  void U3CCreateShouldSerializeTestU3Ec__AnonStorey22__ctor_m957732108 (U3CCreateShouldSerializeTestU3Ec__AnonStorey22_t1797502741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<CreateShouldSerializeTest>c__AnonStorey22::<>m__DE(System.Object)
extern "C"  bool U3CCreateShouldSerializeTestU3Ec__AnonStorey22_U3CU3Em__DE_m1364613062 (U3CCreateShouldSerializeTestU3Ec__AnonStorey22_t1797502741 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
