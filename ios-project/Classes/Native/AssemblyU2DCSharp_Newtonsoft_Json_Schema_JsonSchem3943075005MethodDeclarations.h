﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaBuilder/<MapType>c__AnonStorey1D
struct U3CMapTypeU3Ec__AnonStorey1D_t3943075005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21414869661.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder/<MapType>c__AnonStorey1D::.ctor()
extern "C"  void U3CMapTypeU3Ec__AnonStorey1D__ctor_m3654246904 (U3CMapTypeU3Ec__AnonStorey1D_t3943075005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaBuilder/<MapType>c__AnonStorey1D::<>m__D1(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>)
extern "C"  bool U3CMapTypeU3Ec__AnonStorey1D_U3CU3Em__D1_m2653867666 (U3CMapTypeU3Ec__AnonStorey1D_t3943075005 * __this, KeyValuePair_2_t1414869661  ___kv0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
