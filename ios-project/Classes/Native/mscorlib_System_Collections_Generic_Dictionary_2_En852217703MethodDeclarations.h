﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1764544420(__this, ___dictionary0, method) ((  void (*) (Enumerator_t852217703 *, Dictionary_2_t3827160297 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m141642465(__this, method) ((  Il2CppObject * (*) (Enumerator_t852217703 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3881405321(__this, method) ((  void (*) (Enumerator_t852217703 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4182980422(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t852217703 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4239525215(__this, method) ((  Il2CppObject * (*) (Enumerator_t852217703 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2685896279(__this, method) ((  Il2CppObject * (*) (Enumerator_t852217703 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::MoveNext()
#define Enumerator_MoveNext_m4159283545(__this, method) ((  bool (*) (Enumerator_t852217703 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::get_Current()
#define Enumerator_get_Current_m2523575813(__this, method) ((  KeyValuePair_2_t1584505519  (*) (Enumerator_t852217703 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1400190836(__this, method) ((  String_t* (*) (Enumerator_t852217703 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m138160564(__this, method) ((  VariableListed_t1912381035 * (*) (Enumerator_t852217703 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::Reset()
#define Enumerator_Reset_m3087898398(__this, method) ((  void (*) (Enumerator_t852217703 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::VerifyState()
#define Enumerator_VerifyState_m1262861511(__this, method) ((  void (*) (Enumerator_t852217703 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m709481689(__this, method) ((  void (*) (Enumerator_t852217703 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SA.IOSDeploy.VariableListed>::Dispose()
#define Enumerator_Dispose_m1553489060(__this, method) ((  void (*) (Enumerator_t852217703 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
