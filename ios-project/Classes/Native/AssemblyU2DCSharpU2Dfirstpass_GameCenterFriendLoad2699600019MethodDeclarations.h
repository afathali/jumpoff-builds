﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameCenterFriendLoadExample
struct GameCenterFriendLoadExample_t2699600019;
// SA.Common.Models.Result
struct Result_t4287219743;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"

// System.Void GameCenterFriendLoadExample::.ctor()
extern "C"  void GameCenterFriendLoadExample__ctor_m997452516 (GameCenterFriendLoadExample_t2699600019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterFriendLoadExample::Awake()
extern "C"  void GameCenterFriendLoadExample_Awake_m914800741 (GameCenterFriendLoadExample_t2699600019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterFriendLoadExample::OnGUI()
extern "C"  void GameCenterFriendLoadExample_OnGUI_m3578904244 (GameCenterFriendLoadExample_t2699600019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterFriendLoadExample::HandleOnAuthFinished(SA.Common.Models.Result)
extern "C"  void GameCenterFriendLoadExample_HandleOnAuthFinished_m1279310797 (GameCenterFriendLoadExample_t2699600019 * __this, Result_t4287219743 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterFriendLoadExample::OnFriendsListLoaded(SA.Common.Models.Result)
extern "C"  void GameCenterFriendLoadExample_OnFriendsListLoaded_m2415154217 (GameCenterFriendLoadExample_t2699600019 * __this, Result_t4287219743 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
