﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Course
struct Course_t3483112699;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// System.Collections.Generic.List`1<JumpPoint>
struct List_1_t378981738;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Course::.ctor()
extern "C"  void Course__ctor_m682882968 (Course_t3483112699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Course::get_isNew()
extern "C"  bool Course_get_isNew_m996316857 (Course_t3483112699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Course::get_Id()
extern "C"  int64_t Course_get_Id_m3176614495 (Course_t3483112699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Course::set_Id(System.Int64)
extern "C"  void Course_set_Id_m489525674 (Course_t3483112699 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Course::get_Image()
extern "C"  ByteU5BU5D_t3397334013* Course_get_Image_m569066106 (Course_t3483112699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Course::set_Image(System.Byte[])
extern "C"  void Course_set_Image_m1835280143 (Course_t3483112699 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Course::get_Name()
extern "C"  String_t* Course_get_Name_m2575603829 (Course_t3483112699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Course::set_Name(System.String)
extern "C"  void Course_set_Name_m3730945210 (Course_t3483112699 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Course::get_Show()
extern "C"  String_t* Course_get_Show_m3478833145 (Course_t3483112699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Course::set_Show(System.String)
extern "C"  void Course_set_Show_m4197491284 (Course_t3483112699 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Course::get_Date()
extern "C"  String_t* Course_get_Date_m2575614882 (Course_t3483112699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Course::set_Date(System.String)
extern "C"  void Course_set_Date_m2114143117 (Course_t3483112699 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Course::get_Notes()
extern "C"  String_t* Course_get_Notes_m2944493897 (Course_t3483112699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Course::set_Notes(System.String)
extern "C"  void Course_set_Notes_m2603581820 (Course_t3483112699 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JumpPoint> Course::get_Jumps()
extern "C"  List_1_t378981738 * Course_get_Jumps_m2512894141 (Course_t3483112699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Course::set_Jumps(System.Collections.Generic.List`1<JumpPoint>)
extern "C"  void Course_set_Jumps_m459551120 (Course_t3483112699 * __this, List_1_t378981738 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Course::get_Scale()
extern "C"  String_t* Course_get_Scale_m1408300350 (Course_t3483112699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Course::set_Scale(System.String)
extern "C"  void Course_set_Scale_m2430698401 (Course_t3483112699 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
