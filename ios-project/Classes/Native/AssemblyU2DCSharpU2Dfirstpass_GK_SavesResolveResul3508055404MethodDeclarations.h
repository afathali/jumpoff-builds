﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_SavesResolveResult
struct GK_SavesResolveResult_t3508055404;
// System.Collections.Generic.List`1<GK_SavedGame>
struct List_1_t2689214752;
// SA.Common.Models.Error
struct Error_t445207774;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GK_SavesResolveResult::.ctor(System.Collections.Generic.List`1<GK_SavedGame>)
extern "C"  void GK_SavesResolveResult__ctor_m3274261387 (GK_SavesResolveResult_t3508055404 * __this, List_1_t2689214752 * ___saves0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_SavesResolveResult::.ctor(SA.Common.Models.Error)
extern "C"  void GK_SavesResolveResult__ctor_m1463221778 (GK_SavesResolveResult_t3508055404 * __this, Error_t445207774 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_SavesResolveResult::.ctor(System.String)
extern "C"  void GK_SavesResolveResult__ctor_m891752371 (GK_SavesResolveResult_t3508055404 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GK_SavedGame> GK_SavesResolveResult::get_SavedGames()
extern "C"  List_1_t2689214752 * GK_SavesResolveResult_get_SavedGames_m3434201641 (GK_SavesResolveResult_t3508055404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
