﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenScaler
struct ScreenScaler_t358654512;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenScaler::.ctor()
extern "C"  void ScreenScaler__ctor_m1770846097 (ScreenScaler_t358654512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenScaler::Awake()
extern "C"  void ScreenScaler_Awake_m3658808412 (ScreenScaler_t358654512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenScaler::Update()
extern "C"  void ScreenScaler_Update_m4012890906 (ScreenScaler_t358654512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenScaler::placementCalculation()
extern "C"  void ScreenScaler_placementCalculation_m214012697 (ScreenScaler_t358654512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
