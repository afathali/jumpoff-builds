﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoogleCloudResult
struct GoogleCloudResult_t743628707;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_AppStateStatusCodes2689651680.h"

// System.Void GoogleCloudResult::.ctor(System.String)
extern "C"  void GoogleCloudResult__ctor_m2750223022 (GoogleCloudResult_t743628707 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudResult::.ctor(System.String,System.String)
extern "C"  void GoogleCloudResult__ctor_m1499560154 (GoogleCloudResult_t743628707 * __this, String_t* ___code0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_AppStateStatusCodes GoogleCloudResult::get_response()
extern "C"  int32_t GoogleCloudResult_get_response_m2404867991 (GoogleCloudResult_t743628707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleCloudResult::get_stateDataString()
extern "C"  String_t* GoogleCloudResult_get_stateDataString_m1029063346 (GoogleCloudResult_t743628707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleCloudResult::get_serverConflictDataString()
extern "C"  String_t* GoogleCloudResult_get_serverConflictDataString_m2359844248 (GoogleCloudResult_t743628707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleCloudResult::get_message()
extern "C"  String_t* GoogleCloudResult_get_message_m2029896849 (GoogleCloudResult_t743628707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GoogleCloudResult::get_stateKey()
extern "C"  int32_t GoogleCloudResult_get_stateKey_m2844819229 (GoogleCloudResult_t743628707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoogleCloudResult::get_isSuccess()
extern "C"  bool GoogleCloudResult_get_isSuccess_m3329876570 (GoogleCloudResult_t743628707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoogleCloudResult::get_isFailure()
extern "C"  bool GoogleCloudResult_get_isFailure_m1259560469 (GoogleCloudResult_t743628707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
