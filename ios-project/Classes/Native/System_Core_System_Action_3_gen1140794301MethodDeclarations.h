﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen905177775MethodDeclarations.h"

// System.Void System.Action`3<GK_Player,GK_PlayerConnectionState,GK_RTM_Match>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m2049944312(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t1140794301 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m969386966_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<GK_Player,GK_PlayerConnectionState,GK_RTM_Match>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m3591755478(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t1140794301 *, GK_Player_t2782008294 *, int32_t, GK_RTM_Match_t873568990 *, const MethodInfo*))Action_3_Invoke_m4203154120_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<GK_Player,GK_PlayerConnectionState,GK_RTM_Match>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m3705297453(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t1140794301 *, GK_Player_t2782008294 *, int32_t, GK_RTM_Match_t873568990 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m937272357_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<GK_Player,GK_PlayerConnectionState,GK_RTM_Match>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m3913546466(__this, ___result0, method) ((  void (*) (Action_3_t1140794301 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m3896340280_gshared)(__this, ___result0, method)
