﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si3172014400MethodDeclarations.h"

// System.Void SA.Common.Pattern.Singleton`1<SA_EditorAd>::.ctor()
#define Singleton_1__ctor_m445423581(__this, method) ((  void (*) (Singleton_1_t1892724392 *, const MethodInfo*))Singleton_1__ctor_m4152044218_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<SA_EditorAd>::.cctor()
#define Singleton_1__cctor_m3507495708(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1__cctor_m982417053_gshared)(__this /* static, unused */, method)
// T SA.Common.Pattern.Singleton`1<SA_EditorAd>::get_Instance()
#define Singleton_1_get_Instance_m74131242(__this /* static, unused */, method) ((  SA_EditorAd_t1410159287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m3228489301_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<SA_EditorAd>::get_HasInstance()
#define Singleton_1_get_HasInstance_m3278625563(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_HasInstance_m2551508260_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<SA_EditorAd>::get_IsDestroyed()
#define Singleton_1_get_IsDestroyed_m1260202765(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_IsDestroyed_m4170993228_gshared)(__this /* static, unused */, method)
// System.Void SA.Common.Pattern.Singleton`1<SA_EditorAd>::OnDestroy()
#define Singleton_1_OnDestroy_m3245014128(__this, method) ((  void (*) (Singleton_1_t1892724392 *, const MethodInfo*))Singleton_1_OnDestroy_m3790554761_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<SA_EditorAd>::OnApplicationQuit()
#define Singleton_1_OnApplicationQuit_m3231655111(__this, method) ((  void (*) (Singleton_1_t1892724392 *, const MethodInfo*))Singleton_1_OnApplicationQuit_m1956476828_gshared)(__this, method)
