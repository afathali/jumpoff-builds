﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Animation.SA_iTween/<Start>c__Iterator9
struct U3CStartU3Ec__Iterator9_t44404109;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.Common.Animation.SA_iTween/<Start>c__Iterator9::.ctor()
extern "C"  void U3CStartU3Ec__Iterator9__ctor_m3183901768 (U3CStartU3Ec__Iterator9_t44404109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Animation.SA_iTween/<Start>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1770638814 (U3CStartU3Ec__Iterator9_t44404109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Animation.SA_iTween/<Start>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m767818374 (U3CStartU3Ec__Iterator9_t44404109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA.Common.Animation.SA_iTween/<Start>c__Iterator9::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator9_MoveNext_m2469416804 (U3CStartU3Ec__Iterator9_t44404109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween/<Start>c__Iterator9::Dispose()
extern "C"  void U3CStartU3Ec__Iterator9_Dispose_m1488057069 (U3CStartU3Ec__Iterator9_t44404109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween/<Start>c__Iterator9::Reset()
extern "C"  void U3CStartU3Ec__Iterator9_Reset_m2141360779 (U3CStartU3Ec__Iterator9_t44404109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
