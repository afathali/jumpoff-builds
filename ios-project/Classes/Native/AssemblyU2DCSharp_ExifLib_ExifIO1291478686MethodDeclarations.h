﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.Int16 ExifLib.ExifIO::ReadShort(System.Byte[],System.Int32,System.Boolean)
extern "C"  int16_t ExifIO_ReadShort_m3957960797 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___Data0, int32_t ___offset1, bool ___littleEndian2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLib.ExifIO::ReadUShort(System.Byte[],System.Int32,System.Boolean)
extern "C"  uint16_t ExifIO_ReadUShort_m152549933 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___Data0, int32_t ___offset1, bool ___littleEndian2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLib.ExifIO::ReadInt(System.Byte[],System.Int32,System.Boolean)
extern "C"  int32_t ExifIO_ReadInt_m1924010170 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___Data0, int32_t ___offset1, bool ___littleEndian2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ExifLib.ExifIO::ReadUInt(System.Byte[],System.Int32,System.Boolean)
extern "C"  uint32_t ExifIO_ReadUInt_m4250225746 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___Data0, int32_t ___offset1, bool ___littleEndian2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExifLib.ExifIO::ReadSingle(System.Byte[],System.Int32,System.Boolean)
extern "C"  float ExifIO_ReadSingle_m1130048423 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___Data0, int32_t ___offset1, bool ___littleEndian2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double ExifLib.ExifIO::ReadDouble(System.Byte[],System.Int32,System.Boolean)
extern "C"  double ExifIO_ReadDouble_m1759790579 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___Data0, int32_t ___offset1, bool ___littleEndian2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
