﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.StringUtils/<NumberLines>c__AnonStorey38
struct U3CNumberLinesU3Ec__AnonStorey38_t1771230768;
// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_TextWriter4027217640.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Newtonsoft.Json.Utilities.StringUtils/<NumberLines>c__AnonStorey38::.ctor()
extern "C"  void U3CNumberLinesU3Ec__AnonStorey38__ctor_m2505233657 (U3CNumberLinesU3Ec__AnonStorey38_t1771230768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.StringUtils/<NumberLines>c__AnonStorey38::<>m__105(System.IO.TextWriter,System.String)
extern "C"  void U3CNumberLinesU3Ec__AnonStorey38_U3CU3Em__105_m57072429 (U3CNumberLinesU3Ec__AnonStorey38_t1771230768 * __this, TextWriter_t4027217640 * ___tw0, String_t* ___line1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
