﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_TBM_Match
struct GP_TBM_Match_t1275077981;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_TBM_Match::.ctor()
extern "C"  void GP_TBM_Match__ctor_m2790862326 (GP_TBM_Match_t1275077981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_TBM_Match::SetData(System.String)
extern "C"  void GP_TBM_Match_SetData_m260924140 (GP_TBM_Match_t1275077981 * __this, String_t* ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_TBM_Match::SetPreviousMatchData(System.String)
extern "C"  void GP_TBM_Match_SetPreviousMatchData_m3805612192 (GP_TBM_Match_t1275077981 * __this, String_t* ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
