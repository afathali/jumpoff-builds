﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14
struct U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14::.ctor()
extern "C"  void U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14__ctor_m884550203 (U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m928935285 (U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m114639357 (U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14::MoveNext()
extern "C"  bool U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_MoveNext_m2091432721 (U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14::Dispose()
extern "C"  void U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_Dispose_m2050577308 (U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenCapture/<SaveScreenshot_ReadPixelsAsynch>c__Iterator14::Reset()
extern "C"  void U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_Reset_m1716003842 (U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
