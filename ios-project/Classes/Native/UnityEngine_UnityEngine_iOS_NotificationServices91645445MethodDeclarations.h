﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.iOS.LocalNotification
struct LocalNotification_t317971878;
// UnityEngine.iOS.LocalNotification[]
struct LocalNotificationU5BU5D_t620603651;

#include "codegen/il2cpp-codegen.h"

// System.Int32 UnityEngine.iOS.NotificationServices::get_localNotificationCount()
extern "C"  int32_t NotificationServices_get_localNotificationCount_m3722631625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.iOS.LocalNotification UnityEngine.iOS.NotificationServices::GetLocalNotification(System.Int32)
extern "C"  LocalNotification_t317971878 * NotificationServices_GetLocalNotification_m816601345 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.iOS.LocalNotification[] UnityEngine.iOS.NotificationServices::get_localNotifications()
extern "C"  LocalNotificationU5BU5D_t620603651* NotificationServices_get_localNotifications_m779039864 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
