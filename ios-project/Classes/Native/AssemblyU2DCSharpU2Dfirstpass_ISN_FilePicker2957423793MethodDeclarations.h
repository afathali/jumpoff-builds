﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_FilePicker
struct ISN_FilePicker_t2957423793;
// System.Action`1<ISN_FilePickerResult>
struct Action_1_t2036603368;
// System.String
struct String_t;
// ISN_FilePickerResult
struct ISN_FilePickerResult_t2234803986;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_FilePickerResult2234803986.h"

// System.Void ISN_FilePicker::.ctor()
extern "C"  void ISN_FilePicker__ctor_m1974956560 (ISN_FilePicker_t2957423793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_FilePicker::.cctor()
extern "C"  void ISN_FilePicker__cctor_m3675870029 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_FilePicker::add_MediaPickFinished(System.Action`1<ISN_FilePickerResult>)
extern "C"  void ISN_FilePicker_add_MediaPickFinished_m2073915467 (Il2CppObject * __this /* static, unused */, Action_1_t2036603368 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_FilePicker::remove_MediaPickFinished(System.Action`1<ISN_FilePickerResult>)
extern "C"  void ISN_FilePicker_remove_MediaPickFinished_m434232512 (Il2CppObject * __this /* static, unused */, Action_1_t2036603368 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_FilePicker::Awake()
extern "C"  void ISN_FilePicker_Awake_m2604034507 (ISN_FilePicker_t2957423793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_FilePicker::PickFromCameraRoll(System.Int32)
extern "C"  void ISN_FilePicker_PickFromCameraRoll_m988107914 (ISN_FilePicker_t2957423793 * __this, int32_t ___maxItemsCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_FilePicker::OnSelectImagesComplete(System.String)
extern "C"  void ISN_FilePicker_OnSelectImagesComplete_m2694802660 (ISN_FilePicker_t2957423793 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_FilePicker::<MediaPickFinished>m__31(ISN_FilePickerResult)
extern "C"  void ISN_FilePicker_U3CMediaPickFinishedU3Em__31_m2672655652 (Il2CppObject * __this /* static, unused */, ISN_FilePickerResult_t2234803986 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
