﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_LeaderboardResult
struct GP_LeaderboardResult_t2034215294;
// GPLeaderBoard
struct GPLeaderBoard_t3649577886;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GPLeaderBoard3649577886.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_LeaderboardResult::.ctor(GPLeaderBoard,System.String)
extern "C"  void GP_LeaderboardResult__ctor_m4118231467 (GP_LeaderboardResult_t2034215294 * __this, GPLeaderBoard_t3649577886 * ___leaderboard0, String_t* ___code1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_LeaderboardResult::SetInfo(GPLeaderBoard)
extern "C"  void GP_LeaderboardResult_SetInfo_m2359851521 (GP_LeaderboardResult_t2034215294 * __this, GPLeaderBoard_t3649577886 * ___leaderboard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPLeaderBoard GP_LeaderboardResult::get_Leaderboard()
extern "C"  GPLeaderBoard_t3649577886 * GP_LeaderboardResult_get_Leaderboard_m4086244952 (GP_LeaderboardResult_t2034215294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
