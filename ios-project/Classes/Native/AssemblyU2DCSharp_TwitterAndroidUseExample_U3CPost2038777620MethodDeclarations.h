﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TwitterAndroidUseExample/<PostTWScreenshot>c__Iterator6
struct U3CPostTWScreenshotU3Ec__Iterator6_t2038777620;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TwitterAndroidUseExample/<PostTWScreenshot>c__Iterator6::.ctor()
extern "C"  void U3CPostTWScreenshotU3Ec__Iterator6__ctor_m1761467423 (U3CPostTWScreenshotU3Ec__Iterator6_t2038777620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TwitterAndroidUseExample/<PostTWScreenshot>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPostTWScreenshotU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1241722213 (U3CPostTWScreenshotU3Ec__Iterator6_t2038777620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TwitterAndroidUseExample/<PostTWScreenshot>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPostTWScreenshotU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m2854894221 (U3CPostTWScreenshotU3Ec__Iterator6_t2038777620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TwitterAndroidUseExample/<PostTWScreenshot>c__Iterator6::MoveNext()
extern "C"  bool U3CPostTWScreenshotU3Ec__Iterator6_MoveNext_m303020869 (U3CPostTWScreenshotU3Ec__Iterator6_t2038777620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample/<PostTWScreenshot>c__Iterator6::Dispose()
extern "C"  void U3CPostTWScreenshotU3Ec__Iterator6_Dispose_m1338254470 (U3CPostTWScreenshotU3Ec__Iterator6_t2038777620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterAndroidUseExample/<PostTWScreenshot>c__Iterator6::Reset()
extern "C"  void U3CPostTWScreenshotU3Ec__Iterator6_Reset_m643864948 (U3CPostTWScreenshotU3Ec__Iterator6_t2038777620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
