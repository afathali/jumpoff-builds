﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SA_Ad_EditorUIController
struct SA_Ad_EditorUIController_t4080214878;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action
struct Action_t3226471752;

#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si1892724392.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA_EditorAd
struct  SA_EditorAd_t1410159287  : public Singleton_1_t1892724392
{
public:
	// System.Boolean SA_EditorAd::_IsInterstitialLoading
	bool ____IsInterstitialLoading_6;
	// System.Boolean SA_EditorAd::_IsVideoLoading
	bool ____IsVideoLoading_7;
	// System.Boolean SA_EditorAd::_IsInterstitialReady
	bool ____IsInterstitialReady_8;
	// System.Boolean SA_EditorAd::_IsVideoReady
	bool ____IsVideoReady_9;
	// System.Int32 SA_EditorAd::_FillRate
	int32_t ____FillRate_10;
	// SA_Ad_EditorUIController SA_EditorAd::_EditorUI
	SA_Ad_EditorUIController_t4080214878 * ____EditorUI_11;

public:
	inline static int32_t get_offset_of__IsInterstitialLoading_6() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287, ____IsInterstitialLoading_6)); }
	inline bool get__IsInterstitialLoading_6() const { return ____IsInterstitialLoading_6; }
	inline bool* get_address_of__IsInterstitialLoading_6() { return &____IsInterstitialLoading_6; }
	inline void set__IsInterstitialLoading_6(bool value)
	{
		____IsInterstitialLoading_6 = value;
	}

	inline static int32_t get_offset_of__IsVideoLoading_7() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287, ____IsVideoLoading_7)); }
	inline bool get__IsVideoLoading_7() const { return ____IsVideoLoading_7; }
	inline bool* get_address_of__IsVideoLoading_7() { return &____IsVideoLoading_7; }
	inline void set__IsVideoLoading_7(bool value)
	{
		____IsVideoLoading_7 = value;
	}

	inline static int32_t get_offset_of__IsInterstitialReady_8() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287, ____IsInterstitialReady_8)); }
	inline bool get__IsInterstitialReady_8() const { return ____IsInterstitialReady_8; }
	inline bool* get_address_of__IsInterstitialReady_8() { return &____IsInterstitialReady_8; }
	inline void set__IsInterstitialReady_8(bool value)
	{
		____IsInterstitialReady_8 = value;
	}

	inline static int32_t get_offset_of__IsVideoReady_9() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287, ____IsVideoReady_9)); }
	inline bool get__IsVideoReady_9() const { return ____IsVideoReady_9; }
	inline bool* get_address_of__IsVideoReady_9() { return &____IsVideoReady_9; }
	inline void set__IsVideoReady_9(bool value)
	{
		____IsVideoReady_9 = value;
	}

	inline static int32_t get_offset_of__FillRate_10() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287, ____FillRate_10)); }
	inline int32_t get__FillRate_10() const { return ____FillRate_10; }
	inline int32_t* get_address_of__FillRate_10() { return &____FillRate_10; }
	inline void set__FillRate_10(int32_t value)
	{
		____FillRate_10 = value;
	}

	inline static int32_t get_offset_of__EditorUI_11() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287, ____EditorUI_11)); }
	inline SA_Ad_EditorUIController_t4080214878 * get__EditorUI_11() const { return ____EditorUI_11; }
	inline SA_Ad_EditorUIController_t4080214878 ** get_address_of__EditorUI_11() { return &____EditorUI_11; }
	inline void set__EditorUI_11(SA_Ad_EditorUIController_t4080214878 * value)
	{
		____EditorUI_11 = value;
		Il2CppCodeGenWriteBarrier(&____EditorUI_11, value);
	}
};

struct SA_EditorAd_t1410159287_StaticFields
{
public:
	// System.Action`1<System.Boolean> SA_EditorAd::OnInterstitialFinished
	Action_1_t3627374100 * ___OnInterstitialFinished_12;
	// System.Action`1<System.Boolean> SA_EditorAd::OnInterstitialLoadComplete
	Action_1_t3627374100 * ___OnInterstitialLoadComplete_13;
	// System.Action SA_EditorAd::OnInterstitialLeftApplication
	Action_t3226471752 * ___OnInterstitialLeftApplication_14;
	// System.Action`1<System.Boolean> SA_EditorAd::OnVideoFinished
	Action_1_t3627374100 * ___OnVideoFinished_15;
	// System.Action`1<System.Boolean> SA_EditorAd::OnVideoLoadComplete
	Action_1_t3627374100 * ___OnVideoLoadComplete_16;
	// System.Action SA_EditorAd::OnVideoLeftApplication
	Action_t3226471752 * ___OnVideoLeftApplication_17;
	// System.Action`1<System.Boolean> SA_EditorAd::<>f__am$cacheC
	Action_1_t3627374100 * ___U3CU3Ef__amU24cacheC_18;
	// System.Action`1<System.Boolean> SA_EditorAd::<>f__am$cacheD
	Action_1_t3627374100 * ___U3CU3Ef__amU24cacheD_19;
	// System.Action SA_EditorAd::<>f__am$cacheE
	Action_t3226471752 * ___U3CU3Ef__amU24cacheE_20;
	// System.Action`1<System.Boolean> SA_EditorAd::<>f__am$cacheF
	Action_1_t3627374100 * ___U3CU3Ef__amU24cacheF_21;
	// System.Action`1<System.Boolean> SA_EditorAd::<>f__am$cache10
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache10_22;
	// System.Action SA_EditorAd::<>f__am$cache11
	Action_t3226471752 * ___U3CU3Ef__amU24cache11_23;

public:
	inline static int32_t get_offset_of_OnInterstitialFinished_12() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287_StaticFields, ___OnInterstitialFinished_12)); }
	inline Action_1_t3627374100 * get_OnInterstitialFinished_12() const { return ___OnInterstitialFinished_12; }
	inline Action_1_t3627374100 ** get_address_of_OnInterstitialFinished_12() { return &___OnInterstitialFinished_12; }
	inline void set_OnInterstitialFinished_12(Action_1_t3627374100 * value)
	{
		___OnInterstitialFinished_12 = value;
		Il2CppCodeGenWriteBarrier(&___OnInterstitialFinished_12, value);
	}

	inline static int32_t get_offset_of_OnInterstitialLoadComplete_13() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287_StaticFields, ___OnInterstitialLoadComplete_13)); }
	inline Action_1_t3627374100 * get_OnInterstitialLoadComplete_13() const { return ___OnInterstitialLoadComplete_13; }
	inline Action_1_t3627374100 ** get_address_of_OnInterstitialLoadComplete_13() { return &___OnInterstitialLoadComplete_13; }
	inline void set_OnInterstitialLoadComplete_13(Action_1_t3627374100 * value)
	{
		___OnInterstitialLoadComplete_13 = value;
		Il2CppCodeGenWriteBarrier(&___OnInterstitialLoadComplete_13, value);
	}

	inline static int32_t get_offset_of_OnInterstitialLeftApplication_14() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287_StaticFields, ___OnInterstitialLeftApplication_14)); }
	inline Action_t3226471752 * get_OnInterstitialLeftApplication_14() const { return ___OnInterstitialLeftApplication_14; }
	inline Action_t3226471752 ** get_address_of_OnInterstitialLeftApplication_14() { return &___OnInterstitialLeftApplication_14; }
	inline void set_OnInterstitialLeftApplication_14(Action_t3226471752 * value)
	{
		___OnInterstitialLeftApplication_14 = value;
		Il2CppCodeGenWriteBarrier(&___OnInterstitialLeftApplication_14, value);
	}

	inline static int32_t get_offset_of_OnVideoFinished_15() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287_StaticFields, ___OnVideoFinished_15)); }
	inline Action_1_t3627374100 * get_OnVideoFinished_15() const { return ___OnVideoFinished_15; }
	inline Action_1_t3627374100 ** get_address_of_OnVideoFinished_15() { return &___OnVideoFinished_15; }
	inline void set_OnVideoFinished_15(Action_1_t3627374100 * value)
	{
		___OnVideoFinished_15 = value;
		Il2CppCodeGenWriteBarrier(&___OnVideoFinished_15, value);
	}

	inline static int32_t get_offset_of_OnVideoLoadComplete_16() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287_StaticFields, ___OnVideoLoadComplete_16)); }
	inline Action_1_t3627374100 * get_OnVideoLoadComplete_16() const { return ___OnVideoLoadComplete_16; }
	inline Action_1_t3627374100 ** get_address_of_OnVideoLoadComplete_16() { return &___OnVideoLoadComplete_16; }
	inline void set_OnVideoLoadComplete_16(Action_1_t3627374100 * value)
	{
		___OnVideoLoadComplete_16 = value;
		Il2CppCodeGenWriteBarrier(&___OnVideoLoadComplete_16, value);
	}

	inline static int32_t get_offset_of_OnVideoLeftApplication_17() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287_StaticFields, ___OnVideoLeftApplication_17)); }
	inline Action_t3226471752 * get_OnVideoLeftApplication_17() const { return ___OnVideoLeftApplication_17; }
	inline Action_t3226471752 ** get_address_of_OnVideoLeftApplication_17() { return &___OnVideoLeftApplication_17; }
	inline void set_OnVideoLeftApplication_17(Action_t3226471752 * value)
	{
		___OnVideoLeftApplication_17 = value;
		Il2CppCodeGenWriteBarrier(&___OnVideoLeftApplication_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_18() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287_StaticFields, ___U3CU3Ef__amU24cacheC_18)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cacheC_18() const { return ___U3CU3Ef__amU24cacheC_18; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cacheC_18() { return &___U3CU3Ef__amU24cacheC_18; }
	inline void set_U3CU3Ef__amU24cacheC_18(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cacheC_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_19() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287_StaticFields, ___U3CU3Ef__amU24cacheD_19)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cacheD_19() const { return ___U3CU3Ef__amU24cacheD_19; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cacheD_19() { return &___U3CU3Ef__amU24cacheD_19; }
	inline void set_U3CU3Ef__amU24cacheD_19(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cacheD_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_20() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287_StaticFields, ___U3CU3Ef__amU24cacheE_20)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cacheE_20() const { return ___U3CU3Ef__amU24cacheE_20; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cacheE_20() { return &___U3CU3Ef__amU24cacheE_20; }
	inline void set_U3CU3Ef__amU24cacheE_20(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cacheE_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_21() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287_StaticFields, ___U3CU3Ef__amU24cacheF_21)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cacheF_21() const { return ___U3CU3Ef__amU24cacheF_21; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cacheF_21() { return &___U3CU3Ef__amU24cacheF_21; }
	inline void set_U3CU3Ef__amU24cacheF_21(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cacheF_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_22() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287_StaticFields, ___U3CU3Ef__amU24cache10_22)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache10_22() const { return ___U3CU3Ef__amU24cache10_22; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache10_22() { return &___U3CU3Ef__amU24cache10_22; }
	inline void set_U3CU3Ef__amU24cache10_22(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache10_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_23() { return static_cast<int32_t>(offsetof(SA_EditorAd_t1410159287_StaticFields, ___U3CU3Ef__amU24cache11_23)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache11_23() const { return ___U3CU3Ef__amU24cache11_23; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache11_23() { return &___U3CU3Ef__amU24cache11_23; }
	inline void set_U3CU3Ef__amU24cache11_23(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache11_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
