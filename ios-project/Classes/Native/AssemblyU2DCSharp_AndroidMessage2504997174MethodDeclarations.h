﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidMessage
struct AndroidMessage_t2504997174;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AndroidMessage::.ctor()
extern "C"  void AndroidMessage__ctor_m1051105343 (AndroidMessage_t2504997174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AndroidMessage AndroidMessage::Create(System.String,System.String)
extern "C"  AndroidMessage_t2504997174 * AndroidMessage_Create_m4206211914 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AndroidMessage AndroidMessage::Create(System.String,System.String,System.String)
extern "C"  AndroidMessage_t2504997174 * AndroidMessage_Create_m4212113022 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___ok2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidMessage::init()
extern "C"  void AndroidMessage_init_m1226998885 (AndroidMessage_t2504997174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidMessage::onPopUpCallBack(System.String)
extern "C"  void AndroidMessage_onPopUpCallBack_m457374999 (AndroidMessage_t2504997174 * __this, String_t* ___buttonIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidMessage::<OnComplete>m__7E()
extern "C"  void AndroidMessage_U3COnCompleteU3Em__7E_m2752872524 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
