﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSInAppPurchaseManager
struct IOSInAppPurchaseManager_t644626385;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<IOSStoreKitResult>
struct Action_1_t2161206965;
// System.Action`1<IOSStoreKitRestoreResult>
struct Action_1_t3107075537;
// System.Action`1<SA.Common.Models.Result>
struct Action_1_t4089019125;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<IOSStoreKitVerificationResponse>
struct Action_1_t4065457964;
// System.String
struct String_t;
// IOSProductTemplate
struct IOSProductTemplate_t1036598382;
// IOSStoreProductView
struct IOSStoreProductView_t607200268;
// System.Collections.Generic.List`1<IOSProductTemplate>
struct List_1_t405719514;
// IOSStoreKitResult
struct IOSStoreKitResult_t2359407583;
// IOSStoreKitRestoreResult
struct IOSStoreKitRestoreResult_t3305276155;
// SA.Common.Models.Result
struct Result_t4287219743;
// IOSStoreKitVerificationResponse
struct IOSStoreKitVerificationResponse_t4263658582;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSProductTemplate1036598382.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreProductView607200268.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSTransactionErrorC1822631322.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreKitResult2359407583.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreKitRestoreRe3305276155.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreKitVerificat4263658582.h"

// System.Void IOSInAppPurchaseManager::.ctor()
extern "C"  void IOSInAppPurchaseManager__ctor_m635842 (IOSInAppPurchaseManager_t644626385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::.cctor()
extern "C"  void IOSInAppPurchaseManager__cctor_m3762916945 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::add_OnRestoreStarted(System.Action)
extern "C"  void IOSInAppPurchaseManager_add_OnRestoreStarted_m3066142013 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::remove_OnRestoreStarted(System.Action)
extern "C"  void IOSInAppPurchaseManager_remove_OnRestoreStarted_m1527639320 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::add_OnTransactionStarted(System.Action`1<System.String>)
extern "C"  void IOSInAppPurchaseManager_add_OnTransactionStarted_m2426388274 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::remove_OnTransactionStarted(System.Action`1<System.String>)
extern "C"  void IOSInAppPurchaseManager_remove_OnTransactionStarted_m1066202319 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::add_OnTransactionComplete(System.Action`1<IOSStoreKitResult>)
extern "C"  void IOSInAppPurchaseManager_add_OnTransactionComplete_m1899978007 (Il2CppObject * __this /* static, unused */, Action_1_t2161206965 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::remove_OnTransactionComplete(System.Action`1<IOSStoreKitResult>)
extern "C"  void IOSInAppPurchaseManager_remove_OnTransactionComplete_m689049458 (Il2CppObject * __this /* static, unused */, Action_1_t2161206965 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::add_OnRestoreComplete(System.Action`1<IOSStoreKitRestoreResult>)
extern "C"  void IOSInAppPurchaseManager_add_OnRestoreComplete_m532231947 (Il2CppObject * __this /* static, unused */, Action_1_t3107075537 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::remove_OnRestoreComplete(System.Action`1<IOSStoreKitRestoreResult>)
extern "C"  void IOSInAppPurchaseManager_remove_OnRestoreComplete_m3703538606 (Il2CppObject * __this /* static, unused */, Action_1_t3107075537 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::add_OnStoreKitInitComplete(System.Action`1<SA.Common.Models.Result>)
extern "C"  void IOSInAppPurchaseManager_add_OnStoreKitInitComplete_m1601642617 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::remove_OnStoreKitInitComplete(System.Action`1<SA.Common.Models.Result>)
extern "C"  void IOSInAppPurchaseManager_remove_OnStoreKitInitComplete_m3554123086 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::add_OnPurchasesStateSettingsLoaded(System.Action`1<System.Boolean>)
extern "C"  void IOSInAppPurchaseManager_add_OnPurchasesStateSettingsLoaded_m1379009145 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::remove_OnPurchasesStateSettingsLoaded(System.Action`1<System.Boolean>)
extern "C"  void IOSInAppPurchaseManager_remove_OnPurchasesStateSettingsLoaded_m1005423338 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::add_OnVerificationComplete(System.Action`1<IOSStoreKitVerificationResponse>)
extern "C"  void IOSInAppPurchaseManager_add_OnVerificationComplete_m2777977105 (Il2CppObject * __this /* static, unused */, Action_1_t4065457964 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::remove_OnVerificationComplete(System.Action`1<IOSStoreKitVerificationResponse>)
extern "C"  void IOSInAppPurchaseManager_remove_OnVerificationComplete_m897502726 (Il2CppObject * __this /* static, unused */, Action_1_t4065457964 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::Awake()
extern "C"  void IOSInAppPurchaseManager_Awake_m1209450043 (IOSInAppPurchaseManager_t644626385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::loadStore()
extern "C"  void IOSInAppPurchaseManager_loadStore_m4262180599 (IOSInAppPurchaseManager_t644626385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::LoadStore(System.Boolean)
extern "C"  void IOSInAppPurchaseManager_LoadStore_m2636393022 (IOSInAppPurchaseManager_t644626385 * __this, bool ___forceLoad0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::buyProduct(System.String)
extern "C"  void IOSInAppPurchaseManager_buyProduct_m2302481081 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::BuyProduct(System.String)
extern "C"  void IOSInAppPurchaseManager_BuyProduct_m3341027609 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::addProductId(System.String)
extern "C"  void IOSInAppPurchaseManager_addProductId_m1026983503 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::AddProductId(System.String)
extern "C"  void IOSInAppPurchaseManager_AddProductId_m2125652783 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::AddProductId(IOSProductTemplate)
extern "C"  void IOSInAppPurchaseManager_AddProductId_m3325330247 (IOSInAppPurchaseManager_t644626385 * __this, IOSProductTemplate_t1036598382 * ___product0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSProductTemplate IOSInAppPurchaseManager::GetProductById(System.String)
extern "C"  IOSProductTemplate_t1036598382 * IOSInAppPurchaseManager_GetProductById_m49426186 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___prodcutId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::restorePurchases()
extern "C"  void IOSInAppPurchaseManager_restorePurchases_m1350078606 (IOSInAppPurchaseManager_t644626385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::RestorePurchases()
extern "C"  void IOSInAppPurchaseManager_RestorePurchases_m4052565294 (IOSInAppPurchaseManager_t644626385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::verifyLastPurchase(System.String)
extern "C"  void IOSInAppPurchaseManager_verifyLastPurchase_m636449828 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::VerifyLastPurchase(System.String)
extern "C"  void IOSInAppPurchaseManager_VerifyLastPurchase_m1208027524 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::RegisterProductView(IOSStoreProductView)
extern "C"  void IOSInAppPurchaseManager_RegisterProductView_m2365848953 (IOSInAppPurchaseManager_t644626385 * __this, IOSStoreProductView_t607200268 * ___view0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<IOSProductTemplate> IOSInAppPurchaseManager::get_products()
extern "C"  List_1_t405719514 * IOSInAppPurchaseManager_get_products_m3407969300 (IOSInAppPurchaseManager_t644626385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<IOSProductTemplate> IOSInAppPurchaseManager::get_Products()
extern "C"  List_1_t405719514 * IOSInAppPurchaseManager_get_Products_m3007932148 (IOSInAppPurchaseManager_t644626385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSInAppPurchaseManager::get_IsStoreLoaded()
extern "C"  bool IOSInAppPurchaseManager_get_IsStoreLoaded_m4294242645 (IOSInAppPurchaseManager_t644626385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSInAppPurchaseManager::get_IsInAppPurchasesEnabled()
extern "C"  bool IOSInAppPurchaseManager_get_IsInAppPurchasesEnabled_m1399413860 (IOSInAppPurchaseManager_t644626385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSInAppPurchaseManager::get_IsWaitingLoadResult()
extern "C"  bool IOSInAppPurchaseManager_get_IsWaitingLoadResult_m1460682469 (IOSInAppPurchaseManager_t644626385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IOSInAppPurchaseManager::get_NextId()
extern "C"  int32_t IOSInAppPurchaseManager_get_NextId_m1012462667 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::OnStoreKitInitFailed(System.String)
extern "C"  void IOSInAppPurchaseManager_OnStoreKitInitFailed_m4156273221 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onStoreDataReceived(System.String)
extern "C"  void IOSInAppPurchaseManager_onStoreDataReceived_m1635772385 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onProductBought(System.String)
extern "C"  void IOSInAppPurchaseManager_onProductBought_m496272029 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onProductStateDeferred(System.String)
extern "C"  void IOSInAppPurchaseManager_onProductStateDeferred_m1492045900 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___productIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onTransactionFailed(System.String)
extern "C"  void IOSInAppPurchaseManager_onTransactionFailed_m4274109606 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onVerificationResult(System.String)
extern "C"  void IOSInAppPurchaseManager_onVerificationResult_m3442643457 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onRestoreTransactionFailed(System.String)
extern "C"  void IOSInAppPurchaseManager_onRestoreTransactionFailed_m2742453996 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::onRestoreTransactionComplete(System.String)
extern "C"  void IOSInAppPurchaseManager_onRestoreTransactionComplete_m1033599190 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::OnProductViewLoaded(System.String)
extern "C"  void IOSInAppPurchaseManager_OnProductViewLoaded_m1399747320 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___viewId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::OnProductViewLoadedFailed(System.String)
extern "C"  void IOSInAppPurchaseManager_OnProductViewLoadedFailed_m3825601455 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___viewId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::OnProductViewDismissed(System.String)
extern "C"  void IOSInAppPurchaseManager_OnProductViewDismissed_m1794247912 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___viewId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::FireSuccessInitEvent()
extern "C"  void IOSInAppPurchaseManager_FireSuccessInitEvent_m3081901695 (IOSInAppPurchaseManager_t644626385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::FireRestoreCompleteEvent()
extern "C"  void IOSInAppPurchaseManager_FireRestoreCompleteEvent_m3988585911 (IOSInAppPurchaseManager_t644626385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::FireProductBoughtEvent(System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void IOSInAppPurchaseManager_FireProductBoughtEvent_m323405201 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___productIdentifier0, String_t* ___applicationUsername1, String_t* ___receipt2, String_t* ___transactionIdentifier3, bool ___IsRestored4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::SendTransactionFailEvent(System.String,System.String,IOSTransactionErrorCode)
extern "C"  void IOSInAppPurchaseManager_SendTransactionFailEvent_m3003643074 (IOSInAppPurchaseManager_t644626385 * __this, String_t* ___productIdentifier0, String_t* ___errorDescribtion1, int32_t ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::EditorFakeInitEvent()
extern "C"  void IOSInAppPurchaseManager_EditorFakeInitEvent_m1680724740 (IOSInAppPurchaseManager_t644626385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::<OnRestoreStarted>m__4A()
extern "C"  void IOSInAppPurchaseManager_U3COnRestoreStartedU3Em__4A_m3795052748 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::<OnTransactionStarted>m__4B(System.String)
extern "C"  void IOSInAppPurchaseManager_U3COnTransactionStartedU3Em__4B_m2608320473 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::<OnTransactionComplete>m__4C(IOSStoreKitResult)
extern "C"  void IOSInAppPurchaseManager_U3COnTransactionCompleteU3Em__4C_m2377230259 (Il2CppObject * __this /* static, unused */, IOSStoreKitResult_t2359407583 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::<OnRestoreComplete>m__4D(IOSStoreKitRestoreResult)
extern "C"  void IOSInAppPurchaseManager_U3COnRestoreCompleteU3Em__4D_m3565028494 (Il2CppObject * __this /* static, unused */, IOSStoreKitRestoreResult_t3305276155 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::<OnStoreKitInitComplete>m__4E(SA.Common.Models.Result)
extern "C"  void IOSInAppPurchaseManager_U3COnStoreKitInitCompleteU3Em__4E_m80158199 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::<OnPurchasesStateSettingsLoaded>m__4F(System.Boolean)
extern "C"  void IOSInAppPurchaseManager_U3COnPurchasesStateSettingsLoadedU3Em__4F_m2444617982 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSInAppPurchaseManager::<OnVerificationComplete>m__50(IOSStoreKitVerificationResponse)
extern "C"  void IOSInAppPurchaseManager_U3COnVerificationCompleteU3Em__50_m510583527 (Il2CppObject * __this /* static, unused */, IOSStoreKitVerificationResponse_t4263658582 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
