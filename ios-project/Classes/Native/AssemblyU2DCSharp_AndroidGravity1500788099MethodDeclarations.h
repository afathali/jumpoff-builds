﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidGravity
struct AndroidGravity_t1500788099;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidGravity::.ctor()
extern "C"  void AndroidGravity__ctor_m13750010 (AndroidGravity_t1500788099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
