﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "AssemblyU2DCSharp_MCGBehaviour3307770884.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageUploader
struct  ImageUploader_t3573860837  : public MCGBehaviour_t3307770884
{
public:
	// UnityEngine.GameObject ImageUploader::resultImagePlane
	GameObject_t1756533147 * ___resultImagePlane_4;
	// UnityEngine.GameObject ImageUploader::resultFail
	GameObject_t1756533147 * ___resultFail_5;
	// UnityEngine.GameObject ImageUploader::waiting
	GameObject_t1756533147 * ___waiting_6;
	// UnityEngine.UI.Button ImageUploader::btn_skip
	Button_t2872111280 * ___btn_skip_7;
	// UnityEngine.UI.Button ImageUploader::btn_continue
	Button_t2872111280 * ___btn_continue_8;
	// UnityEngine.UI.Button ImageUploader::btn_closeImageFail
	Button_t2872111280 * ___btn_closeImageFail_9;
	// UnityEngine.UI.InputField ImageUploader::input_name
	InputField_t1631627530 * ___input_name_10;
	// UnityEngine.UI.InputField ImageUploader::input_location
	InputField_t1631627530 * ___input_location_11;
	// UnityEngine.UI.InputField ImageUploader::input_notes
	InputField_t1631627530 * ___input_notes_12;
	// UnityEngine.Texture2D ImageUploader::resultImage
	Texture2D_t3542995729 * ___resultImage_13;
	// UnityEngine.UI.Text ImageUploader::debug
	Text_t356221433 * ___debug_14;
	// UnityEngine.UI.Button ImageUploader::btn_rotate
	Button_t2872111280 * ___btn_rotate_15;
	// UnityEngine.Vector3 ImageUploader::localPosition0
	Vector3_t2243707580  ___localPosition0_16;
	// UnityEngine.Quaternion ImageUploader::localRotation0
	Quaternion_t4030073918  ___localRotation0_17;
	// UnityEngine.Vector3 ImageUploader::localScale0
	Vector3_t2243707580  ___localScale0_18;

public:
	inline static int32_t get_offset_of_resultImagePlane_4() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___resultImagePlane_4)); }
	inline GameObject_t1756533147 * get_resultImagePlane_4() const { return ___resultImagePlane_4; }
	inline GameObject_t1756533147 ** get_address_of_resultImagePlane_4() { return &___resultImagePlane_4; }
	inline void set_resultImagePlane_4(GameObject_t1756533147 * value)
	{
		___resultImagePlane_4 = value;
		Il2CppCodeGenWriteBarrier(&___resultImagePlane_4, value);
	}

	inline static int32_t get_offset_of_resultFail_5() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___resultFail_5)); }
	inline GameObject_t1756533147 * get_resultFail_5() const { return ___resultFail_5; }
	inline GameObject_t1756533147 ** get_address_of_resultFail_5() { return &___resultFail_5; }
	inline void set_resultFail_5(GameObject_t1756533147 * value)
	{
		___resultFail_5 = value;
		Il2CppCodeGenWriteBarrier(&___resultFail_5, value);
	}

	inline static int32_t get_offset_of_waiting_6() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___waiting_6)); }
	inline GameObject_t1756533147 * get_waiting_6() const { return ___waiting_6; }
	inline GameObject_t1756533147 ** get_address_of_waiting_6() { return &___waiting_6; }
	inline void set_waiting_6(GameObject_t1756533147 * value)
	{
		___waiting_6 = value;
		Il2CppCodeGenWriteBarrier(&___waiting_6, value);
	}

	inline static int32_t get_offset_of_btn_skip_7() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___btn_skip_7)); }
	inline Button_t2872111280 * get_btn_skip_7() const { return ___btn_skip_7; }
	inline Button_t2872111280 ** get_address_of_btn_skip_7() { return &___btn_skip_7; }
	inline void set_btn_skip_7(Button_t2872111280 * value)
	{
		___btn_skip_7 = value;
		Il2CppCodeGenWriteBarrier(&___btn_skip_7, value);
	}

	inline static int32_t get_offset_of_btn_continue_8() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___btn_continue_8)); }
	inline Button_t2872111280 * get_btn_continue_8() const { return ___btn_continue_8; }
	inline Button_t2872111280 ** get_address_of_btn_continue_8() { return &___btn_continue_8; }
	inline void set_btn_continue_8(Button_t2872111280 * value)
	{
		___btn_continue_8 = value;
		Il2CppCodeGenWriteBarrier(&___btn_continue_8, value);
	}

	inline static int32_t get_offset_of_btn_closeImageFail_9() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___btn_closeImageFail_9)); }
	inline Button_t2872111280 * get_btn_closeImageFail_9() const { return ___btn_closeImageFail_9; }
	inline Button_t2872111280 ** get_address_of_btn_closeImageFail_9() { return &___btn_closeImageFail_9; }
	inline void set_btn_closeImageFail_9(Button_t2872111280 * value)
	{
		___btn_closeImageFail_9 = value;
		Il2CppCodeGenWriteBarrier(&___btn_closeImageFail_9, value);
	}

	inline static int32_t get_offset_of_input_name_10() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___input_name_10)); }
	inline InputField_t1631627530 * get_input_name_10() const { return ___input_name_10; }
	inline InputField_t1631627530 ** get_address_of_input_name_10() { return &___input_name_10; }
	inline void set_input_name_10(InputField_t1631627530 * value)
	{
		___input_name_10 = value;
		Il2CppCodeGenWriteBarrier(&___input_name_10, value);
	}

	inline static int32_t get_offset_of_input_location_11() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___input_location_11)); }
	inline InputField_t1631627530 * get_input_location_11() const { return ___input_location_11; }
	inline InputField_t1631627530 ** get_address_of_input_location_11() { return &___input_location_11; }
	inline void set_input_location_11(InputField_t1631627530 * value)
	{
		___input_location_11 = value;
		Il2CppCodeGenWriteBarrier(&___input_location_11, value);
	}

	inline static int32_t get_offset_of_input_notes_12() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___input_notes_12)); }
	inline InputField_t1631627530 * get_input_notes_12() const { return ___input_notes_12; }
	inline InputField_t1631627530 ** get_address_of_input_notes_12() { return &___input_notes_12; }
	inline void set_input_notes_12(InputField_t1631627530 * value)
	{
		___input_notes_12 = value;
		Il2CppCodeGenWriteBarrier(&___input_notes_12, value);
	}

	inline static int32_t get_offset_of_resultImage_13() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___resultImage_13)); }
	inline Texture2D_t3542995729 * get_resultImage_13() const { return ___resultImage_13; }
	inline Texture2D_t3542995729 ** get_address_of_resultImage_13() { return &___resultImage_13; }
	inline void set_resultImage_13(Texture2D_t3542995729 * value)
	{
		___resultImage_13 = value;
		Il2CppCodeGenWriteBarrier(&___resultImage_13, value);
	}

	inline static int32_t get_offset_of_debug_14() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___debug_14)); }
	inline Text_t356221433 * get_debug_14() const { return ___debug_14; }
	inline Text_t356221433 ** get_address_of_debug_14() { return &___debug_14; }
	inline void set_debug_14(Text_t356221433 * value)
	{
		___debug_14 = value;
		Il2CppCodeGenWriteBarrier(&___debug_14, value);
	}

	inline static int32_t get_offset_of_btn_rotate_15() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___btn_rotate_15)); }
	inline Button_t2872111280 * get_btn_rotate_15() const { return ___btn_rotate_15; }
	inline Button_t2872111280 ** get_address_of_btn_rotate_15() { return &___btn_rotate_15; }
	inline void set_btn_rotate_15(Button_t2872111280 * value)
	{
		___btn_rotate_15 = value;
		Il2CppCodeGenWriteBarrier(&___btn_rotate_15, value);
	}

	inline static int32_t get_offset_of_localPosition0_16() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___localPosition0_16)); }
	inline Vector3_t2243707580  get_localPosition0_16() const { return ___localPosition0_16; }
	inline Vector3_t2243707580 * get_address_of_localPosition0_16() { return &___localPosition0_16; }
	inline void set_localPosition0_16(Vector3_t2243707580  value)
	{
		___localPosition0_16 = value;
	}

	inline static int32_t get_offset_of_localRotation0_17() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___localRotation0_17)); }
	inline Quaternion_t4030073918  get_localRotation0_17() const { return ___localRotation0_17; }
	inline Quaternion_t4030073918 * get_address_of_localRotation0_17() { return &___localRotation0_17; }
	inline void set_localRotation0_17(Quaternion_t4030073918  value)
	{
		___localRotation0_17 = value;
	}

	inline static int32_t get_offset_of_localScale0_18() { return static_cast<int32_t>(offsetof(ImageUploader_t3573860837, ___localScale0_18)); }
	inline Vector3_t2243707580  get_localScale0_18() const { return ___localScale0_18; }
	inline Vector3_t2243707580 * get_address_of_localScale0_18() { return &___localScale0_18; }
	inline void set_localScale0_18(Vector3_t2243707580  value)
	{
		___localScale0_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
