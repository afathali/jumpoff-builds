﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayServicFridnsLoadExample_New
struct PlayServicFridnsLoadExample_New_t1740499043;
// GooglePlayResult
struct GooglePlayResult_t3097469636;
// GooglePlayConnectionResult
struct GooglePlayConnectionResult_t2758718724;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayResult3097469636.h"
#include "AssemblyU2DCSharp_GooglePlayConnectionResult2758718724.h"

// System.Void PlayServicFridnsLoadExample_New::.ctor()
extern "C"  void PlayServicFridnsLoadExample_New__ctor_m1395742354 (PlayServicFridnsLoadExample_New_t1740499043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServicFridnsLoadExample_New::Awake()
extern "C"  void PlayServicFridnsLoadExample_New_Awake_m4068683331 (PlayServicFridnsLoadExample_New_t1740499043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServicFridnsLoadExample_New::ConncetButtonPress()
extern "C"  void PlayServicFridnsLoadExample_New_ConncetButtonPress_m3087743925 (PlayServicFridnsLoadExample_New_t1740499043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServicFridnsLoadExample_New::Update()
extern "C"  void PlayServicFridnsLoadExample_New_Update_m1003490215 (PlayServicFridnsLoadExample_New_t1740499043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServicFridnsLoadExample_New::FixedUpdate()
extern "C"  void PlayServicFridnsLoadExample_New_FixedUpdate_m333814933 (PlayServicFridnsLoadExample_New_t1740499043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServicFridnsLoadExample_New::LoadFriendsList()
extern "C"  void PlayServicFridnsLoadExample_New_LoadFriendsList_m670902347 (PlayServicFridnsLoadExample_New_t1740499043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServicFridnsLoadExample_New::OnFriendListLoaded(GooglePlayResult)
extern "C"  void PlayServicFridnsLoadExample_New_OnFriendListLoaded_m1731558852 (PlayServicFridnsLoadExample_New_t1740499043 * __this, GooglePlayResult_t3097469636 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServicFridnsLoadExample_New::OnPlayerDisconnected()
extern "C"  void PlayServicFridnsLoadExample_New_OnPlayerDisconnected_m892791281 (PlayServicFridnsLoadExample_New_t1740499043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServicFridnsLoadExample_New::OnPlayerConnected()
extern "C"  void PlayServicFridnsLoadExample_New_OnPlayerConnected_m4118939079 (PlayServicFridnsLoadExample_New_t1740499043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServicFridnsLoadExample_New::OnConnectionResult(GooglePlayConnectionResult)
extern "C"  void PlayServicFridnsLoadExample_New_OnConnectionResult_m4044001952 (PlayServicFridnsLoadExample_New_t1740499043 * __this, GooglePlayConnectionResult_t2758718724 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
