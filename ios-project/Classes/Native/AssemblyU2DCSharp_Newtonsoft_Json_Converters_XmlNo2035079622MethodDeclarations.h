﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlNodeWrapper
struct XmlNodeWrapper_t2035079622;
// System.Xml.XmlNode
struct XmlNode_t616554813;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Converters.IXmlNode>
struct IList_1_t1693285147;
// Newtonsoft.Json.Converters.IXmlNode
struct IXmlNode_t1152344546;
// System.Xml.XmlAttribute
struct XmlAttribute_t175731005;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_System_Xml_XmlAttribute175731005.h"

// System.Void Newtonsoft.Json.Converters.XmlNodeWrapper::.ctor(System.Xml.XmlNode)
extern "C"  void XmlNodeWrapper__ctor_m3746775842 (XmlNodeWrapper_t2035079622 * __this, XmlNode_t616554813 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.XmlNodeWrapper::get_WrappedNode()
extern "C"  Il2CppObject * XmlNodeWrapper_get_WrappedNode_m1448054780 (XmlNodeWrapper_t2035079622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType Newtonsoft.Json.Converters.XmlNodeWrapper::get_NodeType()
extern "C"  int32_t XmlNodeWrapper_get_NodeType_m662415004 (XmlNodeWrapper_t2035079622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_Name()
extern "C"  String_t* XmlNodeWrapper_get_Name_m1329970496 (XmlNodeWrapper_t2035079622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_LocalName()
extern "C"  String_t* XmlNodeWrapper_get_LocalName_m1440466161 (XmlNodeWrapper_t2035079622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::get_ChildNodes()
extern "C"  Il2CppObject* XmlNodeWrapper_get_ChildNodes_m2754684449 (XmlNodeWrapper_t2035079622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::WrapNode(System.Xml.XmlNode)
extern "C"  Il2CppObject * XmlNodeWrapper_WrapNode_m1114397699 (XmlNodeWrapper_t2035079622 * __this, XmlNode_t616554813 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::get_Attributes()
extern "C"  Il2CppObject* XmlNodeWrapper_get_Attributes_m2344103395 (XmlNodeWrapper_t2035079622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::get_ParentNode()
extern "C"  Il2CppObject * XmlNodeWrapper_get_ParentNode_m420987291 (XmlNodeWrapper_t2035079622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_Value()
extern "C"  String_t* XmlNodeWrapper_get_Value_m3327952434 (XmlNodeWrapper_t2035079622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeWrapper::set_Value(System.String)
extern "C"  void XmlNodeWrapper_set_Value_m2145889279 (XmlNodeWrapper_t2035079622 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::AppendChild(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  Il2CppObject * XmlNodeWrapper_AppendChild_m4175600708 (XmlNodeWrapper_t2035079622 * __this, Il2CppObject * ___newChild0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_Prefix()
extern "C"  String_t* XmlNodeWrapper_get_Prefix_m3770435055 (XmlNodeWrapper_t2035079622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_NamespaceURI()
extern "C"  String_t* XmlNodeWrapper_get_NamespaceURI_m1300065260 (XmlNodeWrapper_t2035079622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::<get_ChildNodes>m__BF(System.Xml.XmlNode)
extern "C"  Il2CppObject * XmlNodeWrapper_U3Cget_ChildNodesU3Em__BF_m440584888 (XmlNodeWrapper_t2035079622 * __this, XmlNode_t616554813 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::<get_Attributes>m__C0(System.Xml.XmlAttribute)
extern "C"  Il2CppObject * XmlNodeWrapper_U3Cget_AttributesU3Em__C0_m1699170389 (XmlNodeWrapper_t2035079622 * __this, XmlAttribute_t175731005 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
