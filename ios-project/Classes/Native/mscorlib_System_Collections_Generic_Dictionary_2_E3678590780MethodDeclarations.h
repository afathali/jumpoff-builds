﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct Dictionary_2_t2358566078;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3678590780.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_115911300.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3614838359_gshared (Enumerator_t3678590780 * __this, Dictionary_2_t2358566078 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3614838359(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3678590780 *, Dictionary_2_t2358566078 *, const MethodInfo*))Enumerator__ctor_m3614838359_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1050235448_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1050235448(__this, method) ((  Il2CppObject * (*) (Enumerator_t3678590780 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1050235448_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2690476828_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2690476828(__this, method) ((  void (*) (Enumerator_t3678590780 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2690476828_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3529070385_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3529070385(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3678590780 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3529070385_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1312658566_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1312658566(__this, method) ((  Il2CppObject * (*) (Enumerator_t3678590780 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1312658566_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3689523988_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3689523988(__this, method) ((  Il2CppObject * (*) (Enumerator_t3678590780 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3689523988_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3696685668_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3696685668(__this, method) ((  bool (*) (Enumerator_t3678590780 *, const MethodInfo*))Enumerator_MoveNext_m3696685668_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t115911300  Enumerator_get_Current_m1763492508_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1763492508(__this, method) ((  KeyValuePair_2_t115911300  (*) (Enumerator_t3678590780 *, const MethodInfo*))Enumerator_get_Current_m1763492508_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_CurrentKey()
extern "C"  KeyValuePair_2_t3132015601  Enumerator_get_CurrentKey_m3793490395_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3793490395(__this, method) ((  KeyValuePair_2_t3132015601  (*) (Enumerator_t3678590780 *, const MethodInfo*))Enumerator_get_CurrentKey_m3793490395_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m3934977659_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3934977659(__this, method) ((  int32_t (*) (Enumerator_t3678590780 *, const MethodInfo*))Enumerator_get_CurrentValue_m3934977659_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m3460173937_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3460173937(__this, method) ((  void (*) (Enumerator_t3678590780 *, const MethodInfo*))Enumerator_Reset_m3460173937_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1745338064_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1745338064(__this, method) ((  void (*) (Enumerator_t3678590780 *, const MethodInfo*))Enumerator_VerifyState_m1745338064_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m173248908_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m173248908(__this, method) ((  void (*) (Enumerator_t3678590780 *, const MethodInfo*))Enumerator_VerifyCurrent_m173248908_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2454441307_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2454441307(__this, method) ((  void (*) (Enumerator_t3678590780 *, const MethodInfo*))Enumerator_Dispose_m2454441307_gshared)(__this, method)
