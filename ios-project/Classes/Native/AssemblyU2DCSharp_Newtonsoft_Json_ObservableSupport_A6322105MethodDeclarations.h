﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.ObservableSupport.AddingNewEventHandler
struct AddingNewEventHandler_t6322105;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.ObservableSupport.AddingNewEventArgs
struct AddingNewEventArgs_t1197149700;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObservableSuppor1197149700.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Newtonsoft.Json.ObservableSupport.AddingNewEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void AddingNewEventHandler__ctor_m2617261269 (AddingNewEventHandler_t6322105 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.AddingNewEventHandler::Invoke(System.Object,Newtonsoft.Json.ObservableSupport.AddingNewEventArgs)
extern "C"  void AddingNewEventHandler_Invoke_m4131796406 (AddingNewEventHandler_t6322105 * __this, Il2CppObject * ___sender0, AddingNewEventArgs_t1197149700 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Newtonsoft.Json.ObservableSupport.AddingNewEventHandler::BeginInvoke(System.Object,Newtonsoft.Json.ObservableSupport.AddingNewEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddingNewEventHandler_BeginInvoke_m11458169 (AddingNewEventHandler_t6322105 * __this, Il2CppObject * ___sender0, AddingNewEventArgs_t1197149700 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.AddingNewEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void AddingNewEventHandler_EndInvoke_m237214943 (AddingNewEventHandler_t6322105 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
