﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1621714115MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,UnityEngine.Texture2D,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m1468354941(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t731170233 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3418423829_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,UnityEngine.Texture2D,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1148494393(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t731170233 *, int32_t, Texture2D_t3542995729 *, const MethodInfo*))Transform_1_Invoke_m2961650825_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,UnityEngine.Texture2D,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m36723334(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t731170233 *, int32_t, Texture2D_t3542995729 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2451834188_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,UnityEngine.Texture2D,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m2969880151(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t731170233 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1593666539_gshared)(__this, ___result0, method)
