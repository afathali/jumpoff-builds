﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Glow
struct  Glow_t4087105517  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Glow::minAlpha
	float ___minAlpha_2;
	// System.Single Glow::maxAlpha
	float ___maxAlpha_3;
	// System.Single Glow::speed
	float ___speed_4;

public:
	inline static int32_t get_offset_of_minAlpha_2() { return static_cast<int32_t>(offsetof(Glow_t4087105517, ___minAlpha_2)); }
	inline float get_minAlpha_2() const { return ___minAlpha_2; }
	inline float* get_address_of_minAlpha_2() { return &___minAlpha_2; }
	inline void set_minAlpha_2(float value)
	{
		___minAlpha_2 = value;
	}

	inline static int32_t get_offset_of_maxAlpha_3() { return static_cast<int32_t>(offsetof(Glow_t4087105517, ___maxAlpha_3)); }
	inline float get_maxAlpha_3() const { return ___maxAlpha_3; }
	inline float* get_address_of_maxAlpha_3() { return &___maxAlpha_3; }
	inline void set_maxAlpha_3(float value)
	{
		___maxAlpha_3 = value;
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(Glow_t4087105517, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
