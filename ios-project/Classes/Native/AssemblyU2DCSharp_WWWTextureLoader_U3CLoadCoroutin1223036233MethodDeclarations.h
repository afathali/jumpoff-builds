﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WWWTextureLoader/<LoadCoroutin>c__IteratorC
struct U3CLoadCoroutinU3Ec__IteratorC_t1223036233;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void WWWTextureLoader/<LoadCoroutin>c__IteratorC::.ctor()
extern "C"  void U3CLoadCoroutinU3Ec__IteratorC__ctor_m1891961986 (U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WWWTextureLoader/<LoadCoroutin>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadCoroutinU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1601072068 (U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WWWTextureLoader/<LoadCoroutin>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadCoroutinU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m952981804 (U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WWWTextureLoader/<LoadCoroutin>c__IteratorC::MoveNext()
extern "C"  bool U3CLoadCoroutinU3Ec__IteratorC_MoveNext_m735453058 (U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WWWTextureLoader/<LoadCoroutin>c__IteratorC::Dispose()
extern "C"  void U3CLoadCoroutinU3Ec__IteratorC_Dispose_m1183995683 (U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WWWTextureLoader/<LoadCoroutin>c__IteratorC::Reset()
extern "C"  void U3CLoadCoroutinU3Ec__IteratorC_Reset_m786866165 (U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
