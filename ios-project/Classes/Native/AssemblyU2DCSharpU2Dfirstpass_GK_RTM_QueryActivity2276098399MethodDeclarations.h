﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_RTM_QueryActivityResult
struct GK_RTM_QueryActivityResult_t2276098399;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GK_RTM_QueryActivityResult::.ctor(System.Int32)
extern "C"  void GK_RTM_QueryActivityResult__ctor_m3971478101 (GK_RTM_QueryActivityResult_t2276098399 * __this, int32_t ___activity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_RTM_QueryActivityResult::.ctor(System.String)
extern "C"  void GK_RTM_QueryActivityResult__ctor_m3348096870 (GK_RTM_QueryActivityResult_t2276098399 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GK_RTM_QueryActivityResult::get_Activity()
extern "C"  int32_t GK_RTM_QueryActivityResult_get_Activity_m4118817932 (GK_RTM_QueryActivityResult_t2276098399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
