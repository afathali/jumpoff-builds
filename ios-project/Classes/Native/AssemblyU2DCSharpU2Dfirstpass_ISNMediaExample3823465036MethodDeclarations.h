﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISNMediaExample
struct ISNMediaExample_t3823465036;
// MP_MediaItem
struct MP_MediaItem_t4025623029;
// MP_MediaPickerResult
struct MP_MediaPickerResult_t2204006871;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MediaItem4025623029.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MusicPlaybackStat2364713801.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MediaPickerResult2204006871.h"

// System.Void ISNMediaExample::.ctor()
extern "C"  void ISNMediaExample__ctor_m543896093 (ISNMediaExample_t3823465036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISNMediaExample::Awake()
extern "C"  void ISNMediaExample_Awake_m1072677174 (ISNMediaExample_t3823465036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISNMediaExample::HandleActionNowPlayingItemChanged(MP_MediaItem)
extern "C"  void ISNMediaExample_HandleActionNowPlayingItemChanged_m220678379 (ISNMediaExample_t3823465036 * __this, MP_MediaItem_t4025623029 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISNMediaExample::HandleActionPlaybackStateChanged(MP_MusicPlaybackState)
extern "C"  void ISNMediaExample_HandleActionPlaybackStateChanged_m1311937768 (ISNMediaExample_t3823465036 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISNMediaExample::HandleActionQueueUpdated(MP_MediaPickerResult)
extern "C"  void ISNMediaExample_HandleActionQueueUpdated_m2723325904 (ISNMediaExample_t3823465036 * __this, MP_MediaPickerResult_t2204006871 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISNMediaExample::HandleActionMediaPickerResult(MP_MediaPickerResult)
extern "C"  void ISNMediaExample_HandleActionMediaPickerResult_m3230015887 (ISNMediaExample_t3823465036 * __this, MP_MediaPickerResult_t2204006871 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISNMediaExample::OnGUI()
extern "C"  void ISNMediaExample_OnGUI_m4258141283 (ISNMediaExample_t3823465036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
