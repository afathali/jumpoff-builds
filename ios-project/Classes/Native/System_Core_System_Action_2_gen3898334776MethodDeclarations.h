﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen2525452034MethodDeclarations.h"

// System.Void System.Action`2<System.Boolean,ForgotPasswordResponse>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m562865052(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t3898334776 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m946854823_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.Boolean,ForgotPasswordResponse>::Invoke(T1,T2)
#define Action_2_Invoke_m3235387473(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t3898334776 *, bool, ForgotPasswordResponse_t4062332037 *, const MethodInfo*))Action_2_Invoke_m3842146412_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.Boolean,ForgotPasswordResponse>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m3241126956(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t3898334776 *, bool, ForgotPasswordResponse_t4062332037 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m3907381723_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.Boolean,ForgotPasswordResponse>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m3610153812(__this, ___result0, method) ((  void (*) (Action_2_t3898334776 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m2798191693_gshared)(__this, ___result0, method)
