﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JToken/<AfterSelf>c__IteratorE
struct U3CAfterSelfU3Ec__IteratorE_t1950608209;
// Newtonsoft.Json.Linq.JToken
struct JToken_t2552644013;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t28167840;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.JToken/<AfterSelf>c__IteratorE::.ctor()
extern "C"  void U3CAfterSelfU3Ec__IteratorE__ctor_m1094701196 (U3CAfterSelfU3Ec__IteratorE_t1950608209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<AfterSelf>c__IteratorE::System.Collections.Generic.IEnumerator<Newtonsoft.Json.Linq.JToken>.get_Current()
extern "C"  JToken_t2552644013 * U3CAfterSelfU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m4211814049 (U3CAfterSelfU3Ec__IteratorE_t1950608209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JToken/<AfterSelf>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAfterSelfU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m134955754 (U3CAfterSelfU3Ec__IteratorE_t1950608209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JToken/<AfterSelf>c__IteratorE::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CAfterSelfU3Ec__IteratorE_System_Collections_IEnumerable_GetEnumerator_m3500387813 (U3CAfterSelfU3Ec__IteratorE_t1950608209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken/<AfterSelf>c__IteratorE::System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>.GetEnumerator()
extern "C"  Il2CppObject* U3CAfterSelfU3Ec__IteratorE_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_m820180018 (U3CAfterSelfU3Ec__IteratorE_t1950608209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken/<AfterSelf>c__IteratorE::MoveNext()
extern "C"  bool U3CAfterSelfU3Ec__IteratorE_MoveNext_m942037012 (U3CAfterSelfU3Ec__IteratorE_t1950608209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<AfterSelf>c__IteratorE::Dispose()
extern "C"  void U3CAfterSelfU3Ec__IteratorE_Dispose_m2390543923 (U3CAfterSelfU3Ec__IteratorE_t1950608209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<AfterSelf>c__IteratorE::Reset()
extern "C"  void U3CAfterSelfU3Ec__IteratorE_Reset_m3501284817 (U3CAfterSelfU3Ec__IteratorE_t1950608209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
