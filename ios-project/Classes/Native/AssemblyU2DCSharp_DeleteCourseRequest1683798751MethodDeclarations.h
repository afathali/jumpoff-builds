﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeleteCourseRequest
struct DeleteCourseRequest_t1683798751;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DeleteCourseRequest::.ctor()
extern "C"  void DeleteCourseRequest__ctor_m309438898 (DeleteCourseRequest_t1683798751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DeleteCourseRequest::get_Type()
extern "C"  String_t* DeleteCourseRequest_get_Type_m3444488758 (DeleteCourseRequest_t1683798751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeleteCourseRequest::set_Type(System.String)
extern "C"  void DeleteCourseRequest_set_Type_m4050701241 (DeleteCourseRequest_t1683798751 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 DeleteCourseRequest::get_UserId()
extern "C"  int64_t DeleteCourseRequest_get_UserId_m1567976622 (DeleteCourseRequest_t1683798751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeleteCourseRequest::set_UserId(System.Int64)
extern "C"  void DeleteCourseRequest_set_UserId_m2726112397 (DeleteCourseRequest_t1683798751 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 DeleteCourseRequest::get_Id()
extern "C"  int64_t DeleteCourseRequest_get_Id_m3699327683 (DeleteCourseRequest_t1683798751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeleteCourseRequest::set_Id(System.Int64)
extern "C"  void DeleteCourseRequest_set_Id_m1392494332 (DeleteCourseRequest_t1683798751 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
