﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FB_PermissionResult
struct FB_PermissionResult_t2322011743;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,FB_Permission>
struct Dictionary_2_t3787551384;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FB_PermissionResult::.ctor(System.String,System.String)
extern "C"  void FB_PermissionResult__ctor_m3052044200 (FB_PermissionResult_t2322011743 * __this, String_t* ___RawData0, String_t* ___Error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_PermissionResult::SetPermissions(System.Collections.Generic.Dictionary`2<System.String,FB_Permission>)
extern "C"  void FB_PermissionResult_SetPermissions_m469217409 (FB_PermissionResult_t2322011743 * __this, Dictionary_2_t3787551384 * ___permissions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,FB_Permission> FB_PermissionResult::get_Permissions()
extern "C"  Dictionary_2_t3787551384 * FB_PermissionResult_get_Permissions_m558191741 (FB_PermissionResult_t2322011743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
