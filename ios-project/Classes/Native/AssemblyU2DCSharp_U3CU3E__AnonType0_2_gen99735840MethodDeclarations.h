﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType0_2_gen2748928126MethodDeclarations.h"

// System.Void <>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>::.ctor(<Count>__T,<Members>__T)
#define U3CU3E__AnonType0_2__ctor_m1401960813(__this, ___Count0, ___Members1, method) ((  void (*) (U3CU3E__AnonType0_2_t99735840 *, int32_t, Il2CppObject*, const MethodInfo*))U3CU3E__AnonType0_2__ctor_m3256138576_gshared)(__this, ___Count0, ___Members1, method)
// <Count>__T <>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>::get_Count()
#define U3CU3E__AnonType0_2_get_Count_m2713729365(__this, method) ((  int32_t (*) (U3CU3E__AnonType0_2_t99735840 *, const MethodInfo*))U3CU3E__AnonType0_2_get_Count_m2307950002_gshared)(__this, method)
// <Members>__T <>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>::get_Members()
#define U3CU3E__AnonType0_2_get_Members_m2798565461(__this, method) ((  Il2CppObject* (*) (U3CU3E__AnonType0_2_t99735840 *, const MethodInfo*))U3CU3E__AnonType0_2_get_Members_m2220904018_gshared)(__this, method)
// System.Boolean <>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>::Equals(System.Object)
#define U3CU3E__AnonType0_2_Equals_m2179605028(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType0_2_t99735840 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType0_2_Equals_m3561665657_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>::GetHashCode()
#define U3CU3E__AnonType0_2_GetHashCode_m1400142912(__this, method) ((  int32_t (*) (U3CU3E__AnonType0_2_t99735840 *, const MethodInfo*))U3CU3E__AnonType0_2_GetHashCode_m595221463_gshared)(__this, method)
// System.String <>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>::ToString()
#define U3CU3E__AnonType0_2_ToString_m2534526642(__this, method) ((  String_t* (*) (U3CU3E__AnonType0_2_t99735840 *, const MethodInfo*))U3CU3E__AnonType0_2_ToString_m4103874651_gshared)(__this, method)
