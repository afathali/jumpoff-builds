﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// XScaleModifayer
struct XScaleModifayer_t1656594896;

#include "codegen/il2cpp-codegen.h"

// System.Void XScaleModifayer::.ctor()
extern "C"  void XScaleModifayer__ctor_m2070622509 (XScaleModifayer_t1656594896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XScaleModifayer::Awake()
extern "C"  void XScaleModifayer_Awake_m2321640540 (XScaleModifayer_t1656594896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XScaleModifayer::FixedUpdate()
extern "C"  void XScaleModifayer_FixedUpdate_m2659588320 (XScaleModifayer_t1656594896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XScaleModifayer::Calculate()
extern "C"  void XScaleModifayer_Calculate_m3092807809 (XScaleModifayer_t1656594896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
