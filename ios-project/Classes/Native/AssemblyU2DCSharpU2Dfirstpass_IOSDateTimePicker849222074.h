﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.DateTime>
struct Action_1_t495005051;

#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si1331787179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSDateTimePicker
struct  IOSDateTimePicker_t849222074  : public Singleton_1_t1331787179
{
public:
	// System.Action`1<System.DateTime> IOSDateTimePicker::OnDateChanged
	Action_1_t495005051 * ___OnDateChanged_4;
	// System.Action`1<System.DateTime> IOSDateTimePicker::OnPickerClosed
	Action_1_t495005051 * ___OnPickerClosed_5;

public:
	inline static int32_t get_offset_of_OnDateChanged_4() { return static_cast<int32_t>(offsetof(IOSDateTimePicker_t849222074, ___OnDateChanged_4)); }
	inline Action_1_t495005051 * get_OnDateChanged_4() const { return ___OnDateChanged_4; }
	inline Action_1_t495005051 ** get_address_of_OnDateChanged_4() { return &___OnDateChanged_4; }
	inline void set_OnDateChanged_4(Action_1_t495005051 * value)
	{
		___OnDateChanged_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnDateChanged_4, value);
	}

	inline static int32_t get_offset_of_OnPickerClosed_5() { return static_cast<int32_t>(offsetof(IOSDateTimePicker_t849222074, ___OnPickerClosed_5)); }
	inline Action_1_t495005051 * get_OnPickerClosed_5() const { return ___OnPickerClosed_5; }
	inline Action_1_t495005051 ** get_address_of_OnPickerClosed_5() { return &___OnPickerClosed_5; }
	inline void set_OnPickerClosed_5(Action_1_t495005051 * value)
	{
		___OnPickerClosed_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnPickerClosed_5, value);
	}
};

struct IOSDateTimePicker_t849222074_StaticFields
{
public:
	// System.Action`1<System.DateTime> IOSDateTimePicker::<>f__am$cache2
	Action_1_t495005051 * ___U3CU3Ef__amU24cache2_6;
	// System.Action`1<System.DateTime> IOSDateTimePicker::<>f__am$cache3
	Action_1_t495005051 * ___U3CU3Ef__amU24cache3_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(IOSDateTimePicker_t849222074_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline Action_1_t495005051 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline Action_1_t495005051 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(Action_1_t495005051 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(IOSDateTimePicker_t849222074_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline Action_1_t495005051 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline Action_1_t495005051 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(Action_1_t495005051 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
