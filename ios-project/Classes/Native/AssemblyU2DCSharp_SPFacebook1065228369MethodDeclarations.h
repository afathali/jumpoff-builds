﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SPFacebook
struct SPFacebook_t1065228369;
// System.Action
struct Action_t3226471752;
// System.Action`1<FB_PostResult>
struct Action_1_t794684112;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<FB_LoginResult>
struct Action_1_t1650276661;
// System.Action`1<FB_Result>
struct Action_1_t640047754;
// System.Action`1<FB_AppRequestResult>
struct Action_1_t2252469810;
// System.Action`1<FB_PermissionResult>
struct Action_1_t2123811125;
// System.String[]
struct StringU5BU5D_t1642385972;
// FB_UserInfo
struct FB_UserInfo_t2704578078;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// FB_Result
struct FB_Result_t838248372;
// FB_Score
struct FB_Score_t1450841581;
// System.Collections.Generic.List`1<FB_LikeInfo>
struct List_1_t2582320210;
// System.Collections.Generic.Dictionary`2<System.String,FB_UserInfo>
struct Dictionary_2_t324390044;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<FB_UserInfo>
struct List_1_t2073699210;
// System.Collections.Generic.Dictionary`2<System.String,FB_Score>
struct Dictionary_2_t3365620843;
// System.Collections.Generic.List`1<FB_Score>
struct List_1_t819962713;
// System.Collections.Generic.List`1<FB_AppRequest>
struct List_1_t4165401053;
// SP_FB_API
struct SP_FB_API_t498797273;
// FB_LikesRetrieveTask
struct FB_LikesRetrieveTask_t2495592094;
// FB_LoginResult
struct FB_LoginResult_t1848477279;
// FB_PostResult
struct FB_PostResult_t992884730;
// FB_AppRequestResult
struct FB_AppRequestResult_t2450670428;
// FB_PermissionResult
struct FB_PermissionResult_t2322011743;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharp_FB_RequestActionType2205998668.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "AssemblyU2DCSharp_FB_Result838248372.h"
#include "AssemblyU2DCSharp_FB_LikesRetrieveTask2495592094.h"
#include "AssemblyU2DCSharp_FB_Score1450841581.h"
#include "AssemblyU2DCSharp_FB_LoginResult1848477279.h"
#include "AssemblyU2DCSharp_FB_PostResult992884730.h"
#include "AssemblyU2DCSharp_FB_AppRequestResult2450670428.h"
#include "AssemblyU2DCSharp_FB_PermissionResult2322011743.h"

// System.Void SPFacebook::.ctor()
extern "C"  void SPFacebook__ctor_m994089058 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::.cctor()
extern "C"  void SPFacebook__cctor_m3444805091 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnPostStarted(System.Action)
extern "C"  void SPFacebook_add_OnPostStarted_m1836028031 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnPostStarted(System.Action)
extern "C"  void SPFacebook_remove_OnPostStarted_m1482225374 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnLoginStarted(System.Action)
extern "C"  void SPFacebook_add_OnLoginStarted_m941497642 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnLoginStarted(System.Action)
extern "C"  void SPFacebook_remove_OnLoginStarted_m3661915863 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnLogOut(System.Action)
extern "C"  void SPFacebook_add_OnLogOut_m2993725138 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnLogOut(System.Action)
extern "C"  void SPFacebook_remove_OnLogOut_m1106715647 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnFriendsRequestStarted(System.Action)
extern "C"  void SPFacebook_add_OnFriendsRequestStarted_m3161278191 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnFriendsRequestStarted(System.Action)
extern "C"  void SPFacebook_remove_OnFriendsRequestStarted_m1693189322 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnInitCompleteAction(System.Action)
extern "C"  void SPFacebook_add_OnInitCompleteAction_m1739495479 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnInitCompleteAction(System.Action)
extern "C"  void SPFacebook_remove_OnInitCompleteAction_m781463276 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnPostingCompleteAction(System.Action`1<FB_PostResult>)
extern "C"  void SPFacebook_add_OnPostingCompleteAction_m1801318890 (Il2CppObject * __this /* static, unused */, Action_1_t794684112 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnPostingCompleteAction(System.Action`1<FB_PostResult>)
extern "C"  void SPFacebook_remove_OnPostingCompleteAction_m3096897059 (Il2CppObject * __this /* static, unused */, Action_1_t794684112 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnFocusChangedAction(System.Action`1<System.Boolean>)
extern "C"  void SPFacebook_add_OnFocusChangedAction_m1479343510 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnFocusChangedAction(System.Action`1<System.Boolean>)
extern "C"  void SPFacebook_remove_OnFocusChangedAction_m3511750225 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnAuthCompleteAction(System.Action`1<FB_LoginResult>)
extern "C"  void SPFacebook_add_OnAuthCompleteAction_m721275231 (Il2CppObject * __this /* static, unused */, Action_1_t1650276661 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnAuthCompleteAction(System.Action`1<FB_LoginResult>)
extern "C"  void SPFacebook_remove_OnAuthCompleteAction_m1930955888 (Il2CppObject * __this /* static, unused */, Action_1_t1650276661 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnUserDataRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_add_OnUserDataRequestCompleteAction_m3091390762 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnUserDataRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_remove_OnUserDataRequestCompleteAction_m3568477115 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnFriendsDataRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_add_OnFriendsDataRequestCompleteAction_m646261332 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnFriendsDataRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_remove_OnFriendsDataRequestCompleteAction_m1445360335 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnInvitableFriendsDataRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_add_OnInvitableFriendsDataRequestCompleteAction_m3474336522 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnInvitableFriendsDataRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_remove_OnInvitableFriendsDataRequestCompleteAction_m3219428137 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnAppRequestCompleteAction(System.Action`1<FB_AppRequestResult>)
extern "C"  void SPFacebook_add_OnAppRequestCompleteAction_m2553511066 (Il2CppObject * __this /* static, unused */, Action_1_t2252469810 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnAppRequestCompleteAction(System.Action`1<FB_AppRequestResult>)
extern "C"  void SPFacebook_remove_OnAppRequestCompleteAction_m3854762283 (Il2CppObject * __this /* static, unused */, Action_1_t2252469810 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnAppRequestsLoaded(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_add_OnAppRequestsLoaded_m236404475 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnAppRequestsLoaded(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_remove_OnAppRequestsLoaded_m398220878 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnPermissionsLoaded(System.Action`1<FB_PermissionResult>)
extern "C"  void SPFacebook_add_OnPermissionsLoaded_m1741412311 (Il2CppObject * __this /* static, unused */, Action_1_t2123811125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnPermissionsLoaded(System.Action`1<FB_PermissionResult>)
extern "C"  void SPFacebook_remove_OnPermissionsLoaded_m1366615304 (Il2CppObject * __this /* static, unused */, Action_1_t2123811125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnRevokePermission(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_add_OnRevokePermission_m3986511718 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnRevokePermission(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_remove_OnRevokePermission_m1006885319 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnAppScoresRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_add_OnAppScoresRequestCompleteAction_m3441503959 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnAppScoresRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_remove_OnAppScoresRequestCompleteAction_m1691407652 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnPlayerScoresRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_add_OnPlayerScoresRequestCompleteAction_m1857694289 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnPlayerScoresRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_remove_OnPlayerScoresRequestCompleteAction_m103046544 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnSubmitScoreRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_add_OnSubmitScoreRequestCompleteAction_m1360744965 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnSubmitScoreRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_remove_OnSubmitScoreRequestCompleteAction_m3803828606 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnDeleteScoresRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_add_OnDeleteScoresRequestCompleteAction_m233855875 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnDeleteScoresRequestCompleteAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_remove_OnDeleteScoresRequestCompleteAction_m1325674074 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::add_OnLikesListLoadedAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_add_OnLikesListLoadedAction_m2416175948 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::remove_OnLikesListLoadedAction(System.Action`1<FB_Result>)
extern "C"  void SPFacebook_remove_OnLikesListLoadedAction_m798839339 (Il2CppObject * __this /* static, unused */, Action_1_t640047754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::Awake()
extern "C"  void SPFacebook_Awake_m366806013 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::Init()
extern "C"  void SPFacebook_Init_m289158378 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::Login()
extern "C"  void SPFacebook_Login_m2232572207 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::Login(System.String[])
extern "C"  void SPFacebook_Login_m1153980235 (SPFacebook_t1065228369 * __this, StringU5BU5D_t1642385972* ___scopes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::Logout()
extern "C"  void SPFacebook_Logout_m39719546 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::LoadUserData()
extern "C"  void SPFacebook_LoadUserData_m4288587319 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::LoadInvitableFrientdsInfo(System.Int32)
extern "C"  void SPFacebook_LoadInvitableFrientdsInfo_m4121455816 (SPFacebook_t1065228369 * __this, int32_t ___limit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FB_UserInfo SPFacebook::GetInvitableFriendById(System.String)
extern "C"  FB_UserInfo_t2704578078 * SPFacebook_GetInvitableFriendById_m1197495797 (SPFacebook_t1065228369 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::LoadFrientdsInfo(System.Int32)
extern "C"  void SPFacebook_LoadFrientdsInfo_m333573628 (SPFacebook_t1065228369 * __this, int32_t ___limit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FB_UserInfo SPFacebook::GetFriendById(System.String)
extern "C"  FB_UserInfo_t2704578078 * SPFacebook_GetFriendById_m1349542951 (SPFacebook_t1065228369 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::PostImage(System.String,UnityEngine.Texture2D)
extern "C"  void SPFacebook_PostImage_m1798237407 (SPFacebook_t1065228369 * __this, String_t* ___caption0, Texture2D_t3542995729 * ___image1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::PostText(System.String)
extern "C"  void SPFacebook_PostText_m3127589435 (SPFacebook_t1065228369 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::FeedShare(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void SPFacebook_FeedShare_m3810967121 (SPFacebook_t1065228369 * __this, String_t* ___toId0, String_t* ___link1, String_t* ___linkName2, String_t* ___linkCaption3, String_t* ___linkDescription4, String_t* ___picture5, String_t* ___actionName6, String_t* ___actionLink7, String_t* ___reference8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::SendTrunRequest(System.String,System.String,System.String,System.String[])
extern "C"  void SPFacebook_SendTrunRequest_m1573426744 (SPFacebook_t1065228369 * __this, String_t* ___title0, String_t* ___message1, String_t* ___data2, StringU5BU5D_t1642385972* ___to3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::SendGift(System.String,System.String,System.String,System.String,System.String[])
extern "C"  void SPFacebook_SendGift_m764113990 (SPFacebook_t1065228369 * __this, String_t* ___title0, String_t* ___message1, String_t* ___objectId2, String_t* ___data3, StringU5BU5D_t1642385972* ___to4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::AskGift(System.String,System.String,System.String,System.String,System.String[])
extern "C"  void SPFacebook_AskGift_m870028097 (SPFacebook_t1065228369 * __this, String_t* ___title0, String_t* ___message1, String_t* ___objectId2, String_t* ___data3, StringU5BU5D_t1642385972* ___to4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::SendInvite(System.String,System.String,System.String,System.String[])
extern "C"  void SPFacebook_SendInvite_m444732627 (SPFacebook_t1065228369 * __this, String_t* ___title0, String_t* ___message1, String_t* ___data2, StringU5BU5D_t1642385972* ___to3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::OnAppRequestFailed_AndroidCB(System.String)
extern "C"  void SPFacebook_OnAppRequestFailed_AndroidCB_m1054313701 (SPFacebook_t1065228369 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::OnAppRequestCompleted_AndroidCB(System.String)
extern "C"  void SPFacebook_OnAppRequestCompleted_AndroidCB_m1955252675 (SPFacebook_t1065228369 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::AppRequest(System.String,FB_RequestActionType,System.String,System.String[],System.String,System.String)
extern "C"  void SPFacebook_AppRequest_m1782717544 (SPFacebook_t1065228369 * __this, String_t* ___message0, int32_t ___actionType1, String_t* ___objectId2, StringU5BU5D_t1642385972* ___to3, String_t* ___data4, String_t* ___title5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::AppRequest(System.String,FB_RequestActionType,System.String,System.Collections.Generic.List`1<System.Object>,System.String[],System.Nullable`1<System.Int32>,System.String,System.String)
extern "C"  void SPFacebook_AppRequest_m937289152 (SPFacebook_t1065228369 * __this, String_t* ___message0, int32_t ___actionType1, String_t* ___objectId2, List_1_t2058570427 * ___filters3, StringU5BU5D_t1642385972* ___excludeIds4, Nullable_1_t334943763  ___maxRecipients5, String_t* ___data6, String_t* ___title7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::AppRequest(System.String,System.String[],System.Collections.Generic.List`1<System.Object>,System.String[],System.Nullable`1<System.Int32>,System.String,System.String)
extern "C"  void SPFacebook_AppRequest_m1200449150 (SPFacebook_t1065228369 * __this, String_t* ___message0, StringU5BU5D_t1642385972* ___to1, List_1_t2058570427 * ___filters2, StringU5BU5D_t1642385972* ___excludeIds3, Nullable_1_t334943763  ___maxRecipients4, String_t* ___data5, String_t* ___title6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::LoadPendingRequests()
extern "C"  void SPFacebook_LoadPendingRequests_m2325673335 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::OnRequestsLoadComplete(FB_Result)
extern "C"  void SPFacebook_OnRequestsLoadComplete_m261724190 (SPFacebook_t1065228369 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::LoadPlayerScores()
extern "C"  void SPFacebook_LoadPlayerScores_m2349212558 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::LoadAppScores()
extern "C"  void SPFacebook_LoadAppScores_m422665926 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::SubmitScore(System.Int32)
extern "C"  void SPFacebook_SubmitScore_m1235546237 (SPFacebook_t1065228369 * __this, int32_t ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::DeletePlayerScores()
extern "C"  void SPFacebook_DeletePlayerScores_m1162546041 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::LoadCurrentUserLikes()
extern "C"  void SPFacebook_LoadCurrentUserLikes_m1309383910 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::LoadLikes(System.String)
extern "C"  void SPFacebook_LoadLikes_m341140840 (SPFacebook_t1065228369 * __this, String_t* ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::LoadLikes(System.String,System.String)
extern "C"  void SPFacebook_LoadLikes_m1081252168 (SPFacebook_t1065228369 * __this, String_t* ___userId0, String_t* ___pageId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FB_Score SPFacebook::GetCurrentPlayerScoreByAppId(System.String)
extern "C"  FB_Score_t1450841581 * SPFacebook_GetCurrentPlayerScoreByAppId_m3533941937 (SPFacebook_t1065228369 * __this, String_t* ___appId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SPFacebook::GetCurrentPlayerIntScoreByAppId(System.String)
extern "C"  int32_t SPFacebook_GetCurrentPlayerIntScoreByAppId_m2139738996 (SPFacebook_t1065228369 * __this, String_t* ___appId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SPFacebook::GetScoreByUserId(System.String)
extern "C"  int32_t SPFacebook_GetScoreByUserId_m3478004261 (SPFacebook_t1065228369 * __this, String_t* ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FB_Score SPFacebook::GetScoreObjectByUserId(System.String)
extern "C"  FB_Score_t1450841581 * SPFacebook_GetScoreObjectByUserId_m390775956 (SPFacebook_t1065228369 * __this, String_t* ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FB_LikeInfo> SPFacebook::GerUserLikesList(System.String)
extern "C"  List_1_t2582320210 * SPFacebook_GerUserLikesList_m857403448 (SPFacebook_t1065228369 * __this, String_t* ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SPFacebook::IsUserLikesPage(System.String,System.String)
extern "C"  bool SPFacebook_IsUserLikesPage_m4160233536 (SPFacebook_t1065228369 * __this, String_t* ___userId0, String_t* ___pageId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SPFacebook::get_IsInited()
extern "C"  bool SPFacebook_get_IsInited_m2967349816 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SPFacebook::get_IsLoggedIn()
extern "C"  bool SPFacebook_get_IsLoggedIn_m1377299718 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SPFacebook::get_UserId()
extern "C"  String_t* SPFacebook_get_UserId_m2085529982 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SPFacebook::get_AccessToken()
extern "C"  String_t* SPFacebook_get_AccessToken_m4142108399 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SPFacebook::get_AppId()
extern "C"  String_t* SPFacebook_get_AppId_m3533438328 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FB_UserInfo SPFacebook::get_userInfo()
extern "C"  FB_UserInfo_t2704578078 * SPFacebook_get_userInfo_m4048100853 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,FB_UserInfo> SPFacebook::get_friends()
extern "C"  Dictionary_2_t324390044 * SPFacebook_get_friends_m3272985060 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,FB_UserInfo> SPFacebook::get_InvitableFriends()
extern "C"  Dictionary_2_t324390044 * SPFacebook_get_InvitableFriends_m1910888598 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> SPFacebook::get_friendsIds()
extern "C"  List_1_t1398341365 * SPFacebook_get_friendsIds_m1673532449 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> SPFacebook::get_InvitableFriendsIds()
extern "C"  List_1_t1398341365 * SPFacebook_get_InvitableFriendsIds_m3641093423 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FB_UserInfo> SPFacebook::get_friendsList()
extern "C"  List_1_t2073699210 * SPFacebook_get_friendsList_m1066197375 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FB_UserInfo> SPFacebook::get_InvitableFriendsList()
extern "C"  List_1_t2073699210 * SPFacebook_get_InvitableFriendsList_m1774626905 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,FB_Score> SPFacebook::get_userScores()
extern "C"  Dictionary_2_t3365620843 * SPFacebook_get_userScores_m3942393562 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,FB_Score> SPFacebook::get_appScores()
extern "C"  Dictionary_2_t3365620843 * SPFacebook_get_appScores_m43712376 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FB_Score> SPFacebook::get_applicationScoreList()
extern "C"  List_1_t819962713 * SPFacebook_get_applicationScoreList_m1592258615 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FB_AppRequest> SPFacebook::get_AppRequests()
extern "C"  List_1_t4165401053 * SPFacebook_get_AppRequests_m1478533086 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SP_FB_API SPFacebook::get_FB()
extern "C"  Il2CppObject * SPFacebook_get_FB_m2945357425 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::OnUserLikesResult(FB_Result,FB_LikesRetrieveTask)
extern "C"  void SPFacebook_OnUserLikesResult_m1474140179 (SPFacebook_t1065228369 * __this, FB_Result_t838248372 * ___result0, FB_LikesRetrieveTask_t2495592094 * ___task1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::OnScoreDeleted(FB_Result)
extern "C"  void SPFacebook_OnScoreDeleted_m4096027036 (SPFacebook_t1065228369 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::OnScoreSubmited(FB_Result)
extern "C"  void SPFacebook_OnScoreSubmited_m2049455644 (SPFacebook_t1065228369 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::OnAppScoresComplete(FB_Result)
extern "C"  void SPFacebook_OnAppScoresComplete_m731248076 (SPFacebook_t1065228369 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::AddToAppScores(FB_Score)
extern "C"  void SPFacebook_AddToAppScores_m222331079 (SPFacebook_t1065228369 * __this, FB_Score_t1450841581 * ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::AddToUserScores(FB_Score)
extern "C"  void SPFacebook_AddToUserScores_m3277103543 (SPFacebook_t1065228369 * __this, FB_Score_t1450841581 * ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::OnLoaPlayrScoresComplete(FB_Result)
extern "C"  void SPFacebook_OnLoaPlayrScoresComplete_m3197643597 (SPFacebook_t1065228369 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::ParseInvitableFriendsData(System.String)
extern "C"  void SPFacebook_ParseInvitableFriendsData_m2551626576 (SPFacebook_t1065228369 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::ParseFriendsFromJson(System.String,System.Collections.Generic.Dictionary`2<System.String,FB_UserInfo>,System.Boolean)
extern "C"  void SPFacebook_ParseFriendsFromJson_m1038195296 (SPFacebook_t1065228369 * __this, String_t* ___data0, Dictionary_2_t324390044 * ___friends1, bool ___invitable2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::ParseFriendsData(System.String)
extern "C"  void SPFacebook_ParseFriendsData_m2428779522 (SPFacebook_t1065228369 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::OnInitComplete()
extern "C"  void SPFacebook_OnInitComplete_m1629767304 (SPFacebook_t1065228369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::OnHideUnity(System.Boolean)
extern "C"  void SPFacebook_OnHideUnity_m2935682145 (SPFacebook_t1065228369 * __this, bool ___isGameShown0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::LoginCallback(FB_LoginResult)
extern "C"  void SPFacebook_LoginCallback_m2654123307 (SPFacebook_t1065228369 * __this, FB_LoginResult_t1848477279 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::PostCallback_Internal(FB_Result)
extern "C"  void SPFacebook_PostCallback_Internal_m1179496431 (SPFacebook_t1065228369 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::PostCallback(FB_PostResult)
extern "C"  void SPFacebook_PostCallback_m390104159 (SPFacebook_t1065228369 * __this, FB_PostResult_t992884730 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::AppRequestCallback(FB_AppRequestResult)
extern "C"  void SPFacebook_AppRequestCallback_m3776231583 (SPFacebook_t1065228369 * __this, FB_AppRequestResult_t2450670428 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::UserDataCallBack(FB_Result)
extern "C"  void SPFacebook_UserDataCallBack_m3929646298 (SPFacebook_t1065228369 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::InvitableFriendsDataCallBack(FB_Result)
extern "C"  void SPFacebook_InvitableFriendsDataCallBack_m911467398 (SPFacebook_t1065228369 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::FriendsDataCallBack(FB_Result)
extern "C"  void SPFacebook_FriendsDataCallBack_m846027834 (SPFacebook_t1065228369 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnPostStarted>m__8C()
extern "C"  void SPFacebook_U3COnPostStartedU3Em__8C_m3564703606 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnLoginStarted>m__8D()
extern "C"  void SPFacebook_U3COnLoginStartedU3Em__8D_m2678009246 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnLogOut>m__8E()
extern "C"  void SPFacebook_U3COnLogOutU3Em__8E_m1757123237 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnFriendsRequestStarted>m__8F()
extern "C"  void SPFacebook_U3COnFriendsRequestStartedU3Em__8F_m3148175945 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnInitCompleteAction>m__90()
extern "C"  void SPFacebook_U3COnInitCompleteActionU3Em__90_m1852602618 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnPostingCompleteAction>m__91(FB_PostResult)
extern "C"  void SPFacebook_U3COnPostingCompleteActionU3Em__91_m2946074151 (Il2CppObject * __this /* static, unused */, FB_PostResult_t992884730 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnFocusChangedAction>m__92(System.Boolean)
extern "C"  void SPFacebook_U3COnFocusChangedActionU3Em__92_m440387848 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnAuthCompleteAction>m__93(FB_LoginResult)
extern "C"  void SPFacebook_U3COnAuthCompleteActionU3Em__93_m2533378698 (Il2CppObject * __this /* static, unused */, FB_LoginResult_t1848477279 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnUserDataRequestCompleteAction>m__94(FB_Result)
extern "C"  void SPFacebook_U3COnUserDataRequestCompleteActionU3Em__94_m532202958 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnFriendsDataRequestCompleteAction>m__95(FB_Result)
extern "C"  void SPFacebook_U3COnFriendsDataRequestCompleteActionU3Em__95_m815295939 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnInvitableFriendsDataRequestCompleteAction>m__96(FB_Result)
extern "C"  void SPFacebook_U3COnInvitableFriendsDataRequestCompleteActionU3Em__96_m3450804688 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnAppRequestCompleteAction>m__97(FB_AppRequestResult)
extern "C"  void SPFacebook_U3COnAppRequestCompleteActionU3Em__97_m2676784029 (Il2CppObject * __this /* static, unused */, FB_AppRequestResult_t2450670428 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnAppRequestsLoaded>m__98(FB_Result)
extern "C"  void SPFacebook_U3COnAppRequestsLoadedU3Em__98_m3281502025 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnPermissionsLoaded>m__99(FB_PermissionResult)
extern "C"  void SPFacebook_U3COnPermissionsLoadedU3Em__99_m3321761450 (Il2CppObject * __this /* static, unused */, FB_PermissionResult_t2322011743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnRevokePermission>m__9A(FB_Result)
extern "C"  void SPFacebook_U3COnRevokePermissionU3Em__9A_m744214167 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnAppScoresRequestCompleteAction>m__9B(FB_Result)
extern "C"  void SPFacebook_U3COnAppScoresRequestCompleteActionU3Em__9B_m318464667 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnPlayerScoresRequestCompleteAction>m__9C(FB_Result)
extern "C"  void SPFacebook_U3COnPlayerScoresRequestCompleteActionU3Em__9C_m3841831188 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnSubmitScoreRequestCompleteAction>m__9D(FB_Result)
extern "C"  void SPFacebook_U3COnSubmitScoreRequestCompleteActionU3Em__9D_m879110627 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnDeleteScoresRequestCompleteAction>m__9E(FB_Result)
extern "C"  void SPFacebook_U3COnDeleteScoresRequestCompleteActionU3Em__9E_m2103646124 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook::<OnLikesListLoadedAction>m__9F(FB_Result)
extern "C"  void SPFacebook_U3COnLikesListLoadedActionU3Em__9F_m742082022 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
