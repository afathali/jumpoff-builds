﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PackageAppInfo
struct PackageAppInfo_t260912591;

#include "codegen/il2cpp-codegen.h"

// System.Void PackageAppInfo::.ctor()
extern "C"  void PackageAppInfo__ctor_m1148597554 (PackageAppInfo_t260912591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
