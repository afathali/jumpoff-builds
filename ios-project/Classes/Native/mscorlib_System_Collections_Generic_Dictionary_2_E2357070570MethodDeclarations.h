﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m429577194(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2357070570 *, Dictionary_2_t1037045868 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1721024309(__this, method) ((  Il2CppObject * (*) (Enumerator_t2357070570 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2366294505(__this, method) ((  void (*) (Enumerator_t2357070570 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3362682534(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2357070570 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m307438923(__this, method) ((  Il2CppObject * (*) (Enumerator_t2357070570 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m651461083(__this, method) ((  Il2CppObject * (*) (Enumerator_t2357070570 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::MoveNext()
#define Enumerator_MoveNext_m4113568480(__this, method) ((  bool (*) (Enumerator_t2357070570 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::get_Current()
#define Enumerator_get_Current_m3400199455(__this, method) ((  KeyValuePair_2_t3089358386  (*) (Enumerator_t2357070570 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2286579586(__this, method) ((  int32_t (*) (Enumerator_t2357070570 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m524803946(__this, method) ((  String_t* (*) (Enumerator_t2357070570 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::Reset()
#define Enumerator_Reset_m1163539308(__this, method) ((  void (*) (Enumerator_t2357070570 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::VerifyState()
#define Enumerator_VerifyState_m608582943(__this, method) ((  void (*) (Enumerator_t2357070570 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2980717785(__this, method) ((  void (*) (Enumerator_t2357070570 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.String>::Dispose()
#define Enumerator_Dispose_m2527538842(__this, method) ((  void (*) (Enumerator_t2357070570 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
