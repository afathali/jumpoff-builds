﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSSocialUseExample/<PostFBScreenshot>c__Iterator6
struct U3CPostFBScreenshotU3Ec__Iterator6_t582894989;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSSocialUseExample/<PostFBScreenshot>c__Iterator6::.ctor()
extern "C"  void U3CPostFBScreenshotU3Ec__Iterator6__ctor_m513574068 (U3CPostFBScreenshotU3Ec__Iterator6_t582894989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSSocialUseExample/<PostFBScreenshot>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPostFBScreenshotU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m95754132 (U3CPostFBScreenshotU3Ec__Iterator6_t582894989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSSocialUseExample/<PostFBScreenshot>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPostFBScreenshotU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m922908236 (U3CPostFBScreenshotU3Ec__Iterator6_t582894989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSSocialUseExample/<PostFBScreenshot>c__Iterator6::MoveNext()
extern "C"  bool U3CPostFBScreenshotU3Ec__Iterator6_MoveNext_m1520583784 (U3CPostFBScreenshotU3Ec__Iterator6_t582894989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample/<PostFBScreenshot>c__Iterator6::Dispose()
extern "C"  void U3CPostFBScreenshotU3Ec__Iterator6_Dispose_m3327922941 (U3CPostFBScreenshotU3Ec__Iterator6_t582894989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample/<PostFBScreenshot>c__Iterator6::Reset()
extern "C"  void U3CPostFBScreenshotU3Ec__Iterator6_Reset_m701294419 (U3CPostFBScreenshotU3Ec__Iterator6_t582894989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
