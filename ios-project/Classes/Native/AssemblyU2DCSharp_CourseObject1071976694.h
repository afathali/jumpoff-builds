﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// Course
struct Course_t3483112699;

#include "AssemblyU2DCSharp_MCGBehaviour3307770884.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CourseObject
struct  CourseObject_t1071976694  : public MCGBehaviour_t3307770884
{
public:
	// UnityEngine.UI.Text CourseObject::CourseName
	Text_t356221433 * ___CourseName_4;
	// UnityEngine.UI.Text CourseObject::CourseLocation
	Text_t356221433 * ___CourseLocation_5;
	// UnityEngine.UI.Text CourseObject::CourseDate
	Text_t356221433 * ___CourseDate_6;
	// UnityEngine.UI.Button CourseObject::btn_openCourse
	Button_t2872111280 * ___btn_openCourse_7;
	// UnityEngine.UI.Button CourseObject::btn_details
	Button_t2872111280 * ___btn_details_8;
	// UnityEngine.UI.Button CourseObject::btn_delete
	Button_t2872111280 * ___btn_delete_9;
	// Course CourseObject::MyCourse
	Course_t3483112699 * ___MyCourse_10;

public:
	inline static int32_t get_offset_of_CourseName_4() { return static_cast<int32_t>(offsetof(CourseObject_t1071976694, ___CourseName_4)); }
	inline Text_t356221433 * get_CourseName_4() const { return ___CourseName_4; }
	inline Text_t356221433 ** get_address_of_CourseName_4() { return &___CourseName_4; }
	inline void set_CourseName_4(Text_t356221433 * value)
	{
		___CourseName_4 = value;
		Il2CppCodeGenWriteBarrier(&___CourseName_4, value);
	}

	inline static int32_t get_offset_of_CourseLocation_5() { return static_cast<int32_t>(offsetof(CourseObject_t1071976694, ___CourseLocation_5)); }
	inline Text_t356221433 * get_CourseLocation_5() const { return ___CourseLocation_5; }
	inline Text_t356221433 ** get_address_of_CourseLocation_5() { return &___CourseLocation_5; }
	inline void set_CourseLocation_5(Text_t356221433 * value)
	{
		___CourseLocation_5 = value;
		Il2CppCodeGenWriteBarrier(&___CourseLocation_5, value);
	}

	inline static int32_t get_offset_of_CourseDate_6() { return static_cast<int32_t>(offsetof(CourseObject_t1071976694, ___CourseDate_6)); }
	inline Text_t356221433 * get_CourseDate_6() const { return ___CourseDate_6; }
	inline Text_t356221433 ** get_address_of_CourseDate_6() { return &___CourseDate_6; }
	inline void set_CourseDate_6(Text_t356221433 * value)
	{
		___CourseDate_6 = value;
		Il2CppCodeGenWriteBarrier(&___CourseDate_6, value);
	}

	inline static int32_t get_offset_of_btn_openCourse_7() { return static_cast<int32_t>(offsetof(CourseObject_t1071976694, ___btn_openCourse_7)); }
	inline Button_t2872111280 * get_btn_openCourse_7() const { return ___btn_openCourse_7; }
	inline Button_t2872111280 ** get_address_of_btn_openCourse_7() { return &___btn_openCourse_7; }
	inline void set_btn_openCourse_7(Button_t2872111280 * value)
	{
		___btn_openCourse_7 = value;
		Il2CppCodeGenWriteBarrier(&___btn_openCourse_7, value);
	}

	inline static int32_t get_offset_of_btn_details_8() { return static_cast<int32_t>(offsetof(CourseObject_t1071976694, ___btn_details_8)); }
	inline Button_t2872111280 * get_btn_details_8() const { return ___btn_details_8; }
	inline Button_t2872111280 ** get_address_of_btn_details_8() { return &___btn_details_8; }
	inline void set_btn_details_8(Button_t2872111280 * value)
	{
		___btn_details_8 = value;
		Il2CppCodeGenWriteBarrier(&___btn_details_8, value);
	}

	inline static int32_t get_offset_of_btn_delete_9() { return static_cast<int32_t>(offsetof(CourseObject_t1071976694, ___btn_delete_9)); }
	inline Button_t2872111280 * get_btn_delete_9() const { return ___btn_delete_9; }
	inline Button_t2872111280 ** get_address_of_btn_delete_9() { return &___btn_delete_9; }
	inline void set_btn_delete_9(Button_t2872111280 * value)
	{
		___btn_delete_9 = value;
		Il2CppCodeGenWriteBarrier(&___btn_delete_9, value);
	}

	inline static int32_t get_offset_of_MyCourse_10() { return static_cast<int32_t>(offsetof(CourseObject_t1071976694, ___MyCourse_10)); }
	inline Course_t3483112699 * get_MyCourse_10() const { return ___MyCourse_10; }
	inline Course_t3483112699 ** get_address_of_MyCourse_10() { return &___MyCourse_10; }
	inline void set_MyCourse_10(Course_t3483112699 * value)
	{
		___MyCourse_10 = value;
		Il2CppCodeGenWriteBarrier(&___MyCourse_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
