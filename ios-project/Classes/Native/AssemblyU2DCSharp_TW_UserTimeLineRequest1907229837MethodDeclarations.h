﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TW_UserTimeLineRequest
struct TW_UserTimeLineRequest_t1907229837;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TW_UserTimeLineRequest::.ctor()
extern "C"  void TW_UserTimeLineRequest__ctor_m3312304402 (TW_UserTimeLineRequest_t1907229837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TW_UserTimeLineRequest TW_UserTimeLineRequest::Create()
extern "C"  TW_UserTimeLineRequest_t1907229837 * TW_UserTimeLineRequest_Create_m1308800106 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_UserTimeLineRequest::Awake()
extern "C"  void TW_UserTimeLineRequest_Awake_m3945703501 (TW_UserTimeLineRequest_t1907229837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_UserTimeLineRequest::OnResult(System.String)
extern "C"  void TW_UserTimeLineRequest_OnResult_m3620510938 (TW_UserTimeLineRequest_t1907229837 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
