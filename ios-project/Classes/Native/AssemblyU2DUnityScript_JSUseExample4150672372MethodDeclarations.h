﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSUseExample
struct JSUseExample_t4150672372;

#include "codegen/il2cpp-codegen.h"

// System.Void JSUseExample::.ctor()
extern "C"  void JSUseExample__ctor_m2383184438 (JSUseExample_t4150672372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSUseExample::OnGUI()
extern "C"  void JSUseExample_OnGUI_m213107858 (JSUseExample_t4150672372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSUseExample::PlayerConnectd()
extern "C"  void JSUseExample_PlayerConnectd_m3018680901 (JSUseExample_t4150672372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSUseExample::PlayerDisconected()
extern "C"  void JSUseExample_PlayerDisconected_m1685781752 (JSUseExample_t4150672372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSUseExample::Main()
extern "C"  void JSUseExample_Main_m1751091899 (JSUseExample_t4150672372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
