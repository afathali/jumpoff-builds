﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator5
struct U3CPostTwitterScreenshotU3Ec__Iterator5_t2799471811;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator5::.ctor()
extern "C"  void U3CPostTwitterScreenshotU3Ec__Iterator5__ctor_m3699965316 (U3CPostTwitterScreenshotU3Ec__Iterator5_t2799471811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPostTwitterScreenshotU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4236232706 (U3CPostTwitterScreenshotU3Ec__Iterator5_t2799471811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPostTwitterScreenshotU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m2647607642 (U3CPostTwitterScreenshotU3Ec__Iterator5_t2799471811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator5::MoveNext()
extern "C"  bool U3CPostTwitterScreenshotU3Ec__Iterator5_MoveNext_m1572060788 (U3CPostTwitterScreenshotU3Ec__Iterator5_t2799471811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator5::Dispose()
extern "C"  void U3CPostTwitterScreenshotU3Ec__Iterator5_Dispose_m3230337003 (U3CPostTwitterScreenshotU3Ec__Iterator5_t2799471811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator5::Reset()
extern "C"  void U3CPostTwitterScreenshotU3Ec__Iterator5_Reset_m3337766489 (U3CPostTwitterScreenshotU3Ec__Iterator5_t2799471811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
