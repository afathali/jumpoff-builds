﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidInAppPurchaseManager
struct AndroidInAppPurchaseManager_t2155442111;
// System.Action`1<BillingResult>
struct Action_1_t3313641232;
// System.String
struct String_t;
// GoogleProductTemplate
struct GoogleProductTemplate_t1112616324;
// AndroidInventory
struct AndroidInventory_t701010211;
// BillingResult
struct BillingResult_t3511841850;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GoogleProductTemplate1112616324.h"
#include "AssemblyU2DCSharp_BillingResult3511841850.h"

// System.Void AndroidInAppPurchaseManager::.ctor()
extern "C"  void AndroidInAppPurchaseManager__ctor_m1884861802 (AndroidInAppPurchaseManager_t2155442111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::.cctor()
extern "C"  void AndroidInAppPurchaseManager__cctor_m2804190517 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::add_ActionProductPurchased(System.Action`1<BillingResult>)
extern "C"  void AndroidInAppPurchaseManager_add_ActionProductPurchased_m855461982 (Il2CppObject * __this /* static, unused */, Action_1_t3313641232 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::remove_ActionProductPurchased(System.Action`1<BillingResult>)
extern "C"  void AndroidInAppPurchaseManager_remove_ActionProductPurchased_m326076669 (Il2CppObject * __this /* static, unused */, Action_1_t3313641232 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::add_ActionProductConsumed(System.Action`1<BillingResult>)
extern "C"  void AndroidInAppPurchaseManager_add_ActionProductConsumed_m1852135897 (Il2CppObject * __this /* static, unused */, Action_1_t3313641232 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::remove_ActionProductConsumed(System.Action`1<BillingResult>)
extern "C"  void AndroidInAppPurchaseManager_remove_ActionProductConsumed_m2666864308 (Il2CppObject * __this /* static, unused */, Action_1_t3313641232 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::add_ActionBillingSetupFinished(System.Action`1<BillingResult>)
extern "C"  void AndroidInAppPurchaseManager_add_ActionBillingSetupFinished_m2403216044 (Il2CppObject * __this /* static, unused */, Action_1_t3313641232 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::remove_ActionBillingSetupFinished(System.Action`1<BillingResult>)
extern "C"  void AndroidInAppPurchaseManager_remove_ActionBillingSetupFinished_m256029795 (Il2CppObject * __this /* static, unused */, Action_1_t3313641232 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::add_ActionRetrieveProducsFinished(System.Action`1<BillingResult>)
extern "C"  void AndroidInAppPurchaseManager_add_ActionRetrieveProducsFinished_m2210911296 (Il2CppObject * __this /* static, unused */, Action_1_t3313641232 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::remove_ActionRetrieveProducsFinished(System.Action`1<BillingResult>)
extern "C"  void AndroidInAppPurchaseManager_remove_ActionRetrieveProducsFinished_m895491701 (Il2CppObject * __this /* static, unused */, Action_1_t3313641232 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::Awake()
extern "C"  void AndroidInAppPurchaseManager_Awake_m683953883 (AndroidInAppPurchaseManager_t2155442111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::addProduct(System.String)
extern "C"  void AndroidInAppPurchaseManager_addProduct_m1518413792 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::AddProduct(System.String)
extern "C"  void AndroidInAppPurchaseManager_AddProduct_m2068256192 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::AddProduct(GoogleProductTemplate)
extern "C"  void AndroidInAppPurchaseManager_AddProduct_m897658764 (AndroidInAppPurchaseManager_t2155442111 * __this, GoogleProductTemplate_t1112616324 * ___template0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::retrieveProducDetails()
extern "C"  void AndroidInAppPurchaseManager_retrieveProducDetails_m798130053 (AndroidInAppPurchaseManager_t2155442111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::RetrieveProducDetails()
extern "C"  void AndroidInAppPurchaseManager_RetrieveProducDetails_m677049957 (AndroidInAppPurchaseManager_t2155442111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::purchase(System.String)
extern "C"  void AndroidInAppPurchaseManager_purchase_m261597355 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::purchase(System.String,System.String)
extern "C"  void AndroidInAppPurchaseManager_purchase_m2721667689 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___SKU0, String_t* ___DeveloperPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::Purchase(System.String)
extern "C"  void AndroidInAppPurchaseManager_Purchase_m263113547 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::Purchase(System.String,System.String)
extern "C"  void AndroidInAppPurchaseManager_Purchase_m2982358537 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___SKU0, String_t* ___DeveloperPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::subscribe(System.String)
extern "C"  void AndroidInAppPurchaseManager_subscribe_m2002253816 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::subscribe(System.String,System.String)
extern "C"  void AndroidInAppPurchaseManager_subscribe_m4097670608 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___SKU0, String_t* ___DeveloperPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::Subscribe(System.String)
extern "C"  void AndroidInAppPurchaseManager_Subscribe_m1926307864 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::Subscribe(System.String,System.String)
extern "C"  void AndroidInAppPurchaseManager_Subscribe_m2772748784 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___SKU0, String_t* ___DeveloperPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::consume(System.String)
extern "C"  void AndroidInAppPurchaseManager_consume_m505221254 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::Consume(System.String)
extern "C"  void AndroidInAppPurchaseManager_Consume_m1089419494 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::loadStore()
extern "C"  void AndroidInAppPurchaseManager_loadStore_m3475776663 (AndroidInAppPurchaseManager_t2155442111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::loadStore(System.String)
extern "C"  void AndroidInAppPurchaseManager_loadStore_m1918598245 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___base64EncodedPublicKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::LoadStore()
extern "C"  void AndroidInAppPurchaseManager_LoadStore_m308521015 (AndroidInAppPurchaseManager_t2155442111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::LoadStore(System.String)
extern "C"  void AndroidInAppPurchaseManager_LoadStore_m1114498757 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___base64EncodedPublicKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AndroidInventory AndroidInAppPurchaseManager::get_inventory()
extern "C"  AndroidInventory_t701010211 * AndroidInAppPurchaseManager_get_inventory_m2145755555 (AndroidInAppPurchaseManager_t2155442111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AndroidInventory AndroidInAppPurchaseManager::get_Inventory()
extern "C"  AndroidInventory_t701010211 * AndroidInAppPurchaseManager_get_Inventory_m967097923 (AndroidInAppPurchaseManager_t2155442111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidInAppPurchaseManager::get_IsConnectingToServiceInProcess()
extern "C"  bool AndroidInAppPurchaseManager_get_IsConnectingToServiceInProcess_m320390521 (AndroidInAppPurchaseManager_t2155442111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidInAppPurchaseManager::get_IsProductRetrievingInProcess()
extern "C"  bool AndroidInAppPurchaseManager_get_IsProductRetrievingInProcess_m1528045605 (AndroidInAppPurchaseManager_t2155442111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidInAppPurchaseManager::get_IsConnected()
extern "C"  bool AndroidInAppPurchaseManager_get_IsConnected_m4176368160 (AndroidInAppPurchaseManager_t2155442111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidInAppPurchaseManager::get_IsInventoryLoaded()
extern "C"  bool AndroidInAppPurchaseManager_get_IsInventoryLoaded_m1289832442 (AndroidInAppPurchaseManager_t2155442111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::OnPurchaseFinishedCallback(System.String)
extern "C"  void AndroidInAppPurchaseManager_OnPurchaseFinishedCallback_m2509203993 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::OnConsumeFinishedCallBack(System.String)
extern "C"  void AndroidInAppPurchaseManager_OnConsumeFinishedCallBack_m3696437980 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::OnBillingSetupFinishedCallback(System.String)
extern "C"  void AndroidInAppPurchaseManager_OnBillingSetupFinishedCallback_m590805334 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::OnQueryInventoryFinishedCallBack(System.String)
extern "C"  void AndroidInAppPurchaseManager_OnQueryInventoryFinishedCallBack_m3727478570 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::OnPurchasesRecive(System.String)
extern "C"  void AndroidInAppPurchaseManager_OnPurchasesRecive_m186354407 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::OnProducttDetailsRecive(System.String)
extern "C"  void AndroidInAppPurchaseManager_OnProducttDetailsRecive_m154968202 (AndroidInAppPurchaseManager_t2155442111 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::<ActionProductPurchased>m__10(BillingResult)
extern "C"  void AndroidInAppPurchaseManager_U3CActionProductPurchasedU3Em__10_m2311017134 (Il2CppObject * __this /* static, unused */, BillingResult_t3511841850 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::<ActionProductConsumed>m__11(BillingResult)
extern "C"  void AndroidInAppPurchaseManager_U3CActionProductConsumedU3Em__11_m1239424902 (Il2CppObject * __this /* static, unused */, BillingResult_t3511841850 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::<ActionBillingSetupFinished>m__12(BillingResult)
extern "C"  void AndroidInAppPurchaseManager_U3CActionBillingSetupFinishedU3Em__12_m1977124242 (Il2CppObject * __this /* static, unused */, BillingResult_t3511841850 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInAppPurchaseManager::<ActionRetrieveProducsFinished>m__13(BillingResult)
extern "C"  void AndroidInAppPurchaseManager_U3CActionRetrieveProducsFinishedU3Em__13_m3085452487 (Il2CppObject * __this /* static, unused */, BillingResult_t3511841850 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
