﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NativeIOSActionsExample
struct NativeIOSActionsExample_t1779049155;
// IOSImagePickResult
struct IOSImagePickResult_t1671334394;
// SA.Common.Models.Result
struct Result_t4287219743;
// ISN_CheckUrlResult
struct ISN_CheckUrlResult_t1645724501;
// System.String
struct String_t;
// SA.IOSNative.Contacts.ContactsResult
struct ContactsResult_t1045731284;
// ISN_FilePickerResult
struct ISN_FilePickerResult_t2234803986;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSImagePickResult1671334394.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_CheckUrlResult1645724501.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_SwipeDirection768921696.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contact1045731284.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_FilePickerResult2234803986.h"

// System.Void NativeIOSActionsExample::.ctor()
extern "C"  void NativeIOSActionsExample__ctor_m3744364516 (NativeIOSActionsExample_t1779049155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::Awake()
extern "C"  void NativeIOSActionsExample_Awake_m2550773533 (NativeIOSActionsExample_t1779049155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::OnGUI()
extern "C"  void NativeIOSActionsExample_OnGUI_m3509332428 (NativeIOSActionsExample_t1779049155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::ShowDevoceInfo()
extern "C"  void NativeIOSActionsExample_ShowDevoceInfo_m1712581179 (NativeIOSActionsExample_t1779049155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::OnDateChanged(System.DateTime)
extern "C"  void NativeIOSActionsExample_OnDateChanged_m1745433205 (NativeIOSActionsExample_t1779049155 * __this, DateTime_t693205669  ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::OnPickerClosed(System.DateTime)
extern "C"  void NativeIOSActionsExample_OnPickerClosed_m630673135 (NativeIOSActionsExample_t1779049155 * __this, DateTime_t693205669  ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::OnImage(IOSImagePickResult)
extern "C"  void NativeIOSActionsExample_OnImage_m1823461060 (NativeIOSActionsExample_t1779049155 * __this, IOSImagePickResult_t1671334394 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::OnImageSaved(SA.Common.Models.Result)
extern "C"  void NativeIOSActionsExample_OnImageSaved_m3786196891 (NativeIOSActionsExample_t1779049155 * __this, Result_t4287219743 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::OnUrlCheckResultAction(ISN_CheckUrlResult)
extern "C"  void NativeIOSActionsExample_OnUrlCheckResultAction_m2696400778 (NativeIOSActionsExample_t1779049155 * __this, ISN_CheckUrlResult_t1645724501 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::OnAdvertisingIdentifierLoadedAction(System.String)
extern "C"  void NativeIOSActionsExample_OnAdvertisingIdentifierLoadedAction_m1048772347 (NativeIOSActionsExample_t1779049155 * __this, String_t* ___Identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::<Awake>m__73(ISN_SwipeDirection)
extern "C"  void NativeIOSActionsExample_U3CAwakeU3Em__73_m2580479904 (Il2CppObject * __this /* static, unused */, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::<OnGUI>m__74(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void NativeIOSActionsExample_U3COnGUIU3Em__74_m2003165367 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::<OnGUI>m__75(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void NativeIOSActionsExample_U3COnGUIU3Em__75_m608035288 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeIOSActionsExample::<OnGUI>m__76(ISN_FilePickerResult)
extern "C"  void NativeIOSActionsExample_U3COnGUIU3Em__76_m315407690 (NativeIOSActionsExample_t1779049155 * __this, ISN_FilePickerResult_t2234803986 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
