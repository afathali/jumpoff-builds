﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TVAppController
struct TVAppController_t1564329309;
// System.Action
struct Action_t3226471752;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TVAppController::.ctor()
extern "C"  void TVAppController__ctor_m3954677784 (TVAppController_t1564329309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TVAppController::.cctor()
extern "C"  void TVAppController__cctor_m1706883431 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TVAppController::add_DeviceTypeChecked(System.Action)
extern "C"  void TVAppController_add_DeviceTypeChecked_m21201802 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TVAppController::remove_DeviceTypeChecked(System.Action)
extern "C"  void TVAppController_remove_DeviceTypeChecked_m1736857379 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TVAppController::Awake()
extern "C"  void TVAppController_Awake_m3457950937 (TVAppController_t1564329309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TVAppController::CheckForATVDevice()
extern "C"  void TVAppController_CheckForATVDevice_m3460481458 (TVAppController_t1564329309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TVAppController::OnDeviceStateResponce(System.String)
extern "C"  void TVAppController_OnDeviceStateResponce_m444393851 (TVAppController_t1564329309 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TVAppController::get_IsRuningOnTVDevice()
extern "C"  bool TVAppController_get_IsRuningOnTVDevice_m3794960097 (TVAppController_t1564329309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TVAppController::<DeviceTypeChecked>m__23()
extern "C"  void TVAppController_U3CDeviceTypeCheckedU3Em__23_m2320723113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
