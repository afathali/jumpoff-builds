﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TWAPITest
struct TWAPITest_t1774637995;
// TW_APIRequstResult
struct TW_APIRequstResult_t455151055;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TW_APIRequstResult455151055.h"

// System.Void TWAPITest::.ctor()
extern "C"  void TWAPITest__ctor_m1839933738 (TWAPITest_t1774637995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TWAPITest::Start()
extern "C"  void TWAPITest_Start_m2260385478 (TWAPITest_t1774637995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TWAPITest::OnResult(TW_APIRequstResult)
extern "C"  void TWAPITest_OnResult_m2418480495 (TWAPITest_t1774637995 * __this, TW_APIRequstResult_t455151055 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
