﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidToast
struct AndroidToast_t1991890102;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AndroidToast::.ctor()
extern "C"  void AndroidToast__ctor_m1475669365 (AndroidToast_t1991890102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidToast::ShowToastNotification(System.String)
extern "C"  void AndroidToast_ShowToastNotification_m33264544 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidToast::ShowToastNotification(System.String,System.Int32)
extern "C"  void AndroidToast_ShowToastNotification_m2891628825 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
