﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,GK_Player>
struct Dictionary_2_t401820260;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<GK_LeaderboardSet>
struct List_1_t3669402526;
// System.Collections.Generic.Dictionary`2<System.Int32,GK_FriendRequest>
struct Dictionary_2_t3109446443;
// GK_Player
struct GK_Player_t2782008294;
// System.Action`1<SA.Common.Models.Result>
struct Action_1_t4089019125;
// System.Action`1<GK_LeaderboardResult>
struct Action_1_t667880215;
// System.Action`1<GK_AchievementProgressResult>
struct Action_1_t3341373734;
// System.Action
struct Action_t3226471752;
// System.Action`1<GK_UserInfoLoadResult>
struct Action_1_t979640615;
// System.Action`1<GK_PlayerSignatureResult>
struct Action_1_t4110536157;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCenterManager
struct  GameCenterManager_t1487113918  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct GameCenterManager_t1487113918_StaticFields
{
public:
	// System.Boolean GameCenterManager::_IsInitialized
	bool ____IsInitialized_3;
	// System.Boolean GameCenterManager::_IsAchievementsInfoLoaded
	bool ____IsAchievementsInfoLoaded_4;
	// System.Collections.Generic.Dictionary`2<System.String,GK_Player> GameCenterManager::_players
	Dictionary_2_t401820260 * ____players_5;
	// System.Collections.Generic.List`1<System.String> GameCenterManager::_friendsList
	List_1_t1398341365 * ____friendsList_6;
	// System.Collections.Generic.List`1<GK_LeaderboardSet> GameCenterManager::_LeaderboardSets
	List_1_t3669402526 * ____LeaderboardSets_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,GK_FriendRequest> GameCenterManager::_FriendRequests
	Dictionary_2_t3109446443 * ____FriendRequests_8;
	// GK_Player GameCenterManager::_player
	GK_Player_t2782008294 * ____player_9;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::OnAuthFinished
	Action_1_t4089019125 * ___OnAuthFinished_10;
	// System.Action`1<GK_LeaderboardResult> GameCenterManager::OnScoreSubmitted
	Action_1_t667880215 * ___OnScoreSubmitted_11;
	// System.Action`1<GK_LeaderboardResult> GameCenterManager::OnScoresListLoaded
	Action_1_t667880215 * ___OnScoresListLoaded_12;
	// System.Action`1<GK_LeaderboardResult> GameCenterManager::OnLeadrboardInfoLoaded
	Action_1_t667880215 * ___OnLeadrboardInfoLoaded_13;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::OnLeaderboardSetsInfoLoaded
	Action_1_t4089019125 * ___OnLeaderboardSetsInfoLoaded_14;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::OnAchievementsReset
	Action_1_t4089019125 * ___OnAchievementsReset_15;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::OnAchievementsLoaded
	Action_1_t4089019125 * ___OnAchievementsLoaded_16;
	// System.Action`1<GK_AchievementProgressResult> GameCenterManager::OnAchievementsProgress
	Action_1_t3341373734 * ___OnAchievementsProgress_17;
	// System.Action GameCenterManager::OnGameCenterViewDismissed
	Action_t3226471752 * ___OnGameCenterViewDismissed_18;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::OnFriendsListLoaded
	Action_1_t4089019125 * ___OnFriendsListLoaded_19;
	// System.Action`1<GK_UserInfoLoadResult> GameCenterManager::OnUserInfoLoaded
	Action_1_t979640615 * ___OnUserInfoLoaded_20;
	// System.Action`1<GK_PlayerSignatureResult> GameCenterManager::OnPlayerSignatureRetrieveResult
	Action_1_t4110536157 * ___OnPlayerSignatureRetrieveResult_21;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::<>f__am$cache13
	Action_1_t4089019125 * ___U3CU3Ef__amU24cache13_22;
	// System.Action`1<GK_LeaderboardResult> GameCenterManager::<>f__am$cache14
	Action_1_t667880215 * ___U3CU3Ef__amU24cache14_23;
	// System.Action`1<GK_LeaderboardResult> GameCenterManager::<>f__am$cache15
	Action_1_t667880215 * ___U3CU3Ef__amU24cache15_24;
	// System.Action`1<GK_LeaderboardResult> GameCenterManager::<>f__am$cache16
	Action_1_t667880215 * ___U3CU3Ef__amU24cache16_25;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::<>f__am$cache17
	Action_1_t4089019125 * ___U3CU3Ef__amU24cache17_26;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::<>f__am$cache18
	Action_1_t4089019125 * ___U3CU3Ef__amU24cache18_27;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::<>f__am$cache19
	Action_1_t4089019125 * ___U3CU3Ef__amU24cache19_28;
	// System.Action`1<GK_AchievementProgressResult> GameCenterManager::<>f__am$cache1A
	Action_1_t3341373734 * ___U3CU3Ef__amU24cache1A_29;
	// System.Action GameCenterManager::<>f__am$cache1B
	Action_t3226471752 * ___U3CU3Ef__amU24cache1B_30;
	// System.Action`1<SA.Common.Models.Result> GameCenterManager::<>f__am$cache1C
	Action_1_t4089019125 * ___U3CU3Ef__amU24cache1C_31;
	// System.Action`1<GK_UserInfoLoadResult> GameCenterManager::<>f__am$cache1D
	Action_1_t979640615 * ___U3CU3Ef__amU24cache1D_32;
	// System.Action`1<GK_PlayerSignatureResult> GameCenterManager::<>f__am$cache1E
	Action_1_t4110536157 * ___U3CU3Ef__amU24cache1E_33;

public:
	inline static int32_t get_offset_of__IsInitialized_3() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ____IsInitialized_3)); }
	inline bool get__IsInitialized_3() const { return ____IsInitialized_3; }
	inline bool* get_address_of__IsInitialized_3() { return &____IsInitialized_3; }
	inline void set__IsInitialized_3(bool value)
	{
		____IsInitialized_3 = value;
	}

	inline static int32_t get_offset_of__IsAchievementsInfoLoaded_4() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ____IsAchievementsInfoLoaded_4)); }
	inline bool get__IsAchievementsInfoLoaded_4() const { return ____IsAchievementsInfoLoaded_4; }
	inline bool* get_address_of__IsAchievementsInfoLoaded_4() { return &____IsAchievementsInfoLoaded_4; }
	inline void set__IsAchievementsInfoLoaded_4(bool value)
	{
		____IsAchievementsInfoLoaded_4 = value;
	}

	inline static int32_t get_offset_of__players_5() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ____players_5)); }
	inline Dictionary_2_t401820260 * get__players_5() const { return ____players_5; }
	inline Dictionary_2_t401820260 ** get_address_of__players_5() { return &____players_5; }
	inline void set__players_5(Dictionary_2_t401820260 * value)
	{
		____players_5 = value;
		Il2CppCodeGenWriteBarrier(&____players_5, value);
	}

	inline static int32_t get_offset_of__friendsList_6() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ____friendsList_6)); }
	inline List_1_t1398341365 * get__friendsList_6() const { return ____friendsList_6; }
	inline List_1_t1398341365 ** get_address_of__friendsList_6() { return &____friendsList_6; }
	inline void set__friendsList_6(List_1_t1398341365 * value)
	{
		____friendsList_6 = value;
		Il2CppCodeGenWriteBarrier(&____friendsList_6, value);
	}

	inline static int32_t get_offset_of__LeaderboardSets_7() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ____LeaderboardSets_7)); }
	inline List_1_t3669402526 * get__LeaderboardSets_7() const { return ____LeaderboardSets_7; }
	inline List_1_t3669402526 ** get_address_of__LeaderboardSets_7() { return &____LeaderboardSets_7; }
	inline void set__LeaderboardSets_7(List_1_t3669402526 * value)
	{
		____LeaderboardSets_7 = value;
		Il2CppCodeGenWriteBarrier(&____LeaderboardSets_7, value);
	}

	inline static int32_t get_offset_of__FriendRequests_8() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ____FriendRequests_8)); }
	inline Dictionary_2_t3109446443 * get__FriendRequests_8() const { return ____FriendRequests_8; }
	inline Dictionary_2_t3109446443 ** get_address_of__FriendRequests_8() { return &____FriendRequests_8; }
	inline void set__FriendRequests_8(Dictionary_2_t3109446443 * value)
	{
		____FriendRequests_8 = value;
		Il2CppCodeGenWriteBarrier(&____FriendRequests_8, value);
	}

	inline static int32_t get_offset_of__player_9() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ____player_9)); }
	inline GK_Player_t2782008294 * get__player_9() const { return ____player_9; }
	inline GK_Player_t2782008294 ** get_address_of__player_9() { return &____player_9; }
	inline void set__player_9(GK_Player_t2782008294 * value)
	{
		____player_9 = value;
		Il2CppCodeGenWriteBarrier(&____player_9, value);
	}

	inline static int32_t get_offset_of_OnAuthFinished_10() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___OnAuthFinished_10)); }
	inline Action_1_t4089019125 * get_OnAuthFinished_10() const { return ___OnAuthFinished_10; }
	inline Action_1_t4089019125 ** get_address_of_OnAuthFinished_10() { return &___OnAuthFinished_10; }
	inline void set_OnAuthFinished_10(Action_1_t4089019125 * value)
	{
		___OnAuthFinished_10 = value;
		Il2CppCodeGenWriteBarrier(&___OnAuthFinished_10, value);
	}

	inline static int32_t get_offset_of_OnScoreSubmitted_11() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___OnScoreSubmitted_11)); }
	inline Action_1_t667880215 * get_OnScoreSubmitted_11() const { return ___OnScoreSubmitted_11; }
	inline Action_1_t667880215 ** get_address_of_OnScoreSubmitted_11() { return &___OnScoreSubmitted_11; }
	inline void set_OnScoreSubmitted_11(Action_1_t667880215 * value)
	{
		___OnScoreSubmitted_11 = value;
		Il2CppCodeGenWriteBarrier(&___OnScoreSubmitted_11, value);
	}

	inline static int32_t get_offset_of_OnScoresListLoaded_12() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___OnScoresListLoaded_12)); }
	inline Action_1_t667880215 * get_OnScoresListLoaded_12() const { return ___OnScoresListLoaded_12; }
	inline Action_1_t667880215 ** get_address_of_OnScoresListLoaded_12() { return &___OnScoresListLoaded_12; }
	inline void set_OnScoresListLoaded_12(Action_1_t667880215 * value)
	{
		___OnScoresListLoaded_12 = value;
		Il2CppCodeGenWriteBarrier(&___OnScoresListLoaded_12, value);
	}

	inline static int32_t get_offset_of_OnLeadrboardInfoLoaded_13() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___OnLeadrboardInfoLoaded_13)); }
	inline Action_1_t667880215 * get_OnLeadrboardInfoLoaded_13() const { return ___OnLeadrboardInfoLoaded_13; }
	inline Action_1_t667880215 ** get_address_of_OnLeadrboardInfoLoaded_13() { return &___OnLeadrboardInfoLoaded_13; }
	inline void set_OnLeadrboardInfoLoaded_13(Action_1_t667880215 * value)
	{
		___OnLeadrboardInfoLoaded_13 = value;
		Il2CppCodeGenWriteBarrier(&___OnLeadrboardInfoLoaded_13, value);
	}

	inline static int32_t get_offset_of_OnLeaderboardSetsInfoLoaded_14() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___OnLeaderboardSetsInfoLoaded_14)); }
	inline Action_1_t4089019125 * get_OnLeaderboardSetsInfoLoaded_14() const { return ___OnLeaderboardSetsInfoLoaded_14; }
	inline Action_1_t4089019125 ** get_address_of_OnLeaderboardSetsInfoLoaded_14() { return &___OnLeaderboardSetsInfoLoaded_14; }
	inline void set_OnLeaderboardSetsInfoLoaded_14(Action_1_t4089019125 * value)
	{
		___OnLeaderboardSetsInfoLoaded_14 = value;
		Il2CppCodeGenWriteBarrier(&___OnLeaderboardSetsInfoLoaded_14, value);
	}

	inline static int32_t get_offset_of_OnAchievementsReset_15() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___OnAchievementsReset_15)); }
	inline Action_1_t4089019125 * get_OnAchievementsReset_15() const { return ___OnAchievementsReset_15; }
	inline Action_1_t4089019125 ** get_address_of_OnAchievementsReset_15() { return &___OnAchievementsReset_15; }
	inline void set_OnAchievementsReset_15(Action_1_t4089019125 * value)
	{
		___OnAchievementsReset_15 = value;
		Il2CppCodeGenWriteBarrier(&___OnAchievementsReset_15, value);
	}

	inline static int32_t get_offset_of_OnAchievementsLoaded_16() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___OnAchievementsLoaded_16)); }
	inline Action_1_t4089019125 * get_OnAchievementsLoaded_16() const { return ___OnAchievementsLoaded_16; }
	inline Action_1_t4089019125 ** get_address_of_OnAchievementsLoaded_16() { return &___OnAchievementsLoaded_16; }
	inline void set_OnAchievementsLoaded_16(Action_1_t4089019125 * value)
	{
		___OnAchievementsLoaded_16 = value;
		Il2CppCodeGenWriteBarrier(&___OnAchievementsLoaded_16, value);
	}

	inline static int32_t get_offset_of_OnAchievementsProgress_17() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___OnAchievementsProgress_17)); }
	inline Action_1_t3341373734 * get_OnAchievementsProgress_17() const { return ___OnAchievementsProgress_17; }
	inline Action_1_t3341373734 ** get_address_of_OnAchievementsProgress_17() { return &___OnAchievementsProgress_17; }
	inline void set_OnAchievementsProgress_17(Action_1_t3341373734 * value)
	{
		___OnAchievementsProgress_17 = value;
		Il2CppCodeGenWriteBarrier(&___OnAchievementsProgress_17, value);
	}

	inline static int32_t get_offset_of_OnGameCenterViewDismissed_18() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___OnGameCenterViewDismissed_18)); }
	inline Action_t3226471752 * get_OnGameCenterViewDismissed_18() const { return ___OnGameCenterViewDismissed_18; }
	inline Action_t3226471752 ** get_address_of_OnGameCenterViewDismissed_18() { return &___OnGameCenterViewDismissed_18; }
	inline void set_OnGameCenterViewDismissed_18(Action_t3226471752 * value)
	{
		___OnGameCenterViewDismissed_18 = value;
		Il2CppCodeGenWriteBarrier(&___OnGameCenterViewDismissed_18, value);
	}

	inline static int32_t get_offset_of_OnFriendsListLoaded_19() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___OnFriendsListLoaded_19)); }
	inline Action_1_t4089019125 * get_OnFriendsListLoaded_19() const { return ___OnFriendsListLoaded_19; }
	inline Action_1_t4089019125 ** get_address_of_OnFriendsListLoaded_19() { return &___OnFriendsListLoaded_19; }
	inline void set_OnFriendsListLoaded_19(Action_1_t4089019125 * value)
	{
		___OnFriendsListLoaded_19 = value;
		Il2CppCodeGenWriteBarrier(&___OnFriendsListLoaded_19, value);
	}

	inline static int32_t get_offset_of_OnUserInfoLoaded_20() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___OnUserInfoLoaded_20)); }
	inline Action_1_t979640615 * get_OnUserInfoLoaded_20() const { return ___OnUserInfoLoaded_20; }
	inline Action_1_t979640615 ** get_address_of_OnUserInfoLoaded_20() { return &___OnUserInfoLoaded_20; }
	inline void set_OnUserInfoLoaded_20(Action_1_t979640615 * value)
	{
		___OnUserInfoLoaded_20 = value;
		Il2CppCodeGenWriteBarrier(&___OnUserInfoLoaded_20, value);
	}

	inline static int32_t get_offset_of_OnPlayerSignatureRetrieveResult_21() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___OnPlayerSignatureRetrieveResult_21)); }
	inline Action_1_t4110536157 * get_OnPlayerSignatureRetrieveResult_21() const { return ___OnPlayerSignatureRetrieveResult_21; }
	inline Action_1_t4110536157 ** get_address_of_OnPlayerSignatureRetrieveResult_21() { return &___OnPlayerSignatureRetrieveResult_21; }
	inline void set_OnPlayerSignatureRetrieveResult_21(Action_1_t4110536157 * value)
	{
		___OnPlayerSignatureRetrieveResult_21 = value;
		Il2CppCodeGenWriteBarrier(&___OnPlayerSignatureRetrieveResult_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache13_22() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___U3CU3Ef__amU24cache13_22)); }
	inline Action_1_t4089019125 * get_U3CU3Ef__amU24cache13_22() const { return ___U3CU3Ef__amU24cache13_22; }
	inline Action_1_t4089019125 ** get_address_of_U3CU3Ef__amU24cache13_22() { return &___U3CU3Ef__amU24cache13_22; }
	inline void set_U3CU3Ef__amU24cache13_22(Action_1_t4089019125 * value)
	{
		___U3CU3Ef__amU24cache13_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache13_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_23() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___U3CU3Ef__amU24cache14_23)); }
	inline Action_1_t667880215 * get_U3CU3Ef__amU24cache14_23() const { return ___U3CU3Ef__amU24cache14_23; }
	inline Action_1_t667880215 ** get_address_of_U3CU3Ef__amU24cache14_23() { return &___U3CU3Ef__amU24cache14_23; }
	inline void set_U3CU3Ef__amU24cache14_23(Action_1_t667880215 * value)
	{
		___U3CU3Ef__amU24cache14_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache14_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_24() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___U3CU3Ef__amU24cache15_24)); }
	inline Action_1_t667880215 * get_U3CU3Ef__amU24cache15_24() const { return ___U3CU3Ef__amU24cache15_24; }
	inline Action_1_t667880215 ** get_address_of_U3CU3Ef__amU24cache15_24() { return &___U3CU3Ef__amU24cache15_24; }
	inline void set_U3CU3Ef__amU24cache15_24(Action_1_t667880215 * value)
	{
		___U3CU3Ef__amU24cache15_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache15_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache16_25() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___U3CU3Ef__amU24cache16_25)); }
	inline Action_1_t667880215 * get_U3CU3Ef__amU24cache16_25() const { return ___U3CU3Ef__amU24cache16_25; }
	inline Action_1_t667880215 ** get_address_of_U3CU3Ef__amU24cache16_25() { return &___U3CU3Ef__amU24cache16_25; }
	inline void set_U3CU3Ef__amU24cache16_25(Action_1_t667880215 * value)
	{
		___U3CU3Ef__amU24cache16_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache16_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache17_26() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___U3CU3Ef__amU24cache17_26)); }
	inline Action_1_t4089019125 * get_U3CU3Ef__amU24cache17_26() const { return ___U3CU3Ef__amU24cache17_26; }
	inline Action_1_t4089019125 ** get_address_of_U3CU3Ef__amU24cache17_26() { return &___U3CU3Ef__amU24cache17_26; }
	inline void set_U3CU3Ef__amU24cache17_26(Action_1_t4089019125 * value)
	{
		___U3CU3Ef__amU24cache17_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache17_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache18_27() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___U3CU3Ef__amU24cache18_27)); }
	inline Action_1_t4089019125 * get_U3CU3Ef__amU24cache18_27() const { return ___U3CU3Ef__amU24cache18_27; }
	inline Action_1_t4089019125 ** get_address_of_U3CU3Ef__amU24cache18_27() { return &___U3CU3Ef__amU24cache18_27; }
	inline void set_U3CU3Ef__amU24cache18_27(Action_1_t4089019125 * value)
	{
		___U3CU3Ef__amU24cache18_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache18_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache19_28() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___U3CU3Ef__amU24cache19_28)); }
	inline Action_1_t4089019125 * get_U3CU3Ef__amU24cache19_28() const { return ___U3CU3Ef__amU24cache19_28; }
	inline Action_1_t4089019125 ** get_address_of_U3CU3Ef__amU24cache19_28() { return &___U3CU3Ef__amU24cache19_28; }
	inline void set_U3CU3Ef__amU24cache19_28(Action_1_t4089019125 * value)
	{
		___U3CU3Ef__amU24cache19_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache19_28, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1A_29() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___U3CU3Ef__amU24cache1A_29)); }
	inline Action_1_t3341373734 * get_U3CU3Ef__amU24cache1A_29() const { return ___U3CU3Ef__amU24cache1A_29; }
	inline Action_1_t3341373734 ** get_address_of_U3CU3Ef__amU24cache1A_29() { return &___U3CU3Ef__amU24cache1A_29; }
	inline void set_U3CU3Ef__amU24cache1A_29(Action_1_t3341373734 * value)
	{
		___U3CU3Ef__amU24cache1A_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1A_29, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1B_30() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___U3CU3Ef__amU24cache1B_30)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache1B_30() const { return ___U3CU3Ef__amU24cache1B_30; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache1B_30() { return &___U3CU3Ef__amU24cache1B_30; }
	inline void set_U3CU3Ef__amU24cache1B_30(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache1B_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1B_30, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_31() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___U3CU3Ef__amU24cache1C_31)); }
	inline Action_1_t4089019125 * get_U3CU3Ef__amU24cache1C_31() const { return ___U3CU3Ef__amU24cache1C_31; }
	inline Action_1_t4089019125 ** get_address_of_U3CU3Ef__amU24cache1C_31() { return &___U3CU3Ef__amU24cache1C_31; }
	inline void set_U3CU3Ef__amU24cache1C_31(Action_1_t4089019125 * value)
	{
		___U3CU3Ef__amU24cache1C_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1C_31, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1D_32() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___U3CU3Ef__amU24cache1D_32)); }
	inline Action_1_t979640615 * get_U3CU3Ef__amU24cache1D_32() const { return ___U3CU3Ef__amU24cache1D_32; }
	inline Action_1_t979640615 ** get_address_of_U3CU3Ef__amU24cache1D_32() { return &___U3CU3Ef__amU24cache1D_32; }
	inline void set_U3CU3Ef__amU24cache1D_32(Action_1_t979640615 * value)
	{
		___U3CU3Ef__amU24cache1D_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1D_32, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1E_33() { return static_cast<int32_t>(offsetof(GameCenterManager_t1487113918_StaticFields, ___U3CU3Ef__amU24cache1E_33)); }
	inline Action_1_t4110536157 * get_U3CU3Ef__amU24cache1E_33() const { return ___U3CU3Ef__amU24cache1E_33; }
	inline Action_1_t4110536157 ** get_address_of_U3CU3Ef__amU24cache1E_33() { return &___U3CU3Ef__amU24cache1E_33; }
	inline void set_U3CU3Ef__amU24cache1E_33(Action_1_t4110536157 * value)
	{
		___U3CU3Ef__amU24cache1E_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1E_33, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
