﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23387117823.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3533396040_gshared (KeyValuePair_2_t3387117823 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3533396040(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3387117823 *, Il2CppObject *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m3533396040_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m372290202_gshared (KeyValuePair_2_t3387117823 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m372290202(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3387117823 *, const MethodInfo*))KeyValuePair_2_get_Key_m372290202_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2809222207_gshared (KeyValuePair_2_t3387117823 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2809222207(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3387117823 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m2809222207_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3683399194_gshared (KeyValuePair_2_t3387117823 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3683399194(__this, method) ((  int32_t (*) (KeyValuePair_2_t3387117823 *, const MethodInfo*))KeyValuePair_2_get_Value_m3683399194_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1219699439_gshared (KeyValuePair_2_t3387117823 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1219699439(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3387117823 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1219699439_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3225376289_gshared (KeyValuePair_2_t3387117823 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3225376289(__this, method) ((  String_t* (*) (KeyValuePair_2_t3387117823 *, const MethodInfo*))KeyValuePair_2_ToString_m3225376289_gshared)(__this, method)
