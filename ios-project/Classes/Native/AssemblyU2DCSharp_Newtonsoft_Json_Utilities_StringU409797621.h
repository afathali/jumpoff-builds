﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringUtils/<Indent>c__AnonStorey37
struct  U3CIndentU3Ec__AnonStorey37_t409797621  : public Il2CppObject
{
public:
	// System.Char Newtonsoft.Json.Utilities.StringUtils/<Indent>c__AnonStorey37::indentChar
	Il2CppChar ___indentChar_0;
	// System.Int32 Newtonsoft.Json.Utilities.StringUtils/<Indent>c__AnonStorey37::indentation
	int32_t ___indentation_1;

public:
	inline static int32_t get_offset_of_indentChar_0() { return static_cast<int32_t>(offsetof(U3CIndentU3Ec__AnonStorey37_t409797621, ___indentChar_0)); }
	inline Il2CppChar get_indentChar_0() const { return ___indentChar_0; }
	inline Il2CppChar* get_address_of_indentChar_0() { return &___indentChar_0; }
	inline void set_indentChar_0(Il2CppChar value)
	{
		___indentChar_0 = value;
	}

	inline static int32_t get_offset_of_indentation_1() { return static_cast<int32_t>(offsetof(U3CIndentU3Ec__AnonStorey37_t409797621, ___indentation_1)); }
	inline int32_t get_indentation_1() const { return ___indentation_1; }
	inline int32_t* get_address_of_indentation_1() { return &___indentation_1; }
	inline void set_indentation_1(int32_t value)
	{
		___indentation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
