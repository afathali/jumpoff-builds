﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]>
struct Dictionary_2_t2405159648;
// System.Action`1<GoogleCloudResult>
struct Action_1_t545428089;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen2499348743.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleCloudManager
struct  GoogleCloudManager_t1208504825  : public SA_Singleton_OLD_1_t2499348743
{
public:
	// System.Int32 GoogleCloudManager::_maxStateSize
	int32_t ____maxStateSize_4;
	// System.Int32 GoogleCloudManager::_maxNumKeys
	int32_t ____maxNumKeys_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]> GoogleCloudManager::_states
	Dictionary_2_t2405159648 * ____states_6;

public:
	inline static int32_t get_offset_of__maxStateSize_4() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825, ____maxStateSize_4)); }
	inline int32_t get__maxStateSize_4() const { return ____maxStateSize_4; }
	inline int32_t* get_address_of__maxStateSize_4() { return &____maxStateSize_4; }
	inline void set__maxStateSize_4(int32_t value)
	{
		____maxStateSize_4 = value;
	}

	inline static int32_t get_offset_of__maxNumKeys_5() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825, ____maxNumKeys_5)); }
	inline int32_t get__maxNumKeys_5() const { return ____maxNumKeys_5; }
	inline int32_t* get_address_of__maxNumKeys_5() { return &____maxNumKeys_5; }
	inline void set__maxNumKeys_5(int32_t value)
	{
		____maxNumKeys_5 = value;
	}

	inline static int32_t get_offset_of__states_6() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825, ____states_6)); }
	inline Dictionary_2_t2405159648 * get__states_6() const { return ____states_6; }
	inline Dictionary_2_t2405159648 ** get_address_of__states_6() { return &____states_6; }
	inline void set__states_6(Dictionary_2_t2405159648 * value)
	{
		____states_6 = value;
		Il2CppCodeGenWriteBarrier(&____states_6, value);
	}
};

struct GoogleCloudManager_t1208504825_StaticFields
{
public:
	// System.Action`1<GoogleCloudResult> GoogleCloudManager::ActionStateDeleted
	Action_1_t545428089 * ___ActionStateDeleted_7;
	// System.Action`1<GoogleCloudResult> GoogleCloudManager::ActionStateUpdated
	Action_1_t545428089 * ___ActionStateUpdated_8;
	// System.Action`1<GoogleCloudResult> GoogleCloudManager::ActionStateLoaded
	Action_1_t545428089 * ___ActionStateLoaded_9;
	// System.Action`1<GoogleCloudResult> GoogleCloudManager::ActionStateResolved
	Action_1_t545428089 * ___ActionStateResolved_10;
	// System.Action`1<GoogleCloudResult> GoogleCloudManager::ActionStateConflict
	Action_1_t545428089 * ___ActionStateConflict_11;
	// System.Action`1<GoogleCloudResult> GoogleCloudManager::ActionAllStatesLoaded
	Action_1_t545428089 * ___ActionAllStatesLoaded_12;
	// System.Action`1<GoogleCloudResult> GoogleCloudManager::<>f__am$cache9
	Action_1_t545428089 * ___U3CU3Ef__amU24cache9_13;
	// System.Action`1<GoogleCloudResult> GoogleCloudManager::<>f__am$cacheA
	Action_1_t545428089 * ___U3CU3Ef__amU24cacheA_14;
	// System.Action`1<GoogleCloudResult> GoogleCloudManager::<>f__am$cacheB
	Action_1_t545428089 * ___U3CU3Ef__amU24cacheB_15;
	// System.Action`1<GoogleCloudResult> GoogleCloudManager::<>f__am$cacheC
	Action_1_t545428089 * ___U3CU3Ef__amU24cacheC_16;
	// System.Action`1<GoogleCloudResult> GoogleCloudManager::<>f__am$cacheD
	Action_1_t545428089 * ___U3CU3Ef__amU24cacheD_17;
	// System.Action`1<GoogleCloudResult> GoogleCloudManager::<>f__am$cacheE
	Action_1_t545428089 * ___U3CU3Ef__amU24cacheE_18;

public:
	inline static int32_t get_offset_of_ActionStateDeleted_7() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825_StaticFields, ___ActionStateDeleted_7)); }
	inline Action_1_t545428089 * get_ActionStateDeleted_7() const { return ___ActionStateDeleted_7; }
	inline Action_1_t545428089 ** get_address_of_ActionStateDeleted_7() { return &___ActionStateDeleted_7; }
	inline void set_ActionStateDeleted_7(Action_1_t545428089 * value)
	{
		___ActionStateDeleted_7 = value;
		Il2CppCodeGenWriteBarrier(&___ActionStateDeleted_7, value);
	}

	inline static int32_t get_offset_of_ActionStateUpdated_8() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825_StaticFields, ___ActionStateUpdated_8)); }
	inline Action_1_t545428089 * get_ActionStateUpdated_8() const { return ___ActionStateUpdated_8; }
	inline Action_1_t545428089 ** get_address_of_ActionStateUpdated_8() { return &___ActionStateUpdated_8; }
	inline void set_ActionStateUpdated_8(Action_1_t545428089 * value)
	{
		___ActionStateUpdated_8 = value;
		Il2CppCodeGenWriteBarrier(&___ActionStateUpdated_8, value);
	}

	inline static int32_t get_offset_of_ActionStateLoaded_9() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825_StaticFields, ___ActionStateLoaded_9)); }
	inline Action_1_t545428089 * get_ActionStateLoaded_9() const { return ___ActionStateLoaded_9; }
	inline Action_1_t545428089 ** get_address_of_ActionStateLoaded_9() { return &___ActionStateLoaded_9; }
	inline void set_ActionStateLoaded_9(Action_1_t545428089 * value)
	{
		___ActionStateLoaded_9 = value;
		Il2CppCodeGenWriteBarrier(&___ActionStateLoaded_9, value);
	}

	inline static int32_t get_offset_of_ActionStateResolved_10() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825_StaticFields, ___ActionStateResolved_10)); }
	inline Action_1_t545428089 * get_ActionStateResolved_10() const { return ___ActionStateResolved_10; }
	inline Action_1_t545428089 ** get_address_of_ActionStateResolved_10() { return &___ActionStateResolved_10; }
	inline void set_ActionStateResolved_10(Action_1_t545428089 * value)
	{
		___ActionStateResolved_10 = value;
		Il2CppCodeGenWriteBarrier(&___ActionStateResolved_10, value);
	}

	inline static int32_t get_offset_of_ActionStateConflict_11() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825_StaticFields, ___ActionStateConflict_11)); }
	inline Action_1_t545428089 * get_ActionStateConflict_11() const { return ___ActionStateConflict_11; }
	inline Action_1_t545428089 ** get_address_of_ActionStateConflict_11() { return &___ActionStateConflict_11; }
	inline void set_ActionStateConflict_11(Action_1_t545428089 * value)
	{
		___ActionStateConflict_11 = value;
		Il2CppCodeGenWriteBarrier(&___ActionStateConflict_11, value);
	}

	inline static int32_t get_offset_of_ActionAllStatesLoaded_12() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825_StaticFields, ___ActionAllStatesLoaded_12)); }
	inline Action_1_t545428089 * get_ActionAllStatesLoaded_12() const { return ___ActionAllStatesLoaded_12; }
	inline Action_1_t545428089 ** get_address_of_ActionAllStatesLoaded_12() { return &___ActionAllStatesLoaded_12; }
	inline void set_ActionAllStatesLoaded_12(Action_1_t545428089 * value)
	{
		___ActionAllStatesLoaded_12 = value;
		Il2CppCodeGenWriteBarrier(&___ActionAllStatesLoaded_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_13() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825_StaticFields, ___U3CU3Ef__amU24cache9_13)); }
	inline Action_1_t545428089 * get_U3CU3Ef__amU24cache9_13() const { return ___U3CU3Ef__amU24cache9_13; }
	inline Action_1_t545428089 ** get_address_of_U3CU3Ef__amU24cache9_13() { return &___U3CU3Ef__amU24cache9_13; }
	inline void set_U3CU3Ef__amU24cache9_13(Action_1_t545428089 * value)
	{
		___U3CU3Ef__amU24cache9_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_14() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825_StaticFields, ___U3CU3Ef__amU24cacheA_14)); }
	inline Action_1_t545428089 * get_U3CU3Ef__amU24cacheA_14() const { return ___U3CU3Ef__amU24cacheA_14; }
	inline Action_1_t545428089 ** get_address_of_U3CU3Ef__amU24cacheA_14() { return &___U3CU3Ef__amU24cacheA_14; }
	inline void set_U3CU3Ef__amU24cacheA_14(Action_1_t545428089 * value)
	{
		___U3CU3Ef__amU24cacheA_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Action_1_t545428089 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Action_1_t545428089 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Action_1_t545428089 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_16() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825_StaticFields, ___U3CU3Ef__amU24cacheC_16)); }
	inline Action_1_t545428089 * get_U3CU3Ef__amU24cacheC_16() const { return ___U3CU3Ef__amU24cacheC_16; }
	inline Action_1_t545428089 ** get_address_of_U3CU3Ef__amU24cacheC_16() { return &___U3CU3Ef__amU24cacheC_16; }
	inline void set_U3CU3Ef__amU24cacheC_16(Action_1_t545428089 * value)
	{
		___U3CU3Ef__amU24cacheC_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_17() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825_StaticFields, ___U3CU3Ef__amU24cacheD_17)); }
	inline Action_1_t545428089 * get_U3CU3Ef__amU24cacheD_17() const { return ___U3CU3Ef__amU24cacheD_17; }
	inline Action_1_t545428089 ** get_address_of_U3CU3Ef__amU24cacheD_17() { return &___U3CU3Ef__amU24cacheD_17; }
	inline void set_U3CU3Ef__amU24cacheD_17(Action_1_t545428089 * value)
	{
		___U3CU3Ef__amU24cacheD_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_18() { return static_cast<int32_t>(offsetof(GoogleCloudManager_t1208504825_StaticFields, ___U3CU3Ef__amU24cacheE_18)); }
	inline Action_1_t545428089 * get_U3CU3Ef__amU24cacheE_18() const { return ___U3CU3Ef__amU24cacheE_18; }
	inline Action_1_t545428089 ** get_address_of_U3CU3Ef__amU24cacheE_18() { return &___U3CU3Ef__amU24cacheE_18; }
	inline void set_U3CU3Ef__amU24cacheE_18(Action_1_t545428089 * value)
	{
		___U3CU3Ef__amU24cacheE_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
