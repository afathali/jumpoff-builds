﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>
struct EnumValue_1_t589082027;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>::.ctor(System.String,T)
extern "C"  void EnumValue_1__ctor_m1800650159_gshared (EnumValue_1_t589082027 * __this, String_t* ___name0, int64_t ___value1, const MethodInfo* method);
#define EnumValue_1__ctor_m1800650159(__this, ___name0, ___value1, method) ((  void (*) (EnumValue_1_t589082027 *, String_t*, int64_t, const MethodInfo*))EnumValue_1__ctor_m1800650159_gshared)(__this, ___name0, ___value1, method)
// System.String Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>::get_Name()
extern "C"  String_t* EnumValue_1_get_Name_m2643172728_gshared (EnumValue_1_t589082027 * __this, const MethodInfo* method);
#define EnumValue_1_get_Name_m2643172728(__this, method) ((  String_t* (*) (EnumValue_1_t589082027 *, const MethodInfo*))EnumValue_1_get_Name_m2643172728_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>::get_Value()
extern "C"  int64_t EnumValue_1_get_Value_m2186583602_gshared (EnumValue_1_t589082027 * __this, const MethodInfo* method);
#define EnumValue_1_get_Value_m2186583602(__this, method) ((  int64_t (*) (EnumValue_1_t589082027 *, const MethodInfo*))EnumValue_1_get_Value_m2186583602_gshared)(__this, method)
