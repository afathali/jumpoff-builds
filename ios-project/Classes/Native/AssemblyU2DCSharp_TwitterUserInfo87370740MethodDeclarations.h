﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TwitterUserInfo
struct TwitterUserInfo_t87370740;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;
// TwitterStatus
struct TwitterStatus_t2765035499;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void TwitterUserInfo::.ctor(System.String)
extern "C"  void TwitterUserInfo__ctor_m1842084383 (TwitterUserInfo_t87370740 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterUserInfo::.ctor(System.Collections.IDictionary)
extern "C"  void TwitterUserInfo__ctor_m2734525946 (TwitterUserInfo_t87370740 * __this, Il2CppObject * ___JSON0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterUserInfo::add_ActionProfileImageLoaded(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void TwitterUserInfo_add_ActionProfileImageLoaded_m3050009664 (TwitterUserInfo_t87370740 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterUserInfo::remove_ActionProfileImageLoaded(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void TwitterUserInfo_remove_ActionProfileImageLoaded_m1769448413 (TwitterUserInfo_t87370740 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterUserInfo::add_ActionProfileBackgroundImageLoaded(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void TwitterUserInfo_add_ActionProfileBackgroundImageLoaded_m2026856352 (TwitterUserInfo_t87370740 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterUserInfo::remove_ActionProfileBackgroundImageLoaded(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void TwitterUserInfo_remove_ActionProfileBackgroundImageLoaded_m270843863 (TwitterUserInfo_t87370740 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterUserInfo::LoadProfileImage()
extern "C"  void TwitterUserInfo_LoadProfileImage_m1762410403 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterUserInfo::LoadBackgroundImage()
extern "C"  void TwitterUserInfo_LoadBackgroundImage_m2500582438 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterUserInfo::get_rawJSON()
extern "C"  String_t* TwitterUserInfo_get_rawJSON_m3069431681 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterUserInfo::get_id()
extern "C"  String_t* TwitterUserInfo_get_id_m3137512336 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterUserInfo::get_name()
extern "C"  String_t* TwitterUserInfo_get_name_m1911747022 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterUserInfo::get_description()
extern "C"  String_t* TwitterUserInfo_get_description_m969481391 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterUserInfo::get_screen_name()
extern "C"  String_t* TwitterUserInfo_get_screen_name_m4136318513 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterUserInfo::get_location()
extern "C"  String_t* TwitterUserInfo_get_location_m1042764134 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterUserInfo::get_lang()
extern "C"  String_t* TwitterUserInfo_get_lang_m536462993 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterUserInfo::get_profile_image_url()
extern "C"  String_t* TwitterUserInfo_get_profile_image_url_m1660135716 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterUserInfo::get_profile_image_url_https()
extern "C"  String_t* TwitterUserInfo_get_profile_image_url_https_m887630722 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterUserInfo::get_profile_background_image_url()
extern "C"  String_t* TwitterUserInfo_get_profile_background_image_url_m490475547 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterUserInfo::get_profile_background_image_url_https()
extern "C"  String_t* TwitterUserInfo_get_profile_background_image_url_https_m1640120291 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TwitterUserInfo::get_friends_count()
extern "C"  int32_t TwitterUserInfo_get_friends_count_m1473011967 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TwitterUserInfo::get_statuses_count()
extern "C"  int32_t TwitterUserInfo_get_statuses_count_m919889122 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TwitterStatus TwitterUserInfo::get_status()
extern "C"  TwitterStatus_t2765035499 * TwitterUserInfo_get_status_m2930417650 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D TwitterUserInfo::get_profile_image()
extern "C"  Texture2D_t3542995729 * TwitterUserInfo_get_profile_image_m3292256762 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D TwitterUserInfo::get_profile_background()
extern "C"  Texture2D_t3542995729 * TwitterUserInfo_get_profile_background_m835540159 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TwitterUserInfo::get_profile_background_color()
extern "C"  Color_t2020392075  TwitterUserInfo_get_profile_background_color_m935814061 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TwitterUserInfo::get_profile_text_color()
extern "C"  Color_t2020392075  TwitterUserInfo_get_profile_text_color_m4138052686 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterUserInfo::OnProfileImageLoaded(UnityEngine.Texture2D)
extern "C"  void TwitterUserInfo_OnProfileImageLoaded_m4232127673 (TwitterUserInfo_t87370740 * __this, Texture2D_t3542995729 * ___img0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterUserInfo::OnProfileBackgroundLoaded(UnityEngine.Texture2D)
extern "C"  void TwitterUserInfo_OnProfileBackgroundLoaded_m303523722 (TwitterUserInfo_t87370740 * __this, Texture2D_t3542995729 * ___img0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TwitterUserInfo::HexToColor(System.String)
extern "C"  Color_t2020392075  TwitterUserInfo_HexToColor_m538248085 (TwitterUserInfo_t87370740 * __this, String_t* ___hex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterUserInfo::<ActionProfileImageLoaded>m__AD(UnityEngine.Texture2D)
extern "C"  void TwitterUserInfo_U3CActionProfileImageLoadedU3Em__AD_m3003816570 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterUserInfo::<ActionProfileBackgroundImageLoaded>m__AE(UnityEngine.Texture2D)
extern "C"  void TwitterUserInfo_U3CActionProfileBackgroundImageLoadedU3Em__AE_m3213365547 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
