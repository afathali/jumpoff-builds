﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Models.Result
struct Result_t4287219743;
// SA.Common.Models.Error
struct Error_t445207774;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"

// System.Void SA.Common.Models.Result::.ctor()
extern "C"  void Result__ctor_m114131617 (Result_t4287219743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.Result::.ctor(SA.Common.Models.Error)
extern "C"  void Result__ctor_m67150262 (Result_t4287219743 * __this, Error_t445207774 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SA.Common.Models.Error SA.Common.Models.Result::get_Error()
extern "C"  Error_t445207774 * Result_get_Error_m3741808192 (Result_t4287219743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA.Common.Models.Result::get_HasError()
extern "C"  bool Result_get_HasError_m3106830232 (Result_t4287219743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA.Common.Models.Result::get_IsSucceeded()
extern "C"  bool Result_get_IsSucceeded_m955312237 (Result_t4287219743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA.Common.Models.Result::get_IsFailed()
extern "C"  bool Result_get_IsFailed_m2142044049 (Result_t4287219743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
