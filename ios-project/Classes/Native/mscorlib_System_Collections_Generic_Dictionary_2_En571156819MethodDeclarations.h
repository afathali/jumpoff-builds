﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2489136760(__this, ___dictionary0, method) ((  void (*) (Enumerator_t571156819 *, Dictionary_2_t3546099413 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4241126229(__this, method) ((  Il2CppObject * (*) (Enumerator_t571156819 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3804306409(__this, method) ((  void (*) (Enumerator_t571156819 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m911335584(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t571156819 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m483285467(__this, method) ((  Il2CppObject * (*) (Enumerator_t571156819 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1118196987(__this, method) ((  Il2CppObject * (*) (Enumerator_t571156819 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::MoveNext()
#define Enumerator_MoveNext_m3993548217(__this, method) ((  bool (*) (Enumerator_t571156819 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::get_Current()
#define Enumerator_get_Current_m1538456641(__this, method) ((  KeyValuePair_2_t1303444635  (*) (Enumerator_t571156819 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2868696960(__this, method) ((  int32_t (*) (Enumerator_t571156819 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2115678112(__this, method) ((  CK_Database_t243306482 * (*) (Enumerator_t571156819 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::Reset()
#define Enumerator_Reset_m3824697474(__this, method) ((  void (*) (Enumerator_t571156819 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::VerifyState()
#define Enumerator_VerifyState_m914644207(__this, method) ((  void (*) (Enumerator_t571156819 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3182729457(__this, method) ((  void (*) (Enumerator_t571156819 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Database>::Dispose()
#define Enumerator_Dispose_m3090356160(__this, method) ((  void (*) (Enumerator_t571156819 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
