﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t930005165;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m2108257976_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m2108257976(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m2108257976_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppChar U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3045153909_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3045153909(__this, method) ((  Il2CppChar (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3045153909_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m3060896522_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m3060896522(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m3060896522_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m494015497_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m494015497(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m494015497_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1188047868_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1188047868(__this, method) ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1188047868_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::MoveNext()
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2896766396_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2896766396(__this, method) ((  bool (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2896766396_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::Dispose()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m228712283_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m228712283(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m228712283_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::Reset()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2696210045_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2696210045(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2696210045_gshared)(__this, method)
