﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JObject
struct JObject_t278519297;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Object
struct Il2CppObject;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t3042952059;
// System.String
struct String_t;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t2981295538;
// System.Collections.Generic.ICollection`1<Newtonsoft.Json.Linq.JToken>
struct ICollection_1_t3504719318;
// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>[]
struct KeyValuePair_2U5BU5D_t1550931756;
// System.ComponentModel.PropertyDescriptorCollection
struct PropertyDescriptorCollection_t3166009492;
// System.Attribute[]
struct AttributeU5BU5D_t4255796347;
// System.ComponentModel.AttributeCollection
struct AttributeCollection_t1925812292;
// System.ComponentModel.TypeConverter
struct TypeConverter_t745995970;
// System.ComponentModel.EventDescriptor
struct EventDescriptor_t962731901;
// System.ComponentModel.PropertyDescriptor
struct PropertyDescriptor_t4250402154;
// System.Type
struct Type_t;
// System.ComponentModel.EventDescriptorCollection
struct EventDescriptorCollection_t3053042509;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t3093584614;
// Newtonsoft.Json.Linq.JToken
struct JToken_t2552644013;
// Newtonsoft.Json.Linq.JProperty
struct JProperty_t2956441399;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JProperty>
struct IEnumerable_1_t3248568444;
// Newtonsoft.Json.JsonReader
struct JsonReader_t3154730733;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t1719617802;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t1973729997;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t3128012475;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>>
struct IEnumerator_1_t3995259620;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject278519297.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_System_ComponentModel_PropertyChangedEventH3042952059.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22224768497.h"
#include "mscorlib_System_Type1303803226.h"
#include "System_System_ComponentModel_PropertyDescriptor4250402154.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken2552644013.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JProperty2956441399.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType1307827213.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JEnumerable3820937473.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader3154730733.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer1719617802.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter1973729997.h"

// System.Void Newtonsoft.Json.Linq.JObject::.ctor()
extern "C"  void JObject__ctor_m545014719 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::.ctor(Newtonsoft.Json.Linq.JObject)
extern "C"  void JObject__ctor_m3679323595 (JObject_t278519297 * __this, JObject_t278519297 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::.ctor(System.Object[])
extern "C"  void JObject__ctor_m3747148623 (JObject_t278519297 * __this, ObjectU5BU5D_t3614634134* ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::.ctor(System.Object)
extern "C"  void JObject__ctor_m2154414077 (JObject_t278519297 * __this, Il2CppObject * ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::add_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern "C"  void JObject_add_PropertyChanged_m1954227958 (JObject_t278519297 * __this, PropertyChangedEventHandler_t3042952059 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::remove_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern "C"  void JObject_remove_PropertyChanged_m1517275075 (JObject_t278519297 * __this, PropertyChangedEventHandler_t3042952059 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.IDictionary<string,Newtonsoft.Json.Linq.JToken>.ContainsKey(System.String)
extern "C"  bool JObject_System_Collections_Generic_IDictionaryU3CstringU2CNewtonsoft_Json_Linq_JTokenU3E_ContainsKey_m3144541848 (JObject_t278519297 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.String> Newtonsoft.Json.Linq.JObject::System.Collections.Generic.IDictionary<string,Newtonsoft.Json.Linq.JToken>.get_Keys()
extern "C"  Il2CppObject* JObject_System_Collections_Generic_IDictionaryU3CstringU2CNewtonsoft_Json_Linq_JTokenU3E_get_Keys_m3368719021 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject::System.Collections.Generic.IDictionary<string,Newtonsoft.Json.Linq.JToken>.get_Values()
extern "C"  Il2CppObject* JObject_System_Collections_Generic_IDictionaryU3CstringU2CNewtonsoft_Json_Linq_JTokenU3E_get_Values_m4045476089 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.Add(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>)
extern "C"  void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Add_m349705115 (JObject_t278519297 * __this, KeyValuePair_2_t2224768497  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.Clear()
extern "C"  void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Clear_m3509542378 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.Contains(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>)
extern "C"  bool JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Contains_m1232012049 (JObject_t278519297 * __this, KeyValuePair_2_t2224768497  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>[],System.Int32)
extern "C"  void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_CopyTo_m1065613167 (JObject_t278519297 * __this, KeyValuePair_2U5BU5D_t1550931756* ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.get_IsReadOnly()
extern "C"  bool JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_get_IsReadOnly_m2569515574 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.Remove(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>)
extern "C"  bool JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Remove_m1511290260 (JObject_t278519297 * __this, KeyValuePair_2_t2224768497  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.PropertyDescriptorCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetProperties()
extern "C"  PropertyDescriptorCollection_t3166009492 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m2744695186 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.PropertyDescriptorCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetProperties(System.Attribute[])
extern "C"  PropertyDescriptorCollection_t3166009492 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m1501145027 (JObject_t278519297 * __this, AttributeU5BU5D_t4255796347* ___attributes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.AttributeCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetAttributes()
extern "C"  AttributeCollection_t1925812292 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetAttributes_m145058854 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetClassName()
extern "C"  String_t* JObject_System_ComponentModel_ICustomTypeDescriptor_GetClassName_m3614102255 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetComponentName()
extern "C"  String_t* JObject_System_ComponentModel_ICustomTypeDescriptor_GetComponentName_m348417194 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.TypeConverter Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetConverter()
extern "C"  TypeConverter_t745995970 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetConverter_m2687844705 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.EventDescriptor Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetDefaultEvent()
extern "C"  EventDescriptor_t962731901 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetDefaultEvent_m3111128841 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.PropertyDescriptor Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetDefaultProperty()
extern "C"  PropertyDescriptor_t4250402154 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetDefaultProperty_m1509210225 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetEditor(System.Type)
extern "C"  Il2CppObject * JObject_System_ComponentModel_ICustomTypeDescriptor_GetEditor_m2709674188 (JObject_t278519297 * __this, Type_t * ___editorBaseType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.EventDescriptorCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetEvents(System.Attribute[])
extern "C"  EventDescriptorCollection_t3053042509 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetEvents_m3573081766 (JObject_t278519297 * __this, AttributeU5BU5D_t4255796347* ___attributes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.EventDescriptorCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetEvents()
extern "C"  EventDescriptorCollection_t3053042509 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetEvents_m3727048987 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetPropertyOwner(System.ComponentModel.PropertyDescriptor)
extern "C"  Il2CppObject * JObject_System_ComponentModel_ICustomTypeDescriptor_GetPropertyOwner_m197988561 (JObject_t278519297 * __this, PropertyDescriptor_t4250402154 * ___pd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject::get_ChildrenTokens()
extern "C"  Il2CppObject* JObject_get_ChildrenTokens_m3311840695 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::DeepEquals(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JObject_DeepEquals_m3201021116 (JObject_t278519297 * __this, JToken_t2552644013 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JObject_InsertItem_m2221697624 (JObject_t278519297 * __this, int32_t ___index0, JToken_t2552644013 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::ValidateToken(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  void JObject_ValidateToken_m1379736486 (JObject_t278519297 * __this, JToken_t2552644013 * ___o0, JToken_t2552644013 * ___existing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::InternalPropertyChanged(Newtonsoft.Json.Linq.JProperty)
extern "C"  void JObject_InternalPropertyChanged_m3276021955 (JObject_t278519297 * __this, JProperty_t2956441399 * ___childProperty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::InternalPropertyChanging(Newtonsoft.Json.Linq.JProperty)
extern "C"  void JObject_InternalPropertyChanging_m1417830538 (JObject_t278519297 * __this, JProperty_t2956441399 * ___childProperty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::CloneToken()
extern "C"  JToken_t2552644013 * JObject_CloneToken_m3226609792 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JObject::get_Type()
extern "C"  int32_t JObject_get_Type_m622902355 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JProperty> Newtonsoft.Json.Linq.JObject::Properties()
extern "C"  Il2CppObject* JObject_Properties_m499420070 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JProperty Newtonsoft.Json.Linq.JObject::Property(System.String)
extern "C"  JProperty_t2956441399 * JObject_Property_m3152168807 (JObject_t278519297 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject::PropertyValues()
extern "C"  JEnumerable_1_t3820937473  JObject_PropertyValues_m2915931105 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::get_Item(System.Object)
extern "C"  JToken_t2552644013 * JObject_get_Item_m1750558504 (JObject_t278519297 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::set_Item(System.Object,Newtonsoft.Json.Linq.JToken)
extern "C"  void JObject_set_Item_m2574452123 (JObject_t278519297 * __this, Il2CppObject * ___key0, JToken_t2552644013 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::get_Item(System.String)
extern "C"  JToken_t2552644013 * JObject_get_Item_m2495212894 (JObject_t278519297 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::set_Item(System.String,Newtonsoft.Json.Linq.JToken)
extern "C"  void JObject_set_Item_m3296276295 (JObject_t278519297 * __this, String_t* ___propertyName0, JToken_t2552644013 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::Load(Newtonsoft.Json.JsonReader)
extern "C"  JObject_t278519297 * JObject_Load_m1903512092 (Il2CppObject * __this /* static, unused */, JsonReader_t3154730733 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::Parse(System.String)
extern "C"  JObject_t278519297 * JObject_Parse_m3886499847 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::FromObject(System.Object)
extern "C"  JObject_t278519297 * JObject_FromObject_m3476255965 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::FromObject(System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  JObject_t278519297 * JObject_FromObject_m2772441636 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, JsonSerializer_t1719617802 * ___jsonSerializer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern "C"  void JObject_WriteTo_m279158746 (JObject_t278519297 * __this, JsonWriter_t1973729997 * ___writer0, JsonConverterU5BU5D_t3128012475* ___converters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::Add(System.String,Newtonsoft.Json.Linq.JToken)
extern "C"  void JObject_Add_m458461014 (JObject_t278519297 * __this, String_t* ___propertyName0, JToken_t2552644013 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::Remove(System.String)
extern "C"  bool JObject_Remove_m276380307 (JObject_t278519297 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::TryGetValue(System.String,Newtonsoft.Json.Linq.JToken&)
extern "C"  bool JObject_TryGetValue_m1261947275 (JObject_t278519297 * __this, String_t* ___propertyName0, JToken_t2552644013 ** ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JObject::GetDeepHashCode()
extern "C"  int32_t JObject_GetDeepHashCode_m2874325646 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>> Newtonsoft.Json.Linq.JObject::GetEnumerator()
extern "C"  Il2CppObject* JObject_GetEnumerator_m2567931626 (JObject_t278519297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::OnPropertyChanged(System.String)
extern "C"  void JObject_OnPropertyChanged_m3505839361 (JObject_t278519297 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Linq.JObject::GetTokenPropertyType(Newtonsoft.Json.Linq.JToken)
extern "C"  Type_t * JObject_GetTokenPropertyType_m3582272787 (Il2CppObject * __this /* static, unused */, JToken_t2552644013 * ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::<PropertyValues>m__CC(Newtonsoft.Json.Linq.JProperty)
extern "C"  JToken_t2552644013 * JObject_U3CPropertyValuesU3Em__CC_m3129124452 (Il2CppObject * __this /* static, unused */, JProperty_t2956441399 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
