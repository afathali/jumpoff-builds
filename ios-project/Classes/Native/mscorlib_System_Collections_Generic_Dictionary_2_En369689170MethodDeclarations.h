﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1411730105(__this, ___dictionary0, method) ((  void (*) (Enumerator_t369689170 *, Dictionary_2_t3344631764 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4260924476(__this, method) ((  Il2CppObject * (*) (Enumerator_t369689170 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2541557242(__this, method) ((  void (*) (Enumerator_t369689170 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3702214375(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t369689170 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m108406294(__this, method) ((  Il2CppObject * (*) (Enumerator_t369689170 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1177799176(__this, method) ((  Il2CppObject * (*) (Enumerator_t369689170 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::MoveNext()
#define Enumerator_MoveNext_m453235278(__this, method) ((  bool (*) (Enumerator_t369689170 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::get_Current()
#define Enumerator_get_Current_m2165725934(__this, method) ((  KeyValuePair_2_t1101976986  (*) (Enumerator_t369689170 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3884164133(__this, method) ((  int32_t (*) (Enumerator_t369689170 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m299529293(__this, method) ((  CK_RecordID_t41838833 * (*) (Enumerator_t369689170 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::Reset()
#define Enumerator_Reset_m2597113239(__this, method) ((  void (*) (Enumerator_t369689170 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::VerifyState()
#define Enumerator_VerifyState_m2575055394(__this, method) ((  void (*) (Enumerator_t369689170 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m465574250(__this, method) ((  void (*) (Enumerator_t369689170 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_RecordID>::Dispose()
#define Enumerator_Dispose_m3497278009(__this, method) ((  void (*) (Enumerator_t369689170 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
