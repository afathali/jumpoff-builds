﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Models.ScreenshotMaker
struct ScreenshotMaker_t4063620334;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void SA.Common.Models.ScreenshotMaker::.ctor()
extern "C"  void ScreenshotMaker__ctor_m2310431452 (ScreenshotMaker_t4063620334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SA.Common.Models.ScreenshotMaker SA.Common.Models.ScreenshotMaker::Create()
extern "C"  ScreenshotMaker_t4063620334 * ScreenshotMaker_Create_m2167296356 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.ScreenshotMaker::Awake()
extern "C"  void ScreenshotMaker_Awake_m2936894681 (ScreenshotMaker_t4063620334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.ScreenshotMaker::GetScreenshot()
extern "C"  void ScreenshotMaker_GetScreenshot_m1849147036 (ScreenshotMaker_t4063620334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SA.Common.Models.ScreenshotMaker::SaveScreenshot()
extern "C"  Il2CppObject * ScreenshotMaker_SaveScreenshot_m796781959 (ScreenshotMaker_t4063620334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.ScreenshotMaker::<OnScreenshotReady>m__7E(UnityEngine.Texture2D)
extern "C"  void ScreenshotMaker_U3COnScreenshotReadyU3Em__7E_m1563075177 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
