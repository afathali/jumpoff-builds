﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Mono_Xml_Schema_XsdTime4165680512.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYearMonth1363357165.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonthDay3859978294.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYear3810382607.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonth4076673358.h"
#include "System_Xml_Mono_Xml_Schema_XsdGDay1914244270.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute4015859774.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype1195946242.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMe3165007540.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElement2433337156.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet614309579.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet_Facet3019654938.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInfo2864028808.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObject2050913741.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3365045970.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType248156492.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeCo1606103299.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeLi2170323082.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRe1099506232.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeUnio91327365.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType1795078578.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUtil3576230726.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity995929432.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeAtt850813783.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttr2182839281.h"
#include "System_Xml_System_Xml_Serialization_XmlEnumAttribut919400678.h"
#include "System_Xml_System_Xml_Serialization_XmlIgnoreAttri2333915871.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerN3063656491.h"
#include "System_Xml_System_Xml_ConformanceLevel3761201363.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactory3605390810.h"
#include "System_Xml_Mono_Xml_DTDObjectModel1113953282.h"
#include "System_Xml_Mono_Xml_DictionaryBase1005937181.h"
#include "System_Xml_Mono_Xml_DictionaryBase_U3CU3Ec__Iterat3518389200.h"
#include "System_Xml_Mono_Xml_DTDCollectionBase2621362935.h"
#include "System_Xml_Mono_Xml_DTDElementDeclarationCollectio2224069626.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclarationCollection243645429.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclarationCollection1212505713.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclarationCollectio228085060.h"
#include "System_Xml_Mono_Xml_DTDContentModel445576364.h"
#include "System_Xml_Mono_Xml_DTDContentModelCollection3164170484.h"
#include "System_Xml_Mono_Xml_DTDNode1758286970.h"
#include "System_Xml_Mono_Xml_DTDElementDeclaration8748002.h"
#include "System_Xml_Mono_Xml_DTDAttributeDefinition3692870749.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclaration2272374839.h"
#include "System_Xml_Mono_Xml_DTDEntityBase2353758560.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclaration4283284771.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclaration1758408116.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclarationC3496720022.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclaration252230634.h"
#include "System_Xml_Mono_Xml_DTDContentOrderType3150259539.h"
#include "System_Xml_Mono_Xml_DTDOccurence99371501.h"
#include "System_Xml_System_Xml_DTDReader2453137441.h"
#include "System_Xml_System_Xml_EntityHandling3960499440.h"
#include "System_Xml_System_Xml_NameTable594386929.h"
#include "System_Xml_System_Xml_NameTable_Entry2583369454.h"
#include "System_Xml_System_Xml_NamespaceHandling1452270444.h"
#include "System_Xml_System_Xml_NewLineHandling1737195169.h"
#include "System_Xml_System_Xml_ReadState3138905245.h"
#include "System_Xml_System_Xml_WhitespaceHandling3754063142.h"
#include "System_Xml_System_Xml_WriteState1534871862.h"
#include "System_Xml_System_Xml_XmlAttribute175731005.h"
#include "System_Xml_System_Xml_XmlAttributeCollection3359885287.h"
#include "System_Xml_System_Xml_XmlCDataSection1124775823.h"
#include "System_Xml_System_Xml_XmlChar1369421061.h"
#include "System_Xml_System_Xml_XmlCharacterData575748506.h"
#include "System_Xml_System_Xml_XmlComment3999331572.h"
#include "System_Xml_System_Xml_XmlConvert1936105738.h"
#include "System_Xml_System_Xml_XmlDateTimeSerializationMode137774893.h"
#include "System_Xml_System_Xml_XmlDeclaration1545359137.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlDocumentFragment3083262362.h"
#include "System_Xml_System_Xml_XmlDocumentType824160610.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"
#include "System_Xml_System_Xml_XmlEntity4027255380.h"
#include "System_Xml_System_Xml_XmlEntityReference3053868353.h"
#include "System_Xml_System_Xml_XmlException4188277960.h"
#include "System_Xml_System_Xml_XmlImplementation1664517635.h"
#include "System_Xml_System_Xml_XmlStreamReader2725532304.h"
#include "System_Xml_System_Xml_NonBlockingStreamReader3963211903.h"
#include "System_Xml_System_Xml_XmlInputStream2650744719.h"
#include "System_Xml_System_Xml_XmlLinkedNode1287616130.h"
#include "System_Xml_System_Xml_XmlNameEntry3745551716.h"
#include "System_Xml_System_Xml_XmlNameEntryCache3855584002.h"
#include "System_Xml_System_Xml_XmlNameTable1345805268.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap145210370.h"
#include "System_Xml_System_Xml_XmlNamespaceManager486731501.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3210081295.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope2513625351.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"
#include "System_Xml_System_Xml_XmlNode_EmptyNodeList1718403287.h"
#include "System_Xml_System_Xml_XmlNodeChangedAction1188489541.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventArgs4036174778.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"
#include "System_Xml_System_Xml_XmlNodeListChildren2811458520.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (XsdTime_t4165680512), -1, sizeof(XsdTime_t4165680512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2100[1] = 
{
	XsdTime_t4165680512_StaticFields::get_offset_of_timeFormats_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (XsdGYearMonth_t1363357165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (XsdGMonthDay_t3859978294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (XsdGYear_t3810382607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (XsdGMonth_t4076673358), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (XsdGDay_t1914244270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (XmlSchemaAnnotated_t2082486936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (XmlSchemaAttribute_t4015859774), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (XmlSchemaDatatype_t1195946242), -1, sizeof(XmlSchemaDatatype_t1195946242_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2109[55] = 
{
	XmlSchemaDatatype_t1195946242::get_offset_of_WhitespaceValue_0(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_wsChars_1(),
	XmlSchemaDatatype_t1195946242::get_offset_of_sb_2(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnySimpleType_3(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeString_4(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNormalizedString_5(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeToken_6(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeLanguage_7(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNMToken_8(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNMTokens_9(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeName_10(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNCName_11(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeID_12(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeIDRef_13(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeIDRefs_14(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeEntity_15(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeEntities_16(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNotation_17(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDecimal_18(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeInteger_19(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeLong_20(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeInt_21(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeShort_22(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeByte_23(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNonNegativeInteger_24(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypePositiveInteger_25(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedLong_26(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedInt_27(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedShort_28(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedByte_29(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNonPositiveInteger_30(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNegativeInteger_31(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeFloat_32(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDouble_33(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeBase64Binary_34(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeBoolean_35(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnyURI_36(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDuration_37(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDateTime_38(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDate_39(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeTime_40(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeHexBinary_41(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeQName_42(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGYearMonth_43(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGMonthDay_44(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGYear_45(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGMonth_46(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGDay_47(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnyAtomicType_48(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUntypedAtomic_49(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDayTimeDuration_50(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeYearMonthDuration_51(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map2A_52(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map2B_53(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map2C_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (XmlSchemaDerivationMethod_t3165007540)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2110[9] = 
{
	XmlSchemaDerivationMethod_t3165007540::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (XmlSchemaElement_t2433337156), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (XmlSchemaFacet_t614309579), -1, sizeof(XmlSchemaFacet_t614309579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2112[1] = 
{
	XmlSchemaFacet_t614309579_StaticFields::get_offset_of_AllFacets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (Facet_t3019654938)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2113[14] = 
{
	Facet_t3019654938::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (XmlSchemaInfo_t2864028808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[7] = 
{
	XmlSchemaInfo_t2864028808::get_offset_of_isDefault_0(),
	XmlSchemaInfo_t2864028808::get_offset_of_isNil_1(),
	XmlSchemaInfo_t2864028808::get_offset_of_memberType_2(),
	XmlSchemaInfo_t2864028808::get_offset_of_attr_3(),
	XmlSchemaInfo_t2864028808::get_offset_of_elem_4(),
	XmlSchemaInfo_t2864028808::get_offset_of_type_5(),
	XmlSchemaInfo_t2864028808::get_offset_of_validity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (XmlSchemaObject_t2050913741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[3] = 
{
	XmlSchemaObject_t2050913741::get_offset_of_namespaces_0(),
	XmlSchemaObject_t2050913741::get_offset_of_unhandledAttributeList_1(),
	XmlSchemaObject_t2050913741::get_offset_of_CompilationId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (XmlSchemaParticle_t3365045970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (XmlSchemaSimpleType_t248156492), -1, sizeof(XmlSchemaSimpleType_t248156492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2117[53] = 
{
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_schemaLocationType_9(),
	XmlSchemaSimpleType_t248156492::get_offset_of_content_10(),
	XmlSchemaSimpleType_t248156492::get_offset_of_islocal_11(),
	XmlSchemaSimpleType_t248156492::get_offset_of_variety_12(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsAnySimpleType_13(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsString_14(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsBoolean_15(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDecimal_16(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsFloat_17(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDouble_18(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDuration_19(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDateTime_20(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsTime_21(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDate_22(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGYearMonth_23(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGYear_24(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGMonthDay_25(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGDay_26(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGMonth_27(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsHexBinary_28(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsBase64Binary_29(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsAnyUri_30(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsQName_31(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNotation_32(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNormalizedString_33(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsToken_34(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsLanguage_35(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNMToken_36(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNMTokens_37(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsName_38(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNCName_39(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsID_40(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsIDRef_41(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsIDRefs_42(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsEntity_43(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsEntities_44(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsInteger_45(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNonPositiveInteger_46(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNegativeInteger_47(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsLong_48(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsInt_49(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsShort_50(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsByte_51(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNonNegativeInteger_52(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedLong_53(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedInt_54(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedShort_55(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedByte_56(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsPositiveInteger_57(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtUntypedAtomic_58(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtAnyAtomicType_59(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtYearMonthDuration_60(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtDayTimeDuration_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (XmlSchemaSimpleTypeContent_t1606103299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (XmlSchemaSimpleTypeList_t2170323082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2119[2] = 
{
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemType_3(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemTypeName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (XmlSchemaSimpleTypeRestriction_t1099506232), -1, sizeof(XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2120[2] = 
{
	XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields::get_offset_of_lengthStyle_3(),
	XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields::get_offset_of_listFacets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (XmlSchemaSimpleTypeUnion_t91327365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (XmlSchemaType_t1795078578), -1, sizeof(XmlSchemaType_t1795078578_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2122[6] = 
{
	XmlSchemaType_t1795078578::get_offset_of_final_3(),
	XmlSchemaType_t1795078578::get_offset_of_BaseXmlSchemaTypeInternal_4(),
	XmlSchemaType_t1795078578::get_offset_of_DatatypeInternal_5(),
	XmlSchemaType_t1795078578::get_offset_of_QNameInternal_6(),
	XmlSchemaType_t1795078578_StaticFields::get_offset_of_U3CU3Ef__switchU24map2E_7(),
	XmlSchemaType_t1795078578_StaticFields::get_offset_of_U3CU3Ef__switchU24map2F_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (XmlSchemaUtil_t3576230726), -1, sizeof(XmlSchemaUtil_t3576230726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2123[4] = 
{
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_FinalAllowed_0(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_ElementBlockAllowed_1(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_ComplexTypeBlockAllowed_2(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_StrictMsCompliant_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (XmlSchemaValidity_t995929432)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2124[4] = 
{
	XmlSchemaValidity_t995929432::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (XmlAttributeAttribute_t850813783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[1] = 
{
	XmlAttributeAttribute_t850813783::get_offset_of_attributeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (XmlElementAttribute_t2182839281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[3] = 
{
	XmlElementAttribute_t2182839281::get_offset_of_elementName_0(),
	XmlElementAttribute_t2182839281::get_offset_of_type_1(),
	XmlElementAttribute_t2182839281::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (XmlEnumAttribute_t919400678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[1] = 
{
	XmlEnumAttribute_t919400678::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (XmlIgnoreAttribute_t2333915871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (XmlSerializerNamespaces_t3063656491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[1] = 
{
	XmlSerializerNamespaces_t3063656491::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (ConformanceLevel_t3761201363)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2131[4] = 
{
	ConformanceLevel_t3761201363::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (DTDAutomataFactory_t3605390810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[3] = 
{
	DTDAutomataFactory_t3605390810::get_offset_of_root_0(),
	DTDAutomataFactory_t3605390810::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t3605390810::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (DTDObjectModel_t1113953282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[19] = 
{
	DTDObjectModel_t1113953282::get_offset_of_factory_0(),
	DTDObjectModel_t1113953282::get_offset_of_elementDecls_1(),
	DTDObjectModel_t1113953282::get_offset_of_attListDecls_2(),
	DTDObjectModel_t1113953282::get_offset_of_peDecls_3(),
	DTDObjectModel_t1113953282::get_offset_of_entityDecls_4(),
	DTDObjectModel_t1113953282::get_offset_of_notationDecls_5(),
	DTDObjectModel_t1113953282::get_offset_of_validationErrors_6(),
	DTDObjectModel_t1113953282::get_offset_of_resolver_7(),
	DTDObjectModel_t1113953282::get_offset_of_nameTable_8(),
	DTDObjectModel_t1113953282::get_offset_of_externalResources_9(),
	DTDObjectModel_t1113953282::get_offset_of_baseURI_10(),
	DTDObjectModel_t1113953282::get_offset_of_name_11(),
	DTDObjectModel_t1113953282::get_offset_of_publicId_12(),
	DTDObjectModel_t1113953282::get_offset_of_systemId_13(),
	DTDObjectModel_t1113953282::get_offset_of_intSubset_14(),
	DTDObjectModel_t1113953282::get_offset_of_intSubsetHasPERef_15(),
	DTDObjectModel_t1113953282::get_offset_of_isStandalone_16(),
	DTDObjectModel_t1113953282::get_offset_of_lineNumber_17(),
	DTDObjectModel_t1113953282::get_offset_of_linePosition_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (DictionaryBase_t1005937181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (U3CU3Ec__Iterator3_t3518389200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[5] = 
{
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CU24s_431U3E__0_0(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CpU3E__1_1(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U24PC_2(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (DTDCollectionBase_t2621362935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[1] = 
{
	DTDCollectionBase_t2621362935::get_offset_of_root_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (DTDElementDeclarationCollection_t2224069626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (DTDAttListDeclarationCollection_t243645429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (DTDEntityDeclarationCollection_t1212505713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (DTDNotationDeclarationCollection_t228085060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (DTDContentModel_t445576364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[6] = 
{
	DTDContentModel_t445576364::get_offset_of_root_5(),
	DTDContentModel_t445576364::get_offset_of_ownerElementName_6(),
	DTDContentModel_t445576364::get_offset_of_elementName_7(),
	DTDContentModel_t445576364::get_offset_of_orderType_8(),
	DTDContentModel_t445576364::get_offset_of_childModels_9(),
	DTDContentModel_t445576364::get_offset_of_occurence_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (DTDContentModelCollection_t3164170484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[1] = 
{
	DTDContentModelCollection_t3164170484::get_offset_of_contentModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (DTDNode_t1758286970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[5] = 
{
	DTDNode_t1758286970::get_offset_of_root_0(),
	DTDNode_t1758286970::get_offset_of_isInternalSubset_1(),
	DTDNode_t1758286970::get_offset_of_baseURI_2(),
	DTDNode_t1758286970::get_offset_of_lineNumber_3(),
	DTDNode_t1758286970::get_offset_of_linePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (DTDElementDeclaration_t8748002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[6] = 
{
	DTDElementDeclaration_t8748002::get_offset_of_root_5(),
	DTDElementDeclaration_t8748002::get_offset_of_contentModel_6(),
	DTDElementDeclaration_t8748002::get_offset_of_name_7(),
	DTDElementDeclaration_t8748002::get_offset_of_isEmpty_8(),
	DTDElementDeclaration_t8748002::get_offset_of_isAny_9(),
	DTDElementDeclaration_t8748002::get_offset_of_isMixedContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (DTDAttributeDefinition_t3692870749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[4] = 
{
	DTDAttributeDefinition_t3692870749::get_offset_of_name_5(),
	DTDAttributeDefinition_t3692870749::get_offset_of_datatype_6(),
	DTDAttributeDefinition_t3692870749::get_offset_of_unresolvedDefault_7(),
	DTDAttributeDefinition_t3692870749::get_offset_of_resolvedDefaultValue_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (DTDAttListDeclaration_t2272374839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[3] = 
{
	DTDAttListDeclaration_t2272374839::get_offset_of_name_5(),
	DTDAttListDeclaration_t2272374839::get_offset_of_attributeOrders_6(),
	DTDAttListDeclaration_t2272374839::get_offset_of_attributes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (DTDEntityBase_t2353758560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[10] = 
{
	DTDEntityBase_t2353758560::get_offset_of_name_5(),
	DTDEntityBase_t2353758560::get_offset_of_publicId_6(),
	DTDEntityBase_t2353758560::get_offset_of_systemId_7(),
	DTDEntityBase_t2353758560::get_offset_of_literalValue_8(),
	DTDEntityBase_t2353758560::get_offset_of_replacementText_9(),
	DTDEntityBase_t2353758560::get_offset_of_uriString_10(),
	DTDEntityBase_t2353758560::get_offset_of_absUri_11(),
	DTDEntityBase_t2353758560::get_offset_of_isInvalid_12(),
	DTDEntityBase_t2353758560::get_offset_of_loadFailed_13(),
	DTDEntityBase_t2353758560::get_offset_of_resolver_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (DTDEntityDeclaration_t4283284771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[6] = 
{
	DTDEntityDeclaration_t4283284771::get_offset_of_entityValue_15(),
	DTDEntityDeclaration_t4283284771::get_offset_of_notationName_16(),
	DTDEntityDeclaration_t4283284771::get_offset_of_ReferencingEntities_17(),
	DTDEntityDeclaration_t4283284771::get_offset_of_scanned_18(),
	DTDEntityDeclaration_t4283284771::get_offset_of_recursed_19(),
	DTDEntityDeclaration_t4283284771::get_offset_of_hasExternalReference_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (DTDNotationDeclaration_t1758408116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[5] = 
{
	DTDNotationDeclaration_t1758408116::get_offset_of_name_5(),
	DTDNotationDeclaration_t1758408116::get_offset_of_localName_6(),
	DTDNotationDeclaration_t1758408116::get_offset_of_prefix_7(),
	DTDNotationDeclaration_t1758408116::get_offset_of_publicId_8(),
	DTDNotationDeclaration_t1758408116::get_offset_of_systemId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (DTDParameterEntityDeclarationCollection_t3496720022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[2] = 
{
	DTDParameterEntityDeclarationCollection_t3496720022::get_offset_of_peDecls_0(),
	DTDParameterEntityDeclarationCollection_t3496720022::get_offset_of_root_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (DTDParameterEntityDeclaration_t252230634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (DTDContentOrderType_t3150259539)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2152[4] = 
{
	DTDContentOrderType_t3150259539::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (DTDOccurence_t99371501)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2153[5] = 
{
	DTDOccurence_t99371501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (DTDReader_t2453137441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[14] = 
{
	DTDReader_t2453137441::get_offset_of_currentInput_0(),
	DTDReader_t2453137441::get_offset_of_parserInputStack_1(),
	DTDReader_t2453137441::get_offset_of_nameBuffer_2(),
	DTDReader_t2453137441::get_offset_of_nameLength_3(),
	DTDReader_t2453137441::get_offset_of_nameCapacity_4(),
	DTDReader_t2453137441::get_offset_of_valueBuffer_5(),
	DTDReader_t2453137441::get_offset_of_currentLinkedNodeLineNumber_6(),
	DTDReader_t2453137441::get_offset_of_currentLinkedNodeLinePosition_7(),
	DTDReader_t2453137441::get_offset_of_dtdIncludeSect_8(),
	DTDReader_t2453137441::get_offset_of_normalization_9(),
	DTDReader_t2453137441::get_offset_of_processingInternalSubset_10(),
	DTDReader_t2453137441::get_offset_of_cachedPublicId_11(),
	DTDReader_t2453137441::get_offset_of_cachedSystemId_12(),
	DTDReader_t2453137441::get_offset_of_DTD_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (EntityHandling_t3960499440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2155[3] = 
{
	EntityHandling_t3960499440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (NameTable_t594386929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[3] = 
{
	NameTable_t594386929::get_offset_of_count_0(),
	NameTable_t594386929::get_offset_of_buckets_1(),
	NameTable_t594386929::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (Entry_t2583369454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[4] = 
{
	Entry_t2583369454::get_offset_of_str_0(),
	Entry_t2583369454::get_offset_of_hash_1(),
	Entry_t2583369454::get_offset_of_len_2(),
	Entry_t2583369454::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (NamespaceHandling_t1452270444)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2161[3] = 
{
	NamespaceHandling_t1452270444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (NewLineHandling_t1737195169)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2162[4] = 
{
	NewLineHandling_t1737195169::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (ReadState_t3138905245)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2163[6] = 
{
	ReadState_t3138905245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (WhitespaceHandling_t3754063142)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2164[4] = 
{
	WhitespaceHandling_t3754063142::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (WriteState_t1534871862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2165[8] = 
{
	WriteState_t1534871862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (XmlAttribute_t175731005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[4] = 
{
	XmlAttribute_t175731005::get_offset_of_name_6(),
	XmlAttribute_t175731005::get_offset_of_isDefault_7(),
	XmlAttribute_t175731005::get_offset_of_lastLinkedChild_8(),
	XmlAttribute_t175731005::get_offset_of_schemaInfo_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (XmlAttributeCollection_t3359885287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[2] = 
{
	XmlAttributeCollection_t3359885287::get_offset_of_ownerElement_4(),
	XmlAttributeCollection_t3359885287::get_offset_of_ownerDocument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (XmlCDataSection_t1124775823), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (XmlChar_t1369421061), -1, sizeof(XmlChar_t1369421061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2169[5] = 
{
	XmlChar_t1369421061_StaticFields::get_offset_of_WhitespaceChars_0(),
	XmlChar_t1369421061_StaticFields::get_offset_of_firstNamePages_1(),
	XmlChar_t1369421061_StaticFields::get_offset_of_namePages_2(),
	XmlChar_t1369421061_StaticFields::get_offset_of_nameBitmap_3(),
	XmlChar_t1369421061_StaticFields::get_offset_of_U3CU3Ef__switchU24map47_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (XmlCharacterData_t575748506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[1] = 
{
	XmlCharacterData_t575748506::get_offset_of_data_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (XmlComment_t3999331572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (XmlConvert_t1936105738), -1, sizeof(XmlConvert_t1936105738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2172[8] = 
{
	XmlConvert_t1936105738_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t1936105738_StaticFields::get_offset_of__defaultStyle_6(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_U3CU3Ef__switchU24map49_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (XmlDateTimeSerializationMode_t137774893)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2173[5] = 
{
	XmlDateTimeSerializationMode_t137774893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (XmlDeclaration_t1545359137), -1, sizeof(XmlDeclaration_t1545359137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2174[4] = 
{
	XmlDeclaration_t1545359137::get_offset_of_encoding_7(),
	XmlDeclaration_t1545359137::get_offset_of_standalone_8(),
	XmlDeclaration_t1545359137::get_offset_of_version_9(),
	XmlDeclaration_t1545359137_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (XmlDocument_t3649534162), -1, sizeof(XmlDocument_t3649534162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2175[19] = 
{
	XmlDocument_t3649534162_StaticFields::get_offset_of_optimal_create_types_6(),
	XmlDocument_t3649534162::get_offset_of_optimal_create_element_7(),
	XmlDocument_t3649534162::get_offset_of_optimal_create_attribute_8(),
	XmlDocument_t3649534162::get_offset_of_nameTable_9(),
	XmlDocument_t3649534162::get_offset_of_baseURI_10(),
	XmlDocument_t3649534162::get_offset_of_implementation_11(),
	XmlDocument_t3649534162::get_offset_of_preserveWhitespace_12(),
	XmlDocument_t3649534162::get_offset_of_resolver_13(),
	XmlDocument_t3649534162::get_offset_of_idTable_14(),
	XmlDocument_t3649534162::get_offset_of_nameCache_15(),
	XmlDocument_t3649534162::get_offset_of_lastLinkedChild_16(),
	XmlDocument_t3649534162::get_offset_of_schemaInfo_17(),
	XmlDocument_t3649534162::get_offset_of_loadMode_18(),
	XmlDocument_t3649534162::get_offset_of_NodeChanged_19(),
	XmlDocument_t3649534162::get_offset_of_NodeChanging_20(),
	XmlDocument_t3649534162::get_offset_of_NodeInserted_21(),
	XmlDocument_t3649534162::get_offset_of_NodeInserting_22(),
	XmlDocument_t3649534162::get_offset_of_NodeRemoved_23(),
	XmlDocument_t3649534162::get_offset_of_NodeRemoving_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (XmlDocumentFragment_t3083262362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[1] = 
{
	XmlDocumentFragment_t3083262362::get_offset_of_lastLinkedChild_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (XmlDocumentType_t824160610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[3] = 
{
	XmlDocumentType_t824160610::get_offset_of_entities_7(),
	XmlDocumentType_t824160610::get_offset_of_notations_8(),
	XmlDocumentType_t824160610::get_offset_of_dtd_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (XmlElement_t2877111883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2178[5] = 
{
	XmlElement_t2877111883::get_offset_of_attributes_7(),
	XmlElement_t2877111883::get_offset_of_name_8(),
	XmlElement_t2877111883::get_offset_of_lastLinkedChild_9(),
	XmlElement_t2877111883::get_offset_of_isNotEmpty_10(),
	XmlElement_t2877111883::get_offset_of_schemaInfo_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (XmlEntity_t4027255380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[7] = 
{
	XmlEntity_t4027255380::get_offset_of_name_6(),
	XmlEntity_t4027255380::get_offset_of_NDATA_7(),
	XmlEntity_t4027255380::get_offset_of_publicId_8(),
	XmlEntity_t4027255380::get_offset_of_systemId_9(),
	XmlEntity_t4027255380::get_offset_of_baseUri_10(),
	XmlEntity_t4027255380::get_offset_of_lastLinkedChild_11(),
	XmlEntity_t4027255380::get_offset_of_contentAlreadySet_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (XmlEntityReference_t3053868353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[2] = 
{
	XmlEntityReference_t3053868353::get_offset_of_entityName_7(),
	XmlEntityReference_t3053868353::get_offset_of_lastLinkedChild_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (XmlException_t4188277960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[5] = 
{
	XmlException_t4188277960::get_offset_of_lineNumber_11(),
	XmlException_t4188277960::get_offset_of_linePosition_12(),
	XmlException_t4188277960::get_offset_of_sourceUri_13(),
	XmlException_t4188277960::get_offset_of_res_14(),
	XmlException_t4188277960::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (XmlImplementation_t1664517635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[1] = 
{
	XmlImplementation_t1664517635::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (XmlStreamReader_t2725532304), -1, sizeof(XmlStreamReader_t2725532304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2183[2] = 
{
	XmlStreamReader_t2725532304::get_offset_of_input_12(),
	XmlStreamReader_t2725532304_StaticFields::get_offset_of_invalidDataException_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (NonBlockingStreamReader_t3963211903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[11] = 
{
	NonBlockingStreamReader_t3963211903::get_offset_of_input_buffer_1(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_buffer_2(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_count_3(),
	NonBlockingStreamReader_t3963211903::get_offset_of_pos_4(),
	NonBlockingStreamReader_t3963211903::get_offset_of_buffer_size_5(),
	NonBlockingStreamReader_t3963211903::get_offset_of_encoding_6(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoder_7(),
	NonBlockingStreamReader_t3963211903::get_offset_of_base_stream_8(),
	NonBlockingStreamReader_t3963211903::get_offset_of_mayBlock_9(),
	NonBlockingStreamReader_t3963211903::get_offset_of_line_builder_10(),
	NonBlockingStreamReader_t3963211903::get_offset_of_foundCR_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (XmlInputStream_t2650744719), -1, sizeof(XmlInputStream_t2650744719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2185[7] = 
{
	XmlInputStream_t2650744719_StaticFields::get_offset_of_StrictUTF8_1(),
	XmlInputStream_t2650744719::get_offset_of_enc_2(),
	XmlInputStream_t2650744719::get_offset_of_stream_3(),
	XmlInputStream_t2650744719::get_offset_of_buffer_4(),
	XmlInputStream_t2650744719::get_offset_of_bufLength_5(),
	XmlInputStream_t2650744719::get_offset_of_bufPos_6(),
	XmlInputStream_t2650744719_StaticFields::get_offset_of_encodingException_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (XmlLinkedNode_t1287616130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[1] = 
{
	XmlLinkedNode_t1287616130::get_offset_of_nextSibling_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (XmlNameEntry_t3745551716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[5] = 
{
	XmlNameEntry_t3745551716::get_offset_of_Prefix_0(),
	XmlNameEntry_t3745551716::get_offset_of_LocalName_1(),
	XmlNameEntry_t3745551716::get_offset_of_NS_2(),
	XmlNameEntry_t3745551716::get_offset_of_Hash_3(),
	XmlNameEntry_t3745551716::get_offset_of_prefixed_name_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (XmlNameEntryCache_t3855584002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2188[4] = 
{
	XmlNameEntryCache_t3855584002::get_offset_of_table_0(),
	XmlNameEntryCache_t3855584002::get_offset_of_nameTable_1(),
	XmlNameEntryCache_t3855584002::get_offset_of_dummy_2(),
	XmlNameEntryCache_t3855584002::get_offset_of_cacheBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (XmlNameTable_t1345805268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (XmlNamedNodeMap_t145210370), -1, sizeof(XmlNamedNodeMap_t145210370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2190[4] = 
{
	XmlNamedNodeMap_t145210370_StaticFields::get_offset_of_emptyEnumerator_0(),
	XmlNamedNodeMap_t145210370::get_offset_of_parent_1(),
	XmlNamedNodeMap_t145210370::get_offset_of_nodeList_2(),
	XmlNamedNodeMap_t145210370::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (XmlNamespaceManager_t486731501), -1, sizeof(XmlNamespaceManager_t486731501_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2191[9] = 
{
	XmlNamespaceManager_t486731501::get_offset_of_decls_0(),
	XmlNamespaceManager_t486731501::get_offset_of_declPos_1(),
	XmlNamespaceManager_t486731501::get_offset_of_scopes_2(),
	XmlNamespaceManager_t486731501::get_offset_of_scopePos_3(),
	XmlNamespaceManager_t486731501::get_offset_of_defaultNamespace_4(),
	XmlNamespaceManager_t486731501::get_offset_of_count_5(),
	XmlNamespaceManager_t486731501::get_offset_of_nameTable_6(),
	XmlNamespaceManager_t486731501::get_offset_of_internalAtomizedNames_7(),
	XmlNamespaceManager_t486731501_StaticFields::get_offset_of_U3CU3Ef__switchU24map28_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (NsDecl_t3210081295)+ sizeof (Il2CppObject), sizeof(NsDecl_t3210081295_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2192[2] = 
{
	NsDecl_t3210081295::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsDecl_t3210081295::get_offset_of_Uri_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (NsScope_t2513625351)+ sizeof (Il2CppObject), sizeof(NsScope_t2513625351_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2193[2] = 
{
	NsScope_t2513625351::get_offset_of_DeclCount_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsScope_t2513625351::get_offset_of_DefaultNamespace_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (XmlNode_t616554813), -1, sizeof(XmlNode_t616554813_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2194[6] = 
{
	XmlNode_t616554813_StaticFields::get_offset_of_emptyList_0(),
	XmlNode_t616554813::get_offset_of_ownerDocument_1(),
	XmlNode_t616554813::get_offset_of_parentNode_2(),
	XmlNode_t616554813::get_offset_of_childNodes_3(),
	XmlNode_t616554813_StaticFields::get_offset_of_U3CU3Ef__switchU24map44_4(),
	XmlNode_t616554813_StaticFields::get_offset_of_U3CU3Ef__switchU24map46_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (EmptyNodeList_t1718403287), -1, sizeof(EmptyNodeList_t1718403287_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2195[1] = 
{
	EmptyNodeList_t1718403287_StaticFields::get_offset_of_emptyEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (XmlNodeChangedAction_t1188489541)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2196[4] = 
{
	XmlNodeChangedAction_t1188489541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (XmlNodeChangedEventArgs_t4036174778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2197[6] = 
{
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__oldParent_1(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__newParent_2(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__action_3(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__node_4(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__oldValue_5(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (XmlNodeList_t497326455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (XmlNodeListChildren_t2811458520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[1] = 
{
	XmlNodeListChildren_t2811458520::get_offset_of_parent_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
