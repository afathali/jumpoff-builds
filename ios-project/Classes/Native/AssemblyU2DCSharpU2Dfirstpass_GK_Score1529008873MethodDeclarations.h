﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_Score
struct GK_Score_t1529008873;
// System.String
struct String_t;
// GK_Player
struct GK_Player_t2782008294;
// GK_Leaderboard
struct GK_Leaderboard_t156446466;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TimeSpan1050271570.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_CollectionType3353981271.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void GK_Score::.ctor(System.Int64,System.Int32,System.Int64,GK_TimeSpan,GK_CollectionType,System.String,System.String)
extern "C"  void GK_Score__ctor_m521269092 (GK_Score_t1529008873 * __this, int64_t ___vScore0, int32_t ___vRank1, int64_t ___vContext2, int32_t ___vTimeSpan3, int32_t ___sCollection4, String_t* ___lid5, String_t* ___pid6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GK_Score::get_Rank()
extern "C"  int32_t GK_Score_get_Rank_m1623872883 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GK_Score::get_LongScore()
extern "C"  int64_t GK_Score_get_LongScore_m4146728834 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GK_Score::get_CurrencyScore()
extern "C"  float GK_Score_get_CurrencyScore_m299228414 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GK_Score::get_DecimalFloat_1()
extern "C"  float GK_Score_get_DecimalFloat_1_m2968071294 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GK_Score::get_DecimalFloat_2()
extern "C"  float GK_Score_get_DecimalFloat_2_m2544583791 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GK_Score::get_DecimalFloat_3()
extern "C"  float GK_Score_get_DecimalFloat_3_m2685746292 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GK_Score::get_Context()
extern "C"  int64_t GK_Score_get_Context_m1252203869 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan GK_Score::get_Minutes()
extern "C"  TimeSpan_t3430258949  GK_Score_get_Minutes_m1896647655 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan GK_Score::get_Seconds()
extern "C"  TimeSpan_t3430258949  GK_Score_get_Seconds_m1082856993 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan GK_Score::get_Milliseconds()
extern "C"  TimeSpan_t3430258949  GK_Score_get_Milliseconds_m2048258708 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_Score::get_PlayerId()
extern "C"  String_t* GK_Score_get_PlayerId_m3179401234 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_Player GK_Score::get_Player()
extern "C"  GK_Player_t2782008294 * GK_Score_get_Player_m391363847 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_Score::get_LeaderboardId()
extern "C"  String_t* GK_Score_get_LeaderboardId_m3968838882 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_Leaderboard GK_Score::get_Leaderboard()
extern "C"  GK_Leaderboard_t156446466 * GK_Score_get_Leaderboard_m1009720515 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_CollectionType GK_Score::get_Collection()
extern "C"  int32_t GK_Score_get_Collection_m2786766299 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_TimeSpan GK_Score::get_TimeSpan()
extern "C"  int32_t GK_Score_get_TimeSpan_m511326495 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GK_Score::get_rank()
extern "C"  int32_t GK_Score_get_rank_m1879468243 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GK_Score::get_score()
extern "C"  int64_t GK_Score_get_score_m1775288672 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_Score::get_playerId()
extern "C"  String_t* GK_Score_get_playerId_m3143965106 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_Score::get_leaderboardId()
extern "C"  String_t* GK_Score_get_leaderboardId_m1730595330 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_TimeSpan GK_Score::get_timeSpan()
extern "C"  int32_t GK_Score_get_timeSpan_m547117439 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_CollectionType GK_Score::get_collection()
extern "C"  int32_t GK_Score_get_collection_m1608100219 (GK_Score_t1529008873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
