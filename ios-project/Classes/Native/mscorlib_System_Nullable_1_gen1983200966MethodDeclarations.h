﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1983200966.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObjectCreationHa3720134651.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2583666311_gshared (Nullable_1_t1983200966 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2583666311(__this, ___value0, method) ((  void (*) (Nullable_1_t1983200966 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2583666311_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3403460334_gshared (Nullable_1_t1983200966 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m3403460334(__this, method) ((  bool (*) (Nullable_1_t1983200966 *, const MethodInfo*))Nullable_1_get_HasValue_m3403460334_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m777214248_gshared (Nullable_1_t1983200966 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m777214248(__this, method) ((  int32_t (*) (Nullable_1_t1983200966 *, const MethodInfo*))Nullable_1_get_Value_m777214248_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m4110931490_gshared (Nullable_1_t1983200966 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m4110931490(__this, ___other0, method) ((  bool (*) (Nullable_1_t1983200966 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m4110931490_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m4288729879_gshared (Nullable_1_t1983200966 * __this, Nullable_1_t1983200966  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m4288729879(__this, ___other0, method) ((  bool (*) (Nullable_1_t1983200966 *, Nullable_1_t1983200966 , const MethodInfo*))Nullable_1_Equals_m4288729879_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m92500670_gshared (Nullable_1_t1983200966 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m92500670(__this, method) ((  int32_t (*) (Nullable_1_t1983200966 *, const MethodInfo*))Nullable_1_GetHashCode_m92500670_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3531376961_gshared (Nullable_1_t1983200966 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3531376961(__this, method) ((  int32_t (*) (Nullable_1_t1983200966 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3531376961_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1475212144_gshared (Nullable_1_t1983200966 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1475212144(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t1983200966 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m1475212144_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3840871500_gshared (Nullable_1_t1983200966 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3840871500(__this, method) ((  String_t* (*) (Nullable_1_t1983200966 *, const MethodInfo*))Nullable_1_ToString_m3840871500_gshared)(__this, method)
