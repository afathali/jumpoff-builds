﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4097593678MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1870907883(__this, ___dictionary0, method) ((  void (*) (Enumerator_t656172816 *, Dictionary_2_t3631115410 *, const MethodInfo*))Enumerator__ctor_m792485719_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2893835482(__this, method) ((  Il2CppObject * (*) (Enumerator_t656172816 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3200901816_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3975615430(__this, method) ((  void (*) (Enumerator_t656172816 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m330730428_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2811547777(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t656172816 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1253008017_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m635403676(__this, method) ((  Il2CppObject * (*) (Enumerator_t656172816 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3448913414_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1912111822(__this, method) ((  Il2CppObject * (*) (Enumerator_t656172816 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m197227828_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::MoveNext()
#define Enumerator_MoveNext_m3657317518(__this, method) ((  bool (*) (Enumerator_t656172816 *, const MethodInfo*))Enumerator_MoveNext_m3732010148_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::get_Current()
#define Enumerator_get_Current_m280513798(__this, method) ((  KeyValuePair_2_t1388460632  (*) (Enumerator_t656172816 *, const MethodInfo*))Enumerator_get_Current_m4217501564_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1232793175(__this, method) ((  int32_t (*) (Enumerator_t656172816 *, const MethodInfo*))Enumerator_get_CurrentKey_m2108057179_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2292208631(__this, method) ((  Texture2D_t3542995729 * (*) (Enumerator_t656172816 *, const MethodInfo*))Enumerator_get_CurrentValue_m1126126523_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::Reset()
#define Enumerator_Reset_m4174189649(__this, method) ((  void (*) (Enumerator_t656172816 *, const MethodInfo*))Enumerator_Reset_m812636689_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::VerifyState()
#define Enumerator_VerifyState_m2252344242(__this, method) ((  void (*) (Enumerator_t656172816 *, const MethodInfo*))Enumerator_VerifyState_m293136976_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3789895670(__this, method) ((  void (*) (Enumerator_t656172816 *, const MethodInfo*))Enumerator_VerifyCurrent_m3753703020_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,UnityEngine.Texture2D>::Dispose()
#define Enumerator_Dispose_m3221313623(__this, method) ((  void (*) (Enumerator_t656172816 *, const MethodInfo*))Enumerator_Dispose_m2418656635_gshared)(__this, method)
