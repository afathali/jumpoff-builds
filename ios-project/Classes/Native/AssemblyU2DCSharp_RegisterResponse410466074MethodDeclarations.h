﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RegisterResponse
struct RegisterResponse_t410466074;
// System.String
struct String_t;
// RegisterRequest
struct RegisterRequest_t698226714;
// User
struct User_t719925459;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_RegisterRequest698226714.h"
#include "AssemblyU2DCSharp_User719925459.h"

// System.Void RegisterResponse::.ctor()
extern "C"  void RegisterResponse__ctor_m3845728593 (RegisterResponse_t410466074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RegisterResponse::get_Success()
extern "C"  bool RegisterResponse_get_Success_m856749147 (RegisterResponse_t410466074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterResponse::set_Success(System.Boolean)
extern "C"  void RegisterResponse_set_Success_m3955118416 (RegisterResponse_t410466074 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RegisterResponse::get_ErrorMessage()
extern "C"  String_t* RegisterResponse_get_ErrorMessage_m1394670472 (RegisterResponse_t410466074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterResponse::set_ErrorMessage(System.String)
extern "C"  void RegisterResponse_set_ErrorMessage_m326205021 (RegisterResponse_t410466074 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// RegisterRequest RegisterResponse::get_Request()
extern "C"  RegisterRequest_t698226714 * RegisterResponse_get_Request_m3479122478 (RegisterResponse_t410466074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterResponse::set_Request(RegisterRequest)
extern "C"  void RegisterResponse_set_Request_m1727151565 (RegisterResponse_t410466074 * __this, RegisterRequest_t698226714 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RegisterResponse::get_AutoLogin()
extern "C"  String_t* RegisterResponse_get_AutoLogin_m1477807965 (RegisterResponse_t410466074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterResponse::set_AutoLogin(System.String)
extern "C"  void RegisterResponse_set_AutoLogin_m3176239036 (RegisterResponse_t410466074 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// User RegisterResponse::get_User()
extern "C"  User_t719925459 * RegisterResponse_get_User_m3892511431 (RegisterResponse_t410466074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterResponse::set_User(User)
extern "C"  void RegisterResponse_set_User_m2194187430 (RegisterResponse_t410466074 * __this, User_t719925459 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
