﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2825504181MethodDeclarations.h"

// System.Void System.Func`2<System.Linq.IGrouping`2<System.String,System.Reflection.MemberInfo>,<>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3845951835(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1671270965 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3134197304_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Linq.IGrouping`2<System.String,System.Reflection.MemberInfo>,<>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>>::Invoke(T)
#define Func_2_Invoke_m3036976149(__this, ___arg10, method) ((  U3CU3E__AnonType0_2_t99735840 * (*) (Func_2_t1671270965 *, Il2CppObject*, const MethodInfo*))Func_2_Invoke_m815118996_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Linq.IGrouping`2<System.String,System.Reflection.MemberInfo>,<>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m317747580(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1671270965 *, Il2CppObject*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4034295761_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Linq.IGrouping`2<System.String,System.Reflection.MemberInfo>,<>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2082046735(__this, ___result0, method) ((  U3CU3E__AnonType0_2_t99735840 * (*) (Func_2_t1671270965 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1674435418_gshared)(__this, ___result0, method)
