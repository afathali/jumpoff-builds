﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidSocialNativeExample
struct AndroidSocialNativeExample_t1704557771;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidSocialNativeExample::.ctor()
extern "C"  void AndroidSocialNativeExample__ctor_m1712657598 (AndroidSocialNativeExample_t1704557771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialNativeExample::Awake()
extern "C"  void AndroidSocialNativeExample_Awake_m1083723403 (AndroidSocialNativeExample_t1704557771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialNativeExample::ShareText()
extern "C"  void AndroidSocialNativeExample_ShareText_m3946050362 (AndroidSocialNativeExample_t1704557771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialNativeExample::ShareScreehshot()
extern "C"  void AndroidSocialNativeExample_ShareScreehshot_m2830261881 (AndroidSocialNativeExample_t1704557771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialNativeExample::ShareImage()
extern "C"  void AndroidSocialNativeExample_ShareImage_m3400598580 (AndroidSocialNativeExample_t1704557771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialNativeExample::TwitterShare()
extern "C"  void AndroidSocialNativeExample_TwitterShare_m540273850 (AndroidSocialNativeExample_t1704557771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialNativeExample::ShareMail()
extern "C"  void AndroidSocialNativeExample_ShareMail_m3081838886 (AndroidSocialNativeExample_t1704557771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialNativeExample::InstaShare()
extern "C"  void AndroidSocialNativeExample_InstaShare_m1159246390 (AndroidSocialNativeExample_t1704557771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialNativeExample::GoogleShare()
extern "C"  void AndroidSocialNativeExample_GoogleShare_m3907904396 (AndroidSocialNativeExample_t1704557771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialNativeExample::ShareFB()
extern "C"  void AndroidSocialNativeExample_ShareFB_m941364699 (AndroidSocialNativeExample_t1704557771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AndroidSocialNativeExample::PostScreenshot()
extern "C"  Il2CppObject * AndroidSocialNativeExample_PostScreenshot_m487696858 (AndroidSocialNativeExample_t1704557771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AndroidSocialNativeExample::PostFBScreenshot()
extern "C"  Il2CppObject * AndroidSocialNativeExample_PostFBScreenshot_m1921654042 (AndroidSocialNativeExample_t1704557771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
