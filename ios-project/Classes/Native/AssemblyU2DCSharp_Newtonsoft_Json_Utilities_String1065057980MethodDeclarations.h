﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Object>
struct U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t1065057980;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Object>::.ctor()
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1__ctor_m1411985081_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t1065057980 * __this, const MethodInfo* method);
#define U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1__ctor_m1411985081(__this, method) ((  void (*) (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t1065057980 *, const MethodInfo*))U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1__ctor_m1411985081_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Object>::<>m__106(TSource)
extern "C"  bool U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__106_m3805608978_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t1065057980 * __this, Il2CppObject * ___s0, const MethodInfo* method);
#define U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__106_m3805608978(__this, ___s0, method) ((  bool (*) (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t1065057980 *, Il2CppObject *, const MethodInfo*))U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__106_m3805608978_gshared)(__this, ___s0, method)
// System.Boolean Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Object>::<>m__107(TSource)
extern "C"  bool U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__107_m3804566609_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t1065057980 * __this, Il2CppObject * ___s0, const MethodInfo* method);
#define U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__107_m3804566609(__this, ___s0, method) ((  bool (*) (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t1065057980 *, Il2CppObject *, const MethodInfo*))U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__107_m3804566609_gshared)(__this, ___s0, method)
