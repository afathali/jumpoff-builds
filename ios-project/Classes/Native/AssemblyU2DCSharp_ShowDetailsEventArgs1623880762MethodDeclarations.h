﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShowDetailsEventArgs
struct ShowDetailsEventArgs_t1623880762;
// Course
struct Course_t3483112699;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Course3483112699.h"

// System.Void ShowDetailsEventArgs::.ctor()
extern "C"  void ShowDetailsEventArgs__ctor_m867654573 (ShowDetailsEventArgs_t1623880762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Course ShowDetailsEventArgs::get_Course()
extern "C"  Course_t3483112699 * ShowDetailsEventArgs_get_Course_m3283606239 (ShowDetailsEventArgs_t1623880762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowDetailsEventArgs::set_Course(Course)
extern "C"  void ShowDetailsEventArgs_set_Course_m622952970 (ShowDetailsEventArgs_t1623880762 * __this, Course_t3483112699 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
