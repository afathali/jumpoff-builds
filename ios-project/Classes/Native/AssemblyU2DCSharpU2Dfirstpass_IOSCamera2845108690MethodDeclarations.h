﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSCamera
struct IOSCamera_t2845108690;
// System.Action`1<IOSImagePickResult>
struct Action_1_t1473133776;
// System.Action`1<SA.Common.Models.Result>
struct Action_1_t4089019125;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// IOSImagePickResult
struct IOSImagePickResult_t1671334394;
// SA.Common.Models.Result
struct Result_t4287219743;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_ImageSource2773896895.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSImagePickResult1671334394.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"

// System.Void IOSCamera::.ctor()
extern "C"  void IOSCamera__ctor_m261021613 (IOSCamera_t2845108690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::.cctor()
extern "C"  void IOSCamera__cctor_m2329198174 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::add_OnImagePicked(System.Action`1<IOSImagePickResult>)
extern "C"  void IOSCamera_add_OnImagePicked_m116225361 (Il2CppObject * __this /* static, unused */, Action_1_t1473133776 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::remove_OnImagePicked(System.Action`1<IOSImagePickResult>)
extern "C"  void IOSCamera_remove_OnImagePicked_m3839402096 (Il2CppObject * __this /* static, unused */, Action_1_t1473133776 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::add_OnImageSaved(System.Action`1<SA.Common.Models.Result>)
extern "C"  void IOSCamera_add_OnImageSaved_m2614049300 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::remove_OnImageSaved(System.Action`1<SA.Common.Models.Result>)
extern "C"  void IOSCamera_remove_OnImageSaved_m2896319465 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::add_OnVideoPathPicked(System.Action`1<System.String>)
extern "C"  void IOSCamera_add_OnVideoPathPicked_m2593712258 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::remove_OnVideoPathPicked(System.Action`1<System.String>)
extern "C"  void IOSCamera_remove_OnVideoPathPicked_m1512372517 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::_ISN_SaveToCameraRoll(System.String)
extern "C"  void IOSCamera__ISN_SaveToCameraRoll_m1175828481 (Il2CppObject * __this /* static, unused */, String_t* ___encodedMedia0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::_ISN_GetVideoPathFromAlbum()
extern "C"  void IOSCamera__ISN_GetVideoPathFromAlbum_m2981274142 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::_ISN_PickImage(System.Int32)
extern "C"  void IOSCamera__ISN_PickImage_m1749575898 (Il2CppObject * __this /* static, unused */, int32_t ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::_ISN_InitCameraAPI(System.Single,System.Int32,System.Int32)
extern "C"  void IOSCamera__ISN_InitCameraAPI_m829028627 (Il2CppObject * __this /* static, unused */, float ___compressionRate0, int32_t ___maxSize1, int32_t ___encodingType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::Awake()
extern "C"  void IOSCamera_Awake_m216083644 (IOSCamera_t2845108690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::Init()
extern "C"  void IOSCamera_Init_m3810756275 (IOSCamera_t2845108690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::SaveTextureToCameraRoll(UnityEngine.Texture2D)
extern "C"  void IOSCamera_SaveTextureToCameraRoll_m1892511236 (IOSCamera_t2845108690 * __this, Texture2D_t3542995729 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::SaveScreenshotToCameraRoll()
extern "C"  void IOSCamera_SaveScreenshotToCameraRoll_m503967735 (IOSCamera_t2845108690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::GetVideoPathFromAlbum()
extern "C"  void IOSCamera_GetVideoPathFromAlbum_m568725408 (IOSCamera_t2845108690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::GetImageFromAlbum()
extern "C"  void IOSCamera_GetImageFromAlbum_m3130356797 (IOSCamera_t2845108690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::GetImageFromCamera()
extern "C"  void IOSCamera_GetImageFromCamera_m1887657327 (IOSCamera_t2845108690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::PickImage(ISN_ImageSource)
extern "C"  void IOSCamera_PickImage_m4124702838 (IOSCamera_t2845108690 * __this, int32_t ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::OnImagePickedEvent(System.String)
extern "C"  void IOSCamera_OnImagePickedEvent_m3509839517 (IOSCamera_t2845108690 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::OnImageSaveFailed()
extern "C"  void IOSCamera_OnImageSaveFailed_m4045957869 (IOSCamera_t2845108690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::OnImageSaveSuccess()
extern "C"  void IOSCamera_OnImageSaveSuccess_m2627350721 (IOSCamera_t2845108690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::OnVideoPickedEvent(System.String)
extern "C"  void IOSCamera_OnVideoPickedEvent_m2285716787 (IOSCamera_t2845108690 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IOSCamera::SaveScreenshot()
extern "C"  Il2CppObject * IOSCamera_SaveScreenshot_m1690626942 (IOSCamera_t2845108690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::<OnImagePicked>m__2E(IOSImagePickResult)
extern "C"  void IOSCamera_U3COnImagePickedU3Em__2E_m4244824941 (Il2CppObject * __this /* static, unused */, IOSImagePickResult_t1671334394 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::<OnImageSaved>m__2F(SA.Common.Models.Result)
extern "C"  void IOSCamera_U3COnImageSavedU3Em__2F_m1622883801 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSCamera::<OnVideoPathPicked>m__30(System.String)
extern "C"  void IOSCamera_U3COnVideoPathPickedU3Em__30_m87329758 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
