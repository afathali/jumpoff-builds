﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonObjectContract
struct JsonObjectContract_t2091736265;
// System.Type
struct Type_t;
// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct JsonPropertyCollection_t3302934105;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t2851816542;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MemberSerializati687984360.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js3302934105.h"
#include "mscorlib_System_Reflection_ConstructorInfo2851816542.h"

// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::.ctor(System.Type)
extern "C"  void JsonObjectContract__ctor_m2924807412 (JsonObjectContract_t2091736265 * __this, Type_t * ___underlyingType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonObjectContract::get_MemberSerialization()
extern "C"  int32_t JsonObjectContract_get_MemberSerialization_m1774580472 (JsonObjectContract_t2091736265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_MemberSerialization(Newtonsoft.Json.MemberSerialization)
extern "C"  void JsonObjectContract_set_MemberSerialization_m4272077005 (JsonObjectContract_t2091736265 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::get_Properties()
extern "C"  JsonPropertyCollection_t3302934105 * JsonObjectContract_get_Properties_m3847497184 (JsonObjectContract_t2091736265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_Properties(Newtonsoft.Json.Serialization.JsonPropertyCollection)
extern "C"  void JsonObjectContract_set_Properties_m3423163397 (JsonObjectContract_t2091736265 * __this, JsonPropertyCollection_t3302934105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::get_ConstructorParameters()
extern "C"  JsonPropertyCollection_t3302934105 * JsonObjectContract_get_ConstructorParameters_m2167983349 (JsonObjectContract_t2091736265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ConstructorParameters(Newtonsoft.Json.Serialization.JsonPropertyCollection)
extern "C"  void JsonObjectContract_set_ConstructorParameters_m2327306120 (JsonObjectContract_t2091736265 * __this, JsonPropertyCollection_t3302934105 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JsonObjectContract::get_OverrideConstructor()
extern "C"  ConstructorInfo_t2851816542 * JsonObjectContract_get_OverrideConstructor_m3857690573 (JsonObjectContract_t2091736265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_OverrideConstructor(System.Reflection.ConstructorInfo)
extern "C"  void JsonObjectContract_set_OverrideConstructor_m2729831212 (JsonObjectContract_t2091736265 * __this, ConstructorInfo_t2851816542 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JsonObjectContract::get_ParametrizedConstructor()
extern "C"  ConstructorInfo_t2851816542 * JsonObjectContract_get_ParametrizedConstructor_m3550565311 (JsonObjectContract_t2091736265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ParametrizedConstructor(System.Reflection.ConstructorInfo)
extern "C"  void JsonObjectContract_set_ParametrizedConstructor_m2508970060 (JsonObjectContract_t2091736265 * __this, ConstructorInfo_t2851816542 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
