﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs
struct NotifyCollectionChangedEventArgs_t2707150522;
// System.Collections.IList
struct IList_t3321498491;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObservableSuppor2380857217.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::.ctor(Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedAction)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m3799356624 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::.ctor(Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedAction,System.Collections.IList)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m228607237 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___action0, Il2CppObject * ___changedItems1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::.ctor(Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedAction,System.Object)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m3542035302 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___action0, Il2CppObject * ___changedItem1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::.ctor(Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedAction,System.Collections.IList,System.Collections.IList)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m3204317850 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___action0, Il2CppObject * ___newItems1, Il2CppObject * ___oldItems2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::.ctor(Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedAction,System.Collections.IList,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m3435301098 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___action0, Il2CppObject * ___changedItems1, int32_t ___startingIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::.ctor(Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedAction,System.Object,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m3970390197 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___action0, Il2CppObject * ___changedItem1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::.ctor(Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedAction,System.Object,System.Object)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m1191818320 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___action0, Il2CppObject * ___newItem1, Il2CppObject * ___oldItem2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::.ctor(Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedAction,System.Collections.IList,System.Collections.IList,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m930536447 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___action0, Il2CppObject * ___newItems1, Il2CppObject * ___oldItems2, int32_t ___startingIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::.ctor(Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedAction,System.Collections.IList,System.Int32,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m3591108569 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___action0, Il2CppObject * ___changedItems1, int32_t ___index2, int32_t ___oldIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::.ctor(Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedAction,System.Object,System.Int32,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m3137286264 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___action0, Il2CppObject * ___changedItem1, int32_t ___index2, int32_t ___oldIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::.ctor(Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedAction,System.Object,System.Object,System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs__ctor_m2811609483 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___action0, Il2CppObject * ___newItem1, Il2CppObject * ___oldItem2, int32_t ___index3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedAction Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::get_Action()
extern "C"  int32_t NotifyCollectionChangedEventArgs_get_Action_m3537381782 (NotifyCollectionChangedEventArgs_t2707150522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::set_Action(Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedAction)
extern "C"  void NotifyCollectionChangedEventArgs_set_Action_m3430884753 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::get_NewItems()
extern "C"  Il2CppObject * NotifyCollectionChangedEventArgs_get_NewItems_m4275613307 (NotifyCollectionChangedEventArgs_t2707150522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::set_NewItems(System.Collections.IList)
extern "C"  void NotifyCollectionChangedEventArgs_set_NewItems_m1995632234 (NotifyCollectionChangedEventArgs_t2707150522 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::get_NewStartingIndex()
extern "C"  int32_t NotifyCollectionChangedEventArgs_get_NewStartingIndex_m154099581 (NotifyCollectionChangedEventArgs_t2707150522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::set_NewStartingIndex(System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs_set_NewStartingIndex_m615247946 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::get_OldItems()
extern "C"  Il2CppObject * NotifyCollectionChangedEventArgs_get_OldItems_m3938637992 (NotifyCollectionChangedEventArgs_t2707150522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::set_OldItems(System.Collections.IList)
extern "C"  void NotifyCollectionChangedEventArgs_set_OldItems_m3692740827 (NotifyCollectionChangedEventArgs_t2707150522 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::get_OldStartingIndex()
extern "C"  int32_t NotifyCollectionChangedEventArgs_get_OldStartingIndex_m2874632112 (NotifyCollectionChangedEventArgs_t2707150522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.NotifyCollectionChangedEventArgs::set_OldStartingIndex(System.Int32)
extern "C"  void NotifyCollectionChangedEventArgs_set_OldStartingIndex_m3639988601 (NotifyCollectionChangedEventArgs_t2707150522 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
