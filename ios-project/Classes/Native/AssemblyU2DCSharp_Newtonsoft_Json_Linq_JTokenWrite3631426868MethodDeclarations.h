﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JTokenWriter
struct JTokenWriter_t3631426868;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3538280255;
// Newtonsoft.Json.Linq.JToken
struct JToken_t2552644013;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JValue
struct JValue_t300956845;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Uri
struct Uri_t19570940;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3538280255.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken620654565.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JValue300956845.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_DateTimeOffset1362988906.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Guid2533601593.h"
#include "System_System_Uri19570940.h"

// System.Void Newtonsoft.Json.Linq.JTokenWriter::.ctor(Newtonsoft.Json.Linq.JContainer)
extern "C"  void JTokenWriter__ctor_m811979802 (JTokenWriter_t3631426868 * __this, JContainer_t3538280255 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::.ctor()
extern "C"  void JTokenWriter__ctor_m726107946 (JTokenWriter_t3631426868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenWriter::get_Token()
extern "C"  JToken_t2552644013 * JTokenWriter_get_Token_m2836718405 (JTokenWriter_t3631426868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::Flush()
extern "C"  void JTokenWriter_Flush_m540578616 (JTokenWriter_t3631426868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::Close()
extern "C"  void JTokenWriter_Close_m1714258398 (JTokenWriter_t3631426868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteStartObject()
extern "C"  void JTokenWriter_WriteStartObject_m2991938230 (JTokenWriter_t3631426868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::AddParent(Newtonsoft.Json.Linq.JContainer)
extern "C"  void JTokenWriter_AddParent_m1881839229 (JTokenWriter_t3631426868 * __this, JContainer_t3538280255 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::RemoveParent()
extern "C"  void JTokenWriter_RemoveParent_m554349036 (JTokenWriter_t3631426868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteStartArray()
extern "C"  void JTokenWriter_WriteStartArray_m559121456 (JTokenWriter_t3631426868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteStartConstructor(System.String)
extern "C"  void JTokenWriter_WriteStartConstructor_m1005235341 (JTokenWriter_t3631426868 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern "C"  void JTokenWriter_WriteEnd_m3783788862 (JTokenWriter_t3631426868 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WritePropertyName(System.String)
extern "C"  void JTokenWriter_WritePropertyName_m3482327233 (JTokenWriter_t3631426868 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::AddValue(System.Object,Newtonsoft.Json.JsonToken)
extern "C"  void JTokenWriter_AddValue_m913876344 (JTokenWriter_t3631426868 * __this, Il2CppObject * ___value0, int32_t ___token1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::AddValue(Newtonsoft.Json.Linq.JValue,Newtonsoft.Json.JsonToken)
extern "C"  void JTokenWriter_AddValue_m1051010390 (JTokenWriter_t3631426868 * __this, JValue_t300956845 * ___value0, int32_t ___token1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteNull()
extern "C"  void JTokenWriter_WriteNull_m2885004484 (JTokenWriter_t3631426868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteUndefined()
extern "C"  void JTokenWriter_WriteUndefined_m1596210069 (JTokenWriter_t3631426868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteRaw(System.String)
extern "C"  void JTokenWriter_WriteRaw_m3303873045 (JTokenWriter_t3631426868 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteComment(System.String)
extern "C"  void JTokenWriter_WriteComment_m1596720742 (JTokenWriter_t3631426868 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.String)
extern "C"  void JTokenWriter_WriteValue_m2802253166 (JTokenWriter_t3631426868 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Int32)
extern "C"  void JTokenWriter_WriteValue_m440054389 (JTokenWriter_t3631426868 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.UInt32)
extern "C"  void JTokenWriter_WriteValue_m2591937822 (JTokenWriter_t3631426868 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Int64)
extern "C"  void JTokenWriter_WriteValue_m1602853576 (JTokenWriter_t3631426868 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.UInt64)
extern "C"  void JTokenWriter_WriteValue_m1610706855 (JTokenWriter_t3631426868 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Single)
extern "C"  void JTokenWriter_WriteValue_m569335245 (JTokenWriter_t3631426868 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Double)
extern "C"  void JTokenWriter_WriteValue_m2754700278 (JTokenWriter_t3631426868 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Boolean)
extern "C"  void JTokenWriter_WriteValue_m833370399 (JTokenWriter_t3631426868 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Int16)
extern "C"  void JTokenWriter_WriteValue_m2765653283 (JTokenWriter_t3631426868 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.UInt16)
extern "C"  void JTokenWriter_WriteValue_m2309612816 (JTokenWriter_t3631426868 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Char)
extern "C"  void JTokenWriter_WriteValue_m2038042695 (JTokenWriter_t3631426868 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Byte)
extern "C"  void JTokenWriter_WriteValue_m455821165 (JTokenWriter_t3631426868 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.SByte)
extern "C"  void JTokenWriter_WriteValue_m2480572704 (JTokenWriter_t3631426868 * __this, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Decimal)
extern "C"  void JTokenWriter_WriteValue_m378329176 (JTokenWriter_t3631426868 * __this, Decimal_t724701077  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.DateTime)
extern "C"  void JTokenWriter_WriteValue_m445920594 (JTokenWriter_t3631426868 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.DateTimeOffset)
extern "C"  void JTokenWriter_WriteValue_m3079890287 (JTokenWriter_t3631426868 * __this, DateTimeOffset_t1362988906  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Byte[])
extern "C"  void JTokenWriter_WriteValue_m618222047 (JTokenWriter_t3631426868 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.TimeSpan)
extern "C"  void JTokenWriter_WriteValue_m1826099538 (JTokenWriter_t3631426868 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Guid)
extern "C"  void JTokenWriter_WriteValue_m1038417418 (JTokenWriter_t3631426868 * __this, Guid_t2533601593  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Uri)
extern "C"  void JTokenWriter_WriteValue_m987276401 (JTokenWriter_t3631426868 * __this, Uri_t19570940 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
