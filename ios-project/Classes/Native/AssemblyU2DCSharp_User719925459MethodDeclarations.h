﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// User
struct User_t719925459;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void User::.ctor()
extern "C"  void User__ctor_m2333596438 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 User::get_Id()
extern "C"  int64_t User_get_Id_m1163330207 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::set_Id(System.Int64)
extern "C"  void User_set_Id_m3907032336 (User_t719925459 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String User::get_FullName()
extern "C"  String_t* User_get_FullName_m18201382 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::set_FullName(System.String)
extern "C"  void User_set_FullName_m3337776817 (User_t719925459 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String User::get_Email()
extern "C"  String_t* User_get_Email_m2844587358 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::set_Email(System.String)
extern "C"  void User_set_Email_m2895411485 (User_t719925459 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
