﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_ProxyPool
struct AN_ProxyPool_t3556146334;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_ProxyPool::.ctor()
extern "C"  void AN_ProxyPool__ctor_m3231832159 (AN_ProxyPool_t3556146334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ProxyPool::CallStatic(System.String,System.String,System.Object[])
extern "C"  void AN_ProxyPool_CallStatic_m1914562613 (Il2CppObject * __this /* static, unused */, String_t* ___className0, String_t* ___methodName1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
