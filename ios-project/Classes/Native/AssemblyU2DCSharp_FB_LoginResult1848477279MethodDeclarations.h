﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FB_LoginResult
struct FB_LoginResult_t1848477279;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FB_LoginResult::.ctor(System.String,System.String,System.Boolean)
extern "C"  void FB_LoginResult__ctor_m908674745 (FB_LoginResult_t1848477279 * __this, String_t* ___RawData0, String_t* ___Error1, bool ___isCanceled2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_LoginResult::SetCredential(System.String,System.String)
extern "C"  void FB_LoginResult_SetCredential_m2613763335 (FB_LoginResult_t1848477279 * __this, String_t* ___userId0, String_t* ___accessToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_LoginResult::get_UserId()
extern "C"  String_t* FB_LoginResult_get_UserId_m625699462 (FB_LoginResult_t1848477279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_LoginResult::get_AccessToken()
extern "C"  String_t* FB_LoginResult_get_AccessToken_m4239614981 (FB_LoginResult_t1848477279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FB_LoginResult::get_IsCanceled()
extern "C"  bool FB_LoginResult_get_IsCanceled_m2129651356 (FB_LoginResult_t1848477279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
