﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_PartisipantUI
struct SA_PartisipantUI_t3115332592;
// GP_Participant
struct GP_Participant_t2884377673;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GP_Participant2884377673.h"

// System.Void SA_PartisipantUI::.ctor()
extern "C"  void SA_PartisipantUI__ctor_m2962226053 (SA_PartisipantUI_t3115332592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_PartisipantUI::Awake()
extern "C"  void SA_PartisipantUI_Awake_m557487120 (SA_PartisipantUI_t3115332592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_PartisipantUI::SetParticipant(GP_Participant)
extern "C"  void SA_PartisipantUI_SetParticipant_m2590404987 (SA_PartisipantUI_t3115332592 * __this, GP_Participant_t2884377673 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
