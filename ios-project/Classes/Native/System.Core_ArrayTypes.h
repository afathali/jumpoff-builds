﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Action
struct Action_t3226471752;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;

#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li865133271.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li118159244.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L3674339243.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li116960033.h"
#include "System_Core_System_Action3226471752.h"
#include "System_Core_System_Func_2_gen2825504181.h"

#pragma once
// System.Collections.Generic.HashSet`1/Link<System.Object>[]
struct LinkU5BU5D_t3597933550  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t865133271  m_Items[1];

public:
	inline Link_t865133271  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t865133271 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t865133271  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<UnityEngine.Purchasing.ProductDefinition>[]
struct LinkU5BU5D_t96492741  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t118159244  m_Items[1];

public:
	inline Link_t118159244  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t118159244 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t118159244  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<UnityEngine.Purchasing.Product>[]
struct LinkU5BU5D_t711398730  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t3674339243  m_Items[1];

public:
	inline Link_t3674339243  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t3674339243 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t3674339243  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<UnityEngine.UI.IClippable>[]
struct LinkU5BU5D_t222250812  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t116960033  m_Items[1];

public:
	inline Link_t116960033  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t116960033 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t116960033  value)
	{
		m_Items[index] = value;
	}
};
// System.Action[]
struct ActionU5BU5D_t87223449  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_t3226471752 * m_Items[1];

public:
	inline Action_t3226471752 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_t3226471752 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_t3226471752 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Func`2<System.Object,System.Object>[]
struct Func_2U5BU5D_t3764038616  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Func_2_t2825504181 * m_Items[1];

public:
	inline Func_2_t2825504181 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Func_2_t2825504181 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Func_2_t2825504181 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
