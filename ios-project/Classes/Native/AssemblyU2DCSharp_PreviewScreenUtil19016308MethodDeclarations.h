﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PreviewScreenUtil
struct PreviewScreenUtil_t19016308;
// System.Action
struct Action_t3226471752;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Renderer
struct Renderer_t257310565;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"

// System.Void PreviewScreenUtil::.ctor()
extern "C"  void PreviewScreenUtil__ctor_m1912902025 (PreviewScreenUtil_t19016308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewScreenUtil::.cctor()
extern "C"  void PreviewScreenUtil__cctor_m310267806 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewScreenUtil::add_ActionScreenResized(System.Action)
extern "C"  void PreviewScreenUtil_add_ActionScreenResized_m1076889116 (PreviewScreenUtil_t19016308 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewScreenUtil::remove_ActionScreenResized(System.Action)
extern "C"  void PreviewScreenUtil_remove_ActionScreenResized_m1062568297 (PreviewScreenUtil_t19016308 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PreviewScreenUtil::isInScreenRect(UnityEngine.Rect,UnityEngine.Vector2)
extern "C"  bool PreviewScreenUtil_isInScreenRect_m297815843 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___rect0, Vector2_t2243707579  ___point1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect PreviewScreenUtil::getObjectBounds(UnityEngine.GameObject)
extern "C"  Rect_t3681755626  PreviewScreenUtil_getObjectBounds_m3150637395 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect PreviewScreenUtil::getRendererBounds(UnityEngine.Renderer)
extern "C"  Rect_t3681755626  PreviewScreenUtil_getRendererBounds_m2533056977 (Il2CppObject * __this /* static, unused */, Renderer_t257310565 * ___renderer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewScreenUtil::Awake()
extern "C"  void PreviewScreenUtil_Awake_m434274352 (PreviewScreenUtil_t19016308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewScreenUtil::FixedUpdate()
extern "C"  void PreviewScreenUtil_FixedUpdate_m4136000612 (PreviewScreenUtil_t19016308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PreviewScreenUtil PreviewScreenUtil::get_instance()
extern "C"  PreviewScreenUtil_t19016308 * PreviewScreenUtil_get_instance_m217120786 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewScreenUtil::<ActionScreenResized>m__B7()
extern "C"  void PreviewScreenUtil_U3CActionScreenResizedU3Em__B7_m2994514823 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
