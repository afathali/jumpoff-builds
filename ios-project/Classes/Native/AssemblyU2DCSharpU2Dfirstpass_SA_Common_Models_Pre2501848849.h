﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ResourceRequest
struct ResourceRequest_t2560315377;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;
// SA.Common.Models.PrefabAsyncLoader
struct PrefabAsyncLoader_t3449223737;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.Common.Models.PrefabAsyncLoader/<Load>c__IteratorA
struct  U3CLoadU3Ec__IteratorA_t2501848849  : public Il2CppObject
{
public:
	// UnityEngine.ResourceRequest SA.Common.Models.PrefabAsyncLoader/<Load>c__IteratorA::<request>__0
	ResourceRequest_t2560315377 * ___U3CrequestU3E__0_0;
	// UnityEngine.GameObject SA.Common.Models.PrefabAsyncLoader/<Load>c__IteratorA::<loadedObject>__1
	GameObject_t1756533147 * ___U3CloadedObjectU3E__1_1;
	// System.Int32 SA.Common.Models.PrefabAsyncLoader/<Load>c__IteratorA::$PC
	int32_t ___U24PC_2;
	// System.Object SA.Common.Models.PrefabAsyncLoader/<Load>c__IteratorA::$current
	Il2CppObject * ___U24current_3;
	// SA.Common.Models.PrefabAsyncLoader SA.Common.Models.PrefabAsyncLoader/<Load>c__IteratorA::<>f__this
	PrefabAsyncLoader_t3449223737 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__IteratorA_t2501848849, ___U3CrequestU3E__0_0)); }
	inline ResourceRequest_t2560315377 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline ResourceRequest_t2560315377 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(ResourceRequest_t2560315377 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CloadedObjectU3E__1_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__IteratorA_t2501848849, ___U3CloadedObjectU3E__1_1)); }
	inline GameObject_t1756533147 * get_U3CloadedObjectU3E__1_1() const { return ___U3CloadedObjectU3E__1_1; }
	inline GameObject_t1756533147 ** get_address_of_U3CloadedObjectU3E__1_1() { return &___U3CloadedObjectU3E__1_1; }
	inline void set_U3CloadedObjectU3E__1_1(GameObject_t1756533147 * value)
	{
		___U3CloadedObjectU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CloadedObjectU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__IteratorA_t2501848849, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__IteratorA_t2501848849, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__IteratorA_t2501848849, ___U3CU3Ef__this_4)); }
	inline PrefabAsyncLoader_t3449223737 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline PrefabAsyncLoader_t3449223737 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(PrefabAsyncLoader_t3449223737 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
