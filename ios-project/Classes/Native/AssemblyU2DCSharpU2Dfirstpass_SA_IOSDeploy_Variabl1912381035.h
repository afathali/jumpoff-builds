﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_PlistVa3782075076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.VariableListed
struct  VariableListed_t1912381035  : public Il2CppObject
{
public:
	// System.Boolean SA.IOSDeploy.VariableListed::IsOpen
	bool ___IsOpen_0;
	// System.String SA.IOSDeploy.VariableListed::DictKey
	String_t* ___DictKey_1;
	// System.String SA.IOSDeploy.VariableListed::StringValue
	String_t* ___StringValue_2;
	// System.Int32 SA.IOSDeploy.VariableListed::IntegerValue
	int32_t ___IntegerValue_3;
	// System.Single SA.IOSDeploy.VariableListed::FloatValue
	float ___FloatValue_4;
	// System.Boolean SA.IOSDeploy.VariableListed::BooleanValue
	bool ___BooleanValue_5;
	// SA.IOSDeploy.PlistValueTypes SA.IOSDeploy.VariableListed::Type
	int32_t ___Type_6;

public:
	inline static int32_t get_offset_of_IsOpen_0() { return static_cast<int32_t>(offsetof(VariableListed_t1912381035, ___IsOpen_0)); }
	inline bool get_IsOpen_0() const { return ___IsOpen_0; }
	inline bool* get_address_of_IsOpen_0() { return &___IsOpen_0; }
	inline void set_IsOpen_0(bool value)
	{
		___IsOpen_0 = value;
	}

	inline static int32_t get_offset_of_DictKey_1() { return static_cast<int32_t>(offsetof(VariableListed_t1912381035, ___DictKey_1)); }
	inline String_t* get_DictKey_1() const { return ___DictKey_1; }
	inline String_t** get_address_of_DictKey_1() { return &___DictKey_1; }
	inline void set_DictKey_1(String_t* value)
	{
		___DictKey_1 = value;
		Il2CppCodeGenWriteBarrier(&___DictKey_1, value);
	}

	inline static int32_t get_offset_of_StringValue_2() { return static_cast<int32_t>(offsetof(VariableListed_t1912381035, ___StringValue_2)); }
	inline String_t* get_StringValue_2() const { return ___StringValue_2; }
	inline String_t** get_address_of_StringValue_2() { return &___StringValue_2; }
	inline void set_StringValue_2(String_t* value)
	{
		___StringValue_2 = value;
		Il2CppCodeGenWriteBarrier(&___StringValue_2, value);
	}

	inline static int32_t get_offset_of_IntegerValue_3() { return static_cast<int32_t>(offsetof(VariableListed_t1912381035, ___IntegerValue_3)); }
	inline int32_t get_IntegerValue_3() const { return ___IntegerValue_3; }
	inline int32_t* get_address_of_IntegerValue_3() { return &___IntegerValue_3; }
	inline void set_IntegerValue_3(int32_t value)
	{
		___IntegerValue_3 = value;
	}

	inline static int32_t get_offset_of_FloatValue_4() { return static_cast<int32_t>(offsetof(VariableListed_t1912381035, ___FloatValue_4)); }
	inline float get_FloatValue_4() const { return ___FloatValue_4; }
	inline float* get_address_of_FloatValue_4() { return &___FloatValue_4; }
	inline void set_FloatValue_4(float value)
	{
		___FloatValue_4 = value;
	}

	inline static int32_t get_offset_of_BooleanValue_5() { return static_cast<int32_t>(offsetof(VariableListed_t1912381035, ___BooleanValue_5)); }
	inline bool get_BooleanValue_5() const { return ___BooleanValue_5; }
	inline bool* get_address_of_BooleanValue_5() { return &___BooleanValue_5; }
	inline void set_BooleanValue_5(bool value)
	{
		___BooleanValue_5 = value;
	}

	inline static int32_t get_offset_of_Type_6() { return static_cast<int32_t>(offsetof(VariableListed_t1912381035, ___Type_6)); }
	inline int32_t get_Type_6() const { return ___Type_6; }
	inline int32_t* get_address_of_Type_6() { return &___Type_6; }
	inline void set_Type_6(int32_t value)
	{
		___Type_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
