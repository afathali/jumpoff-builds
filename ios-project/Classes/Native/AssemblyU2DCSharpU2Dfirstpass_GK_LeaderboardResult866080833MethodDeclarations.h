﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_LeaderboardResult
struct GK_LeaderboardResult_t866080833;
// GK_Leaderboard
struct GK_Leaderboard_t156446466;
// SA.Common.Models.Error
struct Error_t445207774;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Leaderboard156446466.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"

// System.Void GK_LeaderboardResult::.ctor(GK_Leaderboard)
extern "C"  void GK_LeaderboardResult__ctor_m3454023750 (GK_LeaderboardResult_t866080833 * __this, GK_Leaderboard_t156446466 * ___leaderboard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_LeaderboardResult::.ctor(GK_Leaderboard,SA.Common.Models.Error)
extern "C"  void GK_LeaderboardResult__ctor_m1724875553 (GK_LeaderboardResult_t866080833 * __this, GK_Leaderboard_t156446466 * ___leaderboard0, Error_t445207774 * ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_LeaderboardResult::Setinfo(GK_Leaderboard)
extern "C"  void GK_LeaderboardResult_Setinfo_m21800418 (GK_LeaderboardResult_t866080833 * __this, GK_Leaderboard_t156446466 * ___leaderboard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_Leaderboard GK_LeaderboardResult::get_Leaderboard()
extern "C"  GK_Leaderboard_t156446466 * GK_LeaderboardResult_get_Leaderboard_m991011739 (GK_LeaderboardResult_t866080833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
