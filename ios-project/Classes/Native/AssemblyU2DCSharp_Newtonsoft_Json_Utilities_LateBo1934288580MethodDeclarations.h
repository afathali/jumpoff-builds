﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey30`1<System.Object>
struct U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_t1934288580;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey30`1<System.Object>::.ctor()
extern "C"  void U3CCreateDefaultConstructorU3Ec__AnonStorey30_1__ctor_m2252876817_gshared (U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_t1934288580 * __this, const MethodInfo* method);
#define U3CCreateDefaultConstructorU3Ec__AnonStorey30_1__ctor_m2252876817(__this, method) ((  void (*) (U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_t1934288580 *, const MethodInfo*))U3CCreateDefaultConstructorU3Ec__AnonStorey30_1__ctor_m2252876817_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey30`1<System.Object>::<>m__F7()
extern "C"  Il2CppObject * U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_U3CU3Em__F7_m1262253726_gshared (U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_t1934288580 * __this, const MethodInfo* method);
#define U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_U3CU3Em__F7_m1262253726(__this, method) ((  Il2CppObject * (*) (U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_t1934288580 *, const MethodInfo*))U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_U3CU3Em__F7_m1262253726_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey30`1<System.Object>::<>m__F8()
extern "C"  Il2CppObject * U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_U3CU3Em__F8_m1840458491_gshared (U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_t1934288580 * __this, const MethodInfo* method);
#define U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_U3CU3Em__F8_m1840458491(__this, method) ((  Il2CppObject * (*) (U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_t1934288580 *, const MethodInfo*))U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_U3CU3Em__F8_m1840458491_gshared)(__this, method)
