﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"

// System.Void System.Action`1<ReplayKitVideoShareResult>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m2474281995(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t2564752916 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m584977596_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<ReplayKitVideoShareResult>::Invoke(T)
#define Action_1_Invoke_m2395793834(__this, ___obj0, method) ((  void (*) (Action_1_t2564752916 *, ReplayKitVideoShareResult_t2762953534 *, const MethodInfo*))Action_1_Invoke_m101203496_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<ReplayKitVideoShareResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m2268655161(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t2564752916 *, ReplayKitVideoShareResult_t2762953534 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1305519803_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<ReplayKitVideoShareResult>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m764015502(__this, ___result0, method) ((  void (*) (Action_1_t2564752916 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m2057605070_gshared)(__this, ___result0, method)
