﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidTwitterManager
struct AndroidTwitterManager_t3465455211;
// System.Action
struct Action_t3226471752;
// System.Action`1<TWResult>
struct Action_1_t1282590442;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// TwitterPostingTask
struct TwitterPostingTask_t1522896362;
// TwitterUserInfo
struct TwitterUserInfo_t87370740;
// TWResult
struct TWResult_t1480791060;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharp_TWResult1480791060.h"

// System.Void AndroidTwitterManager::.ctor()
extern "C"  void AndroidTwitterManager__ctor_m1042406656 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::add_OnTwitterLoginStarted(System.Action)
extern "C"  void AndroidTwitterManager_add_OnTwitterLoginStarted_m2156450675 (AndroidTwitterManager_t3465455211 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::remove_OnTwitterLoginStarted(System.Action)
extern "C"  void AndroidTwitterManager_remove_OnTwitterLoginStarted_m177180490 (AndroidTwitterManager_t3465455211 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::add_OnTwitterLogOut(System.Action)
extern "C"  void AndroidTwitterManager_add_OnTwitterLogOut_m2875236011 (AndroidTwitterManager_t3465455211 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::remove_OnTwitterLogOut(System.Action)
extern "C"  void AndroidTwitterManager_remove_OnTwitterLogOut_m821956754 (AndroidTwitterManager_t3465455211 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::add_OnTwitterPostStarted(System.Action)
extern "C"  void AndroidTwitterManager_add_OnTwitterPostStarted_m3660398232 (AndroidTwitterManager_t3465455211 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::remove_OnTwitterPostStarted(System.Action)
extern "C"  void AndroidTwitterManager_remove_OnTwitterPostStarted_m2928419879 (AndroidTwitterManager_t3465455211 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::add_OnTwitterInitedAction(System.Action`1<TWResult>)
extern "C"  void AndroidTwitterManager_add_OnTwitterInitedAction_m1296735933 (AndroidTwitterManager_t3465455211 * __this, Action_1_t1282590442 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::remove_OnTwitterInitedAction(System.Action`1<TWResult>)
extern "C"  void AndroidTwitterManager_remove_OnTwitterInitedAction_m2013361536 (AndroidTwitterManager_t3465455211 * __this, Action_1_t1282590442 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::add_OnAuthCompleteAction(System.Action`1<TWResult>)
extern "C"  void AndroidTwitterManager_add_OnAuthCompleteAction_m2929279096 (AndroidTwitterManager_t3465455211 * __this, Action_1_t1282590442 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::remove_OnAuthCompleteAction(System.Action`1<TWResult>)
extern "C"  void AndroidTwitterManager_remove_OnAuthCompleteAction_m1698302769 (AndroidTwitterManager_t3465455211 * __this, Action_1_t1282590442 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::add_OnPostingCompleteAction(System.Action`1<TWResult>)
extern "C"  void AndroidTwitterManager_add_OnPostingCompleteAction_m3173042160 (AndroidTwitterManager_t3465455211 * __this, Action_1_t1282590442 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::remove_OnPostingCompleteAction(System.Action`1<TWResult>)
extern "C"  void AndroidTwitterManager_remove_OnPostingCompleteAction_m2070294829 (AndroidTwitterManager_t3465455211 * __this, Action_1_t1282590442 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::add_OnUserDataRequestCompleteAction(System.Action`1<TWResult>)
extern "C"  void AndroidTwitterManager_add_OnUserDataRequestCompleteAction_m896295410 (AndroidTwitterManager_t3465455211 * __this, Action_1_t1282590442 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::remove_OnUserDataRequestCompleteAction(System.Action`1<TWResult>)
extern "C"  void AndroidTwitterManager_remove_OnUserDataRequestCompleteAction_m3221752581 (AndroidTwitterManager_t3465455211 * __this, Action_1_t1282590442 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::Awake()
extern "C"  void AndroidTwitterManager_Awake_m3817949479 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::Init()
extern "C"  void AndroidTwitterManager_Init_m2206786296 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::Init(System.String,System.String)
extern "C"  void AndroidTwitterManager_Init_m4083739770 (AndroidTwitterManager_t3465455211 * __this, String_t* ___consumer_key0, String_t* ___consumer_secret1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::AuthenticateUser()
extern "C"  void AndroidTwitterManager_AuthenticateUser_m2762692400 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::LoadUserData()
extern "C"  void AndroidTwitterManager_LoadUserData_m1009909337 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::Post(System.String)
extern "C"  void AndroidTwitterManager_Post_m29113578 (AndroidTwitterManager_t3465455211 * __this, String_t* ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::Post(System.String,UnityEngine.Texture2D)
extern "C"  void AndroidTwitterManager_Post_m1506998322 (AndroidTwitterManager_t3465455211 * __this, String_t* ___status0, Texture2D_t3542995729 * ___texture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TwitterPostingTask AndroidTwitterManager::PostWithAuthCheck(System.String)
extern "C"  TwitterPostingTask_t1522896362 * AndroidTwitterManager_PostWithAuthCheck_m3606722007 (AndroidTwitterManager_t3465455211 * __this, String_t* ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TwitterPostingTask AndroidTwitterManager::PostWithAuthCheck(System.String,UnityEngine.Texture2D)
extern "C"  TwitterPostingTask_t1522896362 * AndroidTwitterManager_PostWithAuthCheck_m606341605 (AndroidTwitterManager_t3465455211 * __this, String_t* ___status0, Texture2D_t3542995729 * ___texture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::LogOut()
extern "C"  void AndroidTwitterManager_LogOut_m2755631948 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidTwitterManager::get_IsAuthed()
extern "C"  bool AndroidTwitterManager_get_IsAuthed_m3357607198 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidTwitterManager::get_IsInited()
extern "C"  bool AndroidTwitterManager_get_IsInited_m3240304462 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TwitterUserInfo AndroidTwitterManager::get_userInfo()
extern "C"  TwitterUserInfo_t87370740 * AndroidTwitterManager_get_userInfo_m756609661 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidTwitterManager::get_AccessToken()
extern "C"  String_t* AndroidTwitterManager_get_AccessToken_m3993434417 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidTwitterManager::get_AccessTokenSecret()
extern "C"  String_t* AndroidTwitterManager_get_AccessTokenSecret_m555624819 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::OnInited(System.String)
extern "C"  void AndroidTwitterManager_OnInited_m1806930482 (AndroidTwitterManager_t3465455211 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::OnAuthSuccess()
extern "C"  void AndroidTwitterManager_OnAuthSuccess_m3094105566 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::OnAuthFailed()
extern "C"  void AndroidTwitterManager_OnAuthFailed_m1797306536 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::OnPostSuccess()
extern "C"  void AndroidTwitterManager_OnPostSuccess_m835072658 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::OnPostFailed()
extern "C"  void AndroidTwitterManager_OnPostFailed_m2015055228 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::OnUserDataLoaded(System.String)
extern "C"  void AndroidTwitterManager_OnUserDataLoaded_m1168558555 (AndroidTwitterManager_t3465455211 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::OnUserDataLoadFailed()
extern "C"  void AndroidTwitterManager_OnUserDataLoadFailed_m1319492411 (AndroidTwitterManager_t3465455211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::OnAuthInfoReceived(System.String)
extern "C"  void AndroidTwitterManager_OnAuthInfoReceived_m1575869592 (AndroidTwitterManager_t3465455211 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::<OnTwitterLoginStarted>m__A5()
extern "C"  void AndroidTwitterManager_U3COnTwitterLoginStartedU3Em__A5_m1552690371 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::<OnTwitterLogOut>m__A6()
extern "C"  void AndroidTwitterManager_U3COnTwitterLogOutU3Em__A6_m3432106200 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::<OnTwitterPostStarted>m__A7()
extern "C"  void AndroidTwitterManager_U3COnTwitterPostStartedU3Em__A7_m2744616152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::<OnTwitterInitedAction>m__A8(TWResult)
extern "C"  void AndroidTwitterManager_U3COnTwitterInitedActionU3Em__A8_m4149201467 (Il2CppObject * __this /* static, unused */, TWResult_t1480791060 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::<OnAuthCompleteAction>m__A9(TWResult)
extern "C"  void AndroidTwitterManager_U3COnAuthCompleteActionU3Em__A9_m494041305 (Il2CppObject * __this /* static, unused */, TWResult_t1480791060 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::<OnPostingCompleteAction>m__AA(TWResult)
extern "C"  void AndroidTwitterManager_U3COnPostingCompleteActionU3Em__AA_m191245545 (Il2CppObject * __this /* static, unused */, TWResult_t1480791060 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidTwitterManager::<OnUserDataRequestCompleteAction>m__AB(TWResult)
extern "C"  void AndroidTwitterManager_U3COnUserDataRequestCompleteActionU3Em__AB_m2272966924 (Il2CppObject * __this /* static, unused */, TWResult_t1480791060 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
