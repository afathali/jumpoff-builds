﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_t3582361217;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonToken3582361217.h"

// System.Void Newtonsoft.Json.Bson.BsonToken::.ctor()
extern "C"  void BsonToken__ctor_m4239355773 (BsonToken_t3582361217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonToken::get_Parent()
extern "C"  BsonToken_t3582361217 * BsonToken_get_Parent_m2981801561 (BsonToken_t3582361217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonToken::set_Parent(Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonToken_set_Parent_m558658882 (BsonToken_t3582361217 * __this, BsonToken_t3582361217 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonToken::get_CalculatedSize()
extern "C"  int32_t BsonToken_get_CalculatedSize_m2471481191 (BsonToken_t3582361217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonToken::set_CalculatedSize(System.Int32)
extern "C"  void BsonToken_set_CalculatedSize_m2689459016 (BsonToken_t3582361217 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
