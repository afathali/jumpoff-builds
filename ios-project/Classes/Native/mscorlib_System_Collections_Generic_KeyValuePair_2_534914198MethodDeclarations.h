﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_534914198.h"
#include "AssemblyU2DCSharp_FB_ProfileImageSize3003328130.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3234979829_gshared (KeyValuePair_2_t534914198 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3234979829(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t534914198 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3234979829_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1840008439_gshared (KeyValuePair_2_t534914198 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1840008439(__this, method) ((  int32_t (*) (KeyValuePair_2_t534914198 *, const MethodInfo*))KeyValuePair_2_get_Key_m1840008439_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2457389368_gshared (KeyValuePair_2_t534914198 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2457389368(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t534914198 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m2457389368_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1210921623_gshared (KeyValuePair_2_t534914198 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1210921623(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t534914198 *, const MethodInfo*))KeyValuePair_2_get_Value_m1210921623_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2109446280_gshared (KeyValuePair_2_t534914198 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2109446280(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t534914198 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2109446280_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3271927904_gshared (KeyValuePair_2_t534914198 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3271927904(__this, method) ((  String_t* (*) (KeyValuePair_2_t534914198 *, const MethodInfo*))KeyValuePair_2_ToString_m3271927904_gshared)(__this, method)
