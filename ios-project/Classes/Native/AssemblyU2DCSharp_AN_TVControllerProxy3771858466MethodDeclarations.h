﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_TVControllerProxy
struct AN_TVControllerProxy_t3771858466;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_TVControllerProxy::.ctor()
extern "C"  void AN_TVControllerProxy__ctor_m77450949 (AN_TVControllerProxy_t3771858466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_TVControllerProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_TVControllerProxy_CallActivityFunction_m557601004 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_TVControllerProxy::AN_CheckForATVDevice()
extern "C"  void AN_TVControllerProxy_AN_CheckForATVDevice_m2153246599 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
