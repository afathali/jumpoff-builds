﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Compass
struct Compass_t2500694222;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void UnityEngine.Compass::.ctor()
extern "C"  void Compass__ctor_m1546672645 (Compass_t2500694222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Compass::get_rawVector()
extern "C"  Vector3_t2243707580  Compass_get_rawVector_m1339146095 (Compass_t2500694222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Compass::INTERNAL_get_rawVector(UnityEngine.Vector3&)
extern "C"  void Compass_INTERNAL_get_rawVector_m787260242 (Compass_t2500694222 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Compass::set_enabled(System.Boolean)
extern "C"  void Compass_set_enabled_m3437382362 (Compass_t2500694222 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
