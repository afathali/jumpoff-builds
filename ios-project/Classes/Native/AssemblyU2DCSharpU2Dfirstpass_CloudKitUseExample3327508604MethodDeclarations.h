﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloudKitUseExample
struct CloudKitUseExample_t3327508604;
// CK_QueryResult
struct CK_QueryResult_t1174067488;
// CK_RecordResult
struct CK_RecordResult_t4062548349;
// CK_RecordDeleteResult
struct CK_RecordDeleteResult_t3248469484;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_QueryResult1174067488.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_RecordResult4062548349.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_RecordDeleteResul3248469484.h"

// System.Void CloudKitUseExample::.ctor()
extern "C"  void CloudKitUseExample__ctor_m2245931681 (CloudKitUseExample_t3327508604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloudKitUseExample::OnGUI()
extern "C"  void CloudKitUseExample_OnGUI_m3971633131 (CloudKitUseExample_t3327508604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloudKitUseExample::Database_ActionQueryComplete(CK_QueryResult)
extern "C"  void CloudKitUseExample_Database_ActionQueryComplete_m3888003074 (CloudKitUseExample_t3327508604 * __this, CK_QueryResult_t1174067488 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloudKitUseExample::Database_ActionRecordFetchComplete(CK_RecordResult)
extern "C"  void CloudKitUseExample_Database_ActionRecordFetchComplete_m3111306180 (CloudKitUseExample_t3327508604 * __this, CK_RecordResult_t4062548349 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloudKitUseExample::Database_ActionRecordFetchForUpdateComplete(CK_RecordResult)
extern "C"  void CloudKitUseExample_Database_ActionRecordFetchForUpdateComplete_m2309512040 (CloudKitUseExample_t3327508604 * __this, CK_RecordResult_t4062548349 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloudKitUseExample::Database_ActionRecordDeleted(CK_RecordDeleteResult)
extern "C"  void CloudKitUseExample_Database_ActionRecordDeleted_m2710828611 (CloudKitUseExample_t3327508604 * __this, CK_RecordDeleteResult_t3248469484 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloudKitUseExample::Database_ActionRecordSaved(CK_RecordResult)
extern "C"  void CloudKitUseExample_Database_ActionRecordSaved_m1723986724 (CloudKitUseExample_t3327508604 * __this, CK_RecordResult_t4062548349 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
