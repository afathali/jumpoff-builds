﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidSocialNativeExample/<PostFBScreenshot>c__Iterator3
struct U3CPostFBScreenshotU3Ec__Iterator3_t860751190;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidSocialNativeExample/<PostFBScreenshot>c__Iterator3::.ctor()
extern "C"  void U3CPostFBScreenshotU3Ec__Iterator3__ctor_m459708745 (U3CPostFBScreenshotU3Ec__Iterator3_t860751190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AndroidSocialNativeExample/<PostFBScreenshot>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPostFBScreenshotU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2860988295 (U3CPostFBScreenshotU3Ec__Iterator3_t860751190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AndroidSocialNativeExample/<PostFBScreenshot>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPostFBScreenshotU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m96598447 (U3CPostFBScreenshotU3Ec__Iterator3_t860751190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidSocialNativeExample/<PostFBScreenshot>c__Iterator3::MoveNext()
extern "C"  bool U3CPostFBScreenshotU3Ec__Iterator3_MoveNext_m2116016163 (U3CPostFBScreenshotU3Ec__Iterator3_t860751190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialNativeExample/<PostFBScreenshot>c__Iterator3::Dispose()
extern "C"  void U3CPostFBScreenshotU3Ec__Iterator3_Dispose_m753716680 (U3CPostFBScreenshotU3Ec__Iterator3_t860751190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialNativeExample/<PostFBScreenshot>c__Iterator3::Reset()
extern "C"  void U3CPostFBScreenshotU3Ec__Iterator3_Reset_m3577615158 (U3CPostFBScreenshotU3Ec__Iterator3_t860751190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
