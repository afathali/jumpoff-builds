﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_LicenseManager
struct AN_LicenseManager_t1808504078;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AN_LicenseRequestResult2331370561.h"

// System.Void AN_LicenseManager::.ctor()
extern "C"  void AN_LicenseManager__ctor_m3302424667 (AN_LicenseManager_t1808504078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_LicenseManager::.cctor()
extern "C"  void AN_LicenseManager__cctor_m2479821196 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_LicenseManager::Awake()
extern "C"  void AN_LicenseManager_Awake_m2394424602 (AN_LicenseManager_t1808504078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_LicenseManager::StartLicenseRequest()
extern "C"  void AN_LicenseManager_StartLicenseRequest_m702621993 (AN_LicenseManager_t1808504078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_LicenseManager::StartLicenseRequest(System.String)
extern "C"  void AN_LicenseManager_StartLicenseRequest_m798937663 (AN_LicenseManager_t1808504078 * __this, String_t* ___base64PublicKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_LicenseManager::OnLicenseRequestRes(System.String)
extern "C"  void AN_LicenseManager_OnLicenseRequestRes_m3711326858 (AN_LicenseManager_t1808504078 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_LicenseManager::<OnLicenseRequestResult>m__16(AN_LicenseRequestResult)
extern "C"  void AN_LicenseManager_U3COnLicenseRequestResultU3Em__16_m1257924818 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
