﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarketExample
struct MarketExample_t1603367874;
// ISN_LocalReceiptResult
struct ISN_LocalReceiptResult_t3746327569;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// SA.Common.Models.Result
struct Result_t4287219743;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_LocalReceiptResu3746327569.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSDialogResult3739241316.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"

// System.Void MarketExample::.ctor()
extern "C"  void MarketExample__ctor_m3086236765 (MarketExample_t1603367874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::Awake()
extern "C"  void MarketExample_Awake_m3313236212 (MarketExample_t1603367874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::OnGUI()
extern "C"  void MarketExample_OnGUI_m2575939899 (MarketExample_t1603367874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::StoreProductViewDisnissed()
extern "C"  void MarketExample_StoreProductViewDisnissed_m1051389062 (MarketExample_t1603367874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::OnReceiptLoaded(ISN_LocalReceiptResult)
extern "C"  void MarketExample_OnReceiptLoaded_m3399001764 (MarketExample_t1603367874 * __this, ISN_LocalReceiptResult_t3746327569 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::OnVerifayComplete(IOSDialogResult)
extern "C"  void MarketExample_OnVerifayComplete_m2408813551 (MarketExample_t1603367874 * __this, int32_t ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MarketExample::SendRequest()
extern "C"  Il2CppObject * MarketExample_SendRequest_m1327570396 (MarketExample_t1603367874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::OnComplete(IOSDialogResult)
extern "C"  void MarketExample_OnComplete_m3697159025 (MarketExample_t1603367874 * __this, int32_t ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::OnReceiptRefreshComplete(SA.Common.Models.Result)
extern "C"  void MarketExample_OnReceiptRefreshComplete_m3803385860 (MarketExample_t1603367874 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarketExample::Dialog_RetrieveLocalReceipt(IOSDialogResult)
extern "C"  void MarketExample_Dialog_RetrieveLocalReceipt_m2553588721 (MarketExample_t1603367874 * __this, int32_t ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
