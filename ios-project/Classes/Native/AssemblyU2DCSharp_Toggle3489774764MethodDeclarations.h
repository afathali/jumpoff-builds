﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Toggle
struct Toggle_t3489774764;

#include "codegen/il2cpp-codegen.h"

// System.Void Toggle::.ctor()
extern "C"  void Toggle__ctor_m3692448053 (Toggle_t3489774764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Toggle::Start()
extern "C"  void Toggle_Start_m841577677 (Toggle_t3489774764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Toggle::SetCheck(System.Boolean)
extern "C"  void Toggle_SetCheck_m3076962866 (Toggle_t3489774764 * __this, bool ____active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Toggle::ToggleItem()
extern "C"  void Toggle_ToggleItem_m4143654796 (Toggle_t3489774764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Toggle::Check()
extern "C"  void Toggle_Check_m1055665939 (Toggle_t3489774764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Toggle::UnCheck()
extern "C"  void Toggle_UnCheck_m1904224818 (Toggle_t3489774764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Toggle::<Start>m__14D()
extern "C"  void Toggle_U3CStartU3Em__14D_m403502447 (Toggle_t3489774764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
