﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen2525452034MethodDeclarations.h"

// System.Void System.Action`2<System.Boolean,CreateCourseResponse>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m3816206867(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t3636686485 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m946854823_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.Boolean,CreateCourseResponse>::Invoke(T1,T2)
#define Action_2_Invoke_m3562124636(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t3636686485 *, bool, CreateCourseResponse_t3800683746 *, const MethodInfo*))Action_2_Invoke_m3842146412_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.Boolean,CreateCourseResponse>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m3174903843(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t3636686485 *, bool, CreateCourseResponse_t3800683746 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m3907381723_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.Boolean,CreateCourseResponse>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m3222941169(__this, ___result0, method) ((  void (*) (Action_2_t3636686485 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m2798191693_gshared)(__this, ___result0, method)
