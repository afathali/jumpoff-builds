﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlNodeConverter/<SerializeNode>c__AnonStorey1A
struct U3CSerializeNodeU3Ec__AnonStorey1A_t1249742045;
// Newtonsoft.Json.Converters.IXmlNode
struct IXmlNode_t1152344546;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Converters.XmlNodeConverter/<SerializeNode>c__AnonStorey1A::.ctor()
extern "C"  void U3CSerializeNodeU3Ec__AnonStorey1A__ctor_m3465257420 (U3CSerializeNodeU3Ec__AnonStorey1A_t1249742045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter/<SerializeNode>c__AnonStorey1A::<>m__C2(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  bool U3CSerializeNodeU3Ec__AnonStorey1A_U3CU3Em__C2_m2641128164 (U3CSerializeNodeU3Ec__AnonStorey1A_t1249742045 * __this, Il2CppObject * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
