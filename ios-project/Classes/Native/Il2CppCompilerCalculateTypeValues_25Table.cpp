﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AN_Result1919128167.h"
#include "AssemblyU2DCSharp_GP_AdvertisingIdLoadResult2783375090.h"
#include "AssemblyU2DCSharp_AndroidGoogleAnalytics1297670224.h"
#include "AssemblyU2DCSharp_AN_LicenseManager1808504078.h"
#include "AssemblyU2DCSharp_AN_LicenseRequestResult2331370561.h"
#include "AssemblyU2DCSharp_AN_CameraCaptureType74224559.h"
#include "AssemblyU2DCSharp_AN_PermissionState838201224.h"
#include "AssemblyU2DCSharp_AN_PushNotificationService941379374.h"
#include "AssemblyU2DCSharp_AN_SettingsActivityAction2396074174.h"
#include "AssemblyU2DCSharp_AddressBookController3183854505.h"
#include "AssemblyU2DCSharp_AndroidAppInfoLoader2209520163.h"
#include "AssemblyU2DCSharp_AndroidCamera1711267764.h"
#include "AssemblyU2DCSharp_AndroidCameraImageFormat4079396078.h"
#include "AssemblyU2DCSharp_AndroidNativeUtility798160036.h"
#include "AssemblyU2DCSharp_AndroidSocialGate2360988137.h"
#include "AssemblyU2DCSharp_ImmersiveMode15485700.h"
#include "AssemblyU2DCSharp_PermissionsManager4266031513.h"
#include "AssemblyU2DCSharp_TVAppController1564329309.h"
#include "AssemblyU2DCSharp_AN_Locale121755426.h"
#include "AssemblyU2DCSharp_AN_NetworkInfo247793570.h"
#include "AssemblyU2DCSharp_AndroidApp620754602.h"
#include "AssemblyU2DCSharp_PackageAppInfo260912591.h"
#include "AssemblyU2DCSharp_AndroidNotificationBuilder2822362133.h"
#include "AssemblyU2DCSharp_AndroidNotificationManager3325256667.h"
#include "AssemblyU2DCSharp_AndroidToast1991890102.h"
#include "AssemblyU2DCSharp_ParsePushesStub4146409111.h"
#include "AssemblyU2DCSharp_LocalNotificationTemplate2880890350.h"
#include "AssemblyU2DCSharp_AN_GrantPermissionsResult250489657.h"
#include "AssemblyU2DCSharp_GallerySaveResult1643856950.h"
#include "AssemblyU2DCSharp_AN_SoomlaEventType3710443565.h"
#include "AssemblyU2DCSharp_AN_SoomlaGrow3055280006.h"
#include "AssemblyU2DCSharp_AN_SoomlaProxy2595430893.h"
#include "AssemblyU2DCSharp_AN_SoomlaSocialProvider1447552007.h"
#include "AssemblyU2DCSharp_AndroidABAddress3525326208.h"
#include "AssemblyU2DCSharp_AndroidABChat2851498654.h"
#include "AssemblyU2DCSharp_AndroidABEmail2938164884.h"
#include "AssemblyU2DCSharp_AndroidABOrganization1539209357.h"
#include "AssemblyU2DCSharp_AndroidContactInfo2118672179.h"
#include "AssemblyU2DCSharp_AndroidImagePickResult1791733552.h"
#include "AssemblyU2DCSharp_GP_AppInvitesController2424429497.h"
#include "AssemblyU2DCSharp_GoogleCloudManager1208504825.h"
#include "AssemblyU2DCSharp_GoogleCloudMessageService984991750.h"
#include "AssemblyU2DCSharp_GooglePlayConnection348367589.h"
#include "AssemblyU2DCSharp_GooglePlayEvents1613959644.h"
#include "AssemblyU2DCSharp_GooglePlayInvitationManager2586337437.h"
#include "AssemblyU2DCSharp_GooglePlayManager746138120.h"
#include "AssemblyU2DCSharp_GooglePlayQuests1618778050.h"
#include "AssemblyU2DCSharp_GooglePlayRTM547022790.h"
#include "AssemblyU2DCSharp_GooglePlaySavedGamesManager1475146022.h"
#include "AssemblyU2DCSharp_GooglePlayTBM641131310.h"
#include "AssemblyU2DCSharp_GooglePlusAPI3074122137.h"
#include "AssemblyU2DCSharp_GP_AppInvite1121244750.h"
#include "AssemblyU2DCSharp_GP_AppInviteBuilder1976720519.h"
#include "AssemblyU2DCSharp_AN_PlusBtnAnnotation1944163283.h"
#include "AssemblyU2DCSharp_AN_PlusBtnSize3788325417.h"
#include "AssemblyU2DCSharp_AN_PlusButton1370758440.h"
#include "AssemblyU2DCSharp_AN_PlusButtonsManager882255532.h"
#include "AssemblyU2DCSharp_AN_DeepLinkingManager638324589.h"
#include "AssemblyU2DCSharp_AN_PlusShareBuilder211572262.h"
#include "AssemblyU2DCSharp_AN_PlusShareListener257800803.h"
#include "AssemblyU2DCSharp_AN_PlusShareListener_PlusShareCa2327980784.h"
#include "AssemblyU2DCSharp_AN_PlusShareResult3488688014.h"
#include "AssemblyU2DCSharp_GP_RetrieveAppInviteResult18485229.h"
#include "AssemblyU2DCSharp_GP_SendAppInvitesResult3999077544.h"
#include "AssemblyU2DCSharp_GooglePlayUtils968630402.h"
#include "AssemblyU2DCSharp_AndroidDialog3283474837.h"
#include "AssemblyU2DCSharp_AndroidMessage2504997174.h"
#include "AssemblyU2DCSharp_AndroidRateUsPopUp1491925263.h"
#include "AssemblyU2DCSharp_BaseAndroidPopup3200634462.h"
#include "AssemblyU2DCSharp_AN_APIManager1324084101.h"
#include "AssemblyU2DCSharp_AN_API_NAME4153298650.h"
#include "AssemblyU2DCSharp_AndroidAdMobBanner187179330.h"
#include "AssemblyU2DCSharp_AndroidAdMobBannerInterstitial1805481188.h"
#include "AssemblyU2DCSharp_AndroidGoogleAdsExample2727429144.h"
#include "AssemblyU2DCSharp_AndroidGoogleAdsExample_old3329868158.h"
#include "AssemblyU2DCSharp_AndroidNativeExampleBase3916638757.h"
#include "AssemblyU2DCSharp_BillingExample2738956801.h"
#include "AssemblyU2DCSharp_GPaymnetManagerExample841599384.h"
#include "AssemblyU2DCSharp_GameBillingManagerExample1056429950.h"
#include "AssemblyU2DCSharp_GameDataExample175804752.h"
#include "AssemblyU2DCSharp_GamePlayExample3564348922.h"
#include "AssemblyU2DCSharp_CustomLeaderboardFiledsHolder4123707333.h"
#include "AssemblyU2DCSharp_CustomPlayerUIRow1758969648.h"
#include "AssemblyU2DCSharp_CSharpAPIHelper2548466795.h"
#include "AssemblyU2DCSharp_RTM_Game_Example1461391879.h"
#include "AssemblyU2DCSharp_TBM_Game_Example857044347.h"
#include "AssemblyU2DCSharp_ANNativeEventsExample3453490363.h"
#include "AssemblyU2DCSharp_AddressBookExample3542149761.h"
#include "AssemblyU2DCSharp_AnOtherFeaturesPreview3806064552.h"
#include "AssemblyU2DCSharp_AnalyticsUseExample751986731.h"
#include "AssemblyU2DCSharp_AndroidPopUpExamples3185211868.h"
#include "AssemblyU2DCSharp_NotificationsExample734553030.h"
#include "AssemblyU2DCSharp_PermissionsAPIExample2362515826.h"
#include "AssemblyU2DCSharp_PlusButtonsAPIExample2847602409.h"
#include "AssemblyU2DCSharp_PreviewSceneController1557748400.h"
#include "AssemblyU2DCSharp_GoogleCloudUseExample2949985613.h"
#include "AssemblyU2DCSharp_PlayServicFridnsLoadExample_New1740499043.h"
#include "AssemblyU2DCSharp_PlayServiceCustomLBExample1198012866.h"
#include "AssemblyU2DCSharp_PlayServiceExample1159077439.h"
#include "AssemblyU2DCSharp_QuestAndEventsExample3324016540.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (AN_Result_t1919128167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[1] = 
{
	AN_Result_t1919128167::get_offset_of__IsSucceeded_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (GP_AdvertisingIdLoadResult_t2783375090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[2] = 
{
	GP_AdvertisingIdLoadResult_t2783375090::get_offset_of_id_1(),
	GP_AdvertisingIdLoadResult_t2783375090::get_offset_of_isLimitAdTrackingEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (AndroidGoogleAnalytics_t1297670224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[1] = 
{
	AndroidGoogleAnalytics_t1297670224::get_offset_of_IsStarted_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (AN_LicenseManager_t1808504078), -1, sizeof(AN_LicenseManager_t1808504078_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2503[2] = 
{
	AN_LicenseManager_t1808504078_StaticFields::get_offset_of_OnLicenseRequestResult_4(),
	AN_LicenseManager_t1808504078_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (AN_LicenseRequestResult_t2331370561)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2504[11] = 
{
	AN_LicenseRequestResult_t2331370561::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (AN_CameraCaptureType_t74224559)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2505[3] = 
{
	AN_CameraCaptureType_t74224559::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (AN_PermissionState_t838201224)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2506[3] = 
{
	AN_PermissionState_t838201224::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (AN_PushNotificationService_t941379374)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2507[4] = 
{
	AN_PushNotificationService_t941379374::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (AN_SettingsActivityAction_t2396074174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[57] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (AddressBookController_t3183854505), -1, sizeof(AddressBookController_t3183854505_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2509[7] = 
{
	0,
	0,
	0,
	AddressBookController_t3183854505_StaticFields::get_offset_of__isLoaded_7(),
	AddressBookController_t3183854505::get_offset_of__contacts_8(),
	AddressBookController_t3183854505_StaticFields::get_offset_of_OnContactsLoadedAction_9(),
	AddressBookController_t3183854505_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (AndroidAppInfoLoader_t2209520163), -1, sizeof(AndroidAppInfoLoader_t2209520163_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2510[3] = 
{
	AndroidAppInfoLoader_t2209520163::get_offset_of_PacakgeInfo_4(),
	AndroidAppInfoLoader_t2209520163_StaticFields::get_offset_of_ActionPacakgeInfoLoaded_5(),
	AndroidAppInfoLoader_t2209520163_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (AndroidCamera_t1711267764), -1, sizeof(AndroidCamera_t1711267764_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2511[5] = 
{
	AndroidCamera_t1711267764::get_offset_of_OnImagePicked_4(),
	AndroidCamera_t1711267764::get_offset_of_OnImageSaved_5(),
	AndroidCamera_t1711267764_StaticFields::get_offset_of__lastImageName_6(),
	AndroidCamera_t1711267764_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	AndroidCamera_t1711267764_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (AndroidCameraImageFormat_t4079396078)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2512[3] = 
{
	AndroidCameraImageFormat_t4079396078::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (AndroidNativeUtility_t798160036), -1, sizeof(AndroidNativeUtility_t798160036_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2513[14] = 
{
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_OnPackageCheckResult_4(),
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_OnAndroidIdLoaded_5(),
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_InternalStoragePathLoaded_6(),
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_ExternalStoragePathLoaded_7(),
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_LocaleInfoLoaded_8(),
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_ActionDevicePackagesListLoaded_9(),
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_ActionNetworkInfoLoaded_10(),
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_12(),
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_13(),
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_14(),
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_15(),
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_16(),
	AndroidNativeUtility_t798160036_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (AndroidSocialGate_t2360988137), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (ImmersiveMode_t15485700), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (PermissionsManager_t4266031513), -1, sizeof(PermissionsManager_t4266031513_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2516[3] = 
{
	0,
	PermissionsManager_t4266031513_StaticFields::get_offset_of_ActionPermissionsRequestCompleted_5(),
	PermissionsManager_t4266031513_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (TVAppController_t1564329309), -1, sizeof(TVAppController_t1564329309_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2517[3] = 
{
	TVAppController_t1564329309::get_offset_of__IsRuningOnTVDevice_4(),
	TVAppController_t1564329309_StaticFields::get_offset_of_DeviceTypeChecked_5(),
	TVAppController_t1564329309_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (AN_Locale_t121755426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[4] = 
{
	AN_Locale_t121755426::get_offset_of_CountryCode_0(),
	AN_Locale_t121755426::get_offset_of_DisplayCountry_1(),
	AN_Locale_t121755426::get_offset_of_LanguageCode_2(),
	AN_Locale_t121755426::get_offset_of_DisplayLanguage_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (AN_NetworkInfo_t247793570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[7] = 
{
	AN_NetworkInfo_t247793570::get_offset_of_IpAddress_0(),
	AN_NetworkInfo_t247793570::get_offset_of_MacAddress_1(),
	AN_NetworkInfo_t247793570::get_offset_of_SubnetMask_2(),
	AN_NetworkInfo_t247793570::get_offset_of_SSID_3(),
	AN_NetworkInfo_t247793570::get_offset_of_BSSID_4(),
	AN_NetworkInfo_t247793570::get_offset_of_LinkSpeed_5(),
	AN_NetworkInfo_t247793570::get_offset_of_NetworkId_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (AndroidApp_t620754602), -1, sizeof(AndroidApp_t620754602_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2520[2] = 
{
	AndroidApp_t620754602::get_offset_of_OnActivityResult_4(),
	AndroidApp_t620754602_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (PackageAppInfo_t260912591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2521[6] = 
{
	PackageAppInfo_t260912591::get_offset_of_versionName_2(),
	PackageAppInfo_t260912591::get_offset_of_versionCode_3(),
	PackageAppInfo_t260912591::get_offset_of_packageName_4(),
	PackageAppInfo_t260912591::get_offset_of_sharedUserId_5(),
	PackageAppInfo_t260912591::get_offset_of_sharedUserLabel_6(),
	PackageAppInfo_t260912591::get_offset_of_lastUpdateTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (AndroidNotificationBuilder_t2822362133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[11] = 
{
	0,
	AndroidNotificationBuilder_t2822362133::get_offset_of__id_1(),
	AndroidNotificationBuilder_t2822362133::get_offset_of__title_2(),
	AndroidNotificationBuilder_t2822362133::get_offset_of__message_3(),
	AndroidNotificationBuilder_t2822362133::get_offset_of__time_4(),
	AndroidNotificationBuilder_t2822362133::get_offset_of__sound_5(),
	AndroidNotificationBuilder_t2822362133::get_offset_of__smallIcon_6(),
	AndroidNotificationBuilder_t2822362133::get_offset_of__vibration_7(),
	AndroidNotificationBuilder_t2822362133::get_offset_of__showIfAppForeground_8(),
	AndroidNotificationBuilder_t2822362133::get_offset_of__largeIcon_9(),
	AndroidNotificationBuilder_t2822362133::get_offset_of__bigPicture_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (AndroidNotificationManager_t3325256667), -1, sizeof(AndroidNotificationManager_t3325256667_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2523[7] = 
{
	0,
	0,
	0,
	0,
	0,
	AndroidNotificationManager_t3325256667::get_offset_of_OnNotificationIdLoaded_9(),
	AndroidNotificationManager_t3325256667_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (AndroidToast_t1991890102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (ParsePushesStub_t4146409111), -1, sizeof(ParsePushesStub_t4146409111_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2525[2] = 
{
	ParsePushesStub_t4146409111_StaticFields::get_offset_of_OnPushReceived_0(),
	ParsePushesStub_t4146409111_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (LocalNotificationTemplate_t2880890350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[5] = 
{
	0,
	LocalNotificationTemplate_t2880890350::get_offset_of__id_1(),
	LocalNotificationTemplate_t2880890350::get_offset_of__title_2(),
	LocalNotificationTemplate_t2880890350::get_offset_of__message_3(),
	LocalNotificationTemplate_t2880890350::get_offset_of__fireDate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (AN_GrantPermissionsResult_t250489657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[1] = 
{
	AN_GrantPermissionsResult_t250489657::get_offset_of__RequestedPermissionsState_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (GallerySaveResult_t1643856950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[1] = 
{
	GallerySaveResult_t1643856950::get_offset_of__imagePath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (AN_SoomlaEventType_t3710443565)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2529[5] = 
{
	AN_SoomlaEventType_t3710443565::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (AN_SoomlaGrow_t3055280006), -1, sizeof(AN_SoomlaGrow_t3055280006_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2530[7] = 
{
	AN_SoomlaGrow_t3055280006_StaticFields::get_offset_of__IsInitialized_4(),
	AN_SoomlaGrow_t3055280006_StaticFields::get_offset_of_ActionInitialized_5(),
	AN_SoomlaGrow_t3055280006_StaticFields::get_offset_of_ActionConnected_6(),
	AN_SoomlaGrow_t3055280006_StaticFields::get_offset_of_ActionDisconnected_7(),
	AN_SoomlaGrow_t3055280006_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
	AN_SoomlaGrow_t3055280006_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
	AN_SoomlaGrow_t3055280006_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (AN_SoomlaProxy_t2595430893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (AN_SoomlaSocialProvider_t1447552007)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2532[5] = 
{
	AN_SoomlaSocialProvider_t1447552007::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (AndroidABAddress_t3525326208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[7] = 
{
	AndroidABAddress_t3525326208::get_offset_of_poBox_0(),
	AndroidABAddress_t3525326208::get_offset_of_street_1(),
	AndroidABAddress_t3525326208::get_offset_of_city_2(),
	AndroidABAddress_t3525326208::get_offset_of_state_3(),
	AndroidABAddress_t3525326208::get_offset_of_postalCode_4(),
	AndroidABAddress_t3525326208::get_offset_of_country_5(),
	AndroidABAddress_t3525326208::get_offset_of_type_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (AndroidABChat_t2851498654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2534[2] = 
{
	AndroidABChat_t2851498654::get_offset_of_name_0(),
	AndroidABChat_t2851498654::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (AndroidABEmail_t2938164884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[2] = 
{
	AndroidABEmail_t2938164884::get_offset_of_email_0(),
	AndroidABEmail_t2938164884::get_offset_of_emailType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (AndroidABOrganization_t1539209357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2536[2] = 
{
	AndroidABOrganization_t1539209357::get_offset_of_name_0(),
	AndroidABOrganization_t1539209357::get_offset_of_title_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (AndroidContactInfo_t2118672179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[8] = 
{
	AndroidContactInfo_t2118672179::get_offset_of_name_0(),
	AndroidContactInfo_t2118672179::get_offset_of_phone_1(),
	AndroidContactInfo_t2118672179::get_offset_of_note_2(),
	AndroidContactInfo_t2118672179::get_offset_of_email_3(),
	AndroidContactInfo_t2118672179::get_offset_of_chat_4(),
	AndroidContactInfo_t2118672179::get_offset_of_address_5(),
	AndroidContactInfo_t2118672179::get_offset_of_organization_6(),
	AndroidContactInfo_t2118672179::get_offset_of_photo_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (AndroidImagePickResult_t1791733552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[3] = 
{
	AndroidImagePickResult_t1791733552::get_offset_of__Image_2(),
	AndroidImagePickResult_t1791733552::get_offset_of__ImagePath_3(),
	AndroidImagePickResult_t1791733552::get_offset_of_exifOrientation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (GP_AppInvitesController_t2424429497), -1, sizeof(GP_AppInvitesController_t2424429497_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2539[4] = 
{
	GP_AppInvitesController_t2424429497_StaticFields::get_offset_of_ActionAppInvitesSent_4(),
	GP_AppInvitesController_t2424429497_StaticFields::get_offset_of_ActionAppInviteRetrieved_5(),
	GP_AppInvitesController_t2424429497_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	GP_AppInvitesController_t2424429497_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (GoogleCloudManager_t1208504825), -1, sizeof(GoogleCloudManager_t1208504825_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2540[15] = 
{
	GoogleCloudManager_t1208504825::get_offset_of__maxStateSize_4(),
	GoogleCloudManager_t1208504825::get_offset_of__maxNumKeys_5(),
	GoogleCloudManager_t1208504825::get_offset_of__states_6(),
	GoogleCloudManager_t1208504825_StaticFields::get_offset_of_ActionStateDeleted_7(),
	GoogleCloudManager_t1208504825_StaticFields::get_offset_of_ActionStateUpdated_8(),
	GoogleCloudManager_t1208504825_StaticFields::get_offset_of_ActionStateLoaded_9(),
	GoogleCloudManager_t1208504825_StaticFields::get_offset_of_ActionStateResolved_10(),
	GoogleCloudManager_t1208504825_StaticFields::get_offset_of_ActionStateConflict_11(),
	GoogleCloudManager_t1208504825_StaticFields::get_offset_of_ActionAllStatesLoaded_12(),
	GoogleCloudManager_t1208504825_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_13(),
	GoogleCloudManager_t1208504825_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_14(),
	GoogleCloudManager_t1208504825_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_15(),
	GoogleCloudManager_t1208504825_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_16(),
	GoogleCloudManager_t1208504825_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_17(),
	GoogleCloudManager_t1208504825_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (GoogleCloudMessageService_t984991750), -1, sizeof(GoogleCloudMessageService_t984991750_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2541[12] = 
{
	GoogleCloudMessageService_t984991750::get_offset_of__lastMessage_4(),
	GoogleCloudMessageService_t984991750::get_offset_of__registrationId_5(),
	GoogleCloudMessageService_t984991750_StaticFields::get_offset_of_ActionCouldMessageLoaded_6(),
	GoogleCloudMessageService_t984991750_StaticFields::get_offset_of_ActionCMDRegistrationResult_7(),
	GoogleCloudMessageService_t984991750_StaticFields::get_offset_of_ActionGCMPushReceived_8(),
	GoogleCloudMessageService_t984991750_StaticFields::get_offset_of_ActionGameThriveNotificationReceived_9(),
	GoogleCloudMessageService_t984991750_StaticFields::get_offset_of_ActionParsePushReceived_10(),
	GoogleCloudMessageService_t984991750_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
	GoogleCloudMessageService_t984991750_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_12(),
	GoogleCloudMessageService_t984991750_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_13(),
	GoogleCloudMessageService_t984991750_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_14(),
	GoogleCloudMessageService_t984991750_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (GooglePlayConnection_t348367589), -1, sizeof(GooglePlayConnection_t348367589_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2542[10] = 
{
	GooglePlayConnection_t348367589::get_offset_of__IsInitialized_4(),
	GooglePlayConnection_t348367589_StaticFields::get_offset_of__State_5(),
	GooglePlayConnection_t348367589_StaticFields::get_offset_of_ActionConnectionResultReceived_6(),
	GooglePlayConnection_t348367589_StaticFields::get_offset_of_ActionConnectionStateChanged_7(),
	GooglePlayConnection_t348367589_StaticFields::get_offset_of_ActionPlayerConnected_8(),
	GooglePlayConnection_t348367589_StaticFields::get_offset_of_ActionPlayerDisconnected_9(),
	GooglePlayConnection_t348367589_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_10(),
	GooglePlayConnection_t348367589_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
	GooglePlayConnection_t348367589_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_12(),
	GooglePlayConnection_t348367589_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (GooglePlayEvents_t1613959644), -1, sizeof(GooglePlayEvents_t1613959644_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2543[3] = 
{
	GooglePlayEvents_t1613959644::get_offset_of_OnEventsLoaded_4(),
	GooglePlayEvents_t1613959644::get_offset_of__Events_5(),
	GooglePlayEvents_t1613959644_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (GooglePlayInvitationManager_t2586337437), -1, sizeof(GooglePlayInvitationManager_t2586337437_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2544[10] = 
{
	GooglePlayInvitationManager_t2586337437_StaticFields::get_offset_of_ActionInvitationReceived_4(),
	GooglePlayInvitationManager_t2586337437_StaticFields::get_offset_of_ActionInvitationAccepted_5(),
	GooglePlayInvitationManager_t2586337437_StaticFields::get_offset_of_ActionInvitationsListLoaded_6(),
	GooglePlayInvitationManager_t2586337437_StaticFields::get_offset_of_ActionInvitationInboxClosed_7(),
	GooglePlayInvitationManager_t2586337437_StaticFields::get_offset_of_ActionInvitationRemoved_8(),
	GooglePlayInvitationManager_t2586337437_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
	GooglePlayInvitationManager_t2586337437_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_10(),
	GooglePlayInvitationManager_t2586337437_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
	GooglePlayInvitationManager_t2586337437_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_12(),
	GooglePlayInvitationManager_t2586337437_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (GooglePlayManager_t746138120), -1, sizeof(GooglePlayManager_t746138120_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2545[32] = 
{
	GooglePlayManager_t746138120::get_offset_of__player_4(),
	GooglePlayManager_t746138120::get_offset_of__players_5(),
	GooglePlayManager_t746138120::get_offset_of__friendsList_6(),
	GooglePlayManager_t746138120::get_offset_of__deviceGoogleAccountList_7(),
	GooglePlayManager_t746138120::get_offset_of__gameRequests_8(),
	GooglePlayManager_t746138120::get_offset_of__loadedAuthToken_9(),
	GooglePlayManager_t746138120::get_offset_of__currentAccount_10(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of__IsLeaderboardsDataLoaded_11(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_ActionScoreSubmited_12(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_ActionScoresListLoaded_13(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_ActionLeaderboardsLoaded_14(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_ActionAchievementUpdated_15(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_ActionFriendsListLoaded_16(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_ActionAchievementsLoaded_17(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_ActionSendGiftResultReceived_18(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_ActionRequestsInboxDialogDismissed_19(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_ActionPendingGameRequestsDetected_20(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_ActionGameRequestsAccepted_21(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_ActionAvailableDeviceAccountsLoaded_22(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_ActionOAuthTokenLoaded_23(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_U3CU3Ef__amU24cache14_24(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_U3CU3Ef__amU24cache15_25(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_U3CU3Ef__amU24cache16_26(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_U3CU3Ef__amU24cache17_27(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_U3CU3Ef__amU24cache18_28(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_U3CU3Ef__amU24cache19_29(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_U3CU3Ef__amU24cache1A_30(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_U3CU3Ef__amU24cache1B_31(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_32(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_U3CU3Ef__amU24cache1D_33(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_U3CU3Ef__amU24cache1E_34(),
	GooglePlayManager_t746138120_StaticFields::get_offset_of_U3CU3Ef__amU24cache1F_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (GooglePlayQuests_t1618778050), -1, sizeof(GooglePlayQuests_t1618778050_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2546[8] = 
{
	GooglePlayQuests_t1618778050::get_offset_of_OnQuestsLoaded_4(),
	GooglePlayQuests_t1618778050::get_offset_of_OnQuestsAccepted_5(),
	GooglePlayQuests_t1618778050::get_offset_of_OnQuestsCompleted_6(),
	GooglePlayQuests_t1618778050_StaticFields::get_offset_of_SELECT_ALL_QUESTS_7(),
	GooglePlayQuests_t1618778050::get_offset_of__Quests_8(),
	GooglePlayQuests_t1618778050_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
	GooglePlayQuests_t1618778050_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_10(),
	GooglePlayQuests_t1618778050_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (GooglePlayRTM_t547022790), -1, sizeof(GooglePlayRTM_t547022790_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2547[52] = 
{
	0,
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionDataRecieved_5(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionRoomUpdated_6(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionReliableMessageSent_7(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionReliableMessageDelivered_8(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionConnectedToRoom_9(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionDisconnectedFromRoom_10(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionP2PConnected_11(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionP2PDisconnected_12(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionPeerDeclined_13(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionPeerInvitedToRoom_14(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionPeerJoined_15(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionPeerLeft_16(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionPeersConnected_17(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionPeersDisconnected_18(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionRoomAutomatching_19(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionRoomConnecting_20(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionJoinedRoom_21(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionLeftRoom_22(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionRoomConnected_23(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionRoomCreated_24(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionInvitationBoxUIClosed_25(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionWatingRoomIntentClosed_26(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionInvitationReceived_27(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_ActionInvitationRemoved_28(),
	GooglePlayRTM_t547022790::get_offset_of__currentRoom_29(),
	GooglePlayRTM_t547022790::get_offset_of__invitations_30(),
	GooglePlayRTM_t547022790::get_offset_of__ReliableMassageListeners_31(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache1B_32(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_33(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache1D_34(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache1E_35(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache1F_36(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache20_37(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache21_38(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache22_39(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache23_40(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache24_41(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache25_42(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache26_43(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache27_44(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache28_45(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache29_46(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache2A_47(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache2B_48(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache2C_49(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache2D_50(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache2E_51(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache2F_52(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache30_53(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache31_54(),
	GooglePlayRTM_t547022790_StaticFields::get_offset_of_U3CU3Ef__amU24cache32_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (GooglePlaySavedGamesManager_t1475146022), -1, sizeof(GooglePlaySavedGamesManager_t1475146022_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2548[13] = 
{
	GooglePlaySavedGamesManager_t1475146022::get_offset_of__AvailableGameSaves_4(),
	GooglePlaySavedGamesManager_t1475146022_StaticFields::get_offset_of_ActionNewGameSaveRequest_5(),
	GooglePlaySavedGamesManager_t1475146022_StaticFields::get_offset_of_ActionAvailableGameSavesLoaded_6(),
	GooglePlaySavedGamesManager_t1475146022_StaticFields::get_offset_of_ActionGameSaveLoaded_7(),
	GooglePlaySavedGamesManager_t1475146022_StaticFields::get_offset_of_ActionGameSaveResult_8(),
	GooglePlaySavedGamesManager_t1475146022_StaticFields::get_offset_of_ActionConflict_9(),
	GooglePlaySavedGamesManager_t1475146022_StaticFields::get_offset_of_ActionGameSaveRemoved_10(),
	GooglePlaySavedGamesManager_t1475146022_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
	GooglePlaySavedGamesManager_t1475146022_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_12(),
	GooglePlaySavedGamesManager_t1475146022_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_13(),
	GooglePlaySavedGamesManager_t1475146022_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_14(),
	GooglePlaySavedGamesManager_t1475146022_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_15(),
	GooglePlaySavedGamesManager_t1475146022_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (GooglePlayTBM_t641131310), -1, sizeof(GooglePlayTBM_t641131310_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2549[25] = 
{
	GooglePlayTBM_t641131310::get_offset_of__Matches_4(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_ActionMatchesResultLoaded_5(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_ActionMatchInitiated_6(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_ActionMatchCanceled_7(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_ActionMatchLeaved_8(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_ActionMatchDataLoaded_9(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_ActionMatchUpdated_10(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_ActionMatchTurnFinished_11(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_ActionMatchReceived_12(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_ActionMatchRemoved_13(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_ActionMatchCreationCanceled_14(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_ActionMatchInvitationAccepted_15(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_ActionMatchInvitationDeclined_16(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_17(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_18(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_19(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_20(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_21(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_U3CU3Ef__amU24cache12_22(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_23(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_U3CU3Ef__amU24cache14_24(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_U3CU3Ef__amU24cache15_25(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_U3CU3Ef__amU24cache16_26(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_U3CU3Ef__amU24cache17_27(),
	GooglePlayTBM_t641131310_StaticFields::get_offset_of_U3CU3Ef__amU24cache18_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (GooglePlusAPI_t3074122137), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (GP_AppInvite_t1121244750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[3] = 
{
	GP_AppInvite_t1121244750::get_offset_of__Id_0(),
	GP_AppInvite_t1121244750::get_offset_of__DeepLink_1(),
	GP_AppInvite_t1121244750::get_offset_of__IsOpenedFromPlayStore_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (GP_AppInviteBuilder_t1976720519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[1] = 
{
	GP_AppInviteBuilder_t1976720519::get_offset_of__Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (AN_PlusBtnAnnotation_t1944163283)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2553[4] = 
{
	AN_PlusBtnAnnotation_t1944163283::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (AN_PlusBtnSize_t3788325417)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2554[5] = 
{
	AN_PlusBtnSize_t3788325417::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (AN_PlusButton_t1370758440), -1, sizeof(AN_PlusButton_t1370758440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2555[8] = 
{
	AN_PlusButton_t1370758440::get_offset_of__ButtonId_0(),
	AN_PlusButton_t1370758440::get_offset_of__anchor_1(),
	AN_PlusButton_t1370758440::get_offset_of__x_2(),
	AN_PlusButton_t1370758440::get_offset_of__y_3(),
	AN_PlusButton_t1370758440::get_offset_of__IsShowed_4(),
	AN_PlusButton_t1370758440::get_offset_of_ButtonClicked_5(),
	AN_PlusButton_t1370758440_StaticFields::get_offset_of__nextId_6(),
	AN_PlusButton_t1370758440_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (AN_PlusButtonsManager_t882255532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[1] = 
{
	AN_PlusButtonsManager_t882255532::get_offset_of_buttons_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (AN_DeepLinkingManager_t638324589), -1, sizeof(AN_DeepLinkingManager_t638324589_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2557[1] = 
{
	AN_DeepLinkingManager_t638324589_StaticFields::get_offset_of_OnDeepLinkReceived_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (AN_PlusShareBuilder_t211572262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[5] = 
{
	0,
	AN_PlusShareBuilder_t211572262::get_offset_of_listenerObject_1(),
	AN_PlusShareBuilder_t211572262::get_offset_of_message_2(),
	AN_PlusShareBuilder_t211572262::get_offset_of_images_3(),
	AN_PlusShareBuilder_t211572262::get_offset_of_OnPlusShareResult_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (AN_PlusShareListener_t257800803), -1, sizeof(AN_PlusShareListener_t257800803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2559[2] = 
{
	AN_PlusShareListener_t257800803::get_offset_of_builderCallback_2(),
	AN_PlusShareListener_t257800803_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (PlusShareCallback_t2327980784), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (AN_PlusShareResult_t3488688014), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (GP_RetrieveAppInviteResult_t18485229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2562[1] = 
{
	GP_RetrieveAppInviteResult_t18485229::get_offset_of__AppInvite_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (GP_SendAppInvitesResult_t3999077544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[1] = 
{
	GP_SendAppInvitesResult_t3999077544::get_offset_of__InvitationIds_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (GooglePlayUtils_t968630402), -1, sizeof(GooglePlayUtils_t968630402_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2564[2] = 
{
	GooglePlayUtils_t968630402_StaticFields::get_offset_of_ActionAdvertisingIdLoaded_4(),
	GooglePlayUtils_t968630402_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (AndroidDialog_t3283474837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[2] = 
{
	AndroidDialog_t3283474837::get_offset_of_yes_6(),
	AndroidDialog_t3283474837::get_offset_of_no_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (AndroidMessage_t2504997174), -1, sizeof(AndroidMessage_t2504997174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2566[3] = 
{
	AndroidMessage_t2504997174::get_offset_of_ok_6(),
	AndroidMessage_t2504997174::get_offset_of_OnComplete_7(),
	AndroidMessage_t2504997174_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (AndroidRateUsPopUp_t1491925263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[4] = 
{
	AndroidRateUsPopUp_t1491925263::get_offset_of_yes_6(),
	AndroidRateUsPopUp_t1491925263::get_offset_of_later_7(),
	AndroidRateUsPopUp_t1491925263::get_offset_of_no_8(),
	AndroidRateUsPopUp_t1491925263::get_offset_of_url_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (BaseAndroidPopup_t3200634462), -1, sizeof(BaseAndroidPopup_t3200634462_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2568[4] = 
{
	BaseAndroidPopup_t3200634462::get_offset_of_title_2(),
	BaseAndroidPopup_t3200634462::get_offset_of_message_3(),
	BaseAndroidPopup_t3200634462::get_offset_of_ActionComplete_4(),
	BaseAndroidPopup_t3200634462_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (AN_APIManager_t1324084101), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (AN_API_NAME_t4153298650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[21] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (AndroidAdMobBanner_t187179330), -1, sizeof(AndroidAdMobBanner_t187179330_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2571[4] = 
{
	AndroidAdMobBanner_t187179330::get_offset_of_BannersUnityId_2(),
	AndroidAdMobBanner_t187179330::get_offset_of_size_3(),
	AndroidAdMobBanner_t187179330::get_offset_of_anchor_4(),
	AndroidAdMobBanner_t187179330_StaticFields::get_offset_of__refisterdBanners_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (AndroidAdMobBannerInterstitial_t1805481188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2572[1] = 
{
	AndroidAdMobBannerInterstitial_t1805481188::get_offset_of_InterstitialUnityId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (AndroidGoogleAdsExample_t2727429144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[18] = 
{
	0,
	0,
	AndroidGoogleAdsExample_t2727429144::get_offset_of_banner1_4(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_banner2_5(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_IsInterstisialsAdReady_6(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_ShowIntersButton_7(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_b1CreateButtons_8(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_b1Hide_9(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_b1Show_10(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_b1Refresh_11(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_ChangePost1_12(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_ChangePost2_13(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_b1Destroy_14(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_b2CreateButtons_15(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_b2Hide_16(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_b2Show_17(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_b2Refresh_18(),
	AndroidGoogleAdsExample_t2727429144::get_offset_of_b2Destroy_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (AndroidGoogleAdsExample_old_t3329868158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[7] = 
{
	0,
	0,
	AndroidGoogleAdsExample_old_t3329868158::get_offset_of_style_4(),
	AndroidGoogleAdsExample_old_t3329868158::get_offset_of_style2_5(),
	AndroidGoogleAdsExample_old_t3329868158::get_offset_of_banner1_6(),
	AndroidGoogleAdsExample_old_t3329868158::get_offset_of_banner2_7(),
	AndroidGoogleAdsExample_old_t3329868158::get_offset_of_IsInterstisialsAdReady_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (AndroidNativeExampleBase_t3916638757), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (BillingExample_t2738956801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[2] = 
{
	BillingExample_t2738956801::get_offset_of_initButton_2(),
	BillingExample_t2738956801::get_offset_of_initBoundButtons_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (GPaymnetManagerExample_t841599384), -1, sizeof(GPaymnetManagerExample_t841599384_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2577[5] = 
{
	0,
	0,
	0,
	0,
	GPaymnetManagerExample_t841599384_StaticFields::get_offset_of__isInited_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (GameBillingManagerExample_t1056429950), -1, sizeof(GameBillingManagerExample_t1056429950_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2578[6] = 
{
	0,
	0,
	GameBillingManagerExample_t1056429950_StaticFields::get_offset_of__isInited_4(),
	GameBillingManagerExample_t1056429950_StaticFields::get_offset_of_ListnersAdded_5(),
	GameBillingManagerExample_t1056429950_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_6(),
	GameBillingManagerExample_t1056429950_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (GameDataExample_t175804752), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (GamePlayExample_t3564348922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[5] = 
{
	GamePlayExample_t3564348922::get_offset_of_objectToEnbaleOnInit_2(),
	GamePlayExample_t3564348922::get_offset_of_objectToDisableOnInit_3(),
	GamePlayExample_t3564348922::get_offset_of_initBoundButtons_4(),
	GamePlayExample_t3564348922::get_offset_of_coinsLable_5(),
	GamePlayExample_t3564348922::get_offset_of_boostLabel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (CustomLeaderboardFiledsHolder_t4123707333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[5] = 
{
	CustomLeaderboardFiledsHolder_t4123707333::get_offset_of_rank_2(),
	CustomLeaderboardFiledsHolder_t4123707333::get_offset_of_score_3(),
	CustomLeaderboardFiledsHolder_t4123707333::get_offset_of_playerId_4(),
	CustomLeaderboardFiledsHolder_t4123707333::get_offset_of_playerName_5(),
	CustomLeaderboardFiledsHolder_t4123707333::get_offset_of_avatar_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (CustomPlayerUIRow_t1758969648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[5] = 
{
	CustomPlayerUIRow_t1758969648::get_offset_of_playerId_2(),
	CustomPlayerUIRow_t1758969648::get_offset_of_playerName_3(),
	CustomPlayerUIRow_t1758969648::get_offset_of_avatar_4(),
	CustomPlayerUIRow_t1758969648::get_offset_of_hasIcon_5(),
	CustomPlayerUIRow_t1758969648::get_offset_of_hasImage_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (CSharpAPIHelper_t2548466795), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (RTM_Game_Example_t1461391879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[14] = 
{
	RTM_Game_Example_t1461391879::get_offset_of_avatar_2(),
	RTM_Game_Example_t1461391879::get_offset_of_hi_3(),
	RTM_Game_Example_t1461391879::get_offset_of_playerLabel_4(),
	RTM_Game_Example_t1461391879::get_offset_of_gameState_5(),
	RTM_Game_Example_t1461391879::get_offset_of_parisipants_6(),
	RTM_Game_Example_t1461391879::get_offset_of_connectButton_7(),
	RTM_Game_Example_t1461391879::get_offset_of_helloButton_8(),
	RTM_Game_Example_t1461391879::get_offset_of_leaveRoomButton_9(),
	RTM_Game_Example_t1461391879::get_offset_of_showRoomButton_10(),
	RTM_Game_Example_t1461391879::get_offset_of_ConnectionDependedntButtons_11(),
	RTM_Game_Example_t1461391879::get_offset_of_patricipants_12(),
	RTM_Game_Example_t1461391879::get_offset_of_friends_13(),
	RTM_Game_Example_t1461391879::get_offset_of_defaulttexture_14(),
	RTM_Game_Example_t1461391879::get_offset_of_inviteId_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (TBM_Game_Example_t857044347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[11] = 
{
	TBM_Game_Example_t857044347::get_offset_of_avatar_2(),
	TBM_Game_Example_t857044347::get_offset_of_hi_3(),
	TBM_Game_Example_t857044347::get_offset_of_playerLabel_4(),
	TBM_Game_Example_t857044347::get_offset_of_gameState_5(),
	TBM_Game_Example_t857044347::get_offset_of_parisipants_6(),
	TBM_Game_Example_t857044347::get_offset_of_connectButton_7(),
	TBM_Game_Example_t857044347::get_offset_of_helloButton_8(),
	TBM_Game_Example_t857044347::get_offset_of_leaveRoomButton_9(),
	TBM_Game_Example_t857044347::get_offset_of_showRoomButton_10(),
	TBM_Game_Example_t857044347::get_offset_of_ConnectionDependedntButtons_11(),
	TBM_Game_Example_t857044347::get_offset_of_patrisipants_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (ANNativeEventsExample_t3453490363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (AddressBookExample_t3542149761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[7] = 
{
	AddressBookExample_t3542149761::get_offset_of__name_2(),
	AddressBookExample_t3542149761::get_offset_of__phone_3(),
	AddressBookExample_t3542149761::get_offset_of__note_4(),
	AddressBookExample_t3542149761::get_offset_of__email_5(),
	AddressBookExample_t3542149761::get_offset_of__chat_6(),
	AddressBookExample_t3542149761::get_offset_of__address_7(),
	AddressBookExample_t3542149761::get_offset_of_all_contacts_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (AnOtherFeaturesPreview_t3806064552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[2] = 
{
	AnOtherFeaturesPreview_t3806064552::get_offset_of_image_2(),
	AnOtherFeaturesPreview_t3806064552::get_offset_of_helloWorldTexture_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (AnalyticsUseExample_t751986731), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (AndroidPopUpExamples_t3185211868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[2] = 
{
	AndroidPopUpExamples_t3185211868::get_offset_of_rateText_2(),
	AndroidPopUpExamples_t3185211868::get_offset_of_rateUrl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (NotificationsExample_t734553030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2591[2] = 
{
	NotificationsExample_t734553030::get_offset_of_bigPicture_2(),
	NotificationsExample_t734553030::get_offset_of_LastNotificationId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (PermissionsAPIExample_t2362515826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (PlusButtonsAPIExample_t2847602409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2593[3] = 
{
	PlusButtonsAPIExample_t2847602409::get_offset_of_Abuttons_2(),
	PlusButtonsAPIExample_t2847602409::get_offset_of_PlusButton_3(),
	PlusButtonsAPIExample_t2847602409::get_offset_of_PlusUrl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (PreviewSceneController_t1557748400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[1] = 
{
	PreviewSceneController_t1557748400::get_offset_of_title_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (GoogleCloudUseExample_t2949985613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[2] = 
{
	GoogleCloudUseExample_t2949985613::get_offset_of_hideOnConnect_2(),
	GoogleCloudUseExample_t2949985613::get_offset_of_showOnConnect_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (PlayServicFridnsLoadExample_New_t1740499043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[6] = 
{
	PlayServicFridnsLoadExample_New_t1740499043::get_offset_of_avatar_2(),
	PlayServicFridnsLoadExample_New_t1740499043::get_offset_of_playerLabel_3(),
	PlayServicFridnsLoadExample_New_t1740499043::get_offset_of_connectButton_4(),
	PlayServicFridnsLoadExample_New_t1740499043::get_offset_of_defaulttexture_5(),
	PlayServicFridnsLoadExample_New_t1740499043::get_offset_of_ConnectionDependedntButtons_6(),
	PlayServicFridnsLoadExample_New_t1740499043::get_offset_of_rows_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (PlayServiceCustomLBExample_t1198012866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2597[17] = 
{
	0,
	PlayServiceCustomLBExample_t1198012866::get_offset_of_avatar_3(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_defaulttexture_4(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_connectButton_5(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_playerLabel_6(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_GlobalButton_7(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_LocalButton_8(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_AllTimeButton_9(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_WeekButton_10(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_TodayButton_11(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_SubmitScoreButton_12(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_ConnectionDependedntButtons_13(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_lines_14(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_loadedLeaderBoard_15(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_displayCollection_16(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_displayTime_17(),
	PlayServiceCustomLBExample_t1198012866::get_offset_of_score_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (PlayServiceExample_t1159077439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[22] = 
{
	0,
	0,
	0,
	0,
	PlayServiceExample_t1159077439::get_offset_of_score_6(),
	PlayServiceExample_t1159077439::get_offset_of_avatar_7(),
	PlayServiceExample_t1159077439::get_offset_of_defaulttexture_8(),
	PlayServiceExample_t1159077439::get_offset_of_pieIcon_9(),
	PlayServiceExample_t1159077439::get_offset_of_connectButton_10(),
	PlayServiceExample_t1159077439::get_offset_of_scoreSubmit_11(),
	PlayServiceExample_t1159077439::get_offset_of_playerLabel_12(),
	PlayServiceExample_t1159077439::get_offset_of_ConnectionDependedntButtons_13(),
	PlayServiceExample_t1159077439::get_offset_of_a_id_14(),
	PlayServiceExample_t1159077439::get_offset_of_a_name_15(),
	PlayServiceExample_t1159077439::get_offset_of_a_descr_16(),
	PlayServiceExample_t1159077439::get_offset_of_a_type_17(),
	PlayServiceExample_t1159077439::get_offset_of_a_state_18(),
	PlayServiceExample_t1159077439::get_offset_of_a_steps_19(),
	PlayServiceExample_t1159077439::get_offset_of_a_total_20(),
	PlayServiceExample_t1159077439::get_offset_of_b_id_21(),
	PlayServiceExample_t1159077439::get_offset_of_b_name_22(),
	PlayServiceExample_t1159077439::get_offset_of_b_all_time_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (QuestAndEventsExample_t3324016540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[8] = 
{
	0,
	0,
	QuestAndEventsExample_t3324016540::get_offset_of_avatar_4(),
	QuestAndEventsExample_t3324016540::get_offset_of_defaulttexture_5(),
	QuestAndEventsExample_t3324016540::get_offset_of_pieIcon_6(),
	QuestAndEventsExample_t3324016540::get_offset_of_connectButton_7(),
	QuestAndEventsExample_t3324016540::get_offset_of_playerLabel_8(),
	QuestAndEventsExample_t3324016540::get_offset_of_ConnectionDependedntButtons_9(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
