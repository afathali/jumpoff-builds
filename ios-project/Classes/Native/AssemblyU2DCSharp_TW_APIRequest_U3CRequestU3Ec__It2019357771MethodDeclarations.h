﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TW_APIRequest/<Request>c__Iterator9
struct U3CRequestU3Ec__Iterator9_t2019357771;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TW_APIRequest/<Request>c__Iterator9::.ctor()
extern "C"  void U3CRequestU3Ec__Iterator9__ctor_m363089036 (U3CRequestU3Ec__Iterator9_t2019357771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TW_APIRequest/<Request>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRequestU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2859068134 (U3CRequestU3Ec__Iterator9_t2019357771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TW_APIRequest/<Request>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRequestU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m4267429822 (U3CRequestU3Ec__Iterator9_t2019357771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TW_APIRequest/<Request>c__Iterator9::MoveNext()
extern "C"  bool U3CRequestU3Ec__Iterator9_MoveNext_m2746364640 (U3CRequestU3Ec__Iterator9_t2019357771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_APIRequest/<Request>c__Iterator9::Dispose()
extern "C"  void U3CRequestU3Ec__Iterator9_Dispose_m2247813093 (U3CRequestU3Ec__Iterator9_t2019357771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_APIRequest/<Request>c__Iterator9::Reset()
extern "C"  void U3CRequestU3Ec__Iterator9_Reset_m2730130711 (U3CRequestU3Ec__Iterator9_t2019357771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
