﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_TBM_Participant
struct GK_TBM_Participant_t3803955090;
// System.String
struct String_t;
// GK_Player
struct GK_Player_t2782008294;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TurnBasedMatchOut2242380984.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TurnBasedParticip2126479626.h"

// System.Void GK_TBM_Participant::.ctor(System.String,System.String,System.String,System.String,System.String)
extern "C"  void GK_TBM_Participant__ctor_m2617185167 (GK_TBM_Participant_t3803955090 * __this, String_t* ___playerId0, String_t* ___status1, String_t* ___outcome2, String_t* ___timeoutDate3, String_t* ___lastTurnDate4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_TBM_Participant::SetOutcome(GK_TurnBasedMatchOutcome)
extern "C"  void GK_TBM_Participant_SetOutcome_m992397011 (GK_TBM_Participant_t3803955090 * __this, int32_t ___outcome0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_TBM_Participant::SetMatchId(System.String)
extern "C"  void GK_TBM_Participant_SetMatchId_m2949723795 (GK_TBM_Participant_t3803955090 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_TBM_Participant::get_PlayerId()
extern "C"  String_t* GK_TBM_Participant_get_PlayerId_m3280637819 (GK_TBM_Participant_t3803955090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_Player GK_TBM_Participant::get_Player()
extern "C"  GK_Player_t2782008294 * GK_TBM_Participant_get_Player_m3899171040 (GK_TBM_Participant_t3803955090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_TBM_Participant::get_MatchId()
extern "C"  String_t* GK_TBM_Participant_get_MatchId_m1932577599 (GK_TBM_Participant_t3803955090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime GK_TBM_Participant::get_TimeoutDate()
extern "C"  DateTime_t693205669  GK_TBM_Participant_get_TimeoutDate_m2663778324 (GK_TBM_Participant_t3803955090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime GK_TBM_Participant::get_LastTurnDate()
extern "C"  DateTime_t693205669  GK_TBM_Participant_get_LastTurnDate_m574532760 (GK_TBM_Participant_t3803955090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_TurnBasedParticipantStatus GK_TBM_Participant::get_Status()
extern "C"  int32_t GK_TBM_Participant_get_Status_m1792793115 (GK_TBM_Participant_t3803955090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_TurnBasedMatchOutcome GK_TBM_Participant::get_MatchOutcome()
extern "C"  int32_t GK_TBM_Participant_get_MatchOutcome_m1717246970 (GK_TBM_Participant_t3803955090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
