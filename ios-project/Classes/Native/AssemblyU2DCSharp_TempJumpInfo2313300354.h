﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TempJumpInfo
struct  TempJumpInfo_t2313300354  : public Il2CppObject
{
public:
	// System.Single TempJumpInfo::<xPos>k__BackingField
	float ___U3CxPosU3Ek__BackingField_0;
	// System.Single TempJumpInfo::<yPos>k__BackingField
	float ___U3CyPosU3Ek__BackingField_1;
	// System.Single TempJumpInfo::<rotation>k__BackingField
	float ___U3CrotationU3Ek__BackingField_2;
	// System.Int32 TempJumpInfo::<jumpNumber>k__BackingField
	int32_t ___U3CjumpNumberU3Ek__BackingField_3;
	// System.Int32 TempJumpInfo::<jumpType>k__BackingField
	int32_t ___U3CjumpTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CxPosU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TempJumpInfo_t2313300354, ___U3CxPosU3Ek__BackingField_0)); }
	inline float get_U3CxPosU3Ek__BackingField_0() const { return ___U3CxPosU3Ek__BackingField_0; }
	inline float* get_address_of_U3CxPosU3Ek__BackingField_0() { return &___U3CxPosU3Ek__BackingField_0; }
	inline void set_U3CxPosU3Ek__BackingField_0(float value)
	{
		___U3CxPosU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CyPosU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TempJumpInfo_t2313300354, ___U3CyPosU3Ek__BackingField_1)); }
	inline float get_U3CyPosU3Ek__BackingField_1() const { return ___U3CyPosU3Ek__BackingField_1; }
	inline float* get_address_of_U3CyPosU3Ek__BackingField_1() { return &___U3CyPosU3Ek__BackingField_1; }
	inline void set_U3CyPosU3Ek__BackingField_1(float value)
	{
		___U3CyPosU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CrotationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TempJumpInfo_t2313300354, ___U3CrotationU3Ek__BackingField_2)); }
	inline float get_U3CrotationU3Ek__BackingField_2() const { return ___U3CrotationU3Ek__BackingField_2; }
	inline float* get_address_of_U3CrotationU3Ek__BackingField_2() { return &___U3CrotationU3Ek__BackingField_2; }
	inline void set_U3CrotationU3Ek__BackingField_2(float value)
	{
		___U3CrotationU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CjumpNumberU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TempJumpInfo_t2313300354, ___U3CjumpNumberU3Ek__BackingField_3)); }
	inline int32_t get_U3CjumpNumberU3Ek__BackingField_3() const { return ___U3CjumpNumberU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CjumpNumberU3Ek__BackingField_3() { return &___U3CjumpNumberU3Ek__BackingField_3; }
	inline void set_U3CjumpNumberU3Ek__BackingField_3(int32_t value)
	{
		___U3CjumpNumberU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CjumpTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TempJumpInfo_t2313300354, ___U3CjumpTypeU3Ek__BackingField_4)); }
	inline int32_t get_U3CjumpTypeU3Ek__BackingField_4() const { return ___U3CjumpTypeU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CjumpTypeU3Ek__BackingField_4() { return &___U3CjumpTypeU3Ek__BackingField_4; }
	inline void set_U3CjumpTypeU3Ek__BackingField_4(int32_t value)
	{
		___U3CjumpTypeU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
