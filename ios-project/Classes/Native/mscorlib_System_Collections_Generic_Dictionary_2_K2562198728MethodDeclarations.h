﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4180772701MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2296065357(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2562198728 *, Dictionary_2_t78700957 *, const MethodInfo*))KeyCollection__ctor_m3555561037_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2489959043(__this, ___item0, method) ((  void (*) (KeyCollection_t2562198728 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3743129415_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3263763342(__this, method) ((  void (*) (KeyCollection_t2562198728 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m522538414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1509943685(__this, ___item0, method) ((  bool (*) (KeyCollection_t2562198728 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3840423669_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2227775702(__this, ___item0, method) ((  bool (*) (KeyCollection_t2562198728 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2545700230_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1621249810(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2562198728 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3763330846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3590729664(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2562198728 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m4142454252_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1979803939(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2562198728 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1589406383_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2504286848(__this, method) ((  bool (*) (KeyCollection_t2562198728 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1119857328_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1767278900(__this, method) ((  bool (*) (KeyCollection_t2562198728 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1423289640_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3986759706(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2562198728 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3802319686_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3439789994(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2562198728 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4056130090_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3304020437(__this, method) ((  Enumerator_t2768204395  (*) (KeyCollection_t2562198728 *, const MethodInfo*))KeyCollection_GetEnumerator_m627495629_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::get_Count()
#define KeyCollection_get_Count_m2611403164(__this, method) ((  int32_t (*) (KeyCollection_t2562198728 *, const MethodInfo*))KeyCollection_get_Count_m2942903136_gshared)(__this, method)
