﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GP_Invite>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1168122196(__this, ___l0, method) ((  void (*) (Enumerator_t3825747189 *, List_1_t4291017515 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GP_Invite>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3678863958(__this, method) ((  void (*) (Enumerator_t3825747189 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GP_Invite>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2279028778(__this, method) ((  Il2CppObject * (*) (Enumerator_t3825747189 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GP_Invite>::Dispose()
#define Enumerator_Dispose_m1061668023(__this, method) ((  void (*) (Enumerator_t3825747189 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GP_Invite>::VerifyState()
#define Enumerator_VerifyState_m2874926098(__this, method) ((  void (*) (Enumerator_t3825747189 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GP_Invite>::MoveNext()
#define Enumerator_MoveNext_m4206768845(__this, method) ((  bool (*) (Enumerator_t3825747189 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GP_Invite>::get_Current()
#define Enumerator_get_Current_m651687717(__this, method) ((  GP_Invite_t626929087 * (*) (Enumerator_t3825747189 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
