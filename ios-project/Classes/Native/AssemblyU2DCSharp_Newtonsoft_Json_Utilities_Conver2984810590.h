﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>
struct ThreadSafeStore_2_t4029904910;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils
struct  ConvertUtils_t2984810590  : public Il2CppObject
{
public:

public:
};

struct ConvertUtils_t2984810590_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>> Newtonsoft.Json.Utilities.ConvertUtils::CastConverters
	ThreadSafeStore_2_t4029904910 * ___CastConverters_0;

public:
	inline static int32_t get_offset_of_CastConverters_0() { return static_cast<int32_t>(offsetof(ConvertUtils_t2984810590_StaticFields, ___CastConverters_0)); }
	inline ThreadSafeStore_2_t4029904910 * get_CastConverters_0() const { return ___CastConverters_0; }
	inline ThreadSafeStore_2_t4029904910 ** get_address_of_CastConverters_0() { return &___CastConverters_0; }
	inline void set_CastConverters_0(ThreadSafeStore_2_t4029904910 * value)
	{
		___CastConverters_0 = value;
		Il2CppCodeGenWriteBarrier(&___CastConverters_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
