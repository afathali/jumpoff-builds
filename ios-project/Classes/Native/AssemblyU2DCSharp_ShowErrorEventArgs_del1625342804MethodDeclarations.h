﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShowErrorEventArgs/del
struct del_t1625342804;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ShowErrorEventArgs/del::.ctor(System.Object,System.IntPtr)
extern "C"  void del__ctor_m3045145929 (del_t1625342804 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowErrorEventArgs/del::Invoke()
extern "C"  void del_Invoke_m3995252289 (del_t1625342804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ShowErrorEventArgs/del::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * del_BeginInvoke_m1415913338 (del_t1625342804 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowErrorEventArgs/del::EndInvoke(System.IAsyncResult)
extern "C"  void del_EndInvoke_m713599963 (del_t1625342804 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
