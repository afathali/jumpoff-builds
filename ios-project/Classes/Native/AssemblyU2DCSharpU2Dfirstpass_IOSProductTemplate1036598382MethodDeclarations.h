﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSProductTemplate
struct IOSProductTemplate_t1036598382;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_InAppType3300503503.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_InAppPriceTier570012276.h"

// System.Void IOSProductTemplate::.ctor()
extern "C"  void IOSProductTemplate__ctor_m1935821683 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSProductTemplate::UpdatePriceByTier()
extern "C"  void IOSProductTemplate_UpdatePriceByTier_m1527900916 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSProductTemplate::get_Id()
extern "C"  String_t* IOSProductTemplate_get_Id_m1138061718 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSProductTemplate::set_Id(System.String)
extern "C"  void IOSProductTemplate_set_Id_m1285265489 (IOSProductTemplate_t1036598382 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSProductTemplate::get_Title()
extern "C"  String_t* IOSProductTemplate_get_Title_m211469019 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSProductTemplate::set_Title(System.String)
extern "C"  void IOSProductTemplate_set_Title_m2774168576 (IOSProductTemplate_t1036598382 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSProductTemplate::get_DisplayName()
extern "C"  String_t* IOSProductTemplate_get_DisplayName_m1500850862 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSProductTemplate::set_DisplayName(System.String)
extern "C"  void IOSProductTemplate_set_DisplayName_m1815130689 (IOSProductTemplate_t1036598382 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSProductTemplate::get_Description()
extern "C"  String_t* IOSProductTemplate_get_Description_m2698366533 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSProductTemplate::set_Description(System.String)
extern "C"  void IOSProductTemplate_set_Description_m4143404708 (IOSProductTemplate_t1036598382 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ISN_InAppType IOSProductTemplate::get_ProductType()
extern "C"  int32_t IOSProductTemplate_get_ProductType_m2462837023 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSProductTemplate::set_ProductType(ISN_InAppType)
extern "C"  void IOSProductTemplate_set_ProductType_m2302110486 (IOSProductTemplate_t1036598382 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single IOSProductTemplate::get_Price()
extern "C"  float IOSProductTemplate_get_Price_m352821953 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSProductTemplate::set_Price(System.Single)
extern "C"  void IOSProductTemplate_set_Price_m1753920522 (IOSProductTemplate_t1036598382 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IOSProductTemplate::get_PriceInMicros()
extern "C"  int32_t IOSProductTemplate_get_PriceInMicros_m3529878227 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSProductTemplate::get_LocalizedPrice()
extern "C"  String_t* IOSProductTemplate_get_LocalizedPrice_m4226478289 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSProductTemplate::set_LocalizedPrice(System.String)
extern "C"  void IOSProductTemplate_set_LocalizedPrice_m1309282810 (IOSProductTemplate_t1036598382 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSProductTemplate::get_CurrencySymbol()
extern "C"  String_t* IOSProductTemplate_get_CurrencySymbol_m208544696 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSProductTemplate::set_CurrencySymbol(System.String)
extern "C"  void IOSProductTemplate_set_CurrencySymbol_m165101645 (IOSProductTemplate_t1036598382 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSProductTemplate::get_CurrencyCode()
extern "C"  String_t* IOSProductTemplate_get_CurrencyCode_m4130948929 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSProductTemplate::set_CurrencyCode(System.String)
extern "C"  void IOSProductTemplate_set_CurrencyCode_m1994314 (IOSProductTemplate_t1036598382 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D IOSProductTemplate::get_Texture()
extern "C"  Texture2D_t3542995729 * IOSProductTemplate_get_Texture_m464139792 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSProductTemplate::set_Texture(UnityEngine.Texture2D)
extern "C"  void IOSProductTemplate_set_Texture_m736102191 (IOSProductTemplate_t1036598382 * __this, Texture2D_t3542995729 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ISN_InAppPriceTier IOSProductTemplate::get_PriceTier()
extern "C"  int32_t IOSProductTemplate_get_PriceTier_m243957736 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSProductTemplate::set_PriceTier(ISN_InAppPriceTier)
extern "C"  void IOSProductTemplate_set_PriceTier_m783367875 (IOSProductTemplate_t1036598382 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSProductTemplate::get_IsAvaliable()
extern "C"  bool IOSProductTemplate_get_IsAvaliable_m853221465 (IOSProductTemplate_t1036598382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSProductTemplate::set_IsAvaliable(System.Boolean)
extern "C"  void IOSProductTemplate_set_IsAvaliable_m3104091512 (IOSProductTemplate_t1036598382 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
