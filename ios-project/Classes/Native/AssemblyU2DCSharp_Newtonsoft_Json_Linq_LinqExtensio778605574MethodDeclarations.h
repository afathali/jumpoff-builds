﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JProperty>
struct IJEnumerable_1_t2915452442;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JObject>
struct IEnumerable_1_t570646342;
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IJEnumerable_1_t2511655056;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerable_1_t2844771058;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JProperty>
struct IEnumerable_1_t3248568444;
// Newtonsoft.Json.Linq.JObject
struct JObject_t278519297;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject278519297.h"

// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JProperty> Newtonsoft.Json.Linq.LinqExtensions::Properties(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JObject>)
extern "C"  Il2CppObject* LinqExtensions_Properties_m1763607242 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.LinqExtensions::Values(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>,System.Object)
extern "C"  Il2CppObject* LinqExtensions_Values_m2087143691 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Il2CppObject * ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.LinqExtensions::Values(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>)
extern "C"  Il2CppObject* LinqExtensions_Values_m36048713 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.LinqExtensions::AsJEnumerable(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>)
extern "C"  Il2CppObject* LinqExtensions_AsJEnumerable_m246557551 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JProperty> Newtonsoft.Json.Linq.LinqExtensions::<Properties>m__CF(Newtonsoft.Json.Linq.JObject)
extern "C"  Il2CppObject* LinqExtensions_U3CPropertiesU3Em__CF_m751233653 (Il2CppObject * __this /* static, unused */, JObject_t278519297 * ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
