﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_SocialSharingProxy
struct AN_SocialSharingProxy_t924445897;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_SocialSharingProxy::.ctor()
extern "C"  void AN_SocialSharingProxy__ctor_m3792694574 (AN_SocialSharingProxy_t924445897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SocialSharingProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_SocialSharingProxy_CallActivityFunction_m2057039971 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SocialSharingProxy::GetLaunchDeepLinkId()
extern "C"  void AN_SocialSharingProxy_GetLaunchDeepLinkId_m3011411350 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SocialSharingProxy::GooglePlusShare(System.String,System.String[])
extern "C"  void AN_SocialSharingProxy_GooglePlusShare_m196533610 (Il2CppObject * __this /* static, unused */, String_t* ___message0, StringU5BU5D_t1642385972* ___images1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SocialSharingProxy::StartGooglePlusShareIntent(System.String,System.String)
extern "C"  void AN_SocialSharingProxy_StartGooglePlusShareIntent_m505087410 (Il2CppObject * __this /* static, unused */, String_t* ___text0, String_t* ___media1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SocialSharingProxy::StartShareIntent(System.String,System.String,System.String,System.String)
extern "C"  void AN_SocialSharingProxy_StartShareIntent_m810006029 (Il2CppObject * __this /* static, unused */, String_t* ___caption0, String_t* ___message1, String_t* ___subject2, String_t* ___filters3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SocialSharingProxy::StartShareIntent(System.String,System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void AN_SocialSharingProxy_StartShareIntent_m2824721084 (Il2CppObject * __this /* static, unused */, String_t* ___caption0, String_t* ___message1, String_t* ___subject2, String_t* ___media3, String_t* ___filters4, bool ___saveImageToGallery5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SocialSharingProxy::SendMailWithImage(System.String,System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void AN_SocialSharingProxy_SendMailWithImage_m3176724069 (Il2CppObject * __this /* static, unused */, String_t* ___caption0, String_t* ___message1, String_t* ___subject2, String_t* ___email3, String_t* ___media4, bool ___saveImageToGallery5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SocialSharingProxy::SendMail(System.String,System.String,System.String,System.String)
extern "C"  void AN_SocialSharingProxy_SendMail_m161693573 (Il2CppObject * __this /* static, unused */, String_t* ___caption0, String_t* ___message1, String_t* ___subject2, String_t* ___email3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SocialSharingProxy::InstagramPostImage(System.String,System.String)
extern "C"  void AN_SocialSharingProxy_InstagramPostImage_m1031167863 (Il2CppObject * __this /* static, unused */, String_t* ___data0, String_t* ___cpation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
