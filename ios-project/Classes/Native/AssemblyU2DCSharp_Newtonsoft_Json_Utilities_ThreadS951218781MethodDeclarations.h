﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread4023353063MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Serialization.JsonContract>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m3830347235(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t951218781 *, Func_2_t4048337195 *, const MethodInfo*))ThreadSafeStore_2__ctor_m2716591129_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Serialization.JsonContract>::Get(TKey)
#define ThreadSafeStore_2_Get_m4085069343(__this, ___key0, method) ((  JsonContract_t1566984540 * (*) (ThreadSafeStore_2_t951218781 *, Type_t *, const MethodInfo*))ThreadSafeStore_2_Get_m1768706765_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Serialization.JsonContract>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m2021043763(__this, ___key0, method) ((  JsonContract_t1566984540 * (*) (ThreadSafeStore_2_t951218781 *, Type_t *, const MethodInfo*))ThreadSafeStore_2_AddValue_m4069531929_gshared)(__this, ___key0, method)
