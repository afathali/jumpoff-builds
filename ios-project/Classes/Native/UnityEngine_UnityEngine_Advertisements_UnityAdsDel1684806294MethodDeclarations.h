﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel3657054456MethodDeclarations.h"

// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define UnityAdsDelegate_2__ctor_m2496501452(__this, ___object0, ___method1, method) ((  void (*) (UnityAdsDelegate_2_t1684806294 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityAdsDelegate_2__ctor_m1114114262_gshared)(__this, ___object0, ___method1, method)
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>::Invoke(T1,T2)
#define UnityAdsDelegate_2_Invoke_m1826557381(__this, ___p10, ___p21, method) ((  void (*) (UnityAdsDelegate_2_t1684806294 *, String_t*, bool, const MethodInfo*))UnityAdsDelegate_2_Invoke_m342131961_gshared)(__this, ___p10, ___p21, method)
// System.IAsyncResult UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define UnityAdsDelegate_2_BeginInvoke_m1158440678(__this, ___p10, ___p21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (UnityAdsDelegate_2_t1684806294 *, String_t*, bool, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))UnityAdsDelegate_2_BeginInvoke_m673286156_gshared)(__this, ___p10, ___p21, ___callback2, ___object3, method)
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>::EndInvoke(System.IAsyncResult)
#define UnityAdsDelegate_2_EndInvoke_m1712196238(__this, ___result0, method) ((  void (*) (UnityAdsDelegate_2_t1684806294 *, Il2CppObject *, const MethodInfo*))UnityAdsDelegate_2_EndInvoke_m873428936_gshared)(__this, ___result0, method)
