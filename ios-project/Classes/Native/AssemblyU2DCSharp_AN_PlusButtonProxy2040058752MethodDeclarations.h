﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_PlusButtonProxy
struct AN_PlusButtonProxy_t2040058752;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_PlusButtonProxy::.ctor()
extern "C"  void AN_PlusButtonProxy__ctor_m3010899275 (AN_PlusButtonProxy_t2040058752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButtonProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_PlusButtonProxy_CallActivityFunction_m3694597702 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButtonProxy::createPlusButton(System.Int32,System.String,System.Int32,System.Int32)
extern "C"  void AN_PlusButtonProxy_createPlusButton_m3228847452 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___url1, int32_t ___size2, int32_t ___annotation3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButtonProxy::setGravity(System.Int32,System.Int32)
extern "C"  void AN_PlusButtonProxy_setGravity_m2800995335 (Il2CppObject * __this /* static, unused */, int32_t ___gravity0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButtonProxy::setPosition(System.Int32,System.Int32,System.Int32)
extern "C"  void AN_PlusButtonProxy_setPosition_m3101709853 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButtonProxy::show(System.Int32)
extern "C"  void AN_PlusButtonProxy_show_m443260293 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButtonProxy::hide(System.Int32)
extern "C"  void AN_PlusButtonProxy_hide_m1951766826 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButtonProxy::refresh(System.Int32)
extern "C"  void AN_PlusButtonProxy_refresh_m2088874783 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
