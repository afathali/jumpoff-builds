﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Toggle
struct  Toggle_t3489774764  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Toggle::UnCheckedImage
	GameObject_t1756533147 * ___UnCheckedImage_2;
	// UnityEngine.GameObject Toggle::CheckedImage
	GameObject_t1756533147 * ___CheckedImage_3;
	// UnityEngine.UI.Button Toggle::btn_toggle
	Button_t2872111280 * ___btn_toggle_4;
	// System.Boolean Toggle::IsItemChecked
	bool ___IsItemChecked_5;

public:
	inline static int32_t get_offset_of_UnCheckedImage_2() { return static_cast<int32_t>(offsetof(Toggle_t3489774764, ___UnCheckedImage_2)); }
	inline GameObject_t1756533147 * get_UnCheckedImage_2() const { return ___UnCheckedImage_2; }
	inline GameObject_t1756533147 ** get_address_of_UnCheckedImage_2() { return &___UnCheckedImage_2; }
	inline void set_UnCheckedImage_2(GameObject_t1756533147 * value)
	{
		___UnCheckedImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___UnCheckedImage_2, value);
	}

	inline static int32_t get_offset_of_CheckedImage_3() { return static_cast<int32_t>(offsetof(Toggle_t3489774764, ___CheckedImage_3)); }
	inline GameObject_t1756533147 * get_CheckedImage_3() const { return ___CheckedImage_3; }
	inline GameObject_t1756533147 ** get_address_of_CheckedImage_3() { return &___CheckedImage_3; }
	inline void set_CheckedImage_3(GameObject_t1756533147 * value)
	{
		___CheckedImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___CheckedImage_3, value);
	}

	inline static int32_t get_offset_of_btn_toggle_4() { return static_cast<int32_t>(offsetof(Toggle_t3489774764, ___btn_toggle_4)); }
	inline Button_t2872111280 * get_btn_toggle_4() const { return ___btn_toggle_4; }
	inline Button_t2872111280 ** get_address_of_btn_toggle_4() { return &___btn_toggle_4; }
	inline void set_btn_toggle_4(Button_t2872111280 * value)
	{
		___btn_toggle_4 = value;
		Il2CppCodeGenWriteBarrier(&___btn_toggle_4, value);
	}

	inline static int32_t get_offset_of_IsItemChecked_5() { return static_cast<int32_t>(offsetof(Toggle_t3489774764, ___IsItemChecked_5)); }
	inline bool get_IsItemChecked_5() const { return ___IsItemChecked_5; }
	inline bool* get_address_of_IsItemChecked_5() { return &___IsItemChecked_5; }
	inline void set_IsItemChecked_5(bool value)
	{
		___IsItemChecked_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
