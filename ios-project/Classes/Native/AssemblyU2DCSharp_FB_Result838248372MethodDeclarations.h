﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FB_Result
struct FB_Result_t838248372;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FB_Result::.ctor(System.String,System.String)
extern "C"  void FB_Result__ctor_m1364288989 (FB_Result_t838248372 * __this, String_t* ___RawData0, String_t* ___Error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FB_Result::get_IsSucceeded()
extern "C"  bool FB_Result_get_IsSucceeded_m4228074093 (FB_Result_t838248372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FB_Result::get_IsFailed()
extern "C"  bool FB_Result_get_IsFailed_m2710791913 (FB_Result_t838248372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_Result::get_RawData()
extern "C"  String_t* FB_Result_get_RawData_m1048415921 (FB_Result_t838248372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_Result::get_Error()
extern "C"  String_t* FB_Result_get_Error_m4199908021 (FB_Result_t838248372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
