﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>
struct Collection_1_t849571967;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t1368357152;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JTokenType>
struct IEnumerator_1_t3078318336;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JTokenType>
struct IList_1_t1848767814;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType1307827213.h"

// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::.ctor()
extern "C"  void Collection_1__ctor_m2124600603_gshared (Collection_1_t849571967 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2124600603(__this, method) ((  void (*) (Collection_1_t849571967 *, const MethodInfo*))Collection_1__ctor_m2124600603_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m793256928_gshared (Collection_1_t849571967 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m793256928(__this, method) ((  bool (*) (Collection_1_t849571967 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m793256928_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2132504711_gshared (Collection_1_t849571967 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2132504711(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t849571967 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2132504711_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1647237596_gshared (Collection_1_t849571967 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1647237596(__this, method) ((  Il2CppObject * (*) (Collection_1_t849571967 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1647237596_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3114735351_gshared (Collection_1_t849571967 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3114735351(__this, ___value0, method) ((  int32_t (*) (Collection_1_t849571967 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3114735351_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2173973567_gshared (Collection_1_t849571967 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2173973567(__this, ___value0, method) ((  bool (*) (Collection_1_t849571967 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2173973567_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3626881989_gshared (Collection_1_t849571967 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3626881989(__this, ___value0, method) ((  int32_t (*) (Collection_1_t849571967 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3626881989_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2361819534_gshared (Collection_1_t849571967 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2361819534(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t849571967 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2361819534_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m4043457712_gshared (Collection_1_t849571967 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m4043457712(__this, ___value0, method) ((  void (*) (Collection_1_t849571967 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m4043457712_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m4252620767_gshared (Collection_1_t849571967 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m4252620767(__this, method) ((  bool (*) (Collection_1_t849571967 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m4252620767_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1397692231_gshared (Collection_1_t849571967 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1397692231(__this, method) ((  Il2CppObject * (*) (Collection_1_t849571967 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1397692231_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1527853226_gshared (Collection_1_t849571967 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1527853226(__this, method) ((  bool (*) (Collection_1_t849571967 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1527853226_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m804047139_gshared (Collection_1_t849571967 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m804047139(__this, method) ((  bool (*) (Collection_1_t849571967 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m804047139_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2268865918_gshared (Collection_1_t849571967 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2268865918(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t849571967 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2268865918_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1806217349_gshared (Collection_1_t849571967 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1806217349(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t849571967 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1806217349_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::Add(T)
extern "C"  void Collection_1_Add_m2682890986_gshared (Collection_1_t849571967 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m2682890986(__this, ___item0, method) ((  void (*) (Collection_1_t849571967 *, int32_t, const MethodInfo*))Collection_1_Add_m2682890986_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::Clear()
extern "C"  void Collection_1_Clear_m1517527326_gshared (Collection_1_t849571967 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1517527326(__this, method) ((  void (*) (Collection_1_t849571967 *, const MethodInfo*))Collection_1_Clear_m1517527326_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1996711468_gshared (Collection_1_t849571967 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1996711468(__this, method) ((  void (*) (Collection_1_t849571967 *, const MethodInfo*))Collection_1_ClearItems_m1996711468_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::Contains(T)
extern "C"  bool Collection_1_Contains_m4128508600_gshared (Collection_1_t849571967 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m4128508600(__this, ___item0, method) ((  bool (*) (Collection_1_t849571967 *, int32_t, const MethodInfo*))Collection_1_Contains_m4128508600_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3126391174_gshared (Collection_1_t849571967 * __this, JTokenTypeU5BU5D_t1368357152* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3126391174(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t849571967 *, JTokenTypeU5BU5D_t1368357152*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3126391174_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3353371135_gshared (Collection_1_t849571967 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3353371135(__this, method) ((  Il2CppObject* (*) (Collection_1_t849571967 *, const MethodInfo*))Collection_1_GetEnumerator_m3353371135_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3266993504_gshared (Collection_1_t849571967 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3266993504(__this, ___item0, method) ((  int32_t (*) (Collection_1_t849571967 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m3266993504_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3433121953_gshared (Collection_1_t849571967 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3433121953(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t849571967 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m3433121953_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3328943676_gshared (Collection_1_t849571967 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3328943676(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t849571967 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m3328943676_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m3648906120_gshared (Collection_1_t849571967 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m3648906120(__this, method) ((  Il2CppObject* (*) (Collection_1_t849571967 *, const MethodInfo*))Collection_1_get_Items_m3648906120_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::Remove(T)
extern "C"  bool Collection_1_Remove_m2351781561_gshared (Collection_1_t849571967 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2351781561(__this, ___item0, method) ((  bool (*) (Collection_1_t849571967 *, int32_t, const MethodInfo*))Collection_1_Remove_m2351781561_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3219910693_gshared (Collection_1_t849571967 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3219910693(__this, ___index0, method) ((  void (*) (Collection_1_t849571967 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3219910693_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m4081964297_gshared (Collection_1_t849571967 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m4081964297(__this, ___index0, method) ((  void (*) (Collection_1_t849571967 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4081964297_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3342377399_gshared (Collection_1_t849571967 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3342377399(__this, method) ((  int32_t (*) (Collection_1_t849571967 *, const MethodInfo*))Collection_1_get_Count_m3342377399_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m3792674673_gshared (Collection_1_t849571967 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3792674673(__this, ___index0, method) ((  int32_t (*) (Collection_1_t849571967 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3792674673_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1164467876_gshared (Collection_1_t849571967 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1164467876(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t849571967 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m1164467876_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m30013589_gshared (Collection_1_t849571967 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m30013589(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t849571967 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m30013589_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m783915430_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m783915430(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m783915430_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m3644922060_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3644922060(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3644922060_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3859801546_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3859801546(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3859801546_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m284380012_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m284380012(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m284380012_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JTokenType>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m961561465_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m961561465(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m961561465_gshared)(__this /* static, unused */, ___list0, method)
