﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_GestureRecognizer
struct ISN_GestureRecognizer_t4198098372;
// System.Action`1<ISN_SwipeDirection>
struct Action_1_t570721078;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_SwipeDirection768921696.h"

// System.Void ISN_GestureRecognizer::.ctor()
extern "C"  void ISN_GestureRecognizer__ctor_m1133526079 (ISN_GestureRecognizer_t4198098372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GestureRecognizer::add_OnSwipe(System.Action`1<ISN_SwipeDirection>)
extern "C"  void ISN_GestureRecognizer_add_OnSwipe_m3591295284 (ISN_GestureRecognizer_t4198098372 * __this, Action_1_t570721078 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GestureRecognizer::remove_OnSwipe(System.Action`1<ISN_SwipeDirection>)
extern "C"  void ISN_GestureRecognizer_remove_OnSwipe_m1817173939 (ISN_GestureRecognizer_t4198098372 * __this, Action_1_t570721078 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GestureRecognizer::Awake()
extern "C"  void ISN_GestureRecognizer_Awake_m1157455254 (ISN_GestureRecognizer_t4198098372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GestureRecognizer::OnSwipeAction(System.String)
extern "C"  void ISN_GestureRecognizer_OnSwipeAction_m1696330732 (ISN_GestureRecognizer_t4198098372 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GestureRecognizer::<OnSwipe>m__65(ISN_SwipeDirection)
extern "C"  void ISN_GestureRecognizer_U3COnSwipeU3Em__65_m1424041646 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
