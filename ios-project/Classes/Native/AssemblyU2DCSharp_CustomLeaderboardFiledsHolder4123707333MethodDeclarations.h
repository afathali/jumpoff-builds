﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CustomLeaderboardFiledsHolder
struct CustomLeaderboardFiledsHolder_t4123707333;

#include "codegen/il2cpp-codegen.h"

// System.Void CustomLeaderboardFiledsHolder::.ctor()
extern "C"  void CustomLeaderboardFiledsHolder__ctor_m2044227218 (CustomLeaderboardFiledsHolder_t4123707333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomLeaderboardFiledsHolder::Awake()
extern "C"  void CustomLeaderboardFiledsHolder_Awake_m977346129 (CustomLeaderboardFiledsHolder_t4123707333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomLeaderboardFiledsHolder::Disable()
extern "C"  void CustomLeaderboardFiledsHolder_Disable_m78317378 (CustomLeaderboardFiledsHolder_t4123707333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
