﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke676045565MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.Collections.Generic.List`1<AN_PropertyTemplate>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m957082836(__this, ___host0, method) ((  void (*) (Enumerator_t2071585977 *, Dictionary_2_t3677049835 *, const MethodInfo*))Enumerator__ctor_m2650732273_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.Collections.Generic.List`1<AN_PropertyTemplate>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3409811843(__this, method) ((  Il2CppObject * (*) (Enumerator_t2071585977 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1623176564_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.Collections.Generic.List`1<AN_PropertyTemplate>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2626935027(__this, method) ((  void (*) (Enumerator_t2071585977 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3446745716_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.Collections.Generic.List`1<AN_PropertyTemplate>>::Dispose()
#define Enumerator_Dispose_m3316465160(__this, method) ((  void (*) (Enumerator_t2071585977 *, const MethodInfo*))Enumerator_Dispose_m1587212441_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.Collections.Generic.List`1<AN_PropertyTemplate>>::MoveNext()
#define Enumerator_MoveNext_m2241167702(__this, method) ((  bool (*) (Enumerator_t2071585977 *, const MethodInfo*))Enumerator_MoveNext_m235441832_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.Collections.Generic.List`1<AN_PropertyTemplate>>::get_Current()
#define Enumerator_get_Current_m3441139368(__this, method) ((  String_t* (*) (Enumerator_t2071585977 *, const MethodInfo*))Enumerator_get_Current_m937156828_gshared)(__this, method)
