﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<AN_PropertyTemplate>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1444616162(__this, ___l0, method) ((  void (*) (Enumerator_t1297000247 *, List_1_t1762270573 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AN_PropertyTemplate>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1190687888(__this, method) ((  void (*) (Enumerator_t1297000247 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<AN_PropertyTemplate>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1676783032(__this, method) ((  Il2CppObject * (*) (Enumerator_t1297000247 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AN_PropertyTemplate>::Dispose()
#define Enumerator_Dispose_m90802933(__this, method) ((  void (*) (Enumerator_t1297000247 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AN_PropertyTemplate>::VerifyState()
#define Enumerator_VerifyState_m2272803644(__this, method) ((  void (*) (Enumerator_t1297000247 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<AN_PropertyTemplate>::MoveNext()
#define Enumerator_MoveNext_m2566255899(__this, method) ((  bool (*) (Enumerator_t1297000247 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<AN_PropertyTemplate>::get_Current()
#define Enumerator_get_Current_m3752017191(__this, method) ((  AN_PropertyTemplate_t2393149441 * (*) (Enumerator_t1297000247 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
