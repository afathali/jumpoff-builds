﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_FBProxy
struct AN_FBProxy_t1438007924;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_FBProxy::.ctor()
extern "C"  void AN_FBProxy__ctor_m2089218475 (AN_FBProxy_t1438007924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_FBProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_FBProxy_CallActivityFunction_m3165451282 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_FBProxy::SendTrunRequest(System.String,System.String,System.String,System.String)
extern "C"  void AN_FBProxy_SendTrunRequest_m2915959479 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___messgae1, String_t* ___data2, String_t* ___to3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
