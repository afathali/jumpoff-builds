﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_SpanshotLoadResult
struct GP_SpanshotLoadResult_t2263304449;
// System.String
struct String_t;
// GP_Snapshot
struct GP_Snapshot_t1489095664;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_Snapshot1489095664.h"

// System.Void GP_SpanshotLoadResult::.ctor(System.String)
extern "C"  void GP_SpanshotLoadResult__ctor_m1813151106 (GP_SpanshotLoadResult_t2263304449 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_SpanshotLoadResult::SetSnapShot(GP_Snapshot)
extern "C"  void GP_SpanshotLoadResult_SetSnapShot_m243106578 (GP_SpanshotLoadResult_t2263304449 * __this, GP_Snapshot_t1489095664 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_Snapshot GP_SpanshotLoadResult::get_Snapshot()
extern "C"  GP_Snapshot_t1489095664 * GP_SpanshotLoadResult_get_Snapshot_m3412356804 (GP_SpanshotLoadResult_t2263304449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
