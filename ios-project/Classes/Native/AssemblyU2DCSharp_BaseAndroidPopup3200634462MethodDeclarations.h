﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseAndroidPopup
struct BaseAndroidPopup_t3200634462;
// System.Action`1<AndroidDialogResult>
struct Action_1_t1465846336;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AndroidDialogResult1664046954.h"

// System.Void BaseAndroidPopup::.ctor()
extern "C"  void BaseAndroidPopup__ctor_m1860129547 (BaseAndroidPopup_t3200634462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAndroidPopup::add_ActionComplete(System.Action`1<AndroidDialogResult>)
extern "C"  void BaseAndroidPopup_add_ActionComplete_m1102524952 (BaseAndroidPopup_t3200634462 * __this, Action_1_t1465846336 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAndroidPopup::remove_ActionComplete(System.Action`1<AndroidDialogResult>)
extern "C"  void BaseAndroidPopup_remove_ActionComplete_m3217013449 (BaseAndroidPopup_t3200634462 * __this, Action_1_t1465846336 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAndroidPopup::onDismissed(System.String)
extern "C"  void BaseAndroidPopup_onDismissed_m2747403195 (BaseAndroidPopup_t3200634462 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAndroidPopup::DispatchAction(AndroidDialogResult)
extern "C"  void BaseAndroidPopup_DispatchAction_m3029892439 (BaseAndroidPopup_t3200634462 * __this, int32_t ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAndroidPopup::<ActionComplete>m__7D(AndroidDialogResult)
extern "C"  void BaseAndroidPopup_U3CActionCompleteU3Em__7D_m1953376700 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
