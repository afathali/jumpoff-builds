﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_StatusBar
struct SA_StatusBar_t1378080260;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SA_StatusBar::.ctor()
extern "C"  void SA_StatusBar__ctor_m3988460219 (SA_StatusBar_t1378080260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_StatusBar::SetText(System.String)
extern "C"  void SA_StatusBar_SetText_m328526794 (SA_StatusBar_t1378080260 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA_StatusBar::get_text()
extern "C"  String_t* SA_StatusBar_get_text_m2224619284 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_StatusBar::set_text(System.String)
extern "C"  void SA_StatusBar_set_text_m446859561 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
