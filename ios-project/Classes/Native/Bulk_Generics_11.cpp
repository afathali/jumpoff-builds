﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Collections.DictionaryEntry>
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>
struct IEnumerator_1_t524399225;
// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>
struct U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265;
// System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>
struct U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693;
// System.Linq.IGrouping`2<System.Object,System.Object>
struct IGrouping_2_t906937361;
// System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<System.Object,System.Object>>
struct IEnumerator_1_t2677428484;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.List`1<System.Object>>
struct Dictionary_2_t1650630555;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1902082073;
// System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>
struct U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408;
// System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>
struct U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Object>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t930005165;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Char,System.Object>
struct U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Object,System.Object>
struct U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664;
// System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>
struct U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136;
// System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>
struct U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859;
// System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Object>
struct U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct IEnumerator_1_t3513236300;
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct IEnumerator_1_t2945471191;
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220;
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870;
// System.Linq.Grouping`2<System.Object,System.Object>
struct Grouping_2_t308796194;
// System.Linq.OrderedEnumerable`1<System.Object>
struct OrderedEnumerable_1_t4214082274;
// System.Linq.OrderedSequence`2<System.Object,System.Int32>
struct OrderedSequence_2_t1710216260;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t2207932334;
// System.Collections.Generic.IComparer`1<System.Int32>
struct IComparer_1_t26340570;
// System.Linq.SortContext`1<System.Object>
struct SortContext_1_t1798778454;
// System.Linq.OrderedSequence`2<System.Object,System.Object>
struct OrderedSequence_2_t2327788107;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t643912417;
// System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>
struct U3CSortU3Ec__Iterator21_t2488605192;
// System.Linq.QuickSort`1<System.Object>
struct QuickSort_1_t1970792956;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Linq.SortSequenceContext`2<System.Object,System.Int32>
struct SortSequenceContext_2_t2443723226;
// System.Linq.SortSequenceContext`2<System.Object,System.Object>
struct SortSequenceContext_2_t3061295073;
// System.String
struct String_t;
// System.Predicate`1<Newtonsoft.Json.Linq.JTokenType>
struct Predicate_1_t4045764624;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Predicate`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct Predicate_1_t185715292;
// System.Predicate`1<System.Byte>
struct Predicate_1_t2126074551;
// System.Predicate`1<System.Char>
struct Predicate_1_t1897451453;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Predicate_1_t2776792056;
// System.Predicate`1<System.Int32>
struct Predicate_1_t514847563;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;
// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct Predicate_1_t2832094954;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t4236135325;
// System.Predicate`1<UnityEngine.Color32>
struct Predicate_1_t3612454929;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t2759123787;
// System.Predicate`1<UnityEngine.Experimental.Director.Playable>
struct Predicate_1_t2110515663;
// System.Predicate`1<UnityEngine.RuntimePlatform>
struct Predicate_1_t312555082;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t1499606915;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t2064247989;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t3942196229;
// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t686677694;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t686677695;
// System.Predicate`1<UnityEngine.Vector4>
struct Predicate_1_t686677696;
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t4179406139;
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct StaticGetter_1_t1095697167;
// UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Boolean>
struct UnityAdsDelegate_2_t3657054456;
// UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>
struct UnityAdsDelegate_2_t2520929033;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateCastIt1756526812.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateCastIt1756526812MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateCastIt1397100709.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateCastIt1397100709MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateDistin3520812265.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateDistin3520812265MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1022910149.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1022910149MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateGroupB3689409693.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateGroupB3689409693MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2825504181.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1650630555.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1650630555MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2825504181MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2970655257.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2970655257MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23702943073.h"
#include "System_Core_System_Linq_Grouping_2_gen308796194.h"
#include "System_Core_System_Linq_Grouping_2_gen308796194MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23702943073MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateOfType4154598408.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateOfType4154598408MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateRepeat2110762548.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateRepeat2110762548MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect2092700746.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect2092700746MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "System_Core_System_Func_2_gen3251821928.h"
#include "System_Core_System_Func_2_gen3251821928MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelectI860069390.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelectI860069390MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2019190572.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "System_Core_System_Func_2_gen2019190572MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelectI906669209.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelectI906669209MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2065790391.h"
#include "System_Core_System_Func_2_gen2065790391MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect2431415042.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect2431415042MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "System_Core_System_Func_2_gen3590536224.h"
#include "System_Core_System_Func_2_gen3590536224MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect1666382999.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect1666382999MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelectI947348529.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelectI947348529MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2898077773.h"
#include "System_Core_System_Func_3_gen2898077773MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect3233898664.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect3233898664MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2785112090.h"
#include "System_Core_System_Func_3_gen2785112090MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelectM288472136.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelectM288472136MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3117631226.h"
#include "System_Core_System_Func_2_gen3117631226MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateTakeIt1384452859.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateTakeIt1384452859MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateUnionI3206829433.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateUnionI3206829433MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI3175034752.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI3175034752MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"
#include "System_Core_System_Func_2_gen3451171634.h"
#include "System_Core_System_Func_2_gen3451171634MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI2607269643.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI2607269643MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3155315995.h"
#include "System_Core_System_Func_2_gen3155315995MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI1471144220.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI1471144220MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3201915814.h"
#include "System_Core_System_Func_2_gen3201915814MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI4121738870.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI4121738870MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3961629604.h"
#include "System_Core_System_Func_2_gen3961629604MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_Function_1_gen978359491.h"
#include "System_Core_System_Linq_Enumerable_Function_1_gen978359491MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "System_Core_System_Linq_OrderedEnumerable_1_gen4214082274.h"
#include "System_Core_System_Linq_OrderedEnumerable_1_gen4214082274MethodDeclarations.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen1710216260.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen1710216260MethodDeclarations.h"
#include "System_Core_System_Linq_SortDirection759359329.h"
#include "System_Core_System_Func_2_gen2207932334.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen961886567MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen961886567.h"
#include "System_Core_System_Linq_SortContext_1_gen1798778454.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen2443723226.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen2443723226MethodDeclarations.h"
#include "System_Core_System_Linq_QuickSort_1_gen1970792956MethodDeclarations.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen2327788107.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen2327788107MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen3061295073.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen3061295073MethodDeclarations.h"
#include "System_Core_System_Linq_QuickSort_1_U3CSortU3Ec__I2488605192.h"
#include "System_Core_System_Linq_QuickSort_1_U3CSortU3Ec__I2488605192MethodDeclarations.h"
#include "System_Core_System_Linq_QuickSort_1_gen1970792956.h"
#include "mscorlib_ArrayTypes.h"
#include "System_Core_System_Linq_SortContext_1_gen1798778454MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2207932334MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1720961778.h"
#include "mscorlib_System_Nullable_1_gen1720961778MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_DefaultValueHand3457895463.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_ValueType3507792607MethodDeclarations.h"
#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3178688176.h"
#include "mscorlib_System_Nullable_1_gen3178688176MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken620654565.h"
#include "mscorlib_System_Nullable_1_gen3865860824.h"
#include "mscorlib_System_Nullable_1_gen3865860824MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType1307827213.h"
#include "mscorlib_System_Nullable_1_gen1881161680.h"
#include "mscorlib_System_Nullable_1_gen1881161680MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_NullValueHandlin3618095365.h"
#include "mscorlib_System_Nullable_1_gen1983200966.h"
#include "mscorlib_System_Nullable_1_gen1983200966MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObjectCreationHa3720134651.h"
#include "mscorlib_System_Nullable_1_gen3575889505.h"
#include "mscorlib_System_Nullable_1_gen3575889505MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ReferenceLoopHan1017855894.h"
#include "mscorlib_System_Nullable_1_gen5811492.h"
#include "mscorlib_System_Nullable_1_gen5811492MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3889546705.h"
#include "mscorlib_System_Nullable_1_gen3889546705MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_TypeNameHandling1331513094.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Nullable_1_gen2088641033MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1946170751.h"
#include "mscorlib_System_Nullable_1_gen1946170751MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Byte3683104436MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1717547653.h"
#include "mscorlib_System_Nullable_1_gen1717547653MethodDeclarations.h"
#include "mscorlib_System_Char3454481338MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "mscorlib_System_Nullable_1_gen3251239280MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3921022517.h"
#include "mscorlib_System_Nullable_1_gen3921022517MethodDeclarations.h"
#include "mscorlib_System_DateTimeOffset1362988906.h"
#include "mscorlib_System_DateTimeOffset1362988906MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3282734688.h"
#include "mscorlib_System_Nullable_1_gen3282734688MethodDeclarations.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Decimal724701077MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2341081996.h"
#include "mscorlib_System_Nullable_1_gen2341081996MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Double4078015681MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen796667908.h"
#include "mscorlib_System_Nullable_1_gen796667908MethodDeclarations.h"
#include "mscorlib_System_Guid2533601593.h"
#include "mscorlib_System_Guid2533601593MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2304312229.h"
#include "mscorlib_System_Nullable_1_gen2304312229MethodDeclarations.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_Int164041245914MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen334943763MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3467111648.h"
#include "mscorlib_System_Nullable_1_gen3467111648MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3012451160.h"
#include "mscorlib_System_Nullable_1_gen3012451160MethodDeclarations.h"
#include "mscorlib_System_SByte454417549.h"
#include "mscorlib_System_SByte454417549MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen339576247.h"
#include "mscorlib_System_Nullable_1_gen339576247MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1693325264.h"
#include "mscorlib_System_Nullable_1_gen1693325264MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_TimeSpan3430258949MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3544916222.h"
#include "mscorlib_System_Nullable_1_gen3544916222MethodDeclarations.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_UInt16986882611MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen412748336.h"
#include "mscorlib_System_Nullable_1_gen412748336MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1172263229.h"
#include "mscorlib_System_Nullable_1_gen1172263229MethodDeclarations.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_UInt642909196914MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2293140233.h"
#include "mscorlib_System_Nullable_1_gen2293140233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen506773895.h"
#include "mscorlib_System_Nullable_1_gen506773895MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen4045764624.h"
#include "mscorlib_System_Predicate_1_gen4045764624MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Predicate_1_gen185715292.h"
#include "mscorlib_System_Predicate_1_gen185715292MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2126074551.h"
#include "mscorlib_System_Predicate_1_gen2126074551MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1897451453.h"
#include "mscorlib_System_Predicate_1_gen1897451453MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2776792056.h"
#include "mscorlib_System_Predicate_1_gen2776792056MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen514847563.h"
#include "mscorlib_System_Predicate_1_gen514847563MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1132419410.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2832094954.h"
#include "mscorlib_System_Predicate_1_gen2832094954MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Predicate_1_gen4236135325.h"
#include "mscorlib_System_Predicate_1_gen4236135325MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Predicate_1_gen3612454929.h"
#include "mscorlib_System_Predicate_1_gen3612454929MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Predicate_1_gen2759123787.h"
#include "mscorlib_System_Predicate_1_gen2759123787MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "mscorlib_System_Predicate_1_gen2110515663.h"
#include "mscorlib_System_Predicate_1_gen2110515663MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "mscorlib_System_Predicate_1_gen312555082.h"
#include "mscorlib_System_Predicate_1_gen312555082MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "mscorlib_System_Predicate_1_gen1499606915.h"
#include "mscorlib_System_Predicate_1_gen1499606915MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "mscorlib_System_Predicate_1_gen2064247989.h"
#include "mscorlib_System_Predicate_1_gen2064247989MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "mscorlib_System_Predicate_1_gen3942196229.h"
#include "mscorlib_System_Predicate_1_gen3942196229MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "mscorlib_System_Predicate_1_gen686677694.h"
#include "mscorlib_System_Predicate_1_gen686677694MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Predicate_1_gen686677695.h"
#include "mscorlib_System_Predicate_1_gen686677695MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen686677696.h"
#include "mscorlib_System_Predicate_1_gen686677696MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_g4179406139.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_g4179406139MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGett1095697167.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGett1095697167MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel3657054456.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel3657054456MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel2520929033.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel2520929033MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen3207297272.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen3207297272MethodDeclarations.h"

// System.Collections.Generic.List`1<!!1> System.Linq.Enumerable::ContainsGroup<System.Object,System.Object>(System.Collections.Generic.Dictionary`2<!!0,System.Collections.Generic.List`1<!!1>>,!!0,System.Collections.Generic.IEqualityComparer`1<!!0>)
extern "C"  List_1_t2058570427 * Enumerable_ContainsGroup_TisIl2CppObject_TisIl2CppObject_m610282755_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t1650630555 * p0, Il2CppObject * p1, Il2CppObject* p2, const MethodInfo* method);
#define Enumerable_ContainsGroup_TisIl2CppObject_TisIl2CppObject_m610282755(__this /* static, unused */, p0, p1, p2, method) ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Dictionary_2_t1650630555 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))Enumerable_ContainsGroup_TisIl2CppObject_TisIl2CppObject_m610282755_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Boolean System.Linq.Enumerable::Contains<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,!!0,System.Collections.Generic.IEqualityComparer`1<!!0>)
extern "C"  bool Enumerable_Contains_TisIl2CppObject_m3132779447_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject * p1, Il2CppObject* p2, const MethodInfo* method);
#define Enumerable_Contains_TisIl2CppObject_m3132779447(__this /* static, unused */, p0, p1, p2, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, Il2CppObject*, const MethodInfo*))Enumerable_Contains_TisIl2CppObject_m3132779447_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m3301715283(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Collections.DictionaryEntry>::.ctor()
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m2220864174_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Collections.DictionaryEntry>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  DictionaryEntry_t3048875398  U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m244095059_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 * __this, const MethodInfo* method)
{
	{
		DictionaryEntry_t3048875398  L_0 = (DictionaryEntry_t3048875398 )__this->get_U24current_4();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m379990040_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 * __this, const MethodInfo* method)
{
	{
		DictionaryEntry_t3048875398  L_0 = (DictionaryEntry_t3048875398 )__this->get_U24current_4();
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m3925094575_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Collections.DictionaryEntry>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m518367946_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 * __this, const MethodInfo* method)
{
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 * L_2 = (U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 *)L_2;
		U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 * L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CU24U3Esource_5();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Collections.DictionaryEntry>::MoveNext()
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m1537202078_MetadataUsageId;
extern "C"  bool U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m1537202078_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m1537202078_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00af;
	}

IL_0023:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_source_0();
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_U3CU24s_59U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0078;
			}
		}

IL_0043:
		{
			goto IL_0078;
		}

IL_0048:
		{
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_U3CU24s_59U3E__0_1();
			NullCheck((Il2CppObject *)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			__this->set_U3CelementU3E__1_2(((*(DictionaryEntry_t3048875398 *)((DictionaryEntry_t3048875398 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
			DictionaryEntry_t3048875398  L_7 = (DictionaryEntry_t3048875398 )__this->get_U3CelementU3E__1_2();
			__this->set_U24current_4(L_7);
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB1, FINALLY_008d);
		}

IL_0078:
		{
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CU24s_59U3E__0_1();
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_0048;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0xA8, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			bool L_10 = V_1;
			if (!L_10)
			{
				goto IL_0091;
			}
		}

IL_0090:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_0091:
		{
			Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CU24s_59U3E__0_1();
			V_2 = (Il2CppObject *)((Il2CppObject *)IsInst(L_11, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_12 = V_2;
			if (L_12)
			{
				goto IL_00a1;
			}
		}

IL_00a0:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_00a1:
		{
			Il2CppObject * L_13 = V_2;
			NullCheck((Il2CppObject *)L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0xB1, IL_00b1)
		IL2CPP_JUMP_TBL(0xA8, IL_00a8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00a8:
	{
		__this->set_U24PC_3((-1));
	}

IL_00af:
	{
		return (bool)0;
	}

IL_00b1:
	{
		return (bool)1;
	}
	// Dead block : IL_00b3: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Collections.DictionaryEntry>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m1333382577_MetadataUsageId;
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m1333382577_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m1333382577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003d;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = (Il2CppObject *)__this->get_U3CU24s_59U3E__0_1();
			V_1 = (Il2CppObject *)((Il2CppObject *)IsInst(L_2, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_3 = V_1;
			if (L_3)
			{
				goto IL_0036;
			}
		}

IL_0035:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_0036:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck((Il2CppObject *)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Collections.DictionaryEntry>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m153110803_MetadataUsageId;
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m153110803_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1756526812 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m153110803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::.ctor()
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m3321498162_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2659351773_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m2940047778_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m3746809729_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m3785434980_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * L_2 = (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 *)L_2;
		U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CU24U3Esource_5();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m3444743414_MetadataUsageId;
extern "C"  bool U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m3444743414_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m3444743414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00af;
	}

IL_0023:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_source_0();
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_U3CU24s_59U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0078;
			}
		}

IL_0043:
		{
			goto IL_0078;
		}

IL_0048:
		{
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_U3CU24s_59U3E__0_1();
			NullCheck((Il2CppObject *)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			__this->set_U3CelementU3E__1_2(((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
			Il2CppObject * L_7 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_4(L_7);
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB1, FINALLY_008d);
		}

IL_0078:
		{
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CU24s_59U3E__0_1();
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_0048;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0xA8, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			bool L_10 = V_1;
			if (!L_10)
			{
				goto IL_0091;
			}
		}

IL_0090:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_0091:
		{
			Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CU24s_59U3E__0_1();
			V_2 = (Il2CppObject *)((Il2CppObject *)IsInst(L_11, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_12 = V_2;
			if (L_12)
			{
				goto IL_00a1;
			}
		}

IL_00a0:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_00a1:
		{
			Il2CppObject * L_13 = V_2;
			NullCheck((Il2CppObject *)L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0xB1, IL_00b1)
		IL2CPP_JUMP_TBL(0xA8, IL_00a8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00a8:
	{
		__this->set_U24PC_3((-1));
	}

IL_00af:
	{
		return (bool)0;
	}

IL_00b1:
	{
		return (bool)1;
	}
	// Dead block : IL_00b3: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m3972648411_MetadataUsageId;
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m3972648411_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m3972648411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003d;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = (Il2CppObject *)__this->get_U3CU24s_59U3E__0_1();
			V_1 = (Il2CppObject *)((Il2CppObject *)IsInst(L_2, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_3 = V_1;
			if (L_3)
			{
				goto IL_0036;
			}
		}

IL_0035:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_0036:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck((Il2CppObject *)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m2250535601_MetadataUsageId;
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m2250535601_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m2250535601_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::.ctor()
extern "C"  void U3CCreateDistinctIteratorU3Ec__Iterator3_1__ctor_m1619070986_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m4167922749_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_IEnumerator_get_Current_m2592365566_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_IEnumerable_GetEnumerator_m4009283937_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m193659212_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_5();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * L_2 = (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *)L_2;
		U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Ecomparer_7();
		NullCheck(L_3);
		L_3->set_comparer_0(L_4);
		U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * L_5 = V_0;
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24U3Esource_8();
		NullCheck(L_5);
		L_5->set_source_2(L_6);
		U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateDistinctIteratorU3Ec__Iterator3_1_MoveNext_m270250114_MetadataUsageId;
extern "C"  bool U3CCreateDistinctIteratorU3Ec__Iterator3_1_MoveNext_m270250114_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateDistinctIteratorU3Ec__Iterator3_1_MoveNext_m270250114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0048;
		}
	}
	{
		goto IL_00e1;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_comparer_0();
		HashSet_1_t1022910149 * L_3 = (HashSet_1_t1022910149 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (HashSet_1_t1022910149 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		__this->set_U3CitemsU3E__0_1(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_source_2();
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject* L_5 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_4);
		__this->set_U3CU24s_65U3E__1_3(L_5);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0048:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_6 = V_0;
			if (((int32_t)((int32_t)L_6-(int32_t)1)) == 0)
			{
				goto IL_00ac;
			}
		}

IL_0054:
		{
			goto IL_00ac;
		}

IL_0059:
		{
			Il2CppObject* L_7 = (Il2CppObject*)__this->get_U3CU24s_65U3E__1_3();
			NullCheck((Il2CppObject*)L_7);
			Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), (Il2CppObject*)L_7);
			__this->set_U3CelementU3E__2_4(L_8);
			HashSet_1_t1022910149 * L_9 = (HashSet_1_t1022910149 *)__this->get_U3CitemsU3E__0_1();
			Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CelementU3E__2_4();
			NullCheck((HashSet_1_t1022910149 *)L_9);
			bool L_11 = ((  bool (*) (HashSet_1_t1022910149 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((HashSet_1_t1022910149 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
			if (L_11)
			{
				goto IL_00ac;
			}
		}

IL_0080:
		{
			HashSet_1_t1022910149 * L_12 = (HashSet_1_t1022910149 *)__this->get_U3CitemsU3E__0_1();
			Il2CppObject * L_13 = (Il2CppObject *)__this->get_U3CelementU3E__2_4();
			NullCheck((HashSet_1_t1022910149 *)L_12);
			((  bool (*) (HashSet_1_t1022910149 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((HashSet_1_t1022910149 *)L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			Il2CppObject * L_14 = (Il2CppObject *)__this->get_U3CelementU3E__2_4();
			__this->set_U24current_6(L_14);
			__this->set_U24PC_5(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xE3, FINALLY_00c1);
		}

IL_00ac:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_65U3E__1_3();
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0059;
			}
		}

IL_00bc:
		{
			IL2CPP_LEAVE(0xDA, FINALLY_00c1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c1;
	}

FINALLY_00c1:
	{ // begin finally (depth: 1)
		{
			bool L_17 = V_1;
			if (!L_17)
			{
				goto IL_00c5;
			}
		}

IL_00c4:
		{
			IL2CPP_END_FINALLY(193)
		}

IL_00c5:
		{
			Il2CppObject* L_18 = (Il2CppObject*)__this->get_U3CU24s_65U3E__1_3();
			if (L_18)
			{
				goto IL_00ce;
			}
		}

IL_00cd:
		{
			IL2CPP_END_FINALLY(193)
		}

IL_00ce:
		{
			Il2CppObject* L_19 = (Il2CppObject*)__this->get_U3CU24s_65U3E__1_3();
			NullCheck((Il2CppObject *)L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
			IL2CPP_END_FINALLY(193)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(193)
	{
		IL2CPP_JUMP_TBL(0xE3, IL_00e3)
		IL2CPP_JUMP_TBL(0xDA, IL_00da)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00da:
	{
		__this->set_U24PC_5((-1));
	}

IL_00e1:
	{
		return (bool)0;
	}

IL_00e3:
	{
		return (bool)1;
	}
	// Dead block : IL_00e5: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateDistinctIteratorU3Ec__Iterator3_1_Dispose_m2370563095_MetadataUsageId;
extern "C"  void U3CCreateDistinctIteratorU3Ec__Iterator3_1_Dispose_m2370563095_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateDistinctIteratorU3Ec__Iterator3_1_Dispose_m2370563095_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_65U3E__1_3();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_65U3E__1_3();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateDistinctIteratorU3Ec__Iterator3_1_Reset_m3955730001_MetadataUsageId;
extern "C"  void U3CCreateDistinctIteratorU3Ec__Iterator3_1_Reset_m3955730001_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateDistinctIteratorU3Ec__Iterator3_1_Reset_m3955730001_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CCreateGroupByIteratorU3Ec__Iterator5_2__ctor_m4025420589_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Linq.IGrouping`2<TKey,TSource> System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TSource>>.get_Current()
extern "C"  Il2CppObject* U3CCreateGroupByIteratorU3Ec__Iterator5_2_System_Collections_Generic_IEnumeratorU3CSystem_Linq_IGroupingU3CTKeyU2CTSourceU3EU3E_get_Current_m850969262_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U24current_14();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateGroupByIteratorU3Ec__Iterator5_2_System_Collections_IEnumerator_get_Current_m747718943_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U24current_14();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateGroupByIteratorU3Ec__Iterator5_2_System_Collections_IEnumerable_GetEnumerator_m1486229824_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Linq.IGrouping<TKey,TSource>>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateGroupByIteratorU3Ec__Iterator5_2_System_Collections_Generic_IEnumerableU3CSystem_Linq_IGroupingU3CTKeyU2CTSourceU3EU3E_GetEnumerator_m750292983_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_13();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * L_2 = (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 *)L_2;
		U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_15();
		NullCheck(L_3);
		L_3->set_source_4(L_4);
		U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * L_5 = V_0;
		Func_2_t2825504181 * L_6 = (Func_2_t2825504181 *)__this->get_U3CU24U3EkeySelector_16();
		NullCheck(L_5);
		L_5->set_keySelector_7(L_6);
		U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * L_7 = V_0;
		Il2CppObject* L_8 = (Il2CppObject*)__this->get_U3CU24U3Ecomparer_17();
		NullCheck(L_7);
		L_7->set_comparer_9(L_8);
		U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * L_9 = V_0;
		return L_9;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateGroupByIteratorU3Ec__Iterator5_2_MoveNext_m2399691539_MetadataUsageId;
extern "C"  bool U3CCreateGroupByIteratorU3Ec__Iterator5_2_MoveNext_m2399691539_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateGroupByIteratorU3Ec__Iterator5_2_MoveNext_m2399691539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	bool V_4 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_13();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_13((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_0187;
		}
		if (L_1 == 2)
		{
			goto IL_0187;
		}
		if (L_1 == 3)
		{
			goto IL_0292;
		}
	}
	{
		goto IL_02a7;
	}

IL_002b:
	{
		Dictionary_2_t1650630555 * L_2 = (Dictionary_2_t1650630555 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (Dictionary_2_t1650630555 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set_U3CgroupsU3E__0_0(L_2);
		List_1_t2058570427 * L_3 = (List_1_t2058570427 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		__this->set_U3CnullListU3E__1_1(L_3);
		__this->set_U3CcounterU3E__2_2(0);
		__this->set_U3CnullCounterU3E__3_3((-1));
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_source_4();
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject* L_5 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), (Il2CppObject*)L_4);
		__this->set_U3CU24s_71U3E__4_5(L_5);
	}

IL_0060:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0142;
		}

IL_0065:
		{
			Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24s_71U3E__4_5();
			NullCheck((Il2CppObject*)L_6);
			Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (Il2CppObject*)L_6);
			__this->set_U3CelementU3E__5_6(L_7);
			Func_2_t2825504181 * L_8 = (Func_2_t2825504181 *)__this->get_keySelector_7();
			Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CelementU3E__5_6();
			NullCheck((Func_2_t2825504181 *)L_8);
			Il2CppObject * L_10 = ((  Il2CppObject * (*) (Func_2_t2825504181 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Func_2_t2825504181 *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			__this->set_U3CkeyU3E__6_8(L_10);
			Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CkeyU3E__6_8();
			if (L_11)
			{
				goto IL_00d9;
			}
		}

IL_009d:
		{
			List_1_t2058570427 * L_12 = (List_1_t2058570427 *)__this->get_U3CnullListU3E__1_1();
			Il2CppObject * L_13 = (Il2CppObject *)__this->get_U3CelementU3E__5_6();
			NullCheck((List_1_t2058570427 *)L_12);
			((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t2058570427 *)L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			int32_t L_14 = (int32_t)__this->get_U3CnullCounterU3E__3_3();
			if ((!(((uint32_t)L_14) == ((uint32_t)(-1)))))
			{
				goto IL_00d4;
			}
		}

IL_00ba:
		{
			int32_t L_15 = (int32_t)__this->get_U3CcounterU3E__2_2();
			__this->set_U3CnullCounterU3E__3_3(L_15);
			int32_t L_16 = (int32_t)__this->get_U3CcounterU3E__2_2();
			__this->set_U3CcounterU3E__2_2(((int32_t)((int32_t)L_16+(int32_t)1)));
		}

IL_00d4:
		{
			goto IL_0142;
		}

IL_00d9:
		{
			Dictionary_2_t1650630555 * L_17 = (Dictionary_2_t1650630555 *)__this->get_U3CgroupsU3E__0_0();
			Il2CppObject * L_18 = (Il2CppObject *)__this->get_U3CkeyU3E__6_8();
			Il2CppObject* L_19 = (Il2CppObject*)__this->get_comparer_9();
			List_1_t2058570427 * L_20 = ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Dictionary_2_t1650630555 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Dictionary_2_t1650630555 *)L_17, (Il2CppObject *)L_18, (Il2CppObject*)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
			__this->set_U3CgroupU3E__7_10(L_20);
			List_1_t2058570427 * L_21 = (List_1_t2058570427 *)__this->get_U3CgroupU3E__7_10();
			if (L_21)
			{
				goto IL_0131;
			}
		}

IL_0101:
		{
			List_1_t2058570427 * L_22 = (List_1_t2058570427 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
			((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U3CgroupU3E__7_10(L_22);
			Dictionary_2_t1650630555 * L_23 = (Dictionary_2_t1650630555 *)__this->get_U3CgroupsU3E__0_0();
			Il2CppObject * L_24 = (Il2CppObject *)__this->get_U3CkeyU3E__6_8();
			List_1_t2058570427 * L_25 = (List_1_t2058570427 *)__this->get_U3CgroupU3E__7_10();
			NullCheck((Dictionary_2_t1650630555 *)L_23);
			((  void (*) (Dictionary_2_t1650630555 *, Il2CppObject *, List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t1650630555 *)L_23, (Il2CppObject *)L_24, (List_1_t2058570427 *)L_25, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			int32_t L_26 = (int32_t)__this->get_U3CcounterU3E__2_2();
			__this->set_U3CcounterU3E__2_2(((int32_t)((int32_t)L_26+(int32_t)1)));
		}

IL_0131:
		{
			List_1_t2058570427 * L_27 = (List_1_t2058570427 *)__this->get_U3CgroupU3E__7_10();
			Il2CppObject * L_28 = (Il2CppObject *)__this->get_U3CelementU3E__5_6();
			NullCheck((List_1_t2058570427 *)L_27);
			((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t2058570427 *)L_27, (Il2CppObject *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		}

IL_0142:
		{
			Il2CppObject* L_29 = (Il2CppObject*)__this->get_U3CU24s_71U3E__4_5();
			NullCheck((Il2CppObject *)L_29);
			bool L_30 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_29);
			if (L_30)
			{
				goto IL_0065;
			}
		}

IL_0152:
		{
			IL2CPP_LEAVE(0x16C, FINALLY_0157);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0157;
	}

FINALLY_0157:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_31 = (Il2CppObject*)__this->get_U3CU24s_71U3E__4_5();
			if (L_31)
			{
				goto IL_0160;
			}
		}

IL_015f:
		{
			IL2CPP_END_FINALLY(343)
		}

IL_0160:
		{
			Il2CppObject* L_32 = (Il2CppObject*)__this->get_U3CU24s_71U3E__4_5();
			NullCheck((Il2CppObject *)L_32);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_32);
			IL2CPP_END_FINALLY(343)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(343)
	{
		IL2CPP_JUMP_TBL(0x16C, IL_016c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_016c:
	{
		__this->set_U3CcounterU3E__2_2(0);
		Dictionary_2_t1650630555 * L_33 = (Dictionary_2_t1650630555 *)__this->get_U3CgroupsU3E__0_0();
		NullCheck((Dictionary_2_t1650630555 *)L_33);
		Enumerator_t2970655257  L_34 = ((  Enumerator_t2970655257  (*) (Dictionary_2_t1650630555 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((Dictionary_2_t1650630555 *)L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		__this->set_U3CU24s_72U3E__8_11(L_34);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0187:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_35 = V_0;
			if (((int32_t)((int32_t)L_35-(int32_t)1)) == 0)
			{
				goto IL_01e6;
			}
			if (((int32_t)((int32_t)L_35-(int32_t)1)) == 1)
			{
				goto IL_0223;
			}
		}

IL_0197:
		{
			goto IL_0231;
		}

IL_019c:
		{
			Enumerator_t2970655257 * L_36 = (Enumerator_t2970655257 *)__this->get_address_of_U3CU24s_72U3E__8_11();
			KeyValuePair_2_t3702943073  L_37 = Enumerator_get_Current_m1443704642((Enumerator_t2970655257 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
			__this->set_U3CgroupU3E__9_12(L_37);
			int32_t L_38 = (int32_t)__this->get_U3CcounterU3E__2_2();
			int32_t L_39 = (int32_t)__this->get_U3CnullCounterU3E__3_3();
			if ((!(((uint32_t)L_38) == ((uint32_t)L_39))))
			{
				goto IL_01f4;
			}
		}

IL_01be:
		{
			Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
			Il2CppObject * L_40 = V_2;
			List_1_t2058570427 * L_41 = (List_1_t2058570427 *)__this->get_U3CnullListU3E__1_1();
			Grouping_2_t308796194 * L_42 = (Grouping_2_t308796194 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16));
			((  void (*) (Grouping_2_t308796194 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)(L_42, (Il2CppObject *)L_40, (Il2CppObject*)L_41, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
			__this->set_U24current_14(L_42);
			__this->set_U24PC_13(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x2A9, FINALLY_0246);
		}

IL_01e6:
		{
			int32_t L_43 = (int32_t)__this->get_U3CcounterU3E__2_2();
			__this->set_U3CcounterU3E__2_2(((int32_t)((int32_t)L_43+(int32_t)1)));
		}

IL_01f4:
		{
			KeyValuePair_2_t3702943073 * L_44 = (KeyValuePair_2_t3702943073 *)__this->get_address_of_U3CgroupU3E__9_12();
			Il2CppObject * L_45 = KeyValuePair_2_get_Key_m185780481((KeyValuePair_2_t3702943073 *)L_44, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
			KeyValuePair_2_t3702943073 * L_46 = (KeyValuePair_2_t3702943073 *)__this->get_address_of_U3CgroupU3E__9_12();
			List_1_t2058570427 * L_47 = KeyValuePair_2_get_Value_m4024827393((KeyValuePair_2_t3702943073 *)L_46, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
			Grouping_2_t308796194 * L_48 = (Grouping_2_t308796194 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16));
			((  void (*) (Grouping_2_t308796194 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)(L_48, (Il2CppObject *)L_45, (Il2CppObject*)L_47, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
			__this->set_U24current_14(L_48);
			__this->set_U24PC_13(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x2A9, FINALLY_0246);
		}

IL_0223:
		{
			int32_t L_49 = (int32_t)__this->get_U3CcounterU3E__2_2();
			__this->set_U3CcounterU3E__2_2(((int32_t)((int32_t)L_49+(int32_t)1)));
		}

IL_0231:
		{
			Enumerator_t2970655257 * L_50 = (Enumerator_t2970655257 *)__this->get_address_of_U3CU24s_72U3E__8_11();
			bool L_51 = Enumerator_MoveNext_m1991620226((Enumerator_t2970655257 *)L_50, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
			if (L_51)
			{
				goto IL_019c;
			}
		}

IL_0241:
		{
			IL2CPP_LEAVE(0x25B, FINALLY_0246);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0246;
	}

FINALLY_0246:
	{ // begin finally (depth: 1)
		{
			bool L_52 = V_1;
			if (!L_52)
			{
				goto IL_024a;
			}
		}

IL_0249:
		{
			IL2CPP_END_FINALLY(582)
		}

IL_024a:
		{
			Enumerator_t2970655257  L_53 = (Enumerator_t2970655257 )__this->get_U3CU24s_72U3E__8_11();
			Enumerator_t2970655257  L_54 = L_53;
			Il2CppObject * L_55 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), &L_54);
			NullCheck((Il2CppObject *)L_55);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_55);
			IL2CPP_END_FINALLY(582)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(582)
	{
		IL2CPP_JUMP_TBL(0x2A9, IL_02a9)
		IL2CPP_JUMP_TBL(0x25B, IL_025b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_025b:
	{
		int32_t L_56 = (int32_t)__this->get_U3CcounterU3E__2_2();
		int32_t L_57 = (int32_t)__this->get_U3CnullCounterU3E__3_3();
		if ((!(((uint32_t)L_56) == ((uint32_t)L_57))))
		{
			goto IL_02a0;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_58 = V_3;
		List_1_t2058570427 * L_59 = (List_1_t2058570427 *)__this->get_U3CnullListU3E__1_1();
		Grouping_2_t308796194 * L_60 = (Grouping_2_t308796194 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16));
		((  void (*) (Grouping_2_t308796194 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)(L_60, (Il2CppObject *)L_58, (Il2CppObject*)L_59, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		__this->set_U24current_14(L_60);
		__this->set_U24PC_13(3);
		goto IL_02a9;
	}

IL_0292:
	{
		int32_t L_61 = (int32_t)__this->get_U3CcounterU3E__2_2();
		__this->set_U3CcounterU3E__2_2(((int32_t)((int32_t)L_61+(int32_t)1)));
	}

IL_02a0:
	{
		__this->set_U24PC_13((-1));
	}

IL_02a7:
	{
		return (bool)0;
	}

IL_02a9:
	{
		return (bool)1;
	}
	// Dead block : IL_02ab: ldloc.s V_4
}
// System.Void System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateGroupByIteratorU3Ec__Iterator5_2_Dispose_m3365483304_MetadataUsageId;
extern "C"  void U3CCreateGroupByIteratorU3Ec__Iterator5_2_Dispose_m3365483304_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateGroupByIteratorU3Ec__Iterator5_2_Dispose_m3365483304_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_13();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_13((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003f;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0029;
		}
		if (L_1 == 3)
		{
			goto IL_003f;
		}
	}
	{
		goto IL_003f;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3F, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		Enumerator_t2970655257  L_2 = (Enumerator_t2970655257 )__this->get_U3CU24s_72U3E__8_11();
		Enumerator_t2970655257  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), &L_3);
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		IL2CPP_END_FINALLY(46)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateGroupByIteratorU3Ec__Iterator5_2_Reset_m318413546_MetadataUsageId;
extern "C"  void U3CCreateGroupByIteratorU3Ec__Iterator5_2_Reset_m318413546_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateGroupByIteratorU3Ec__Iterator5_2_Reset_m318413546_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::.ctor()
extern "C"  void U3CCreateOfTypeIteratorU3Ec__IteratorC_1__ctor_m1442118915_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateOfTypeIteratorU3Ec__IteratorC_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2657146748_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateOfTypeIteratorU3Ec__IteratorC_1_System_Collections_IEnumerator_get_Current_m2253659481_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateOfTypeIteratorU3Ec__IteratorC_1_System_Collections_IEnumerable_GetEnumerator_m3988951508_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateOfTypeIteratorU3Ec__IteratorC_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2665668891_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * L_2 = (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *)L_2;
		U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CU24U3Esource_5();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateOfTypeIteratorU3Ec__IteratorC_1_MoveNext_m2883054397_MetadataUsageId;
extern "C"  bool U3CCreateOfTypeIteratorU3Ec__IteratorC_1_MoveNext_m2883054397_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_MoveNext_m2883054397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00bf;
	}

IL_0023:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_source_0();
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_U3CU24s_90U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0088;
			}
		}

IL_0043:
		{
			goto IL_0088;
		}

IL_0048:
		{
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_U3CU24s_90U3E__0_1();
			NullCheck((Il2CppObject *)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Il2CppObject * L_7 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			if (!((Il2CppObject *)IsInst(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
			{
				goto IL_0088;
			}
		}

IL_0069:
		{
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_4(((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC1, FINALLY_009d);
		}

IL_0088:
		{
			Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CU24s_90U3E__0_1();
			NullCheck((Il2CppObject *)L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			if (L_10)
			{
				goto IL_0048;
			}
		}

IL_0098:
		{
			IL2CPP_LEAVE(0xB8, FINALLY_009d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009d;
	}

FINALLY_009d:
	{ // begin finally (depth: 1)
		{
			bool L_11 = V_1;
			if (!L_11)
			{
				goto IL_00a1;
			}
		}

IL_00a0:
		{
			IL2CPP_END_FINALLY(157)
		}

IL_00a1:
		{
			Il2CppObject * L_12 = (Il2CppObject *)__this->get_U3CU24s_90U3E__0_1();
			V_2 = (Il2CppObject *)((Il2CppObject *)IsInst(L_12, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_13 = V_2;
			if (L_13)
			{
				goto IL_00b1;
			}
		}

IL_00b0:
		{
			IL2CPP_END_FINALLY(157)
		}

IL_00b1:
		{
			Il2CppObject * L_14 = V_2;
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(157)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(157)
	{
		IL2CPP_JUMP_TBL(0xC1, IL_00c1)
		IL2CPP_JUMP_TBL(0xB8, IL_00b8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b8:
	{
		__this->set_U24PC_3((-1));
	}

IL_00bf:
	{
		return (bool)0;
	}

IL_00c1:
	{
		return (bool)1;
	}
	// Dead block : IL_00c3: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Dispose_m2751205856_MetadataUsageId;
extern "C"  void U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Dispose_m2751205856_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Dispose_m2751205856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003d;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = (Il2CppObject *)__this->get_U3CU24s_90U3E__0_1();
			V_1 = (Il2CppObject *)((Il2CppObject *)IsInst(L_2, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_3 = V_1;
			if (L_3)
			{
				goto IL_0036;
			}
		}

IL_0035:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_0036:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck((Il2CppObject *)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Reset_m2518766250_MetadataUsageId;
extern "C"  void U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Reset_m2518766250_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Reset_m2518766250_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::.ctor()
extern "C"  void U3CCreateRepeatIteratorU3Ec__IteratorE_1__ctor_m181366867_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2551204196_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerator_get_Current_m1095994885_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerable_GetEnumerator_m2771816396_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2824030563_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method)
{
	U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * L_2 = (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 *)L_2;
		U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * L_3 = V_0;
		int32_t L_4 = (int32_t)__this->get_U3CU24U3Ecount_5();
		NullCheck(L_3);
		L_3->set_count_1(L_4);
		U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * L_5 = V_0;
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_U3CU24U3Eelement_6();
		NullCheck(L_5);
		L_5->set_element_2(L_6);
		U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::MoveNext()
extern "C"  bool U3CCreateRepeatIteratorU3Ec__IteratorE_1_MoveNext_m302587737_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_006b;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0053;
	}

IL_002d:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_element_2();
		__this->set_U24current_4(L_2);
		__this->set_U24PC_3(1);
		goto IL_006d;
	}

IL_0045:
	{
		int32_t L_3 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_3+(int32_t)1)));
	}

IL_0053:
	{
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		int32_t L_5 = (int32_t)__this->get_count_1();
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_3((-1));
	}

IL_006b:
	{
		return (bool)0;
	}

IL_006d:
	{
		return (bool)1;
	}
	// Dead block : IL_006f: ldloc.1
}
// System.Void System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::Dispose()
extern "C"  void U3CCreateRepeatIteratorU3Ec__IteratorE_1_Dispose_m1026478484_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateRepeatIteratorU3Ec__IteratorE_1_Reset_m3405375562_MetadataUsageId;
extern "C"  void U3CCreateRepeatIteratorU3Ec__IteratorE_1_Reset_m3405375562_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2110762548 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateRepeatIteratorU3Ec__IteratorE_1_Reset_m3405375562_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m1050683520_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  KeyValuePair_2_t38854645  U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2493583845_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m403409254_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_U24current_5();
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2334077993_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1038271860_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 * L_5 = V_0;
		Func_2_t3251821928 * L_6 = (Func_2_t3251821928 *)__this->get_U3CU24U3Eselector_7();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2496152208_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2496152208_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2496152208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00b3;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_91U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_007e;
			}
		}

IL_0043:
		{
			goto IL_007e;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			DictionaryEntry_t3048875398  L_6 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3251821928 * L_7 = (Func_2_t3251821928 *)__this->get_selector_3();
			DictionaryEntry_t3048875398  L_8 = (DictionaryEntry_t3048875398 )__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3251821928 *)L_7);
			KeyValuePair_2_t38854645  L_9 = ((  KeyValuePair_2_t38854645  (*) (Func_2_t3251821928 *, DictionaryEntry_t3048875398 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3251821928 *)L_7, (DictionaryEntry_t3048875398 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_5(L_9);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_0093);
		}

IL_007e:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xAC, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0097;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_0097:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ac:
	{
		__this->set_U24PC_4((-1));
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		return (bool)1;
	}
	// Dead block : IL_00b7: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m4066811839_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m4066811839_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m4066811839_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2559083421_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2559083421_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2092700746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2559083421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Object>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m543428203_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m917838622_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m2682929409_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m810311158_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2101417039_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 * L_5 = V_0;
		Func_2_t2019190572 * L_6 = (Func_2_t2019190572 *)__this->get_U3CU24U3Eselector_7();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2469156361_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2469156361_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2469156361_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00b3;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_91U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_007e;
			}
		}

IL_0043:
		{
			goto IL_007e;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			KeyValuePair_2_t1174980068  L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t1174980068  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t2019190572 * L_7 = (Func_2_t2019190572 *)__this->get_selector_3();
			KeyValuePair_2_t1174980068  L_8 = (KeyValuePair_2_t1174980068 )__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t2019190572 *)L_7);
			Il2CppObject * L_9 = ((  Il2CppObject * (*) (Func_2_t2019190572 *, KeyValuePair_2_t1174980068 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2019190572 *)L_7, (KeyValuePair_2_t1174980068 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_5(L_9);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_0093);
		}

IL_007e:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xAC, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0097;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_0097:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ac:
	{
		__this->set_U24PC_4((-1));
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		return (bool)1;
	}
	// Dead block : IL_00b7: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m3918854442_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m3918854442_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m3918854442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2809549096_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2809549096_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t860069390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2809549096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m229389818_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3962703587_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m3247994342_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2050097479_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m4086472356_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 * L_5 = V_0;
		Func_2_t2065790391 * L_6 = (Func_2_t2065790391 *)__this->get_U3CU24U3Eselector_7();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m4287015222_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m4287015222_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m4287015222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00b3;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_91U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_007e;
			}
		}

IL_0043:
		{
			goto IL_007e;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			KeyValuePair_2_t38854645  L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t38854645  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t2065790391 * L_7 = (Func_2_t2065790391 *)__this->get_selector_3();
			KeyValuePair_2_t38854645  L_8 = (KeyValuePair_2_t38854645 )__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t2065790391 *)L_7);
			Il2CppObject * L_9 = ((  Il2CppObject * (*) (Func_2_t2065790391 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2065790391 *)L_7, (KeyValuePair_2_t38854645 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_5(L_9);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_0093);
		}

IL_007e:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xAC, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0097;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_0097:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ac:
	{
		__this->set_U24PC_4((-1));
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		return (bool)1;
	}
	// Dead block : IL_00b7: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m3502713357_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m3502713357_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m3502713357_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m414668899_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m414668899_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t906669209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m414668899_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m2108257976_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppChar U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3045153909_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = (Il2CppChar)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m3060896522_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = (Il2CppChar)__this->get_U24current_5();
		Il2CppChar L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m494015497_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1188047868_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * L_5 = V_0;
		Func_2_t3590536224 * L_6 = (Func_2_t3590536224 *)__this->get_U3CU24U3Eselector_7();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2896766396_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2896766396_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m2896766396_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00b3;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_91U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_007e;
			}
		}

IL_0043:
		{
			goto IL_007e;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3590536224 * L_7 = (Func_2_t3590536224 *)__this->get_selector_3();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3590536224 *)L_7);
			Il2CppChar L_9 = ((  Il2CppChar (*) (Func_2_t3590536224 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3590536224 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_5(L_9);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_0093);
		}

IL_007e:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xAC, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0097;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_0097:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ac:
	{
		__this->set_U24PC_4((-1));
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		return (bool)1;
	}
	// Dead block : IL_00b7: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m228712283_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m228712283_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m228712283_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Char>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2696210045_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2696210045_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2431415042 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2696210045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m2480296441_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1090887326_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m2800698283_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m291907278_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m3708752605_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * L_5 = V_0;
		Func_2_t2825504181 * L_6 = (Func_2_t2825504181 *)__this->get_U3CU24U3Eselector_7();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3623917119_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3623917119_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3623917119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00b3;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_91U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_007e;
			}
		}

IL_0043:
		{
			goto IL_007e;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t2825504181 * L_7 = (Func_2_t2825504181 *)__this->get_selector_3();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t2825504181 *)L_7);
			Il2CppObject * L_9 = ((  Il2CppObject * (*) (Func_2_t2825504181 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2825504181 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_5(L_9);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_0093);
		}

IL_007e:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xAC, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0097;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_0097:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ac:
	{
		__this->set_U24PC_4((-1));
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		return (bool)1;
	}
	// Dead block : IL_00b7: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1234776362_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1234776362_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1234776362_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_91U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m4058120828_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m4058120828_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m4058120828_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Char,System.Object>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator11_2__ctor_m410789533_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Char,System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator11_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3226634874_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Char,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator11_2_System_Collections_IEnumerator_get_Current_m2852961519_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Char,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator11_2_System_Collections_IEnumerable_GetEnumerator_m1770984874_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Char,System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator11_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m628392673_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_5();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_7();
		NullCheck(L_3);
		L_3->set_source_1(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 * L_5 = V_0;
		Func_3_t2898077773 * L_6 = (Func_3_t2898077773 *)__this->get_U3CU24U3Eselector_8();
		NullCheck(L_5);
		L_5->set_selector_4(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Char,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator11_2_MoveNext_m1003948699_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator11_2_MoveNext_m1003948699_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator11_2_MoveNext_m1003948699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_00ce;
	}

IL_0023:
	{
		__this->set_U3CcounterU3E__0_0(0);
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_1();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Char>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_92U3E__1_2(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_003e:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_008b;
			}
		}

IL_004a:
		{
			goto IL_0099;
		}

IL_004f:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_92U3E__1_2();
			NullCheck((Il2CppObject*)L_5);
			Il2CppChar L_6 = InterfaceFuncInvoker0< Il2CppChar >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Char>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__2_3(L_6);
			Func_3_t2898077773 * L_7 = (Func_3_t2898077773 *)__this->get_selector_4();
			Il2CppChar L_8 = (Il2CppChar)__this->get_U3CelementU3E__2_3();
			int32_t L_9 = (int32_t)__this->get_U3CcounterU3E__0_0();
			NullCheck((Func_3_t2898077773 *)L_7);
			Il2CppObject * L_10 = ((  Il2CppObject * (*) (Func_3_t2898077773 *, Il2CppChar, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_3_t2898077773 *)L_7, (Il2CppChar)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_6(L_10);
			__this->set_U24PC_5(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xD0, FINALLY_00ae);
		}

IL_008b:
		{
			int32_t L_11 = (int32_t)__this->get_U3CcounterU3E__0_0();
			__this->set_U3CcounterU3E__0_0(((int32_t)((int32_t)L_11+(int32_t)1)));
		}

IL_0099:
		{
			Il2CppObject* L_12 = (Il2CppObject*)__this->get_U3CU24s_92U3E__1_2();
			NullCheck((Il2CppObject *)L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			if (L_13)
			{
				goto IL_004f;
			}
		}

IL_00a9:
		{
			IL2CPP_LEAVE(0xC7, FINALLY_00ae);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00ae;
	}

FINALLY_00ae:
	{ // begin finally (depth: 1)
		{
			bool L_14 = V_1;
			if (!L_14)
			{
				goto IL_00b2;
			}
		}

IL_00b1:
		{
			IL2CPP_END_FINALLY(174)
		}

IL_00b2:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_92U3E__1_2();
			if (L_15)
			{
				goto IL_00bb;
			}
		}

IL_00ba:
		{
			IL2CPP_END_FINALLY(174)
		}

IL_00bb:
		{
			Il2CppObject* L_16 = (Il2CppObject*)__this->get_U3CU24s_92U3E__1_2();
			NullCheck((Il2CppObject *)L_16);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_16);
			IL2CPP_END_FINALLY(174)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(174)
	{
		IL2CPP_JUMP_TBL(0xD0, IL_00d0)
		IL2CPP_JUMP_TBL(0xC7, IL_00c7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00c7:
	{
		__this->set_U24PC_5((-1));
	}

IL_00ce:
	{
		return (bool)0;
	}

IL_00d0:
	{
		return (bool)1;
	}
	// Dead block : IL_00d2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Char,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator11_2_Dispose_m2481899094_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator11_2_Dispose_m2481899094_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator11_2_Dispose_m2481899094_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_92U3E__1_2();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_92U3E__1_2();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Char,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator11_2_Reset_m2012683992_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator11_2_Reset_m2012683992_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t947348529 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator11_2_Reset_m2012683992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator11_2__ctor_m977913684_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator11_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m315178457_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator11_2_System_Collections_IEnumerator_get_Current_m416190534_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator11_2_System_Collections_IEnumerable_GetEnumerator_m815218413_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator11_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2061289208_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_5();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_7();
		NullCheck(L_3);
		L_3->set_source_1(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 * L_5 = V_0;
		Func_3_t2785112090 * L_6 = (Func_3_t2785112090 *)__this->get_U3CU24U3Eselector_8();
		NullCheck(L_5);
		L_5->set_selector_4(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator11_2_MoveNext_m3799994016_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator11_2_MoveNext_m3799994016_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator11_2_MoveNext_m3799994016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_00ce;
	}

IL_0023:
	{
		__this->set_U3CcounterU3E__0_0(0);
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_1();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_92U3E__1_2(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_003e:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_008b;
			}
		}

IL_004a:
		{
			goto IL_0099;
		}

IL_004f:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_92U3E__1_2();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__2_3(L_6);
			Func_3_t2785112090 * L_7 = (Func_3_t2785112090 *)__this->get_selector_4();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__2_3();
			int32_t L_9 = (int32_t)__this->get_U3CcounterU3E__0_0();
			NullCheck((Func_3_t2785112090 *)L_7);
			Il2CppObject * L_10 = ((  Il2CppObject * (*) (Func_3_t2785112090 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_3_t2785112090 *)L_7, (Il2CppObject *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_6(L_10);
			__this->set_U24PC_5(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xD0, FINALLY_00ae);
		}

IL_008b:
		{
			int32_t L_11 = (int32_t)__this->get_U3CcounterU3E__0_0();
			__this->set_U3CcounterU3E__0_0(((int32_t)((int32_t)L_11+(int32_t)1)));
		}

IL_0099:
		{
			Il2CppObject* L_12 = (Il2CppObject*)__this->get_U3CU24s_92U3E__1_2();
			NullCheck((Il2CppObject *)L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			if (L_13)
			{
				goto IL_004f;
			}
		}

IL_00a9:
		{
			IL2CPP_LEAVE(0xC7, FINALLY_00ae);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00ae;
	}

FINALLY_00ae:
	{ // begin finally (depth: 1)
		{
			bool L_14 = V_1;
			if (!L_14)
			{
				goto IL_00b2;
			}
		}

IL_00b1:
		{
			IL2CPP_END_FINALLY(174)
		}

IL_00b2:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_92U3E__1_2();
			if (L_15)
			{
				goto IL_00bb;
			}
		}

IL_00ba:
		{
			IL2CPP_END_FINALLY(174)
		}

IL_00bb:
		{
			Il2CppObject* L_16 = (Il2CppObject*)__this->get_U3CU24s_92U3E__1_2();
			NullCheck((Il2CppObject *)L_16);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_16);
			IL2CPP_END_FINALLY(174)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(174)
	{
		IL2CPP_JUMP_TBL(0xD0, IL_00d0)
		IL2CPP_JUMP_TBL(0xC7, IL_00c7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00c7:
	{
		__this->set_U24PC_5((-1));
	}

IL_00ce:
	{
		return (bool)0;
	}

IL_00d0:
	{
		return (bool)1;
	}
	// Dead block : IL_00d2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator11_2_Dispose_m760956911_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator11_2_Dispose_m760956911_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator11_2_Dispose_m760956911_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_92U3E__1_2();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_92U3E__1_2();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator11`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator11_2_Reset_m1541804129_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator11_2_Reset_m1541804129_gshared (U3CCreateSelectIteratorU3Ec__Iterator11_2_t3233898664 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator11_2_Reset_m1541804129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CCreateSelectManyIteratorU3Ec__Iterator12_2__ctor_m1032211654_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectManyIteratorU3Ec__Iterator12_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2602589465_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectManyIteratorU3Ec__Iterator12_2_System_Collections_IEnumerator_get_Current_m2008076796_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectManyIteratorU3Ec__Iterator12_2_System_Collections_IEnumerable_GetEnumerator_m2451344661_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectManyIteratorU3Ec__Iterator12_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m458591338_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_6();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * L_2 = (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 *)L_2;
		U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_8();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * L_5 = V_0;
		Func_2_t3117631226 * L_6 = (Func_2_t3117631226 *)__this->get_U3CU24U3Eselector_9();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectManyIteratorU3Ec__Iterator12_2_MoveNext_m2494996458_MetadataUsageId;
extern "C"  bool U3CCreateSelectManyIteratorU3Ec__Iterator12_2_MoveNext_m2494996458_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_MoveNext_m2494996458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_0117;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_93U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0078;
			}
		}

IL_0043:
		{
			goto IL_00e2;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_93U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3117631226 * L_7 = (Func_2_t3117631226 *)__this->get_selector_3();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3117631226 *)L_7);
			Il2CppObject* L_9 = ((  Il2CppObject* (*) (Func_2_t3117631226 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3117631226 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			NullCheck((Il2CppObject*)L_9);
			Il2CppObject* L_10 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), (Il2CppObject*)L_9);
			__this->set_U3CU24s_94U3E__2_4(L_10);
			V_0 = (uint32_t)((int32_t)-3);
		}

IL_0078:
		try
		{ // begin try (depth: 2)
			{
				uint32_t L_11 = V_0;
				if (((int32_t)((int32_t)L_11-(int32_t)1)) == 0)
				{
					goto IL_00b4;
				}
			}

IL_0084:
			{
				goto IL_00b4;
			}

IL_0089:
			{
				Il2CppObject* L_12 = (Il2CppObject*)__this->get_U3CU24s_94U3E__2_4();
				NullCheck((Il2CppObject*)L_12);
				Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (Il2CppObject*)L_12);
				__this->set_U3CitemU3E__3_5(L_13);
				Il2CppObject * L_14 = (Il2CppObject *)__this->get_U3CitemU3E__3_5();
				__this->set_U24current_7(L_14);
				__this->set_U24PC_6(1);
				V_1 = (bool)1;
				IL2CPP_LEAVE(0x119, FINALLY_00c9);
			}

IL_00b4:
			{
				Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_94U3E__2_4();
				NullCheck((Il2CppObject *)L_15);
				bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
				if (L_16)
				{
					goto IL_0089;
				}
			}

IL_00c4:
			{
				IL2CPP_LEAVE(0xE2, FINALLY_00c9);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_00c9;
		}

FINALLY_00c9:
		{ // begin finally (depth: 2)
			{
				bool L_17 = V_1;
				if (!L_17)
				{
					goto IL_00cd;
				}
			}

IL_00cc:
			{
				IL2CPP_END_FINALLY(201)
			}

IL_00cd:
			{
				Il2CppObject* L_18 = (Il2CppObject*)__this->get_U3CU24s_94U3E__2_4();
				if (L_18)
				{
					goto IL_00d6;
				}
			}

IL_00d5:
			{
				IL2CPP_END_FINALLY(201)
			}

IL_00d6:
			{
				Il2CppObject* L_19 = (Il2CppObject*)__this->get_U3CU24s_94U3E__2_4();
				NullCheck((Il2CppObject *)L_19);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
				IL2CPP_END_FINALLY(201)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(201)
		{
			IL2CPP_END_CLEANUP(0x119, FINALLY_00f7);
			IL2CPP_JUMP_TBL(0xE2, IL_00e2)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_00e2:
		{
			Il2CppObject* L_20 = (Il2CppObject*)__this->get_U3CU24s_93U3E__0_1();
			NullCheck((Il2CppObject *)L_20);
			bool L_21 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_20);
			if (L_21)
			{
				goto IL_0048;
			}
		}

IL_00f2:
		{
			IL2CPP_LEAVE(0x110, FINALLY_00f7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		{
			bool L_22 = V_1;
			if (!L_22)
			{
				goto IL_00fb;
			}
		}

IL_00fa:
		{
			IL2CPP_END_FINALLY(247)
		}

IL_00fb:
		{
			Il2CppObject* L_23 = (Il2CppObject*)__this->get_U3CU24s_93U3E__0_1();
			if (L_23)
			{
				goto IL_0104;
			}
		}

IL_0103:
		{
			IL2CPP_END_FINALLY(247)
		}

IL_0104:
		{
			Il2CppObject* L_24 = (Il2CppObject*)__this->get_U3CU24s_93U3E__0_1();
			NullCheck((Il2CppObject *)L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_24);
			IL2CPP_END_FINALLY(247)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0x119, IL_0119)
		IL2CPP_JUMP_TBL(0x110, IL_0110)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0110:
	{
		__this->set_U24PC_6((-1));
	}

IL_0117:
	{
		return (bool)0;
	}

IL_0119:
	{
		return (bool)1;
	}
	// Dead block : IL_011b: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectManyIteratorU3Ec__Iterator12_2_Dispose_m1260464891_MetadataUsageId;
extern "C"  void U3CCreateSelectManyIteratorU3Ec__Iterator12_2_Dispose_m1260464891_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_Dispose_m1260464891_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0055;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0055;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			IL2CPP_LEAVE(0x3B, FINALLY_0026);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0026;
		}

FINALLY_0026:
		{ // begin finally (depth: 2)
			{
				Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_94U3E__2_4();
				if (L_2)
				{
					goto IL_002f;
				}
			}

IL_002e:
			{
				IL2CPP_END_FINALLY(38)
			}

IL_002f:
			{
				Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_94U3E__2_4();
				NullCheck((Il2CppObject *)L_3);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
				IL2CPP_END_FINALLY(38)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(38)
		{
			IL2CPP_JUMP_TBL(0x3B, IL_003b)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x55, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24s_93U3E__0_1();
			if (L_4)
			{
				goto IL_0049;
			}
		}

IL_0048:
		{
			IL2CPP_END_FINALLY(64)
		}

IL_0049:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_93U3E__0_1();
			NullCheck((Il2CppObject *)L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			IL2CPP_END_FINALLY(64)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectManyIteratorU3Ec__Iterator12_2_Reset_m3641182809_MetadataUsageId;
extern "C"  void U3CCreateSelectManyIteratorU3Ec__Iterator12_2_Reset_m3641182809_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_Reset_m3641182809_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::.ctor()
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1__ctor_m875182872_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m4044887707_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerator_get_Current_m574771290_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerable_GetEnumerator_m416486751_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1315560348_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 * __this, const MethodInfo* method)
{
	U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_5();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 * L_2 = (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 *)L_2;
		U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 * L_3 = V_0;
		int32_t L_4 = (int32_t)__this->get_U3CU24U3Ecount_7();
		NullCheck(L_3);
		L_3->set_count_0(L_4);
		U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 * L_5 = V_0;
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24U3Esource_8();
		NullCheck(L_5);
		L_5->set_source_2(L_6);
		U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m2948841636_MetadataUsageId;
extern "C"  bool U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m2948841636_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m2948841636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_004f;
		}
	}
	{
		goto IL_00e1;
	}

IL_0023:
	{
		int32_t L_2 = (int32_t)__this->get_count_0();
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_00e1;
	}

IL_0034:
	{
		__this->set_U3CcounterU3E__0_1(0);
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_source_2();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_3);
		__this->set_U3CU24s_113U3E__1_3(L_4);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
			{
				goto IL_008b;
			}
		}

IL_005b:
		{
			goto IL_00ac;
		}

IL_0060:
		{
			Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24s_113U3E__1_3();
			NullCheck((Il2CppObject*)L_6);
			Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_6);
			__this->set_U3CelementU3E__2_4(L_7);
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__2_4();
			__this->set_U24current_6(L_8);
			__this->set_U24PC_5(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xE3, FINALLY_00c1);
		}

IL_008b:
		{
			int32_t L_9 = (int32_t)__this->get_U3CcounterU3E__0_1();
			int32_t L_10 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
			V_2 = (int32_t)L_10;
			__this->set_U3CcounterU3E__0_1(L_10);
			int32_t L_11 = V_2;
			int32_t L_12 = (int32_t)__this->get_count_0();
			if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
			{
				goto IL_00ac;
			}
		}

IL_00a7:
		{
			IL2CPP_LEAVE(0xE1, FINALLY_00c1);
		}

IL_00ac:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_113U3E__1_3();
			NullCheck((Il2CppObject *)L_13);
			bool L_14 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
			if (L_14)
			{
				goto IL_0060;
			}
		}

IL_00bc:
		{
			IL2CPP_LEAVE(0xDA, FINALLY_00c1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c1;
	}

FINALLY_00c1:
	{ // begin finally (depth: 1)
		{
			bool L_15 = V_1;
			if (!L_15)
			{
				goto IL_00c5;
			}
		}

IL_00c4:
		{
			IL2CPP_END_FINALLY(193)
		}

IL_00c5:
		{
			Il2CppObject* L_16 = (Il2CppObject*)__this->get_U3CU24s_113U3E__1_3();
			if (L_16)
			{
				goto IL_00ce;
			}
		}

IL_00cd:
		{
			IL2CPP_END_FINALLY(193)
		}

IL_00ce:
		{
			Il2CppObject* L_17 = (Il2CppObject*)__this->get_U3CU24s_113U3E__1_3();
			NullCheck((Il2CppObject *)L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_17);
			IL2CPP_END_FINALLY(193)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(193)
	{
		IL2CPP_JUMP_TBL(0xE3, IL_00e3)
		IL2CPP_JUMP_TBL(0xE1, IL_00e1)
		IL2CPP_JUMP_TBL(0xDA, IL_00da)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00da:
	{
		__this->set_U24PC_5((-1));
	}

IL_00e1:
	{
		return (bool)0;
	}

IL_00e3:
	{
		return (bool)1;
	}
	// Dead block : IL_00e5: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m1966473705_MetadataUsageId;
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m1966473705_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m1966473705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_113U3E__1_3();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_113U3E__1_3();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m3242233227_MetadataUsageId;
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m3242233227_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1384452859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m3242233227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Object>::.ctor()
extern "C"  void U3CCreateUnionIteratorU3Ec__Iterator1C_1__ctor_m2313864678_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m770439025_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_9();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_IEnumerator_get_Current_m1871300990_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_9();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_IEnumerable_GetEnumerator_m2178731317_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m4102151200_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * __this, const MethodInfo* method)
{
	U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_8();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * L_2 = (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 *)L_2;
		U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Ecomparer_10();
		NullCheck(L_3);
		L_3->set_comparer_0(L_4);
		U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * L_5 = V_0;
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24U3Efirst_11();
		NullCheck(L_5);
		L_5->set_first_2(L_6);
		U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * L_7 = V_0;
		Il2CppObject* L_8 = (Il2CppObject*)__this->get_U3CU24U3Esecond_12();
		NullCheck(L_7);
		L_7->set_second_5(L_8);
		U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * L_9 = V_0;
		return L_9;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateUnionIteratorU3Ec__Iterator1C_1_MoveNext_m2175622162_MetadataUsageId;
extern "C"  bool U3CCreateUnionIteratorU3Ec__Iterator1C_1_MoveNext_m2175622162_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateUnionIteratorU3Ec__Iterator1C_1_MoveNext_m2175622162_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_8();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_8((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0027;
		}
		if (L_1 == 1)
		{
			goto IL_004c;
		}
		if (L_1 == 2)
		{
			goto IL_00f2;
		}
	}
	{
		goto IL_0191;
	}

IL_0027:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_comparer_0();
		HashSet_1_t1022910149 * L_3 = (HashSet_1_t1022910149 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (HashSet_1_t1022910149 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		__this->set_U3CitemsU3E__0_1(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_first_2();
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject* L_5 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_4);
		__this->set_U3CU24s_118U3E__1_3(L_5);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_004c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_6 = V_0;
			if (((int32_t)((int32_t)L_6-(int32_t)1)) == 0)
			{
				goto IL_00b0;
			}
		}

IL_0058:
		{
			goto IL_00b0;
		}

IL_005d:
		{
			Il2CppObject* L_7 = (Il2CppObject*)__this->get_U3CU24s_118U3E__1_3();
			NullCheck((Il2CppObject*)L_7);
			Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), (Il2CppObject*)L_7);
			__this->set_U3CelementU3E__2_4(L_8);
			HashSet_1_t1022910149 * L_9 = (HashSet_1_t1022910149 *)__this->get_U3CitemsU3E__0_1();
			Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CelementU3E__2_4();
			NullCheck((HashSet_1_t1022910149 *)L_9);
			bool L_11 = ((  bool (*) (HashSet_1_t1022910149 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((HashSet_1_t1022910149 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
			if (L_11)
			{
				goto IL_00b0;
			}
		}

IL_0084:
		{
			HashSet_1_t1022910149 * L_12 = (HashSet_1_t1022910149 *)__this->get_U3CitemsU3E__0_1();
			Il2CppObject * L_13 = (Il2CppObject *)__this->get_U3CelementU3E__2_4();
			NullCheck((HashSet_1_t1022910149 *)L_12);
			((  bool (*) (HashSet_1_t1022910149 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((HashSet_1_t1022910149 *)L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			Il2CppObject * L_14 = (Il2CppObject *)__this->get_U3CelementU3E__2_4();
			__this->set_U24current_9(L_14);
			__this->set_U24PC_8(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x193, FINALLY_00c5);
		}

IL_00b0:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_118U3E__1_3();
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_005d;
			}
		}

IL_00c0:
		{
			IL2CPP_LEAVE(0xDE, FINALLY_00c5);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c5;
	}

FINALLY_00c5:
	{ // begin finally (depth: 1)
		{
			bool L_17 = V_1;
			if (!L_17)
			{
				goto IL_00c9;
			}
		}

IL_00c8:
		{
			IL2CPP_END_FINALLY(197)
		}

IL_00c9:
		{
			Il2CppObject* L_18 = (Il2CppObject*)__this->get_U3CU24s_118U3E__1_3();
			if (L_18)
			{
				goto IL_00d2;
			}
		}

IL_00d1:
		{
			IL2CPP_END_FINALLY(197)
		}

IL_00d2:
		{
			Il2CppObject* L_19 = (Il2CppObject*)__this->get_U3CU24s_118U3E__1_3();
			NullCheck((Il2CppObject *)L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
			IL2CPP_END_FINALLY(197)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(197)
	{
		IL2CPP_JUMP_TBL(0x193, IL_0193)
		IL2CPP_JUMP_TBL(0xDE, IL_00de)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00de:
	{
		Il2CppObject* L_20 = (Il2CppObject*)__this->get_second_5();
		NullCheck((Il2CppObject*)L_20);
		Il2CppObject* L_21 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_20);
		__this->set_U3CU24s_119U3E__3_6(L_21);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_00f2:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_22 = V_0;
			if (((int32_t)((int32_t)L_22-(int32_t)2)) == 0)
			{
				goto IL_015c;
			}
		}

IL_00fe:
		{
			goto IL_015c;
		}

IL_0103:
		{
			Il2CppObject* L_23 = (Il2CppObject*)__this->get_U3CU24s_119U3E__3_6();
			NullCheck((Il2CppObject*)L_23);
			Il2CppObject * L_24 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), (Il2CppObject*)L_23);
			__this->set_U3CelementU3E__4_7(L_24);
			HashSet_1_t1022910149 * L_25 = (HashSet_1_t1022910149 *)__this->get_U3CitemsU3E__0_1();
			Il2CppObject * L_26 = (Il2CppObject *)__this->get_U3CelementU3E__4_7();
			Il2CppObject* L_27 = (Il2CppObject*)__this->get_comparer_0();
			bool L_28 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_25, (Il2CppObject *)L_26, (Il2CppObject*)L_27, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			if (L_28)
			{
				goto IL_015c;
			}
		}

IL_0130:
		{
			HashSet_1_t1022910149 * L_29 = (HashSet_1_t1022910149 *)__this->get_U3CitemsU3E__0_1();
			Il2CppObject * L_30 = (Il2CppObject *)__this->get_U3CelementU3E__4_7();
			NullCheck((HashSet_1_t1022910149 *)L_29);
			((  bool (*) (HashSet_1_t1022910149 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((HashSet_1_t1022910149 *)L_29, (Il2CppObject *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			Il2CppObject * L_31 = (Il2CppObject *)__this->get_U3CelementU3E__4_7();
			__this->set_U24current_9(L_31);
			__this->set_U24PC_8(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x193, FINALLY_0171);
		}

IL_015c:
		{
			Il2CppObject* L_32 = (Il2CppObject*)__this->get_U3CU24s_119U3E__3_6();
			NullCheck((Il2CppObject *)L_32);
			bool L_33 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_32);
			if (L_33)
			{
				goto IL_0103;
			}
		}

IL_016c:
		{
			IL2CPP_LEAVE(0x18A, FINALLY_0171);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0171;
	}

FINALLY_0171:
	{ // begin finally (depth: 1)
		{
			bool L_34 = V_1;
			if (!L_34)
			{
				goto IL_0175;
			}
		}

IL_0174:
		{
			IL2CPP_END_FINALLY(369)
		}

IL_0175:
		{
			Il2CppObject* L_35 = (Il2CppObject*)__this->get_U3CU24s_119U3E__3_6();
			if (L_35)
			{
				goto IL_017e;
			}
		}

IL_017d:
		{
			IL2CPP_END_FINALLY(369)
		}

IL_017e:
		{
			Il2CppObject* L_36 = (Il2CppObject*)__this->get_U3CU24s_119U3E__3_6();
			NullCheck((Il2CppObject *)L_36);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_36);
			IL2CPP_END_FINALLY(369)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(369)
	{
		IL2CPP_JUMP_TBL(0x193, IL_0193)
		IL2CPP_JUMP_TBL(0x18A, IL_018a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_018a:
	{
		__this->set_U24PC_8((-1));
	}

IL_0191:
	{
		return (bool)0;
	}

IL_0193:
	{
		return (bool)1;
	}
	// Dead block : IL_0195: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateUnionIteratorU3Ec__Iterator1C_1_Dispose_m3920016711_MetadataUsageId;
extern "C"  void U3CCreateUnionIteratorU3Ec__Iterator1C_1_Dispose_m3920016711_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateUnionIteratorU3Ec__Iterator1C_1_Dispose_m3920016711_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_8();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_8((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_005e;
		}
		if (L_1 == 1)
		{
			goto IL_0025;
		}
		if (L_1 == 2)
		{
			goto IL_0044;
		}
	}
	{
		goto IL_005e;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3F, FINALLY_002a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_118U3E__1_3();
			if (L_2)
			{
				goto IL_0033;
			}
		}

IL_0032:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_0033:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_118U3E__1_3();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003f:
	{
		goto IL_005e;
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x5E, FINALLY_0049);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24s_119U3E__3_6();
			if (L_4)
			{
				goto IL_0052;
			}
		}

IL_0051:
		{
			IL2CPP_END_FINALLY(73)
		}

IL_0052:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_119U3E__3_6();
			NullCheck((Il2CppObject *)L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			IL2CPP_END_FINALLY(73)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005e:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateUnionIteratorU3Ec__Iterator1C_1_Reset_m3148265245_MetadataUsageId;
extern "C"  void U3CCreateUnionIteratorU3Ec__Iterator1C_1_Reset_m3148265245_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t3206829433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateUnionIteratorU3Ec__Iterator1C_1_Reset_m3148265245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m1749763100_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  int32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2517370587_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m437866568_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U24current_5();
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m2698500399_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1921361290_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * L_5 = V_0;
		Func_2_t3451171634 * L_6 = (Func_2_t3451171634 *)__this->get_U3CU24U3Epredicate_7();
		NullCheck(L_5);
		L_5->set_predicate_3(L_6);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m2026968792_MetadataUsageId;
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m2026968792_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m2026968792_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Schema.JsonSchemaType>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_120U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0089;
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3451171634 * L_7 = (Func_2_t3451171634 *)__this->get_predicate_3();
			int32_t L_8 = (int32_t)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3451171634 *)L_7);
			bool L_9 = ((  bool (*) (Func_2_t3451171634 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3451171634 *)L_7, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			int32_t L_10 = (int32_t)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_5(L_10);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b7:
	{
		__this->set_U24PC_4((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m2109986641_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m2109986641_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m2109986641_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m3466846523_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m3466846523_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m3466846523_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m2460085447_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  KeyValuePair_2_t1174980068  U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m4004224286_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1174980068  L_0 = (KeyValuePair_2_t1174980068 )__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m305792465_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1174980068  L_0 = (KeyValuePair_2_t1174980068 )__this->get_U24current_5();
		KeyValuePair_2_t1174980068  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m1937803638_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2307257487_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 * L_5 = V_0;
		Func_2_t3155315995 * L_6 = (Func_2_t3155315995 *)__this->get_U3CU24U3Epredicate_7();
		NullCheck(L_5);
		L_5->set_predicate_3(L_6);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m851497709_MetadataUsageId;
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m851497709_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m851497709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_120U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0089;
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			KeyValuePair_2_t1174980068  L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t1174980068  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3155315995 * L_7 = (Func_2_t3155315995 *)__this->get_predicate_3();
			KeyValuePair_2_t1174980068  L_8 = (KeyValuePair_2_t1174980068 )__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3155315995 *)L_7);
			bool L_9 = ((  bool (*) (Func_2_t3155315995 *, KeyValuePair_2_t1174980068 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3155315995 *)L_7, (KeyValuePair_2_t1174980068 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			KeyValuePair_2_t1174980068  L_10 = (KeyValuePair_2_t1174980068 )__this->get_U3CelementU3E__1_2();
			__this->set_U24current_5(L_10);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b7:
	{
		__this->set_U24PC_4((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m2246955402_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m2246955402_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m2246955402_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1391564032_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1391564032_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2607269643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1391564032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m3241491898_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  KeyValuePair_2_t38854645  U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3715114631_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3290621412_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_U24current_5();
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3210790035_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2045467526_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 * L_5 = V_0;
		Func_2_t3201915814 * L_6 = (Func_2_t3201915814 *)__this->get_U3CU24U3Epredicate_7();
		NullCheck(L_5);
		L_5->set_predicate_3(L_6);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m272601826_MetadataUsageId;
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m272601826_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m272601826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_120U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0089;
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			KeyValuePair_2_t38854645  L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t38854645  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3201915814 * L_7 = (Func_2_t3201915814 *)__this->get_predicate_3();
			KeyValuePair_2_t38854645  L_8 = (KeyValuePair_2_t38854645 )__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3201915814 *)L_7);
			bool L_9 = ((  bool (*) (Func_2_t3201915814 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3201915814 *)L_7, (KeyValuePair_2_t38854645 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			KeyValuePair_2_t38854645  L_10 = (KeyValuePair_2_t38854645 )__this->get_U3CelementU3E__1_2();
			__this->set_U24current_5(L_10);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b7:
	{
		__this->set_U24PC_4((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m3693355429_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m3693355429_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m3693355429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1329026503_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1329026503_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1471144220 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1329026503_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m1958283157_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3602665650_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m269113779_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3279674866_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2682676065_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_5 = V_0;
		Func_2_t3961629604 * L_6 = (Func_2_t3961629604 *)__this->get_U3CU24U3Epredicate_7();
		NullCheck(L_5);
		L_5->set_predicate_3(L_6);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_MetadataUsageId;
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_120U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0089;
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3961629604 * L_7 = (Func_2_t3961629604 *)__this->get_predicate_3();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3961629604 *)L_7);
			bool L_9 = ((  bool (*) (Func_2_t3961629604 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3961629604 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_5(L_10);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b7:
	{
		__this->set_U24PC_4((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/Function`1<System.Object>::.cctor()
extern "C"  void Function_1__cctor_m3392077889_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Func_2_t2825504181 * L_0 = ((Function_1_t978359491_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Func_2_t2825504181 * L_2 = (Func_2_t2825504181 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Func_2_t2825504181 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((Function_1_t978359491_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
	}

IL_0018:
	{
		Func_2_t2825504181 * L_3 = ((Function_1_t978359491_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		((Function_1_t978359491_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Identity_0(L_3);
		return;
	}
}
// T System.Linq.Enumerable/Function`1<System.Object>::<Identity>m__4E(T)
extern "C"  Il2CppObject * Function_1_U3CIdentityU3Em__4E_m3892209529_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___t0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___t0;
		return L_0;
	}
}
// System.Void System.Linq.Grouping`2<System.Object,System.Object>::.ctor(K,System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void Grouping_2__ctor_m2008618495_gshared (Grouping_2_t308796194 * __this, Il2CppObject * ___key0, Il2CppObject* ___group1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___group1;
		__this->set_group_1(L_0);
		Il2CppObject * L_1 = ___key0;
		__this->set_key_0(L_1);
		return;
	}
}
// System.Collections.IEnumerator System.Linq.Grouping`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Grouping_2_System_Collections_IEnumerable_GetEnumerator_m3099579774_gshared (Grouping_2_t308796194 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_group_1();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Linq.Grouping`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* Grouping_2_GetEnumerator_m4103693023_gshared (Grouping_2_t308796194 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_group_1();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Void System.Linq.OrderedEnumerable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  void OrderedEnumerable_1__ctor_m1650659453_gshared (OrderedEnumerable_1_t4214082274 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___source0;
		__this->set_source_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Linq.OrderedEnumerable`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m430310697_gshared (OrderedEnumerable_1_t4214082274 * __this, const MethodInfo* method)
{
	{
		NullCheck((OrderedEnumerable_1_t4214082274 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (OrderedEnumerable_1_t4214082274 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((OrderedEnumerable_1_t4214082274 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* OrderedEnumerable_1_GetEnumerator_m2325923724_gshared (OrderedEnumerable_1_t4214082274 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_0();
		NullCheck((OrderedEnumerable_1_t4214082274 *)__this);
		Il2CppObject* L_1 = VirtFuncInvoker1< Il2CppObject*, Il2CppObject* >::Invoke(7 /* System.Collections.Generic.IEnumerable`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::Sort(System.Collections.Generic.IEnumerable`1<TElement>) */, (OrderedEnumerable_1_t4214082274 *)__this, (Il2CppObject*)L_0);
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_1);
		return L_2;
	}
}
// System.Void System.Linq.OrderedSequence`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m1933455561_gshared (OrderedSequence_2_t1710216260 * __this, Il2CppObject* ___source0, Func_2_t2207932334 * ___key_selector1, Il2CppObject* ___comparer2, int32_t ___direction3, const MethodInfo* method)
{
	Il2CppObject* G_B2_0 = NULL;
	OrderedSequence_2_t1710216260 * G_B2_1 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	OrderedSequence_2_t1710216260 * G_B1_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedEnumerable_1_t4214082274 *)__this);
		((  void (*) (OrderedEnumerable_1_t4214082274 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((OrderedEnumerable_1_t4214082274 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t2207932334 * L_1 = ___key_selector1;
		__this->set_selector_2(L_1);
		Il2CppObject* L_2 = ___comparer2;
		Il2CppObject* L_3 = (Il2CppObject*)L_2;
		G_B1_0 = L_3;
		G_B1_1 = ((OrderedSequence_2_t1710216260 *)(__this));
		if (L_3)
		{
			G_B2_0 = L_3;
			G_B2_1 = ((OrderedSequence_2_t1710216260 *)(__this));
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		Comparer_1_t961886567 * L_4 = ((  Comparer_1_t961886567 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		G_B2_0 = ((Il2CppObject*)(L_4));
		G_B2_1 = ((OrderedSequence_2_t1710216260 *)(G_B1_1));
	}

IL_001c:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_comparer_3(G_B2_0);
		int32_t L_5 = ___direction3;
		__this->set_direction_4(L_5);
		return;
	}
}
// System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Int32>::CreateContext(System.Linq.SortContext`1<TElement>)
extern "C"  SortContext_1_t1798778454 * OrderedSequence_2_CreateContext_m2905460903_gshared (OrderedSequence_2_t1710216260 * __this, SortContext_1_t1798778454 * ___current0, const MethodInfo* method)
{
	SortContext_1_t1798778454 * V_0 = NULL;
	{
		Func_2_t2207932334 * L_0 = (Func_2_t2207932334 *)__this->get_selector_2();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_comparer_3();
		int32_t L_2 = (int32_t)__this->get_direction_4();
		SortContext_1_t1798778454 * L_3 = ___current0;
		SortSequenceContext_2_t2443723226 * L_4 = (SortSequenceContext_2_t2443723226 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (SortSequenceContext_2_t2443723226 *, Func_2_t2207932334 *, Il2CppObject*, int32_t, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_4, (Func_2_t2207932334 *)L_0, (Il2CppObject*)L_1, (int32_t)L_2, (SortContext_1_t1798778454 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (SortContext_1_t1798778454 *)L_4;
		OrderedEnumerable_1_t4214082274 * L_5 = (OrderedEnumerable_1_t4214082274 *)__this->get_parent_1();
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		OrderedEnumerable_1_t4214082274 * L_6 = (OrderedEnumerable_1_t4214082274 *)__this->get_parent_1();
		SortContext_1_t1798778454 * L_7 = V_0;
		NullCheck((OrderedEnumerable_1_t4214082274 *)L_6);
		SortContext_1_t1798778454 * L_8 = VirtFuncInvoker1< SortContext_1_t1798778454 *, SortContext_1_t1798778454 * >::Invoke(6 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedEnumerable_1_t4214082274 *)L_6, (SortContext_1_t1798778454 *)L_7);
		return L_8;
	}

IL_0031:
	{
		SortContext_1_t1798778454 * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Int32>::Sort(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  Il2CppObject* OrderedSequence_2_Sort_m3623126160_gshared (OrderedSequence_2_t1710216260 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedSequence_2_t1710216260 *)__this);
		SortContext_1_t1798778454 * L_1 = VirtFuncInvoker1< SortContext_1_t1798778454 *, SortContext_1_t1798778454 * >::Invoke(6 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Int32>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedSequence_2_t1710216260 *)__this, (SortContext_1_t1798778454 *)NULL);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, (SortContext_1_t1798778454 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_2;
	}
}
// System.Void System.Linq.OrderedSequence`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m1061657384_gshared (OrderedSequence_2_t2327788107 * __this, Il2CppObject* ___source0, Func_2_t2825504181 * ___key_selector1, Il2CppObject* ___comparer2, int32_t ___direction3, const MethodInfo* method)
{
	Il2CppObject* G_B2_0 = NULL;
	OrderedSequence_2_t2327788107 * G_B2_1 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	OrderedSequence_2_t2327788107 * G_B1_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedEnumerable_1_t4214082274 *)__this);
		((  void (*) (OrderedEnumerable_1_t4214082274 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((OrderedEnumerable_1_t4214082274 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t2825504181 * L_1 = ___key_selector1;
		__this->set_selector_2(L_1);
		Il2CppObject* L_2 = ___comparer2;
		Il2CppObject* L_3 = (Il2CppObject*)L_2;
		G_B1_0 = L_3;
		G_B1_1 = ((OrderedSequence_2_t2327788107 *)(__this));
		if (L_3)
		{
			G_B2_0 = L_3;
			G_B2_1 = ((OrderedSequence_2_t2327788107 *)(__this));
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		Comparer_1_t1579458414 * L_4 = ((  Comparer_1_t1579458414 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		G_B2_0 = ((Il2CppObject*)(L_4));
		G_B2_1 = ((OrderedSequence_2_t2327788107 *)(G_B1_1));
	}

IL_001c:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_comparer_3(G_B2_0);
		int32_t L_5 = ___direction3;
		__this->set_direction_4(L_5);
		return;
	}
}
// System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Object>::CreateContext(System.Linq.SortContext`1<TElement>)
extern "C"  SortContext_1_t1798778454 * OrderedSequence_2_CreateContext_m3921228830_gshared (OrderedSequence_2_t2327788107 * __this, SortContext_1_t1798778454 * ___current0, const MethodInfo* method)
{
	SortContext_1_t1798778454 * V_0 = NULL;
	{
		Func_2_t2825504181 * L_0 = (Func_2_t2825504181 *)__this->get_selector_2();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_comparer_3();
		int32_t L_2 = (int32_t)__this->get_direction_4();
		SortContext_1_t1798778454 * L_3 = ___current0;
		SortSequenceContext_2_t3061295073 * L_4 = (SortSequenceContext_2_t3061295073 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (SortSequenceContext_2_t3061295073 *, Func_2_t2825504181 *, Il2CppObject*, int32_t, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_4, (Func_2_t2825504181 *)L_0, (Il2CppObject*)L_1, (int32_t)L_2, (SortContext_1_t1798778454 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (SortContext_1_t1798778454 *)L_4;
		OrderedEnumerable_1_t4214082274 * L_5 = (OrderedEnumerable_1_t4214082274 *)__this->get_parent_1();
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		OrderedEnumerable_1_t4214082274 * L_6 = (OrderedEnumerable_1_t4214082274 *)__this->get_parent_1();
		SortContext_1_t1798778454 * L_7 = V_0;
		NullCheck((OrderedEnumerable_1_t4214082274 *)L_6);
		SortContext_1_t1798778454 * L_8 = VirtFuncInvoker1< SortContext_1_t1798778454 *, SortContext_1_t1798778454 * >::Invoke(6 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedEnumerable_1_t4214082274 *)L_6, (SortContext_1_t1798778454 *)L_7);
		return L_8;
	}

IL_0031:
	{
		SortContext_1_t1798778454 * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Object>::Sort(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  Il2CppObject* OrderedSequence_2_Sort_m3566252129_gshared (OrderedSequence_2_t2327788107 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedSequence_2_t2327788107 *)__this);
		SortContext_1_t1798778454 * L_1 = VirtFuncInvoker1< SortContext_1_t1798778454 *, SortContext_1_t1798778454 * >::Invoke(6 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Object>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedSequence_2_t2327788107 *)__this, (SortContext_1_t1798778454 *)NULL);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, (SortContext_1_t1798778454 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_2;
	}
}
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::.ctor()
extern "C"  void U3CSortU3Ec__Iterator21__ctor_m435832983_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TElement System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.Generic.IEnumerator<TElement>.get_Current()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m3627878530_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m4063555545_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m2220308456_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CSortU3Ec__Iterator21_t2488605192 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CSortU3Ec__Iterator21_t2488605192 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CSortU3Ec__Iterator21_t2488605192 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.Generic.IEnumerable<TElement>.GetEnumerator()
extern "C"  Il2CppObject* U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m2832696871_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	U3CSortU3Ec__Iterator21_t2488605192 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CSortU3Ec__Iterator21_t2488605192 * L_2 = (U3CSortU3Ec__Iterator21_t2488605192 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CSortU3Ec__Iterator21_t2488605192 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CSortU3Ec__Iterator21_t2488605192 *)L_2;
		U3CSortU3Ec__Iterator21_t2488605192 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CSortU3Ec__Iterator21_t2488605192 * L_5 = V_0;
		SortContext_1_t1798778454 * L_6 = (SortContext_1_t1798778454 *)__this->get_U3CU24U3Econtext_7();
		NullCheck(L_5);
		L_5->set_context_1(L_6);
		U3CSortU3Ec__Iterator21_t2488605192 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::MoveNext()
extern "C"  bool U3CSortU3Ec__Iterator21_MoveNext_m2095803797_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0083;
		}
	}
	{
		goto IL_00b0;
	}

IL_0021:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		SortContext_1_t1798778454 * L_3 = (SortContext_1_t1798778454 *)__this->get_context_1();
		QuickSort_1_t1970792956 * L_4 = (QuickSort_1_t1970792956 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (QuickSort_1_t1970792956 *, Il2CppObject*, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_4, (Il2CppObject*)L_2, (SortContext_1_t1798778454 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		__this->set_U3CsorterU3E__0_2(L_4);
		QuickSort_1_t1970792956 * L_5 = (QuickSort_1_t1970792956 *)__this->get_U3CsorterU3E__0_2();
		NullCheck((QuickSort_1_t1970792956 *)L_5);
		((  void (*) (QuickSort_1_t1970792956 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		__this->set_U3CiU3E__1_3(0);
		goto IL_0091;
	}

IL_004f:
	{
		QuickSort_1_t1970792956 * L_6 = (QuickSort_1_t1970792956 *)__this->get_U3CsorterU3E__0_2();
		NullCheck(L_6);
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)L_6->get_elements_0();
		QuickSort_1_t1970792956 * L_8 = (QuickSort_1_t1970792956 *)__this->get_U3CsorterU3E__0_2();
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)L_8->get_indexes_1();
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__1_3();
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_12);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		__this->set_U24current_5(L_14);
		__this->set_U24PC_4(1);
		goto IL_00b2;
	}

IL_0083:
	{
		int32_t L_15 = (int32_t)__this->get_U3CiU3E__1_3();
		__this->set_U3CiU3E__1_3(((int32_t)((int32_t)L_15+(int32_t)1)));
	}

IL_0091:
	{
		int32_t L_16 = (int32_t)__this->get_U3CiU3E__1_3();
		QuickSort_1_t1970792956 * L_17 = (QuickSort_1_t1970792956 *)__this->get_U3CsorterU3E__0_2();
		NullCheck(L_17);
		Int32U5BU5D_t3030399641* L_18 = (Int32U5BU5D_t3030399641*)L_17->get_indexes_1();
		NullCheck(L_18);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_004f;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_00b0:
	{
		return (bool)0;
	}

IL_00b2:
	{
		return (bool)1;
	}
	// Dead block : IL_00b4: ldloc.1
}
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::Dispose()
extern "C"  void U3CSortU3Ec__Iterator21_Dispose_m2211739520_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSortU3Ec__Iterator21_Reset_m2399833958_MetadataUsageId;
extern "C"  void U3CSortU3Ec__Iterator21_Reset_m2399833958_gshared (U3CSortU3Ec__Iterator21_t2488605192 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSortU3Ec__Iterator21_Reset_m2399833958_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  void QuickSort_1__ctor_m1650062348_gshared (QuickSort_1_t1970792956 * __this, Il2CppObject* ___source0, SortContext_1_t1798778454 * ___context1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___source0;
		ObjectU5BU5D_t3614634134* L_1 = ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_elements_0(L_1);
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_elements_0();
		NullCheck(L_2);
		Int32U5BU5D_t3030399641* L_3 = ((  Int32U5BU5D_t3030399641* (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_indexes_1(L_3);
		SortContext_1_t1798778454 * L_4 = ___context1;
		__this->set_context_2(L_4);
		return;
	}
}
// System.Int32[] System.Linq.QuickSort`1<System.Object>::CreateIndexes(System.Int32)
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const uint32_t QuickSort_1_CreateIndexes_m2577858579_MetadataUsageId;
extern "C"  Int32U5BU5D_t3030399641* QuickSort_1_CreateIndexes_m2577858579_gshared (Il2CppObject * __this /* static, unused */, int32_t ___length0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QuickSort_1_CreateIndexes_m2577858579_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t3030399641* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___length0;
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)L_0));
		V_1 = (int32_t)0;
		goto IL_0016;
	}

IL_000e:
	{
		Int32U5BU5D_t3030399641* L_1 = V_0;
		int32_t L_2 = V_1;
		int32_t L_3 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (int32_t)L_3);
		int32_t L_4 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0016:
	{
		int32_t L_5 = V_1;
		int32_t L_6 = ___length0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_000e;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_7 = V_0;
		return L_7;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::PerformSort()
extern "C"  void QuickSort_1_PerformSort_m3295377581_gshared (QuickSort_1_t1970792956 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_elements_0();
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) > ((int32_t)1)))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}

IL_000f:
	{
		SortContext_1_t1798778454 * L_1 = (SortContext_1_t1798778454 *)__this->get_context_2();
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_elements_0();
		NullCheck((SortContext_1_t1798778454 *)L_1);
		VirtActionInvoker1< ObjectU5BU5D_t3614634134* >::Invoke(4 /* System.Void System.Linq.SortContext`1<System.Object>::Initialize(TElement[]) */, (SortContext_1_t1798778454 *)L_1, (ObjectU5BU5D_t3614634134*)L_2);
		Int32U5BU5D_t3030399641* L_3 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		NullCheck(L_3);
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)0, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Int32 System.Linq.QuickSort`1<System.Object>::CompareItems(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_CompareItems_m2598468721_gshared (QuickSort_1_t1970792956 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method)
{
	{
		SortContext_1_t1798778454 * L_0 = (SortContext_1_t1798778454 *)__this->get_context_2();
		int32_t L_1 = ___first_index0;
		int32_t L_2 = ___second_index1;
		NullCheck((SortContext_1_t1798778454 *)L_0);
		int32_t L_3 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Linq.SortContext`1<System.Object>::Compare(System.Int32,System.Int32) */, (SortContext_1_t1798778454 *)L_0, (int32_t)L_1, (int32_t)L_2);
		return L_3;
	}
}
// System.Int32 System.Linq.QuickSort`1<System.Object>::MedianOfThree(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_MedianOfThree_m968647497_gshared (QuickSort_1_t1970792956 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___left0;
		int32_t L_1 = ___right1;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1))/(int32_t)2));
		Int32U5BU5D_t3030399641* L_2 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		int32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		Int32U5BU5D_t3030399641* L_6 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_7 = ___left0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		int32_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_10 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_5, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_11 = ___left0;
		int32_t L_12 = V_0;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
	}

IL_002a:
	{
		Int32U5BU5D_t3030399641* L_13 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_14 = ___right1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		int32_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		Int32U5BU5D_t3030399641* L_17 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_18 = ___left0;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		int32_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_21 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_16, (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_21) >= ((int32_t)0)))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_22 = ___left0;
		int32_t L_23 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_22, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
	}

IL_004e:
	{
		Int32U5BU5D_t3030399641* L_24 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_25 = ___right1;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		int32_t L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		Int32U5BU5D_t3030399641* L_28 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_29 = V_0;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		int32_t L_30 = L_29;
		int32_t L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_32 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_27, (int32_t)L_31, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_32) >= ((int32_t)0)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_33 = V_0;
		int32_t L_34 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_33, (int32_t)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
	}

IL_0072:
	{
		int32_t L_35 = V_0;
		int32_t L_36 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_35, (int32_t)((int32_t)((int32_t)L_36-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		Int32U5BU5D_t3030399641* L_37 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_38 = ___right1;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, ((int32_t)((int32_t)L_38-(int32_t)1)));
		int32_t L_39 = ((int32_t)((int32_t)L_38-(int32_t)1));
		int32_t L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		return L_40;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::Sort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Sort_m700141710_gshared (QuickSort_1_t1970792956 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___left0;
		int32_t L_1 = ___right1;
		if ((((int32_t)((int32_t)((int32_t)L_0+(int32_t)3))) > ((int32_t)L_1)))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_2 = ___left0;
		V_0 = (int32_t)L_2;
		int32_t L_3 = ___right1;
		V_1 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		int32_t L_4 = ___left0;
		int32_t L_5 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_6 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_2 = (int32_t)L_6;
	}

IL_0018:
	{
		goto IL_001d;
	}

IL_001d:
	{
		Int32U5BU5D_t3030399641* L_7 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		V_0 = (int32_t)L_9;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_9);
		int32_t L_10 = L_9;
		int32_t L_11 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		int32_t L_12 = V_2;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_13 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_13) < ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		goto IL_003b;
	}

IL_003b:
	{
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_15 = V_1;
		int32_t L_16 = (int32_t)((int32_t)((int32_t)L_15-(int32_t)1));
		V_1 = (int32_t)L_16;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_16);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		int32_t L_19 = V_2;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_20 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_18, (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_20) > ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_21 = V_0;
		int32_t L_22 = V_1;
		if ((((int32_t)L_21) >= ((int32_t)L_22)))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_23 = V_0;
		int32_t L_24 = V_1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_23, (int32_t)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		goto IL_006d;
	}

IL_0068:
	{
		goto IL_0072;
	}

IL_006d:
	{
		goto IL_0018;
	}

IL_0072:
	{
		int32_t L_25 = V_0;
		int32_t L_26 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_25, (int32_t)((int32_t)((int32_t)L_26-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		int32_t L_27 = ___left0;
		int32_t L_28 = V_0;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_27, (int32_t)((int32_t)((int32_t)L_28-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_29 = V_0;
		int32_t L_30 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)((int32_t)((int32_t)L_29+(int32_t)1)), (int32_t)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		goto IL_009d;
	}

IL_0095:
	{
		int32_t L_31 = ___left0;
		int32_t L_32 = ___right1;
		NullCheck((QuickSort_1_t1970792956 *)__this);
		((  void (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_31, (int32_t)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
	}

IL_009d:
	{
		return;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::InsertionSort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_InsertionSort_m3575279495_gshared (QuickSort_1_t1970792956 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___left0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)1));
		goto IL_005a;
	}

IL_0009:
	{
		Int32U5BU5D_t3030399641* L_1 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = (int32_t)L_4;
		int32_t L_5 = V_0;
		V_1 = (int32_t)L_5;
		goto IL_002f;
	}

IL_0019:
	{
		Int32U5BU5D_t3030399641* L_6 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_7 = V_1;
		Int32U5BU5D_t3030399641* L_8 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)L_9-(int32_t)1)));
		int32_t L_10 = ((int32_t)((int32_t)L_9-(int32_t)1));
		int32_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (int32_t)L_11);
		int32_t L_12 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_002f:
	{
		int32_t L_13 = V_1;
		int32_t L_14 = ___left0;
		if ((((int32_t)L_13) <= ((int32_t)L_14)))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_15 = V_2;
		Int32U5BU5D_t3030399641* L_16 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_17 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17-(int32_t)1)));
		int32_t L_18 = ((int32_t)((int32_t)L_17-(int32_t)1));
		int32_t L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck((QuickSort_1_t1970792956 *)__this);
		int32_t L_20 = ((  int32_t (*) (QuickSort_1_t1970792956 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((QuickSort_1_t1970792956 *)__this, (int32_t)L_15, (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_20) < ((int32_t)0)))
		{
			goto IL_0019;
		}
	}

IL_004d:
	{
		Int32U5BU5D_t3030399641* L_21 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_22 = V_1;
		int32_t L_23 = V_2;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (int32_t)L_23);
		int32_t L_24 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_005a:
	{
		int32_t L_25 = V_0;
		int32_t L_26 = ___right1;
		if ((((int32_t)L_25) <= ((int32_t)L_26)))
		{
			goto IL_0009;
		}
	}
	{
		return;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::Swap(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Swap_m1740429939_gshared (QuickSort_1_t1970792956 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_1 = ___right1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = (int32_t)L_3;
		Int32U5BU5D_t3030399641* L_4 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_5 = ___right1;
		Int32U5BU5D_t3030399641* L_6 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_7 = ___left0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		int32_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_9);
		Int32U5BU5D_t3030399641* L_10 = (Int32U5BU5D_t3030399641*)__this->get_indexes_1();
		int32_t L_11 = ___left0;
		int32_t L_12 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (int32_t)L_12);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.QuickSort`1<System.Object>::Sort(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  Il2CppObject* QuickSort_1_Sort_m2490553768_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, SortContext_1_t1798778454 * ___context1, const MethodInfo* method)
{
	U3CSortU3Ec__Iterator21_t2488605192 * V_0 = NULL;
	{
		U3CSortU3Ec__Iterator21_t2488605192 * L_0 = (U3CSortU3Ec__Iterator21_t2488605192 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CSortU3Ec__Iterator21_t2488605192 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CSortU3Ec__Iterator21_t2488605192 *)L_0;
		U3CSortU3Ec__Iterator21_t2488605192 * L_1 = V_0;
		Il2CppObject* L_2 = ___source0;
		NullCheck(L_1);
		L_1->set_source_0(L_2);
		U3CSortU3Ec__Iterator21_t2488605192 * L_3 = V_0;
		SortContext_1_t1798778454 * L_4 = ___context1;
		NullCheck(L_3);
		L_3->set_context_1(L_4);
		U3CSortU3Ec__Iterator21_t2488605192 * L_5 = V_0;
		Il2CppObject* L_6 = ___source0;
		NullCheck(L_5);
		L_5->set_U3CU24U3Esource_6(L_6);
		U3CSortU3Ec__Iterator21_t2488605192 * L_7 = V_0;
		SortContext_1_t1798778454 * L_8 = ___context1;
		NullCheck(L_7);
		L_7->set_U3CU24U3Econtext_7(L_8);
		U3CSortU3Ec__Iterator21_t2488605192 * L_9 = V_0;
		U3CSortU3Ec__Iterator21_t2488605192 * L_10 = (U3CSortU3Ec__Iterator21_t2488605192 *)L_9;
		NullCheck(L_10);
		L_10->set_U24PC_4(((int32_t)-2));
		return L_10;
	}
}
// System.Void System.Linq.SortContext`1<System.Object>::.ctor(System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortContext_1__ctor_m1159198615_gshared (SortContext_1_t1798778454 * __this, int32_t ___direction0, SortContext_1_t1798778454 * ___child_context1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___direction0;
		__this->set_direction_0(L_0);
		SortContext_1_t1798778454 * L_1 = ___child_context1;
		__this->set_child_context_1(L_1);
		return;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Int32>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortSequenceContext_2__ctor_m1776296909_gshared (SortSequenceContext_2_t2443723226 * __this, Func_2_t2207932334 * ___selector0, Il2CppObject* ___comparer1, int32_t ___direction2, SortContext_1_t1798778454 * ___child_context3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___direction2;
		SortContext_1_t1798778454 * L_1 = ___child_context3;
		NullCheck((SortContext_1_t1798778454 *)__this);
		((  void (*) (SortContext_1_t1798778454 *, int32_t, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((SortContext_1_t1798778454 *)__this, (int32_t)L_0, (SortContext_1_t1798778454 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t2207932334 * L_2 = ___selector0;
		__this->set_selector_2(L_2);
		Il2CppObject* L_3 = ___comparer1;
		__this->set_comparer_3(L_3);
		return;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Int32>::Initialize(TElement[])
extern "C"  void SortSequenceContext_2_Initialize_m3278535173_gshared (SortSequenceContext_2_t2443723226 * __this, ObjectU5BU5D_t3614634134* ___elements0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SortContext_1_t1798778454 * L_0 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		SortContext_1_t1798778454 * L_1 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		ObjectU5BU5D_t3614634134* L_2 = ___elements0;
		NullCheck((SortContext_1_t1798778454 *)L_1);
		VirtActionInvoker1< ObjectU5BU5D_t3614634134* >::Invoke(4 /* System.Void System.Linq.SortContext`1<System.Object>::Initialize(TElement[]) */, (SortContext_1_t1798778454 *)L_1, (ObjectU5BU5D_t3614634134*)L_2);
	}

IL_0017:
	{
		ObjectU5BU5D_t3614634134* L_3 = ___elements0;
		NullCheck(L_3);
		__this->set_keys_4(((Int32U5BU5D_t3030399641*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))));
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_002c:
	{
		Int32U5BU5D_t3030399641* L_4 = (Int32U5BU5D_t3030399641*)__this->get_keys_4();
		int32_t L_5 = V_0;
		Func_2_t2207932334 * L_6 = (Func_2_t2207932334 *)__this->get_selector_2();
		ObjectU5BU5D_t3614634134* L_7 = ___elements0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((Func_2_t2207932334 *)L_6);
		int32_t L_11 = ((  int32_t (*) (Func_2_t2207932334 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Func_2_t2207932334 *)L_6, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_11);
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_13 = V_0;
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)__this->get_keys_4();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		return;
	}
}
// System.Int32 System.Linq.SortSequenceContext`2<System.Object,System.Int32>::Compare(System.Int32,System.Int32)
extern "C"  int32_t SortSequenceContext_2_Compare_m2394750062_gshared (SortSequenceContext_2_t2443723226 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B10_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_comparer_3();
		Int32U5BU5D_t3030399641* L_1 = (Int32U5BU5D_t3030399641*)__this->get_keys_4();
		int32_t L_2 = ___first_index0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		Int32U5BU5D_t3030399641* L_5 = (Int32U5BU5D_t3030399641*)__this->get_keys_4();
		int32_t L_6 = ___second_index1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		int32_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck((Il2CppObject*)L_0);
		int32_t L_9 = InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Int32>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_4, (int32_t)L_8);
		V_0 = (int32_t)L_9;
		int32_t L_10 = V_0;
		if (L_10)
		{
			goto IL_005b;
		}
	}
	{
		SortContext_1_t1798778454 * L_11 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		SortContext_1_t1798778454 * L_12 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		int32_t L_13 = ___first_index0;
		int32_t L_14 = ___second_index1;
		NullCheck((SortContext_1_t1798778454 *)L_12);
		int32_t L_15 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Linq.SortContext`1<System.Object>::Compare(System.Int32,System.Int32) */, (SortContext_1_t1798778454 *)L_12, (int32_t)L_13, (int32_t)L_14);
		return L_15;
	}

IL_0043:
	{
		int32_t L_16 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_17 = ___second_index1;
		int32_t L_18 = ___first_index0;
		G_B6_0 = ((int32_t)((int32_t)L_17-(int32_t)L_18));
		goto IL_005a;
	}

IL_0057:
	{
		int32_t L_19 = ___first_index0;
		int32_t L_20 = ___second_index1;
		G_B6_0 = ((int32_t)((int32_t)L_19-(int32_t)L_20));
	}

IL_005a:
	{
		V_0 = (int32_t)G_B6_0;
	}

IL_005b:
	{
		int32_t L_21 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_22 = V_0;
		G_B10_0 = ((-L_22));
		goto IL_006f;
	}

IL_006e:
	{
		int32_t L_23 = V_0;
		G_B10_0 = L_23;
	}

IL_006f:
	{
		return G_B10_0;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Object>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortSequenceContext_2__ctor_m2181983522_gshared (SortSequenceContext_2_t3061295073 * __this, Func_2_t2825504181 * ___selector0, Il2CppObject* ___comparer1, int32_t ___direction2, SortContext_1_t1798778454 * ___child_context3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___direction2;
		SortContext_1_t1798778454 * L_1 = ___child_context3;
		NullCheck((SortContext_1_t1798778454 *)__this);
		((  void (*) (SortContext_1_t1798778454 *, int32_t, SortContext_1_t1798778454 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((SortContext_1_t1798778454 *)__this, (int32_t)L_0, (SortContext_1_t1798778454 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Func_2_t2825504181 * L_2 = ___selector0;
		__this->set_selector_2(L_2);
		Il2CppObject* L_3 = ___comparer1;
		__this->set_comparer_3(L_3);
		return;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Object>::Initialize(TElement[])
extern "C"  void SortSequenceContext_2_Initialize_m2437603572_gshared (SortSequenceContext_2_t3061295073 * __this, ObjectU5BU5D_t3614634134* ___elements0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SortContext_1_t1798778454 * L_0 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		SortContext_1_t1798778454 * L_1 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		ObjectU5BU5D_t3614634134* L_2 = ___elements0;
		NullCheck((SortContext_1_t1798778454 *)L_1);
		VirtActionInvoker1< ObjectU5BU5D_t3614634134* >::Invoke(4 /* System.Void System.Linq.SortContext`1<System.Object>::Initialize(TElement[]) */, (SortContext_1_t1798778454 *)L_1, (ObjectU5BU5D_t3614634134*)L_2);
	}

IL_0017:
	{
		ObjectU5BU5D_t3614634134* L_3 = ___elements0;
		NullCheck(L_3);
		__this->set_keys_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))));
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_002c:
	{
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get_keys_4();
		int32_t L_5 = V_0;
		Func_2_t2825504181 * L_6 = (Func_2_t2825504181 *)__this->get_selector_2();
		ObjectU5BU5D_t3614634134* L_7 = ___elements0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((Func_2_t2825504181 *)L_6);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (Func_2_t2825504181 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Func_2_t2825504181 *)L_6, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_11);
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_13 = V_0;
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)__this->get_keys_4();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		return;
	}
}
// System.Int32 System.Linq.SortSequenceContext`2<System.Object,System.Object>::Compare(System.Int32,System.Int32)
extern "C"  int32_t SortSequenceContext_2_Compare_m259092091_gshared (SortSequenceContext_2_t3061295073 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B10_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_comparer_3();
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_keys_4();
		int32_t L_2 = ___first_index0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)__this->get_keys_4();
		int32_t L_6 = ___second_index1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck((Il2CppObject*)L_0);
		int32_t L_9 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject *)L_4, (Il2CppObject *)L_8);
		V_0 = (int32_t)L_9;
		int32_t L_10 = V_0;
		if (L_10)
		{
			goto IL_005b;
		}
	}
	{
		SortContext_1_t1798778454 * L_11 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		SortContext_1_t1798778454 * L_12 = (SortContext_1_t1798778454 *)((SortContext_1_t1798778454 *)__this)->get_child_context_1();
		int32_t L_13 = ___first_index0;
		int32_t L_14 = ___second_index1;
		NullCheck((SortContext_1_t1798778454 *)L_12);
		int32_t L_15 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Linq.SortContext`1<System.Object>::Compare(System.Int32,System.Int32) */, (SortContext_1_t1798778454 *)L_12, (int32_t)L_13, (int32_t)L_14);
		return L_15;
	}

IL_0043:
	{
		int32_t L_16 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_17 = ___second_index1;
		int32_t L_18 = ___first_index0;
		G_B6_0 = ((int32_t)((int32_t)L_17-(int32_t)L_18));
		goto IL_005a;
	}

IL_0057:
	{
		int32_t L_19 = ___first_index0;
		int32_t L_20 = ___second_index1;
		G_B6_0 = ((int32_t)((int32_t)L_19-(int32_t)L_20));
	}

IL_005a:
	{
		V_0 = (int32_t)G_B6_0;
	}

IL_005b:
	{
		int32_t L_21 = (int32_t)((SortContext_1_t1798778454 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_22 = V_0;
		G_B10_0 = ((-L_22));
		goto IL_006f;
	}

IL_006e:
	{
		int32_t L_23 = V_0;
		G_B10_0 = L_23;
	}

IL_006f:
	{
		return G_B10_0;
	}
}
// System.Void System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2068966643_gshared (Nullable_1_t1720961778 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m2068966643_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t1720961778  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m2068966643(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1513687722_gshared (Nullable_1_t1720961778 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1513687722_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1720961778  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1513687722(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m1403232044_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m1403232044_gshared (Nullable_1_t1720961778 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1403232044_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m1403232044_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1720961778  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m1403232044(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3004160134_gshared (Nullable_1_t1720961778 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1720961778 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m4253846971((Nullable_1_t1720961778 *)__this, (Nullable_1_t1720961778 )((*(Nullable_1_t1720961778 *)((Nullable_1_t1720961778 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3004160134_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1720961778  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3004160134(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m4253846971_gshared (Nullable_1_t1720961778 * __this, Nullable_1_t1720961778  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t3507792607 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m4253846971_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1720961778  ___other0, const MethodInfo* method)
{
	Nullable_1_t1720961778  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m4253846971(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2097155330_gshared (Nullable_1_t1720961778 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m2097155330_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1720961778  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m2097155330(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::GetValueOrDefault()
extern Il2CppClass* DefaultValueHandling_t3457895463_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1567032315_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1567032315_gshared (Nullable_1_t1720961778 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1567032315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (DefaultValueHandling_t3457895463_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1567032315_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1720961778  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m1567032315(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3754965196_gshared (Nullable_1_t1720961778 * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int32_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3754965196_AdjustorThunk (Il2CppObject * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t1720961778  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m3754965196(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1559051584_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1559051584_gshared (Nullable_1_t1720961778 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1559051584_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1559051584_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1720961778  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1559051584(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<Newtonsoft.Json.JsonToken>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2814422035_gshared (Nullable_1_t3178688176 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m2814422035_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t3178688176  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m2814422035(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.JsonToken>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m4178320334_gshared (Nullable_1_t3178688176 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m4178320334_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3178688176  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m4178320334(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.JsonToken>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m4103180136_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m4103180136_gshared (Nullable_1_t3178688176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m4103180136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m4103180136_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3178688176  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m4103180136(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.JsonToken>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3258573044_gshared (Nullable_1_t3178688176 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3178688176 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m2626801911((Nullable_1_t3178688176 *)__this, (Nullable_1_t3178688176 )((*(Nullable_1_t3178688176 *)((Nullable_1_t3178688176 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3258573044_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3178688176  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3258573044(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.JsonToken>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2626801911_gshared (Nullable_1_t3178688176 * __this, Nullable_1_t3178688176  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t3507792607 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m2626801911_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3178688176  ___other0, const MethodInfo* method)
{
	Nullable_1_t3178688176  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2626801911(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Newtonsoft.Json.JsonToken>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1108866868_gshared (Nullable_1_t3178688176 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1108866868_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3178688176  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1108866868(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.JsonToken>::GetValueOrDefault()
extern Il2CppClass* JsonToken_t620654565_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m125996873_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m125996873_gshared (Nullable_1_t3178688176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m125996873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (JsonToken_t620654565_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m125996873_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3178688176  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m125996873(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.JsonToken>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1188702985_gshared (Nullable_1_t3178688176 * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int32_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1188702985_AdjustorThunk (Il2CppObject * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t3178688176  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m1188702985(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Newtonsoft.Json.JsonToken>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2329127732_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2329127732_gshared (Nullable_1_t3178688176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2329127732_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2329127732_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3178688176  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2329127732(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>::.ctor(T)
extern "C"  void Nullable_1__ctor_m784282157_gshared (Nullable_1_t3865860824 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m784282157_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t3865860824  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m784282157(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m774323654_gshared (Nullable_1_t3865860824 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m774323654_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3865860824  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m774323654(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m2777946976_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m2777946976_gshared (Nullable_1_t3865860824 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2777946976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m2777946976_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3865860824  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m2777946976(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3671155116_gshared (Nullable_1_t3865860824 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3865860824 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3863949061((Nullable_1_t3865860824 *)__this, (Nullable_1_t3865860824 )((*(Nullable_1_t3865860824 *)((Nullable_1_t3865860824 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3671155116_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3865860824  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3671155116(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3863949061_gshared (Nullable_1_t3865860824 * __this, Nullable_1_t3865860824  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t3507792607 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m3863949061_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3865860824  ___other0, const MethodInfo* method)
{
	Nullable_1_t3865860824  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3863949061(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m936604960_gshared (Nullable_1_t3865860824 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m936604960_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3865860824  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m936604960(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>::GetValueOrDefault()
extern Il2CppClass* JTokenType_t1307827213_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m508673823_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m508673823_gshared (Nullable_1_t3865860824 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m508673823_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (JTokenType_t1307827213_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m508673823_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3865860824  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m508673823(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3179102031_gshared (Nullable_1_t3865860824 * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int32_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3179102031_AdjustorThunk (Il2CppObject * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t3865860824  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m3179102031(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m4234297076_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m4234297076_gshared (Nullable_1_t3865860824 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m4234297076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m4234297076_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3865860824  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m4234297076(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<Newtonsoft.Json.NullValueHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m343635383_gshared (Nullable_1_t1881161680 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m343635383_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t1881161680  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m343635383(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.NullValueHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3063251426_gshared (Nullable_1_t1881161680 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3063251426_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1881161680  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3063251426(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.NullValueHandling>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m2337357804_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m2337357804_gshared (Nullable_1_t1881161680 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2337357804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m2337357804_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1881161680  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m2337357804(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.NullValueHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1630906388_gshared (Nullable_1_t1881161680 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1881161680 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m2735260059((Nullable_1_t1881161680 *)__this, (Nullable_1_t1881161680 )((*(Nullable_1_t1881161680 *)((Nullable_1_t1881161680 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1630906388_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1881161680  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1630906388(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.NullValueHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2735260059_gshared (Nullable_1_t1881161680 * __this, Nullable_1_t1881161680  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t3507792607 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m2735260059_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1881161680  ___other0, const MethodInfo* method)
{
	Nullable_1_t1881161680  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2735260059(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Newtonsoft.Json.NullValueHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1985616208_gshared (Nullable_1_t1881161680 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1985616208_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1881161680  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1985616208(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.NullValueHandling>::GetValueOrDefault()
extern Il2CppClass* NullValueHandling_t3618095365_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3958747527_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3958747527_gshared (Nullable_1_t1881161680 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3958747527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (NullValueHandling_t3618095365_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3958747527_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1881161680  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m3958747527(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.NullValueHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3416342428_gshared (Nullable_1_t1881161680 * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int32_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3416342428_AdjustorThunk (Il2CppObject * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t1881161680  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m3416342428(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Newtonsoft.Json.NullValueHandling>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2216346312_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2216346312_gshared (Nullable_1_t1881161680 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2216346312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2216346312_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1881161680  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2216346312(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2583666311_gshared (Nullable_1_t1983200966 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m2583666311_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t1983200966  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m2583666311(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3403460334_gshared (Nullable_1_t1983200966 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3403460334_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1983200966  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3403460334(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m777214248_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m777214248_gshared (Nullable_1_t1983200966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m777214248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m777214248_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1983200966  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m777214248(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m4110931490_gshared (Nullable_1_t1983200966 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1983200966 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m4288729879((Nullable_1_t1983200966 *)__this, (Nullable_1_t1983200966 )((*(Nullable_1_t1983200966 *)((Nullable_1_t1983200966 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m4110931490_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1983200966  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m4110931490(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m4288729879_gshared (Nullable_1_t1983200966 * __this, Nullable_1_t1983200966  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t3507792607 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m4288729879_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1983200966  ___other0, const MethodInfo* method)
{
	Nullable_1_t1983200966  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m4288729879(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m92500670_gshared (Nullable_1_t1983200966 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m92500670_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1983200966  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m92500670(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::GetValueOrDefault()
extern Il2CppClass* ObjectCreationHandling_t3720134651_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3531376961_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3531376961_gshared (Nullable_1_t1983200966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3531376961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (ObjectCreationHandling_t3720134651_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3531376961_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1983200966  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m3531376961(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1475212144_gshared (Nullable_1_t1983200966 * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int32_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1475212144_AdjustorThunk (Il2CppObject * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t1983200966  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m1475212144(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3840871500_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3840871500_gshared (Nullable_1_t1983200966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3840871500_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3840871500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1983200966  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3840871500(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3532675964_gshared (Nullable_1_t3575889505 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3532675964_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t3575889505  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3532675964(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3996434809_gshared (Nullable_1_t3575889505 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3996434809_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3575889505  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3996434809(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m4010966997_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m4010966997_gshared (Nullable_1_t3575889505 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m4010966997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m4010966997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3575889505  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m4010966997(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1588113867_gshared (Nullable_1_t3575889505 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3575889505 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3013415406((Nullable_1_t3575889505 *)__this, (Nullable_1_t3575889505 )((*(Nullable_1_t3575889505 *)((Nullable_1_t3575889505 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1588113867_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3575889505  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1588113867(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3013415406_gshared (Nullable_1_t3575889505 * __this, Nullable_1_t3575889505  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t3507792607 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m3013415406_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3575889505  ___other0, const MethodInfo* method)
{
	Nullable_1_t3575889505  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3013415406(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1123015825_gshared (Nullable_1_t3575889505 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1123015825_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3575889505  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1123015825(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::GetValueOrDefault()
extern Il2CppClass* ReferenceLoopHandling_t1017855894_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m175247762_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m175247762_gshared (Nullable_1_t3575889505 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m175247762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (ReferenceLoopHandling_t1017855894_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m175247762_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3575889505  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m175247762(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3116500313_gshared (Nullable_1_t3575889505 * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int32_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3116500313_AdjustorThunk (Il2CppObject * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t3575889505  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m3116500313(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3449987673_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3449987673_gshared (Nullable_1_t3575889505 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3449987673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3449987673_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3575889505  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3449987673(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(T)
extern "C"  void Nullable_1__ctor_m681591852_gshared (Nullable_1_t5811492 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m681591852_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t5811492  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m681591852(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1003835587_gshared (Nullable_1_t5811492 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1003835587_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t5811492  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1003835587(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m2736388043_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m2736388043_gshared (Nullable_1_t5811492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2736388043_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m2736388043_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t5811492  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m2736388043(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1948246969_gshared (Nullable_1_t5811492 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t5811492 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m2735515702((Nullable_1_t5811492 *)__this, (Nullable_1_t5811492 )((*(Nullable_1_t5811492 *)((Nullable_1_t5811492 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1948246969_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t5811492  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1948246969(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2735515702_gshared (Nullable_1_t5811492 * __this, Nullable_1_t5811492  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t3507792607 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m2735515702_AdjustorThunk (Il2CppObject * __this, Nullable_1_t5811492  ___other0, const MethodInfo* method)
{
	Nullable_1_t5811492  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2735515702(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2700679387_gshared (Nullable_1_t5811492 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m2700679387_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t5811492  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m2700679387(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::GetValueOrDefault()
extern Il2CppClass* JsonSchemaType_t1742745177_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2672151126_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2672151126_gshared (Nullable_1_t5811492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2672151126_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (JsonSchemaType_t1742745177_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2672151126_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t5811492  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m2672151126(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1197931512_gshared (Nullable_1_t5811492 * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int32_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1197931512_AdjustorThunk (Il2CppObject * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t5811492  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m1197931512(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2930416727_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2930416727_gshared (Nullable_1_t5811492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2930416727_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2930416727_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t5811492  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2930416727(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3545537844_gshared (Nullable_1_t3889546705 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3545537844_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t3889546705  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3545537844(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2323669597_gshared (Nullable_1_t3889546705 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2323669597_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3889546705  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2323669597(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m4246645389_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m4246645389_gshared (Nullable_1_t3889546705 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m4246645389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m4246645389_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3889546705  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m4246645389(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m951361147_gshared (Nullable_1_t3889546705 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3889546705 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3699731620((Nullable_1_t3889546705 *)__this, (Nullable_1_t3889546705 )((*(Nullable_1_t3889546705 *)((Nullable_1_t3889546705 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m951361147_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3889546705  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m951361147(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3699731620_gshared (Nullable_1_t3889546705 * __this, Nullable_1_t3889546705  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t3507792607 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m3699731620_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3889546705  ___other0, const MethodInfo* method)
{
	Nullable_1_t3889546705  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3699731620(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3066184921_gshared (Nullable_1_t3889546705 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m3066184921_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3889546705  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m3066184921(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::GetValueOrDefault()
extern Il2CppClass* TypeNameHandling_t1331513094_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3408539626_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3408539626_gshared (Nullable_1_t3889546705 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3408539626_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (TypeNameHandling_t1331513094_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3408539626_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3889546705  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m3408539626(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2431479780_gshared (Nullable_1_t3889546705 * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int32_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2431479780_AdjustorThunk (Il2CppObject * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t3889546705  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m2431479780(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3702235529_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3702235529_gshared (Nullable_1_t3889546705 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3702235529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t3507792607 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3702235529_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3889546705  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3702235529(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Boolean>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3575886808_gshared (Nullable_1_t2088641033 * __this, bool ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		bool L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3575886808_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3575886808(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1733730025_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1733730025_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1733730025(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Boolean>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m2519643161_MetadataUsageId;
extern "C"  bool Nullable_1_get_Value_m2519643161_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2519643161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		bool L_2 = (bool)__this->get_value_0();
		return L_2;
	}
}
extern "C"  bool Nullable_1_get_Value_m2519643161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_Value_m2519643161(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1318699267_gshared (Nullable_1_t2088641033 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2088641033 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m2189684888((Nullable_1_t2088641033 *)__this, (Nullable_1_t2088641033 )((*(Nullable_1_t2088641033 *)((Nullable_1_t2088641033 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1318699267_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1318699267(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2189684888_gshared (Nullable_1_t2088641033 * __this, Nullable_1_t2088641033  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		bool* L_3 = (bool*)(&___other0)->get_address_of_value_0();
		bool L_4 = (bool)__this->get_value_0();
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Boolean_Equals_m2118901528((bool*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2189684888_AdjustorThunk (Il2CppObject * __this, Nullable_1_t2088641033  ___other0, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2189684888(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Boolean>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1645245653_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		bool* L_1 = (bool*)__this->get_address_of_value_0();
		int32_t L_2 = Boolean_GetHashCode_m1894638460((bool*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1645245653_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1645245653(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Boolean>::GetValueOrDefault()
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2607283502_MetadataUsageId;
extern "C"  bool Nullable_1_GetValueOrDefault_m2607283502_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2607283502_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool G_B3_0 = false;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Boolean_t3825574718_il2cpp_TypeInfo_var, (&V_0));
		bool L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  bool Nullable_1_GetValueOrDefault_m2607283502_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_GetValueOrDefault_m2607283502(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Boolean>::GetValueOrDefault(T)
extern "C"  bool Nullable_1_GetValueOrDefault_m721352268_gshared (Nullable_1_t2088641033 * __this, bool ___defaultValue0, const MethodInfo* method)
{
	bool G_B3_0 = false;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		bool L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  bool Nullable_1_GetValueOrDefault_m721352268_AdjustorThunk (Il2CppObject * __this, bool ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_GetValueOrDefault_m721352268(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Boolean>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m678068069_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m678068069_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m678068069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		bool* L_1 = (bool*)__this->get_address_of_value_0();
		String_t* L_2 = Boolean_ToString_m1253164328((bool*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m678068069_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m678068069(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Byte>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1976450384_gshared (Nullable_1_t1946170751 * __this, uint8_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		uint8_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1976450384_AdjustorThunk (Il2CppObject * __this, uint8_t ___value0, const MethodInfo* method)
{
	Nullable_1_t1946170751  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1976450384(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<uint8_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Byte>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1064511687_gshared (Nullable_1_t1946170751 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1064511687_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1946170751  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1064511687(&_thisAdjusted, method);
	*reinterpret_cast<uint8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Byte>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m1339761255_MetadataUsageId;
extern "C"  uint8_t Nullable_1_get_Value_m1339761255_gshared (Nullable_1_t1946170751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1339761255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		uint8_t L_2 = (uint8_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  uint8_t Nullable_1_get_Value_m1339761255_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1946170751  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint8_t _returnValue = Nullable_1_get_Value_m1339761255(&_thisAdjusted, method);
	*reinterpret_cast<uint8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Byte>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m4129458261_gshared (Nullable_1_t1946170751 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1946170751 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m2306880636((Nullable_1_t1946170751 *)__this, (Nullable_1_t1946170751 )((*(Nullable_1_t1946170751 *)((Nullable_1_t1946170751 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m4129458261_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1946170751  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m4129458261(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Byte>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2306880636_gshared (Nullable_1_t1946170751 * __this, Nullable_1_t1946170751  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		uint8_t* L_3 = (uint8_t*)(&___other0)->get_address_of_value_0();
		uint8_t L_4 = (uint8_t)__this->get_value_0();
		uint8_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Byte_Equals_m3803173058((uint8_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2306880636_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1946170751  ___other0, const MethodInfo* method)
{
	Nullable_1_t1946170751  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2306880636(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Byte>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3007125615_gshared (Nullable_1_t1946170751 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		uint8_t* L_1 = (uint8_t*)__this->get_address_of_value_0();
		int32_t L_2 = Byte_GetHashCode_m2885528842((uint8_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m3007125615_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1946170751  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m3007125615(&_thisAdjusted, method);
	*reinterpret_cast<uint8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Byte>::GetValueOrDefault()
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1118026340_MetadataUsageId;
extern "C"  uint8_t Nullable_1_GetValueOrDefault_m1118026340_gshared (Nullable_1_t1946170751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1118026340_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0x0;
	uint8_t G_B3_0 = 0x0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint8_t L_1 = (uint8_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Byte_t3683104436_il2cpp_TypeInfo_var, (&V_0));
		uint8_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  uint8_t Nullable_1_GetValueOrDefault_m1118026340_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1946170751  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint8_t _returnValue = Nullable_1_GetValueOrDefault_m1118026340(&_thisAdjusted, method);
	*reinterpret_cast<uint8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Byte>::GetValueOrDefault(T)
extern "C"  uint8_t Nullable_1_GetValueOrDefault_m4257560350_gshared (Nullable_1_t1946170751 * __this, uint8_t ___defaultValue0, const MethodInfo* method)
{
	uint8_t G_B3_0 = 0x0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint8_t L_1 = (uint8_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		uint8_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  uint8_t Nullable_1_GetValueOrDefault_m4257560350_AdjustorThunk (Il2CppObject * __this, uint8_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t1946170751  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint8_t _returnValue = Nullable_1_GetValueOrDefault_m4257560350(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<uint8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Byte>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1226445411_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1226445411_gshared (Nullable_1_t1946170751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1226445411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		uint8_t* L_1 = (uint8_t*)__this->get_address_of_value_0();
		String_t* L_2 = Byte_ToString_m1526839480((uint8_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1226445411_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1946170751  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1226445411(&_thisAdjusted, method);
	*reinterpret_cast<uint8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Char>::.ctor(T)
extern "C"  void Nullable_1__ctor_m45911704_gshared (Nullable_1_t1717547653 * __this, Il2CppChar ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Il2CppChar L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m45911704_AdjustorThunk (Il2CppObject * __this, Il2CppChar ___value0, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m45911704(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Char>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2585673045_gshared (Nullable_1_t1717547653 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2585673045_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2585673045(&_thisAdjusted, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Char>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m3846225905_MetadataUsageId;
extern "C"  Il2CppChar Nullable_1_get_Value_m3846225905_gshared (Nullable_1_t1717547653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3846225905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppChar L_2 = (Il2CppChar)__this->get_value_0();
		return L_2;
	}
}
extern "C"  Il2CppChar Nullable_1_get_Value_m3846225905_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Il2CppChar _returnValue = Nullable_1_get_Value_m3846225905(&_thisAdjusted, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Char>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m848199703_gshared (Nullable_1_t1717547653 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1717547653 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1944572282((Nullable_1_t1717547653 *)__this, (Nullable_1_t1717547653 )((*(Nullable_1_t1717547653 *)((Nullable_1_t1717547653 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m848199703_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m848199703(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Char>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1944572282_gshared (Nullable_1_t1717547653 * __this, Nullable_1_t1717547653  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Il2CppChar* L_3 = (Il2CppChar*)(&___other0)->get_address_of_value_0();
		Il2CppChar L_4 = (Il2CppChar)__this->get_value_0();
		Il2CppChar L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Char_Equals_m1374454116((Il2CppChar*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1944572282_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1717547653  ___other0, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1944572282(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Char>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m4003870389_gshared (Nullable_1_t1717547653 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Il2CppChar* L_1 = (Il2CppChar*)__this->get_address_of_value_0();
		int32_t L_2 = Char_GetHashCode_m2343577184((Il2CppChar*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m4003870389_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m4003870389(&_thisAdjusted, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Char>::GetValueOrDefault()
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1517005582_MetadataUsageId;
extern "C"  Il2CppChar Nullable_1_GetValueOrDefault_m1517005582_gshared (Nullable_1_t1717547653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1517005582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppChar V_0 = 0x0;
	Il2CppChar G_B3_0 = 0x0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppChar L_1 = (Il2CppChar)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Char_t3454481338_il2cpp_TypeInfo_var, (&V_0));
		Il2CppChar L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  Il2CppChar Nullable_1_GetValueOrDefault_m1517005582_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Il2CppChar _returnValue = Nullable_1_GetValueOrDefault_m1517005582(&_thisAdjusted, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Char>::GetValueOrDefault(T)
extern "C"  Il2CppChar Nullable_1_GetValueOrDefault_m880623264_gshared (Nullable_1_t1717547653 * __this, Il2CppChar ___defaultValue0, const MethodInfo* method)
{
	Il2CppChar G_B3_0 = 0x0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppChar L_1 = (Il2CppChar)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		Il2CppChar L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  Il2CppChar Nullable_1_GetValueOrDefault_m880623264_AdjustorThunk (Il2CppObject * __this, Il2CppChar ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Il2CppChar _returnValue = Nullable_1_GetValueOrDefault_m880623264(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Char>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2704273093_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2704273093_gshared (Nullable_1_t1717547653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2704273093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppChar* L_1 = (Il2CppChar*)__this->get_address_of_value_0();
		String_t* L_2 = Char_ToString_m1976753030((Il2CppChar*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2704273093_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1717547653  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Il2CppChar*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2704273093(&_thisAdjusted, method);
	*reinterpret_cast<Il2CppChar*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.DateTime>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3532149783_gshared (Nullable_1_t3251239280 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		DateTime_t693205669  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3532149783_AdjustorThunk (Il2CppObject * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3532149783(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.DateTime>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3795771450_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3795771450_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3795771450(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTime>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m4067120636_MetadataUsageId;
extern "C"  DateTime_t693205669  Nullable_1_get_Value_m4067120636_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m4067120636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		DateTime_t693205669  L_2 = (DateTime_t693205669 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  DateTime_t693205669  Nullable_1_get_Value_m4067120636_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTime_t693205669  _returnValue = Nullable_1_get_Value_m4067120636(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1089953100_gshared (Nullable_1_t3251239280 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3251239280 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1817623273((Nullable_1_t3251239280 *)__this, (Nullable_1_t3251239280 )((*(Nullable_1_t3251239280 *)((Nullable_1_t3251239280 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1089953100_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1089953100(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1817623273_gshared (Nullable_1_t3251239280 * __this, Nullable_1_t3251239280  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		DateTime_t693205669 * L_3 = (DateTime_t693205669 *)(&___other0)->get_address_of_value_0();
		DateTime_t693205669  L_4 = (DateTime_t693205669 )__this->get_value_0();
		DateTime_t693205669  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = DateTime_Equals_m2562884703((DateTime_t693205669 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1817623273_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3251239280  ___other0, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1817623273(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.DateTime>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3047479588_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		DateTime_t693205669 * L_1 = (DateTime_t693205669 *)__this->get_address_of_value_0();
		int32_t L_2 = DateTime_GetHashCode_m974799321((DateTime_t693205669 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m3047479588_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m3047479588(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTime>::GetValueOrDefault()
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3824690815_MetadataUsageId;
extern "C"  DateTime_t693205669  Nullable_1_GetValueOrDefault_m3824690815_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3824690815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	DateTime_t693205669  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		DateTime_t693205669  L_1 = (DateTime_t693205669 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (DateTime_t693205669_il2cpp_TypeInfo_var, (&V_0));
		DateTime_t693205669  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  DateTime_t693205669  Nullable_1_GetValueOrDefault_m3824690815_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTime_t693205669  _returnValue = Nullable_1_GetValueOrDefault_m3824690815(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTime>::GetValueOrDefault(T)
extern "C"  DateTime_t693205669  Nullable_1_GetValueOrDefault_m3536317119_gshared (Nullable_1_t3251239280 * __this, DateTime_t693205669  ___defaultValue0, const MethodInfo* method)
{
	DateTime_t693205669  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		DateTime_t693205669  L_1 = (DateTime_t693205669 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		DateTime_t693205669  L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  DateTime_t693205669  Nullable_1_GetValueOrDefault_m3536317119_AdjustorThunk (Il2CppObject * __this, DateTime_t693205669  ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTime_t693205669  _returnValue = Nullable_1_GetValueOrDefault_m3536317119(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.DateTime>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1419821888_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1419821888_gshared (Nullable_1_t3251239280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1419821888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		DateTime_t693205669 * L_1 = (DateTime_t693205669 *)__this->get_address_of_value_0();
		String_t* L_2 = DateTime_ToString_m1117481977((DateTime_t693205669 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1419821888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3251239280  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTime_t693205669 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1419821888(&_thisAdjusted, method);
	*reinterpret_cast<DateTime_t693205669 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.DateTimeOffset>::.ctor(T)
extern "C"  void Nullable_1__ctor_m898599766_gshared (Nullable_1_t3921022517 * __this, DateTimeOffset_t1362988906  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		DateTimeOffset_t1362988906  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m898599766_AdjustorThunk (Il2CppObject * __this, DateTimeOffset_t1362988906  ___value0, const MethodInfo* method)
{
	Nullable_1_t3921022517  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m898599766(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.DateTimeOffset>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3112582513_gshared (Nullable_1_t3921022517 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3112582513_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3921022517  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3112582513(&_thisAdjusted, method);
	*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTimeOffset>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m4014532765_MetadataUsageId;
extern "C"  DateTimeOffset_t1362988906  Nullable_1_get_Value_m4014532765_gshared (Nullable_1_t3921022517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m4014532765_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		DateTimeOffset_t1362988906  L_2 = (DateTimeOffset_t1362988906 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  DateTimeOffset_t1362988906  Nullable_1_get_Value_m4014532765_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3921022517  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTimeOffset_t1362988906  _returnValue = Nullable_1_get_Value_m4014532765(&_thisAdjusted, method);
	*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.DateTimeOffset>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1869064775_gshared (Nullable_1_t3921022517 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3921022517 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1307612228((Nullable_1_t3921022517 *)__this, (Nullable_1_t3921022517 )((*(Nullable_1_t3921022517 *)((Nullable_1_t3921022517 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1869064775_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3921022517  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1869064775(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.DateTimeOffset>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1307612228_gshared (Nullable_1_t3921022517 * __this, Nullable_1_t3921022517  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		DateTimeOffset_t1362988906 * L_3 = (DateTimeOffset_t1362988906 *)(&___other0)->get_address_of_value_0();
		DateTimeOffset_t1362988906  L_4 = (DateTimeOffset_t1362988906 )__this->get_value_0();
		DateTimeOffset_t1362988906  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = DateTimeOffset_Equals_m3467127074((DateTimeOffset_t1362988906 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1307612228_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3921022517  ___other0, const MethodInfo* method)
{
	Nullable_1_t3921022517  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1307612228(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.DateTimeOffset>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3366477305_gshared (Nullable_1_t3921022517 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		DateTimeOffset_t1362988906 * L_1 = (DateTimeOffset_t1362988906 *)__this->get_address_of_value_0();
		int32_t L_2 = DateTimeOffset_GetHashCode_m3312197462((DateTimeOffset_t1362988906 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m3366477305_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3921022517  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m3366477305(&_thisAdjusted, method);
	*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTimeOffset>::GetValueOrDefault()
extern Il2CppClass* DateTimeOffset_t1362988906_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2243157920_MetadataUsageId;
extern "C"  DateTimeOffset_t1362988906  Nullable_1_GetValueOrDefault_m2243157920_gshared (Nullable_1_t3921022517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2243157920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTimeOffset_t1362988906  V_0;
	memset(&V_0, 0, sizeof(V_0));
	DateTimeOffset_t1362988906  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		DateTimeOffset_t1362988906  L_1 = (DateTimeOffset_t1362988906 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (DateTimeOffset_t1362988906_il2cpp_TypeInfo_var, (&V_0));
		DateTimeOffset_t1362988906  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  DateTimeOffset_t1362988906  Nullable_1_GetValueOrDefault_m2243157920_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3921022517  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTimeOffset_t1362988906  _returnValue = Nullable_1_GetValueOrDefault_m2243157920(&_thisAdjusted, method);
	*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.DateTimeOffset>::GetValueOrDefault(T)
extern "C"  DateTimeOffset_t1362988906  Nullable_1_GetValueOrDefault_m3662385574_gshared (Nullable_1_t3921022517 * __this, DateTimeOffset_t1362988906  ___defaultValue0, const MethodInfo* method)
{
	DateTimeOffset_t1362988906  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		DateTimeOffset_t1362988906  L_1 = (DateTimeOffset_t1362988906 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		DateTimeOffset_t1362988906  L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  DateTimeOffset_t1362988906  Nullable_1_GetValueOrDefault_m3662385574_AdjustorThunk (Il2CppObject * __this, DateTimeOffset_t1362988906  ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t3921022517  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	DateTimeOffset_t1362988906  _returnValue = Nullable_1_GetValueOrDefault_m3662385574(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.DateTimeOffset>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2408878353_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2408878353_gshared (Nullable_1_t3921022517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2408878353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		DateTimeOffset_t1362988906 * L_1 = (DateTimeOffset_t1362988906 *)__this->get_address_of_value_0();
		String_t* L_2 = DateTimeOffset_ToString_m2511385248((DateTimeOffset_t1362988906 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2408878353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3921022517  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2408878353(&_thisAdjusted, method);
	*reinterpret_cast<DateTimeOffset_t1362988906 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Decimal>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3148227567_gshared (Nullable_1_t3282734688 * __this, Decimal_t724701077  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Decimal_t724701077  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3148227567_AdjustorThunk (Il2CppObject * __this, Decimal_t724701077  ___value0, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3148227567(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Decimal>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m4188106120_gshared (Nullable_1_t3282734688 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m4188106120_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m4188106120(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Decimal>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m3829861286_MetadataUsageId;
extern "C"  Decimal_t724701077  Nullable_1_get_Value_m3829861286_gshared (Nullable_1_t3282734688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3829861286_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Decimal_t724701077  L_2 = (Decimal_t724701077 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  Decimal_t724701077  Nullable_1_get_Value_m3829861286_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Decimal_t724701077  _returnValue = Nullable_1_get_Value_m3829861286(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Decimal>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2494697376_gshared (Nullable_1_t3282734688 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3282734688 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3951113015((Nullable_1_t3282734688 *)__this, (Nullable_1_t3282734688 )((*(Nullable_1_t3282734688 *)((Nullable_1_t3282734688 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m2494697376_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2494697376(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Decimal>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3951113015_gshared (Nullable_1_t3282734688 * __this, Nullable_1_t3282734688  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Decimal_t724701077 * L_3 = (Decimal_t724701077 *)(&___other0)->get_address_of_value_0();
		Decimal_t724701077  L_4 = (Decimal_t724701077 )__this->get_value_0();
		Decimal_t724701077  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Decimal_Equals_m1798565377((Decimal_t724701077 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3951113015_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3282734688  ___other0, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3951113015(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Decimal>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m387122928_gshared (Nullable_1_t3282734688 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Decimal_t724701077 * L_1 = (Decimal_t724701077 *)__this->get_address_of_value_0();
		int32_t L_2 = Decimal_GetHashCode_m703641627((Decimal_t724701077 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m387122928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m387122928(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Decimal>::GetValueOrDefault()
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1335859897_MetadataUsageId;
extern "C"  Decimal_t724701077  Nullable_1_GetValueOrDefault_m1335859897_gshared (Nullable_1_t3282734688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1335859897_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Decimal_t724701077  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Decimal_t724701077  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Decimal_t724701077  L_1 = (Decimal_t724701077 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Decimal_t724701077_il2cpp_TypeInfo_var, (&V_0));
		Decimal_t724701077  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  Decimal_t724701077  Nullable_1_GetValueOrDefault_m1335859897_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Decimal_t724701077  _returnValue = Nullable_1_GetValueOrDefault_m1335859897(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Decimal>::GetValueOrDefault(T)
extern "C"  Decimal_t724701077  Nullable_1_GetValueOrDefault_m540580473_gshared (Nullable_1_t3282734688 * __this, Decimal_t724701077  ___defaultValue0, const MethodInfo* method)
{
	Decimal_t724701077  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Decimal_t724701077  L_1 = (Decimal_t724701077 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		Decimal_t724701077  L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  Decimal_t724701077  Nullable_1_GetValueOrDefault_m540580473_AdjustorThunk (Il2CppObject * __this, Decimal_t724701077  ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Decimal_t724701077  _returnValue = Nullable_1_GetValueOrDefault_m540580473(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Decimal>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m810070050_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m810070050_gshared (Nullable_1_t3282734688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m810070050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Decimal_t724701077 * L_1 = (Decimal_t724701077 *)__this->get_address_of_value_0();
		String_t* L_2 = Decimal_ToString_m759431975((Decimal_t724701077 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m810070050_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3282734688  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Decimal_t724701077 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m810070050(&_thisAdjusted, method);
	*reinterpret_cast<Decimal_t724701077 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Double>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3063707597_gshared (Nullable_1_t2341081996 * __this, double ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		double L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3063707597_AdjustorThunk (Il2CppObject * __this, double ___value0, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3063707597(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Double>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2218224334_gshared (Nullable_1_t2341081996 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2218224334_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2218224334(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Double>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m2026332880_MetadataUsageId;
extern "C"  double Nullable_1_get_Value_m2026332880_gshared (Nullable_1_t2341081996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2026332880_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		double L_2 = (double)__this->get_value_0();
		return L_2;
	}
}
extern "C"  double Nullable_1_get_Value_m2026332880_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	double _returnValue = Nullable_1_get_Value_m2026332880(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Double>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m646915120_gshared (Nullable_1_t2341081996 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2341081996 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3211299085((Nullable_1_t2341081996 *)__this, (Nullable_1_t2341081996 )((*(Nullable_1_t2341081996 *)((Nullable_1_t2341081996 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m646915120_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m646915120(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Double>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3211299085_gshared (Nullable_1_t2341081996 * __this, Nullable_1_t2341081996  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		double* L_3 = (double*)(&___other0)->get_address_of_value_0();
		double L_4 = (double)__this->get_value_0();
		double L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Double_Equals_m2792387643((double*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3211299085_AdjustorThunk (Il2CppObject * __this, Nullable_1_t2341081996  ___other0, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3211299085(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Double>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m810535872_gshared (Nullable_1_t2341081996 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		double* L_1 = (double*)__this->get_address_of_value_0();
		int32_t L_2 = Double_GetHashCode_m3403732029((double*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m810535872_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m810535872(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Double>::GetValueOrDefault()
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2895307009_MetadataUsageId;
extern "C"  double Nullable_1_GetValueOrDefault_m2895307009_gshared (Nullable_1_t2341081996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2895307009_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	double G_B3_0 = 0.0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		double L_1 = (double)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Double_t4078015681_il2cpp_TypeInfo_var, (&V_0));
		double L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  double Nullable_1_GetValueOrDefault_m2895307009_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	double _returnValue = Nullable_1_GetValueOrDefault_m2895307009(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Double>::GetValueOrDefault(T)
extern "C"  double Nullable_1_GetValueOrDefault_m2686689907_gshared (Nullable_1_t2341081996 * __this, double ___defaultValue0, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		double L_1 = (double)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		double L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  double Nullable_1_GetValueOrDefault_m2686689907_AdjustorThunk (Il2CppObject * __this, double ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	double _returnValue = Nullable_1_GetValueOrDefault_m2686689907(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Double>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3380318548_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3380318548_gshared (Nullable_1_t2341081996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3380318548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		double* L_1 = (double*)__this->get_address_of_value_0();
		String_t* L_2 = Double_ToString_m1864290157((double*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3380318548_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2341081996  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<double*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3380318548(&_thisAdjusted, method);
	*reinterpret_cast<double*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Guid>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1112653897_gshared (Nullable_1_t796667908 * __this, Guid_t2533601593  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Guid_t2533601593  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1112653897_AdjustorThunk (Il2CppObject * __this, Guid_t2533601593  ___value0, const MethodInfo* method)
{
	Nullable_1_t796667908  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2533601593 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1112653897(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<Guid_t2533601593 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Guid>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1677936334_gshared (Nullable_1_t796667908 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1677936334_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t796667908  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2533601593 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1677936334(&_thisAdjusted, method);
	*reinterpret_cast<Guid_t2533601593 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Guid>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m148240832_MetadataUsageId;
extern "C"  Guid_t2533601593  Nullable_1_get_Value_m148240832_gshared (Nullable_1_t796667908 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m148240832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Guid_t2533601593  L_2 = (Guid_t2533601593 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  Guid_t2533601593  Nullable_1_get_Value_m148240832_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t796667908  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2533601593 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Guid_t2533601593  _returnValue = Nullable_1_get_Value_m148240832(&_thisAdjusted, method);
	*reinterpret_cast<Guid_t2533601593 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Guid>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3135547224_gshared (Nullable_1_t796667908 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t796667908 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m2049755679((Nullable_1_t796667908 *)__this, (Nullable_1_t796667908 )((*(Nullable_1_t796667908 *)((Nullable_1_t796667908 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3135547224_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t796667908  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2533601593 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3135547224(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Guid_t2533601593 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Guid>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2049755679_gshared (Nullable_1_t796667908 * __this, Nullable_1_t796667908  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Guid_t2533601593 * L_3 = (Guid_t2533601593 *)(&___other0)->get_address_of_value_0();
		Guid_t2533601593  L_4 = (Guid_t2533601593 )__this->get_value_0();
		Guid_t2533601593  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Guid_Equals_m2346664749((Guid_t2533601593 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2049755679_AdjustorThunk (Il2CppObject * __this, Nullable_1_t796667908  ___other0, const MethodInfo* method)
{
	Nullable_1_t796667908  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2533601593 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2049755679(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Guid_t2533601593 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Guid>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1323264804_gshared (Nullable_1_t796667908 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Guid_t2533601593 * L_1 = (Guid_t2533601593 *)__this->get_address_of_value_0();
		int32_t L_2 = Guid_GetHashCode_m1401300871((Guid_t2533601593 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1323264804_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t796667908  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2533601593 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1323264804(&_thisAdjusted, method);
	*reinterpret_cast<Guid_t2533601593 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Guid>::GetValueOrDefault()
extern Il2CppClass* Guid_t2533601593_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3936076765_MetadataUsageId;
extern "C"  Guid_t2533601593  Nullable_1_GetValueOrDefault_m3936076765_gshared (Nullable_1_t796667908 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3936076765_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Guid_t2533601593  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Guid_t2533601593  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Guid_t2533601593  L_1 = (Guid_t2533601593 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Guid_t2533601593_il2cpp_TypeInfo_var, (&V_0));
		Guid_t2533601593  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  Guid_t2533601593  Nullable_1_GetValueOrDefault_m3936076765_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t796667908  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2533601593 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Guid_t2533601593  _returnValue = Nullable_1_GetValueOrDefault_m3936076765(&_thisAdjusted, method);
	*reinterpret_cast<Guid_t2533601593 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Guid>::GetValueOrDefault(T)
extern "C"  Guid_t2533601593  Nullable_1_GetValueOrDefault_m2798365637_gshared (Nullable_1_t796667908 * __this, Guid_t2533601593  ___defaultValue0, const MethodInfo* method)
{
	Guid_t2533601593  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Guid_t2533601593  L_1 = (Guid_t2533601593 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		Guid_t2533601593  L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  Guid_t2533601593  Nullable_1_GetValueOrDefault_m2798365637_AdjustorThunk (Il2CppObject * __this, Guid_t2533601593  ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t796667908  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2533601593 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Guid_t2533601593  _returnValue = Nullable_1_GetValueOrDefault_m2798365637(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<Guid_t2533601593 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Guid>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1509164196_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1509164196_gshared (Nullable_1_t796667908 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1509164196_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Guid_t2533601593 * L_1 = (Guid_t2533601593 *)__this->get_address_of_value_0();
		String_t* L_2 = Guid_ToString_m3927110175((Guid_t2533601593 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1509164196_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t796667908  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Guid_t2533601593 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1509164196(&_thisAdjusted, method);
	*reinterpret_cast<Guid_t2533601593 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Int16>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1382577830_gshared (Nullable_1_t2304312229 * __this, int16_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int16_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1382577830_AdjustorThunk (Il2CppObject * __this, int16_t ___value0, const MethodInfo* method)
{
	Nullable_1_t2304312229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1382577830(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Int16>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3975669485_gshared (Nullable_1_t2304312229 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3975669485_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2304312229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3975669485(&_thisAdjusted, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int16>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m2163578429_MetadataUsageId;
extern "C"  int16_t Nullable_1_get_Value_m2163578429_gshared (Nullable_1_t2304312229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2163578429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int16_t L_2 = (int16_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int16_t Nullable_1_get_Value_m2163578429_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2304312229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int16_t _returnValue = Nullable_1_get_Value_m2163578429(&_thisAdjusted, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int16>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m818129807_gshared (Nullable_1_t2304312229 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2304312229 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1178649862((Nullable_1_t2304312229 *)__this, (Nullable_1_t2304312229 )((*(Nullable_1_t2304312229 *)((Nullable_1_t2304312229 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m818129807_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t2304312229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m818129807(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int16>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1178649862_gshared (Nullable_1_t2304312229 * __this, Nullable_1_t2304312229  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int16_t* L_3 = (int16_t*)(&___other0)->get_address_of_value_0();
		int16_t L_4 = (int16_t)__this->get_value_0();
		int16_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Int16_Equals_m3925135434((int16_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1178649862_AdjustorThunk (Il2CppObject * __this, Nullable_1_t2304312229  ___other0, const MethodInfo* method)
{
	Nullable_1_t2304312229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1178649862(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Int16>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m731873249_gshared (Nullable_1_t2304312229 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int16_t* L_1 = (int16_t*)__this->get_address_of_value_0();
		int32_t L_2 = Int16_GetHashCode_m304503362((int16_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m731873249_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2304312229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m731873249(&_thisAdjusted, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int16>::GetValueOrDefault()
extern Il2CppClass* Int16_t4041245914_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2972857984_MetadataUsageId;
extern "C"  int16_t Nullable_1_GetValueOrDefault_m2972857984_gshared (Nullable_1_t2304312229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2972857984_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int16_t V_0 = 0;
	int16_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int16_t L_1 = (int16_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Int16_t4041245914_il2cpp_TypeInfo_var, (&V_0));
		int16_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int16_t Nullable_1_GetValueOrDefault_m2972857984_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2304312229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int16_t _returnValue = Nullable_1_GetValueOrDefault_m2972857984(&_thisAdjusted, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int16>::GetValueOrDefault(T)
extern "C"  int16_t Nullable_1_GetValueOrDefault_m1921315238_gshared (Nullable_1_t2304312229 * __this, int16_t ___defaultValue0, const MethodInfo* method)
{
	int16_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int16_t L_1 = (int16_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int16_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int16_t Nullable_1_GetValueOrDefault_m1921315238_AdjustorThunk (Il2CppObject * __this, int16_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t2304312229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int16_t _returnValue = Nullable_1_GetValueOrDefault_m1921315238(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Int16>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1805746705_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1805746705_gshared (Nullable_1_t2304312229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1805746705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int16_t* L_1 = (int16_t*)__this->get_address_of_value_0();
		String_t* L_2 = Int16_ToString_m1247017262((int16_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1805746705_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2304312229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1805746705(&_thisAdjusted, method);
	*reinterpret_cast<int16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Int32>::.ctor(T)
extern "C"  void Nullable_1__ctor_m255206972_gshared (Nullable_1_t334943763 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m255206972_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m255206972(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m4233035231_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m4233035231_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m4233035231(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int32>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m3953935279_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m3953935279_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3953935279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m3953935279_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m3953935279(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2848647165_gshared (Nullable_1_t334943763 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t334943763 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1118562548((Nullable_1_t334943763 *)__this, (Nullable_1_t334943763 )((*(Nullable_1_t334943763 *)((Nullable_1_t334943763 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m2848647165_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2848647165(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1118562548_gshared (Nullable_1_t334943763 * __this, Nullable_1_t334943763  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Int32_Equals_m753832628((int32_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1118562548_AdjustorThunk (Il2CppObject * __this, Nullable_1_t334943763  ___other0, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1118562548(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Int32>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1859855859_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		int32_t L_2 = Int32_GetHashCode_m1381647448((int32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1859855859_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1859855859(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int32>::GetValueOrDefault()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3117741790_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3117741790_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3117741790_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3117741790_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m3117741790(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int32>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1051777376_gshared (Nullable_1_t334943763 * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int32_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1051777376_AdjustorThunk (Il2CppObject * __this, int32_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m1051777376(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Int32>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2285560203_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2285560203_gshared (Nullable_1_t334943763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2285560203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		String_t* L_2 = Int32_ToString_m2960866144((int32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2285560203_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t334943763  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2285560203(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Int64>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2102803219_gshared (Nullable_1_t3467111648 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int64_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m2102803219_AdjustorThunk (Il2CppObject * __this, int64_t ___value0, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m2102803219(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Int64>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m776883952_gshared (Nullable_1_t3467111648 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m776883952_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m776883952(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int64>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m2865988002_MetadataUsageId;
extern "C"  int64_t Nullable_1_get_Value_m2865988002_gshared (Nullable_1_t3467111648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2865988002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int64_t L_2 = (int64_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int64_t Nullable_1_get_Value_m2865988002_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int64_t _returnValue = Nullable_1_get_Value_m2865988002(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int64>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m4207337156_gshared (Nullable_1_t3467111648 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3467111648 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3974016973((Nullable_1_t3467111648 *)__this, (Nullable_1_t3467111648 )((*(Nullable_1_t3467111648 *)((Nullable_1_t3467111648 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m4207337156_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m4207337156(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Int64>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3974016973_gshared (Nullable_1_t3467111648 * __this, Nullable_1_t3467111648  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int64_t* L_3 = (int64_t*)(&___other0)->get_address_of_value_0();
		int64_t L_4 = (int64_t)__this->get_value_0();
		int64_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Int64_Equals_m3608806223((int64_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3974016973_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3467111648  ___other0, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3974016973(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Int64>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2841619984_gshared (Nullable_1_t3467111648 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int64_t* L_1 = (int64_t*)__this->get_address_of_value_0();
		int32_t L_2 = Int64_GetHashCode_m4047499913((int64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m2841619984_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m2841619984(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int64>::GetValueOrDefault()
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2478588639_MetadataUsageId;
extern "C"  int64_t Nullable_1_GetValueOrDefault_m2478588639_gshared (Nullable_1_t3467111648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2478588639_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	int64_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int64_t L_1 = (int64_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Int64_t909078037_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int64_t Nullable_1_GetValueOrDefault_m2478588639_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int64_t _returnValue = Nullable_1_GetValueOrDefault_m2478588639(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Int64>::GetValueOrDefault(T)
extern "C"  int64_t Nullable_1_GetValueOrDefault_m4186601455_gshared (Nullable_1_t3467111648 * __this, int64_t ___defaultValue0, const MethodInfo* method)
{
	int64_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int64_t L_1 = (int64_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int64_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int64_t Nullable_1_GetValueOrDefault_m4186601455_AdjustorThunk (Il2CppObject * __this, int64_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int64_t _returnValue = Nullable_1_GetValueOrDefault_m4186601455(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Int64>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3201666422_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3201666422_gshared (Nullable_1_t3467111648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3201666422_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int64_t* L_1 = (int64_t*)__this->get_address_of_value_0();
		String_t* L_2 = Int64_ToString_m689375889((int64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3201666422_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3467111648  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3201666422(&_thisAdjusted, method);
	*reinterpret_cast<int64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.SByte>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3868947219_gshared (Nullable_1_t3012451160 * __this, int8_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int8_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3868947219_AdjustorThunk (Il2CppObject * __this, int8_t ___value0, const MethodInfo* method)
{
	Nullable_1_t3012451160  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3868947219(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.SByte>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3684326092_gshared (Nullable_1_t3012451160 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3684326092_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3012451160  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3684326092(&_thisAdjusted, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.SByte>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m3630276758_MetadataUsageId;
extern "C"  int8_t Nullable_1_get_Value_m3630276758_gshared (Nullable_1_t3012451160 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3630276758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int8_t L_2 = (int8_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int8_t Nullable_1_get_Value_m3630276758_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3012451160  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int8_t _returnValue = Nullable_1_get_Value_m3630276758(&_thisAdjusted, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.SByte>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m4001216772_gshared (Nullable_1_t3012451160 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3012451160 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3573951673((Nullable_1_t3012451160 *)__this, (Nullable_1_t3012451160 )((*(Nullable_1_t3012451160 *)((Nullable_1_t3012451160 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m4001216772_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3012451160  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m4001216772(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.SByte>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3573951673_gshared (Nullable_1_t3012451160 * __this, Nullable_1_t3012451160  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int8_t* L_3 = (int8_t*)(&___other0)->get_address_of_value_0();
		int8_t L_4 = (int8_t)__this->get_value_0();
		int8_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = SByte_Equals_m755715567((int8_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3573951673_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3012451160  ___other0, const MethodInfo* method)
{
	Nullable_1_t3012451160  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3573951673(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.SByte>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3397837692_gshared (Nullable_1_t3012451160 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int8_t* L_1 = (int8_t*)__this->get_address_of_value_0();
		int32_t L_2 = SByte_GetHashCode_m1692727069((int8_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m3397837692_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3012451160  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m3397837692(&_thisAdjusted, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.SByte>::GetValueOrDefault()
extern Il2CppClass* SByte_t454417549_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2746404639_MetadataUsageId;
extern "C"  int8_t Nullable_1_GetValueOrDefault_m2746404639_gshared (Nullable_1_t3012451160 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2746404639_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int8_t V_0 = 0x0;
	int8_t G_B3_0 = 0x0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int8_t L_1 = (int8_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (SByte_t454417549_il2cpp_TypeInfo_var, (&V_0));
		int8_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int8_t Nullable_1_GetValueOrDefault_m2746404639_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3012451160  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int8_t _returnValue = Nullable_1_GetValueOrDefault_m2746404639(&_thisAdjusted, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.SByte>::GetValueOrDefault(T)
extern "C"  int8_t Nullable_1_GetValueOrDefault_m1522108711_gshared (Nullable_1_t3012451160 * __this, int8_t ___defaultValue0, const MethodInfo* method)
{
	int8_t G_B3_0 = 0x0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int8_t L_1 = (int8_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		int8_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  int8_t Nullable_1_GetValueOrDefault_m1522108711_AdjustorThunk (Il2CppObject * __this, int8_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t3012451160  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int8_t _returnValue = Nullable_1_GetValueOrDefault_m1522108711(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.SByte>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2936678402_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2936678402_gshared (Nullable_1_t3012451160 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2936678402_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int8_t* L_1 = (int8_t*)__this->get_address_of_value_0();
		String_t* L_2 = SByte_ToString_m2615560365((int8_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m2936678402_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3012451160  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int8_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m2936678402(&_thisAdjusted, method);
	*reinterpret_cast<int8_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Single>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2886742484_gshared (Nullable_1_t339576247 * __this, float ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		float L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m2886742484_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m2886742484(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1754410771_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1754410771_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1754410771(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Single>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m3550128851_MetadataUsageId;
extern "C"  float Nullable_1_get_Value_m3550128851_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3550128851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		float L_2 = (float)__this->get_value_0();
		return L_2;
	}
}
extern "C"  float Nullable_1_get_Value_m3550128851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	float _returnValue = Nullable_1_get_Value_m3550128851(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1038420037_gshared (Nullable_1_t339576247 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t339576247 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m3850685758((Nullable_1_t339576247 *)__this, (Nullable_1_t339576247 )((*(Nullable_1_t339576247 *)((Nullable_1_t339576247 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1038420037_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1038420037(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3850685758_gshared (Nullable_1_t339576247 * __this, Nullable_1_t339576247  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		float* L_3 = (float*)(&___other0)->get_address_of_value_0();
		float L_4 = (float)__this->get_value_0();
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Single_Equals_m3679433096((float*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3850685758_AdjustorThunk (Il2CppObject * __this, Nullable_1_t339576247  ___other0, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3850685758(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Single>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m572762171_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		float* L_1 = (float*)__this->get_address_of_value_0();
		int32_t L_2 = Single_GetHashCode_m3102305584((float*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m572762171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m572762171(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Single>::GetValueOrDefault()
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3764440182_MetadataUsageId;
extern "C"  float Nullable_1_GetValueOrDefault_m3764440182_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3764440182_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		float L_1 = (float)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  float Nullable_1_GetValueOrDefault_m3764440182_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	float _returnValue = Nullable_1_GetValueOrDefault_m3764440182(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Single>::GetValueOrDefault(T)
extern "C"  float Nullable_1_GetValueOrDefault_m1353198268_gshared (Nullable_1_t339576247 * __this, float ___defaultValue0, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		float L_1 = (float)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		float L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  float Nullable_1_GetValueOrDefault_m1353198268_AdjustorThunk (Il2CppObject * __this, float ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	float _returnValue = Nullable_1_GetValueOrDefault_m1353198268(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Single>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m940266439_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m940266439_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m940266439_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		float* L_1 = (float*)__this->get_address_of_value_0();
		String_t* L_2 = Single_ToString_m1813392066((float*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m940266439_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m940266439(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C"  void Nullable_1__ctor_m796575255_gshared (Nullable_1_t1693325264 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		TimeSpan_t3430258949  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m796575255_AdjustorThunk (Il2CppObject * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m796575255(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3663286555_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3663286555_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3663286555(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m1743067844_MetadataUsageId;
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1743067844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		TimeSpan_t3430258949  L_2 = (TimeSpan_t3430258949 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t3430258949  _returnValue = Nullable_1_get_Value_m1743067844(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3860982732_gshared (Nullable_1_t1693325264 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1693325264 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1889119397((Nullable_1_t1693325264 *)__this, (Nullable_1_t1693325264 )((*(Nullable_1_t1693325264 *)((Nullable_1_t1693325264 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3860982732_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3860982732(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1889119397_gshared (Nullable_1_t1693325264 * __this, Nullable_1_t1693325264  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		TimeSpan_t3430258949 * L_3 = (TimeSpan_t3430258949 *)(&___other0)->get_address_of_value_0();
		TimeSpan_t3430258949  L_4 = (TimeSpan_t3430258949 )__this->get_value_0();
		TimeSpan_t3430258949  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = TimeSpan_Equals_m4102942751((TimeSpan_t3430258949 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1889119397_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1693325264  ___other0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1889119397(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		TimeSpan_t3430258949 * L_1 = (TimeSpan_t3430258949 *)__this->get_address_of_value_0();
		int32_t L_2 = TimeSpan_GetHashCode_m550404245((TimeSpan_t3430258949 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1791015856(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::GetValueOrDefault()
extern Il2CppClass* TimeSpan_t3430258949_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1253254751_MetadataUsageId;
extern "C"  TimeSpan_t3430258949  Nullable_1_GetValueOrDefault_m1253254751_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1253254751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t3430258949  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TimeSpan_t3430258949  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		TimeSpan_t3430258949  L_1 = (TimeSpan_t3430258949 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (TimeSpan_t3430258949_il2cpp_TypeInfo_var, (&V_0));
		TimeSpan_t3430258949  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  TimeSpan_t3430258949  Nullable_1_GetValueOrDefault_m1253254751_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t3430258949  _returnValue = Nullable_1_GetValueOrDefault_m1253254751(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::GetValueOrDefault(T)
extern "C"  TimeSpan_t3430258949  Nullable_1_GetValueOrDefault_m521074095_gshared (Nullable_1_t1693325264 * __this, TimeSpan_t3430258949  ___defaultValue0, const MethodInfo* method)
{
	TimeSpan_t3430258949  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		TimeSpan_t3430258949  L_1 = (TimeSpan_t3430258949 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		TimeSpan_t3430258949  L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  TimeSpan_t3430258949  Nullable_1_GetValueOrDefault_m521074095_AdjustorThunk (Il2CppObject * __this, TimeSpan_t3430258949  ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t3430258949  _returnValue = Nullable_1_GetValueOrDefault_m521074095(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1238126148_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1238126148_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1238126148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		TimeSpan_t3430258949 * L_1 = (TimeSpan_t3430258949 *)__this->get_address_of_value_0();
		String_t* L_2 = TimeSpan_ToString_m2947282901((TimeSpan_t3430258949 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1238126148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1238126148(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.UInt16>::.ctor(T)
extern "C"  void Nullable_1__ctor_m960711409_gshared (Nullable_1_t3544916222 * __this, uint16_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		uint16_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m960711409_AdjustorThunk (Il2CppObject * __this, uint16_t ___value0, const MethodInfo* method)
{
	Nullable_1_t3544916222  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m960711409(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.UInt16>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3144126760_gshared (Nullable_1_t3544916222 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3144126760_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3544916222  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3144126760(&_thisAdjusted, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt16>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m3580999918_MetadataUsageId;
extern "C"  uint16_t Nullable_1_get_Value_m3580999918_gshared (Nullable_1_t3544916222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3580999918_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		uint16_t L_2 = (uint16_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  uint16_t Nullable_1_get_Value_m3580999918_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3544916222  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint16_t _returnValue = Nullable_1_get_Value_m3580999918(&_thisAdjusted, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.UInt16>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3624552446_gshared (Nullable_1_t3544916222 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3544916222 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m2954062977((Nullable_1_t3544916222 *)__this, (Nullable_1_t3544916222 )((*(Nullable_1_t3544916222 *)((Nullable_1_t3544916222 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3624552446_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t3544916222  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3624552446(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.UInt16>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2954062977_gshared (Nullable_1_t3544916222 * __this, Nullable_1_t3544916222  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		uint16_t* L_3 = (uint16_t*)(&___other0)->get_address_of_value_0();
		uint16_t L_4 = (uint16_t)__this->get_value_0();
		uint16_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = UInt16_Equals_m1047376923((uint16_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2954062977_AdjustorThunk (Il2CppObject * __this, Nullable_1_t3544916222  ___other0, const MethodInfo* method)
{
	Nullable_1_t3544916222  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2954062977(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.UInt16>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3718174238_gshared (Nullable_1_t3544916222 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		uint16_t* L_1 = (uint16_t*)__this->get_address_of_value_0();
		int32_t L_2 = UInt16_GetHashCode_m1468226569((uint16_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m3718174238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3544916222  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m3718174238(&_thisAdjusted, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt16>::GetValueOrDefault()
extern Il2CppClass* UInt16_t986882611_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m257080427_MetadataUsageId;
extern "C"  uint16_t Nullable_1_GetValueOrDefault_m257080427_gshared (Nullable_1_t3544916222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m257080427_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	uint16_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint16_t L_1 = (uint16_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (UInt16_t986882611_il2cpp_TypeInfo_var, (&V_0));
		uint16_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  uint16_t Nullable_1_GetValueOrDefault_m257080427_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3544916222  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint16_t _returnValue = Nullable_1_GetValueOrDefault_m257080427(&_thisAdjusted, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt16>::GetValueOrDefault(T)
extern "C"  uint16_t Nullable_1_GetValueOrDefault_m3275284163_gshared (Nullable_1_t3544916222 * __this, uint16_t ___defaultValue0, const MethodInfo* method)
{
	uint16_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint16_t L_1 = (uint16_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		uint16_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  uint16_t Nullable_1_GetValueOrDefault_m3275284163_AdjustorThunk (Il2CppObject * __this, uint16_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t3544916222  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint16_t _returnValue = Nullable_1_GetValueOrDefault_m3275284163(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.UInt16>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3771839058_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3771839058_gshared (Nullable_1_t3544916222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3771839058_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		uint16_t* L_1 = (uint16_t*)__this->get_address_of_value_0();
		String_t* L_2 = UInt16_ToString_m2038947049((uint16_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3771839058_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t3544916222  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint16_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3771839058(&_thisAdjusted, method);
	*reinterpret_cast<uint16_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.UInt32>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2315265885_gshared (Nullable_1_t412748336 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		uint32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m2315265885_AdjustorThunk (Il2CppObject * __this, uint32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t412748336  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m2315265885(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.UInt32>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1739134206_gshared (Nullable_1_t412748336 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1739134206_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t412748336  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1739134206(&_thisAdjusted, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt32>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m1072616600_MetadataUsageId;
extern "C"  uint32_t Nullable_1_get_Value_m1072616600_gshared (Nullable_1_t412748336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1072616600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		uint32_t L_2 = (uint32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  uint32_t Nullable_1_get_Value_m1072616600_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t412748336  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint32_t _returnValue = Nullable_1_get_Value_m1072616600(&_thisAdjusted, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.UInt32>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1902564036_gshared (Nullable_1_t412748336 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t412748336 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m664996039((Nullable_1_t412748336 *)__this, (Nullable_1_t412748336 )((*(Nullable_1_t412748336 *)((Nullable_1_t412748336 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1902564036_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t412748336  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1902564036(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.UInt32>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m664996039_gshared (Nullable_1_t412748336 * __this, Nullable_1_t412748336  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		uint32_t* L_3 = (uint32_t*)(&___other0)->get_address_of_value_0();
		uint32_t L_4 = (uint32_t)__this->get_value_0();
		uint32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = UInt32_Equals_m3998179817((uint32_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m664996039_AdjustorThunk (Il2CppObject * __this, Nullable_1_t412748336  ___other0, const MethodInfo* method)
{
	Nullable_1_t412748336  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m664996039(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.UInt32>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1673973092_gshared (Nullable_1_t412748336 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		uint32_t* L_1 = (uint32_t*)__this->get_address_of_value_0();
		int32_t L_2 = UInt32_GetHashCode_m2903162199((uint32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1673973092_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t412748336  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1673973092(&_thisAdjusted, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt32>::GetValueOrDefault()
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3441876505_MetadataUsageId;
extern "C"  uint32_t Nullable_1_GetValueOrDefault_m3441876505_gshared (Nullable_1_t412748336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3441876505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint32_t L_1 = (uint32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (UInt32_t2149682021_il2cpp_TypeInfo_var, (&V_0));
		uint32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  uint32_t Nullable_1_GetValueOrDefault_m3441876505_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t412748336  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint32_t _returnValue = Nullable_1_GetValueOrDefault_m3441876505(&_thisAdjusted, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt32>::GetValueOrDefault(T)
extern "C"  uint32_t Nullable_1_GetValueOrDefault_m1527788505_gshared (Nullable_1_t412748336 * __this, uint32_t ___defaultValue0, const MethodInfo* method)
{
	uint32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint32_t L_1 = (uint32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		uint32_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  uint32_t Nullable_1_GetValueOrDefault_m1527788505_AdjustorThunk (Il2CppObject * __this, uint32_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t412748336  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint32_t _returnValue = Nullable_1_GetValueOrDefault_m1527788505(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.UInt32>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1129936676_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1129936676_gshared (Nullable_1_t412748336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1129936676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		uint32_t* L_1 = (uint32_t*)__this->get_address_of_value_0();
		String_t* L_2 = UInt32_ToString_m554020223((uint32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1129936676_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t412748336  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1129936676(&_thisAdjusted, method);
	*reinterpret_cast<uint32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.UInt64>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1546696000_gshared (Nullable_1_t1172263229 * __this, uint64_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		uint64_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1546696000_AdjustorThunk (Il2CppObject * __this, uint64_t ___value0, const MethodInfo* method)
{
	Nullable_1_t1172263229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1546696000(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.UInt64>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m931590617_gshared (Nullable_1_t1172263229 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m931590617_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1172263229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m931590617(&_thisAdjusted, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt64>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m4149034565_MetadataUsageId;
extern "C"  uint64_t Nullable_1_get_Value_m4149034565_gshared (Nullable_1_t1172263229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m4149034565_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		uint64_t L_2 = (uint64_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  uint64_t Nullable_1_get_Value_m4149034565_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1172263229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint64_t _returnValue = Nullable_1_get_Value_m4149034565(&_thisAdjusted, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.UInt64>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3942107583_gshared (Nullable_1_t1172263229 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1172263229 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1735403772((Nullable_1_t1172263229 *)__this, (Nullable_1_t1172263229 )((*(Nullable_1_t1172263229 *)((Nullable_1_t1172263229 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3942107583_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1172263229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3942107583(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.UInt64>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1735403772_gshared (Nullable_1_t1172263229 * __this, Nullable_1_t1172263229  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		uint64_t* L_3 = (uint64_t*)(&___other0)->get_address_of_value_0();
		uint64_t L_4 = (uint64_t)__this->get_value_0();
		uint64_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = UInt64_Equals_m1243465002((uint64_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1735403772_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1172263229  ___other0, const MethodInfo* method)
{
	Nullable_1_t1172263229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1735403772(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.UInt64>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3496532209_gshared (Nullable_1_t1172263229 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		uint64_t* L_1 = (uint64_t*)__this->get_address_of_value_0();
		int32_t L_2 = UInt64_GetHashCode_m3478338766((uint64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m3496532209_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1172263229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m3496532209(&_thisAdjusted, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt64>::GetValueOrDefault()
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3177485320_MetadataUsageId;
extern "C"  uint64_t Nullable_1_GetValueOrDefault_m3177485320_gshared (Nullable_1_t1172263229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3177485320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	uint64_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint64_t L_1 = (uint64_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (UInt64_t2909196914_il2cpp_TypeInfo_var, (&V_0));
		uint64_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  uint64_t Nullable_1_GetValueOrDefault_m3177485320_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1172263229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint64_t _returnValue = Nullable_1_GetValueOrDefault_m3177485320(&_thisAdjusted, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.UInt64>::GetValueOrDefault(T)
extern "C"  uint64_t Nullable_1_GetValueOrDefault_m2057895358_gshared (Nullable_1_t1172263229 * __this, uint64_t ___defaultValue0, const MethodInfo* method)
{
	uint64_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint64_t L_1 = (uint64_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		uint64_t L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  uint64_t Nullable_1_GetValueOrDefault_m2057895358_AdjustorThunk (Il2CppObject * __this, uint64_t ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t1172263229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	uint64_t _returnValue = Nullable_1_GetValueOrDefault_m2057895358(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.UInt64>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m582229801_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m582229801_gshared (Nullable_1_t1172263229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m582229801_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		uint64_t* L_1 = (uint64_t*)__this->get_address_of_value_0();
		String_t* L_2 = UInt64_ToString_m446228920((uint64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m582229801_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1172263229  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<uint64_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m582229801(&_thisAdjusted, method);
	*reinterpret_cast<uint64_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<UnityEngine.Quaternion>::.ctor(T)
extern "C"  void Nullable_1__ctor_m375970372_gshared (Nullable_1_t2293140233 * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Quaternion_t4030073918  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m375970372_AdjustorThunk (Il2CppObject * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method)
{
	Nullable_1_t2293140233  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m375970372(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<UnityEngine.Quaternion>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1733763085_gshared (Nullable_1_t2293140233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1733763085_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2293140233  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1733763085(&_thisAdjusted, method);
	*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Quaternion>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m825790621_MetadataUsageId;
extern "C"  Quaternion_t4030073918  Nullable_1_get_Value_m825790621_gshared (Nullable_1_t2293140233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m825790621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Quaternion_t4030073918  L_2 = (Quaternion_t4030073918 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  Quaternion_t4030073918  Nullable_1_get_Value_m825790621_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2293140233  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Quaternion_t4030073918  _returnValue = Nullable_1_get_Value_m825790621(&_thisAdjusted, method);
	*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.Quaternion>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3653349131_gshared (Nullable_1_t2293140233 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2293140233 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m288025588((Nullable_1_t2293140233 *)__this, (Nullable_1_t2293140233 )((*(Nullable_1_t2293140233 *)((Nullable_1_t2293140233 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3653349131_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t2293140233  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3653349131(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.Quaternion>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m288025588_gshared (Nullable_1_t2293140233 * __this, Nullable_1_t2293140233  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Quaternion_t4030073918 * L_3 = (Quaternion_t4030073918 *)(&___other0)->get_address_of_value_0();
		Quaternion_t4030073918  L_4 = (Quaternion_t4030073918 )__this->get_value_0();
		Quaternion_t4030073918  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Quaternion_Equals_m3730391696((Quaternion_t4030073918 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m288025588_AdjustorThunk (Il2CppObject * __this, Nullable_1_t2293140233  ___other0, const MethodInfo* method)
{
	Nullable_1_t2293140233  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m288025588(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<UnityEngine.Quaternion>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2070783433_gshared (Nullable_1_t2293140233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Quaternion_t4030073918 * L_1 = (Quaternion_t4030073918 *)__this->get_address_of_value_0();
		int32_t L_2 = Quaternion_GetHashCode_m2270520528((Quaternion_t4030073918 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m2070783433_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2293140233  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m2070783433(&_thisAdjusted, method);
	*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Quaternion>::GetValueOrDefault()
extern Il2CppClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m4264653786_MetadataUsageId;
extern "C"  Quaternion_t4030073918  Nullable_1_GetValueOrDefault_m4264653786_gshared (Nullable_1_t2293140233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m4264653786_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Quaternion_t4030073918  L_1 = (Quaternion_t4030073918 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Quaternion_t4030073918_il2cpp_TypeInfo_var, (&V_0));
		Quaternion_t4030073918  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  Quaternion_t4030073918  Nullable_1_GetValueOrDefault_m4264653786_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2293140233  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Quaternion_t4030073918  _returnValue = Nullable_1_GetValueOrDefault_m4264653786(&_thisAdjusted, method);
	*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Quaternion>::GetValueOrDefault(T)
extern "C"  Quaternion_t4030073918  Nullable_1_GetValueOrDefault_m3365882100_gshared (Nullable_1_t2293140233 * __this, Quaternion_t4030073918  ___defaultValue0, const MethodInfo* method)
{
	Quaternion_t4030073918  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Quaternion_t4030073918  L_1 = (Quaternion_t4030073918 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		Quaternion_t4030073918  L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  Quaternion_t4030073918  Nullable_1_GetValueOrDefault_m3365882100_AdjustorThunk (Il2CppObject * __this, Quaternion_t4030073918  ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t2293140233  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Quaternion_t4030073918  _returnValue = Nullable_1_GetValueOrDefault_m3365882100(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<UnityEngine.Quaternion>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m516194009_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m516194009_gshared (Nullable_1_t2293140233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m516194009_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Quaternion_t4030073918 * L_1 = (Quaternion_t4030073918 *)__this->get_address_of_value_0();
		String_t* L_2 = Quaternion_ToString_m2638853272((Quaternion_t4030073918 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m516194009_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2293140233  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m516194009(&_thisAdjusted, method);
	*reinterpret_cast<Quaternion_t4030073918 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3568989592_gshared (Nullable_1_t506773895 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Vector3_t2243707580  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3568989592_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3568989592(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m4112732909_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m4112732909_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m4112732909(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Vector3>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m463576537_MetadataUsageId;
extern "C"  Vector3_t2243707580  Nullable_1_get_Value_m463576537_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m463576537_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Vector3_t2243707580  L_2 = (Vector3_t2243707580 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  Vector3_t2243707580  Nullable_1_get_Value_m463576537_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Vector3_t2243707580  _returnValue = Nullable_1_get_Value_m463576537(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1233761477_gshared (Nullable_1_t506773895 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t506773895 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m2313387266((Nullable_1_t506773895 *)__this, (Nullable_1_t506773895 )((*(Nullable_1_t506773895 *)((Nullable_1_t506773895 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1233761477_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1233761477(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2313387266_gshared (Nullable_1_t506773895 * __this, Nullable_1_t506773895  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Vector3_t2243707580 * L_3 = (Vector3_t2243707580 *)(&___other0)->get_address_of_value_0();
		Vector3_t2243707580  L_4 = (Vector3_t2243707580 )__this->get_value_0();
		Vector3_t2243707580  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Vector3_Equals_m2692262876((Vector3_t2243707580 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2313387266_AdjustorThunk (Il2CppObject * __this, Nullable_1_t506773895  ___other0, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2313387266(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<UnityEngine.Vector3>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1916216271_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Vector3_t2243707580 * L_1 = (Vector3_t2243707580 *)__this->get_address_of_value_0();
		int32_t L_2 = Vector3_GetHashCode_m1754570744((Vector3_t2243707580 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1916216271_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1916216271(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Vector3>::GetValueOrDefault()
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m587112038_MetadataUsageId;
extern "C"  Vector3_t2243707580  Nullable_1_GetValueOrDefault_m587112038_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m587112038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector3_t2243707580  L_1 = (Vector3_t2243707580 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  Vector3_t2243707580  Nullable_1_GetValueOrDefault_m587112038_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Vector3_t2243707580  _returnValue = Nullable_1_GetValueOrDefault_m587112038(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Vector3>::GetValueOrDefault(T)
extern "C"  Vector3_t2243707580  Nullable_1_GetValueOrDefault_m2354825460_gshared (Nullable_1_t506773895 * __this, Vector3_t2243707580  ___defaultValue0, const MethodInfo* method)
{
	Vector3_t2243707580  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector3_t2243707580  L_1 = (Vector3_t2243707580 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		Vector3_t2243707580  L_2 = ___defaultValue0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
extern "C"  Vector3_t2243707580  Nullable_1_GetValueOrDefault_m2354825460_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___defaultValue0, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Vector3_t2243707580  _returnValue = Nullable_1_GetValueOrDefault_m2354825460(&_thisAdjusted, ___defaultValue0, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<UnityEngine.Vector3>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m769970515_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m769970515_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m769970515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Vector3_t2243707580 * L_1 = (Vector3_t2243707580 *)__this->get_address_of_value_0();
		String_t* L_2 = Vector3_ToString_m3857189970((Vector3_t2243707580 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m769970515_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m769970515(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Predicate`1<Newtonsoft.Json.Linq.JTokenType>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3071612375_gshared (Predicate_1_t4045764624 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Newtonsoft.Json.Linq.JTokenType>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m386521311_gshared (Predicate_1_t4045764624 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m386521311((Predicate_1_t4045764624 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Newtonsoft.Json.Linq.JTokenType>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* JTokenType_t1307827213_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3833769488_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3833769488_gshared (Predicate_1_t4045764624 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3833769488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(JTokenType_t1307827213_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Newtonsoft.Json.Linq.JTokenType>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m822792193_gshared (Predicate_1_t4045764624 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3489771108_gshared (Predicate_1_t185715292 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Newtonsoft.Json.Schema.JsonSchemaType>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1181115596_gshared (Predicate_1_t185715292 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1181115596((Predicate_1_t185715292 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Newtonsoft.Json.Schema.JsonSchemaType>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* JsonSchemaType_t1742745177_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4078930111_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4078930111_gshared (Predicate_1_t185715292 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4078930111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(JsonSchemaType_t1742745177_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Newtonsoft.Json.Schema.JsonSchemaType>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1784835974_gshared (Predicate_1_t185715292 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Byte>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m461821738_gshared (Predicate_1_t2126074551 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Byte>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2886027714_gshared (Predicate_1_t2126074551 * __this, uint8_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2886027714((Predicate_1_t2126074551 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint8_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, uint8_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Byte>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m767759683_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m767759683_gshared (Predicate_1_t2126074551 * __this, uint8_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m767759683_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Byte>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2487624220_gshared (Predicate_1_t2126074551 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Char>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3460606476_gshared (Predicate_1_t1897451453 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Char>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m762264976_gshared (Predicate_1_t1897451453 * __this, Il2CppChar ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m762264976((Predicate_1_t1897451453 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppChar ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppChar ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Char>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1856533029_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1856533029_gshared (Predicate_1_t1897451453 * __this, Il2CppChar ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1856533029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Char_t3454481338_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Char>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1807093618_gshared (Predicate_1_t1897451453 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2359416570_gshared (Predicate_1_t2776792056 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m332691618_gshared (Predicate_1_t2776792056 * __this, KeyValuePair_2_t38854645  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m332691618((Predicate_1_t2776792056 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t38854645  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, KeyValuePair_2_t38854645  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t38854645_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3157529563_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3157529563_gshared (Predicate_1_t2776792056 * __this, KeyValuePair_2_t38854645  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3157529563_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t38854645_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m548603360_gshared (Predicate_1_t2776792056 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2826800414_gshared (Predicate_1_t514847563 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Int32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m695569038_gshared (Predicate_1_t514847563 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m695569038((Predicate_1_t514847563 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2559992383_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2559992383_gshared (Predicate_1_t514847563 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2559992383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1202813828_gshared (Predicate_1_t514847563 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2910895094_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Object>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m921875257_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m921875257((Predicate_1_t1132419410 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3556950370_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3656575065_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1767993638_gshared (Predicate_1_t2832094954 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m527131606_gshared (Predicate_1_t2832094954 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m527131606((Predicate_1_t2832094954 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1448216027_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1448216027_gshared (Predicate_1_t2832094954 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1448216027_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m215026240_gshared (Predicate_1_t2832094954 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1292402863_gshared (Predicate_1_t4236135325 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2060780095_gshared (Predicate_1_t4236135325 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2060780095((Predicate_1_t4236135325 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1856151290_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1856151290_gshared (Predicate_1_t4236135325 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1856151290_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m259774785_gshared (Predicate_1_t4236135325 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3811123782_gshared (Predicate_1_t3612454929 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m122788314_gshared (Predicate_1_t3612454929 * __this, Color32_t874517518  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m122788314((Predicate_1_t3612454929 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Color32_t874517518  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Color32_t874517518  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Color32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color32_t874517518_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2959352225_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2959352225_gshared (Predicate_1_t3612454929 * __this, Color32_t874517518  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2959352225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color32_t874517518_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m924884444_gshared (Predicate_1_t3612454929 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1567825400_gshared (Predicate_1_t2759123787 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3860206640_gshared (Predicate_1_t2759123787 * __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3860206640((Predicate_1_t2759123787 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RaycastResult_t21186376_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4068629879_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4068629879_gshared (Predicate_1_t2759123787 * __this, RaycastResult_t21186376  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4068629879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RaycastResult_t21186376_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m973058386_gshared (Predicate_1_t2759123787 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Experimental.Director.Playable>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3105937642_gshared (Predicate_1_t2110515663 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Experimental.Director.Playable>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1617267354_gshared (Predicate_1_t2110515663 * __this, Playable_t3667545548  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1617267354((Predicate_1_t2110515663 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Playable_t3667545548  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Playable_t3667545548  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Experimental.Director.Playable>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Playable_t3667545548_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3423161611_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3423161611_gshared (Predicate_1_t2110515663 * __this, Playable_t3667545548  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3423161611_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Playable_t3667545548_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Experimental.Director.Playable>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2453294608_gshared (Predicate_1_t2110515663 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.RuntimePlatform>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m955159061_gshared (Predicate_1_t312555082 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.RuntimePlatform>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1264076617_gshared (Predicate_1_t312555082 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1264076617((Predicate_1_t312555082 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.RuntimePlatform>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RuntimePlatform_t1869584967_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2418430170_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2418430170_gshared (Predicate_1_t312555082 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2418430170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RuntimePlatform_t1869584967_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.RuntimePlatform>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3100405575_gshared (Predicate_1_t312555082 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1020292372_gshared (Predicate_1_t1499606915 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3539717340_gshared (Predicate_1_t1499606915 * __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3539717340((Predicate_1_t1499606915 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UICharInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UICharInfo_t3056636800_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3056726495_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3056726495_gshared (Predicate_1_t1499606915 * __this, UICharInfo_t3056636800  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3056726495_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UICharInfo_t3056636800_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2354180346_gshared (Predicate_1_t1499606915 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m784266182_gshared (Predicate_1_t2064247989 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m577088274_gshared (Predicate_1_t2064247989 * __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m577088274((Predicate_1_t2064247989 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UILineInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UILineInfo_t3621277874_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2329589669_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2329589669_gshared (Predicate_1_t2064247989 * __this, UILineInfo_t3621277874  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2329589669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UILineInfo_t3621277874_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3442731496_gshared (Predicate_1_t2064247989 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UIVertex>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m549279630_gshared (Predicate_1_t3942196229 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2883675618_gshared (Predicate_1_t3942196229 * __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2883675618((Predicate_1_t3942196229 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UIVertex>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UIVertex_t1204258818_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3926587117_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3926587117_gshared (Predicate_1_t3942196229 * __this, UIVertex_t1204258818  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3926587117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UIVertex_t1204258818_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m337889472_gshared (Predicate_1_t3942196229 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2863314033_gshared (Predicate_1_t686677694 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3001657933_gshared (Predicate_1_t686677694 * __this, Vector2_t2243707579  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3001657933((Predicate_1_t686677694 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m866207434_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m866207434_gshared (Predicate_1_t686677694 * __this, Vector2_t2243707579  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m866207434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3406729927_gshared (Predicate_1_t686677694 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3243601712_gshared (Predicate_1_t686677695 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2775223656_gshared (Predicate_1_t686677695 * __this, Vector3_t2243707580  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2775223656((Predicate_1_t686677695 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1764756107_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1764756107_gshared (Predicate_1_t686677695 * __this, Vector3_t2243707580  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1764756107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1035116514_gshared (Predicate_1_t686677695 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2995226103_gshared (Predicate_1_t686677696 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2407726575_gshared (Predicate_1_t686677696 * __this, Vector4_t2243707581  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2407726575((Predicate_1_t686677696 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2425667920_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2425667920_gshared (Predicate_1_t686677696 * __this, Vector4_t2243707581  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2425667920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2420144145_gshared (Predicate_1_t686677696 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Getter_2__ctor_m653998582_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Getter_2_Invoke_m3338489829_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ____this0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Getter_2_Invoke_m3338489829((Getter_2_t4179406139 *)__this->get_prev_9(),____this0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Getter_2_BeginInvoke_m2080015031_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ____this0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____this0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Getter_2_EndInvoke_m977999903_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Reflection.MonoProperty/StaticGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void StaticGetter_1__ctor_m1290492285_gshared (StaticGetter_1_t1095697167 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * StaticGetter_1_Invoke_m1348877692_gshared (StaticGetter_1_t1095697167 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StaticGetter_1_Invoke_m1348877692((StaticGetter_1_t1095697167 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StaticGetter_1_BeginInvoke_m2732579814_gshared (StaticGetter_1_t1095697167 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * StaticGetter_1_EndInvoke_m44757160_gshared (StaticGetter_1_t1095697167 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAdsDelegate_2__ctor_m1114114262_gshared (UnityAdsDelegate_2_t3657054456 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Boolean>::Invoke(T1,T2)
extern "C"  void UnityAdsDelegate_2_Invoke_m342131961_gshared (UnityAdsDelegate_2_t3657054456 * __this, Il2CppObject * ___p10, bool ___p21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAdsDelegate_2_Invoke_m342131961((UnityAdsDelegate_2_t3657054456 *)__this->get_prev_9(),___p10, ___p21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___p10, bool ___p21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___p10, ___p21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___p10, bool ___p21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___p10, ___p21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___p21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___p10, ___p21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsDelegate_2_BeginInvoke_m673286156_MetadataUsageId;
extern "C"  Il2CppObject * UnityAdsDelegate_2_BeginInvoke_m673286156_gshared (UnityAdsDelegate_2_t3657054456 * __this, Il2CppObject * ___p10, bool ___p21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsDelegate_2_BeginInvoke_m673286156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___p10;
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___p21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAdsDelegate_2_EndInvoke_m873428936_gshared (UnityAdsDelegate_2_t3657054456 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAdsDelegate_2__ctor_m854135587_gshared (UnityAdsDelegate_2_t2520929033 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void UnityAdsDelegate_2_Invoke_m2324892202_gshared (UnityAdsDelegate_2_t2520929033 * __this, Il2CppObject * ___p10, Il2CppObject * ___p21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAdsDelegate_2_Invoke_m2324892202((UnityAdsDelegate_2_t2520929033 *)__this->get_prev_9(),___p10, ___p21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___p10, Il2CppObject * ___p21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___p10, ___p21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___p10, Il2CppObject * ___p21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___p10, ___p21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___p21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___p10, ___p21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAdsDelegate_2_BeginInvoke_m373029159_gshared (UnityAdsDelegate_2_t2520929033 * __this, Il2CppObject * ___p10, Il2CppObject * ___p21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___p10;
	__d_args[1] = ___p21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAdsDelegate_2_EndInvoke_m1644903117_gshared (UnityAdsDelegate_2_t2520929033 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
