﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidGameActivityResult
struct AndroidGameActivityResult_t1444576507;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidGameActivityResult::.ctor()
extern "C"  void AndroidGameActivityResult__ctor_m3429318364 (AndroidGameActivityResult_t1444576507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
