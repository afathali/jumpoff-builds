﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// SavedGamesExample
struct SavedGamesExample_t916988576;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1
struct  U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061  : public Il2CppObject
{
public:
	// System.Int32 SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::<height>__1
	int32_t ___U3CheightU3E__1_1;
	// UnityEngine.Texture2D SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::<Screenshot>__2
	Texture2D_t3542995729 * ___U3CScreenshotU3E__2_2;
	// System.Int64 SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::<TotalPlayedTime>__3
	int64_t ___U3CTotalPlayedTimeU3E__3_3;
	// System.String SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::<currentSaveName>__4
	String_t* ___U3CcurrentSaveNameU3E__4_4;
	// System.String SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::<description>__5
	String_t* ___U3CdescriptionU3E__5_5;
	// System.Int32 SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::$PC
	int32_t ___U24PC_6;
	// System.Object SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::$current
	Il2CppObject * ___U24current_7;
	// SavedGamesExample SavedGamesExample/<MakeScreenshotAndSaveGameData>c__Iterator1::<>f__this
	SavedGamesExample_t916988576 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__1_1() { return static_cast<int32_t>(offsetof(U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061, ___U3CheightU3E__1_1)); }
	inline int32_t get_U3CheightU3E__1_1() const { return ___U3CheightU3E__1_1; }
	inline int32_t* get_address_of_U3CheightU3E__1_1() { return &___U3CheightU3E__1_1; }
	inline void set_U3CheightU3E__1_1(int32_t value)
	{
		___U3CheightU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CScreenshotU3E__2_2() { return static_cast<int32_t>(offsetof(U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061, ___U3CScreenshotU3E__2_2)); }
	inline Texture2D_t3542995729 * get_U3CScreenshotU3E__2_2() const { return ___U3CScreenshotU3E__2_2; }
	inline Texture2D_t3542995729 ** get_address_of_U3CScreenshotU3E__2_2() { return &___U3CScreenshotU3E__2_2; }
	inline void set_U3CScreenshotU3E__2_2(Texture2D_t3542995729 * value)
	{
		___U3CScreenshotU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CScreenshotU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CTotalPlayedTimeU3E__3_3() { return static_cast<int32_t>(offsetof(U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061, ___U3CTotalPlayedTimeU3E__3_3)); }
	inline int64_t get_U3CTotalPlayedTimeU3E__3_3() const { return ___U3CTotalPlayedTimeU3E__3_3; }
	inline int64_t* get_address_of_U3CTotalPlayedTimeU3E__3_3() { return &___U3CTotalPlayedTimeU3E__3_3; }
	inline void set_U3CTotalPlayedTimeU3E__3_3(int64_t value)
	{
		___U3CTotalPlayedTimeU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentSaveNameU3E__4_4() { return static_cast<int32_t>(offsetof(U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061, ___U3CcurrentSaveNameU3E__4_4)); }
	inline String_t* get_U3CcurrentSaveNameU3E__4_4() const { return ___U3CcurrentSaveNameU3E__4_4; }
	inline String_t** get_address_of_U3CcurrentSaveNameU3E__4_4() { return &___U3CcurrentSaveNameU3E__4_4; }
	inline void set_U3CcurrentSaveNameU3E__4_4(String_t* value)
	{
		___U3CcurrentSaveNameU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentSaveNameU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3E__5_5() { return static_cast<int32_t>(offsetof(U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061, ___U3CdescriptionU3E__5_5)); }
	inline String_t* get_U3CdescriptionU3E__5_5() const { return ___U3CdescriptionU3E__5_5; }
	inline String_t** get_address_of_U3CdescriptionU3E__5_5() { return &___U3CdescriptionU3E__5_5; }
	inline void set_U3CdescriptionU3E__5_5(String_t* value)
	{
		___U3CdescriptionU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdescriptionU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061, ___U3CU3Ef__this_8)); }
	inline SavedGamesExample_t916988576 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline SavedGamesExample_t916988576 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(SavedGamesExample_t916988576 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
