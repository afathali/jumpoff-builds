﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TW_OAuthAPIRequest
struct TW_OAuthAPIRequest_t3823919588;
// System.Action`1<TW_APIRequstResult>
struct Action_1_t256950437;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// TW_APIRequstResult
struct TW_APIRequstResult_t455151055;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_TW_APIRequstResult455151055.h"

// System.Void TW_OAuthAPIRequest::.ctor()
extern "C"  void TW_OAuthAPIRequest__ctor_m2058475409 (TW_OAuthAPIRequest_t3823919588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_OAuthAPIRequest::add_OnResult(System.Action`1<TW_APIRequstResult>)
extern "C"  void TW_OAuthAPIRequest_add_OnResult_m476250238 (TW_OAuthAPIRequest_t3823919588 * __this, Action_1_t256950437 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_OAuthAPIRequest::remove_OnResult(System.Action`1<TW_APIRequstResult>)
extern "C"  void TW_OAuthAPIRequest_remove_OnResult_m1144729707 (TW_OAuthAPIRequest_t3823919588 * __this, Action_1_t256950437 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TW_OAuthAPIRequest TW_OAuthAPIRequest::Create()
extern "C"  TW_OAuthAPIRequest_t3823919588 * TW_OAuthAPIRequest_Create_m605319100 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_OAuthAPIRequest::Send(System.String)
extern "C"  void TW_OAuthAPIRequest_Send_m1687966115 (TW_OAuthAPIRequest_t3823919588 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_OAuthAPIRequest::AddParam(System.String,System.Int32)
extern "C"  void TW_OAuthAPIRequest_AddParam_m2680956470 (TW_OAuthAPIRequest_t3823919588 * __this, String_t* ___name0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_OAuthAPIRequest::AddParam(System.String,System.String)
extern "C"  void TW_OAuthAPIRequest_AddParam_m372050447 (TW_OAuthAPIRequest_t3823919588 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_OAuthAPIRequest::SetUrl(System.String)
extern "C"  void TW_OAuthAPIRequest_SetUrl_m4214219988 (TW_OAuthAPIRequest_t3823919588 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TW_OAuthAPIRequest::Request()
extern "C"  Il2CppObject * TW_OAuthAPIRequest_Request_m2564663186 (TW_OAuthAPIRequest_t3823919588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_OAuthAPIRequest::<OnResult>m__B0(TW_APIRequstResult)
extern "C"  void TW_OAuthAPIRequest_U3COnResultU3Em__B0_m1917750621 (Il2CppObject * __this /* static, unused */, TW_APIRequstResult_t455151055 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
