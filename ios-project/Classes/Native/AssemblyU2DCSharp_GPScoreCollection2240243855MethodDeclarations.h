﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPScoreCollection
struct GPScoreCollection_t2240243855;

#include "codegen/il2cpp-codegen.h"

// System.Void GPScoreCollection::.ctor()
extern "C"  void GPScoreCollection__ctor_m1426001934 (GPScoreCollection_t2240243855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
