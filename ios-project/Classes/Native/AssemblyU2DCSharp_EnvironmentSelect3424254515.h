﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// EnvironmentListItem
struct EnvironmentListItem_t1261052282;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvironmentSelect
struct  EnvironmentSelect_t3424254515  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button EnvironmentSelect::env_btn
	Button_t2872111280 * ___env_btn_2;
	// UnityEngine.GameObject EnvironmentSelect::environmentUI
	GameObject_t1756533147 * ___environmentUI_3;
	// UnityEngine.UI.Button EnvironmentSelect::env1_btn
	Button_t2872111280 * ___env1_btn_4;
	// UnityEngine.UI.Button EnvironmentSelect::env2_btn
	Button_t2872111280 * ___env2_btn_5;
	// UnityEngine.UI.Button EnvironmentSelect::env3_btn
	Button_t2872111280 * ___env3_btn_6;
	// EnvironmentListItem EnvironmentSelect::env_farm
	EnvironmentListItem_t1261052282 * ___env_farm_7;
	// EnvironmentListItem EnvironmentSelect::env_horseShow
	EnvironmentListItem_t1261052282 * ___env_horseShow_8;
	// EnvironmentListItem EnvironmentSelect::env_indoor
	EnvironmentListItem_t1261052282 * ___env_indoor_9;

public:
	inline static int32_t get_offset_of_env_btn_2() { return static_cast<int32_t>(offsetof(EnvironmentSelect_t3424254515, ___env_btn_2)); }
	inline Button_t2872111280 * get_env_btn_2() const { return ___env_btn_2; }
	inline Button_t2872111280 ** get_address_of_env_btn_2() { return &___env_btn_2; }
	inline void set_env_btn_2(Button_t2872111280 * value)
	{
		___env_btn_2 = value;
		Il2CppCodeGenWriteBarrier(&___env_btn_2, value);
	}

	inline static int32_t get_offset_of_environmentUI_3() { return static_cast<int32_t>(offsetof(EnvironmentSelect_t3424254515, ___environmentUI_3)); }
	inline GameObject_t1756533147 * get_environmentUI_3() const { return ___environmentUI_3; }
	inline GameObject_t1756533147 ** get_address_of_environmentUI_3() { return &___environmentUI_3; }
	inline void set_environmentUI_3(GameObject_t1756533147 * value)
	{
		___environmentUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___environmentUI_3, value);
	}

	inline static int32_t get_offset_of_env1_btn_4() { return static_cast<int32_t>(offsetof(EnvironmentSelect_t3424254515, ___env1_btn_4)); }
	inline Button_t2872111280 * get_env1_btn_4() const { return ___env1_btn_4; }
	inline Button_t2872111280 ** get_address_of_env1_btn_4() { return &___env1_btn_4; }
	inline void set_env1_btn_4(Button_t2872111280 * value)
	{
		___env1_btn_4 = value;
		Il2CppCodeGenWriteBarrier(&___env1_btn_4, value);
	}

	inline static int32_t get_offset_of_env2_btn_5() { return static_cast<int32_t>(offsetof(EnvironmentSelect_t3424254515, ___env2_btn_5)); }
	inline Button_t2872111280 * get_env2_btn_5() const { return ___env2_btn_5; }
	inline Button_t2872111280 ** get_address_of_env2_btn_5() { return &___env2_btn_5; }
	inline void set_env2_btn_5(Button_t2872111280 * value)
	{
		___env2_btn_5 = value;
		Il2CppCodeGenWriteBarrier(&___env2_btn_5, value);
	}

	inline static int32_t get_offset_of_env3_btn_6() { return static_cast<int32_t>(offsetof(EnvironmentSelect_t3424254515, ___env3_btn_6)); }
	inline Button_t2872111280 * get_env3_btn_6() const { return ___env3_btn_6; }
	inline Button_t2872111280 ** get_address_of_env3_btn_6() { return &___env3_btn_6; }
	inline void set_env3_btn_6(Button_t2872111280 * value)
	{
		___env3_btn_6 = value;
		Il2CppCodeGenWriteBarrier(&___env3_btn_6, value);
	}

	inline static int32_t get_offset_of_env_farm_7() { return static_cast<int32_t>(offsetof(EnvironmentSelect_t3424254515, ___env_farm_7)); }
	inline EnvironmentListItem_t1261052282 * get_env_farm_7() const { return ___env_farm_7; }
	inline EnvironmentListItem_t1261052282 ** get_address_of_env_farm_7() { return &___env_farm_7; }
	inline void set_env_farm_7(EnvironmentListItem_t1261052282 * value)
	{
		___env_farm_7 = value;
		Il2CppCodeGenWriteBarrier(&___env_farm_7, value);
	}

	inline static int32_t get_offset_of_env_horseShow_8() { return static_cast<int32_t>(offsetof(EnvironmentSelect_t3424254515, ___env_horseShow_8)); }
	inline EnvironmentListItem_t1261052282 * get_env_horseShow_8() const { return ___env_horseShow_8; }
	inline EnvironmentListItem_t1261052282 ** get_address_of_env_horseShow_8() { return &___env_horseShow_8; }
	inline void set_env_horseShow_8(EnvironmentListItem_t1261052282 * value)
	{
		___env_horseShow_8 = value;
		Il2CppCodeGenWriteBarrier(&___env_horseShow_8, value);
	}

	inline static int32_t get_offset_of_env_indoor_9() { return static_cast<int32_t>(offsetof(EnvironmentSelect_t3424254515, ___env_indoor_9)); }
	inline EnvironmentListItem_t1261052282 * get_env_indoor_9() const { return ___env_indoor_9; }
	inline EnvironmentListItem_t1261052282 ** get_address_of_env_indoor_9() { return &___env_indoor_9; }
	inline void set_env_indoor_9(EnvironmentListItem_t1261052282 * value)
	{
		___env_indoor_9 = value;
		Il2CppCodeGenWriteBarrier(&___env_indoor_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
