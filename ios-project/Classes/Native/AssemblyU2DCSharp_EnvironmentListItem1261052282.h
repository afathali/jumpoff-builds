﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvironmentListItem
struct  EnvironmentListItem_t1261052282  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject EnvironmentListItem::lockIcon
	GameObject_t1756533147 * ___lockIcon_2;
	// UnityEngine.GameObject EnvironmentListItem::selectedText
	GameObject_t1756533147 * ___selectedText_3;

public:
	inline static int32_t get_offset_of_lockIcon_2() { return static_cast<int32_t>(offsetof(EnvironmentListItem_t1261052282, ___lockIcon_2)); }
	inline GameObject_t1756533147 * get_lockIcon_2() const { return ___lockIcon_2; }
	inline GameObject_t1756533147 ** get_address_of_lockIcon_2() { return &___lockIcon_2; }
	inline void set_lockIcon_2(GameObject_t1756533147 * value)
	{
		___lockIcon_2 = value;
		Il2CppCodeGenWriteBarrier(&___lockIcon_2, value);
	}

	inline static int32_t get_offset_of_selectedText_3() { return static_cast<int32_t>(offsetof(EnvironmentListItem_t1261052282, ___selectedText_3)); }
	inline GameObject_t1756533147 * get_selectedText_3() const { return ___selectedText_3; }
	inline GameObject_t1756533147 ** get_address_of_selectedText_3() { return &___selectedText_3; }
	inline void set_selectedText_3(GameObject_t1756533147 * value)
	{
		___selectedText_3 = value;
		Il2CppCodeGenWriteBarrier(&___selectedText_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
