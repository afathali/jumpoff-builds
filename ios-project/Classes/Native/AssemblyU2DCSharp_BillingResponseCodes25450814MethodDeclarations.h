﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BillingResponseCodes
struct BillingResponseCodes_t25450814;

#include "codegen/il2cpp-codegen.h"

// System.Void BillingResponseCodes::.ctor()
extern "C"  void BillingResponseCodes__ctor_m1578475593 (BillingResponseCodes_t25450814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
