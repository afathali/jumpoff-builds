﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocialPlatfromSettings
struct SocialPlatfromSettings_t3418207459;

#include "codegen/il2cpp-codegen.h"

// System.Void SocialPlatfromSettings::.ctor()
extern "C"  void SocialPlatfromSettings__ctor_m217715046 (SocialPlatfromSettings_t3418207459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocialPlatfromSettings::.cctor()
extern "C"  void SocialPlatfromSettings__cctor_m3824262005 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocialPlatfromSettings SocialPlatfromSettings::get_Instance()
extern "C"  SocialPlatfromSettings_t3418207459 * SocialPlatfromSettings_get_Instance_m2217924876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocialPlatfromSettings::AddDefaultScopes()
extern "C"  void SocialPlatfromSettings_AddDefaultScopes_m3345250901 (SocialPlatfromSettings_t3418207459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
