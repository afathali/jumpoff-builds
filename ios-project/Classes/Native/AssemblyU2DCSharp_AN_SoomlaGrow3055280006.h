﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3226471752;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen51156628.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AN_SoomlaGrow
struct  AN_SoomlaGrow_t3055280006  : public SA_Singleton_OLD_1_t51156628
{
public:

public:
};

struct AN_SoomlaGrow_t3055280006_StaticFields
{
public:
	// System.Boolean AN_SoomlaGrow::_IsInitialized
	bool ____IsInitialized_4;
	// System.Action AN_SoomlaGrow::ActionInitialized
	Action_t3226471752 * ___ActionInitialized_5;
	// System.Action AN_SoomlaGrow::ActionConnected
	Action_t3226471752 * ___ActionConnected_6;
	// System.Action AN_SoomlaGrow::ActionDisconnected
	Action_t3226471752 * ___ActionDisconnected_7;
	// System.Action AN_SoomlaGrow::<>f__am$cache4
	Action_t3226471752 * ___U3CU3Ef__amU24cache4_8;
	// System.Action AN_SoomlaGrow::<>f__am$cache5
	Action_t3226471752 * ___U3CU3Ef__amU24cache5_9;
	// System.Action AN_SoomlaGrow::<>f__am$cache6
	Action_t3226471752 * ___U3CU3Ef__amU24cache6_10;

public:
	inline static int32_t get_offset_of__IsInitialized_4() { return static_cast<int32_t>(offsetof(AN_SoomlaGrow_t3055280006_StaticFields, ____IsInitialized_4)); }
	inline bool get__IsInitialized_4() const { return ____IsInitialized_4; }
	inline bool* get_address_of__IsInitialized_4() { return &____IsInitialized_4; }
	inline void set__IsInitialized_4(bool value)
	{
		____IsInitialized_4 = value;
	}

	inline static int32_t get_offset_of_ActionInitialized_5() { return static_cast<int32_t>(offsetof(AN_SoomlaGrow_t3055280006_StaticFields, ___ActionInitialized_5)); }
	inline Action_t3226471752 * get_ActionInitialized_5() const { return ___ActionInitialized_5; }
	inline Action_t3226471752 ** get_address_of_ActionInitialized_5() { return &___ActionInitialized_5; }
	inline void set_ActionInitialized_5(Action_t3226471752 * value)
	{
		___ActionInitialized_5 = value;
		Il2CppCodeGenWriteBarrier(&___ActionInitialized_5, value);
	}

	inline static int32_t get_offset_of_ActionConnected_6() { return static_cast<int32_t>(offsetof(AN_SoomlaGrow_t3055280006_StaticFields, ___ActionConnected_6)); }
	inline Action_t3226471752 * get_ActionConnected_6() const { return ___ActionConnected_6; }
	inline Action_t3226471752 ** get_address_of_ActionConnected_6() { return &___ActionConnected_6; }
	inline void set_ActionConnected_6(Action_t3226471752 * value)
	{
		___ActionConnected_6 = value;
		Il2CppCodeGenWriteBarrier(&___ActionConnected_6, value);
	}

	inline static int32_t get_offset_of_ActionDisconnected_7() { return static_cast<int32_t>(offsetof(AN_SoomlaGrow_t3055280006_StaticFields, ___ActionDisconnected_7)); }
	inline Action_t3226471752 * get_ActionDisconnected_7() const { return ___ActionDisconnected_7; }
	inline Action_t3226471752 ** get_address_of_ActionDisconnected_7() { return &___ActionDisconnected_7; }
	inline void set_ActionDisconnected_7(Action_t3226471752 * value)
	{
		___ActionDisconnected_7 = value;
		Il2CppCodeGenWriteBarrier(&___ActionDisconnected_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(AN_SoomlaGrow_t3055280006_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_9() { return static_cast<int32_t>(offsetof(AN_SoomlaGrow_t3055280006_StaticFields, ___U3CU3Ef__amU24cache5_9)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache5_9() const { return ___U3CU3Ef__amU24cache5_9; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache5_9() { return &___U3CU3Ef__amU24cache5_9; }
	inline void set_U3CU3Ef__amU24cache5_9(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache5_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_10() { return static_cast<int32_t>(offsetof(AN_SoomlaGrow_t3055280006_StaticFields, ___U3CU3Ef__amU24cache6_10)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache6_10() const { return ___U3CU3Ef__amU24cache6_10; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache6_10() { return &___U3CU3Ef__amU24cache6_10; }
	inline void set_U3CU3Ef__amU24cache6_10(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache6_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
