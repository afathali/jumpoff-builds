﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase
struct SampleBase_t2925764113;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase::.ctor()
extern "C"  void SampleBase__ctor_m280805186 (SampleBase_t2925764113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase::get_TextValue()
extern "C"  String_t* SampleBase_get_TextValue_m1443252730 (SampleBase_t2925764113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase::set_TextValue(System.String)
extern "C"  void SampleBase_set_TextValue_m2593017983 (SampleBase_t2925764113 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase::get_NumberValue()
extern "C"  int32_t SampleBase_get_NumberValue_m3126990029 (SampleBase_t2925764113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase::set_NumberValue(System.Int32)
extern "C"  void SampleBase_set_NumberValue_m336961514 (SampleBase_t2925764113 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase::get_VectorValue()
extern "C"  Vector3_t2243707580  SampleBase_get_VectorValue_m4043518339 (SampleBase_t2925764113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase::set_VectorValue(UnityEngine.Vector3)
extern "C"  void SampleBase_set_VectorValue_m4040561878 (SampleBase_t2925764113 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
