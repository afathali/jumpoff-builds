﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookAnalyticsExample
struct FacebookAnalyticsExample_t2074400518;

#include "codegen/il2cpp-codegen.h"

// System.Void FacebookAnalyticsExample::.ctor()
extern "C"  void FacebookAnalyticsExample__ctor_m1076236613 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAnalyticsExample::ActivateApp()
extern "C"  void FacebookAnalyticsExample_ActivateApp_m2330996221 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAnalyticsExample::AchievedLevel()
extern "C"  void FacebookAnalyticsExample_AchievedLevel_m2209361728 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAnalyticsExample::AddedPaymentInfo()
extern "C"  void FacebookAnalyticsExample_AddedPaymentInfo_m2087952713 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAnalyticsExample::AddedToCart()
extern "C"  void FacebookAnalyticsExample_AddedToCart_m3375420976 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAnalyticsExample::AddedToWishlist()
extern "C"  void FacebookAnalyticsExample_AddedToWishlist_m1124668761 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAnalyticsExample::CompletedRegistration()
extern "C"  void FacebookAnalyticsExample_CompletedRegistration_m2139932837 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAnalyticsExample::InitiatedCheckout()
extern "C"  void FacebookAnalyticsExample_InitiatedCheckout_m1624619464 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAnalyticsExample::Purchased()
extern "C"  void FacebookAnalyticsExample_Purchased_m714802112 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAnalyticsExample::Rated()
extern "C"  void FacebookAnalyticsExample_Rated_m3360992183 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAnalyticsExample::Searched()
extern "C"  void FacebookAnalyticsExample_Searched_m4067947488 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAnalyticsExample::SpentCredits()
extern "C"  void FacebookAnalyticsExample_SpentCredits_m409281423 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAnalyticsExample::UnlockedAchievement()
extern "C"  void FacebookAnalyticsExample_UnlockedAchievement_m687323579 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAnalyticsExample::ViewedContent()
extern "C"  void FacebookAnalyticsExample_ViewedContent_m1902127980 (FacebookAnalyticsExample_t2074400518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
