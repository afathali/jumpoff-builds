﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MCGBehaviour/Component
struct Component_t1531646212;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MCGBehaviour/InjectionContainer/<Register>c__AnonStorey3A
struct  U3CRegisterU3Ec__AnonStorey3A_t3929677068  : public Il2CppObject
{
public:
	// MCGBehaviour/Component MCGBehaviour/InjectionContainer/<Register>c__AnonStorey3A::component
	Component_t1531646212 * ___component_0;

public:
	inline static int32_t get_offset_of_component_0() { return static_cast<int32_t>(offsetof(U3CRegisterU3Ec__AnonStorey3A_t3929677068, ___component_0)); }
	inline Component_t1531646212 * get_component_0() const { return ___component_0; }
	inline Component_t1531646212 ** get_address_of_component_0() { return &___component_0; }
	inline void set_component_0(Component_t1531646212 * value)
	{
		___component_0 = value;
		Il2CppCodeGenWriteBarrier(&___component_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
