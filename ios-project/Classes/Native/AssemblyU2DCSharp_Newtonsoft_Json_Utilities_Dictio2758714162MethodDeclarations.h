﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Dictio2758714162.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TEnumeratorKey,TEnumeratorValue>>)
extern "C"  void DictionaryEnumerator_2__ctor_m2268843174_gshared (DictionaryEnumerator_2_t2758714162 * __this, Il2CppObject* ___e0, const MethodInfo* method);
#define DictionaryEnumerator_2__ctor_m2268843174(__this, ___e0, method) ((  void (*) (DictionaryEnumerator_2_t2758714162 *, Il2CppObject*, const MethodInfo*))DictionaryEnumerator_2__ctor_m2268843174_gshared)(__this, ___e0, method)
// System.Collections.DictionaryEntry Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  DictionaryEnumerator_2_get_Entry_m4176687428_gshared (DictionaryEnumerator_2_t2758714162 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_get_Entry_m4176687428(__this, method) ((  DictionaryEntry_t3048875398  (*) (DictionaryEnumerator_2_t2758714162 *, const MethodInfo*))DictionaryEnumerator_2_get_Entry_m4176687428_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Key_m155013433_gshared (DictionaryEnumerator_2_t2758714162 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_get_Key_m155013433(__this, method) ((  Il2CppObject * (*) (DictionaryEnumerator_2_t2758714162 *, const MethodInfo*))DictionaryEnumerator_2_get_Key_m155013433_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Value_m262458185_gshared (DictionaryEnumerator_2_t2758714162 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_get_Value_m262458185(__this, method) ((  Il2CppObject * (*) (DictionaryEnumerator_2_t2758714162 *, const MethodInfo*))DictionaryEnumerator_2_get_Value_m262458185_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Current_m222679567_gshared (DictionaryEnumerator_2_t2758714162 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_get_Current_m222679567(__this, method) ((  Il2CppObject * (*) (DictionaryEnumerator_2_t2758714162 *, const MethodInfo*))DictionaryEnumerator_2_get_Current_m222679567_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::MoveNext()
extern "C"  bool DictionaryEnumerator_2_MoveNext_m2168681928_gshared (DictionaryEnumerator_2_t2758714162 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_MoveNext_m2168681928(__this, method) ((  bool (*) (DictionaryEnumerator_2_t2758714162 *, const MethodInfo*))DictionaryEnumerator_2_MoveNext_m2168681928_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::Reset()
extern "C"  void DictionaryEnumerator_2_Reset_m3518201151_gshared (DictionaryEnumerator_2_t2758714162 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_Reset_m3518201151(__this, method) ((  void (*) (DictionaryEnumerator_2_t2758714162 *, const MethodInfo*))DictionaryEnumerator_2_Reset_m3518201151_gshared)(__this, method)
