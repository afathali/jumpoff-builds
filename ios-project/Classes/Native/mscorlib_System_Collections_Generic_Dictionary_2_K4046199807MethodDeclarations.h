﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>
struct Dictionary_2_t1356696369;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4046199807.h"
#include "AssemblyU2DCSharp_AN_ManifestPermission1772110202.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m911887285_gshared (Enumerator_t4046199807 * __this, Dictionary_2_t1356696369 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m911887285(__this, ___host0, method) ((  void (*) (Enumerator_t4046199807 *, Dictionary_2_t1356696369 *, const MethodInfo*))Enumerator__ctor_m911887285_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m130813306_gshared (Enumerator_t4046199807 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m130813306(__this, method) ((  Il2CppObject * (*) (Enumerator_t4046199807 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m130813306_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3953624924_gshared (Enumerator_t4046199807 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3953624924(__this, method) ((  void (*) (Enumerator_t4046199807 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3953624924_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::Dispose()
extern "C"  void Enumerator_Dispose_m3584203781_gshared (Enumerator_t4046199807 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3584203781(__this, method) ((  void (*) (Enumerator_t4046199807 *, const MethodInfo*))Enumerator_Dispose_m3584203781_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3013070576_gshared (Enumerator_t4046199807 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3013070576(__this, method) ((  bool (*) (Enumerator_t4046199807 *, const MethodInfo*))Enumerator_MoveNext_m3013070576_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m315949484_gshared (Enumerator_t4046199807 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m315949484(__this, method) ((  int32_t (*) (Enumerator_t4046199807 *, const MethodInfo*))Enumerator_get_Current_m315949484_gshared)(__this, method)
