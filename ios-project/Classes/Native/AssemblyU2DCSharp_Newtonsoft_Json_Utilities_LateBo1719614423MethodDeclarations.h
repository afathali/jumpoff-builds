﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey32`1<System.Object>
struct U3CCreateGetU3Ec__AnonStorey32_1_t1719614423;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey32`1<System.Object>::.ctor()
extern "C"  void U3CCreateGetU3Ec__AnonStorey32_1__ctor_m1450616450_gshared (U3CCreateGetU3Ec__AnonStorey32_1_t1719614423 * __this, const MethodInfo* method);
#define U3CCreateGetU3Ec__AnonStorey32_1__ctor_m1450616450(__this, method) ((  void (*) (U3CCreateGetU3Ec__AnonStorey32_1_t1719614423 *, const MethodInfo*))U3CCreateGetU3Ec__AnonStorey32_1__ctor_m1450616450_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey32`1<System.Object>::<>m__FA(T)
extern "C"  Il2CppObject * U3CCreateGetU3Ec__AnonStorey32_1_U3CU3Em__FA_m2814438295_gshared (U3CCreateGetU3Ec__AnonStorey32_1_t1719614423 * __this, Il2CppObject * ___o0, const MethodInfo* method);
#define U3CCreateGetU3Ec__AnonStorey32_1_U3CU3Em__FA_m2814438295(__this, ___o0, method) ((  Il2CppObject * (*) (U3CCreateGetU3Ec__AnonStorey32_1_t1719614423 *, Il2CppObject *, const MethodInfo*))U3CCreateGetU3Ec__AnonStorey32_1_U3CU3Em__FA_m2814438295_gshared)(__this, ___o0, method)
