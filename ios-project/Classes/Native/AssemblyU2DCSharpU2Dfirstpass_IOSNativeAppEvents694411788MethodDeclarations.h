﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNativeAppEvents
struct IOSNativeAppEvents_t694411788;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"

// System.Void IOSNativeAppEvents::.ctor()
extern "C"  void IOSNativeAppEvents__ctor_m3707312683 (IOSNativeAppEvents_t694411788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::.cctor()
extern "C"  void IOSNativeAppEvents__cctor_m2177585352 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::add_OnApplicationDidEnterBackground(System.Action)
extern "C"  void IOSNativeAppEvents_add_OnApplicationDidEnterBackground_m3368066140 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::remove_OnApplicationDidEnterBackground(System.Action)
extern "C"  void IOSNativeAppEvents_remove_OnApplicationDidEnterBackground_m2811982895 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::add_OnApplicationDidBecomeActive(System.Action)
extern "C"  void IOSNativeAppEvents_add_OnApplicationDidBecomeActive_m4166605189 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::remove_OnApplicationDidBecomeActive(System.Action)
extern "C"  void IOSNativeAppEvents_remove_OnApplicationDidBecomeActive_m2911726044 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::add_OnApplicationDidReceiveMemoryWarning(System.Action)
extern "C"  void IOSNativeAppEvents_add_OnApplicationDidReceiveMemoryWarning_m540380076 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::remove_OnApplicationDidReceiveMemoryWarning(System.Action)
extern "C"  void IOSNativeAppEvents_remove_OnApplicationDidReceiveMemoryWarning_m273321401 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::add_OnApplicationWillResignActive(System.Action)
extern "C"  void IOSNativeAppEvents_add_OnApplicationWillResignActive_m2542041225 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::remove_OnApplicationWillResignActive(System.Action)
extern "C"  void IOSNativeAppEvents_remove_OnApplicationWillResignActive_m729521988 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::add_OnApplicationWillTerminate(System.Action)
extern "C"  void IOSNativeAppEvents_add_OnApplicationWillTerminate_m3919585892 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::remove_OnApplicationWillTerminate(System.Action)
extern "C"  void IOSNativeAppEvents_remove_OnApplicationWillTerminate_m861993335 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::_ISNsubscribe()
extern "C"  void IOSNativeAppEvents__ISNsubscribe_m3383725282 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::Awake()
extern "C"  void IOSNativeAppEvents_Awake_m39554838 (IOSNativeAppEvents_t694411788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::Subscribe()
extern "C"  void IOSNativeAppEvents_Subscribe_m1007059903 (IOSNativeAppEvents_t694411788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::applicationDidEnterBackground()
extern "C"  void IOSNativeAppEvents_applicationDidEnterBackground_m3709688722 (IOSNativeAppEvents_t694411788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::applicationDidBecomeActive()
extern "C"  void IOSNativeAppEvents_applicationDidBecomeActive_m43747335 (IOSNativeAppEvents_t694411788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::applicationDidReceiveMemoryWarning()
extern "C"  void IOSNativeAppEvents_applicationDidReceiveMemoryWarning_m479087864 (IOSNativeAppEvents_t694411788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::applicationWillResignActive()
extern "C"  void IOSNativeAppEvents_applicationWillResignActive_m283769187 (IOSNativeAppEvents_t694411788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::applicationWillTerminate()
extern "C"  void IOSNativeAppEvents_applicationWillTerminate_m3623868136 (IOSNativeAppEvents_t694411788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::<OnApplicationDidEnterBackground>m__58()
extern "C"  void IOSNativeAppEvents_U3COnApplicationDidEnterBackgroundU3Em__58_m415612117 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::<OnApplicationDidBecomeActive>m__59()
extern "C"  void IOSNativeAppEvents_U3COnApplicationDidBecomeActiveU3Em__59_m880858185 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::<OnApplicationDidReceiveMemoryWarning>m__5A()
extern "C"  void IOSNativeAppEvents_U3COnApplicationDidReceiveMemoryWarningU3Em__5A_m4083229716 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::<OnApplicationWillResignActive>m__5B()
extern "C"  void IOSNativeAppEvents_U3COnApplicationWillResignActiveU3Em__5B_m2003220306 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeAppEvents::<OnApplicationWillTerminate>m__5C()
extern "C"  void IOSNativeAppEvents_U3COnApplicationWillTerminateU3Em__5C_m1770910442 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
