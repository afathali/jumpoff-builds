﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSSocialUseExample/<PostScreenshot>c__Iterator4
struct U3CPostScreenshotU3Ec__Iterator4_t1020461047;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSSocialUseExample/<PostScreenshot>c__Iterator4::.ctor()
extern "C"  void U3CPostScreenshotU3Ec__Iterator4__ctor_m4224744326 (U3CPostScreenshotU3Ec__Iterator4_t1020461047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSSocialUseExample/<PostScreenshot>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPostScreenshotU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1616063942 (U3CPostScreenshotU3Ec__Iterator4_t1020461047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSSocialUseExample/<PostScreenshot>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPostScreenshotU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3194295486 (U3CPostScreenshotU3Ec__Iterator4_t1020461047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSSocialUseExample/<PostScreenshot>c__Iterator4::MoveNext()
extern "C"  bool U3CPostScreenshotU3Ec__Iterator4_MoveNext_m18319010 (U3CPostScreenshotU3Ec__Iterator4_t1020461047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample/<PostScreenshot>c__Iterator4::Dispose()
extern "C"  void U3CPostScreenshotU3Ec__Iterator4_Dispose_m124866663 (U3CPostScreenshotU3Ec__Iterator4_t1020461047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample/<PostScreenshot>c__Iterator4::Reset()
extern "C"  void U3CPostScreenshotU3Ec__Iterator4_Reset_m2539316989 (U3CPostScreenshotU3Ec__Iterator4_t1020461047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
