﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2065790391MethodDeclarations.h"

// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Reflection.ParameterInfo,System.Object>,System.String>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1786978096(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3589153805 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1492231369_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Reflection.ParameterInfo,System.Object>,System.String>::Invoke(T)
#define Func_2_Invoke_m2401558356(__this, ___arg10, method) ((  String_t* (*) (Func_2_t3589153805 *, KeyValuePair_2_t2995057417 , const MethodInfo*))Func_2_Invoke_m1587848815_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Reflection.ParameterInfo,System.Object>,System.String>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m3392829293(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3589153805 *, KeyValuePair_2_t2995057417 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2659573344_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Reflection.ParameterInfo,System.Object>,System.String>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2758906010(__this, ___result0, method) ((  String_t* (*) (Func_2_t3589153805 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m3552534409_gshared)(__this, ___result0, method)
