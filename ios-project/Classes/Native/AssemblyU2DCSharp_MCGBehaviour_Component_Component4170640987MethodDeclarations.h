﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MCGBehaviour/Component/ComponentLifestyle
struct ComponentLifestyle_t4170640987;
// MCGBehaviour/Component
struct Component_t1531646212;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MCGBehaviour_Component1531646212.h"
#include "AssemblyU2DCSharp_MCGBehaviour_LifestyleType936921768.h"

// System.Void MCGBehaviour/Component/ComponentLifestyle::.ctor(MCGBehaviour/Component)
extern "C"  void ComponentLifestyle__ctor_m1280700214 (ComponentLifestyle_t4170640987 * __this, Component_t1531646212 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MCGBehaviour/Component MCGBehaviour/Component/ComponentLifestyle::Is(MCGBehaviour/LifestyleType)
extern "C"  Component_t1531646212 * ComponentLifestyle_Is_m3884407737 (ComponentLifestyle_t4170640987 * __this, int32_t ___lifestyleType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
