﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t2919945039;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Object
struct Il2CppObject;
// MainPage_Controller
struct MainPage_Controller_t1135564517;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainPage_Controller/<GetBannerImage>c__Iterator16
struct  U3CGetBannerImageU3Ec__Iterator16_t1818337539  : public Il2CppObject
{
public:
	// UnityEngine.WWW MainPage_Controller/<GetBannerImage>c__Iterator16::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_0;
	// UnityEngine.Texture2D MainPage_Controller/<GetBannerImage>c__Iterator16::<tex>__1
	Texture2D_t3542995729 * ___U3CtexU3E__1_1;
	// System.Int32 MainPage_Controller/<GetBannerImage>c__Iterator16::$PC
	int32_t ___U24PC_2;
	// System.Object MainPage_Controller/<GetBannerImage>c__Iterator16::$current
	Il2CppObject * ___U24current_3;
	// MainPage_Controller MainPage_Controller/<GetBannerImage>c__Iterator16::<>f__this
	MainPage_Controller_t1135564517 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetBannerImageU3Ec__Iterator16_t1818337539, ___U3CwwwU3E__0_0)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CtexU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetBannerImageU3Ec__Iterator16_t1818337539, ___U3CtexU3E__1_1)); }
	inline Texture2D_t3542995729 * get_U3CtexU3E__1_1() const { return ___U3CtexU3E__1_1; }
	inline Texture2D_t3542995729 ** get_address_of_U3CtexU3E__1_1() { return &___U3CtexU3E__1_1; }
	inline void set_U3CtexU3E__1_1(Texture2D_t3542995729 * value)
	{
		___U3CtexU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtexU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CGetBannerImageU3Ec__Iterator16_t1818337539, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetBannerImageU3Ec__Iterator16_t1818337539, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CGetBannerImageU3Ec__Iterator16_t1818337539, ___U3CU3Ef__this_4)); }
	inline MainPage_Controller_t1135564517 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline MainPage_Controller_t1135564517 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(MainPage_Controller_t1135564517 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
