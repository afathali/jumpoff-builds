﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3980293213MethodDeclarations.h"

// System.Void SA_Singleton_OLD`1<AndroidInstagramManager>::.ctor()
#define SA_Singleton_OLD_1__ctor_m213290318(__this, method) ((  void (*) (SA_Singleton_OLD_1_t4050051854 *, const MethodInfo*))SA_Singleton_OLD_1__ctor_m2643683696_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<AndroidInstagramManager>::.cctor()
#define SA_Singleton_OLD_1__cctor_m2800810653(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1__cctor_m1355192463_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<AndroidInstagramManager>::get_instance()
#define SA_Singleton_OLD_1_get_instance_m2205380757(__this /* static, unused */, method) ((  AndroidInstagramManager_t2759207936 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_instance_m1963118739_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<AndroidInstagramManager>::get_Instance()
#define SA_Singleton_OLD_1_get_Instance_m1200699445(__this /* static, unused */, method) ((  AndroidInstagramManager_t2759207936 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_Instance_m2967800051_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<AndroidInstagramManager>::get_HasInstance()
#define SA_Singleton_OLD_1_get_HasInstance_m2900529812(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_HasInstance_m4184706798_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<AndroidInstagramManager>::get_IsDestroyed()
#define SA_Singleton_OLD_1_get_IsDestroyed_m1811194400(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_IsDestroyed_m1648274602_gshared)(__this /* static, unused */, method)
// System.Void SA_Singleton_OLD`1<AndroidInstagramManager>::OnDestroy()
#define SA_Singleton_OLD_1_OnDestroy_m4139761633(__this, method) ((  void (*) (SA_Singleton_OLD_1_t4050051854 *, const MethodInfo*))SA_Singleton_OLD_1_OnDestroy_m1680414419_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<AndroidInstagramManager>::OnApplicationQuit()
#define SA_Singleton_OLD_1_OnApplicationQuit_m2386647888(__this, method) ((  void (*) (SA_Singleton_OLD_1_t4050051854 *, const MethodInfo*))SA_Singleton_OLD_1_OnApplicationQuit_m1864960902_gshared)(__this, method)
