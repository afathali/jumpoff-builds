﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_GMSQuestsEventsProxy
struct AN_GMSQuestsEventsProxy_t3325636741;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_GMSQuestsEventsProxy::.ctor()
extern "C"  void AN_GMSQuestsEventsProxy__ctor_m2302814828 (AN_GMSQuestsEventsProxy_t3325636741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSQuestsEventsProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_GMSQuestsEventsProxy_CallActivityFunction_m1148826231 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSQuestsEventsProxy::sumbitEvent(System.String,System.Int32)
extern "C"  void AN_GMSQuestsEventsProxy_sumbitEvent_m1310783335 (Il2CppObject * __this /* static, unused */, String_t* ___eventId0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSQuestsEventsProxy::loadEvents()
extern "C"  void AN_GMSQuestsEventsProxy_loadEvents_m1935058097 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSQuestsEventsProxy::showSelectedQuests(System.String)
extern "C"  void AN_GMSQuestsEventsProxy_showSelectedQuests_m2564215645 (Il2CppObject * __this /* static, unused */, String_t* ___questSelectors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSQuestsEventsProxy::acceptQuest(System.String)
extern "C"  void AN_GMSQuestsEventsProxy_acceptQuest_m169952132 (Il2CppObject * __this /* static, unused */, String_t* ___questId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSQuestsEventsProxy::loadQuests(System.String,System.Int32)
extern "C"  void AN_GMSQuestsEventsProxy_loadQuests_m1083373184 (Il2CppObject * __this /* static, unused */, String_t* ___questSelectors0, int32_t ___sortOrder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
