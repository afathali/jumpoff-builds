﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.EnumValues`1<System.Int64>
struct EnumValues_1_t1738794714;
// System.String
struct String_t;
// Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>
struct EnumValue_1_t589082027;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.EnumValues`1<System.Int64>::.ctor()
extern "C"  void EnumValues_1__ctor_m4226035808_gshared (EnumValues_1_t1738794714 * __this, const MethodInfo* method);
#define EnumValues_1__ctor_m4226035808(__this, method) ((  void (*) (EnumValues_1_t1738794714 *, const MethodInfo*))EnumValues_1__ctor_m4226035808_gshared)(__this, method)
// System.String Newtonsoft.Json.Utilities.EnumValues`1<System.Int64>::GetKeyForItem(Newtonsoft.Json.Utilities.EnumValue`1<T>)
extern "C"  String_t* EnumValues_1_GetKeyForItem_m1915937120_gshared (EnumValues_1_t1738794714 * __this, EnumValue_1_t589082027 * ___item0, const MethodInfo* method);
#define EnumValues_1_GetKeyForItem_m1915937120(__this, ___item0, method) ((  String_t* (*) (EnumValues_1_t1738794714 *, EnumValue_1_t589082027 *, const MethodInfo*))EnumValues_1_GetKeyForItem_m1915937120_gshared)(__this, ___item0, method)
