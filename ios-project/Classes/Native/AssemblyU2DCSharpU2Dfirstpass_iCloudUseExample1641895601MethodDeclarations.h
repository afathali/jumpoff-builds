﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iCloudUseExample
struct iCloudUseExample_t1641895601;
// System.Collections.Generic.List`1<iCloudData>
struct List_1_t2449758620;
// SA.Common.Models.Result
struct Result_t4287219743;
// iCloudData
struct iCloudData_t3080637488;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iCloudData3080637488.h"

// System.Void iCloudUseExample::.ctor()
extern "C"  void iCloudUseExample__ctor_m358611168 (iCloudUseExample_t1641895601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudUseExample::Awake()
extern "C"  void iCloudUseExample_Awake_m2248399739 (iCloudUseExample_t1641895601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudUseExample::HandleOnStoreDidChangeExternally(System.Collections.Generic.List`1<iCloudData>)
extern "C"  void iCloudUseExample_HandleOnStoreDidChangeExternally_m4212592015 (iCloudUseExample_t1641895601 * __this, List_1_t2449758620 * ___changedData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudUseExample::OnGUI()
extern "C"  void iCloudUseExample_OnGUI_m1912061372 (iCloudUseExample_t1641895601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudUseExample::OnCloundInitAction(SA.Common.Models.Result)
extern "C"  void iCloudUseExample_OnCloundInitAction_m231512384 (iCloudUseExample_t1641895601 * __this, Result_t4287219743 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudUseExample::OnCloudDataReceivedAction(iCloudData)
extern "C"  void iCloudUseExample_OnCloudDataReceivedAction_m2411758091 (iCloudUseExample_t1641895601 * __this, iCloudData_t3080637488 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudUseExample::OnDestroy()
extern "C"  void iCloudUseExample_OnDestroy_m2970403301 (iCloudUseExample_t1641895601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
