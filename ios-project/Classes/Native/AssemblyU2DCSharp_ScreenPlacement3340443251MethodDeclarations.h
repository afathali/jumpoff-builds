﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenPlacement
struct ScreenPlacement_t3340443251;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenPlacement::.ctor()
extern "C"  void ScreenPlacement__ctor_m1115859946 (ScreenPlacement_t3340443251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenPlacement::Start()
extern "C"  void ScreenPlacement_Start_m3241896398 (ScreenPlacement_t3340443251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenPlacement::FixedUpdate()
extern "C"  void ScreenPlacement_FixedUpdate_m4082621757 (ScreenPlacement_t3340443251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenPlacement::placementCalculation()
extern "C"  void ScreenPlacement_placementCalculation_m157997628 (ScreenPlacement_t3340443251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
