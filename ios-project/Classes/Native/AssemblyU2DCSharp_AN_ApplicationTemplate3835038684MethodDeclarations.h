﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_ApplicationTemplate
struct AN_ApplicationTemplate_t3835038684;
// AN_ActivityTemplate
struct AN_ActivityTemplate_t3380616875;
// System.String
struct String_t;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// System.Collections.Generic.Dictionary`2<System.Int32,AN_ActivityTemplate>
struct Dictionary_2_t2388442510;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AN_ActivityTemplate3380616875.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void AN_ApplicationTemplate::.ctor()
extern "C"  void AN_ApplicationTemplate__ctor_m1210915757 (AN_ApplicationTemplate_t3835038684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ApplicationTemplate::AddActivity(AN_ActivityTemplate)
extern "C"  void AN_ApplicationTemplate_AddActivity_m1351857412 (AN_ApplicationTemplate_t3835038684 * __this, AN_ActivityTemplate_t3380616875 * ___activity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ApplicationTemplate::RemoveActivity(AN_ActivityTemplate)
extern "C"  void AN_ApplicationTemplate_RemoveActivity_m520602943 (AN_ApplicationTemplate_t3835038684 * __this, AN_ActivityTemplate_t3380616875 * ___activity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AN_ActivityTemplate AN_ApplicationTemplate::GetOrCreateActivityWithName(System.String)
extern "C"  AN_ActivityTemplate_t3380616875 * AN_ApplicationTemplate_GetOrCreateActivityWithName_m1685129218 (AN_ApplicationTemplate_t3835038684 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AN_ActivityTemplate AN_ApplicationTemplate::GetActivityWithName(System.String)
extern "C"  AN_ActivityTemplate_t3380616875 * AN_ApplicationTemplate_GetActivityWithName_m499065815 (AN_ApplicationTemplate_t3835038684 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AN_ActivityTemplate AN_ApplicationTemplate::GetLauncherActivity()
extern "C"  AN_ActivityTemplate_t3380616875 * AN_ApplicationTemplate_GetLauncherActivity_m2925249026 (AN_ApplicationTemplate_t3835038684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ApplicationTemplate::ToXmlElement(System.Xml.XmlDocument,System.Xml.XmlElement)
extern "C"  void AN_ApplicationTemplate_ToXmlElement_m3468091352 (AN_ApplicationTemplate_t3835038684 * __this, XmlDocument_t3649534162 * ___doc0, XmlElement_t2877111883 * ___parent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,AN_ActivityTemplate> AN_ApplicationTemplate::get_Activities()
extern "C"  Dictionary_2_t2388442510 * AN_ApplicationTemplate_get_Activities_m3289623991 (AN_ApplicationTemplate_t3835038684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
