﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SavedGamesExample916988576.h"
#include "AssemblyU2DCSharp_SavedGamesExample_U3CMakeScreens1318283061.h"
#include "AssemblyU2DCSharp_AndroidSocialNativeExample1704557771.h"
#include "AssemblyU2DCSharp_AndroidSocialNativeExample_U3CPos559519129.h"
#include "AssemblyU2DCSharp_AndroidSocialNativeExample_U3CPos860751190.h"
#include "AssemblyU2DCSharp_FacebookAnalyticsExample2074400518.h"
#include "AssemblyU2DCSharp_FacebookAndroidTurnBasedAndGiftsEx99045661.h"
#include "AssemblyU2DCSharp_FacebookAndroidUseExample2072023080.h"
#include "AssemblyU2DCSharp_FacebookAndroidUseExample_U3CPos3249146850.h"
#include "AssemblyU2DCSharp_FacebookAndroidUseExample_U3CPos1609060767.h"
#include "AssemblyU2DCSharp_TwitterAndroidUseExample3716461687.h"
#include "AssemblyU2DCSharp_TwitterAndroidUseExample_U3CPost2038777620.h"
#include "AssemblyU2DCSharp_TwitterAndroidUseExample_U3CPost3015627852.h"
#include "AssemblyU2DCSharp_AndroidAdMobController1638300356.h"
#include "AssemblyU2DCSharp_AN_GoogleAdProxy3994141898.h"
#include "AssemblyU2DCSharp_AN_PoupsProxy2977617695.h"
#include "AssemblyU2DCSharp_AN_ProxyPool3556146334.h"
#include "AssemblyU2DCSharp_AN_AppInvitesProxy250409945.h"
#include "AssemblyU2DCSharp_AN_BillingProxy3904793637.h"
#include "AssemblyU2DCSharp_AN_FBProxy1438007924.h"
#include "AssemblyU2DCSharp_AN_GMSGeneralProxy49831053.h"
#include "AssemblyU2DCSharp_AN_GMSGiftsProxy1705430956.h"
#include "AssemblyU2DCSharp_AN_GMSInvitationProxy2905962638.h"
#include "AssemblyU2DCSharp_AN_GMSQuestsEventsProxy3325636741.h"
#include "AssemblyU2DCSharp_AN_GMSRTMProxy1149422734.h"
#include "AssemblyU2DCSharp_AN_GoogleAnalyticsProxy595315245.h"
#include "AssemblyU2DCSharp_AN_GooglePlayUtilsProxy535695022.h"
#include "AssemblyU2DCSharp_AN_ImmersiveModeProxy2791662232.h"
#include "AssemblyU2DCSharp_AN_LicenseManagerProxy1907271486.h"
#include "AssemblyU2DCSharp_AN_NotificationProxy3523749487.h"
#include "AssemblyU2DCSharp_AN_PlusButtonProxy2040058752.h"
#include "AssemblyU2DCSharp_AN_SocialSharingProxy924445897.h"
#include "AssemblyU2DCSharp_AN_TVControllerProxy3771858466.h"
#include "AssemblyU2DCSharp_AndroidNative876365532.h"
#include "AssemblyU2DCSharp_AndroidNativeSettings3397157021.h"
#include "AssemblyU2DCSharp_SocialPlatfromSettings3418207459.h"
#include "AssemblyU2DCSharp_AdroidActivityResultCodes2848269969.h"
#include "AssemblyU2DCSharp_AndroidGravity1500788099.h"
#include "AssemblyU2DCSharp_AndroidLogLevel2302792355.h"
#include "AssemblyU2DCSharp_AndroidMonth1370057857.h"
#include "AssemblyU2DCSharp_GoogleCloudSlot3383174108.h"
#include "AssemblyU2DCSharp_GoogleGender966451998.h"
#include "AssemblyU2DCSharp_GoogleGravity2146990713.h"
#include "AssemblyU2DCSharp_GADBannerSize1988135029.h"
#include "AssemblyU2DCSharp_GADInAppResolution612781804.h"
#include "AssemblyU2DCSharp_GPAchievementState819112501.h"
#include "AssemblyU2DCSharp_GPAchievementType785407574.h"
#include "AssemblyU2DCSharp_GPBoardTimeSpan42003024.h"
#include "AssemblyU2DCSharp_GPCollectionType2617299399.h"
#include "AssemblyU2DCSharp_GPConnectionState647346880.h"
#include "AssemblyU2DCSharp_GPGameRequestType1088795774.h"
#include "AssemblyU2DCSharp_GPLogLevel1212391055.h"
#include "AssemblyU2DCSharp_GP_AppStateStatusCodes2689651680.h"
#include "AssemblyU2DCSharp_GP_ConnectionResultCode3996068124.h"
#include "AssemblyU2DCSharp_GP_GamesActivityResultCodes70181657.h"
#include "AssemblyU2DCSharp_GP_GamesStatusCodes1013506173.h"
#include "AssemblyU2DCSharp_GP_InvitationType3295179663.h"
#include "AssemblyU2DCSharp_GP_MatchesSortOrder1055105683.h"
#include "AssemblyU2DCSharp_GP_RTM_MessageType1912197865.h"
#include "AssemblyU2DCSharp_GP_RTM_PackageType1787524174.h"
#include "AssemblyU2DCSharp_GP_RTM_ParticipantStatus2642611273.h"
#include "AssemblyU2DCSharp_GP_RTM_RoomStatus2051988161.h"
#include "AssemblyU2DCSharp_GP_QuestSortOrder4026176042.h"
#include "AssemblyU2DCSharp_GP_QuestState1072114879.h"
#include "AssemblyU2DCSharp_GP_QuestsSelect2071002355.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchStatus2102223509.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchTurnStatus2221730550.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchesSortOrder3142286675.h"
#include "AssemblyU2DCSharp_GP_TBM_ParticipantResult1952842762.h"
#include "AssemblyU2DCSharp_FacebookEvents1690892297.h"
#include "AssemblyU2DCSharp_GoogleMobileAdEvents1054095093.h"
#include "AssemblyU2DCSharp_TwitterEvents3891486642.h"
#include "AssemblyU2DCSharp_AndroidADBanner886219444.h"
#include "AssemblyU2DCSharp_AndroidActivityResult3757510801.h"
#include "AssemblyU2DCSharp_AndroidGameActivityResult1444576507.h"
#include "AssemblyU2DCSharp_AsyncTask1407929465.h"
#include "AssemblyU2DCSharp_GADBannerIdFactory603853557.h"
#include "AssemblyU2DCSharp_GPAchievement4279788054.h"
#include "AssemblyU2DCSharp_GPGameRequest4158842942.h"
#include "AssemblyU2DCSharp_GPLeaderBoard3649577886.h"
#include "AssemblyU2DCSharp_GPScoreCollection2240243855.h"
#include "AssemblyU2DCSharp_GP_Invite626929087.h"
#include "AssemblyU2DCSharp_GP_LocalPlayerScoreUpdateListener158126391.h"
#include "AssemblyU2DCSharp_GP_Participant2884377673.h"
#include "AssemblyU2DCSharp_GP_ParticipantResult2469018720.h"
#include "AssemblyU2DCSharp_GP_RTM_Network_Package2050307869.h"
#include "AssemblyU2DCSharp_GP_RTM_ReliableMessageDeliveredR1694070004.h"
#include "AssemblyU2DCSharp_GP_RTM_ReliableMessageListener1343560679.h"
#include "AssemblyU2DCSharp_GP_RTM_ReliableMessageSentResult2743629396.h"
#include "AssemblyU2DCSharp_GP_RTM_Result473289279.h"
#include "AssemblyU2DCSharp_GP_RTM_Room851604529.h"
#include "AssemblyU2DCSharp_GoogleCloudResult743628707.h"
#include "AssemblyU2DCSharp_GooglePlayConnectionResult2758718724.h"
#include "AssemblyU2DCSharp_GooglePlayGiftRequestResult350202007.h"
#include "AssemblyU2DCSharp_GooglePlayResult3097469636.h"
#include "AssemblyU2DCSharp_GP_Event323143668.h"
#include "AssemblyU2DCSharp_GP_Quest1641883470.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (SavedGamesExample_t916988576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[5] = 
{
	SavedGamesExample_t916988576::get_offset_of_avatar_2(),
	SavedGamesExample_t916988576::get_offset_of_defaulttexture_3(),
	SavedGamesExample_t916988576::get_offset_of_connectButton_4(),
	SavedGamesExample_t916988576::get_offset_of_playerLabel_5(),
	SavedGamesExample_t916988576::get_offset_of_ConnectionDependedntButtons_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2601[9] = 
{
	U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061::get_offset_of_U3CwidthU3E__0_0(),
	U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061::get_offset_of_U3CheightU3E__1_1(),
	U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061::get_offset_of_U3CScreenshotU3E__2_2(),
	U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061::get_offset_of_U3CTotalPlayedTimeU3E__3_3(),
	U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061::get_offset_of_U3CcurrentSaveNameU3E__4_4(),
	U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061::get_offset_of_U3CdescriptionU3E__5_5(),
	U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061::get_offset_of_U24PC_6(),
	U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061::get_offset_of_U24current_7(),
	U3CMakeScreenshotAndSaveGameDataU3Ec__Iterator1_t1318283061::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (AndroidSocialNativeExample_t1704557771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2602[1] = 
{
	AndroidSocialNativeExample_t1704557771::get_offset_of_shareTexture_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (U3CPostScreenshotU3Ec__Iterator2_t559519129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2603[5] = 
{
	U3CPostScreenshotU3Ec__Iterator2_t559519129::get_offset_of_U3CwidthU3E__0_0(),
	U3CPostScreenshotU3Ec__Iterator2_t559519129::get_offset_of_U3CheightU3E__1_1(),
	U3CPostScreenshotU3Ec__Iterator2_t559519129::get_offset_of_U3CtexU3E__2_2(),
	U3CPostScreenshotU3Ec__Iterator2_t559519129::get_offset_of_U24PC_3(),
	U3CPostScreenshotU3Ec__Iterator2_t559519129::get_offset_of_U24current_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (U3CPostFBScreenshotU3Ec__Iterator3_t860751190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[5] = 
{
	U3CPostFBScreenshotU3Ec__Iterator3_t860751190::get_offset_of_U3CwidthU3E__0_0(),
	U3CPostFBScreenshotU3Ec__Iterator3_t860751190::get_offset_of_U3CheightU3E__1_1(),
	U3CPostFBScreenshotU3Ec__Iterator3_t860751190::get_offset_of_U3CtexU3E__2_2(),
	U3CPostFBScreenshotU3Ec__Iterator3_t860751190::get_offset_of_U24PC_3(),
	U3CPostFBScreenshotU3Ec__Iterator3_t860751190::get_offset_of_U24current_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (FacebookAnalyticsExample_t2074400518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (FacebookAndroidTurnBasedAndGiftsExample_t99045661), -1, sizeof(FacebookAndroidTurnBasedAndGiftsExample_t99045661_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2606[10] = 
{
	FacebookAndroidTurnBasedAndGiftsExample_t99045661_StaticFields::get_offset_of_IsUserInfoLoaded_2(),
	FacebookAndroidTurnBasedAndGiftsExample_t99045661_StaticFields::get_offset_of_IsAuntificated_3(),
	FacebookAndroidTurnBasedAndGiftsExample_t99045661::get_offset_of_ConnectionDependedntButtons_4(),
	FacebookAndroidTurnBasedAndGiftsExample_t99045661::get_offset_of_connectButton_5(),
	FacebookAndroidTurnBasedAndGiftsExample_t99045661::get_offset_of_avatar_6(),
	FacebookAndroidTurnBasedAndGiftsExample_t99045661::get_offset_of_Location_7(),
	FacebookAndroidTurnBasedAndGiftsExample_t99045661::get_offset_of_Language_8(),
	FacebookAndroidTurnBasedAndGiftsExample_t99045661::get_offset_of_Mail_9(),
	FacebookAndroidTurnBasedAndGiftsExample_t99045661::get_offset_of_Name_10(),
	FacebookAndroidTurnBasedAndGiftsExample_t99045661::get_offset_of_BombItemId_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (FacebookAndroidUseExample_t2072023080), -1, sizeof(FacebookAndroidUseExample_t2072023080_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2607[18] = 
{
	FacebookAndroidUseExample_t2072023080_StaticFields::get_offset_of_IsUserInfoLoaded_2(),
	FacebookAndroidUseExample_t2072023080_StaticFields::get_offset_of_IsFrindsInfoLoaded_3(),
	FacebookAndroidUseExample_t2072023080_StaticFields::get_offset_of_IsAuntificated_4(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_ConnectionDependedntButtons_5(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_connectButton_6(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_avatar_7(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_Location_8(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_Language_9(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_Mail_10(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_Name_11(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_f1_12(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_f2_13(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_fi1_14(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_fi2_15(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_ImageToShare_16(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_friends_17(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_startScore_18(),
	FacebookAndroidUseExample_t2072023080::get_offset_of_UNION_ASSETS_PAGE_ID_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (U3CPostFBScreenshotU3Ec__Iterator4_t3249146850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[5] = 
{
	U3CPostFBScreenshotU3Ec__Iterator4_t3249146850::get_offset_of_U3CwidthU3E__0_0(),
	U3CPostFBScreenshotU3Ec__Iterator4_t3249146850::get_offset_of_U3CheightU3E__1_1(),
	U3CPostFBScreenshotU3Ec__Iterator4_t3249146850::get_offset_of_U3CtexU3E__2_2(),
	U3CPostFBScreenshotU3Ec__Iterator4_t3249146850::get_offset_of_U24PC_3(),
	U3CPostFBScreenshotU3Ec__Iterator4_t3249146850::get_offset_of_U24current_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (U3CPostScreenshotU3Ec__Iterator5_t1609060767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2609[5] = 
{
	U3CPostScreenshotU3Ec__Iterator5_t1609060767::get_offset_of_U3CwidthU3E__0_0(),
	U3CPostScreenshotU3Ec__Iterator5_t1609060767::get_offset_of_U3CheightU3E__1_1(),
	U3CPostScreenshotU3Ec__Iterator5_t1609060767::get_offset_of_U3CtexU3E__2_2(),
	U3CPostScreenshotU3Ec__Iterator5_t1609060767::get_offset_of_U24PC_3(),
	U3CPostScreenshotU3Ec__Iterator5_t1609060767::get_offset_of_U24current_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (TwitterAndroidUseExample_t3716461687), -1, sizeof(TwitterAndroidUseExample_t3716461687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2610[10] = 
{
	TwitterAndroidUseExample_t3716461687_StaticFields::get_offset_of_IsUserInfoLoaded_2(),
	TwitterAndroidUseExample_t3716461687_StaticFields::get_offset_of_IsAuthenticated_3(),
	TwitterAndroidUseExample_t3716461687::get_offset_of_ImageToShare_4(),
	TwitterAndroidUseExample_t3716461687::get_offset_of_connectButton_5(),
	TwitterAndroidUseExample_t3716461687::get_offset_of_avatar_6(),
	TwitterAndroidUseExample_t3716461687::get_offset_of_Location_7(),
	TwitterAndroidUseExample_t3716461687::get_offset_of_Language_8(),
	TwitterAndroidUseExample_t3716461687::get_offset_of_Status_9(),
	TwitterAndroidUseExample_t3716461687::get_offset_of_Name_10(),
	TwitterAndroidUseExample_t3716461687::get_offset_of_AuthDependedButtons_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (U3CPostTWScreenshotU3Ec__Iterator6_t2038777620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2611[5] = 
{
	U3CPostTWScreenshotU3Ec__Iterator6_t2038777620::get_offset_of_U3CwidthU3E__0_0(),
	U3CPostTWScreenshotU3Ec__Iterator6_t2038777620::get_offset_of_U3CheightU3E__1_1(),
	U3CPostTWScreenshotU3Ec__Iterator6_t2038777620::get_offset_of_U3CtexU3E__2_2(),
	U3CPostTWScreenshotU3Ec__Iterator6_t2038777620::get_offset_of_U24PC_3(),
	U3CPostTWScreenshotU3Ec__Iterator6_t2038777620::get_offset_of_U24current_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (U3CPostScreenshotU3Ec__Iterator7_t3015627852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2612[5] = 
{
	U3CPostScreenshotU3Ec__Iterator7_t3015627852::get_offset_of_U3CwidthU3E__0_0(),
	U3CPostScreenshotU3Ec__Iterator7_t3015627852::get_offset_of_U3CheightU3E__1_1(),
	U3CPostScreenshotU3Ec__Iterator7_t3015627852::get_offset_of_U3CtexU3E__2_2(),
	U3CPostScreenshotU3Ec__Iterator7_t3015627852::get_offset_of_U24PC_3(),
	U3CPostScreenshotU3Ec__Iterator7_t3015627852::get_offset_of_U24current_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (AndroidAdMobController_t1638300356), -1, sizeof(AndroidAdMobController_t1638300356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2613[17] = 
{
	0,
	AndroidAdMobController_t1638300356::get_offset_of__IsInited_5(),
	AndroidAdMobController_t1638300356::get_offset_of__banners_6(),
	AndroidAdMobController_t1638300356::get_offset_of__BannersUunitId_7(),
	AndroidAdMobController_t1638300356::get_offset_of__InterstisialUnitId_8(),
	AndroidAdMobController_t1638300356::get_offset_of__OnInterstitialLoaded_9(),
	AndroidAdMobController_t1638300356::get_offset_of__OnInterstitialFailedLoading_10(),
	AndroidAdMobController_t1638300356::get_offset_of__OnInterstitialOpened_11(),
	AndroidAdMobController_t1638300356::get_offset_of__OnInterstitialClosed_12(),
	AndroidAdMobController_t1638300356::get_offset_of__OnInterstitialLeftApplication_13(),
	AndroidAdMobController_t1638300356::get_offset_of__OnAdInAppRequest_14(),
	AndroidAdMobController_t1638300356_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_15(),
	AndroidAdMobController_t1638300356_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_16(),
	AndroidAdMobController_t1638300356_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_17(),
	AndroidAdMobController_t1638300356_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_18(),
	AndroidAdMobController_t1638300356_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_19(),
	AndroidAdMobController_t1638300356_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (AN_GoogleAdProxy_t3994141898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2614[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (AN_PoupsProxy_t2977617695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2615[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (AN_ProxyPool_t3556146334), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (AN_AppInvitesProxy_t250409945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (AN_BillingProxy_t3904793637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2618[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (AN_FBProxy_t1438007924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2619[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (AN_GMSGeneralProxy_t49831053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2620[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (AN_GMSGiftsProxy_t1705430956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (AN_GMSInvitationProxy_t2905962638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2622[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (AN_GMSQuestsEventsProxy_t3325636741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2623[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (AN_GMSRTMProxy_t1149422734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (AN_GoogleAnalyticsProxy_t595315245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (AN_GooglePlayUtilsProxy_t535695022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (AN_ImmersiveModeProxy_t2791662232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2627[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (AN_LicenseManagerProxy_t1907271486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2628[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (AN_NotificationProxy_t3523749487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (AN_PlusButtonProxy_t2040058752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2630[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (AN_SocialSharingProxy_t924445897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2631[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (AN_TVControllerProxy_t3771858466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (AndroidNative_t876365532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2633[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (AndroidNativeSettings_t3397157021), -1, sizeof(AndroidNativeSettings_t3397157021_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2634[91] = 
{
	0,
	0,
	0,
	0,
	0,
	AndroidNativeSettings_t3397157021::get_offset_of_EnablePlusAPI_7(),
	AndroidNativeSettings_t3397157021::get_offset_of_EnableGamesAPI_8(),
	AndroidNativeSettings_t3397157021::get_offset_of_EnableAppInviteAPI_9(),
	AndroidNativeSettings_t3397157021::get_offset_of_EnableDriveAPI_10(),
	AndroidNativeSettings_t3397157021::get_offset_of_LoadProfileIcons_11(),
	AndroidNativeSettings_t3397157021::get_offset_of_LoadProfileImages_12(),
	AndroidNativeSettings_t3397157021::get_offset_of_LoadQuestsImages_13(),
	AndroidNativeSettings_t3397157021::get_offset_of_LoadQuestsIcons_14(),
	AndroidNativeSettings_t3397157021::get_offset_of_LoadEventsIcons_15(),
	AndroidNativeSettings_t3397157021::get_offset_of_ShowConnectingPopup_16(),
	AndroidNativeSettings_t3397157021::get_offset_of_EnableATCSupport_17(),
	AndroidNativeSettings_t3397157021::get_offset_of_OneSignalEnabled_18(),
	AndroidNativeSettings_t3397157021::get_offset_of_OneSignalAppID_19(),
	AndroidNativeSettings_t3397157021::get_offset_of_OneSignalDownloadLink_20(),
	AndroidNativeSettings_t3397157021::get_offset_of_OneSignalDocLink_21(),
	AndroidNativeSettings_t3397157021::get_offset_of_UseParsePushNotifications_22(),
	AndroidNativeSettings_t3397157021::get_offset_of_ParseAppId_23(),
	AndroidNativeSettings_t3397157021::get_offset_of_DotNetKey_24(),
	AndroidNativeSettings_t3397157021::get_offset_of_ParseDocLink_25(),
	AndroidNativeSettings_t3397157021::get_offset_of_ParseDownloadLink_26(),
	AndroidNativeSettings_t3397157021::get_offset_of_EnableSoomla_27(),
	AndroidNativeSettings_t3397157021::get_offset_of_SoomlaDownloadLink_28(),
	AndroidNativeSettings_t3397157021::get_offset_of_SoomlaDocsLink_29(),
	AndroidNativeSettings_t3397157021::get_offset_of_SoomlaGameKey_30(),
	AndroidNativeSettings_t3397157021::get_offset_of_SoomlaEnvKey_31(),
	AndroidNativeSettings_t3397157021::get_offset_of_GCM_SenderId_32(),
	AndroidNativeSettings_t3397157021::get_offset_of_PushService_33(),
	AndroidNativeSettings_t3397157021::get_offset_of_SaveCameraImageToGallery_34(),
	AndroidNativeSettings_t3397157021::get_offset_of_UseProductNameAsFolderName_35(),
	AndroidNativeSettings_t3397157021::get_offset_of_GalleryFolderName_36(),
	AndroidNativeSettings_t3397157021::get_offset_of_MaxImageLoadSize_37(),
	AndroidNativeSettings_t3397157021::get_offset_of_CameraCaptureMode_38(),
	AndroidNativeSettings_t3397157021::get_offset_of_ImageFormat_39(),
	AndroidNativeSettings_t3397157021::get_offset_of_ShowAppPermissions_40(),
	AndroidNativeSettings_t3397157021::get_offset_of_EnableBillingAPI_41(),
	AndroidNativeSettings_t3397157021::get_offset_of_EnablePSAPI_42(),
	AndroidNativeSettings_t3397157021::get_offset_of_EnableSocialAPI_43(),
	AndroidNativeSettings_t3397157021::get_offset_of_EnableCameraAPI_44(),
	AndroidNativeSettings_t3397157021::get_offset_of_ExpandNativeAPI_45(),
	AndroidNativeSettings_t3397157021::get_offset_of_ExpandPSAPI_46(),
	AndroidNativeSettings_t3397157021::get_offset_of_ExpandBillingAPI_47(),
	AndroidNativeSettings_t3397157021::get_offset_of_ExpandSocialAPI_48(),
	AndroidNativeSettings_t3397157021::get_offset_of_ExpandCameraAPI_49(),
	AndroidNativeSettings_t3397157021::get_offset_of_ThirdPartyParams_50(),
	AndroidNativeSettings_t3397157021::get_offset_of_ShowPSSettingsResources_51(),
	AndroidNativeSettings_t3397157021::get_offset_of_ShowActions_52(),
	AndroidNativeSettings_t3397157021::get_offset_of_GCMSettingsActinve_53(),
	AndroidNativeSettings_t3397157021::get_offset_of_LocalNotificationsAPI_54(),
	AndroidNativeSettings_t3397157021::get_offset_of_ImmersiveModeAPI_55(),
	AndroidNativeSettings_t3397157021::get_offset_of_ApplicationInformationAPI_56(),
	AndroidNativeSettings_t3397157021::get_offset_of_ExternalAppsAPI_57(),
	AndroidNativeSettings_t3397157021::get_offset_of_PoupsandPreloadersAPI_58(),
	AndroidNativeSettings_t3397157021::get_offset_of_CheckAppLicenseAPI_59(),
	AndroidNativeSettings_t3397157021::get_offset_of_NetworkStateAPI_60(),
	AndroidNativeSettings_t3397157021::get_offset_of_InAppPurchasesAPI_61(),
	AndroidNativeSettings_t3397157021::get_offset_of_GooglePlayServicesAPI_62(),
	AndroidNativeSettings_t3397157021::get_offset_of_PlayServicesAdvancedSignInAPI_63(),
	AndroidNativeSettings_t3397157021::get_offset_of_GoogleButtonAPI_64(),
	AndroidNativeSettings_t3397157021::get_offset_of_AnalyticsAPI_65(),
	AndroidNativeSettings_t3397157021::get_offset_of_GoogleCloudSaveAPI_66(),
	AndroidNativeSettings_t3397157021::get_offset_of_PushNotificationsAPI_67(),
	AndroidNativeSettings_t3397157021::get_offset_of_GoogleMobileAdAPI_68(),
	AndroidNativeSettings_t3397157021::get_offset_of_GalleryAPI_69(),
	AndroidNativeSettings_t3397157021::get_offset_of_CameraAPI_70(),
	AndroidNativeSettings_t3397157021::get_offset_of_KeepManifestClean_71(),
	AndroidNativeSettings_t3397157021::get_offset_of_GooglePlayServiceAppID_72(),
	AndroidNativeSettings_t3397157021::get_offset_of_ToolbarSelectedIndex_73(),
	AndroidNativeSettings_t3397157021::get_offset_of_base64EncodedPublicKey_74(),
	AndroidNativeSettings_t3397157021::get_offset_of_ShowStoreProducts_75(),
	AndroidNativeSettings_t3397157021::get_offset_of_InAppProducts_76(),
	AndroidNativeSettings_t3397157021::get_offset_of_ShowLeaderboards_77(),
	AndroidNativeSettings_t3397157021::get_offset_of_Leaderboards_78(),
	AndroidNativeSettings_t3397157021::get_offset_of_ShowAchievements_79(),
	AndroidNativeSettings_t3397157021::get_offset_of_Achievements_80(),
	AndroidNativeSettings_t3397157021::get_offset_of_ShowWhenAppIsForeground_81(),
	AndroidNativeSettings_t3397157021::get_offset_of_EnableVibrationLocal_82(),
	AndroidNativeSettings_t3397157021::get_offset_of_LocalNotificationSmallIcon_83(),
	AndroidNativeSettings_t3397157021::get_offset_of_LocalNotificationLargeIcon_84(),
	AndroidNativeSettings_t3397157021::get_offset_of_LocalNotificationSound_85(),
	AndroidNativeSettings_t3397157021::get_offset_of_ReplaceOldNotificationWithNew_86(),
	AndroidNativeSettings_t3397157021::get_offset_of_ShowPushWhenAppIsForeground_87(),
	AndroidNativeSettings_t3397157021::get_offset_of_EnableVibrationPush_88(),
	AndroidNativeSettings_t3397157021::get_offset_of_PushNotificationSmallIcon_89(),
	AndroidNativeSettings_t3397157021::get_offset_of_PushNotificationLargeIcon_90(),
	AndroidNativeSettings_t3397157021::get_offset_of_PushNotificationSound_91(),
	AndroidNativeSettings_t3397157021_StaticFields::get_offset_of_instance_92(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (SocialPlatfromSettings_t3418207459), -1, sizeof(SocialPlatfromSettings_t3418207459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2635[22] = 
{
	0,
	0,
	0,
	0,
	0,
	SocialPlatfromSettings_t3418207459::get_offset_of_ShowImageSharingSettings_7(),
	SocialPlatfromSettings_t3418207459::get_offset_of_SaveImageToGallery_8(),
	SocialPlatfromSettings_t3418207459::get_offset_of_showPermitions_9(),
	SocialPlatfromSettings_t3418207459::get_offset_of_ShowActions_10(),
	SocialPlatfromSettings_t3418207459::get_offset_of_ShowAPIS_11(),
	SocialPlatfromSettings_t3418207459::get_offset_of_fb_scopes_list_12(),
	SocialPlatfromSettings_t3418207459::get_offset_of_TWITTER_CONSUMER_KEY_13(),
	SocialPlatfromSettings_t3418207459::get_offset_of_TWITTER_CONSUMER_SECRET_14(),
	SocialPlatfromSettings_t3418207459::get_offset_of_TWITTER_ACCESS_TOKEN_15(),
	SocialPlatfromSettings_t3418207459::get_offset_of_TWITTER_ACCESS_TOKEN_SECRET_16(),
	SocialPlatfromSettings_t3418207459::get_offset_of_ShowEditorOauthTestingBlock_17(),
	SocialPlatfromSettings_t3418207459::get_offset_of_TwitterAPI_18(),
	SocialPlatfromSettings_t3418207459::get_offset_of_NativeSharingAPI_19(),
	SocialPlatfromSettings_t3418207459::get_offset_of_InstagramAPI_20(),
	SocialPlatfromSettings_t3418207459::get_offset_of_EnableImageSharing_21(),
	SocialPlatfromSettings_t3418207459::get_offset_of_KeepManifestClean_22(),
	SocialPlatfromSettings_t3418207459_StaticFields::get_offset_of_instance_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (AdroidActivityResultCodes_t2848269969)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2636[4] = 
{
	AdroidActivityResultCodes_t2848269969::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (AndroidGravity_t1500788099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2637[27] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (AndroidLogLevel_t2302792355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2638[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (AndroidMonth_t1370057857)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2639[13] = 
{
	AndroidMonth_t1370057857::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (GoogleCloudSlot_t3383174108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (GoogleGender_t966451998)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2641[4] = 
{
	GoogleGender_t966451998::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (GoogleGravity_t2146990713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (GADBannerSize_t1988135029)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2643[6] = 
{
	GADBannerSize_t1988135029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (GADInAppResolution_t612781804)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2644[5] = 
{
	GADInAppResolution_t612781804::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (GPAchievementState_t819112501)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2645[4] = 
{
	GPAchievementState_t819112501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (GPAchievementType_t785407574)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2646[3] = 
{
	GPAchievementType_t785407574::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (GPBoardTimeSpan_t42003024)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2647[4] = 
{
	GPBoardTimeSpan_t42003024::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (GPCollectionType_t2617299399)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2648[3] = 
{
	GPCollectionType_t2617299399::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (GPConnectionState_t647346880)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2649[5] = 
{
	GPConnectionState_t647346880::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (GPGameRequestType_t1088795774)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2650[3] = 
{
	GPGameRequestType_t1088795774::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (GPLogLevel_t1212391055)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2651[5] = 
{
	GPLogLevel_t1212391055::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (GP_AppStateStatusCodes_t2689651680)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2652[15] = 
{
	GP_AppStateStatusCodes_t2689651680::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (GP_ConnectionResultCode_t3996068124)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2653[18] = 
{
	GP_ConnectionResultCode_t3996068124::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (GP_GamesActivityResultCodes_t70181657)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2654[12] = 
{
	GP_GamesActivityResultCodes_t70181657::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (GP_GamesStatusCodes_t1013506173)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2655[40] = 
{
	GP_GamesStatusCodes_t1013506173::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (GP_InvitationType_t3295179663)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2656[3] = 
{
	GP_InvitationType_t3295179663::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (GP_MatchesSortOrder_t1055105683), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (GP_RTM_MessageType_t1912197865)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2658[3] = 
{
	GP_RTM_MessageType_t1912197865::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (GP_RTM_PackageType_t1787524174)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2659[3] = 
{
	GP_RTM_PackageType_t1787524174::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (GP_RTM_ParticipantStatus_t2642611273)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2660[8] = 
{
	GP_RTM_ParticipantStatus_t2642611273::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (GP_RTM_RoomStatus_t2051988161)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2661[6] = 
{
	GP_RTM_RoomStatus_t2051988161::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (GP_QuestSortOrder_t4026176042)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2662[3] = 
{
	GP_QuestSortOrder_t4026176042::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (GP_QuestState_t1072114879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2663[7] = 
{
	GP_QuestState_t1072114879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (GP_QuestsSelect_t2071002355)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2664[9] = 
{
	GP_QuestsSelect_t2071002355::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (GP_TBM_MatchStatus_t2102223509)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2665[6] = 
{
	GP_TBM_MatchStatus_t2102223509::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (GP_TBM_MatchTurnStatus_t2221730550)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2666[5] = 
{
	GP_TBM_MatchTurnStatus_t2221730550::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (GP_TBM_MatchesSortOrder_t3142286675)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2667[3] = 
{
	GP_TBM_MatchesSortOrder_t3142286675::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (GP_TBM_ParticipantResult_t1952842762)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2668[8] = 
{
	GP_TBM_ParticipantResult_t1952842762::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (FacebookEvents_t1690892297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (GoogleMobileAdEvents_t1054095093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (TwitterEvents_t3891486642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (AndroidADBanner_t886219444), -1, sizeof(AndroidADBanner_t886219444_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2675[20] = 
{
	AndroidADBanner_t886219444::get_offset_of__id_0(),
	AndroidADBanner_t886219444::get_offset_of__size_1(),
	AndroidADBanner_t886219444::get_offset_of__anchor_2(),
	AndroidADBanner_t886219444::get_offset_of__IsLoaded_3(),
	AndroidADBanner_t886219444::get_offset_of__IsOnScreen_4(),
	AndroidADBanner_t886219444::get_offset_of_firstLoad_5(),
	AndroidADBanner_t886219444::get_offset_of_destroyOnLoad_6(),
	AndroidADBanner_t886219444::get_offset_of__ShowOnLoad_7(),
	AndroidADBanner_t886219444::get_offset_of__width_8(),
	AndroidADBanner_t886219444::get_offset_of__height_9(),
	AndroidADBanner_t886219444::get_offset_of__OnLoadedAction_10(),
	AndroidADBanner_t886219444::get_offset_of__OnFailedLoadingAction_11(),
	AndroidADBanner_t886219444::get_offset_of__OnOpenedAction_12(),
	AndroidADBanner_t886219444::get_offset_of__OnClosedAction_13(),
	AndroidADBanner_t886219444::get_offset_of__OnLeftApplicationAction_14(),
	AndroidADBanner_t886219444_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_15(),
	AndroidADBanner_t886219444_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_16(),
	AndroidADBanner_t886219444_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_17(),
	AndroidADBanner_t886219444_StaticFields::get_offset_of_U3CU3Ef__amU24cache12_18(),
	AndroidADBanner_t886219444_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (AndroidActivityResult_t3757510801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[2] = 
{
	AndroidActivityResult_t3757510801::get_offset_of__code_0(),
	AndroidActivityResult_t3757510801::get_offset_of__requestId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (AndroidGameActivityResult_t1444576507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[1] = 
{
	AndroidGameActivityResult_t1444576507::get_offset_of_code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (AsyncTask_t1407929465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (GADBannerIdFactory_t603853557), -1, sizeof(GADBannerIdFactory_t603853557_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2679[1] = 
{
	GADBannerIdFactory_t603853557_StaticFields::get_offset_of__nextId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (GPAchievement_t4279788054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2680[9] = 
{
	GPAchievement_t4279788054::get_offset_of_IsOpen_0(),
	GPAchievement_t4279788054::get_offset_of__id_1(),
	GPAchievement_t4279788054::get_offset_of__name_2(),
	GPAchievement_t4279788054::get_offset_of__description_3(),
	GPAchievement_t4279788054::get_offset_of__Texture_4(),
	GPAchievement_t4279788054::get_offset_of__currentSteps_5(),
	GPAchievement_t4279788054::get_offset_of__totalSteps_6(),
	GPAchievement_t4279788054::get_offset_of__type_7(),
	GPAchievement_t4279788054::get_offset_of__state_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (GPGameRequest_t4158842942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[6] = 
{
	GPGameRequest_t4158842942::get_offset_of_id_0(),
	GPGameRequest_t4158842942::get_offset_of_playload_1(),
	GPGameRequest_t4158842942::get_offset_of_expirationTimestamp_2(),
	GPGameRequest_t4158842942::get_offset_of_creationTimestamp_3(),
	GPGameRequest_t4158842942::get_offset_of_sender_4(),
	GPGameRequest_t4158842942::get_offset_of_type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (GPLeaderBoard_t3649577886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2682[9] = 
{
	GPLeaderBoard_t3649577886::get_offset_of_IsOpen_0(),
	GPLeaderBoard_t3649577886::get_offset_of__id_1(),
	GPLeaderBoard_t3649577886::get_offset_of__name_2(),
	GPLeaderBoard_t3649577886::get_offset_of__description_3(),
	GPLeaderBoard_t3649577886::get_offset_of__Texture_4(),
	GPLeaderBoard_t3649577886::get_offset_of_SocsialCollection_5(),
	GPLeaderBoard_t3649577886::get_offset_of_GlobalCollection_6(),
	GPLeaderBoard_t3649577886::get_offset_of_CurrentPlayerScore_7(),
	GPLeaderBoard_t3649577886::get_offset_of__ScoreUpdateListners_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (GPScoreCollection_t2240243855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[3] = 
{
	GPScoreCollection_t2240243855::get_offset_of_AllTimeScores_0(),
	GPScoreCollection_t2240243855::get_offset_of_WeekScores_1(),
	GPScoreCollection_t2240243855::get_offset_of_TodayScores_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (GP_Invite_t626929087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2684[5] = 
{
	GP_Invite_t626929087::get_offset_of_Id_0(),
	GP_Invite_t626929087::get_offset_of_CreationTimestamp_1(),
	GP_Invite_t626929087::get_offset_of_InvitationType_2(),
	GP_Invite_t626929087::get_offset_of_Variant_3(),
	GP_Invite_t626929087::get_offset_of_Participant_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (GP_LocalPlayerScoreUpdateListener_t158126391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2685[4] = 
{
	GP_LocalPlayerScoreUpdateListener_t158126391::get_offset_of__RequestId_0(),
	GP_LocalPlayerScoreUpdateListener_t158126391::get_offset_of__leaderboardId_1(),
	GP_LocalPlayerScoreUpdateListener_t158126391::get_offset_of__ErrorData_2(),
	GP_LocalPlayerScoreUpdateListener_t158126391::get_offset_of_Scores_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (GP_Participant_t2884377673), -1, sizeof(GP_Participant_t2884377673_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2686[13] = 
{
	GP_Participant_t2884377673::get_offset_of__id_0(),
	GP_Participant_t2884377673::get_offset_of__playerid_1(),
	GP_Participant_t2884377673::get_offset_of__HiResImageUrl_2(),
	GP_Participant_t2884377673::get_offset_of__IconImageUrl_3(),
	GP_Participant_t2884377673::get_offset_of__DisplayName_4(),
	GP_Participant_t2884377673::get_offset_of__result_5(),
	GP_Participant_t2884377673::get_offset_of__status_6(),
	GP_Participant_t2884377673::get_offset_of__SmallPhoto_7(),
	GP_Participant_t2884377673::get_offset_of__BigPhoto_8(),
	GP_Participant_t2884377673::get_offset_of_BigPhotoLoaded_9(),
	GP_Participant_t2884377673::get_offset_of_SmallPhotoLoaded_10(),
	GP_Participant_t2884377673_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_11(),
	GP_Participant_t2884377673_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (GP_ParticipantResult_t2469018720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[4] = 
{
	GP_ParticipantResult_t2469018720::get_offset_of__Placing_0(),
	GP_ParticipantResult_t2469018720::get_offset_of__Result_1(),
	GP_ParticipantResult_t2469018720::get_offset_of__VersionCode_2(),
	GP_ParticipantResult_t2469018720::get_offset_of__ParticipantId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (GP_RTM_Network_Package_t2050307869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[3] = 
{
	0,
	GP_RTM_Network_Package_t2050307869::get_offset_of__playerId_1(),
	GP_RTM_Network_Package_t2050307869::get_offset_of__buffer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (GP_RTM_ReliableMessageDeliveredResult_t1694070004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[2] = 
{
	GP_RTM_ReliableMessageDeliveredResult_t1694070004::get_offset_of__MessageTokenId_2(),
	GP_RTM_ReliableMessageDeliveredResult_t1694070004::get_offset_of__Data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (GP_RTM_ReliableMessageListener_t1343560679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[3] = 
{
	GP_RTM_ReliableMessageListener_t1343560679::get_offset_of__Data_0(),
	GP_RTM_ReliableMessageListener_t1343560679::get_offset_of__DataTokenId_1(),
	GP_RTM_ReliableMessageListener_t1343560679::get_offset_of__ReliableMessagesCounter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (GP_RTM_ReliableMessageSentResult_t2743629396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[2] = 
{
	GP_RTM_ReliableMessageSentResult_t2743629396::get_offset_of__MessageTokenId_2(),
	GP_RTM_ReliableMessageSentResult_t2743629396::get_offset_of__Data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (GP_RTM_Result_t473289279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[2] = 
{
	GP_RTM_Result_t473289279::get_offset_of__status_0(),
	GP_RTM_Result_t473289279::get_offset_of__roomId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (GP_RTM_Room_t851604529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2693[5] = 
{
	GP_RTM_Room_t851604529::get_offset_of_id_0(),
	GP_RTM_Room_t851604529::get_offset_of_creatorId_1(),
	GP_RTM_Room_t851604529::get_offset_of_status_2(),
	GP_RTM_Room_t851604529::get_offset_of_creationTimestamp_3(),
	GP_RTM_Room_t851604529::get_offset_of_participants_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (GoogleCloudResult_t743628707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[6] = 
{
	GoogleCloudResult_t743628707::get_offset_of__response_0(),
	GoogleCloudResult_t743628707::get_offset_of__message_1(),
	GoogleCloudResult_t743628707::get_offset_of__stateKey_2(),
	GoogleCloudResult_t743628707::get_offset_of_stateData_3(),
	GoogleCloudResult_t743628707::get_offset_of_serverConflictData_4(),
	GoogleCloudResult_t743628707::get_offset_of_resolvedVersion_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (GooglePlayConnectionResult_t2758718724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[2] = 
{
	GooglePlayConnectionResult_t2758718724::get_offset_of_code_0(),
	GooglePlayConnectionResult_t2758718724::get_offset_of_HasResolution_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (GooglePlayGiftRequestResult_t350202007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2696[1] = 
{
	GooglePlayGiftRequestResult_t350202007::get_offset_of__code_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (GooglePlayResult_t3097469636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[2] = 
{
	GooglePlayResult_t3097469636::get_offset_of__response_0(),
	GooglePlayResult_t3097469636::get_offset_of__message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (GP_Event_t323143668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[6] = 
{
	GP_Event_t323143668::get_offset_of_Id_0(),
	GP_Event_t323143668::get_offset_of_Description_1(),
	GP_Event_t323143668::get_offset_of_IconImageUrl_2(),
	GP_Event_t323143668::get_offset_of_FormattedValue_3(),
	GP_Event_t323143668::get_offset_of_Value_4(),
	GP_Event_t323143668::get_offset_of__icon_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (GP_Quest_t1641883470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[14] = 
{
	GP_Quest_t1641883470::get_offset_of_Id_0(),
	GP_Quest_t1641883470::get_offset_of_Name_1(),
	GP_Quest_t1641883470::get_offset_of_Description_2(),
	GP_Quest_t1641883470::get_offset_of_IconImageUrl_3(),
	GP_Quest_t1641883470::get_offset_of_BannerImageUrl_4(),
	GP_Quest_t1641883470::get_offset_of_state_5(),
	GP_Quest_t1641883470::get_offset_of_LastUpdatedTimestamp_6(),
	GP_Quest_t1641883470::get_offset_of_AcceptedTimestamp_7(),
	GP_Quest_t1641883470::get_offset_of_EndTimestamp_8(),
	GP_Quest_t1641883470::get_offset_of_RewardData_9(),
	GP_Quest_t1641883470::get_offset_of_CurrentProgress_10(),
	GP_Quest_t1641883470::get_offset_of_TargetProgress_11(),
	GP_Quest_t1641883470::get_offset_of__icon_12(),
	GP_Quest_t1641883470::get_offset_of__banner_13(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
