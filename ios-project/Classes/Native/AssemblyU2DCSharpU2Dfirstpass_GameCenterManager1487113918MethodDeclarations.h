﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameCenterManager
struct GameCenterManager_t1487113918;
// System.Action`1<SA.Common.Models.Result>
struct Action_1_t4089019125;
// System.Action`1<GK_LeaderboardResult>
struct Action_1_t667880215;
// System.Action`1<GK_AchievementProgressResult>
struct Action_1_t3341373734;
// System.Action
struct Action_t3226471752;
// System.Action`1<GK_UserInfoLoadResult>
struct Action_1_t979640615;
// System.Action`1<GK_PlayerSignatureResult>
struct Action_1_t4110536157;
// System.String
struct String_t;
// GK_AchievementTemplate
struct GK_AchievementTemplate_t2296152240;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// GK_Leaderboard
struct GK_Leaderboard_t156446466;
// GK_Player
struct GK_Player_t2782008294;
// GK_FriendRequest
struct GK_FriendRequest_t4101620808;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<GK_AchievementTemplate>
struct List_1_t1665273372;
// System.Collections.Generic.List`1<GK_Leaderboard>
struct List_1_t3820534894;
// System.Collections.Generic.Dictionary`2<System.String,GK_Player>
struct Dictionary_2_t401820260;
// System.Collections.Generic.List`1<GK_LeaderboardSet>
struct List_1_t3669402526;
// GK_LeaderboardResult
struct GK_LeaderboardResult_t866080833;
// System.Collections.Generic.List`1<GK_TBM_Participant>
struct List_1_t3173076222;
// GK_TBM_Participant
struct GK_TBM_Participant_t3803955090;
// SA.Common.Models.Result
struct Result_t4287219743;
// GK_AchievementProgressResult
struct GK_AchievementProgressResult_t3539574352;
// GK_UserInfoLoadResult
struct GK_UserInfoLoadResult_t1177841233;
// GK_PlayerSignatureResult
struct GK_PlayerSignatureResult_t13769479;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_AchievementTempla2296152240.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TimeSpan1050271570.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_PhotoSize3124681388.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_CollectionType3353981271.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_FriendRequest4101620808.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_LeaderboardResult866080833.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_AchievementProgre3539574352.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_UserInfoLoadResul1177841233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_PlayerSignatureResu13769479.h"

// System.Void GameCenterManager::.ctor()
extern "C"  void GameCenterManager__ctor_m3529889311 (GameCenterManager_t1487113918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::.cctor()
extern "C"  void GameCenterManager__cctor_m1383671914 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::add_OnAuthFinished(System.Action`1<SA.Common.Models.Result>)
extern "C"  void GameCenterManager_add_OnAuthFinished_m1732264708 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::remove_OnAuthFinished(System.Action`1<SA.Common.Models.Result>)
extern "C"  void GameCenterManager_remove_OnAuthFinished_m675256871 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::add_OnScoreSubmitted(System.Action`1<GK_LeaderboardResult>)
extern "C"  void GameCenterManager_add_OnScoreSubmitted_m806779428 (Il2CppObject * __this /* static, unused */, Action_1_t667880215 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::remove_OnScoreSubmitted(System.Action`1<GK_LeaderboardResult>)
extern "C"  void GameCenterManager_remove_OnScoreSubmitted_m1677066483 (Il2CppObject * __this /* static, unused */, Action_1_t667880215 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::add_OnScoresListLoaded(System.Action`1<GK_LeaderboardResult>)
extern "C"  void GameCenterManager_add_OnScoresListLoaded_m3904250687 (Il2CppObject * __this /* static, unused */, Action_1_t667880215 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::remove_OnScoresListLoaded(System.Action`1<GK_LeaderboardResult>)
extern "C"  void GameCenterManager_remove_OnScoresListLoaded_m3339032928 (Il2CppObject * __this /* static, unused */, Action_1_t667880215 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::add_OnLeadrboardInfoLoaded(System.Action`1<GK_LeaderboardResult>)
extern "C"  void GameCenterManager_add_OnLeadrboardInfoLoaded_m3546710302 (Il2CppObject * __this /* static, unused */, Action_1_t667880215 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::remove_OnLeadrboardInfoLoaded(System.Action`1<GK_LeaderboardResult>)
extern "C"  void GameCenterManager_remove_OnLeadrboardInfoLoaded_m817319411 (Il2CppObject * __this /* static, unused */, Action_1_t667880215 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::add_OnLeaderboardSetsInfoLoaded(System.Action`1<SA.Common.Models.Result>)
extern "C"  void GameCenterManager_add_OnLeaderboardSetsInfoLoaded_m1935871251 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::remove_OnLeaderboardSetsInfoLoaded(System.Action`1<SA.Common.Models.Result>)
extern "C"  void GameCenterManager_remove_OnLeaderboardSetsInfoLoaded_m1988388322 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::add_OnAchievementsReset(System.Action`1<SA.Common.Models.Result>)
extern "C"  void GameCenterManager_add_OnAchievementsReset_m2625065747 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::remove_OnAchievementsReset(System.Action`1<SA.Common.Models.Result>)
extern "C"  void GameCenterManager_remove_OnAchievementsReset_m1196593518 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::add_OnAchievementsLoaded(System.Action`1<SA.Common.Models.Result>)
extern "C"  void GameCenterManager_add_OnAchievementsLoaded_m2020303677 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::remove_OnAchievementsLoaded(System.Action`1<SA.Common.Models.Result>)
extern "C"  void GameCenterManager_remove_OnAchievementsLoaded_m2425136216 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::add_OnAchievementsProgress(System.Action`1<GK_AchievementProgressResult>)
extern "C"  void GameCenterManager_add_OnAchievementsProgress_m3017036099 (Il2CppObject * __this /* static, unused */, Action_1_t3341373734 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::remove_OnAchievementsProgress(System.Action`1<GK_AchievementProgressResult>)
extern "C"  void GameCenterManager_remove_OnAchievementsProgress_m3061185700 (Il2CppObject * __this /* static, unused */, Action_1_t3341373734 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::add_OnGameCenterViewDismissed(System.Action)
extern "C"  void GameCenterManager_add_OnGameCenterViewDismissed_m2446555634 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::remove_OnGameCenterViewDismissed(System.Action)
extern "C"  void GameCenterManager_remove_OnGameCenterViewDismissed_m233603341 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::add_OnFriendsListLoaded(System.Action`1<SA.Common.Models.Result>)
extern "C"  void GameCenterManager_add_OnFriendsListLoaded_m3261364194 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::remove_OnFriendsListLoaded(System.Action`1<SA.Common.Models.Result>)
extern "C"  void GameCenterManager_remove_OnFriendsListLoaded_m3387043025 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::add_OnUserInfoLoaded(System.Action`1<GK_UserInfoLoadResult>)
extern "C"  void GameCenterManager_add_OnUserInfoLoaded_m3304651797 (Il2CppObject * __this /* static, unused */, Action_1_t979640615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::remove_OnUserInfoLoaded(System.Action`1<GK_UserInfoLoadResult>)
extern "C"  void GameCenterManager_remove_OnUserInfoLoaded_m716801382 (Il2CppObject * __this /* static, unused */, Action_1_t979640615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::add_OnPlayerSignatureRetrieveResult(System.Action`1<GK_PlayerSignatureResult>)
extern "C"  void GameCenterManager_add_OnPlayerSignatureRetrieveResult_m1760434439 (Il2CppObject * __this /* static, unused */, Action_1_t4110536157 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::remove_OnPlayerSignatureRetrieveResult(System.Action`1<GK_PlayerSignatureResult>)
extern "C"  void GameCenterManager_remove_OnPlayerSignatureRetrieveResult_m2225429912 (Il2CppObject * __this /* static, unused */, Action_1_t4110536157 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::init()
extern "C"  void GameCenterManager_init_m1638700913 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::Init()
extern "C"  void GameCenterManager_Init_m2787824017 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::RetrievePlayerSignature()
extern "C"  void GameCenterManager_RetrievePlayerSignature_m397289194 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ShowGmaeKitNotification(System.String,System.String)
extern "C"  void GameCenterManager_ShowGmaeKitNotification_m2453749561 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::RegisterAchievement(System.String)
extern "C"  void GameCenterManager_RegisterAchievement_m335971487 (Il2CppObject * __this /* static, unused */, String_t* ___achievementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::RegisterAchievement(GK_AchievementTemplate)
extern "C"  void GameCenterManager_RegisterAchievement_m907441865 (Il2CppObject * __this /* static, unused */, GK_AchievementTemplate_t2296152240 * ___achievement0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ShowLeaderboard(System.String)
extern "C"  void GameCenterManager_ShowLeaderboard_m2697806147 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ShowLeaderboard(System.String,GK_TimeSpan)
extern "C"  void GameCenterManager_ShowLeaderboard_m444205617 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, int32_t ___timeSpan1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ShowLeaderboards()
extern "C"  void GameCenterManager_ShowLeaderboards_m2373312026 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ReportScore(System.Int64,System.String,System.Int64)
extern "C"  void GameCenterManager_ReportScore_m1076767671 (Il2CppObject * __this /* static, unused */, int64_t ___score0, String_t* ___leaderboardId1, int64_t ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ReportScore(System.Double,System.String)
extern "C"  void GameCenterManager_ReportScore_m4038171793 (Il2CppObject * __this /* static, unused */, double ___score0, String_t* ___leaderboardId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::RetrieveFriends()
extern "C"  void GameCenterManager_RetrieveFriends_m381071378 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::LoadUsersData(System.String[])
extern "C"  void GameCenterManager_LoadUsersData_m4287488915 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___UIDs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::LoadGKPlayerInfo(System.String)
extern "C"  void GameCenterManager_LoadGKPlayerInfo_m3806363386 (Il2CppObject * __this /* static, unused */, String_t* ___playerId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::LoadGKPlayerPhoto(System.String,GK_PhotoSize)
extern "C"  void GameCenterManager_LoadGKPlayerPhoto_m181638398 (Il2CppObject * __this /* static, unused */, String_t* ___playerId0, int32_t ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::LoadCurrentPlayerScore(System.String,GK_TimeSpan,GK_CollectionType)
extern "C"  void GameCenterManager_LoadCurrentPlayerScore_m3478523806 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, int32_t ___timeSpan1, int32_t ___collection2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::LoadLeaderboardInfo(System.String)
extern "C"  void GameCenterManager_LoadLeaderboardInfo_m877324586 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameCenterManager::LoadLeaderboardInfoLocal(System.String)
extern "C"  Il2CppObject * GameCenterManager_LoadLeaderboardInfoLocal_m3336378523 (GameCenterManager_t1487113918 * __this, String_t* ___leaderboardId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::LoadScore(System.String,System.Int32,System.Int32,GK_TimeSpan,GK_CollectionType)
extern "C"  void GameCenterManager_LoadScore_m2766104624 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, int32_t ___startIndex1, int32_t ___length2, int32_t ___timeSpan3, int32_t ___collection4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::IssueLeaderboardChallenge(System.String,System.String,System.String)
extern "C"  void GameCenterManager_IssueLeaderboardChallenge_m2084202126 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, String_t* ___message1, String_t* ___playerId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::IssueLeaderboardChallenge(System.String,System.String,System.String[])
extern "C"  void GameCenterManager_IssueLeaderboardChallenge_m1813491482 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, String_t* ___message1, StringU5BU5D_t1642385972* ___playerIds2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::IssueLeaderboardChallenge(System.String,System.String)
extern "C"  void GameCenterManager_IssueLeaderboardChallenge_m503493478 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::IssueAchievementChallenge(System.String,System.String,System.String)
extern "C"  void GameCenterManager_IssueAchievementChallenge_m268078578 (Il2CppObject * __this /* static, unused */, String_t* ___achievementId0, String_t* ___message1, String_t* ___playerId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::LoadLeaderboardSetInfo()
extern "C"  void GameCenterManager_LoadLeaderboardSetInfo_m1085687202 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::LoadLeaderboardsForSet(System.String)
extern "C"  void GameCenterManager_LoadLeaderboardsForSet_m2056765420 (Il2CppObject * __this /* static, unused */, String_t* ___setId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::LoadAchievements()
extern "C"  void GameCenterManager_LoadAchievements_m3262597453 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::IssueAchievementChallenge(System.String,System.String,System.String[])
extern "C"  void GameCenterManager_IssueAchievementChallenge_m1748835438 (Il2CppObject * __this /* static, unused */, String_t* ___achievementId0, String_t* ___message1, StringU5BU5D_t1642385972* ___playerIds2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::IssueAchievementChallenge(System.String,System.String)
extern "C"  void GameCenterManager_IssueAchievementChallenge_m1838288474 (Il2CppObject * __this /* static, unused */, String_t* ___achievementId0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ShowAchievements()
extern "C"  void GameCenterManager_ShowAchievements_m2958253366 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ResetAchievements()
extern "C"  void GameCenterManager_ResetAchievements_m2434559496 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::SubmitAchievement(System.Single,System.String)
extern "C"  void GameCenterManager_SubmitAchievement_m3100486707 (Il2CppObject * __this /* static, unused */, float ___percent0, String_t* ___achievementId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::SubmitAchievementNoCache(System.Single,System.String)
extern "C"  void GameCenterManager_SubmitAchievementNoCache_m1635404662 (Il2CppObject * __this /* static, unused */, float ___percent0, String_t* ___achievementId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::SubmitAchievement(System.Single,System.String,System.Boolean)
extern "C"  void GameCenterManager_SubmitAchievement_m3899097204 (Il2CppObject * __this /* static, unused */, float ___percent0, String_t* ___achievementId1, bool ___isCompleteNotification2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameCenterManager::GetAchievementProgress(System.String)
extern "C"  float GameCenterManager_GetAchievementProgress_m1021320515 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_AchievementTemplate GameCenterManager::GetAchievement(System.String)
extern "C"  GK_AchievementTemplate_t2296152240 * GameCenterManager_GetAchievement_m3900604247 (Il2CppObject * __this /* static, unused */, String_t* ___achievementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_Leaderboard GameCenterManager::GetLeaderboard(System.String)
extern "C"  GK_Leaderboard_t156446466 * GameCenterManager_GetLeaderboard_m3734423211 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_Player GameCenterManager::GetPlayerById(System.String)
extern "C"  GK_Player_t2782008294 * GameCenterManager_GetPlayerById_m2476615593 (Il2CppObject * __this /* static, unused */, String_t* ___playerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::SendFriendRequest(GK_FriendRequest,System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<System.String>)
extern "C"  void GameCenterManager_SendFriendRequest_m1713443724 (Il2CppObject * __this /* static, unused */, GK_FriendRequest_t4101620808 * ___request0, List_1_t1398341365 * ___emails1, List_1_t1398341365 * ___players2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GK_AchievementTemplate> GameCenterManager::get_Achievements()
extern "C"  List_1_t1665273372 * GameCenterManager_get_Achievements_m432646139 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GK_Leaderboard> GameCenterManager::get_Leaderboards()
extern "C"  List_1_t3820534894 * GameCenterManager_get_Leaderboards_m3223598799 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,GK_Player> GameCenterManager::get_Players()
extern "C"  Dictionary_2_t401820260 * GameCenterManager_get_Players_m91614910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_Player GameCenterManager::get_Player()
extern "C"  GK_Player_t2782008294 * GameCenterManager_get_Player_m3574869014 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameCenterManager::get_IsInitialized()
extern "C"  bool GameCenterManager_get_IsInitialized_m638957368 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GK_LeaderboardSet> GameCenterManager::get_LeaderboardSets()
extern "C"  List_1_t3669402526 * GameCenterManager_get_LeaderboardSets_m881848407 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameCenterManager::get_IsPlayerAuthenticated()
extern "C"  bool GameCenterManager_get_IsPlayerAuthenticated_m1875190954 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameCenterManager::get_IsAchievementsInfoLoaded()
extern "C"  bool GameCenterManager_get_IsAchievementsInfoLoaded_m1777978969 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> GameCenterManager::get_FriendsList()
extern "C"  List_1_t1398341365 * GameCenterManager_get_FriendsList_m1765526358 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameCenterManager::get_IsPlayerUnderage()
extern "C"  bool GameCenterManager_get_IsPlayerUnderage_m2302893352 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::OnLoaderBoardInfoRetrivedFail(System.String)
extern "C"  void GameCenterManager_OnLoaderBoardInfoRetrivedFail_m3182515910 (GameCenterManager_t1487113918 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::OnLoaderBoardInfoRetrived(System.String)
extern "C"  void GameCenterManager_OnLoaderBoardInfoRetrived_m3073192158 (GameCenterManager_t1487113918 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onScoreSubmittedEvent(System.String)
extern "C"  void GameCenterManager_onScoreSubmittedEvent_m865468069 (GameCenterManager_t1487113918 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onScoreSubmittedFailed(System.String)
extern "C"  void GameCenterManager_onScoreSubmittedFailed_m456333558 (GameCenterManager_t1487113918 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::OnLeaderboardScoreListLoaded(System.String)
extern "C"  void GameCenterManager_OnLeaderboardScoreListLoaded_m4119410314 (GameCenterManager_t1487113918 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::OnLeaderboardScoreListLoadFailed(System.String)
extern "C"  void GameCenterManager_OnLeaderboardScoreListLoadFailed_m1671174162 (GameCenterManager_t1487113918 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAchievementsReset(System.String)
extern "C"  void GameCenterManager_onAchievementsReset_m2973776477 (GameCenterManager_t1487113918 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAchievementsResetFailed(System.String)
extern "C"  void GameCenterManager_onAchievementsResetFailed_m628189892 (GameCenterManager_t1487113918 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAchievementProgressChanged(System.String)
extern "C"  void GameCenterManager_onAchievementProgressChanged_m4103207184 (GameCenterManager_t1487113918 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAchievementsLoaded(System.String)
extern "C"  void GameCenterManager_onAchievementsLoaded_m678038071 (GameCenterManager_t1487113918 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAchievementsLoadedFailed(System.String)
extern "C"  void GameCenterManager_onAchievementsLoadedFailed_m2113411500 (GameCenterManager_t1487113918 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAuthenticateLocalPlayer(System.String)
extern "C"  void GameCenterManager_onAuthenticateLocalPlayer_m2266709335 (GameCenterManager_t1487113918 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::onAuthenticationFailed(System.String)
extern "C"  void GameCenterManager_onAuthenticationFailed_m1737671407 (GameCenterManager_t1487113918 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::OnUserInfoLoadedEvent(System.String)
extern "C"  void GameCenterManager_OnUserInfoLoadedEvent_m4230874916 (GameCenterManager_t1487113918 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::OnUserInfoLoadFailedEvent(System.String)
extern "C"  void GameCenterManager_OnUserInfoLoadFailedEvent_m190103480 (GameCenterManager_t1487113918 * __this, String_t* ___playerId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::OnUserPhotoLoadedEvent(System.String)
extern "C"  void GameCenterManager_OnUserPhotoLoadedEvent_m2237577856 (GameCenterManager_t1487113918 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::OnUserPhotoLoadFailedEvent(System.String)
extern "C"  void GameCenterManager_OnUserPhotoLoadFailedEvent_m1345442704 (GameCenterManager_t1487113918 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::OnFriendListLoadedEvent(System.String)
extern "C"  void GameCenterManager_OnFriendListLoadedEvent_m2009848707 (GameCenterManager_t1487113918 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::OnFriendListLoadFailEvent(System.String)
extern "C"  void GameCenterManager_OnFriendListLoadFailEvent_m197533898 (GameCenterManager_t1487113918 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::OnGameCenterViewDismissedEvent(System.String)
extern "C"  void GameCenterManager_OnGameCenterViewDismissedEvent_m3290227331 (GameCenterManager_t1487113918 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::VerificationSignatureRetrieveFailed(System.String)
extern "C"  void GameCenterManager_VerificationSignatureRetrieveFailed_m416030397 (GameCenterManager_t1487113918 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::VerificationSignatureRetrieved(System.String)
extern "C"  void GameCenterManager_VerificationSignatureRetrieved_m3282596000 (GameCenterManager_t1487113918 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::SaveLocalProgress(GK_AchievementTemplate)
extern "C"  void GameCenterManager_SaveLocalProgress_m3017032444 (GameCenterManager_t1487113918 * __this, GK_AchievementTemplate_t2296152240 * ___tpl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ResetStoredProgress()
extern "C"  void GameCenterManager_ResetStoredProgress_m393819340 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::SaveAchievementProgress(System.String,System.Single)
extern "C"  void GameCenterManager_SaveAchievementProgress_m992985035 (Il2CppObject * __this /* static, unused */, String_t* ___achievementId0, float ___progress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GameCenterManager::GetStoredAchievementProgress(System.String)
extern "C"  float GameCenterManager_GetStoredAchievementProgress_m424995270 (Il2CppObject * __this /* static, unused */, String_t* ___achievementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ISN_OnLBSetsLoaded(System.String)
extern "C"  void GameCenterManager_ISN_OnLBSetsLoaded_m2800643431 (GameCenterManager_t1487113918 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ISN_OnLBSetsLoadFailed(System.String)
extern "C"  void GameCenterManager_ISN_OnLBSetsLoadFailed_m4020358673 (GameCenterManager_t1487113918 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ISN_OnLBSetsBoardsLoadFailed(System.String)
extern "C"  void GameCenterManager_ISN_OnLBSetsBoardsLoadFailed_m1582600992 (GameCenterManager_t1487113918 * __this, String_t* ___identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::ISN_OnLBSetsBoardsLoaded(System.String)
extern "C"  void GameCenterManager_ISN_OnLBSetsBoardsLoaded_m3526487324 (GameCenterManager_t1487113918 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::DispatchLeaderboardUpdateEvent(GK_LeaderboardResult,System.Boolean)
extern "C"  void GameCenterManager_DispatchLeaderboardUpdateEvent_m1473596267 (Il2CppObject * __this /* static, unused */, GK_LeaderboardResult_t866080833 * ___result0, bool ___isInternal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GK_TBM_Participant> GameCenterManager::ParseParticipantsData(System.String[],System.Int32)
extern "C"  List_1_t3173076222 * GameCenterManager_ParseParticipantsData_m1917808284 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___data0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_TBM_Participant GameCenterManager::ParseParticipanData(System.String[],System.Int32)
extern "C"  GK_TBM_Participant_t3803955090 * GameCenterManager_ParseParticipanData_m2746472811 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___data0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnAuthFinished>m__6(SA.Common.Models.Result)
extern "C"  void GameCenterManager_U3COnAuthFinishedU3Em__6_m3510362889 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnScoreSubmitted>m__7(GK_LeaderboardResult)
extern "C"  void GameCenterManager_U3COnScoreSubmittedU3Em__7_m2976957916 (Il2CppObject * __this /* static, unused */, GK_LeaderboardResult_t866080833 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnScoresListLoaded>m__8(GK_LeaderboardResult)
extern "C"  void GameCenterManager_U3COnScoresListLoadedU3Em__8_m4200609610 (Il2CppObject * __this /* static, unused */, GK_LeaderboardResult_t866080833 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnLeadrboardInfoLoaded>m__9(GK_LeaderboardResult)
extern "C"  void GameCenterManager_U3COnLeadrboardInfoLoadedU3Em__9_m2282752008 (Il2CppObject * __this /* static, unused */, GK_LeaderboardResult_t866080833 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnLeaderboardSetsInfoLoaded>m__A(SA.Common.Models.Result)
extern "C"  void GameCenterManager_U3COnLeaderboardSetsInfoLoadedU3Em__A_m3836104205 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnAchievementsReset>m__B(SA.Common.Models.Result)
extern "C"  void GameCenterManager_U3COnAchievementsResetU3Em__B_m1404983730 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnAchievementsLoaded>m__C(SA.Common.Models.Result)
extern "C"  void GameCenterManager_U3COnAchievementsLoadedU3Em__C_m2457495953 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnAchievementsProgress>m__D(GK_AchievementProgressResult)
extern "C"  void GameCenterManager_U3COnAchievementsProgressU3Em__D_m581100504 (Il2CppObject * __this /* static, unused */, GK_AchievementProgressResult_t3539574352 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnGameCenterViewDismissed>m__E()
extern "C"  void GameCenterManager_U3COnGameCenterViewDismissedU3Em__E_m1949358423 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnFriendsListLoaded>m__F(SA.Common.Models.Result)
extern "C"  void GameCenterManager_U3COnFriendsListLoadedU3Em__F_m4128360379 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnUserInfoLoaded>m__10(GK_UserInfoLoadResult)
extern "C"  void GameCenterManager_U3COnUserInfoLoadedU3Em__10_m838561987 (Il2CppObject * __this /* static, unused */, GK_UserInfoLoadResult_t1177841233 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager::<OnPlayerSignatureRetrieveResult>m__11(GK_PlayerSignatureResult)
extern "C"  void GameCenterManager_U3COnPlayerSignatureRetrieveResultU3Em__11_m2893674806 (Il2CppObject * __this /* static, unused */, GK_PlayerSignatureResult_t13769479 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
