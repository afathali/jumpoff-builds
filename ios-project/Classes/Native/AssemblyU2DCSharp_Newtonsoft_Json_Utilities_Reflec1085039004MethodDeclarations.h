﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ReflectionUtils/<GetFieldsAndProperties>c__AnonStorey35
struct U3CGetFieldsAndPropertiesU3Ec__AnonStorey35_t1085039004;
// System.Reflection.MemberInfo
struct MemberInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"

// System.Void Newtonsoft.Json.Utilities.ReflectionUtils/<GetFieldsAndProperties>c__AnonStorey35::.ctor()
extern "C"  void U3CGetFieldsAndPropertiesU3Ec__AnonStorey35__ctor_m1387174637 (U3CGetFieldsAndPropertiesU3Ec__AnonStorey35_t1085039004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils/<GetFieldsAndProperties>c__AnonStorey35::<>m__FF(System.Reflection.MemberInfo)
extern "C"  bool U3CGetFieldsAndPropertiesU3Ec__AnonStorey35_U3CU3Em__FF_m3664950418 (U3CGetFieldsAndPropertiesU3Ec__AnonStorey35_t1085039004 * __this, MemberInfo_t * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
