﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TW_FollowersIdsRequest
struct TW_FollowersIdsRequest_t261436078;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TW_FollowersIdsRequest::.ctor()
extern "C"  void TW_FollowersIdsRequest__ctor_m2483649145 (TW_FollowersIdsRequest_t261436078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TW_FollowersIdsRequest TW_FollowersIdsRequest::Create()
extern "C"  TW_FollowersIdsRequest_t261436078 * TW_FollowersIdsRequest_Create_m3572787012 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_FollowersIdsRequest::Awake()
extern "C"  void TW_FollowersIdsRequest_Awake_m1852550014 (TW_FollowersIdsRequest_t261436078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_FollowersIdsRequest::OnResult(System.String)
extern "C"  void TW_FollowersIdsRequest_OnResult_m711567955 (TW_FollowersIdsRequest_t261436078 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
