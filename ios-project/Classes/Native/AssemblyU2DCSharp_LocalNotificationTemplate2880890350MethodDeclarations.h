﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalNotificationTemplate
struct LocalNotificationTemplate_t2880890350;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void LocalNotificationTemplate::.ctor(System.String)
extern "C"  void LocalNotificationTemplate__ctor_m2396898745 (LocalNotificationTemplate_t2880890350 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalNotificationTemplate::.ctor(System.Int32,System.String,System.String,System.DateTime)
extern "C"  void LocalNotificationTemplate__ctor_m2961307240 (LocalNotificationTemplate_t2880890350 * __this, int32_t ___nId0, String_t* ___ttl1, String_t* ___msg2, DateTime_t693205669  ___date3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LocalNotificationTemplate::get_id()
extern "C"  int32_t LocalNotificationTemplate_get_id_m1447488205 (LocalNotificationTemplate_t2880890350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalNotificationTemplate::get_title()
extern "C"  String_t* LocalNotificationTemplate_get_title_m1177807735 (LocalNotificationTemplate_t2880890350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalNotificationTemplate::get_message()
extern "C"  String_t* LocalNotificationTemplate_get_message_m479312188 (LocalNotificationTemplate_t2880890350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime LocalNotificationTemplate::get_fireDate()
extern "C"  DateTime_t693205669  LocalNotificationTemplate_get_fireDate_m3059212767 (LocalNotificationTemplate_t2880890350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalNotificationTemplate::get_SerializedString()
extern "C"  String_t* LocalNotificationTemplate_get_SerializedString_m2528955816 (LocalNotificationTemplate_t2880890350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalNotificationTemplate::get_IsFired()
extern "C"  bool LocalNotificationTemplate_get_IsFired_m1192291784 (LocalNotificationTemplate_t2880890350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
