﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen321133289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidNotificationManager
struct  AndroidNotificationManager_t3325256667  : public SA_Singleton_OLD_1_t321133289
{
public:
	// System.Action`1<System.Int32> AndroidNotificationManager::OnNotificationIdLoaded
	Action_1_t1873676830 * ___OnNotificationIdLoaded_9;

public:
	inline static int32_t get_offset_of_OnNotificationIdLoaded_9() { return static_cast<int32_t>(offsetof(AndroidNotificationManager_t3325256667, ___OnNotificationIdLoaded_9)); }
	inline Action_1_t1873676830 * get_OnNotificationIdLoaded_9() const { return ___OnNotificationIdLoaded_9; }
	inline Action_1_t1873676830 ** get_address_of_OnNotificationIdLoaded_9() { return &___OnNotificationIdLoaded_9; }
	inline void set_OnNotificationIdLoaded_9(Action_1_t1873676830 * value)
	{
		___OnNotificationIdLoaded_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnNotificationIdLoaded_9, value);
	}
};

struct AndroidNotificationManager_t3325256667_StaticFields
{
public:
	// System.Action`1<System.Int32> AndroidNotificationManager::<>f__am$cache1
	Action_1_t1873676830 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(AndroidNotificationManager_t3325256667_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Action_1_t1873676830 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Action_1_t1873676830 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Action_1_t1873676830 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
