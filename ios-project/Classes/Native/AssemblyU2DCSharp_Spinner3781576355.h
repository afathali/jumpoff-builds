﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Char[]
struct CharU5BU5D_t1328083999;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spinner
struct  Spinner_t3781576355  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button Spinner::btn_up
	Button_t2872111280 * ___btn_up_2;
	// UnityEngine.UI.Button Spinner::btn_down
	Button_t2872111280 * ___btn_down_3;
	// UnityEngine.UI.Text Spinner::text
	Text_t356221433 * ___text_4;
	// System.Boolean Spinner::alphabet
	bool ___alphabet_5;
	// System.Int32 Spinner::val
	int32_t ___val_6;
	// System.Int32 Spinner::minVal
	int32_t ___minVal_7;
	// System.Int32 Spinner::maxVal
	int32_t ___maxVal_8;
	// System.Boolean Spinner::inited
	bool ___inited_9;

public:
	inline static int32_t get_offset_of_btn_up_2() { return static_cast<int32_t>(offsetof(Spinner_t3781576355, ___btn_up_2)); }
	inline Button_t2872111280 * get_btn_up_2() const { return ___btn_up_2; }
	inline Button_t2872111280 ** get_address_of_btn_up_2() { return &___btn_up_2; }
	inline void set_btn_up_2(Button_t2872111280 * value)
	{
		___btn_up_2 = value;
		Il2CppCodeGenWriteBarrier(&___btn_up_2, value);
	}

	inline static int32_t get_offset_of_btn_down_3() { return static_cast<int32_t>(offsetof(Spinner_t3781576355, ___btn_down_3)); }
	inline Button_t2872111280 * get_btn_down_3() const { return ___btn_down_3; }
	inline Button_t2872111280 ** get_address_of_btn_down_3() { return &___btn_down_3; }
	inline void set_btn_down_3(Button_t2872111280 * value)
	{
		___btn_down_3 = value;
		Il2CppCodeGenWriteBarrier(&___btn_down_3, value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(Spinner_t3781576355, ___text_4)); }
	inline Text_t356221433 * get_text_4() const { return ___text_4; }
	inline Text_t356221433 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_t356221433 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier(&___text_4, value);
	}

	inline static int32_t get_offset_of_alphabet_5() { return static_cast<int32_t>(offsetof(Spinner_t3781576355, ___alphabet_5)); }
	inline bool get_alphabet_5() const { return ___alphabet_5; }
	inline bool* get_address_of_alphabet_5() { return &___alphabet_5; }
	inline void set_alphabet_5(bool value)
	{
		___alphabet_5 = value;
	}

	inline static int32_t get_offset_of_val_6() { return static_cast<int32_t>(offsetof(Spinner_t3781576355, ___val_6)); }
	inline int32_t get_val_6() const { return ___val_6; }
	inline int32_t* get_address_of_val_6() { return &___val_6; }
	inline void set_val_6(int32_t value)
	{
		___val_6 = value;
	}

	inline static int32_t get_offset_of_minVal_7() { return static_cast<int32_t>(offsetof(Spinner_t3781576355, ___minVal_7)); }
	inline int32_t get_minVal_7() const { return ___minVal_7; }
	inline int32_t* get_address_of_minVal_7() { return &___minVal_7; }
	inline void set_minVal_7(int32_t value)
	{
		___minVal_7 = value;
	}

	inline static int32_t get_offset_of_maxVal_8() { return static_cast<int32_t>(offsetof(Spinner_t3781576355, ___maxVal_8)); }
	inline int32_t get_maxVal_8() const { return ___maxVal_8; }
	inline int32_t* get_address_of_maxVal_8() { return &___maxVal_8; }
	inline void set_maxVal_8(int32_t value)
	{
		___maxVal_8 = value;
	}

	inline static int32_t get_offset_of_inited_9() { return static_cast<int32_t>(offsetof(Spinner_t3781576355, ___inited_9)); }
	inline bool get_inited_9() const { return ___inited_9; }
	inline bool* get_address_of_inited_9() { return &___inited_9; }
	inline void set_inited_9(bool value)
	{
		___inited_9 = value;
	}
};

struct Spinner_t3781576355_StaticFields
{
public:
	// System.Char[] Spinner::alpha
	CharU5BU5D_t1328083999* ___alpha_10;

public:
	inline static int32_t get_offset_of_alpha_10() { return static_cast<int32_t>(offsetof(Spinner_t3781576355_StaticFields, ___alpha_10)); }
	inline CharU5BU5D_t1328083999* get_alpha_10() const { return ___alpha_10; }
	inline CharU5BU5D_t1328083999** get_address_of_alpha_10() { return &___alpha_10; }
	inline void set_alpha_10(CharU5BU5D_t1328083999* value)
	{
		___alpha_10 = value;
		Il2CppCodeGenWriteBarrier(&___alpha_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
