﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FB_Permission
struct FB_Permission_t1872772122;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_FB_PermissionStatus2970074352.h"

// System.Void FB_Permission::.ctor(System.String,FB_PermissionStatus)
extern "C"  void FB_Permission__ctor_m846174609 (FB_Permission_t1872772122 * __this, String_t* ___permission0, int32_t ___status1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_Permission::get_Name()
extern "C"  String_t* FB_Permission_get_Name_m211824748 (FB_Permission_t1872772122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FB_PermissionStatus FB_Permission::get_Status()
extern "C"  int32_t FB_Permission_get_Status_m3716728249 (FB_Permission_t1872772122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
