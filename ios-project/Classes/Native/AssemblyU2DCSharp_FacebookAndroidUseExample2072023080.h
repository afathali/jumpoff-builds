﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DefaultPreviewButton[]
struct DefaultPreviewButtonU5BU5D_t3800739864;
// DefaultPreviewButton
struct DefaultPreviewButton_t12674677;
// SA_Texture
struct SA_Texture_t3361108684;
// SA_Label
struct SA_Label_t226960149;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FacebookAndroidUseExample
struct  FacebookAndroidUseExample_t2072023080  : public MonoBehaviour_t1158329972
{
public:
	// DefaultPreviewButton[] FacebookAndroidUseExample::ConnectionDependedntButtons
	DefaultPreviewButtonU5BU5D_t3800739864* ___ConnectionDependedntButtons_5;
	// DefaultPreviewButton FacebookAndroidUseExample::connectButton
	DefaultPreviewButton_t12674677 * ___connectButton_6;
	// SA_Texture FacebookAndroidUseExample::avatar
	SA_Texture_t3361108684 * ___avatar_7;
	// SA_Label FacebookAndroidUseExample::Location
	SA_Label_t226960149 * ___Location_8;
	// SA_Label FacebookAndroidUseExample::Language
	SA_Label_t226960149 * ___Language_9;
	// SA_Label FacebookAndroidUseExample::Mail
	SA_Label_t226960149 * ___Mail_10;
	// SA_Label FacebookAndroidUseExample::Name
	SA_Label_t226960149 * ___Name_11;
	// SA_Label FacebookAndroidUseExample::f1
	SA_Label_t226960149 * ___f1_12;
	// SA_Label FacebookAndroidUseExample::f2
	SA_Label_t226960149 * ___f2_13;
	// SA_Texture FacebookAndroidUseExample::fi1
	SA_Texture_t3361108684 * ___fi1_14;
	// SA_Texture FacebookAndroidUseExample::fi2
	SA_Texture_t3361108684 * ___fi2_15;
	// UnityEngine.Texture2D FacebookAndroidUseExample::ImageToShare
	Texture2D_t3542995729 * ___ImageToShare_16;
	// UnityEngine.GameObject FacebookAndroidUseExample::friends
	GameObject_t1756533147 * ___friends_17;
	// System.Int32 FacebookAndroidUseExample::startScore
	int32_t ___startScore_18;
	// System.String FacebookAndroidUseExample::UNION_ASSETS_PAGE_ID
	String_t* ___UNION_ASSETS_PAGE_ID_19;

public:
	inline static int32_t get_offset_of_ConnectionDependedntButtons_5() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___ConnectionDependedntButtons_5)); }
	inline DefaultPreviewButtonU5BU5D_t3800739864* get_ConnectionDependedntButtons_5() const { return ___ConnectionDependedntButtons_5; }
	inline DefaultPreviewButtonU5BU5D_t3800739864** get_address_of_ConnectionDependedntButtons_5() { return &___ConnectionDependedntButtons_5; }
	inline void set_ConnectionDependedntButtons_5(DefaultPreviewButtonU5BU5D_t3800739864* value)
	{
		___ConnectionDependedntButtons_5 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionDependedntButtons_5, value);
	}

	inline static int32_t get_offset_of_connectButton_6() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___connectButton_6)); }
	inline DefaultPreviewButton_t12674677 * get_connectButton_6() const { return ___connectButton_6; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_connectButton_6() { return &___connectButton_6; }
	inline void set_connectButton_6(DefaultPreviewButton_t12674677 * value)
	{
		___connectButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___connectButton_6, value);
	}

	inline static int32_t get_offset_of_avatar_7() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___avatar_7)); }
	inline SA_Texture_t3361108684 * get_avatar_7() const { return ___avatar_7; }
	inline SA_Texture_t3361108684 ** get_address_of_avatar_7() { return &___avatar_7; }
	inline void set_avatar_7(SA_Texture_t3361108684 * value)
	{
		___avatar_7 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_7, value);
	}

	inline static int32_t get_offset_of_Location_8() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___Location_8)); }
	inline SA_Label_t226960149 * get_Location_8() const { return ___Location_8; }
	inline SA_Label_t226960149 ** get_address_of_Location_8() { return &___Location_8; }
	inline void set_Location_8(SA_Label_t226960149 * value)
	{
		___Location_8 = value;
		Il2CppCodeGenWriteBarrier(&___Location_8, value);
	}

	inline static int32_t get_offset_of_Language_9() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___Language_9)); }
	inline SA_Label_t226960149 * get_Language_9() const { return ___Language_9; }
	inline SA_Label_t226960149 ** get_address_of_Language_9() { return &___Language_9; }
	inline void set_Language_9(SA_Label_t226960149 * value)
	{
		___Language_9 = value;
		Il2CppCodeGenWriteBarrier(&___Language_9, value);
	}

	inline static int32_t get_offset_of_Mail_10() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___Mail_10)); }
	inline SA_Label_t226960149 * get_Mail_10() const { return ___Mail_10; }
	inline SA_Label_t226960149 ** get_address_of_Mail_10() { return &___Mail_10; }
	inline void set_Mail_10(SA_Label_t226960149 * value)
	{
		___Mail_10 = value;
		Il2CppCodeGenWriteBarrier(&___Mail_10, value);
	}

	inline static int32_t get_offset_of_Name_11() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___Name_11)); }
	inline SA_Label_t226960149 * get_Name_11() const { return ___Name_11; }
	inline SA_Label_t226960149 ** get_address_of_Name_11() { return &___Name_11; }
	inline void set_Name_11(SA_Label_t226960149 * value)
	{
		___Name_11 = value;
		Il2CppCodeGenWriteBarrier(&___Name_11, value);
	}

	inline static int32_t get_offset_of_f1_12() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___f1_12)); }
	inline SA_Label_t226960149 * get_f1_12() const { return ___f1_12; }
	inline SA_Label_t226960149 ** get_address_of_f1_12() { return &___f1_12; }
	inline void set_f1_12(SA_Label_t226960149 * value)
	{
		___f1_12 = value;
		Il2CppCodeGenWriteBarrier(&___f1_12, value);
	}

	inline static int32_t get_offset_of_f2_13() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___f2_13)); }
	inline SA_Label_t226960149 * get_f2_13() const { return ___f2_13; }
	inline SA_Label_t226960149 ** get_address_of_f2_13() { return &___f2_13; }
	inline void set_f2_13(SA_Label_t226960149 * value)
	{
		___f2_13 = value;
		Il2CppCodeGenWriteBarrier(&___f2_13, value);
	}

	inline static int32_t get_offset_of_fi1_14() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___fi1_14)); }
	inline SA_Texture_t3361108684 * get_fi1_14() const { return ___fi1_14; }
	inline SA_Texture_t3361108684 ** get_address_of_fi1_14() { return &___fi1_14; }
	inline void set_fi1_14(SA_Texture_t3361108684 * value)
	{
		___fi1_14 = value;
		Il2CppCodeGenWriteBarrier(&___fi1_14, value);
	}

	inline static int32_t get_offset_of_fi2_15() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___fi2_15)); }
	inline SA_Texture_t3361108684 * get_fi2_15() const { return ___fi2_15; }
	inline SA_Texture_t3361108684 ** get_address_of_fi2_15() { return &___fi2_15; }
	inline void set_fi2_15(SA_Texture_t3361108684 * value)
	{
		___fi2_15 = value;
		Il2CppCodeGenWriteBarrier(&___fi2_15, value);
	}

	inline static int32_t get_offset_of_ImageToShare_16() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___ImageToShare_16)); }
	inline Texture2D_t3542995729 * get_ImageToShare_16() const { return ___ImageToShare_16; }
	inline Texture2D_t3542995729 ** get_address_of_ImageToShare_16() { return &___ImageToShare_16; }
	inline void set_ImageToShare_16(Texture2D_t3542995729 * value)
	{
		___ImageToShare_16 = value;
		Il2CppCodeGenWriteBarrier(&___ImageToShare_16, value);
	}

	inline static int32_t get_offset_of_friends_17() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___friends_17)); }
	inline GameObject_t1756533147 * get_friends_17() const { return ___friends_17; }
	inline GameObject_t1756533147 ** get_address_of_friends_17() { return &___friends_17; }
	inline void set_friends_17(GameObject_t1756533147 * value)
	{
		___friends_17 = value;
		Il2CppCodeGenWriteBarrier(&___friends_17, value);
	}

	inline static int32_t get_offset_of_startScore_18() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___startScore_18)); }
	inline int32_t get_startScore_18() const { return ___startScore_18; }
	inline int32_t* get_address_of_startScore_18() { return &___startScore_18; }
	inline void set_startScore_18(int32_t value)
	{
		___startScore_18 = value;
	}

	inline static int32_t get_offset_of_UNION_ASSETS_PAGE_ID_19() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080, ___UNION_ASSETS_PAGE_ID_19)); }
	inline String_t* get_UNION_ASSETS_PAGE_ID_19() const { return ___UNION_ASSETS_PAGE_ID_19; }
	inline String_t** get_address_of_UNION_ASSETS_PAGE_ID_19() { return &___UNION_ASSETS_PAGE_ID_19; }
	inline void set_UNION_ASSETS_PAGE_ID_19(String_t* value)
	{
		___UNION_ASSETS_PAGE_ID_19 = value;
		Il2CppCodeGenWriteBarrier(&___UNION_ASSETS_PAGE_ID_19, value);
	}
};

struct FacebookAndroidUseExample_t2072023080_StaticFields
{
public:
	// System.Boolean FacebookAndroidUseExample::IsUserInfoLoaded
	bool ___IsUserInfoLoaded_2;
	// System.Boolean FacebookAndroidUseExample::IsFrindsInfoLoaded
	bool ___IsFrindsInfoLoaded_3;
	// System.Boolean FacebookAndroidUseExample::IsAuntificated
	bool ___IsAuntificated_4;

public:
	inline static int32_t get_offset_of_IsUserInfoLoaded_2() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080_StaticFields, ___IsUserInfoLoaded_2)); }
	inline bool get_IsUserInfoLoaded_2() const { return ___IsUserInfoLoaded_2; }
	inline bool* get_address_of_IsUserInfoLoaded_2() { return &___IsUserInfoLoaded_2; }
	inline void set_IsUserInfoLoaded_2(bool value)
	{
		___IsUserInfoLoaded_2 = value;
	}

	inline static int32_t get_offset_of_IsFrindsInfoLoaded_3() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080_StaticFields, ___IsFrindsInfoLoaded_3)); }
	inline bool get_IsFrindsInfoLoaded_3() const { return ___IsFrindsInfoLoaded_3; }
	inline bool* get_address_of_IsFrindsInfoLoaded_3() { return &___IsFrindsInfoLoaded_3; }
	inline void set_IsFrindsInfoLoaded_3(bool value)
	{
		___IsFrindsInfoLoaded_3 = value;
	}

	inline static int32_t get_offset_of_IsAuntificated_4() { return static_cast<int32_t>(offsetof(FacebookAndroidUseExample_t2072023080_StaticFields, ___IsAuntificated_4)); }
	inline bool get_IsAuntificated_4() const { return ___IsAuntificated_4; }
	inline bool* get_address_of_IsAuntificated_4() { return &___IsAuntificated_4; }
	inline void set_IsAuntificated_4(bool value)
	{
		___IsAuntificated_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
