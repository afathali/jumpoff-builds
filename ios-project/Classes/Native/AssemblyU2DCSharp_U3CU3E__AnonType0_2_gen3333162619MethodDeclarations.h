﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType0`2<System.Object,System.Object>
struct U3CU3E__AnonType0_2_t3333162619;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void <>__AnonType0`2<System.Object,System.Object>::.ctor(<Count>__T,<Members>__T)
extern "C"  void U3CU3E__AnonType0_2__ctor_m1715261235_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, Il2CppObject * ___Count0, Il2CppObject * ___Members1, const MethodInfo* method);
#define U3CU3E__AnonType0_2__ctor_m1715261235(__this, ___Count0, ___Members1, method) ((  void (*) (U3CU3E__AnonType0_2_t3333162619 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType0_2__ctor_m1715261235_gshared)(__this, ___Count0, ___Members1, method)
// <Count>__T <>__AnonType0`2<System.Object,System.Object>::get_Count()
extern "C"  Il2CppObject * U3CU3E__AnonType0_2_get_Count_m281527827_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_2_get_Count_m281527827(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType0_2_t3333162619 *, const MethodInfo*))U3CU3E__AnonType0_2_get_Count_m281527827_gshared)(__this, method)
// <Members>__T <>__AnonType0`2<System.Object,System.Object>::get_Members()
extern "C"  Il2CppObject * U3CU3E__AnonType0_2_get_Members_m4072232531_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_2_get_Members_m4072232531(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType0_2_t3333162619 *, const MethodInfo*))U3CU3E__AnonType0_2_get_Members_m4072232531_gshared)(__this, method)
// System.Boolean <>__AnonType0`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType0_2_Equals_m2151427210_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType0_2_Equals_m2151427210(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType0_2_t3333162619 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType0_2_Equals_m2151427210_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType0`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType0_2_GetHashCode_m2678831190_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_2_GetHashCode_m2678831190(__this, method) ((  int32_t (*) (U3CU3E__AnonType0_2_t3333162619 *, const MethodInfo*))U3CU3E__AnonType0_2_GetHashCode_m2678831190_gshared)(__this, method)
// System.String <>__AnonType0`2<System.Object,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType0_2_ToString_m2057022320_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_2_ToString_m2057022320(__this, method) ((  String_t* (*) (U3CU3E__AnonType0_2_t3333162619 *, const MethodInfo*))U3CU3E__AnonType0_2_ToString_m2057022320_gshared)(__this, method)
