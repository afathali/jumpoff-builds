﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_SaveResult
struct GK_SaveResult_t3946576453;
// GK_SavedGame
struct GK_SavedGame_t3320093620;
// System.String
struct String_t;
// SA.Common.Models.Error
struct Error_t445207774;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SavedGame3320093620.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"

// System.Void GK_SaveResult::.ctor(GK_SavedGame)
extern "C"  void GK_SaveResult__ctor_m3136740824 (GK_SaveResult_t3946576453 * __this, GK_SavedGame_t3320093620 * ___save0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_SaveResult::.ctor(System.String)
extern "C"  void GK_SaveResult__ctor_m3351078822 (GK_SaveResult_t3946576453 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_SaveResult::.ctor(SA.Common.Models.Error)
extern "C"  void GK_SaveResult__ctor_m2176866095 (GK_SaveResult_t3946576453 * __this, Error_t445207774 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_SavedGame GK_SaveResult::get_SavedGame()
extern "C"  GK_SavedGame_t3320093620 * GK_SaveResult_get_SavedGame_m3343241371 (GK_SaveResult_t3946576453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
