﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlaySavedGamesManager
struct GooglePlaySavedGamesManager_t1475146022;
// System.Action
struct Action_t3226471752;
// System.Action`1<GooglePlayResult>
struct Action_1_t2899269018;
// System.Action`1<GP_SpanshotLoadResult>
struct Action_1_t2065103831;
// System.Action`1<GP_SnapshotConflict>
struct Action_1_t3804386152;
// System.Action`1<GP_DeleteSnapshotResult>
struct Action_1_t3108589392;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.List`1<GP_SnapshotMeta>
struct List_1_t723900571;
// GooglePlayResult
struct GooglePlayResult_t3097469636;
// GP_SpanshotLoadResult
struct GP_SpanshotLoadResult_t2263304449;
// GP_SnapshotConflict
struct GP_SnapshotConflict_t4002586770;
// GP_DeleteSnapshotResult
struct GP_DeleteSnapshotResult_t3306790010;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharp_GooglePlayResult3097469636.h"
#include "AssemblyU2DCSharp_GP_SpanshotLoadResult2263304449.h"
#include "AssemblyU2DCSharp_GP_SnapshotConflict4002586770.h"
#include "AssemblyU2DCSharp_GP_DeleteSnapshotResult3306790010.h"

// System.Void GooglePlaySavedGamesManager::.ctor()
extern "C"  void GooglePlaySavedGamesManager__ctor_m2921444657 (GooglePlaySavedGamesManager_t1475146022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::.cctor()
extern "C"  void GooglePlaySavedGamesManager__cctor_m3241010704 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::add_ActionNewGameSaveRequest(System.Action)
extern "C"  void GooglePlaySavedGamesManager_add_ActionNewGameSaveRequest_m3755337912 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::remove_ActionNewGameSaveRequest(System.Action)
extern "C"  void GooglePlaySavedGamesManager_remove_ActionNewGameSaveRequest_m2317422101 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::add_ActionAvailableGameSavesLoaded(System.Action`1<GooglePlayResult>)
extern "C"  void GooglePlaySavedGamesManager_add_ActionAvailableGameSavesLoaded_m585547825 (Il2CppObject * __this /* static, unused */, Action_1_t2899269018 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::remove_ActionAvailableGameSavesLoaded(System.Action`1<GooglePlayResult>)
extern "C"  void GooglePlaySavedGamesManager_remove_ActionAvailableGameSavesLoaded_m1868372266 (Il2CppObject * __this /* static, unused */, Action_1_t2899269018 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::add_ActionGameSaveLoaded(System.Action`1<GP_SpanshotLoadResult>)
extern "C"  void GooglePlaySavedGamesManager_add_ActionGameSaveLoaded_m3165792474 (Il2CppObject * __this /* static, unused */, Action_1_t2065103831 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::remove_ActionGameSaveLoaded(System.Action`1<GP_SpanshotLoadResult>)
extern "C"  void GooglePlaySavedGamesManager_remove_ActionGameSaveLoaded_m359319615 (Il2CppObject * __this /* static, unused */, Action_1_t2065103831 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::add_ActionGameSaveResult(System.Action`1<GP_SpanshotLoadResult>)
extern "C"  void GooglePlaySavedGamesManager_add_ActionGameSaveResult_m817124854 (Il2CppObject * __this /* static, unused */, Action_1_t2065103831 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::remove_ActionGameSaveResult(System.Action`1<GP_SpanshotLoadResult>)
extern "C"  void GooglePlaySavedGamesManager_remove_ActionGameSaveResult_m1934398309 (Il2CppObject * __this /* static, unused */, Action_1_t2065103831 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::add_ActionConflict(System.Action`1<GP_SnapshotConflict>)
extern "C"  void GooglePlaySavedGamesManager_add_ActionConflict_m21373239 (Il2CppObject * __this /* static, unused */, Action_1_t3804386152 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::remove_ActionConflict(System.Action`1<GP_SnapshotConflict>)
extern "C"  void GooglePlaySavedGamesManager_remove_ActionConflict_m1145548516 (Il2CppObject * __this /* static, unused */, Action_1_t3804386152 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::add_ActionGameSaveRemoved(System.Action`1<GP_DeleteSnapshotResult>)
extern "C"  void GooglePlaySavedGamesManager_add_ActionGameSaveRemoved_m1267036442 (Il2CppObject * __this /* static, unused */, Action_1_t3108589392 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::remove_ActionGameSaveRemoved(System.Action`1<GP_DeleteSnapshotResult>)
extern "C"  void GooglePlaySavedGamesManager_remove_ActionGameSaveRemoved_m1718330669 (Il2CppObject * __this /* static, unused */, Action_1_t3108589392 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::Awake()
extern "C"  void GooglePlaySavedGamesManager_Awake_m3064255858 (GooglePlaySavedGamesManager_t1475146022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::ShowSavedGamesUI(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  void GooglePlaySavedGamesManager_ShowSavedGamesUI_m1588428149 (GooglePlaySavedGamesManager_t1475146022 * __this, String_t* ___title0, int32_t ___maxNumberOfSavedGamesToShow1, bool ___allowAddButton2, bool ___allowDelete3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::CreateNewSnapshot(System.String,System.String,UnityEngine.Texture2D,System.String,System.Int64)
extern "C"  void GooglePlaySavedGamesManager_CreateNewSnapshot_m2959442043 (GooglePlaySavedGamesManager_t1475146022 * __this, String_t* ___name0, String_t* ___description1, Texture2D_t3542995729 * ___coverImage2, String_t* ___spanshotData3, int64_t ___PlayedTime4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::CreateNewSnapshot(System.String,System.String,UnityEngine.Texture2D,System.Byte[],System.Int64)
extern "C"  void GooglePlaySavedGamesManager_CreateNewSnapshot_m1348871648 (GooglePlaySavedGamesManager_t1475146022 * __this, String_t* ___name0, String_t* ___description1, Texture2D_t3542995729 * ___coverImage2, ByteU5BU5D_t3397334013* ___spanshotData3, int64_t ___PlayedTime4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::LoadSpanshotByName(System.String)
extern "C"  void GooglePlaySavedGamesManager_LoadSpanshotByName_m4038169857 (GooglePlaySavedGamesManager_t1475146022 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::DeleteSpanshotByName(System.String)
extern "C"  void GooglePlaySavedGamesManager_DeleteSpanshotByName_m3645464330 (GooglePlaySavedGamesManager_t1475146022 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::LoadAvailableSavedGames()
extern "C"  void GooglePlaySavedGamesManager_LoadAvailableSavedGames_m3405081984 (GooglePlaySavedGamesManager_t1475146022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GP_SnapshotMeta> GooglePlaySavedGamesManager::get_AvailableGameSaves()
extern "C"  List_1_t723900571 * GooglePlaySavedGamesManager_get_AvailableGameSaves_m3167169325 (GooglePlaySavedGamesManager_t1475146022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlaySavedGamesManager::GetBytes(System.String)
extern "C"  ByteU5BU5D_t3397334013* GooglePlaySavedGamesManager_GetBytes_m2804634744 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlaySavedGamesManager::GetString(System.Byte[])
extern "C"  String_t* GooglePlaySavedGamesManager_GetString_m1530539356 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::OnLoadSnapshotsResult(System.String)
extern "C"  void GooglePlaySavedGamesManager_OnLoadSnapshotsResult_m1110159416 (GooglePlaySavedGamesManager_t1475146022 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::OnSavedGamePicked(System.String)
extern "C"  void GooglePlaySavedGamesManager_OnSavedGamePicked_m47270137 (GooglePlaySavedGamesManager_t1475146022 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::OnSavedGameSaveResult(System.String)
extern "C"  void GooglePlaySavedGamesManager_OnSavedGameSaveResult_m625161893 (GooglePlaySavedGamesManager_t1475146022 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::OnConflict(System.String)
extern "C"  void GooglePlaySavedGamesManager_OnConflict_m4202121624 (GooglePlaySavedGamesManager_t1475146022 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::OnNewGameSaveRequest(System.String)
extern "C"  void GooglePlaySavedGamesManager_OnNewGameSaveRequest_m9992772 (GooglePlaySavedGamesManager_t1475146022 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::OnDeleteResult(System.String)
extern "C"  void GooglePlaySavedGamesManager_OnDeleteResult_m371911280 (GooglePlaySavedGamesManager_t1475146022 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::<ActionNewGameSaveRequest>m__68()
extern "C"  void GooglePlaySavedGamesManager_U3CActionNewGameSaveRequestU3Em__68_m2620164644 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::<ActionAvailableGameSavesLoaded>m__69(GooglePlayResult)
extern "C"  void GooglePlaySavedGamesManager_U3CActionAvailableGameSavesLoadedU3Em__69_m2739414793 (Il2CppObject * __this /* static, unused */, GooglePlayResult_t3097469636 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::<ActionGameSaveLoaded>m__6A(GP_SpanshotLoadResult)
extern "C"  void GooglePlaySavedGamesManager_U3CActionGameSaveLoadedU3Em__6A_m2151742128 (Il2CppObject * __this /* static, unused */, GP_SpanshotLoadResult_t2263304449 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::<ActionGameSaveResult>m__6B(GP_SpanshotLoadResult)
extern "C"  void GooglePlaySavedGamesManager_U3CActionGameSaveResultU3Em__6B_m2266771053 (Il2CppObject * __this /* static, unused */, GP_SpanshotLoadResult_t2263304449 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::<ActionConflict>m__6C(GP_SnapshotConflict)
extern "C"  void GooglePlaySavedGamesManager_U3CActionConflictU3Em__6C_m1534975201 (Il2CppObject * __this /* static, unused */, GP_SnapshotConflict_t4002586770 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlaySavedGamesManager::<ActionGameSaveRemoved>m__6D(GP_DeleteSnapshotResult)
extern "C"  void GooglePlaySavedGamesManager_U3CActionGameSaveRemovedU3Em__6D_m1690997755 (Il2CppObject * __this /* static, unused */, GP_DeleteSnapshotResult_t3306790010 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
