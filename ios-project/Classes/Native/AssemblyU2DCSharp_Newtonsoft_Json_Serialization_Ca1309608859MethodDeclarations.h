﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Cac666803094MethodDeclarations.h"

// System.Void Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Runtime.Serialization.DataContractAttribute>::.cctor()
#define CachedAttributeGetter_1__cctor_m3700268156(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))CachedAttributeGetter_1__cctor_m1655531471_gshared)(__this /* static, unused */, method)
// T Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Runtime.Serialization.DataContractAttribute>::GetAttribute(System.Reflection.ICustomAttributeProvider)
#define CachedAttributeGetter_1_GetAttribute_m630405485(__this /* static, unused */, ___type0, method) ((  DataContractAttribute_t3332255060 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))CachedAttributeGetter_1_GetAttribute_m2147397194_gshared)(__this /* static, unused */, ___type0, method)
