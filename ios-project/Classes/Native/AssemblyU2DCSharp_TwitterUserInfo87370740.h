﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// TwitterStatus
struct TwitterStatus_t2765035499;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwitterUserInfo
struct  TwitterUserInfo_t87370740  : public Il2CppObject
{
public:
	// System.String TwitterUserInfo::_id
	String_t* ____id_0;
	// System.String TwitterUserInfo::_description
	String_t* ____description_1;
	// System.String TwitterUserInfo::_name
	String_t* ____name_2;
	// System.String TwitterUserInfo::_screen_name
	String_t* ____screen_name_3;
	// System.String TwitterUserInfo::_location
	String_t* ____location_4;
	// System.String TwitterUserInfo::_lang
	String_t* ____lang_5;
	// System.String TwitterUserInfo::_rawJSON
	String_t* ____rawJSON_6;
	// System.String TwitterUserInfo::_profile_image_url
	String_t* ____profile_image_url_7;
	// System.String TwitterUserInfo::_profile_image_url_https
	String_t* ____profile_image_url_https_8;
	// System.String TwitterUserInfo::_profile_background_image_url
	String_t* ____profile_background_image_url_9;
	// System.String TwitterUserInfo::_profile_background_image_url_https
	String_t* ____profile_background_image_url_https_10;
	// UnityEngine.Texture2D TwitterUserInfo::_profile_image
	Texture2D_t3542995729 * ____profile_image_11;
	// UnityEngine.Texture2D TwitterUserInfo::_profile_background
	Texture2D_t3542995729 * ____profile_background_12;
	// UnityEngine.Color TwitterUserInfo::_profile_background_color
	Color_t2020392075  ____profile_background_color_13;
	// UnityEngine.Color TwitterUserInfo::_profile_text_color
	Color_t2020392075  ____profile_text_color_14;
	// System.Int32 TwitterUserInfo::_friends_count
	int32_t ____friends_count_15;
	// System.Int32 TwitterUserInfo::_statuses_count
	int32_t ____statuses_count_16;
	// TwitterStatus TwitterUserInfo::_status
	TwitterStatus_t2765035499 * ____status_17;
	// System.Action`1<UnityEngine.Texture2D> TwitterUserInfo::ActionProfileImageLoaded
	Action_1_t3344795111 * ___ActionProfileImageLoaded_18;
	// System.Action`1<UnityEngine.Texture2D> TwitterUserInfo::ActionProfileBackgroundImageLoaded
	Action_1_t3344795111 * ___ActionProfileBackgroundImageLoaded_19;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____id_0)); }
	inline String_t* get__id_0() const { return ____id_0; }
	inline String_t** get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(String_t* value)
	{
		____id_0 = value;
		Il2CppCodeGenWriteBarrier(&____id_0, value);
	}

	inline static int32_t get_offset_of__description_1() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____description_1)); }
	inline String_t* get__description_1() const { return ____description_1; }
	inline String_t** get_address_of__description_1() { return &____description_1; }
	inline void set__description_1(String_t* value)
	{
		____description_1 = value;
		Il2CppCodeGenWriteBarrier(&____description_1, value);
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier(&____name_2, value);
	}

	inline static int32_t get_offset_of__screen_name_3() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____screen_name_3)); }
	inline String_t* get__screen_name_3() const { return ____screen_name_3; }
	inline String_t** get_address_of__screen_name_3() { return &____screen_name_3; }
	inline void set__screen_name_3(String_t* value)
	{
		____screen_name_3 = value;
		Il2CppCodeGenWriteBarrier(&____screen_name_3, value);
	}

	inline static int32_t get_offset_of__location_4() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____location_4)); }
	inline String_t* get__location_4() const { return ____location_4; }
	inline String_t** get_address_of__location_4() { return &____location_4; }
	inline void set__location_4(String_t* value)
	{
		____location_4 = value;
		Il2CppCodeGenWriteBarrier(&____location_4, value);
	}

	inline static int32_t get_offset_of__lang_5() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____lang_5)); }
	inline String_t* get__lang_5() const { return ____lang_5; }
	inline String_t** get_address_of__lang_5() { return &____lang_5; }
	inline void set__lang_5(String_t* value)
	{
		____lang_5 = value;
		Il2CppCodeGenWriteBarrier(&____lang_5, value);
	}

	inline static int32_t get_offset_of__rawJSON_6() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____rawJSON_6)); }
	inline String_t* get__rawJSON_6() const { return ____rawJSON_6; }
	inline String_t** get_address_of__rawJSON_6() { return &____rawJSON_6; }
	inline void set__rawJSON_6(String_t* value)
	{
		____rawJSON_6 = value;
		Il2CppCodeGenWriteBarrier(&____rawJSON_6, value);
	}

	inline static int32_t get_offset_of__profile_image_url_7() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____profile_image_url_7)); }
	inline String_t* get__profile_image_url_7() const { return ____profile_image_url_7; }
	inline String_t** get_address_of__profile_image_url_7() { return &____profile_image_url_7; }
	inline void set__profile_image_url_7(String_t* value)
	{
		____profile_image_url_7 = value;
		Il2CppCodeGenWriteBarrier(&____profile_image_url_7, value);
	}

	inline static int32_t get_offset_of__profile_image_url_https_8() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____profile_image_url_https_8)); }
	inline String_t* get__profile_image_url_https_8() const { return ____profile_image_url_https_8; }
	inline String_t** get_address_of__profile_image_url_https_8() { return &____profile_image_url_https_8; }
	inline void set__profile_image_url_https_8(String_t* value)
	{
		____profile_image_url_https_8 = value;
		Il2CppCodeGenWriteBarrier(&____profile_image_url_https_8, value);
	}

	inline static int32_t get_offset_of__profile_background_image_url_9() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____profile_background_image_url_9)); }
	inline String_t* get__profile_background_image_url_9() const { return ____profile_background_image_url_9; }
	inline String_t** get_address_of__profile_background_image_url_9() { return &____profile_background_image_url_9; }
	inline void set__profile_background_image_url_9(String_t* value)
	{
		____profile_background_image_url_9 = value;
		Il2CppCodeGenWriteBarrier(&____profile_background_image_url_9, value);
	}

	inline static int32_t get_offset_of__profile_background_image_url_https_10() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____profile_background_image_url_https_10)); }
	inline String_t* get__profile_background_image_url_https_10() const { return ____profile_background_image_url_https_10; }
	inline String_t** get_address_of__profile_background_image_url_https_10() { return &____profile_background_image_url_https_10; }
	inline void set__profile_background_image_url_https_10(String_t* value)
	{
		____profile_background_image_url_https_10 = value;
		Il2CppCodeGenWriteBarrier(&____profile_background_image_url_https_10, value);
	}

	inline static int32_t get_offset_of__profile_image_11() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____profile_image_11)); }
	inline Texture2D_t3542995729 * get__profile_image_11() const { return ____profile_image_11; }
	inline Texture2D_t3542995729 ** get_address_of__profile_image_11() { return &____profile_image_11; }
	inline void set__profile_image_11(Texture2D_t3542995729 * value)
	{
		____profile_image_11 = value;
		Il2CppCodeGenWriteBarrier(&____profile_image_11, value);
	}

	inline static int32_t get_offset_of__profile_background_12() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____profile_background_12)); }
	inline Texture2D_t3542995729 * get__profile_background_12() const { return ____profile_background_12; }
	inline Texture2D_t3542995729 ** get_address_of__profile_background_12() { return &____profile_background_12; }
	inline void set__profile_background_12(Texture2D_t3542995729 * value)
	{
		____profile_background_12 = value;
		Il2CppCodeGenWriteBarrier(&____profile_background_12, value);
	}

	inline static int32_t get_offset_of__profile_background_color_13() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____profile_background_color_13)); }
	inline Color_t2020392075  get__profile_background_color_13() const { return ____profile_background_color_13; }
	inline Color_t2020392075 * get_address_of__profile_background_color_13() { return &____profile_background_color_13; }
	inline void set__profile_background_color_13(Color_t2020392075  value)
	{
		____profile_background_color_13 = value;
	}

	inline static int32_t get_offset_of__profile_text_color_14() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____profile_text_color_14)); }
	inline Color_t2020392075  get__profile_text_color_14() const { return ____profile_text_color_14; }
	inline Color_t2020392075 * get_address_of__profile_text_color_14() { return &____profile_text_color_14; }
	inline void set__profile_text_color_14(Color_t2020392075  value)
	{
		____profile_text_color_14 = value;
	}

	inline static int32_t get_offset_of__friends_count_15() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____friends_count_15)); }
	inline int32_t get__friends_count_15() const { return ____friends_count_15; }
	inline int32_t* get_address_of__friends_count_15() { return &____friends_count_15; }
	inline void set__friends_count_15(int32_t value)
	{
		____friends_count_15 = value;
	}

	inline static int32_t get_offset_of__statuses_count_16() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____statuses_count_16)); }
	inline int32_t get__statuses_count_16() const { return ____statuses_count_16; }
	inline int32_t* get_address_of__statuses_count_16() { return &____statuses_count_16; }
	inline void set__statuses_count_16(int32_t value)
	{
		____statuses_count_16 = value;
	}

	inline static int32_t get_offset_of__status_17() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ____status_17)); }
	inline TwitterStatus_t2765035499 * get__status_17() const { return ____status_17; }
	inline TwitterStatus_t2765035499 ** get_address_of__status_17() { return &____status_17; }
	inline void set__status_17(TwitterStatus_t2765035499 * value)
	{
		____status_17 = value;
		Il2CppCodeGenWriteBarrier(&____status_17, value);
	}

	inline static int32_t get_offset_of_ActionProfileImageLoaded_18() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ___ActionProfileImageLoaded_18)); }
	inline Action_1_t3344795111 * get_ActionProfileImageLoaded_18() const { return ___ActionProfileImageLoaded_18; }
	inline Action_1_t3344795111 ** get_address_of_ActionProfileImageLoaded_18() { return &___ActionProfileImageLoaded_18; }
	inline void set_ActionProfileImageLoaded_18(Action_1_t3344795111 * value)
	{
		___ActionProfileImageLoaded_18 = value;
		Il2CppCodeGenWriteBarrier(&___ActionProfileImageLoaded_18, value);
	}

	inline static int32_t get_offset_of_ActionProfileBackgroundImageLoaded_19() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740, ___ActionProfileBackgroundImageLoaded_19)); }
	inline Action_1_t3344795111 * get_ActionProfileBackgroundImageLoaded_19() const { return ___ActionProfileBackgroundImageLoaded_19; }
	inline Action_1_t3344795111 ** get_address_of_ActionProfileBackgroundImageLoaded_19() { return &___ActionProfileBackgroundImageLoaded_19; }
	inline void set_ActionProfileBackgroundImageLoaded_19(Action_1_t3344795111 * value)
	{
		___ActionProfileBackgroundImageLoaded_19 = value;
		Il2CppCodeGenWriteBarrier(&___ActionProfileBackgroundImageLoaded_19, value);
	}
};

struct TwitterUserInfo_t87370740_StaticFields
{
public:
	// System.Action`1<UnityEngine.Texture2D> TwitterUserInfo::<>f__am$cache14
	Action_1_t3344795111 * ___U3CU3Ef__amU24cache14_20;
	// System.Action`1<UnityEngine.Texture2D> TwitterUserInfo::<>f__am$cache15
	Action_1_t3344795111 * ___U3CU3Ef__amU24cache15_21;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_20() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740_StaticFields, ___U3CU3Ef__amU24cache14_20)); }
	inline Action_1_t3344795111 * get_U3CU3Ef__amU24cache14_20() const { return ___U3CU3Ef__amU24cache14_20; }
	inline Action_1_t3344795111 ** get_address_of_U3CU3Ef__amU24cache14_20() { return &___U3CU3Ef__amU24cache14_20; }
	inline void set_U3CU3Ef__amU24cache14_20(Action_1_t3344795111 * value)
	{
		___U3CU3Ef__amU24cache14_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache14_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_21() { return static_cast<int32_t>(offsetof(TwitterUserInfo_t87370740_StaticFields, ___U3CU3Ef__amU24cache15_21)); }
	inline Action_1_t3344795111 * get_U3CU3Ef__amU24cache15_21() const { return ___U3CU3Ef__amU24cache15_21; }
	inline Action_1_t3344795111 ** get_address_of_U3CU3Ef__amU24cache15_21() { return &___U3CU3Ef__amU24cache15_21; }
	inline void set_U3CU3Ef__amU24cache15_21(Action_1_t3344795111 * value)
	{
		___U3CU3Ef__amU24cache15_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache15_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
