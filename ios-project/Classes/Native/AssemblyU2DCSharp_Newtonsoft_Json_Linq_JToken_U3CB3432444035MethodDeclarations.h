﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__IteratorF
struct U3CBeforeSelfU3Ec__IteratorF_t3432444035;
// Newtonsoft.Json.Linq.JToken
struct JToken_t2552644013;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t28167840;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__IteratorF::.ctor()
extern "C"  void U3CBeforeSelfU3Ec__IteratorF__ctor_m3068357160 (U3CBeforeSelfU3Ec__IteratorF_t3432444035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__IteratorF::System.Collections.Generic.IEnumerator<Newtonsoft.Json.Linq.JToken>.get_Current()
extern "C"  JToken_t2552644013 * U3CBeforeSelfU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m3690939939 (U3CBeforeSelfU3Ec__IteratorF_t3432444035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBeforeSelfU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m2521761544 (U3CBeforeSelfU3Ec__IteratorF_t3432444035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__IteratorF::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CBeforeSelfU3Ec__IteratorF_System_Collections_IEnumerable_GetEnumerator_m2022200035 (U3CBeforeSelfU3Ec__IteratorF_t3432444035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__IteratorF::System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>.GetEnumerator()
extern "C"  Il2CppObject* U3CBeforeSelfU3Ec__IteratorF_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_m105131894 (U3CBeforeSelfU3Ec__IteratorF_t3432444035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__IteratorF::MoveNext()
extern "C"  bool U3CBeforeSelfU3Ec__IteratorF_MoveNext_m3492292232 (U3CBeforeSelfU3Ec__IteratorF_t3432444035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__IteratorF::Dispose()
extern "C"  void U3CBeforeSelfU3Ec__IteratorF_Dispose_m2846023097 (U3CBeforeSelfU3Ec__IteratorF_t3432444035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__IteratorF::Reset()
extern "C"  void U3CBeforeSelfU3Ec__IteratorF_Reset_m739268543 (U3CBeforeSelfU3Ec__IteratorF_t3432444035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
