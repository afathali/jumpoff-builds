﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GamePlayExample
struct GamePlayExample_t3564348922;

#include "codegen/il2cpp-codegen.h"

// System.Void GamePlayExample::.ctor()
extern "C"  void GamePlayExample__ctor_m2525090789 (GamePlayExample_t3564348922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlayExample::Awake()
extern "C"  void GamePlayExample_Awake_m2059818806 (GamePlayExample_t3564348922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlayExample::FixedUpdate()
extern "C"  void GamePlayExample_FixedUpdate_m3535887422 (GamePlayExample_t3564348922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlayExample::AddCoins()
extern "C"  void GamePlayExample_AddCoins_m3348797366 (GamePlayExample_t3564348922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlayExample::Boost()
extern "C"  void GamePlayExample_Boost_m3218909514 (GamePlayExample_t3564348922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
