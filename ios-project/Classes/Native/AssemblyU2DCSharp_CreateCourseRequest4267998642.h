﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// JumpPoint[]
struct JumpPointU5BU5D_t4004280907;

#include "AssemblyU2DCSharp_Shared_Request3213492751.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateCourseRequest
struct  CreateCourseRequest_t4267998642  : public Request_t3213492751
{
public:
	// System.String CreateCourseRequest::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_2;
	// System.Int64 CreateCourseRequest::<Id>k__BackingField
	int64_t ___U3CIdU3Ek__BackingField_3;
	// System.Int64 CreateCourseRequest::<UserId>k__BackingField
	int64_t ___U3CUserIdU3Ek__BackingField_4;
	// System.String CreateCourseRequest::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_5;
	// System.String CreateCourseRequest::<Show>k__BackingField
	String_t* ___U3CShowU3Ek__BackingField_6;
	// System.String CreateCourseRequest::<Date>k__BackingField
	String_t* ___U3CDateU3Ek__BackingField_7;
	// System.String CreateCourseRequest::<Notes>k__BackingField
	String_t* ___U3CNotesU3Ek__BackingField_8;
	// System.Byte[] CreateCourseRequest::<Image>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CImageU3Ek__BackingField_9;
	// UnityEngine.Vector3 CreateCourseRequest::<ImagePosition>k__BackingField
	Vector3_t2243707580  ___U3CImagePositionU3Ek__BackingField_10;
	// UnityEngine.Quaternion CreateCourseRequest::<ImageRotation>k__BackingField
	Quaternion_t4030073918  ___U3CImageRotationU3Ek__BackingField_11;
	// UnityEngine.Vector3 CreateCourseRequest::<ImageScale>k__BackingField
	Vector3_t2243707580  ___U3CImageScaleU3Ek__BackingField_12;
	// JumpPoint[] CreateCourseRequest::<Jumps>k__BackingField
	JumpPointU5BU5D_t4004280907* ___U3CJumpsU3Ek__BackingField_13;
	// System.String CreateCourseRequest::<Scale>k__BackingField
	String_t* ___U3CScaleU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateCourseRequest_t4267998642, ___U3CTypeU3Ek__BackingField_2)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTypeU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CreateCourseRequest_t4267998642, ___U3CIdU3Ek__BackingField_3)); }
	inline int64_t get_U3CIdU3Ek__BackingField_3() const { return ___U3CIdU3Ek__BackingField_3; }
	inline int64_t* get_address_of_U3CIdU3Ek__BackingField_3() { return &___U3CIdU3Ek__BackingField_3; }
	inline void set_U3CIdU3Ek__BackingField_3(int64_t value)
	{
		___U3CIdU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CreateCourseRequest_t4267998642, ___U3CUserIdU3Ek__BackingField_4)); }
	inline int64_t get_U3CUserIdU3Ek__BackingField_4() const { return ___U3CUserIdU3Ek__BackingField_4; }
	inline int64_t* get_address_of_U3CUserIdU3Ek__BackingField_4() { return &___U3CUserIdU3Ek__BackingField_4; }
	inline void set_U3CUserIdU3Ek__BackingField_4(int64_t value)
	{
		___U3CUserIdU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CreateCourseRequest_t4267998642, ___U3CNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CNameU3Ek__BackingField_5() const { return ___U3CNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_5() { return &___U3CNameU3Ek__BackingField_5; }
	inline void set_U3CNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNameU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CShowU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CreateCourseRequest_t4267998642, ___U3CShowU3Ek__BackingField_6)); }
	inline String_t* get_U3CShowU3Ek__BackingField_6() const { return ___U3CShowU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CShowU3Ek__BackingField_6() { return &___U3CShowU3Ek__BackingField_6; }
	inline void set_U3CShowU3Ek__BackingField_6(String_t* value)
	{
		___U3CShowU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CShowU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CDateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CreateCourseRequest_t4267998642, ___U3CDateU3Ek__BackingField_7)); }
	inline String_t* get_U3CDateU3Ek__BackingField_7() const { return ___U3CDateU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CDateU3Ek__BackingField_7() { return &___U3CDateU3Ek__BackingField_7; }
	inline void set_U3CDateU3Ek__BackingField_7(String_t* value)
	{
		___U3CDateU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDateU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CNotesU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CreateCourseRequest_t4267998642, ___U3CNotesU3Ek__BackingField_8)); }
	inline String_t* get_U3CNotesU3Ek__BackingField_8() const { return ___U3CNotesU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CNotesU3Ek__BackingField_8() { return &___U3CNotesU3Ek__BackingField_8; }
	inline void set_U3CNotesU3Ek__BackingField_8(String_t* value)
	{
		___U3CNotesU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNotesU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CImageU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CreateCourseRequest_t4267998642, ___U3CImageU3Ek__BackingField_9)); }
	inline ByteU5BU5D_t3397334013* get_U3CImageU3Ek__BackingField_9() const { return ___U3CImageU3Ek__BackingField_9; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CImageU3Ek__BackingField_9() { return &___U3CImageU3Ek__BackingField_9; }
	inline void set_U3CImageU3Ek__BackingField_9(ByteU5BU5D_t3397334013* value)
	{
		___U3CImageU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CImageU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CImagePositionU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CreateCourseRequest_t4267998642, ___U3CImagePositionU3Ek__BackingField_10)); }
	inline Vector3_t2243707580  get_U3CImagePositionU3Ek__BackingField_10() const { return ___U3CImagePositionU3Ek__BackingField_10; }
	inline Vector3_t2243707580 * get_address_of_U3CImagePositionU3Ek__BackingField_10() { return &___U3CImagePositionU3Ek__BackingField_10; }
	inline void set_U3CImagePositionU3Ek__BackingField_10(Vector3_t2243707580  value)
	{
		___U3CImagePositionU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CImageRotationU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CreateCourseRequest_t4267998642, ___U3CImageRotationU3Ek__BackingField_11)); }
	inline Quaternion_t4030073918  get_U3CImageRotationU3Ek__BackingField_11() const { return ___U3CImageRotationU3Ek__BackingField_11; }
	inline Quaternion_t4030073918 * get_address_of_U3CImageRotationU3Ek__BackingField_11() { return &___U3CImageRotationU3Ek__BackingField_11; }
	inline void set_U3CImageRotationU3Ek__BackingField_11(Quaternion_t4030073918  value)
	{
		___U3CImageRotationU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CImageScaleU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CreateCourseRequest_t4267998642, ___U3CImageScaleU3Ek__BackingField_12)); }
	inline Vector3_t2243707580  get_U3CImageScaleU3Ek__BackingField_12() const { return ___U3CImageScaleU3Ek__BackingField_12; }
	inline Vector3_t2243707580 * get_address_of_U3CImageScaleU3Ek__BackingField_12() { return &___U3CImageScaleU3Ek__BackingField_12; }
	inline void set_U3CImageScaleU3Ek__BackingField_12(Vector3_t2243707580  value)
	{
		___U3CImageScaleU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CJumpsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CreateCourseRequest_t4267998642, ___U3CJumpsU3Ek__BackingField_13)); }
	inline JumpPointU5BU5D_t4004280907* get_U3CJumpsU3Ek__BackingField_13() const { return ___U3CJumpsU3Ek__BackingField_13; }
	inline JumpPointU5BU5D_t4004280907** get_address_of_U3CJumpsU3Ek__BackingField_13() { return &___U3CJumpsU3Ek__BackingField_13; }
	inline void set_U3CJumpsU3Ek__BackingField_13(JumpPointU5BU5D_t4004280907* value)
	{
		___U3CJumpsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CJumpsU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CScaleU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CreateCourseRequest_t4267998642, ___U3CScaleU3Ek__BackingField_14)); }
	inline String_t* get_U3CScaleU3Ek__BackingField_14() const { return ___U3CScaleU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CScaleU3Ek__BackingField_14() { return &___U3CScaleU3Ek__BackingField_14; }
	inline void set_U3CScaleU3Ek__BackingField_14(String_t* value)
	{
		___U3CScaleU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CScaleU3Ek__BackingField_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
