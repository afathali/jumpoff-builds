﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3980293213MethodDeclarations.h"

// System.Void SA_Singleton_OLD`1<SPFacebook>::.ctor()
#define SA_Singleton_OLD_1__ctor_m374587093(__this, method) ((  void (*) (SA_Singleton_OLD_1_t2356072287 *, const MethodInfo*))SA_Singleton_OLD_1__ctor_m2643683696_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<SPFacebook>::.cctor()
#define SA_Singleton_OLD_1__cctor_m2177447846(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1__cctor_m1355192463_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<SPFacebook>::get_instance()
#define SA_Singleton_OLD_1_get_instance_m3797360560(__this /* static, unused */, method) ((  SPFacebook_t1065228369 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_instance_m1963118739_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<SPFacebook>::get_Instance()
#define SA_Singleton_OLD_1_get_Instance_m3761913744(__this /* static, unused */, method) ((  SPFacebook_t1065228369 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_Instance_m2967800051_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<SPFacebook>::get_HasInstance()
#define SA_Singleton_OLD_1_get_HasInstance_m627362831(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_HasInstance_m4184706798_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<SPFacebook>::get_IsDestroyed()
#define SA_Singleton_OLD_1_get_IsDestroyed_m3558472649(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_IsDestroyed_m1648274602_gshared)(__this /* static, unused */, method)
// System.Void SA_Singleton_OLD`1<SPFacebook>::OnDestroy()
#define SA_Singleton_OLD_1_OnDestroy_m1209977626(__this, method) ((  void (*) (SA_Singleton_OLD_1_t2356072287 *, const MethodInfo*))SA_Singleton_OLD_1_OnDestroy_m1680414419_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<SPFacebook>::OnApplicationQuit()
#define SA_Singleton_OLD_1_OnApplicationQuit_m542915971(__this, method) ((  void (*) (SA_Singleton_OLD_1_t2356072287 *, const MethodInfo*))SA_Singleton_OLD_1_OnApplicationQuit_m1864960902_gshared)(__this, method)
