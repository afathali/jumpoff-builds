﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_GMSGeneralProxy
struct AN_GMSGeneralProxy_t49831053;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_GMSGeneralProxy::.ctor()
extern "C"  void AN_GMSGeneralProxy__ctor_m4040380404 (AN_GMSGeneralProxy_t49831053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_GMSGeneralProxy_CallActivityFunction_m3007613487 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::initTagManager(System.String,System.String)
extern "C"  void AN_GMSGeneralProxy_initTagManager_m2307496731 (Il2CppObject * __this /* static, unused */, String_t* ___containerId0, String_t* ___binDataId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::getValueFromContainer(System.String)
extern "C"  void AN_GMSGeneralProxy_getValueFromContainer_m379256370 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::refreshContainer()
extern "C"  void AN_GMSGeneralProxy_refreshContainer_m2177491152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::pushValue(System.String,System.String)
extern "C"  void AN_GMSGeneralProxy_pushValue_m3328450761 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::pushEvent(System.String,System.String,System.String)
extern "C"  void AN_GMSGeneralProxy_pushEvent_m2253120798 (Il2CppObject * __this /* static, unused */, String_t* ___tagEvent0, String_t* ___key1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::loadGoogleAccountNames()
extern "C"  void AN_GMSGeneralProxy_loadGoogleAccountNames_m734983056 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::clearDefaultAccount()
extern "C"  void AN_GMSGeneralProxy_clearDefaultAccount_m3598594915 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::playServiceInit(System.String)
extern "C"  void AN_GMSGeneralProxy_playServiceInit_m3940401069 (Il2CppObject * __this /* static, unused */, String_t* ___scopes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::setConnectionParams(System.Boolean)
extern "C"  void AN_GMSGeneralProxy_setConnectionParams_m2262854915 (Il2CppObject * __this /* static, unused */, bool ___showPopup0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::playServiceConnect()
extern "C"  void AN_GMSGeneralProxy_playServiceConnect_m652036167 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::playServiceConnect(System.String)
extern "C"  void AN_GMSGeneralProxy_playServiceConnect_m1320560865 (Il2CppObject * __this /* static, unused */, String_t* ___accountName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::loadToken(System.String,System.String)
extern "C"  void AN_GMSGeneralProxy_loadToken_m2617246801 (Il2CppObject * __this /* static, unused */, String_t* ___accountName0, String_t* ___scope1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::invalidateToken(System.String)
extern "C"  void AN_GMSGeneralProxy_invalidateToken_m3807273472 (Il2CppObject * __this /* static, unused */, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::playServiceDisconnect()
extern "C"  void AN_GMSGeneralProxy_playServiceDisconnect_m1169251777 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::showAchievementsUI()
extern "C"  void AN_GMSGeneralProxy_showAchievementsUI_m667164863 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::showLeaderBoardsUI()
extern "C"  void AN_GMSGeneralProxy_showLeaderBoardsUI_m1770341465 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::loadConnectedPlayers()
extern "C"  void AN_GMSGeneralProxy_loadConnectedPlayers_m2118423629 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::showLeaderBoard(System.String)
extern "C"  void AN_GMSGeneralProxy_showLeaderBoard_m367107070 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::showLeaderBoardById(System.String)
extern "C"  void AN_GMSGeneralProxy_showLeaderBoardById_m3348169530 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::submitScore(System.String,System.Int64)
extern "C"  void AN_GMSGeneralProxy_submitScore_m1436027270 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardName0, int64_t ___score1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::submitScoreById(System.String,System.Int64)
extern "C"  void AN_GMSGeneralProxy_submitScoreById_m3919305170 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, int64_t ___score1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::loadLeaderBoards()
extern "C"  void AN_GMSGeneralProxy_loadLeaderBoards_m1303918630 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::loadLeaderboardInfoLocal(System.String,System.Int32)
extern "C"  void AN_GMSGeneralProxy_loadLeaderboardInfoLocal_m1353475275 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, int32_t ___requestId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::loadPlayerCenteredScores(System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void AN_GMSGeneralProxy_loadPlayerCenteredScores_m3230265687 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, int32_t ___span1, int32_t ___leaderboardCollection2, int32_t ___maxResults3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::loadTopScores(System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void AN_GMSGeneralProxy_loadTopScores_m772691725 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, int32_t ___span1, int32_t ___leaderboardCollection2, int32_t ___maxResults3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::reportAchievement(System.String)
extern "C"  void AN_GMSGeneralProxy_reportAchievement_m2340840675 (Il2CppObject * __this /* static, unused */, String_t* ___achievementName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::reportAchievementById(System.String)
extern "C"  void AN_GMSGeneralProxy_reportAchievementById_m1874907121 (Il2CppObject * __this /* static, unused */, String_t* ___achievementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::revealAchievement(System.String)
extern "C"  void AN_GMSGeneralProxy_revealAchievement_m2944886998 (Il2CppObject * __this /* static, unused */, String_t* ___achievementName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::revealAchievementById(System.String)
extern "C"  void AN_GMSGeneralProxy_revealAchievementById_m328644254 (Il2CppObject * __this /* static, unused */, String_t* ___achievementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::incrementAchievement(System.String,System.String)
extern "C"  void AN_GMSGeneralProxy_incrementAchievement_m1537768 (Il2CppObject * __this /* static, unused */, String_t* ___achievementName0, String_t* ___numsteps1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::incrementAchievementById(System.String,System.String)
extern "C"  void AN_GMSGeneralProxy_incrementAchievementById_m3952357480 (Il2CppObject * __this /* static, unused */, String_t* ___achievementId0, String_t* ___numsteps1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::setStepsImmediate(System.String,System.String)
extern "C"  void AN_GMSGeneralProxy_setStepsImmediate_m31576542 (Il2CppObject * __this /* static, unused */, String_t* ___achievementId0, String_t* ___numsteps1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::loadAchievements()
extern "C"  void AN_GMSGeneralProxy_loadAchievements_m1469206170 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::resetAchievement(System.String)
extern "C"  void AN_GMSGeneralProxy_resetAchievement_m747862514 (Il2CppObject * __this /* static, unused */, String_t* ___achievementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::ResetAllAchievements()
extern "C"  void AN_GMSGeneralProxy_ResetAllAchievements_m3715510264 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::resetLeaderBoard(System.String)
extern "C"  void AN_GMSGeneralProxy_resetLeaderBoard_m1521601154 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::OnApplicationPause(System.Boolean)
extern "C"  void AN_GMSGeneralProxy_OnApplicationPause_m1734602430 (Il2CppObject * __this /* static, unused */, bool ___isPaused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::ShowSavedGamesUI_Bridge(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  void AN_GMSGeneralProxy_ShowSavedGamesUI_Bridge_m3967010092 (Il2CppObject * __this /* static, unused */, String_t* ___title0, int32_t ___maxNumberOfSavedGamesToShow1, bool ___allowAddButton2, bool ___allowDelete3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::CreateNewSpanshot_Bridge(System.String,System.String,System.String,System.String,System.Int64)
extern "C"  void AN_GMSGeneralProxy_CreateNewSpanshot_Bridge_m2878545820 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___description1, String_t* ___ImageData2, String_t* ___Data3, int64_t ___PlayedTime4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::ResolveSnapshotsConflict_Bridge(System.Int32)
extern "C"  void AN_GMSGeneralProxy_ResolveSnapshotsConflict_Bridge_m3296701008 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::LoadSpanshots_Bridge()
extern "C"  void AN_GMSGeneralProxy_LoadSpanshots_Bridge_m2644289193 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::OpenSpanshotByName_Bridge(System.String)
extern "C"  void AN_GMSGeneralProxy_OpenSpanshotByName_Bridge_m2090149168 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::DeleteSpanshotByName_Bridge(System.String)
extern "C"  void AN_GMSGeneralProxy_DeleteSpanshotByName_Bridge_m1676239143 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::ListStates()
extern "C"  void AN_GMSGeneralProxy_ListStates_m168694244 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::UpdateState(System.Int32,System.String)
extern "C"  void AN_GMSGeneralProxy_UpdateState_m694720921 (Il2CppObject * __this /* static, unused */, int32_t ___stateKey0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::ResolveState(System.Int32,System.String,System.String)
extern "C"  void AN_GMSGeneralProxy_ResolveState_m3679658218 (Il2CppObject * __this /* static, unused */, int32_t ___stateKey0, String_t* ___resolvedData1, String_t* ___resolvedVersion2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::DeleteState(System.Int32)
extern "C"  void AN_GMSGeneralProxy_DeleteState_m2751499167 (Il2CppObject * __this /* static, unused */, int32_t ___stateKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGeneralProxy::LoadState(System.Int32)
extern "C"  void AN_GMSGeneralProxy_LoadState_m2410222904 (Il2CppObject * __this /* static, unused */, int32_t ___stateKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
