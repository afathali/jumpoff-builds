﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameCenter_TBM
struct GameCenter_TBM_t3457554475;
// System.Action`1<GK_TBM_LoadMatchResult>
struct Action_1_t1441048655;
// System.Action`1<GK_TBM_LoadMatchesResult>
struct Action_1_t172291117;
// System.Action`1<GK_TBM_MatchDataUpdateResult>
struct Action_1_t1157805416;
// System.Action`1<GK_TBM_MatchInitResult>
struct Action_1_t3649630279;
// System.Action`1<GK_TBM_MatchQuitResult>
struct Action_1_t1035620038;
// System.Action`1<GK_TBM_EndTrunResult>
struct Action_1_t1319180072;
// System.Action`1<GK_TBM_MatchEndResult>
struct Action_1_t3263568192;
// System.Action`1<GK_TBM_RematchResult>
struct Action_1_t2961573082;
// System.Action`1<GK_TBM_MatchRemovedResult>
struct Action_1_t710925695;
// System.Action`1<GK_TBM_Match>
struct Action_1_t4228799808;
// System.Action`1<GK_TBM_MatchTurnResult>
struct Action_1_t3385457542;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// GK_TBM_Match
struct GK_TBM_Match_t132033130;
// System.Collections.Generic.Dictionary`2<System.String,GK_TBM_Match>
struct Dictionary_2_t2046812392;
// System.Collections.Generic.List`1<GK_TBM_Match>
struct List_1_t3796121558;
// GK_TBM_LoadMatchResult
struct GK_TBM_LoadMatchResult_t1639249273;
// GK_TBM_LoadMatchesResult
struct GK_TBM_LoadMatchesResult_t370491735;
// GK_TBM_MatchDataUpdateResult
struct GK_TBM_MatchDataUpdateResult_t1356006034;
// GK_TBM_MatchInitResult
struct GK_TBM_MatchInitResult_t3847830897;
// GK_TBM_MatchQuitResult
struct GK_TBM_MatchQuitResult_t1233820656;
// GK_TBM_EndTrunResult
struct GK_TBM_EndTrunResult_t1517380690;
// GK_TBM_MatchEndResult
struct GK_TBM_MatchEndResult_t3461768810;
// GK_TBM_RematchResult
struct GK_TBM_RematchResult_t3159773700;
// GK_TBM_MatchRemovedResult
struct GK_TBM_MatchRemovedResult_t909126313;
// GK_TBM_MatchTurnResult
struct GK_TBM_MatchTurnResult_t3583658160;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TurnBasedMatchOut2242380984.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_Match132033130.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_LoadMatchResu1639249273.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_LoadMatchesRes370491735.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchDataUpda1356006034.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchInitResu3847830897.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchQuitResu1233820656.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_EndTrunResult1517380690.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchEndResul3461768810.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_RematchResult3159773700.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchRemovedRe909126313.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchTurnResu3583658160.h"

// System.Void GameCenter_TBM::.ctor()
extern "C"  void GameCenter_TBM__ctor_m2857468586 (GameCenter_TBM_t3457554475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::.cctor()
extern "C"  void GameCenter_TBM__cctor_m80363127 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::add_ActionMatchInfoLoaded(System.Action`1<GK_TBM_LoadMatchResult>)
extern "C"  void GameCenter_TBM_add_ActionMatchInfoLoaded_m2138760907 (Il2CppObject * __this /* static, unused */, Action_1_t1441048655 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::remove_ActionMatchInfoLoaded(System.Action`1<GK_TBM_LoadMatchResult>)
extern "C"  void GameCenter_TBM_remove_ActionMatchInfoLoaded_m2547810308 (Il2CppObject * __this /* static, unused */, Action_1_t1441048655 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::add_ActionMatchesInfoLoaded(System.Action`1<GK_TBM_LoadMatchesResult>)
extern "C"  void GameCenter_TBM_add_ActionMatchesInfoLoaded_m3018774395 (Il2CppObject * __this /* static, unused */, Action_1_t172291117 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::remove_ActionMatchesInfoLoaded(System.Action`1<GK_TBM_LoadMatchesResult>)
extern "C"  void GameCenter_TBM_remove_ActionMatchesInfoLoaded_m3664428832 (Il2CppObject * __this /* static, unused */, Action_1_t172291117 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::add_ActionMatchDataUpdated(System.Action`1<GK_TBM_MatchDataUpdateResult>)
extern "C"  void GameCenter_TBM_add_ActionMatchDataUpdated_m4255370712 (Il2CppObject * __this /* static, unused */, Action_1_t1157805416 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::remove_ActionMatchDataUpdated(System.Action`1<GK_TBM_MatchDataUpdateResult>)
extern "C"  void GameCenter_TBM_remove_ActionMatchDataUpdated_m287121403 (Il2CppObject * __this /* static, unused */, Action_1_t1157805416 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::add_ActionMatchFound(System.Action`1<GK_TBM_MatchInitResult>)
extern "C"  void GameCenter_TBM_add_ActionMatchFound_m2449499778 (Il2CppObject * __this /* static, unused */, Action_1_t3649630279 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::remove_ActionMatchFound(System.Action`1<GK_TBM_MatchInitResult>)
extern "C"  void GameCenter_TBM_remove_ActionMatchFound_m1195516151 (Il2CppObject * __this /* static, unused */, Action_1_t3649630279 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::add_ActionMatchQuit(System.Action`1<GK_TBM_MatchQuitResult>)
extern "C"  void GameCenter_TBM_add_ActionMatchQuit_m1402319166 (Il2CppObject * __this /* static, unused */, Action_1_t1035620038 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::remove_ActionMatchQuit(System.Action`1<GK_TBM_MatchQuitResult>)
extern "C"  void GameCenter_TBM_remove_ActionMatchQuit_m2336970707 (Il2CppObject * __this /* static, unused */, Action_1_t1035620038 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::add_ActionTrunEnded(System.Action`1<GK_TBM_EndTrunResult>)
extern "C"  void GameCenter_TBM_add_ActionTrunEnded_m2225327805 (Il2CppObject * __this /* static, unused */, Action_1_t1319180072 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::remove_ActionTrunEnded(System.Action`1<GK_TBM_EndTrunResult>)
extern "C"  void GameCenter_TBM_remove_ActionTrunEnded_m1803575896 (Il2CppObject * __this /* static, unused */, Action_1_t1319180072 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::add_ActionMacthEnded(System.Action`1<GK_TBM_MatchEndResult>)
extern "C"  void GameCenter_TBM_add_ActionMacthEnded_m1847574209 (Il2CppObject * __this /* static, unused */, Action_1_t3263568192 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::remove_ActionMacthEnded(System.Action`1<GK_TBM_MatchEndResult>)
extern "C"  void GameCenter_TBM_remove_ActionMacthEnded_m1318723784 (Il2CppObject * __this /* static, unused */, Action_1_t3263568192 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::add_ActionRematched(System.Action`1<GK_TBM_RematchResult>)
extern "C"  void GameCenter_TBM_add_ActionRematched_m4139304973 (Il2CppObject * __this /* static, unused */, Action_1_t2961573082 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::remove_ActionRematched(System.Action`1<GK_TBM_RematchResult>)
extern "C"  void GameCenter_TBM_remove_ActionRematched_m321578426 (Il2CppObject * __this /* static, unused */, Action_1_t2961573082 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::add_ActionMatchRemoved(System.Action`1<GK_TBM_MatchRemovedResult>)
extern "C"  void GameCenter_TBM_add_ActionMatchRemoved_m1720421448 (Il2CppObject * __this /* static, unused */, Action_1_t710925695 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::remove_ActionMatchRemoved(System.Action`1<GK_TBM_MatchRemovedResult>)
extern "C"  void GameCenter_TBM_remove_ActionMatchRemoved_m4248501503 (Il2CppObject * __this /* static, unused */, Action_1_t710925695 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::add_ActionMatchInvitationAccepted(System.Action`1<GK_TBM_MatchInitResult>)
extern "C"  void GameCenter_TBM_add_ActionMatchInvitationAccepted_m3952363396 (Il2CppObject * __this /* static, unused */, Action_1_t3649630279 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::remove_ActionMatchInvitationAccepted(System.Action`1<GK_TBM_MatchInitResult>)
extern "C"  void GameCenter_TBM_remove_ActionMatchInvitationAccepted_m1491221869 (Il2CppObject * __this /* static, unused */, Action_1_t3649630279 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::add_ActionMatchInvitationDeclined(System.Action`1<GK_TBM_MatchRemovedResult>)
extern "C"  void GameCenter_TBM_add_ActionMatchInvitationDeclined_m526045271 (Il2CppObject * __this /* static, unused */, Action_1_t710925695 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::remove_ActionMatchInvitationDeclined(System.Action`1<GK_TBM_MatchRemovedResult>)
extern "C"  void GameCenter_TBM_remove_ActionMatchInvitationDeclined_m3998102888 (Il2CppObject * __this /* static, unused */, Action_1_t710925695 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::add_ActionPlayerQuitForMatch(System.Action`1<GK_TBM_Match>)
extern "C"  void GameCenter_TBM_add_ActionPlayerQuitForMatch_m3411530422 (Il2CppObject * __this /* static, unused */, Action_1_t4228799808 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::remove_ActionPlayerQuitForMatch(System.Action`1<GK_TBM_Match>)
extern "C"  void GameCenter_TBM_remove_ActionPlayerQuitForMatch_m368755 (Il2CppObject * __this /* static, unused */, Action_1_t4228799808 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::add_ActionTrunReceived(System.Action`1<GK_TBM_MatchTurnResult>)
extern "C"  void GameCenter_TBM_add_ActionTrunReceived_m4259553140 (Il2CppObject * __this /* static, unused */, Action_1_t3385457542 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::remove_ActionTrunReceived(System.Action`1<GK_TBM_MatchTurnResult>)
extern "C"  void GameCenter_TBM_remove_ActionTrunReceived_m1045780235 (Il2CppObject * __this /* static, unused */, Action_1_t3385457542 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::Awake()
extern "C"  void GameCenter_TBM_Awake_m971884573 (GameCenter_TBM_t3457554475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::LoadMatchesInfo()
extern "C"  void GameCenter_TBM_LoadMatchesInfo_m1825241001 (GameCenter_TBM_t3457554475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::LoadMatch(System.String)
extern "C"  void GameCenter_TBM_LoadMatch_m799614495 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::FindMatch(System.Int32,System.Int32,System.String,System.String[])
extern "C"  void GameCenter_TBM_FindMatch_m895359874 (GameCenter_TBM_t3457554475 * __this, int32_t ___minPlayers0, int32_t ___maxPlayers1, String_t* ___msg2, StringU5BU5D_t1642385972* ___playersToInvite3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::FindMatchWithNativeUI(System.Int32,System.Int32,System.String,System.String[])
extern "C"  void GameCenter_TBM_FindMatchWithNativeUI_m3228567259 (GameCenter_TBM_t3457554475 * __this, int32_t ___minPlayers0, int32_t ___maxPlayers1, String_t* ___msg2, StringU5BU5D_t1642385972* ___playersToInvite3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::SetPlayerGroup(System.Int32)
extern "C"  void GameCenter_TBM_SetPlayerGroup_m682444943 (GameCenter_TBM_t3457554475 * __this, int32_t ___group0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::SetPlayerAttributes(System.Int32)
extern "C"  void GameCenter_TBM_SetPlayerAttributes_m2263879447 (GameCenter_TBM_t3457554475 * __this, int32_t ___attributes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::SaveCurrentTurn(System.String,System.Byte[])
extern "C"  void GameCenter_TBM_SaveCurrentTurn_m1797122022 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, ByteU5BU5D_t3397334013* ___matchData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::EndTurn(System.String,System.Byte[],System.String)
extern "C"  void GameCenter_TBM_EndTurn_m851226489 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, ByteU5BU5D_t3397334013* ___matchData1, String_t* ___nextPlayerId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::QuitInTurn(System.String,GK_TurnBasedMatchOutcome,System.String,System.Byte[])
extern "C"  void GameCenter_TBM_QuitInTurn_m2606081482 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, int32_t ___outcome1, String_t* ___nextPlayerId2, ByteU5BU5D_t3397334013* ___matchData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::QuitOutOfTurn(System.String,GK_TurnBasedMatchOutcome)
extern "C"  void GameCenter_TBM_QuitOutOfTurn_m1358880707 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, int32_t ___outcome1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::EndMatch(System.String,System.Byte[])
extern "C"  void GameCenter_TBM_EndMatch_m3469373837 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, ByteU5BU5D_t3397334013* ___matchData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::Rematch(System.String)
extern "C"  void GameCenter_TBM_Rematch_m1948842086 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::RemoveMatch(System.String)
extern "C"  void GameCenter_TBM_RemoveMatch_m2792781327 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::AcceptInvite(System.String)
extern "C"  void GameCenter_TBM_AcceptInvite_m4163631259 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::DeclineInvite(System.String)
extern "C"  void GameCenter_TBM_DeclineInvite_m4240911939 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::UpdateParticipantOutcome(System.String,System.Int32,System.String)
extern "C"  void GameCenter_TBM_UpdateParticipantOutcome_m3807272777 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, int32_t ___outcome1, String_t* ___playerId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_TBM_Match GameCenter_TBM::GetMatchById(System.String)
extern "C"  GK_TBM_Match_t132033130 * GameCenter_TBM_GetMatchById_m633336074 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::PrintMatchInfo(GK_TBM_Match)
extern "C"  void GameCenter_TBM_PrintMatchInfo_m1940379666 (Il2CppObject * __this /* static, unused */, GK_TBM_Match_t132033130 * ___match0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,GK_TBM_Match> GameCenter_TBM::get_Matches()
extern "C"  Dictionary_2_t2046812392 * GameCenter_TBM_get_Matches_m1703533532 (GameCenter_TBM_t3457554475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GK_TBM_Match> GameCenter_TBM::get_MatchesList()
extern "C"  List_1_t3796121558 * GameCenter_TBM_get_MatchesList_m2702417999 (GameCenter_TBM_t3457554475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnLoadMatchesResult(System.String)
extern "C"  void GameCenter_TBM_OnLoadMatchesResult_m3496272669 (GameCenter_TBM_t3457554475 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnLoadMatchesResultFailed(System.String)
extern "C"  void GameCenter_TBM_OnLoadMatchesResultFailed_m1352887778 (GameCenter_TBM_t3457554475 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnLoadMatchResult(System.String)
extern "C"  void GameCenter_TBM_OnLoadMatchResult_m553977219 (GameCenter_TBM_t3457554475 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnLoadMatchResultFailed(System.String)
extern "C"  void GameCenter_TBM_OnLoadMatchResultFailed_m2025953802 (GameCenter_TBM_t3457554475 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnUpdateMatchResult(System.String)
extern "C"  void GameCenter_TBM_OnUpdateMatchResult_m2483835104 (GameCenter_TBM_t3457554475 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnUpdateMatchResultFailed(System.String)
extern "C"  void GameCenter_TBM_OnUpdateMatchResultFailed_m4264491707 (GameCenter_TBM_t3457554475 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnMatchFoundResult(System.String)
extern "C"  void GameCenter_TBM_OnMatchFoundResult_m3071203803 (GameCenter_TBM_t3457554475 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnMatchFoundResultFailed(System.String)
extern "C"  void GameCenter_TBM_OnMatchFoundResultFailed_m902584420 (GameCenter_TBM_t3457554475 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnPlayerQuitForMatch(System.String)
extern "C"  void GameCenter_TBM_OnPlayerQuitForMatch_m1752550225 (GameCenter_TBM_t3457554475 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnMatchQuitResult(System.String)
extern "C"  void GameCenter_TBM_OnMatchQuitResult_m1613938384 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnMatchQuitResultFailed(System.String)
extern "C"  void GameCenter_TBM_OnMatchQuitResultFailed_m3487772603 (GameCenter_TBM_t3457554475 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnEndTurnResult(System.String)
extern "C"  void GameCenter_TBM_OnEndTurnResult_m2427542820 (GameCenter_TBM_t3457554475 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnEndTurnResultFailed(System.String)
extern "C"  void GameCenter_TBM_OnEndTurnResultFailed_m3685268939 (GameCenter_TBM_t3457554475 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnEndMatch(System.String)
extern "C"  void GameCenter_TBM_OnEndMatch_m3794400563 (GameCenter_TBM_t3457554475 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnEndMatchResult(System.String)
extern "C"  void GameCenter_TBM_OnEndMatchResult_m1346352428 (GameCenter_TBM_t3457554475 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnRematchResult(System.String)
extern "C"  void GameCenter_TBM_OnRematchResult_m2317365132 (GameCenter_TBM_t3457554475 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnRematchFailed(System.String)
extern "C"  void GameCenter_TBM_OnRematchFailed_m1690537656 (GameCenter_TBM_t3457554475 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnMatchRemoved(System.String)
extern "C"  void GameCenter_TBM_OnMatchRemoved_m4144138616 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnMatchRemoveFailed(System.String)
extern "C"  void GameCenter_TBM_OnMatchRemoveFailed_m2243928377 (GameCenter_TBM_t3457554475 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnMatchInvitationAccepted(System.String)
extern "C"  void GameCenter_TBM_OnMatchInvitationAccepted_m885205948 (GameCenter_TBM_t3457554475 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnMatchInvitationAcceptedFailed(System.String)
extern "C"  void GameCenter_TBM_OnMatchInvitationAcceptedFailed_m191192723 (GameCenter_TBM_t3457554475 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnMatchInvitationDeclined(System.String)
extern "C"  void GameCenter_TBM_OnMatchInvitationDeclined_m1523748141 (GameCenter_TBM_t3457554475 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnMatchInvitationDeclineFailed(System.String)
extern "C"  void GameCenter_TBM_OnMatchInvitationDeclineFailed_m1552701046 (GameCenter_TBM_t3457554475 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::OnTrunReceived(System.String)
extern "C"  void GameCenter_TBM_OnTrunReceived_m503063637 (GameCenter_TBM_t3457554475 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::UpdateMatchInfo(GK_TBM_Match)
extern "C"  void GameCenter_TBM_UpdateMatchInfo_m3751419122 (GameCenter_TBM_t3457554475 * __this, GK_TBM_Match_t132033130 * ___match0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_TBM_Match GameCenter_TBM::ParceMatchInfo(System.String)
extern "C"  GK_TBM_Match_t132033130 * GameCenter_TBM_ParceMatchInfo_m4157653733 (Il2CppObject * __this /* static, unused */, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_TBM_Match GameCenter_TBM::ParceMatchInfo(System.String[],System.Int32)
extern "C"  GK_TBM_Match_t132033130 * GameCenter_TBM_ParceMatchInfo_m206630712 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___MatchData0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::<ActionMatchInfoLoaded>m__1A(GK_TBM_LoadMatchResult)
extern "C"  void GameCenter_TBM_U3CActionMatchInfoLoadedU3Em__1A_m1517185882 (Il2CppObject * __this /* static, unused */, GK_TBM_LoadMatchResult_t1639249273 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::<ActionMatchesInfoLoaded>m__1B(GK_TBM_LoadMatchesResult)
extern "C"  void GameCenter_TBM_U3CActionMatchesInfoLoadedU3Em__1B_m2755204263 (Il2CppObject * __this /* static, unused */, GK_TBM_LoadMatchesResult_t370491735 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::<ActionMatchDataUpdated>m__1C(GK_TBM_MatchDataUpdateResult)
extern "C"  void GameCenter_TBM_U3CActionMatchDataUpdatedU3Em__1C_m3121390525 (Il2CppObject * __this /* static, unused */, GK_TBM_MatchDataUpdateResult_t1356006034 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::<ActionMatchFound>m__1D(GK_TBM_MatchInitResult)
extern "C"  void GameCenter_TBM_U3CActionMatchFoundU3Em__1D_m814204698 (Il2CppObject * __this /* static, unused */, GK_TBM_MatchInitResult_t3847830897 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::<ActionMatchQuit>m__1E(GK_TBM_MatchQuitResult)
extern "C"  void GameCenter_TBM_U3CActionMatchQuitU3Em__1E_m2920795027 (Il2CppObject * __this /* static, unused */, GK_TBM_MatchQuitResult_t1233820656 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::<ActionTrunEnded>m__1F(GK_TBM_EndTrunResult)
extern "C"  void GameCenter_TBM_U3CActionTrunEndedU3Em__1F_m516439293 (Il2CppObject * __this /* static, unused */, GK_TBM_EndTrunResult_t1517380690 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::<ActionMacthEnded>m__20(GK_TBM_MatchEndResult)
extern "C"  void GameCenter_TBM_U3CActionMacthEndedU3Em__20_m274220012 (Il2CppObject * __this /* static, unused */, GK_TBM_MatchEndResult_t3461768810 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::<ActionRematched>m__21(GK_TBM_RematchResult)
extern "C"  void GameCenter_TBM_U3CActionRematchedU3Em__21_m3419371705 (Il2CppObject * __this /* static, unused */, GK_TBM_RematchResult_t3159773700 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::<ActionMatchRemoved>m__22(GK_TBM_MatchRemovedResult)
extern "C"  void GameCenter_TBM_U3CActionMatchRemovedU3Em__22_m449822275 (Il2CppObject * __this /* static, unused */, GK_TBM_MatchRemovedResult_t909126313 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::<ActionMatchInvitationAccepted>m__23(GK_TBM_MatchInitResult)
extern "C"  void GameCenter_TBM_U3CActionMatchInvitationAcceptedU3Em__23_m1676881262 (Il2CppObject * __this /* static, unused */, GK_TBM_MatchInitResult_t3847830897 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::<ActionMatchInvitationDeclined>m__24(GK_TBM_MatchRemovedResult)
extern "C"  void GameCenter_TBM_U3CActionMatchInvitationDeclinedU3Em__24_m3479057214 (Il2CppObject * __this /* static, unused */, GK_TBM_MatchRemovedResult_t909126313 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::<ActionPlayerQuitForMatch>m__25(GK_TBM_Match)
extern "C"  void GameCenter_TBM_U3CActionPlayerQuitForMatchU3Em__25_m197119926 (Il2CppObject * __this /* static, unused */, GK_TBM_Match_t132033130 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenter_TBM::<ActionTrunReceived>m__26(GK_TBM_MatchTurnResult)
extern "C"  void GameCenter_TBM_U3CActionTrunReceivedU3Em__26_m2538507515 (Il2CppObject * __this /* static, unused */, GK_TBM_MatchTurnResult_t3583658160 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
