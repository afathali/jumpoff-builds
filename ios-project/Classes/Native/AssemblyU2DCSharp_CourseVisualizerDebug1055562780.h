﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CourseVisualizerDebug
struct  CourseVisualizerDebug_t1055562780  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text CourseVisualizerDebug::debug1
	Text_t356221433 * ___debug1_2;
	// UnityEngine.UI.Text CourseVisualizerDebug::debug2
	Text_t356221433 * ___debug2_3;
	// UnityEngine.UI.Text CourseVisualizerDebug::debug3
	Text_t356221433 * ___debug3_4;
	// UnityEngine.UI.Button CourseVisualizerDebug::button1
	Button_t2872111280 * ___button1_5;
	// UnityEngine.UI.Button CourseVisualizerDebug::button2
	Button_t2872111280 * ___button2_6;

public:
	inline static int32_t get_offset_of_debug1_2() { return static_cast<int32_t>(offsetof(CourseVisualizerDebug_t1055562780, ___debug1_2)); }
	inline Text_t356221433 * get_debug1_2() const { return ___debug1_2; }
	inline Text_t356221433 ** get_address_of_debug1_2() { return &___debug1_2; }
	inline void set_debug1_2(Text_t356221433 * value)
	{
		___debug1_2 = value;
		Il2CppCodeGenWriteBarrier(&___debug1_2, value);
	}

	inline static int32_t get_offset_of_debug2_3() { return static_cast<int32_t>(offsetof(CourseVisualizerDebug_t1055562780, ___debug2_3)); }
	inline Text_t356221433 * get_debug2_3() const { return ___debug2_3; }
	inline Text_t356221433 ** get_address_of_debug2_3() { return &___debug2_3; }
	inline void set_debug2_3(Text_t356221433 * value)
	{
		___debug2_3 = value;
		Il2CppCodeGenWriteBarrier(&___debug2_3, value);
	}

	inline static int32_t get_offset_of_debug3_4() { return static_cast<int32_t>(offsetof(CourseVisualizerDebug_t1055562780, ___debug3_4)); }
	inline Text_t356221433 * get_debug3_4() const { return ___debug3_4; }
	inline Text_t356221433 ** get_address_of_debug3_4() { return &___debug3_4; }
	inline void set_debug3_4(Text_t356221433 * value)
	{
		___debug3_4 = value;
		Il2CppCodeGenWriteBarrier(&___debug3_4, value);
	}

	inline static int32_t get_offset_of_button1_5() { return static_cast<int32_t>(offsetof(CourseVisualizerDebug_t1055562780, ___button1_5)); }
	inline Button_t2872111280 * get_button1_5() const { return ___button1_5; }
	inline Button_t2872111280 ** get_address_of_button1_5() { return &___button1_5; }
	inline void set_button1_5(Button_t2872111280 * value)
	{
		___button1_5 = value;
		Il2CppCodeGenWriteBarrier(&___button1_5, value);
	}

	inline static int32_t get_offset_of_button2_6() { return static_cast<int32_t>(offsetof(CourseVisualizerDebug_t1055562780, ___button2_6)); }
	inline Button_t2872111280 * get_button2_6() const { return ___button2_6; }
	inline Button_t2872111280 ** get_address_of_button2_6() { return &___button2_6; }
	inline void set_button2_6(Button_t2872111280 * value)
	{
		___button2_6 = value;
		Il2CppCodeGenWriteBarrier(&___button2_6, value);
	}
};

struct CourseVisualizerDebug_t1055562780_StaticFields
{
public:
	// UnityEngine.Events.UnityAction CourseVisualizerDebug::<>f__am$cache5
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache5_7;
	// UnityEngine.Events.UnityAction CourseVisualizerDebug::<>f__am$cache6
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache6_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_7() { return static_cast<int32_t>(offsetof(CourseVisualizerDebug_t1055562780_StaticFields, ___U3CU3Ef__amU24cache5_7)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache5_7() const { return ___U3CU3Ef__amU24cache5_7; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache5_7() { return &___U3CU3Ef__amU24cache5_7; }
	inline void set_U3CU3Ef__amU24cache5_7(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(CourseVisualizerDebug_t1055562780_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
