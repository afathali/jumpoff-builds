﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoursesNumberExceeded
struct  CoursesNumberExceeded_t3039661446  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean CoursesNumberExceeded::wantBlankCourse
	bool ___wantBlankCourse_2;

public:
	inline static int32_t get_offset_of_wantBlankCourse_2() { return static_cast<int32_t>(offsetof(CoursesNumberExceeded_t3039661446, ___wantBlankCourse_2)); }
	inline bool get_wantBlankCourse_2() const { return ___wantBlankCourse_2; }
	inline bool* get_address_of_wantBlankCourse_2() { return &___wantBlankCourse_2; }
	inline void set_wantBlankCourse_2(bool value)
	{
		___wantBlankCourse_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
