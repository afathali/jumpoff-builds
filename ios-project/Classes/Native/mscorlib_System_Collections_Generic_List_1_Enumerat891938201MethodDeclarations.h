﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3439936144(__this, ___l0, method) ((  void (*) (Enumerator_t891938201 *, List_1_t1357208527 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3195992682(__this, method) ((  void (*) (Enumerator_t891938201 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m164049002(__this, method) ((  Il2CppObject * (*) (Enumerator_t891938201 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::Dispose()
#define Enumerator_Dispose_m3369028909(__this, method) ((  void (*) (Enumerator_t891938201 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::VerifyState()
#define Enumerator_VerifyState_m1648808482(__this, method) ((  void (*) (Enumerator_t891938201 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::MoveNext()
#define Enumerator_MoveNext_m2532936862(__this, method) ((  bool (*) (Enumerator_t891938201 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::get_Current()
#define Enumerator_get_Current_m299742469(__this, method) ((  SimpleClassObject_t1988087395 * (*) (Enumerator_t891938201 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
