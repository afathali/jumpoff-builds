﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<FB_AppRequest>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1540617680(__this, ___l0, method) ((  void (*) (Enumerator_t3700130727 *, List_1_t4165401053 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FB_AppRequest>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1513706346(__this, method) ((  void (*) (Enumerator_t3700130727 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<FB_AppRequest>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m145248802(__this, method) ((  Il2CppObject * (*) (Enumerator_t3700130727 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FB_AppRequest>::Dispose()
#define Enumerator_Dispose_m3409065597(__this, method) ((  void (*) (Enumerator_t3700130727 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FB_AppRequest>::VerifyState()
#define Enumerator_VerifyState_m2806609738(__this, method) ((  void (*) (Enumerator_t3700130727 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<FB_AppRequest>::MoveNext()
#define Enumerator_MoveNext_m1933829931(__this, method) ((  bool (*) (Enumerator_t3700130727 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<FB_AppRequest>::get_Current()
#define Enumerator_get_Current_m788952871(__this, method) ((  FB_AppRequest_t501312625 * (*) (Enumerator_t3700130727 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
