﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CustomPlayerUIRow
struct CustomPlayerUIRow_t1758969648;

#include "codegen/il2cpp-codegen.h"

// System.Void CustomPlayerUIRow::.ctor()
extern "C"  void CustomPlayerUIRow__ctor_m2356921773 (CustomPlayerUIRow_t1758969648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomPlayerUIRow::Awake()
extern "C"  void CustomPlayerUIRow_Awake_m2128998124 (CustomPlayerUIRow_t1758969648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomPlayerUIRow::Disable()
extern "C"  void CustomPlayerUIRow_Disable_m2834159693 (CustomPlayerUIRow_t1758969648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
