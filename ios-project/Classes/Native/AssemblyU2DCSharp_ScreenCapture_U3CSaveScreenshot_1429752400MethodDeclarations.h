﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15
struct U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::.ctor()
extern "C"  void U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15__ctor_m3902610011 (U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m571976321 (U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m3122566249 (U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::MoveNext()
extern "C"  bool U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_MoveNext_m3776293209 (U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::Dispose()
extern "C"  void U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_Dispose_m1668498570 (U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::Reset()
extern "C"  void U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_Reset_m2747861880 (U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
