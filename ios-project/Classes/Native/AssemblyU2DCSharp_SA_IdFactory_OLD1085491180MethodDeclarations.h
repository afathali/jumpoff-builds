﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_IdFactory_OLD
struct SA_IdFactory_OLD_t1085491180;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void SA_IdFactory_OLD::.ctor()
extern "C"  void SA_IdFactory_OLD__ctor_m575701095 (SA_IdFactory_OLD_t1085491180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SA_IdFactory_OLD::get_NextId()
extern "C"  int32_t SA_IdFactory_OLD_get_NextId_m1125650192 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA_IdFactory_OLD::get_RandomString()
extern "C"  String_t* SA_IdFactory_OLD_get_RandomString_m4131655785 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
