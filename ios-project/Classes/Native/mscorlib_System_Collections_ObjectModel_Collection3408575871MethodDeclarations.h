﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2231194049MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::.ctor()
#define Collection_1__ctor_m1114851466(__this, method) ((  void (*) (Collection_1_t3408575871 *, const MethodInfo*))Collection_1__ctor_m3383758099_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2999094193(__this, method) ((  bool (*) (Collection_1_t3408575871 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2832435102_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m1864215842(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3408575871 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2795445359_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3554872989(__this, method) ((  Il2CppObject * (*) (Collection_1_t3408575871 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m539985258_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m3937954906(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3408575871 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m916188271_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m1092752652(__this, ___value0, method) ((  bool (*) (Collection_1_t3408575871 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3240760119_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m3867865300(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3408575871 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3460849589_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m3860487789(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3408575871 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3482199744_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m297860469(__this, ___value0, method) ((  void (*) (Collection_1_t3408575871 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1739078822_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1242313554(__this, method) ((  bool (*) (Collection_1_t3408575871 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1442644511_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3277202354(__this, method) ((  Il2CppObject * (*) (Collection_1_t3408575871 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1422512927_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1774601897(__this, method) ((  bool (*) (Collection_1_t3408575871 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2968235316_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3378449782(__this, method) ((  bool (*) (Collection_1_t3408575871 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1990189611_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m3489091209(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3408575871 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m75082808_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m2488855824(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3408575871 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m507853765_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::Add(T)
#define Collection_1_Add_m1218447851(__this, ___item0, method) ((  void (*) (Collection_1_t3408575871 *, JsonSchemaNode_t3866831117 *, const MethodInfo*))Collection_1_Add_m2987402052_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::Clear()
#define Collection_1_Clear_m1316480893(__this, method) ((  void (*) (Collection_1_t3408575871 *, const MethodInfo*))Collection_1_Clear_m1596645192_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::ClearItems()
#define Collection_1_ClearItems_m2366113111(__this, method) ((  void (*) (Collection_1_t3408575871 *, const MethodInfo*))Collection_1_ClearItems_m1175603758_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::Contains(T)
#define Collection_1_Contains_m1334110699(__this, ___item0, method) ((  bool (*) (Collection_1_t3408575871 *, JsonSchemaNode_t3866831117 *, const MethodInfo*))Collection_1_Contains_m2116635914_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m409009393(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3408575871 *, JsonSchemaNodeU5BU5D_t2651346976*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1578267616_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::GetEnumerator()
#define Collection_1_GetEnumerator_m520319470(__this, method) ((  Il2CppObject* (*) (Collection_1_t3408575871 *, const MethodInfo*))Collection_1_GetEnumerator_m2963411583_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::IndexOf(T)
#define Collection_1_IndexOf_m2274843661(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3408575871 *, JsonSchemaNode_t3866831117 *, const MethodInfo*))Collection_1_IndexOf_m3885709710_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::Insert(System.Int32,T)
#define Collection_1_Insert_m2373062368(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3408575871 *, int32_t, JsonSchemaNode_t3866831117 *, const MethodInfo*))Collection_1_Insert_m2334889193_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m2221333451(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3408575871 *, int32_t, JsonSchemaNode_t3866831117 *, const MethodInfo*))Collection_1_InsertItem_m3611385334_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::get_Items()
#define Collection_1_get_Items_m870434275(__this, method) ((  Il2CppObject* (*) (Collection_1_t3408575871 *, const MethodInfo*))Collection_1_get_Items_m2226526738_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::Remove(T)
#define Collection_1_Remove_m2265364262(__this, ___item0, method) ((  bool (*) (Collection_1_t3408575871 *, JsonSchemaNode_t3866831117 *, const MethodInfo*))Collection_1_Remove_m452558737_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m2556761444(__this, ___index0, method) ((  void (*) (Collection_1_t3408575871 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1632496813_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m1897453102(__this, ___index0, method) ((  void (*) (Collection_1_t3408575871 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4104600353_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::get_Count()
#define Collection_1_get_Count_m2144426218(__this, method) ((  int32_t (*) (Collection_1_t3408575871 *, const MethodInfo*))Collection_1_get_Count_m2250721247_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::get_Item(System.Int32)
#define Collection_1_get_Item_m2230512770(__this, ___index0, method) ((  JsonSchemaNode_t3866831117 * (*) (Collection_1_t3408575871 *, int32_t, const MethodInfo*))Collection_1_get_Item_m266052953_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m2319255865(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3408575871 *, int32_t, JsonSchemaNode_t3866831117 *, const MethodInfo*))Collection_1_set_Item_m3489932746_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m529821040(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3408575871 *, int32_t, JsonSchemaNode_t3866831117 *, const MethodInfo*))Collection_1_SetItem_m1075410277_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m1528817463(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3443424420_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m3441334439(__this /* static, unused */, ___item0, method) ((  JsonSchemaNode_t3866831117 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1521356246_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m2700545259(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m215419136_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m3267333931(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m328767958_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaNode>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m304088076(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3594284193_gshared)(__this /* static, unused */, ___list0, method)
