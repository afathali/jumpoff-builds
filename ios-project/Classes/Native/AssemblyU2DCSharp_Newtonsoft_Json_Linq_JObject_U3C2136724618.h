﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t28167840;
// Newtonsoft.Json.Linq.JProperty
struct JProperty_t2956441399;
// Newtonsoft.Json.Linq.JObject
struct JObject_t278519297;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22224768497.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator11
struct  U3CGetEnumeratorU3Ec__Iterator11_t2136724618  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator11::<$s_246>__0
	Il2CppObject* ___U3CU24s_246U3E__0_0;
	// Newtonsoft.Json.Linq.JProperty Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator11::<property>__1
	JProperty_t2956441399 * ___U3CpropertyU3E__1_1;
	// System.Int32 Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator11::$PC
	int32_t ___U24PC_2;
	// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator11::$current
	KeyValuePair_2_t2224768497  ___U24current_3;
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator11::<>f__this
	JObject_t278519297 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CU24s_246U3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator11_t2136724618, ___U3CU24s_246U3E__0_0)); }
	inline Il2CppObject* get_U3CU24s_246U3E__0_0() const { return ___U3CU24s_246U3E__0_0; }
	inline Il2CppObject** get_address_of_U3CU24s_246U3E__0_0() { return &___U3CU24s_246U3E__0_0; }
	inline void set_U3CU24s_246U3E__0_0(Il2CppObject* value)
	{
		___U3CU24s_246U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_246U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CpropertyU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator11_t2136724618, ___U3CpropertyU3E__1_1)); }
	inline JProperty_t2956441399 * get_U3CpropertyU3E__1_1() const { return ___U3CpropertyU3E__1_1; }
	inline JProperty_t2956441399 ** get_address_of_U3CpropertyU3E__1_1() { return &___U3CpropertyU3E__1_1; }
	inline void set_U3CpropertyU3E__1_1(JProperty_t2956441399 * value)
	{
		___U3CpropertyU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpropertyU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator11_t2136724618, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator11_t2136724618, ___U24current_3)); }
	inline KeyValuePair_2_t2224768497  get_U24current_3() const { return ___U24current_3; }
	inline KeyValuePair_2_t2224768497 * get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(KeyValuePair_2_t2224768497  value)
	{
		___U24current_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator11_t2136724618, ___U3CU3Ef__this_4)); }
	inline JObject_t278519297 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline JObject_t278519297 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(JObject_t278519297 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
