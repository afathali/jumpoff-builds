﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SK_CloudService
struct SK_CloudService_t588287925;
// System.Action`1<SK_AuthorizationResult>
struct Action_1_t1849553253;
// System.Action`1<SK_RequestCapabilitieResult>
struct Action_1_t2311963226;
// System.Action`1<SK_RequestStorefrontIdentifierResult>
struct Action_1_t914276544;
// System.String
struct String_t;
// SK_AuthorizationResult
struct SK_AuthorizationResult_t2047753871;
// SK_RequestCapabilitieResult
struct SK_RequestCapabilitieResult_t2510163844;
// SK_RequestStorefrontIdentifierResult
struct SK_RequestStorefrontIdentifierResult_t1112477162;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_AuthorizationResu2047753871.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_RequestCapabiliti2510163844.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_RequestStorefront1112477162.h"

// System.Void SK_CloudService::.ctor()
extern "C"  void SK_CloudService__ctor_m2360440912 (SK_CloudService_t588287925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::.cctor()
extern "C"  void SK_CloudService__cctor_m1424263585 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::add_OnAuthorizationFinished(System.Action`1<SK_AuthorizationResult>)
extern "C"  void SK_CloudService_add_OnAuthorizationFinished_m427846867 (Il2CppObject * __this /* static, unused */, Action_1_t1849553253 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::remove_OnAuthorizationFinished(System.Action`1<SK_AuthorizationResult>)
extern "C"  void SK_CloudService_remove_OnAuthorizationFinished_m474800222 (Il2CppObject * __this /* static, unused */, Action_1_t1849553253 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::add_OnCapabilitiesRequestFinished(System.Action`1<SK_RequestCapabilitieResult>)
extern "C"  void SK_CloudService_add_OnCapabilitiesRequestFinished_m1336743292 (Il2CppObject * __this /* static, unused */, Action_1_t2311963226 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::remove_OnCapabilitiesRequestFinished(System.Action`1<SK_RequestCapabilitieResult>)
extern "C"  void SK_CloudService_remove_OnCapabilitiesRequestFinished_m1749211023 (Il2CppObject * __this /* static, unused */, Action_1_t2311963226 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::add_OnStorefrontIdentifierRequestFinished(System.Action`1<SK_RequestStorefrontIdentifierResult>)
extern "C"  void SK_CloudService_add_OnStorefrontIdentifierRequestFinished_m3166541867 (Il2CppObject * __this /* static, unused */, Action_1_t914276544 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::remove_OnStorefrontIdentifierRequestFinished(System.Action`1<SK_RequestStorefrontIdentifierResult>)
extern "C"  void SK_CloudService_remove_OnStorefrontIdentifierRequestFinished_m3563341042 (Il2CppObject * __this /* static, unused */, Action_1_t914276544 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::Awake()
extern "C"  void SK_CloudService_Awake_m1413994399 (SK_CloudService_t588287925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::RequestAuthorization()
extern "C"  void SK_CloudService_RequestAuthorization_m3901679232 (SK_CloudService_t588287925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::RequestCapabilities()
extern "C"  void SK_CloudService_RequestCapabilities_m3908666353 (SK_CloudService_t588287925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::RequestStorefrontIdentifier()
extern "C"  void SK_CloudService_RequestStorefrontIdentifier_m2230795202 (SK_CloudService_t588287925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SK_CloudService::get_AuthorizationStatus()
extern "C"  int32_t SK_CloudService_get_AuthorizationStatus_m3074601638 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::Event_AuthorizationFinished(System.String)
extern "C"  void SK_CloudService_Event_AuthorizationFinished_m2104971666 (SK_CloudService_t588287925 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::Event_RequestCapabilitieSsuccess(System.String)
extern "C"  void SK_CloudService_Event_RequestCapabilitieSsuccess_m182284955 (SK_CloudService_t588287925 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::Event_RequestCapabilitiesFailed(System.String)
extern "C"  void SK_CloudService_Event_RequestCapabilitiesFailed_m149570975 (SK_CloudService_t588287925 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::Event_RequestStorefrontIdentifierSsuccess(System.String)
extern "C"  void SK_CloudService_Event_RequestStorefrontIdentifierSsuccess_m2080645007 (SK_CloudService_t588287925 * __this, String_t* ___storefrontIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::Event_RequestStorefrontIdentifierFailed(System.String)
extern "C"  void SK_CloudService_Event_RequestStorefrontIdentifierFailed_m3271524126 (SK_CloudService_t588287925 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::<OnAuthorizationFinished>m__55(SK_AuthorizationResult)
extern "C"  void SK_CloudService_U3COnAuthorizationFinishedU3Em__55_m641725672 (Il2CppObject * __this /* static, unused */, SK_AuthorizationResult_t2047753871 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::<OnCapabilitiesRequestFinished>m__56(SK_RequestCapabilitieResult)
extern "C"  void SK_CloudService_U3COnCapabilitiesRequestFinishedU3Em__56_m2689700734 (Il2CppObject * __this /* static, unused */, SK_RequestCapabilitieResult_t2510163844 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_CloudService::<OnStorefrontIdentifierRequestFinished>m__57(SK_RequestStorefrontIdentifierResult)
extern "C"  void SK_CloudService_U3COnStorefrontIdentifierRequestFinishedU3Em__57_m232922430 (Il2CppObject * __this /* static, unused */, SK_RequestStorefrontIdentifierResult_t1112477162 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
