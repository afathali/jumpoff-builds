﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopUpExamples
struct PopUpExamples_t1949923855;
// ISN_Locale
struct ISN_Locale_t2162888085;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSDialogResult3739241316.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_Locale2162888085.h"

// System.Void PopUpExamples::.ctor()
extern "C"  void PopUpExamples__ctor_m2838665914 (PopUpExamples_t1949923855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::Awake()
extern "C"  void PopUpExamples_Awake_m452222105 (PopUpExamples_t1949923855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::OnGUI()
extern "C"  void PopUpExamples_OnGUI_m2939815878 (PopUpExamples_t1949923855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::HidePreloader()
extern "C"  void PopUpExamples_HidePreloader_m1096461688 (PopUpExamples_t1949923855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::dismissAlert()
extern "C"  void PopUpExamples_dismissAlert_m1610860206 (PopUpExamples_t1949923855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::onRatePopUpClose(IOSDialogResult)
extern "C"  void PopUpExamples_onRatePopUpClose_m4222499171 (PopUpExamples_t1949923855 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::onDialogClose(IOSDialogResult)
extern "C"  void PopUpExamples_onDialogClose_m639561367 (PopUpExamples_t1949923855 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::onMessageClose()
extern "C"  void PopUpExamples_onMessageClose_m108063456 (PopUpExamples_t1949923855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopUpExamples::GetLocale(ISN_Locale)
extern "C"  void PopUpExamples_GetLocale_m4163169529 (PopUpExamples_t1949923855 * __this, ISN_Locale_t2162888085 * ___locale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
