﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnOtherFeaturesPreview
struct AnOtherFeaturesPreview_t3806064552;
// AN_NetworkInfo
struct AN_NetworkInfo_t247793570;
// System.String
struct String_t;
// AN_PackageCheckResult
struct AN_PackageCheckResult_t3695415755;
// AndroidImagePickResult
struct AndroidImagePickResult_t1791733552;
// GallerySaveResult
struct GallerySaveResult_t1643856950;
// PackageAppInfo
struct PackageAppInfo_t260912591;
// AN_Locale
struct AN_Locale_t121755426;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AN_NetworkInfo247793570.h"
#include "AssemblyU2DCSharp_AN_LicenseRequestResult2331370561.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AN_PackageCheckResult3695415755.h"
#include "AssemblyU2DCSharp_AndroidImagePickResult1791733552.h"
#include "AssemblyU2DCSharp_GallerySaveResult1643856950.h"
#include "AssemblyU2DCSharp_PackageAppInfo260912591.h"
#include "AssemblyU2DCSharp_AN_Locale121755426.h"

// System.Void AnOtherFeaturesPreview::.ctor()
extern "C"  void AnOtherFeaturesPreview__ctor_m436327381 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::Start()
extern "C"  void AnOtherFeaturesPreview_Start_m477962485 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::SaveToGalalry()
extern "C"  void AnOtherFeaturesPreview_SaveToGalalry_m2970434287 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::SaveScreenshot()
extern "C"  void AnOtherFeaturesPreview_SaveScreenshot_m2856272282 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::GetImageFromGallery()
extern "C"  void AnOtherFeaturesPreview_GetImageFromGallery_m3240775378 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::GetImageFromCamera()
extern "C"  void AnOtherFeaturesPreview_GetImageFromCamera_m453358943 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::CheckForTV()
extern "C"  void AnOtherFeaturesPreview_CheckForTV_m898590466 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::LoadNetworkInfo()
extern "C"  void AnOtherFeaturesPreview_LoadNetworkInfo_m3187001299 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::HandleActionNetworkInfoLoaded(AN_NetworkInfo)
extern "C"  void AnOtherFeaturesPreview_HandleActionNetworkInfoLoaded_m915968710 (AnOtherFeaturesPreview_t3806064552 * __this, AN_NetworkInfo_t247793570 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::CheckAppInstalation()
extern "C"  void AnOtherFeaturesPreview_CheckAppInstalation_m3198740016 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::RunApp()
extern "C"  void AnOtherFeaturesPreview_RunApp_m1925521581 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::CheckAppLicense()
extern "C"  void AnOtherFeaturesPreview_CheckAppLicense_m1773160929 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::LicenseRequestResult(AN_LicenseRequestResult)
extern "C"  void AnOtherFeaturesPreview_LicenseRequestResult_m967392965 (AnOtherFeaturesPreview_t3806064552 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::EnableImmersiveMode()
extern "C"  void AnOtherFeaturesPreview_EnableImmersiveMode_m68225418 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::GetAndroidId()
extern "C"  void AnOtherFeaturesPreview_GetAndroidId_m1031816837 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::OnAndroidIdLoaded(System.String)
extern "C"  void AnOtherFeaturesPreview_OnAndroidIdLoaded_m2191903499 (AnOtherFeaturesPreview_t3806064552 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::LoadAppInfo()
extern "C"  void AnOtherFeaturesPreview_LoadAppInfo_m886107048 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::LoadAdressBook()
extern "C"  void AnOtherFeaturesPreview_LoadAdressBook_m1451834684 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::OnDeviceTypeChecked()
extern "C"  void AnOtherFeaturesPreview_OnDeviceTypeChecked_m1294474471 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::OnPackageCheckResult(AN_PackageCheckResult)
extern "C"  void AnOtherFeaturesPreview_OnPackageCheckResult_m3545054224 (AnOtherFeaturesPreview_t3806064552 * __this, AN_PackageCheckResult_t3695415755 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::OnContactsLoaded()
extern "C"  void AnOtherFeaturesPreview_OnContactsLoaded_m1144383558 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::OnImagePicked(AndroidImagePickResult)
extern "C"  void AnOtherFeaturesPreview_OnImagePicked_m3453509855 (AnOtherFeaturesPreview_t3806064552 * __this, AndroidImagePickResult_t1791733552 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::OnImageSaved(GallerySaveResult)
extern "C"  void AnOtherFeaturesPreview_OnImageSaved_m811020822 (AnOtherFeaturesPreview_t3806064552 * __this, GallerySaveResult_t1643856950 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::OnPackageInfoLoaded(PackageAppInfo)
extern "C"  void AnOtherFeaturesPreview_OnPackageInfoLoaded_m1908090048 (AnOtherFeaturesPreview_t3806064552 * __this, PackageAppInfo_t260912591 * ___PacakgeInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::LoadInternal()
extern "C"  void AnOtherFeaturesPreview_LoadInternal_m2897999242 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::LoadExternal()
extern "C"  void AnOtherFeaturesPreview_LoadExternal_m2219258684 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::LoadLocaleInfo()
extern "C"  void AnOtherFeaturesPreview_LoadLocaleInfo_m2622868785 (AnOtherFeaturesPreview_t3806064552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::LocaleInfoLoaded(AN_Locale)
extern "C"  void AnOtherFeaturesPreview_LocaleInfoLoaded_m4044112360 (AnOtherFeaturesPreview_t3806064552 * __this, AN_Locale_t121755426 * ___locale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::ExternalStoragePathLoaded(System.String)
extern "C"  void AnOtherFeaturesPreview_ExternalStoragePathLoaded_m2167854577 (AnOtherFeaturesPreview_t3806064552 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnOtherFeaturesPreview::InternalStoragePathLoaded(System.String)
extern "C"  void AnOtherFeaturesPreview_InternalStoragePathLoaded_m908330255 (AnOtherFeaturesPreview_t3806064552 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
