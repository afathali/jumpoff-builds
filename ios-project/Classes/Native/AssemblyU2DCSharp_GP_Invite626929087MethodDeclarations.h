﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_Invite
struct GP_Invite_t626929087;

#include "codegen/il2cpp-codegen.h"

// System.Void GP_Invite::.ctor()
extern "C"  void GP_Invite__ctor_m3168950740 (GP_Invite_t626929087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
