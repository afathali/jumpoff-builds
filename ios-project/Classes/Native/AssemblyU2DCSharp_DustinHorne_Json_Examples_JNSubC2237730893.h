﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_DustinHorne_Json_Examples_JNSimp1947941312.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DustinHorne.Json.Examples.JNSubClassModel
struct  JNSubClassModel_t2237730893  : public JNSimpleObjectModel_t1947941312
{
public:
	// System.String DustinHorne.Json.Examples.JNSubClassModel::<SubClassStringValue>k__BackingField
	String_t* ___U3CSubClassStringValueU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CSubClassStringValueU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(JNSubClassModel_t2237730893, ___U3CSubClassStringValueU3Ek__BackingField_5)); }
	inline String_t* get_U3CSubClassStringValueU3Ek__BackingField_5() const { return ___U3CSubClassStringValueU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CSubClassStringValueU3Ek__BackingField_5() { return &___U3CSubClassStringValueU3Ek__BackingField_5; }
	inline void set_U3CSubClassStringValueU3Ek__BackingField_5(String_t* value)
	{
		___U3CSubClassStringValueU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSubClassStringValueU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
