﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_EditorAd
struct SA_EditorAd_t1410159287;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action
struct Action_t3226471752;
// SA_Ad_EditorUIController
struct SA_Ad_EditorUIController_t4080214878;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"

// System.Void SA_EditorAd::.ctor()
extern "C"  void SA_EditorAd__ctor_m221181330 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::.cctor()
extern "C"  void SA_EditorAd__cctor_m1704305763 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::add_OnInterstitialFinished(System.Action`1<System.Boolean>)
extern "C"  void SA_EditorAd_add_OnInterstitialFinished_m3371953136 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::remove_OnInterstitialFinished(System.Action`1<System.Boolean>)
extern "C"  void SA_EditorAd_remove_OnInterstitialFinished_m3646045237 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::add_OnInterstitialLoadComplete(System.Action`1<System.Boolean>)
extern "C"  void SA_EditorAd_add_OnInterstitialLoadComplete_m3568679983 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::remove_OnInterstitialLoadComplete(System.Action`1<System.Boolean>)
extern "C"  void SA_EditorAd_remove_OnInterstitialLoadComplete_m3690804946 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::add_OnInterstitialLeftApplication(System.Action)
extern "C"  void SA_EditorAd_add_OnInterstitialLeftApplication_m455497281 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::remove_OnInterstitialLeftApplication(System.Action)
extern "C"  void SA_EditorAd_remove_OnInterstitialLeftApplication_m4089595218 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::add_OnVideoFinished(System.Action`1<System.Boolean>)
extern "C"  void SA_EditorAd_add_OnVideoFinished_m1331794961 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::remove_OnVideoFinished(System.Action`1<System.Boolean>)
extern "C"  void SA_EditorAd_remove_OnVideoFinished_m3221228756 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::add_OnVideoLoadComplete(System.Action`1<System.Boolean>)
extern "C"  void SA_EditorAd_add_OnVideoLoadComplete_m3949273972 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::remove_OnVideoLoadComplete(System.Action`1<System.Boolean>)
extern "C"  void SA_EditorAd_remove_OnVideoLoadComplete_m4259709539 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::add_OnVideoLeftApplication(System.Action)
extern "C"  void SA_EditorAd_add_OnVideoLeftApplication_m1976118804 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::remove_OnVideoLeftApplication(System.Action)
extern "C"  void SA_EditorAd_remove_OnVideoLeftApplication_m292313749 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::Awake()
extern "C"  void SA_EditorAd_Awake_m2091180225 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::SetFillRate(System.Int32)
extern "C"  void SA_EditorAd_SetFillRate_m841461312 (SA_EditorAd_t1410159287 * __this, int32_t ___fillRate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::LoadInterstitial()
extern "C"  void SA_EditorAd_LoadInterstitial_m92056142 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::ShowInterstitial()
extern "C"  void SA_EditorAd_ShowInterstitial_m668797125 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::LoadVideo()
extern "C"  void SA_EditorAd_LoadVideo_m534603967 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::ShowVideo()
extern "C"  void SA_EditorAd_ShowVideo_m434293846 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA_EditorAd::get_IsVideoReady()
extern "C"  bool SA_EditorAd_get_IsVideoReady_m2967159589 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA_EditorAd::get_IsVideoLoading()
extern "C"  bool SA_EditorAd_get_IsVideoLoading_m597033854 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA_EditorAd::get_IsInterstitialReady()
extern "C"  bool SA_EditorAd_get_IsInterstitialReady_m4069673862 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA_EditorAd::get_IsInterstitialLoading()
extern "C"  bool SA_EditorAd_get_IsInterstitialLoading_m917384547 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA_EditorAd::get_HasFill()
extern "C"  bool SA_EditorAd_get_HasFill_m4283363370 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SA_EditorAd::get_FillRate()
extern "C"  int32_t SA_EditorAd_get_FillRate_m2541661572 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SA_Ad_EditorUIController SA_EditorAd::get_EditorUI()
extern "C"  SA_Ad_EditorUIController_t4080214878 * SA_EditorAd_get_EditorUI_m1488010437 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::OnVideoRequestComplete()
extern "C"  void SA_EditorAd_OnVideoRequestComplete_m2394915786 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::OnInterstitialRequestComplete()
extern "C"  void SA_EditorAd_OnInterstitialRequestComplete_m829538427 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::OnInterstitialFinished_UIEvent(System.Boolean)
extern "C"  void SA_EditorAd_OnInterstitialFinished_UIEvent_m3974037417 (SA_EditorAd_t1410159287 * __this, bool ___IsRewarded0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::OnVideoFinished_UIEvent(System.Boolean)
extern "C"  void SA_EditorAd_OnVideoFinished_UIEvent_m3652767796 (SA_EditorAd_t1410159287 * __this, bool ___IsRewarded0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::OnInterstitialLeftApplication_UIEvent()
extern "C"  void SA_EditorAd_OnInterstitialLeftApplication_UIEvent_m582665065 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::OnVideoLeftApplication_UIEvent()
extern "C"  void SA_EditorAd_OnVideoLeftApplication_UIEvent_m3753138856 (SA_EditorAd_t1410159287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::<OnInterstitialFinished>m__85(System.Boolean)
extern "C"  void SA_EditorAd_U3COnInterstitialFinishedU3Em__85_m1726536410 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::<OnInterstitialLoadComplete>m__86(System.Boolean)
extern "C"  void SA_EditorAd_U3COnInterstitialLoadCompleteU3Em__86_m3467906502 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::<OnInterstitialLeftApplication>m__87()
extern "C"  void SA_EditorAd_U3COnInterstitialLeftApplicationU3Em__87_m2918659926 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::<OnVideoFinished>m__88(System.Boolean)
extern "C"  void SA_EditorAd_U3COnVideoFinishedU3Em__88_m143744916 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::<OnVideoLoadComplete>m__89(System.Boolean)
extern "C"  void SA_EditorAd_U3COnVideoLoadCompleteU3Em__89_m3518109402 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorAd::<OnVideoLeftApplication>m__8A()
extern "C"  void SA_EditorAd_U3COnVideoLeftApplicationU3Em__8A_m1595797995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
