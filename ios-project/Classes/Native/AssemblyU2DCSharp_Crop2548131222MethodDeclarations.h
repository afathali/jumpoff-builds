﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Crop
struct Crop_t2548131222;

#include "codegen/il2cpp-codegen.h"

// System.Void Crop::.ctor()
extern "C"  void Crop__ctor_m858826385 (Crop_t2548131222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Crop::Start()
extern "C"  void Crop_Start_m459816129 (Crop_t2548131222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Crop::Update_OneFinger()
extern "C"  void Crop_Update_OneFinger_m252751350 (Crop_t2548131222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Crop::CancelCurrentTouch()
extern "C"  void Crop_CancelCurrentTouch_m3593446823 (Crop_t2548131222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Crop::Update_TwoFingers()
extern "C"  void Crop_Update_TwoFingers_m1220766925 (Crop_t2548131222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Crop::Update()
extern "C"  void Crop_Update_m2498249944 (Crop_t2548131222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
