﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GP_TBM_Match>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m566998204(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t947202465 *, String_t*, GP_TBM_Match_t1275077981 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,GP_TBM_Match>::get_Key()
#define KeyValuePair_2_get_Key_m2613230554(__this, method) ((  String_t* (*) (KeyValuePair_2_t947202465 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GP_TBM_Match>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3511684341(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t947202465 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,GP_TBM_Match>::get_Value()
#define KeyValuePair_2_get_Value_m3432087874(__this, method) ((  GP_TBM_Match_t1275077981 * (*) (KeyValuePair_2_t947202465 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GP_TBM_Match>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2411175949(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t947202465 *, GP_TBM_Match_t1275077981 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,GP_TBM_Match>::ToString()
#define KeyValuePair_2_ToString_m2608919679(__this, method) ((  String_t* (*) (KeyValuePair_2_t947202465 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
