﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_SettingsActivityAction
struct AN_SettingsActivityAction_t2396074174;

#include "codegen/il2cpp-codegen.h"

// System.Void AN_SettingsActivityAction::.ctor()
extern "C"  void AN_SettingsActivityAction__ctor_m1184271149 (AN_SettingsActivityAction_t2396074174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
