﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_PlusShareListener/PlusShareCallback
struct PlusShareCallback_t2327980784;
// System.Object
struct Il2CppObject;
// AN_PlusShareResult
struct AN_PlusShareResult_t3488688014;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_AN_PlusShareResult3488688014.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void AN_PlusShareListener/PlusShareCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PlusShareCallback__ctor_m3951498183 (PlusShareCallback_t2327980784 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusShareListener/PlusShareCallback::Invoke(AN_PlusShareResult)
extern "C"  void PlusShareCallback_Invoke_m2720685393 (PlusShareCallback_t2327980784 * __this, AN_PlusShareResult_t3488688014 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult AN_PlusShareListener/PlusShareCallback::BeginInvoke(AN_PlusShareResult,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PlusShareCallback_BeginInvoke_m1990307986 (PlusShareCallback_t2327980784 * __this, AN_PlusShareResult_t3488688014 * ___result0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusShareListener/PlusShareCallback::EndInvoke(System.IAsyncResult)
extern "C"  void PlusShareCallback_EndInvoke_m2374522921 (PlusShareCallback_t2327980784 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
