﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_DeleteSnapshotResult
struct GP_DeleteSnapshotResult_t3306790010;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_DeleteSnapshotResult::.ctor(System.String)
extern "C"  void GP_DeleteSnapshotResult__ctor_m4169981357 (GP_DeleteSnapshotResult_t3306790010 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_DeleteSnapshotResult::SetId(System.String)
extern "C"  void GP_DeleteSnapshotResult_SetId_m2919455990 (GP_DeleteSnapshotResult_t3306790010 * __this, String_t* ___sid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GP_DeleteSnapshotResult::get_SnapshotId()
extern "C"  String_t* GP_DeleteSnapshotResult_get_SnapshotId_m3516711558 (GP_DeleteSnapshotResult_t3306790010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
