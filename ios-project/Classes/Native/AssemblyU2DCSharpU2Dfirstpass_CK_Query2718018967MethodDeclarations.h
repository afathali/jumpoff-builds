﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CK_Query
struct CK_Query_t2718018967;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void CK_Query::.ctor(System.String,System.String)
extern "C"  void CK_Query__ctor_m2900360620 (CK_Query_t2718018967 * __this, String_t* ___predicate0, String_t* ___recordType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CK_Query::get_Predicate()
extern "C"  String_t* CK_Query_get_Predicate_m499686793 (CK_Query_t2718018967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CK_Query::get_RecordType()
extern "C"  String_t* CK_Query_get_RecordType_m979925537 (CK_Query_t2718018967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
