﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_Locale
struct ISN_Locale_t2162888085;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ISN_Locale::.ctor(System.String,System.String,System.String,System.String)
extern "C"  void ISN_Locale__ctor_m3308182620 (ISN_Locale_t2162888085 * __this, String_t* ___countryCode0, String_t* ___contryName1, String_t* ___languageCode2, String_t* ___languageName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_Locale::get_CountryCode()
extern "C"  String_t* ISN_Locale_get_CountryCode_m1863389635 (ISN_Locale_t2162888085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_Locale::get_DisplayCountry()
extern "C"  String_t* ISN_Locale_get_DisplayCountry_m1035608744 (ISN_Locale_t2162888085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_Locale::get_LanguageCode()
extern "C"  String_t* ISN_Locale_get_LanguageCode_m3621591829 (ISN_Locale_t2162888085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_Locale::get_DisplayLanguage()
extern "C"  String_t* ISN_Locale_get_DisplayLanguage_m3941091078 (ISN_Locale_t2162888085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
