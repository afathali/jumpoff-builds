﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_MatchesSortOrder
struct GP_MatchesSortOrder_t1055105683;

#include "codegen/il2cpp-codegen.h"

// System.Void GP_MatchesSortOrder::.ctor()
extern "C"  void GP_MatchesSortOrder__ctor_m3905832182 (GP_MatchesSortOrder_t1055105683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_MatchesSortOrder::Start()
extern "C"  void GP_MatchesSortOrder_Start_m1854635290 (GP_MatchesSortOrder_t1055105683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_MatchesSortOrder::Update()
extern "C"  void GP_MatchesSortOrder_Update_m1529219507 (GP_MatchesSortOrder_t1055105683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
