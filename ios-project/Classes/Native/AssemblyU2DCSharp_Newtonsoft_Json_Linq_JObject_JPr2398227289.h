﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_ObjectModel_KeyedColle3479973989.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject/JPropertKeyedCollection
struct  JPropertKeyedCollection_t2398227289  : public KeyedCollection_2_t3479973989
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
