﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BillingExample
struct BillingExample_t2738956801;

#include "codegen/il2cpp-codegen.h"

// System.Void BillingExample::.ctor()
extern "C"  void BillingExample__ctor_m1813243846 (BillingExample_t2738956801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BillingExample::init()
extern "C"  void BillingExample_init_m338661274 (BillingExample_t2738956801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BillingExample::FixedUpdate()
extern "C"  void BillingExample_FixedUpdate_m1183626735 (BillingExample_t2738956801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BillingExample::SuccsesPurchase()
extern "C"  void BillingExample_SuccsesPurchase_m3151591566 (BillingExample_t2738956801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BillingExample::FailPurchase()
extern "C"  void BillingExample_FailPurchase_m2836200485 (BillingExample_t2738956801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BillingExample::ConsumeProduct()
extern "C"  void BillingExample_ConsumeProduct_m1353808219 (BillingExample_t2738956801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
