﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Models.Error
struct Error_t445207774;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SA.Common.Models.Error::.ctor()
extern "C"  void Error__ctor_m2318891734 (Error_t445207774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.Error::.ctor(System.Int32,System.String)
extern "C"  void Error__ctor_m3167320083 (Error_t445207774 * __this, int32_t ___code0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.Error::.ctor(System.String)
extern "C"  void Error__ctor_m994168232 (Error_t445207774 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SA.Common.Models.Error::get_Code()
extern "C"  int32_t Error_get_Code_m2921933138 (Error_t445207774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA.Common.Models.Error::get_Message()
extern "C"  String_t* Error_get_Message_m419911227 (Error_t445207774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
