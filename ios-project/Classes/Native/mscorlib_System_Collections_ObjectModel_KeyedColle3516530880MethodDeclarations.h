﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_KeyedColle1294060137MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::.ctor()
#define KeyedCollection_2__ctor_m2068790253(__this, method) ((  void (*) (KeyedCollection_2_t3516530880 *, const MethodInfo*))KeyedCollection_2__ctor_m3929680866_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>,System.Int32)
#define KeyedCollection_2__ctor_m4267599787(__this, ___comparer0, ___dictionaryCreationThreshold1, method) ((  void (*) (KeyedCollection_2_t3516530880 *, Il2CppObject*, int32_t, const MethodInfo*))KeyedCollection_2__ctor_m1398787942_gshared)(__this, ___comparer0, ___dictionaryCreationThreshold1, method)
// System.Boolean System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::Contains(TKey)
#define KeyedCollection_2_Contains_m3934665737(__this, ___key0, method) ((  bool (*) (KeyedCollection_2_t3516530880 *, String_t*, const MethodInfo*))KeyedCollection_2_Contains_m3918219472_gshared)(__this, ___key0, method)
// System.Int32 System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::IndexOfKey(TKey)
#define KeyedCollection_2_IndexOfKey_m2935406142(__this, ___key0, method) ((  int32_t (*) (KeyedCollection_2_t3516530880 *, String_t*, const MethodInfo*))KeyedCollection_2_IndexOfKey_m872387181_gshared)(__this, ___key0, method)
// TItem System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::get_Item(TKey)
#define KeyedCollection_2_get_Item_m1426678828(__this, ___key0, method) ((  EnumValue_1_t2589200904 * (*) (KeyedCollection_2_t3516530880 *, String_t*, const MethodInfo*))KeyedCollection_2_get_Item_m1357505817_gshared)(__this, ___key0, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::ClearItems()
#define KeyedCollection_2_ClearItems_m3657090036(__this, method) ((  void (*) (KeyedCollection_2_t3516530880 *, const MethodInfo*))KeyedCollection_2_ClearItems_m3110573139_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::InsertItem(System.Int32,TItem)
#define KeyedCollection_2_InsertItem_m878023231(__this, ___index0, ___item1, method) ((  void (*) (KeyedCollection_2_t3516530880 *, int32_t, EnumValue_1_t2589200904 *, const MethodInfo*))KeyedCollection_2_InsertItem_m2740229522_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::RemoveItem(System.Int32)
#define KeyedCollection_2_RemoveItem_m3641524427(__this, ___index0, method) ((  void (*) (KeyedCollection_2_t3516530880 *, int32_t, const MethodInfo*))KeyedCollection_2_RemoveItem_m1708519878_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::SetItem(System.Int32,TItem)
#define KeyedCollection_2_SetItem_m3423353488(__this, ___index0, ___item1, method) ((  void (*) (KeyedCollection_2_t3516530880 *, int32_t, EnumValue_1_t2589200904 *, const MethodInfo*))KeyedCollection_2_SetItem_m4134855463_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IDictionary`2<TKey,TItem> System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::get_Dictionary()
#define KeyedCollection_2_get_Dictionary_m3357932271(__this, method) ((  Il2CppObject* (*) (KeyedCollection_2_t3516530880 *, const MethodInfo*))KeyedCollection_2_get_Dictionary_m686852578_gshared)(__this, method)
