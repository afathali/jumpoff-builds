﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Data.Converter
struct Converter_t243910518;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SA.Common.Data.Converter::.ctor()
extern "C"  void Converter__ctor_m1827483542 (Converter_t243910518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA.Common.Data.Converter::SerializeArray(System.String[],System.String)
extern "C"  String_t* Converter_SerializeArray_m1363968320 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___array0, String_t* ___splitter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] SA.Common.Data.Converter::ParseArray(System.String,System.String)
extern "C"  StringU5BU5D_t1642385972* Converter_ParseArray_m783528499 (Il2CppObject * __this /* static, unused */, String_t* ___arrayData0, String_t* ___splitter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
