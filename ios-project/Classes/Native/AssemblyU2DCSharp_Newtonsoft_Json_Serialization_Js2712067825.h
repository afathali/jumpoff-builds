﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Newtonsoft.Json.Serialization.IValueProvider
struct IValueProvider_t561708391;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t1964060750;
// System.Object
struct Il2CppObject;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2572051853;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Required2961887721.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Nullable_1_gen1881161680.h"
#include "mscorlib_System_Nullable_1_gen1720961778.h"
#include "mscorlib_System_Nullable_1_gen3575889505.h"
#include "mscorlib_System_Nullable_1_gen1983200966.h"
#include "mscorlib_System_Nullable_1_gen3889546705.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonProperty
struct  JsonProperty_t2712067825  : public Il2CppObject
{
public:
	// System.String Newtonsoft.Json.Serialization.JsonProperty::<PropertyName>k__BackingField
	String_t* ___U3CPropertyNameU3Ek__BackingField_0;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.JsonProperty::<Order>k__BackingField
	Nullable_1_t334943763  ___U3COrderU3Ek__BackingField_1;
	// System.String Newtonsoft.Json.Serialization.JsonProperty::<UnderlyingName>k__BackingField
	String_t* ___U3CUnderlyingNameU3Ek__BackingField_2;
	// Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.JsonProperty::<ValueProvider>k__BackingField
	Il2CppObject * ___U3CValueProviderU3Ek__BackingField_3;
	// System.Type Newtonsoft.Json.Serialization.JsonProperty::<PropertyType>k__BackingField
	Type_t * ___U3CPropertyTypeU3Ek__BackingField_4;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::<Converter>k__BackingField
	JsonConverter_t1964060750 * ___U3CConverterU3Ek__BackingField_5;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::<MemberConverter>k__BackingField
	JsonConverter_t1964060750 * ___U3CMemberConverterU3Ek__BackingField_6;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<Ignored>k__BackingField
	bool ___U3CIgnoredU3Ek__BackingField_7;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<Readable>k__BackingField
	bool ___U3CReadableU3Ek__BackingField_8;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<Writable>k__BackingField
	bool ___U3CWritableU3Ek__BackingField_9;
	// System.Object Newtonsoft.Json.Serialization.JsonProperty::<DefaultValue>k__BackingField
	Il2CppObject * ___U3CDefaultValueU3Ek__BackingField_10;
	// Newtonsoft.Json.Required Newtonsoft.Json.Serialization.JsonProperty::<Required>k__BackingField
	int32_t ___U3CRequiredU3Ek__BackingField_11;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::<IsReference>k__BackingField
	Nullable_1_t2088641033  ___U3CIsReferenceU3Ek__BackingField_12;
	// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.JsonProperty::<NullValueHandling>k__BackingField
	Nullable_1_t1881161680  ___U3CNullValueHandlingU3Ek__BackingField_13;
	// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.Serialization.JsonProperty::<DefaultValueHandling>k__BackingField
	Nullable_1_t1720961778  ___U3CDefaultValueHandlingU3Ek__BackingField_14;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::<ReferenceLoopHandling>k__BackingField
	Nullable_1_t3575889505  ___U3CReferenceLoopHandlingU3Ek__BackingField_15;
	// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.Serialization.JsonProperty::<ObjectCreationHandling>k__BackingField
	Nullable_1_t1983200966  ___U3CObjectCreationHandlingU3Ek__BackingField_16;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::<TypeNameHandling>k__BackingField
	Nullable_1_t3889546705  ___U3CTypeNameHandlingU3Ek__BackingField_17;
	// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::<ShouldSerialize>k__BackingField
	Predicate_1_t1132419410 * ___U3CShouldSerializeU3Ek__BackingField_18;
	// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::<GetIsSpecified>k__BackingField
	Predicate_1_t1132419410 * ___U3CGetIsSpecifiedU3Ek__BackingField_19;
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.JsonProperty::<SetIsSpecified>k__BackingField
	Action_2_t2572051853 * ___U3CSetIsSpecifiedU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_U3CPropertyNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CPropertyNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CPropertyNameU3Ek__BackingField_0() const { return ___U3CPropertyNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPropertyNameU3Ek__BackingField_0() { return &___U3CPropertyNameU3Ek__BackingField_0; }
	inline void set_U3CPropertyNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CPropertyNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPropertyNameU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3COrderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3COrderU3Ek__BackingField_1)); }
	inline Nullable_1_t334943763  get_U3COrderU3Ek__BackingField_1() const { return ___U3COrderU3Ek__BackingField_1; }
	inline Nullable_1_t334943763 * get_address_of_U3COrderU3Ek__BackingField_1() { return &___U3COrderU3Ek__BackingField_1; }
	inline void set_U3COrderU3Ek__BackingField_1(Nullable_1_t334943763  value)
	{
		___U3COrderU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CUnderlyingNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CUnderlyingNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CUnderlyingNameU3Ek__BackingField_2() const { return ___U3CUnderlyingNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CUnderlyingNameU3Ek__BackingField_2() { return &___U3CUnderlyingNameU3Ek__BackingField_2; }
	inline void set_U3CUnderlyingNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CUnderlyingNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUnderlyingNameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CValueProviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CValueProviderU3Ek__BackingField_3)); }
	inline Il2CppObject * get_U3CValueProviderU3Ek__BackingField_3() const { return ___U3CValueProviderU3Ek__BackingField_3; }
	inline Il2CppObject ** get_address_of_U3CValueProviderU3Ek__BackingField_3() { return &___U3CValueProviderU3Ek__BackingField_3; }
	inline void set_U3CValueProviderU3Ek__BackingField_3(Il2CppObject * value)
	{
		___U3CValueProviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CValueProviderU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CPropertyTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CPropertyTypeU3Ek__BackingField_4)); }
	inline Type_t * get_U3CPropertyTypeU3Ek__BackingField_4() const { return ___U3CPropertyTypeU3Ek__BackingField_4; }
	inline Type_t ** get_address_of_U3CPropertyTypeU3Ek__BackingField_4() { return &___U3CPropertyTypeU3Ek__BackingField_4; }
	inline void set_U3CPropertyTypeU3Ek__BackingField_4(Type_t * value)
	{
		___U3CPropertyTypeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPropertyTypeU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CConverterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CConverterU3Ek__BackingField_5)); }
	inline JsonConverter_t1964060750 * get_U3CConverterU3Ek__BackingField_5() const { return ___U3CConverterU3Ek__BackingField_5; }
	inline JsonConverter_t1964060750 ** get_address_of_U3CConverterU3Ek__BackingField_5() { return &___U3CConverterU3Ek__BackingField_5; }
	inline void set_U3CConverterU3Ek__BackingField_5(JsonConverter_t1964060750 * value)
	{
		___U3CConverterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CConverterU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CMemberConverterU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CMemberConverterU3Ek__BackingField_6)); }
	inline JsonConverter_t1964060750 * get_U3CMemberConverterU3Ek__BackingField_6() const { return ___U3CMemberConverterU3Ek__BackingField_6; }
	inline JsonConverter_t1964060750 ** get_address_of_U3CMemberConverterU3Ek__BackingField_6() { return &___U3CMemberConverterU3Ek__BackingField_6; }
	inline void set_U3CMemberConverterU3Ek__BackingField_6(JsonConverter_t1964060750 * value)
	{
		___U3CMemberConverterU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMemberConverterU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CIgnoredU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CIgnoredU3Ek__BackingField_7)); }
	inline bool get_U3CIgnoredU3Ek__BackingField_7() const { return ___U3CIgnoredU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIgnoredU3Ek__BackingField_7() { return &___U3CIgnoredU3Ek__BackingField_7; }
	inline void set_U3CIgnoredU3Ek__BackingField_7(bool value)
	{
		___U3CIgnoredU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CReadableU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CReadableU3Ek__BackingField_8)); }
	inline bool get_U3CReadableU3Ek__BackingField_8() const { return ___U3CReadableU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CReadableU3Ek__BackingField_8() { return &___U3CReadableU3Ek__BackingField_8; }
	inline void set_U3CReadableU3Ek__BackingField_8(bool value)
	{
		___U3CReadableU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CWritableU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CWritableU3Ek__BackingField_9)); }
	inline bool get_U3CWritableU3Ek__BackingField_9() const { return ___U3CWritableU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CWritableU3Ek__BackingField_9() { return &___U3CWritableU3Ek__BackingField_9; }
	inline void set_U3CWritableU3Ek__BackingField_9(bool value)
	{
		___U3CWritableU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultValueU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CDefaultValueU3Ek__BackingField_10)); }
	inline Il2CppObject * get_U3CDefaultValueU3Ek__BackingField_10() const { return ___U3CDefaultValueU3Ek__BackingField_10; }
	inline Il2CppObject ** get_address_of_U3CDefaultValueU3Ek__BackingField_10() { return &___U3CDefaultValueU3Ek__BackingField_10; }
	inline void set_U3CDefaultValueU3Ek__BackingField_10(Il2CppObject * value)
	{
		___U3CDefaultValueU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDefaultValueU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CRequiredU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CRequiredU3Ek__BackingField_11)); }
	inline int32_t get_U3CRequiredU3Ek__BackingField_11() const { return ___U3CRequiredU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CRequiredU3Ek__BackingField_11() { return &___U3CRequiredU3Ek__BackingField_11; }
	inline void set_U3CRequiredU3Ek__BackingField_11(int32_t value)
	{
		___U3CRequiredU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CIsReferenceU3Ek__BackingField_12)); }
	inline Nullable_1_t2088641033  get_U3CIsReferenceU3Ek__BackingField_12() const { return ___U3CIsReferenceU3Ek__BackingField_12; }
	inline Nullable_1_t2088641033 * get_address_of_U3CIsReferenceU3Ek__BackingField_12() { return &___U3CIsReferenceU3Ek__BackingField_12; }
	inline void set_U3CIsReferenceU3Ek__BackingField_12(Nullable_1_t2088641033  value)
	{
		___U3CIsReferenceU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CNullValueHandlingU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CNullValueHandlingU3Ek__BackingField_13)); }
	inline Nullable_1_t1881161680  get_U3CNullValueHandlingU3Ek__BackingField_13() const { return ___U3CNullValueHandlingU3Ek__BackingField_13; }
	inline Nullable_1_t1881161680 * get_address_of_U3CNullValueHandlingU3Ek__BackingField_13() { return &___U3CNullValueHandlingU3Ek__BackingField_13; }
	inline void set_U3CNullValueHandlingU3Ek__BackingField_13(Nullable_1_t1881161680  value)
	{
		___U3CNullValueHandlingU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CDefaultValueHandlingU3Ek__BackingField_14)); }
	inline Nullable_1_t1720961778  get_U3CDefaultValueHandlingU3Ek__BackingField_14() const { return ___U3CDefaultValueHandlingU3Ek__BackingField_14; }
	inline Nullable_1_t1720961778 * get_address_of_U3CDefaultValueHandlingU3Ek__BackingField_14() { return &___U3CDefaultValueHandlingU3Ek__BackingField_14; }
	inline void set_U3CDefaultValueHandlingU3Ek__BackingField_14(Nullable_1_t1720961778  value)
	{
		___U3CDefaultValueHandlingU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CReferenceLoopHandlingU3Ek__BackingField_15)); }
	inline Nullable_1_t3575889505  get_U3CReferenceLoopHandlingU3Ek__BackingField_15() const { return ___U3CReferenceLoopHandlingU3Ek__BackingField_15; }
	inline Nullable_1_t3575889505 * get_address_of_U3CReferenceLoopHandlingU3Ek__BackingField_15() { return &___U3CReferenceLoopHandlingU3Ek__BackingField_15; }
	inline void set_U3CReferenceLoopHandlingU3Ek__BackingField_15(Nullable_1_t3575889505  value)
	{
		___U3CReferenceLoopHandlingU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CObjectCreationHandlingU3Ek__BackingField_16)); }
	inline Nullable_1_t1983200966  get_U3CObjectCreationHandlingU3Ek__BackingField_16() const { return ___U3CObjectCreationHandlingU3Ek__BackingField_16; }
	inline Nullable_1_t1983200966 * get_address_of_U3CObjectCreationHandlingU3Ek__BackingField_16() { return &___U3CObjectCreationHandlingU3Ek__BackingField_16; }
	inline void set_U3CObjectCreationHandlingU3Ek__BackingField_16(Nullable_1_t1983200966  value)
	{
		___U3CObjectCreationHandlingU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CTypeNameHandlingU3Ek__BackingField_17)); }
	inline Nullable_1_t3889546705  get_U3CTypeNameHandlingU3Ek__BackingField_17() const { return ___U3CTypeNameHandlingU3Ek__BackingField_17; }
	inline Nullable_1_t3889546705 * get_address_of_U3CTypeNameHandlingU3Ek__BackingField_17() { return &___U3CTypeNameHandlingU3Ek__BackingField_17; }
	inline void set_U3CTypeNameHandlingU3Ek__BackingField_17(Nullable_1_t3889546705  value)
	{
		___U3CTypeNameHandlingU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CShouldSerializeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CShouldSerializeU3Ek__BackingField_18)); }
	inline Predicate_1_t1132419410 * get_U3CShouldSerializeU3Ek__BackingField_18() const { return ___U3CShouldSerializeU3Ek__BackingField_18; }
	inline Predicate_1_t1132419410 ** get_address_of_U3CShouldSerializeU3Ek__BackingField_18() { return &___U3CShouldSerializeU3Ek__BackingField_18; }
	inline void set_U3CShouldSerializeU3Ek__BackingField_18(Predicate_1_t1132419410 * value)
	{
		___U3CShouldSerializeU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CShouldSerializeU3Ek__BackingField_18, value);
	}

	inline static int32_t get_offset_of_U3CGetIsSpecifiedU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CGetIsSpecifiedU3Ek__BackingField_19)); }
	inline Predicate_1_t1132419410 * get_U3CGetIsSpecifiedU3Ek__BackingField_19() const { return ___U3CGetIsSpecifiedU3Ek__BackingField_19; }
	inline Predicate_1_t1132419410 ** get_address_of_U3CGetIsSpecifiedU3Ek__BackingField_19() { return &___U3CGetIsSpecifiedU3Ek__BackingField_19; }
	inline void set_U3CGetIsSpecifiedU3Ek__BackingField_19(Predicate_1_t1132419410 * value)
	{
		___U3CGetIsSpecifiedU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGetIsSpecifiedU3Ek__BackingField_19, value);
	}

	inline static int32_t get_offset_of_U3CSetIsSpecifiedU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonProperty_t2712067825, ___U3CSetIsSpecifiedU3Ek__BackingField_20)); }
	inline Action_2_t2572051853 * get_U3CSetIsSpecifiedU3Ek__BackingField_20() const { return ___U3CSetIsSpecifiedU3Ek__BackingField_20; }
	inline Action_2_t2572051853 ** get_address_of_U3CSetIsSpecifiedU3Ek__BackingField_20() { return &___U3CSetIsSpecifiedU3Ek__BackingField_20; }
	inline void set_U3CSetIsSpecifiedU3Ek__BackingField_20(Action_2_t2572051853 * value)
	{
		___U3CSetIsSpecifiedU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSetIsSpecifiedU3Ek__BackingField_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
