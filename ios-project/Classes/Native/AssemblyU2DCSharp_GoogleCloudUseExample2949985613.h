﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleCloudUseExample
struct  GoogleCloudUseExample_t2949985613  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] GoogleCloudUseExample::hideOnConnect
	GameObjectU5BU5D_t3057952154* ___hideOnConnect_2;
	// UnityEngine.GameObject[] GoogleCloudUseExample::showOnConnect
	GameObjectU5BU5D_t3057952154* ___showOnConnect_3;

public:
	inline static int32_t get_offset_of_hideOnConnect_2() { return static_cast<int32_t>(offsetof(GoogleCloudUseExample_t2949985613, ___hideOnConnect_2)); }
	inline GameObjectU5BU5D_t3057952154* get_hideOnConnect_2() const { return ___hideOnConnect_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_hideOnConnect_2() { return &___hideOnConnect_2; }
	inline void set_hideOnConnect_2(GameObjectU5BU5D_t3057952154* value)
	{
		___hideOnConnect_2 = value;
		Il2CppCodeGenWriteBarrier(&___hideOnConnect_2, value);
	}

	inline static int32_t get_offset_of_showOnConnect_3() { return static_cast<int32_t>(offsetof(GoogleCloudUseExample_t2949985613, ___showOnConnect_3)); }
	inline GameObjectU5BU5D_t3057952154* get_showOnConnect_3() const { return ___showOnConnect_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_showOnConnect_3() { return &___showOnConnect_3; }
	inline void set_showOnConnect_3(GameObjectU5BU5D_t3057952154* value)
	{
		___showOnConnect_3 = value;
		Il2CppCodeGenWriteBarrier(&___showOnConnect_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
