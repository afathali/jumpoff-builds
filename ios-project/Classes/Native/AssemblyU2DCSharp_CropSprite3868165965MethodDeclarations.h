﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CropSprite
struct CropSprite_t3868165965;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void CropSprite::.ctor()
extern "C"  void CropSprite__ctor_m1422363410 (CropSprite_t3868165965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CropSprite::Start()
extern "C"  void CropSprite_Start_m1704943642 (CropSprite_t3868165965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LineRenderer CropSprite::createAndGetLine(System.String)
extern "C"  LineRenderer_t849157671 * CropSprite_createAndGetLine_m2144995534 (CropSprite_t3868165965 * __this, String_t* ___lineName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CropSprite::Update()
extern "C"  void CropSprite_Update_m1885690193 (CropSprite_t3868165965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CropSprite::drawRectangle()
extern "C"  void CropSprite_drawRectangle_m36672761 (CropSprite_t3868165965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CropSprite::cropSprite()
extern "C"  void CropSprite_cropSprite_m4232470947 (CropSprite_t3868165965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CropSprite::isSpriteTouched(UnityEngine.GameObject)
extern "C"  bool CropSprite_isSpriteTouched_m2307427487 (CropSprite_t3868165965 * __this, GameObject_t1756533147 * ___sprite0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
