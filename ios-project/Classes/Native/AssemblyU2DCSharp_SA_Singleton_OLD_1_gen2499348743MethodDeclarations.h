﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3980293213MethodDeclarations.h"

// System.Void SA_Singleton_OLD`1<GoogleCloudManager>::.ctor()
#define SA_Singleton_OLD_1__ctor_m3675811457(__this, method) ((  void (*) (SA_Singleton_OLD_1_t2499348743 *, const MethodInfo*))SA_Singleton_OLD_1__ctor_m2643683696_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<GoogleCloudManager>::.cctor()
#define SA_Singleton_OLD_1__cctor_m2136100302(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1__cctor_m1355192463_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<GoogleCloudManager>::get_instance()
#define SA_Singleton_OLD_1_get_instance_m1254794008(__this /* static, unused */, method) ((  GoogleCloudManager_t1208504825 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_instance_m1963118739_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<GoogleCloudManager>::get_Instance()
#define SA_Singleton_OLD_1_get_Instance_m1290232376(__this /* static, unused */, method) ((  GoogleCloudManager_t1208504825 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_Instance_m2967800051_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<GoogleCloudManager>::get_HasInstance()
#define SA_Singleton_OLD_1_get_HasInstance_m2823422247(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_HasInstance_m4184706798_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<GoogleCloudManager>::get_IsDestroyed()
#define SA_Singleton_OLD_1_get_IsDestroyed_m828651189(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_IsDestroyed_m1648274602_gshared)(__this /* static, unused */, method)
// System.Void SA_Singleton_OLD`1<GoogleCloudManager>::OnDestroy()
#define SA_Singleton_OLD_1_OnDestroy_m3147807618(__this, method) ((  void (*) (SA_Singleton_OLD_1_t2499348743 *, const MethodInfo*))SA_Singleton_OLD_1_OnDestroy_m1680414419_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<GoogleCloudManager>::OnApplicationQuit()
#define SA_Singleton_OLD_1_OnApplicationQuit_m3438535579(__this, method) ((  void (*) (SA_Singleton_OLD_1_t2499348743 *, const MethodInfo*))SA_Singleton_OLD_1_OnApplicationQuit_m1864960902_gshared)(__this, method)
