﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlNodeConverter/<DeserializeNode>c__AnonStorey1C
struct U3CDeserializeNodeU3Ec__AnonStorey1C_t3970047118;
// Newtonsoft.Json.Converters.IXmlElement
struct IXmlElement_t2005722770;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Converters.XmlNodeConverter/<DeserializeNode>c__AnonStorey1C::.ctor()
extern "C"  void U3CDeserializeNodeU3Ec__AnonStorey1C__ctor_m1370299047 (U3CDeserializeNodeU3Ec__AnonStorey1C_t3970047118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter/<DeserializeNode>c__AnonStorey1C::<>m__C4(Newtonsoft.Json.Converters.IXmlElement)
extern "C"  bool U3CDeserializeNodeU3Ec__AnonStorey1C_U3CU3Em__C4_m1758652923 (U3CDeserializeNodeU3Ec__AnonStorey1C_t3970047118 * __this, Il2CppObject * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
