﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidSocialNativeExample
struct  AndroidSocialNativeExample_t1704557771  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture2D AndroidSocialNativeExample::shareTexture
	Texture2D_t3542995729 * ___shareTexture_2;

public:
	inline static int32_t get_offset_of_shareTexture_2() { return static_cast<int32_t>(offsetof(AndroidSocialNativeExample_t1704557771, ___shareTexture_2)); }
	inline Texture2D_t3542995729 * get_shareTexture_2() const { return ___shareTexture_2; }
	inline Texture2D_t3542995729 ** get_address_of_shareTexture_2() { return &___shareTexture_2; }
	inline void set_shareTexture_2(Texture2D_t3542995729 * value)
	{
		___shareTexture_2 = value;
		Il2CppCodeGenWriteBarrier(&___shareTexture_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
