﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageUploaderPageManager
struct  ImageUploaderPageManager_t1025178313  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button ImageUploaderPageManager::btn_crop
	Button_t2872111280 * ___btn_crop_2;
	// UnityEngine.UI.Button ImageUploaderPageManager::btn_details
	Button_t2872111280 * ___btn_details_3;
	// UnityEngine.UI.Button ImageUploaderPageManager::btn_back
	Button_t2872111280 * ___btn_back_4;
	// UnityEngine.GameObject ImageUploaderPageManager::btn_info
	GameObject_t1756533147 * ___btn_info_5;
	// UnityEngine.GameObject ImageUploaderPageManager::btn_skip
	GameObject_t1756533147 * ___btn_skip_6;
	// UnityEngine.GameObject ImageUploaderPageManager::btn_rotate_container
	GameObject_t1756533147 * ___btn_rotate_container_7;
	// UnityEngine.GameObject ImageUploaderPageManager::cropSelected
	GameObject_t1756533147 * ___cropSelected_8;
	// UnityEngine.GameObject ImageUploaderPageManager::cropUnselected
	GameObject_t1756533147 * ___cropUnselected_9;
	// UnityEngine.GameObject ImageUploaderPageManager::detailsSelected
	GameObject_t1756533147 * ___detailsSelected_10;
	// UnityEngine.GameObject ImageUploaderPageManager::detailsUnselected
	GameObject_t1756533147 * ___detailsUnselected_11;
	// UnityEngine.GameObject ImageUploaderPageManager::selected
	GameObject_t1756533147 * ___selected_12;
	// UnityEngine.GameObject ImageUploaderPageManager::cropArea
	GameObject_t1756533147 * ___cropArea_13;
	// UnityEngine.GameObject ImageUploaderPageManager::detailsArea
	GameObject_t1756533147 * ___detailsArea_14;
	// UnityEngine.GameObject ImageUploaderPageManager::details_glow
	GameObject_t1756533147 * ___details_glow_15;
	// UnityEngine.UI.Text ImageUploaderPageManager::title
	Text_t356221433 * ___title_16;
	// System.Boolean ImageUploaderPageManager::detailsSelectedOnce
	bool ___detailsSelectedOnce_17;
	// System.Single ImageUploaderPageManager::lastActivityTime
	float ___lastActivityTime_18;

public:
	inline static int32_t get_offset_of_btn_crop_2() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___btn_crop_2)); }
	inline Button_t2872111280 * get_btn_crop_2() const { return ___btn_crop_2; }
	inline Button_t2872111280 ** get_address_of_btn_crop_2() { return &___btn_crop_2; }
	inline void set_btn_crop_2(Button_t2872111280 * value)
	{
		___btn_crop_2 = value;
		Il2CppCodeGenWriteBarrier(&___btn_crop_2, value);
	}

	inline static int32_t get_offset_of_btn_details_3() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___btn_details_3)); }
	inline Button_t2872111280 * get_btn_details_3() const { return ___btn_details_3; }
	inline Button_t2872111280 ** get_address_of_btn_details_3() { return &___btn_details_3; }
	inline void set_btn_details_3(Button_t2872111280 * value)
	{
		___btn_details_3 = value;
		Il2CppCodeGenWriteBarrier(&___btn_details_3, value);
	}

	inline static int32_t get_offset_of_btn_back_4() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___btn_back_4)); }
	inline Button_t2872111280 * get_btn_back_4() const { return ___btn_back_4; }
	inline Button_t2872111280 ** get_address_of_btn_back_4() { return &___btn_back_4; }
	inline void set_btn_back_4(Button_t2872111280 * value)
	{
		___btn_back_4 = value;
		Il2CppCodeGenWriteBarrier(&___btn_back_4, value);
	}

	inline static int32_t get_offset_of_btn_info_5() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___btn_info_5)); }
	inline GameObject_t1756533147 * get_btn_info_5() const { return ___btn_info_5; }
	inline GameObject_t1756533147 ** get_address_of_btn_info_5() { return &___btn_info_5; }
	inline void set_btn_info_5(GameObject_t1756533147 * value)
	{
		___btn_info_5 = value;
		Il2CppCodeGenWriteBarrier(&___btn_info_5, value);
	}

	inline static int32_t get_offset_of_btn_skip_6() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___btn_skip_6)); }
	inline GameObject_t1756533147 * get_btn_skip_6() const { return ___btn_skip_6; }
	inline GameObject_t1756533147 ** get_address_of_btn_skip_6() { return &___btn_skip_6; }
	inline void set_btn_skip_6(GameObject_t1756533147 * value)
	{
		___btn_skip_6 = value;
		Il2CppCodeGenWriteBarrier(&___btn_skip_6, value);
	}

	inline static int32_t get_offset_of_btn_rotate_container_7() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___btn_rotate_container_7)); }
	inline GameObject_t1756533147 * get_btn_rotate_container_7() const { return ___btn_rotate_container_7; }
	inline GameObject_t1756533147 ** get_address_of_btn_rotate_container_7() { return &___btn_rotate_container_7; }
	inline void set_btn_rotate_container_7(GameObject_t1756533147 * value)
	{
		___btn_rotate_container_7 = value;
		Il2CppCodeGenWriteBarrier(&___btn_rotate_container_7, value);
	}

	inline static int32_t get_offset_of_cropSelected_8() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___cropSelected_8)); }
	inline GameObject_t1756533147 * get_cropSelected_8() const { return ___cropSelected_8; }
	inline GameObject_t1756533147 ** get_address_of_cropSelected_8() { return &___cropSelected_8; }
	inline void set_cropSelected_8(GameObject_t1756533147 * value)
	{
		___cropSelected_8 = value;
		Il2CppCodeGenWriteBarrier(&___cropSelected_8, value);
	}

	inline static int32_t get_offset_of_cropUnselected_9() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___cropUnselected_9)); }
	inline GameObject_t1756533147 * get_cropUnselected_9() const { return ___cropUnselected_9; }
	inline GameObject_t1756533147 ** get_address_of_cropUnselected_9() { return &___cropUnselected_9; }
	inline void set_cropUnselected_9(GameObject_t1756533147 * value)
	{
		___cropUnselected_9 = value;
		Il2CppCodeGenWriteBarrier(&___cropUnselected_9, value);
	}

	inline static int32_t get_offset_of_detailsSelected_10() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___detailsSelected_10)); }
	inline GameObject_t1756533147 * get_detailsSelected_10() const { return ___detailsSelected_10; }
	inline GameObject_t1756533147 ** get_address_of_detailsSelected_10() { return &___detailsSelected_10; }
	inline void set_detailsSelected_10(GameObject_t1756533147 * value)
	{
		___detailsSelected_10 = value;
		Il2CppCodeGenWriteBarrier(&___detailsSelected_10, value);
	}

	inline static int32_t get_offset_of_detailsUnselected_11() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___detailsUnselected_11)); }
	inline GameObject_t1756533147 * get_detailsUnselected_11() const { return ___detailsUnselected_11; }
	inline GameObject_t1756533147 ** get_address_of_detailsUnselected_11() { return &___detailsUnselected_11; }
	inline void set_detailsUnselected_11(GameObject_t1756533147 * value)
	{
		___detailsUnselected_11 = value;
		Il2CppCodeGenWriteBarrier(&___detailsUnselected_11, value);
	}

	inline static int32_t get_offset_of_selected_12() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___selected_12)); }
	inline GameObject_t1756533147 * get_selected_12() const { return ___selected_12; }
	inline GameObject_t1756533147 ** get_address_of_selected_12() { return &___selected_12; }
	inline void set_selected_12(GameObject_t1756533147 * value)
	{
		___selected_12 = value;
		Il2CppCodeGenWriteBarrier(&___selected_12, value);
	}

	inline static int32_t get_offset_of_cropArea_13() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___cropArea_13)); }
	inline GameObject_t1756533147 * get_cropArea_13() const { return ___cropArea_13; }
	inline GameObject_t1756533147 ** get_address_of_cropArea_13() { return &___cropArea_13; }
	inline void set_cropArea_13(GameObject_t1756533147 * value)
	{
		___cropArea_13 = value;
		Il2CppCodeGenWriteBarrier(&___cropArea_13, value);
	}

	inline static int32_t get_offset_of_detailsArea_14() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___detailsArea_14)); }
	inline GameObject_t1756533147 * get_detailsArea_14() const { return ___detailsArea_14; }
	inline GameObject_t1756533147 ** get_address_of_detailsArea_14() { return &___detailsArea_14; }
	inline void set_detailsArea_14(GameObject_t1756533147 * value)
	{
		___detailsArea_14 = value;
		Il2CppCodeGenWriteBarrier(&___detailsArea_14, value);
	}

	inline static int32_t get_offset_of_details_glow_15() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___details_glow_15)); }
	inline GameObject_t1756533147 * get_details_glow_15() const { return ___details_glow_15; }
	inline GameObject_t1756533147 ** get_address_of_details_glow_15() { return &___details_glow_15; }
	inline void set_details_glow_15(GameObject_t1756533147 * value)
	{
		___details_glow_15 = value;
		Il2CppCodeGenWriteBarrier(&___details_glow_15, value);
	}

	inline static int32_t get_offset_of_title_16() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___title_16)); }
	inline Text_t356221433 * get_title_16() const { return ___title_16; }
	inline Text_t356221433 ** get_address_of_title_16() { return &___title_16; }
	inline void set_title_16(Text_t356221433 * value)
	{
		___title_16 = value;
		Il2CppCodeGenWriteBarrier(&___title_16, value);
	}

	inline static int32_t get_offset_of_detailsSelectedOnce_17() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___detailsSelectedOnce_17)); }
	inline bool get_detailsSelectedOnce_17() const { return ___detailsSelectedOnce_17; }
	inline bool* get_address_of_detailsSelectedOnce_17() { return &___detailsSelectedOnce_17; }
	inline void set_detailsSelectedOnce_17(bool value)
	{
		___detailsSelectedOnce_17 = value;
	}

	inline static int32_t get_offset_of_lastActivityTime_18() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313, ___lastActivityTime_18)); }
	inline float get_lastActivityTime_18() const { return ___lastActivityTime_18; }
	inline float* get_address_of_lastActivityTime_18() { return &___lastActivityTime_18; }
	inline void set_lastActivityTime_18(float value)
	{
		___lastActivityTime_18 = value;
	}
};

struct ImageUploaderPageManager_t1025178313_StaticFields
{
public:
	// UnityEngine.Events.UnityAction ImageUploaderPageManager::<>f__am$cache11
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache11_19;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_19() { return static_cast<int32_t>(offsetof(ImageUploaderPageManager_t1025178313_StaticFields, ___U3CU3Ef__amU24cache11_19)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache11_19() const { return ___U3CU3Ef__amU24cache11_19; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache11_19() { return &___U3CU3Ef__amU24cache11_19; }
	inline void set_U3CU3Ef__amU24cache11_19(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache11_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
