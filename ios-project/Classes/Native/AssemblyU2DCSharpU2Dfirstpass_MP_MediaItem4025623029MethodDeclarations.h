﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MP_MediaItem
struct MP_MediaItem_t4025623029;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MP_MediaItem::.ctor(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void MP_MediaItem__ctor_m3960503828 (MP_MediaItem_t4025623029 * __this, String_t* ___id0, String_t* ___title1, String_t* ___artist2, String_t* ___albumTitle3, String_t* ___albumArtist4, String_t* ___genre5, String_t* ___playbackDuration6, String_t* ___composer7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MP_MediaItem::get_Id()
extern "C"  String_t* MP_MediaItem_get_Id_m1258628623 (MP_MediaItem_t4025623029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MP_MediaItem::get_Title()
extern "C"  String_t* MP_MediaItem_get_Title_m2208073964 (MP_MediaItem_t4025623029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MP_MediaItem::get_Artist()
extern "C"  String_t* MP_MediaItem_get_Artist_m1675828759 (MP_MediaItem_t4025623029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MP_MediaItem::get_AlbumTitle()
extern "C"  String_t* MP_MediaItem_get_AlbumTitle_m476772025 (MP_MediaItem_t4025623029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MP_MediaItem::get_AlbumArtist()
extern "C"  String_t* MP_MediaItem_get_AlbumArtist_m4106911622 (MP_MediaItem_t4025623029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MP_MediaItem::get_PlaybackDuration()
extern "C"  String_t* MP_MediaItem_get_PlaybackDuration_m1346863771 (MP_MediaItem_t4025623029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MP_MediaItem::get_Genre()
extern "C"  String_t* MP_MediaItem_get_Genre_m1294725561 (MP_MediaItem_t4025623029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MP_MediaItem::get_Composer()
extern "C"  String_t* MP_MediaItem_get_Composer_m3740545674 (MP_MediaItem_t4025623029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
