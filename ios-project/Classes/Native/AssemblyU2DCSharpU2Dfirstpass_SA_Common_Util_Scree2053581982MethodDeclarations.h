﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.Common.Util.Screen::TakeScreenshot(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void Screen_TakeScreenshot_m914170047 (Il2CppObject * __this /* static, unused */, Action_1_t3344795111 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
