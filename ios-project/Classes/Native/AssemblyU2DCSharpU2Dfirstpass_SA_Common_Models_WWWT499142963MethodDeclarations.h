﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Models.WWWTextureLoader
struct WWWTextureLoader_t499142963;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void SA.Common.Models.WWWTextureLoader::.ctor()
extern "C"  void WWWTextureLoader__ctor_m2946596821 (WWWTextureLoader_t499142963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.WWWTextureLoader::.cctor()
extern "C"  void WWWTextureLoader__cctor_m2134451092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.WWWTextureLoader::add_OnLoad(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void WWWTextureLoader_add_OnLoad_m3430700354 (WWWTextureLoader_t499142963 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.WWWTextureLoader::remove_OnLoad(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void WWWTextureLoader_remove_OnLoad_m1071821479 (WWWTextureLoader_t499142963 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SA.Common.Models.WWWTextureLoader SA.Common.Models.WWWTextureLoader::Create()
extern "C"  WWWTextureLoader_t499142963 * WWWTextureLoader_Create_m2538406430 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.WWWTextureLoader::Awake()
extern "C"  void WWWTextureLoader_Awake_m3904252358 (WWWTextureLoader_t499142963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.WWWTextureLoader::LoadTexture(System.String)
extern "C"  void WWWTextureLoader_LoadTexture_m1770160252 (WWWTextureLoader_t499142963 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SA.Common.Models.WWWTextureLoader::LoadCoroutin()
extern "C"  Il2CppObject * WWWTextureLoader_LoadCoroutin_m2261796762 (WWWTextureLoader_t499142963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.WWWTextureLoader::UpdateLocalCache(System.String,UnityEngine.Texture2D)
extern "C"  void WWWTextureLoader_UpdateLocalCache_m84164665 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Texture2D_t3542995729 * ___image1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.WWWTextureLoader::<OnLoad>m__7F(UnityEngine.Texture2D)
extern "C"  void WWWTextureLoader_U3COnLoadU3Em__7F_m4093233772 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
