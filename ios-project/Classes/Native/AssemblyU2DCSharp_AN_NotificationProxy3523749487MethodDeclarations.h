﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_NotificationProxy
struct AN_NotificationProxy_t3523749487;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// AndroidNotificationBuilder
struct AndroidNotificationBuilder_t2822362133;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AndroidNotificationBuilder2822362133.h"

// System.Void AN_NotificationProxy::.ctor()
extern "C"  void AN_NotificationProxy__ctor_m2242124348 (AN_NotificationProxy_t3523749487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_NotificationProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_NotificationProxy_CallActivityFunction_m2536875125 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_NotificationProxy::ShowToastNotification(System.String,System.Int32)
extern "C"  void AN_NotificationProxy_ShowToastNotification_m4007703882 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_NotificationProxy::HideAllNotifications()
extern "C"  void AN_NotificationProxy_HideAllNotifications_m143432157 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_NotificationProxy::requestCurrentAppLaunchNotificationId()
extern "C"  void AN_NotificationProxy_requestCurrentAppLaunchNotificationId_m1325458348 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_NotificationProxy::ScheduleLocalNotification(AndroidNotificationBuilder)
extern "C"  void AN_NotificationProxy_ScheduleLocalNotification_m2213352078 (Il2CppObject * __this /* static, unused */, AndroidNotificationBuilder_t2822362133 * ___builder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_NotificationProxy::CanselLocalNotification(System.Int32)
extern "C"  void AN_NotificationProxy_CanselLocalNotification_m3232107097 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_NotificationProxy::InitPushNotifications(System.String,System.String,System.String,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void AN_NotificationProxy_InitPushNotifications_m638354569 (Il2CppObject * __this /* static, unused */, String_t* ___smallIcon0, String_t* ___largeIcon1, String_t* ___sound2, bool ___vibration3, bool ___showWhenAppForeground4, bool ___replaceOldNotificationWithNew5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_NotificationProxy::GCMRgisterDevice(System.String)
extern "C"  void AN_NotificationProxy_GCMRgisterDevice_m3343305921 (Il2CppObject * __this /* static, unused */, String_t* ___senderId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_NotificationProxy::GCMLoadLastMessage()
extern "C"  void AN_NotificationProxy_GCMLoadLastMessage_m2893237758 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_NotificationProxy::GCMRemoveLastMessageInfo()
extern "C"  void AN_NotificationProxy_GCMRemoveLastMessageInfo_m254584282 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
