﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SavedGamesExample
struct SavedGamesExample_t916988576;
// GooglePlayResult
struct GooglePlayResult_t3097469636;
// GP_SpanshotLoadResult
struct GP_SpanshotLoadResult_t2263304449;
// GP_SnapshotConflict
struct GP_SnapshotConflict_t4002586770;
// GooglePlayConnectionResult
struct GooglePlayConnectionResult_t2758718724;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayResult3097469636.h"
#include "AssemblyU2DCSharp_AndroidDialogResult1664046954.h"
#include "AssemblyU2DCSharp_GP_SpanshotLoadResult2263304449.h"
#include "AssemblyU2DCSharp_GP_SnapshotConflict4002586770.h"
#include "AssemblyU2DCSharp_GooglePlayConnectionResult2758718724.h"

// System.Void SavedGamesExample::.ctor()
extern "C"  void SavedGamesExample__ctor_m1296058839 (SavedGamesExample_t916988576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::Start()
extern "C"  void SavedGamesExample_Start_m594361659 (SavedGamesExample_t916988576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::OnDestroy()
extern "C"  void SavedGamesExample_OnDestroy_m2852461594 (SavedGamesExample_t916988576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::ConncetButtonPress()
extern "C"  void SavedGamesExample_ConncetButtonPress_m2094967970 (SavedGamesExample_t916988576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::FixedUpdate()
extern "C"  void SavedGamesExample_FixedUpdate_m989392592 (SavedGamesExample_t916988576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::CreateNewSnapshot()
extern "C"  void SavedGamesExample_CreateNewSnapshot_m2613467627 (SavedGamesExample_t916988576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::ShowSavedGamesUI()
extern "C"  void SavedGamesExample_ShowSavedGamesUI_m2502058442 (SavedGamesExample_t916988576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::LoadSavedGames()
extern "C"  void SavedGamesExample_LoadSavedGames_m1981322657 (SavedGamesExample_t916988576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::ActionAvailableGameSavesLoaded(GooglePlayResult)
extern "C"  void SavedGamesExample_ActionAvailableGameSavesLoaded_m910984971 (SavedGamesExample_t916988576 * __this, GooglePlayResult_t3097469636 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::OnSpanshotLoadDialogComplete(AndroidDialogResult)
extern "C"  void SavedGamesExample_OnSpanshotLoadDialogComplete_m2690568083 (SavedGamesExample_t916988576 * __this, int32_t ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::ActionNewGameSaveRequest()
extern "C"  void SavedGamesExample_ActionNewGameSaveRequest_m2583634435 (SavedGamesExample_t916988576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::ActionGameSaveLoaded(GP_SpanshotLoadResult)
extern "C"  void SavedGamesExample_ActionGameSaveLoaded_m470668428 (SavedGamesExample_t916988576 * __this, GP_SpanshotLoadResult_t2263304449 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::ActionGameSaveResult(GP_SpanshotLoadResult)
extern "C"  void SavedGamesExample_ActionGameSaveResult_m670484252 (SavedGamesExample_t916988576 * __this, GP_SpanshotLoadResult_t2263304449 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::ActionConflict(GP_SnapshotConflict)
extern "C"  void SavedGamesExample_ActionConflict_m3316644457 (SavedGamesExample_t916988576 * __this, GP_SnapshotConflict_t4002586770 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::OnPlayerDisconnected()
extern "C"  void SavedGamesExample_OnPlayerDisconnected_m3058532980 (SavedGamesExample_t916988576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::OnPlayerConnected()
extern "C"  void SavedGamesExample_OnPlayerConnected_m4098009148 (SavedGamesExample_t916988576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SavedGamesExample::OnConnectionResult(GooglePlayConnectionResult)
extern "C"  void SavedGamesExample_OnConnectionResult_m240128851 (SavedGamesExample_t916988576 * __this, GooglePlayConnectionResult_t2758718724 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SavedGamesExample::MakeScreenshotAndSaveGameData()
extern "C"  Il2CppObject * SavedGamesExample_MakeScreenshotAndSaveGameData_m55723735 (SavedGamesExample_t916988576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
