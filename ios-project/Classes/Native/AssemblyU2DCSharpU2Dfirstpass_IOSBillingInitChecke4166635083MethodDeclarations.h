﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSBillingInitChecker
struct IOSBillingInitChecker_t4166635083;
// IOSBillingInitChecker/BillingInitListener
struct BillingInitListener_t4162012659;
// SA.Common.Models.Result
struct Result_t4287219743;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSBillingInitChecke4162012659.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"

// System.Void IOSBillingInitChecker::.ctor(IOSBillingInitChecker/BillingInitListener)
extern "C"  void IOSBillingInitChecker__ctor_m3288865537 (IOSBillingInitChecker_t4166635083 * __this, BillingInitListener_t4162012659 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSBillingInitChecker::HandleOnStoreKitInitComplete(SA.Common.Models.Result)
extern "C"  void IOSBillingInitChecker_HandleOnStoreKitInitComplete_m790593649 (IOSBillingInitChecker_t4166635083 * __this, Result_t4287219743 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
