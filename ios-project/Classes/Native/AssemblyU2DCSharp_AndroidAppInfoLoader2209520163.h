﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PackageAppInfo
struct PackageAppInfo_t260912591;
// System.Action`1<PackageAppInfo>
struct Action_1_t62711973;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3500364081.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidAppInfoLoader
struct  AndroidAppInfoLoader_t2209520163  : public SA_Singleton_OLD_1_t3500364081
{
public:
	// PackageAppInfo AndroidAppInfoLoader::PacakgeInfo
	PackageAppInfo_t260912591 * ___PacakgeInfo_4;

public:
	inline static int32_t get_offset_of_PacakgeInfo_4() { return static_cast<int32_t>(offsetof(AndroidAppInfoLoader_t2209520163, ___PacakgeInfo_4)); }
	inline PackageAppInfo_t260912591 * get_PacakgeInfo_4() const { return ___PacakgeInfo_4; }
	inline PackageAppInfo_t260912591 ** get_address_of_PacakgeInfo_4() { return &___PacakgeInfo_4; }
	inline void set_PacakgeInfo_4(PackageAppInfo_t260912591 * value)
	{
		___PacakgeInfo_4 = value;
		Il2CppCodeGenWriteBarrier(&___PacakgeInfo_4, value);
	}
};

struct AndroidAppInfoLoader_t2209520163_StaticFields
{
public:
	// System.Action`1<PackageAppInfo> AndroidAppInfoLoader::ActionPacakgeInfoLoaded
	Action_1_t62711973 * ___ActionPacakgeInfoLoaded_5;
	// System.Action`1<PackageAppInfo> AndroidAppInfoLoader::<>f__am$cache2
	Action_1_t62711973 * ___U3CU3Ef__amU24cache2_6;

public:
	inline static int32_t get_offset_of_ActionPacakgeInfoLoaded_5() { return static_cast<int32_t>(offsetof(AndroidAppInfoLoader_t2209520163_StaticFields, ___ActionPacakgeInfoLoaded_5)); }
	inline Action_1_t62711973 * get_ActionPacakgeInfoLoaded_5() const { return ___ActionPacakgeInfoLoaded_5; }
	inline Action_1_t62711973 ** get_address_of_ActionPacakgeInfoLoaded_5() { return &___ActionPacakgeInfoLoaded_5; }
	inline void set_ActionPacakgeInfoLoaded_5(Action_1_t62711973 * value)
	{
		___ActionPacakgeInfoLoaded_5 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPacakgeInfoLoaded_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(AndroidAppInfoLoader_t2209520163_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline Action_1_t62711973 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline Action_1_t62711973 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(Action_1_t62711973 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
