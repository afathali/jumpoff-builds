﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_TBM_LoadMatchResult
struct GK_TBM_LoadMatchResult_t1639249273;
// GK_TBM_Match
struct GK_TBM_Match_t132033130;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_Match132033130.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GK_TBM_LoadMatchResult::.ctor(GK_TBM_Match)
extern "C"  void GK_TBM_LoadMatchResult__ctor_m2586090958 (GK_TBM_LoadMatchResult_t1639249273 * __this, GK_TBM_Match_t132033130 * ___match0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_TBM_LoadMatchResult::.ctor(System.String)
extern "C"  void GK_TBM_LoadMatchResult__ctor_m3525342890 (GK_TBM_LoadMatchResult_t1639249273 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_TBM_Match GK_TBM_LoadMatchResult::get_Match()
extern "C"  GK_TBM_Match_t132033130 * GK_TBM_LoadMatchResult_get_Match_m2715652411 (GK_TBM_LoadMatchResult_t1639249273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
