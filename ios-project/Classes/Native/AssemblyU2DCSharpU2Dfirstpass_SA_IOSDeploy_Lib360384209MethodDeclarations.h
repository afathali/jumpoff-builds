﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.IOSDeploy.Lib
struct Lib_t360384209;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.IOSDeploy.Lib::.ctor()
extern "C"  void Lib__ctor_m1131801570 (Lib_t360384209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
