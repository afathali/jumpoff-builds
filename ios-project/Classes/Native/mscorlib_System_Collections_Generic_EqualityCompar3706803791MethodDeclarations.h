﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<AN_PermissionState>
struct EqualityComparer_1_t3706803791;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<AN_PermissionState>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m847247857_gshared (EqualityComparer_1_t3706803791 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m847247857(__this, method) ((  void (*) (EqualityComparer_1_t3706803791 *, const MethodInfo*))EqualityComparer_1__ctor_m847247857_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<AN_PermissionState>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1334520030_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m1334520030(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m1334520030_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<AN_PermissionState>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3270568736_gshared (EqualityComparer_1_t3706803791 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3270568736(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t3706803791 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3270568736_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<AN_PermissionState>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2950208538_gshared (EqualityComparer_1_t3706803791 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2950208538(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t3706803791 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2950208538_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<AN_PermissionState>::get_Default()
extern "C"  EqualityComparer_1_t3706803791 * EqualityComparer_1_get_Default_m2147393709_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m2147393709(__this /* static, unused */, method) ((  EqualityComparer_1_t3706803791 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m2147393709_gshared)(__this /* static, unused */, method)
