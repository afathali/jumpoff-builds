﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonPrimitiveContract
struct JsonPrimitiveContract_t2604782005;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void Newtonsoft.Json.Serialization.JsonPrimitiveContract::.ctor(System.Type)
extern "C"  void JsonPrimitiveContract__ctor_m4168730652 (JsonPrimitiveContract_t2604782005 * __this, Type_t * ___underlyingType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
