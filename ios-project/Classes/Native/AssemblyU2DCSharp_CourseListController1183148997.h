﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "AssemblyU2DCSharp_MCGBehaviour3307770884.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CourseListController
struct  CourseListController_t1183148997  : public MCGBehaviour_t3307770884
{
public:
	// UnityEngine.UI.Button CourseListController::btn_close
	Button_t2872111280 * ___btn_close_4;
	// UnityEngine.UI.Button CourseListController::btn_addNew
	Button_t2872111280 * ___btn_addNew_5;
	// UnityEngine.UI.Button CourseListController::btn_backToList
	Button_t2872111280 * ___btn_backToList_6;
	// UnityEngine.UI.Button CourseListController::btn_backToImageSelect
	Button_t2872111280 * ___btn_backToImageSelect_7;
	// UnityEngine.UI.Button CourseListController::btn_continue
	Button_t2872111280 * ___btn_continue_8;
	// UnityEngine.UI.Button CourseListController::btn_finish
	Button_t2872111280 * ___btn_finish_9;
	// UnityEngine.UI.Button CourseListController::btn_skip
	Button_t2872111280 * ___btn_skip_10;
	// UnityEngine.GameObject CourseListController::ListContainer
	GameObject_t1756533147 * ___ListContainer_11;
	// UnityEngine.GameObject CourseListController::DetailsPage
	GameObject_t1756533147 * ___DetailsPage_12;
	// UnityEngine.GameObject CourseListController::Course
	GameObject_t1756533147 * ___Course_13;
	// UnityEngine.Camera CourseListController::MainCamera
	Camera_t189460977 * ___MainCamera_14;
	// UnityEngine.UI.InputField CourseListController::input_name
	InputField_t1631627530 * ___input_name_15;
	// UnityEngine.UI.InputField CourseListController::input_show
	InputField_t1631627530 * ___input_show_16;
	// UnityEngine.UI.InputField CourseListController::input_notes
	InputField_t1631627530 * ___input_notes_17;
	// UnityEngine.GameObject CourseListController::DeleteMessage
	GameObject_t1756533147 * ___DeleteMessage_18;
	// UnityEngine.UI.Button CourseListController::btn_cancelDelete
	Button_t2872111280 * ___btn_cancelDelete_19;
	// UnityEngine.UI.Button CourseListController::btn_confirmDelete
	Button_t2872111280 * ___btn_confirmDelete_20;
	// System.Int64 CourseListController::DeleteCourseId
	int64_t ___DeleteCourseId_21;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CourseListController::CourseObjects
	List_1_t1125654279 * ___CourseObjects_22;
	// UnityEngine.UI.Slider CourseListController::slider_courseImageScale
	Slider_t297367283 * ___slider_courseImageScale_23;
	// UnityEngine.GameObject CourseListController::waiting
	GameObject_t1756533147 * ___waiting_24;
	// UnityEngine.GameObject CourseListController::fail
	GameObject_t1756533147 * ___fail_25;

public:
	inline static int32_t get_offset_of_btn_close_4() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___btn_close_4)); }
	inline Button_t2872111280 * get_btn_close_4() const { return ___btn_close_4; }
	inline Button_t2872111280 ** get_address_of_btn_close_4() { return &___btn_close_4; }
	inline void set_btn_close_4(Button_t2872111280 * value)
	{
		___btn_close_4 = value;
		Il2CppCodeGenWriteBarrier(&___btn_close_4, value);
	}

	inline static int32_t get_offset_of_btn_addNew_5() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___btn_addNew_5)); }
	inline Button_t2872111280 * get_btn_addNew_5() const { return ___btn_addNew_5; }
	inline Button_t2872111280 ** get_address_of_btn_addNew_5() { return &___btn_addNew_5; }
	inline void set_btn_addNew_5(Button_t2872111280 * value)
	{
		___btn_addNew_5 = value;
		Il2CppCodeGenWriteBarrier(&___btn_addNew_5, value);
	}

	inline static int32_t get_offset_of_btn_backToList_6() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___btn_backToList_6)); }
	inline Button_t2872111280 * get_btn_backToList_6() const { return ___btn_backToList_6; }
	inline Button_t2872111280 ** get_address_of_btn_backToList_6() { return &___btn_backToList_6; }
	inline void set_btn_backToList_6(Button_t2872111280 * value)
	{
		___btn_backToList_6 = value;
		Il2CppCodeGenWriteBarrier(&___btn_backToList_6, value);
	}

	inline static int32_t get_offset_of_btn_backToImageSelect_7() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___btn_backToImageSelect_7)); }
	inline Button_t2872111280 * get_btn_backToImageSelect_7() const { return ___btn_backToImageSelect_7; }
	inline Button_t2872111280 ** get_address_of_btn_backToImageSelect_7() { return &___btn_backToImageSelect_7; }
	inline void set_btn_backToImageSelect_7(Button_t2872111280 * value)
	{
		___btn_backToImageSelect_7 = value;
		Il2CppCodeGenWriteBarrier(&___btn_backToImageSelect_7, value);
	}

	inline static int32_t get_offset_of_btn_continue_8() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___btn_continue_8)); }
	inline Button_t2872111280 * get_btn_continue_8() const { return ___btn_continue_8; }
	inline Button_t2872111280 ** get_address_of_btn_continue_8() { return &___btn_continue_8; }
	inline void set_btn_continue_8(Button_t2872111280 * value)
	{
		___btn_continue_8 = value;
		Il2CppCodeGenWriteBarrier(&___btn_continue_8, value);
	}

	inline static int32_t get_offset_of_btn_finish_9() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___btn_finish_9)); }
	inline Button_t2872111280 * get_btn_finish_9() const { return ___btn_finish_9; }
	inline Button_t2872111280 ** get_address_of_btn_finish_9() { return &___btn_finish_9; }
	inline void set_btn_finish_9(Button_t2872111280 * value)
	{
		___btn_finish_9 = value;
		Il2CppCodeGenWriteBarrier(&___btn_finish_9, value);
	}

	inline static int32_t get_offset_of_btn_skip_10() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___btn_skip_10)); }
	inline Button_t2872111280 * get_btn_skip_10() const { return ___btn_skip_10; }
	inline Button_t2872111280 ** get_address_of_btn_skip_10() { return &___btn_skip_10; }
	inline void set_btn_skip_10(Button_t2872111280 * value)
	{
		___btn_skip_10 = value;
		Il2CppCodeGenWriteBarrier(&___btn_skip_10, value);
	}

	inline static int32_t get_offset_of_ListContainer_11() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___ListContainer_11)); }
	inline GameObject_t1756533147 * get_ListContainer_11() const { return ___ListContainer_11; }
	inline GameObject_t1756533147 ** get_address_of_ListContainer_11() { return &___ListContainer_11; }
	inline void set_ListContainer_11(GameObject_t1756533147 * value)
	{
		___ListContainer_11 = value;
		Il2CppCodeGenWriteBarrier(&___ListContainer_11, value);
	}

	inline static int32_t get_offset_of_DetailsPage_12() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___DetailsPage_12)); }
	inline GameObject_t1756533147 * get_DetailsPage_12() const { return ___DetailsPage_12; }
	inline GameObject_t1756533147 ** get_address_of_DetailsPage_12() { return &___DetailsPage_12; }
	inline void set_DetailsPage_12(GameObject_t1756533147 * value)
	{
		___DetailsPage_12 = value;
		Il2CppCodeGenWriteBarrier(&___DetailsPage_12, value);
	}

	inline static int32_t get_offset_of_Course_13() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___Course_13)); }
	inline GameObject_t1756533147 * get_Course_13() const { return ___Course_13; }
	inline GameObject_t1756533147 ** get_address_of_Course_13() { return &___Course_13; }
	inline void set_Course_13(GameObject_t1756533147 * value)
	{
		___Course_13 = value;
		Il2CppCodeGenWriteBarrier(&___Course_13, value);
	}

	inline static int32_t get_offset_of_MainCamera_14() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___MainCamera_14)); }
	inline Camera_t189460977 * get_MainCamera_14() const { return ___MainCamera_14; }
	inline Camera_t189460977 ** get_address_of_MainCamera_14() { return &___MainCamera_14; }
	inline void set_MainCamera_14(Camera_t189460977 * value)
	{
		___MainCamera_14 = value;
		Il2CppCodeGenWriteBarrier(&___MainCamera_14, value);
	}

	inline static int32_t get_offset_of_input_name_15() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___input_name_15)); }
	inline InputField_t1631627530 * get_input_name_15() const { return ___input_name_15; }
	inline InputField_t1631627530 ** get_address_of_input_name_15() { return &___input_name_15; }
	inline void set_input_name_15(InputField_t1631627530 * value)
	{
		___input_name_15 = value;
		Il2CppCodeGenWriteBarrier(&___input_name_15, value);
	}

	inline static int32_t get_offset_of_input_show_16() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___input_show_16)); }
	inline InputField_t1631627530 * get_input_show_16() const { return ___input_show_16; }
	inline InputField_t1631627530 ** get_address_of_input_show_16() { return &___input_show_16; }
	inline void set_input_show_16(InputField_t1631627530 * value)
	{
		___input_show_16 = value;
		Il2CppCodeGenWriteBarrier(&___input_show_16, value);
	}

	inline static int32_t get_offset_of_input_notes_17() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___input_notes_17)); }
	inline InputField_t1631627530 * get_input_notes_17() const { return ___input_notes_17; }
	inline InputField_t1631627530 ** get_address_of_input_notes_17() { return &___input_notes_17; }
	inline void set_input_notes_17(InputField_t1631627530 * value)
	{
		___input_notes_17 = value;
		Il2CppCodeGenWriteBarrier(&___input_notes_17, value);
	}

	inline static int32_t get_offset_of_DeleteMessage_18() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___DeleteMessage_18)); }
	inline GameObject_t1756533147 * get_DeleteMessage_18() const { return ___DeleteMessage_18; }
	inline GameObject_t1756533147 ** get_address_of_DeleteMessage_18() { return &___DeleteMessage_18; }
	inline void set_DeleteMessage_18(GameObject_t1756533147 * value)
	{
		___DeleteMessage_18 = value;
		Il2CppCodeGenWriteBarrier(&___DeleteMessage_18, value);
	}

	inline static int32_t get_offset_of_btn_cancelDelete_19() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___btn_cancelDelete_19)); }
	inline Button_t2872111280 * get_btn_cancelDelete_19() const { return ___btn_cancelDelete_19; }
	inline Button_t2872111280 ** get_address_of_btn_cancelDelete_19() { return &___btn_cancelDelete_19; }
	inline void set_btn_cancelDelete_19(Button_t2872111280 * value)
	{
		___btn_cancelDelete_19 = value;
		Il2CppCodeGenWriteBarrier(&___btn_cancelDelete_19, value);
	}

	inline static int32_t get_offset_of_btn_confirmDelete_20() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___btn_confirmDelete_20)); }
	inline Button_t2872111280 * get_btn_confirmDelete_20() const { return ___btn_confirmDelete_20; }
	inline Button_t2872111280 ** get_address_of_btn_confirmDelete_20() { return &___btn_confirmDelete_20; }
	inline void set_btn_confirmDelete_20(Button_t2872111280 * value)
	{
		___btn_confirmDelete_20 = value;
		Il2CppCodeGenWriteBarrier(&___btn_confirmDelete_20, value);
	}

	inline static int32_t get_offset_of_DeleteCourseId_21() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___DeleteCourseId_21)); }
	inline int64_t get_DeleteCourseId_21() const { return ___DeleteCourseId_21; }
	inline int64_t* get_address_of_DeleteCourseId_21() { return &___DeleteCourseId_21; }
	inline void set_DeleteCourseId_21(int64_t value)
	{
		___DeleteCourseId_21 = value;
	}

	inline static int32_t get_offset_of_CourseObjects_22() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___CourseObjects_22)); }
	inline List_1_t1125654279 * get_CourseObjects_22() const { return ___CourseObjects_22; }
	inline List_1_t1125654279 ** get_address_of_CourseObjects_22() { return &___CourseObjects_22; }
	inline void set_CourseObjects_22(List_1_t1125654279 * value)
	{
		___CourseObjects_22 = value;
		Il2CppCodeGenWriteBarrier(&___CourseObjects_22, value);
	}

	inline static int32_t get_offset_of_slider_courseImageScale_23() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___slider_courseImageScale_23)); }
	inline Slider_t297367283 * get_slider_courseImageScale_23() const { return ___slider_courseImageScale_23; }
	inline Slider_t297367283 ** get_address_of_slider_courseImageScale_23() { return &___slider_courseImageScale_23; }
	inline void set_slider_courseImageScale_23(Slider_t297367283 * value)
	{
		___slider_courseImageScale_23 = value;
		Il2CppCodeGenWriteBarrier(&___slider_courseImageScale_23, value);
	}

	inline static int32_t get_offset_of_waiting_24() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___waiting_24)); }
	inline GameObject_t1756533147 * get_waiting_24() const { return ___waiting_24; }
	inline GameObject_t1756533147 ** get_address_of_waiting_24() { return &___waiting_24; }
	inline void set_waiting_24(GameObject_t1756533147 * value)
	{
		___waiting_24 = value;
		Il2CppCodeGenWriteBarrier(&___waiting_24, value);
	}

	inline static int32_t get_offset_of_fail_25() { return static_cast<int32_t>(offsetof(CourseListController_t1183148997, ___fail_25)); }
	inline GameObject_t1756533147 * get_fail_25() const { return ___fail_25; }
	inline GameObject_t1756533147 ** get_address_of_fail_25() { return &___fail_25; }
	inline void set_fail_25(GameObject_t1756533147 * value)
	{
		___fail_25 = value;
		Il2CppCodeGenWriteBarrier(&___fail_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
