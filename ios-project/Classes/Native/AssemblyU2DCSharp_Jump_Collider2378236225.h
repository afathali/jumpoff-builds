﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_MCGBehaviour3307770884.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jump_Collider
struct  Jump_Collider_t2378236225  : public MCGBehaviour_t3307770884
{
public:
	// UnityEngine.GameObject Jump_Collider::Parent
	GameObject_t1756533147 * ___Parent_4;

public:
	inline static int32_t get_offset_of_Parent_4() { return static_cast<int32_t>(offsetof(Jump_Collider_t2378236225, ___Parent_4)); }
	inline GameObject_t1756533147 * get_Parent_4() const { return ___Parent_4; }
	inline GameObject_t1756533147 ** get_address_of_Parent_4() { return &___Parent_4; }
	inline void set_Parent_4(GameObject_t1756533147 * value)
	{
		___Parent_4 = value;
		Il2CppCodeGenWriteBarrier(&___Parent_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
