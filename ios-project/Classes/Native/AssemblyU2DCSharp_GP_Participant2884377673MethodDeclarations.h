﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_Participant
struct GP_Participant_t2884377673;
// System.String
struct String_t;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;
// GP_ParticipantResult
struct GP_ParticipantResult_t2469018720;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_ParticipantResult2469018720.h"
#include "AssemblyU2DCSharp_GP_RTM_ParticipantStatus2642611273.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void GP_Participant::.ctor(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void GP_Participant__ctor_m4075950142 (GP_Participant_t2884377673 * __this, String_t* ___uid0, String_t* ___playerUid1, String_t* ___stat2, String_t* ___hiResImg3, String_t* ___IconImg4, String_t* ___Name5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Participant::add_BigPhotoLoaded(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void GP_Participant_add_BigPhotoLoaded_m3808639437 (GP_Participant_t2884377673 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Participant::remove_BigPhotoLoaded(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void GP_Participant_remove_BigPhotoLoaded_m3350090240 (GP_Participant_t2884377673 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Participant::add_SmallPhotoLoaded(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void GP_Participant_add_SmallPhotoLoaded_m2273647708 (GP_Participant_t2884377673 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Participant::remove_SmallPhotoLoaded(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void GP_Participant_remove_SmallPhotoLoaded_m727206453 (GP_Participant_t2884377673 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Participant::SetResult(GP_ParticipantResult)
extern "C"  void GP_Participant_SetResult_m3762865037 (GP_Participant_t2884377673 * __this, GP_ParticipantResult_t2469018720 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Participant::LoadBigPhoto()
extern "C"  void GP_Participant_LoadBigPhoto_m2321472496 (GP_Participant_t2884377673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Participant::LoadSmallPhoto()
extern "C"  void GP_Participant_LoadSmallPhoto_m2681096813 (GP_Participant_t2884377673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GP_Participant::get_SmallPhoto()
extern "C"  Texture2D_t3542995729 * GP_Participant_get_SmallPhoto_m1425472383 (GP_Participant_t2884377673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GP_Participant::get_BigPhoto()
extern "C"  Texture2D_t3542995729 * GP_Participant_get_BigPhoto_m1252992960 (GP_Participant_t2884377673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GP_Participant::get_id()
extern "C"  String_t* GP_Participant_get_id_m1816161509 (GP_Participant_t2884377673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GP_Participant::get_playerId()
extern "C"  String_t* GP_Participant_get_playerId_m2645754642 (GP_Participant_t2884377673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GP_Participant::get_HiResImageUrl()
extern "C"  String_t* GP_Participant_get_HiResImageUrl_m3903028165 (GP_Participant_t2884377673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GP_Participant::get_IconImageUrl()
extern "C"  String_t* GP_Participant_get_IconImageUrl_m2568384263 (GP_Participant_t2884377673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GP_Participant::get_DisplayName()
extern "C"  String_t* GP_Participant_get_DisplayName_m3356480585 (GP_Participant_t2884377673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_RTM_ParticipantStatus GP_Participant::get_Status()
extern "C"  int32_t GP_Participant_get_Status_m4262474181 (GP_Participant_t2884377673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_ParticipantResult GP_Participant::get_Result()
extern "C"  GP_ParticipantResult_t2469018720 * GP_Participant_get_Result_m2217706213 (GP_Participant_t2884377673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Participant::HandheBigPhotoLoaed(UnityEngine.Texture2D)
extern "C"  void GP_Participant_HandheBigPhotoLoaed_m2620479651 (GP_Participant_t2884377673 * __this, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Participant::HandheSmallPhotoLoaed(UnityEngine.Texture2D)
extern "C"  void GP_Participant_HandheSmallPhotoLoaed_m4255980306 (GP_Participant_t2884377673 * __this, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Participant::<BigPhotoLoaded>m__8A(UnityEngine.Texture2D)
extern "C"  void GP_Participant_U3CBigPhotoLoadedU3Em__8A_m3796422275 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Participant::<SmallPhotoLoaded>m__8B(UnityEngine.Texture2D)
extern "C"  void GP_Participant_U3CSmallPhotoLoadedU3Em__8B_m1451554943 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
