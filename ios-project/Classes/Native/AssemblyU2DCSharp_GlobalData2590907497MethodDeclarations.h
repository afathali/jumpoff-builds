﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalData
struct GlobalData_t2590907497;
// User
struct User_t719925459;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void GlobalData::.ctor()
extern "C"  void GlobalData__ctor_m3036322558 (GlobalData_t2590907497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalData::.cctor()
extern "C"  void GlobalData__cctor_m1402771003 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// User GlobalData::get_User()
extern "C"  User_t719925459 * GlobalData_get_User_m882853540 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalData::set_User(User)
extern "C"  void GlobalData_set_User_m3180401593 (Il2CppObject * __this /* static, unused */, User_t719925459 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalData::PushScene(System.String)
extern "C"  void GlobalData_PushScene_m194178204 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalData::PopScene()
extern "C"  void GlobalData_PopScene_m2912004397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalData::PopToScene(System.String)
extern "C"  void GlobalData_PopToScene_m1550867768 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalData::LoadCurrentScene()
extern "C"  void GlobalData_LoadCurrentScene_m4037695193 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalData::StartNewCourseAttempt(System.Boolean)
extern "C"  void GlobalData_StartNewCourseAttempt_m4186013799 (Il2CppObject * __this /* static, unused */, bool ___blank0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalData::StartNewBlankCourse()
extern "C"  void GlobalData_StartNewBlankCourse_m1147129679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GlobalData::get_scale()
extern "C"  float GlobalData_get_scale_m3290671255 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalData::set_scale(System.Single)
extern "C"  void GlobalData_set_scale_m2842078280 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalData::onLogout()
extern "C"  void GlobalData_onLogout_m3105245731 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GlobalData::FixImageForSprite(UnityEngine.Texture2D)
extern "C"  Texture2D_t3542995729 * GlobalData_FixImageForSprite_m717388357 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
