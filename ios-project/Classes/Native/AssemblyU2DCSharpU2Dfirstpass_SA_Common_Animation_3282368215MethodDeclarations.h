﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Animation.SA_iTween/<TweenDelay>c__Iterator7
struct U3CTweenDelayU3Ec__Iterator7_t3282368215;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.Common.Animation.SA_iTween/<TweenDelay>c__Iterator7::.ctor()
extern "C"  void U3CTweenDelayU3Ec__Iterator7__ctor_m293800190 (U3CTweenDelayU3Ec__Iterator7_t3282368215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Animation.SA_iTween/<TweenDelay>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTweenDelayU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3769140362 (U3CTweenDelayU3Ec__Iterator7_t3282368215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Animation.SA_iTween/<TweenDelay>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTweenDelayU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1312722114 (U3CTweenDelayU3Ec__Iterator7_t3282368215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA.Common.Animation.SA_iTween/<TweenDelay>c__Iterator7::MoveNext()
extern "C"  bool U3CTweenDelayU3Ec__Iterator7_MoveNext_m3368741926 (U3CTweenDelayU3Ec__Iterator7_t3282368215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween/<TweenDelay>c__Iterator7::Dispose()
extern "C"  void U3CTweenDelayU3Ec__Iterator7_Dispose_m3888985427 (U3CTweenDelayU3Ec__Iterator7_t3282368215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween/<TweenDelay>c__Iterator7::Reset()
extern "C"  void U3CTweenDelayU3Ec__Iterator7_Reset_m1367212861 (U3CTweenDelayU3Ec__Iterator7_t3282368215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
