﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_PlayerSignatureResult
struct GK_PlayerSignatureResult_t13769479;
// System.String
struct String_t;
// SA.Common.Models.Error
struct Error_t445207774;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"

// System.Void GK_PlayerSignatureResult::.ctor(System.String,System.String,System.String,System.String)
extern "C"  void GK_PlayerSignatureResult__ctor_m813284204 (GK_PlayerSignatureResult_t13769479 * __this, String_t* ___publicKeyUrl0, String_t* ___signature1, String_t* ___salt2, String_t* ___timestamp3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_PlayerSignatureResult::.ctor(SA.Common.Models.Error)
extern "C"  void GK_PlayerSignatureResult__ctor_m1760301685 (GK_PlayerSignatureResult_t13769479 * __this, Error_t445207774 * ___errro0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_PlayerSignatureResult::get_PublicKeyUrl()
extern "C"  String_t* GK_PlayerSignatureResult_get_PublicKeyUrl_m1726883097 (GK_PlayerSignatureResult_t13769479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GK_PlayerSignatureResult::get_Signature()
extern "C"  ByteU5BU5D_t3397334013* GK_PlayerSignatureResult_get_Signature_m380871273 (GK_PlayerSignatureResult_t13769479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GK_PlayerSignatureResult::get_Salt()
extern "C"  ByteU5BU5D_t3397334013* GK_PlayerSignatureResult_get_Salt_m3903193911 (GK_PlayerSignatureResult_t13769479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GK_PlayerSignatureResult::get_Timestamp()
extern "C"  int64_t GK_PlayerSignatureResult_get_Timestamp_m1020712976 (GK_PlayerSignatureResult_t13769479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
