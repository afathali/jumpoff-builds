﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_534914198MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,UnityEngine.Texture2D>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m819376621(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1388460632 *, int32_t, Texture2D_t3542995729 *, const MethodInfo*))KeyValuePair_2__ctor_m3234979829_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,UnityEngine.Texture2D>::get_Key()
#define KeyValuePair_2_get_Key_m2519501707(__this, method) ((  int32_t (*) (KeyValuePair_2_t1388460632 *, const MethodInfo*))KeyValuePair_2_get_Key_m1840008439_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,UnityEngine.Texture2D>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m765593066(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1388460632 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m2457389368_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,UnityEngine.Texture2D>::get_Value()
#define KeyValuePair_2_get_Value_m730046315(__this, method) ((  Texture2D_t3542995729 * (*) (KeyValuePair_2_t1388460632 *, const MethodInfo*))KeyValuePair_2_get_Value_m1210921623_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,UnityEngine.Texture2D>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m540635338(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1388460632 *, Texture2D_t3542995729 *, const MethodInfo*))KeyValuePair_2_set_Value_m2109446280_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,UnityEngine.Texture2D>::ToString()
#define KeyValuePair_2_ToString_m2868491602(__this, method) ((  String_t* (*) (KeyValuePair_2_t1388460632 *, const MethodInfo*))KeyValuePair_2_ToString_m3271927904_gshared)(__this, method)
