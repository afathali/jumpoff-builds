﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<GP_RTM_Network_Package>
struct Action_1_t1852107251;
// System.Action`1<GP_RTM_Room>
struct Action_1_t653403911;
// System.Action`1<GP_RTM_ReliableMessageSentResult>
struct Action_1_t2545428778;
// System.Action`1<GP_RTM_ReliableMessageDeliveredResult>
struct Action_1_t1495869386;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<System.String[]>
struct Action_1_t1444185354;
// System.Action`1<GP_GamesStatusCodes>
struct Action_1_t815305555;
// System.Action`1<GP_RTM_Result>
struct Action_1_t275088661;
// System.Action`1<AndroidActivityResult>
struct Action_1_t3559310183;
// System.Action`1<GP_Invite>
struct Action_1_t428728469;
// GP_RTM_Room
struct GP_RTM_Room_t851604529;
// System.Collections.Generic.List`1<GP_Invite>
struct List_1_t4291017515;
// System.Collections.Generic.Dictionary`2<System.Int32,GP_RTM_ReliableMessageListener>
struct Dictionary_2_t351386314;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen1837866708.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayRTM
struct  GooglePlayRTM_t547022790  : public SA_Singleton_OLD_1_t1837866708
{
public:
	// GP_RTM_Room GooglePlayRTM::_currentRoom
	GP_RTM_Room_t851604529 * ____currentRoom_29;
	// System.Collections.Generic.List`1<GP_Invite> GooglePlayRTM::_invitations
	List_1_t4291017515 * ____invitations_30;
	// System.Collections.Generic.Dictionary`2<System.Int32,GP_RTM_ReliableMessageListener> GooglePlayRTM::_ReliableMassageListeners
	Dictionary_2_t351386314 * ____ReliableMassageListeners_31;

public:
	inline static int32_t get_offset_of__currentRoom_29() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790, ____currentRoom_29)); }
	inline GP_RTM_Room_t851604529 * get__currentRoom_29() const { return ____currentRoom_29; }
	inline GP_RTM_Room_t851604529 ** get_address_of__currentRoom_29() { return &____currentRoom_29; }
	inline void set__currentRoom_29(GP_RTM_Room_t851604529 * value)
	{
		____currentRoom_29 = value;
		Il2CppCodeGenWriteBarrier(&____currentRoom_29, value);
	}

	inline static int32_t get_offset_of__invitations_30() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790, ____invitations_30)); }
	inline List_1_t4291017515 * get__invitations_30() const { return ____invitations_30; }
	inline List_1_t4291017515 ** get_address_of__invitations_30() { return &____invitations_30; }
	inline void set__invitations_30(List_1_t4291017515 * value)
	{
		____invitations_30 = value;
		Il2CppCodeGenWriteBarrier(&____invitations_30, value);
	}

	inline static int32_t get_offset_of__ReliableMassageListeners_31() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790, ____ReliableMassageListeners_31)); }
	inline Dictionary_2_t351386314 * get__ReliableMassageListeners_31() const { return ____ReliableMassageListeners_31; }
	inline Dictionary_2_t351386314 ** get_address_of__ReliableMassageListeners_31() { return &____ReliableMassageListeners_31; }
	inline void set__ReliableMassageListeners_31(Dictionary_2_t351386314 * value)
	{
		____ReliableMassageListeners_31 = value;
		Il2CppCodeGenWriteBarrier(&____ReliableMassageListeners_31, value);
	}
};

struct GooglePlayRTM_t547022790_StaticFields
{
public:
	// System.Action`1<GP_RTM_Network_Package> GooglePlayRTM::ActionDataRecieved
	Action_1_t1852107251 * ___ActionDataRecieved_5;
	// System.Action`1<GP_RTM_Room> GooglePlayRTM::ActionRoomUpdated
	Action_1_t653403911 * ___ActionRoomUpdated_6;
	// System.Action`1<GP_RTM_ReliableMessageSentResult> GooglePlayRTM::ActionReliableMessageSent
	Action_1_t2545428778 * ___ActionReliableMessageSent_7;
	// System.Action`1<GP_RTM_ReliableMessageDeliveredResult> GooglePlayRTM::ActionReliableMessageDelivered
	Action_1_t1495869386 * ___ActionReliableMessageDelivered_8;
	// System.Action GooglePlayRTM::ActionConnectedToRoom
	Action_t3226471752 * ___ActionConnectedToRoom_9;
	// System.Action GooglePlayRTM::ActionDisconnectedFromRoom
	Action_t3226471752 * ___ActionDisconnectedFromRoom_10;
	// System.Action`1<System.String> GooglePlayRTM::ActionP2PConnected
	Action_1_t1831019615 * ___ActionP2PConnected_11;
	// System.Action`1<System.String> GooglePlayRTM::ActionP2PDisconnected
	Action_1_t1831019615 * ___ActionP2PDisconnected_12;
	// System.Action`1<System.String[]> GooglePlayRTM::ActionPeerDeclined
	Action_1_t1444185354 * ___ActionPeerDeclined_13;
	// System.Action`1<System.String[]> GooglePlayRTM::ActionPeerInvitedToRoom
	Action_1_t1444185354 * ___ActionPeerInvitedToRoom_14;
	// System.Action`1<System.String[]> GooglePlayRTM::ActionPeerJoined
	Action_1_t1444185354 * ___ActionPeerJoined_15;
	// System.Action`1<System.String[]> GooglePlayRTM::ActionPeerLeft
	Action_1_t1444185354 * ___ActionPeerLeft_16;
	// System.Action`1<System.String[]> GooglePlayRTM::ActionPeersConnected
	Action_1_t1444185354 * ___ActionPeersConnected_17;
	// System.Action`1<System.String[]> GooglePlayRTM::ActionPeersDisconnected
	Action_1_t1444185354 * ___ActionPeersDisconnected_18;
	// System.Action GooglePlayRTM::ActionRoomAutomatching
	Action_t3226471752 * ___ActionRoomAutomatching_19;
	// System.Action GooglePlayRTM::ActionRoomConnecting
	Action_t3226471752 * ___ActionRoomConnecting_20;
	// System.Action`1<GP_GamesStatusCodes> GooglePlayRTM::ActionJoinedRoom
	Action_1_t815305555 * ___ActionJoinedRoom_21;
	// System.Action`1<GP_RTM_Result> GooglePlayRTM::ActionLeftRoom
	Action_1_t275088661 * ___ActionLeftRoom_22;
	// System.Action`1<GP_GamesStatusCodes> GooglePlayRTM::ActionRoomConnected
	Action_1_t815305555 * ___ActionRoomConnected_23;
	// System.Action`1<GP_GamesStatusCodes> GooglePlayRTM::ActionRoomCreated
	Action_1_t815305555 * ___ActionRoomCreated_24;
	// System.Action`1<AndroidActivityResult> GooglePlayRTM::ActionInvitationBoxUIClosed
	Action_1_t3559310183 * ___ActionInvitationBoxUIClosed_25;
	// System.Action`1<AndroidActivityResult> GooglePlayRTM::ActionWatingRoomIntentClosed
	Action_1_t3559310183 * ___ActionWatingRoomIntentClosed_26;
	// System.Action`1<GP_Invite> GooglePlayRTM::ActionInvitationReceived
	Action_1_t428728469 * ___ActionInvitationReceived_27;
	// System.Action`1<System.String> GooglePlayRTM::ActionInvitationRemoved
	Action_1_t1831019615 * ___ActionInvitationRemoved_28;
	// System.Action`1<GP_RTM_Network_Package> GooglePlayRTM::<>f__am$cache1B
	Action_1_t1852107251 * ___U3CU3Ef__amU24cache1B_32;
	// System.Action`1<GP_RTM_Room> GooglePlayRTM::<>f__am$cache1C
	Action_1_t653403911 * ___U3CU3Ef__amU24cache1C_33;
	// System.Action`1<GP_RTM_ReliableMessageSentResult> GooglePlayRTM::<>f__am$cache1D
	Action_1_t2545428778 * ___U3CU3Ef__amU24cache1D_34;
	// System.Action`1<GP_RTM_ReliableMessageDeliveredResult> GooglePlayRTM::<>f__am$cache1E
	Action_1_t1495869386 * ___U3CU3Ef__amU24cache1E_35;
	// System.Action GooglePlayRTM::<>f__am$cache1F
	Action_t3226471752 * ___U3CU3Ef__amU24cache1F_36;
	// System.Action GooglePlayRTM::<>f__am$cache20
	Action_t3226471752 * ___U3CU3Ef__amU24cache20_37;
	// System.Action`1<System.String> GooglePlayRTM::<>f__am$cache21
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache21_38;
	// System.Action`1<System.String> GooglePlayRTM::<>f__am$cache22
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache22_39;
	// System.Action`1<System.String[]> GooglePlayRTM::<>f__am$cache23
	Action_1_t1444185354 * ___U3CU3Ef__amU24cache23_40;
	// System.Action`1<System.String[]> GooglePlayRTM::<>f__am$cache24
	Action_1_t1444185354 * ___U3CU3Ef__amU24cache24_41;
	// System.Action`1<System.String[]> GooglePlayRTM::<>f__am$cache25
	Action_1_t1444185354 * ___U3CU3Ef__amU24cache25_42;
	// System.Action`1<System.String[]> GooglePlayRTM::<>f__am$cache26
	Action_1_t1444185354 * ___U3CU3Ef__amU24cache26_43;
	// System.Action`1<System.String[]> GooglePlayRTM::<>f__am$cache27
	Action_1_t1444185354 * ___U3CU3Ef__amU24cache27_44;
	// System.Action`1<System.String[]> GooglePlayRTM::<>f__am$cache28
	Action_1_t1444185354 * ___U3CU3Ef__amU24cache28_45;
	// System.Action GooglePlayRTM::<>f__am$cache29
	Action_t3226471752 * ___U3CU3Ef__amU24cache29_46;
	// System.Action GooglePlayRTM::<>f__am$cache2A
	Action_t3226471752 * ___U3CU3Ef__amU24cache2A_47;
	// System.Action`1<GP_GamesStatusCodes> GooglePlayRTM::<>f__am$cache2B
	Action_1_t815305555 * ___U3CU3Ef__amU24cache2B_48;
	// System.Action`1<GP_RTM_Result> GooglePlayRTM::<>f__am$cache2C
	Action_1_t275088661 * ___U3CU3Ef__amU24cache2C_49;
	// System.Action`1<GP_GamesStatusCodes> GooglePlayRTM::<>f__am$cache2D
	Action_1_t815305555 * ___U3CU3Ef__amU24cache2D_50;
	// System.Action`1<GP_GamesStatusCodes> GooglePlayRTM::<>f__am$cache2E
	Action_1_t815305555 * ___U3CU3Ef__amU24cache2E_51;
	// System.Action`1<AndroidActivityResult> GooglePlayRTM::<>f__am$cache2F
	Action_1_t3559310183 * ___U3CU3Ef__amU24cache2F_52;
	// System.Action`1<AndroidActivityResult> GooglePlayRTM::<>f__am$cache30
	Action_1_t3559310183 * ___U3CU3Ef__amU24cache30_53;
	// System.Action`1<GP_Invite> GooglePlayRTM::<>f__am$cache31
	Action_1_t428728469 * ___U3CU3Ef__amU24cache31_54;
	// System.Action`1<System.String> GooglePlayRTM::<>f__am$cache32
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache32_55;

public:
	inline static int32_t get_offset_of_ActionDataRecieved_5() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionDataRecieved_5)); }
	inline Action_1_t1852107251 * get_ActionDataRecieved_5() const { return ___ActionDataRecieved_5; }
	inline Action_1_t1852107251 ** get_address_of_ActionDataRecieved_5() { return &___ActionDataRecieved_5; }
	inline void set_ActionDataRecieved_5(Action_1_t1852107251 * value)
	{
		___ActionDataRecieved_5 = value;
		Il2CppCodeGenWriteBarrier(&___ActionDataRecieved_5, value);
	}

	inline static int32_t get_offset_of_ActionRoomUpdated_6() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionRoomUpdated_6)); }
	inline Action_1_t653403911 * get_ActionRoomUpdated_6() const { return ___ActionRoomUpdated_6; }
	inline Action_1_t653403911 ** get_address_of_ActionRoomUpdated_6() { return &___ActionRoomUpdated_6; }
	inline void set_ActionRoomUpdated_6(Action_1_t653403911 * value)
	{
		___ActionRoomUpdated_6 = value;
		Il2CppCodeGenWriteBarrier(&___ActionRoomUpdated_6, value);
	}

	inline static int32_t get_offset_of_ActionReliableMessageSent_7() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionReliableMessageSent_7)); }
	inline Action_1_t2545428778 * get_ActionReliableMessageSent_7() const { return ___ActionReliableMessageSent_7; }
	inline Action_1_t2545428778 ** get_address_of_ActionReliableMessageSent_7() { return &___ActionReliableMessageSent_7; }
	inline void set_ActionReliableMessageSent_7(Action_1_t2545428778 * value)
	{
		___ActionReliableMessageSent_7 = value;
		Il2CppCodeGenWriteBarrier(&___ActionReliableMessageSent_7, value);
	}

	inline static int32_t get_offset_of_ActionReliableMessageDelivered_8() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionReliableMessageDelivered_8)); }
	inline Action_1_t1495869386 * get_ActionReliableMessageDelivered_8() const { return ___ActionReliableMessageDelivered_8; }
	inline Action_1_t1495869386 ** get_address_of_ActionReliableMessageDelivered_8() { return &___ActionReliableMessageDelivered_8; }
	inline void set_ActionReliableMessageDelivered_8(Action_1_t1495869386 * value)
	{
		___ActionReliableMessageDelivered_8 = value;
		Il2CppCodeGenWriteBarrier(&___ActionReliableMessageDelivered_8, value);
	}

	inline static int32_t get_offset_of_ActionConnectedToRoom_9() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionConnectedToRoom_9)); }
	inline Action_t3226471752 * get_ActionConnectedToRoom_9() const { return ___ActionConnectedToRoom_9; }
	inline Action_t3226471752 ** get_address_of_ActionConnectedToRoom_9() { return &___ActionConnectedToRoom_9; }
	inline void set_ActionConnectedToRoom_9(Action_t3226471752 * value)
	{
		___ActionConnectedToRoom_9 = value;
		Il2CppCodeGenWriteBarrier(&___ActionConnectedToRoom_9, value);
	}

	inline static int32_t get_offset_of_ActionDisconnectedFromRoom_10() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionDisconnectedFromRoom_10)); }
	inline Action_t3226471752 * get_ActionDisconnectedFromRoom_10() const { return ___ActionDisconnectedFromRoom_10; }
	inline Action_t3226471752 ** get_address_of_ActionDisconnectedFromRoom_10() { return &___ActionDisconnectedFromRoom_10; }
	inline void set_ActionDisconnectedFromRoom_10(Action_t3226471752 * value)
	{
		___ActionDisconnectedFromRoom_10 = value;
		Il2CppCodeGenWriteBarrier(&___ActionDisconnectedFromRoom_10, value);
	}

	inline static int32_t get_offset_of_ActionP2PConnected_11() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionP2PConnected_11)); }
	inline Action_1_t1831019615 * get_ActionP2PConnected_11() const { return ___ActionP2PConnected_11; }
	inline Action_1_t1831019615 ** get_address_of_ActionP2PConnected_11() { return &___ActionP2PConnected_11; }
	inline void set_ActionP2PConnected_11(Action_1_t1831019615 * value)
	{
		___ActionP2PConnected_11 = value;
		Il2CppCodeGenWriteBarrier(&___ActionP2PConnected_11, value);
	}

	inline static int32_t get_offset_of_ActionP2PDisconnected_12() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionP2PDisconnected_12)); }
	inline Action_1_t1831019615 * get_ActionP2PDisconnected_12() const { return ___ActionP2PDisconnected_12; }
	inline Action_1_t1831019615 ** get_address_of_ActionP2PDisconnected_12() { return &___ActionP2PDisconnected_12; }
	inline void set_ActionP2PDisconnected_12(Action_1_t1831019615 * value)
	{
		___ActionP2PDisconnected_12 = value;
		Il2CppCodeGenWriteBarrier(&___ActionP2PDisconnected_12, value);
	}

	inline static int32_t get_offset_of_ActionPeerDeclined_13() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionPeerDeclined_13)); }
	inline Action_1_t1444185354 * get_ActionPeerDeclined_13() const { return ___ActionPeerDeclined_13; }
	inline Action_1_t1444185354 ** get_address_of_ActionPeerDeclined_13() { return &___ActionPeerDeclined_13; }
	inline void set_ActionPeerDeclined_13(Action_1_t1444185354 * value)
	{
		___ActionPeerDeclined_13 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPeerDeclined_13, value);
	}

	inline static int32_t get_offset_of_ActionPeerInvitedToRoom_14() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionPeerInvitedToRoom_14)); }
	inline Action_1_t1444185354 * get_ActionPeerInvitedToRoom_14() const { return ___ActionPeerInvitedToRoom_14; }
	inline Action_1_t1444185354 ** get_address_of_ActionPeerInvitedToRoom_14() { return &___ActionPeerInvitedToRoom_14; }
	inline void set_ActionPeerInvitedToRoom_14(Action_1_t1444185354 * value)
	{
		___ActionPeerInvitedToRoom_14 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPeerInvitedToRoom_14, value);
	}

	inline static int32_t get_offset_of_ActionPeerJoined_15() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionPeerJoined_15)); }
	inline Action_1_t1444185354 * get_ActionPeerJoined_15() const { return ___ActionPeerJoined_15; }
	inline Action_1_t1444185354 ** get_address_of_ActionPeerJoined_15() { return &___ActionPeerJoined_15; }
	inline void set_ActionPeerJoined_15(Action_1_t1444185354 * value)
	{
		___ActionPeerJoined_15 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPeerJoined_15, value);
	}

	inline static int32_t get_offset_of_ActionPeerLeft_16() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionPeerLeft_16)); }
	inline Action_1_t1444185354 * get_ActionPeerLeft_16() const { return ___ActionPeerLeft_16; }
	inline Action_1_t1444185354 ** get_address_of_ActionPeerLeft_16() { return &___ActionPeerLeft_16; }
	inline void set_ActionPeerLeft_16(Action_1_t1444185354 * value)
	{
		___ActionPeerLeft_16 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPeerLeft_16, value);
	}

	inline static int32_t get_offset_of_ActionPeersConnected_17() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionPeersConnected_17)); }
	inline Action_1_t1444185354 * get_ActionPeersConnected_17() const { return ___ActionPeersConnected_17; }
	inline Action_1_t1444185354 ** get_address_of_ActionPeersConnected_17() { return &___ActionPeersConnected_17; }
	inline void set_ActionPeersConnected_17(Action_1_t1444185354 * value)
	{
		___ActionPeersConnected_17 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPeersConnected_17, value);
	}

	inline static int32_t get_offset_of_ActionPeersDisconnected_18() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionPeersDisconnected_18)); }
	inline Action_1_t1444185354 * get_ActionPeersDisconnected_18() const { return ___ActionPeersDisconnected_18; }
	inline Action_1_t1444185354 ** get_address_of_ActionPeersDisconnected_18() { return &___ActionPeersDisconnected_18; }
	inline void set_ActionPeersDisconnected_18(Action_1_t1444185354 * value)
	{
		___ActionPeersDisconnected_18 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPeersDisconnected_18, value);
	}

	inline static int32_t get_offset_of_ActionRoomAutomatching_19() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionRoomAutomatching_19)); }
	inline Action_t3226471752 * get_ActionRoomAutomatching_19() const { return ___ActionRoomAutomatching_19; }
	inline Action_t3226471752 ** get_address_of_ActionRoomAutomatching_19() { return &___ActionRoomAutomatching_19; }
	inline void set_ActionRoomAutomatching_19(Action_t3226471752 * value)
	{
		___ActionRoomAutomatching_19 = value;
		Il2CppCodeGenWriteBarrier(&___ActionRoomAutomatching_19, value);
	}

	inline static int32_t get_offset_of_ActionRoomConnecting_20() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionRoomConnecting_20)); }
	inline Action_t3226471752 * get_ActionRoomConnecting_20() const { return ___ActionRoomConnecting_20; }
	inline Action_t3226471752 ** get_address_of_ActionRoomConnecting_20() { return &___ActionRoomConnecting_20; }
	inline void set_ActionRoomConnecting_20(Action_t3226471752 * value)
	{
		___ActionRoomConnecting_20 = value;
		Il2CppCodeGenWriteBarrier(&___ActionRoomConnecting_20, value);
	}

	inline static int32_t get_offset_of_ActionJoinedRoom_21() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionJoinedRoom_21)); }
	inline Action_1_t815305555 * get_ActionJoinedRoom_21() const { return ___ActionJoinedRoom_21; }
	inline Action_1_t815305555 ** get_address_of_ActionJoinedRoom_21() { return &___ActionJoinedRoom_21; }
	inline void set_ActionJoinedRoom_21(Action_1_t815305555 * value)
	{
		___ActionJoinedRoom_21 = value;
		Il2CppCodeGenWriteBarrier(&___ActionJoinedRoom_21, value);
	}

	inline static int32_t get_offset_of_ActionLeftRoom_22() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionLeftRoom_22)); }
	inline Action_1_t275088661 * get_ActionLeftRoom_22() const { return ___ActionLeftRoom_22; }
	inline Action_1_t275088661 ** get_address_of_ActionLeftRoom_22() { return &___ActionLeftRoom_22; }
	inline void set_ActionLeftRoom_22(Action_1_t275088661 * value)
	{
		___ActionLeftRoom_22 = value;
		Il2CppCodeGenWriteBarrier(&___ActionLeftRoom_22, value);
	}

	inline static int32_t get_offset_of_ActionRoomConnected_23() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionRoomConnected_23)); }
	inline Action_1_t815305555 * get_ActionRoomConnected_23() const { return ___ActionRoomConnected_23; }
	inline Action_1_t815305555 ** get_address_of_ActionRoomConnected_23() { return &___ActionRoomConnected_23; }
	inline void set_ActionRoomConnected_23(Action_1_t815305555 * value)
	{
		___ActionRoomConnected_23 = value;
		Il2CppCodeGenWriteBarrier(&___ActionRoomConnected_23, value);
	}

	inline static int32_t get_offset_of_ActionRoomCreated_24() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionRoomCreated_24)); }
	inline Action_1_t815305555 * get_ActionRoomCreated_24() const { return ___ActionRoomCreated_24; }
	inline Action_1_t815305555 ** get_address_of_ActionRoomCreated_24() { return &___ActionRoomCreated_24; }
	inline void set_ActionRoomCreated_24(Action_1_t815305555 * value)
	{
		___ActionRoomCreated_24 = value;
		Il2CppCodeGenWriteBarrier(&___ActionRoomCreated_24, value);
	}

	inline static int32_t get_offset_of_ActionInvitationBoxUIClosed_25() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionInvitationBoxUIClosed_25)); }
	inline Action_1_t3559310183 * get_ActionInvitationBoxUIClosed_25() const { return ___ActionInvitationBoxUIClosed_25; }
	inline Action_1_t3559310183 ** get_address_of_ActionInvitationBoxUIClosed_25() { return &___ActionInvitationBoxUIClosed_25; }
	inline void set_ActionInvitationBoxUIClosed_25(Action_1_t3559310183 * value)
	{
		___ActionInvitationBoxUIClosed_25 = value;
		Il2CppCodeGenWriteBarrier(&___ActionInvitationBoxUIClosed_25, value);
	}

	inline static int32_t get_offset_of_ActionWatingRoomIntentClosed_26() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionWatingRoomIntentClosed_26)); }
	inline Action_1_t3559310183 * get_ActionWatingRoomIntentClosed_26() const { return ___ActionWatingRoomIntentClosed_26; }
	inline Action_1_t3559310183 ** get_address_of_ActionWatingRoomIntentClosed_26() { return &___ActionWatingRoomIntentClosed_26; }
	inline void set_ActionWatingRoomIntentClosed_26(Action_1_t3559310183 * value)
	{
		___ActionWatingRoomIntentClosed_26 = value;
		Il2CppCodeGenWriteBarrier(&___ActionWatingRoomIntentClosed_26, value);
	}

	inline static int32_t get_offset_of_ActionInvitationReceived_27() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionInvitationReceived_27)); }
	inline Action_1_t428728469 * get_ActionInvitationReceived_27() const { return ___ActionInvitationReceived_27; }
	inline Action_1_t428728469 ** get_address_of_ActionInvitationReceived_27() { return &___ActionInvitationReceived_27; }
	inline void set_ActionInvitationReceived_27(Action_1_t428728469 * value)
	{
		___ActionInvitationReceived_27 = value;
		Il2CppCodeGenWriteBarrier(&___ActionInvitationReceived_27, value);
	}

	inline static int32_t get_offset_of_ActionInvitationRemoved_28() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___ActionInvitationRemoved_28)); }
	inline Action_1_t1831019615 * get_ActionInvitationRemoved_28() const { return ___ActionInvitationRemoved_28; }
	inline Action_1_t1831019615 ** get_address_of_ActionInvitationRemoved_28() { return &___ActionInvitationRemoved_28; }
	inline void set_ActionInvitationRemoved_28(Action_1_t1831019615 * value)
	{
		___ActionInvitationRemoved_28 = value;
		Il2CppCodeGenWriteBarrier(&___ActionInvitationRemoved_28, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1B_32() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache1B_32)); }
	inline Action_1_t1852107251 * get_U3CU3Ef__amU24cache1B_32() const { return ___U3CU3Ef__amU24cache1B_32; }
	inline Action_1_t1852107251 ** get_address_of_U3CU3Ef__amU24cache1B_32() { return &___U3CU3Ef__amU24cache1B_32; }
	inline void set_U3CU3Ef__amU24cache1B_32(Action_1_t1852107251 * value)
	{
		___U3CU3Ef__amU24cache1B_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1B_32, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_33() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache1C_33)); }
	inline Action_1_t653403911 * get_U3CU3Ef__amU24cache1C_33() const { return ___U3CU3Ef__amU24cache1C_33; }
	inline Action_1_t653403911 ** get_address_of_U3CU3Ef__amU24cache1C_33() { return &___U3CU3Ef__amU24cache1C_33; }
	inline void set_U3CU3Ef__amU24cache1C_33(Action_1_t653403911 * value)
	{
		___U3CU3Ef__amU24cache1C_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1C_33, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1D_34() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache1D_34)); }
	inline Action_1_t2545428778 * get_U3CU3Ef__amU24cache1D_34() const { return ___U3CU3Ef__amU24cache1D_34; }
	inline Action_1_t2545428778 ** get_address_of_U3CU3Ef__amU24cache1D_34() { return &___U3CU3Ef__amU24cache1D_34; }
	inline void set_U3CU3Ef__amU24cache1D_34(Action_1_t2545428778 * value)
	{
		___U3CU3Ef__amU24cache1D_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1D_34, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1E_35() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache1E_35)); }
	inline Action_1_t1495869386 * get_U3CU3Ef__amU24cache1E_35() const { return ___U3CU3Ef__amU24cache1E_35; }
	inline Action_1_t1495869386 ** get_address_of_U3CU3Ef__amU24cache1E_35() { return &___U3CU3Ef__amU24cache1E_35; }
	inline void set_U3CU3Ef__amU24cache1E_35(Action_1_t1495869386 * value)
	{
		___U3CU3Ef__amU24cache1E_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1E_35, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1F_36() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache1F_36)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache1F_36() const { return ___U3CU3Ef__amU24cache1F_36; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache1F_36() { return &___U3CU3Ef__amU24cache1F_36; }
	inline void set_U3CU3Ef__amU24cache1F_36(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache1F_36 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1F_36, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache20_37() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache20_37)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache20_37() const { return ___U3CU3Ef__amU24cache20_37; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache20_37() { return &___U3CU3Ef__amU24cache20_37; }
	inline void set_U3CU3Ef__amU24cache20_37(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache20_37 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache20_37, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache21_38() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache21_38)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache21_38() const { return ___U3CU3Ef__amU24cache21_38; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache21_38() { return &___U3CU3Ef__amU24cache21_38; }
	inline void set_U3CU3Ef__amU24cache21_38(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache21_38 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache21_38, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache22_39() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache22_39)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache22_39() const { return ___U3CU3Ef__amU24cache22_39; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache22_39() { return &___U3CU3Ef__amU24cache22_39; }
	inline void set_U3CU3Ef__amU24cache22_39(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache22_39 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache22_39, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache23_40() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache23_40)); }
	inline Action_1_t1444185354 * get_U3CU3Ef__amU24cache23_40() const { return ___U3CU3Ef__amU24cache23_40; }
	inline Action_1_t1444185354 ** get_address_of_U3CU3Ef__amU24cache23_40() { return &___U3CU3Ef__amU24cache23_40; }
	inline void set_U3CU3Ef__amU24cache23_40(Action_1_t1444185354 * value)
	{
		___U3CU3Ef__amU24cache23_40 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache23_40, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache24_41() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache24_41)); }
	inline Action_1_t1444185354 * get_U3CU3Ef__amU24cache24_41() const { return ___U3CU3Ef__amU24cache24_41; }
	inline Action_1_t1444185354 ** get_address_of_U3CU3Ef__amU24cache24_41() { return &___U3CU3Ef__amU24cache24_41; }
	inline void set_U3CU3Ef__amU24cache24_41(Action_1_t1444185354 * value)
	{
		___U3CU3Ef__amU24cache24_41 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache24_41, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache25_42() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache25_42)); }
	inline Action_1_t1444185354 * get_U3CU3Ef__amU24cache25_42() const { return ___U3CU3Ef__amU24cache25_42; }
	inline Action_1_t1444185354 ** get_address_of_U3CU3Ef__amU24cache25_42() { return &___U3CU3Ef__amU24cache25_42; }
	inline void set_U3CU3Ef__amU24cache25_42(Action_1_t1444185354 * value)
	{
		___U3CU3Ef__amU24cache25_42 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache25_42, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache26_43() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache26_43)); }
	inline Action_1_t1444185354 * get_U3CU3Ef__amU24cache26_43() const { return ___U3CU3Ef__amU24cache26_43; }
	inline Action_1_t1444185354 ** get_address_of_U3CU3Ef__amU24cache26_43() { return &___U3CU3Ef__amU24cache26_43; }
	inline void set_U3CU3Ef__amU24cache26_43(Action_1_t1444185354 * value)
	{
		___U3CU3Ef__amU24cache26_43 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache26_43, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache27_44() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache27_44)); }
	inline Action_1_t1444185354 * get_U3CU3Ef__amU24cache27_44() const { return ___U3CU3Ef__amU24cache27_44; }
	inline Action_1_t1444185354 ** get_address_of_U3CU3Ef__amU24cache27_44() { return &___U3CU3Ef__amU24cache27_44; }
	inline void set_U3CU3Ef__amU24cache27_44(Action_1_t1444185354 * value)
	{
		___U3CU3Ef__amU24cache27_44 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache27_44, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache28_45() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache28_45)); }
	inline Action_1_t1444185354 * get_U3CU3Ef__amU24cache28_45() const { return ___U3CU3Ef__amU24cache28_45; }
	inline Action_1_t1444185354 ** get_address_of_U3CU3Ef__amU24cache28_45() { return &___U3CU3Ef__amU24cache28_45; }
	inline void set_U3CU3Ef__amU24cache28_45(Action_1_t1444185354 * value)
	{
		___U3CU3Ef__amU24cache28_45 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache28_45, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache29_46() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache29_46)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache29_46() const { return ___U3CU3Ef__amU24cache29_46; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache29_46() { return &___U3CU3Ef__amU24cache29_46; }
	inline void set_U3CU3Ef__amU24cache29_46(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache29_46 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache29_46, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2A_47() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache2A_47)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache2A_47() const { return ___U3CU3Ef__amU24cache2A_47; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache2A_47() { return &___U3CU3Ef__amU24cache2A_47; }
	inline void set_U3CU3Ef__amU24cache2A_47(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache2A_47 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2A_47, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2B_48() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache2B_48)); }
	inline Action_1_t815305555 * get_U3CU3Ef__amU24cache2B_48() const { return ___U3CU3Ef__amU24cache2B_48; }
	inline Action_1_t815305555 ** get_address_of_U3CU3Ef__amU24cache2B_48() { return &___U3CU3Ef__amU24cache2B_48; }
	inline void set_U3CU3Ef__amU24cache2B_48(Action_1_t815305555 * value)
	{
		___U3CU3Ef__amU24cache2B_48 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2B_48, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2C_49() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache2C_49)); }
	inline Action_1_t275088661 * get_U3CU3Ef__amU24cache2C_49() const { return ___U3CU3Ef__amU24cache2C_49; }
	inline Action_1_t275088661 ** get_address_of_U3CU3Ef__amU24cache2C_49() { return &___U3CU3Ef__amU24cache2C_49; }
	inline void set_U3CU3Ef__amU24cache2C_49(Action_1_t275088661 * value)
	{
		___U3CU3Ef__amU24cache2C_49 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2C_49, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2D_50() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache2D_50)); }
	inline Action_1_t815305555 * get_U3CU3Ef__amU24cache2D_50() const { return ___U3CU3Ef__amU24cache2D_50; }
	inline Action_1_t815305555 ** get_address_of_U3CU3Ef__amU24cache2D_50() { return &___U3CU3Ef__amU24cache2D_50; }
	inline void set_U3CU3Ef__amU24cache2D_50(Action_1_t815305555 * value)
	{
		___U3CU3Ef__amU24cache2D_50 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2D_50, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2E_51() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache2E_51)); }
	inline Action_1_t815305555 * get_U3CU3Ef__amU24cache2E_51() const { return ___U3CU3Ef__amU24cache2E_51; }
	inline Action_1_t815305555 ** get_address_of_U3CU3Ef__amU24cache2E_51() { return &___U3CU3Ef__amU24cache2E_51; }
	inline void set_U3CU3Ef__amU24cache2E_51(Action_1_t815305555 * value)
	{
		___U3CU3Ef__amU24cache2E_51 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2E_51, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2F_52() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache2F_52)); }
	inline Action_1_t3559310183 * get_U3CU3Ef__amU24cache2F_52() const { return ___U3CU3Ef__amU24cache2F_52; }
	inline Action_1_t3559310183 ** get_address_of_U3CU3Ef__amU24cache2F_52() { return &___U3CU3Ef__amU24cache2F_52; }
	inline void set_U3CU3Ef__amU24cache2F_52(Action_1_t3559310183 * value)
	{
		___U3CU3Ef__amU24cache2F_52 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2F_52, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache30_53() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache30_53)); }
	inline Action_1_t3559310183 * get_U3CU3Ef__amU24cache30_53() const { return ___U3CU3Ef__amU24cache30_53; }
	inline Action_1_t3559310183 ** get_address_of_U3CU3Ef__amU24cache30_53() { return &___U3CU3Ef__amU24cache30_53; }
	inline void set_U3CU3Ef__amU24cache30_53(Action_1_t3559310183 * value)
	{
		___U3CU3Ef__amU24cache30_53 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache30_53, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache31_54() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache31_54)); }
	inline Action_1_t428728469 * get_U3CU3Ef__amU24cache31_54() const { return ___U3CU3Ef__amU24cache31_54; }
	inline Action_1_t428728469 ** get_address_of_U3CU3Ef__amU24cache31_54() { return &___U3CU3Ef__amU24cache31_54; }
	inline void set_U3CU3Ef__amU24cache31_54(Action_1_t428728469 * value)
	{
		___U3CU3Ef__amU24cache31_54 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache31_54, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache32_55() { return static_cast<int32_t>(offsetof(GooglePlayRTM_t547022790_StaticFields, ___U3CU3Ef__amU24cache32_55)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache32_55() const { return ___U3CU3Ef__amU24cache32_55; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache32_55() { return &___U3CU3Ef__amU24cache32_55; }
	inline void set_U3CU3Ef__amU24cache32_55(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache32_55 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache32_55, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
