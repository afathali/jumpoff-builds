﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer
struct JTokenReferenceEqualityComparer_t4173827918;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer
struct  JTokenReferenceEqualityComparer_t4173827918  : public Il2CppObject
{
public:

public:
};

struct JTokenReferenceEqualityComparer_t4173827918_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer::Instance
	JTokenReferenceEqualityComparer_t4173827918 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(JTokenReferenceEqualityComparer_t4173827918_StaticFields, ___Instance_0)); }
	inline JTokenReferenceEqualityComparer_t4173827918 * get_Instance_0() const { return ___Instance_0; }
	inline JTokenReferenceEqualityComparer_t4173827918 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(JTokenReferenceEqualityComparer_t4173827918 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
