﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CK_QueryResult
struct CK_QueryResult_t1174067488;
// System.Collections.Generic.List`1<CK_Record>
struct List_1_t3342662894;
// System.String
struct String_t;
// CK_Database
struct CK_Database_t243306482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_Database243306482.h"

// System.Void CK_QueryResult::.ctor(System.Collections.Generic.List`1<CK_Record>)
extern "C"  void CK_QueryResult__ctor_m731557337 (CK_QueryResult_t1174067488 * __this, List_1_t3342662894 * ___records0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_QueryResult::.ctor(System.String)
extern "C"  void CK_QueryResult__ctor_m3516705479 (CK_QueryResult_t1174067488 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_QueryResult::SetDatabase(CK_Database)
extern "C"  void CK_QueryResult_SetDatabase_m3190537700 (CK_QueryResult_t1174067488 * __this, CK_Database_t243306482 * ___database0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CK_Database CK_QueryResult::get_Database()
extern "C"  CK_Database_t243306482 * CK_QueryResult_get_Database_m1974163646 (CK_QueryResult_t1174067488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CK_Record> CK_QueryResult::get_Records()
extern "C"  List_1_t3342662894 * CK_QueryResult_get_Records_m2719044305 (CK_QueryResult_t1174067488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
