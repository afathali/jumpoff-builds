﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>
struct ListWrapper_1_t3921709322;
// System.Collections.IList
struct IList_t3321498491;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::.ctor(System.Collections.IList)
extern "C"  void ListWrapper_1__ctor_m2366675627_gshared (ListWrapper_1_t3921709322 * __this, Il2CppObject * ___list0, const MethodInfo* method);
#define ListWrapper_1__ctor_m2366675627(__this, ___list0, method) ((  void (*) (ListWrapper_1_t3921709322 *, Il2CppObject *, const MethodInfo*))ListWrapper_1__ctor_m2366675627_gshared)(__this, ___list0, method)
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ListWrapper_1__ctor_m2554805603_gshared (ListWrapper_1_t3921709322 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ListWrapper_1__ctor_m2554805603(__this, ___list0, method) ((  void (*) (ListWrapper_1_t3921709322 *, Il2CppObject*, const MethodInfo*))ListWrapper_1__ctor_m2554805603_gshared)(__this, ___list0, method)
// System.Int32 Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::IndexOf(T)
extern "C"  int32_t ListWrapper_1_IndexOf_m1072497437_gshared (ListWrapper_1_t3921709322 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListWrapper_1_IndexOf_m1072497437(__this, ___item0, method) ((  int32_t (*) (ListWrapper_1_t3921709322 *, Il2CppObject *, const MethodInfo*))ListWrapper_1_IndexOf_m1072497437_gshared)(__this, ___item0, method)
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ListWrapper_1_Insert_m3073337676_gshared (ListWrapper_1_t3921709322 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define ListWrapper_1_Insert_m3073337676(__this, ___index0, ___item1, method) ((  void (*) (ListWrapper_1_t3921709322 *, int32_t, Il2CppObject *, const MethodInfo*))ListWrapper_1_Insert_m3073337676_gshared)(__this, ___index0, ___item1, method)
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ListWrapper_1_RemoveAt_m704212032_gshared (ListWrapper_1_t3921709322 * __this, int32_t ___index0, const MethodInfo* method);
#define ListWrapper_1_RemoveAt_m704212032(__this, ___index0, method) ((  void (*) (ListWrapper_1_t3921709322 *, int32_t, const MethodInfo*))ListWrapper_1_RemoveAt_m704212032_gshared)(__this, ___index0, method)
// T Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ListWrapper_1_get_Item_m745842054_gshared (ListWrapper_1_t3921709322 * __this, int32_t ___index0, const MethodInfo* method);
#define ListWrapper_1_get_Item_m745842054(__this, ___index0, method) ((  Il2CppObject * (*) (ListWrapper_1_t3921709322 *, int32_t, const MethodInfo*))ListWrapper_1_get_Item_m745842054_gshared)(__this, ___index0, method)
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ListWrapper_1_set_Item_m4199764057_gshared (ListWrapper_1_t3921709322 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ListWrapper_1_set_Item_m4199764057(__this, ___index0, ___value1, method) ((  void (*) (ListWrapper_1_t3921709322 *, int32_t, Il2CppObject *, const MethodInfo*))ListWrapper_1_set_Item_m4199764057_gshared)(__this, ___index0, ___value1, method)
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Add(T)
extern "C"  void ListWrapper_1_Add_m60348073_gshared (ListWrapper_1_t3921709322 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListWrapper_1_Add_m60348073(__this, ___item0, method) ((  void (*) (ListWrapper_1_t3921709322 *, Il2CppObject *, const MethodInfo*))ListWrapper_1_Add_m60348073_gshared)(__this, ___item0, method)
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Clear()
extern "C"  void ListWrapper_1_Clear_m2871348141_gshared (ListWrapper_1_t3921709322 * __this, const MethodInfo* method);
#define ListWrapper_1_Clear_m2871348141(__this, method) ((  void (*) (ListWrapper_1_t3921709322 *, const MethodInfo*))ListWrapper_1_Clear_m2871348141_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Contains(T)
extern "C"  bool ListWrapper_1_Contains_m2494154183_gshared (ListWrapper_1_t3921709322 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListWrapper_1_Contains_m2494154183(__this, ___item0, method) ((  bool (*) (ListWrapper_1_t3921709322 *, Il2CppObject *, const MethodInfo*))ListWrapper_1_Contains_m2494154183_gshared)(__this, ___item0, method)
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ListWrapper_1_CopyTo_m263509521_gshared (ListWrapper_1_t3921709322 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ListWrapper_1_CopyTo_m263509521(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ListWrapper_1_t3921709322 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ListWrapper_1_CopyTo_m263509521_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::get_Count()
extern "C"  int32_t ListWrapper_1_get_Count_m822578180_gshared (ListWrapper_1_t3921709322 * __this, const MethodInfo* method);
#define ListWrapper_1_get_Count_m822578180(__this, method) ((  int32_t (*) (ListWrapper_1_t3921709322 *, const MethodInfo*))ListWrapper_1_get_Count_m822578180_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::get_IsReadOnly()
extern "C"  bool ListWrapper_1_get_IsReadOnly_m2983319533_gshared (ListWrapper_1_t3921709322 * __this, const MethodInfo* method);
#define ListWrapper_1_get_IsReadOnly_m2983319533(__this, method) ((  bool (*) (ListWrapper_1_t3921709322 *, const MethodInfo*))ListWrapper_1_get_IsReadOnly_m2983319533_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Remove(T)
extern "C"  bool ListWrapper_1_Remove_m225401320_gshared (ListWrapper_1_t3921709322 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListWrapper_1_Remove_m225401320(__this, ___item0, method) ((  bool (*) (ListWrapper_1_t3921709322 *, Il2CppObject *, const MethodInfo*))ListWrapper_1_Remove_m225401320_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ListWrapper_1_GetEnumerator_m3921208890_gshared (ListWrapper_1_t3921709322 * __this, const MethodInfo* method);
#define ListWrapper_1_GetEnumerator_m3921208890(__this, method) ((  Il2CppObject* (*) (ListWrapper_1_t3921709322 *, const MethodInfo*))ListWrapper_1_GetEnumerator_m3921208890_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::get_UnderlyingList()
extern "C"  Il2CppObject * ListWrapper_1_get_UnderlyingList_m960612259_gshared (ListWrapper_1_t3921709322 * __this, const MethodInfo* method);
#define ListWrapper_1_get_UnderlyingList_m960612259(__this, method) ((  Il2CppObject * (*) (ListWrapper_1_t3921709322 *, const MethodInfo*))ListWrapper_1_get_UnderlyingList_m960612259_gshared)(__this, method)
