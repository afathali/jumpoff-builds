﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TwitterPostingTask
struct TwitterPostingTask_t1522896362;
// System.Action`1<TWResult>
struct Action_1_t1282590442;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// TwitterManagerInterface
struct TwitterManagerInterface_t481322177;
// TWResult
struct TWResult_t1480791060;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharp_TWResult1480791060.h"

// System.Void TwitterPostingTask::.ctor()
extern "C"  void TwitterPostingTask__ctor_m1145425343 (TwitterPostingTask_t1522896362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterPostingTask::add_ActionComplete(System.Action`1<TWResult>)
extern "C"  void TwitterPostingTask_add_ActionComplete_m3394794260 (TwitterPostingTask_t1522896362 * __this, Action_1_t1282590442 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterPostingTask::remove_ActionComplete(System.Action`1<TWResult>)
extern "C"  void TwitterPostingTask_remove_ActionComplete_m1588807473 (TwitterPostingTask_t1522896362 * __this, Action_1_t1282590442 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TwitterPostingTask TwitterPostingTask::Cretae()
extern "C"  TwitterPostingTask_t1522896362 * TwitterPostingTask_Cretae_m2915952766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterPostingTask::Post(System.String,UnityEngine.Texture2D,TwitterManagerInterface)
extern "C"  void TwitterPostingTask_Post_m891788832 (TwitterPostingTask_t1522896362 * __this, String_t* ___status0, Texture2D_t3542995729 * ___texture1, Il2CppObject * ___controller2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterPostingTask::OnTWInited(TWResult)
extern "C"  void TwitterPostingTask_OnTWInited_m2823250626 (TwitterPostingTask_t1522896362 * __this, TWResult_t1480791060 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterPostingTask::OnTWAuth(TWResult)
extern "C"  void TwitterPostingTask_OnTWAuth_m3844445475 (TwitterPostingTask_t1522896362 * __this, TWResult_t1480791060 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterPostingTask::OnPost(TWResult)
extern "C"  void TwitterPostingTask_OnPost_m3612295638 (TwitterPostingTask_t1522896362 * __this, TWResult_t1480791060 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterPostingTask::<ActionComplete>m__B1(TWResult)
extern "C"  void TwitterPostingTask_U3CActionCompleteU3Em__B1_m2643367298 (Il2CppObject * __this /* static, unused */, TWResult_t1480791060 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
