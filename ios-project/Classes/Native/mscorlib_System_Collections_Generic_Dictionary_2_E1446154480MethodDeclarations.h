﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3721161463(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1446154480 *, Dictionary_2_t126129778 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3766004088(__this, method) ((  Il2CppObject * (*) (Enumerator_t1446154480 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2389577886(__this, method) ((  void (*) (Enumerator_t1446154480 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1947979853(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1446154480 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3965920802(__this, method) ((  Il2CppObject * (*) (Enumerator_t1446154480 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2749686872(__this, method) ((  Il2CppObject * (*) (Enumerator_t1446154480 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::MoveNext()
#define Enumerator_MoveNext_m4008573606(__this, method) ((  bool (*) (Enumerator_t1446154480 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::get_Current()
#define Enumerator_get_Current_m1274453014(__this, method) ((  KeyValuePair_2_t2178442296  (*) (Enumerator_t1446154480 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3423562771(__this, method) ((  String_t* (*) (Enumerator_t1446154480 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2249082283(__this, method) ((  GooglePlayerTemplate_t2506317812 * (*) (Enumerator_t1446154480 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::Reset()
#define Enumerator_Reset_m2682413941(__this, method) ((  void (*) (Enumerator_t1446154480 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::VerifyState()
#define Enumerator_VerifyState_m3347219874(__this, method) ((  void (*) (Enumerator_t1446154480 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3566985166(__this, method) ((  void (*) (Enumerator_t1446154480 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayerTemplate>::Dispose()
#define Enumerator_Dispose_m1573374467(__this, method) ((  void (*) (Enumerator_t1446154480 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
