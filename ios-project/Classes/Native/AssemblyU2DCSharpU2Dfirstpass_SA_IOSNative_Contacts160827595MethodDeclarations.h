﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.IOSNative.Contacts.ContactStore
struct ContactStore_t160827595;
// System.Action`1<SA.IOSNative.Contacts.ContactsResult>
struct Action_1_t847530666;
// SA.IOSNative.Contacts.Contact
struct Contact_t4178394798;
// System.String
struct String_t;
// System.Collections.Generic.List`1<SA.IOSNative.Contacts.Contact>
struct List_1_t3547515930;
// SA.IOSNative.Contacts.ContactsResult
struct ContactsResult_t1045731284;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contact1045731284.h"

// System.Void SA.IOSNative.Contacts.ContactStore::.ctor()
extern "C"  void ContactStore__ctor_m3006704629 (ContactStore_t160827595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::add_ContactsLoadResult(System.Action`1<SA.IOSNative.Contacts.ContactsResult>)
extern "C"  void ContactStore_add_ContactsLoadResult_m3108040362 (ContactStore_t160827595 * __this, Action_1_t847530666 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::remove_ContactsLoadResult(System.Action`1<SA.IOSNative.Contacts.ContactsResult>)
extern "C"  void ContactStore_remove_ContactsLoadResult_m3252485327 (ContactStore_t160827595 * __this, Action_1_t847530666 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::add_ContactsPickResult(System.Action`1<SA.IOSNative.Contacts.ContactsResult>)
extern "C"  void ContactStore_add_ContactsPickResult_m904606315 (ContactStore_t160827595 * __this, Action_1_t847530666 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::remove_ContactsPickResult(System.Action`1<SA.IOSNative.Contacts.ContactsResult>)
extern "C"  void ContactStore_remove_ContactsPickResult_m2678985818 (ContactStore_t160827595 * __this, Action_1_t847530666 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::Awake()
extern "C"  void ContactStore_Awake_m599978168 (ContactStore_t160827595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::ShowContactsPickerUI(System.Action`1<SA.IOSNative.Contacts.ContactsResult>)
extern "C"  void ContactStore_ShowContactsPickerUI_m3091324270 (ContactStore_t160827595 * __this, Action_1_t847530666 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::RetrievePhoneContacts(System.Action`1<SA.IOSNative.Contacts.ContactsResult>)
extern "C"  void ContactStore_RetrievePhoneContacts_m1324477995 (ContactStore_t160827595 * __this, Action_1_t847530666 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SA.IOSNative.Contacts.Contact SA.IOSNative.Contacts.ContactStore::ParseContactData(System.String)
extern "C"  Contact_t4178394798 * ContactStore_ParseContactData_m2938065374 (ContactStore_t160827595 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<SA.IOSNative.Contacts.Contact> SA.IOSNative.Contacts.ContactStore::ParseContactArray(System.String)
extern "C"  List_1_t3547515930 * ContactStore_ParseContactArray_m1185376369 (ContactStore_t160827595 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::OnContactPickerDidCancel(System.String)
extern "C"  void ContactStore_OnContactPickerDidCancel_m2845941731 (ContactStore_t160827595 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::OnPickerDidSelectContacts(System.String)
extern "C"  void ContactStore_OnPickerDidSelectContacts_m1476770768 (ContactStore_t160827595 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::OnContactsRetrieveFailed(System.String)
extern "C"  void ContactStore_OnContactsRetrieveFailed_m107372548 (ContactStore_t160827595 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::OnContactsRetrieveFinished(System.String)
extern "C"  void ContactStore_OnContactsRetrieveFinished_m3883776513 (ContactStore_t160827595 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::<ContactsLoadResult>m__5D(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void ContactStore_U3CContactsLoadResultU3Em__5D_m1939839080 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::<ContactsPickResult>m__5E(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void ContactStore_U3CContactsPickResultU3Em__5E_m1450293290 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::<OnContactPickerDidCancel>m__5F(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void ContactStore_U3COnContactPickerDidCancelU3Em__5F_m3804620746 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::<OnPickerDidSelectContacts>m__60(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void ContactStore_U3COnPickerDidSelectContactsU3Em__60_m1357108638 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::<OnContactsRetrieveFailed>m__61(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void ContactStore_U3COnContactsRetrieveFailedU3Em__61_m3095485511 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactStore::<OnContactsRetrieveFinished>m__62(SA.IOSNative.Contacts.ContactsResult)
extern "C"  void ContactStore_U3COnContactsRetrieveFinishedU3Em__62_m3905779813 (Il2CppObject * __this /* static, unused */, ContactsResult_t1045731284 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
