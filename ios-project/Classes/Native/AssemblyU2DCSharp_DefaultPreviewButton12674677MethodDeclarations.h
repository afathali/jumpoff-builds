﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefaultPreviewButton
struct DefaultPreviewButton_t12674677;
// System.Action
struct Action_t3226471752;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DefaultPreviewButton::.ctor()
extern "C"  void DefaultPreviewButton__ctor_m289299190 (DefaultPreviewButton_t12674677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultPreviewButton::add_ActionClick(System.Action)
extern "C"  void DefaultPreviewButton_add_ActionClick_m200718951 (DefaultPreviewButton_t12674677 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultPreviewButton::remove_ActionClick(System.Action)
extern "C"  void DefaultPreviewButton_remove_ActionClick_m1135670636 (DefaultPreviewButton_t12674677 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultPreviewButton::Awake()
extern "C"  void DefaultPreviewButton_Awake_m917988529 (DefaultPreviewButton_t12674677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultPreviewButton::Select()
extern "C"  void DefaultPreviewButton_Select_m2512533852 (DefaultPreviewButton_t12674677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultPreviewButton::Unselect()
extern "C"  void DefaultPreviewButton_Unselect_m963974975 (DefaultPreviewButton_t12674677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultPreviewButton::DisabledButton()
extern "C"  void DefaultPreviewButton_DisabledButton_m2581367488 (DefaultPreviewButton_t12674677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultPreviewButton::EnabledButton()
extern "C"  void DefaultPreviewButton_EnabledButton_m1221746635 (DefaultPreviewButton_t12674677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DefaultPreviewButton::get_text()
extern "C"  String_t* DefaultPreviewButton_get_text_m3115078185 (DefaultPreviewButton_t12674677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultPreviewButton::set_text(System.String)
extern "C"  void DefaultPreviewButton_set_text_m3342183024 (DefaultPreviewButton_t12674677 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultPreviewButton::Update()
extern "C"  void DefaultPreviewButton_Update_m45909141 (DefaultPreviewButton_t12674677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultPreviewButton::OnClick()
extern "C"  void DefaultPreviewButton_OnClick_m3938583795 (DefaultPreviewButton_t12674677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultPreviewButton::OnTimeoutPress()
extern "C"  void DefaultPreviewButton_OnTimeoutPress_m1645112869 (DefaultPreviewButton_t12674677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultPreviewButton::<ActionClick>m__B6()
extern "C"  void DefaultPreviewButton_U3CActionClickU3Em__B6_m1260062955 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
