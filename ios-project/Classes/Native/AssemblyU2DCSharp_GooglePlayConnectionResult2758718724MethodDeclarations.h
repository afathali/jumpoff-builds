﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayConnectionResult
struct GooglePlayConnectionResult_t2758718724;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayConnectionResult::.ctor()
extern "C"  void GooglePlayConnectionResult__ctor_m982736069 (GooglePlayConnectionResult_t2758718724 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayConnectionResult::get_IsSuccess()
extern "C"  bool GooglePlayConnectionResult_get_IsSuccess_m1348003141 (GooglePlayConnectionResult_t2758718724 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
