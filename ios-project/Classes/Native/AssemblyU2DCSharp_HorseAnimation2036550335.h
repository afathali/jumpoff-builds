﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HorseAnimation
struct  HorseAnimation_t2036550335  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean HorseAnimation::animate
	bool ___animate_3;
	// System.Single HorseAnimation::headTurn
	float ___headTurn_4;
	// System.Single HorseAnimation::bodyMovementScale
	float ___bodyMovementScale_5;
	// System.Single HorseAnimation::headMovementScale
	float ___headMovementScale_6;
	// System.Single HorseAnimation::movementSpeed
	float ___movementSpeed_7;
	// System.Single HorseAnimation::yMovementScale
	float ___yMovementScale_8;
	// UnityEngine.Transform HorseAnimation::bodyBone
	Transform_t3275118058 * ___bodyBone_9;
	// UnityEngine.Transform HorseAnimation::neckBone
	Transform_t3275118058 * ___neckBone_10;
	// UnityEngine.Transform HorseAnimation::headBone
	Transform_t3275118058 * ___headBone_11;
	// System.Single HorseAnimation::startNeck
	float ___startNeck_12;
	// System.Single HorseAnimation::startHead
	float ___startHead_13;
	// System.Single HorseAnimation::startBody
	float ___startBody_14;
	// UnityEngine.Vector3 HorseAnimation::startBodyBonePos
	Vector3_t2243707580  ___startBodyBonePos_15;
	// System.Single HorseAnimation::delta
	float ___delta_16;
	// System.Single HorseAnimation::currentNeckRot
	float ___currentNeckRot_17;
	// System.Boolean HorseAnimation::jumping
	bool ___jumping_18;
	// System.Single HorseAnimation::jumpTime
	float ___jumpTime_19;

public:
	inline static int32_t get_offset_of_animate_3() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___animate_3)); }
	inline bool get_animate_3() const { return ___animate_3; }
	inline bool* get_address_of_animate_3() { return &___animate_3; }
	inline void set_animate_3(bool value)
	{
		___animate_3 = value;
	}

	inline static int32_t get_offset_of_headTurn_4() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___headTurn_4)); }
	inline float get_headTurn_4() const { return ___headTurn_4; }
	inline float* get_address_of_headTurn_4() { return &___headTurn_4; }
	inline void set_headTurn_4(float value)
	{
		___headTurn_4 = value;
	}

	inline static int32_t get_offset_of_bodyMovementScale_5() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___bodyMovementScale_5)); }
	inline float get_bodyMovementScale_5() const { return ___bodyMovementScale_5; }
	inline float* get_address_of_bodyMovementScale_5() { return &___bodyMovementScale_5; }
	inline void set_bodyMovementScale_5(float value)
	{
		___bodyMovementScale_5 = value;
	}

	inline static int32_t get_offset_of_headMovementScale_6() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___headMovementScale_6)); }
	inline float get_headMovementScale_6() const { return ___headMovementScale_6; }
	inline float* get_address_of_headMovementScale_6() { return &___headMovementScale_6; }
	inline void set_headMovementScale_6(float value)
	{
		___headMovementScale_6 = value;
	}

	inline static int32_t get_offset_of_movementSpeed_7() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___movementSpeed_7)); }
	inline float get_movementSpeed_7() const { return ___movementSpeed_7; }
	inline float* get_address_of_movementSpeed_7() { return &___movementSpeed_7; }
	inline void set_movementSpeed_7(float value)
	{
		___movementSpeed_7 = value;
	}

	inline static int32_t get_offset_of_yMovementScale_8() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___yMovementScale_8)); }
	inline float get_yMovementScale_8() const { return ___yMovementScale_8; }
	inline float* get_address_of_yMovementScale_8() { return &___yMovementScale_8; }
	inline void set_yMovementScale_8(float value)
	{
		___yMovementScale_8 = value;
	}

	inline static int32_t get_offset_of_bodyBone_9() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___bodyBone_9)); }
	inline Transform_t3275118058 * get_bodyBone_9() const { return ___bodyBone_9; }
	inline Transform_t3275118058 ** get_address_of_bodyBone_9() { return &___bodyBone_9; }
	inline void set_bodyBone_9(Transform_t3275118058 * value)
	{
		___bodyBone_9 = value;
		Il2CppCodeGenWriteBarrier(&___bodyBone_9, value);
	}

	inline static int32_t get_offset_of_neckBone_10() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___neckBone_10)); }
	inline Transform_t3275118058 * get_neckBone_10() const { return ___neckBone_10; }
	inline Transform_t3275118058 ** get_address_of_neckBone_10() { return &___neckBone_10; }
	inline void set_neckBone_10(Transform_t3275118058 * value)
	{
		___neckBone_10 = value;
		Il2CppCodeGenWriteBarrier(&___neckBone_10, value);
	}

	inline static int32_t get_offset_of_headBone_11() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___headBone_11)); }
	inline Transform_t3275118058 * get_headBone_11() const { return ___headBone_11; }
	inline Transform_t3275118058 ** get_address_of_headBone_11() { return &___headBone_11; }
	inline void set_headBone_11(Transform_t3275118058 * value)
	{
		___headBone_11 = value;
		Il2CppCodeGenWriteBarrier(&___headBone_11, value);
	}

	inline static int32_t get_offset_of_startNeck_12() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___startNeck_12)); }
	inline float get_startNeck_12() const { return ___startNeck_12; }
	inline float* get_address_of_startNeck_12() { return &___startNeck_12; }
	inline void set_startNeck_12(float value)
	{
		___startNeck_12 = value;
	}

	inline static int32_t get_offset_of_startHead_13() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___startHead_13)); }
	inline float get_startHead_13() const { return ___startHead_13; }
	inline float* get_address_of_startHead_13() { return &___startHead_13; }
	inline void set_startHead_13(float value)
	{
		___startHead_13 = value;
	}

	inline static int32_t get_offset_of_startBody_14() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___startBody_14)); }
	inline float get_startBody_14() const { return ___startBody_14; }
	inline float* get_address_of_startBody_14() { return &___startBody_14; }
	inline void set_startBody_14(float value)
	{
		___startBody_14 = value;
	}

	inline static int32_t get_offset_of_startBodyBonePos_15() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___startBodyBonePos_15)); }
	inline Vector3_t2243707580  get_startBodyBonePos_15() const { return ___startBodyBonePos_15; }
	inline Vector3_t2243707580 * get_address_of_startBodyBonePos_15() { return &___startBodyBonePos_15; }
	inline void set_startBodyBonePos_15(Vector3_t2243707580  value)
	{
		___startBodyBonePos_15 = value;
	}

	inline static int32_t get_offset_of_delta_16() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___delta_16)); }
	inline float get_delta_16() const { return ___delta_16; }
	inline float* get_address_of_delta_16() { return &___delta_16; }
	inline void set_delta_16(float value)
	{
		___delta_16 = value;
	}

	inline static int32_t get_offset_of_currentNeckRot_17() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___currentNeckRot_17)); }
	inline float get_currentNeckRot_17() const { return ___currentNeckRot_17; }
	inline float* get_address_of_currentNeckRot_17() { return &___currentNeckRot_17; }
	inline void set_currentNeckRot_17(float value)
	{
		___currentNeckRot_17 = value;
	}

	inline static int32_t get_offset_of_jumping_18() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___jumping_18)); }
	inline bool get_jumping_18() const { return ___jumping_18; }
	inline bool* get_address_of_jumping_18() { return &___jumping_18; }
	inline void set_jumping_18(bool value)
	{
		___jumping_18 = value;
	}

	inline static int32_t get_offset_of_jumpTime_19() { return static_cast<int32_t>(offsetof(HorseAnimation_t2036550335, ___jumpTime_19)); }
	inline float get_jumpTime_19() const { return ___jumpTime_19; }
	inline float* get_address_of_jumpTime_19() { return &___jumpTime_19; }
	inline void set_jumpTime_19(float value)
	{
		___jumpTime_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
