﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator11
struct U3CGetEnumeratorU3Ec__Iterator11_t2136724618;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22224768497.h"

// System.Void Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator11::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator11__ctor_m2963067169 (U3CGetEnumeratorU3Ec__Iterator11_t2136724618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator11::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.get_Current()
extern "C"  KeyValuePair_2_t2224768497  U3CGetEnumeratorU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_get_Current_m827543397 (U3CGetEnumeratorU3Ec__Iterator11_t2136724618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m4240240211 (U3CGetEnumeratorU3Ec__Iterator11_t2136724618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator11::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator11_MoveNext_m3470023471 (U3CGetEnumeratorU3Ec__Iterator11_t2136724618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator11::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator11_Dispose_m1341352932 (U3CGetEnumeratorU3Ec__Iterator11_t2136724618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject/<GetEnumerator>c__Iterator11::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator11_Reset_m910138550 (U3CGetEnumeratorU3Ec__Iterator11_t2136724618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
