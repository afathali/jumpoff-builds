﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayUtils
struct GooglePlayUtils_t968630402;
// System.String
struct String_t;
// GP_AdvertisingIdLoadResult
struct GP_AdvertisingIdLoadResult_t2783375090;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_AdvertisingIdLoadResult2783375090.h"

// System.Void GooglePlayUtils::.ctor()
extern "C"  void GooglePlayUtils__ctor_m121274941 (GooglePlayUtils_t968630402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayUtils::.cctor()
extern "C"  void GooglePlayUtils__cctor_m3882777452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayUtils::Awake()
extern "C"  void GooglePlayUtils_Awake_m2818302622 (GooglePlayUtils_t968630402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayUtils::GetAdvertisingId()
extern "C"  void GooglePlayUtils_GetAdvertisingId_m689748730 (GooglePlayUtils_t968630402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayUtils::OnAdvertisingIdLoaded(System.String)
extern "C"  void GooglePlayUtils_OnAdvertisingIdLoaded_m1396036462 (GooglePlayUtils_t968630402 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayUtils::<ActionAdvertisingIdLoaded>m__7C(GP_AdvertisingIdLoadResult)
extern "C"  void GooglePlayUtils_U3CActionAdvertisingIdLoadedU3Em__7C_m2042488318 (Il2CppObject * __this /* static, unused */, GP_AdvertisingIdLoadResult_t2783375090 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
