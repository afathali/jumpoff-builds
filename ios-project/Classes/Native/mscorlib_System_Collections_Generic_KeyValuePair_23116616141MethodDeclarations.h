﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,TweetTemplate>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2930822436(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3116616141 *, String_t*, TweetTemplate_t3444491657 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,TweetTemplate>::get_Key()
#define KeyValuePair_2_get_Key_m980060602(__this, method) ((  String_t* (*) (KeyValuePair_2_t3116616141 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,TweetTemplate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m27983641(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3116616141 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,TweetTemplate>::get_Value()
#define KeyValuePair_2_get_Value_m2198939706(__this, method) ((  TweetTemplate_t3444491657 * (*) (KeyValuePair_2_t3116616141 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,TweetTemplate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2546388785(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3116616141 *, TweetTemplate_t3444491657 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,TweetTemplate>::ToString()
#define KeyValuePair_2_ToString_m4215372479(__this, method) ((  String_t* (*) (KeyValuePair_2_t3116616141 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
