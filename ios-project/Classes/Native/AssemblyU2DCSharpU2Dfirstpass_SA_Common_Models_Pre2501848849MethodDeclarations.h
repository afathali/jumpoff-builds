﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Models.PrefabAsyncLoader/<Load>c__IteratorA
struct U3CLoadU3Ec__IteratorA_t2501848849;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.Common.Models.PrefabAsyncLoader/<Load>c__IteratorA::.ctor()
extern "C"  void U3CLoadU3Ec__IteratorA__ctor_m2157395730 (U3CLoadU3Ec__IteratorA_t2501848849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Models.PrefabAsyncLoader/<Load>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2376973936 (U3CLoadU3Ec__IteratorA_t2501848849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Models.PrefabAsyncLoader/<Load>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3281309768 (U3CLoadU3Ec__IteratorA_t2501848849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA.Common.Models.PrefabAsyncLoader/<Load>c__IteratorA::MoveNext()
extern "C"  bool U3CLoadU3Ec__IteratorA_MoveNext_m3138230922 (U3CLoadU3Ec__IteratorA_t2501848849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.PrefabAsyncLoader/<Load>c__IteratorA::Dispose()
extern "C"  void U3CLoadU3Ec__IteratorA_Dispose_m3715710513 (U3CLoadU3Ec__IteratorA_t2501848849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.PrefabAsyncLoader/<Load>c__IteratorA::Reset()
extern "C"  void U3CLoadU3Ec__IteratorA_Reset_m2684136031 (U3CLoadU3Ec__IteratorA_t2501848849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
