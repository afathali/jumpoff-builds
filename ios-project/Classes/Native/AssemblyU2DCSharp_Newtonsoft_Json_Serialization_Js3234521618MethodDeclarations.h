﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct JsonSerializerInternalWriter_t3234521618;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t1719617802;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t1973729997;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t27144642;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1566984540;
// Newtonsoft.Json.Serialization.JsonPrimitiveContract
struct JsonPrimitiveContract_t2604782005;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t2712067825;
// System.Type
struct Type_t;
// System.String
struct String_t;
// Newtonsoft.Json.Serialization.JsonStringContract
struct JsonStringContract_t2979008531;
// Newtonsoft.Json.Serialization.JsonObjectContract
struct JsonObjectContract_t2091736265;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t1964060750;
// Newtonsoft.Json.Utilities.IWrappedCollection
struct IWrappedCollection_t1324248768;
// Newtonsoft.Json.Serialization.JsonArrayContract
struct JsonArrayContract_t2625589241;
// System.Array
struct Il2CppArray;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Runtime.Serialization.ISerializable
struct ISerializable_t1245643778;
// Newtonsoft.Json.Serialization.JsonISerializableContract
struct JsonISerializableContract_t122701872;
// Newtonsoft.Json.Utilities.IWrappedDictionary
struct IWrappedDictionary_t1593229298;
// Newtonsoft.Json.Serialization.JsonDictionaryContract
struct JsonDictionaryContract_t3573211912;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer1719617802.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter1973729997.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2604782005.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2712067825.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js1566984540.h"
#include "mscorlib_System_Nullable_1_gen3575889505.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2979008531.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2091736265.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_DefaultValueHand3457895463.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_PreserveReferenc3019117943.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_TypeNameHandling1331513094.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter1964060750.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2625589241.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso122701872.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js3573211912.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::.ctor(Newtonsoft.Json.JsonSerializer)
extern "C"  void JsonSerializerInternalWriter__ctor_m4288492299 (JsonSerializerInternalWriter_t3234521618 * __this, JsonSerializer_t1719617802 * ___serializer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::get_SerializeStack()
extern "C"  List_1_t2058570427 * JsonSerializerInternalWriter_get_SerializeStack_m1131748742 (JsonSerializerInternalWriter_t3234521618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::Serialize(Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializerInternalWriter_Serialize_m3465221450 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___jsonWriter0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetInternalSerializer()
extern "C"  JsonSerializerProxy_t27144642 * JsonSerializerInternalWriter_GetInternalSerializer_m2812915465 (JsonSerializerInternalWriter_t3234521618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetContractSafe(System.Object)
extern "C"  JsonContract_t1566984540 * JsonSerializerInternalWriter_GetContractSafe_m2449289265 (JsonSerializerInternalWriter_t3234521618 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializePrimitive(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonPrimitiveContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_SerializePrimitive_m2447562720 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, JsonPrimitiveContract_t2604782005 * ___contract2, JsonProperty_t2712067825 * ___member3, JsonContract_t1566984540 * ___collectionValueContract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeValue(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_SerializeValue_m2995572475 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, JsonContract_t1566984540 * ___valueContract2, JsonProperty_t2712067825 * ___member3, JsonContract_t1566984540 * ___collectionValueContract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteReference(System.Object,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  bool JsonSerializerInternalWriter_ShouldWriteReference_m3792639214 (JsonSerializerInternalWriter_t3234521618 * __this, Il2CppObject * ___value0, JsonProperty_t2712067825 * ___property1, JsonContract_t1566984540 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteMemberInfoProperty(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_WriteMemberInfoProperty_m4253996563 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___memberValue1, JsonProperty_t2712067825 * ___property2, JsonContract_t1566984540 * ___contract3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::CheckForCircularReference(System.Object,System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  bool JsonSerializerInternalWriter_CheckForCircularReference_m1004153192 (JsonSerializerInternalWriter_t3234521618 * __this, Il2CppObject * ___value0, Nullable_1_t3575889505  ___referenceLoopHandling1, JsonContract_t1566984540 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteReference(Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializerInternalWriter_WriteReference_m4034124646 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::TryConvertToString(System.Object,System.Type,System.String&)
extern "C"  bool JsonSerializerInternalWriter_TryConvertToString_m3962448071 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___type1, String_t** ___s2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeString(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonStringContract)
extern "C"  void JsonSerializerInternalWriter_SerializeString_m28131561 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, JsonStringContract_t2979008531 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeObject(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_SerializeObject_m1806822752 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, JsonObjectContract_t2091736265 * ___contract2, JsonProperty_t2712067825 * ___member3, JsonContract_t1566984540 * ___collectionValueContract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteTypeProperty(Newtonsoft.Json.JsonWriter,System.Type)
extern "C"  void JsonSerializerInternalWriter_WriteTypeProperty_m2294044141 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern "C"  bool JsonSerializerInternalWriter_HasFlag_m2756302136 (JsonSerializerInternalWriter_t3234521618 * __this, int32_t ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.PreserveReferencesHandling,Newtonsoft.Json.PreserveReferencesHandling)
extern "C"  bool JsonSerializerInternalWriter_HasFlag_m3366473992 (JsonSerializerInternalWriter_t3234521618 * __this, int32_t ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.TypeNameHandling,Newtonsoft.Json.TypeNameHandling)
extern "C"  bool JsonSerializerInternalWriter_HasFlag_m3747903170 (JsonSerializerInternalWriter_t3234521618 * __this, int32_t ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeConvertable(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter,System.Object,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_SerializeConvertable_m3292759319 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, JsonConverter_t1964060750 * ___converter1, Il2CppObject * ___value2, JsonContract_t1566984540 * ___contract3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeList(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Utilities.IWrappedCollection,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_SerializeList_m1635736370 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___values1, JsonArrayContract_t2625589241 * ___contract2, JsonProperty_t2712067825 * ___member3, JsonContract_t1566984540 * ___collectionValueContract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeMultidimensionalArray(Newtonsoft.Json.JsonWriter,System.Array,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_SerializeMultidimensionalArray_m4209344334 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppArray * ___values1, JsonArrayContract_t2625589241 * ___contract2, JsonProperty_t2712067825 * ___member3, JsonContract_t1566984540 * ___collectionContract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeMultidimensionalArray(Newtonsoft.Json.JsonWriter,System.Array,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,System.Int32,System.Int32[])
extern "C"  void JsonSerializerInternalWriter_SerializeMultidimensionalArray_m2420044611 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppArray * ___values1, JsonArrayContract_t2625589241 * ___contract2, JsonProperty_t2712067825 * ___member3, int32_t ___initialDepth4, Int32U5BU5D_t3030399641* ___indices5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetReference(Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  String_t* JsonSerializerInternalWriter_GetReference_m4166494878 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteStartArray(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  bool JsonSerializerInternalWriter_WriteStartArray_m512211751 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___values1, JsonArrayContract_t2625589241 * ___contract2, JsonProperty_t2712067825 * ___member3, JsonContract_t1566984540 * ___containerContract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeISerializable(Newtonsoft.Json.JsonWriter,System.Runtime.Serialization.ISerializable,Newtonsoft.Json.Serialization.JsonISerializableContract)
extern "C"  void JsonSerializerInternalWriter_SerializeISerializable_m3811514826 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, JsonISerializableContract_t122701872 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteType(Newtonsoft.Json.TypeNameHandling,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  bool JsonSerializerInternalWriter_ShouldWriteType_m3494429155 (JsonSerializerInternalWriter_t3234521618 * __this, int32_t ___typeNameHandlingFlag0, JsonContract_t1566984540 * ___contract1, JsonProperty_t2712067825 * ___member2, JsonContract_t1566984540 * ___collectionValueContract3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeDictionary(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Utilities.IWrappedDictionary,Newtonsoft.Json.Serialization.JsonDictionaryContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_SerializeDictionary_m4116623825 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___values1, JsonDictionaryContract_t3573211912 * ___contract2, JsonProperty_t2712067825 * ___member3, JsonContract_t1566984540 * ___collectionValueContract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetPropertyName(System.Collections.DictionaryEntry)
extern "C"  String_t* JsonSerializerInternalWriter_GetPropertyName_m1122778599 (JsonSerializerInternalWriter_t3234521618 * __this, DictionaryEntry_t3048875398  ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HandleError(Newtonsoft.Json.JsonWriter,System.Int32)
extern "C"  void JsonSerializerInternalWriter_HandleError_m1570084043 (JsonSerializerInternalWriter_t3234521618 * __this, JsonWriter_t1973729997 * ___writer0, int32_t ___initialDepth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldSerialize(Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  bool JsonSerializerInternalWriter_ShouldSerialize_m3329204037 (JsonSerializerInternalWriter_t3234521618 * __this, JsonProperty_t2712067825 * ___property0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::IsSpecified(Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  bool JsonSerializerInternalWriter_IsSpecified_m436982152 (JsonSerializerInternalWriter_t3234521618 * __this, JsonProperty_t2712067825 * ___property0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
