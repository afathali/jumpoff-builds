﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread4023353063MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m1746157593(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t688037467 *, Func_2_t3785155881 *, const MethodInfo*))ThreadSafeStore_2__ctor_m2716591129_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type>::Get(TKey)
#define ThreadSafeStore_2_Get_m2940125517(__this, ___key0, method) ((  Type_t * (*) (ThreadSafeStore_2_t688037467 *, Type_t *, const MethodInfo*))ThreadSafeStore_2_Get_m1768706765_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m1071457049(__this, ___key0, method) ((  Type_t * (*) (ThreadSafeStore_2_t688037467 *, Type_t *, const MethodInfo*))ThreadSafeStore_2_AddValue_m4069531929_gshared)(__this, ___key0, method)
