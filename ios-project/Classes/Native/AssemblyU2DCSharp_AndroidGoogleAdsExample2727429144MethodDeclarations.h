﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidGoogleAdsExample
struct AndroidGoogleAdsExample_t2727429144;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AndroidGoogleAdsExample::.ctor()
extern "C"  void AndroidGoogleAdsExample__ctor_m577230547 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::Start()
extern "C"  void AndroidGoogleAdsExample_Start_m3421641503 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::StartInterstitialAd()
extern "C"  void AndroidGoogleAdsExample_StartInterstitialAd_m4045425176 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::LoadInterstitialAd()
extern "C"  void AndroidGoogleAdsExample_LoadInterstitialAd_m2574245036 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::ShowInterstitialAd()
extern "C"  void AndroidGoogleAdsExample_ShowInterstitialAd_m3905551875 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::CreateBannerCustomPos()
extern "C"  void AndroidGoogleAdsExample_CreateBannerCustomPos_m99035438 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::CreateBannerUpperLeft()
extern "C"  void AndroidGoogleAdsExample_CreateBannerUpperLeft_m838870308 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::CreateBannerUpperCneter()
extern "C"  void AndroidGoogleAdsExample_CreateBannerUpperCneter_m3781843134 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::CreateBannerBottomLeft()
extern "C"  void AndroidGoogleAdsExample_CreateBannerBottomLeft_m1164592631 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::CreateBannerBottomCenter()
extern "C"  void AndroidGoogleAdsExample_CreateBannerBottomCenter_m3668234527 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::CreateBannerBottomRight()
extern "C"  void AndroidGoogleAdsExample_CreateBannerBottomRight_m4281498822 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::B1Hide()
extern "C"  void AndroidGoogleAdsExample_B1Hide_m247118354 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::B1Show()
extern "C"  void AndroidGoogleAdsExample_B1Show_m3872479091 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::B1Refresh()
extern "C"  void AndroidGoogleAdsExample_B1Refresh_m3243727973 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::B1Destrouy()
extern "C"  void AndroidGoogleAdsExample_B1Destrouy_m3521527247 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::SmartTOP()
extern "C"  void AndroidGoogleAdsExample_SmartTOP_m3979921065 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::SmartBottom()
extern "C"  void AndroidGoogleAdsExample_SmartBottom_m2134091685 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::B2Hide()
extern "C"  void AndroidGoogleAdsExample_B2Hide_m247297907 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::B2Show()
extern "C"  void AndroidGoogleAdsExample_B2Show_m3872656338 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::B2Refresh()
extern "C"  void AndroidGoogleAdsExample_B2Refresh_m3249651176 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::B2Destrouy()
extern "C"  void AndroidGoogleAdsExample_B2Destrouy_m3717127854 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::ChnagePostToMiddle()
extern "C"  void AndroidGoogleAdsExample_ChnagePostToMiddle_m146027603 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::ChangePostRandom()
extern "C"  void AndroidGoogleAdsExample_ChangePostRandom_m1296852746 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::FixedUpdate()
extern "C"  void AndroidGoogleAdsExample_FixedUpdate_m1675774944 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::OnInterstisialsLoaded()
extern "C"  void AndroidGoogleAdsExample_OnInterstisialsLoaded_m933951463 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::OnInterstisialsOpen()
extern "C"  void AndroidGoogleAdsExample_OnInterstisialsOpen_m1937354010 (AndroidGoogleAdsExample_t2727429144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample::OnInAppRequest(System.String)
extern "C"  void AndroidGoogleAdsExample_OnInAppRequest_m3635442651 (AndroidGoogleAdsExample_t2727429144 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
