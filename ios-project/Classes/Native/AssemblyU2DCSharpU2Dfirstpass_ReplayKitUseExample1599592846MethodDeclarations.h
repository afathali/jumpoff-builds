﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayKitUseExample
struct ReplayKitUseExample_t1599592846;
// SA.Common.Models.Error
struct Error_t445207774;
// SA.Common.Models.Result
struct Result_t4287219743;
// ReplayKitVideoShareResult
struct ReplayKitVideoShareResult_t2762953534;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ReplayKitVideoShareR2762953534.h"

// System.Void ReplayKitUseExample::.ctor()
extern "C"  void ReplayKitUseExample__ctor_m3388084351 (ReplayKitUseExample_t1599592846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayKitUseExample::Awake()
extern "C"  void ReplayKitUseExample_Awake_m357482328 (ReplayKitUseExample_t1599592846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayKitUseExample::OnDestroy()
extern "C"  void ReplayKitUseExample_OnDestroy_m459196658 (ReplayKitUseExample_t1599592846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayKitUseExample::OnGUI()
extern "C"  void ReplayKitUseExample_OnGUI_m585974605 (ReplayKitUseExample_t1599592846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayKitUseExample::HandleActionRecordInterrupted(SA.Common.Models.Error)
extern "C"  void ReplayKitUseExample_HandleActionRecordInterrupted_m1431643101 (ReplayKitUseExample_t1599592846 * __this, Error_t445207774 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayKitUseExample::HandleActionRecordStoped(SA.Common.Models.Result)
extern "C"  void ReplayKitUseExample_HandleActionRecordStoped_m2630385471 (ReplayKitUseExample_t1599592846 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayKitUseExample::HandleActionShareDialogFinished(ReplayKitVideoShareResult)
extern "C"  void ReplayKitUseExample_HandleActionShareDialogFinished_m1330361442 (ReplayKitUseExample_t1599592846 * __this, ReplayKitVideoShareResult_t2762953534 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayKitUseExample::HandleActionRecordStarted(SA.Common.Models.Result)
extern "C"  void ReplayKitUseExample_HandleActionRecordStarted_m2708932351 (ReplayKitUseExample_t1599592846 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayKitUseExample::HandleActionRecorderDidChangeAvailability(System.Boolean)
extern "C"  void ReplayKitUseExample_HandleActionRecorderDidChangeAvailability_m2235840422 (ReplayKitUseExample_t1599592846 * __this, bool ___IsRecordingAvaliable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayKitUseExample::HandleActionRecordDiscard()
extern "C"  void ReplayKitUseExample_HandleActionRecordDiscard_m3430884534 (ReplayKitUseExample_t1599592846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
