﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ImageUploaderPageManager
struct ImageUploaderPageManager_t1025178313;

#include "codegen/il2cpp-codegen.h"

// System.Void ImageUploaderPageManager::.ctor()
extern "C"  void ImageUploaderPageManager__ctor_m3801215478 (ImageUploaderPageManager_t1025178313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploaderPageManager::Update()
extern "C"  void ImageUploaderPageManager_Update_m3731112525 (ImageUploaderPageManager_t1025178313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploaderPageManager::Start()
extern "C"  void ImageUploaderPageManager_Start_m3103610198 (ImageUploaderPageManager_t1025178313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploaderPageManager::SelectCrop()
extern "C"  void ImageUploaderPageManager_SelectCrop_m3775915766 (ImageUploaderPageManager_t1025178313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploaderPageManager::SelectDetails()
extern "C"  void ImageUploaderPageManager_SelectDetails_m3554520104 (ImageUploaderPageManager_t1025178313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploaderPageManager::<Start>m__138()
extern "C"  void ImageUploaderPageManager_U3CStartU3Em__138_m1360454309 (ImageUploaderPageManager_t1025178313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploaderPageManager::<Start>m__139()
extern "C"  void ImageUploaderPageManager_U3CStartU3Em__139_m1360454276 (ImageUploaderPageManager_t1025178313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUploaderPageManager::<Start>m__13A()
extern "C"  void ImageUploaderPageManager_U3CStartU3Em__13A_m1360452508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
