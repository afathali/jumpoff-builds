﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_PlusShareListener
struct AN_PlusShareListener_t257800803;
// AN_PlusShareListener/PlusShareCallback
struct PlusShareCallback_t2327980784;
// System.String
struct String_t;
// AN_PlusShareResult
struct AN_PlusShareResult_t3488688014;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AN_PlusShareListener_PlusShareCa2327980784.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AN_PlusShareResult3488688014.h"

// System.Void AN_PlusShareListener::.ctor()
extern "C"  void AN_PlusShareListener__ctor_m1945690782 (AN_PlusShareListener_t257800803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusShareListener::AttachBuilderCallback(AN_PlusShareListener/PlusShareCallback)
extern "C"  void AN_PlusShareListener_AttachBuilderCallback_m1858191665 (AN_PlusShareListener_t257800803 * __this, PlusShareCallback_t2327980784 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusShareListener::OnPlusShareCallback(System.String)
extern "C"  void AN_PlusShareListener_OnPlusShareCallback_m3140671551 (AN_PlusShareListener_t257800803 * __this, String_t* ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusShareListener::<builderCallback>m__7B(AN_PlusShareResult)
extern "C"  void AN_PlusShareListener_U3CbuilderCallbackU3Em__7B_m3717339564 (Il2CppObject * __this /* static, unused */, AN_PlusShareResult_t3488688014 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
