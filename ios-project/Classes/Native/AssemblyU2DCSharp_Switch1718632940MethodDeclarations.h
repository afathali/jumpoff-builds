﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Switch
struct Switch_t1718632940;
// Switch/ChangeCallback
struct ChangeCallback_t1172037850;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Switch_ChangeCallback1172037850.h"

// System.Void Switch::.ctor()
extern "C"  void Switch__ctor_m248524977 (Switch_t1718632940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Switch::AddListener(Switch/ChangeCallback)
extern "C"  void Switch_AddListener_m3653227996 (Switch_t1718632940 * __this, ChangeCallback_t1172037850 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Switch::Set(System.Boolean)
extern "C"  void Switch_Set_m1649742818 (Switch_t1718632940 * __this, bool ___newOn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Switch::Start()
extern "C"  void Switch_Start_m2682099129 (Switch_t1718632940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Switch::Update()
extern "C"  void Switch_Update_m722648058 (Switch_t1718632940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Switch::<Start>m__159()
extern "C"  void Switch_U3CStartU3Em__159_m4242365247 (Switch_t1718632940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
