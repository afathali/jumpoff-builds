﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidCamera
struct AndroidCamera_t1711267764;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.String
struct String_t;
// AndroidImagePickResult
struct AndroidImagePickResult_t1791733552;
// GallerySaveResult
struct GallerySaveResult_t1643856950;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AndroidImagePickResult1791733552.h"
#include "AssemblyU2DCSharp_GallerySaveResult1643856950.h"

// System.Void AndroidCamera::.ctor()
extern "C"  void AndroidCamera__ctor_m3923623945 (AndroidCamera_t1711267764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidCamera::.cctor()
extern "C"  void AndroidCamera__cctor_m1187402270 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidCamera::Awake()
extern "C"  void AndroidCamera_Awake_m1990167696 (AndroidCamera_t1711267764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidCamera::SaveImageToGalalry(UnityEngine.Texture2D,System.String)
extern "C"  void AndroidCamera_SaveImageToGalalry_m2728029792 (AndroidCamera_t1711267764 * __this, Texture2D_t3542995729 * ___image0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidCamera::SaveImageToGallery(UnityEngine.Texture2D,System.String)
extern "C"  void AndroidCamera_SaveImageToGallery_m851136290 (AndroidCamera_t1711267764 * __this, Texture2D_t3542995729 * ___image0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidCamera::SaveScreenshotToGallery(System.String)
extern "C"  void AndroidCamera_SaveScreenshotToGallery_m4212164121 (AndroidCamera_t1711267764 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidCamera::GetImageFromGallery()
extern "C"  void AndroidCamera_GetImageFromGallery_m1843134150 (AndroidCamera_t1711267764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidCamera::GetImageFromCamera()
extern "C"  void AndroidCamera_GetImageFromCamera_m2917491375 (AndroidCamera_t1711267764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidCamera::OnImagePickedEvent(System.String)
extern "C"  void AndroidCamera_OnImagePickedEvent_m1014769681 (AndroidCamera_t1711267764 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidCamera::OnImageSavedEvent(System.String)
extern "C"  void AndroidCamera_OnImageSavedEvent_m496340066 (AndroidCamera_t1711267764 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidCamera::OnImageSaveFailedEvent(System.String)
extern "C"  void AndroidCamera_OnImageSaveFailedEvent_m2259121579 (AndroidCamera_t1711267764 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidCamera::OnScreenshotReady(UnityEngine.Texture2D)
extern "C"  void AndroidCamera_OnScreenshotReady_m4061385405 (AndroidCamera_t1711267764 * __this, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidCamera::GetRandomString()
extern "C"  String_t* AndroidCamera_GetRandomString_m1894196024 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidCamera::<OnImagePicked>m__19(AndroidImagePickResult)
extern "C"  void AndroidCamera_U3COnImagePickedU3Em__19_m2295037252 (Il2CppObject * __this /* static, unused */, AndroidImagePickResult_t1791733552 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidCamera::<OnImageSaved>m__1A(GallerySaveResult)
extern "C"  void AndroidCamera_U3COnImageSavedU3Em__1A_m3168547409 (Il2CppObject * __this /* static, unused */, GallerySaveResult_t1643856950 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
