﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<FB_ProfileImageSize>
struct DefaultComparer_t330290995;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FB_ProfileImageSize3003328130.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<FB_ProfileImageSize>::.ctor()
extern "C"  void DefaultComparer__ctor_m3424372168_gshared (DefaultComparer_t330290995 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3424372168(__this, method) ((  void (*) (DefaultComparer_t330290995 *, const MethodInfo*))DefaultComparer__ctor_m3424372168_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<FB_ProfileImageSize>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1026324049_gshared (DefaultComparer_t330290995 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1026324049(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t330290995 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1026324049_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<FB_ProfileImageSize>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2731152809_gshared (DefaultComparer_t330290995 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2731152809(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t330290995 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m2731152809_gshared)(__this, ___x0, ___y1, method)
