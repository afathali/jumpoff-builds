﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GADBannerIdFactory
struct GADBannerIdFactory_t603853557;

#include "codegen/il2cpp-codegen.h"

// System.Void GADBannerIdFactory::.ctor()
extern "C"  void GADBannerIdFactory__ctor_m1676588882 (GADBannerIdFactory_t603853557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GADBannerIdFactory::.cctor()
extern "C"  void GADBannerIdFactory__cctor_m886868687 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GADBannerIdFactory::get_nextId()
extern "C"  int32_t GADBannerIdFactory_get_nextId_m224006057 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
