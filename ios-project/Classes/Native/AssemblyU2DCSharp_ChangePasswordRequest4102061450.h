﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Shared_Request3213492751.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChangePasswordRequest
struct  ChangePasswordRequest_t4102061450  : public Request_t3213492751
{
public:
	// System.String ChangePasswordRequest::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_2;
	// System.Int64 ChangePasswordRequest::<UserId>k__BackingField
	int64_t ___U3CUserIdU3Ek__BackingField_3;
	// System.String ChangePasswordRequest::<NewPassword>k__BackingField
	String_t* ___U3CNewPasswordU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ChangePasswordRequest_t4102061450, ___U3CTypeU3Ek__BackingField_2)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTypeU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ChangePasswordRequest_t4102061450, ___U3CUserIdU3Ek__BackingField_3)); }
	inline int64_t get_U3CUserIdU3Ek__BackingField_3() const { return ___U3CUserIdU3Ek__BackingField_3; }
	inline int64_t* get_address_of_U3CUserIdU3Ek__BackingField_3() { return &___U3CUserIdU3Ek__BackingField_3; }
	inline void set_U3CUserIdU3Ek__BackingField_3(int64_t value)
	{
		___U3CUserIdU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CNewPasswordU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ChangePasswordRequest_t4102061450, ___U3CNewPasswordU3Ek__BackingField_4)); }
	inline String_t* get_U3CNewPasswordU3Ek__BackingField_4() const { return ___U3CNewPasswordU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CNewPasswordU3Ek__BackingField_4() { return &___U3CNewPasswordU3Ek__BackingField_4; }
	inline void set_U3CNewPasswordU3Ek__BackingField_4(String_t* value)
	{
		___U3CNewPasswordU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNewPasswordU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
