﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey2C`1<System.Object>
struct U3CTryConvertOrCastU3Ec__AnonStorey2C_1_t283520851;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey2C`1<System.Object>::.ctor()
extern "C"  void U3CTryConvertOrCastU3Ec__AnonStorey2C_1__ctor_m3772060474_gshared (U3CTryConvertOrCastU3Ec__AnonStorey2C_1_t283520851 * __this, const MethodInfo* method);
#define U3CTryConvertOrCastU3Ec__AnonStorey2C_1__ctor_m3772060474(__this, method) ((  void (*) (U3CTryConvertOrCastU3Ec__AnonStorey2C_1_t283520851 *, const MethodInfo*))U3CTryConvertOrCastU3Ec__AnonStorey2C_1__ctor_m3772060474_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey2C`1<System.Object>::<>m__EE()
extern "C"  Il2CppObject * U3CTryConvertOrCastU3Ec__AnonStorey2C_1_U3CU3Em__EE_m208200466_gshared (U3CTryConvertOrCastU3Ec__AnonStorey2C_1_t283520851 * __this, const MethodInfo* method);
#define U3CTryConvertOrCastU3Ec__AnonStorey2C_1_U3CU3Em__EE_m208200466(__this, method) ((  Il2CppObject * (*) (U3CTryConvertOrCastU3Ec__AnonStorey2C_1_t283520851 *, const MethodInfo*))U3CTryConvertOrCastU3Ec__AnonStorey2C_1_U3CU3Em__EE_m208200466_gshared)(__this, method)
