﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_Device
struct ISN_Device_t2325020285;
// System.String
struct String_t;
// ISN_DeviceGUID
struct ISN_DeviceGUID_t1787531836;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_InterfaceIdiom3178386526.h"

// System.Void ISN_Device::.ctor()
extern "C"  void ISN_Device__ctor_m3614204570 (ISN_Device_t2325020285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Device::.ctor(System.String)
extern "C"  void ISN_Device__ctor_m1492311832 (ISN_Device_t2325020285 * __this, String_t* ___deviceData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Device::.cctor()
extern "C"  void ISN_Device__cctor_m1359907817 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_Device::_ISN_RetriveDeviceData()
extern "C"  String_t* ISN_Device__ISN_RetriveDeviceData_m2911944254 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_Device::get_Name()
extern "C"  String_t* ISN_Device_get_Name_m2435751201 (ISN_Device_t2325020285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_Device::get_SystemName()
extern "C"  String_t* ISN_Device_get_SystemName_m2873026142 (ISN_Device_t2325020285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_Device::get_Model()
extern "C"  String_t* ISN_Device_get_Model_m431142577 (ISN_Device_t2325020285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_Device::get_LocalizedModel()
extern "C"  String_t* ISN_Device_get_LocalizedModel_m1824929342 (ISN_Device_t2325020285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_Device::get_SystemVersion()
extern "C"  String_t* ISN_Device_get_SystemVersion_m3605922481 (ISN_Device_t2325020285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ISN_Device::get_MajorSystemVersion()
extern "C"  int32_t ISN_Device_get_MajorSystemVersion_m4001688973 (ISN_Device_t2325020285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ISN_InterfaceIdiom ISN_Device::get_InterfaceIdiom()
extern "C"  int32_t ISN_Device_get_InterfaceIdiom_m3215823531 (ISN_Device_t2325020285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ISN_DeviceGUID ISN_Device::get_GUID()
extern "C"  ISN_DeviceGUID_t1787531836 * ISN_Device_get_GUID_m336871087 (ISN_Device_t2325020285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ISN_Device ISN_Device::get_CurrentDevice()
extern "C"  ISN_Device_t2325020285 * ISN_Device_get_CurrentDevice_m3142029226 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
