﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_ReplayKit
struct ISN_ReplayKit_t2994976952;
// System.Action`1<SA.Common.Models.Result>
struct Action_1_t4089019125;
// System.Action`1<ReplayKitVideoShareResult>
struct Action_1_t2564752916;
// System.Action`1<SA.Common.Models.Error>
struct Action_1_t247007156;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action
struct Action_t3226471752;
// System.String
struct String_t;
// SA.Common.Models.Result
struct Result_t4287219743;
// ReplayKitVideoShareResult
struct ReplayKitVideoShareResult_t2762953534;
// SA.Common.Models.Error
struct Error_t445207774;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ReplayKitVideoShareR2762953534.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"

// System.Void ISN_ReplayKit::.ctor()
extern "C"  void ISN_ReplayKit__ctor_m4130066579 (ISN_ReplayKit_t2994976952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::.cctor()
extern "C"  void ISN_ReplayKit__cctor_m1118498840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::add_ActionRecordStarted(System.Action`1<SA.Common.Models.Result>)
extern "C"  void ISN_ReplayKit_add_ActionRecordStarted_m2292548981 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::remove_ActionRecordStarted(System.Action`1<SA.Common.Models.Result>)
extern "C"  void ISN_ReplayKit_remove_ActionRecordStarted_m3607330896 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::add_ActionRecordStoped(System.Action`1<SA.Common.Models.Result>)
extern "C"  void ISN_ReplayKit_add_ActionRecordStoped_m3409668061 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::remove_ActionRecordStoped(System.Action`1<SA.Common.Models.Result>)
extern "C"  void ISN_ReplayKit_remove_ActionRecordStoped_m2806912472 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::add_ActionShareDialogFinished(System.Action`1<ReplayKitVideoShareResult>)
extern "C"  void ISN_ReplayKit_add_ActionShareDialogFinished_m1649086130 (Il2CppObject * __this /* static, unused */, Action_1_t2564752916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::remove_ActionShareDialogFinished(System.Action`1<ReplayKitVideoShareResult>)
extern "C"  void ISN_ReplayKit_remove_ActionShareDialogFinished_m3984689773 (Il2CppObject * __this /* static, unused */, Action_1_t2564752916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::add_ActionRecordInterrupted(System.Action`1<SA.Common.Models.Error>)
extern "C"  void ISN_ReplayKit_add_ActionRecordInterrupted_m1874482119 (Il2CppObject * __this /* static, unused */, Action_1_t247007156 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::remove_ActionRecordInterrupted(System.Action`1<SA.Common.Models.Error>)
extern "C"  void ISN_ReplayKit_remove_ActionRecordInterrupted_m2804399034 (Il2CppObject * __this /* static, unused */, Action_1_t247007156 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::add_ActionRecorderDidChangeAvailability(System.Action`1<System.Boolean>)
extern "C"  void ISN_ReplayKit_add_ActionRecorderDidChangeAvailability_m528178726 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::remove_ActionRecorderDidChangeAvailability(System.Action`1<System.Boolean>)
extern "C"  void ISN_ReplayKit_remove_ActionRecorderDidChangeAvailability_m4147419245 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::add_ActionRecordDiscard(System.Action)
extern "C"  void ISN_ReplayKit_add_ActionRecordDiscard_m1638652477 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::remove_ActionRecordDiscard(System.Action)
extern "C"  void ISN_ReplayKit_remove_ActionRecordDiscard_m933862652 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::Awake()
extern "C"  void ISN_ReplayKit_Awake_m3935982474 (ISN_ReplayKit_t2994976952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::StartRecording(System.Boolean)
extern "C"  void ISN_ReplayKit_StartRecording_m3311129257 (ISN_ReplayKit_t2994976952 * __this, bool ___microphoneEnabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::StopRecording()
extern "C"  void ISN_ReplayKit_StopRecording_m3056786782 (ISN_ReplayKit_t2994976952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::DiscardRecording()
extern "C"  void ISN_ReplayKit_DiscardRecording_m3683709688 (ISN_ReplayKit_t2994976952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::ShowVideoShareDialog()
extern "C"  void ISN_ReplayKit_ShowVideoShareDialog_m878992610 (ISN_ReplayKit_t2994976952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ISN_ReplayKit::get_IsRecording()
extern "C"  bool ISN_ReplayKit_get_IsRecording_m1707116081 (ISN_ReplayKit_t2994976952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ISN_ReplayKit::get_IsRecodingAvailableToShare()
extern "C"  bool ISN_ReplayKit_get_IsRecodingAvailableToShare_m43125220 (ISN_ReplayKit_t2994976952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ISN_ReplayKit::get_IsAvailable()
extern "C"  bool ISN_ReplayKit_get_IsAvailable_m1593564345 (ISN_ReplayKit_t2994976952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ISN_ReplayKit::get_IsMicEnabled()
extern "C"  bool ISN_ReplayKit_get_IsMicEnabled_m3811208466 (ISN_ReplayKit_t2994976952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::OnRecorStartSuccess(System.String)
extern "C"  void ISN_ReplayKit_OnRecorStartSuccess_m739821396 (ISN_ReplayKit_t2994976952 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::OnRecorStartFailed(System.String)
extern "C"  void ISN_ReplayKit_OnRecorStartFailed_m1484088502 (ISN_ReplayKit_t2994976952 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::OnRecorStopFailed(System.String)
extern "C"  void ISN_ReplayKit_OnRecorStopFailed_m296973558 (ISN_ReplayKit_t2994976952 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::OnRecorStopSuccess()
extern "C"  void ISN_ReplayKit_OnRecorStopSuccess_m2225635318 (ISN_ReplayKit_t2994976952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::OnRecordInterrupted(System.String)
extern "C"  void ISN_ReplayKit_OnRecordInterrupted_m2684397979 (ISN_ReplayKit_t2994976952 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::OnRecorderDidChangeAvailability(System.String)
extern "C"  void ISN_ReplayKit_OnRecorderDidChangeAvailability_m1280408936 (ISN_ReplayKit_t2994976952 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::OnSaveResult(System.String)
extern "C"  void ISN_ReplayKit_OnSaveResult_m744421282 (ISN_ReplayKit_t2994976952 * __this, String_t* ___sourcesData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::OnRecordDiscard(System.String)
extern "C"  void ISN_ReplayKit_OnRecordDiscard_m3359418039 (ISN_ReplayKit_t2994976952 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::<ActionRecordStarted>m__36(SA.Common.Models.Result)
extern "C"  void ISN_ReplayKit_U3CActionRecordStartedU3Em__36_m1648129447 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::<ActionRecordStoped>m__37(SA.Common.Models.Result)
extern "C"  void ISN_ReplayKit_U3CActionRecordStopedU3Em__37_m2336257188 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::<ActionShareDialogFinished>m__38(ReplayKitVideoShareResult)
extern "C"  void ISN_ReplayKit_U3CActionShareDialogFinishedU3Em__38_m3341107742 (Il2CppObject * __this /* static, unused */, ReplayKitVideoShareResult_t2762953534 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::<ActionRecordInterrupted>m__39(SA.Common.Models.Error)
extern "C"  void ISN_ReplayKit_U3CActionRecordInterruptedU3Em__39_m3562501174 (Il2CppObject * __this /* static, unused */, Error_t445207774 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::<ActionRecorderDidChangeAvailability>m__3A(System.Boolean)
extern "C"  void ISN_ReplayKit_U3CActionRecorderDidChangeAvailabilityU3Em__3A_m3456542715 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_ReplayKit::<ActionRecordDiscard>m__3B()
extern "C"  void ISN_ReplayKit_U3CActionRecordDiscardU3Em__3B_m2870692924 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
