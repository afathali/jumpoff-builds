﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidNativeSettings
struct AndroidNativeSettings_t3397157021;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidNativeSettings::.ctor()
extern "C"  void AndroidNativeSettings__ctor_m1149630424 (AndroidNativeSettings_t3397157021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeSettings::.cctor()
extern "C"  void AndroidNativeSettings__cctor_m2783947623 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AndroidNativeSettings AndroidNativeSettings::get_Instance()
extern "C"  AndroidNativeSettings_t3397157021 * AndroidNativeSettings_get_Instance_m891384146 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidNativeSettings::get_IsBase64KeyWasReplaced()
extern "C"  bool AndroidNativeSettings_get_IsBase64KeyWasReplaced_m3340949810 (AndroidNativeSettings_t3397157021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
