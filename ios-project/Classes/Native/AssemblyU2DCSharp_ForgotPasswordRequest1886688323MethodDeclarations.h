﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ForgotPasswordRequest
struct ForgotPasswordRequest_t1886688323;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ForgotPasswordRequest::.ctor()
extern "C"  void ForgotPasswordRequest__ctor_m383578982 (ForgotPasswordRequest_t1886688323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ForgotPasswordRequest::get_Type()
extern "C"  String_t* ForgotPasswordRequest_get_Type_m3873314746 (ForgotPasswordRequest_t1886688323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ForgotPasswordRequest::set_Type(System.String)
extern "C"  void ForgotPasswordRequest_set_Type_m2518320157 (ForgotPasswordRequest_t1886688323 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ForgotPasswordRequest::get_Email()
extern "C"  String_t* ForgotPasswordRequest_get_Email_m1170744516 (ForgotPasswordRequest_t1886688323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ForgotPasswordRequest::set_Email(System.String)
extern "C"  void ForgotPasswordRequest_set_Email_m3322852105 (ForgotPasswordRequest_t1886688323 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
