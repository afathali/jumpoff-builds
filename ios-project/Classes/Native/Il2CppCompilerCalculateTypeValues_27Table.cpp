﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GP_AchievementResult3970926374.h"
#include "AssemblyU2DCSharp_GP_GCM_RegistrationResult2892492118.h"
#include "AssemblyU2DCSharp_GP_LeaderboardResult2034215294.h"
#include "AssemblyU2DCSharp_GP_QuestResult3390940437.h"
#include "AssemblyU2DCSharp_GP_ScoreResult381836467.h"
#include "AssemblyU2DCSharp_GP_SnapshotConflict4002586770.h"
#include "AssemblyU2DCSharp_GP_SpanshotLoadResult2263304449.h"
#include "AssemblyU2DCSharp_AN_InvitationInboxCloseResult1284128610.h"
#include "AssemblyU2DCSharp_GP_DeleteSnapshotResult3306790010.h"
#include "AssemblyU2DCSharp_GP_TBM_CancelMatchResult3879293152.h"
#include "AssemblyU2DCSharp_GP_TBM_LeaveMatchResult3657803719.h"
#include "AssemblyU2DCSharp_GP_TBM_LoadMatchResult1357418930.h"
#include "AssemblyU2DCSharp_GP_TBM_LoadMatchesResult841773038.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchInitiatedResult4144060847.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchReceivedResult2394672915.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchRemovedResult686355120.h"
#include "AssemblyU2DCSharp_GP_TBM_UpdateMatchResult3943005969.h"
#include "AssemblyU2DCSharp_GP_Snapshot1489095664.h"
#include "AssemblyU2DCSharp_GP_SnapshotMeta1354779439.h"
#include "AssemblyU2DCSharp_GP_TBM_Match1275077981.h"
#include "AssemblyU2DCSharp_ANMiniJSON_Json181042048.h"
#include "AssemblyU2DCSharp_ANMiniJSON_Json_Parser1231454700.h"
#include "AssemblyU2DCSharp_ANMiniJSON_Json_Parser_TOKEN2018455390.h"
#include "AssemblyU2DCSharp_ANMiniJSON_Json_Serializer3679305835.h"
#include "AssemblyU2DCSharp_PlayServiceUtil2802694711.h"
#include "AssemblyU2DCSharp_FB_AppRequestState1209389490.h"
#include "AssemblyU2DCSharp_FB_Gender2517284304.h"
#include "AssemblyU2DCSharp_FB_HttpMethod1543704700.h"
#include "AssemblyU2DCSharp_FB_PermissionStatus2970074352.h"
#include "AssemblyU2DCSharp_FB_RequestActionType2205998668.h"
#include "AssemblyU2DCSharp_SPFacebook1065228369.h"
#include "AssemblyU2DCSharp_SPFacebook_FB_Delegate3934797786.h"
#include "AssemblyU2DCSharp_SPFacebookAnalytics2144030727.h"
#include "AssemblyU2DCSharp_SP_FB_API_v61505850536.h"
#include "AssemblyU2DCSharp_SP_FB_API_v74234733891.h"
#include "AssemblyU2DCSharp_FB_AppRequest501312625.h"
#include "AssemblyU2DCSharp_FB_AppRequestResult2450670428.h"
#include "AssemblyU2DCSharp_FB_LikeInfo3213199078.h"
#include "AssemblyU2DCSharp_FB_Object1438399806.h"
#include "AssemblyU2DCSharp_FB_Permission1872772122.h"
#include "AssemblyU2DCSharp_FB_PermissionResult2322011743.h"
#include "AssemblyU2DCSharp_FB_ProfileImageSize3003328130.h"
#include "AssemblyU2DCSharp_FB_Score1450841581.h"
#include "AssemblyU2DCSharp_FB_UserInfo2704578078.h"
#include "AssemblyU2DCSharp_FB_LoginResult1848477279.h"
#include "AssemblyU2DCSharp_FB_PostResult992884730.h"
#include "AssemblyU2DCSharp_FB_Result838248372.h"
#include "AssemblyU2DCSharp_FB_LikesRetrieveTask2495592094.h"
#include "AssemblyU2DCSharp_FB_PostingTask2068490008.h"
#include "AssemblyU2DCSharp_InstagramEvents2237296277.h"
#include "AssemblyU2DCSharp_InstagramPostResult436083195.h"
#include "AssemblyU2DCSharp_AndroidInstagramManager2759207936.h"
#include "AssemblyU2DCSharp_AndroidTwitterManager3465455211.h"
#include "AssemblyU2DCSharp_TwitterApplicationOnlyToken3636409970.h"
#include "AssemblyU2DCSharp_TwitterApplicationOnlyToken_U3CL1598146762.h"
#include "AssemblyU2DCSharp_TwitterDataCash3686392272.h"
#include "AssemblyU2DCSharp_TWResult1480791060.h"
#include "AssemblyU2DCSharp_TW_APIRequstResult455151055.h"
#include "AssemblyU2DCSharp_TweetTemplate3444491657.h"
#include "AssemblyU2DCSharp_TwitterStatus2765035499.h"
#include "AssemblyU2DCSharp_TwitterUserInfo87370740.h"
#include "AssemblyU2DCSharp_TWAPITest1774637995.h"
#include "AssemblyU2DCSharp_TW_APIRequest675604147.h"
#include "AssemblyU2DCSharp_TW_APIRequest_U3CRequestU3Ec__It2019357771.h"
#include "AssemblyU2DCSharp_TW_FollowersIdsRequest261436078.h"
#include "AssemblyU2DCSharp_TW_FriendsIdsRequest151035516.h"
#include "AssemblyU2DCSharp_TW_OAuthAPIRequest3823919588.h"
#include "AssemblyU2DCSharp_TW_OAuthAPIRequest_U3CRequestU3E1902108118.h"
#include "AssemblyU2DCSharp_TW_SearchTweetsRequest525743233.h"
#include "AssemblyU2DCSharp_TW_UserTimeLineRequest1907229837.h"
#include "AssemblyU2DCSharp_TW_UsersLookUpRequest889831987.h"
#include "AssemblyU2DCSharp_TwitterPostingTask1522896362.h"
#include "AssemblyU2DCSharp_GPScore3219488889.h"
#include "AssemblyU2DCSharp_GooglePlayerTemplate2506317812.h"
#include "AssemblyU2DCSharp_SA_DataConverter_OLD3088030515.h"
#include "AssemblyU2DCSharp_SA_IdFactory_OLD1085491180.h"
#include "AssemblyU2DCSharp_SA_IdFactory_OLD_U3CU3Ec__AnonSt1266469818.h"
#include "AssemblyU2DCSharp_SA_ScreenShotMaker_OLD795198627.h"
#include "AssemblyU2DCSharp_SA_ScreenShotMaker_OLD_U3CSaveScr479799330.h"
#include "AssemblyU2DCSharp_SA_ModulesInfo3047057538.h"
#include "AssemblyU2DCSharp_WWWTextureLoader155872171.h"
#include "AssemblyU2DCSharp_WWWTextureLoader_U3CLoadCoroutin1223036233.h"
#include "AssemblyU2DCSharp_DefaultPreviewButton12674677.h"
#include "AssemblyU2DCSharp_SALevelLoader4125067327.h"
#include "AssemblyU2DCSharp_SALoadedSceneOnClick4265150718.h"
#include "AssemblyU2DCSharp_SAOnClickAction650504801.h"
#include "AssemblyU2DCSharp_SAOpenUrlOnClick1829835980.h"
#include "AssemblyU2DCSharp_SASendMessageOnClick243868668.h"
#include "AssemblyU2DCSharp_SA_BackButton1901565506.h"
#include "AssemblyU2DCSharp_SA_FriendUI3775341837.h"
#include "AssemblyU2DCSharp_SA_Label226960149.h"
#include "AssemblyU2DCSharp_SA_PartisipantUI3115332592.h"
#include "AssemblyU2DCSharp_SA_StatusBar1378080260.h"
#include "AssemblyU2DCSharp_SA_Texture3361108684.h"
#include "AssemblyU2DCSharp_PreviewScreenUtil19016308.h"
#include "AssemblyU2DCSharp_ScreenPlacement3340443251.h"
#include "AssemblyU2DCSharp_ScreenPosition2449785599.h"
#include "AssemblyU2DCSharp_ScreenPlacementExtension1575904396.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (GP_AchievementResult_t3970926374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[1] = 
{
	GP_AchievementResult_t3970926374::get_offset_of_achievementId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (GP_GCM_RegistrationResult_t2892492118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[1] = 
{
	GP_GCM_RegistrationResult_t2892492118::get_offset_of__RegistrationDeviceId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (GP_LeaderboardResult_t2034215294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[1] = 
{
	GP_LeaderboardResult_t2034215294::get_offset_of__Leaderboard_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (GP_QuestResult_t3390940437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[1] = 
{
	GP_QuestResult_t3390940437::get_offset_of_quest_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (GP_ScoreResult_t381836467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[1] = 
{
	GP_ScoreResult_t381836467::get_offset_of_score_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (GP_SnapshotConflict_t4002586770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[2] = 
{
	GP_SnapshotConflict_t4002586770::get_offset_of__s1_0(),
	GP_SnapshotConflict_t4002586770::get_offset_of__s2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (GP_SpanshotLoadResult_t2263304449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2706[1] = 
{
	GP_SpanshotLoadResult_t2263304449::get_offset_of__snapshot_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (AN_InvitationInboxCloseResult_t1284128610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[1] = 
{
	AN_InvitationInboxCloseResult_t1284128610::get_offset_of__resultCode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (GP_DeleteSnapshotResult_t3306790010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[1] = 
{
	GP_DeleteSnapshotResult_t3306790010::get_offset_of__SnapshotId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (GP_TBM_CancelMatchResult_t3879293152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[1] = 
{
	GP_TBM_CancelMatchResult_t3879293152::get_offset_of_MatchId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (GP_TBM_LeaveMatchResult_t3657803719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2710[1] = 
{
	GP_TBM_LeaveMatchResult_t3657803719::get_offset_of_MatchId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (GP_TBM_LoadMatchResult_t1357418930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[1] = 
{
	GP_TBM_LoadMatchResult_t1357418930::get_offset_of_Match_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (GP_TBM_LoadMatchesResult_t841773038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[1] = 
{
	GP_TBM_LoadMatchesResult_t841773038::get_offset_of_LoadedMatches_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (GP_TBM_MatchInitiatedResult_t4144060847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[1] = 
{
	GP_TBM_MatchInitiatedResult_t4144060847::get_offset_of_Match_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (GP_TBM_MatchReceivedResult_t2394672915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[1] = 
{
	GP_TBM_MatchReceivedResult_t2394672915::get_offset_of__Match_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (GP_TBM_MatchRemovedResult_t686355120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2715[1] = 
{
	GP_TBM_MatchRemovedResult_t686355120::get_offset_of__MatchId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (GP_TBM_UpdateMatchResult_t3943005969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2716[1] = 
{
	GP_TBM_UpdateMatchResult_t3943005969::get_offset_of_Match_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (GP_Snapshot_t1489095664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[3] = 
{
	GP_Snapshot_t1489095664::get_offset_of_meta_0(),
	GP_Snapshot_t1489095664::get_offset_of_bytes_1(),
	GP_Snapshot_t1489095664::get_offset_of_stringData_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (GP_SnapshotMeta_t1354779439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2718[5] = 
{
	GP_SnapshotMeta_t1354779439::get_offset_of_Title_0(),
	GP_SnapshotMeta_t1354779439::get_offset_of_Description_1(),
	GP_SnapshotMeta_t1354779439::get_offset_of_CoverImageUrl_2(),
	GP_SnapshotMeta_t1354779439::get_offset_of_LastModifiedTimestamp_3(),
	GP_SnapshotMeta_t1354779439::get_offset_of_TotalPlayedTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (GP_TBM_Match_t1275077981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[18] = 
{
	GP_TBM_Match_t1275077981::get_offset_of_Id_0(),
	GP_TBM_Match_t1275077981::get_offset_of_RematchId_1(),
	GP_TBM_Match_t1275077981::get_offset_of_CreatorId_2(),
	GP_TBM_Match_t1275077981::get_offset_of_LastUpdaterId_3(),
	GP_TBM_Match_t1275077981::get_offset_of_PendingParticipantId_4(),
	GP_TBM_Match_t1275077981::get_offset_of_MatchNumber_5(),
	GP_TBM_Match_t1275077981::get_offset_of_Description_6(),
	GP_TBM_Match_t1275077981::get_offset_of_AvailableAutoMatchSlots_7(),
	GP_TBM_Match_t1275077981::get_offset_of_CreationTimestamp_8(),
	GP_TBM_Match_t1275077981::get_offset_of_LastUpdatedTimestamp_9(),
	GP_TBM_Match_t1275077981::get_offset_of_Status_10(),
	GP_TBM_Match_t1275077981::get_offset_of_TurnStatus_11(),
	GP_TBM_Match_t1275077981::get_offset_of_CanRematch_12(),
	GP_TBM_Match_t1275077981::get_offset_of_Variant_13(),
	GP_TBM_Match_t1275077981::get_offset_of_Version_14(),
	GP_TBM_Match_t1275077981::get_offset_of_Data_15(),
	GP_TBM_Match_t1275077981::get_offset_of_PreviousMatchData_16(),
	GP_TBM_Match_t1275077981::get_offset_of_Participants_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (Json_t181042048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (Parser_t1231454700), -1, sizeof(Parser_t1231454700_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2721[4] = 
{
	0,
	0,
	Parser_t1231454700::get_offset_of_json_2(),
	Parser_t1231454700_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (TOKEN_t2018455390)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2722[13] = 
{
	TOKEN_t2018455390::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (Serializer_t3679305835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2723[1] = 
{
	Serializer_t3679305835::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (PlayServiceUtil_t2802694711), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (FB_AppRequestState_t1209389490)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2725[3] = 
{
	FB_AppRequestState_t1209389490::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (FB_Gender_t2517284304)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2726[3] = 
{
	FB_Gender_t2517284304::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (FB_HttpMethod_t1543704700)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2727[4] = 
{
	FB_HttpMethod_t1543704700::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (FB_PermissionStatus_t2970074352)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2728[3] = 
{
	FB_PermissionStatus_t2970074352::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (FB_RequestActionType_t2205998668)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2729[5] = 
{
	FB_RequestActionType_t2205998668::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (SPFacebook_t1065228369), -1, sizeof(SPFacebook_t1065228369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2731[52] = 
{
	SPFacebook_t1065228369::get_offset_of__userInfo_4(),
	SPFacebook_t1065228369::get_offset_of__friends_5(),
	SPFacebook_t1065228369::get_offset_of__invitableFriends_6(),
	SPFacebook_t1065228369::get_offset_of__IsInited_7(),
	SPFacebook_t1065228369::get_offset_of__userScores_8(),
	SPFacebook_t1065228369::get_offset_of__appScores_9(),
	SPFacebook_t1065228369::get_offset_of_lastSubmitedScore_10(),
	SPFacebook_t1065228369::get_offset_of__likes_11(),
	SPFacebook_t1065228369::get_offset_of__AppRequests_12(),
	SPFacebook_t1065228369::get_offset_of__FB_13(),
	SPFacebook_t1065228369::get_offset_of_IsLoginRequestInProgress_14(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnPostStarted_15(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnLoginStarted_16(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnLogOut_17(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnFriendsRequestStarted_18(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnInitCompleteAction_19(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnPostingCompleteAction_20(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnFocusChangedAction_21(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnAuthCompleteAction_22(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnUserDataRequestCompleteAction_23(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnFriendsDataRequestCompleteAction_24(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnInvitableFriendsDataRequestCompleteAction_25(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnAppRequestCompleteAction_26(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnAppRequestsLoaded_27(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnPermissionsLoaded_28(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnRevokePermission_29(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnAppScoresRequestCompleteAction_30(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnPlayerScoresRequestCompleteAction_31(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnSubmitScoreRequestCompleteAction_32(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnDeleteScoresRequestCompleteAction_33(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_OnLikesListLoadedAction_34(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache1F_35(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache20_36(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache21_37(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache22_38(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache23_39(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache24_40(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache25_41(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache26_42(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache27_43(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache28_44(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache29_45(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache2A_46(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache2B_47(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache2C_48(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache2D_49(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache2E_50(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache2F_51(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache30_52(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache31_53(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__amU24cache32_54(),
	SPFacebook_t1065228369_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (FB_Delegate_t3934797786), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (SPFacebookAnalytics_t2144030727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (SP_FB_API_v6_t1505850536), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (SP_FB_API_v7_t4234733891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[2] = 
{
	SP_FB_API_v7_t4234733891::get_offset_of__UserId_0(),
	SP_FB_API_v7_t4234733891::get_offset_of__AccessToken_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (FB_AppRequest_t501312625), -1, sizeof(FB_AppRequest_t501312625_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2736[13] = 
{
	FB_AppRequest_t501312625::get_offset_of_Id_0(),
	FB_AppRequest_t501312625::get_offset_of_ApplicationId_1(),
	FB_AppRequest_t501312625::get_offset_of_Message_2(),
	FB_AppRequest_t501312625::get_offset_of_ActionType_3(),
	FB_AppRequest_t501312625::get_offset_of_State_4(),
	FB_AppRequest_t501312625::get_offset_of_FromId_5(),
	FB_AppRequest_t501312625::get_offset_of_FromName_6(),
	FB_AppRequest_t501312625::get_offset_of_CreatedTime_7(),
	FB_AppRequest_t501312625::get_offset_of_CreatedTimeString_8(),
	FB_AppRequest_t501312625::get_offset_of_Data_9(),
	FB_AppRequest_t501312625::get_offset_of_Object_10(),
	FB_AppRequest_t501312625::get_offset_of_OnDeleteRequestFinished_11(),
	FB_AppRequest_t501312625_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (FB_AppRequestResult_t2450670428), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[2] = 
{
	FB_AppRequestResult_t2450670428::get_offset_of__ReuqestId_3(),
	FB_AppRequestResult_t2450670428::get_offset_of__Recipients_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (FB_LikeInfo_t3213199078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[3] = 
{
	FB_LikeInfo_t3213199078::get_offset_of_Id_0(),
	FB_LikeInfo_t3213199078::get_offset_of_Name_1(),
	FB_LikeInfo_t3213199078::get_offset_of_Category_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (FB_Object_t1438399806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[6] = 
{
	FB_Object_t1438399806::get_offset_of_Id_0(),
	FB_Object_t1438399806::get_offset_of_ImageUrls_1(),
	FB_Object_t1438399806::get_offset_of_Title_2(),
	FB_Object_t1438399806::get_offset_of_Type_3(),
	FB_Object_t1438399806::get_offset_of_CreatedTime_4(),
	FB_Object_t1438399806::get_offset_of_CreatedTimeString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (FB_Permission_t1872772122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[2] = 
{
	FB_Permission_t1872772122::get_offset_of__Name_0(),
	FB_Permission_t1872772122::get_offset_of__Status_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (FB_PermissionResult_t2322011743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[1] = 
{
	FB_PermissionResult_t2322011743::get_offset_of__Permissions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (FB_ProfileImageSize_t3003328130)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2742[5] = 
{
	FB_ProfileImageSize_t3003328130::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (FB_Score_t1450841581), -1, sizeof(FB_Score_t1450841581_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2743[8] = 
{
	FB_Score_t1450841581::get_offset_of_UserId_0(),
	FB_Score_t1450841581::get_offset_of_UserName_1(),
	FB_Score_t1450841581::get_offset_of_AppId_2(),
	FB_Score_t1450841581::get_offset_of_AppName_3(),
	FB_Score_t1450841581::get_offset_of_value_4(),
	FB_Score_t1450841581::get_offset_of_profileImages_5(),
	FB_Score_t1450841581::get_offset_of_OnProfileImageLoaded_6(),
	FB_Score_t1450841581_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (FB_UserInfo_t2704578078), -1, sizeof(FB_UserInfo_t2704578078_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2744[15] = 
{
	FB_UserInfo_t2704578078::get_offset_of__id_0(),
	FB_UserInfo_t2704578078::get_offset_of__name_1(),
	FB_UserInfo_t2704578078::get_offset_of__first_name_2(),
	FB_UserInfo_t2704578078::get_offset_of__last_name_3(),
	FB_UserInfo_t2704578078::get_offset_of__username_4(),
	FB_UserInfo_t2704578078::get_offset_of__profile_url_5(),
	FB_UserInfo_t2704578078::get_offset_of__email_6(),
	FB_UserInfo_t2704578078::get_offset_of__location_7(),
	FB_UserInfo_t2704578078::get_offset_of__locale_8(),
	FB_UserInfo_t2704578078::get_offset_of__rawJSON_9(),
	FB_UserInfo_t2704578078::get_offset_of__Birthday_10(),
	FB_UserInfo_t2704578078::get_offset_of__gender_11(),
	FB_UserInfo_t2704578078::get_offset_of_profileImages_12(),
	FB_UserInfo_t2704578078::get_offset_of_OnProfileImageLoaded_13(),
	FB_UserInfo_t2704578078_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (FB_LoginResult_t1848477279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[3] = 
{
	FB_LoginResult_t1848477279::get_offset_of__IsCanceled_3(),
	FB_LoginResult_t1848477279::get_offset_of__UserId_4(),
	FB_LoginResult_t1848477279::get_offset_of__AccessToken_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (FB_PostResult_t992884730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[1] = 
{
	FB_PostResult_t992884730::get_offset_of__PostId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (FB_Result_t838248372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[3] = 
{
	FB_Result_t838248372::get_offset_of__RawData_0(),
	FB_Result_t838248372::get_offset_of__Error_1(),
	FB_Result_t838248372::get_offset_of__IsSucceeded_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (FB_LikesRetrieveTask_t2495592094), -1, sizeof(FB_LikesRetrieveTask_t2495592094_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2748[3] = 
{
	FB_LikesRetrieveTask_t2495592094::get_offset_of__userId_2(),
	FB_LikesRetrieveTask_t2495592094::get_offset_of_ActionComplete_3(),
	FB_LikesRetrieveTask_t2495592094_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (FB_PostingTask_t2068490008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[9] = 
{
	FB_PostingTask_t2068490008::get_offset_of__toId_2(),
	FB_PostingTask_t2068490008::get_offset_of__link_3(),
	FB_PostingTask_t2068490008::get_offset_of__linkName_4(),
	FB_PostingTask_t2068490008::get_offset_of__linkCaption_5(),
	FB_PostingTask_t2068490008::get_offset_of__linkDescription_6(),
	FB_PostingTask_t2068490008::get_offset_of__picture_7(),
	FB_PostingTask_t2068490008::get_offset_of__actionName_8(),
	FB_PostingTask_t2068490008::get_offset_of__actionLink_9(),
	FB_PostingTask_t2068490008::get_offset_of__reference_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (InstagramEvents_t2237296277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2750[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (InstagramPostResult_t436083195)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2751[6] = 
{
	InstagramPostResult_t436083195::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (AndroidInstagramManager_t2759207936), -1, sizeof(AndroidInstagramManager_t2759207936_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2752[2] = 
{
	AndroidInstagramManager_t2759207936_StaticFields::get_offset_of_OnPostingCompleteAction_4(),
	AndroidInstagramManager_t2759207936_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (AndroidTwitterManager_t3465455211), -1, sizeof(AndroidTwitterManager_t3465455211_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2753[19] = 
{
	AndroidTwitterManager_t3465455211::get_offset_of__IsAuthed_4(),
	AndroidTwitterManager_t3465455211::get_offset_of__IsInited_5(),
	AndroidTwitterManager_t3465455211::get_offset_of__AccessToken_6(),
	AndroidTwitterManager_t3465455211::get_offset_of__AccessTokenSecret_7(),
	AndroidTwitterManager_t3465455211::get_offset_of__userInfo_8(),
	AndroidTwitterManager_t3465455211::get_offset_of_OnTwitterLoginStarted_9(),
	AndroidTwitterManager_t3465455211::get_offset_of_OnTwitterLogOut_10(),
	AndroidTwitterManager_t3465455211::get_offset_of_OnTwitterPostStarted_11(),
	AndroidTwitterManager_t3465455211::get_offset_of_OnTwitterInitedAction_12(),
	AndroidTwitterManager_t3465455211::get_offset_of_OnAuthCompleteAction_13(),
	AndroidTwitterManager_t3465455211::get_offset_of_OnPostingCompleteAction_14(),
	AndroidTwitterManager_t3465455211::get_offset_of_OnUserDataRequestCompleteAction_15(),
	AndroidTwitterManager_t3465455211_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_16(),
	AndroidTwitterManager_t3465455211_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_17(),
	AndroidTwitterManager_t3465455211_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_18(),
	AndroidTwitterManager_t3465455211_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_19(),
	AndroidTwitterManager_t3465455211_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_20(),
	AndroidTwitterManager_t3465455211_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_21(),
	AndroidTwitterManager_t3465455211_StaticFields::get_offset_of_U3CU3Ef__amU24cache12_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (TwitterApplicationOnlyToken_t3636409970), -1, sizeof(TwitterApplicationOnlyToken_t3636409970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2754[7] = 
{
	0,
	0,
	0,
	TwitterApplicationOnlyToken_t3636409970::get_offset_of__currentToken_7(),
	TwitterApplicationOnlyToken_t3636409970::get_offset_of_Headers_8(),
	TwitterApplicationOnlyToken_t3636409970::get_offset_of_ActionComplete_9(),
	TwitterApplicationOnlyToken_t3636409970_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (U3CLoadU3Ec__Iterator8_t1598146762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2755[9] = 
{
	U3CLoadU3Ec__Iterator8_t1598146762::get_offset_of_U3CurlU3E__0_0(),
	U3CLoadU3Ec__Iterator8_t1598146762::get_offset_of_U3CplainTextBytesU3E__1_1(),
	U3CLoadU3Ec__Iterator8_t1598146762::get_offset_of_U3CencodedAccessTokenU3E__2_2(),
	U3CLoadU3Ec__Iterator8_t1598146762::get_offset_of_U3CformU3E__3_3(),
	U3CLoadU3Ec__Iterator8_t1598146762::get_offset_of_U3CwwwU3E__4_4(),
	U3CLoadU3Ec__Iterator8_t1598146762::get_offset_of_U3CmapU3E__5_5(),
	U3CLoadU3Ec__Iterator8_t1598146762::get_offset_of_U24PC_6(),
	U3CLoadU3Ec__Iterator8_t1598146762::get_offset_of_U24current_7(),
	U3CLoadU3Ec__Iterator8_t1598146762::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (TwitterDataCash_t3686392272), -1, sizeof(TwitterDataCash_t3686392272_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2756[2] = 
{
	TwitterDataCash_t3686392272_StaticFields::get_offset_of_tweets_0(),
	TwitterDataCash_t3686392272_StaticFields::get_offset_of_users_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (TWResult_t1480791060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2757[2] = 
{
	TWResult_t1480791060::get_offset_of__IsSucceeded_0(),
	TWResult_t1480791060::get_offset_of__data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (TW_APIRequstResult_t455151055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2758[5] = 
{
	TW_APIRequstResult_t455151055::get_offset_of_tweets_0(),
	TW_APIRequstResult_t455151055::get_offset_of_users_1(),
	TW_APIRequstResult_t455151055::get_offset_of_ids_2(),
	TW_APIRequstResult_t455151055::get_offset_of__IsSucceeded_3(),
	TW_APIRequstResult_t455151055::get_offset_of__data_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (TweetTemplate_t3444491657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[13] = 
{
	TweetTemplate_t3444491657::get_offset_of_id_0(),
	TweetTemplate_t3444491657::get_offset_of_user_id_1(),
	TweetTemplate_t3444491657::get_offset_of_created_at_2(),
	TweetTemplate_t3444491657::get_offset_of_text_3(),
	TweetTemplate_t3444491657::get_offset_of_source_4(),
	TweetTemplate_t3444491657::get_offset_of_in_reply_to_status_id_5(),
	TweetTemplate_t3444491657::get_offset_of_in_reply_to_user_id_6(),
	TweetTemplate_t3444491657::get_offset_of_in_reply_to_screen_name_7(),
	TweetTemplate_t3444491657::get_offset_of_geo_8(),
	TweetTemplate_t3444491657::get_offset_of_place_9(),
	TweetTemplate_t3444491657::get_offset_of_lang_10(),
	TweetTemplate_t3444491657::get_offset_of_retweet_count_11(),
	TweetTemplate_t3444491657::get_offset_of_favorite_count_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (TwitterStatus_t2765035499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[3] = 
{
	TwitterStatus_t2765035499::get_offset_of__rawJSON_0(),
	TwitterStatus_t2765035499::get_offset_of__text_1(),
	TwitterStatus_t2765035499::get_offset_of__geo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (TwitterUserInfo_t87370740), -1, sizeof(TwitterUserInfo_t87370740_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2761[22] = 
{
	TwitterUserInfo_t87370740::get_offset_of__id_0(),
	TwitterUserInfo_t87370740::get_offset_of__description_1(),
	TwitterUserInfo_t87370740::get_offset_of__name_2(),
	TwitterUserInfo_t87370740::get_offset_of__screen_name_3(),
	TwitterUserInfo_t87370740::get_offset_of__location_4(),
	TwitterUserInfo_t87370740::get_offset_of__lang_5(),
	TwitterUserInfo_t87370740::get_offset_of__rawJSON_6(),
	TwitterUserInfo_t87370740::get_offset_of__profile_image_url_7(),
	TwitterUserInfo_t87370740::get_offset_of__profile_image_url_https_8(),
	TwitterUserInfo_t87370740::get_offset_of__profile_background_image_url_9(),
	TwitterUserInfo_t87370740::get_offset_of__profile_background_image_url_https_10(),
	TwitterUserInfo_t87370740::get_offset_of__profile_image_11(),
	TwitterUserInfo_t87370740::get_offset_of__profile_background_12(),
	TwitterUserInfo_t87370740::get_offset_of__profile_background_color_13(),
	TwitterUserInfo_t87370740::get_offset_of__profile_text_color_14(),
	TwitterUserInfo_t87370740::get_offset_of__friends_count_15(),
	TwitterUserInfo_t87370740::get_offset_of__statuses_count_16(),
	TwitterUserInfo_t87370740::get_offset_of__status_17(),
	TwitterUserInfo_t87370740::get_offset_of_ActionProfileImageLoaded_18(),
	TwitterUserInfo_t87370740::get_offset_of_ActionProfileBackgroundImageLoaded_19(),
	TwitterUserInfo_t87370740_StaticFields::get_offset_of_U3CU3Ef__amU24cache14_20(),
	TwitterUserInfo_t87370740_StaticFields::get_offset_of_U3CU3Ef__amU24cache15_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (TWAPITest_t1774637995), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (TW_APIRequest_t675604147), -1, sizeof(TW_APIRequest_t675604147_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2763[6] = 
{
	TW_APIRequest_t675604147::get_offset_of_IsFirst_2(),
	TW_APIRequest_t675604147::get_offset_of_GetParams_3(),
	TW_APIRequest_t675604147::get_offset_of_requestUrl_4(),
	TW_APIRequest_t675604147::get_offset_of_Headers_5(),
	TW_APIRequest_t675604147::get_offset_of_ActionComplete_6(),
	TW_APIRequest_t675604147_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (U3CRequestU3Ec__Iterator9_t2019357771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[4] = 
{
	U3CRequestU3Ec__Iterator9_t2019357771::get_offset_of_U3CwwwU3E__0_0(),
	U3CRequestU3Ec__Iterator9_t2019357771::get_offset_of_U24PC_1(),
	U3CRequestU3Ec__Iterator9_t2019357771::get_offset_of_U24current_2(),
	U3CRequestU3Ec__Iterator9_t2019357771::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (TW_FollowersIdsRequest_t261436078), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (TW_FriendsIdsRequest_t151035516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (TW_OAuthAPIRequest_t3823919588), -1, sizeof(TW_OAuthAPIRequest_t3823919588_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2767[7] = 
{
	TW_OAuthAPIRequest_t3823919588::get_offset_of_IsFirst_2(),
	TW_OAuthAPIRequest_t3823919588::get_offset_of_GetParams_3(),
	TW_OAuthAPIRequest_t3823919588::get_offset_of_requestUrl_4(),
	TW_OAuthAPIRequest_t3823919588::get_offset_of_Headers_5(),
	TW_OAuthAPIRequest_t3823919588::get_offset_of_requestParams_6(),
	TW_OAuthAPIRequest_t3823919588::get_offset_of_OnResult_7(),
	TW_OAuthAPIRequest_t3823919588_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (U3CRequestU3Ec__IteratorA_t1902108118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2768[21] = 
{
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3CtsU3E__0_0(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3Coauth_consumer_keyU3E__1_1(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3Coauth_tokenU3E__2_2(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3Coauth_signature_methodU3E__3_3(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3Coauth_timestampU3E__4_4(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3Coauth_nonceU3E__5_5(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3Coauth_versionU3E__6_6(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3CbaseStringU3E__7_7(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3CU24s_193U3E__8_8(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3CentryU3E__9_9(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3CconsumerSecretU3E__10_10(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3Coauth_token_secretU3E__11_11(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3CsigningKeyU3E__12_12(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3ChasherU3E__13_13(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3CsignatureStringU3E__14_14(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3CauthorizationHeaderParamsU3E__15_15(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3CwwwU3E__16_16(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3CresultU3E__17_17(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U24PC_18(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U24current_19(),
	U3CRequestU3Ec__IteratorA_t1902108118::get_offset_of_U3CU3Ef__this_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (TW_SearchTweetsRequest_t525743233), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (TW_UserTimeLineRequest_t1907229837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (TW_UsersLookUpRequest_t889831987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (TwitterPostingTask_t1522896362), -1, sizeof(TwitterPostingTask_t1522896362_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2772[5] = 
{
	TwitterPostingTask_t1522896362::get_offset_of__status_2(),
	TwitterPostingTask_t1522896362::get_offset_of__texture_3(),
	TwitterPostingTask_t1522896362::get_offset_of__controller_4(),
	TwitterPostingTask_t1522896362::get_offset_of_ActionComplete_5(),
	TwitterPostingTask_t1522896362_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (GPScore_t3219488889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2773[6] = 
{
	GPScore_t3219488889::get_offset_of__rank_0(),
	GPScore_t3219488889::get_offset_of__score_1(),
	GPScore_t3219488889::get_offset_of__playerId_2(),
	GPScore_t3219488889::get_offset_of__leaderboardId_3(),
	GPScore_t3219488889::get_offset_of__collection_4(),
	GPScore_t3219488889::get_offset_of__timeSpan_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (GooglePlayerTemplate_t2506317812), -1, sizeof(GooglePlayerTemplate_t2506317812_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2774[12] = 
{
	GooglePlayerTemplate_t2506317812::get_offset_of__id_0(),
	GooglePlayerTemplate_t2506317812::get_offset_of__name_1(),
	GooglePlayerTemplate_t2506317812::get_offset_of__iconImageUrl_2(),
	GooglePlayerTemplate_t2506317812::get_offset_of__hiResImageUrl_3(),
	GooglePlayerTemplate_t2506317812::get_offset_of__icon_4(),
	GooglePlayerTemplate_t2506317812::get_offset_of__image_5(),
	GooglePlayerTemplate_t2506317812::get_offset_of__hasIconImage_6(),
	GooglePlayerTemplate_t2506317812::get_offset_of__hasHiResImage_7(),
	GooglePlayerTemplate_t2506317812::get_offset_of_BigPhotoLoaded_8(),
	GooglePlayerTemplate_t2506317812::get_offset_of_SmallPhotoLoaded_9(),
	GooglePlayerTemplate_t2506317812_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_10(),
	GooglePlayerTemplate_t2506317812_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (SA_DataConverter_OLD_t3088030515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (SA_IdFactory_OLD_t1085491180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (U3CU3Ec__AnonStorey19_t1266469818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[1] = 
{
	U3CU3Ec__AnonStorey19_t1266469818::get_offset_of_random_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (SA_ScreenShotMaker_OLD_t795198627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2778[1] = 
{
	SA_ScreenShotMaker_OLD_t795198627::get_offset_of_OnScreenshotReady_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (U3CSaveScreenshotU3Ec__IteratorB_t479799330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2779[6] = 
{
	U3CSaveScreenshotU3Ec__IteratorB_t479799330::get_offset_of_U3CwidthU3E__0_0(),
	U3CSaveScreenshotU3Ec__IteratorB_t479799330::get_offset_of_U3CheightU3E__1_1(),
	U3CSaveScreenshotU3Ec__IteratorB_t479799330::get_offset_of_U3CtexU3E__2_2(),
	U3CSaveScreenshotU3Ec__IteratorB_t479799330::get_offset_of_U24PC_3(),
	U3CSaveScreenshotU3Ec__IteratorB_t479799330::get_offset_of_U24current_4(),
	U3CSaveScreenshotU3Ec__IteratorB_t479799330::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (SA_ModulesInfo_t3047057538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2781[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (WWWTextureLoader_t155872171), -1, sizeof(WWWTextureLoader_t155872171_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2782[3] = 
{
	WWWTextureLoader_t155872171::get_offset_of__url_2(),
	WWWTextureLoader_t155872171::get_offset_of_OnLoad_3(),
	WWWTextureLoader_t155872171_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (U3CLoadCoroutinU3Ec__IteratorC_t1223036233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2783[4] = 
{
	U3CLoadCoroutinU3Ec__IteratorC_t1223036233::get_offset_of_U3CwwwU3E__0_0(),
	U3CLoadCoroutinU3Ec__IteratorC_t1223036233::get_offset_of_U24PC_1(),
	U3CLoadCoroutinU3Ec__IteratorC_t1223036233::get_offset_of_U24current_2(),
	U3CLoadCoroutinU3Ec__IteratorC_t1223036233::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (DefaultPreviewButton_t12674677), -1, sizeof(DefaultPreviewButton_t12674677_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2784[10] = 
{
	DefaultPreviewButton_t12674677::get_offset_of_normalTexture_2(),
	DefaultPreviewButton_t12674677::get_offset_of_pressedTexture_3(),
	DefaultPreviewButton_t12674677::get_offset_of_disabledTexture_4(),
	DefaultPreviewButton_t12674677::get_offset_of_selectedTexture_5(),
	DefaultPreviewButton_t12674677::get_offset_of_normalTex_6(),
	DefaultPreviewButton_t12674677::get_offset_of_sound_7(),
	DefaultPreviewButton_t12674677::get_offset_of_disabledsound_8(),
	DefaultPreviewButton_t12674677::get_offset_of_IsDisabled_9(),
	DefaultPreviewButton_t12674677::get_offset_of_ActionClick_10(),
	DefaultPreviewButton_t12674677_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (SALevelLoader_t4125067327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2785[1] = 
{
	SALevelLoader_t4125067327::get_offset_of_bg_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (SALoadedSceneOnClick_t4265150718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2786[1] = 
{
	SALoadedSceneOnClick_t4265150718::get_offset_of_levelName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (SAOnClickAction_t650504801), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (SAOpenUrlOnClick_t1829835980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2788[1] = 
{
	SAOpenUrlOnClick_t1829835980::get_offset_of_url_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (SASendMessageOnClick_t243868668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2789[2] = 
{
	SASendMessageOnClick_t243868668::get_offset_of_Reciver_2(),
	SASendMessageOnClick_t243868668::get_offset_of_MethodName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (SA_BackButton_t1901565506), -1, sizeof(SA_BackButton_t1901565506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2790[1] = 
{
	SA_BackButton_t1901565506_StaticFields::get_offset_of_firstLevel_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (SA_FriendUI_t3775341837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[5] = 
{
	SA_FriendUI_t3775341837::get_offset_of__pId_2(),
	SA_FriendUI_t3775341837::get_offset_of_avatar_3(),
	SA_FriendUI_t3775341837::get_offset_of_playerId_4(),
	SA_FriendUI_t3775341837::get_offset_of_playerName_5(),
	SA_FriendUI_t3775341837::get_offset_of_defaulttexture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (SA_Label_t226960149), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (SA_PartisipantUI_t3115332592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[6] = 
{
	SA_PartisipantUI_t3115332592::get_offset_of_avatar_2(),
	SA_PartisipantUI_t3115332592::get_offset_of_id_3(),
	SA_PartisipantUI_t3115332592::get_offset_of_status_4(),
	SA_PartisipantUI_t3115332592::get_offset_of_playerId_5(),
	SA_PartisipantUI_t3115332592::get_offset_of_playerName_6(),
	SA_PartisipantUI_t3115332592::get_offset_of_defaulttexture_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (SA_StatusBar_t1378080260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2794[2] = 
{
	SA_StatusBar_t1378080260::get_offset_of_title_2(),
	SA_StatusBar_t1378080260::get_offset_of_shadow_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (SA_Texture_t3361108684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (PreviewScreenUtil_t19016308), -1, sizeof(PreviewScreenUtil_t19016308_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2796[5] = 
{
	PreviewScreenUtil_t19016308_StaticFields::get_offset_of__instance_2(),
	PreviewScreenUtil_t19016308::get_offset_of_W_3(),
	PreviewScreenUtil_t19016308::get_offset_of_H_4(),
	PreviewScreenUtil_t19016308::get_offset_of_ActionScreenResized_5(),
	PreviewScreenUtil_t19016308_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (ScreenPlacement_t3340443251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2797[7] = 
{
	ScreenPlacement_t3340443251::get_offset_of_position_2(),
	ScreenPlacement_t3340443251::get_offset_of_pixelOffset_3(),
	ScreenPlacement_t3340443251::get_offset_of_persents_4(),
	ScreenPlacement_t3340443251::get_offset_of_calulateStartOnly_5(),
	ScreenPlacement_t3340443251::get_offset_of_boundsTransform_6(),
	ScreenPlacement_t3340443251::get_offset_of_actualOffset_7(),
	ScreenPlacement_t3340443251::get_offset_of_orinetation_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (ScreenPosition_t2449785599)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2798[10] = 
{
	ScreenPosition_t2449785599::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (ScreenPlacementExtension_t1575904396), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
