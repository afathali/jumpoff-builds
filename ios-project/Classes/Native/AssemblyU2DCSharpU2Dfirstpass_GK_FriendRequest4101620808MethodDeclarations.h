﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_FriendRequest
struct GK_FriendRequest_t4101620808;
// System.String[]
struct StringU5BU5D_t1642385972;
// GK_Player[]
struct GK_PlayerU5BU5D_t1642762691;

#include "codegen/il2cpp-codegen.h"

// System.Void GK_FriendRequest::.ctor()
extern "C"  void GK_FriendRequest__ctor_m212504341 (GK_FriendRequest_t4101620808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_FriendRequest::addRecipientsWithEmailAddresses(System.String[])
extern "C"  void GK_FriendRequest_addRecipientsWithEmailAddresses_m747256592 (GK_FriendRequest_t4101620808 * __this, StringU5BU5D_t1642385972* ___emailAddresses0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_FriendRequest::addRecipientPlayers(GK_Player[])
extern "C"  void GK_FriendRequest_addRecipientPlayers_m1337057945 (GK_FriendRequest_t4101620808 * __this, GK_PlayerU5BU5D_t1642762691* ___players0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_FriendRequest::Send()
extern "C"  void GK_FriendRequest_Send_m1173762309 (GK_FriendRequest_t4101620808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GK_FriendRequest::get_Id()
extern "C"  int32_t GK_FriendRequest_get_Id_m2684624371 (GK_FriendRequest_t4101620808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
