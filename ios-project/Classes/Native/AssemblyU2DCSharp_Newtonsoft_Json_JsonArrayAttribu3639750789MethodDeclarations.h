﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonArrayAttribute
struct JsonArrayAttribute_t3639750789;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Newtonsoft.Json.JsonArrayAttribute::.ctor()
extern "C"  void JsonArrayAttribute__ctor_m1690784357 (JsonArrayAttribute_t3639750789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonArrayAttribute::.ctor(System.Boolean)
extern "C"  void JsonArrayAttribute__ctor_m2185981226 (JsonArrayAttribute_t3639750789 * __this, bool ___allowNullItems0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonArrayAttribute::.ctor(System.String)
extern "C"  void JsonArrayAttribute__ctor_m1498578395 (JsonArrayAttribute_t3639750789 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonArrayAttribute::get_AllowNullItems()
extern "C"  bool JsonArrayAttribute_get_AllowNullItems_m375092568 (JsonArrayAttribute_t3639750789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonArrayAttribute::set_AllowNullItems(System.Boolean)
extern "C"  void JsonArrayAttribute_set_AllowNullItems_m2246652695 (JsonArrayAttribute_t3639750789 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
