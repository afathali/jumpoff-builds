﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TW_SearchTweetsRequest
struct TW_SearchTweetsRequest_t525743233;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TW_SearchTweetsRequest::.ctor()
extern "C"  void TW_SearchTweetsRequest__ctor_m718160328 (TW_SearchTweetsRequest_t525743233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TW_SearchTweetsRequest TW_SearchTweetsRequest::Create()
extern "C"  TW_SearchTweetsRequest_t525743233 * TW_SearchTweetsRequest_Create_m3603265098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_SearchTweetsRequest::Awake()
extern "C"  void TW_SearchTweetsRequest_Awake_m86917581 (TW_SearchTweetsRequest_t525743233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_SearchTweetsRequest::OnResult(System.String)
extern "C"  void TW_SearchTweetsRequest_OnResult_m3068133632 (TW_SearchTweetsRequest_t525743233 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
