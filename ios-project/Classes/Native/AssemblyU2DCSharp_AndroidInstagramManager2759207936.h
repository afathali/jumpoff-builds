﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<InstagramPostResult>
struct Action_1_t237882577;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen4050051854.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidInstagramManager
struct  AndroidInstagramManager_t2759207936  : public SA_Singleton_OLD_1_t4050051854
{
public:

public:
};

struct AndroidInstagramManager_t2759207936_StaticFields
{
public:
	// System.Action`1<InstagramPostResult> AndroidInstagramManager::OnPostingCompleteAction
	Action_1_t237882577 * ___OnPostingCompleteAction_4;
	// System.Action`1<InstagramPostResult> AndroidInstagramManager::<>f__am$cache1
	Action_1_t237882577 * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_OnPostingCompleteAction_4() { return static_cast<int32_t>(offsetof(AndroidInstagramManager_t2759207936_StaticFields, ___OnPostingCompleteAction_4)); }
	inline Action_1_t237882577 * get_OnPostingCompleteAction_4() const { return ___OnPostingCompleteAction_4; }
	inline Action_1_t237882577 ** get_address_of_OnPostingCompleteAction_4() { return &___OnPostingCompleteAction_4; }
	inline void set_OnPostingCompleteAction_4(Action_1_t237882577 * value)
	{
		___OnPostingCompleteAction_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnPostingCompleteAction_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(AndroidInstagramManager_t2759207936_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Action_1_t237882577 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Action_1_t237882577 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Action_1_t237882577 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
