﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlDeclarationWrapper
struct XmlDeclarationWrapper_t3510733548;
// System.Xml.XmlDeclaration
struct XmlDeclaration_t1545359137;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlDeclaration1545359137.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Newtonsoft.Json.Converters.XmlDeclarationWrapper::.ctor(System.Xml.XmlDeclaration)
extern "C"  void XmlDeclarationWrapper__ctor_m2919100170 (XmlDeclarationWrapper_t3510733548 * __this, XmlDeclaration_t1545359137 * ___declaration0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlDeclarationWrapper::get_Version()
extern "C"  String_t* XmlDeclarationWrapper_get_Version_m1049034041 (XmlDeclarationWrapper_t3510733548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlDeclarationWrapper::get_Encoding()
extern "C"  String_t* XmlDeclarationWrapper_get_Encoding_m468818410 (XmlDeclarationWrapper_t3510733548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlDeclarationWrapper::set_Encoding(System.String)
extern "C"  void XmlDeclarationWrapper_set_Encoding_m4293269455 (XmlDeclarationWrapper_t3510733548 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlDeclarationWrapper::get_Standalone()
extern "C"  String_t* XmlDeclarationWrapper_get_Standalone_m2845552284 (XmlDeclarationWrapper_t3510733548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlDeclarationWrapper::set_Standalone(System.String)
extern "C"  void XmlDeclarationWrapper_set_Standalone_m2807807825 (XmlDeclarationWrapper_t3510733548 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
