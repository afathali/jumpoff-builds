﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>
struct ShimEnumerator_t1461821190;
// System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>
struct Dictionary_2_t1356696369;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1953638932_gshared (ShimEnumerator_t1461821190 * __this, Dictionary_2_t1356696369 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1953638932(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1461821190 *, Dictionary_2_t1356696369 *, const MethodInfo*))ShimEnumerator__ctor_m1953638932_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2122901333_gshared (ShimEnumerator_t1461821190 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2122901333(__this, method) ((  bool (*) (ShimEnumerator_t1461821190 *, const MethodInfo*))ShimEnumerator_MoveNext_m2122901333_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m781064369_gshared (ShimEnumerator_t1461821190 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m781064369(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1461821190 *, const MethodInfo*))ShimEnumerator_get_Entry_m781064369_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2343961516_gshared (ShimEnumerator_t1461821190 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2343961516(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1461821190 *, const MethodInfo*))ShimEnumerator_get_Key_m2343961516_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1947321390_gshared (ShimEnumerator_t1461821190 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1947321390(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1461821190 *, const MethodInfo*))ShimEnumerator_get_Value_m1947321390_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1719580234_gshared (ShimEnumerator_t1461821190 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1719580234(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1461821190 *, const MethodInfo*))ShimEnumerator_get_Current_m1719580234_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::Reset()
extern "C"  void ShimEnumerator_Reset_m44744342_gshared (ShimEnumerator_t1461821190 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m44744342(__this, method) ((  void (*) (ShimEnumerator_t1461821190 *, const MethodInfo*))ShimEnumerator_Reset_m44744342_gshared)(__this, method)
