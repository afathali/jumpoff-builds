﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.CollectionUtils/<CreateListWrapper>c__AnonStorey27
struct U3CCreateListWrapperU3Ec__AnonStorey27_t463578570;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void Newtonsoft.Json.Utilities.CollectionUtils/<CreateListWrapper>c__AnonStorey27::.ctor()
extern "C"  void U3CCreateListWrapperU3Ec__AnonStorey27__ctor_m4140594359 (U3CCreateListWrapperU3Ec__AnonStorey27_t463578570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.CollectionUtils/<CreateListWrapper>c__AnonStorey27::<>m__E9(System.Type,System.Collections.Generic.IList`1<System.Object>)
extern "C"  Il2CppObject * U3CCreateListWrapperU3Ec__AnonStorey27_U3CU3Em__E9_m2870850307 (U3CCreateListWrapperU3Ec__AnonStorey27_t463578570 * __this, Type_t * ___t0, Il2CppObject* ___a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
