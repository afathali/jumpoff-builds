﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// DefaultPreviewButton
struct DefaultPreviewButton_t12674677;
// SA_Label
struct SA_Label_t226960149;
// DefaultPreviewButton[]
struct DefaultPreviewButtonU5BU5D_t3800739864;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestAndEventsExample
struct  QuestAndEventsExample_t3324016540  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject QuestAndEventsExample::avatar
	GameObject_t1756533147 * ___avatar_4;
	// UnityEngine.Texture QuestAndEventsExample::defaulttexture
	Texture_t2243626319 * ___defaulttexture_5;
	// UnityEngine.Texture2D QuestAndEventsExample::pieIcon
	Texture2D_t3542995729 * ___pieIcon_6;
	// DefaultPreviewButton QuestAndEventsExample::connectButton
	DefaultPreviewButton_t12674677 * ___connectButton_7;
	// SA_Label QuestAndEventsExample::playerLabel
	SA_Label_t226960149 * ___playerLabel_8;
	// DefaultPreviewButton[] QuestAndEventsExample::ConnectionDependedntButtons
	DefaultPreviewButtonU5BU5D_t3800739864* ___ConnectionDependedntButtons_9;

public:
	inline static int32_t get_offset_of_avatar_4() { return static_cast<int32_t>(offsetof(QuestAndEventsExample_t3324016540, ___avatar_4)); }
	inline GameObject_t1756533147 * get_avatar_4() const { return ___avatar_4; }
	inline GameObject_t1756533147 ** get_address_of_avatar_4() { return &___avatar_4; }
	inline void set_avatar_4(GameObject_t1756533147 * value)
	{
		___avatar_4 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_4, value);
	}

	inline static int32_t get_offset_of_defaulttexture_5() { return static_cast<int32_t>(offsetof(QuestAndEventsExample_t3324016540, ___defaulttexture_5)); }
	inline Texture_t2243626319 * get_defaulttexture_5() const { return ___defaulttexture_5; }
	inline Texture_t2243626319 ** get_address_of_defaulttexture_5() { return &___defaulttexture_5; }
	inline void set_defaulttexture_5(Texture_t2243626319 * value)
	{
		___defaulttexture_5 = value;
		Il2CppCodeGenWriteBarrier(&___defaulttexture_5, value);
	}

	inline static int32_t get_offset_of_pieIcon_6() { return static_cast<int32_t>(offsetof(QuestAndEventsExample_t3324016540, ___pieIcon_6)); }
	inline Texture2D_t3542995729 * get_pieIcon_6() const { return ___pieIcon_6; }
	inline Texture2D_t3542995729 ** get_address_of_pieIcon_6() { return &___pieIcon_6; }
	inline void set_pieIcon_6(Texture2D_t3542995729 * value)
	{
		___pieIcon_6 = value;
		Il2CppCodeGenWriteBarrier(&___pieIcon_6, value);
	}

	inline static int32_t get_offset_of_connectButton_7() { return static_cast<int32_t>(offsetof(QuestAndEventsExample_t3324016540, ___connectButton_7)); }
	inline DefaultPreviewButton_t12674677 * get_connectButton_7() const { return ___connectButton_7; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_connectButton_7() { return &___connectButton_7; }
	inline void set_connectButton_7(DefaultPreviewButton_t12674677 * value)
	{
		___connectButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___connectButton_7, value);
	}

	inline static int32_t get_offset_of_playerLabel_8() { return static_cast<int32_t>(offsetof(QuestAndEventsExample_t3324016540, ___playerLabel_8)); }
	inline SA_Label_t226960149 * get_playerLabel_8() const { return ___playerLabel_8; }
	inline SA_Label_t226960149 ** get_address_of_playerLabel_8() { return &___playerLabel_8; }
	inline void set_playerLabel_8(SA_Label_t226960149 * value)
	{
		___playerLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___playerLabel_8, value);
	}

	inline static int32_t get_offset_of_ConnectionDependedntButtons_9() { return static_cast<int32_t>(offsetof(QuestAndEventsExample_t3324016540, ___ConnectionDependedntButtons_9)); }
	inline DefaultPreviewButtonU5BU5D_t3800739864* get_ConnectionDependedntButtons_9() const { return ___ConnectionDependedntButtons_9; }
	inline DefaultPreviewButtonU5BU5D_t3800739864** get_address_of_ConnectionDependedntButtons_9() { return &___ConnectionDependedntButtons_9; }
	inline void set_ConnectionDependedntButtons_9(DefaultPreviewButtonU5BU5D_t3800739864* value)
	{
		___ConnectionDependedntButtons_9 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionDependedntButtons_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
