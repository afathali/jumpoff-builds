﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PaymentManagerExample
struct PaymentManagerExample_t3283960187;
// System.String
struct String_t;
// IOSStoreKitResult
struct IOSStoreKitResult_t2359407583;
// IOSStoreKitRestoreResult
struct IOSStoreKitRestoreResult_t3305276155;
// IOSStoreKitVerificationResponse
struct IOSStoreKitVerificationResponse_t4263658582;
// SA.Common.Models.Result
struct Result_t4287219743;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreKitResult2359407583.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreKitRestoreRe3305276155.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreKitVerificat4263658582.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"

// System.Void PaymentManagerExample::.ctor()
extern "C"  void PaymentManagerExample__ctor_m320657222 (PaymentManagerExample_t3283960187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::.cctor()
extern "C"  void PaymentManagerExample__cctor_m356267195 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::init()
extern "C"  void PaymentManagerExample_init_m1458338142 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::buyItem(System.String)
extern "C"  void PaymentManagerExample_buyItem_m2963797393 (Il2CppObject * __this /* static, unused */, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::UnlockProducts(System.String)
extern "C"  void PaymentManagerExample_UnlockProducts_m3049995594 (Il2CppObject * __this /* static, unused */, String_t* ___productIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::OnTransactionComplete(IOSStoreKitResult)
extern "C"  void PaymentManagerExample_OnTransactionComplete_m1885538609 (Il2CppObject * __this /* static, unused */, IOSStoreKitResult_t2359407583 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::OnRestoreComplete(IOSStoreKitRestoreResult)
extern "C"  void PaymentManagerExample_OnRestoreComplete_m414079553 (Il2CppObject * __this /* static, unused */, IOSStoreKitRestoreResult_t3305276155 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::HandleOnVerificationComplete(IOSStoreKitVerificationResponse)
extern "C"  void PaymentManagerExample_HandleOnVerificationComplete_m747086841 (Il2CppObject * __this /* static, unused */, IOSStoreKitVerificationResponse_t4263658582 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PaymentManagerExample::OnStoreKitInitComplete(SA.Common.Models.Result)
extern "C"  void PaymentManagerExample_OnStoreKitInitComplete_m3919835779 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
