﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchemaModel>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m513258618(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1389980064 *, JsonSchemaNode_t3866831117 *, JsonSchemaModel_t708894576 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchemaModel>::get_Key()
#define KeyValuePair_2_get_Key_m332697808(__this, method) ((  JsonSchemaNode_t3866831117 * (*) (KeyValuePair_2_t1389980064 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchemaModel>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2009330687(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1389980064 *, JsonSchemaNode_t3866831117 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchemaModel>::get_Value()
#define KeyValuePair_2_get_Value_m1342486736(__this, method) ((  JsonSchemaModel_t708894576 * (*) (KeyValuePair_2_t1389980064 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchemaModel>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3075090223(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1389980064 *, JsonSchemaModel_t708894576 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchemaModel>::ToString()
#define KeyValuePair_2_ToString_m3519532761(__this, method) ((  String_t* (*) (KeyValuePair_2_t1389980064 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
