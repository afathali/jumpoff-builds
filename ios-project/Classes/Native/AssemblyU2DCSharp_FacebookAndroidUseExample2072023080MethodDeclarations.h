﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookAndroidUseExample
struct FacebookAndroidUseExample_t2072023080;
// FB_Result
struct FB_Result_t838248372;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// FB_PostResult
struct FB_PostResult_t992884730;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FB_Result838248372.h"
#include "AssemblyU2DCSharp_FB_PostResult992884730.h"

// System.Void FacebookAndroidUseExample::.ctor()
extern "C"  void FacebookAndroidUseExample__ctor_m1984193359 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::.cctor()
extern "C"  void FacebookAndroidUseExample__cctor_m2828516990 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::Awake()
extern "C"  void FacebookAndroidUseExample_Awake_m3168955704 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::HandleOnRevokePermission(FB_Result)
extern "C"  void FacebookAndroidUseExample_HandleOnRevokePermission_m3419168525 (FacebookAndroidUseExample_t2072023080 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::FixedUpdate()
extern "C"  void FacebookAndroidUseExample_FixedUpdate_m899903176 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::PostWithAuthCheck()
extern "C"  void FacebookAndroidUseExample_PostWithAuthCheck_m1571842815 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::PostNativeScreenshot()
extern "C"  void FacebookAndroidUseExample_PostNativeScreenshot_m2507019158 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::PostImage()
extern "C"  void FacebookAndroidUseExample_PostImage_m2535372304 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FacebookAndroidUseExample::PostFBScreenshot()
extern "C"  Il2CppObject * FacebookAndroidUseExample_PostFBScreenshot_m3067730413 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::Connect()
extern "C"  void FacebookAndroidUseExample_Connect_m2822611365 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::LoadUserData()
extern "C"  void FacebookAndroidUseExample_LoadUserData_m2584415324 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::PostMessage()
extern "C"  void FacebookAndroidUseExample_PostMessage_m3371942214 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::PostScreehShot()
extern "C"  void FacebookAndroidUseExample_PostScreehShot_m2657976301 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::LoadFriends()
extern "C"  void FacebookAndroidUseExample_LoadFriends_m2025857358 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::AppRequest()
extern "C"  void FacebookAndroidUseExample_AppRequest_m1885581487 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::GetPermissions()
extern "C"  void FacebookAndroidUseExample_GetPermissions_m807287585 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::RevokePermission()
extern "C"  void FacebookAndroidUseExample_RevokePermission_m1318489326 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::LoadScore()
extern "C"  void FacebookAndroidUseExample_LoadScore_m3436982213 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::LoadAppScores()
extern "C"  void FacebookAndroidUseExample_LoadAppScores_m1970018831 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::SubmitScore()
extern "C"  void FacebookAndroidUseExample_SubmitScore_m612892715 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::DeletePlayerScores()
extern "C"  void FacebookAndroidUseExample_DeletePlayerScores_m2860148834 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::LikePage()
extern "C"  void FacebookAndroidUseExample_LikePage_m68274415 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::CheckLike()
extern "C"  void FacebookAndroidUseExample_CheckLike_m497430684 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::OnLikesLoaded(FB_Result)
extern "C"  void FacebookAndroidUseExample_OnLikesLoaded_m1370450583 (FacebookAndroidUseExample_t2072023080 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::OnFocusChanged(System.Boolean)
extern "C"  void FacebookAndroidUseExample_OnFocusChanged_m133716031 (FacebookAndroidUseExample_t2072023080 * __this, bool ___focus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::OnUserDataLoaded(FB_Result)
extern "C"  void FacebookAndroidUseExample_OnUserDataLoaded_m565968990 (FacebookAndroidUseExample_t2072023080 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::OnFriendsDataLoaded(FB_Result)
extern "C"  void FacebookAndroidUseExample_OnFriendsDataLoaded_m2609697574 (FacebookAndroidUseExample_t2072023080 * __this, FB_Result_t838248372 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::OnInit()
extern "C"  void FacebookAndroidUseExample_OnInit_m1533410796 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::OnAuth(FB_Result)
extern "C"  void FacebookAndroidUseExample_OnAuth_m1731201222 (FacebookAndroidUseExample_t2072023080 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::OnPost(FB_PostResult)
extern "C"  void FacebookAndroidUseExample_OnPost_m3312914400 (FacebookAndroidUseExample_t2072023080 * __this, FB_PostResult_t992884730 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::OnPlayerScoreRequestComplete(FB_Result)
extern "C"  void FacebookAndroidUseExample_OnPlayerScoreRequestComplete_m1431487331 (FacebookAndroidUseExample_t2072023080 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::OnAppScoreRequestComplete(FB_Result)
extern "C"  void FacebookAndroidUseExample_OnAppScoreRequestComplete_m2826140549 (FacebookAndroidUseExample_t2072023080 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::OnSubmitScoreRequestComplete(FB_Result)
extern "C"  void FacebookAndroidUseExample_OnSubmitScoreRequestComplete_m1659956332 (FacebookAndroidUseExample_t2072023080 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::OnDeleteScoreRequestComplete(FB_Result)
extern "C"  void FacebookAndroidUseExample_OnDeleteScoreRequestComplete_m3479554521 (FacebookAndroidUseExample_t2072023080 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FacebookAndroidUseExample::PostScreenshot()
extern "C"  Il2CppObject * FacebookAndroidUseExample_PostScreenshot_m3086615845 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample::LogOut()
extern "C"  void FacebookAndroidUseExample_LogOut_m527136961 (FacebookAndroidUseExample_t2072023080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
