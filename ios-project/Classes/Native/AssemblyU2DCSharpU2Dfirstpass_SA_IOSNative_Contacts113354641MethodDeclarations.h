﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.IOSNative.Contacts.PhoneNumber
struct PhoneNumber_t113354641;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.IOSNative.Contacts.PhoneNumber::.ctor()
extern "C"  void PhoneNumber__ctor_m564836165 (PhoneNumber_t113354641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
