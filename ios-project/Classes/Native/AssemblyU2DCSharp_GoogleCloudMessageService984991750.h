﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<GP_GCM_RegistrationResult>
struct Action_1_t2694291500;
// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct Action_2_t2514582953;
// System.Action`3<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean>
struct Action_3_t1218892226;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen2275835668.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleCloudMessageService
struct  GoogleCloudMessageService_t984991750  : public SA_Singleton_OLD_1_t2275835668
{
public:
	// System.String GoogleCloudMessageService::_lastMessage
	String_t* ____lastMessage_4;
	// System.String GoogleCloudMessageService::_registrationId
	String_t* ____registrationId_5;

public:
	inline static int32_t get_offset_of__lastMessage_4() { return static_cast<int32_t>(offsetof(GoogleCloudMessageService_t984991750, ____lastMessage_4)); }
	inline String_t* get__lastMessage_4() const { return ____lastMessage_4; }
	inline String_t** get_address_of__lastMessage_4() { return &____lastMessage_4; }
	inline void set__lastMessage_4(String_t* value)
	{
		____lastMessage_4 = value;
		Il2CppCodeGenWriteBarrier(&____lastMessage_4, value);
	}

	inline static int32_t get_offset_of__registrationId_5() { return static_cast<int32_t>(offsetof(GoogleCloudMessageService_t984991750, ____registrationId_5)); }
	inline String_t* get__registrationId_5() const { return ____registrationId_5; }
	inline String_t** get_address_of__registrationId_5() { return &____registrationId_5; }
	inline void set__registrationId_5(String_t* value)
	{
		____registrationId_5 = value;
		Il2CppCodeGenWriteBarrier(&____registrationId_5, value);
	}
};

struct GoogleCloudMessageService_t984991750_StaticFields
{
public:
	// System.Action`1<System.String> GoogleCloudMessageService::ActionCouldMessageLoaded
	Action_1_t1831019615 * ___ActionCouldMessageLoaded_6;
	// System.Action`1<GP_GCM_RegistrationResult> GoogleCloudMessageService::ActionCMDRegistrationResult
	Action_1_t2694291500 * ___ActionCMDRegistrationResult_7;
	// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>> GoogleCloudMessageService::ActionGCMPushReceived
	Action_2_t2514582953 * ___ActionGCMPushReceived_8;
	// System.Action`3<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean> GoogleCloudMessageService::ActionGameThriveNotificationReceived
	Action_3_t1218892226 * ___ActionGameThriveNotificationReceived_9;
	// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>> GoogleCloudMessageService::ActionParsePushReceived
	Action_2_t2514582953 * ___ActionParsePushReceived_10;
	// System.Action`1<System.String> GoogleCloudMessageService::<>f__am$cache7
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache7_11;
	// System.Action`1<GP_GCM_RegistrationResult> GoogleCloudMessageService::<>f__am$cache8
	Action_1_t2694291500 * ___U3CU3Ef__amU24cache8_12;
	// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>> GoogleCloudMessageService::<>f__am$cache9
	Action_2_t2514582953 * ___U3CU3Ef__amU24cache9_13;
	// System.Action`3<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean> GoogleCloudMessageService::<>f__am$cacheA
	Action_3_t1218892226 * ___U3CU3Ef__amU24cacheA_14;
	// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>> GoogleCloudMessageService::<>f__am$cacheB
	Action_2_t2514582953 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_ActionCouldMessageLoaded_6() { return static_cast<int32_t>(offsetof(GoogleCloudMessageService_t984991750_StaticFields, ___ActionCouldMessageLoaded_6)); }
	inline Action_1_t1831019615 * get_ActionCouldMessageLoaded_6() const { return ___ActionCouldMessageLoaded_6; }
	inline Action_1_t1831019615 ** get_address_of_ActionCouldMessageLoaded_6() { return &___ActionCouldMessageLoaded_6; }
	inline void set_ActionCouldMessageLoaded_6(Action_1_t1831019615 * value)
	{
		___ActionCouldMessageLoaded_6 = value;
		Il2CppCodeGenWriteBarrier(&___ActionCouldMessageLoaded_6, value);
	}

	inline static int32_t get_offset_of_ActionCMDRegistrationResult_7() { return static_cast<int32_t>(offsetof(GoogleCloudMessageService_t984991750_StaticFields, ___ActionCMDRegistrationResult_7)); }
	inline Action_1_t2694291500 * get_ActionCMDRegistrationResult_7() const { return ___ActionCMDRegistrationResult_7; }
	inline Action_1_t2694291500 ** get_address_of_ActionCMDRegistrationResult_7() { return &___ActionCMDRegistrationResult_7; }
	inline void set_ActionCMDRegistrationResult_7(Action_1_t2694291500 * value)
	{
		___ActionCMDRegistrationResult_7 = value;
		Il2CppCodeGenWriteBarrier(&___ActionCMDRegistrationResult_7, value);
	}

	inline static int32_t get_offset_of_ActionGCMPushReceived_8() { return static_cast<int32_t>(offsetof(GoogleCloudMessageService_t984991750_StaticFields, ___ActionGCMPushReceived_8)); }
	inline Action_2_t2514582953 * get_ActionGCMPushReceived_8() const { return ___ActionGCMPushReceived_8; }
	inline Action_2_t2514582953 ** get_address_of_ActionGCMPushReceived_8() { return &___ActionGCMPushReceived_8; }
	inline void set_ActionGCMPushReceived_8(Action_2_t2514582953 * value)
	{
		___ActionGCMPushReceived_8 = value;
		Il2CppCodeGenWriteBarrier(&___ActionGCMPushReceived_8, value);
	}

	inline static int32_t get_offset_of_ActionGameThriveNotificationReceived_9() { return static_cast<int32_t>(offsetof(GoogleCloudMessageService_t984991750_StaticFields, ___ActionGameThriveNotificationReceived_9)); }
	inline Action_3_t1218892226 * get_ActionGameThriveNotificationReceived_9() const { return ___ActionGameThriveNotificationReceived_9; }
	inline Action_3_t1218892226 ** get_address_of_ActionGameThriveNotificationReceived_9() { return &___ActionGameThriveNotificationReceived_9; }
	inline void set_ActionGameThriveNotificationReceived_9(Action_3_t1218892226 * value)
	{
		___ActionGameThriveNotificationReceived_9 = value;
		Il2CppCodeGenWriteBarrier(&___ActionGameThriveNotificationReceived_9, value);
	}

	inline static int32_t get_offset_of_ActionParsePushReceived_10() { return static_cast<int32_t>(offsetof(GoogleCloudMessageService_t984991750_StaticFields, ___ActionParsePushReceived_10)); }
	inline Action_2_t2514582953 * get_ActionParsePushReceived_10() const { return ___ActionParsePushReceived_10; }
	inline Action_2_t2514582953 ** get_address_of_ActionParsePushReceived_10() { return &___ActionParsePushReceived_10; }
	inline void set_ActionParsePushReceived_10(Action_2_t2514582953 * value)
	{
		___ActionParsePushReceived_10 = value;
		Il2CppCodeGenWriteBarrier(&___ActionParsePushReceived_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_11() { return static_cast<int32_t>(offsetof(GoogleCloudMessageService_t984991750_StaticFields, ___U3CU3Ef__amU24cache7_11)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache7_11() const { return ___U3CU3Ef__amU24cache7_11; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache7_11() { return &___U3CU3Ef__amU24cache7_11; }
	inline void set_U3CU3Ef__amU24cache7_11(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache7_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_12() { return static_cast<int32_t>(offsetof(GoogleCloudMessageService_t984991750_StaticFields, ___U3CU3Ef__amU24cache8_12)); }
	inline Action_1_t2694291500 * get_U3CU3Ef__amU24cache8_12() const { return ___U3CU3Ef__amU24cache8_12; }
	inline Action_1_t2694291500 ** get_address_of_U3CU3Ef__amU24cache8_12() { return &___U3CU3Ef__amU24cache8_12; }
	inline void set_U3CU3Ef__amU24cache8_12(Action_1_t2694291500 * value)
	{
		___U3CU3Ef__amU24cache8_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_13() { return static_cast<int32_t>(offsetof(GoogleCloudMessageService_t984991750_StaticFields, ___U3CU3Ef__amU24cache9_13)); }
	inline Action_2_t2514582953 * get_U3CU3Ef__amU24cache9_13() const { return ___U3CU3Ef__amU24cache9_13; }
	inline Action_2_t2514582953 ** get_address_of_U3CU3Ef__amU24cache9_13() { return &___U3CU3Ef__amU24cache9_13; }
	inline void set_U3CU3Ef__amU24cache9_13(Action_2_t2514582953 * value)
	{
		___U3CU3Ef__amU24cache9_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_14() { return static_cast<int32_t>(offsetof(GoogleCloudMessageService_t984991750_StaticFields, ___U3CU3Ef__amU24cacheA_14)); }
	inline Action_3_t1218892226 * get_U3CU3Ef__amU24cacheA_14() const { return ___U3CU3Ef__amU24cacheA_14; }
	inline Action_3_t1218892226 ** get_address_of_U3CU3Ef__amU24cacheA_14() { return &___U3CU3Ef__amU24cacheA_14; }
	inline void set_U3CU3Ef__amU24cacheA_14(Action_3_t1218892226 * value)
	{
		___U3CU3Ef__amU24cacheA_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(GoogleCloudMessageService_t984991750_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Action_2_t2514582953 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Action_2_t2514582953 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Action_2_t2514582953 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
