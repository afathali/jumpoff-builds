﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// AndroidInventory
struct AndroidInventory_t701010211;
// System.Action`1<BillingResult>
struct Action_1_t3313641232;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3446286029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidInAppPurchaseManager
struct  AndroidInAppPurchaseManager_t2155442111  : public SA_Singleton_OLD_1_t3446286029
{
public:
	// System.String AndroidInAppPurchaseManager::_processedSKU
	String_t* ____processedSKU_4;
	// AndroidInventory AndroidInAppPurchaseManager::_inventory
	AndroidInventory_t701010211 * ____inventory_5;
	// System.Boolean AndroidInAppPurchaseManager::_IsConnectingToServiceInProcess
	bool ____IsConnectingToServiceInProcess_6;
	// System.Boolean AndroidInAppPurchaseManager::_IsProductRetrievingInProcess
	bool ____IsProductRetrievingInProcess_7;
	// System.Boolean AndroidInAppPurchaseManager::_IsConnected
	bool ____IsConnected_8;
	// System.Boolean AndroidInAppPurchaseManager::_IsInventoryLoaded
	bool ____IsInventoryLoaded_9;

public:
	inline static int32_t get_offset_of__processedSKU_4() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111, ____processedSKU_4)); }
	inline String_t* get__processedSKU_4() const { return ____processedSKU_4; }
	inline String_t** get_address_of__processedSKU_4() { return &____processedSKU_4; }
	inline void set__processedSKU_4(String_t* value)
	{
		____processedSKU_4 = value;
		Il2CppCodeGenWriteBarrier(&____processedSKU_4, value);
	}

	inline static int32_t get_offset_of__inventory_5() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111, ____inventory_5)); }
	inline AndroidInventory_t701010211 * get__inventory_5() const { return ____inventory_5; }
	inline AndroidInventory_t701010211 ** get_address_of__inventory_5() { return &____inventory_5; }
	inline void set__inventory_5(AndroidInventory_t701010211 * value)
	{
		____inventory_5 = value;
		Il2CppCodeGenWriteBarrier(&____inventory_5, value);
	}

	inline static int32_t get_offset_of__IsConnectingToServiceInProcess_6() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111, ____IsConnectingToServiceInProcess_6)); }
	inline bool get__IsConnectingToServiceInProcess_6() const { return ____IsConnectingToServiceInProcess_6; }
	inline bool* get_address_of__IsConnectingToServiceInProcess_6() { return &____IsConnectingToServiceInProcess_6; }
	inline void set__IsConnectingToServiceInProcess_6(bool value)
	{
		____IsConnectingToServiceInProcess_6 = value;
	}

	inline static int32_t get_offset_of__IsProductRetrievingInProcess_7() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111, ____IsProductRetrievingInProcess_7)); }
	inline bool get__IsProductRetrievingInProcess_7() const { return ____IsProductRetrievingInProcess_7; }
	inline bool* get_address_of__IsProductRetrievingInProcess_7() { return &____IsProductRetrievingInProcess_7; }
	inline void set__IsProductRetrievingInProcess_7(bool value)
	{
		____IsProductRetrievingInProcess_7 = value;
	}

	inline static int32_t get_offset_of__IsConnected_8() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111, ____IsConnected_8)); }
	inline bool get__IsConnected_8() const { return ____IsConnected_8; }
	inline bool* get_address_of__IsConnected_8() { return &____IsConnected_8; }
	inline void set__IsConnected_8(bool value)
	{
		____IsConnected_8 = value;
	}

	inline static int32_t get_offset_of__IsInventoryLoaded_9() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111, ____IsInventoryLoaded_9)); }
	inline bool get__IsInventoryLoaded_9() const { return ____IsInventoryLoaded_9; }
	inline bool* get_address_of__IsInventoryLoaded_9() { return &____IsInventoryLoaded_9; }
	inline void set__IsInventoryLoaded_9(bool value)
	{
		____IsInventoryLoaded_9 = value;
	}
};

struct AndroidInAppPurchaseManager_t2155442111_StaticFields
{
public:
	// System.Action`1<BillingResult> AndroidInAppPurchaseManager::ActionProductPurchased
	Action_1_t3313641232 * ___ActionProductPurchased_10;
	// System.Action`1<BillingResult> AndroidInAppPurchaseManager::ActionProductConsumed
	Action_1_t3313641232 * ___ActionProductConsumed_11;
	// System.Action`1<BillingResult> AndroidInAppPurchaseManager::ActionBillingSetupFinished
	Action_1_t3313641232 * ___ActionBillingSetupFinished_12;
	// System.Action`1<BillingResult> AndroidInAppPurchaseManager::ActionRetrieveProducsFinished
	Action_1_t3313641232 * ___ActionRetrieveProducsFinished_13;
	// System.Action`1<BillingResult> AndroidInAppPurchaseManager::<>f__am$cacheA
	Action_1_t3313641232 * ___U3CU3Ef__amU24cacheA_14;
	// System.Action`1<BillingResult> AndroidInAppPurchaseManager::<>f__am$cacheB
	Action_1_t3313641232 * ___U3CU3Ef__amU24cacheB_15;
	// System.Action`1<BillingResult> AndroidInAppPurchaseManager::<>f__am$cacheC
	Action_1_t3313641232 * ___U3CU3Ef__amU24cacheC_16;
	// System.Action`1<BillingResult> AndroidInAppPurchaseManager::<>f__am$cacheD
	Action_1_t3313641232 * ___U3CU3Ef__amU24cacheD_17;

public:
	inline static int32_t get_offset_of_ActionProductPurchased_10() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111_StaticFields, ___ActionProductPurchased_10)); }
	inline Action_1_t3313641232 * get_ActionProductPurchased_10() const { return ___ActionProductPurchased_10; }
	inline Action_1_t3313641232 ** get_address_of_ActionProductPurchased_10() { return &___ActionProductPurchased_10; }
	inline void set_ActionProductPurchased_10(Action_1_t3313641232 * value)
	{
		___ActionProductPurchased_10 = value;
		Il2CppCodeGenWriteBarrier(&___ActionProductPurchased_10, value);
	}

	inline static int32_t get_offset_of_ActionProductConsumed_11() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111_StaticFields, ___ActionProductConsumed_11)); }
	inline Action_1_t3313641232 * get_ActionProductConsumed_11() const { return ___ActionProductConsumed_11; }
	inline Action_1_t3313641232 ** get_address_of_ActionProductConsumed_11() { return &___ActionProductConsumed_11; }
	inline void set_ActionProductConsumed_11(Action_1_t3313641232 * value)
	{
		___ActionProductConsumed_11 = value;
		Il2CppCodeGenWriteBarrier(&___ActionProductConsumed_11, value);
	}

	inline static int32_t get_offset_of_ActionBillingSetupFinished_12() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111_StaticFields, ___ActionBillingSetupFinished_12)); }
	inline Action_1_t3313641232 * get_ActionBillingSetupFinished_12() const { return ___ActionBillingSetupFinished_12; }
	inline Action_1_t3313641232 ** get_address_of_ActionBillingSetupFinished_12() { return &___ActionBillingSetupFinished_12; }
	inline void set_ActionBillingSetupFinished_12(Action_1_t3313641232 * value)
	{
		___ActionBillingSetupFinished_12 = value;
		Il2CppCodeGenWriteBarrier(&___ActionBillingSetupFinished_12, value);
	}

	inline static int32_t get_offset_of_ActionRetrieveProducsFinished_13() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111_StaticFields, ___ActionRetrieveProducsFinished_13)); }
	inline Action_1_t3313641232 * get_ActionRetrieveProducsFinished_13() const { return ___ActionRetrieveProducsFinished_13; }
	inline Action_1_t3313641232 ** get_address_of_ActionRetrieveProducsFinished_13() { return &___ActionRetrieveProducsFinished_13; }
	inline void set_ActionRetrieveProducsFinished_13(Action_1_t3313641232 * value)
	{
		___ActionRetrieveProducsFinished_13 = value;
		Il2CppCodeGenWriteBarrier(&___ActionRetrieveProducsFinished_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_14() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111_StaticFields, ___U3CU3Ef__amU24cacheA_14)); }
	inline Action_1_t3313641232 * get_U3CU3Ef__amU24cacheA_14() const { return ___U3CU3Ef__amU24cacheA_14; }
	inline Action_1_t3313641232 ** get_address_of_U3CU3Ef__amU24cacheA_14() { return &___U3CU3Ef__amU24cacheA_14; }
	inline void set_U3CU3Ef__amU24cacheA_14(Action_1_t3313641232 * value)
	{
		___U3CU3Ef__amU24cacheA_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Action_1_t3313641232 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Action_1_t3313641232 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Action_1_t3313641232 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_16() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111_StaticFields, ___U3CU3Ef__amU24cacheC_16)); }
	inline Action_1_t3313641232 * get_U3CU3Ef__amU24cacheC_16() const { return ___U3CU3Ef__amU24cacheC_16; }
	inline Action_1_t3313641232 ** get_address_of_U3CU3Ef__amU24cacheC_16() { return &___U3CU3Ef__amU24cacheC_16; }
	inline void set_U3CU3Ef__amU24cacheC_16(Action_1_t3313641232 * value)
	{
		___U3CU3Ef__amU24cacheC_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_17() { return static_cast<int32_t>(offsetof(AndroidInAppPurchaseManager_t2155442111_StaticFields, ___U3CU3Ef__amU24cacheD_17)); }
	inline Action_1_t3313641232 * get_U3CU3Ef__amU24cacheD_17() const { return ___U3CU3Ef__amU24cacheD_17; }
	inline Action_1_t3313641232 ** get_address_of_U3CU3Ef__amU24cacheD_17() { return &___U3CU3Ef__amU24cacheD_17; }
	inline void set_U3CU3Ef__amU24cacheD_17(Action_1_t3313641232 * value)
	{
		___U3CU3Ef__amU24cacheD_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
