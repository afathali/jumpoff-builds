﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CK_RecordID
struct CK_RecordID_t41838833;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void CK_RecordID::.ctor(System.String)
extern "C"  void CK_RecordID__ctor_m737169830 (CK_RecordID_t41838833 * __this, String_t* ___recordName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_RecordID::.cctor()
extern "C"  void CK_RecordID__cctor_m604959377 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CK_RecordID::get_Name()
extern "C"  String_t* CK_RecordID_get_Name_m1621459981 (CK_RecordID_t41838833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CK_RecordID::get_Internal_Id()
extern "C"  int32_t CK_RecordID_get_Internal_Id_m2114877166 (CK_RecordID_t41838833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CK_RecordID CK_RecordID::GetRecordIdByInternalId(System.Int32)
extern "C"  CK_RecordID_t41838833 * CK_RecordID_GetRecordIdByInternalId_m811518148 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
