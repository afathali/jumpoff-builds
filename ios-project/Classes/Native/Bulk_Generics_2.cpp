﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t1579458414;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>
struct Comparer_1_t3279133958;
// System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>
struct Comparer_1_t388207033;
// System.Collections.Generic.Comparer`1<System.TimeSpan>
struct Comparer_1_t2320268068;
// System.Collections.Generic.Comparer`1<UnityEngine.Color32>
struct Comparer_1_t4059493933;
// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>
struct Comparer_1_t3206162791;
// System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>
struct Comparer_1_t2557554667;
// System.Collections.Generic.Comparer`1<UnityEngine.RuntimePlatform>
struct Comparer_1_t759594086;
// System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>
struct Comparer_1_t1946645919;
// System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
struct Comparer_1_t2511286993;
// System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>
struct Comparer_1_t94267937;
// System.Collections.Generic.Comparer`1<UnityEngine.Vector2>
struct Comparer_1_t1133716698;
// System.Collections.Generic.Comparer`1<UnityEngine.Vector3>
struct Comparer_1_t1133716699;
// System.Collections.Generic.Comparer`1<UnityEngine.Vector4>
struct Comparer_1_t1133716700;
// System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>
struct Dictionary_2_t1356696369;
// System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>
struct Dictionary_2_t2777568976;
// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct Dictionary_2_t2336777489;
// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct Dictionary_2_t2152006384;
// System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct Dictionary_2_t2358566078;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1079703083;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1697274930;
// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>
struct Dictionary_2_t1334805305;
// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct Dictionary_2_t4107228356;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3417634846;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t3925882069;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1663937576;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AndroidStore,System.Object>
struct Dictionary_2_t2809368284;
// System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>
struct KeyCollection_t3840194140;
// System.Collections.Generic.IEnumerator`1<AN_ManifestPermission>
struct IEnumerator_1_t3542601325;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,AN_ManifestPermission>
struct Transform_1_t2201006514;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// AN_ManifestPermission[]
struct AN_ManifestPermissionU5BU5D_t246101855;
// System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>
struct KeyCollection_t966099451;
// System.Collections.Generic.IEnumerator`1<FB_ProfileImageSize>
struct IEnumerator_1_t478851957;
// System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,FB_ProfileImageSize>
struct Transform_1_t1576166847;
// FB_ProfileImageSize[]
struct FB_ProfileImageSizeU5BU5D_t4110693687;
// System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct KeyCollection_t525307964;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct IEnumerator_1_t530586504;
// System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct Transform_1_t2880512365;
// Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey[]
struct TypeNameKeyU5BU5D_t3669902200;
// System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct KeyCollection_t340536859;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct IEnumerator_1_t3558973909;
// System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct Transform_1_t1610273663;
// Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey[]
struct TypeConvertKeyU5BU5D_t3485131095;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct KeyCollection_t547096553;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEnumerator_1_t607539428;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct Transform_1_t4217319880;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t14295340;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t3563200854;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>
struct Transform_1_t2737799006;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>
struct KeyCollection_t4180772701;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>
struct Transform_1_t3322033499;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>
struct KeyCollection_t3818303076;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Object>
struct Transform_1_t1390570591;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct KeyCollection_t2295758831;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Object>
struct Transform_1_t3906118728;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>
struct KeyCollection_t1606165321;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>
struct Transform_1_t1854428742;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct KeyCollection_t2114412544;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>
struct Transform_1_t1141314771;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>
struct KeyCollection_t4147435347;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>
struct Transform_1_t1316794068;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t470039898;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>
struct Transform_1_t1901028561;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>
struct KeyCollection_t997898759;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.AndroidStore>
struct IEnumerator_1_t678579033;
// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Purchasing.AndroidStore,System.Object,UnityEngine.Purchasing.AndroidStore>
struct Transform_1_t572557287;
// UnityEngine.Purchasing.AndroidStore[]
struct AndroidStoreU5BU5D_t4142492995;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>
struct ShimEnumerator_t1461821190;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>
struct ShimEnumerator_t2882693797;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct ShimEnumerator_t2441902310;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct ShimEnumerator_t2257131205;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct ShimEnumerator_t2463690899;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>
struct ShimEnumerator_t1184827904;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t1802399751;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>
struct ShimEnumerator_t1439930126;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct ShimEnumerator_t4212353177;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>
struct ShimEnumerator_t3522759667;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ShimEnumerator_t4031006890;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>
struct ShimEnumerator_t1769062397;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t2386634244;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AndroidStore,System.Object>
struct ShimEnumerator_t2914493105;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,AN_PermissionState>
struct Transform_1_t1267097536;
// System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,System.Collections.DictionaryEntry>
struct Transform_1_t3477771710;
// System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>
struct Transform_1_t3837905199;
// System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t1621714115;
// System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>
struct Transform_1_t3402720211;
// System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Object>
struct Transform_1_t1262288012;
// System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2874325086;
// System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>
struct Transform_1_t4214539695;
// System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Object>
struct Transform_1_t2514898983;
// System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2870666275;
// System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>
struct Transform_1_t4026109779;
// System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Object>
struct Transform_1_t2511240172;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t4134179677;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>
struct Transform_1_t1201215579;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Int32>
struct Transform_1_t3157181727;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t3714796956;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct Transform_1_t3797937159;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t4064153;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Transform_1_t704776203;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>
struct Transform_1_t3939605346;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,Newtonsoft.Json.Schema.JsonSchemaType>
struct Transform_1_t443866473;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.DictionaryEntry>
struct Transform_1_t1749996694;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>
struct Transform_1_t2088239119;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Activator1850728717MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1178719528.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1178719528MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3279133958.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3279133958MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2878395072.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2878395072MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen388207033.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen388207033MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def4282435443.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def4282435443MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2320268068.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2320268068MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1919529182.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1919529182MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen4059493933.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen4059493933MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3658755047.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3658755047MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3206162791.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3206162791MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2805423905.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2805423905MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2557554667.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2557554667MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2156815781.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2156815781MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen759594086.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen759594086MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa358855200.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa358855200MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1946645919.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1946645919MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1545907033.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def1545907033MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2511286993.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2511286993MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2110548107.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2110548107MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen94267937.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen94267937MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3988496347.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3988496347MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716698.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716698MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977812.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977812MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716699.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716699MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977813.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977813MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716700.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716700MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977814.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977814MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2676721071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2676721071MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1356696369.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23409008887.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23409008887MethodDeclarations.h"
#include "AssemblyU2DCSharp_AN_ManifestPermission1772110202.h"
#include "AssemblyU2DCSharp_AN_PermissionState838201224.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link2723257478.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_ObjectDisposedException2695136451MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4097593678.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4097593678MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2777568976.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_534914198.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_534914198MethodDeclarations.h"
#include "AssemblyU2DCSharp_FB_ProfileImageSize3003328130.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3656802191.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3656802191MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2336777489.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g94122711.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g94122711MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De3055062677.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3472031086.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3472031086MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2152006384.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24204318902.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24204318902MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Conver1788482786.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3678590780.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3678590780MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2358566078.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_115911300.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_115911300MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2399727785.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2399727785MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1079703083.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1697274930.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2654830007.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2654830007MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1334805305.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23387117823.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23387117823MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1132285762.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1132285762MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4107228356.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21864573578.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21864573578MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso220200932.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En442692252.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En442692252MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3417634846.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En950939475.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En950939475MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3925882069.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21683227291.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21683227291MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2983962278.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2983962278MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1663937576.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4129392986.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4129392986MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2809368284.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_566713506.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_566713506MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_AndroidStore3203055206.h"
#include "Stores_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4046199807.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4046199807MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1356696369MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1172105118.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1172105118MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2777568976MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke731313631.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke731313631MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2336777489MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke546542526.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke546542526MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2152006384MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke753102220.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke753102220MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2358566078MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3769206521.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3769206521MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1079703083MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key91811072.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key91811072MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1697274930MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4024308743.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4024308743MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1334805305MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2501764498.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2501764498MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4107228356MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1812170988.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1812170988MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3417634846MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2320418211.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2320418211MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3925882069MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key58473718.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key58473718MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1663937576MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke676045565.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke676045565MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1203904426.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1203904426MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2809368284MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3840194140.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3840194140MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2201006514.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2201006514MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke966099451.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke966099451MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1576166847.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1576166847MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke525307964.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke525307964MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2880512365.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2880512365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke340536859.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke340536859MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1610273663.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1610273663MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke547096553.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke547096553MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4217319880.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4217319880MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3563200854.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3563200854MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2737799006.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2737799006MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4180772701.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4180772701MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3322033499.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3322033499MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3818303076.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3818303076MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1390570591.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1390570591MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2295758831.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2295758831MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3906118728.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3906118728MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1606165321.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1606165321MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1854428742.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1854428742MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2114412544.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2114412544MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1141314771.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1141314771MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4147435347.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4147435347MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1316794068.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1316794068MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke470039898.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke470039898MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1901028561.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1901028561MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke997898759.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke997898759MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr572557287.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr572557287MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1461821190.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1461821190MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2882693797.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2882693797MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2441902310.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2441902310MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2257131205.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2257131205MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2463690899.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2463690899MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1184827904.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1184827904MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1802399751.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1802399751MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1439930126.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1439930126MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S4212353177.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S4212353177MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3522759667.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3522759667MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S4031006890.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S4031006890MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1769062397.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1769062397MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2386634244.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2386634244MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2914493105.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2914493105MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1267097536.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1267097536MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3477771710.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3477771710MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3837905199.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3837905199MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1621714115.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1621714115MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3402720211.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3402720211MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1262288012.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1262288012MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2874325086.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2874325086MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4214539695.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4214539695MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2514898983.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2514898983MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2870666275.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2870666275MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4026109779.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4026109779MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2511240172.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2511240172MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4134179677.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4134179677MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1201215579.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1201215579MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3157181727.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3157181727MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3714796956.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3714796956MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3797937159.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3797937159MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tran4064153.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tran4064153MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr704776203.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr704776203MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3939605346.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3939605346MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr443866473.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr443866473MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1749996694.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1749996694MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2088239119.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2088239119MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::Do_ICollectionCopyTo<AN_ManifestPermission>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisAN_ManifestPermission_t1772110202_m336589133_gshared (Dictionary_2_t1356696369 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t2201006514 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisAN_ManifestPermission_t1772110202_m336589133(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1356696369 *, Il2CppArray *, int32_t, Transform_1_t2201006514 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisAN_ManifestPermission_t1772110202_m336589133_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::Do_CopyTo<AN_ManifestPermission,AN_ManifestPermission>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisAN_ManifestPermission_t1772110202_TisAN_ManifestPermission_t1772110202_m17266899_gshared (Dictionary_2_t1356696369 * __this, AN_ManifestPermissionU5BU5D_t246101855* p0, int32_t p1, Transform_1_t2201006514 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisAN_ManifestPermission_t1772110202_TisAN_ManifestPermission_t1772110202_m17266899(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1356696369 *, AN_ManifestPermissionU5BU5D_t246101855*, int32_t, Transform_1_t2201006514 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisAN_ManifestPermission_t1772110202_TisAN_ManifestPermission_t1772110202_m17266899_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::Do_ICollectionCopyTo<FB_ProfileImageSize>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisFB_ProfileImageSize_t3003328130_m1862267655_gshared (Dictionary_2_t2777568976 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1576166847 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisFB_ProfileImageSize_t3003328130_m1862267655(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2777568976 *, Il2CppArray *, int32_t, Transform_1_t1576166847 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisFB_ProfileImageSize_t3003328130_m1862267655_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::Do_CopyTo<FB_ProfileImageSize,FB_ProfileImageSize>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisFB_ProfileImageSize_t3003328130_TisFB_ProfileImageSize_t3003328130_m3608910687_gshared (Dictionary_2_t2777568976 * __this, FB_ProfileImageSizeU5BU5D_t4110693687* p0, int32_t p1, Transform_1_t1576166847 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisFB_ProfileImageSize_t3003328130_TisFB_ProfileImageSize_t3003328130_m3608910687(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2777568976 *, FB_ProfileImageSizeU5BU5D_t4110693687*, int32_t, Transform_1_t1576166847 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisFB_ProfileImageSize_t3003328130_TisFB_ProfileImageSize_t3003328130_m3608910687_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Do_ICollectionCopyTo<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisTypeNameKey_t3055062677_m3899827955_gshared (Dictionary_2_t2336777489 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t2880512365 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisTypeNameKey_t3055062677_m3899827955(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2336777489 *, Il2CppArray *, int32_t, Transform_1_t2880512365 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisTypeNameKey_t3055062677_m3899827955_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Do_CopyTo<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisTypeNameKey_t3055062677_TisTypeNameKey_t3055062677_m3156700672_gshared (Dictionary_2_t2336777489 * __this, TypeNameKeyU5BU5D_t3669902200* p0, int32_t p1, Transform_1_t2880512365 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisTypeNameKey_t3055062677_TisTypeNameKey_t3055062677_m3156700672(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2336777489 *, TypeNameKeyU5BU5D_t3669902200*, int32_t, Transform_1_t2880512365 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisTypeNameKey_t3055062677_TisTypeNameKey_t3055062677_m3156700672_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::Do_ICollectionCopyTo<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisTypeConvertKey_t1788482786_m3166217855_gshared (Dictionary_2_t2152006384 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1610273663 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisTypeConvertKey_t1788482786_m3166217855(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2152006384 *, Il2CppArray *, int32_t, Transform_1_t1610273663 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisTypeConvertKey_t1788482786_m3166217855_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::Do_CopyTo<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisTypeConvertKey_t1788482786_TisTypeConvertKey_t1788482786_m3494350733_gshared (Dictionary_2_t2152006384 * __this, TypeConvertKeyU5BU5D_t3485131095* p0, int32_t p1, Transform_1_t1610273663 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisTypeConvertKey_t1788482786_TisTypeConvertKey_t1788482786_m3494350733(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2152006384 *, TypeConvertKeyU5BU5D_t3485131095*, int32_t, Transform_1_t1610273663 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisTypeConvertKey_t1788482786_TisTypeConvertKey_t1788482786_m3494350733_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3132015601_m3138599314_gshared (Dictionary_2_t2358566078 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t4217319880 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3132015601_m3138599314(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2358566078 *, Il2CppArray *, int32_t, Transform_1_t4217319880 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3132015601_m3138599314_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3132015601_TisKeyValuePair_2_t3132015601_m3996158107_gshared (Dictionary_2_t2358566078 * __this, KeyValuePair_2U5BU5D_t14295340* p0, int32_t p1, Transform_1_t4217319880 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3132015601_TisKeyValuePair_2_t3132015601_m3996158107(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2358566078 *, KeyValuePair_2U5BU5D_t14295340*, int32_t, Transform_1_t4217319880 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3132015601_TisKeyValuePair_2_t3132015601_m3996158107_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Do_ICollectionCopyTo<System.Int32>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m4216657666_gshared (Dictionary_2_t1079703083 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t2737799006 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m4216657666(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, Transform_1_t2737799006 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m4216657666_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Do_CopyTo<System.Int32,System.Int32>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m2922514531_gshared (Dictionary_2_t1079703083 * __this, Int32U5BU5D_t3030399641* p0, int32_t p1, Transform_1_t2737799006 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m2922514531(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1079703083 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t2737799006 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m2922514531_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Do_ICollectionCopyTo<System.Int32>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m1526703923_gshared (Dictionary_2_t1697274930 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t3322033499 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m1526703923(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, Transform_1_t3322033499 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m1526703923_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Do_CopyTo<System.Int32,System.Int32>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m838060550_gshared (Dictionary_2_t1697274930 * __this, Int32U5BU5D_t3030399641* p0, int32_t p1, Transform_1_t3322033499 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m838060550(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1697274930 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t3322033499 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m838060550_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1676305770_gshared (Dictionary_2_t1334805305 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1390570591 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1676305770(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1334805305 *, Il2CppArray *, int32_t, Transform_1_t1390570591 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1676305770_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m62955902_gshared (Dictionary_2_t1334805305 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t1390570591 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m62955902(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1334805305 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1390570591 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m62955902_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4150014057_gshared (Dictionary_2_t4107228356 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t3906118728 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4150014057(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t4107228356 *, Il2CppArray *, int32_t, Transform_1_t3906118728 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4150014057_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1685784117_gshared (Dictionary_2_t4107228356 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t3906118728 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1685784117(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t4107228356 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t3906118728 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1685784117_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1664293864_gshared (Dictionary_2_t3417634846 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1854428742 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1664293864(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3417634846 *, Il2CppArray *, int32_t, Transform_1_t1854428742 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1664293864_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3704460300_gshared (Dictionary_2_t3417634846 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t1854428742 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3704460300(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3417634846 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1854428742 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3704460300_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2664518956_gshared (Dictionary_2_t3925882069 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1141314771 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2664518956(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3925882069 *, Il2CppArray *, int32_t, Transform_1_t1141314771 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2664518956_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3563626296_gshared (Dictionary_2_t3925882069 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t1141314771 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3563626296(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3925882069 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1141314771 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3563626296_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m63223048_gshared (Dictionary_2_t1663937576 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1316794068 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m63223048(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1663937576 *, Il2CppArray *, int32_t, Transform_1_t1316794068 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m63223048_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3552744036_gshared (Dictionary_2_t1663937576 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t1316794068 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3552744036(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1663937576 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1316794068 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3552744036_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4092802079_gshared (Dictionary_2_t2281509423 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1901028561 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4092802079(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2281509423 *, Il2CppArray *, int32_t, Transform_1_t1901028561 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4092802079_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1804181923_gshared (Dictionary_2_t2281509423 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t1901028561 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1804181923(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2281509423 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1901028561 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1804181923_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AndroidStore,System.Object>::Do_ICollectionCopyTo<UnityEngine.Purchasing.AndroidStore>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisAndroidStore_t3203055206_m424822243_gshared (Dictionary_2_t2809368284 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t572557287 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisAndroidStore_t3203055206_m424822243(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2809368284 *, Il2CppArray *, int32_t, Transform_1_t572557287 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisAndroidStore_t3203055206_m424822243_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AndroidStore,System.Object>::Do_CopyTo<UnityEngine.Purchasing.AndroidStore,UnityEngine.Purchasing.AndroidStore>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisAndroidStore_t3203055206_TisAndroidStore_t3203055206_m3207515728_gshared (Dictionary_2_t2809368284 * __this, AndroidStoreU5BU5D_t4142492995* p0, int32_t p1, Transform_1_t572557287 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisAndroidStore_t3203055206_TisAndroidStore_t3203055206_m3207515728(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2809368284 *, AndroidStoreU5BU5D_t4142492995*, int32_t, Transform_1_t572557287 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisAndroidStore_t3203055206_TisAndroidStore_t3203055206_m3207515728_gshared)(__this, p0, p1, p2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.Comparer`1<System.Object>::.ctor()
extern "C"  void Comparer_1__ctor_m4082958187_gshared (Comparer_1_t1579458414 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Object>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2962395036_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2962395036_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2962395036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1579458414_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1579458414 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1178719528 * L_8 = (DefaultComparer_t1178719528 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1178719528 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1579458414_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Object>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m872902762_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m872902762_gshared (Comparer_1_t1579458414 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m872902762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1579458414 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Object>::Compare(T,T) */, (Comparer_1_t1579458414 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Object>::get_Default()
extern "C"  Comparer_1_t1579458414 * Comparer_1_get_Default_m40106963_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1579458414 * L_0 = ((Comparer_1_t1579458414_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void Comparer_1__ctor_m844571340_gshared (Comparer_1_t3279133958 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3112251759_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3112251759_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3112251759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3279133958_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t3279133958 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2878395072 * L_8 = (DefaultComparer_t2878395072 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2878395072 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3279133958_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m3203078743_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3203078743_gshared (Comparer_1_t3279133958 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m3203078743_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t3279133958 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, CustomAttributeNamedArgument_t94157543 , CustomAttributeNamedArgument_t94157543  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::Compare(T,T) */, (Comparer_1_t3279133958 *)__this, (CustomAttributeNamedArgument_t94157543 )((*(CustomAttributeNamedArgument_t94157543 *)((CustomAttributeNamedArgument_t94157543 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (CustomAttributeNamedArgument_t94157543 )((*(CustomAttributeNamedArgument_t94157543 *)((CustomAttributeNamedArgument_t94157543 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeNamedArgument>::get_Default()
extern "C"  Comparer_1_t3279133958 * Comparer_1_get_Default_m2605397692_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3279133958 * L_0 = ((Comparer_1_t3279133958_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void Comparer_1__ctor_m2364183619_gshared (Comparer_1_t388207033 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m580294992_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m580294992_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m580294992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t388207033_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t388207033 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t4282435443 * L_8 = (DefaultComparer_t4282435443 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t4282435443 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t388207033_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1635186002_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1635186002_gshared (Comparer_1_t388207033 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1635186002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t388207033 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, CustomAttributeTypedArgument_t1498197914 , CustomAttributeTypedArgument_t1498197914  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::Compare(T,T) */, (Comparer_1_t388207033 *)__this, (CustomAttributeTypedArgument_t1498197914 )((*(CustomAttributeTypedArgument_t1498197914 *)((CustomAttributeTypedArgument_t1498197914 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (CustomAttributeTypedArgument_t1498197914 )((*(CustomAttributeTypedArgument_t1498197914 *)((CustomAttributeTypedArgument_t1498197914 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Reflection.CustomAttributeTypedArgument>::get_Default()
extern "C"  Comparer_1_t388207033 * Comparer_1_get_Default_m3643271627_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t388207033 * L_0 = ((Comparer_1_t388207033_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.TimeSpan>::.ctor()
extern "C"  void Comparer_1__ctor_m2195903267_gshared (Comparer_1_t2320268068 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.TimeSpan>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2494715342_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2494715342_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2494715342_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2320268068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2320268068 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1919529182 * L_8 = (DefaultComparer_t1919529182 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1919529182 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2320268068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.TimeSpan>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2490067344_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2490067344_gshared (Comparer_1_t2320268068 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2490067344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2320268068 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, TimeSpan_t3430258949 , TimeSpan_t3430258949  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.TimeSpan>::Compare(T,T) */, (Comparer_1_t2320268068 *)__this, (TimeSpan_t3430258949 )((*(TimeSpan_t3430258949 *)((TimeSpan_t3430258949 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (TimeSpan_t3430258949 )((*(TimeSpan_t3430258949 *)((TimeSpan_t3430258949 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.TimeSpan>::get_Default()
extern "C"  Comparer_1_t2320268068 * Comparer_1_get_Default_m2204997355_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2320268068 * L_0 = ((Comparer_1_t2320268068_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Color32>::.ctor()
extern "C"  void Comparer_1__ctor_m2264852056_gshared (Comparer_1_t4059493933 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Color32>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m179359609_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m179359609_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m179359609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t4059493933_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t4059493933 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3658755047 * L_8 = (DefaultComparer_t3658755047 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3658755047 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t4059493933_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Color32>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2785607073_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2785607073_gshared (Comparer_1_t4059493933 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2785607073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t4059493933 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Color32_t874517518 , Color32_t874517518  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Color32>::Compare(T,T) */, (Comparer_1_t4059493933 *)__this, (Color32_t874517518 )((*(Color32_t874517518 *)((Color32_t874517518 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Color32_t874517518 )((*(Color32_t874517518 *)((Color32_t874517518 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Color32>::get_Default()
extern "C"  Comparer_1_t4059493933 * Comparer_1_get_Default_m1826646524_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t4059493933 * L_0 = ((Comparer_1_t4059493933_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C"  void Comparer_1__ctor_m1728777074_gshared (Comparer_1_t3206162791 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3237813171_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3237813171_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3237813171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3206162791_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t3206162791 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2805423905 * L_8 = (DefaultComparer_t2805423905 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2805423905 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3206162791_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1153499515_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1153499515_gshared (Comparer_1_t3206162791 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1153499515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t3206162791 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, RaycastResult_t21186376 , RaycastResult_t21186376  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::Compare(T,T) */, (Comparer_1_t3206162791 *)__this, (RaycastResult_t21186376 )((*(RaycastResult_t21186376 *)((RaycastResult_t21186376 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (RaycastResult_t21186376 )((*(RaycastResult_t21186376 *)((RaycastResult_t21186376 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>::get_Default()
extern "C"  Comparer_1_t3206162791 * Comparer_1_get_Default_m4282764954_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3206162791 * L_0 = ((Comparer_1_t3206162791_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::.ctor()
extern "C"  void Comparer_1__ctor_m105573688_gshared (Comparer_1_t2557554667 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1146589207_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1146589207_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1146589207_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2557554667_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2557554667 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2156815781 * L_8 = (DefaultComparer_t2156815781 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2156815781 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2557554667_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m4240302767_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m4240302767_gshared (Comparer_1_t2557554667 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m4240302767_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2557554667 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Playable_t3667545548 , Playable_t3667545548  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::Compare(T,T) */, (Comparer_1_t2557554667 *)__this, (Playable_t3667545548 )((*(Playable_t3667545548 *)((Playable_t3667545548 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Playable_t3667545548 )((*(Playable_t3667545548 *)((Playable_t3667545548 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Experimental.Director.Playable>::get_Default()
extern "C"  Comparer_1_t2557554667 * Comparer_1_get_Default_m1078895976_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2557554667 * L_0 = ((Comparer_1_t2557554667_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.RuntimePlatform>::.ctor()
extern "C"  void Comparer_1__ctor_m2685011545_gshared (Comparer_1_t759594086 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.RuntimePlatform>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1057157428_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1057157428_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1057157428_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t759594086_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t759594086 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t358855200 * L_8 = (DefaultComparer_t358855200 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t358855200 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t759594086_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.RuntimePlatform>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m479919198_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m479919198_gshared (Comparer_1_t759594086 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m479919198_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t759594086 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.RuntimePlatform>::Compare(T,T) */, (Comparer_1_t759594086 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.RuntimePlatform>::get_Default()
extern "C"  Comparer_1_t759594086 * Comparer_1_get_Default_m2285864681_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t759594086 * L_0 = ((Comparer_1_t759594086_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::.ctor()
extern "C"  void Comparer_1__ctor_m1184061702_gshared (Comparer_1_t1946645919 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3069041651_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3069041651_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3069041651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1946645919_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1946645919 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1545907033 * L_8 = (DefaultComparer_t1545907033 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1545907033 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1946645919_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1621919467_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1621919467_gshared (Comparer_1_t1946645919 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1621919467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1946645919 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, UICharInfo_t3056636800 , UICharInfo_t3056636800  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::Compare(T,T) */, (Comparer_1_t1946645919 *)__this, (UICharInfo_t3056636800 )((*(UICharInfo_t3056636800 *)((UICharInfo_t3056636800 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UICharInfo_t3056636800 )((*(UICharInfo_t3056636800 *)((UICharInfo_t3056636800 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::get_Default()
extern "C"  Comparer_1_t1946645919 * Comparer_1_get_Default_m91842798_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1946645919 * L_0 = ((Comparer_1_t1946645919_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.ctor()
extern "C"  void Comparer_1__ctor_m806168336_gshared (Comparer_1_t2511286993 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3996541505_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3996541505_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3996541505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2511286993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2511286993 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2110548107 * L_8 = (DefaultComparer_t2110548107 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2110548107 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2511286993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2964757477_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2964757477_gshared (Comparer_1_t2511286993 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2964757477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2511286993 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, UILineInfo_t3621277874 , UILineInfo_t3621277874  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::Compare(T,T) */, (Comparer_1_t2511286993 *)__this, (UILineInfo_t3621277874 )((*(UILineInfo_t3621277874 *)((UILineInfo_t3621277874 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UILineInfo_t3621277874 )((*(UILineInfo_t3621277874 *)((UILineInfo_t3621277874 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::get_Default()
extern "C"  Comparer_1_t2511286993 * Comparer_1_get_Default_m501796660_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2511286993 * L_0 = ((Comparer_1_t2511286993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.ctor()
extern "C"  void Comparer_1__ctor_m1157133632_gshared (Comparer_1_t94267937 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m4067993089_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m4067993089_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m4067993089_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t94267937_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t94267937 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3988496347 * L_8 = (DefaultComparer_t3988496347 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3988496347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t94267937_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2324509253_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2324509253_gshared (Comparer_1_t94267937 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2324509253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t94267937 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, UIVertex_t1204258818 , UIVertex_t1204258818  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::Compare(T,T) */, (Comparer_1_t94267937 *)__this, (UIVertex_t1204258818 )((*(UIVertex_t1204258818 *)((UIVertex_t1204258818 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UIVertex_t1204258818 )((*(UIVertex_t1204258818 *)((UIVertex_t1204258818 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::get_Default()
extern "C"  Comparer_1_t94267937 * Comparer_1_get_Default_m1960140044_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t94267937 * L_0 = ((Comparer_1_t94267937_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::.ctor()
extern "C"  void Comparer_1__ctor_m2941434245_gshared (Comparer_1_t1133716698 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2253684996_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2253684996_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2253684996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1133716698_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1133716698 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t732977812 * L_8 = (DefaultComparer_t732977812 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t732977812 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1133716698_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m637596782_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m637596782_gshared (Comparer_1_t1133716698 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m637596782_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1133716698 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Vector2_t2243707579 , Vector2_t2243707579  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::Compare(T,T) */, (Comparer_1_t1133716698 *)__this, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::get_Default()
extern "C"  Comparer_1_t1133716698 * Comparer_1_get_Default_m492688901_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1133716698 * L_0 = ((Comparer_1_t1133716698_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::.ctor()
extern "C"  void Comparer_1__ctor_m1169723274_gshared (Comparer_1_t1133716699 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1573451391_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1573451391_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1573451391_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1133716699_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1133716699 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t732977813 * L_8 = (DefaultComparer_t732977813 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t732977813 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1133716699_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2615431023_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2615431023_gshared (Comparer_1_t1133716699 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2615431023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1133716699 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Vector3_t2243707580 , Vector3_t2243707580  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::Compare(T,T) */, (Comparer_1_t1133716699 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::get_Default()
extern "C"  Comparer_1_t1133716699 * Comparer_1_get_Default_m3185432070_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1133716699 * L_0 = ((Comparer_1_t1133716699_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::.ctor()
extern "C"  void Comparer_1__ctor_m4052560291_gshared (Comparer_1_t1133716700 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1911230094_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1911230094_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1911230094_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(89 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1133716700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1133716700 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t732977814 * L_8 = (DefaultComparer_t732977814 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t732977814 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1133716700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m577428976_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m577428976_gshared (Comparer_1_t1133716700 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m577428976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1133716700 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Vector4_t2243707581 , Vector4_t2243707581  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::Compare(T,T) */, (Comparer_1_t1133716700 *)__this, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::get_Default()
extern "C"  Comparer_1_t1133716700 * Comparer_1_get_Default_m48739979_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1133716700 * L_0 = ((Comparer_1_t1133716700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3639829663_gshared (Enumerator_t2676721071 * __this, Dictionary_2_t1356696369 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1356696369 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1356696369 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3639829663_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1356696369 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	Enumerator__ctor_m3639829663(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m707319646_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2881515912((Enumerator_t2676721071 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3409008887  L_0 = (KeyValuePair_2_t3409008887 )__this->get_current_3();
		KeyValuePair_2_t3409008887  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m707319646_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m707319646(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m79201504_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m3948831101((Enumerator_t2676721071 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m79201504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m79201504(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1803753565_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2881515912((Enumerator_t2676721071 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3409008887 * L_0 = (KeyValuePair_2_t3409008887 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m232446462((KeyValuePair_2_t3409008887 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t3409008887 * L_4 = (KeyValuePair_2_t3409008887 *)__this->get_address_of_current_3();
		int32_t L_5 = KeyValuePair_2_get_Value_m1557526483((KeyValuePair_2_t3409008887 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t3048875398  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2901884110(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1803753565_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1803753565(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3208901104_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m4071352643((Enumerator_t2676721071 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3208901104_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3208901104(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3415387586_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m3254424323((Enumerator_t2676721071 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3415387586_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3415387586(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1965426417_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m3727973704((Enumerator_t2676721071 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1356696369 * L_4 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1356696369 * L_8 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		NullCheck(L_8);
		AN_ManifestPermissionU5BU5D_t246101855* L_9 = (AN_ManifestPermissionU5BU5D_t246101855*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1356696369 * L_13 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		NullCheck(L_13);
		AN_PermissionStateU5BU5D_t700971353* L_14 = (AN_PermissionStateU5BU5D_t700971353*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3409008887  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3420454981(&L_18, (int32_t)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1356696369 * L_20 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1965426417_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	return Enumerator_MoveNext_m1965426417(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::get_Current()
extern "C"  KeyValuePair_2_t3409008887  Enumerator_get_Current_m4125464454_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3409008887  L_0 = (KeyValuePair_2_t3409008887 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3409008887  Enumerator_get_Current_m4125464454_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	return Enumerator_get_Current_m4125464454(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m4071352643_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2881515912((Enumerator_t2676721071 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3409008887 * L_0 = (KeyValuePair_2_t3409008887 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m232446462((KeyValuePair_2_t3409008887 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m4071352643_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	return Enumerator_get_CurrentKey_m4071352643(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m3254424323_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2881515912((Enumerator_t2676721071 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3409008887 * L_0 = (KeyValuePair_2_t3409008887 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m1557526483((KeyValuePair_2_t3409008887 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m3254424323_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3254424323(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::Reset()
extern "C"  void Enumerator_Reset_m3948831101_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3727973704((Enumerator_t2676721071 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m3948831101_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	Enumerator_Reset_m3948831101(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m3727973704_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3727973704_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3727973704_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1356696369 * L_0 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1356696369 * L_2 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3727973704_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	Enumerator_VerifyState_m3727973704(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m2881515912_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m2881515912_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m2881515912_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m3727973704((Enumerator_t2676721071 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m2881515912_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	Enumerator_VerifyCurrent_m2881515912(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::Dispose()
extern "C"  void Enumerator_Dispose_m2041202203_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1356696369 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2041202203_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2676721071 * _thisAdjusted = reinterpret_cast<Enumerator_t2676721071 *>(__this + 1);
	Enumerator_Dispose_m2041202203(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m792485719_gshared (Enumerator_t4097593678 * __this, Dictionary_2_t2777568976 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2777568976 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2777568976 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m792485719_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2777568976 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	Enumerator__ctor_m792485719(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3200901816_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3753703020((Enumerator_t4097593678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t534914198  L_0 = (KeyValuePair_2_t534914198 )__this->get_current_3();
		KeyValuePair_2_t534914198  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3200901816_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3200901816(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m330730428_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m812636689((Enumerator_t4097593678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m330730428_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m330730428(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1253008017_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3753703020((Enumerator_t4097593678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t534914198 * L_0 = (KeyValuePair_2_t534914198 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1840008439((KeyValuePair_2_t534914198 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t534914198 * L_4 = (KeyValuePair_2_t534914198 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m1210921623((KeyValuePair_2_t534914198 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1253008017_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1253008017(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3448913414_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m2108057179((Enumerator_t4097593678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3448913414_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3448913414(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m197227828_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m1126126523((Enumerator_t4097593678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m197227828_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m197227828(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3732010148_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m293136976((Enumerator_t4097593678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2777568976 * L_4 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2777568976 * L_8 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		NullCheck(L_8);
		FB_ProfileImageSizeU5BU5D_t4110693687* L_9 = (FB_ProfileImageSizeU5BU5D_t4110693687*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t2777568976 * L_13 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t534914198  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3234979829(&L_18, (int32_t)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t2777568976 * L_20 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3732010148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	return Enumerator_MoveNext_m3732010148(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t534914198  Enumerator_get_Current_m4217501564_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t534914198  L_0 = (KeyValuePair_2_t534914198 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t534914198  Enumerator_get_Current_m4217501564_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	return Enumerator_get_Current_m4217501564(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m2108057179_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3753703020((Enumerator_t4097593678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t534914198 * L_0 = (KeyValuePair_2_t534914198 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1840008439((KeyValuePair_2_t534914198 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m2108057179_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	return Enumerator_get_CurrentKey_m2108057179(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1126126523_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3753703020((Enumerator_t4097593678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t534914198 * L_0 = (KeyValuePair_2_t534914198 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m1210921623((KeyValuePair_2_t534914198 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1126126523_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	return Enumerator_get_CurrentValue_m1126126523(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m812636689_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m293136976((Enumerator_t4097593678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m812636689_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	Enumerator_Reset_m812636689(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m293136976_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m293136976_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m293136976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2777568976 * L_0 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2777568976 * L_2 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m293136976_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	Enumerator_VerifyState_m293136976(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3753703020_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3753703020_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3753703020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m293136976((Enumerator_t4097593678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3753703020_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	Enumerator_VerifyCurrent_m3753703020(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2418656635_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2777568976 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2418656635_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4097593678 * _thisAdjusted = reinterpret_cast<Enumerator_t4097593678 *>(__this + 1);
	Enumerator_Dispose_m2418656635(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1478019578_gshared (Enumerator_t3656802191 * __this, Dictionary_2_t2336777489 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2336777489 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2336777489 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1478019578_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2336777489 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	Enumerator__ctor_m1478019578(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1545882507_gshared (Enumerator_t3656802191 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3565804163((Enumerator_t3656802191 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t94122711  L_0 = (KeyValuePair_2_t94122711 )__this->get_current_3();
		KeyValuePair_2_t94122711  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1545882507_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1545882507(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2545679379_gshared (Enumerator_t3656802191 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1824444848((Enumerator_t3656802191 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2545679379_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2545679379(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1368716030_gshared (Enumerator_t3656802191 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3565804163((Enumerator_t3656802191 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t94122711 * L_0 = (KeyValuePair_2_t94122711 *)__this->get_address_of_current_3();
		TypeNameKey_t3055062677  L_1 = KeyValuePair_2_get_Key_m2430213422((KeyValuePair_2_t94122711 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		TypeNameKey_t3055062677  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t94122711 * L_4 = (KeyValuePair_2_t94122711 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m470622750((KeyValuePair_2_t94122711 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1368716030_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1368716030(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m72549421_gshared (Enumerator_t3656802191 * __this, const MethodInfo* method)
{
	{
		TypeNameKey_t3055062677  L_0 = Enumerator_get_CurrentKey_m3423730762((Enumerator_t3656802191 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		TypeNameKey_t3055062677  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m72549421_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m72549421(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1891998901_gshared (Enumerator_t3656802191 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m1342331410((Enumerator_t3656802191 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1891998901_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1891998901(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1720213575_gshared (Enumerator_t3656802191 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m3004094405((Enumerator_t3656802191 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2336777489 * L_4 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2336777489 * L_8 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		NullCheck(L_8);
		TypeNameKeyU5BU5D_t3669902200* L_9 = (TypeNameKeyU5BU5D_t3669902200*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		TypeNameKey_t3055062677  L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t2336777489 * L_13 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t94122711  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m4047295548(&L_18, (TypeNameKey_t3055062677 )L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t2336777489 * L_20 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1720213575_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	return Enumerator_MoveNext_m1720213575(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t94122711  Enumerator_get_Current_m4237110135_gshared (Enumerator_t3656802191 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t94122711  L_0 = (KeyValuePair_2_t94122711 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t94122711  Enumerator_get_Current_m4237110135_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	return Enumerator_get_Current_m4237110135(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_CurrentKey()
extern "C"  TypeNameKey_t3055062677  Enumerator_get_CurrentKey_m3423730762_gshared (Enumerator_t3656802191 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3565804163((Enumerator_t3656802191 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t94122711 * L_0 = (KeyValuePair_2_t94122711 *)__this->get_address_of_current_3();
		TypeNameKey_t3055062677  L_1 = KeyValuePair_2_get_Key_m2430213422((KeyValuePair_2_t94122711 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  TypeNameKey_t3055062677  Enumerator_get_CurrentKey_m3423730762_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	return Enumerator_get_CurrentKey_m3423730762(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1342331410_gshared (Enumerator_t3656802191 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3565804163((Enumerator_t3656802191 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t94122711 * L_0 = (KeyValuePair_2_t94122711 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m470622750((KeyValuePair_2_t94122711 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1342331410_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	return Enumerator_get_CurrentValue_m1342331410(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1824444848_gshared (Enumerator_t3656802191 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3004094405((Enumerator_t3656802191 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1824444848_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	Enumerator_Reset_m1824444848(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m3004094405_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3004094405_gshared (Enumerator_t3656802191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3004094405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2336777489 * L_0 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2336777489 * L_2 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3004094405_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	Enumerator_VerifyState_m3004094405(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3565804163_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3565804163_gshared (Enumerator_t3656802191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3565804163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m3004094405((Enumerator_t3656802191 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3565804163_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	Enumerator_VerifyCurrent_m3565804163(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1064077118_gshared (Enumerator_t3656802191 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2336777489 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1064077118_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3656802191 * _thisAdjusted = reinterpret_cast<Enumerator_t3656802191 *>(__this + 1);
	Enumerator_Dispose_m1064077118(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m390443469_gshared (Enumerator_t3472031086 * __this, Dictionary_2_t2152006384 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2152006384 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2152006384 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m390443469_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2152006384 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	Enumerator__ctor_m390443469(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m707136600_gshared (Enumerator_t3472031086 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2318507328((Enumerator_t3472031086 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t4204318902  L_0 = (KeyValuePair_2_t4204318902 )__this->get_current_3();
		KeyValuePair_2_t4204318902  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m707136600_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m707136600(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3776366864_gshared (Enumerator_t3472031086 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m62122567((Enumerator_t3472031086 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3776366864_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3776366864(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1184485699_gshared (Enumerator_t3472031086 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2318507328((Enumerator_t3472031086 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t4204318902 * L_0 = (KeyValuePair_2_t4204318902 *)__this->get_address_of_current_3();
		TypeConvertKey_t1788482786  L_1 = KeyValuePair_2_get_Key_m3955619893((KeyValuePair_2_t4204318902 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		TypeConvertKey_t1788482786  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t4204318902 * L_4 = (KeyValuePair_2_t4204318902 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m2877229269((KeyValuePair_2_t4204318902 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1184485699_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1184485699(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4113737478_gshared (Enumerator_t3472031086 * __this, const MethodInfo* method)
{
	{
		TypeConvertKey_t1788482786  L_0 = Enumerator_get_CurrentKey_m3080089425((Enumerator_t3472031086 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		TypeConvertKey_t1788482786  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4113737478_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4113737478(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2829867808_gshared (Enumerator_t3472031086 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m2705484977((Enumerator_t3472031086 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2829867808_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2829867808(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m72413836_gshared (Enumerator_t3472031086 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m4174937680((Enumerator_t3472031086 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2152006384 * L_4 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2152006384 * L_8 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		NullCheck(L_8);
		TypeConvertKeyU5BU5D_t3485131095* L_9 = (TypeConvertKeyU5BU5D_t3485131095*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		TypeConvertKey_t1788482786  L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t2152006384 * L_13 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t4204318902  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1143198019(&L_18, (TypeConvertKey_t1788482786 )L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t2152006384 * L_20 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m72413836_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	return Enumerator_MoveNext_m72413836(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t4204318902  Enumerator_get_Current_m2852501768_gshared (Enumerator_t3472031086 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t4204318902  L_0 = (KeyValuePair_2_t4204318902 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t4204318902  Enumerator_get_Current_m2852501768_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	return Enumerator_get_Current_m2852501768(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_CurrentKey()
extern "C"  TypeConvertKey_t1788482786  Enumerator_get_CurrentKey_m3080089425_gshared (Enumerator_t3472031086 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2318507328((Enumerator_t3472031086 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t4204318902 * L_0 = (KeyValuePair_2_t4204318902 *)__this->get_address_of_current_3();
		TypeConvertKey_t1788482786  L_1 = KeyValuePair_2_get_Key_m3955619893((KeyValuePair_2_t4204318902 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  TypeConvertKey_t1788482786  Enumerator_get_CurrentKey_m3080089425_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	return Enumerator_get_CurrentKey_m3080089425(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m2705484977_gshared (Enumerator_t3472031086 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2318507328((Enumerator_t3472031086 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t4204318902 * L_0 = (KeyValuePair_2_t4204318902 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m2877229269((KeyValuePair_2_t4204318902 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m2705484977_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2705484977(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m62122567_gshared (Enumerator_t3472031086 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m4174937680((Enumerator_t3472031086 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m62122567_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	Enumerator_Reset_m62122567(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m4174937680_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4174937680_gshared (Enumerator_t3472031086 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4174937680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2152006384 * L_0 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2152006384 * L_2 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4174937680_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	Enumerator_VerifyState_m4174937680(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m2318507328_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m2318507328_gshared (Enumerator_t3472031086 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m2318507328_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m4174937680((Enumerator_t3472031086 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m2318507328_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	Enumerator_VerifyCurrent_m2318507328(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m496361693_gshared (Enumerator_t3472031086 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2152006384 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m496361693_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3472031086 * _thisAdjusted = reinterpret_cast<Enumerator_t3472031086 *>(__this + 1);
	Enumerator_Dispose_m496361693(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3614838359_gshared (Enumerator_t3678590780 * __this, Dictionary_2_t2358566078 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2358566078 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2358566078 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3614838359_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2358566078 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	Enumerator__ctor_m3614838359(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1050235448_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m173248908((Enumerator_t3678590780 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t115911300  L_0 = (KeyValuePair_2_t115911300 )__this->get_current_3();
		KeyValuePair_2_t115911300  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1050235448_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1050235448(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2690476828_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m3460173937((Enumerator_t3678590780 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2690476828_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2690476828(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3529070385_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m173248908((Enumerator_t3678590780 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t115911300 * L_0 = (KeyValuePair_2_t115911300 *)__this->get_address_of_current_3();
		KeyValuePair_2_t3132015601  L_1 = KeyValuePair_2_get_Key_m3310904759((KeyValuePair_2_t115911300 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t3132015601  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t115911300 * L_4 = (KeyValuePair_2_t115911300 *)__this->get_address_of_current_3();
		int32_t L_5 = KeyValuePair_2_get_Value_m2441171031((KeyValuePair_2_t115911300 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t3048875398  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2901884110(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3529070385_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3529070385(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1312658566_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3132015601  L_0 = Enumerator_get_CurrentKey_m3793490395((Enumerator_t3678590780 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		KeyValuePair_2_t3132015601  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1312658566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1312658566(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3689523988_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m3934977659((Enumerator_t3678590780 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3689523988_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3689523988(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3696685668_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1745338064((Enumerator_t3678590780 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2358566078 * L_4 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2358566078 * L_8 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		NullCheck(L_8);
		KeyValuePair_2U5BU5D_t14295340* L_9 = (KeyValuePair_2U5BU5D_t14295340*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		KeyValuePair_2_t3132015601  L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t2358566078 * L_13 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		NullCheck(L_13);
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t115911300  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1576491477(&L_18, (KeyValuePair_2_t3132015601 )L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t2358566078 * L_20 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3696685668_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	return Enumerator_MoveNext_m3696685668(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t115911300  Enumerator_get_Current_m1763492508_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t115911300  L_0 = (KeyValuePair_2_t115911300 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t115911300  Enumerator_get_Current_m1763492508_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	return Enumerator_get_Current_m1763492508(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_CurrentKey()
extern "C"  KeyValuePair_2_t3132015601  Enumerator_get_CurrentKey_m3793490395_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m173248908((Enumerator_t3678590780 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t115911300 * L_0 = (KeyValuePair_2_t115911300 *)__this->get_address_of_current_3();
		KeyValuePair_2_t3132015601  L_1 = KeyValuePair_2_get_Key_m3310904759((KeyValuePair_2_t115911300 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  KeyValuePair_2_t3132015601  Enumerator_get_CurrentKey_m3793490395_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	return Enumerator_get_CurrentKey_m3793490395(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m3934977659_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m173248908((Enumerator_t3678590780 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t115911300 * L_0 = (KeyValuePair_2_t115911300 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m2441171031((KeyValuePair_2_t115911300 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m3934977659_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3934977659(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m3460173937_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1745338064((Enumerator_t3678590780 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m3460173937_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	Enumerator_Reset_m3460173937(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1745338064_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1745338064_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1745338064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2358566078 * L_0 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2358566078 * L_2 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1745338064_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	Enumerator_VerifyState_m1745338064(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m173248908_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m173248908_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m173248908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m1745338064((Enumerator_t3678590780 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m173248908_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	Enumerator_VerifyCurrent_m173248908(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2454441307_gshared (Enumerator_t3678590780 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2358566078 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2454441307_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3678590780 * _thisAdjusted = reinterpret_cast<Enumerator_t3678590780 *>(__this + 1);
	Enumerator_Dispose_m2454441307(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m648457671_gshared (Enumerator_t2399727785 * __this, Dictionary_2_t1079703083 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1079703083 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m648457671_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1079703083 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator__ctor_m648457671(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2741808790_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3667735842((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601  L_0 = (KeyValuePair_2_t3132015601 )__this->get_current_3();
		KeyValuePair_2_t3132015601  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2741808790_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2741808790(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2861488778_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m2787013825((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2861488778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2861488778(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1723581873_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3667735842((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601 * L_0 = (KeyValuePair_2_t3132015601 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m3881724((KeyValuePair_2_t3132015601 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t3132015601 * L_4 = (KeyValuePair_2_t3132015601 *)__this->get_address_of_current_3();
		int32_t L_5 = KeyValuePair_2_get_Value_m1512166271((KeyValuePair_2_t3132015601 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t3048875398  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2901884110(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1723581873_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1723581873(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4169232220_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m3410494779((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4169232220_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4169232220(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3772256406_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m2708916123((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3772256406_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3772256406(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3809319098_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m4145755786((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1079703083 * L_4 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1079703083 * L_8 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1079703083 * L_13 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_13);
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3132015601  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m629383883(&L_18, (int32_t)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1079703083 * L_20 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3809319098_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_MoveNext_m3809319098(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t3132015601  Enumerator_get_Current_m3655806242_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3132015601  L_0 = (KeyValuePair_2_t3132015601 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3132015601  Enumerator_get_Current_m3655806242_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_get_Current_m3655806242(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3410494779_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3667735842((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601 * L_0 = (KeyValuePair_2_t3132015601 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m3881724((KeyValuePair_2_t3132015601 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m3410494779_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_get_CurrentKey_m3410494779(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m2708916123_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3667735842((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601 * L_0 = (KeyValuePair_2_t3132015601 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m1512166271((KeyValuePair_2_t3132015601 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m2708916123_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2708916123(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m2787013825_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m4145755786((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m2787013825_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_Reset_m2787013825(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m4145755786_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4145755786_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4145755786_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1079703083 * L_2 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4145755786_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_VerifyState_m4145755786(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3667735842_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3667735842_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3667735842_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m4145755786((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3667735842_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_VerifyCurrent_m3667735842(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1631869499_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1079703083 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1631869499_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_Dispose_m1631869499(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1702560852_gshared (Enumerator_t3017299632 * __this, Dictionary_2_t1697274930 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1697274930 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1702560852_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1697274930 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator__ctor_m1702560852(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448  L_0 = (KeyValuePair_2_t3749587448 )__this->get_current_3();
		KeyValuePair_2_t3749587448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1631145297(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m761796566((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2828524109_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2828524109(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448 * L_0 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t3749587448 * L_4 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m447338908((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m3562053380((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2770956757_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m2118679243((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1697274930 * L_4 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1697274930 * L_8 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1697274930 * L_13 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3749587448  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3201181706(&L_18, (int32_t)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1697274930 * L_20 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2770956757_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_MoveNext_m2770956757(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3749587448  Enumerator_get_Current_m2230224741_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3749587448  L_0 = (KeyValuePair_2_t3749587448 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3749587448  Enumerator_get_Current_m2230224741_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_get_Current_m2230224741(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m447338908_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448 * L_0 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m447338908_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_get_CurrentKey_m447338908(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3562053380_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448 * L_0 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3562053380_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3562053380(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m761796566_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2118679243((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m761796566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_Reset_m761796566(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m2118679243_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2118679243_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2118679243_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1697274930 * L_2 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2118679243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_VerifyState_m2118679243(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m4246196125_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m4246196125_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m4246196125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2118679243((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m4246196125_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_VerifyCurrent_m4246196125(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2243145188_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1697274930 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2243145188_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_Dispose_m2243145188(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3888930488_gshared (Enumerator_t2654830007 * __this, Dictionary_2_t1334805305 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1334805305 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1334805305 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3888930488_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1334805305 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	Enumerator__ctor_m3888930488(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m649871021_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m231503237((Enumerator_t2654830007 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3387117823  L_0 = (KeyValuePair_2_t3387117823 )__this->get_current_3();
		KeyValuePair_2_t3387117823  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m649871021_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m649871021(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m593851829_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m367248862((Enumerator_t2654830007 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m593851829_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m593851829(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m218337734_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m231503237((Enumerator_t2654830007 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3387117823 * L_0 = (KeyValuePair_2_t3387117823 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m372290202((KeyValuePair_2_t3387117823 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t3387117823 * L_2 = (KeyValuePair_2_t3387117823 *)__this->get_address_of_current_3();
		int32_t L_3 = KeyValuePair_2_get_Value_m3683399194((KeyValuePair_2_t3387117823 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m218337734_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m218337734(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2021025471_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m1574894968((Enumerator_t2654830007 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2021025471_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2021025471(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2671660943_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m1369392376((Enumerator_t2654830007 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2671660943_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2671660943(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m657838813_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1102849887((Enumerator_t2654830007 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1334805305 * L_4 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1334805305 * L_8 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1334805305 * L_13 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		NullCheck(L_13);
		JsonSchemaTypeU5BU5D_t3104176164* L_14 = (JsonSchemaTypeU5BU5D_t3104176164*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3387117823  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3533396040(&L_18, (Il2CppObject *)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1334805305 * L_20 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m657838813_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	return Enumerator_MoveNext_m657838813(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Current()
extern "C"  KeyValuePair_2_t3387117823  Enumerator_get_Current_m954143681_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3387117823  L_0 = (KeyValuePair_2_t3387117823 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3387117823  Enumerator_get_Current_m954143681_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	return Enumerator_get_Current_m954143681(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1574894968_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m231503237((Enumerator_t2654830007 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3387117823 * L_0 = (KeyValuePair_2_t3387117823 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m372290202((KeyValuePair_2_t3387117823 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1574894968_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1574894968(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m1369392376_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m231503237((Enumerator_t2654830007 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3387117823 * L_0 = (KeyValuePair_2_t3387117823 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m3683399194((KeyValuePair_2_t3387117823 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m1369392376_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	return Enumerator_get_CurrentValue_m1369392376(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::Reset()
extern "C"  void Enumerator_Reset_m367248862_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1102849887((Enumerator_t2654830007 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m367248862_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	Enumerator_Reset_m367248862(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1102849887_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1102849887_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1102849887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1334805305 * L_0 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1334805305 * L_2 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1102849887_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	Enumerator_VerifyState_m1102849887(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m231503237_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m231503237_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m231503237_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m1102849887((Enumerator_t2654830007 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m231503237_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	Enumerator_VerifyCurrent_m231503237(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::Dispose()
extern "C"  void Enumerator_Dispose_m3267139496_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1334805305 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3267139496_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2654830007 * _thisAdjusted = reinterpret_cast<Enumerator_t2654830007 *>(__this + 1);
	Enumerator_Dispose_m3267139496(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2723936117_gshared (Enumerator_t1132285762 * __this, Dictionary_2_t4107228356 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t4107228356 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t4107228356 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2723936117_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t4107228356 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	Enumerator__ctor_m2723936117(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1932712986_gshared (Enumerator_t1132285762 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3620343242((Enumerator_t1132285762 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1864573578  L_0 = (KeyValuePair_2_t1864573578 )__this->get_current_3();
		KeyValuePair_2_t1864573578  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1932712986_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1932712986(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3437818522_gshared (Enumerator_t1132285762 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m3814399987((Enumerator_t1132285762 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3437818522_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3437818522(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m835998055_gshared (Enumerator_t1132285762 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3620343242((Enumerator_t1132285762 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1864573578 * L_0 = (KeyValuePair_2_t1864573578 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1570484949((KeyValuePair_2_t1864573578 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t1864573578 * L_2 = (KeyValuePair_2_t1864573578 *)__this->get_address_of_current_3();
		int32_t L_3 = KeyValuePair_2_get_Value_m2261596981((KeyValuePair_2_t1864573578 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m835998055_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m835998055(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2463170276_gshared (Enumerator_t1132285762 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m41078577((Enumerator_t1132285762 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2463170276_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2463170276(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2528254738_gshared (Enumerator_t1132285762 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m3935508497((Enumerator_t1132285762 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2528254738_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2528254738(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1405845710_gshared (Enumerator_t1132285762 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1748155442((Enumerator_t1132285762 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t4107228356 * L_4 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t4107228356 * L_8 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t4107228356 * L_13 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		NullCheck(L_13);
		PropertyPresenceU5BU5D_t1324757005* L_14 = (PropertyPresenceU5BU5D_t1324757005*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1864573578  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m2181336095(&L_18, (Il2CppObject *)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t4107228356 * L_20 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1405845710_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	return Enumerator_MoveNext_m1405845710(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Current()
extern "C"  KeyValuePair_2_t1864573578  Enumerator_get_Current_m3066235090_gshared (Enumerator_t1132285762 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1864573578  L_0 = (KeyValuePair_2_t1864573578 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1864573578  Enumerator_get_Current_m3066235090_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	return Enumerator_get_Current_m3066235090(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m41078577_gshared (Enumerator_t1132285762 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3620343242((Enumerator_t1132285762 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1864573578 * L_0 = (KeyValuePair_2_t1864573578 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1570484949((KeyValuePair_2_t1864573578 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m41078577_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	return Enumerator_get_CurrentKey_m41078577(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m3935508497_gshared (Enumerator_t1132285762 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3620343242((Enumerator_t1132285762 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1864573578 * L_0 = (KeyValuePair_2_t1864573578 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m2261596981((KeyValuePair_2_t1864573578 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m3935508497_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3935508497(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Reset()
extern "C"  void Enumerator_Reset_m3814399987_gshared (Enumerator_t1132285762 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1748155442((Enumerator_t1132285762 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m3814399987_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	Enumerator_Reset_m3814399987(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1748155442_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1748155442_gshared (Enumerator_t1132285762 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1748155442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t4107228356 * L_0 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t4107228356 * L_2 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1748155442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	Enumerator_VerifyState_m1748155442(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3620343242_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3620343242_gshared (Enumerator_t1132285762 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3620343242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m1748155442((Enumerator_t1132285762 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3620343242_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	Enumerator_VerifyCurrent_m3620343242(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Dispose()
extern "C"  void Enumerator_Dispose_m745350365_gshared (Enumerator_t1132285762 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t4107228356 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m745350365_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1132285762 * _thisAdjusted = reinterpret_cast<Enumerator_t1132285762 *>(__this + 1);
	Enumerator_Dispose_m745350365(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m661036428_gshared (Enumerator_t442692252 * __this, Dictionary_2_t3417634846 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3417634846 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m661036428_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3417634846 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator__ctor_m661036428(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1692692619_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068  L_0 = (KeyValuePair_2_t1174980068 )__this->get_current_3();
		KeyValuePair_2_t1174980068  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1692692619_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1692692619(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m70453843_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m3115320746((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m70453843_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m70453843(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3667889028_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068 * L_0 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t1174980068 * L_2 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		bool L_3 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3667889028_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3667889028(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1214978221_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m565000604((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1214978221_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1214978221(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m313528997_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Enumerator_get_CurrentValue_m4143929484((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m313528997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m313528997(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1856697671_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1165543189((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3417634846 * L_4 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3417634846 * L_8 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3417634846 * L_13 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_13);
		BooleanU5BU5D_t3568034315* L_14 = (BooleanU5BU5D_t3568034315*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		bool L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1174980068  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m4040336782(&L_18, (Il2CppObject *)L_12, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3417634846 * L_20 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1856697671_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_MoveNext_m1856697671(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C"  KeyValuePair_2_t1174980068  Enumerator_get_Current_m1020413567_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1174980068  L_0 = (KeyValuePair_2_t1174980068 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1174980068  Enumerator_get_Current_m1020413567_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_get_Current_m1020413567(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m565000604_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068 * L_0 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m565000604_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_get_CurrentKey_m565000604(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C"  bool Enumerator_get_CurrentValue_m4143929484_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068 * L_0 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		bool L_1 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_get_CurrentValue_m4143929484_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_get_CurrentValue_m4143929484(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Reset()
extern "C"  void Enumerator_Reset_m3115320746_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1165543189((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m3115320746_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_Reset_m3115320746(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1165543189_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1165543189_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1165543189_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3417634846 * L_2 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1165543189_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_VerifyState_m1165543189(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3330382363_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3330382363_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3330382363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m1165543189((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3330382363_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_VerifyCurrent_m3330382363(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m2711120408_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3417634846 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2711120408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_Dispose_m2711120408(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2153525756_gshared (Enumerator_t950939475 * __this, Dictionary_2_t3925882069 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3925882069 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3925882069 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2153525756_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3925882069 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	Enumerator__ctor_m2153525756(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m469144049_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4104698621((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1683227291  L_0 = (KeyValuePair_2_t1683227291 )__this->get_current_3();
		KeyValuePair_2_t1683227291  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m469144049_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m469144049(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2381541637_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m535811698((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2381541637_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2381541637(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2303546672_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4104698621((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1683227291 * L_0 = (KeyValuePair_2_t1683227291 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m4247498472((KeyValuePair_2_t1683227291 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t1683227291 * L_2 = (KeyValuePair_2_t1683227291 *)__this->get_address_of_current_3();
		KeyValuePair_2_t38854645  L_3 = KeyValuePair_2_get_Value_m667219360((KeyValuePair_2_t1683227291 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		KeyValuePair_2_t38854645  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2303546672_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2303546672(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1158804235_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m1920795876((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1158804235_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1158804235(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1378836675_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = Enumerator_get_CurrentValue_m960757924((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1378836675_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1378836675(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1022654781_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m2572643079((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3925882069 * L_4 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3925882069 * L_8 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3925882069 * L_13 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		NullCheck(L_13);
		KeyValuePair_2U5BU5D_t2854920344* L_14 = (KeyValuePair_2U5BU5D_t2854920344*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		KeyValuePair_2_t38854645  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1683227291  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m400583398(&L_18, (Il2CppObject *)L_12, (KeyValuePair_2_t38854645 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3925882069 * L_20 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1022654781_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_MoveNext_m1022654781(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1683227291  Enumerator_get_Current_m304262005_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1683227291  L_0 = (KeyValuePair_2_t1683227291 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1683227291  Enumerator_get_Current_m304262005_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_get_Current_m304262005(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1920795876_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4104698621((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1683227291 * L_0 = (KeyValuePair_2_t1683227291 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m4247498472((KeyValuePair_2_t1683227291 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1920795876_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1920795876(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_CurrentValue()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_CurrentValue_m960757924_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4104698621((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1683227291 * L_0 = (KeyValuePair_2_t1683227291 *)__this->get_address_of_current_3();
		KeyValuePair_2_t38854645  L_1 = KeyValuePair_2_get_Value_m667219360((KeyValuePair_2_t1683227291 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_CurrentValue_m960757924_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	return Enumerator_get_CurrentValue_m960757924(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reset()
extern "C"  void Enumerator_Reset_m535811698_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2572643079((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m535811698_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	Enumerator_Reset_m535811698(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m2572643079_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2572643079_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2572643079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3925882069 * L_0 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3925882069 * L_2 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2572643079_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	Enumerator_VerifyState_m2572643079(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m4104698621_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m4104698621_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m4104698621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2572643079((Enumerator_t950939475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m4104698621_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	Enumerator_VerifyCurrent_m4104698621(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m3456852724_gshared (Enumerator_t950939475 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3925882069 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3456852724_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t950939475 * _thisAdjusted = reinterpret_cast<Enumerator_t950939475 *>(__this + 1);
	Enumerator_Dispose_m3456852724(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3597047336_gshared (Enumerator_t2983962278 * __this, Dictionary_2_t1663937576 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1663937576 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3597047336_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1663937576 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator__ctor_m3597047336(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2010873149_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094  L_0 = (KeyValuePair_2_t3716250094 )__this->get_current_3();
		KeyValuePair_2_t3716250094  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2010873149_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2010873149(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3085583937_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1132695838((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3085583937_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3085583937(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m487599172_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094 * L_0 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t3716250094 * L_2 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		int32_t L_3 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m487599172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m487599172(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m677423231_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m1408186928((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m677423231_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m677423231(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3005608231_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m2645962456((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3005608231_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3005608231(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m435964161_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m3173176371((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1663937576 * L_4 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1663937576 * L_8 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1663937576 * L_13 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_13);
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3716250094  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1877755778(&L_18, (Il2CppObject *)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1663937576 * L_20 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m435964161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_MoveNext_m435964161(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t3716250094  Enumerator_get_Current_m1932198897_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3716250094  L_0 = (KeyValuePair_2_t3716250094 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3716250094  Enumerator_get_Current_m1932198897_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_get_Current_m1932198897(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1408186928_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094 * L_0 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1408186928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1408186928(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m2645962456_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094 * L_0 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m2645962456_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2645962456(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m1132695838_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3173176371((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1132695838_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_Reset_m1132695838(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m3173176371_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3173176371_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3173176371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1663937576 * L_2 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3173176371_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_VerifyState_m3173176371(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3278789713_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3278789713_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3278789713_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m3173176371((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3278789713_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_VerifyCurrent_m3278789713(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m401572848_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1663937576 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m401572848_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_Dispose_m401572848(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3742107451_gshared (Enumerator_t3601534125 * __this, Dictionary_2_t2281509423 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2281509423 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3742107451_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2281509423 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator__ctor_m3742107451(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m229223308_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m229223308(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m3129803197((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3225937576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3225937576(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645 * L_0 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t38854645 * L_2 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_3 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DictionaryEntry__ctor_m2901884110(&L_4, (Il2CppObject *)L_1, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m3839846791((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m402763047((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3349738440_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m262343092((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2281509423 * L_4 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2281509423 * L_8 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t2281509423 * L_13 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t38854645  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1640124561(&L_18, (Il2CppObject *)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t2281509423 * L_20 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3349738440_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_MoveNext_m3349738440(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m25299632_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m25299632_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_get_Current_m25299632(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3839846791_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645 * L_0 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3839846791_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_get_CurrentKey_m3839846791(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m402763047_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645 * L_0 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m402763047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_get_CurrentValue_m402763047(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3129803197_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m262343092((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m3129803197_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_Reset_m3129803197(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m262343092_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m262343092_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m262343092_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m262343092_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_VerifyState_m262343092(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m1702320752_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m1702320752_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m1702320752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m262343092((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m1702320752_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_VerifyCurrent_m1702320752(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1905011127_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2281509423 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1905011127_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_Dispose_m1905011127(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m960028898_gshared (Enumerator_t4129392986 * __this, Dictionary_2_t2809368284 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2809368284 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2809368284 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m960028898_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2809368284 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	Enumerator__ctor_m960028898(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m98200979_gshared (Enumerator_t4129392986 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1740774343((Enumerator_t4129392986 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t566713506  L_0 = (KeyValuePair_2_t566713506 )__this->get_current_3();
		KeyValuePair_2_t566713506  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m98200979_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m98200979(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m659462039_gshared (Enumerator_t4129392986 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1125442816((Enumerator_t4129392986 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m659462039_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m659462039(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2678233676_gshared (Enumerator_t4129392986 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1740774343((Enumerator_t4129392986 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t566713506 * L_0 = (KeyValuePair_2_t566713506 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m3358872092((KeyValuePair_2_t566713506 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t566713506 * L_4 = (KeyValuePair_2_t566713506 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m121202140((KeyValuePair_2_t566713506 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2678233676_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2678233676(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2689818157_gshared (Enumerator_t4129392986 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m1432453562((Enumerator_t4129392986 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2689818157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2689818157(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2864547893_gshared (Enumerator_t4129392986 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m3330496250((Enumerator_t4129392986 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2864547893_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2864547893(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4270128035_gshared (Enumerator_t4129392986 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m838284361((Enumerator_t4129392986 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2809368284 * L_4 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2809368284 * L_8 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		NullCheck(L_8);
		AndroidStoreU5BU5D_t4142492995* L_9 = (AndroidStoreU5BU5D_t4142492995*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t2809368284 * L_13 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t566713506  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3743311962(&L_18, (int32_t)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t2809368284 * L_20 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4270128035_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	return Enumerator_MoveNext_m4270128035(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t566713506  Enumerator_get_Current_m359771003_gshared (Enumerator_t4129392986 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t566713506  L_0 = (KeyValuePair_2_t566713506 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t566713506  Enumerator_get_Current_m359771003_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	return Enumerator_get_Current_m359771003(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1432453562_gshared (Enumerator_t4129392986 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1740774343((Enumerator_t4129392986 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t566713506 * L_0 = (KeyValuePair_2_t566713506 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m3358872092((KeyValuePair_2_t566713506 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m1432453562_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1432453562(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3330496250_gshared (Enumerator_t4129392986 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1740774343((Enumerator_t4129392986 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t566713506 * L_0 = (KeyValuePair_2_t566713506 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m121202140((KeyValuePair_2_t566713506 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3330496250_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3330496250(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1125442816_gshared (Enumerator_t4129392986 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m838284361((Enumerator_t4129392986 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1125442816_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	Enumerator_Reset_m1125442816(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m838284361_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m838284361_gshared (Enumerator_t4129392986 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m838284361_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2809368284 * L_0 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2809368284 * L_2 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m838284361_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	Enumerator_VerifyState_m838284361(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m1740774343_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m1740774343_gshared (Enumerator_t4129392986 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m1740774343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m838284361((Enumerator_t4129392986 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m1740774343_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	Enumerator_VerifyCurrent_m1740774343(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m534754534_gshared (Enumerator_t4129392986 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2809368284 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m534754534_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4129392986 * _thisAdjusted = reinterpret_cast<Enumerator_t4129392986 *>(__this + 1);
	Enumerator_Dispose_m534754534(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m911887285_gshared (Enumerator_t4046199807 * __this, Dictionary_2_t1356696369 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1356696369 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1356696369 *)L_0);
		Enumerator_t2676721071  L_1 = ((  Enumerator_t2676721071  (*) (Dictionary_2_t1356696369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1356696369 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m911887285_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1356696369 * ___host0, const MethodInfo* method)
{
	Enumerator_t4046199807 * _thisAdjusted = reinterpret_cast<Enumerator_t4046199807 *>(__this + 1);
	Enumerator__ctor_m911887285(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m130813306_gshared (Enumerator_t4046199807 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2676721071 * L_0 = (Enumerator_t2676721071 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m4071352643((Enumerator_t2676721071 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m130813306_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4046199807 * _thisAdjusted = reinterpret_cast<Enumerator_t4046199807 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m130813306(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3953624924_gshared (Enumerator_t4046199807 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2676721071 * L_0 = (Enumerator_t2676721071 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3948831101((Enumerator_t2676721071 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3953624924_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4046199807 * _thisAdjusted = reinterpret_cast<Enumerator_t4046199807 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3953624924(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::Dispose()
extern "C"  void Enumerator_Dispose_m3584203781_gshared (Enumerator_t4046199807 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2676721071 * L_0 = (Enumerator_t2676721071 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2041202203((Enumerator_t2676721071 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3584203781_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4046199807 * _thisAdjusted = reinterpret_cast<Enumerator_t4046199807 *>(__this + 1);
	Enumerator_Dispose_m3584203781(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3013070576_gshared (Enumerator_t4046199807 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2676721071 * L_0 = (Enumerator_t2676721071 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1965426417((Enumerator_t2676721071 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3013070576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4046199807 * _thisAdjusted = reinterpret_cast<Enumerator_t4046199807 *>(__this + 1);
	return Enumerator_MoveNext_m3013070576(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m315949484_gshared (Enumerator_t4046199807 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2676721071 * L_0 = (Enumerator_t2676721071 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3409008887 * L_1 = (KeyValuePair_2_t3409008887 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m232446462((KeyValuePair_2_t3409008887 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m315949484_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4046199807 * _thisAdjusted = reinterpret_cast<Enumerator_t4046199807 *>(__this + 1);
	return Enumerator_get_Current_m315949484(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<FB_ProfileImageSize,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4138210453_gshared (Enumerator_t1172105118 * __this, Dictionary_2_t2777568976 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2777568976 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2777568976 *)L_0);
		Enumerator_t4097593678  L_1 = ((  Enumerator_t4097593678  (*) (Dictionary_2_t2777568976 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2777568976 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m4138210453_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2777568976 * ___host0, const MethodInfo* method)
{
	Enumerator_t1172105118 * _thisAdjusted = reinterpret_cast<Enumerator_t1172105118 *>(__this + 1);
	Enumerator__ctor_m4138210453(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m798504968_gshared (Enumerator_t1172105118 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4097593678 * L_0 = (Enumerator_t4097593678 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m2108057179((Enumerator_t4097593678 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m798504968_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1172105118 * _thisAdjusted = reinterpret_cast<Enumerator_t1172105118 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m798504968(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3808585376_gshared (Enumerator_t1172105118 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4097593678 * L_0 = (Enumerator_t4097593678 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m812636689((Enumerator_t4097593678 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3808585376_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1172105118 * _thisAdjusted = reinterpret_cast<Enumerator_t1172105118 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3808585376(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<FB_ProfileImageSize,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m586563925_gshared (Enumerator_t1172105118 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4097593678 * L_0 = (Enumerator_t4097593678 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2418656635((Enumerator_t4097593678 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m586563925_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1172105118 * _thisAdjusted = reinterpret_cast<Enumerator_t1172105118 *>(__this + 1);
	Enumerator_Dispose_m586563925(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<FB_ProfileImageSize,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1763605196_gshared (Enumerator_t1172105118 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4097593678 * L_0 = (Enumerator_t4097593678 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3732010148((Enumerator_t4097593678 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1763605196_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1172105118 * _thisAdjusted = reinterpret_cast<Enumerator_t1172105118 *>(__this + 1);
	return Enumerator_MoveNext_m1763605196(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<FB_ProfileImageSize,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1997155432_gshared (Enumerator_t1172105118 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4097593678 * L_0 = (Enumerator_t4097593678 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t534914198 * L_1 = (KeyValuePair_2_t534914198 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m1840008439((KeyValuePair_2_t534914198 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m1997155432_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1172105118 * _thisAdjusted = reinterpret_cast<Enumerator_t1172105118 *>(__this + 1);
	return Enumerator_get_Current_m1997155432(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1781268946_gshared (Enumerator_t731313631 * __this, Dictionary_2_t2336777489 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2336777489 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2336777489 *)L_0);
		Enumerator_t3656802191  L_1 = ((  Enumerator_t3656802191  (*) (Dictionary_2_t2336777489 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2336777489 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1781268946_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2336777489 * ___host0, const MethodInfo* method)
{
	Enumerator_t731313631 * _thisAdjusted = reinterpret_cast<Enumerator_t731313631 *>(__this + 1);
	Enumerator__ctor_m1781268946(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m893933349_gshared (Enumerator_t731313631 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3656802191 * L_0 = (Enumerator_t3656802191 *)__this->get_address_of_host_enumerator_0();
		TypeNameKey_t3055062677  L_1 = Enumerator_get_CurrentKey_m3423730762((Enumerator_t3656802191 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		TypeNameKey_t3055062677  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m893933349_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t731313631 * _thisAdjusted = reinterpret_cast<Enumerator_t731313631 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m893933349(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3958863633_gshared (Enumerator_t731313631 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3656802191 * L_0 = (Enumerator_t3656802191 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1824444848((Enumerator_t3656802191 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3958863633_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t731313631 * _thisAdjusted = reinterpret_cast<Enumerator_t731313631 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3958863633(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2412861866_gshared (Enumerator_t731313631 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3656802191 * L_0 = (Enumerator_t3656802191 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1064077118((Enumerator_t3656802191 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2412861866_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t731313631 * _thisAdjusted = reinterpret_cast<Enumerator_t731313631 *>(__this + 1);
	Enumerator_Dispose_m2412861866(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3303551985_gshared (Enumerator_t731313631 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3656802191 * L_0 = (Enumerator_t3656802191 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1720213575((Enumerator_t3656802191 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3303551985_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t731313631 * _thisAdjusted = reinterpret_cast<Enumerator_t731313631 *>(__this + 1);
	return Enumerator_MoveNext_m3303551985(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Current()
extern "C"  TypeNameKey_t3055062677  Enumerator_get_Current_m3438609299_gshared (Enumerator_t731313631 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3656802191 * L_0 = (Enumerator_t3656802191 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t94122711 * L_1 = (KeyValuePair_2_t94122711 *)L_0->get_address_of_current_3();
		TypeNameKey_t3055062677  L_2 = KeyValuePair_2_get_Key_m2430213422((KeyValuePair_2_t94122711 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  TypeNameKey_t3055062677  Enumerator_get_Current_m3438609299_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t731313631 * _thisAdjusted = reinterpret_cast<Enumerator_t731313631 *>(__this + 1);
	return Enumerator_get_Current_m3438609299(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1214207411_gshared (Enumerator_t546542526 * __this, Dictionary_2_t2152006384 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2152006384 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2152006384 *)L_0);
		Enumerator_t3472031086  L_1 = ((  Enumerator_t3472031086  (*) (Dictionary_2_t2152006384 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2152006384 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1214207411_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2152006384 * ___host0, const MethodInfo* method)
{
	Enumerator_t546542526 * _thisAdjusted = reinterpret_cast<Enumerator_t546542526 *>(__this + 1);
	Enumerator__ctor_m1214207411(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2623082900_gshared (Enumerator_t546542526 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3472031086 * L_0 = (Enumerator_t3472031086 *)__this->get_address_of_host_enumerator_0();
		TypeConvertKey_t1788482786  L_1 = Enumerator_get_CurrentKey_m3080089425((Enumerator_t3472031086 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		TypeConvertKey_t1788482786  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2623082900_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t546542526 * _thisAdjusted = reinterpret_cast<Enumerator_t546542526 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2623082900(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3691285984_gshared (Enumerator_t546542526 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3472031086 * L_0 = (Enumerator_t3472031086 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m62122567((Enumerator_t3472031086 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3691285984_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t546542526 * _thisAdjusted = reinterpret_cast<Enumerator_t546542526 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3691285984(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m885252255_gshared (Enumerator_t546542526 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3472031086 * L_0 = (Enumerator_t3472031086 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m496361693((Enumerator_t3472031086 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m885252255_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t546542526 * _thisAdjusted = reinterpret_cast<Enumerator_t546542526 *>(__this + 1);
	Enumerator_Dispose_m885252255(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m254045600_gshared (Enumerator_t546542526 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3472031086 * L_0 = (Enumerator_t3472031086 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m72413836((Enumerator_t3472031086 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m254045600_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t546542526 * _thisAdjusted = reinterpret_cast<Enumerator_t546542526 *>(__this + 1);
	return Enumerator_MoveNext_m254045600(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Current()
extern "C"  TypeConvertKey_t1788482786  Enumerator_get_Current_m3256390096_gshared (Enumerator_t546542526 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3472031086 * L_0 = (Enumerator_t3472031086 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t4204318902 * L_1 = (KeyValuePair_2_t4204318902 *)L_0->get_address_of_current_3();
		TypeConvertKey_t1788482786  L_2 = KeyValuePair_2_get_Key_m3955619893((KeyValuePair_2_t4204318902 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  TypeConvertKey_t1788482786  Enumerator_get_Current_m3256390096_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t546542526 * _thisAdjusted = reinterpret_cast<Enumerator_t546542526 *>(__this + 1);
	return Enumerator_get_Current_m3256390096(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3725287957_gshared (Enumerator_t753102220 * __this, Dictionary_2_t2358566078 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2358566078 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2358566078 *)L_0);
		Enumerator_t3678590780  L_1 = ((  Enumerator_t3678590780  (*) (Dictionary_2_t2358566078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2358566078 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3725287957_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2358566078 * ___host0, const MethodInfo* method)
{
	Enumerator_t753102220 * _thisAdjusted = reinterpret_cast<Enumerator_t753102220 *>(__this + 1);
	Enumerator__ctor_m3725287957(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2429470792_gshared (Enumerator_t753102220 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3678590780 * L_0 = (Enumerator_t3678590780 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3132015601  L_1 = Enumerator_get_CurrentKey_m3793490395((Enumerator_t3678590780 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		KeyValuePair_2_t3132015601  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2429470792_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t753102220 * _thisAdjusted = reinterpret_cast<Enumerator_t753102220 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2429470792(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1948070592_gshared (Enumerator_t753102220 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3678590780 * L_0 = (Enumerator_t3678590780 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3460173937((Enumerator_t3678590780 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1948070592_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t753102220 * _thisAdjusted = reinterpret_cast<Enumerator_t753102220 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1948070592(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1696555509_gshared (Enumerator_t753102220 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3678590780 * L_0 = (Enumerator_t3678590780 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2454441307((Enumerator_t3678590780 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1696555509_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t753102220 * _thisAdjusted = reinterpret_cast<Enumerator_t753102220 *>(__this + 1);
	Enumerator_Dispose_m1696555509(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m614304652_gshared (Enumerator_t753102220 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3678590780 * L_0 = (Enumerator_t3678590780 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3696685668((Enumerator_t3678590780 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m614304652_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t753102220 * _thisAdjusted = reinterpret_cast<Enumerator_t753102220 *>(__this + 1);
	return Enumerator_MoveNext_m614304652(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t3132015601  Enumerator_get_Current_m530044104_gshared (Enumerator_t753102220 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3678590780 * L_0 = (Enumerator_t3678590780 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t115911300 * L_1 = (KeyValuePair_2_t115911300 *)L_0->get_address_of_current_3();
		KeyValuePair_2_t3132015601  L_2 = KeyValuePair_2_get_Key_m3310904759((KeyValuePair_2_t115911300 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  KeyValuePair_2_t3132015601  Enumerator_get_Current_m530044104_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t753102220 * _thisAdjusted = reinterpret_cast<Enumerator_t753102220 *>(__this + 1);
	return Enumerator_get_Current_m530044104(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m356032285_gshared (Enumerator_t3769206521 * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		Enumerator_t2399727785  L_1 = ((  Enumerator_t2399727785  (*) (Dictionary_2_t1079703083 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m356032285_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	Enumerator__ctor_m356032285(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1856980258_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m3410494779((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1856980258_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1856980258(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m84942922_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2787013825((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m84942922_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m84942922(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2091286541_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1631869499((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2091286541_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	Enumerator_Dispose_m2091286541(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3192922974_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3809319098((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3192922974_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	return Enumerator_MoveNext_m3192922974(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m4198336486_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3132015601 * L_1 = (KeyValuePair_2_t3132015601 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m3881724((KeyValuePair_2_t3132015601 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m4198336486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	return Enumerator_get_Current_m4198336486(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2975000652_gshared (Enumerator_t91811072 * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		Enumerator_t3017299632  L_1 = ((  Enumerator_t3017299632  (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2975000652_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	Enumerator__ctor_m2975000652(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1197115851_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m447338908((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1197115851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1197115851(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1291229611_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m761796566((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1291229611_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1291229611(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3298895664_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2243145188((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3298895664_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	Enumerator_Dispose_m3298895664(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1388125215_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2770956757((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1388125215_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	return Enumerator_MoveNext_m1388125215(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m862874469_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3749587448 * L_1 = (KeyValuePair_2_t3749587448 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m862874469_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	return Enumerator_get_Current_m862874469(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1888479228_gshared (Enumerator_t4024308743 * __this, Dictionary_2_t1334805305 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1334805305 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1334805305 *)L_0);
		Enumerator_t2654830007  L_1 = ((  Enumerator_t2654830007  (*) (Dictionary_2_t1334805305 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1334805305 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1888479228_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1334805305 * ___host0, const MethodInfo* method)
{
	Enumerator_t4024308743 * _thisAdjusted = reinterpret_cast<Enumerator_t4024308743 *>(__this + 1);
	Enumerator__ctor_m1888479228(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1103411995_gshared (Enumerator_t4024308743 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2654830007 * L_0 = (Enumerator_t2654830007 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m1574894968((Enumerator_t2654830007 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1103411995_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4024308743 * _thisAdjusted = reinterpret_cast<Enumerator_t4024308743 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1103411995(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1868764239_gshared (Enumerator_t4024308743 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2654830007 * L_0 = (Enumerator_t2654830007 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m367248862((Enumerator_t2654830007 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1868764239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4024308743 * _thisAdjusted = reinterpret_cast<Enumerator_t4024308743 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1868764239(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::Dispose()
extern "C"  void Enumerator_Dispose_m900475408_gshared (Enumerator_t4024308743 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2654830007 * L_0 = (Enumerator_t2654830007 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3267139496((Enumerator_t2654830007 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m900475408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4024308743 * _thisAdjusted = reinterpret_cast<Enumerator_t4024308743 *>(__this + 1);
	Enumerator_Dispose_m900475408(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1448737131_gshared (Enumerator_t4024308743 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2654830007 * L_0 = (Enumerator_t2654830007 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m657838813((Enumerator_t2654830007 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1448737131_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4024308743 * _thisAdjusted = reinterpret_cast<Enumerator_t4024308743 *>(__this + 1);
	return Enumerator_MoveNext_m1448737131(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m4186407577_gshared (Enumerator_t4024308743 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2654830007 * L_0 = (Enumerator_t2654830007 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3387117823 * L_1 = (KeyValuePair_2_t3387117823 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m372290202((KeyValuePair_2_t3387117823 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m4186407577_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4024308743 * _thisAdjusted = reinterpret_cast<Enumerator_t4024308743 *>(__this + 1);
	return Enumerator_get_Current_m4186407577(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3042794935_gshared (Enumerator_t2501764498 * __this, Dictionary_2_t4107228356 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t4107228356 * L_0 = ___host0;
		NullCheck((Dictionary_2_t4107228356 *)L_0);
		Enumerator_t1132285762  L_1 = ((  Enumerator_t1132285762  (*) (Dictionary_2_t4107228356 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t4107228356 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3042794935_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t4107228356 * ___host0, const MethodInfo* method)
{
	Enumerator_t2501764498 * _thisAdjusted = reinterpret_cast<Enumerator_t2501764498 *>(__this + 1);
	Enumerator__ctor_m3042794935(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1954157990_gshared (Enumerator_t2501764498 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1132285762 * L_0 = (Enumerator_t1132285762 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m41078577((Enumerator_t1132285762 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1954157990_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2501764498 * _thisAdjusted = reinterpret_cast<Enumerator_t2501764498 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1954157990(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3164446082_gshared (Enumerator_t2501764498 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1132285762 * L_0 = (Enumerator_t1132285762 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3814399987((Enumerator_t1132285762 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3164446082_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2501764498 * _thisAdjusted = reinterpret_cast<Enumerator_t2501764498 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3164446082(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Dispose()
extern "C"  void Enumerator_Dispose_m2164216115_gshared (Enumerator_t2501764498 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1132285762 * L_0 = (Enumerator_t1132285762 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m745350365((Enumerator_t1132285762 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2164216115_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2501764498 * _thisAdjusted = reinterpret_cast<Enumerator_t2501764498 *>(__this + 1);
	Enumerator_Dispose_m2164216115(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4112125410_gshared (Enumerator_t2501764498 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1132285762 * L_0 = (Enumerator_t1132285762 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1405845710((Enumerator_t1132285762 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m4112125410_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2501764498 * _thisAdjusted = reinterpret_cast<Enumerator_t2501764498 *>(__this + 1);
	return Enumerator_MoveNext_m4112125410(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3395620306_gshared (Enumerator_t2501764498 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1132285762 * L_0 = (Enumerator_t1132285762 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1864573578 * L_1 = (KeyValuePair_2_t1864573578 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1570484949((KeyValuePair_2_t1864573578 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m3395620306_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2501764498 * _thisAdjusted = reinterpret_cast<Enumerator_t2501764498 *>(__this + 1);
	return Enumerator_get_Current_m3395620306(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4035163080_gshared (Enumerator_t1812170988 * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		Enumerator_t442692252  L_1 = ((  Enumerator_t442692252  (*) (Dictionary_2_t3417634846 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m4035163080_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	Enumerator__ctor_m4035163080(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3424705613_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m565000604((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3424705613_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3424705613(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2102606953_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3115320746((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2102606953_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2102606953(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m4156741384_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2711120408((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m4156741384_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	Enumerator_Dispose_m4156741384(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3293466169_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1856697671((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3293466169_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	return Enumerator_MoveNext_m3293466169(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3073091827_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1174980068 * L_1 = (KeyValuePair_2_t1174980068 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m3073091827_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	return Enumerator_get_Current_m3073091827(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m526128336_gshared (Enumerator_t2320418211 * __this, Dictionary_2_t3925882069 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3925882069 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3925882069 *)L_0);
		Enumerator_t950939475  L_1 = ((  Enumerator_t950939475  (*) (Dictionary_2_t3925882069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3925882069 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m526128336_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3925882069 * ___host0, const MethodInfo* method)
{
	Enumerator_t2320418211 * _thisAdjusted = reinterpret_cast<Enumerator_t2320418211 *>(__this + 1);
	Enumerator__ctor_m526128336(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m628685503_gshared (Enumerator_t2320418211 * __this, const MethodInfo* method)
{
	{
		Enumerator_t950939475 * L_0 = (Enumerator_t950939475 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m1920795876((Enumerator_t950939475 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m628685503_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2320418211 * _thisAdjusted = reinterpret_cast<Enumerator_t2320418211 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m628685503(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3767943327_gshared (Enumerator_t2320418211 * __this, const MethodInfo* method)
{
	{
		Enumerator_t950939475 * L_0 = (Enumerator_t950939475 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m535811698((Enumerator_t950939475 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3767943327_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2320418211 * _thisAdjusted = reinterpret_cast<Enumerator_t2320418211 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3767943327(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m3925424820_gshared (Enumerator_t2320418211 * __this, const MethodInfo* method)
{
	{
		Enumerator_t950939475 * L_0 = (Enumerator_t950939475 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3456852724((Enumerator_t950939475 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3925424820_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2320418211 * _thisAdjusted = reinterpret_cast<Enumerator_t2320418211 *>(__this + 1);
	Enumerator_Dispose_m3925424820(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3316447291_gshared (Enumerator_t2320418211 * __this, const MethodInfo* method)
{
	{
		Enumerator_t950939475 * L_0 = (Enumerator_t950939475 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1022654781((Enumerator_t950939475 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3316447291_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2320418211 * _thisAdjusted = reinterpret_cast<Enumerator_t2320418211 *>(__this + 1);
	return Enumerator_MoveNext_m3316447291(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m193695133_gshared (Enumerator_t2320418211 * __this, const MethodInfo* method)
{
	{
		Enumerator_t950939475 * L_0 = (Enumerator_t950939475 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1683227291 * L_1 = (KeyValuePair_2_t1683227291 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m4247498472((KeyValuePair_2_t1683227291 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m193695133_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2320418211 * _thisAdjusted = reinterpret_cast<Enumerator_t2320418211 *>(__this + 1);
	return Enumerator_get_Current_m193695133(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1642475044_gshared (Enumerator_t58473718 * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		Enumerator_t2983962278  L_1 = ((  Enumerator_t2983962278  (*) (Dictionary_2_t1663937576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1642475044_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	Enumerator__ctor_m1642475044(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m610493107_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m1408186928((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m610493107_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m610493107(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1684647011_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1132695838((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1684647011_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1684647011(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m688620792_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m401572848((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m688620792_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	Enumerator_Dispose_m688620792(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m639383991_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m435964161((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m639383991_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	return Enumerator_MoveNext_m639383991(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1529104153_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3716250094 * L_1 = (KeyValuePair_2_t3716250094 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m1529104153_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	return Enumerator_get_Current_m1529104153(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2650732273_gshared (Enumerator_t676045565 * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2650732273_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	Enumerator__ctor_m2650732273(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1623176564_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m3839846791((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1623176564_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1623176564(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3446745716_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3129803197((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3446745716_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3446745716(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1587212441_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1905011127((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1587212441_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	Enumerator_Dispose_m1587212441(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m235441832_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3349738440((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m235441832_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	return Enumerator_MoveNext_m235441832(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m937156828_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t38854645 * L_1 = (KeyValuePair_2_t38854645 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m937156828_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	return Enumerator_get_Current_m937156828(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2213644398_gshared (Enumerator_t1203904426 * __this, Dictionary_2_t2809368284 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2809368284 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2809368284 *)L_0);
		Enumerator_t4129392986  L_1 = ((  Enumerator_t4129392986  (*) (Dictionary_2_t2809368284 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2809368284 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2213644398_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2809368284 * ___host0, const MethodInfo* method)
{
	Enumerator_t1203904426 * _thisAdjusted = reinterpret_cast<Enumerator_t1203904426 *>(__this + 1);
	Enumerator__ctor_m2213644398(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2410253161_gshared (Enumerator_t1203904426 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4129392986 * L_0 = (Enumerator_t4129392986 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m1432453562((Enumerator_t4129392986 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2410253161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1203904426 * _thisAdjusted = reinterpret_cast<Enumerator_t1203904426 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2410253161(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2160772617_gshared (Enumerator_t1203904426 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4129392986 * L_0 = (Enumerator_t4129392986 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1125442816((Enumerator_t4129392986 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2160772617_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1203904426 * _thisAdjusted = reinterpret_cast<Enumerator_t1203904426 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2160772617(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2619204102_gshared (Enumerator_t1203904426 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4129392986 * L_0 = (Enumerator_t4129392986 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m534754534((Enumerator_t4129392986 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2619204102_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1203904426 * _thisAdjusted = reinterpret_cast<Enumerator_t1203904426 *>(__this + 1);
	Enumerator_Dispose_m2619204102(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3537458233_gshared (Enumerator_t1203904426 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4129392986 * L_0 = (Enumerator_t4129392986 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m4270128035((Enumerator_t4129392986 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3537458233_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1203904426 * _thisAdjusted = reinterpret_cast<Enumerator_t1203904426 *>(__this + 1);
	return Enumerator_MoveNext_m3537458233(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1373663543_gshared (Enumerator_t1203904426 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4129392986 * L_0 = (Enumerator_t4129392986 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t566713506 * L_1 = (KeyValuePair_2_t566713506 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m3358872092((KeyValuePair_2_t566713506 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m1373663543_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1203904426 * _thisAdjusted = reinterpret_cast<Enumerator_t1203904426 *>(__this + 1);
	return Enumerator_get_Current_m1373663543(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m1101369442_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m1101369442_gshared (KeyCollection_t3840194140 * __this, Dictionary_2_t1356696369 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m1101369442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1356696369 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1356696369 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2930401916_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2930401916_gshared (KeyCollection_t3840194140 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2930401916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4225081399_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4225081399_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4225081399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2116509612_gshared (KeyCollection_t3840194140 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1356696369 * L_0 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t1356696369 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1356696369 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1356696369 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3754536599_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3754536599_gshared (KeyCollection_t3840194140 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3754536599_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3753455025_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3840194140 *)__this);
		Enumerator_t4046199807  L_0 = ((  Enumerator_t4046199807  (*) (KeyCollection_t3840194140 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3840194140 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t4046199807  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m267383841_gshared (KeyCollection_t3840194140 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	AN_ManifestPermissionU5BU5D_t246101855* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (AN_ManifestPermissionU5BU5D_t246101855*)((AN_ManifestPermissionU5BU5D_t246101855*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		AN_ManifestPermissionU5BU5D_t246101855* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		AN_ManifestPermissionU5BU5D_t246101855* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t3840194140 *)__this);
		((  void (*) (KeyCollection_t3840194140 *, AN_ManifestPermissionU5BU5D_t246101855*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t3840194140 *)__this, (AN_ManifestPermissionU5BU5D_t246101855*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1356696369 * L_4 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1356696369 *)L_4);
		((  void (*) (Dictionary_2_t1356696369 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1356696369 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1356696369 * L_7 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2201006514 * L_11 = (Transform_1_t2201006514 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2201006514 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1356696369 *)L_7);
		((  void (*) (Dictionary_2_t1356696369 *, Il2CppArray *, int32_t, Transform_1_t2201006514 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1356696369 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2201006514 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3253189504_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3840194140 *)__this);
		Enumerator_t4046199807  L_0 = ((  Enumerator_t4046199807  (*) (KeyCollection_t3840194140 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3840194140 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t4046199807  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m97127555_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2743757997_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m692264245_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m692264245_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m692264245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1356696369 * L_0 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3843875607_gshared (KeyCollection_t3840194140 * __this, AN_ManifestPermissionU5BU5D_t246101855* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1356696369 * L_0 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		AN_ManifestPermissionU5BU5D_t246101855* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1356696369 *)L_0);
		((  void (*) (Dictionary_2_t1356696369 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1356696369 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1356696369 * L_3 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		AN_ManifestPermissionU5BU5D_t246101855* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2201006514 * L_7 = (Transform_1_t2201006514 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2201006514 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1356696369 *)L_3);
		((  void (*) (Dictionary_2_t1356696369 *, AN_ManifestPermissionU5BU5D_t246101855*, int32_t, Transform_1_t2201006514 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1356696369 *)L_3, (AN_ManifestPermissionU5BU5D_t246101855*)L_4, (int32_t)L_5, (Transform_1_t2201006514 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::GetEnumerator()
extern "C"  Enumerator_t4046199807  KeyCollection_GetEnumerator_m3089413580_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1356696369 * L_0 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		Enumerator_t4046199807  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m911887285(&L_1, (Dictionary_2_t1356696369 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3544792145_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1356696369 * L_0 = (Dictionary_2_t1356696369 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1356696369 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1356696369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1356696369 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3523539860_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3523539860_gshared (KeyCollection_t966099451 * __this, Dictionary_2_t2777568976 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3523539860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2777568976 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t2777568976 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3322330018_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3322330018_gshared (KeyCollection_t966099451 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3322330018_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m903715163_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m903715163_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m903715163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m161914186_gshared (KeyCollection_t966099451 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t2777568976 * L_0 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t2777568976 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2777568976 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2777568976 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1659795907_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1659795907_gshared (KeyCollection_t966099451 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1659795907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m421845341_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t966099451 *)__this);
		Enumerator_t1172105118  L_0 = ((  Enumerator_t1172105118  (*) (KeyCollection_t966099451 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t966099451 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1172105118  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m355819049_gshared (KeyCollection_t966099451 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	FB_ProfileImageSizeU5BU5D_t4110693687* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (FB_ProfileImageSizeU5BU5D_t4110693687*)((FB_ProfileImageSizeU5BU5D_t4110693687*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		FB_ProfileImageSizeU5BU5D_t4110693687* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		FB_ProfileImageSizeU5BU5D_t4110693687* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t966099451 *)__this);
		((  void (*) (KeyCollection_t966099451 *, FB_ProfileImageSizeU5BU5D_t4110693687*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t966099451 *)__this, (FB_ProfileImageSizeU5BU5D_t4110693687*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2777568976 * L_4 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t2777568976 *)L_4);
		((  void (*) (Dictionary_2_t2777568976 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2777568976 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2777568976 * L_7 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1576166847 * L_11 = (Transform_1_t1576166847 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1576166847 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2777568976 *)L_7);
		((  void (*) (Dictionary_2_t2777568976 *, Il2CppArray *, int32_t, Transform_1_t1576166847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2777568976 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1576166847 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3039926290_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t966099451 *)__this);
		Enumerator_t1172105118  L_0 = ((  Enumerator_t1172105118  (*) (KeyCollection_t966099451 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t966099451 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1172105118  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4188921679_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3755359973_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m2694683641_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2694683641_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m2694683641_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2777568976 * L_0 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m285260995_gshared (KeyCollection_t966099451 * __this, FB_ProfileImageSizeU5BU5D_t4110693687* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t2777568976 * L_0 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		FB_ProfileImageSizeU5BU5D_t4110693687* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t2777568976 *)L_0);
		((  void (*) (Dictionary_2_t2777568976 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2777568976 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2777568976 * L_3 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		FB_ProfileImageSizeU5BU5D_t4110693687* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1576166847 * L_7 = (Transform_1_t1576166847 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1576166847 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2777568976 *)L_3);
		((  void (*) (Dictionary_2_t2777568976 *, FB_ProfileImageSizeU5BU5D_t4110693687*, int32_t, Transform_1_t1576166847 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2777568976 *)L_3, (FB_ProfileImageSizeU5BU5D_t4110693687*)L_4, (int32_t)L_5, (Transform_1_t1576166847 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1172105118  KeyCollection_GetEnumerator_m1955007816_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2777568976 * L_0 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		Enumerator_t1172105118  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m4138210453(&L_1, (Dictionary_2_t2777568976 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1680202429_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2777568976 * L_0 = (Dictionary_2_t2777568976 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t2777568976 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2777568976 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2777568976 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m4062613339_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m4062613339_gshared (KeyCollection_t525307964 * __this, Dictionary_2_t2336777489 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m4062613339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2336777489 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t2336777489 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3268284149_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3268284149_gshared (KeyCollection_t525307964 * __this, TypeNameKey_t3055062677  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3268284149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3452861472_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3452861472_gshared (KeyCollection_t525307964 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3452861472_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m792889935_gshared (KeyCollection_t525307964 * __this, TypeNameKey_t3055062677  ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t2336777489 * L_0 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		TypeNameKey_t3055062677  L_1 = ___item0;
		NullCheck((Dictionary_2_t2336777489 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2336777489 *, TypeNameKey_t3055062677 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2336777489 *)L_0, (TypeNameKey_t3055062677 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3172496972_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3172496972_gshared (KeyCollection_t525307964 * __this, TypeNameKey_t3055062677  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3172496972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2979423620_gshared (KeyCollection_t525307964 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t525307964 *)__this);
		Enumerator_t731313631  L_0 = ((  Enumerator_t731313631  (*) (KeyCollection_t525307964 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t525307964 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t731313631  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1224354938_gshared (KeyCollection_t525307964 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	TypeNameKeyU5BU5D_t3669902200* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (TypeNameKeyU5BU5D_t3669902200*)((TypeNameKeyU5BU5D_t3669902200*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		TypeNameKeyU5BU5D_t3669902200* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		TypeNameKeyU5BU5D_t3669902200* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t525307964 *)__this);
		((  void (*) (KeyCollection_t525307964 *, TypeNameKeyU5BU5D_t3669902200*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t525307964 *)__this, (TypeNameKeyU5BU5D_t3669902200*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2336777489 * L_4 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t2336777489 *)L_4);
		((  void (*) (Dictionary_2_t2336777489 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2336777489 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2336777489 * L_7 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2880512365 * L_11 = (Transform_1_t2880512365 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2880512365 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2336777489 *)L_7);
		((  void (*) (Dictionary_2_t2336777489 *, Il2CppArray *, int32_t, Transform_1_t2880512365 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2336777489 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2880512365 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1281160885_gshared (KeyCollection_t525307964 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t525307964 *)__this);
		Enumerator_t731313631  L_0 = ((  Enumerator_t731313631  (*) (KeyCollection_t525307964 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t525307964 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t731313631  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2503073622_gshared (KeyCollection_t525307964 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3401010050_gshared (KeyCollection_t525307964 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m2530940020_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2530940020_gshared (KeyCollection_t525307964 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m2530940020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2336777489 * L_0 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4034080824_gshared (KeyCollection_t525307964 * __this, TypeNameKeyU5BU5D_t3669902200* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t2336777489 * L_0 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		TypeNameKeyU5BU5D_t3669902200* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t2336777489 *)L_0);
		((  void (*) (Dictionary_2_t2336777489 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2336777489 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2336777489 * L_3 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		TypeNameKeyU5BU5D_t3669902200* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2880512365 * L_7 = (Transform_1_t2880512365 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2880512365 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2336777489 *)L_3);
		((  void (*) (Dictionary_2_t2336777489 *, TypeNameKeyU5BU5D_t3669902200*, int32_t, Transform_1_t2880512365 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2336777489 *)L_3, (TypeNameKeyU5BU5D_t3669902200*)L_4, (int32_t)L_5, (Transform_1_t2880512365 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::GetEnumerator()
extern "C"  Enumerator_t731313631  KeyCollection_GetEnumerator_m3700718939_gshared (KeyCollection_t525307964 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2336777489 * L_0 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		Enumerator_t731313631  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m1781268946(&L_1, (Dictionary_2_t2336777489 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m753038022_gshared (KeyCollection_t525307964 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2336777489 * L_0 = (Dictionary_2_t2336777489 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t2336777489 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2336777489 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2336777489 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m1127182464_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m1127182464_gshared (KeyCollection_t340536859 * __this, Dictionary_2_t2152006384 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m1127182464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2152006384 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t2152006384 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2759076618_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2759076618_gshared (KeyCollection_t340536859 * __this, TypeConvertKey_t1788482786  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2759076618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1211068205_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1211068205_gshared (KeyCollection_t340536859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1211068205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1044309878_gshared (KeyCollection_t340536859 * __this, TypeConvertKey_t1788482786  ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t2152006384 * L_0 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		TypeConvertKey_t1788482786  L_1 = ___item0;
		NullCheck((Dictionary_2_t2152006384 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2152006384 *, TypeConvertKey_t1788482786 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2152006384 *)L_0, (TypeConvertKey_t1788482786 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3879351865_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3879351865_gshared (KeyCollection_t340536859 * __this, TypeConvertKey_t1788482786  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3879351865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1126951839_gshared (KeyCollection_t340536859 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t340536859 *)__this);
		Enumerator_t546542526  L_0 = ((  Enumerator_t546542526  (*) (KeyCollection_t340536859 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t340536859 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t546542526  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2022837179_gshared (KeyCollection_t340536859 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	TypeConvertKeyU5BU5D_t3485131095* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (TypeConvertKeyU5BU5D_t3485131095*)((TypeConvertKeyU5BU5D_t3485131095*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		TypeConvertKeyU5BU5D_t3485131095* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		TypeConvertKeyU5BU5D_t3485131095* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t340536859 *)__this);
		((  void (*) (KeyCollection_t340536859 *, TypeConvertKeyU5BU5D_t3485131095*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t340536859 *)__this, (TypeConvertKeyU5BU5D_t3485131095*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2152006384 * L_4 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t2152006384 *)L_4);
		((  void (*) (Dictionary_2_t2152006384 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2152006384 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2152006384 * L_7 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1610273663 * L_11 = (Transform_1_t1610273663 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1610273663 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2152006384 *)L_7);
		((  void (*) (Dictionary_2_t2152006384 *, Il2CppArray *, int32_t, Transform_1_t1610273663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2152006384 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1610273663 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1608467718_gshared (KeyCollection_t340536859 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t340536859 *)__this);
		Enumerator_t546542526  L_0 = ((  Enumerator_t546542526  (*) (KeyCollection_t340536859 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t340536859 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t546542526  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17005197_gshared (KeyCollection_t340536859 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4076288283_gshared (KeyCollection_t340536859 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m1378479819_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1378479819_gshared (KeyCollection_t340536859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m1378479819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2152006384 * L_0 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3905328761_gshared (KeyCollection_t340536859 * __this, TypeConvertKeyU5BU5D_t3485131095* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t2152006384 * L_0 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		TypeConvertKeyU5BU5D_t3485131095* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t2152006384 *)L_0);
		((  void (*) (Dictionary_2_t2152006384 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2152006384 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2152006384 * L_3 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		TypeConvertKeyU5BU5D_t3485131095* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1610273663 * L_7 = (Transform_1_t1610273663 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1610273663 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2152006384 *)L_3);
		((  void (*) (Dictionary_2_t2152006384 *, TypeConvertKeyU5BU5D_t3485131095*, int32_t, Transform_1_t1610273663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2152006384 *)L_3, (TypeConvertKeyU5BU5D_t3485131095*)L_4, (int32_t)L_5, (Transform_1_t1610273663 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::GetEnumerator()
extern "C"  Enumerator_t546542526  KeyCollection_GetEnumerator_m3560649844_gshared (KeyCollection_t340536859 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2152006384 * L_0 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		Enumerator_t546542526  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m1214207411(&L_1, (Dictionary_2_t2152006384 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1001756307_gshared (KeyCollection_t340536859 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2152006384 * L_0 = (Dictionary_2_t2152006384 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t2152006384 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2152006384 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2152006384 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m4147384596_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m4147384596_gshared (KeyCollection_t547096553 * __this, Dictionary_2_t2358566078 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m4147384596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2358566078 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t2358566078 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1988457122_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1988457122_gshared (KeyCollection_t547096553 * __this, KeyValuePair_2_t3132015601  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1988457122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1204074107_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1204074107_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1204074107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1316007466_gshared (KeyCollection_t547096553 * __this, KeyValuePair_2_t3132015601  ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t2358566078 * L_0 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		KeyValuePair_2_t3132015601  L_1 = ___item0;
		NullCheck((Dictionary_2_t2358566078 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2358566078 *, KeyValuePair_2_t3132015601 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2358566078 *)L_0, (KeyValuePair_2_t3132015601 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m530240419_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m530240419_gshared (KeyCollection_t547096553 * __this, KeyValuePair_2_t3132015601  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m530240419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3039809725_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t547096553 *)__this);
		Enumerator_t753102220  L_0 = ((  Enumerator_t753102220  (*) (KeyCollection_t547096553 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t547096553 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t753102220  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1974943017_gshared (KeyCollection_t547096553 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	KeyValuePair_2U5BU5D_t14295340* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (KeyValuePair_2U5BU5D_t14295340*)((KeyValuePair_2U5BU5D_t14295340*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		KeyValuePair_2U5BU5D_t14295340* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		KeyValuePair_2U5BU5D_t14295340* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t547096553 *)__this);
		((  void (*) (KeyCollection_t547096553 *, KeyValuePair_2U5BU5D_t14295340*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t547096553 *)__this, (KeyValuePair_2U5BU5D_t14295340*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2358566078 * L_4 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t2358566078 *)L_4);
		((  void (*) (Dictionary_2_t2358566078 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2358566078 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2358566078 * L_7 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t4217319880 * L_11 = (Transform_1_t4217319880 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t4217319880 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2358566078 *)L_7);
		((  void (*) (Dictionary_2_t2358566078 *, Il2CppArray *, int32_t, Transform_1_t4217319880 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2358566078 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t4217319880 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1790121746_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t547096553 *)__this);
		Enumerator_t753102220  L_0 = ((  Enumerator_t753102220  (*) (KeyCollection_t547096553 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t547096553 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t753102220  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2634487951_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1603362373_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m963524697_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m963524697_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m963524697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2358566078 * L_0 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3856638403_gshared (KeyCollection_t547096553 * __this, KeyValuePair_2U5BU5D_t14295340* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t2358566078 * L_0 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		KeyValuePair_2U5BU5D_t14295340* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t2358566078 *)L_0);
		((  void (*) (Dictionary_2_t2358566078 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2358566078 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2358566078 * L_3 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		KeyValuePair_2U5BU5D_t14295340* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t4217319880 * L_7 = (Transform_1_t4217319880 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t4217319880 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2358566078 *)L_3);
		((  void (*) (Dictionary_2_t2358566078 *, KeyValuePair_2U5BU5D_t14295340*, int32_t, Transform_1_t4217319880 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2358566078 *)L_3, (KeyValuePair_2U5BU5D_t14295340*)L_4, (int32_t)L_5, (Transform_1_t4217319880 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t753102220  KeyCollection_GetEnumerator_m2656796808_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2358566078 * L_0 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		Enumerator_t753102220  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m3725287957(&L_1, (Dictionary_2_t2358566078 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2682794749_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2358566078 * L_0 = (Dictionary_2_t2358566078 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t2358566078 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2358566078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2358566078 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3425792570_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3425792570_gshared (KeyCollection_t3563200854 * __this, Dictionary_2_t1079703083 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3425792570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1079703083 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1079703083 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3194320320_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3194320320_gshared (KeyCollection_t3563200854 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3194320320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1966210003_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1966210003_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1966210003_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1541777540_gshared (KeyCollection_t3563200854 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1079703083 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1079703083 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4021128907_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4021128907_gshared (KeyCollection_t3563200854 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4021128907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2865503421_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3563200854 *)__this);
		Enumerator_t3769206521  L_0 = ((  Enumerator_t3769206521  (*) (KeyCollection_t3563200854 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3563200854 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3769206521  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m4204328697_gshared (KeyCollection_t3563200854 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		Int32U5BU5D_t3030399641* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t3563200854 *)__this);
		((  void (*) (KeyCollection_t3563200854 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t3563200854 *)__this, (Int32U5BU5D_t3030399641*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1079703083 * L_4 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1079703083 *)L_4);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1079703083 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1079703083 * L_7 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2737799006 * L_11 = (Transform_1_t2737799006 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2737799006 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1079703083 *)L_7);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, Transform_1_t2737799006 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1079703083 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2737799006 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2660051048_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3563200854 *)__this);
		Enumerator_t3769206521  L_0 = ((  Enumerator_t3769206521  (*) (KeyCollection_t3563200854 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3563200854 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3769206521  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1819096223_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1330657765_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m1792869049_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1792869049_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m1792869049_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1956025779_gshared (KeyCollection_t3563200854 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1079703083 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1079703083 * L_3 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2737799006 * L_7 = (Transform_1_t2737799006 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2737799006 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1079703083 *)L_3);
		((  void (*) (Dictionary_2_t1079703083 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t2737799006 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1079703083 *)L_3, (Int32U5BU5D_t3030399641*)L_4, (int32_t)L_5, (Transform_1_t2737799006 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t3769206521  KeyCollection_GetEnumerator_m2531464174_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Enumerator_t3769206521  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m356032285(&L_1, (Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1839036997_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1079703083 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3555561037_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3555561037_gshared (KeyCollection_t4180772701 * __this, Dictionary_2_t1697274930 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3555561037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1697274930 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1697274930 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3743129415_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3743129415_gshared (KeyCollection_t4180772701 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3743129415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m522538414_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m522538414_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m522538414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3840423669_gshared (KeyCollection_t4180772701 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1697274930 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2545700230_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2545700230_gshared (KeyCollection_t4180772701 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2545700230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3763330846_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t4180772701 *)__this);
		Enumerator_t91811072  L_0 = ((  Enumerator_t91811072  (*) (KeyCollection_t4180772701 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t4180772701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t91811072  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m4142454252_gshared (KeyCollection_t4180772701 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		Int32U5BU5D_t3030399641* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t4180772701 *)__this);
		((  void (*) (KeyCollection_t4180772701 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t4180772701 *)__this, (Int32U5BU5D_t3030399641*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1697274930 * L_4 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1697274930 *)L_4);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1697274930 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1697274930 * L_7 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3322033499 * L_11 = (Transform_1_t3322033499 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3322033499 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1697274930 *)L_7);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, Transform_1_t3322033499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1697274930 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t3322033499 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1589406383_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t4180772701 *)__this);
		Enumerator_t91811072  L_0 = ((  Enumerator_t91811072  (*) (KeyCollection_t4180772701 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t4180772701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t91811072  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1119857328_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1423289640_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3802319686_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3802319686_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3802319686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4056130090_gshared (KeyCollection_t4180772701 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1697274930 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1697274930 * L_3 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3322033499 * L_7 = (Transform_1_t3322033499 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3322033499 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1697274930 *)L_3);
		((  void (*) (Dictionary_2_t1697274930 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t3322033499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1697274930 *)L_3, (Int32U5BU5D_t3030399641*)L_4, (int32_t)L_5, (Transform_1_t3322033499 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t91811072  KeyCollection_GetEnumerator_m627495629_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Enumerator_t91811072  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m2975000652(&L_1, (Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2942903136_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m1987576649_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m1987576649_gshared (KeyCollection_t3818303076 * __this, Dictionary_2_t1334805305 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m1987576649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1334805305 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1334805305 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1663820351_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1663820351_gshared (KeyCollection_t3818303076 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1663820351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4215059836_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4215059836_gshared (KeyCollection_t3818303076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4215059836_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2522413849_gshared (KeyCollection_t3818303076 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1334805305 * L_0 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t1334805305 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1334805305 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1334805305 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1777369004_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1777369004_gshared (KeyCollection_t3818303076 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1777369004_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m794820358_gshared (KeyCollection_t3818303076 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3818303076 *)__this);
		Enumerator_t4024308743  L_0 = ((  Enumerator_t4024308743  (*) (KeyCollection_t3818303076 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3818303076 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t4024308743  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m764572622_gshared (KeyCollection_t3818303076 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t3818303076 *)__this);
		((  void (*) (KeyCollection_t3818303076 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t3818303076 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1334805305 * L_4 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1334805305 *)L_4);
		((  void (*) (Dictionary_2_t1334805305 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1334805305 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1334805305 * L_7 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1390570591 * L_11 = (Transform_1_t1390570591 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1390570591 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1334805305 *)L_7);
		((  void (*) (Dictionary_2_t1334805305 *, Il2CppArray *, int32_t, Transform_1_t1390570591 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1334805305 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1390570591 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m640671711_gshared (KeyCollection_t3818303076 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3818303076 *)__this);
		Enumerator_t4024308743  L_0 = ((  Enumerator_t4024308743  (*) (KeyCollection_t3818303076 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3818303076 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t4024308743  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2854085378_gshared (KeyCollection_t3818303076 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3114769898_gshared (KeyCollection_t3818303076 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m2841664318_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2841664318_gshared (KeyCollection_t3818303076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m2841664318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1334805305 * L_0 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1976612112_gshared (KeyCollection_t3818303076 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1334805305 * L_0 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1334805305 *)L_0);
		((  void (*) (Dictionary_2_t1334805305 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1334805305 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1334805305 * L_3 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1390570591 * L_7 = (Transform_1_t1390570591 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1390570591 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1334805305 *)L_3);
		((  void (*) (Dictionary_2_t1334805305 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1390570591 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1334805305 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t1390570591 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::GetEnumerator()
extern "C"  Enumerator_t4024308743  KeyCollection_GetEnumerator_m841135157_gshared (KeyCollection_t3818303076 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1334805305 * L_0 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		Enumerator_t4024308743  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m1888479228(&L_1, (Dictionary_2_t1334805305 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1140266162_gshared (KeyCollection_t3818303076 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1334805305 * L_0 = (Dictionary_2_t1334805305 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1334805305 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1334805305 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1334805305 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m1097052202_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m1097052202_gshared (KeyCollection_t2295758831 * __this, Dictionary_2_t4107228356 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m1097052202_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t4107228356 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t4107228356 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m524273720_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m524273720_gshared (KeyCollection_t2295758831 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m524273720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2288412869_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2288412869_gshared (KeyCollection_t2295758831 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2288412869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3977583980_gshared (KeyCollection_t2295758831 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t4107228356 * L_0 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t4107228356 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t4107228356 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t4107228356 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2967689953_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2967689953_gshared (KeyCollection_t2295758831 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2967689953_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m804534971_gshared (KeyCollection_t2295758831 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t2295758831 *)__this);
		Enumerator_t2501764498  L_0 = ((  Enumerator_t2501764498  (*) (KeyCollection_t2295758831 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t2295758831 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2501764498  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1706443839_gshared (KeyCollection_t2295758831 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t2295758831 *)__this);
		((  void (*) (KeyCollection_t2295758831 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t2295758831 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t4107228356 * L_4 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t4107228356 *)L_4);
		((  void (*) (Dictionary_2_t4107228356 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t4107228356 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t4107228356 * L_7 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3906118728 * L_11 = (Transform_1_t3906118728 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3906118728 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t4107228356 *)L_7);
		((  void (*) (Dictionary_2_t4107228356 *, Il2CppArray *, int32_t, Transform_1_t3906118728 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t4107228356 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t3906118728 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1169987572_gshared (KeyCollection_t2295758831 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t2295758831 *)__this);
		Enumerator_t2501764498  L_0 = ((  Enumerator_t2501764498  (*) (KeyCollection_t2295758831 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t2295758831 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2501764498  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3440331565_gshared (KeyCollection_t2295758831 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3165087239_gshared (KeyCollection_t2295758831 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m2142628175_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2142628175_gshared (KeyCollection_t2295758831 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m2142628175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t4107228356 * L_0 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2061850329_gshared (KeyCollection_t2295758831 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t4107228356 * L_0 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t4107228356 *)L_0);
		((  void (*) (Dictionary_2_t4107228356 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t4107228356 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t4107228356 * L_3 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3906118728 * L_7 = (Transform_1_t3906118728 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3906118728 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t4107228356 *)L_3);
		((  void (*) (Dictionary_2_t4107228356 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t3906118728 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t4107228356 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t3906118728 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::GetEnumerator()
extern "C"  Enumerator_t2501764498  KeyCollection_GetEnumerator_m2850483230_gshared (KeyCollection_t2295758831 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t4107228356 * L_0 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		Enumerator_t2501764498  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m3042794935(&L_1, (Dictionary_2_t4107228356 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m498834783_gshared (KeyCollection_t2295758831 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t4107228356 * L_0 = (Dictionary_2_t4107228356 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t4107228356 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t4107228356 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t4107228356 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m1150705803_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m1150705803_gshared (KeyCollection_t1606165321 * __this, Dictionary_2_t3417634846 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m1150705803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3417634846 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3417634846 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3083232821_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3083232821_gshared (KeyCollection_t1606165321 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3083232821_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1899586830_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1899586830_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1899586830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4184116031_gshared (KeyCollection_t1606165321 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3417634846 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3417634846 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2523591834_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2523591834_gshared (KeyCollection_t1606165321 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2523591834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2668668382_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1606165321 *)__this);
		Enumerator_t1812170988  L_0 = ((  Enumerator_t1812170988  (*) (KeyCollection_t1606165321 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1606165321 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1812170988  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2400289464_gshared (KeyCollection_t1606165321 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t1606165321 *)__this);
		((  void (*) (KeyCollection_t1606165321 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t1606165321 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3417634846 * L_4 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3417634846 *)L_4);
		((  void (*) (Dictionary_2_t3417634846 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3417634846 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3417634846 * L_7 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1854428742 * L_11 = (Transform_1_t1854428742 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1854428742 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3417634846 *)L_7);
		((  void (*) (Dictionary_2_t3417634846 *, Il2CppArray *, int32_t, Transform_1_t1854428742 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3417634846 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1854428742 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3918984589_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1606165321 *)__this);
		Enumerator_t1812170988  L_0 = ((  Enumerator_t1812170988  (*) (KeyCollection_t1606165321 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1606165321 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1812170988  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2773140624_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1443910992_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m4267671038_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m4267671038_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m4267671038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m668976366_gshared (KeyCollection_t1606165321 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		((  void (*) (Dictionary_2_t3417634846 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3417634846 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3417634846 * L_3 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1854428742 * L_7 = (Transform_1_t1854428742 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1854428742 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3417634846 *)L_3);
		((  void (*) (Dictionary_2_t3417634846 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1854428742 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3417634846 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t1854428742 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::GetEnumerator()
extern "C"  Enumerator_t1812170988  KeyCollection_GetEnumerator_m136587363_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		Enumerator_t1812170988  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m4035163080(&L_1, (Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m515229044_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3417634846 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3137980037_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3137980037_gshared (KeyCollection_t2114412544 * __this, Dictionary_2_t3925882069 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3137980037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3925882069 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3925882069 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m558540235_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m558540235_gshared (KeyCollection_t2114412544 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m558540235_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1125331542_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1125331542_gshared (KeyCollection_t2114412544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1125331542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3989501389_gshared (KeyCollection_t2114412544 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3925882069 * L_0 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3925882069 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3925882069 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3925882069 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1138580718_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1138580718_gshared (KeyCollection_t2114412544 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1138580718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3275159338_gshared (KeyCollection_t2114412544 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t2114412544 *)__this);
		Enumerator_t2320418211  L_0 = ((  Enumerator_t2320418211  (*) (KeyCollection_t2114412544 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t2114412544 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2320418211  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3071996584_gshared (KeyCollection_t2114412544 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t2114412544 *)__this);
		((  void (*) (KeyCollection_t2114412544 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t2114412544 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3925882069 * L_4 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3925882069 *)L_4);
		((  void (*) (Dictionary_2_t3925882069 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3925882069 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3925882069 * L_7 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1141314771 * L_11 = (Transform_1_t1141314771 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1141314771 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3925882069 *)L_7);
		((  void (*) (Dictionary_2_t3925882069 *, Il2CppArray *, int32_t, Transform_1_t1141314771 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3925882069 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1141314771 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m891332795_gshared (KeyCollection_t2114412544 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t2114412544 *)__this);
		Enumerator_t2320418211  L_0 = ((  Enumerator_t2320418211  (*) (KeyCollection_t2114412544 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t2114412544 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2320418211  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2512933368_gshared (KeyCollection_t2114412544 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m411580812_gshared (KeyCollection_t2114412544 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m9211794_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m9211794_gshared (KeyCollection_t2114412544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m9211794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3925882069 * L_0 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m678521234_gshared (KeyCollection_t2114412544 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3925882069 * L_0 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3925882069 *)L_0);
		((  void (*) (Dictionary_2_t3925882069 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3925882069 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3925882069 * L_3 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1141314771 * L_7 = (Transform_1_t1141314771 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1141314771 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3925882069 *)L_3);
		((  void (*) (Dictionary_2_t3925882069 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1141314771 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3925882069 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t1141314771 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C"  Enumerator_t2320418211  KeyCollection_GetEnumerator_m1898342493_gshared (KeyCollection_t2114412544 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3925882069 * L_0 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		Enumerator_t2320418211  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m526128336(&L_1, (Dictionary_2_t3925882069 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1955941012_gshared (KeyCollection_t2114412544 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3925882069 * L_0 = (Dictionary_2_t3925882069 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3925882069 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3925882069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3925882069 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m1627846113_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m1627846113_gshared (KeyCollection_t4147435347 * __this, Dictionary_2_t1663937576 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m1627846113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1663937576 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1663937576 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3888838575_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3888838575_gshared (KeyCollection_t4147435347 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3888838575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m237922362_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m237922362_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m237922362_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1114577217_gshared (KeyCollection_t4147435347 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1663937576 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1663937576 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2082552954_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2082552954_gshared (KeyCollection_t4147435347 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2082552954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4237862166_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t4147435347 *)__this);
		Enumerator_t58473718  L_0 = ((  Enumerator_t58473718  (*) (KeyCollection_t4147435347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t4147435347 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t58473718  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3219210900_gshared (KeyCollection_t4147435347 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t4147435347 *)__this);
		((  void (*) (KeyCollection_t4147435347 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t4147435347 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1663937576 * L_4 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1663937576 *)L_4);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1663937576 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1663937576 * L_7 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1316794068 * L_11 = (Transform_1_t1316794068 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1316794068 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1663937576 *)L_7);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppArray *, int32_t, Transform_1_t1316794068 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1663937576 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1316794068 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3069883463_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t4147435347 *)__this);
		Enumerator_t58473718  L_0 = ((  Enumerator_t58473718  (*) (KeyCollection_t4147435347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t4147435347 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t58473718  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2823494596_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1497186736_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3825728526_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3825728526_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3825728526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1942070334_gshared (KeyCollection_t4147435347 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1663937576 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1663937576 * L_3 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1316794068 * L_7 = (Transform_1_t1316794068 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1316794068 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1663937576 *)L_3);
		((  void (*) (Dictionary_2_t1663937576 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1316794068 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1663937576 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t1316794068 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t58473718  KeyCollection_GetEnumerator_m3925297057_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		Enumerator_t58473718  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m1642475044(&L_1, (Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3930094200_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1663937576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m4000691336_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m4000691336_gshared (KeyCollection_t470039898 * __this, Dictionary_2_t2281509423 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m4000691336_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2281509423 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t2281509423 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m726860246_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m726860246_gshared (KeyCollection_t470039898 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m726860246_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3185000447_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3185000447_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3185000447_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m580889838_gshared (KeyCollection_t470039898 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1818919095_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1818919095_gshared (KeyCollection_t470039898 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1818919095_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m701895513_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t470039898 *)__this);
		Enumerator_t676045565  L_0 = ((  Enumerator_t676045565  (*) (KeyCollection_t470039898 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t470039898 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t676045565  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m201091229_gshared (KeyCollection_t470039898 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t470039898 *)__this);
		((  void (*) (KeyCollection_t470039898 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t470039898 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2281509423 * L_4 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t2281509423 *)L_4);
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2281509423 * L_7 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1901028561 * L_11 = (Transform_1_t1901028561 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1901028561 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2281509423 *)L_7);
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppArray *, int32_t, Transform_1_t1901028561 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2281509423 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1901028561 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1743416022_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t470039898 *)__this);
		Enumerator_t676045565  L_0 = ((  Enumerator_t676045565  (*) (KeyCollection_t470039898 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t470039898 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t676045565  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m701366755_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4278618649_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3348206461_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3348206461_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3348206461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1469814847_gshared (KeyCollection_t470039898 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2281509423 * L_3 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1901028561 * L_7 = (Transform_1_t1901028561 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1901028561 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2281509423 *)L_3);
		((  void (*) (Dictionary_2_t2281509423 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1901028561 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2281509423 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t1901028561 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t676045565  KeyCollection_GetEnumerator_m3123493604_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		Enumerator_t676045565  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m2650732273(&L_1, (Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2913499705_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3936869699_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3936869699_gshared (KeyCollection_t997898759 * __this, Dictionary_2_t2809368284 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3936869699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2809368284 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t2809368284 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1332390941_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1332390941_gshared (KeyCollection_t997898759 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1332390941_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1622226282_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1622226282_gshared (KeyCollection_t997898759 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1622226282_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3889275247_gshared (KeyCollection_t997898759 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t2809368284 * L_0 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t2809368284 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2809368284 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2809368284 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2072201598_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2072201598_gshared (KeyCollection_t997898759 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2072201598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1724912324_gshared (KeyCollection_t997898759 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t997898759 *)__this);
		Enumerator_t1203904426  L_0 = ((  Enumerator_t1203904426  (*) (KeyCollection_t997898759 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t997898759 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1203904426  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m973491460_gshared (KeyCollection_t997898759 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	AndroidStoreU5BU5D_t4142492995* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (AndroidStoreU5BU5D_t4142492995*)((AndroidStoreU5BU5D_t4142492995*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		AndroidStoreU5BU5D_t4142492995* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		AndroidStoreU5BU5D_t4142492995* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t997898759 *)__this);
		((  void (*) (KeyCollection_t997898759 *, AndroidStoreU5BU5D_t4142492995*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t997898759 *)__this, (AndroidStoreU5BU5D_t4142492995*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2809368284 * L_4 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t2809368284 *)L_4);
		((  void (*) (Dictionary_2_t2809368284 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2809368284 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2809368284 * L_7 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t572557287 * L_11 = (Transform_1_t572557287 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t572557287 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2809368284 *)L_7);
		((  void (*) (Dictionary_2_t2809368284 *, Il2CppArray *, int32_t, Transform_1_t572557287 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2809368284 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t572557287 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2949017121_gshared (KeyCollection_t997898759 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t997898759 *)__this);
		Enumerator_t1203904426  L_0 = ((  Enumerator_t1203904426  (*) (KeyCollection_t997898759 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t997898759 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1203904426  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m511215476_gshared (KeyCollection_t997898759 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1658887108_gshared (KeyCollection_t997898759 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m1786882788_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1786882788_gshared (KeyCollection_t997898759 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m1786882788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2809368284 * L_0 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2775247106_gshared (KeyCollection_t997898759 * __this, AndroidStoreU5BU5D_t4142492995* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t2809368284 * L_0 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		AndroidStoreU5BU5D_t4142492995* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t2809368284 *)L_0);
		((  void (*) (Dictionary_2_t2809368284 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2809368284 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2809368284 * L_3 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		AndroidStoreU5BU5D_t4142492995* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t572557287 * L_7 = (Transform_1_t572557287 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t572557287 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2809368284 *)L_3);
		((  void (*) (Dictionary_2_t2809368284 *, AndroidStoreU5BU5D_t4142492995*, int32_t, Transform_1_t572557287 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2809368284 *)L_3, (AndroidStoreU5BU5D_t4142492995*)L_4, (int32_t)L_5, (Transform_1_t572557287 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1203904426  KeyCollection_GetEnumerator_m905785543_gshared (KeyCollection_t997898759 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2809368284 * L_0 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		Enumerator_t1203904426  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m2213644398(&L_1, (Dictionary_2_t2809368284 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AndroidStore,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3954210300_gshared (KeyCollection_t997898759 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2809368284 * L_0 = (Dictionary_2_t2809368284 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t2809368284 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2809368284 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2809368284 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1953638932_gshared (ShimEnumerator_t1461821190 * __this, Dictionary_2_t1356696369 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1356696369 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1356696369 *)L_0);
		Enumerator_t2676721071  L_1 = ((  Enumerator_t2676721071  (*) (Dictionary_2_t1356696369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1356696369 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2122901333_gshared (ShimEnumerator_t1461821190 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2676721071 * L_0 = (Enumerator_t2676721071 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1965426417((Enumerator_t2676721071 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m781064369_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m781064369_gshared (ShimEnumerator_t1461821190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m781064369_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t2676721071  L_0 = (Enumerator_t2676721071 )__this->get_host_enumerator_0();
		Enumerator_t2676721071  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2343961516_gshared (ShimEnumerator_t1461821190 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3409008887  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2676721071 * L_0 = (Enumerator_t2676721071 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3409008887  L_1 = Enumerator_get_Current_m4125464454((Enumerator_t2676721071 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3409008887 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m232446462((KeyValuePair_2_t3409008887 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1947321390_gshared (ShimEnumerator_t1461821190 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3409008887  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2676721071 * L_0 = (Enumerator_t2676721071 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3409008887  L_1 = Enumerator_get_Current_m4125464454((Enumerator_t2676721071 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3409008887 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m1557526483((KeyValuePair_2_t3409008887 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m1719580234_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1719580234_gshared (ShimEnumerator_t1461821190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m1719580234_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1461821190 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1461821190 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1461821190 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<AN_ManifestPermission,AN_PermissionState>::Reset()
extern "C"  void ShimEnumerator_Reset_m44744342_gshared (ShimEnumerator_t1461821190 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2676721071 * L_0 = (Enumerator_t2676721071 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3948831101((Enumerator_t2676721071 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3091092150_gshared (ShimEnumerator_t2882693797 * __this, Dictionary_2_t2777568976 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2777568976 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2777568976 *)L_0);
		Enumerator_t4097593678  L_1 = ((  Enumerator_t4097593678  (*) (Dictionary_2_t2777568976 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2777568976 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3382097401_gshared (ShimEnumerator_t2882693797 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4097593678 * L_0 = (Enumerator_t4097593678 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3732010148((Enumerator_t4097593678 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2722709869_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2722709869_gshared (ShimEnumerator_t2882693797 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2722709869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t4097593678  L_0 = (Enumerator_t4097593678 )__this->get_host_enumerator_0();
		Enumerator_t4097593678  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1988482512_gshared (ShimEnumerator_t2882693797 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t534914198  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t4097593678 * L_0 = (Enumerator_t4097593678 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t534914198  L_1 = Enumerator_get_Current_m4217501564((Enumerator_t4097593678 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t534914198 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m1840008439((KeyValuePair_2_t534914198 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2201824138_gshared (ShimEnumerator_t2882693797 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t534914198  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t4097593678 * L_0 = (Enumerator_t4097593678 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t534914198  L_1 = Enumerator_get_Current_m4217501564((Enumerator_t4097593678 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t534914198 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m1210921623((KeyValuePair_2_t534914198 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m56500262_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m56500262_gshared (ShimEnumerator_t2882693797 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m56500262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2882693797 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2882693797 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2882693797 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3295896912_gshared (ShimEnumerator_t2882693797 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4097593678 * L_0 = (Enumerator_t4097593678 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m812636689((Enumerator_t4097593678 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1589479225_gshared (ShimEnumerator_t2441902310 * __this, Dictionary_2_t2336777489 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2336777489 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2336777489 *)L_0);
		Enumerator_t3656802191  L_1 = ((  Enumerator_t3656802191  (*) (Dictionary_2_t2336777489 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2336777489 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1540488542_gshared (ShimEnumerator_t2441902310 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3656802191 * L_0 = (Enumerator_t3656802191 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1720213575((Enumerator_t3656802191 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m1622615648_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m1622615648_gshared (ShimEnumerator_t2441902310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m1622615648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3656802191  L_0 = (Enumerator_t3656802191 )__this->get_host_enumerator_0();
		Enumerator_t3656802191  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2942383841_gshared (ShimEnumerator_t2441902310 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t94122711  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3656802191 * L_0 = (Enumerator_t3656802191 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t94122711  L_1 = Enumerator_get_Current_m4237110135((Enumerator_t3656802191 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t94122711 )L_1;
		TypeNameKey_t3055062677  L_2 = KeyValuePair_2_get_Key_m2430213422((KeyValuePair_2_t94122711 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		TypeNameKey_t3055062677  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2278482673_gshared (ShimEnumerator_t2441902310 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t94122711  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3656802191 * L_0 = (Enumerator_t3656802191 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t94122711  L_1 = Enumerator_get_Current_m4237110135((Enumerator_t3656802191 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t94122711 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m470622750((KeyValuePair_2_t94122711 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m1303198503_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1303198503_gshared (ShimEnumerator_t2441902310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m1303198503_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2441902310 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2441902310 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2441902310 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1230563683_gshared (ShimEnumerator_t2441902310 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3656802191 * L_0 = (Enumerator_t3656802191 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1824444848((Enumerator_t3656802191 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1916723382_gshared (ShimEnumerator_t2257131205 * __this, Dictionary_2_t2152006384 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2152006384 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2152006384 *)L_0);
		Enumerator_t3472031086  L_1 = ((  Enumerator_t3472031086  (*) (Dictionary_2_t2152006384 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2152006384 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1876215823_gshared (ShimEnumerator_t2257131205 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3472031086 * L_0 = (Enumerator_t3472031086 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m72413836((Enumerator_t3472031086 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m44962303_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m44962303_gshared (ShimEnumerator_t2257131205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m44962303_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3472031086  L_0 = (Enumerator_t3472031086 )__this->get_host_enumerator_0();
		Enumerator_t3472031086  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2204103376_gshared (ShimEnumerator_t2257131205 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4204318902  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3472031086 * L_0 = (Enumerator_t3472031086 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t4204318902  L_1 = Enumerator_get_Current_m2852501768((Enumerator_t3472031086 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t4204318902 )L_1;
		TypeConvertKey_t1788482786  L_2 = KeyValuePair_2_get_Key_m3955619893((KeyValuePair_2_t4204318902 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		TypeConvertKey_t1788482786  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1332664062_gshared (ShimEnumerator_t2257131205 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4204318902  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3472031086 * L_0 = (Enumerator_t3472031086 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t4204318902  L_1 = Enumerator_get_Current_m2852501768((Enumerator_t3472031086 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t4204318902 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m2877229269((KeyValuePair_2_t4204318902 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3654388742_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3654388742_gshared (ShimEnumerator_t2257131205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3654388742_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2257131205 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2257131205 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2257131205 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1919889040_gshared (ShimEnumerator_t2257131205 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3472031086 * L_0 = (Enumerator_t3472031086 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m62122567((Enumerator_t3472031086 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m951601814_gshared (ShimEnumerator_t2463690899 * __this, Dictionary_2_t2358566078 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2358566078 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2358566078 *)L_0);
		Enumerator_t3678590780  L_1 = ((  Enumerator_t3678590780  (*) (Dictionary_2_t2358566078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2358566078 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2051636185_gshared (ShimEnumerator_t2463690899 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3678590780 * L_0 = (Enumerator_t3678590780 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3696685668((Enumerator_t3678590780 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2321025869_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2321025869_gshared (ShimEnumerator_t2463690899 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2321025869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3678590780  L_0 = (Enumerator_t3678590780 )__this->get_host_enumerator_0();
		Enumerator_t3678590780  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1655108688_gshared (ShimEnumerator_t2463690899 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t115911300  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3678590780 * L_0 = (Enumerator_t3678590780 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t115911300  L_1 = Enumerator_get_Current_m1763492508((Enumerator_t3678590780 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t115911300 )L_1;
		KeyValuePair_2_t3132015601  L_2 = KeyValuePair_2_get_Key_m3310904759((KeyValuePair_2_t115911300 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		KeyValuePair_2_t3132015601  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m456005610_gshared (ShimEnumerator_t2463690899 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t115911300  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3678590780 * L_0 = (Enumerator_t3678590780 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t115911300  L_1 = Enumerator_get_Current_m1763492508((Enumerator_t3678590780 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t115911300 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m2441171031((KeyValuePair_2_t115911300 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3788940230_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3788940230_gshared (ShimEnumerator_t2463690899 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3788940230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2463690899 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2463690899 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2463690899 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m1423171536_gshared (ShimEnumerator_t2463690899 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3678590780 * L_0 = (Enumerator_t3678590780 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3460173937((Enumerator_t3678590780 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m4003681516_gshared (ShimEnumerator_t1184827904 * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1079703083 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		Enumerator_t2399727785  L_1 = ((  Enumerator_t2399727785  (*) (Dictionary_2_t1079703083 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2566611377_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3809319098((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m1209161525_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m1209161525_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m1209161525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t2399727785  L_0 = (Enumerator_t2399727785 )__this->get_host_enumerator_0();
		Enumerator_t2399727785  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3497731930_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3132015601  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3132015601  L_1 = Enumerator_get_Current_m3655806242((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3132015601 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m3881724((KeyValuePair_2_t3132015601 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m184655936_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3132015601  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3132015601  L_1 = Enumerator_get_Current_m3655806242((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3132015601 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m1512166271((KeyValuePair_2_t3132015601 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m972214768_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m972214768_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m972214768_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1184827904 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1184827904 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1184827904 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m2258067398_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2787013825((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3996137855_gshared (ShimEnumerator_t1802399751 * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1697274930 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		Enumerator_t3017299632  L_1 = ((  Enumerator_t3017299632  (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3313047792_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2770956757((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2387156530_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2387156530_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2387156530_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3017299632  L_0 = (Enumerator_t3017299632 )__this->get_host_enumerator_0();
		Enumerator_t3017299632  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2823867931_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3749587448  L_1 = Enumerator_get_Current_m2230224741((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3749587448 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3551354763_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3749587448  L_1 = Enumerator_get_Current_m2230224741((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3749587448 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m1093801549_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1093801549_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m1093801549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1802399751 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1802399751 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1802399751 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m98005789_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m761796566((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m999822191_gshared (ShimEnumerator_t1439930126 * __this, Dictionary_2_t1334805305 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1334805305 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1334805305 *)L_0);
		Enumerator_t2654830007  L_1 = ((  Enumerator_t2654830007  (*) (Dictionary_2_t1334805305 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1334805305 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3177276602_gshared (ShimEnumerator_t1439930126 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2654830007 * L_0 = (Enumerator_t2654830007 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m657838813((Enumerator_t2654830007 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m3548669866_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3548669866_gshared (ShimEnumerator_t1439930126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m3548669866_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t2654830007  L_0 = (Enumerator_t2654830007 )__this->get_host_enumerator_0();
		Enumerator_t2654830007  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2569292831_gshared (ShimEnumerator_t1439930126 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3387117823  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2654830007 * L_0 = (Enumerator_t2654830007 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3387117823  L_1 = Enumerator_get_Current_m954143681((Enumerator_t2654830007 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3387117823 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m372290202((KeyValuePair_2_t3387117823 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m954032039_gshared (ShimEnumerator_t1439930126 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3387117823  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2654830007 * L_0 = (Enumerator_t2654830007 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3387117823  L_1 = Enumerator_get_Current_m954143681((Enumerator_t2654830007 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3387117823 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m3683399194((KeyValuePair_2_t3387117823 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m1572723561_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1572723561_gshared (ShimEnumerator_t1439930126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m1572723561_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1439930126 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1439930126 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1439930126 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::Reset()
extern "C"  void ShimEnumerator_Reset_m3751950453_gshared (ShimEnumerator_t1439930126 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2654830007 * L_0 = (Enumerator_t2654830007 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m367248862((Enumerator_t2654830007 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1079062548_gshared (ShimEnumerator_t4212353177 * __this, Dictionary_2_t4107228356 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t4107228356 * L_0 = ___host0;
		NullCheck((Dictionary_2_t4107228356 *)L_0);
		Enumerator_t1132285762  L_1 = ((  Enumerator_t1132285762  (*) (Dictionary_2_t4107228356 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t4107228356 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1721352163_gshared (ShimEnumerator_t4212353177 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1132285762 * L_0 = (Enumerator_t1132285762 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1405845710((Enumerator_t1132285762 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m1044141251_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m1044141251_gshared (ShimEnumerator_t4212353177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m1044141251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1132285762  L_0 = (Enumerator_t1132285762 )__this->get_host_enumerator_0();
		Enumerator_t1132285762  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2234662578_gshared (ShimEnumerator_t4212353177 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1864573578  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1132285762 * L_0 = (Enumerator_t1132285762 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1864573578  L_1 = Enumerator_get_Current_m3066235090((Enumerator_t1132285762 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1864573578 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1570484949((KeyValuePair_2_t1864573578 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3180960364_gshared (ShimEnumerator_t4212353177 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1864573578  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1132285762 * L_0 = (Enumerator_t1132285762 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1864573578  L_1 = Enumerator_get_Current_m3066235090((Enumerator_t1132285762 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1864573578 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m2261596981((KeyValuePair_2_t1864573578 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2858995588_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2858995588_gshared (ShimEnumerator_t4212353177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2858995588_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t4212353177 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t4212353177 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t4212353177 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Reset()
extern "C"  void ShimEnumerator_Reset_m39425650_gshared (ShimEnumerator_t4212353177 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1132285762 * L_0 = (Enumerator_t1132285762 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3814399987((Enumerator_t1132285762 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2428699265_gshared (ShimEnumerator_t3522759667 * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3417634846 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		Enumerator_t442692252  L_1 = ((  Enumerator_t442692252  (*) (Dictionary_2_t3417634846 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2943029388_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1856697671((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2332479818_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2332479818_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2332479818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t442692252  L_0 = (Enumerator_t442692252 )__this->get_host_enumerator_0();
		Enumerator_t442692252  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m616785465_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1174980068  L_1 = Enumerator_get_Current_m1020413567((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1174980068 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1396288849_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1174980068  L_1 = Enumerator_get_Current_m1020413567((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1174980068 )L_1;
		bool L_2 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		bool L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2516732679_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2516732679_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2516732679_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3522759667 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3522759667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3522759667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::Reset()
extern "C"  void ShimEnumerator_Reset_m2247049027_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3115320746((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m238060027_gshared (ShimEnumerator_t4031006890 * __this, Dictionary_2_t3925882069 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3925882069 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3925882069 *)L_0);
		Enumerator_t950939475  L_1 = ((  Enumerator_t950939475  (*) (Dictionary_2_t3925882069 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3925882069 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3839820244_gshared (ShimEnumerator_t4031006890 * __this, const MethodInfo* method)
{
	{
		Enumerator_t950939475 * L_0 = (Enumerator_t950939475 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1022654781((Enumerator_t950939475 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2561860342_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2561860342_gshared (ShimEnumerator_t4031006890 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2561860342_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t950939475  L_0 = (Enumerator_t950939475 )__this->get_host_enumerator_0();
		Enumerator_t950939475  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m119909167_gshared (ShimEnumerator_t4031006890 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t950939475 * L_0 = (Enumerator_t950939475 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1683227291  L_1 = Enumerator_get_Current_m304262005((Enumerator_t950939475 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1683227291 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m4247498472((KeyValuePair_2_t1683227291 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3300397079_gshared (ShimEnumerator_t4031006890 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t950939475 * L_0 = (Enumerator_t950939475 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1683227291  L_1 = Enumerator_get_Current_m304262005((Enumerator_t950939475 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1683227291 )L_1;
		KeyValuePair_2_t38854645  L_2 = KeyValuePair_2_get_Value_m667219360((KeyValuePair_2_t1683227291 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		KeyValuePair_2_t38854645  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m665867845_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m665867845_gshared (ShimEnumerator_t4031006890 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m665867845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t4031006890 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t4031006890 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t4031006890 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reset()
extern "C"  void ShimEnumerator_Reset_m3429861797_gshared (ShimEnumerator_t4031006890 * __this, const MethodInfo* method)
{
	{
		Enumerator_t950939475 * L_0 = (Enumerator_t950939475 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m535811698((Enumerator_t950939475 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1807768263_gshared (ShimEnumerator_t1769062397 * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1663937576 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		Enumerator_t2983962278  L_1 = ((  Enumerator_t2983962278  (*) (Dictionary_2_t1663937576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2728191736_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m435964161((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2171963450_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2171963450_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2171963450_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t2983962278  L_0 = (Enumerator_t2983962278 )__this->get_host_enumerator_0();
		Enumerator_t2983962278  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m4014537779_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3716250094  L_1 = Enumerator_get_Current_m1932198897((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3716250094 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1198202883_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3716250094  L_1 = Enumerator_get_Current_m1932198897((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3716250094 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m696250329_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m696250329_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m696250329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1769062397 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1769062397 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1769062397 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m208070833_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1132695838((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m119758426_gshared (ShimEnumerator_t2386634244 * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2281509423 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2013866013_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3349738440((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m4233876641_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m4233876641_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m4233876641_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3601534125  L_0 = (Enumerator_t3601534125 )__this->get_host_enumerator_0();
		Enumerator_t3601534125  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3962796804_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t38854645  L_1 = Enumerator_get_Current_m25299632((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t38854645 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2522747790_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t38854645  L_1 = Enumerator_get_Current_m25299632((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t38854645 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2121723938_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2121723938_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2121723938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2386634244 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2386634244 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2386634244 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1100368508_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3129803197((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1027581885_gshared (ShimEnumerator_t2914493105 * __this, Dictionary_2_t2809368284 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2809368284 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2809368284 *)L_0);
		Enumerator_t4129392986  L_1 = ((  Enumerator_t4129392986  (*) (Dictionary_2_t2809368284 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2809368284 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3225225260_gshared (ShimEnumerator_t2914493105 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4129392986 * L_0 = (Enumerator_t4129392986 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m4270128035((Enumerator_t4129392986 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m822661256_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m822661256_gshared (ShimEnumerator_t2914493105 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m822661256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t4129392986  L_0 = (Enumerator_t4129392986 )__this->get_host_enumerator_0();
		Enumerator_t4129392986  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m137456889_gshared (ShimEnumerator_t2914493105 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t566713506  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t4129392986 * L_0 = (Enumerator_t4129392986 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t566713506  L_1 = Enumerator_get_Current_m359771003((Enumerator_t4129392986 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t566713506 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m3358872092((KeyValuePair_2_t566713506 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m863061545_gshared (ShimEnumerator_t2914493105 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t566713506  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t4129392986 * L_0 = (Enumerator_t4129392986 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t566713506  L_1 = Enumerator_get_Current_m359771003((Enumerator_t4129392986 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t566713506 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m121202140((KeyValuePair_2_t566713506 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2573204027_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2573204027_gshared (ShimEnumerator_t2914493105 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2573204027_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2914493105 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2914493105 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2914493105 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AndroidStore,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1767204987_gshared (ShimEnumerator_t2914493105 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4129392986 * L_0 = (Enumerator_t4129392986 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1125442816((Enumerator_t4129392986 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,AN_ManifestPermission>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4246032573_gshared (Transform_1_t2201006514 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,AN_ManifestPermission>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1308411657_gshared (Transform_1_t2201006514 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1308411657((Transform_1_t2201006514 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,AN_ManifestPermission>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* AN_ManifestPermission_t1772110202_il2cpp_TypeInfo_var;
extern Il2CppClass* AN_PermissionState_t838201224_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1552783184_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1552783184_gshared (Transform_1_t2201006514 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1552783184_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(AN_ManifestPermission_t1772110202_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(AN_PermissionState_t838201224_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,AN_ManifestPermission>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m714762367_gshared (Transform_1_t2201006514 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,AN_PermissionState>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1485627017_gshared (Transform_1_t1267097536 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,AN_PermissionState>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2586772981_gshared (Transform_1_t1267097536 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2586772981((Transform_1_t1267097536 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,AN_PermissionState>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* AN_ManifestPermission_t1772110202_il2cpp_TypeInfo_var;
extern Il2CppClass* AN_PermissionState_t838201224_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2152616554_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2152616554_gshared (Transform_1_t1267097536 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2152616554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(AN_ManifestPermission_t1772110202_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(AN_PermissionState_t838201224_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,AN_PermissionState>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1744035479_gshared (Transform_1_t1267097536 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m922634565_gshared (Transform_1_t3477771710 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1971516945_gshared (Transform_1_t3477771710 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1971516945((Transform_1_t3477771710 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* AN_ManifestPermission_t1772110202_il2cpp_TypeInfo_var;
extern Il2CppClass* AN_PermissionState_t838201224_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2441926554_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2441926554_gshared (Transform_1_t3477771710 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2441926554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(AN_ManifestPermission_t1772110202_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(AN_PermissionState_t838201224_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m981130543_gshared (Transform_1_t3477771710 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2093557380_gshared (Transform_1_t3837905199 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3409008887  Transform_1_Invoke_m1539324708_gshared (Transform_1_t3837905199 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1539324708((Transform_1_t3837905199 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3409008887  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3409008887  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* AN_ManifestPermission_t1772110202_il2cpp_TypeInfo_var;
extern Il2CppClass* AN_PermissionState_t838201224_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m403507131_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m403507131_gshared (Transform_1_t3837905199 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m403507131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(AN_ManifestPermission_t1772110202_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(AN_PermissionState_t838201224_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<AN_ManifestPermission,AN_PermissionState,System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3409008887  Transform_1_EndInvoke_m2878056366_gshared (Transform_1_t3837905199 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3409008887 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,FB_ProfileImageSize>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1740663031_gshared (Transform_1_t1576166847 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,FB_ProfileImageSize>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m302575779_gshared (Transform_1_t1576166847 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m302575779((Transform_1_t1576166847 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,FB_ProfileImageSize>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* FB_ProfileImageSize_t3003328130_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4193502274_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4193502274_gshared (Transform_1_t1576166847 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4193502274_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(FB_ProfileImageSize_t3003328130_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,FB_ProfileImageSize>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1459725537_gshared (Transform_1_t1576166847 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3418423829_gshared (Transform_1_t1621714115 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2961650825_gshared (Transform_1_t1621714115 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2961650825((Transform_1_t1621714115 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* FB_ProfileImageSize_t3003328130_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2451834188_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2451834188_gshared (Transform_1_t1621714115 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2451834188_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(FB_ProfileImageSize_t3003328130_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m1593666539_gshared (Transform_1_t1621714115 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1242909878_gshared (Transform_1_t3402720211 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t534914198  Transform_1_Invoke_m2238554678_gshared (Transform_1_t3402720211 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2238554678((Transform_1_t3402720211 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t534914198  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t534914198  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* FB_ProfileImageSize_t3003328130_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3497541983_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3497541983_gshared (Transform_1_t3402720211 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3497541983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(FB_ProfileImageSize_t3003328130_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t534914198  Transform_1_EndInvoke_m2825378316_gshared (Transform_1_t3402720211 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t534914198 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1656594587_gshared (Transform_1_t1262288012 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2821835871_gshared (Transform_1_t1262288012 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2821835871((Transform_1_t1262288012 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* FB_ProfileImageSize_t3003328130_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m559093446_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m559093446_gshared (Transform_1_t1262288012 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m559093446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(FB_ProfileImageSize_t3003328130_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<FB_ProfileImageSize,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m4038714389_gshared (Transform_1_t1262288012 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3401206923_gshared (Transform_1_t2880512365 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::Invoke(TKey,TValue)
extern "C"  TypeNameKey_t3055062677  Transform_1_Invoke_m710362159_gshared (Transform_1_t2880512365 * __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m710362159((Transform_1_t2880512365 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef TypeNameKey_t3055062677  (*FunctionPointerType) (Il2CppObject *, void* __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef TypeNameKey_t3055062677  (*FunctionPointerType) (void* __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TypeNameKey_t3055062677_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1222685860_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1222685860_gshared (Transform_1_t2880512365 * __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1222685860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TypeNameKey_t3055062677_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::EndInvoke(System.IAsyncResult)
extern "C"  TypeNameKey_t3055062677  Transform_1_EndInvoke_m1693676493_gshared (Transform_1_t2880512365 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(TypeNameKey_t3055062677 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1575691540_gshared (Transform_1_t2874325086 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2847095676_gshared (Transform_1_t2874325086 * __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2847095676((Transform_1_t2874325086 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TypeNameKey_t3055062677_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2203528219_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2203528219_gshared (Transform_1_t2874325086 * __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2203528219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TypeNameKey_t3055062677_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m1019606674_gshared (Transform_1_t2874325086 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2030652522_gshared (Transform_1_t4214539695 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t94122711  Transform_1_Invoke_m1045128842_gshared (Transform_1_t4214539695 * __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1045128842((Transform_1_t4214539695 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t94122711  (*FunctionPointerType) (Il2CppObject *, void* __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t94122711  (*FunctionPointerType) (void* __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TypeNameKey_t3055062677_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3184063799_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3184063799_gshared (Transform_1_t4214539695 * __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3184063799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TypeNameKey_t3055062677_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t94122711  Transform_1_EndInvoke_m36531412_gshared (Transform_1_t4214539695 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t94122711 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3438402792_gshared (Transform_1_t2514898983 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m978486312_gshared (Transform_1_t2514898983 * __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m978486312((Transform_1_t2514898983 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TypeNameKey_t3055062677_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2336330045_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2336330045_gshared (Transform_1_t2514898983 * __this, TypeNameKey_t3055062677  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2336330045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TypeNameKey_t3055062677_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m2741365418_gshared (Transform_1_t2514898983 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m545728375_gshared (Transform_1_t1610273663 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::Invoke(TKey,TValue)
extern "C"  TypeConvertKey_t1788482786  Transform_1_Invoke_m3638210083_gshared (Transform_1_t1610273663 * __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3638210083((Transform_1_t1610273663 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef TypeConvertKey_t1788482786  (*FunctionPointerType) (Il2CppObject *, void* __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef TypeConvertKey_t1788482786  (*FunctionPointerType) (void* __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TypeConvertKey_t1788482786_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1869378498_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1869378498_gshared (Transform_1_t1610273663 * __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1869378498_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TypeConvertKey_t1788482786_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::EndInvoke(System.IAsyncResult)
extern "C"  TypeConvertKey_t1788482786  Transform_1_EndInvoke_m716690017_gshared (Transform_1_t1610273663 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(TypeConvertKey_t1788482786 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3913237987_gshared (Transform_1_t2870666275 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m3330446311_gshared (Transform_1_t2870666275 * __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3330446311((Transform_1_t2870666275 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TypeConvertKey_t1788482786_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3445908692_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3445908692_gshared (Transform_1_t2870666275 * __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3445908692_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TypeConvertKey_t1788482786_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m435132693_gshared (Transform_1_t2870666275 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m751918486_gshared (Transform_1_t4026109779 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t4204318902  Transform_1_Invoke_m293722518_gshared (Transform_1_t4026109779 * __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m293722518((Transform_1_t4026109779 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4204318902  (*FunctionPointerType) (Il2CppObject *, void* __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t4204318902  (*FunctionPointerType) (void* __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TypeConvertKey_t1788482786_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3550161791_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3550161791_gshared (Transform_1_t4026109779 * __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3550161791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TypeConvertKey_t1788482786_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t4204318902  Transform_1_EndInvoke_m102472620_gshared (Transform_1_t4026109779 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t4204318902 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3264512721_gshared (Transform_1_t2511240172 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m360659445_gshared (Transform_1_t2511240172 * __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m360659445((Transform_1_t2511240172 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TypeConvertKey_t1788482786_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1743736878_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1743736878_gshared (Transform_1_t2511240172 * __this, TypeConvertKey_t1788482786  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1743736878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TypeConvertKey_t1788482786_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1324415403_gshared (Transform_1_t2511240172 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1135904341_gshared (Transform_1_t4134179677 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1188496425_gshared (Transform_1_t4134179677 * __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1188496425((Transform_1_t4134179677 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t3132015601_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2375012652_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2375012652_gshared (Transform_1_t4134179677 * __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2375012652_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(KeyValuePair_2_t3132015601_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m3961677419_gshared (Transform_1_t4134179677 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2301067254_gshared (Transform_1_t1201215579 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t115911300  Transform_1_Invoke_m1524397302_gshared (Transform_1_t1201215579 * __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1524397302((Transform_1_t1201215579 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t115911300  (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t115911300  (*FunctionPointerType) (void* __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t3132015601_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3344055327_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3344055327_gshared (Transform_1_t1201215579 * __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3344055327_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(KeyValuePair_2_t3132015601_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t115911300  Transform_1_EndInvoke_m3187321548_gshared (Transform_1_t1201215579 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t115911300 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m404190732_gshared (Transform_1_t4217319880 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3132015601  Transform_1_Invoke_m3220905796_gshared (Transform_1_t4217319880 * __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3220905796((Transform_1_t4217319880 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3132015601  (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3132015601  (*FunctionPointerType) (void* __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t3132015601_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1181084279_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1181084279_gshared (Transform_1_t4217319880 * __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1181084279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(KeyValuePair_2_t3132015601_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3132015601  Transform_1_EndInvoke_m867477282_gshared (Transform_1_t4217319880 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3132015601 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3493862774_gshared (Transform_1_t3157181727 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m175130038_gshared (Transform_1_t3157181727 * __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m175130038((Transform_1_t3157181727 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t3132015601_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2548307423_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2548307423_gshared (Transform_1_t3157181727 * __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2548307423_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(KeyValuePair_2_t3132015601_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m3959619980_gshared (Transform_1_t3157181727 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1301543437_gshared (Transform_1_t3714796956 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m3240675425_gshared (Transform_1_t3714796956 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3240675425((Transform_1_t3714796956 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2628363682_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2628363682_gshared (Transform_1_t3714796956 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2628363682_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m1726077867_gshared (Transform_1_t3714796956 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3002075318_gshared (Transform_1_t3797937159 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3132015601  Transform_1_Invoke_m388048438_gshared (Transform_1_t3797937159 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m388048438((Transform_1_t3797937159 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3132015601  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3132015601  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3071137759_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3071137759_gshared (Transform_1_t3797937159 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3071137759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3132015601  Transform_1_EndInvoke_m3831512460_gshared (Transform_1_t3797937159 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3132015601 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3512061196_gshared (Transform_1_t2737799006 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m488493188_gshared (Transform_1_t2737799006 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m488493188((Transform_1_t2737799006 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m929665079_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m929665079_gshared (Transform_1_t2737799006 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m929665079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m805300450_gshared (Transform_1_t2737799006 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2152205186_gshared (Transform_1_t4064153 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m4020530914_gshared (Transform_1_t4064153 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4020530914((Transform_1_t4064153 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2179239469_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2179239469_gshared (Transform_1_t4064153 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2179239469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m620026520_gshared (Transform_1_t4064153 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m713310742_gshared (Transform_1_t704776203 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3749587448  Transform_1_Invoke_m1436021910_gshared (Transform_1_t704776203 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1436021910((Transform_1_t704776203 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3749587448  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3749587448  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1786442111_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1786442111_gshared (Transform_1_t704776203 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1786442111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3749587448  Transform_1_EndInvoke_m590952364_gshared (Transform_1_t704776203 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3749587448 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3844763875_gshared (Transform_1_t3322033499 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3763570295_gshared (Transform_1_t3322033499 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3763570295((Transform_1_t3322033499 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m802712344_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m802712344_gshared (Transform_1_t3322033499 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m802712344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m336301945_gshared (Transform_1_t3322033499 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2914458810_gshared (Transform_1_t3939605346 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2347662626_gshared (Transform_1_t3939605346 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2347662626((Transform_1_t3939605346 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1919808363_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1919808363_gshared (Transform_1_t3939605346 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1919808363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1010744720_gshared (Transform_1_t3939605346 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2234205335_gshared (Transform_1_t443866473 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,Newtonsoft.Json.Schema.JsonSchemaType>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1211038787_gshared (Transform_1_t443866473 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1211038787((Transform_1_t443866473 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,Newtonsoft.Json.Schema.JsonSchemaType>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* JsonSchemaType_t1742745177_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m216590498_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m216590498_gshared (Transform_1_t443866473 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m216590498_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(JsonSchemaType_t1742745177_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,Newtonsoft.Json.Schema.JsonSchemaType>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2461796929_gshared (Transform_1_t443866473 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1539627944_gshared (Transform_1_t1749996694 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m3233619656_gshared (Transform_1_t1749996694 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3233619656((Transform_1_t1749996694 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* JsonSchemaType_t1742745177_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1617831605_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1617831605_gshared (Transform_1_t1749996694 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1617831605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(JsonSchemaType_t1742745177_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m838239034_gshared (Transform_1_t1749996694 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1906224182_gshared (Transform_1_t2088239119 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3387117823  Transform_1_Invoke_m3932065974_gshared (Transform_1_t2088239119 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3932065974((Transform_1_t2088239119 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3387117823  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3387117823  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3387117823  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* JsonSchemaType_t1742745177_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1412094047_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1412094047_gshared (Transform_1_t2088239119 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1412094047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(JsonSchemaType_t1742745177_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Newtonsoft.Json.Schema.JsonSchemaType,System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3387117823  Transform_1_EndInvoke_m603179020_gshared (Transform_1_t2088239119 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3387117823 *)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
