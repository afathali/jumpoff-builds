﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidPopUpExamples
struct AndroidPopUpExamples_t3185211868;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AndroidDialogResult1664046954.h"

// System.Void AndroidPopUpExamples::.ctor()
extern "C"  void AndroidPopUpExamples__ctor_m2234186809 (AndroidPopUpExamples_t3185211868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPopUpExamples::RateDialogPopUp()
extern "C"  void AndroidPopUpExamples_RateDialogPopUp_m1878154453 (AndroidPopUpExamples_t3185211868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPopUpExamples::DialogPopUp()
extern "C"  void AndroidPopUpExamples_DialogPopUp_m2556623979 (AndroidPopUpExamples_t3185211868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPopUpExamples::MessagePopUp()
extern "C"  void AndroidPopUpExamples_MessagePopUp_m2613410636 (AndroidPopUpExamples_t3185211868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPopUpExamples::ShowPreloader()
extern "C"  void AndroidPopUpExamples_ShowPreloader_m3913828526 (AndroidPopUpExamples_t3185211868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPopUpExamples::HidePreloader()
extern "C"  void AndroidPopUpExamples_HidePreloader_m2574541599 (AndroidPopUpExamples_t3185211868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPopUpExamples::OpenRatingPage()
extern "C"  void AndroidPopUpExamples_OpenRatingPage_m2790438901 (AndroidPopUpExamples_t3185211868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPopUpExamples::OnRatePopUpClose(AndroidDialogResult)
extern "C"  void AndroidPopUpExamples_OnRatePopUpClose_m2152638798 (AndroidPopUpExamples_t3185211868 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPopUpExamples::OnDialogClose(AndroidDialogResult)
extern "C"  void AndroidPopUpExamples_OnDialogClose_m1968367688 (AndroidPopUpExamples_t3185211868 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPopUpExamples::OnMessageClose(AndroidDialogResult)
extern "C"  void AndroidPopUpExamples_OnMessageClose_m3177412879 (AndroidPopUpExamples_t3185211868 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
