﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_FetchResult
struct GK_FetchResult_t1611512656;
// System.Collections.Generic.List`1<GK_SavedGame>
struct List_1_t2689214752;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GK_FetchResult::.ctor(System.Collections.Generic.List`1<GK_SavedGame>)
extern "C"  void GK_FetchResult__ctor_m974135741 (GK_FetchResult_t1611512656 * __this, List_1_t2689214752 * ___saves0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_FetchResult::.ctor(System.String)
extern "C"  void GK_FetchResult__ctor_m1159995173 (GK_FetchResult_t1611512656 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GK_SavedGame> GK_FetchResult::get_SavedGames()
extern "C"  List_1_t2689214752 * GK_FetchResult_get_SavedGames_m321412187 (GK_FetchResult_t1611512656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
