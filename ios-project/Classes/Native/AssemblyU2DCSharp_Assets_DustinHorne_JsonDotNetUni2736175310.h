﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>
struct List_1_t1357208527;
// System.Collections.Generic.Dictionary`2<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>
struct Dictionary_2_t995913030;

#include "AssemblyU2DCSharp_Assets_DustinHorne_JsonDotNetUni2925764113.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleChild
struct  SampleChild_t2736175310  : public SampleBase_t2925764113
{
public:
	// System.Collections.Generic.List`1<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject> Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleChild::<ObjectList>k__BackingField
	List_1_t1357208527 * ___U3CObjectListU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject> Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleChild::<ObjectDictionary>k__BackingField
	Dictionary_2_t995913030 * ___U3CObjectDictionaryU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CObjectListU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SampleChild_t2736175310, ___U3CObjectListU3Ek__BackingField_3)); }
	inline List_1_t1357208527 * get_U3CObjectListU3Ek__BackingField_3() const { return ___U3CObjectListU3Ek__BackingField_3; }
	inline List_1_t1357208527 ** get_address_of_U3CObjectListU3Ek__BackingField_3() { return &___U3CObjectListU3Ek__BackingField_3; }
	inline void set_U3CObjectListU3Ek__BackingField_3(List_1_t1357208527 * value)
	{
		___U3CObjectListU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CObjectListU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CObjectDictionaryU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SampleChild_t2736175310, ___U3CObjectDictionaryU3Ek__BackingField_4)); }
	inline Dictionary_2_t995913030 * get_U3CObjectDictionaryU3Ek__BackingField_4() const { return ___U3CObjectDictionaryU3Ek__BackingField_4; }
	inline Dictionary_2_t995913030 ** get_address_of_U3CObjectDictionaryU3Ek__BackingField_4() { return &___U3CObjectDictionaryU3Ek__BackingField_4; }
	inline void set_U3CObjectDictionaryU3Ek__BackingField_4(Dictionary_2_t995913030 * value)
	{
		___U3CObjectDictionaryU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CObjectDictionaryU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
