﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayManager
struct GooglePlayManager_t746138120;
// System.Action`1<GP_LeaderboardResult>
struct Action_1_t1836014676;
// System.Action`1<GooglePlayResult>
struct Action_1_t2899269018;
// System.Action`1<GP_AchievementResult>
struct Action_1_t3772725756;
// System.Action`1<GooglePlayGiftRequestResult>
struct Action_1_t152001389;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.Collections.Generic.List`1<GPGameRequest>>
struct Action_1_t3329763456;
// System.Action`1<System.Collections.Generic.List`1<System.String>>
struct Action_1_t1200140747;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.String
struct String_t;
// GPLeaderBoard
struct GPLeaderBoard_t3649577886;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.String[]
struct StringU5BU5D_t1642385972;
// GP_LeaderboardResult
struct GP_LeaderboardResult_t2034215294;
// GPAchievement
struct GPAchievement_t4279788054;
// GooglePlayerTemplate
struct GooglePlayerTemplate_t2506317812;
// GPGameRequest
struct GPGameRequest_t4158842942;
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayerTemplate>
struct Dictionary_2_t126129778;
// System.Collections.Generic.List`1<GPLeaderBoard>
struct List_1_t3018699018;
// System.Collections.Generic.List`1<GPAchievement>
struct List_1_t3648909186;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<GPGameRequest>
struct List_1_t3527964074;
// GP_Participant
struct GP_Participant_t2884377673;
// System.Collections.Generic.List`1<GP_Participant>
struct List_1_t2253498805;
// GooglePlayResult
struct GooglePlayResult_t3097469636;
// GP_AchievementResult
struct GP_AchievementResult_t3970926374;
// GooglePlayGiftRequestResult
struct GooglePlayGiftRequestResult_t350202007;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GPLeaderBoard3649577886.h"
#include "AssemblyU2DCSharp_GPBoardTimeSpan42003024.h"
#include "AssemblyU2DCSharp_GPCollectionType2617299399.h"
#include "AssemblyU2DCSharp_GPGameRequestType1088795774.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharp_GP_LeaderboardResult2034215294.h"
#include "AssemblyU2DCSharp_GooglePlayerTemplate2506317812.h"
#include "AssemblyU2DCSharp_GooglePlayResult3097469636.h"
#include "AssemblyU2DCSharp_GP_AchievementResult3970926374.h"
#include "AssemblyU2DCSharp_GooglePlayGiftRequestResult350202007.h"

// System.Void GooglePlayManager::.ctor()
extern "C"  void GooglePlayManager__ctor_m2656753493 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::.cctor()
extern "C"  void GooglePlayManager__cctor_m4278642010 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::add_ActionScoreSubmited(System.Action`1<GP_LeaderboardResult>)
extern "C"  void GooglePlayManager_add_ActionScoreSubmited_m2291049420 (Il2CppObject * __this /* static, unused */, Action_1_t1836014676 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::remove_ActionScoreSubmited(System.Action`1<GP_LeaderboardResult>)
extern "C"  void GooglePlayManager_remove_ActionScoreSubmited_m2263082213 (Il2CppObject * __this /* static, unused */, Action_1_t1836014676 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::add_ActionScoresListLoaded(System.Action`1<GP_LeaderboardResult>)
extern "C"  void GooglePlayManager_add_ActionScoresListLoaded_m1449533753 (Il2CppObject * __this /* static, unused */, Action_1_t1836014676 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::remove_ActionScoresListLoaded(System.Action`1<GP_LeaderboardResult>)
extern "C"  void GooglePlayManager_remove_ActionScoresListLoaded_m1501696080 (Il2CppObject * __this /* static, unused */, Action_1_t1836014676 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::add_ActionLeaderboardsLoaded(System.Action`1<GooglePlayResult>)
extern "C"  void GooglePlayManager_add_ActionLeaderboardsLoaded_m186818576 (Il2CppObject * __this /* static, unused */, Action_1_t2899269018 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::remove_ActionLeaderboardsLoaded(System.Action`1<GooglePlayResult>)
extern "C"  void GooglePlayManager_remove_ActionLeaderboardsLoaded_m1365619613 (Il2CppObject * __this /* static, unused */, Action_1_t2899269018 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::add_ActionAchievementUpdated(System.Action`1<GP_AchievementResult>)
extern "C"  void GooglePlayManager_add_ActionAchievementUpdated_m1726277375 (Il2CppObject * __this /* static, unused */, Action_1_t3772725756 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::remove_ActionAchievementUpdated(System.Action`1<GP_AchievementResult>)
extern "C"  void GooglePlayManager_remove_ActionAchievementUpdated_m2840620724 (Il2CppObject * __this /* static, unused */, Action_1_t3772725756 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::add_ActionFriendsListLoaded(System.Action`1<GooglePlayResult>)
extern "C"  void GooglePlayManager_add_ActionFriendsListLoaded_m4102779847 (Il2CppObject * __this /* static, unused */, Action_1_t2899269018 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::remove_ActionFriendsListLoaded(System.Action`1<GooglePlayResult>)
extern "C"  void GooglePlayManager_remove_ActionFriendsListLoaded_m1513058722 (Il2CppObject * __this /* static, unused */, Action_1_t2899269018 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::add_ActionAchievementsLoaded(System.Action`1<GooglePlayResult>)
extern "C"  void GooglePlayManager_add_ActionAchievementsLoaded_m3430582956 (Il2CppObject * __this /* static, unused */, Action_1_t2899269018 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::remove_ActionAchievementsLoaded(System.Action`1<GooglePlayResult>)
extern "C"  void GooglePlayManager_remove_ActionAchievementsLoaded_m506642343 (Il2CppObject * __this /* static, unused */, Action_1_t2899269018 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::add_ActionSendGiftResultReceived(System.Action`1<GooglePlayGiftRequestResult>)
extern "C"  void GooglePlayManager_add_ActionSendGiftResultReceived_m695700 (Il2CppObject * __this /* static, unused */, Action_1_t152001389 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::remove_ActionSendGiftResultReceived(System.Action`1<GooglePlayGiftRequestResult>)
extern "C"  void GooglePlayManager_remove_ActionSendGiftResultReceived_m1261449831 (Il2CppObject * __this /* static, unused */, Action_1_t152001389 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::add_ActionRequestsInboxDialogDismissed(System.Action)
extern "C"  void GooglePlayManager_add_ActionRequestsInboxDialogDismissed_m2024108175 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::remove_ActionRequestsInboxDialogDismissed(System.Action)
extern "C"  void GooglePlayManager_remove_ActionRequestsInboxDialogDismissed_m3319467454 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::add_ActionPendingGameRequestsDetected(System.Action`1<System.Collections.Generic.List`1<GPGameRequest>>)
extern "C"  void GooglePlayManager_add_ActionPendingGameRequestsDetected_m712183406 (Il2CppObject * __this /* static, unused */, Action_1_t3329763456 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::remove_ActionPendingGameRequestsDetected(System.Action`1<System.Collections.Generic.List`1<GPGameRequest>>)
extern "C"  void GooglePlayManager_remove_ActionPendingGameRequestsDetected_m3670169775 (Il2CppObject * __this /* static, unused */, Action_1_t3329763456 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::add_ActionGameRequestsAccepted(System.Action`1<System.Collections.Generic.List`1<GPGameRequest>>)
extern "C"  void GooglePlayManager_add_ActionGameRequestsAccepted_m2400400802 (Il2CppObject * __this /* static, unused */, Action_1_t3329763456 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::remove_ActionGameRequestsAccepted(System.Action`1<System.Collections.Generic.List`1<GPGameRequest>>)
extern "C"  void GooglePlayManager_remove_ActionGameRequestsAccepted_m1065923167 (Il2CppObject * __this /* static, unused */, Action_1_t3329763456 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::add_ActionAvailableDeviceAccountsLoaded(System.Action`1<System.Collections.Generic.List`1<System.String>>)
extern "C"  void GooglePlayManager_add_ActionAvailableDeviceAccountsLoaded_m96332539 (Il2CppObject * __this /* static, unused */, Action_1_t1200140747 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::remove_ActionAvailableDeviceAccountsLoaded(System.Action`1<System.Collections.Generic.List`1<System.String>>)
extern "C"  void GooglePlayManager_remove_ActionAvailableDeviceAccountsLoaded_m1771833854 (Il2CppObject * __this /* static, unused */, Action_1_t1200140747 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::add_ActionOAuthTokenLoaded(System.Action`1<System.String>)
extern "C"  void GooglePlayManager_add_ActionOAuthTokenLoaded_m3999729038 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::remove_ActionOAuthTokenLoaded(System.Action`1<System.String>)
extern "C"  void GooglePlayManager_remove_ActionOAuthTokenLoaded_m2330853833 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::Awake()
extern "C"  void GooglePlayManager_Awake_m2665671540 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::Create()
extern "C"  void GooglePlayManager_Create_m3741148861 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::RetrieveDeviceGoogleAccounts()
extern "C"  void GooglePlayManager_RetrieveDeviceGoogleAccounts_m2602956738 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::LoadToken(System.String,System.String)
extern "C"  void GooglePlayManager_LoadToken_m4176419980 (GooglePlayManager_t746138120 * __this, String_t* ___accountName0, String_t* ___scopes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::LoadToken()
extern "C"  void GooglePlayManager_LoadToken_m2651770918 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::InvalidateToken(System.String)
extern "C"  void GooglePlayManager_InvalidateToken_m2230615585 (GooglePlayManager_t746138120 * __this, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::showAchievementsUI()
extern "C"  void GooglePlayManager_showAchievementsUI_m2628188988 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::ShowAchievementsUI()
extern "C"  void GooglePlayManager_ShowAchievementsUI_m2962249628 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::showLeaderBoardsUI()
extern "C"  void GooglePlayManager_showLeaderBoardsUI_m3353439372 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::ShowLeaderBoardsUI()
extern "C"  void GooglePlayManager_ShowLeaderBoardsUI_m2028445612 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::showLeaderBoard(System.String)
extern "C"  void GooglePlayManager_showLeaderBoard_m3275320293 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::ShowLeaderBoard(System.String)
extern "C"  void GooglePlayManager_ShowLeaderBoard_m136268869 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::showLeaderBoardById(System.String)
extern "C"  void GooglePlayManager_showLeaderBoardById_m2387916307 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::ShowLeaderBoardById(System.String)
extern "C"  void GooglePlayManager_ShowLeaderBoardById_m2601471731 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::submitScore(System.String,System.Int64)
extern "C"  void GooglePlayManager_submitScore_m2792385069 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardName0, int64_t ___score1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::SubmitScore(System.String,System.Int64)
extern "C"  void GooglePlayManager_SubmitScore_m1089677517 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardName0, int64_t ___score1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::submitScoreById(System.String,System.Int64)
extern "C"  void GooglePlayManager_submitScoreById_m2101242219 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardId0, int64_t ___score1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::SubmitScoreById(System.String,System.Int64)
extern "C"  void GooglePlayManager_SubmitScoreById_m1791766155 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardId0, int64_t ___score1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::loadLeaderBoards()
extern "C"  void GooglePlayManager_loadLeaderBoards_m3491765165 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::LoadLeaderBoards()
extern "C"  void GooglePlayManager_LoadLeaderBoards_m2550410061 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::UpdatePlayerScoreLocal(GPLeaderBoard)
extern "C"  void GooglePlayManager_UpdatePlayerScoreLocal_m2204417122 (GooglePlayManager_t746138120 * __this, GPLeaderBoard_t3649577886 * ___leaderboard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::loadPlayerCenteredScores(System.String,GPBoardTimeSpan,GPCollectionType,System.Int32)
extern "C"  void GooglePlayManager_loadPlayerCenteredScores_m217678735 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardId0, int32_t ___span1, int32_t ___collection2, int32_t ___maxResults3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::LoadPlayerCenteredScores(System.String,GPBoardTimeSpan,GPCollectionType,System.Int32)
extern "C"  void GooglePlayManager_LoadPlayerCenteredScores_m612459631 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardId0, int32_t ___span1, int32_t ___collection2, int32_t ___maxResults3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::loadTopScores(System.String,GPBoardTimeSpan,GPCollectionType,System.Int32)
extern "C"  void GooglePlayManager_loadTopScores_m1461307169 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardId0, int32_t ___span1, int32_t ___collection2, int32_t ___maxResults3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::LoadTopScores(System.String,GPBoardTimeSpan,GPCollectionType,System.Int32)
extern "C"  void GooglePlayManager_LoadTopScores_m1351431617 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardId0, int32_t ___span1, int32_t ___collection2, int32_t ___maxResults3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::UnlockAchievement(System.String)
extern "C"  void GooglePlayManager_UnlockAchievement_m182374390 (GooglePlayManager_t746138120 * __this, String_t* ___achievementName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::UnlockAchievementById(System.String)
extern "C"  void GooglePlayManager_UnlockAchievementById_m1440451834 (GooglePlayManager_t746138120 * __this, String_t* ___achievementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::reportAchievement(System.String)
extern "C"  void GooglePlayManager_reportAchievement_m690711144 (GooglePlayManager_t746138120 * __this, String_t* ___achievementName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::reportAchievementById(System.String)
extern "C"  void GooglePlayManager_reportAchievementById_m1479653532 (GooglePlayManager_t746138120 * __this, String_t* ___achievementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::revealAchievement(System.String)
extern "C"  void GooglePlayManager_revealAchievement_m1247281581 (GooglePlayManager_t746138120 * __this, String_t* ___achievementName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::RevealAchievement(System.String)
extern "C"  void GooglePlayManager_RevealAchievement_m3817457677 (GooglePlayManager_t746138120 * __this, String_t* ___achievementName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::revealAchievementById(System.String)
extern "C"  void GooglePlayManager_revealAchievementById_m707697975 (GooglePlayManager_t746138120 * __this, String_t* ___achievementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::RevealAchievementById(System.String)
extern "C"  void GooglePlayManager_RevealAchievementById_m103537687 (GooglePlayManager_t746138120 * __this, String_t* ___achievementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::incrementAchievement(System.String,System.Int32)
extern "C"  void GooglePlayManager_incrementAchievement_m1285559766 (GooglePlayManager_t746138120 * __this, String_t* ___achievementName0, int32_t ___numsteps1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::IncrementAchievement(System.String,System.Int32)
extern "C"  void GooglePlayManager_IncrementAchievement_m128922550 (GooglePlayManager_t746138120 * __this, String_t* ___achievementName0, int32_t ___numsteps1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::incrementAchievementById(System.String,System.Int32)
extern "C"  void GooglePlayManager_incrementAchievementById_m1259049118 (GooglePlayManager_t746138120 * __this, String_t* ___achievementId0, int32_t ___numsteps1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::IncrementAchievementById(System.String,System.Int32)
extern "C"  void GooglePlayManager_IncrementAchievementById_m1758619902 (GooglePlayManager_t746138120 * __this, String_t* ___achievementId0, int32_t ___numsteps1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::SetStepsImmediate(System.String,System.Int32)
extern "C"  void GooglePlayManager_SetStepsImmediate_m1235983690 (GooglePlayManager_t746138120 * __this, String_t* ___achievementId0, int32_t ___numsteps1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::loadAchievements()
extern "C"  void GooglePlayManager_loadAchievements_m1627654675 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::LoadAchievements()
extern "C"  void GooglePlayManager_LoadAchievements_m2301887987 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::resetAchievement(System.String)
extern "C"  void GooglePlayManager_resetAchievement_m2661135345 (GooglePlayManager_t746138120 * __this, String_t* ___achievementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::ResetAchievement(System.String)
extern "C"  void GooglePlayManager_ResetAchievement_m725441041 (GooglePlayManager_t746138120 * __this, String_t* ___achievementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::ResetAllAchievements()
extern "C"  void GooglePlayManager_ResetAllAchievements_m1848555025 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::resetLeaderBoard(System.String)
extern "C"  void GooglePlayManager_resetLeaderBoard_m1535940699 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::ResetLeaderBoard(System.String)
extern "C"  void GooglePlayManager_ResetLeaderBoard_m3222665979 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::loadConnectedPlayers()
extern "C"  void GooglePlayManager_loadConnectedPlayers_m353500560 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::LoadConnectedPlayers()
extern "C"  void GooglePlayManager_LoadConnectedPlayers_m1915778544 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::LoadFriends()
extern "C"  void GooglePlayManager_LoadFriends_m2634476930 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::SendGiftRequest(GPGameRequestType,System.Int32,UnityEngine.Texture2D,System.String,System.String)
extern "C"  void GooglePlayManager_SendGiftRequest_m2825341503 (GooglePlayManager_t746138120 * __this, int32_t ___type0, int32_t ___requestLifetimeDays1, Texture2D_t3542995729 * ___icon2, String_t* ___description3, String_t* ___playload4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayManager::get_currentAccount()
extern "C"  String_t* GooglePlayManager_get_currentAccount_m1798389969 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::ShowRequestsAccepDialog()
extern "C"  void GooglePlayManager_ShowRequestsAccepDialog_m3307046834 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::AcceptRequests(System.String[])
extern "C"  void GooglePlayManager_AcceptRequests_m1674026219 (GooglePlayManager_t746138120 * __this, StringU5BU5D_t1642385972* ___ids0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::DismissRequest(System.String[])
extern "C"  void GooglePlayManager_DismissRequest_m2033041836 (GooglePlayManager_t746138120 * __this, StringU5BU5D_t1642385972* ___ids0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::DispatchLeaderboardUpdateEvent(GP_LeaderboardResult)
extern "C"  void GooglePlayManager_DispatchLeaderboardUpdateEvent_m1548639285 (GooglePlayManager_t746138120 * __this, GP_LeaderboardResult_t2034215294 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPLeaderBoard GooglePlayManager::GetLeaderBoard(System.String)
extern "C"  GPLeaderBoard_t3649577886 * GooglePlayManager_GetLeaderBoard_m3891800057 (GooglePlayManager_t746138120 * __this, String_t* ___leaderboardId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPAchievement GooglePlayManager::GetAchievement(System.String)
extern "C"  GPAchievement_t4279788054 * GooglePlayManager_GetAchievement_m667265913 (GooglePlayManager_t746138120 * __this, String_t* ___achievementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayerTemplate GooglePlayManager::GetPlayerById(System.String)
extern "C"  GooglePlayerTemplate_t2506317812 * GooglePlayManager_GetPlayerById_m356107553 (GooglePlayManager_t746138120 * __this, String_t* ___playerId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPGameRequest GooglePlayManager::GetGameRequestById(System.String)
extern "C"  GPGameRequest_t4158842942 * GooglePlayManager_GetGameRequestById_m261913767 (GooglePlayManager_t746138120 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayerTemplate GooglePlayManager::get_player()
extern "C"  GooglePlayerTemplate_t2506317812 * GooglePlayManager_get_player_m3386316490 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayerTemplate> GooglePlayManager::get_players()
extern "C"  Dictionary_2_t126129778 * GooglePlayManager_get_players_m3371430816 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GPLeaderBoard> GooglePlayManager::get_leaderBoards()
extern "C"  List_1_t3018699018 * GooglePlayManager_get_leaderBoards_m2539373 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GPLeaderBoard> GooglePlayManager::get_LeaderBoards()
extern "C"  List_1_t3018699018 * GooglePlayManager_get_LeaderBoards_m4058670989 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GPAchievement> GooglePlayManager::get_achievements()
extern "C"  List_1_t3648909186 * GooglePlayManager_get_achievements_m77225709 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GPAchievement> GooglePlayManager::get_Achievements()
extern "C"  List_1_t3648909186 * GooglePlayManager_get_Achievements_m318217613 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> GooglePlayManager::get_friendsList()
extern "C"  List_1_t1398341365 * GooglePlayManager_get_friendsList_m1236934122 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GPGameRequest> GooglePlayManager::get_gameRequests()
extern "C"  List_1_t3527964074 * GooglePlayManager_get_gameRequests_m465249197 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> GooglePlayManager::get_deviceGoogleAccountList()
extern "C"  List_1_t1398341365 * GooglePlayManager_get_deviceGoogleAccountList_m1650163565 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayManager::get_loadedAuthToken()
extern "C"  String_t* GooglePlayManager_get_loadedAuthToken_m753259277 (GooglePlayManager_t746138120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayManager::get_IsLeaderboardsDataLoaded()
extern "C"  bool GooglePlayManager_get_IsLeaderboardsDataLoaded_m3670887327 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnGiftSendResult(System.String)
extern "C"  void GooglePlayManager_OnGiftSendResult_m1771576387 (GooglePlayManager_t746138120 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnRequestsInboxDialogDismissed(System.String)
extern "C"  void GooglePlayManager_OnRequestsInboxDialogDismissed_m3558703833 (GooglePlayManager_t746138120 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnAchievementsLoaded(System.String)
extern "C"  void GooglePlayManager_OnAchievementsLoaded_m4257695501 (GooglePlayManager_t746138120 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnAchievementUpdated(System.String)
extern "C"  void GooglePlayManager_OnAchievementUpdated_m3848955270 (GooglePlayManager_t746138120 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnScoreDataRecevied(System.String)
extern "C"  void GooglePlayManager_OnScoreDataRecevied_m3710864225 (GooglePlayManager_t746138120 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnLeaderboardDataLoaded(System.String)
extern "C"  void GooglePlayManager_OnLeaderboardDataLoaded_m3223780272 (GooglePlayManager_t746138120 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnPlayerScoreUpdated(System.String)
extern "C"  void GooglePlayManager_OnPlayerScoreUpdated_m731964252 (GooglePlayManager_t746138120 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnScoreSubmitted(System.String)
extern "C"  void GooglePlayManager_OnScoreSubmitted_m2179858919 (GooglePlayManager_t746138120 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnPlayerDataLoaded(System.String)
extern "C"  void GooglePlayManager_OnPlayerDataLoaded_m3813847772 (GooglePlayManager_t746138120 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnPlayersLoaded(System.String)
extern "C"  void GooglePlayManager_OnPlayersLoaded_m750013105 (GooglePlayManager_t746138120 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnGameRequestsLoaded(System.String)
extern "C"  void GooglePlayManager_OnGameRequestsLoaded_m294439301 (GooglePlayManager_t746138120 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnGameRequestsAccepted(System.String)
extern "C"  void GooglePlayManager_OnGameRequestsAccepted_m2139206903 (GooglePlayManager_t746138120 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnAccountsLoaded(System.String)
extern "C"  void GooglePlayManager_OnAccountsLoaded_m1496309151 (GooglePlayManager_t746138120 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::OnTokenLoaded(System.String)
extern "C"  void GooglePlayManager_OnTokenLoaded_m3739805838 (GooglePlayManager_t746138120 * __this, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_Participant GooglePlayManager::ParseParticipanData(System.String[],System.Int32)
extern "C"  GP_Participant_t2884377673 * GooglePlayManager_ParseParticipanData_m4068292822 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___data0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GP_Participant> GooglePlayManager::ParseParticipantsData(System.String[],System.Int32)
extern "C"  List_1_t2253498805 * GooglePlayManager_ParseParticipantsData_m2330436455 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___data0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::AddPlayer(GooglePlayerTemplate)
extern "C"  void GooglePlayManager_AddPlayer_m2131155281 (GooglePlayManager_t746138120 * __this, GooglePlayerTemplate_t2506317812 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::<ActionScoreSubmited>m__41(GP_LeaderboardResult)
extern "C"  void GooglePlayManager_U3CActionScoreSubmitedU3Em__41_m1606367344 (Il2CppObject * __this /* static, unused */, GP_LeaderboardResult_t2034215294 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::<ActionScoresListLoaded>m__42(GP_LeaderboardResult)
extern "C"  void GooglePlayManager_U3CActionScoresListLoadedU3Em__42_m1067091488 (Il2CppObject * __this /* static, unused */, GP_LeaderboardResult_t2034215294 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::<ActionLeaderboardsLoaded>m__43(GooglePlayResult)
extern "C"  void GooglePlayManager_U3CActionLeaderboardsLoadedU3Em__43_m2118757242 (Il2CppObject * __this /* static, unused */, GooglePlayResult_t3097469636 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::<ActionAchievementUpdated>m__44(GP_AchievementResult)
extern "C"  void GooglePlayManager_U3CActionAchievementUpdatedU3Em__44_m3727092290 (Il2CppObject * __this /* static, unused */, GP_AchievementResult_t3970926374 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::<ActionFriendsListLoaded>m__45(GooglePlayResult)
extern "C"  void GooglePlayManager_U3CActionFriendsListLoadedU3Em__45_m2113993073 (Il2CppObject * __this /* static, unused */, GooglePlayResult_t3097469636 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::<ActionAchievementsLoaded>m__46(GooglePlayResult)
extern "C"  void GooglePlayManager_U3CActionAchievementsLoadedU3Em__46_m3645354661 (Il2CppObject * __this /* static, unused */, GooglePlayResult_t3097469636 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::<ActionSendGiftResultReceived>m__47(GooglePlayGiftRequestResult)
extern "C"  void GooglePlayManager_U3CActionSendGiftResultReceivedU3Em__47_m273743974 (Il2CppObject * __this /* static, unused */, GooglePlayGiftRequestResult_t350202007 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::<ActionRequestsInboxDialogDismissed>m__48()
extern "C"  void GooglePlayManager_U3CActionRequestsInboxDialogDismissedU3Em__48_m1804654831 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::<ActionPendingGameRequestsDetected>m__49(System.Collections.Generic.List`1<GPGameRequest>)
extern "C"  void GooglePlayManager_U3CActionPendingGameRequestsDetectedU3Em__49_m1838286546 (Il2CppObject * __this /* static, unused */, List_1_t3527964074 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::<ActionGameRequestsAccepted>m__4A(System.Collections.Generic.List`1<GPGameRequest>)
extern "C"  void GooglePlayManager_U3CActionGameRequestsAcceptedU3Em__4A_m3546191514 (Il2CppObject * __this /* static, unused */, List_1_t3527964074 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::<ActionAvailableDeviceAccountsLoaded>m__4B(System.Collections.Generic.List`1<System.String>)
extern "C"  void GooglePlayManager_U3CActionAvailableDeviceAccountsLoadedU3Em__4B_m2976086628 (Il2CppObject * __this /* static, unused */, List_1_t1398341365 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayManager::<ActionOAuthTokenLoaded>m__4C(System.String)
extern "C"  void GooglePlayManager_U3CActionOAuthTokenLoadedU3Em__4C_m2815697148 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
