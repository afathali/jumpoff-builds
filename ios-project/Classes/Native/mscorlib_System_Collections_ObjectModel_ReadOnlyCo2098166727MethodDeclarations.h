﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2875234987MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m2913503017(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2098166727 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3671019970_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m893176119(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2098166727 *, VariableListed_t1912381035 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2989589458_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4187709227(__this, method) ((  void (*) (ReadOnlyCollection_1_t2098166727 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m454937302_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m236103892(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2098166727 *, int32_t, VariableListed_t1912381035 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4272763307_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3584219546(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2098166727 *, VariableListed_t1912381035 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3199809075_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m788341040(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2098166727 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m962041751_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1808544046(__this, ___index0, method) ((  VariableListed_t1912381035 * (*) (ReadOnlyCollection_1_t2098166727 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m70085287_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3789509787(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2098166727 *, int32_t, VariableListed_t1912381035 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1547026160_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m101970695(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2098166727 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4041967064_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2649810298(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2098166727 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3664791405_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m217708251(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2098166727 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m531171980_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m772610070(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2098166727 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3780136817_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m4145386924(__this, method) ((  void (*) (ReadOnlyCollection_1_t2098166727 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3983677501_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m4016819096(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2098166727 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1990607517_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2140378288(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2098166727 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m606942423_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1141544011(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2098166727 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m691705570_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1128772131(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2098166727 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3182494192_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2541070809(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2098166727 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m572840272_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1087570374(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2098166727 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2871048729_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3943256298(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2098166727 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m769863805_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m32582907(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2098166727 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m942145650_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m703150394(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2098166727 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1367736517_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3548536083(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2098166727 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3336878134_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m136926820(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2098166727 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1799572719_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::Contains(T)
#define ReadOnlyCollection_1_Contains_m1077336069(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2098166727 *, VariableListed_t1912381035 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m1227826160_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m3354117675(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2098166727 *, VariableListedU5BU5D_t3352902538*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m4257276542_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m4179856842(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2098166727 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1627519329_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m2615950871(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2098166727 *, VariableListed_t1912381035 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1981423404_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::get_Count()
#define ReadOnlyCollection_1_get_Count_m2952649990(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2098166727 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2562379905_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SA.IOSDeploy.VariableListed>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m3323464434(__this, ___index0, method) ((  VariableListed_t1912381035 * (*) (ReadOnlyCollection_1_t2098166727 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m191392387_gshared)(__this, ___index0, method)
