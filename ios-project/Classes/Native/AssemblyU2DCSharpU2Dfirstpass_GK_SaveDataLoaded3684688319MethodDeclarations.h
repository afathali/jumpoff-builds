﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_SaveDataLoaded
struct GK_SaveDataLoaded_t3684688319;
// GK_SavedGame
struct GK_SavedGame_t3320093620;
// SA.Common.Models.Error
struct Error_t445207774;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SavedGame3320093620.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"

// System.Void GK_SaveDataLoaded::.ctor(GK_SavedGame)
extern "C"  void GK_SaveDataLoaded__ctor_m2718711436 (GK_SaveDataLoaded_t3684688319 * __this, GK_SavedGame_t3320093620 * ___save0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_SaveDataLoaded::.ctor(SA.Common.Models.Error)
extern "C"  void GK_SaveDataLoaded__ctor_m2387842333 (GK_SaveDataLoaded_t3684688319 * __this, Error_t445207774 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_SavedGame GK_SaveDataLoaded::get_SavedGame()
extern "C"  GK_SavedGame_t3320093620 * GK_SaveDataLoaded_get_SavedGame_m4066559317 (GK_SaveDataLoaded_t3684688319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
