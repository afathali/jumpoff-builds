﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke966099451MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2177448334(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1819645885 *, Dictionary_2_t3631115410 *, const MethodInfo*))KeyCollection__ctor_m3523539860_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4222607904(__this, ___item0, method) ((  void (*) (KeyCollection_t1819645885 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3322330018_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m79939703(__this, method) ((  void (*) (KeyCollection_t1819645885 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m903715163_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1332603608(__this, ___item0, method) ((  bool (*) (KeyCollection_t1819645885 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m161914186_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4171595471(__this, ___item0, method) ((  bool (*) (KeyCollection_t1819645885 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1659795907_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4043248405(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1819645885 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m421845341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m4258342617(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1819645885 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m355819049_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m34325200(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1819645885 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3039926290_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4121210083(__this, method) ((  bool (*) (KeyCollection_t1819645885 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4188921679_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3478232621(__this, method) ((  bool (*) (KeyCollection_t1819645885 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3755359973_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m697717753(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1819645885 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2694683641_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3043185567(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1819645885 *, FB_ProfileImageSizeU5BU5D_t4110693687*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m285260995_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3000032922(__this, method) ((  Enumerator_t2025651552  (*) (KeyCollection_t1819645885 *, const MethodInfo*))KeyCollection_GetEnumerator_m1955007816_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,UnityEngine.Texture2D>::get_Count()
#define KeyCollection_get_Count_m169575605(__this, method) ((  int32_t (*) (KeyCollection_t1819645885 *, const MethodInfo*))KeyCollection_get_Count_m1680202429_gshared)(__this, method)
