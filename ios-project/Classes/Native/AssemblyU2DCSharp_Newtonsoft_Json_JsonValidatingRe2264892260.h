﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.JsonReader
struct JsonReader_t3154730733;
// System.Collections.Generic.Stack`1<Newtonsoft.Json.JsonValidatingReader/SchemaScope>
struct Stack_1_t1011649401;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t3772113849;
// Newtonsoft.Json.Schema.JsonSchemaModel
struct JsonSchemaModel_t708894576;
// Newtonsoft.Json.JsonValidatingReader/SchemaScope
struct SchemaScope_t4218888543;
// Newtonsoft.Json.Schema.ValidationEventHandler
struct ValidationEventHandler_t1731902491;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>,System.Boolean>
struct Func_2_t3633335109;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>,System.String>
struct Func_2_t1836980624;

#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader3154730733.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonValidatingReader
struct  JsonValidatingReader_t2264892260  : public JsonReader_t3154730733
{
public:
	// Newtonsoft.Json.JsonReader Newtonsoft.Json.JsonValidatingReader::_reader
	JsonReader_t3154730733 * ____reader_9;
	// System.Collections.Generic.Stack`1<Newtonsoft.Json.JsonValidatingReader/SchemaScope> Newtonsoft.Json.JsonValidatingReader::_stack
	Stack_1_t1011649401 * ____stack_10;
	// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.JsonValidatingReader::_schema
	JsonSchema_t3772113849 * ____schema_11;
	// Newtonsoft.Json.Schema.JsonSchemaModel Newtonsoft.Json.JsonValidatingReader::_model
	JsonSchemaModel_t708894576 * ____model_12;
	// Newtonsoft.Json.JsonValidatingReader/SchemaScope Newtonsoft.Json.JsonValidatingReader::_currentScope
	SchemaScope_t4218888543 * ____currentScope_13;
	// Newtonsoft.Json.Schema.ValidationEventHandler Newtonsoft.Json.JsonValidatingReader::ValidationEventHandler
	ValidationEventHandler_t1731902491 * ___ValidationEventHandler_14;

public:
	inline static int32_t get_offset_of__reader_9() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t2264892260, ____reader_9)); }
	inline JsonReader_t3154730733 * get__reader_9() const { return ____reader_9; }
	inline JsonReader_t3154730733 ** get_address_of__reader_9() { return &____reader_9; }
	inline void set__reader_9(JsonReader_t3154730733 * value)
	{
		____reader_9 = value;
		Il2CppCodeGenWriteBarrier(&____reader_9, value);
	}

	inline static int32_t get_offset_of__stack_10() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t2264892260, ____stack_10)); }
	inline Stack_1_t1011649401 * get__stack_10() const { return ____stack_10; }
	inline Stack_1_t1011649401 ** get_address_of__stack_10() { return &____stack_10; }
	inline void set__stack_10(Stack_1_t1011649401 * value)
	{
		____stack_10 = value;
		Il2CppCodeGenWriteBarrier(&____stack_10, value);
	}

	inline static int32_t get_offset_of__schema_11() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t2264892260, ____schema_11)); }
	inline JsonSchema_t3772113849 * get__schema_11() const { return ____schema_11; }
	inline JsonSchema_t3772113849 ** get_address_of__schema_11() { return &____schema_11; }
	inline void set__schema_11(JsonSchema_t3772113849 * value)
	{
		____schema_11 = value;
		Il2CppCodeGenWriteBarrier(&____schema_11, value);
	}

	inline static int32_t get_offset_of__model_12() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t2264892260, ____model_12)); }
	inline JsonSchemaModel_t708894576 * get__model_12() const { return ____model_12; }
	inline JsonSchemaModel_t708894576 ** get_address_of__model_12() { return &____model_12; }
	inline void set__model_12(JsonSchemaModel_t708894576 * value)
	{
		____model_12 = value;
		Il2CppCodeGenWriteBarrier(&____model_12, value);
	}

	inline static int32_t get_offset_of__currentScope_13() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t2264892260, ____currentScope_13)); }
	inline SchemaScope_t4218888543 * get__currentScope_13() const { return ____currentScope_13; }
	inline SchemaScope_t4218888543 ** get_address_of__currentScope_13() { return &____currentScope_13; }
	inline void set__currentScope_13(SchemaScope_t4218888543 * value)
	{
		____currentScope_13 = value;
		Il2CppCodeGenWriteBarrier(&____currentScope_13, value);
	}

	inline static int32_t get_offset_of_ValidationEventHandler_14() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t2264892260, ___ValidationEventHandler_14)); }
	inline ValidationEventHandler_t1731902491 * get_ValidationEventHandler_14() const { return ___ValidationEventHandler_14; }
	inline ValidationEventHandler_t1731902491 ** get_address_of_ValidationEventHandler_14() { return &___ValidationEventHandler_14; }
	inline void set_ValidationEventHandler_14(ValidationEventHandler_t1731902491 * value)
	{
		___ValidationEventHandler_14 = value;
		Il2CppCodeGenWriteBarrier(&___ValidationEventHandler_14, value);
	}
};

struct JsonValidatingReader_t2264892260_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>,System.Boolean> Newtonsoft.Json.JsonValidatingReader::<>f__am$cache6
	Func_2_t3633335109 * ___U3CU3Ef__amU24cache6_15;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>,System.String> Newtonsoft.Json.JsonValidatingReader::<>f__am$cache7
	Func_2_t1836980624 * ___U3CU3Ef__amU24cache7_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_15() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t2264892260_StaticFields, ___U3CU3Ef__amU24cache6_15)); }
	inline Func_2_t3633335109 * get_U3CU3Ef__amU24cache6_15() const { return ___U3CU3Ef__amU24cache6_15; }
	inline Func_2_t3633335109 ** get_address_of_U3CU3Ef__amU24cache6_15() { return &___U3CU3Ef__amU24cache6_15; }
	inline void set_U3CU3Ef__amU24cache6_15(Func_2_t3633335109 * value)
	{
		___U3CU3Ef__amU24cache6_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_16() { return static_cast<int32_t>(offsetof(JsonValidatingReader_t2264892260_StaticFields, ___U3CU3Ef__amU24cache7_16)); }
	inline Func_2_t1836980624 * get_U3CU3Ef__amU24cache7_16() const { return ___U3CU3Ef__amU24cache7_16; }
	inline Func_2_t1836980624 ** get_address_of_U3CU3Ef__amU24cache7_16() { return &___U3CU3Ef__amU24cache7_16; }
	inline void set_U3CU3Ef__amU24cache7_16(Func_2_t1836980624 * value)
	{
		___U3CU3Ef__amU24cache7_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
