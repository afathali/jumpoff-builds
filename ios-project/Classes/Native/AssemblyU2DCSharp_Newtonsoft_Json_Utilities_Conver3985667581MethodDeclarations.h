﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ConvertUtils/<CreateCastConverter>c__AnonStorey29
struct U3CCreateCastConverterU3Ec__AnonStorey29_t3985667581;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Newtonsoft.Json.Utilities.ConvertUtils/<CreateCastConverter>c__AnonStorey29::.ctor()
extern "C"  void U3CCreateCastConverterU3Ec__AnonStorey29__ctor_m629016674 (U3CCreateCastConverterU3Ec__AnonStorey29_t3985667581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils/<CreateCastConverter>c__AnonStorey29::<>m__EB(System.Object)
extern "C"  Il2CppObject * U3CCreateCastConverterU3Ec__AnonStorey29_U3CU3Em__EB_m2819345443 (U3CCreateCastConverterU3Ec__AnonStorey29_t3985667581 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
