﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidImagePickResult
struct AndroidImagePickResult_t1791733552;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AndroidImagePickResult::.ctor(System.String,System.String,System.String)
extern "C"  void AndroidImagePickResult__ctor_m2722885745 (AndroidImagePickResult_t1791733552 * __this, String_t* ___codeString0, String_t* ___ImageData1, String_t* ___ImagePathInfo2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D AndroidImagePickResult::get_image()
extern "C"  Texture2D_t3542995729 * AndroidImagePickResult_get_image_m3452723510 (AndroidImagePickResult_t1791733552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D AndroidImagePickResult::get_Image()
extern "C"  Texture2D_t3542995729 * AndroidImagePickResult_get_Image_m3451641046 (AndroidImagePickResult_t1791733552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidImagePickResult::get_ImagePath()
extern "C"  String_t* AndroidImagePickResult_get_ImagePath_m206435877 (AndroidImagePickResult_t1791733552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
