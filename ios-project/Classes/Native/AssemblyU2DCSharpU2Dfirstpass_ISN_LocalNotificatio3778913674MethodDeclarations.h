﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_LocalNotificationsController
struct ISN_LocalNotificationsController_t3778913674;
// System.Action`1<SA.Common.Models.Result>
struct Action_1_t4089019125;
// System.Action`1<ISN_LocalNotification>
struct Action_1_t74986071;
// System.String
struct String_t;
// ISN_LocalNotification
struct ISN_LocalNotification_t273186689;
// System.Collections.Generic.List`1<ISN_LocalNotification>
struct List_1_t3937275117;
// SA.Common.Models.Result
struct Result_t4287219743;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_LocalNotification273186689.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"

// System.Void ISN_LocalNotificationsController::.ctor()
extern "C"  void ISN_LocalNotificationsController__ctor_m306658881 (ISN_LocalNotificationsController_t3778913674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::.cctor()
extern "C"  void ISN_LocalNotificationsController__cctor_m2339628782 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::add_OnNotificationScheduleResult(System.Action`1<SA.Common.Models.Result>)
extern "C"  void ISN_LocalNotificationsController_add_OnNotificationScheduleResult_m2341820689 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::remove_OnNotificationScheduleResult(System.Action`1<SA.Common.Models.Result>)
extern "C"  void ISN_LocalNotificationsController_remove_OnNotificationScheduleResult_m4124130146 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::add_OnLocalNotificationReceived(System.Action`1<ISN_LocalNotification>)
extern "C"  void ISN_LocalNotificationsController_add_OnLocalNotificationReceived_m2987860248 (Il2CppObject * __this /* static, unused */, Action_1_t74986071 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::remove_OnLocalNotificationReceived(System.Action`1<ISN_LocalNotification>)
extern "C"  void ISN_LocalNotificationsController_remove_OnLocalNotificationReceived_m4020696635 (Il2CppObject * __this /* static, unused */, Action_1_t74986071 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::_ISN_ScheduleNotification(System.Int32,System.String,System.Boolean,System.String,System.Int32,System.String,System.String)
extern "C"  void ISN_LocalNotificationsController__ISN_ScheduleNotification_m2717806720 (Il2CppObject * __this /* static, unused */, int32_t ___time0, String_t* ___message1, bool ___sound2, String_t* ___nId3, int32_t ___badges4, String_t* ___data5, String_t* ___soundName6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::_ISN_CancelNotifications()
extern "C"  void ISN_LocalNotificationsController__ISN_CancelNotifications_m3206758973 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::_ISN_RequestNotificationPermissions()
extern "C"  void ISN_LocalNotificationsController__ISN_RequestNotificationPermissions_m4166019405 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::_ISN_CancelNotificationById(System.String)
extern "C"  void ISN_LocalNotificationsController__ISN_CancelNotificationById_m2572137142 (Il2CppObject * __this /* static, unused */, String_t* ___nId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::_ISN_ApplicationIconBadgeNumber(System.Int32)
extern "C"  void ISN_LocalNotificationsController__ISN_ApplicationIconBadgeNumber_m454454175 (Il2CppObject * __this /* static, unused */, int32_t ___badges0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ISN_LocalNotificationsController::_ISN_CurrentNotificationSettings()
extern "C"  int32_t ISN_LocalNotificationsController__ISN_CurrentNotificationSettings_m4149149064 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::Awake()
extern "C"  void ISN_LocalNotificationsController_Awake_m2717919868 (ISN_LocalNotificationsController_t3778913674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::RequestNotificationPermissions()
extern "C"  void ISN_LocalNotificationsController_RequestNotificationPermissions_m768496437 (ISN_LocalNotificationsController_t3778913674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::ShowGmaeKitNotification(System.String,System.String)
extern "C"  void ISN_LocalNotificationsController_ShowGmaeKitNotification_m2291356407 (ISN_LocalNotificationsController_t3778913674 * __this, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::CancelAllLocalNotifications()
extern "C"  void ISN_LocalNotificationsController_CancelAllLocalNotifications_m2657511777 (ISN_LocalNotificationsController_t3778913674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::CancelLocalNotification(ISN_LocalNotification)
extern "C"  void ISN_LocalNotificationsController_CancelLocalNotification_m742033010 (ISN_LocalNotificationsController_t3778913674 * __this, ISN_LocalNotification_t273186689 * ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::CancelLocalNotificationById(System.Int32)
extern "C"  void ISN_LocalNotificationsController_CancelLocalNotificationById_m43634356 (ISN_LocalNotificationsController_t3778913674 * __this, int32_t ___notificationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::ScheduleNotification(ISN_LocalNotification)
extern "C"  void ISN_LocalNotificationsController_ScheduleNotification_m850721026 (ISN_LocalNotificationsController_t3778913674 * __this, ISN_LocalNotification_t273186689 * ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ISN_LocalNotification> ISN_LocalNotificationsController::LoadPendingNotifications(System.Boolean)
extern "C"  List_1_t3937275117 * ISN_LocalNotificationsController_LoadPendingNotifications_m665925595 (ISN_LocalNotificationsController_t3778913674 * __this, bool ___includeAll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::ApplicationIconBadgeNumber(System.Int32)
extern "C"  void ISN_LocalNotificationsController_ApplicationIconBadgeNumber_m1086111827 (ISN_LocalNotificationsController_t3778913674 * __this, int32_t ___badges0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ISN_LocalNotificationsController::get_AllowedNotificationsType()
extern "C"  int32_t ISN_LocalNotificationsController_get_AllowedNotificationsType_m881518468 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ISN_LocalNotification ISN_LocalNotificationsController::get_LaunchNotification()
extern "C"  ISN_LocalNotification_t273186689 * ISN_LocalNotificationsController_get_LaunchNotification_m2491982024 (ISN_LocalNotificationsController_t3778913674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::OnNotificationScheduleResultAction(System.String)
extern "C"  void ISN_LocalNotificationsController_OnNotificationScheduleResultAction_m3340692013 (ISN_LocalNotificationsController_t3778913674 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::OnLocalNotificationReceived_Event(System.String)
extern "C"  void ISN_LocalNotificationsController_OnLocalNotificationReceived_Event_m748704362 (ISN_LocalNotificationsController_t3778913674 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::SaveNotifications(System.Collections.Generic.List`1<ISN_LocalNotification>)
extern "C"  void ISN_LocalNotificationsController_SaveNotifications_m3262626299 (ISN_LocalNotificationsController_t3778913674 * __this, List_1_t3937275117 * ___notifications0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::<OnNotificationScheduleResult>m__3C(SA.Common.Models.Result)
extern "C"  void ISN_LocalNotificationsController_U3COnNotificationScheduleResultU3Em__3C_m1191595770 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LocalNotificationsController::<OnLocalNotificationReceived>m__3D(ISN_LocalNotification)
extern "C"  void ISN_LocalNotificationsController_U3COnLocalNotificationReceivedU3Em__3D_m2240139736 (Il2CppObject * __this /* static, unused */, ISN_LocalNotification_t273186689 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
