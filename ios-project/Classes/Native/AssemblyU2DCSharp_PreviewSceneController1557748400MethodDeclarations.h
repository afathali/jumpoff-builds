﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PreviewSceneController
struct PreviewSceneController_t1557748400;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AN_LicenseRequestResult2331370561.h"

// System.Void PreviewSceneController::.ctor()
extern "C"  void PreviewSceneController__ctor_m2012016419 (PreviewSceneController_t1557748400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewSceneController::Awake()
extern "C"  void PreviewSceneController_Awake_m1385247152 (PreviewSceneController_t1557748400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewSceneController::SendMail()
extern "C"  void PreviewSceneController_SendMail_m3355628078 (PreviewSceneController_t1557748400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewSceneController::SendBug()
extern "C"  void PreviewSceneController_SendBug_m485126507 (PreviewSceneController_t1557748400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewSceneController::LicenseRequestResult(AN_LicenseRequestResult)
extern "C"  void PreviewSceneController_LicenseRequestResult_m1550438751 (PreviewSceneController_t1557748400 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewSceneController::OpenDocs()
extern "C"  void PreviewSceneController_OpenDocs_m3400346528 (PreviewSceneController_t1557748400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewSceneController::OpenAssetStore()
extern "C"  void PreviewSceneController_OpenAssetStore_m98399814 (PreviewSceneController_t1557748400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewSceneController::MorePlugins()
extern "C"  void PreviewSceneController_MorePlugins_m1714593792 (PreviewSceneController_t1557748400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
