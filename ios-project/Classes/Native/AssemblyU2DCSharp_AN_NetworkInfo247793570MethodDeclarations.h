﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_NetworkInfo
struct AN_NetworkInfo_t247793570;

#include "codegen/il2cpp-codegen.h"

// System.Void AN_NetworkInfo::.ctor()
extern "C"  void AN_NetworkInfo__ctor_m3630964807 (AN_NetworkInfo_t247793570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
