﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DustinHorne.Json.Examples.JNSimpleObjectSample
struct JNSimpleObjectSample_t2759437545;

#include "codegen/il2cpp-codegen.h"

// System.Void DustinHorne.Json.Examples.JNSimpleObjectSample::.ctor()
extern "C"  void JNSimpleObjectSample__ctor_m2811085256 (JNSimpleObjectSample_t2759437545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DustinHorne.Json.Examples.JNSimpleObjectSample::Sample()
extern "C"  void JNSimpleObjectSample_Sample_m503971148 (JNSimpleObjectSample_t2759437545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
