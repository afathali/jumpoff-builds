﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonTextWriter
struct JsonTextWriter_t2524035668;
// System.IO.TextWriter
struct TextWriter_t4027217640;
// Newtonsoft.Json.Utilities.Base64Encoder
struct Base64Encoder_t2682770749;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Uri
struct Uri_t19570940;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_TextWriter4027217640.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken620654565.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_DateTimeOffset1362988906.h"
#include "mscorlib_System_Guid2533601593.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "System_System_Uri19570940.h"

// System.Void Newtonsoft.Json.JsonTextWriter::.ctor(System.IO.TextWriter)
extern "C"  void JsonTextWriter__ctor_m2980545907 (JsonTextWriter_t2524035668 * __this, TextWriter_t4027217640 * ___textWriter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.Base64Encoder Newtonsoft.Json.JsonTextWriter::get_Base64Encoder()
extern "C"  Base64Encoder_t2682770749 * JsonTextWriter_get_Base64Encoder_m3532934867 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonTextWriter::get_Indentation()
extern "C"  int32_t JsonTextWriter_get_Indentation_m2881304432 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::set_Indentation(System.Int32)
extern "C"  void JsonTextWriter_set_Indentation_m3502254167 (JsonTextWriter_t2524035668 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.JsonTextWriter::get_QuoteChar()
extern "C"  Il2CppChar JsonTextWriter_get_QuoteChar_m3713320349 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::set_QuoteChar(System.Char)
extern "C"  void JsonTextWriter_set_QuoteChar_m503674322 (JsonTextWriter_t2524035668 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.JsonTextWriter::get_IndentChar()
extern "C"  Il2CppChar JsonTextWriter_get_IndentChar_m2453583723 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::set_IndentChar(System.Char)
extern "C"  void JsonTextWriter_set_IndentChar_m2063781436 (JsonTextWriter_t2524035668 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextWriter::get_QuoteName()
extern "C"  bool JsonTextWriter_get_QuoteName_m2706381464 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::set_QuoteName(System.Boolean)
extern "C"  void JsonTextWriter_set_QuoteName_m845101465 (JsonTextWriter_t2524035668 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::Flush()
extern "C"  void JsonTextWriter_Flush_m3660244584 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::Close()
extern "C"  void JsonTextWriter_Close_m3660117270 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteStartObject()
extern "C"  void JsonTextWriter_WriteStartObject_m4206416270 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteStartArray()
extern "C"  void JsonTextWriter_WriteStartArray_m542747312 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteStartConstructor(System.String)
extern "C"  void JsonTextWriter_WriteStartConstructor_m2891928265 (JsonTextWriter_t2524035668 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern "C"  void JsonTextWriter_WriteEnd_m1638987958 (JsonTextWriter_t2524035668 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WritePropertyName(System.String)
extern "C"  void JsonTextWriter_WritePropertyName_m1425497525 (JsonTextWriter_t2524035668 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteIndent()
extern "C"  void JsonTextWriter_WriteIndent_m4137705955 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValueDelimiter()
extern "C"  void JsonTextWriter_WriteValueDelimiter_m3009150007 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteIndentSpace()
extern "C"  void JsonTextWriter_WriteIndentSpace_m1252087471 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValueInternal(System.String,Newtonsoft.Json.JsonToken)
extern "C"  void JsonTextWriter_WriteValueInternal_m1829524345 (JsonTextWriter_t2524035668 * __this, String_t* ___value0, int32_t ___token1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteNull()
extern "C"  void JsonTextWriter_WriteNull_m2134593948 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteUndefined()
extern "C"  void JsonTextWriter_WriteUndefined_m3029768025 (JsonTextWriter_t2524035668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteRaw(System.String)
extern "C"  void JsonTextWriter_WriteRaw_m3356180185 (JsonTextWriter_t2524035668 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.String)
extern "C"  void JsonTextWriter_WriteValue_m797814358 (JsonTextWriter_t2524035668 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int32)
extern "C"  void JsonTextWriter_WriteValue_m1675337881 (JsonTextWriter_t2524035668 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt32)
extern "C"  void JsonTextWriter_WriteValue_m990465214 (JsonTextWriter_t2524035668 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int64)
extern "C"  void JsonTextWriter_WriteValue_m2381150392 (JsonTextWriter_t2524035668 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt64)
extern "C"  void JsonTextWriter_WriteValue_m4122633059 (JsonTextWriter_t2524035668 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Single)
extern "C"  void JsonTextWriter_WriteValue_m86323769 (JsonTextWriter_t2524035668 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Double)
extern "C"  void JsonTextWriter_WriteValue_m2623689678 (JsonTextWriter_t2524035668 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Boolean)
extern "C"  void JsonTextWriter_WriteValue_m575480683 (JsonTextWriter_t2524035668 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int16)
extern "C"  void JsonTextWriter_WriteValue_m1957662887 (JsonTextWriter_t2524035668 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt16)
extern "C"  void JsonTextWriter_WriteValue_m2959833616 (JsonTextWriter_t2524035668 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Char)
extern "C"  void JsonTextWriter_WriteValue_m237357043 (JsonTextWriter_t2524035668 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Byte)
extern "C"  void JsonTextWriter_WriteValue_m1017326305 (JsonTextWriter_t2524035668 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.SByte)
extern "C"  void JsonTextWriter_WriteValue_m3012499328 (JsonTextWriter_t2524035668 * __this, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Decimal)
extern "C"  void JsonTextWriter_WriteValue_m3436188944 (JsonTextWriter_t2524035668 * __this, Decimal_t724701077  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.DateTime)
extern "C"  void JsonTextWriter_WriteValue_m4235723074 (JsonTextWriter_t2524035668 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Byte[])
extern "C"  void JsonTextWriter_WriteValue_m1658588619 (JsonTextWriter_t2524035668 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.DateTimeOffset)
extern "C"  void JsonTextWriter_WriteValue_m182762811 (JsonTextWriter_t2524035668 * __this, DateTimeOffset_t1362988906  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Guid)
extern "C"  void JsonTextWriter_WriteValue_m2791832218 (JsonTextWriter_t2524035668 * __this, Guid_t2533601593  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.TimeSpan)
extern "C"  void JsonTextWriter_WriteValue_m3250752834 (JsonTextWriter_t2524035668 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Uri)
extern "C"  void JsonTextWriter_WriteValue_m2752927941 (JsonTextWriter_t2524035668 * __this, Uri_t19570940 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteComment(System.String)
extern "C"  void JsonTextWriter_WriteComment_m1181623326 (JsonTextWriter_t2524035668 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteWhitespace(System.String)
extern "C"  void JsonTextWriter_WriteWhitespace_m2569445886 (JsonTextWriter_t2524035668 * __this, String_t* ___ws0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
