﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_PlusShareBuilder
struct AN_PlusShareBuilder_t211572262;
// System.String
struct String_t;
// System.Action`1<AN_PlusShareResult>
struct Action_1_t3290487396;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// AN_PlusShareResult
struct AN_PlusShareResult_t3488688014;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharp_AN_PlusShareResult3488688014.h"

// System.Void AN_PlusShareBuilder::.ctor(System.String)
extern "C"  void AN_PlusShareBuilder__ctor_m879615039 (AN_PlusShareBuilder_t211572262 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusShareBuilder::add_OnPlusShareResult(System.Action`1<AN_PlusShareResult>)
extern "C"  void AN_PlusShareBuilder_add_OnPlusShareResult_m2653678494 (AN_PlusShareBuilder_t211572262 * __this, Action_1_t3290487396 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusShareBuilder::remove_OnPlusShareResult(System.Action`1<AN_PlusShareResult>)
extern "C"  void AN_PlusShareBuilder_remove_OnPlusShareResult_m463859819 (AN_PlusShareBuilder_t211572262 * __this, Action_1_t3290487396 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusShareBuilder::AddImage(UnityEngine.Texture2D)
extern "C"  void AN_PlusShareBuilder_AddImage_m1533682049 (AN_PlusShareBuilder_t211572262 * __this, Texture2D_t3542995729 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusShareBuilder::Share()
extern "C"  void AN_PlusShareBuilder_Share_m4118309490 (AN_PlusShareBuilder_t211572262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusShareBuilder::PlusShareCallback(AN_PlusShareResult)
extern "C"  void AN_PlusShareBuilder_PlusShareCallback_m1385456421 (AN_PlusShareBuilder_t211572262 * __this, AN_PlusShareResult_t3488688014 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
