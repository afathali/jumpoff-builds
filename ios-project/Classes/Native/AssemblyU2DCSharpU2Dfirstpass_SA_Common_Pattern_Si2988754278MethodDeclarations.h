﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si3172014400MethodDeclarations.h"

// System.Void SA.Common.Pattern.Singleton`1<iCloudManager>::.ctor()
#define Singleton_1__ctor_m122516707(__this, method) ((  void (*) (Singleton_1_t2988754278 *, const MethodInfo*))Singleton_1__ctor_m4152044218_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<iCloudManager>::.cctor()
#define Singleton_1__cctor_m2841010402(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1__cctor_m982417053_gshared)(__this /* static, unused */, method)
// T SA.Common.Pattern.Singleton`1<iCloudManager>::get_Instance()
#define Singleton_1_get_Instance_m1107454056(__this /* static, unused */, method) ((  iCloudManager_t2506189173 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m3228489301_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<iCloudManager>::get_HasInstance()
#define Singleton_1_get_HasInstance_m3644764709(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_HasInstance_m2551508260_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<iCloudManager>::get_IsDestroyed()
#define Singleton_1_get_IsDestroyed_m2912958503(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_IsDestroyed_m4170993228_gshared)(__this /* static, unused */, method)
// System.Void SA.Common.Pattern.Singleton`1<iCloudManager>::OnDestroy()
#define Singleton_1_OnDestroy_m1848609294(__this, method) ((  void (*) (Singleton_1_t2988754278 *, const MethodInfo*))Singleton_1_OnDestroy_m3790554761_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<iCloudManager>::OnApplicationQuit()
#define Singleton_1_OnApplicationQuit_m218227897(__this, method) ((  void (*) (Singleton_1_t2988754278 *, const MethodInfo*))Singleton_1_OnApplicationQuit_m1956476828_gshared)(__this, method)
