﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DefaultPreviewButton[]
struct DefaultPreviewButtonU5BU5D_t3800739864;
// DefaultPreviewButton
struct DefaultPreviewButton_t12674677;
// SA_Texture
struct SA_Texture_t3361108684;
// SA_Label
struct SA_Label_t226960149;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FacebookAndroidTurnBasedAndGiftsExample
struct  FacebookAndroidTurnBasedAndGiftsExample_t99045661  : public MonoBehaviour_t1158329972
{
public:
	// DefaultPreviewButton[] FacebookAndroidTurnBasedAndGiftsExample::ConnectionDependedntButtons
	DefaultPreviewButtonU5BU5D_t3800739864* ___ConnectionDependedntButtons_4;
	// DefaultPreviewButton FacebookAndroidTurnBasedAndGiftsExample::connectButton
	DefaultPreviewButton_t12674677 * ___connectButton_5;
	// SA_Texture FacebookAndroidTurnBasedAndGiftsExample::avatar
	SA_Texture_t3361108684 * ___avatar_6;
	// SA_Label FacebookAndroidTurnBasedAndGiftsExample::Location
	SA_Label_t226960149 * ___Location_7;
	// SA_Label FacebookAndroidTurnBasedAndGiftsExample::Language
	SA_Label_t226960149 * ___Language_8;
	// SA_Label FacebookAndroidTurnBasedAndGiftsExample::Mail
	SA_Label_t226960149 * ___Mail_9;
	// SA_Label FacebookAndroidTurnBasedAndGiftsExample::Name
	SA_Label_t226960149 * ___Name_10;
	// System.String FacebookAndroidTurnBasedAndGiftsExample::BombItemId
	String_t* ___BombItemId_11;

public:
	inline static int32_t get_offset_of_ConnectionDependedntButtons_4() { return static_cast<int32_t>(offsetof(FacebookAndroidTurnBasedAndGiftsExample_t99045661, ___ConnectionDependedntButtons_4)); }
	inline DefaultPreviewButtonU5BU5D_t3800739864* get_ConnectionDependedntButtons_4() const { return ___ConnectionDependedntButtons_4; }
	inline DefaultPreviewButtonU5BU5D_t3800739864** get_address_of_ConnectionDependedntButtons_4() { return &___ConnectionDependedntButtons_4; }
	inline void set_ConnectionDependedntButtons_4(DefaultPreviewButtonU5BU5D_t3800739864* value)
	{
		___ConnectionDependedntButtons_4 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionDependedntButtons_4, value);
	}

	inline static int32_t get_offset_of_connectButton_5() { return static_cast<int32_t>(offsetof(FacebookAndroidTurnBasedAndGiftsExample_t99045661, ___connectButton_5)); }
	inline DefaultPreviewButton_t12674677 * get_connectButton_5() const { return ___connectButton_5; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_connectButton_5() { return &___connectButton_5; }
	inline void set_connectButton_5(DefaultPreviewButton_t12674677 * value)
	{
		___connectButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___connectButton_5, value);
	}

	inline static int32_t get_offset_of_avatar_6() { return static_cast<int32_t>(offsetof(FacebookAndroidTurnBasedAndGiftsExample_t99045661, ___avatar_6)); }
	inline SA_Texture_t3361108684 * get_avatar_6() const { return ___avatar_6; }
	inline SA_Texture_t3361108684 ** get_address_of_avatar_6() { return &___avatar_6; }
	inline void set_avatar_6(SA_Texture_t3361108684 * value)
	{
		___avatar_6 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_6, value);
	}

	inline static int32_t get_offset_of_Location_7() { return static_cast<int32_t>(offsetof(FacebookAndroidTurnBasedAndGiftsExample_t99045661, ___Location_7)); }
	inline SA_Label_t226960149 * get_Location_7() const { return ___Location_7; }
	inline SA_Label_t226960149 ** get_address_of_Location_7() { return &___Location_7; }
	inline void set_Location_7(SA_Label_t226960149 * value)
	{
		___Location_7 = value;
		Il2CppCodeGenWriteBarrier(&___Location_7, value);
	}

	inline static int32_t get_offset_of_Language_8() { return static_cast<int32_t>(offsetof(FacebookAndroidTurnBasedAndGiftsExample_t99045661, ___Language_8)); }
	inline SA_Label_t226960149 * get_Language_8() const { return ___Language_8; }
	inline SA_Label_t226960149 ** get_address_of_Language_8() { return &___Language_8; }
	inline void set_Language_8(SA_Label_t226960149 * value)
	{
		___Language_8 = value;
		Il2CppCodeGenWriteBarrier(&___Language_8, value);
	}

	inline static int32_t get_offset_of_Mail_9() { return static_cast<int32_t>(offsetof(FacebookAndroidTurnBasedAndGiftsExample_t99045661, ___Mail_9)); }
	inline SA_Label_t226960149 * get_Mail_9() const { return ___Mail_9; }
	inline SA_Label_t226960149 ** get_address_of_Mail_9() { return &___Mail_9; }
	inline void set_Mail_9(SA_Label_t226960149 * value)
	{
		___Mail_9 = value;
		Il2CppCodeGenWriteBarrier(&___Mail_9, value);
	}

	inline static int32_t get_offset_of_Name_10() { return static_cast<int32_t>(offsetof(FacebookAndroidTurnBasedAndGiftsExample_t99045661, ___Name_10)); }
	inline SA_Label_t226960149 * get_Name_10() const { return ___Name_10; }
	inline SA_Label_t226960149 ** get_address_of_Name_10() { return &___Name_10; }
	inline void set_Name_10(SA_Label_t226960149 * value)
	{
		___Name_10 = value;
		Il2CppCodeGenWriteBarrier(&___Name_10, value);
	}

	inline static int32_t get_offset_of_BombItemId_11() { return static_cast<int32_t>(offsetof(FacebookAndroidTurnBasedAndGiftsExample_t99045661, ___BombItemId_11)); }
	inline String_t* get_BombItemId_11() const { return ___BombItemId_11; }
	inline String_t** get_address_of_BombItemId_11() { return &___BombItemId_11; }
	inline void set_BombItemId_11(String_t* value)
	{
		___BombItemId_11 = value;
		Il2CppCodeGenWriteBarrier(&___BombItemId_11, value);
	}
};

struct FacebookAndroidTurnBasedAndGiftsExample_t99045661_StaticFields
{
public:
	// System.Boolean FacebookAndroidTurnBasedAndGiftsExample::IsUserInfoLoaded
	bool ___IsUserInfoLoaded_2;
	// System.Boolean FacebookAndroidTurnBasedAndGiftsExample::IsAuntificated
	bool ___IsAuntificated_3;

public:
	inline static int32_t get_offset_of_IsUserInfoLoaded_2() { return static_cast<int32_t>(offsetof(FacebookAndroidTurnBasedAndGiftsExample_t99045661_StaticFields, ___IsUserInfoLoaded_2)); }
	inline bool get_IsUserInfoLoaded_2() const { return ___IsUserInfoLoaded_2; }
	inline bool* get_address_of_IsUserInfoLoaded_2() { return &___IsUserInfoLoaded_2; }
	inline void set_IsUserInfoLoaded_2(bool value)
	{
		___IsUserInfoLoaded_2 = value;
	}

	inline static int32_t get_offset_of_IsAuntificated_3() { return static_cast<int32_t>(offsetof(FacebookAndroidTurnBasedAndGiftsExample_t99045661_StaticFields, ___IsAuntificated_3)); }
	inline bool get_IsAuntificated_3() const { return ___IsAuntificated_3; }
	inline bool* get_address_of_IsAuntificated_3() { return &___IsAuntificated_3; }
	inline void set_IsAuntificated_3(bool value)
	{
		___IsAuntificated_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
