﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Texture2DExtensions.Texture2DExtensions::Crop(UnityEngine.Texture2D,UnityEngine.Rect)
extern "C"  void Texture2DExtensions_Crop_m69142804 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, Rect_t3681755626  ___cropRect1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::Scale(UnityEngine.Texture2D,System.Int32,System.Int32)
extern "C"  void Texture2DExtensions_Scale_m1210964219 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, int32_t ___width1, int32_t ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::Rotate(UnityEngine.Texture2D,System.Single)
extern "C"  void Texture2DExtensions_Rotate_m3535766975 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, float ___degrees1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::Mirror(UnityEngine.Texture2D,System.Boolean,System.Boolean)
extern "C"  void Texture2DExtensions_Mirror_m407902250 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, bool ___horizontal1, bool ___vertical2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::Hue(UnityEngine.Texture2D,System.Single)
extern "C"  void Texture2DExtensions_Hue_m2317953844 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, float ___deltaHue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::Saturation(UnityEngine.Texture2D,System.Single)
extern "C"  void Texture2DExtensions_Saturation_m3958584274 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, float ___deltaSaturation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::Brightness(UnityEngine.Texture2D,System.Single)
extern "C"  void Texture2DExtensions_Brightness_m2497389151 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, float ___deltaBrightness1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::ChangeHSB(UnityEngine.Texture2D,System.Single,System.Single,System.Single)
extern "C"  void Texture2DExtensions_ChangeHSB_m3144622305 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, float ___deltaHue1, float ___deltaSaturation2, float ___deltaBrightness3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::Luminance(UnityEngine.Texture2D,System.Single)
extern "C"  void Texture2DExtensions_Luminance_m3673743724 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, float ___deltaLuminance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::Contrast(UnityEngine.Texture2D,System.Single)
extern "C"  void Texture2DExtensions_Contrast_m1326967312 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, float ___deltaContrast1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::Colorize(UnityEngine.Texture2D,UnityEngine.Color,System.Single)
extern "C"  void Texture2DExtensions_Colorize_m1812906279 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, Color_t2020392075  ___color1, float ___amount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::MakeColorTransparent(UnityEngine.Texture2D,UnityEngine.Color,System.Single)
extern "C"  void Texture2DExtensions_MakeColorTransparent_m2818366301 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, Color_t2020392075  ___color1, float ___amount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::MakeColorTransparent(UnityEngine.Texture2D,UnityEngine.Color,System.Single,System.Single)
extern "C"  void Texture2DExtensions_MakeColorTransparent_m1080287468 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, Color_t2020392075  ___color1, float ___amount2, float ___tolerance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::ReplaceColor(UnityEngine.Texture2D,UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  void Texture2DExtensions_ReplaceColor_m1805719239 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, Color_t2020392075  ___color1, Color_t2020392075  ___newColor2, float ___tolerance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::FadeToColor(UnityEngine.Texture2D,UnityEngine.Color,System.Single)
extern "C"  void Texture2DExtensions_FadeToColor_m3878513294 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, Color_t2020392075  ___color1, float ___amount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Texture2DExtensions.Texture2DExtensions::GetCopy(UnityEngine.Texture2D)
extern "C"  Texture2D_t3542995729 * Texture2DExtensions_GetCopy_m3426811649 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Texture2DExtensions.Texture2DExtensions::GetCopy(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Texture2D_t3542995729 * Texture2DExtensions_GetCopy_m3579542721 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, int32_t ___x1, int32_t ___y2, int32_t ___w3, int32_t ___h4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Texture2DExtensions.Texture2DExtensions::GetSection(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Texture2D_t3542995729 * Texture2DExtensions_GetSection_m2799690621 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, int32_t ___x1, int32_t ___y2, int32_t ___w3, int32_t ___h4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Texture2DExtensions.Texture2DExtensions::GetCopy(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern "C"  Texture2D_t3542995729 * Texture2DExtensions_GetCopy_m32106208 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, int32_t ___x1, int32_t ___y2, int32_t ___w3, int32_t ___h4, bool ___mipMaps5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Texture2DExtensions.Texture2DExtensions::ScaledCopy(UnityEngine.Texture2D,System.Int32,System.Int32,System.Boolean)
extern "C"  Texture2D_t3542995729 * Texture2DExtensions_ScaledCopy_m2083910234 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, int32_t ___width1, int32_t ___height2, bool ___mipMaps3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::RevertTo(UnityEngine.Texture2D,UnityEngine.Texture2D)
extern "C"  void Texture2DExtensions_RevertTo_m2627863640 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, Texture2D_t3542995729 * ___original1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::MakeEqual(UnityEngine.Texture2D,UnityEngine.Texture2D)
extern "C"  void Texture2DExtensions_MakeEqual_m2564805507 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, Texture2D_t3542995729 * ___original1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::CopyFrom(UnityEngine.Texture2D,UnityEngine.Texture2D)
extern "C"  void Texture2DExtensions_CopyFrom_m726441618 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, Texture2D_t3542995729 * ___original1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::CopyFrom(UnityEngine.Texture2D,UnityEngine.Texture2D,System.Boolean)
extern "C"  void Texture2DExtensions_CopyFrom_m2997052513 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, Texture2D_t3542995729 * ___original1, bool ___mipMaps2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Texture2DExtensions.Texture2DExtensions::GetAverageColor(UnityEngine.Texture2D)
extern "C"  Color_t2020392075  Texture2DExtensions_GetAverageColor_m841604086 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Texture2DExtensions.Texture2DExtensions::GetAverageColor(UnityEngine.Texture2D,UnityEngine.Color)
extern "C"  Color_t2020392075  Texture2DExtensions_GetAverageColor_m567708592 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, Color_t2020392075  ___useThisColorForAlpha1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Texture2DExtensions.Texture2DExtensions::IsNormalMap(UnityEngine.Texture2D)
extern "C"  bool Texture2DExtensions_IsNormalMap_m710178926 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___aTexture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Texture2DExtensions.Texture2DExtensions::ConvertToNormalMap(UnityEngine.Texture2D,System.Single)
extern "C"  void Texture2DExtensions_ConvertToNormalMap_m2463949361 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, float ___strength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Texture2DExtensions.Texture2DExtensions::GetNormalMap(UnityEngine.Texture2D,System.Single)
extern "C"  Texture2D_t3542995729 * Texture2DExtensions_GetNormalMap_m3640351494 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, float ___strength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Texture2DExtensions.Texture2DExtensions::GetUnityNormalMap(UnityEngine.Texture2D)
extern "C"  Texture2D_t3542995729 * Texture2DExtensions_GetUnityNormalMap_m399576054 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Texture2DExtensions.Texture2DExtensions::GetHSBColorDistance(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  float Texture2DExtensions_GetHSBColorDistance_m142889131 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color10, Color_t2020392075  ___color21, float ___power2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Texture2DExtensions.Texture2DExtensions::GetRGBColorDistance(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  float Texture2DExtensions_GetRGBColorDistance_m2538477161 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color10, Color_t2020392075  ___color21, float ___power2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Texture2DExtensions.Texture2DExtensions::GetColorDistance(UnityEngine.Color,UnityEngine.Color)
extern "C"  float Texture2DExtensions_GetColorDistance_m2040107279 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color10, Color_t2020392075  ___color21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Texture2DExtensions.Texture2DExtensions::GrayScale(UnityEngine.Color)
extern "C"  float Texture2DExtensions_GrayScale_m2917207040 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Texture2DExtensions.Texture2DExtensions::MakeColor(UnityEngine.Vector3)
extern "C"  Color_t2020392075  Texture2DExtensions_MakeColor_m1015824760 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___hsb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Texture2DExtensions.Texture2DExtensions::MakeHSB(UnityEngine.Color)
extern "C"  Vector3_t2243707580  Texture2DExtensions_MakeHSB_m4005786068 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] Texture2DExtensions.Texture2DExtensions::ScaledPixels(UnityEngine.Color[],System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  ColorU5BU5D_t672350442* Texture2DExtensions_ScaledPixels_m2477281361 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t672350442* ___originalPixels0, int32_t ___oldWidth1, int32_t ___oldHeight2, int32_t ___width3, int32_t ___height4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
