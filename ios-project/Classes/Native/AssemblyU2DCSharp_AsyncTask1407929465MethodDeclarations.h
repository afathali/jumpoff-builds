﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AsyncTask
struct AsyncTask_t1407929465;

#include "codegen/il2cpp-codegen.h"

// System.Void AsyncTask::.ctor()
extern "C"  void AsyncTask__ctor_m758758648 (AsyncTask_t1407929465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AsyncTask::Awake()
extern "C"  void AsyncTask_Awake_m396808169 (AsyncTask_t1407929465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
