﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookAndroidTurnBasedAndGiftsExample
struct FacebookAndroidTurnBasedAndGiftsExample_t99045661;
// FB_Result
struct FB_Result_t838248372;
// FB_AppRequestResult
struct FB_AppRequestResult_t2450670428;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FB_Result838248372.h"
#include "AssemblyU2DCSharp_FB_AppRequestResult2450670428.h"

// System.Void FacebookAndroidTurnBasedAndGiftsExample::.ctor()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample__ctor_m2000026966 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::.cctor()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample__cctor_m2904269911 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::Awake()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_Awake_m3642945453 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::FixedUpdate()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_FixedUpdate_m1401697083 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::RetriveAppRequests()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_RetriveAppRequests_m2206571524 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::OnAppRequestsLoaded(FB_Result)
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_OnAppRequestsLoaded_m4034981471 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::SendTrunhRequest()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_SendTrunhRequest_m2497835430 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::SendTrunhRequestToSpecifiedFriend()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_SendTrunhRequestToSpecifiedFriend_m1282939847 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::AskItem()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_AskItem_m64076028 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::SendItem()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_SendItem_m1832383593 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::SendInv()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_SendInv_m3328270081 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::SendInvToSpecifayedFirend()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_SendInvToSpecifayedFirend_m1394271407 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::SendToSpecifiedFriend()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_SendToSpecifiedFriend_m1084878211 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::OnAppRequestCompleteAction(FB_AppRequestResult)
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_OnAppRequestCompleteAction_m211900566 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, FB_AppRequestResult_t2450670428 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::Connect()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_Connect_m2911276364 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::LoadUserData()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_LoadUserData_m2125420963 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::OnFocusChanged(System.Boolean)
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_OnFocusChanged_m2246412356 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, bool ___focus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::OnUserDataLoaded(FB_Result)
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_OnUserDataLoaded_m1448863669 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::OnInit()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_OnInit_m214363367 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::OnAuth(FB_Result)
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_OnAuth_m520151147 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidTurnBasedAndGiftsExample::LogOut()
extern "C"  void FacebookAndroidTurnBasedAndGiftsExample_LogOut_m3737808082 (FacebookAndroidTurnBasedAndGiftsExample_t99045661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
