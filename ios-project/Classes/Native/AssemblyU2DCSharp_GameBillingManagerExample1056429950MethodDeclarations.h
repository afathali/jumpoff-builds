﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameBillingManagerExample
struct GameBillingManagerExample_t1056429950;
// System.String
struct String_t;
// GooglePurchaseTemplate
struct GooglePurchaseTemplate_t2609331866;
// BillingResult
struct BillingResult_t3511841850;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GooglePurchaseTemplate2609331866.h"
#include "AssemblyU2DCSharp_BillingResult3511841850.h"

// System.Void GameBillingManagerExample::.ctor()
extern "C"  void GameBillingManagerExample__ctor_m2309484601 (GameBillingManagerExample_t1056429950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameBillingManagerExample::.cctor()
extern "C"  void GameBillingManagerExample__cctor_m1159392296 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameBillingManagerExample::init()
extern "C"  void GameBillingManagerExample_init_m2538358571 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameBillingManagerExample::purchase(System.String)
extern "C"  void GameBillingManagerExample_purchase_m484742252 (Il2CppObject * __this /* static, unused */, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameBillingManagerExample::consume(System.String)
extern "C"  void GameBillingManagerExample_consume_m3558251331 (Il2CppObject * __this /* static, unused */, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameBillingManagerExample::get_isInited()
extern "C"  bool GameBillingManagerExample_get_isInited_m2564965445 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameBillingManagerExample::OnProcessingPurchasedProduct(GooglePurchaseTemplate)
extern "C"  void GameBillingManagerExample_OnProcessingPurchasedProduct_m3524914041 (Il2CppObject * __this /* static, unused */, GooglePurchaseTemplate_t2609331866 * ___purchase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameBillingManagerExample::OnProcessingConsumeProduct(GooglePurchaseTemplate)
extern "C"  void GameBillingManagerExample_OnProcessingConsumeProduct_m1794344760 (Il2CppObject * __this /* static, unused */, GooglePurchaseTemplate_t2609331866 * ___purchase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameBillingManagerExample::OnProductPurchased(BillingResult)
extern "C"  void GameBillingManagerExample_OnProductPurchased_m3357950872 (Il2CppObject * __this /* static, unused */, BillingResult_t3511841850 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameBillingManagerExample::OnProductConsumed(BillingResult)
extern "C"  void GameBillingManagerExample_OnProductConsumed_m1798542145 (Il2CppObject * __this /* static, unused */, BillingResult_t3511841850 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameBillingManagerExample::OnBillingConnected(BillingResult)
extern "C"  void GameBillingManagerExample_OnBillingConnected_m3244440586 (Il2CppObject * __this /* static, unused */, BillingResult_t3511841850 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameBillingManagerExample::OnRetrieveProductsFinised(BillingResult)
extern "C"  void GameBillingManagerExample_OnRetrieveProductsFinised_m4012631050 (Il2CppObject * __this /* static, unused */, BillingResult_t3511841850 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameBillingManagerExample::UpdateStoreData()
extern "C"  void GameBillingManagerExample_UpdateStoreData_m3876131755 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
