﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_GMSGiftsProxy
struct AN_GMSGiftsProxy_t1705430956;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_GMSGiftsProxy::.ctor()
extern "C"  void AN_GMSGiftsProxy__ctor_m1379999549 (AN_GMSGiftsProxy_t1705430956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGiftsProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_GMSGiftsProxy_CallActivityFunction_m3952316086 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGiftsProxy::sendGiftRequest(System.Int32,System.String,System.Int32,System.String,System.String)
extern "C"  void AN_GMSGiftsProxy_sendGiftRequest_m589495758 (Il2CppObject * __this /* static, unused */, int32_t ___type0, String_t* ___playload1, int32_t ___requestLifetimeDays2, String_t* ___icon3, String_t* ___description4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGiftsProxy::showRequestAccepDialog()
extern "C"  void AN_GMSGiftsProxy_showRequestAccepDialog_m838556395 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGiftsProxy::acceptRequests(System.String)
extern "C"  void AN_GMSGiftsProxy_acceptRequests_m898156469 (Il2CppObject * __this /* static, unused */, String_t* ___ids0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGiftsProxy::dismissRequest(System.String)
extern "C"  void AN_GMSGiftsProxy_dismissRequest_m1606683692 (Il2CppObject * __this /* static, unused */, String_t* ___ids0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGiftsProxy::leaveRoom()
extern "C"  void AN_GMSGiftsProxy_leaveRoom_m3282987625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSGiftsProxy::showInvitationBox()
extern "C"  void AN_GMSGiftsProxy_showInvitationBox_m853931028 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
