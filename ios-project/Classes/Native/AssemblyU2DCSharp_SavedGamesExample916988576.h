﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Texture
struct Texture_t2243626319;
// DefaultPreviewButton
struct DefaultPreviewButton_t12674677;
// SA_Label
struct SA_Label_t226960149;
// DefaultPreviewButton[]
struct DefaultPreviewButtonU5BU5D_t3800739864;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SavedGamesExample
struct  SavedGamesExample_t916988576  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SavedGamesExample::avatar
	GameObject_t1756533147 * ___avatar_2;
	// UnityEngine.Texture SavedGamesExample::defaulttexture
	Texture_t2243626319 * ___defaulttexture_3;
	// DefaultPreviewButton SavedGamesExample::connectButton
	DefaultPreviewButton_t12674677 * ___connectButton_4;
	// SA_Label SavedGamesExample::playerLabel
	SA_Label_t226960149 * ___playerLabel_5;
	// DefaultPreviewButton[] SavedGamesExample::ConnectionDependedntButtons
	DefaultPreviewButtonU5BU5D_t3800739864* ___ConnectionDependedntButtons_6;

public:
	inline static int32_t get_offset_of_avatar_2() { return static_cast<int32_t>(offsetof(SavedGamesExample_t916988576, ___avatar_2)); }
	inline GameObject_t1756533147 * get_avatar_2() const { return ___avatar_2; }
	inline GameObject_t1756533147 ** get_address_of_avatar_2() { return &___avatar_2; }
	inline void set_avatar_2(GameObject_t1756533147 * value)
	{
		___avatar_2 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_2, value);
	}

	inline static int32_t get_offset_of_defaulttexture_3() { return static_cast<int32_t>(offsetof(SavedGamesExample_t916988576, ___defaulttexture_3)); }
	inline Texture_t2243626319 * get_defaulttexture_3() const { return ___defaulttexture_3; }
	inline Texture_t2243626319 ** get_address_of_defaulttexture_3() { return &___defaulttexture_3; }
	inline void set_defaulttexture_3(Texture_t2243626319 * value)
	{
		___defaulttexture_3 = value;
		Il2CppCodeGenWriteBarrier(&___defaulttexture_3, value);
	}

	inline static int32_t get_offset_of_connectButton_4() { return static_cast<int32_t>(offsetof(SavedGamesExample_t916988576, ___connectButton_4)); }
	inline DefaultPreviewButton_t12674677 * get_connectButton_4() const { return ___connectButton_4; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_connectButton_4() { return &___connectButton_4; }
	inline void set_connectButton_4(DefaultPreviewButton_t12674677 * value)
	{
		___connectButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___connectButton_4, value);
	}

	inline static int32_t get_offset_of_playerLabel_5() { return static_cast<int32_t>(offsetof(SavedGamesExample_t916988576, ___playerLabel_5)); }
	inline SA_Label_t226960149 * get_playerLabel_5() const { return ___playerLabel_5; }
	inline SA_Label_t226960149 ** get_address_of_playerLabel_5() { return &___playerLabel_5; }
	inline void set_playerLabel_5(SA_Label_t226960149 * value)
	{
		___playerLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___playerLabel_5, value);
	}

	inline static int32_t get_offset_of_ConnectionDependedntButtons_6() { return static_cast<int32_t>(offsetof(SavedGamesExample_t916988576, ___ConnectionDependedntButtons_6)); }
	inline DefaultPreviewButtonU5BU5D_t3800739864* get_ConnectionDependedntButtons_6() const { return ___ConnectionDependedntButtons_6; }
	inline DefaultPreviewButtonU5BU5D_t3800739864** get_address_of_ConnectionDependedntButtons_6() { return &___ConnectionDependedntButtons_6; }
	inline void set_ConnectionDependedntButtons_6(DefaultPreviewButtonU5BU5D_t3800739864* value)
	{
		___ConnectionDependedntButtons_6 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionDependedntButtons_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
