﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenCapture
struct  ScreenCapture_t1787384414  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct ScreenCapture_t1787384414_StaticFields
{
public:
	// System.Int32 ScreenCapture::tempFileCount
	int32_t ___tempFileCount_2;

public:
	inline static int32_t get_offset_of_tempFileCount_2() { return static_cast<int32_t>(offsetof(ScreenCapture_t1787384414_StaticFields, ___tempFileCount_2)); }
	inline int32_t get_tempFileCount_2() const { return ___tempFileCount_2; }
	inline int32_t* get_address_of_tempFileCount_2() { return &___tempFileCount_2; }
	inline void set_tempFileCount_2(int32_t value)
	{
		___tempFileCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
