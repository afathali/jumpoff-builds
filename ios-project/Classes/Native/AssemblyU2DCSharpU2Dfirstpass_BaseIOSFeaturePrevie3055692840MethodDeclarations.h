﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseIOSFeaturePreview
struct BaseIOSFeaturePreview_t3055692840;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BaseIOSFeaturePreview::.ctor()
extern "C"  void BaseIOSFeaturePreview__ctor_m3771582115 (BaseIOSFeaturePreview_t3055692840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseIOSFeaturePreview::InitStyles()
extern "C"  void BaseIOSFeaturePreview_InitStyles_m2907669349 (BaseIOSFeaturePreview_t3055692840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseIOSFeaturePreview::Start()
extern "C"  void BaseIOSFeaturePreview_Start_m2963835903 (BaseIOSFeaturePreview_t3055692840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseIOSFeaturePreview::UpdateToStartPos()
extern "C"  void BaseIOSFeaturePreview_UpdateToStartPos_m848793309 (BaseIOSFeaturePreview_t3055692840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseIOSFeaturePreview::LoadLevel(System.String)
extern "C"  void BaseIOSFeaturePreview_LoadLevel_m322349969 (BaseIOSFeaturePreview_t3055692840 * __this, String_t* ___levelName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
