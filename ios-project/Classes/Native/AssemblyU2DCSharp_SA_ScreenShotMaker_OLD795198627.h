﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen2086042545.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA_ScreenShotMaker_OLD
struct  SA_ScreenShotMaker_OLD_t795198627  : public SA_Singleton_OLD_1_t2086042545
{
public:
	// System.Action`1<UnityEngine.Texture2D> SA_ScreenShotMaker_OLD::OnScreenshotReady
	Action_1_t3344795111 * ___OnScreenshotReady_4;

public:
	inline static int32_t get_offset_of_OnScreenshotReady_4() { return static_cast<int32_t>(offsetof(SA_ScreenShotMaker_OLD_t795198627, ___OnScreenshotReady_4)); }
	inline Action_1_t3344795111 * get_OnScreenshotReady_4() const { return ___OnScreenshotReady_4; }
	inline Action_1_t3344795111 ** get_address_of_OnScreenshotReady_4() { return &___OnScreenshotReady_4; }
	inline void set_OnScreenshotReady_4(Action_1_t3344795111 * value)
	{
		___OnScreenshotReady_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnScreenshotReady_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
