﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Converters.IXmlNode
struct IXmlNode_t1152344546;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlNodeConverter/<SerializeNode>c__AnonStorey1A
struct  U3CSerializeNodeU3Ec__AnonStorey1A_t1249742045  : public Il2CppObject
{
public:
	// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeConverter/<SerializeNode>c__AnonStorey1A::node
	Il2CppObject * ___node_0;

public:
	inline static int32_t get_offset_of_node_0() { return static_cast<int32_t>(offsetof(U3CSerializeNodeU3Ec__AnonStorey1A_t1249742045, ___node_0)); }
	inline Il2CppObject * get_node_0() const { return ___node_0; }
	inline Il2CppObject ** get_address_of_node_0() { return &___node_0; }
	inline void set_node_0(Il2CppObject * value)
	{
		___node_0 = value;
		Il2CppCodeGenWriteBarrier(&___node_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
