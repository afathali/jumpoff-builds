﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// System.Type
struct Type_t;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2570160834;
// System.Reflection.FieldInfo
struct FieldInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Reflection_FieldInfo255040150.h"

// System.Collections.Generic.IList`1<System.Object> Newtonsoft.Json.Utilities.EnumUtils::GetValues(System.Type)
extern "C"  Il2CppObject* EnumUtils_GetValues_m294859823 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.String> Newtonsoft.Json.Utilities.EnumUtils::GetNames(System.Type)
extern "C"  Il2CppObject* EnumUtils_GetNames_m4272741915 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.EnumUtils::<GetValues>m__F3(System.Reflection.FieldInfo)
extern "C"  bool EnumUtils_U3CGetValuesU3Em__F3_m2970079480 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___field0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.EnumUtils::<GetNames>m__F4(System.Reflection.FieldInfo)
extern "C"  bool EnumUtils_U3CGetNamesU3Em__F4_m1457105609 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___field0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
