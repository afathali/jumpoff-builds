﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSSocialUseExample
struct IOSSocialUseExample_t1825493211;
// IOSImagePickResult
struct IOSImagePickResult_t1671334394;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// SA.Common.Models.Result
struct Result_t4287219743;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSImagePickResult1671334394.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"

// System.Void IOSSocialUseExample::.ctor()
extern "C"  void IOSSocialUseExample__ctor_m4022354630 (IOSSocialUseExample_t1825493211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::Awake()
extern "C"  void IOSSocialUseExample_Awake_m1370340669 (IOSSocialUseExample_t1825493211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::InitStyles()
extern "C"  void IOSSocialUseExample_InitStyles_m1215834804 (IOSSocialUseExample_t1825493211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::OnGUI()
extern "C"  void IOSSocialUseExample_OnGUI_m3907727746 (IOSSocialUseExample_t1825493211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::OnPostImageInstagram(IOSImagePickResult)
extern "C"  void IOSSocialUseExample_OnPostImageInstagram_m3701560318 (IOSSocialUseExample_t1825493211 * __this, IOSImagePickResult_t1671334394 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IOSSocialUseExample::PostScreenshotInstagram()
extern "C"  Il2CppObject * IOSSocialUseExample_PostScreenshotInstagram_m2868778740 (IOSSocialUseExample_t1825493211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IOSSocialUseExample::PostScreenshot()
extern "C"  Il2CppObject * IOSSocialUseExample_PostScreenshot_m2505883350 (IOSSocialUseExample_t1825493211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IOSSocialUseExample::PostTwitterScreenshot()
extern "C"  Il2CppObject * IOSSocialUseExample_PostTwitterScreenshot_m2080883575 (IOSSocialUseExample_t1825493211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IOSSocialUseExample::PostFBScreenshot()
extern "C"  Il2CppObject * IOSSocialUseExample_PostFBScreenshot_m3671317994 (IOSSocialUseExample_t1825493211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::HandleOnInstagramPostResult(SA.Common.Models.Result)
extern "C"  void IOSSocialUseExample_HandleOnInstagramPostResult_m4139929374 (IOSSocialUseExample_t1825493211 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::HandleOnTwitterPostResult(SA.Common.Models.Result)
extern "C"  void IOSSocialUseExample_HandleOnTwitterPostResult_m160861343 (IOSSocialUseExample_t1825493211 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::HandleOnFacebookPostResult(SA.Common.Models.Result)
extern "C"  void IOSSocialUseExample_HandleOnFacebookPostResult_m1049668606 (IOSSocialUseExample_t1825493211 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample::OnMailResult(SA.Common.Models.Result)
extern "C"  void IOSSocialUseExample_OnMailResult_m1498615685 (IOSSocialUseExample_t1825493211 * __this, Result_t4287219743 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
