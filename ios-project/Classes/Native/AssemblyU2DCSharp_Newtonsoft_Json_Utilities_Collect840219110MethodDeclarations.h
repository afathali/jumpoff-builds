﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>
struct CollectionWrapper_1_t840219110;
// System.Collections.IList
struct IList_t3321498491;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Array
struct Il2CppArray;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"

// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::.ctor(System.Collections.IList)
extern "C"  void CollectionWrapper_1__ctor_m730688311_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___list0, const MethodInfo* method);
#define CollectionWrapper_1__ctor_m730688311(__this, ___list0, method) ((  void (*) (CollectionWrapper_1_t840219110 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1__ctor_m730688311_gshared)(__this, ___list0, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::.ctor(System.Collections.Generic.ICollection`1<T>)
extern "C"  void CollectionWrapper_1__ctor_m1250166947_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define CollectionWrapper_1__ctor_m1250166947(__this, ___list0, method) ((  void (*) (CollectionWrapper_1_t840219110 *, Il2CppObject*, const MethodInfo*))CollectionWrapper_1__ctor_m1250166947_gshared)(__this, ___list0, method)
// System.Collections.IEnumerator Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * CollectionWrapper_1_System_Collections_IEnumerable_GetEnumerator_m1635240117_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IEnumerable_GetEnumerator_m1635240117(__this, method) ((  Il2CppObject * (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))CollectionWrapper_1_System_Collections_IEnumerable_GetEnumerator_m1635240117_gshared)(__this, method)
// System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t CollectionWrapper_1_System_Collections_IList_Add_m3251969948_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_Add_m3251969948(__this, ___value0, method) ((  int32_t (*) (CollectionWrapper_1_t840219110 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_Add_m3251969948_gshared)(__this, ___value0, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C"  bool CollectionWrapper_1_System_Collections_IList_Contains_m4137645066_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_Contains_m4137645066(__this, ___value0, method) ((  bool (*) (CollectionWrapper_1_t840219110 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_Contains_m4137645066_gshared)(__this, ___value0, method)
// System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t CollectionWrapper_1_System_Collections_IList_IndexOf_m1783924686_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_IndexOf_m1783924686(__this, ___value0, method) ((  int32_t (*) (CollectionWrapper_1_t840219110 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_IndexOf_m1783924686_gshared)(__this, ___value0, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void CollectionWrapper_1_System_Collections_IList_RemoveAt_m828926619_gshared (CollectionWrapper_1_t840219110 * __this, int32_t ___index0, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_RemoveAt_m828926619(__this, ___index0, method) ((  void (*) (CollectionWrapper_1_t840219110 *, int32_t, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_RemoveAt_m828926619_gshared)(__this, ___index0, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void CollectionWrapper_1_System_Collections_IList_Insert_m3242781493_gshared (CollectionWrapper_1_t840219110 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_Insert_m3242781493(__this, ___index0, ___value1, method) ((  void (*) (CollectionWrapper_1_t840219110 *, int32_t, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_Insert_m3242781493_gshared)(__this, ___index0, ___value1, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool CollectionWrapper_1_System_Collections_IList_get_IsFixedSize_m4116392417_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_get_IsFixedSize_m4116392417(__this, method) ((  bool (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_get_IsFixedSize_m4116392417_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C"  void CollectionWrapper_1_System_Collections_IList_Remove_m2883738989_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_Remove_m2883738989(__this, ___value0, method) ((  void (*) (CollectionWrapper_1_t840219110 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_Remove_m2883738989_gshared)(__this, ___value0, method)
// System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * CollectionWrapper_1_System_Collections_IList_get_Item_m632945545_gshared (CollectionWrapper_1_t840219110 * __this, int32_t ___index0, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_get_Item_m632945545(__this, ___index0, method) ((  Il2CppObject * (*) (CollectionWrapper_1_t840219110 *, int32_t, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_get_Item_m632945545_gshared)(__this, ___index0, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void CollectionWrapper_1_System_Collections_IList_set_Item_m2724715840_gshared (CollectionWrapper_1_t840219110 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_set_Item_m2724715840(__this, ___index0, ___value1, method) ((  void (*) (CollectionWrapper_1_t840219110 *, int32_t, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_set_Item_m2724715840_gshared)(__this, ___index0, ___value1, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void CollectionWrapper_1_System_Collections_ICollection_CopyTo_m1279341670_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_ICollection_CopyTo_m1279341670(__this, ___array0, ___arrayIndex1, method) ((  void (*) (CollectionWrapper_1_t840219110 *, Il2CppArray *, int32_t, const MethodInfo*))CollectionWrapper_1_System_Collections_ICollection_CopyTo_m1279341670_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool CollectionWrapper_1_System_Collections_ICollection_get_IsSynchronized_m3682572004_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_ICollection_get_IsSynchronized_m3682572004(__this, method) ((  bool (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))CollectionWrapper_1_System_Collections_ICollection_get_IsSynchronized_m3682572004_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * CollectionWrapper_1_System_Collections_ICollection_get_SyncRoot_m2090561030_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_ICollection_get_SyncRoot_m2090561030(__this, method) ((  Il2CppObject * (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))CollectionWrapper_1_System_Collections_ICollection_get_SyncRoot_m2090561030_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Add(T)
extern "C"  void CollectionWrapper_1_Add_m3694381833_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define CollectionWrapper_1_Add_m3694381833(__this, ___item0, method) ((  void (*) (CollectionWrapper_1_t840219110 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_Add_m3694381833_gshared)(__this, ___item0, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Clear()
extern "C"  void CollectionWrapper_1_Clear_m1906909381_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method);
#define CollectionWrapper_1_Clear_m1906909381(__this, method) ((  void (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))CollectionWrapper_1_Clear_m1906909381_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Contains(T)
extern "C"  bool CollectionWrapper_1_Contains_m286598843_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define CollectionWrapper_1_Contains_m286598843(__this, ___item0, method) ((  bool (*) (CollectionWrapper_1_t840219110 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_Contains_m286598843_gshared)(__this, ___item0, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void CollectionWrapper_1_CopyTo_m3640586785_gshared (CollectionWrapper_1_t840219110 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define CollectionWrapper_1_CopyTo_m3640586785(__this, ___array0, ___arrayIndex1, method) ((  void (*) (CollectionWrapper_1_t840219110 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))CollectionWrapper_1_CopyTo_m3640586785_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_Count()
extern "C"  int32_t CollectionWrapper_1_get_Count_m2582336668_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method);
#define CollectionWrapper_1_get_Count_m2582336668(__this, method) ((  int32_t (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))CollectionWrapper_1_get_Count_m2582336668_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_IsReadOnly()
extern "C"  bool CollectionWrapper_1_get_IsReadOnly_m1401533917_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method);
#define CollectionWrapper_1_get_IsReadOnly_m1401533917(__this, method) ((  bool (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))CollectionWrapper_1_get_IsReadOnly_m1401533917_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Remove(T)
extern "C"  bool CollectionWrapper_1_Remove_m673209964_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define CollectionWrapper_1_Remove_m673209964(__this, ___item0, method) ((  bool (*) (CollectionWrapper_1_t840219110 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_Remove_m673209964_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* CollectionWrapper_1_GetEnumerator_m1507375274_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method);
#define CollectionWrapper_1_GetEnumerator_m1507375274(__this, method) ((  Il2CppObject* (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))CollectionWrapper_1_GetEnumerator_m1507375274_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::IsGenericCollection()
extern "C"  bool CollectionWrapper_1_IsGenericCollection_m3709139545_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method);
#define CollectionWrapper_1_IsGenericCollection_m3709139545(__this, method) ((  bool (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))CollectionWrapper_1_IsGenericCollection_m3709139545_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::VerifyValueType(System.Object)
extern "C"  void CollectionWrapper_1_VerifyValueType_m524701668_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionWrapper_1_VerifyValueType_m524701668(__this /* static, unused */, ___value0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_VerifyValueType_m524701668_gshared)(__this /* static, unused */, ___value0, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::IsCompatibleObject(System.Object)
extern "C"  bool CollectionWrapper_1_IsCompatibleObject_m3337514079_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionWrapper_1_IsCompatibleObject_m3337514079(__this /* static, unused */, ___value0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_IsCompatibleObject_m3337514079_gshared)(__this /* static, unused */, ___value0, method)
// System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_UnderlyingCollection()
extern "C"  Il2CppObject * CollectionWrapper_1_get_UnderlyingCollection_m2218800867_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method);
#define CollectionWrapper_1_get_UnderlyingCollection_m2218800867(__this, method) ((  Il2CppObject * (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))CollectionWrapper_1_get_UnderlyingCollection_m2218800867_gshared)(__this, method)
