﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>
struct Dictionary_2_t1334805305;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2654830007.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23387117823.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3888930488_gshared (Enumerator_t2654830007 * __this, Dictionary_2_t1334805305 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3888930488(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2654830007 *, Dictionary_2_t1334805305 *, const MethodInfo*))Enumerator__ctor_m3888930488_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m649871021_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m649871021(__this, method) ((  Il2CppObject * (*) (Enumerator_t2654830007 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m649871021_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m593851829_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m593851829(__this, method) ((  void (*) (Enumerator_t2654830007 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m593851829_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m218337734_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m218337734(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2654830007 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m218337734_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2021025471_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2021025471(__this, method) ((  Il2CppObject * (*) (Enumerator_t2654830007 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2021025471_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2671660943_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2671660943(__this, method) ((  Il2CppObject * (*) (Enumerator_t2654830007 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2671660943_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m657838813_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m657838813(__this, method) ((  bool (*) (Enumerator_t2654830007 *, const MethodInfo*))Enumerator_MoveNext_m657838813_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Current()
extern "C"  KeyValuePair_2_t3387117823  Enumerator_get_Current_m954143681_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m954143681(__this, method) ((  KeyValuePair_2_t3387117823  (*) (Enumerator_t2654830007 *, const MethodInfo*))Enumerator_get_Current_m954143681_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1574894968_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1574894968(__this, method) ((  Il2CppObject * (*) (Enumerator_t2654830007 *, const MethodInfo*))Enumerator_get_CurrentKey_m1574894968_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m1369392376_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1369392376(__this, method) ((  int32_t (*) (Enumerator_t2654830007 *, const MethodInfo*))Enumerator_get_CurrentValue_m1369392376_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::Reset()
extern "C"  void Enumerator_Reset_m367248862_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method);
#define Enumerator_Reset_m367248862(__this, method) ((  void (*) (Enumerator_t2654830007 *, const MethodInfo*))Enumerator_Reset_m367248862_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1102849887_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1102849887(__this, method) ((  void (*) (Enumerator_t2654830007 *, const MethodInfo*))Enumerator_VerifyState_m1102849887_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m231503237_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m231503237(__this, method) ((  void (*) (Enumerator_t2654830007 *, const MethodInfo*))Enumerator_VerifyCurrent_m231503237_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::Dispose()
extern "C"  void Enumerator_Dispose_m3267139496_gshared (Enumerator_t2654830007 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3267139496(__this, method) ((  void (*) (Enumerator_t2654830007 *, const MethodInfo*))Enumerator_Dispose_m3267139496_gshared)(__this, method)
