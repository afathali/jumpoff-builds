﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Data.Json/Parser
struct Parser_t527802111;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Data_Json_P458804911.h"

// System.Void SA.Common.Data.Json/Parser::.ctor(System.String)
extern "C"  void Parser__ctor_m2145299164 (Parser_t527802111 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Data.Json/Parser::Parse(System.String)
extern "C"  Il2CppObject * Parser_Parse_m2279268558 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Data.Json/Parser::Dispose()
extern "C"  void Parser_Dispose_m2172192927 (Parser_t527802111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> SA.Common.Data.Json/Parser::ParseObject()
extern "C"  Dictionary_2_t309261261 * Parser_ParseObject_m1457893454 (Parser_t527802111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> SA.Common.Data.Json/Parser::ParseArray()
extern "C"  List_1_t2058570427 * Parser_ParseArray_m4191322319 (Parser_t527802111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Data.Json/Parser::ParseValue()
extern "C"  Il2CppObject * Parser_ParseValue_m7729699 (Parser_t527802111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Data.Json/Parser::ParseByToken(SA.Common.Data.Json/Parser/TOKEN)
extern "C"  Il2CppObject * Parser_ParseByToken_m2425380245 (Parser_t527802111 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA.Common.Data.Json/Parser::ParseString()
extern "C"  String_t* Parser_ParseString_m2907983701 (Parser_t527802111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Data.Json/Parser::ParseNumber()
extern "C"  Il2CppObject * Parser_ParseNumber_m1310783787 (Parser_t527802111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Data.Json/Parser::EatWhitespace()
extern "C"  void Parser_EatWhitespace_m3453111 (Parser_t527802111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char SA.Common.Data.Json/Parser::get_PeekChar()
extern "C"  Il2CppChar Parser_get_PeekChar_m2266531840 (Parser_t527802111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char SA.Common.Data.Json/Parser::get_NextChar()
extern "C"  Il2CppChar Parser_get_NextChar_m2046224414 (Parser_t527802111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA.Common.Data.Json/Parser::get_NextWord()
extern "C"  String_t* Parser_get_NextWord_m974568501 (Parser_t527802111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SA.Common.Data.Json/Parser/TOKEN SA.Common.Data.Json/Parser::get_NextToken()
extern "C"  int32_t Parser_get_NextToken_m4287462303 (Parser_t527802111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
