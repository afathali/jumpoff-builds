﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<IOSImagePickResult>
struct Action_1_t1473133776;
// System.Action`1<SA.Common.Models.Result>
struct Action_1_t4089019125;
// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si3327673795.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSCamera
struct  IOSCamera_t2845108690  : public Singleton_1_t3327673795
{
public:
	// System.Boolean IOSCamera::_IsWaitngForResponce
	bool ____IsWaitngForResponce_4;
	// System.Boolean IOSCamera::_IsInitialized
	bool ____IsInitialized_5;

public:
	inline static int32_t get_offset_of__IsWaitngForResponce_4() { return static_cast<int32_t>(offsetof(IOSCamera_t2845108690, ____IsWaitngForResponce_4)); }
	inline bool get__IsWaitngForResponce_4() const { return ____IsWaitngForResponce_4; }
	inline bool* get_address_of__IsWaitngForResponce_4() { return &____IsWaitngForResponce_4; }
	inline void set__IsWaitngForResponce_4(bool value)
	{
		____IsWaitngForResponce_4 = value;
	}

	inline static int32_t get_offset_of__IsInitialized_5() { return static_cast<int32_t>(offsetof(IOSCamera_t2845108690, ____IsInitialized_5)); }
	inline bool get__IsInitialized_5() const { return ____IsInitialized_5; }
	inline bool* get_address_of__IsInitialized_5() { return &____IsInitialized_5; }
	inline void set__IsInitialized_5(bool value)
	{
		____IsInitialized_5 = value;
	}
};

struct IOSCamera_t2845108690_StaticFields
{
public:
	// System.Action`1<IOSImagePickResult> IOSCamera::OnImagePicked
	Action_1_t1473133776 * ___OnImagePicked_6;
	// System.Action`1<SA.Common.Models.Result> IOSCamera::OnImageSaved
	Action_1_t4089019125 * ___OnImageSaved_7;
	// System.Action`1<System.String> IOSCamera::OnVideoPathPicked
	Action_1_t1831019615 * ___OnVideoPathPicked_8;
	// System.Action`1<IOSImagePickResult> IOSCamera::<>f__am$cache5
	Action_1_t1473133776 * ___U3CU3Ef__amU24cache5_9;
	// System.Action`1<SA.Common.Models.Result> IOSCamera::<>f__am$cache6
	Action_1_t4089019125 * ___U3CU3Ef__amU24cache6_10;
	// System.Action`1<System.String> IOSCamera::<>f__am$cache7
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache7_11;

public:
	inline static int32_t get_offset_of_OnImagePicked_6() { return static_cast<int32_t>(offsetof(IOSCamera_t2845108690_StaticFields, ___OnImagePicked_6)); }
	inline Action_1_t1473133776 * get_OnImagePicked_6() const { return ___OnImagePicked_6; }
	inline Action_1_t1473133776 ** get_address_of_OnImagePicked_6() { return &___OnImagePicked_6; }
	inline void set_OnImagePicked_6(Action_1_t1473133776 * value)
	{
		___OnImagePicked_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnImagePicked_6, value);
	}

	inline static int32_t get_offset_of_OnImageSaved_7() { return static_cast<int32_t>(offsetof(IOSCamera_t2845108690_StaticFields, ___OnImageSaved_7)); }
	inline Action_1_t4089019125 * get_OnImageSaved_7() const { return ___OnImageSaved_7; }
	inline Action_1_t4089019125 ** get_address_of_OnImageSaved_7() { return &___OnImageSaved_7; }
	inline void set_OnImageSaved_7(Action_1_t4089019125 * value)
	{
		___OnImageSaved_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnImageSaved_7, value);
	}

	inline static int32_t get_offset_of_OnVideoPathPicked_8() { return static_cast<int32_t>(offsetof(IOSCamera_t2845108690_StaticFields, ___OnVideoPathPicked_8)); }
	inline Action_1_t1831019615 * get_OnVideoPathPicked_8() const { return ___OnVideoPathPicked_8; }
	inline Action_1_t1831019615 ** get_address_of_OnVideoPathPicked_8() { return &___OnVideoPathPicked_8; }
	inline void set_OnVideoPathPicked_8(Action_1_t1831019615 * value)
	{
		___OnVideoPathPicked_8 = value;
		Il2CppCodeGenWriteBarrier(&___OnVideoPathPicked_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_9() { return static_cast<int32_t>(offsetof(IOSCamera_t2845108690_StaticFields, ___U3CU3Ef__amU24cache5_9)); }
	inline Action_1_t1473133776 * get_U3CU3Ef__amU24cache5_9() const { return ___U3CU3Ef__amU24cache5_9; }
	inline Action_1_t1473133776 ** get_address_of_U3CU3Ef__amU24cache5_9() { return &___U3CU3Ef__amU24cache5_9; }
	inline void set_U3CU3Ef__amU24cache5_9(Action_1_t1473133776 * value)
	{
		___U3CU3Ef__amU24cache5_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_10() { return static_cast<int32_t>(offsetof(IOSCamera_t2845108690_StaticFields, ___U3CU3Ef__amU24cache6_10)); }
	inline Action_1_t4089019125 * get_U3CU3Ef__amU24cache6_10() const { return ___U3CU3Ef__amU24cache6_10; }
	inline Action_1_t4089019125 ** get_address_of_U3CU3Ef__amU24cache6_10() { return &___U3CU3Ef__amU24cache6_10; }
	inline void set_U3CU3Ef__amU24cache6_10(Action_1_t4089019125 * value)
	{
		___U3CU3Ef__amU24cache6_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_11() { return static_cast<int32_t>(offsetof(IOSCamera_t2845108690_StaticFields, ___U3CU3Ef__amU24cache7_11)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache7_11() const { return ___U3CU3Ef__amU24cache7_11; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache7_11() { return &___U3CU3Ef__amU24cache7_11; }
	inline void set_U3CU3Ef__amU24cache7_11(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache7_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
