﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__IteratorB
struct U3CSaveScreenshotU3Ec__IteratorB_t3554841484;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__IteratorB::.ctor()
extern "C"  void U3CSaveScreenshotU3Ec__IteratorB__ctor_m1219519095 (U3CSaveScreenshotU3Ec__IteratorB_t3554841484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenshotU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3671079125 (U3CSaveScreenshotU3Ec__IteratorB_t3554841484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenshotU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m2764009197 (U3CSaveScreenshotU3Ec__IteratorB_t3554841484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__IteratorB::MoveNext()
extern "C"  bool U3CSaveScreenshotU3Ec__IteratorB_MoveNext_m3101783081 (U3CSaveScreenshotU3Ec__IteratorB_t3554841484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__IteratorB::Dispose()
extern "C"  void U3CSaveScreenshotU3Ec__IteratorB_Dispose_m2578378860 (U3CSaveScreenshotU3Ec__IteratorB_t3554841484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.ScreenshotMaker/<SaveScreenshot>c__IteratorB::Reset()
extern "C"  void U3CSaveScreenshotU3Ec__IteratorB_Reset_m2797330810 (U3CSaveScreenshotU3Ec__IteratorB_t3554841484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
