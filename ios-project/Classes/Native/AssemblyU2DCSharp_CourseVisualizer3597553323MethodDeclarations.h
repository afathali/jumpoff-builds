﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CourseVisualizer
struct CourseVisualizer_t3597553323;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// System.Void CourseVisualizer::.ctor()
extern "C"  void CourseVisualizer__ctor_m3499870920 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::setPlaying(System.Boolean)
extern "C"  void CourseVisualizer_setPlaying_m3295222195 (CourseVisualizer_t3597553323 * __this, bool ___newPlaying0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::OnApplicationPause(System.Boolean)
extern "C"  void CourseVisualizer_OnApplicationPause_m1985575226 (CourseVisualizer_t3597553323 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::getLevelBounds(System.Single&,System.Single&,System.Single&,System.Single&)
extern "C"  void CourseVisualizer_getLevelBounds_m3152450931 (CourseVisualizer_t3597553323 * __this, float* ___minX0, float* ___maxX1, float* ___minY2, float* ___maxY3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::setupIndoor()
extern "C"  void CourseVisualizer_setupIndoor_m3852744426 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::setupFarm()
extern "C"  void CourseVisualizer_setupFarm_m1326433549 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::InstantiateTribune(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void CourseVisualizer_InstantiateTribune_m1666899805 (CourseVisualizer_t3597553323 * __this, Vector3_t2243707580  ___pos0, Quaternion_t4030073918  ___rot1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::Instantiate_fenceFarm(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void CourseVisualizer_Instantiate_fenceFarm_m3531275470 (CourseVisualizer_t3597553323 * __this, Vector3_t2243707580  ___pos0, Quaternion_t4030073918  ___rot1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::Instantiate_fenceShow(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void CourseVisualizer_Instantiate_fenceShow_m1146163545 (CourseVisualizer_t3597553323 * __this, Vector3_t2243707580  ___pos0, Quaternion_t4030073918  ___rot1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::setupShow()
extern "C"  void CourseVisualizer_setupShow_m629206024 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::LoadEnvBanners()
extern "C"  void CourseVisualizer_LoadEnvBanners_m2364920808 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CourseVisualizer::LoadBanners()
extern "C"  Il2CppObject * CourseVisualizer_LoadBanners_m2100196041 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::setupEnv()
extern "C"  void CourseVisualizer_setupEnv_m3914260326 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::Start()
extern "C"  void CourseVisualizer_Start_m3145328076 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::LateInit()
extern "C"  void CourseVisualizer_LateInit_m99832716 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion CourseVisualizer::ConvertRotation(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  CourseVisualizer_ConvertRotation_m2643760258 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::buildTrackRotations()
extern "C"  void CourseVisualizer_buildTrackRotations_m2350842826 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::seek()
extern "C"  void CourseVisualizer_seek_m1033803134 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::FixedUpdate()
extern "C"  void CourseVisualizer_FixedUpdate_m2552904489 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::jumpNow()
extern "C"  void CourseVisualizer_jumpNow_m254048616 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::<Start>m__0()
extern "C"  void CourseVisualizer_U3CStartU3Em__0_m3375706381 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::<Start>m__1(System.Boolean)
extern "C"  void CourseVisualizer_U3CStartU3Em__1_m1172331115 (CourseVisualizer_t3597553323 * __this, bool ___on0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::<Start>m__2()
extern "C"  void CourseVisualizer_U3CStartU3Em__2_m3375706447 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::<Start>m__3(System.Single)
extern "C"  void CourseVisualizer_U3CStartU3Em__3_m981035759 (CourseVisualizer_t3597553323 * __this, float ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer::<Start>m__4()
extern "C"  void CourseVisualizer_U3CStartU3Em__4_m3375706249 (CourseVisualizer_t3597553323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
