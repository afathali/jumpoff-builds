﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_GP_AppStateStatusCodes2689651680.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleCloudResult
struct  GoogleCloudResult_t743628707  : public Il2CppObject
{
public:
	// GP_AppStateStatusCodes GoogleCloudResult::_response
	int32_t ____response_0;
	// System.String GoogleCloudResult::_message
	String_t* ____message_1;
	// System.Int32 GoogleCloudResult::_stateKey
	int32_t ____stateKey_2;
	// System.Byte[] GoogleCloudResult::stateData
	ByteU5BU5D_t3397334013* ___stateData_3;
	// System.Byte[] GoogleCloudResult::serverConflictData
	ByteU5BU5D_t3397334013* ___serverConflictData_4;
	// System.String GoogleCloudResult::resolvedVersion
	String_t* ___resolvedVersion_5;

public:
	inline static int32_t get_offset_of__response_0() { return static_cast<int32_t>(offsetof(GoogleCloudResult_t743628707, ____response_0)); }
	inline int32_t get__response_0() const { return ____response_0; }
	inline int32_t* get_address_of__response_0() { return &____response_0; }
	inline void set__response_0(int32_t value)
	{
		____response_0 = value;
	}

	inline static int32_t get_offset_of__message_1() { return static_cast<int32_t>(offsetof(GoogleCloudResult_t743628707, ____message_1)); }
	inline String_t* get__message_1() const { return ____message_1; }
	inline String_t** get_address_of__message_1() { return &____message_1; }
	inline void set__message_1(String_t* value)
	{
		____message_1 = value;
		Il2CppCodeGenWriteBarrier(&____message_1, value);
	}

	inline static int32_t get_offset_of__stateKey_2() { return static_cast<int32_t>(offsetof(GoogleCloudResult_t743628707, ____stateKey_2)); }
	inline int32_t get__stateKey_2() const { return ____stateKey_2; }
	inline int32_t* get_address_of__stateKey_2() { return &____stateKey_2; }
	inline void set__stateKey_2(int32_t value)
	{
		____stateKey_2 = value;
	}

	inline static int32_t get_offset_of_stateData_3() { return static_cast<int32_t>(offsetof(GoogleCloudResult_t743628707, ___stateData_3)); }
	inline ByteU5BU5D_t3397334013* get_stateData_3() const { return ___stateData_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_stateData_3() { return &___stateData_3; }
	inline void set_stateData_3(ByteU5BU5D_t3397334013* value)
	{
		___stateData_3 = value;
		Il2CppCodeGenWriteBarrier(&___stateData_3, value);
	}

	inline static int32_t get_offset_of_serverConflictData_4() { return static_cast<int32_t>(offsetof(GoogleCloudResult_t743628707, ___serverConflictData_4)); }
	inline ByteU5BU5D_t3397334013* get_serverConflictData_4() const { return ___serverConflictData_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_serverConflictData_4() { return &___serverConflictData_4; }
	inline void set_serverConflictData_4(ByteU5BU5D_t3397334013* value)
	{
		___serverConflictData_4 = value;
		Il2CppCodeGenWriteBarrier(&___serverConflictData_4, value);
	}

	inline static int32_t get_offset_of_resolvedVersion_5() { return static_cast<int32_t>(offsetof(GoogleCloudResult_t743628707, ___resolvedVersion_5)); }
	inline String_t* get_resolvedVersion_5() const { return ___resolvedVersion_5; }
	inline String_t** get_address_of_resolvedVersion_5() { return &___resolvedVersion_5; }
	inline void set_resolvedVersion_5(String_t* value)
	{
		___resolvedVersion_5 = value;
		Il2CppCodeGenWriteBarrier(&___resolvedVersion_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
