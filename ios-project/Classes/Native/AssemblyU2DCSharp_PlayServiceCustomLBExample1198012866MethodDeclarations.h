﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayServiceCustomLBExample
struct PlayServiceCustomLBExample_t1198012866;
// GooglePlayConnectionResult
struct GooglePlayConnectionResult_t2758718724;
// GooglePlayResult
struct GooglePlayResult_t3097469636;
// GP_LeaderboardResult
struct GP_LeaderboardResult_t2034215294;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayConnectionResult2758718724.h"
#include "AssemblyU2DCSharp_GooglePlayResult3097469636.h"
#include "AssemblyU2DCSharp_GP_LeaderboardResult2034215294.h"

// System.Void PlayServiceCustomLBExample::.ctor()
extern "C"  void PlayServiceCustomLBExample__ctor_m4249086099 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::Start()
extern "C"  void PlayServiceCustomLBExample_Start_m1107287503 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::LoadScore()
extern "C"  void PlayServiceCustomLBExample_LoadScore_m2309613481 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::OpenUI()
extern "C"  void PlayServiceCustomLBExample_OpenUI_m12240135 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::ShowGlobal()
extern "C"  void PlayServiceCustomLBExample_ShowGlobal_m3425284255 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::ShowLocal()
extern "C"  void PlayServiceCustomLBExample_ShowLocal_m2787542537 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::ShowAllTime()
extern "C"  void PlayServiceCustomLBExample_ShowAllTime_m2397639568 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::ShowWeek()
extern "C"  void PlayServiceCustomLBExample_ShowWeek_m1596455124 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::ShowDay()
extern "C"  void PlayServiceCustomLBExample_ShowDay_m4013836832 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::ConncetButtonPress()
extern "C"  void PlayServiceCustomLBExample_ConncetButtonPress_m4173330312 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::UpdateScoresDisaplay()
extern "C"  void PlayServiceCustomLBExample_UpdateScoresDisaplay_m2948268270 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::FixedUpdate()
extern "C"  void PlayServiceCustomLBExample_FixedUpdate_m481653586 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::SubmitScore()
extern "C"  void PlayServiceCustomLBExample_SubmitScore_m359316623 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::OnPlayerDisconnected()
extern "C"  void PlayServiceCustomLBExample_OnPlayerDisconnected_m455314654 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::OnPlayerConnected()
extern "C"  void PlayServiceCustomLBExample_OnPlayerConnected_m1530899634 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::OnConnectionResult(GooglePlayConnectionResult)
extern "C"  void PlayServiceCustomLBExample_OnConnectionResult_m2611616655 (PlayServiceCustomLBExample_t1198012866 * __this, GooglePlayConnectionResult_t2758718724 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::ActionScoreRequestReceived(GooglePlayResult)
extern "C"  void PlayServiceCustomLBExample_ActionScoreRequestReceived_m2899182235 (PlayServiceCustomLBExample_t1198012866 * __this, GooglePlayResult_t3097469636 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::OnScoreSbumitted(GP_LeaderboardResult)
extern "C"  void PlayServiceCustomLBExample_OnScoreSbumitted_m3671651495 (PlayServiceCustomLBExample_t1198012866 * __this, GP_LeaderboardResult_t2034215294 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceCustomLBExample::OnDestroy()
extern "C"  void PlayServiceCustomLBExample_OnDestroy_m3335841960 (PlayServiceCustomLBExample_t1198012866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
