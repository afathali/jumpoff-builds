﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TvOsCloudExample
struct TvOsCloudExample_t1625417505;
// iCloudData
struct iCloudData_t3080637488;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iCloudData3080637488.h"

// System.Void TvOsCloudExample::.ctor()
extern "C"  void TvOsCloudExample__ctor_m1384387766 (TvOsCloudExample_t1625417505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TvOsCloudExample::Start()
extern "C"  void TvOsCloudExample_Start_m1785053202 (TvOsCloudExample_t1625417505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TvOsCloudExample::OnCloudDataReceivedAction(iCloudData)
extern "C"  void TvOsCloudExample_OnCloudDataReceivedAction_m3322197275 (TvOsCloudExample_t1625417505 * __this, iCloudData_t3080637488 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
