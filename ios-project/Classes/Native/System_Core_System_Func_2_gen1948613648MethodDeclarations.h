﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2825504181MethodDeclarations.h"

// System.Void System.Func`2<System.Reflection.ICustomAttributeProvider,System.Type>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1761970644(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1948613648 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3134197304_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Reflection.ICustomAttributeProvider,System.Type>::Invoke(T)
#define Func_2_Invoke_m237153424(__this, ___arg10, method) ((  Type_t * (*) (Func_2_t1948613648 *, Il2CppObject *, const MethodInfo*))Func_2_Invoke_m815118996_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Reflection.ICustomAttributeProvider,System.Type>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m619863391(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1948613648 *, Il2CppObject *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4034295761_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Reflection.ICustomAttributeProvider,System.Type>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1866158474(__this, ___result0, method) ((  Type_t * (*) (Func_2_t1948613648 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1674435418_gshared)(__this, ___result0, method)
