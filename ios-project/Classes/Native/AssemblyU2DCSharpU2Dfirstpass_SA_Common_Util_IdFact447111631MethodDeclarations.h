﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Util.IdFactory
struct IdFactory_t447111631;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.Common.Util.IdFactory::.ctor()
extern "C"  void IdFactory__ctor_m1566963585 (IdFactory_t447111631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SA.Common.Util.IdFactory::get_NextId()
extern "C"  int32_t IdFactory_get_NextId_m3560925746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA.Common.Util.IdFactory::get_RandomString()
extern "C"  String_t* IdFactory_get_RandomString_m2255678543 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
