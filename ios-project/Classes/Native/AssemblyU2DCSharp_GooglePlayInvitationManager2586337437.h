﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<GP_Invite>
struct Action_1_t428728469;
// System.Action`1<System.Collections.Generic.List`1<GP_Invite>>
struct Action_1_t4092816897;
// System.Action`1<AN_InvitationInboxCloseResult>
struct Action_1_t1085927992;
// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3877181355.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayInvitationManager
struct  GooglePlayInvitationManager_t2586337437  : public SA_Singleton_OLD_1_t3877181355
{
public:

public:
};

struct GooglePlayInvitationManager_t2586337437_StaticFields
{
public:
	// System.Action`1<GP_Invite> GooglePlayInvitationManager::ActionInvitationReceived
	Action_1_t428728469 * ___ActionInvitationReceived_4;
	// System.Action`1<GP_Invite> GooglePlayInvitationManager::ActionInvitationAccepted
	Action_1_t428728469 * ___ActionInvitationAccepted_5;
	// System.Action`1<System.Collections.Generic.List`1<GP_Invite>> GooglePlayInvitationManager::ActionInvitationsListLoaded
	Action_1_t4092816897 * ___ActionInvitationsListLoaded_6;
	// System.Action`1<AN_InvitationInboxCloseResult> GooglePlayInvitationManager::ActionInvitationInboxClosed
	Action_1_t1085927992 * ___ActionInvitationInboxClosed_7;
	// System.Action`1<System.String> GooglePlayInvitationManager::ActionInvitationRemoved
	Action_1_t1831019615 * ___ActionInvitationRemoved_8;
	// System.Action`1<GP_Invite> GooglePlayInvitationManager::<>f__am$cache5
	Action_1_t428728469 * ___U3CU3Ef__amU24cache5_9;
	// System.Action`1<GP_Invite> GooglePlayInvitationManager::<>f__am$cache6
	Action_1_t428728469 * ___U3CU3Ef__amU24cache6_10;
	// System.Action`1<System.Collections.Generic.List`1<GP_Invite>> GooglePlayInvitationManager::<>f__am$cache7
	Action_1_t4092816897 * ___U3CU3Ef__amU24cache7_11;
	// System.Action`1<AN_InvitationInboxCloseResult> GooglePlayInvitationManager::<>f__am$cache8
	Action_1_t1085927992 * ___U3CU3Ef__amU24cache8_12;
	// System.Action`1<System.String> GooglePlayInvitationManager::<>f__am$cache9
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache9_13;

public:
	inline static int32_t get_offset_of_ActionInvitationReceived_4() { return static_cast<int32_t>(offsetof(GooglePlayInvitationManager_t2586337437_StaticFields, ___ActionInvitationReceived_4)); }
	inline Action_1_t428728469 * get_ActionInvitationReceived_4() const { return ___ActionInvitationReceived_4; }
	inline Action_1_t428728469 ** get_address_of_ActionInvitationReceived_4() { return &___ActionInvitationReceived_4; }
	inline void set_ActionInvitationReceived_4(Action_1_t428728469 * value)
	{
		___ActionInvitationReceived_4 = value;
		Il2CppCodeGenWriteBarrier(&___ActionInvitationReceived_4, value);
	}

	inline static int32_t get_offset_of_ActionInvitationAccepted_5() { return static_cast<int32_t>(offsetof(GooglePlayInvitationManager_t2586337437_StaticFields, ___ActionInvitationAccepted_5)); }
	inline Action_1_t428728469 * get_ActionInvitationAccepted_5() const { return ___ActionInvitationAccepted_5; }
	inline Action_1_t428728469 ** get_address_of_ActionInvitationAccepted_5() { return &___ActionInvitationAccepted_5; }
	inline void set_ActionInvitationAccepted_5(Action_1_t428728469 * value)
	{
		___ActionInvitationAccepted_5 = value;
		Il2CppCodeGenWriteBarrier(&___ActionInvitationAccepted_5, value);
	}

	inline static int32_t get_offset_of_ActionInvitationsListLoaded_6() { return static_cast<int32_t>(offsetof(GooglePlayInvitationManager_t2586337437_StaticFields, ___ActionInvitationsListLoaded_6)); }
	inline Action_1_t4092816897 * get_ActionInvitationsListLoaded_6() const { return ___ActionInvitationsListLoaded_6; }
	inline Action_1_t4092816897 ** get_address_of_ActionInvitationsListLoaded_6() { return &___ActionInvitationsListLoaded_6; }
	inline void set_ActionInvitationsListLoaded_6(Action_1_t4092816897 * value)
	{
		___ActionInvitationsListLoaded_6 = value;
		Il2CppCodeGenWriteBarrier(&___ActionInvitationsListLoaded_6, value);
	}

	inline static int32_t get_offset_of_ActionInvitationInboxClosed_7() { return static_cast<int32_t>(offsetof(GooglePlayInvitationManager_t2586337437_StaticFields, ___ActionInvitationInboxClosed_7)); }
	inline Action_1_t1085927992 * get_ActionInvitationInboxClosed_7() const { return ___ActionInvitationInboxClosed_7; }
	inline Action_1_t1085927992 ** get_address_of_ActionInvitationInboxClosed_7() { return &___ActionInvitationInboxClosed_7; }
	inline void set_ActionInvitationInboxClosed_7(Action_1_t1085927992 * value)
	{
		___ActionInvitationInboxClosed_7 = value;
		Il2CppCodeGenWriteBarrier(&___ActionInvitationInboxClosed_7, value);
	}

	inline static int32_t get_offset_of_ActionInvitationRemoved_8() { return static_cast<int32_t>(offsetof(GooglePlayInvitationManager_t2586337437_StaticFields, ___ActionInvitationRemoved_8)); }
	inline Action_1_t1831019615 * get_ActionInvitationRemoved_8() const { return ___ActionInvitationRemoved_8; }
	inline Action_1_t1831019615 ** get_address_of_ActionInvitationRemoved_8() { return &___ActionInvitationRemoved_8; }
	inline void set_ActionInvitationRemoved_8(Action_1_t1831019615 * value)
	{
		___ActionInvitationRemoved_8 = value;
		Il2CppCodeGenWriteBarrier(&___ActionInvitationRemoved_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_9() { return static_cast<int32_t>(offsetof(GooglePlayInvitationManager_t2586337437_StaticFields, ___U3CU3Ef__amU24cache5_9)); }
	inline Action_1_t428728469 * get_U3CU3Ef__amU24cache5_9() const { return ___U3CU3Ef__amU24cache5_9; }
	inline Action_1_t428728469 ** get_address_of_U3CU3Ef__amU24cache5_9() { return &___U3CU3Ef__amU24cache5_9; }
	inline void set_U3CU3Ef__amU24cache5_9(Action_1_t428728469 * value)
	{
		___U3CU3Ef__amU24cache5_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_10() { return static_cast<int32_t>(offsetof(GooglePlayInvitationManager_t2586337437_StaticFields, ___U3CU3Ef__amU24cache6_10)); }
	inline Action_1_t428728469 * get_U3CU3Ef__amU24cache6_10() const { return ___U3CU3Ef__amU24cache6_10; }
	inline Action_1_t428728469 ** get_address_of_U3CU3Ef__amU24cache6_10() { return &___U3CU3Ef__amU24cache6_10; }
	inline void set_U3CU3Ef__amU24cache6_10(Action_1_t428728469 * value)
	{
		___U3CU3Ef__amU24cache6_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_11() { return static_cast<int32_t>(offsetof(GooglePlayInvitationManager_t2586337437_StaticFields, ___U3CU3Ef__amU24cache7_11)); }
	inline Action_1_t4092816897 * get_U3CU3Ef__amU24cache7_11() const { return ___U3CU3Ef__amU24cache7_11; }
	inline Action_1_t4092816897 ** get_address_of_U3CU3Ef__amU24cache7_11() { return &___U3CU3Ef__amU24cache7_11; }
	inline void set_U3CU3Ef__amU24cache7_11(Action_1_t4092816897 * value)
	{
		___U3CU3Ef__amU24cache7_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_12() { return static_cast<int32_t>(offsetof(GooglePlayInvitationManager_t2586337437_StaticFields, ___U3CU3Ef__amU24cache8_12)); }
	inline Action_1_t1085927992 * get_U3CU3Ef__amU24cache8_12() const { return ___U3CU3Ef__amU24cache8_12; }
	inline Action_1_t1085927992 ** get_address_of_U3CU3Ef__amU24cache8_12() { return &___U3CU3Ef__amU24cache8_12; }
	inline void set_U3CU3Ef__amU24cache8_12(Action_1_t1085927992 * value)
	{
		___U3CU3Ef__amU24cache8_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_13() { return static_cast<int32_t>(offsetof(GooglePlayInvitationManager_t2586337437_StaticFields, ___U3CU3Ef__amU24cache9_13)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache9_13() const { return ___U3CU3Ef__amU24cache9_13; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache9_13() { return &___U3CU3Ef__amU24cache9_13; }
	inline void set_U3CU3Ef__amU24cache9_13(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache9_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
