﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenCapture
struct ScreenCapture_t1787384414;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CaptureMethod3448917501.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ScreenCapture::.ctor()
extern "C"  void ScreenCapture__ctor_m2696116267 (ScreenCapture_t1787384414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenCapture::.cctor()
extern "C"  void ScreenCapture__cctor_m636135068 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenCapture::SaveScreenshot(CaptureMethod,System.String)
extern "C"  void ScreenCapture_SaveScreenshot_m2397276623 (ScreenCapture_t1787384414 * __this, int32_t ___method0, String_t* ___filePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScreenCapture::SaveScreenshot_ReadPixelsAsynch(System.String)
extern "C"  Il2CppObject * ScreenCapture_SaveScreenshot_ReadPixelsAsynch_m1206178784 (ScreenCapture_t1787384414 * __this, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScreenCapture::SaveScreenshot_RenderToTexAsynch(System.String)
extern "C"  Il2CppObject * ScreenCapture_SaveScreenshot_RenderToTexAsynch_m3829844939 (ScreenCapture_t1787384414 * __this, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D ScreenCapture::GetScreenshot(CaptureMethod)
extern "C"  Texture2D_t3542995729 * ScreenCapture_GetScreenshot_m1184843311 (ScreenCapture_t1787384414 * __this, int32_t ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
