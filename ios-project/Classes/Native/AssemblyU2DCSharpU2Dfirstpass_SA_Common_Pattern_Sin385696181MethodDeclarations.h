﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si3172014400MethodDeclarations.h"

// System.Void SA.Common.Pattern.Singleton`1<ISN_GestureRecognizer>::.ctor()
#define Singleton_1__ctor_m2732238004(__this, method) ((  void (*) (Singleton_1_t385696181 *, const MethodInfo*))Singleton_1__ctor_m4152044218_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<ISN_GestureRecognizer>::.cctor()
#define Singleton_1__cctor_m849372337(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1__cctor_m982417053_gshared)(__this /* static, unused */, method)
// T SA.Common.Pattern.Singleton`1<ISN_GestureRecognizer>::get_Instance()
#define Singleton_1_get_Instance_m3543222121(__this /* static, unused */, method) ((  ISN_GestureRecognizer_t4198098372 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m3228489301_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<ISN_GestureRecognizer>::get_HasInstance()
#define Singleton_1_get_HasInstance_m2321140938(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_HasInstance_m2551508260_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<ISN_GestureRecognizer>::get_IsDestroyed()
#define Singleton_1_get_IsDestroyed_m2926314066(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_IsDestroyed_m4170993228_gshared)(__this /* static, unused */, method)
// System.Void SA.Common.Pattern.Singleton`1<ISN_GestureRecognizer>::OnDestroy()
#define Singleton_1_OnDestroy_m1408201917(__this, method) ((  void (*) (Singleton_1_t385696181 *, const MethodInfo*))Singleton_1_OnDestroy_m3790554761_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<ISN_GestureRecognizer>::OnApplicationQuit()
#define Singleton_1_OnApplicationQuit_m3675107882(__this, method) ((  void (*) (Singleton_1_t385696181 *, const MethodInfo*))Singleton_1_OnApplicationQuit_m1956476828_gshared)(__this, method)
