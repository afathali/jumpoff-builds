﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSStoreKitRestoreResult
struct IOSStoreKitRestoreResult_t3305276155;
// SA.Common.Models.Error
struct Error_t445207774;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSTransactionErrorC1822631322.h"

// System.Void IOSStoreKitRestoreResult::.ctor(SA.Common.Models.Error)
extern "C"  void IOSStoreKitRestoreResult__ctor_m2461468089 (IOSStoreKitRestoreResult_t3305276155 * __this, Error_t445207774 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreKitRestoreResult::.ctor()
extern "C"  void IOSStoreKitRestoreResult__ctor_m1153753154 (IOSStoreKitRestoreResult_t3305276155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSTransactionErrorCode IOSStoreKitRestoreResult::get_TransactionErrorCode()
extern "C"  int32_t IOSStoreKitRestoreResult_get_TransactionErrorCode_m1002729055 (IOSStoreKitRestoreResult_t3305276155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
