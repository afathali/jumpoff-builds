﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2630862464.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_AN_ManifestPermission1772110202.h"

// System.Void System.Array/InternalEnumerator`1<AN_ManifestPermission>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2417370447_gshared (InternalEnumerator_1_t2630862464 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2417370447(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2630862464 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2417370447_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AN_ManifestPermission>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3447903255_gshared (InternalEnumerator_1_t2630862464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3447903255(__this, method) ((  void (*) (InternalEnumerator_1_t2630862464 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3447903255_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AN_ManifestPermission>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2578798271_gshared (InternalEnumerator_1_t2630862464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2578798271(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2630862464 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2578798271_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AN_ManifestPermission>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2224690162_gshared (InternalEnumerator_1_t2630862464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2224690162(__this, method) ((  void (*) (InternalEnumerator_1_t2630862464 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2224690162_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AN_ManifestPermission>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2898560851_gshared (InternalEnumerator_1_t2630862464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2898560851(__this, method) ((  bool (*) (InternalEnumerator_1_t2630862464 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2898560851_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AN_ManifestPermission>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1803544294_gshared (InternalEnumerator_1_t2630862464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1803544294(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2630862464 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1803544294_gshared)(__this, method)
