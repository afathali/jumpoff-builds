﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<FB_ProfileImageSize>
struct EqualityComparer_1_t1576963401;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<FB_ProfileImageSize>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m4122620623_gshared (EqualityComparer_1_t1576963401 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m4122620623(__this, method) ((  void (*) (EqualityComparer_1_t1576963401 *, const MethodInfo*))EqualityComparer_1__ctor_m4122620623_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<FB_ProfileImageSize>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m4125072704_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m4125072704(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m4125072704_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<FB_ProfileImageSize>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1453505350_gshared (EqualityComparer_1_t1576963401 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1453505350(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t1576963401 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1453505350_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<FB_ProfileImageSize>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m434322012_gshared (EqualityComparer_1_t1576963401 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m434322012(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t1576963401 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m434322012_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<FB_ProfileImageSize>::get_Default()
extern "C"  EqualityComparer_1_t1576963401 * EqualityComparer_1_get_Default_m1490049479_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m1490049479(__this /* static, unused */, method) ((  EqualityComparer_1_t1576963401 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1490049479_gshared)(__this /* static, unused */, method)
