﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_Leaderboard
struct GK_Leaderboard_t156446466;
// System.String
struct String_t;
// GK_Score
struct GK_Score_t1529008873;
// System.Collections.Generic.List`1<GK_Score>
struct List_1_t898130005;
// GK_LeaderBoardInfo
struct GK_LeaderBoardInfo_t3670215494;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TimeSpan1050271570.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_CollectionType3353981271.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Score1529008873.h"

// System.Void GK_Leaderboard::.ctor(System.String)
extern "C"  void GK_Leaderboard__ctor_m19936061 (GK_Leaderboard_t156446466 * __this, String_t* ___leaderboardId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_Leaderboard::Refresh()
extern "C"  void GK_Leaderboard_Refresh_m1824521608 (GK_Leaderboard_t156446466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_Score GK_Leaderboard::GetCurrentPlayerScore(GK_TimeSpan,GK_CollectionType)
extern "C"  GK_Score_t1529008873 * GK_Leaderboard_GetCurrentPlayerScore_m2446329292 (GK_Leaderboard_t156446466 * __this, int32_t ___timeSpan0, int32_t ___collection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_Score GK_Leaderboard::GetScoreByPlayerId(System.String,GK_TimeSpan,GK_CollectionType)
extern "C"  GK_Score_t1529008873 * GK_Leaderboard_GetScoreByPlayerId_m3493116801 (GK_Leaderboard_t156446466 * __this, String_t* ___playerId0, int32_t ___timeSpan1, int32_t ___collection2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GK_Score> GK_Leaderboard::GetScoresList(GK_TimeSpan,GK_CollectionType)
extern "C"  List_1_t898130005 * GK_Leaderboard_GetScoresList_m2737440357 (GK_Leaderboard_t156446466 * __this, int32_t ___timeSpan0, int32_t ___collection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_Score GK_Leaderboard::GetScore(System.Int32,GK_TimeSpan,GK_CollectionType)
extern "C"  GK_Score_t1529008873 * GK_Leaderboard_GetScore_m1540554075 (GK_Leaderboard_t156446466 * __this, int32_t ___rank0, int32_t ___timeSpan1, int32_t ___collection2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_LeaderBoardInfo GK_Leaderboard::get_Info()
extern "C"  GK_LeaderBoardInfo_t3670215494 * GK_Leaderboard_get_Info_m2488386895 (GK_Leaderboard_t156446466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_Leaderboard::get_Id()
extern "C"  String_t* GK_Leaderboard_get_Id_m2465616898 (GK_Leaderboard_t156446466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GK_Leaderboard::get_CurrentPlayerScoreLoaded()
extern "C"  bool GK_Leaderboard_get_CurrentPlayerScoreLoaded_m626855199 (GK_Leaderboard_t156446466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_Leaderboard::CreateScoreListener(System.Int32,System.Boolean)
extern "C"  void GK_Leaderboard_CreateScoreListener_m2255789175 (GK_Leaderboard_t156446466 * __this, int32_t ___requestId0, bool ___isInternal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_Leaderboard::ReportLocalPlayerScoreUpdate(GK_Score,System.Int32)
extern "C"  void GK_Leaderboard_ReportLocalPlayerScoreUpdate_m4259069260 (GK_Leaderboard_t156446466 * __this, GK_Score_t1529008873 * ___score0, int32_t ___requestId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_Leaderboard::UpdateCurrentPlayerScore(System.Collections.Generic.List`1<GK_Score>)
extern "C"  void GK_Leaderboard_UpdateCurrentPlayerScore_m1548692105 (GK_Leaderboard_t156446466 * __this, List_1_t898130005 * ___newScores0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_Leaderboard::UpdateCurrentPlayerScore(GK_Score)
extern "C"  void GK_Leaderboard_UpdateCurrentPlayerScore_m946005651 (GK_Leaderboard_t156446466 * __this, GK_Score_t1529008873 * ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_Leaderboard::ReportLocalPlayerScoreUpdateFail(System.String,System.Int32)
extern "C"  void GK_Leaderboard_ReportLocalPlayerScoreUpdateFail_m910635203 (GK_Leaderboard_t156446466 * __this, String_t* ___errorData0, int32_t ___requestId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_Leaderboard::UpdateScore(GK_Score)
extern "C"  void GK_Leaderboard_UpdateScore_m2072260401 (GK_Leaderboard_t156446466 * __this, GK_Score_t1529008873 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_Leaderboard::get_id()
extern "C"  String_t* GK_Leaderboard_get_id_m2465617890 (GK_Leaderboard_t156446466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_Leaderboard::UpdateMaxRange(System.Int32)
extern "C"  void GK_Leaderboard_UpdateMaxRange_m2502640816 (GK_Leaderboard_t156446466 * __this, int32_t ___MR0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
