﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen2525452034MethodDeclarations.h"

// System.Void System.Action`2<System.Boolean,ChangePasswordResponse>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m1181069667(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t3733895773 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m946854823_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.Boolean,ChangePasswordResponse>::Invoke(T1,T2)
#define Action_2_Invoke_m3338125988(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t3733895773 *, bool, ChangePasswordResponse_t3897893034 *, const MethodInfo*))Action_2_Invoke_m3842146412_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.Boolean,ChangePasswordResponse>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m1113851155(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t3733895773 *, bool, ChangePasswordResponse_t3897893034 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m3907381723_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.Boolean,ChangePasswordResponse>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m3934670753(__this, ___result0, method) ((  void (*) (Action_2_t3733895773 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m2798191693_gshared)(__this, ___result0, method)
