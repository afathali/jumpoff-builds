﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Jump3D
struct Jump3D_t438928399;

#include "codegen/il2cpp-codegen.h"

// System.Void Jump3D::.ctor()
extern "C"  void Jump3D__ctor_m2945517466 (Jump3D_t438928399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jump3D::Start()
extern "C"  void Jump3D_Start_m1261433102 (Jump3D_t438928399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jump3D::Update()
extern "C"  void Jump3D_Update_m1455597371 (Jump3D_t438928399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
