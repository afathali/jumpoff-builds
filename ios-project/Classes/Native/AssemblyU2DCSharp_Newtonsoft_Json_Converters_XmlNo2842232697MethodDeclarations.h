﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlNodeConverter
struct XmlNodeConverter_t2842232697;
// System.String
struct String_t;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t1973729997;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t1719617802;
// Newtonsoft.Json.Converters.IXmlNode
struct IXmlNode_t1152344546;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;
// Newtonsoft.Json.JsonReader
struct JsonReader_t3154730733;
// System.Type
struct Type_t;
// Newtonsoft.Json.Converters.IXmlDocument
struct IXmlDocument_t1642480471;
// Newtonsoft.Json.Converters.IXmlElement
struct IXmlElement_t2005722770;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Converters.IXmlNode>
struct IEnumerable_1_t1444471591;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter1973729997.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer1719617802.h"
#include "System_Xml_System_Xml_XmlNamespaceManager486731501.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader3154730733.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::.ctor()
extern "C"  void XmlNodeConverter__ctor_m2375819016 (XmlNodeConverter_t2842232697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::get_DeserializeRootElementName()
extern "C"  String_t* XmlNodeConverter_get_DeserializeRootElementName_m3555955650 (XmlNodeConverter_t2842232697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::set_DeserializeRootElementName(System.String)
extern "C"  void XmlNodeConverter_set_DeserializeRootElementName_m2757227071 (XmlNodeConverter_t2842232697 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::get_WriteArrayAttribute()
extern "C"  bool XmlNodeConverter_get_WriteArrayAttribute_m1302555777 (XmlNodeConverter_t2842232697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::set_WriteArrayAttribute(System.Boolean)
extern "C"  void XmlNodeConverter_set_WriteArrayAttribute_m2982551810 (XmlNodeConverter_t2842232697 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::get_OmitRootObject()
extern "C"  bool XmlNodeConverter_get_OmitRootObject_m1297859677 (XmlNodeConverter_t2842232697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::set_OmitRootObject(System.Boolean)
extern "C"  void XmlNodeConverter_set_OmitRootObject_m2583415828 (XmlNodeConverter_t2842232697 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void XmlNodeConverter_WriteJson_m4033945938 (XmlNodeConverter_t2842232697 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t1719617802 * ___serializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeConverter::WrapXml(System.Object)
extern "C"  Il2CppObject * XmlNodeConverter_WrapXml_m2125412238 (XmlNodeConverter_t2842232697 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::PushParentNamespaces(Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern "C"  void XmlNodeConverter_PushParentNamespaces_m1525720629 (XmlNodeConverter_t2842232697 * __this, Il2CppObject * ___node0, XmlNamespaceManager_t486731501 * ___manager1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::ResolveFullName(Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern "C"  String_t* XmlNodeConverter_ResolveFullName_m1231259232 (XmlNodeConverter_t2842232697 * __this, Il2CppObject * ___node0, XmlNamespaceManager_t486731501 * ___manager1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeConverter::GetPropertyName(Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern "C"  String_t* XmlNodeConverter_GetPropertyName_m1667212960 (XmlNodeConverter_t2842232697 * __this, Il2CppObject * ___node0, XmlNamespaceManager_t486731501 * ___manager1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::IsArray(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  bool XmlNodeConverter_IsArray_m1754507697 (XmlNodeConverter_t2842232697 * __this, Il2CppObject * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::SerializeGroupedNodes(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager,System.Boolean)
extern "C"  void XmlNodeConverter_SerializeGroupedNodes_m3960894251 (XmlNodeConverter_t2842232697 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___node1, XmlNamespaceManager_t486731501 * ___manager2, bool ___writePropertyName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::SerializeNode(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager,System.Boolean)
extern "C"  void XmlNodeConverter_SerializeNode_m4032343410 (XmlNodeConverter_t2842232697 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___node1, XmlNamespaceManager_t486731501 * ___manager2, bool ___writePropertyName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.XmlNodeConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * XmlNodeConverter_ReadJson_m1205874203 (XmlNodeConverter_t2842232697 * __this, JsonReader_t3154730733 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t1719617802 * ___serializer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::DeserializeValue(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.Xml.XmlNamespaceManager,System.String,Newtonsoft.Json.Converters.IXmlNode)
extern "C"  void XmlNodeConverter_DeserializeValue_m2029222552 (XmlNodeConverter_t2842232697 * __this, JsonReader_t3154730733 * ___reader0, Il2CppObject * ___document1, XmlNamespaceManager_t486731501 * ___manager2, String_t* ___propertyName3, Il2CppObject * ___currentNode4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ReadElement(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode,System.String,System.Xml.XmlNamespaceManager)
extern "C"  void XmlNodeConverter_ReadElement_m4077091626 (XmlNodeConverter_t2842232697 * __this, JsonReader_t3154730733 * ___reader0, Il2CppObject * ___document1, Il2CppObject * ___currentNode2, String_t* ___propertyName3, XmlNamespaceManager_t486731501 * ___manager4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ReadArrayElements(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.String,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern "C"  void XmlNodeConverter_ReadArrayElements_m255892438 (XmlNodeConverter_t2842232697 * __this, JsonReader_t3154730733 * ___reader0, Il2CppObject * ___document1, String_t* ___propertyName2, Il2CppObject * ___currentNode3, XmlNamespaceManager_t486731501 * ___manager4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::AddJsonArrayAttribute(Newtonsoft.Json.Converters.IXmlElement,Newtonsoft.Json.Converters.IXmlDocument)
extern "C"  void XmlNodeConverter_AddJsonArrayAttribute_m333429657 (XmlNodeConverter_t2842232697 * __this, Il2CppObject * ___element0, Il2CppObject * ___document1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Newtonsoft.Json.Converters.XmlNodeConverter::ReadAttributeElements(Newtonsoft.Json.JsonReader,System.Xml.XmlNamespaceManager)
extern "C"  Dictionary_2_t3943999495 * XmlNodeConverter_ReadAttributeElements_m30456074 (XmlNodeConverter_t2842232697 * __this, JsonReader_t3154730733 * ___reader0, XmlNamespaceManager_t486731501 * ___manager1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::CreateInstruction(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode,System.String)
extern "C"  void XmlNodeConverter_CreateInstruction_m419890307 (XmlNodeConverter_t2842232697 * __this, JsonReader_t3154730733 * ___reader0, Il2CppObject * ___document1, Il2CppObject * ___currentNode2, String_t* ___propertyName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlNodeConverter::CreateElement(System.String,Newtonsoft.Json.Converters.IXmlDocument,System.String,System.Xml.XmlNamespaceManager)
extern "C"  Il2CppObject * XmlNodeConverter_CreateElement_m1154909009 (XmlNodeConverter_t2842232697 * __this, String_t* ___elementName0, Il2CppObject * ___document1, String_t* ___elementPrefix2, XmlNamespaceManager_t486731501 * ___manager3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeConverter::DeserializeNode(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.Xml.XmlNamespaceManager,Newtonsoft.Json.Converters.IXmlNode)
extern "C"  void XmlNodeConverter_DeserializeNode_m4042495103 (XmlNodeConverter_t2842232697 * __this, JsonReader_t3154730733 * ___reader0, Il2CppObject * ___document1, XmlNamespaceManager_t486731501 * ___manager2, Il2CppObject * ___currentNode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::IsNamespaceAttribute(System.String,System.String&)
extern "C"  bool XmlNodeConverter_IsNamespaceAttribute_m3392659031 (XmlNodeConverter_t2842232697 * __this, String_t* ___attributeName0, String_t** ___prefix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeConverter::ValueAttributes(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Converters.IXmlNode>)
extern "C"  Il2CppObject* XmlNodeConverter_ValueAttributes_m1781397831 (XmlNodeConverter_t2842232697 * __this, Il2CppObject* ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::CanConvert(System.Type)
extern "C"  bool XmlNodeConverter_CanConvert_m2997837298 (XmlNodeConverter_t2842232697 * __this, Type_t * ___valueType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<IsArray>m__C1(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  bool XmlNodeConverter_U3CIsArrayU3Em__C1_m1181688198 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<ValueAttributes>m__C5(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  bool XmlNodeConverter_U3CValueAttributesU3Em__C5_m1166306823 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
