﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2601497439.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"

// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3831311062_gshared (InternalEnumerator_1_t2601497439 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3831311062(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2601497439 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3831311062_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4832614_gshared (InternalEnumerator_1_t2601497439 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4832614(__this, method) ((  void (*) (InternalEnumerator_1_t2601497439 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4832614_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810357208_gshared (InternalEnumerator_1_t2601497439 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810357208(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2601497439 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810357208_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m371216459_gshared (InternalEnumerator_1_t2601497439 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m371216459(__this, method) ((  void (*) (InternalEnumerator_1_t2601497439 *, const MethodInfo*))InternalEnumerator_1_Dispose_m371216459_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3460721294_gshared (InternalEnumerator_1_t2601497439 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3460721294(__this, method) ((  bool (*) (InternalEnumerator_1_t2601497439 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3460721294_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m4208916447_gshared (InternalEnumerator_1_t2601497439 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4208916447(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2601497439 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4208916447_gshared)(__this, method)
