﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_UIWidthDependence
struct SA_UIWidthDependence_t1527301732;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "codegen/il2cpp-codegen.h"

// System.Void SA_UIWidthDependence::.ctor()
extern "C"  void SA_UIWidthDependence__ctor_m2486248185 (SA_UIWidthDependence_t1527301732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_UIWidthDependence::Awake()
extern "C"  void SA_UIWidthDependence_Awake_m77143550 (SA_UIWidthDependence_t1527301732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_UIWidthDependence::Update()
extern "C"  void SA_UIWidthDependence_Update_m79544464 (SA_UIWidthDependence_t1527301732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_UIWidthDependence::ApplyTransformation()
extern "C"  void SA_UIWidthDependence_ApplyTransformation_m2761767072 (SA_UIWidthDependence_t1527301732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform SA_UIWidthDependence::get_rect()
extern "C"  RectTransform_t3349966182 * SA_UIWidthDependence_get_rect_m3387637258 (SA_UIWidthDependence_t1527301732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_UIWidthDependence::OnDetroy()
extern "C"  void SA_UIWidthDependence_OnDetroy_m2883930857 (SA_UIWidthDependence_t1527301732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
