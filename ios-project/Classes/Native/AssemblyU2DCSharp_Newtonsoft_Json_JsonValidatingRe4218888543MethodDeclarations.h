﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonValidatingReader/SchemaScope
struct SchemaScope_t4218888543;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel>
struct IList_1_t1249835177;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1445386684;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// Newtonsoft.Json.Schema.JsonSchemaModel
struct JsonSchemaModel_t708894576;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType1307827213.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema708894576.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_381019060.h"

// System.Void Newtonsoft.Json.JsonValidatingReader/SchemaScope::.ctor(Newtonsoft.Json.Linq.JTokenType,System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel>)
extern "C"  void SchemaScope__ctor_m1389390281 (SchemaScope_t4218888543 * __this, int32_t ___tokenType0, Il2CppObject* ___schemas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonValidatingReader/SchemaScope::get_CurrentPropertyName()
extern "C"  String_t* SchemaScope_get_CurrentPropertyName_m467723215 (SchemaScope_t4218888543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader/SchemaScope::set_CurrentPropertyName(System.String)
extern "C"  void SchemaScope_set_CurrentPropertyName_m3133591036 (SchemaScope_t4218888543 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonValidatingReader/SchemaScope::get_ArrayItemCount()
extern "C"  int32_t SchemaScope_get_ArrayItemCount_m1392804758 (SchemaScope_t4218888543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonValidatingReader/SchemaScope::set_ArrayItemCount(System.Int32)
extern "C"  void SchemaScope_set_ArrayItemCount_m1732724797 (SchemaScope_t4218888543 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.JsonValidatingReader/SchemaScope::get_Schemas()
extern "C"  Il2CppObject* SchemaScope_get_Schemas_m1243982411 (SchemaScope_t4218888543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> Newtonsoft.Json.JsonValidatingReader/SchemaScope::get_RequiredProperties()
extern "C"  Dictionary_2_t1445386684 * SchemaScope_get_RequiredProperties_m3619193096 (SchemaScope_t4218888543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.JsonValidatingReader/SchemaScope::get_TokenType()
extern "C"  int32_t SchemaScope_get_TokenType_m3800378977 (SchemaScope_t4218888543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.String> Newtonsoft.Json.JsonValidatingReader/SchemaScope::GetRequiredProperties(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Il2CppObject* SchemaScope_GetRequiredProperties_m4066413226 (SchemaScope_t4218888543 * __this, JsonSchemaModel_t708894576 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonValidatingReader/SchemaScope::<SchemaScope>m__C8(System.String)
extern "C"  String_t* SchemaScope_U3CSchemaScopeU3Em__C8_m1696451158 (Il2CppObject * __this /* static, unused */, String_t* ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader/SchemaScope::<SchemaScope>m__C9(System.String)
extern "C"  bool SchemaScope_U3CSchemaScopeU3Em__C9_m1781283974 (Il2CppObject * __this /* static, unused */, String_t* ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonValidatingReader/SchemaScope::<GetRequiredProperties>m__CA(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>)
extern "C"  bool SchemaScope_U3CGetRequiredPropertiesU3Em__CA_m487850812 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t381019060  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonValidatingReader/SchemaScope::<GetRequiredProperties>m__CB(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>)
extern "C"  String_t* SchemaScope_U3CGetRequiredPropertiesU3Em__CB_m2884817194 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t381019060  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
