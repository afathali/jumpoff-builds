﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<Newtonsoft.Json.Linq.JTokenType>
struct Comparer_1_t197836332;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<Newtonsoft.Json.Linq.JTokenType>::.ctor()
extern "C"  void Comparer_1__ctor_m1442870467_gshared (Comparer_1_t197836332 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m1442870467(__this, method) ((  void (*) (Comparer_1_t197836332 *, const MethodInfo*))Comparer_1__ctor_m1442870467_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Newtonsoft.Json.Linq.JTokenType>::.cctor()
extern "C"  void Comparer_1__cctor_m4063870542_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m4063870542(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m4063870542_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3668611936_gshared (Comparer_1_t197836332 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m3668611936(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t197836332 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m3668611936_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Newtonsoft.Json.Linq.JTokenType>::get_Default()
extern "C"  Comparer_1_t197836332 * Comparer_1_get_Default_m1127605163_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m1127605163(__this /* static, unused */, method) ((  Comparer_1_t197836332 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m1127605163_gshared)(__this /* static, unused */, method)
