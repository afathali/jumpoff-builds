﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3968042187MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3349618609(__this, ___host0, method) ((  void (*) (Enumerator_t1995559482 *, Dictionary_2_t309026718 *, const MethodInfo*))Enumerator__ctor_m3819430617_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3034952918(__this, method) ((  Il2CppObject * (*) (Enumerator_t1995559482 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3933483934_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m558580958(__this, method) ((  void (*) (Enumerator_t1995559482 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2482663638_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::Dispose()
#define Enumerator_Dispose_m1690205793(__this, method) ((  void (*) (Enumerator_t1995559482 *, const MethodInfo*))Enumerator_Dispose_m4238653081_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::MoveNext()
#define Enumerator_MoveNext_m2962213301(__this, method) ((  bool (*) (Enumerator_t1995559482 *, const MethodInfo*))Enumerator_MoveNext_m335649778_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<GK_SavedGame>>::get_Current()
#define Enumerator_get_Current_m3496474696(__this, method) ((  List_1_t2689214752 * (*) (Enumerator_t1995559482 *, const MethodInfo*))Enumerator_get_Current_m4025002300_gshared)(__this, method)
