﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PTPGameController
struct PTPGameController_t90729750;
// SA.Common.Models.Result
struct Result_t4287219743;
// GK_Player
struct GK_Player_t2782008294;
// GK_RTM_Match
struct GK_RTM_Match_t873568990;
// GK_RTM_MatchStartedResult
struct GK_RTM_MatchStartedResult_t833698690;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Player2782008294.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_PlayerConnectionS2434478783.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_RTM_Match873568990.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_RTM_MatchStartedRe833698690.h"

// System.Void PTPGameController::.ctor()
extern "C"  void PTPGameController__ctor_m3911390263 (PTPGameController_t90729750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::Awake()
extern "C"  void PTPGameController_Awake_m3673155392 (PTPGameController_t90729750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::createRedSphere(UnityEngine.Vector3)
extern "C"  void PTPGameController_createRedSphere_m3186932546 (PTPGameController_t90729750 * __this, Vector3_t2243707580  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::createGreenSphere(UnityEngine.Vector3)
extern "C"  void PTPGameController_createGreenSphere_m1211603630 (PTPGameController_t90729750 * __this, Vector3_t2243707580  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::OnAuthFinished(SA.Common.Models.Result)
extern "C"  void PTPGameController_OnAuthFinished_m269642600 (PTPGameController_t90729750 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::HandleActionPlayerStateChanged(GK_Player,GK_PlayerConnectionState,GK_RTM_Match)
extern "C"  void PTPGameController_HandleActionPlayerStateChanged_m3581317514 (PTPGameController_t90729750 * __this, GK_Player_t2782008294 * ___player0, int32_t ___state1, GK_RTM_Match_t873568990 * ___macth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::HandleActionMatchStarted(GK_RTM_MatchStartedResult)
extern "C"  void PTPGameController_HandleActionMatchStarted_m2333351317 (PTPGameController_t90729750 * __this, GK_RTM_MatchStartedResult_t833698690 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::CheckMatchState(GK_RTM_Match)
extern "C"  void PTPGameController_CheckMatchState_m2606017161 (PTPGameController_t90729750 * __this, GK_RTM_Match_t873568990 * ___macth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PTPGameController::cleanUpScene()
extern "C"  void PTPGameController_cleanUpScene_m1039820701 (PTPGameController_t90729750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
