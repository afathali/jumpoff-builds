﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SampleClassLibrary.SampleExternalClass
struct SampleExternalClass_t2192168175;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t1037045868;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String SampleClassLibrary.SampleExternalClass::get_SampleString()
extern "C"  String_t* SampleExternalClass_get_SampleString_m3396780743 (SampleExternalClass_t2192168175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleClassLibrary.SampleExternalClass::set_SampleString(System.String)
extern "C"  void SampleExternalClass_set_SampleString_m2575781044 (SampleExternalClass_t2192168175 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String> SampleClassLibrary.SampleExternalClass::get_SampleDictionary()
extern "C"  Dictionary_2_t1037045868 * SampleExternalClass_get_SampleDictionary_m2896714854 (SampleExternalClass_t2192168175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleClassLibrary.SampleExternalClass::set_SampleDictionary(System.Collections.Generic.Dictionary`2<System.Int32,System.String>)
extern "C"  void SampleExternalClass_set_SampleDictionary_m3988185127 (SampleExternalClass_t2192168175 * __this, Dictionary_2_t1037045868 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleClassLibrary.SampleExternalClass::.ctor()
extern "C"  void SampleExternalClass__ctor_m670993782 (SampleExternalClass_t2192168175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
