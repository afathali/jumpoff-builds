﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPScore
struct GPScore_t3219488889;
// System.String
struct String_t;
// GooglePlayerTemplate
struct GooglePlayerTemplate_t2506317812;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GPBoardTimeSpan42003024.h"
#include "AssemblyU2DCSharp_GPCollectionType2617299399.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void GPScore::.ctor(System.Int64,System.Int32,GPBoardTimeSpan,GPCollectionType,System.String,System.String)
extern "C"  void GPScore__ctor_m4055153824 (GPScore_t3219488889 * __this, int64_t ___vScore0, int32_t ___vRank1, int32_t ___vTimeSpan2, int32_t ___sCollection3, String_t* ___lid4, String_t* ___pid5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPScore::UpdateScore(System.Int64)
extern "C"  void GPScore_UpdateScore_m383345495 (GPScore_t3219488889 * __this, int64_t ___vScore0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GPScore::get_rank()
extern "C"  int32_t GPScore_get_rank_m1299795409 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GPScore::get_Rank()
extern "C"  int32_t GPScore_get_Rank_m1299754161 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GPScore::get_score()
extern "C"  int64_t GPScore_get_score_m787022660 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GPScore::get_LongScore()
extern "C"  int64_t GPScore_get_LongScore_m4243909002 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GPScore::get_CurrencyScore()
extern "C"  float GPScore_get_CurrencyScore_m3726383680 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan GPScore::get_TimeScore()
extern "C"  TimeSpan_t3430258949  GPScore_get_TimeScore_m3761055041 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPScore::get_playerId()
extern "C"  String_t* GPScore_get_playerId_m2794220138 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPScore::get_PlayerId()
extern "C"  String_t* GPScore_get_PlayerId_m1949717514 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayerTemplate GPScore::get_Player()
extern "C"  GooglePlayerTemplate_t2506317812 * GPScore_get_Player_m3739343415 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPScore::get_leaderboardId()
extern "C"  String_t* GPScore_get_leaderboardId_m1989073854 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPScore::get_LeaderboardId()
extern "C"  String_t* GPScore_get_LeaderboardId_m3619161822 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPCollectionType GPScore::get_collection()
extern "C"  int32_t GPScore_get_collection_m2973160421 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPCollectionType GPScore::get_Collection()
extern "C"  int32_t GPScore_get_Collection_m4151826373 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPBoardTimeSpan GPScore::get_timeSpan()
extern "C"  int32_t GPScore_get_timeSpan_m2274227787 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPBoardTimeSpan GPScore::get_TimeSpan()
extern "C"  int32_t GPScore_get_TimeSpan_m2107692843 (GPScore_t3219488889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
