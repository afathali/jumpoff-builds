﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1206604463(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3322174704 *, Dictionary_2_t2002150002 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3341635328(__this, method) ((  Il2CppObject * (*) (Enumerator_t3322174704 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1263248212(__this, method) ((  void (*) (Enumerator_t3322174704 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2330181589(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3322174704 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3018063210(__this, method) ((  Il2CppObject * (*) (Enumerator_t3322174704 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4047974328(__this, method) ((  Il2CppObject * (*) (Enumerator_t3322174704 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::MoveNext()
#define Enumerator_MoveNext_m3524388124(__this, method) ((  bool (*) (Enumerator_t3322174704 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::get_Current()
#define Enumerator_get_Current_m2445355364(__this, method) ((  KeyValuePair_2_t4054462520  (*) (Enumerator_t3322174704 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2147980867(__this, method) ((  String_t* (*) (Enumerator_t3322174704 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3470572835(__this, method) ((  TwitterUserInfo_t87370740 * (*) (Enumerator_t3322174704 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::Reset()
#define Enumerator_Reset_m964302189(__this, method) ((  void (*) (Enumerator_t3322174704 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::VerifyState()
#define Enumerator_VerifyState_m4290252588(__this, method) ((  void (*) (Enumerator_t3322174704 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m773327012(__this, method) ((  void (*) (Enumerator_t3322174704 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,TwitterUserInfo>::Dispose()
#define Enumerator_Dispose_m2613349891(__this, method) ((  void (*) (Enumerator_t3322174704 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
