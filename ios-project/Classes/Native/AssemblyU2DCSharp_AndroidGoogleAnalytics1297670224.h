﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen2588514142.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidGoogleAnalytics
struct  AndroidGoogleAnalytics_t1297670224  : public SA_Singleton_OLD_1_t2588514142
{
public:
	// System.Boolean AndroidGoogleAnalytics::IsStarted
	bool ___IsStarted_4;

public:
	inline static int32_t get_offset_of_IsStarted_4() { return static_cast<int32_t>(offsetof(AndroidGoogleAnalytics_t1297670224, ___IsStarted_4)); }
	inline bool get_IsStarted_4() const { return ___IsStarted_4; }
	inline bool* get_address_of_IsStarted_4() { return &___IsStarted_4; }
	inline void set_IsStarted_4(bool value)
	{
		___IsStarted_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
