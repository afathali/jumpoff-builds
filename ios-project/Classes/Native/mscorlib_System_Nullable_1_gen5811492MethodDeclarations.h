﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen5811492.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(T)
extern "C"  void Nullable_1__ctor_m681591852_gshared (Nullable_1_t5811492 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m681591852(__this, ___value0, method) ((  void (*) (Nullable_1_t5811492 *, int32_t, const MethodInfo*))Nullable_1__ctor_m681591852_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1003835587_gshared (Nullable_1_t5811492 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1003835587(__this, method) ((  bool (*) (Nullable_1_t5811492 *, const MethodInfo*))Nullable_1_get_HasValue_m1003835587_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m2736388043_gshared (Nullable_1_t5811492 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2736388043(__this, method) ((  int32_t (*) (Nullable_1_t5811492 *, const MethodInfo*))Nullable_1_get_Value_m2736388043_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1948246969_gshared (Nullable_1_t5811492 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1948246969(__this, ___other0, method) ((  bool (*) (Nullable_1_t5811492 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1948246969_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2735515702_gshared (Nullable_1_t5811492 * __this, Nullable_1_t5811492  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2735515702(__this, ___other0, method) ((  bool (*) (Nullable_1_t5811492 *, Nullable_1_t5811492 , const MethodInfo*))Nullable_1_Equals_m2735515702_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2700679387_gshared (Nullable_1_t5811492 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m2700679387(__this, method) ((  int32_t (*) (Nullable_1_t5811492 *, const MethodInfo*))Nullable_1_GetHashCode_m2700679387_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2672151126_gshared (Nullable_1_t5811492 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2672151126(__this, method) ((  int32_t (*) (Nullable_1_t5811492 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2672151126_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1197931512_gshared (Nullable_1_t5811492 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1197931512(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t5811492 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m1197931512_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2930416727_gshared (Nullable_1_t5811492 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2930416727(__this, method) ((  String_t* (*) (Nullable_1_t5811492 *, const MethodInfo*))Nullable_1_ToString_m2930416727_gshared)(__this, method)
