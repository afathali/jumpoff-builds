﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_AppInvitesProxy
struct AN_AppInvitesProxy_t250409945;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_AppInvitesProxy::.ctor()
extern "C"  void AN_AppInvitesProxy__ctor_m529351108 (AN_AppInvitesProxy_t250409945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_AppInvitesProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_AppInvitesProxy_CallActivityFunction_m2190733091 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_AppInvitesProxy::CreateBuilder(System.Int32,System.String)
extern "C"  void AN_AppInvitesProxy_CreateBuilder_m470570058 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___title1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_AppInvitesProxy::SetMessage(System.Int32,System.String)
extern "C"  void AN_AppInvitesProxy_SetMessage_m3338555532 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_AppInvitesProxy::SetDeepLink(System.Int32,System.String)
extern "C"  void AN_AppInvitesProxy_SetDeepLink_m4183331013 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___url1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_AppInvitesProxy::SetCallToActionText(System.Int32,System.String)
extern "C"  void AN_AppInvitesProxy_SetCallToActionText_m537906967 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___actionText1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_AppInvitesProxy::SetGoogleAnalyticsTrackingId(System.Int32,System.String)
extern "C"  void AN_AppInvitesProxy_SetGoogleAnalyticsTrackingId_m1408780538 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___trackingId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_AppInvitesProxy::SetAndroidMinimumVersionCode(System.Int32,System.Int32)
extern "C"  void AN_AppInvitesProxy_SetAndroidMinimumVersionCode_m1837986672 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___versionCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_AppInvitesProxy::SetAdditionalReferralParameters(System.Int32,System.String,System.String)
extern "C"  void AN_AppInvitesProxy_SetAdditionalReferralParameters_m896951465 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___keys1, String_t* ___values2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_AppInvitesProxy::StartInvitationDialog(System.Int32)
extern "C"  void AN_AppInvitesProxy_StartInvitationDialog_m1867238362 (Il2CppObject * __this /* static, unused */, int32_t ___builderId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_AppInvitesProxy::GetInvitation(System.Boolean)
extern "C"  void AN_AppInvitesProxy_GetInvitation_m1622125138 (Il2CppObject * __this /* static, unused */, bool ___autoLaunchDeepLink0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
