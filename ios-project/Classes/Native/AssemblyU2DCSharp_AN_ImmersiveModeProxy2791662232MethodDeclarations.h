﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_ImmersiveModeProxy
struct AN_ImmersiveModeProxy_t2791662232;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_ImmersiveModeProxy::.ctor()
extern "C"  void AN_ImmersiveModeProxy__ctor_m925144639 (AN_ImmersiveModeProxy_t2791662232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ImmersiveModeProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_ImmersiveModeProxy_CallActivityFunction_m918089938 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ImmersiveModeProxy::enableImmersiveMode()
extern "C"  void AN_ImmersiveModeProxy_enableImmersiveMode_m3497869134 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
