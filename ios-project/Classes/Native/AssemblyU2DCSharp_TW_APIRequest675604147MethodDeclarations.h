﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TW_APIRequest
struct TW_APIRequest_t675604147;
// System.Action`1<TW_APIRequstResult>
struct Action_1_t256950437;
// System.String
struct String_t;
// TW_APIRequstResult
struct TW_APIRequstResult_t455151055;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_TW_APIRequstResult455151055.h"

// System.Void TW_APIRequest::.ctor()
extern "C"  void TW_APIRequest__ctor_m2921999370 (TW_APIRequest_t675604147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_APIRequest::add_ActionComplete(System.Action`1<TW_APIRequstResult>)
extern "C"  void TW_APIRequest_add_ActionComplete_m4098498040 (TW_APIRequest_t675604147 * __this, Action_1_t256950437 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_APIRequest::remove_ActionComplete(System.Action`1<TW_APIRequstResult>)
extern "C"  void TW_APIRequest_remove_ActionComplete_m308839855 (TW_APIRequest_t675604147 * __this, Action_1_t256950437 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_APIRequest::Send()
extern "C"  void TW_APIRequest_Send_m3706210730 (TW_APIRequest_t675604147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_APIRequest::AddParam(System.String,System.Int32)
extern "C"  void TW_APIRequest_AddParam_m692067147 (TW_APIRequest_t675604147 * __this, String_t* ___name0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_APIRequest::AddParam(System.String,System.String)
extern "C"  void TW_APIRequest_AddParam_m3887194682 (TW_APIRequest_t675604147 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_APIRequest::SendCompleteResult(TW_APIRequstResult)
extern "C"  void TW_APIRequest_SendCompleteResult_m2360055243 (TW_APIRequest_t675604147 * __this, TW_APIRequstResult_t455151055 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_APIRequest::SetUrl(System.String)
extern "C"  void TW_APIRequest_SetUrl_m1692842805 (TW_APIRequest_t675604147 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TW_APIRequest::Request()
extern "C"  Il2CppObject * TW_APIRequest_Request_m2240669629 (TW_APIRequest_t675604147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_APIRequest::OnTokenLoaded()
extern "C"  void TW_APIRequest_OnTokenLoaded_m952319911 (TW_APIRequest_t675604147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_APIRequest::<ActionComplete>m__AF(TW_APIRequstResult)
extern "C"  void TW_APIRequest_U3CActionCompleteU3Em__AF_m1027575270 (Il2CppObject * __this /* static, unused */, TW_APIRequstResult_t455151055 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
