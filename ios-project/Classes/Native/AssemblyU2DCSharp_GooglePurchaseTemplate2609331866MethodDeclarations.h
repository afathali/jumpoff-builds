﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePurchaseTemplate
struct GooglePurchaseTemplate_t2609331866;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GooglePurchaseTemplate::.ctor()
extern "C"  void GooglePurchaseTemplate__ctor_m3456426117 (GooglePurchaseTemplate_t2609331866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePurchaseTemplate::SetState(System.String)
extern "C"  void GooglePurchaseTemplate_SetState_m2909074002 (GooglePurchaseTemplate_t2609331866 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
