﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CourseVisualizer/<LoadBanners>c__Iterator0
struct U3CLoadBannersU3Ec__Iterator0_t4124176354;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CourseVisualizer/<LoadBanners>c__Iterator0::.ctor()
extern "C"  void U3CLoadBannersU3Ec__Iterator0__ctor_m3460124455 (U3CLoadBannersU3Ec__Iterator0_t4124176354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CourseVisualizer/<LoadBanners>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadBannersU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3572371497 (U3CLoadBannersU3Ec__Iterator0_t4124176354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CourseVisualizer/<LoadBanners>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadBannersU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2165056161 (U3CLoadBannersU3Ec__Iterator0_t4124176354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CourseVisualizer/<LoadBanners>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadBannersU3Ec__Iterator0_MoveNext_m1051674357 (U3CLoadBannersU3Ec__Iterator0_t4124176354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer/<LoadBanners>c__Iterator0::Dispose()
extern "C"  void U3CLoadBannersU3Ec__Iterator0_Dispose_m1751980528 (U3CLoadBannersU3Ec__Iterator0_t4124176354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizer/<LoadBanners>c__Iterator0::Reset()
extern "C"  void U3CLoadBannersU3Ec__Iterator0_Reset_m2386147446 (U3CLoadBannersU3Ec__Iterator0_t4124176354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
