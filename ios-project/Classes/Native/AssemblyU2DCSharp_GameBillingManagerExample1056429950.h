﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameBillingManagerExample
struct  GameBillingManagerExample_t1056429950  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct GameBillingManagerExample_t1056429950_StaticFields
{
public:
	// System.Boolean GameBillingManagerExample::_isInited
	bool ____isInited_4;
	// System.Boolean GameBillingManagerExample::ListnersAdded
	bool ___ListnersAdded_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> GameBillingManagerExample::<>f__switch$map0
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map0_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> GameBillingManagerExample::<>f__switch$map1
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1_7;

public:
	inline static int32_t get_offset_of__isInited_4() { return static_cast<int32_t>(offsetof(GameBillingManagerExample_t1056429950_StaticFields, ____isInited_4)); }
	inline bool get__isInited_4() const { return ____isInited_4; }
	inline bool* get_address_of__isInited_4() { return &____isInited_4; }
	inline void set__isInited_4(bool value)
	{
		____isInited_4 = value;
	}

	inline static int32_t get_offset_of_ListnersAdded_5() { return static_cast<int32_t>(offsetof(GameBillingManagerExample_t1056429950_StaticFields, ___ListnersAdded_5)); }
	inline bool get_ListnersAdded_5() const { return ___ListnersAdded_5; }
	inline bool* get_address_of_ListnersAdded_5() { return &___ListnersAdded_5; }
	inline void set_ListnersAdded_5(bool value)
	{
		___ListnersAdded_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_6() { return static_cast<int32_t>(offsetof(GameBillingManagerExample_t1056429950_StaticFields, ___U3CU3Ef__switchU24map0_6)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map0_6() const { return ___U3CU3Ef__switchU24map0_6; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map0_6() { return &___U3CU3Ef__switchU24map0_6; }
	inline void set_U3CU3Ef__switchU24map0_6(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_7() { return static_cast<int32_t>(offsetof(GameBillingManagerExample_t1056429950_StaticFields, ___U3CU3Ef__switchU24map1_7)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1_7() const { return ___U3CU3Ef__switchU24map1_7; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1_7() { return &___U3CU3Ef__switchU24map1_7; }
	inline void set_U3CU3Ef__switchU24map1_7(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
