﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlNode
struct XmlNode_t616554813;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlNodeWrapper
struct  XmlNodeWrapper_t2035079622  : public Il2CppObject
{
public:
	// System.Xml.XmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::_node
	XmlNode_t616554813 * ____node_0;

public:
	inline static int32_t get_offset_of__node_0() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_t2035079622, ____node_0)); }
	inline XmlNode_t616554813 * get__node_0() const { return ____node_0; }
	inline XmlNode_t616554813 ** get_address_of__node_0() { return &____node_0; }
	inline void set__node_0(XmlNode_t616554813 * value)
	{
		____node_0 = value;
		Il2CppCodeGenWriteBarrier(&____node_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
