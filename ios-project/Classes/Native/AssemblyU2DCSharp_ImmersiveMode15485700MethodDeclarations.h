﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ImmersiveMode
struct ImmersiveMode_t15485700;

#include "codegen/il2cpp-codegen.h"

// System.Void ImmersiveMode::.ctor()
extern "C"  void ImmersiveMode__ctor_m1764769019 (ImmersiveMode_t15485700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImmersiveMode::Awake()
extern "C"  void ImmersiveMode_Awake_m3613121268 (ImmersiveMode_t15485700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImmersiveMode::EnableImmersiveMode()
extern "C"  void ImmersiveMode_EnableImmersiveMode_m57357706 (ImmersiveMode_t15485700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
