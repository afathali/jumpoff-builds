﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MCGBehaviour/Component
struct Component_t1531646212;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MCGBehaviour/Component/ComponentLifestyle
struct  ComponentLifestyle_t4170640987  : public Il2CppObject
{
public:
	// MCGBehaviour/Component MCGBehaviour/Component/ComponentLifestyle::_component
	Component_t1531646212 * ____component_0;

public:
	inline static int32_t get_offset_of__component_0() { return static_cast<int32_t>(offsetof(ComponentLifestyle_t4170640987, ____component_0)); }
	inline Component_t1531646212 * get__component_0() const { return ____component_0; }
	inline Component_t1531646212 ** get_address_of__component_0() { return &____component_0; }
	inline void set__component_0(Component_t1531646212 * value)
	{
		____component_0 = value;
		Il2CppCodeGenWriteBarrier(&____component_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
