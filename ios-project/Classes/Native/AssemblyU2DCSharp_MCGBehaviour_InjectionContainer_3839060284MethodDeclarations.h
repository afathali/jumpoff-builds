﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MCGBehaviour/InjectionContainer/<Resolve>c__AnonStorey3B
struct U3CResolveU3Ec__AnonStorey3B_t3839060284;
// MCGBehaviour/InjectionContainer/TypeData
struct TypeData_t212254090;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MCGBehaviour_InjectionContainer_T212254090.h"

// System.Void MCGBehaviour/InjectionContainer/<Resolve>c__AnonStorey3B::.ctor()
extern "C"  void U3CResolveU3Ec__AnonStorey3B__ctor_m312354479 (U3CResolveU3Ec__AnonStorey3B_t3839060284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MCGBehaviour/InjectionContainer/<Resolve>c__AnonStorey3B::<>m__10A(MCGBehaviour/InjectionContainer/TypeData)
extern "C"  bool U3CResolveU3Ec__AnonStorey3B_U3CU3Em__10A_m1121563746 (U3CResolveU3Ec__AnonStorey3B_t3839060284 * __this, TypeData_t212254090 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
