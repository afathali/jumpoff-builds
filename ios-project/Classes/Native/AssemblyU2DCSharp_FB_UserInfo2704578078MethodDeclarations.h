﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FB_UserInfo
struct FB_UserInfo_t2704578078;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Action`1<FB_UserInfo>
struct Action_1_t2506377460;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_FB_ProfileImageSize3003328130.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_FB_Gender2517284304.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharp_FB_UserInfo2704578078.h"

// System.Void FB_UserInfo::.ctor(System.String)
extern "C"  void FB_UserInfo__ctor_m511519921 (FB_UserInfo_t2704578078 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_UserInfo::.ctor(System.Collections.IDictionary)
extern "C"  void FB_UserInfo__ctor_m3093244112 (FB_UserInfo_t2704578078 * __this, Il2CppObject * ___JSON0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_UserInfo::add_OnProfileImageLoaded(System.Action`1<FB_UserInfo>)
extern "C"  void FB_UserInfo_add_OnProfileImageLoaded_m477792157 (FB_UserInfo_t2704578078 * __this, Action_1_t2506377460 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_UserInfo::remove_OnProfileImageLoaded(System.Action`1<FB_UserInfo>)
extern "C"  void FB_UserInfo_remove_OnProfileImageLoaded_m2189714374 (FB_UserInfo_t2704578078 * __this, Action_1_t2506377460 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_UserInfo::InitializeData(System.Collections.IDictionary)
extern "C"  void FB_UserInfo_InitializeData_m3153822230 (FB_UserInfo_t2704578078 * __this, Il2CppObject * ___JSON0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_UserInfo::GetProfileUrl(FB_ProfileImageSize)
extern "C"  String_t* FB_UserInfo_GetProfileUrl_m1436903938 (FB_UserInfo_t2704578078 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D FB_UserInfo::GetProfileImage(FB_ProfileImageSize)
extern "C"  Texture2D_t3542995729 * FB_UserInfo_GetProfileImage_m2186983574 (FB_UserInfo_t2704578078 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_UserInfo::LoadProfileImage(FB_ProfileImageSize)
extern "C"  void FB_UserInfo_LoadProfileImage_m1238347435 (FB_UserInfo_t2704578078 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_UserInfo::get_RawJSON()
extern "C"  String_t* FB_UserInfo_get_RawJSON_m2870237619 (FB_UserInfo_t2704578078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_UserInfo::get_Id()
extern "C"  String_t* FB_UserInfo_get_Id_m1235095746 (FB_UserInfo_t2704578078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime FB_UserInfo::get_Birthday()
extern "C"  DateTime_t693205669  FB_UserInfo_get_Birthday_m649507746 (FB_UserInfo_t2704578078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_UserInfo::get_Name()
extern "C"  String_t* FB_UserInfo_get_Name_m1778651392 (FB_UserInfo_t2704578078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_UserInfo::get_FirstName()
extern "C"  String_t* FB_UserInfo_get_FirstName_m425766698 (FB_UserInfo_t2704578078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_UserInfo::get_LastName()
extern "C"  String_t* FB_UserInfo_get_LastName_m1857205264 (FB_UserInfo_t2704578078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_UserInfo::get_UserName()
extern "C"  String_t* FB_UserInfo_get_UserName_m3465753009 (FB_UserInfo_t2704578078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_UserInfo::get_ProfileUrl()
extern "C"  String_t* FB_UserInfo_get_ProfileUrl_m677323847 (FB_UserInfo_t2704578078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_UserInfo::get_Email()
extern "C"  String_t* FB_UserInfo_get_Email_m1467677539 (FB_UserInfo_t2704578078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_UserInfo::get_Locale()
extern "C"  String_t* FB_UserInfo_get_Locale_m1447404679 (FB_UserInfo_t2704578078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_UserInfo::get_Location()
extern "C"  String_t* FB_UserInfo_get_Location_m2466048240 (FB_UserInfo_t2704578078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FB_Gender FB_UserInfo::get_Gender()
extern "C"  int32_t FB_UserInfo_get_Gender_m641765586 (FB_UserInfo_t2704578078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_UserInfo::OnSquareImageLoaded(UnityEngine.Texture2D)
extern "C"  void FB_UserInfo_OnSquareImageLoaded_m3905903677 (FB_UserInfo_t2704578078 * __this, Texture2D_t3542995729 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_UserInfo::OnLargeImageLoaded(UnityEngine.Texture2D)
extern "C"  void FB_UserInfo_OnLargeImageLoaded_m1769266265 (FB_UserInfo_t2704578078 * __this, Texture2D_t3542995729 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_UserInfo::OnNormalImageLoaded(UnityEngine.Texture2D)
extern "C"  void FB_UserInfo_OnNormalImageLoaded_m2901266687 (FB_UserInfo_t2704578078 * __this, Texture2D_t3542995729 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_UserInfo::OnSmallImageLoaded(UnityEngine.Texture2D)
extern "C"  void FB_UserInfo_OnSmallImageLoaded_m704955179 (FB_UserInfo_t2704578078 * __this, Texture2D_t3542995729 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_UserInfo::<OnProfileImageLoaded>m__A2(FB_UserInfo)
extern "C"  void FB_UserInfo_U3COnProfileImageLoadedU3Em__A2_m1137993193 (Il2CppObject * __this /* static, unused */, FB_UserInfo_t2704578078 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
