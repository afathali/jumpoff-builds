﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_IdFactory_OLD/<>c__AnonStorey19
struct U3CU3Ec__AnonStorey19_t1266469818;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SA_IdFactory_OLD/<>c__AnonStorey19::.ctor()
extern "C"  void U3CU3Ec__AnonStorey19__ctor_m1473079557 (U3CU3Ec__AnonStorey19_t1266469818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char SA_IdFactory_OLD/<>c__AnonStorey19::<>m__B4(System.String)
extern "C"  Il2CppChar U3CU3Ec__AnonStorey19_U3CU3Em__B4_m1678050992 (U3CU3Ec__AnonStorey19_t1266469818 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
