﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_RTM_Result
struct GP_RTM_Result_t473289279;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_GamesStatusCodes1013506173.h"

// System.Void GP_RTM_Result::.ctor(System.String,System.String)
extern "C"  void GP_RTM_Result__ctor_m2853090252 (GP_RTM_Result_t473289279 * __this, String_t* ___r_status0, String_t* ___r_roomId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_GamesStatusCodes GP_RTM_Result::get_status()
extern "C"  int32_t GP_RTM_Result_get_status_m275124811 (GP_RTM_Result_t473289279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GP_RTM_Result::get_roomId()
extern "C"  String_t* GP_RTM_Result_get_roomId_m2052397116 (GP_RTM_Result_t473289279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
