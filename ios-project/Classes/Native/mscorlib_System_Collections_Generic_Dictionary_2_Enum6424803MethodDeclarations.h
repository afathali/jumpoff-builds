﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4102229300(__this, ___dictionary0, method) ((  void (*) (Enumerator_t6424803 *, Dictionary_2_t2981367397 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2319530937(__this, method) ((  Il2CppObject * (*) (Enumerator_t6424803 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m946715365(__this, method) ((  void (*) (Enumerator_t6424803 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2472894608(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t6424803 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3090946811(__this, method) ((  Il2CppObject * (*) (Enumerator_t6424803 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1706372251(__this, method) ((  Il2CppObject * (*) (Enumerator_t6424803 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::MoveNext()
#define Enumerator_MoveNext_m578969245(__this, method) ((  bool (*) (Enumerator_t6424803 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::get_Current()
#define Enumerator_get_Current_m3331173429(__this, method) ((  KeyValuePair_2_t738712619  (*) (Enumerator_t6424803 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m392892252(__this, method) ((  int32_t (*) (Enumerator_t6424803 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m183343532(__this, method) ((  CK_Record_t3973541762 * (*) (Enumerator_t6424803 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::Reset()
#define Enumerator_Reset_m3673046802(__this, method) ((  void (*) (Enumerator_t6424803 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::VerifyState()
#define Enumerator_VerifyState_m2095977279(__this, method) ((  void (*) (Enumerator_t6424803 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2652573933(__this, method) ((  void (*) (Enumerator_t6424803 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CK_Record>::Dispose()
#define Enumerator_Dispose_m517031108(__this, method) ((  void (*) (Enumerator_t6424803 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
