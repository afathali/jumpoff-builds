﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ReflectionUtils/<GetChildPrivateProperties>c__AnonStorey36
struct U3CGetChildPrivatePropertiesU3Ec__AnonStorey36_t1041854794;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"

// System.Void Newtonsoft.Json.Utilities.ReflectionUtils/<GetChildPrivateProperties>c__AnonStorey36::.ctor()
extern "C"  void U3CGetChildPrivatePropertiesU3Ec__AnonStorey36__ctor_m1518637163 (U3CGetChildPrivatePropertiesU3Ec__AnonStorey36_t1041854794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils/<GetChildPrivateProperties>c__AnonStorey36::<>m__103(System.Reflection.PropertyInfo)
extern "C"  bool U3CGetChildPrivatePropertiesU3Ec__AnonStorey36_U3CU3Em__103_m981400869 (U3CGetChildPrivatePropertiesU3Ec__AnonStorey36_t1041854794 * __this, PropertyInfo_t * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
