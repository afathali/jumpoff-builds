﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct KeyCollection_t547096553;
// System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct Dictionary_2_t2358566078;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEnumerator_1_t607539428;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t14295340;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke753102220.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m4147384596_gshared (KeyCollection_t547096553 * __this, Dictionary_2_t2358566078 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m4147384596(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t547096553 *, Dictionary_2_t2358566078 *, const MethodInfo*))KeyCollection__ctor_m4147384596_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1988457122_gshared (KeyCollection_t547096553 * __this, KeyValuePair_2_t3132015601  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1988457122(__this, ___item0, method) ((  void (*) (KeyCollection_t547096553 *, KeyValuePair_2_t3132015601 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1988457122_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1204074107_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1204074107(__this, method) ((  void (*) (KeyCollection_t547096553 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1204074107_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1316007466_gshared (KeyCollection_t547096553 * __this, KeyValuePair_2_t3132015601  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1316007466(__this, ___item0, method) ((  bool (*) (KeyCollection_t547096553 *, KeyValuePair_2_t3132015601 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1316007466_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m530240419_gshared (KeyCollection_t547096553 * __this, KeyValuePair_2_t3132015601  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m530240419(__this, ___item0, method) ((  bool (*) (KeyCollection_t547096553 *, KeyValuePair_2_t3132015601 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m530240419_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3039809725_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3039809725(__this, method) ((  Il2CppObject* (*) (KeyCollection_t547096553 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3039809725_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1974943017_gshared (KeyCollection_t547096553 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1974943017(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t547096553 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1974943017_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1790121746_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1790121746(__this, method) ((  Il2CppObject * (*) (KeyCollection_t547096553 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1790121746_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2634487951_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2634487951(__this, method) ((  bool (*) (KeyCollection_t547096553 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2634487951_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1603362373_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1603362373(__this, method) ((  bool (*) (KeyCollection_t547096553 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1603362373_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m963524697_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m963524697(__this, method) ((  Il2CppObject * (*) (KeyCollection_t547096553 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m963524697_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3856638403_gshared (KeyCollection_t547096553 * __this, KeyValuePair_2U5BU5D_t14295340* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3856638403(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t547096553 *, KeyValuePair_2U5BU5D_t14295340*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3856638403_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t753102220  KeyCollection_GetEnumerator_m2656796808_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m2656796808(__this, method) ((  Enumerator_t753102220  (*) (KeyCollection_t547096553 *, const MethodInfo*))KeyCollection_GetEnumerator_m2656796808_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2682794749_gshared (KeyCollection_t547096553 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2682794749(__this, method) ((  int32_t (*) (KeyCollection_t547096553 *, const MethodInfo*))KeyCollection_get_Count_m2682794749_gshared)(__this, method)
