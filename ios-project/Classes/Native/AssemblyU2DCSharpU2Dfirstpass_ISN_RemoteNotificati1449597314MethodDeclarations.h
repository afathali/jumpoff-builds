﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_RemoteNotification
struct ISN_RemoteNotification_t1449597314;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ISN_RemoteNotification::.ctor(System.String)
extern "C"  void ISN_RemoteNotification__ctor_m150142673 (ISN_RemoteNotification_t1449597314 * __this, String_t* ___body0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_RemoteNotification::get_Body()
extern "C"  String_t* ISN_RemoteNotification_get_Body_m1912314247 (ISN_RemoteNotification_t1449597314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
