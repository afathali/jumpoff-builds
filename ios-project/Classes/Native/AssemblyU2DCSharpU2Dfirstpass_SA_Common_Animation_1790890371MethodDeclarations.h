﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Animation.SA_iTween/CRSpline
struct CRSpline_t1790890371;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void SA.Common.Animation.SA_iTween/CRSpline::.ctor(UnityEngine.Vector3[])
extern "C"  void CRSpline__ctor_m2890324667 (CRSpline_t1790890371 * __this, Vector3U5BU5D_t1172311765* ___pts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SA.Common.Animation.SA_iTween/CRSpline::Interp(System.Single)
extern "C"  Vector3_t2243707580  CRSpline_Interp_m3086687675 (CRSpline_t1790890371 * __this, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
