﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_FilePickerResult
struct ISN_FilePickerResult_t2234803986;

#include "codegen/il2cpp-codegen.h"

// System.Void ISN_FilePickerResult::.ctor()
extern "C"  void ISN_FilePickerResult__ctor_m638559375 (ISN_FilePickerResult_t2234803986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
