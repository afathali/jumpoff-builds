﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlElementWrapper
struct XmlElementWrapper_t832553344;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// Newtonsoft.Json.Converters.IXmlNode
struct IXmlNode_t1152344546;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Newtonsoft.Json.Converters.XmlElementWrapper::.ctor(System.Xml.XmlElement)
extern "C"  void XmlElementWrapper__ctor_m4257468106 (XmlElementWrapper_t832553344 * __this, XmlElement_t2877111883 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlElementWrapper::SetAttributeNode(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  void XmlElementWrapper_SetAttributeNode_m1759033405 (XmlElementWrapper_t832553344 * __this, Il2CppObject * ___attribute0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlElementWrapper::GetPrefixOfNamespace(System.String)
extern "C"  String_t* XmlElementWrapper_GetPrefixOfNamespace_m4236691940 (XmlElementWrapper_t832553344 * __this, String_t* ___namespaceURI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
