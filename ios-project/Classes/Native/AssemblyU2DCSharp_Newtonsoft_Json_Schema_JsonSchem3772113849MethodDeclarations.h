﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t3772113849;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema>
struct IList_1_t18087154;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchema>
struct IDictionary_2_t3685976532;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2570160834;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t3093584614;
// System.Collections.Generic.IDictionary`2<Newtonsoft.Json.Linq.JToken,System.String>
struct IDictionary_2_t2133323376;
// Newtonsoft.Json.Linq.JToken
struct JToken_t2552644013;
// Newtonsoft.Json.JsonReader
struct JsonReader_t3154730733;
// Newtonsoft.Json.Schema.JsonSchemaResolver
struct JsonSchemaResolver_t3305548243;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t1973729997;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Nullable_1_gen5811492.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen2341081996.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3772113849.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken2552644013.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader3154730733.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3305548243.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter1973729997.h"

// System.Void Newtonsoft.Json.Schema.JsonSchema::.ctor()
extern "C"  void JsonSchema__ctor_m3010763260 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchema::get_Id()
extern "C"  String_t* JsonSchema_get_Id_m3741491087 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Id(System.String)
extern "C"  void JsonSchema_set_Id_m2171367056 (JsonSchema_t3772113849 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchema::get_Title()
extern "C"  String_t* JsonSchema_get_Title_m874212688 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Title(System.String)
extern "C"  void JsonSchema_set_Title_m1713815693 (JsonSchema_t3772113849 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Schema.JsonSchema::get_Required()
extern "C"  Nullable_1_t2088641033  JsonSchema_get_Required_m2869993915 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Required(System.Nullable`1<System.Boolean>)
extern "C"  void JsonSchema_set_Required_m3226223068 (JsonSchema_t3772113849 * __this, Nullable_1_t2088641033  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Schema.JsonSchema::get_ReadOnly()
extern "C"  Nullable_1_t2088641033  JsonSchema_get_ReadOnly_m1020502830 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_ReadOnly(System.Nullable`1<System.Boolean>)
extern "C"  void JsonSchema_set_ReadOnly_m2774368731 (JsonSchema_t3772113849 * __this, Nullable_1_t2088641033  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Schema.JsonSchema::get_Hidden()
extern "C"  Nullable_1_t2088641033  JsonSchema_get_Hidden_m3723721260 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Hidden(System.Nullable`1<System.Boolean>)
extern "C"  void JsonSchema_set_Hidden_m4158901467 (JsonSchema_t3772113849 * __this, Nullable_1_t2088641033  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Schema.JsonSchema::get_Transient()
extern "C"  Nullable_1_t2088641033  JsonSchema_get_Transient_m3048883308 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Transient(System.Nullable`1<System.Boolean>)
extern "C"  void JsonSchema_set_Transient_m972612609 (JsonSchema_t3772113849 * __this, Nullable_1_t2088641033  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchema::get_Description()
extern "C"  String_t* JsonSchema_get_Description_m344996480 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Description(System.String)
extern "C"  void JsonSchema_set_Description_m3122455335 (JsonSchema_t3772113849 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType> Newtonsoft.Json.Schema.JsonSchema::get_Type()
extern "C"  Nullable_1_t5811492  JsonSchema_get_Type_m2883270690 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Type(System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>)
extern "C"  void JsonSchema_set_Type_m1425870867 (JsonSchema_t3772113849 * __this, Nullable_1_t5811492  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchema::get_Pattern()
extern "C"  String_t* JsonSchema_get_Pattern_m3714647504 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Pattern(System.String)
extern "C"  void JsonSchema_set_Pattern_m1779796801 (JsonSchema_t3772113849 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Schema.JsonSchema::get_MinimumLength()
extern "C"  Nullable_1_t334943763  JsonSchema_get_MinimumLength_m3320509230 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_MinimumLength(System.Nullable`1<System.Int32>)
extern "C"  void JsonSchema_set_MinimumLength_m1361193357 (JsonSchema_t3772113849 * __this, Nullable_1_t334943763  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Schema.JsonSchema::get_MaximumLength()
extern "C"  Nullable_1_t334943763  JsonSchema_get_MaximumLength_m3482569808 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_MaximumLength(System.Nullable`1<System.Int32>)
extern "C"  void JsonSchema_set_MaximumLength_m523084627 (JsonSchema_t3772113849 * __this, Nullable_1_t334943763  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.Schema.JsonSchema::get_DivisibleBy()
extern "C"  Nullable_1_t2341081996  JsonSchema_get_DivisibleBy_m3928994679 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_DivisibleBy(System.Nullable`1<System.Double>)
extern "C"  void JsonSchema_set_DivisibleBy_m218190896 (JsonSchema_t3772113849 * __this, Nullable_1_t2341081996  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.Schema.JsonSchema::get_Minimum()
extern "C"  Nullable_1_t2341081996  JsonSchema_get_Minimum_m1448706413 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Minimum(System.Nullable`1<System.Double>)
extern "C"  void JsonSchema_set_Minimum_m4035726118 (JsonSchema_t3772113849 * __this, Nullable_1_t2341081996  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.Schema.JsonSchema::get_Maximum()
extern "C"  Nullable_1_t2341081996  JsonSchema_get_Maximum_m664458463 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Maximum(System.Nullable`1<System.Double>)
extern "C"  void JsonSchema_set_Maximum_m3113177452 (JsonSchema_t3772113849 * __this, Nullable_1_t2341081996  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Schema.JsonSchema::get_ExclusiveMinimum()
extern "C"  Nullable_1_t2088641033  JsonSchema_get_ExclusiveMinimum_m3009340056 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_ExclusiveMinimum(System.Nullable`1<System.Boolean>)
extern "C"  void JsonSchema_set_ExclusiveMinimum_m2867193625 (JsonSchema_t3772113849 * __this, Nullable_1_t2088641033  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Schema.JsonSchema::get_ExclusiveMaximum()
extern "C"  Nullable_1_t2088641033  JsonSchema_get_ExclusiveMaximum_m3794873026 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_ExclusiveMaximum(System.Nullable`1<System.Boolean>)
extern "C"  void JsonSchema_set_ExclusiveMaximum_m3840896195 (JsonSchema_t3772113849 * __this, Nullable_1_t2088641033  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Schema.JsonSchema::get_MinimumItems()
extern "C"  Nullable_1_t334943763  JsonSchema_get_MinimumItems_m2507246652 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_MinimumItems(System.Nullable`1<System.Int32>)
extern "C"  void JsonSchema_set_MinimumItems_m219900927 (JsonSchema_t3772113849 * __this, Nullable_1_t334943763  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Schema.JsonSchema::get_MaximumItems()
extern "C"  Nullable_1_t334943763  JsonSchema_get_MaximumItems_m2134755074 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_MaximumItems(System.Nullable`1<System.Int32>)
extern "C"  void JsonSchema_set_MaximumItems_m454648769 (JsonSchema_t3772113849 * __this, Nullable_1_t334943763  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema> Newtonsoft.Json.Schema.JsonSchema::get_Items()
extern "C"  Il2CppObject* JsonSchema_get_Items_m3908683370 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Items(System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema>)
extern "C"  void JsonSchema_set_Items_m3334622563 (JsonSchema_t3772113849 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchema> Newtonsoft.Json.Schema.JsonSchema::get_Properties()
extern "C"  Il2CppObject* JsonSchema_get_Properties_m2749152470 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Properties(System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchema>)
extern "C"  void JsonSchema_set_Properties_m1300453559 (JsonSchema_t3772113849 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchema::get_AdditionalProperties()
extern "C"  JsonSchema_t3772113849 * JsonSchema_get_AdditionalProperties_m1140031633 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_AdditionalProperties(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchema_set_AdditionalProperties_m3383646340 (JsonSchema_t3772113849 * __this, JsonSchema_t3772113849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchema> Newtonsoft.Json.Schema.JsonSchema::get_PatternProperties()
extern "C"  Il2CppObject* JsonSchema_get_PatternProperties_m2533542316 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_PatternProperties(System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchema>)
extern "C"  void JsonSchema_set_PatternProperties_m138617591 (JsonSchema_t3772113849 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchema::get_AllowAdditionalProperties()
extern "C"  bool JsonSchema_get_AllowAdditionalProperties_m163551640 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_AllowAdditionalProperties(System.Boolean)
extern "C"  void JsonSchema_set_AllowAdditionalProperties_m2151566665 (JsonSchema_t3772113849 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchema::get_Requires()
extern "C"  String_t* JsonSchema_get_Requires_m2359192216 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Requires(System.String)
extern "C"  void JsonSchema_set_Requires_m173035047 (JsonSchema_t3772113849 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.String> Newtonsoft.Json.Schema.JsonSchema::get_Identity()
extern "C"  Il2CppObject* JsonSchema_get_Identity_m2886249195 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Identity(System.Collections.Generic.IList`1<System.String>)
extern "C"  void JsonSchema_set_Identity_m4000398204 (JsonSchema_t3772113849 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Schema.JsonSchema::get_Enum()
extern "C"  Il2CppObject* JsonSchema_get_Enum_m1243887370 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Enum(System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>)
extern "C"  void JsonSchema_set_Enum_m2519575261 (JsonSchema_t3772113849 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<Newtonsoft.Json.Linq.JToken,System.String> Newtonsoft.Json.Schema.JsonSchema::get_Options()
extern "C"  Il2CppObject* JsonSchema_get_Options_m1023490304 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Options(System.Collections.Generic.IDictionary`2<Newtonsoft.Json.Linq.JToken,System.String>)
extern "C"  void JsonSchema_set_Options_m424557249 (JsonSchema_t3772113849 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType> Newtonsoft.Json.Schema.JsonSchema::get_Disallow()
extern "C"  Nullable_1_t5811492  JsonSchema_get_Disallow_m1923800995 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Disallow(System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>)
extern "C"  void JsonSchema_set_Disallow_m2024701050 (JsonSchema_t3772113849 * __this, Nullable_1_t5811492  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Schema.JsonSchema::get_Default()
extern "C"  JToken_t2552644013 * JsonSchema_get_Default_m1260793309 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Default(Newtonsoft.Json.Linq.JToken)
extern "C"  void JsonSchema_set_Default_m2979153988 (JsonSchema_t3772113849 * __this, JToken_t2552644013 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchema::get_Extends()
extern "C"  JsonSchema_t3772113849 * JsonSchema_get_Extends_m807745088 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Extends(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchema_set_Extends_m911960471 (JsonSchema_t3772113849 * __this, JsonSchema_t3772113849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchema::get_Format()
extern "C"  String_t* JsonSchema_get_Format_m3439002953 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::set_Format(System.String)
extern "C"  void JsonSchema_set_Format_m3640971082 (JsonSchema_t3772113849 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchema::get_InternalId()
extern "C"  String_t* JsonSchema_get_InternalId_m1065163020 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchema::Read(Newtonsoft.Json.JsonReader)
extern "C"  JsonSchema_t3772113849 * JsonSchema_Read_m323599324 (Il2CppObject * __this /* static, unused */, JsonReader_t3154730733 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchema::Read(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Schema.JsonSchemaResolver)
extern "C"  JsonSchema_t3772113849 * JsonSchema_Read_m3704579567 (Il2CppObject * __this /* static, unused */, JsonReader_t3154730733 * ___reader0, JsonSchemaResolver_t3305548243 * ___resolver1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchema::Parse(System.String)
extern "C"  JsonSchema_t3772113849 * JsonSchema_Parse_m3672769207 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchema::Parse(System.String,Newtonsoft.Json.Schema.JsonSchemaResolver)
extern "C"  JsonSchema_t3772113849 * JsonSchema_Parse_m4025586228 (Il2CppObject * __this /* static, unused */, String_t* ___json0, JsonSchemaResolver_t3305548243 * ___resolver1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::WriteTo(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonSchema_WriteTo_m1415777162 (JsonSchema_t3772113849 * __this, JsonWriter_t1973729997 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchema::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Schema.JsonSchemaResolver)
extern "C"  void JsonSchema_WriteTo_m2746842415 (JsonSchema_t3772113849 * __this, JsonWriter_t1973729997 * ___writer0, JsonSchemaResolver_t3305548243 * ___resolver1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchema::ToString()
extern "C"  String_t* JsonSchema_ToString_m539641929 (JsonSchema_t3772113849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
