﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBM_Game_Example
struct TBM_Game_Example_t857044347;
// GP_TBM_UpdateMatchResult
struct GP_TBM_UpdateMatchResult_t3943005969;
// AndroidActivityResult
struct AndroidActivityResult_t3757510801;
// GP_TBM_MatchInitiatedResult
struct GP_TBM_MatchInitiatedResult_t4144060847;
// GooglePlayConnectionResult
struct GooglePlayConnectionResult_t2758718724;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GP_TBM_UpdateMatchResult3943005969.h"
#include "AssemblyU2DCSharp_AndroidActivityResult3757510801.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchInitiatedResult4144060847.h"
#include "AssemblyU2DCSharp_GooglePlayConnectionResult2758718724.h"

// System.Void TBM_Game_Example::.ctor()
extern "C"  void TBM_Game_Example__ctor_m3447250840 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::Start()
extern "C"  void TBM_Game_Example_Start_m3705778460 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::Init()
extern "C"  void TBM_Game_Example_Init_m738027120 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::ActionMatchUpdated(GP_TBM_UpdateMatchResult)
extern "C"  void TBM_Game_Example_ActionMatchUpdated_m2510106821 (TBM_Game_Example_t857044347 * __this, GP_TBM_UpdateMatchResult_t3943005969 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::InitTBM()
extern "C"  void TBM_Game_Example_InitTBM_m995655939 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::ShowInboxUI()
extern "C"  void TBM_Game_Example_ShowInboxUI_m3500480251 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::FinishMathc()
extern "C"  void TBM_Game_Example_FinishMathc_m167673950 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::findMatch()
extern "C"  void TBM_Game_Example_findMatch_m3349850540 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::ActionMatchCreationCanceled(AndroidActivityResult)
extern "C"  void TBM_Game_Example_ActionMatchCreationCanceled_m2310517784 (TBM_Game_Example_t857044347 * __this, AndroidActivityResult_t3757510801 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::ActionMatchInitiated(GP_TBM_MatchInitiatedResult)
extern "C"  void TBM_Game_Example_ActionMatchInitiated_m3517981685 (TBM_Game_Example_t857044347 * __this, GP_TBM_MatchInitiatedResult_t4144060847 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::LoadAllMatchersInfo()
extern "C"  void TBM_Game_Example_LoadAllMatchersInfo_m2316657644 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::LoadActiveMatchesInfo()
extern "C"  void TBM_Game_Example_LoadActiveMatchesInfo_m3144665971 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::ConncetButtonPress()
extern "C"  void TBM_Game_Example_ConncetButtonPress_m3827027425 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::DrawParticipants()
extern "C"  void TBM_Game_Example_DrawParticipants_m2189234226 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::FixedUpdate()
extern "C"  void TBM_Game_Example_FixedUpdate_m2040812537 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::OnPlayerDisconnected()
extern "C"  void TBM_Game_Example_OnPlayerDisconnected_m698689517 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::OnPlayerConnected()
extern "C"  void TBM_Game_Example_OnPlayerConnected_m3375493239 (TBM_Game_Example_t857044347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBM_Game_Example::OnConnectionResult(GooglePlayConnectionResult)
extern "C"  void TBM_Game_Example_OnConnectionResult_m4234980698 (TBM_Game_Example_t857044347 * __this, GooglePlayConnectionResult_t2758718724 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
