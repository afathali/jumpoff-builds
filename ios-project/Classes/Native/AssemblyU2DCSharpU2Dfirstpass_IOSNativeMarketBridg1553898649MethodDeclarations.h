﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNativeMarketBridge
struct IOSNativeMarketBridge_t1553898649;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void IOSNativeMarketBridge::.ctor()
extern "C"  void IOSNativeMarketBridge__ctor_m3222944628 (IOSNativeMarketBridge_t1553898649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::loadStore(System.String)
extern "C"  void IOSNativeMarketBridge_loadStore_m2957974273 (Il2CppObject * __this /* static, unused */, String_t* ___ids0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::buyProduct(System.String)
extern "C"  void IOSNativeMarketBridge_buyProduct_m4275725953 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::restorePurchases()
extern "C"  void IOSNativeMarketBridge_restorePurchases_m2787390784 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::verifyLastPurchase(System.String)
extern "C"  void IOSNativeMarketBridge_verifyLastPurchase_m3396407114 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSNativeMarketBridge::ISN_InAppSettingState()
extern "C"  bool IOSNativeMarketBridge_ISN_InAppSettingState_m245653116 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IOSNativeMarketBridge::CloudService_AuthorizationStatus()
extern "C"  int32_t IOSNativeMarketBridge_CloudService_AuthorizationStatus_m408060408 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::CloudService_RequestAuthorization()
extern "C"  void IOSNativeMarketBridge_CloudService_RequestAuthorization_m4114280947 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::CloudService_RequestCapabilities()
extern "C"  void IOSNativeMarketBridge_CloudService_RequestCapabilities_m1052414014 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeMarketBridge::CloudService_RequestStorefrontIdentifier()
extern "C"  void IOSNativeMarketBridge_CloudService_RequestStorefrontIdentifier_m2269887697 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
