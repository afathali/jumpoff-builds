﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>
struct Dictionary_2_t2777568976;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4097593678.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_534914198.h"
#include "AssemblyU2DCSharp_FB_ProfileImageSize3003328130.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m792485719_gshared (Enumerator_t4097593678 * __this, Dictionary_2_t2777568976 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m792485719(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4097593678 *, Dictionary_2_t2777568976 *, const MethodInfo*))Enumerator__ctor_m792485719_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3200901816_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3200901816(__this, method) ((  Il2CppObject * (*) (Enumerator_t4097593678 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3200901816_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m330730428_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m330730428(__this, method) ((  void (*) (Enumerator_t4097593678 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m330730428_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1253008017_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1253008017(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t4097593678 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1253008017_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3448913414_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3448913414(__this, method) ((  Il2CppObject * (*) (Enumerator_t4097593678 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3448913414_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m197227828_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m197227828(__this, method) ((  Il2CppObject * (*) (Enumerator_t4097593678 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m197227828_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3732010148_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3732010148(__this, method) ((  bool (*) (Enumerator_t4097593678 *, const MethodInfo*))Enumerator_MoveNext_m3732010148_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t534914198  Enumerator_get_Current_m4217501564_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4217501564(__this, method) ((  KeyValuePair_2_t534914198  (*) (Enumerator_t4097593678 *, const MethodInfo*))Enumerator_get_Current_m4217501564_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m2108057179_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2108057179(__this, method) ((  int32_t (*) (Enumerator_t4097593678 *, const MethodInfo*))Enumerator_get_CurrentKey_m2108057179_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1126126523_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1126126523(__this, method) ((  Il2CppObject * (*) (Enumerator_t4097593678 *, const MethodInfo*))Enumerator_get_CurrentValue_m1126126523_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m812636689_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method);
#define Enumerator_Reset_m812636689(__this, method) ((  void (*) (Enumerator_t4097593678 *, const MethodInfo*))Enumerator_Reset_m812636689_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m293136976_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m293136976(__this, method) ((  void (*) (Enumerator_t4097593678 *, const MethodInfo*))Enumerator_VerifyState_m293136976_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3753703020_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3753703020(__this, method) ((  void (*) (Enumerator_t4097593678 *, const MethodInfo*))Enumerator_VerifyCurrent_m3753703020_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FB_ProfileImageSize,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2418656635_gshared (Enumerator_t4097593678 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2418656635(__this, method) ((  void (*) (Enumerator_t4097593678 *, const MethodInfo*))Enumerator_Dispose_m2418656635_gshared)(__this, method)
