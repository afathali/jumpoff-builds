﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>
struct SortedDictionary_2_t2729314972;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary2594437238.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1182407323_gshared (Enumerator_t2594437238 * __this, SortedDictionary_2_t2729314972 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m1182407323(__this, ___dic0, method) ((  void (*) (Enumerator_t2594437238 *, SortedDictionary_2_t2729314972 *, const MethodInfo*))Enumerator__ctor_m1182407323_gshared)(__this, ___dic0, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2760730413_gshared (Enumerator_t2594437238 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2760730413(__this, method) ((  Il2CppObject * (*) (Enumerator_t2594437238 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2760730413_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m98389277_gshared (Enumerator_t2594437238 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m98389277(__this, method) ((  void (*) (Enumerator_t2594437238 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m98389277_gshared)(__this, method)
// TKey System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3011505015_gshared (Enumerator_t2594437238 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3011505015(__this, method) ((  int32_t (*) (Enumerator_t2594437238 *, const MethodInfo*))Enumerator_get_Current_m3011505015_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2389773001_gshared (Enumerator_t2594437238 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2389773001(__this, method) ((  bool (*) (Enumerator_t2594437238 *, const MethodInfo*))Enumerator_MoveNext_m2389773001_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1241611840_gshared (Enumerator_t2594437238 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1241611840(__this, method) ((  void (*) (Enumerator_t2594437238 *, const MethodInfo*))Enumerator_Dispose_m1241611840_gshared)(__this, method)
