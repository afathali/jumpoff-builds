﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SAOpenUrlOnClick
struct SAOpenUrlOnClick_t1829835980;

#include "codegen/il2cpp-codegen.h"

// System.Void SAOpenUrlOnClick::.ctor()
extern "C"  void SAOpenUrlOnClick__ctor_m1393320817 (SAOpenUrlOnClick_t1829835980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SAOpenUrlOnClick::OnClick()
extern "C"  void SAOpenUrlOnClick_OnClick_m4198488348 (SAOpenUrlOnClick_t1829835980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
