﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.IOSDeploy.VariableListed
struct VariableListed_t1912381035;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.IOSDeploy.VariableListed::.ctor()
extern "C"  void VariableListed__ctor_m2185474432 (VariableListed_t1912381035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
