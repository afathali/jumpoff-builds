﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t4220419316;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HorseSkinSelect
struct  HorseSkinSelect_t2411928240  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject HorseSkinSelect::environmentUI
	GameObject_t1756533147 * ___environmentUI_2;
	// UnityEngine.UI.Button HorseSkinSelect::horseSkin0_button
	Button_t2872111280 * ___horseSkin0_button_3;
	// UnityEngine.UI.Button HorseSkinSelect::horseSkin1_button
	Button_t2872111280 * ___horseSkin1_button_4;
	// UnityEngine.UI.Button HorseSkinSelect::horseSkin2_button
	Button_t2872111280 * ___horseSkin2_button_5;
	// UnityEngine.UI.Button HorseSkinSelect::horseSkin3_button
	Button_t2872111280 * ___horseSkin3_button_6;
	// UnityEngine.Material HorseSkinSelect::horseSkin0
	Material_t193706927 * ___horseSkin0_7;
	// UnityEngine.Material HorseSkinSelect::horseSkin1
	Material_t193706927 * ___horseSkin1_8;
	// UnityEngine.Material HorseSkinSelect::horseSkin2
	Material_t193706927 * ___horseSkin2_9;
	// UnityEngine.Material HorseSkinSelect::horseSkin3
	Material_t193706927 * ___horseSkin3_10;
	// UnityEngine.SkinnedMeshRenderer HorseSkinSelect::horseModel
	SkinnedMeshRenderer_t4220419316 * ___horseModel_11;
	// UnityEngine.GameObject HorseSkinSelect::unicornHorn
	GameObject_t1756533147 * ___unicornHorn_12;

public:
	inline static int32_t get_offset_of_environmentUI_2() { return static_cast<int32_t>(offsetof(HorseSkinSelect_t2411928240, ___environmentUI_2)); }
	inline GameObject_t1756533147 * get_environmentUI_2() const { return ___environmentUI_2; }
	inline GameObject_t1756533147 ** get_address_of_environmentUI_2() { return &___environmentUI_2; }
	inline void set_environmentUI_2(GameObject_t1756533147 * value)
	{
		___environmentUI_2 = value;
		Il2CppCodeGenWriteBarrier(&___environmentUI_2, value);
	}

	inline static int32_t get_offset_of_horseSkin0_button_3() { return static_cast<int32_t>(offsetof(HorseSkinSelect_t2411928240, ___horseSkin0_button_3)); }
	inline Button_t2872111280 * get_horseSkin0_button_3() const { return ___horseSkin0_button_3; }
	inline Button_t2872111280 ** get_address_of_horseSkin0_button_3() { return &___horseSkin0_button_3; }
	inline void set_horseSkin0_button_3(Button_t2872111280 * value)
	{
		___horseSkin0_button_3 = value;
		Il2CppCodeGenWriteBarrier(&___horseSkin0_button_3, value);
	}

	inline static int32_t get_offset_of_horseSkin1_button_4() { return static_cast<int32_t>(offsetof(HorseSkinSelect_t2411928240, ___horseSkin1_button_4)); }
	inline Button_t2872111280 * get_horseSkin1_button_4() const { return ___horseSkin1_button_4; }
	inline Button_t2872111280 ** get_address_of_horseSkin1_button_4() { return &___horseSkin1_button_4; }
	inline void set_horseSkin1_button_4(Button_t2872111280 * value)
	{
		___horseSkin1_button_4 = value;
		Il2CppCodeGenWriteBarrier(&___horseSkin1_button_4, value);
	}

	inline static int32_t get_offset_of_horseSkin2_button_5() { return static_cast<int32_t>(offsetof(HorseSkinSelect_t2411928240, ___horseSkin2_button_5)); }
	inline Button_t2872111280 * get_horseSkin2_button_5() const { return ___horseSkin2_button_5; }
	inline Button_t2872111280 ** get_address_of_horseSkin2_button_5() { return &___horseSkin2_button_5; }
	inline void set_horseSkin2_button_5(Button_t2872111280 * value)
	{
		___horseSkin2_button_5 = value;
		Il2CppCodeGenWriteBarrier(&___horseSkin2_button_5, value);
	}

	inline static int32_t get_offset_of_horseSkin3_button_6() { return static_cast<int32_t>(offsetof(HorseSkinSelect_t2411928240, ___horseSkin3_button_6)); }
	inline Button_t2872111280 * get_horseSkin3_button_6() const { return ___horseSkin3_button_6; }
	inline Button_t2872111280 ** get_address_of_horseSkin3_button_6() { return &___horseSkin3_button_6; }
	inline void set_horseSkin3_button_6(Button_t2872111280 * value)
	{
		___horseSkin3_button_6 = value;
		Il2CppCodeGenWriteBarrier(&___horseSkin3_button_6, value);
	}

	inline static int32_t get_offset_of_horseSkin0_7() { return static_cast<int32_t>(offsetof(HorseSkinSelect_t2411928240, ___horseSkin0_7)); }
	inline Material_t193706927 * get_horseSkin0_7() const { return ___horseSkin0_7; }
	inline Material_t193706927 ** get_address_of_horseSkin0_7() { return &___horseSkin0_7; }
	inline void set_horseSkin0_7(Material_t193706927 * value)
	{
		___horseSkin0_7 = value;
		Il2CppCodeGenWriteBarrier(&___horseSkin0_7, value);
	}

	inline static int32_t get_offset_of_horseSkin1_8() { return static_cast<int32_t>(offsetof(HorseSkinSelect_t2411928240, ___horseSkin1_8)); }
	inline Material_t193706927 * get_horseSkin1_8() const { return ___horseSkin1_8; }
	inline Material_t193706927 ** get_address_of_horseSkin1_8() { return &___horseSkin1_8; }
	inline void set_horseSkin1_8(Material_t193706927 * value)
	{
		___horseSkin1_8 = value;
		Il2CppCodeGenWriteBarrier(&___horseSkin1_8, value);
	}

	inline static int32_t get_offset_of_horseSkin2_9() { return static_cast<int32_t>(offsetof(HorseSkinSelect_t2411928240, ___horseSkin2_9)); }
	inline Material_t193706927 * get_horseSkin2_9() const { return ___horseSkin2_9; }
	inline Material_t193706927 ** get_address_of_horseSkin2_9() { return &___horseSkin2_9; }
	inline void set_horseSkin2_9(Material_t193706927 * value)
	{
		___horseSkin2_9 = value;
		Il2CppCodeGenWriteBarrier(&___horseSkin2_9, value);
	}

	inline static int32_t get_offset_of_horseSkin3_10() { return static_cast<int32_t>(offsetof(HorseSkinSelect_t2411928240, ___horseSkin3_10)); }
	inline Material_t193706927 * get_horseSkin3_10() const { return ___horseSkin3_10; }
	inline Material_t193706927 ** get_address_of_horseSkin3_10() { return &___horseSkin3_10; }
	inline void set_horseSkin3_10(Material_t193706927 * value)
	{
		___horseSkin3_10 = value;
		Il2CppCodeGenWriteBarrier(&___horseSkin3_10, value);
	}

	inline static int32_t get_offset_of_horseModel_11() { return static_cast<int32_t>(offsetof(HorseSkinSelect_t2411928240, ___horseModel_11)); }
	inline SkinnedMeshRenderer_t4220419316 * get_horseModel_11() const { return ___horseModel_11; }
	inline SkinnedMeshRenderer_t4220419316 ** get_address_of_horseModel_11() { return &___horseModel_11; }
	inline void set_horseModel_11(SkinnedMeshRenderer_t4220419316 * value)
	{
		___horseModel_11 = value;
		Il2CppCodeGenWriteBarrier(&___horseModel_11, value);
	}

	inline static int32_t get_offset_of_unicornHorn_12() { return static_cast<int32_t>(offsetof(HorseSkinSelect_t2411928240, ___unicornHorn_12)); }
	inline GameObject_t1756533147 * get_unicornHorn_12() const { return ___unicornHorn_12; }
	inline GameObject_t1756533147 ** get_address_of_unicornHorn_12() { return &___unicornHorn_12; }
	inline void set_unicornHorn_12(GameObject_t1756533147 * value)
	{
		___unicornHorn_12 = value;
		Il2CppCodeGenWriteBarrier(&___unicornHorn_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
