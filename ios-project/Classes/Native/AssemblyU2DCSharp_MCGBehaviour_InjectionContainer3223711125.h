﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>
struct Dictionary_2_t1518733119;
// System.Func`2<System.Reflection.ConstructorInfo,System.Int32>
struct Func_2_t589981571;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MCGBehaviour/InjectionContainer
struct  InjectionContainer_t3223711125  : public Il2CppObject
{
public:

public:
};

struct InjectionContainer_t3223711125_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>> MCGBehaviour/InjectionContainer::_map
	Dictionary_2_t1518733119 * ____map_0;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Int32> MCGBehaviour/InjectionContainer::<>f__am$cache1
	Func_2_t589981571 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of__map_0() { return static_cast<int32_t>(offsetof(InjectionContainer_t3223711125_StaticFields, ____map_0)); }
	inline Dictionary_2_t1518733119 * get__map_0() const { return ____map_0; }
	inline Dictionary_2_t1518733119 ** get_address_of__map_0() { return &____map_0; }
	inline void set__map_0(Dictionary_2_t1518733119 * value)
	{
		____map_0 = value;
		Il2CppCodeGenWriteBarrier(&____map_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(InjectionContainer_t3223711125_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t589981571 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t589981571 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t589981571 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
