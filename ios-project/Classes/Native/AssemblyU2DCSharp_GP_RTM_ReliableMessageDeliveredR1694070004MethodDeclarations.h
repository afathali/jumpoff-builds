﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_RTM_ReliableMessageDeliveredResult
struct GP_RTM_ReliableMessageDeliveredResult_t1694070004;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_RTM_ReliableMessageDeliveredResult::.ctor(System.String,System.String,System.Int32,System.Byte[])
extern "C"  void GP_RTM_ReliableMessageDeliveredResult__ctor_m2791991655 (GP_RTM_ReliableMessageDeliveredResult_t1694070004 * __this, String_t* ___status0, String_t* ___roomId1, int32_t ___messageTokedId2, ByteU5BU5D_t3397334013* ___data3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GP_RTM_ReliableMessageDeliveredResult::get_MessageTokenId()
extern "C"  int32_t GP_RTM_ReliableMessageDeliveredResult_get_MessageTokenId_m424097263 (GP_RTM_ReliableMessageDeliveredResult_t1694070004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GP_RTM_ReliableMessageDeliveredResult::get_Data()
extern "C"  ByteU5BU5D_t3397334013* GP_RTM_ReliableMessageDeliveredResult_get_Data_m1828470474 (GP_RTM_ReliableMessageDeliveredResult_t1694070004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
