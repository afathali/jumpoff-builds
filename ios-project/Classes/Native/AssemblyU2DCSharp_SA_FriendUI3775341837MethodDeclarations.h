﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_FriendUI
struct SA_FriendUI_t3775341837;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SA_FriendUI::.ctor()
extern "C"  void SA_FriendUI__ctor_m3715441514 (SA_FriendUI_t3775341837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_FriendUI::Awake()
extern "C"  void SA_FriendUI_Awake_m1194151641 (SA_FriendUI_t3775341837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_FriendUI::SetFriendId(System.String)
extern "C"  void SA_FriendUI_SetFriendId_m3742933071 (SA_FriendUI_t3775341837 * __this, String_t* ___pId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_FriendUI::PlayWithFried()
extern "C"  void SA_FriendUI_PlayWithFried_m477465426 (SA_FriendUI_t3775341837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_FriendUI::FixedUpdate()
extern "C"  void SA_FriendUI_FixedUpdate_m4144063043 (SA_FriendUI_t3775341837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
