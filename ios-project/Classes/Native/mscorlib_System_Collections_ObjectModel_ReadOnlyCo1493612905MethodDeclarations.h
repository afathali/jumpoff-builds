﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>
struct ReadOnlyCollection_1_t1493612905;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JTokenType>
struct IList_1_t1848767814;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t1368357152;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JTokenType>
struct IEnumerator_1_t3078318336;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType1307827213.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3264052564_gshared (ReadOnlyCollection_1_t1493612905 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3264052564(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1493612905 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3264052564_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2199709776_gshared (ReadOnlyCollection_1_t1493612905 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2199709776(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1493612905 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2199709776_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1983668100_gshared (ReadOnlyCollection_1_t1493612905 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1983668100(__this, method) ((  void (*) (ReadOnlyCollection_1_t1493612905 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1983668100_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m935188427_gshared (ReadOnlyCollection_1_t1493612905 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m935188427(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1493612905 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m935188427_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3498920851_gshared (ReadOnlyCollection_1_t1493612905 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3498920851(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1493612905 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3498920851_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1399976695_gshared (ReadOnlyCollection_1_t1493612905 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1399976695(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1493612905 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1399976695_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m712715191_gshared (ReadOnlyCollection_1_t1493612905 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m712715191(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1493612905 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m712715191_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1711360450_gshared (ReadOnlyCollection_1_t1493612905 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1711360450(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1493612905 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1711360450_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3071444410_gshared (ReadOnlyCollection_1_t1493612905 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3071444410(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1493612905 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3071444410_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m372577701_gshared (ReadOnlyCollection_1_t1493612905 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m372577701(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1493612905 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m372577701_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m843261662_gshared (ReadOnlyCollection_1_t1493612905 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m843261662(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1493612905 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m843261662_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m411261465_gshared (ReadOnlyCollection_1_t1493612905 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m411261465(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1493612905 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m411261465_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3271026821_gshared (ReadOnlyCollection_1_t1493612905 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3271026821(__this, method) ((  void (*) (ReadOnlyCollection_1_t1493612905 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3271026821_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2289984965_gshared (ReadOnlyCollection_1_t1493612905 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2289984965(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1493612905 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2289984965_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3858058951_gshared (ReadOnlyCollection_1_t1493612905 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3858058951(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1493612905 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3858058951_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m469998992_gshared (ReadOnlyCollection_1_t1493612905 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m469998992(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1493612905 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m469998992_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1819912730_gshared (ReadOnlyCollection_1_t1493612905 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1819912730(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1493612905 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1819912730_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m456103950_gshared (ReadOnlyCollection_1_t1493612905 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m456103950(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1493612905 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m456103950_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1975057593_gshared (ReadOnlyCollection_1_t1493612905 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1975057593(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1493612905 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1975057593_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3496222341_gshared (ReadOnlyCollection_1_t1493612905 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3496222341(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1493612905 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3496222341_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3911705992_gshared (ReadOnlyCollection_1_t1493612905 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3911705992(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1493612905 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3911705992_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2530457821_gshared (ReadOnlyCollection_1_t1493612905 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2530457821(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1493612905 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2530457821_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m4286738460_gshared (ReadOnlyCollection_1_t1493612905 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m4286738460(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1493612905 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m4286738460_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m653077519_gshared (ReadOnlyCollection_1_t1493612905 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m653077519(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1493612905 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m653077519_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1628983870_gshared (ReadOnlyCollection_1_t1493612905 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1628983870(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1493612905 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m1628983870_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2653638788_gshared (ReadOnlyCollection_1_t1493612905 * __this, JTokenTypeU5BU5D_t1368357152* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2653638788(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1493612905 *, JTokenTypeU5BU5D_t1368357152*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2653638788_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2727944257_gshared (ReadOnlyCollection_1_t1493612905 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2727944257(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1493612905 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2727944257_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m4138424350_gshared (ReadOnlyCollection_1_t1493612905 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m4138424350(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1493612905 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m4138424350_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1663385785_gshared (ReadOnlyCollection_1_t1493612905 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1663385785(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1493612905 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1663385785_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m2301519355_gshared (ReadOnlyCollection_1_t1493612905 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2301519355(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1493612905 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2301519355_gshared)(__this, ___index0, method)
