﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidABChat
struct AndroidABChat_t2851498654;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidABChat::.ctor()
extern "C"  void AndroidABChat__ctor_m1518483029 (AndroidABChat_t2851498654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
