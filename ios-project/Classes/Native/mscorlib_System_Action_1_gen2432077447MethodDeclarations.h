﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"

// System.Void System.Action`1<DeleteCourseEventArgs>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m743332954(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t2432077447 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m584977596_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<DeleteCourseEventArgs>::Invoke(T)
#define Action_1_Invoke_m1451153613(__this, ___obj0, method) ((  void (*) (Action_1_t2432077447 *, DeleteCourseEventArgs_t2630278065 *, const MethodInfo*))Action_1_Invoke_m101203496_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<DeleteCourseEventArgs>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m1589283996(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t2432077447 *, DeleteCourseEventArgs_t2630278065 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1305519803_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<DeleteCourseEventArgs>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m118703599(__this, ___result0, method) ((  void (*) (Action_1_t2432077447 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m2057605070_gshared)(__this, ___result0, method)
