﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SASendMessageOnClick
struct SASendMessageOnClick_t243868668;

#include "codegen/il2cpp-codegen.h"

// System.Void SASendMessageOnClick::.ctor()
extern "C"  void SASendMessageOnClick__ctor_m3716668825 (SASendMessageOnClick_t243868668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SASendMessageOnClick::OnClick()
extern "C"  void SASendMessageOnClick_OnClick_m2253389876 (SASendMessageOnClick_t243868668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
