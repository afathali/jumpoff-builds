﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.Collections.Generic.List`1<Switch/ChangeCallback>
struct List_1_t541158982;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Switch
struct  Switch_t1718632940  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Switch::onImage
	GameObject_t1756533147 * ___onImage_2;
	// UnityEngine.UI.Button Switch::button
	Button_t2872111280 * ___button_3;
	// System.Boolean Switch::on
	bool ___on_4;
	// System.Collections.Generic.List`1<Switch/ChangeCallback> Switch::listeners
	List_1_t541158982 * ___listeners_5;

public:
	inline static int32_t get_offset_of_onImage_2() { return static_cast<int32_t>(offsetof(Switch_t1718632940, ___onImage_2)); }
	inline GameObject_t1756533147 * get_onImage_2() const { return ___onImage_2; }
	inline GameObject_t1756533147 ** get_address_of_onImage_2() { return &___onImage_2; }
	inline void set_onImage_2(GameObject_t1756533147 * value)
	{
		___onImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___onImage_2, value);
	}

	inline static int32_t get_offset_of_button_3() { return static_cast<int32_t>(offsetof(Switch_t1718632940, ___button_3)); }
	inline Button_t2872111280 * get_button_3() const { return ___button_3; }
	inline Button_t2872111280 ** get_address_of_button_3() { return &___button_3; }
	inline void set_button_3(Button_t2872111280 * value)
	{
		___button_3 = value;
		Il2CppCodeGenWriteBarrier(&___button_3, value);
	}

	inline static int32_t get_offset_of_on_4() { return static_cast<int32_t>(offsetof(Switch_t1718632940, ___on_4)); }
	inline bool get_on_4() const { return ___on_4; }
	inline bool* get_address_of_on_4() { return &___on_4; }
	inline void set_on_4(bool value)
	{
		___on_4 = value;
	}

	inline static int32_t get_offset_of_listeners_5() { return static_cast<int32_t>(offsetof(Switch_t1718632940, ___listeners_5)); }
	inline List_1_t541158982 * get_listeners_5() const { return ___listeners_5; }
	inline List_1_t541158982 ** get_address_of_listeners_5() { return &___listeners_5; }
	inline void set_listeners_5(List_1_t541158982 * value)
	{
		___listeners_5 = value;
		Il2CppCodeGenWriteBarrier(&___listeners_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
