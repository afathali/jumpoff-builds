﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_Logger
struct ISN_Logger_t85856405;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"

// System.Void ISN_Logger::.ctor()
extern "C"  void ISN_Logger__ctor_m2010489596 (ISN_Logger_t85856405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Logger::_ISN_NativeLog(System.String)
extern "C"  void ISN_Logger__ISN_NativeLog_m2080972529 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Logger::_ISN_SetLogState(System.Boolean)
extern "C"  void ISN_Logger__ISN_SetLogState_m2793170434 (Il2CppObject * __this /* static, unused */, bool ___isEnabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Logger::Awake()
extern "C"  void ISN_Logger_Awake_m124517239 (ISN_Logger_t85856405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Logger::Create()
extern "C"  void ISN_Logger_Create_m3698344238 (ISN_Logger_t85856405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Logger::Log(System.Object,UnityEngine.LogType)
extern "C"  void ISN_Logger_Log_m3550720543 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, int32_t ___logType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Logger::ISNEditorLog(UnityEngine.LogType,System.Object)
extern "C"  void ISN_Logger_ISNEditorLog_m2607564762 (Il2CppObject * __this /* static, unused */, int32_t ___logType0, Il2CppObject * ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
