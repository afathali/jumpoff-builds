﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_SoomlaGrow
struct AN_SoomlaGrow_t3055280006;
// System.Action
struct Action_t3226471752;
// System.String
struct String_t;
// FB_Result
struct FB_Result_t838248372;
// FB_PostResult
struct FB_PostResult_t992884730;
// TWResult
struct TWResult_t1480791060;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AN_SoomlaEventType3710443565.h"
#include "AssemblyU2DCSharp_AN_SoomlaSocialProvider1447552007.h"
#include "AssemblyU2DCSharp_FB_Result838248372.h"
#include "AssemblyU2DCSharp_FB_PostResult992884730.h"
#include "AssemblyU2DCSharp_TWResult1480791060.h"

// System.Void AN_SoomlaGrow::.ctor()
extern "C"  void AN_SoomlaGrow__ctor_m325507181 (AN_SoomlaGrow_t3055280006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::.cctor()
extern "C"  void AN_SoomlaGrow__cctor_m575060552 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::add_ActionInitialized(System.Action)
extern "C"  void AN_SoomlaGrow_add_ActionInitialized_m3795144996 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::remove_ActionInitialized(System.Action)
extern "C"  void AN_SoomlaGrow_remove_ActionInitialized_m2753266487 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::add_ActionConnected(System.Action)
extern "C"  void AN_SoomlaGrow_add_ActionConnected_m1618947875 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::remove_ActionConnected(System.Action)
extern "C"  void AN_SoomlaGrow_remove_ActionConnected_m3619670758 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::add_ActionDisconnected(System.Action)
extern "C"  void AN_SoomlaGrow_add_ActionDisconnected_m4210638345 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::remove_ActionDisconnected(System.Action)
extern "C"  void AN_SoomlaGrow_remove_ActionDisconnected_m4060298540 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::CreateListner()
extern "C"  void AN_SoomlaGrow_CreateListner_m3336968216 (AN_SoomlaGrow_t3055280006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::Init()
extern "C"  void AN_SoomlaGrow_Init_m1316723635 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::PurchaseStarted(System.String)
extern "C"  void AN_SoomlaGrow_PurchaseStarted_m161326549 (Il2CppObject * __this /* static, unused */, String_t* ___prodcutId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::PurchaseFinished(System.String,System.Int64,System.String)
extern "C"  void AN_SoomlaGrow_PurchaseFinished_m2010129372 (Il2CppObject * __this /* static, unused */, String_t* ___prodcutId0, int64_t ___priceInMicros1, String_t* ___currency2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::PurchaseCanceled(System.String)
extern "C"  void AN_SoomlaGrow_PurchaseCanceled_m213467251 (Il2CppObject * __this /* static, unused */, String_t* ___prodcutId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::SetPurhsesSupportedState(System.Boolean)
extern "C"  void AN_SoomlaGrow_SetPurhsesSupportedState_m2794954891 (Il2CppObject * __this /* static, unused */, bool ___isSupported0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::PurchaseError()
extern "C"  void AN_SoomlaGrow_PurchaseError_m415510502 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::FriendsRequest(AN_SoomlaEventType,AN_SoomlaSocialProvider)
extern "C"  void AN_SoomlaGrow_FriendsRequest_m2755349555 (Il2CppObject * __this /* static, unused */, int32_t ___eventType0, int32_t ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::SocialLogin(AN_SoomlaEventType,AN_SoomlaSocialProvider)
extern "C"  void AN_SoomlaGrow_SocialLogin_m3346374115 (Il2CppObject * __this /* static, unused */, int32_t ___eventType0, int32_t ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::SocialLoginFinished(AN_SoomlaSocialProvider,System.String)
extern "C"  void AN_SoomlaGrow_SocialLoginFinished_m2597920542 (Il2CppObject * __this /* static, unused */, int32_t ___provider0, String_t* ___ProfileId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::SocialLogOut(AN_SoomlaEventType,AN_SoomlaSocialProvider)
extern "C"  void AN_SoomlaGrow_SocialLogOut_m399205868 (Il2CppObject * __this /* static, unused */, int32_t ___eventType0, int32_t ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::SocialShare(AN_SoomlaEventType,AN_SoomlaSocialProvider)
extern "C"  void AN_SoomlaGrow_SocialShare_m2609851197 (Il2CppObject * __this /* static, unused */, int32_t ___eventType0, int32_t ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::FB_OnFriendsRequestStarted()
extern "C"  void AN_SoomlaGrow_FB_OnFriendsRequestStarted_m2332131086 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::FB_HandleOnFriendsDataRequestCompleteAction(FB_Result)
extern "C"  void AN_SoomlaGrow_FB_HandleOnFriendsDataRequestCompleteAction_m2244046818 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::FB_OnAuthCompleteAction(FB_Result)
extern "C"  void AN_SoomlaGrow_FB_OnAuthCompleteAction_m2835021224 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::FB_OnLoginStarted()
extern "C"  void AN_SoomlaGrow_FB_OnLoginStarted_m2514185609 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::FB_OnLogOut()
extern "C"  void AN_SoomlaGrow_FB_OnLogOut_m3901493393 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::FB_PostStarted()
extern "C"  void AN_SoomlaGrow_FB_PostStarted_m3985470215 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::FB_PostCompleted(FB_PostResult)
extern "C"  void AN_SoomlaGrow_FB_PostCompleted_m4054303569 (Il2CppObject * __this /* static, unused */, FB_PostResult_t992884730 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::HandleOnAuthCompleteAction(TWResult)
extern "C"  void AN_SoomlaGrow_HandleOnAuthCompleteAction_m3461834339 (Il2CppObject * __this /* static, unused */, TWResult_t1480791060 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::HandleOnUserDataRequestCompleteAction(TWResult)
extern "C"  void AN_SoomlaGrow_HandleOnUserDataRequestCompleteAction_m3873063383 (Il2CppObject * __this /* static, unused */, TWResult_t1480791060 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::OnTwitterLoginStarted()
extern "C"  void AN_SoomlaGrow_OnTwitterLoginStarted_m3554597011 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::OnTwitterLogOut()
extern "C"  void AN_SoomlaGrow_OnTwitterLogOut_m2177824539 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::TW_PostStarted()
extern "C"  void AN_SoomlaGrow_TW_PostStarted_m644865728 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::TW_PostCompleted(TWResult)
extern "C"  void AN_SoomlaGrow_TW_PostCompleted_m2584191132 (Il2CppObject * __this /* static, unused */, TWResult_t1480791060 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AN_SoomlaGrow::CheckState()
extern "C"  bool AN_SoomlaGrow_CheckState_m1785118658 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::OnInitialized()
extern "C"  void AN_SoomlaGrow_OnInitialized_m3457180652 (AN_SoomlaGrow_t3055280006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::OnConnected()
extern "C"  void AN_SoomlaGrow_OnConnected_m1716367031 (AN_SoomlaGrow_t3055280006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::OnDisconnected()
extern "C"  void AN_SoomlaGrow_OnDisconnected_m2258814381 (AN_SoomlaGrow_t3055280006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::<ActionInitialized>m__27()
extern "C"  void AN_SoomlaGrow_U3CActionInitializedU3Em__27_m4227925589 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::<ActionConnected>m__28()
extern "C"  void AN_SoomlaGrow_U3CActionConnectedU3Em__28_m1621635737 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaGrow::<ActionDisconnected>m__29()
extern "C"  void AN_SoomlaGrow_U3CActionDisconnectedU3Em__29_m3590397574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
