﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>
struct U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::.ctor()
extern "C"  void U3CCreateDistinctIteratorU3Ec__Iterator3_1__ctor_m1619070986_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method);
#define U3CCreateDistinctIteratorU3Ec__Iterator3_1__ctor_m1619070986(__this, method) ((  void (*) (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *, const MethodInfo*))U3CCreateDistinctIteratorU3Ec__Iterator3_1__ctor_m1619070986_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m4167922749_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method);
#define U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m4167922749(__this, method) ((  Il2CppObject * (*) (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *, const MethodInfo*))U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m4167922749_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_IEnumerator_get_Current_m2592365566_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method);
#define U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_IEnumerator_get_Current_m2592365566(__this, method) ((  Il2CppObject * (*) (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *, const MethodInfo*))U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_IEnumerator_get_Current_m2592365566_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_IEnumerable_GetEnumerator_m4009283937_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method);
#define U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_IEnumerable_GetEnumerator_m4009283937(__this, method) ((  Il2CppObject * (*) (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *, const MethodInfo*))U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_IEnumerable_GetEnumerator_m4009283937_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m193659212_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method);
#define U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m193659212(__this, method) ((  Il2CppObject* (*) (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *, const MethodInfo*))U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m193659212_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::MoveNext()
extern "C"  bool U3CCreateDistinctIteratorU3Ec__Iterator3_1_MoveNext_m270250114_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method);
#define U3CCreateDistinctIteratorU3Ec__Iterator3_1_MoveNext_m270250114(__this, method) ((  bool (*) (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *, const MethodInfo*))U3CCreateDistinctIteratorU3Ec__Iterator3_1_MoveNext_m270250114_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::Dispose()
extern "C"  void U3CCreateDistinctIteratorU3Ec__Iterator3_1_Dispose_m2370563095_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method);
#define U3CCreateDistinctIteratorU3Ec__Iterator3_1_Dispose_m2370563095(__this, method) ((  void (*) (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *, const MethodInfo*))U3CCreateDistinctIteratorU3Ec__Iterator3_1_Dispose_m2370563095_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::Reset()
extern "C"  void U3CCreateDistinctIteratorU3Ec__Iterator3_1_Reset_m3955730001_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method);
#define U3CCreateDistinctIteratorU3Ec__Iterator3_1_Reset_m3955730001(__this, method) ((  void (*) (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *, const MethodInfo*))U3CCreateDistinctIteratorU3Ec__Iterator3_1_Reset_m3955730001_gshared)(__this, method)
