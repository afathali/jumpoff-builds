﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m954522911(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3708467212 *, Dictionary_2_t2388442510 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2791086448(__this, method) ((  Il2CppObject * (*) (Enumerator_t3708467212 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m609015558(__this, method) ((  void (*) (Enumerator_t3708467212 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2182274945(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3708467212 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1915589742(__this, method) ((  Il2CppObject * (*) (Enumerator_t3708467212 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m958400036(__this, method) ((  Il2CppObject * (*) (Enumerator_t3708467212 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::MoveNext()
#define Enumerator_MoveNext_m3987541989(__this, method) ((  bool (*) (Enumerator_t3708467212 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::get_Current()
#define Enumerator_get_Current_m1508486528(__this, method) ((  KeyValuePair_2_t145787732  (*) (Enumerator_t3708467212 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3656404587(__this, method) ((  int32_t (*) (Enumerator_t3708467212 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2362301507(__this, method) ((  AN_ActivityTemplate_t3380616875 * (*) (Enumerator_t3708467212 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::Reset()
#define Enumerator_Reset_m4049979273(__this, method) ((  void (*) (Enumerator_t3708467212 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::VerifyState()
#define Enumerator_VerifyState_m54439478(__this, method) ((  void (*) (Enumerator_t3708467212 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m153111094(__this, method) ((  void (*) (Enumerator_t3708467212 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,AN_ActivityTemplate>::Dispose()
#define Enumerator_Dispose_m1677888731(__this, method) ((  void (*) (Enumerator_t3708467212 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
