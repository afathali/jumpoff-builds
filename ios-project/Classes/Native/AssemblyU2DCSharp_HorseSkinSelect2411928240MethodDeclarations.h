﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HorseSkinSelect
struct HorseSkinSelect_t2411928240;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"

// System.Void HorseSkinSelect::.ctor()
extern "C"  void HorseSkinSelect__ctor_m1289637531 (HorseSkinSelect_t2411928240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HorseSkinSelect::Start()
extern "C"  void HorseSkinSelect_Start_m1206695479 (HorseSkinSelect_t2411928240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HorseSkinSelect::<Start>m__B8(UnityEngine.EventSystems.BaseEventData)
extern "C"  void HorseSkinSelect_U3CStartU3Em__B8_m562301638 (HorseSkinSelect_t2411928240 * __this, BaseEventData_t2681005625 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HorseSkinSelect::<Start>m__B9(UnityEngine.EventSystems.BaseEventData)
extern "C"  void HorseSkinSelect_U3CStartU3Em__B9_m526783109 (HorseSkinSelect_t2411928240 * __this, BaseEventData_t2681005625 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HorseSkinSelect::<Start>m__BA(UnityEngine.EventSystems.BaseEventData)
extern "C"  void HorseSkinSelect_U3CStartU3Em__BA_m3880996829 (HorseSkinSelect_t2411928240 * __this, BaseEventData_t2681005625 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HorseSkinSelect::<Start>m__BB(UnityEngine.EventSystems.BaseEventData)
extern "C"  void HorseSkinSelect_U3CStartU3Em__BB_m718608160 (HorseSkinSelect_t2411928240 * __this, BaseEventData_t2681005625 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
