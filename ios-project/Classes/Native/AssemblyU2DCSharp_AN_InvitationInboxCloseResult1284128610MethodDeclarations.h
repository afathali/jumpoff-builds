﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_InvitationInboxCloseResult
struct AN_InvitationInboxCloseResult_t1284128610;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AdroidActivityResultCodes2848269969.h"

// System.Void AN_InvitationInboxCloseResult::.ctor(System.String)
extern "C"  void AN_InvitationInboxCloseResult__ctor_m4279496645 (AN_InvitationInboxCloseResult_t1284128610 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AdroidActivityResultCodes AN_InvitationInboxCloseResult::get_ResultCode()
extern "C"  int32_t AN_InvitationInboxCloseResult_get_ResultCode_m1020304576 (AN_InvitationInboxCloseResult_t1284128610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
