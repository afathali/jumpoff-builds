﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey2B
struct U3CTryConvertU3Ec__AnonStorey2B_t1202049151;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey2B::.ctor()
extern "C"  void U3CTryConvertU3Ec__AnonStorey2B__ctor_m392593142 (U3CTryConvertU3Ec__AnonStorey2B_t1202049151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey2B::<>m__ED()
extern "C"  Il2CppObject * U3CTryConvertU3Ec__AnonStorey2B_U3CU3Em__ED_m653345493 (U3CTryConvertU3Ec__AnonStorey2B_t1202049151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
