﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginRequest
struct LoginRequest_t2273172322;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LoginRequest::.ctor()
extern "C"  void LoginRequest__ctor_m500747817 (LoginRequest_t2273172322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LoginRequest::get_Type()
extern "C"  String_t* LoginRequest_get_Type_m2435799469 (LoginRequest_t2273172322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRequest::set_Type(System.String)
extern "C"  void LoginRequest_set_Type_m1791420946 (LoginRequest_t2273172322 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LoginRequest::get_Email()
extern "C"  String_t* LoginRequest_get_Email_m3519595189 (LoginRequest_t2273172322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRequest::set_Email(System.String)
extern "C"  void LoginRequest_set_Email_m1536085620 (LoginRequest_t2273172322 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LoginRequest::get_Password()
extern "C"  String_t* LoginRequest_get_Password_m2124583428 (LoginRequest_t2273172322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRequest::set_Password(System.String)
extern "C"  void LoginRequest_set_Password_m3341480301 (LoginRequest_t2273172322 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LoginRequest::get_AutoLogin()
extern "C"  String_t* LoginRequest_get_AutoLogin_m3763084293 (LoginRequest_t2273172322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRequest::set_AutoLogin(System.String)
extern "C"  void LoginRequest_set_AutoLogin_m4188047060 (LoginRequest_t2273172322 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
