﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// ShowErrorEventArgs/del
struct del_t1625342804;

#include "AssemblyU2DCSharp_MCGBehaviour3307770884.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ErrorMessage
struct  ErrorMessage_t521535845  : public MCGBehaviour_t3307770884
{
public:
	// UnityEngine.UI.Text ErrorMessage::Message
	Text_t356221433 * ___Message_4;
	// UnityEngine.UI.Button ErrorMessage::btn_close
	Button_t2872111280 * ___btn_close_5;
	// ShowErrorEventArgs/del ErrorMessage::onDismiss
	del_t1625342804 * ___onDismiss_6;

public:
	inline static int32_t get_offset_of_Message_4() { return static_cast<int32_t>(offsetof(ErrorMessage_t521535845, ___Message_4)); }
	inline Text_t356221433 * get_Message_4() const { return ___Message_4; }
	inline Text_t356221433 ** get_address_of_Message_4() { return &___Message_4; }
	inline void set_Message_4(Text_t356221433 * value)
	{
		___Message_4 = value;
		Il2CppCodeGenWriteBarrier(&___Message_4, value);
	}

	inline static int32_t get_offset_of_btn_close_5() { return static_cast<int32_t>(offsetof(ErrorMessage_t521535845, ___btn_close_5)); }
	inline Button_t2872111280 * get_btn_close_5() const { return ___btn_close_5; }
	inline Button_t2872111280 ** get_address_of_btn_close_5() { return &___btn_close_5; }
	inline void set_btn_close_5(Button_t2872111280 * value)
	{
		___btn_close_5 = value;
		Il2CppCodeGenWriteBarrier(&___btn_close_5, value);
	}

	inline static int32_t get_offset_of_onDismiss_6() { return static_cast<int32_t>(offsetof(ErrorMessage_t521535845, ___onDismiss_6)); }
	inline del_t1625342804 * get_onDismiss_6() const { return ___onDismiss_6; }
	inline del_t1625342804 ** get_address_of_onDismiss_6() { return &___onDismiss_6; }
	inline void set_onDismiss_6(del_t1625342804 * value)
	{
		___onDismiss_6 = value;
		Il2CppCodeGenWriteBarrier(&___onDismiss_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
