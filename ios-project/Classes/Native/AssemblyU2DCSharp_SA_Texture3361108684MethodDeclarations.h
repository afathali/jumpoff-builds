﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_Texture
struct SA_Texture_t3361108684;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"

// System.Void SA_Texture::.ctor()
extern "C"  void SA_Texture__ctor_m1318521237 (SA_Texture_t3361108684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Texture::Awake()
extern "C"  void SA_Texture_Awake_m3206153320 (SA_Texture_t3361108684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture SA_Texture::get_texture()
extern "C"  Texture_t2243626319 * SA_Texture_get_texture_m2652148574 (SA_Texture_t3361108684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Texture::set_texture(UnityEngine.Texture)
extern "C"  void SA_Texture_set_texture_m300389463 (SA_Texture_t3361108684 * __this, Texture_t2243626319 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
