﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CK_RecordDeleteResult
struct CK_RecordDeleteResult_t3248469484;
// System.String
struct String_t;
// CK_Database
struct CK_Database_t243306482;
// CK_RecordID
struct CK_RecordID_t41838833;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_Database243306482.h"

// System.Void CK_RecordDeleteResult::.ctor(System.Int32)
extern "C"  void CK_RecordDeleteResult__ctor_m4177819264 (CK_RecordDeleteResult_t3248469484 * __this, int32_t ___recordId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_RecordDeleteResult::.ctor(System.String)
extern "C"  void CK_RecordDeleteResult__ctor_m2929980089 (CK_RecordDeleteResult_t3248469484 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_RecordDeleteResult::SetDatabase(CK_Database)
extern "C"  void CK_RecordDeleteResult_SetDatabase_m935221056 (CK_RecordDeleteResult_t3248469484 * __this, CK_Database_t243306482 * ___database0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CK_Database CK_RecordDeleteResult::get_Database()
extern "C"  CK_Database_t243306482 * CK_RecordDeleteResult_get_Database_m4012482044 (CK_RecordDeleteResult_t3248469484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CK_RecordID CK_RecordDeleteResult::get_RecordID()
extern "C"  CK_RecordID_t41838833 * CK_RecordDeleteResult_get_RecordID_m1867908988 (CK_RecordDeleteResult_t3248469484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
