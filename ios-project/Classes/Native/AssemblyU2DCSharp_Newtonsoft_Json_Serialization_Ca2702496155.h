﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De1136768961.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver
struct  CamelCasePropertyNamesContractResolver_t2702496155  : public DefaultContractResolver_t1136768961
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
