﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Security.Cryptography.HMACSHA1
struct HMACSHA1_t1958407246;
// UnityEngine.WWW
struct WWW_t2919945039;
// TW_APIRequstResult
struct TW_APIRequstResult_t455151055;
// System.Object
struct Il2CppObject;
// TW_OAuthAPIRequest
struct TW_OAuthAPIRequest_t3823919588;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "System_System_Collections_Generic_SortedDictionary2667205588.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TW_OAuthAPIRequest/<Request>c__IteratorA
struct  U3CRequestU3Ec__IteratorA_t1902108118  : public Il2CppObject
{
public:
	// System.TimeSpan TW_OAuthAPIRequest/<Request>c__IteratorA::<ts>__0
	TimeSpan_t3430258949  ___U3CtsU3E__0_0;
	// System.String TW_OAuthAPIRequest/<Request>c__IteratorA::<oauth_consumer_key>__1
	String_t* ___U3Coauth_consumer_keyU3E__1_1;
	// System.String TW_OAuthAPIRequest/<Request>c__IteratorA::<oauth_token>__2
	String_t* ___U3Coauth_tokenU3E__2_2;
	// System.String TW_OAuthAPIRequest/<Request>c__IteratorA::<oauth_signature_method>__3
	String_t* ___U3Coauth_signature_methodU3E__3_3;
	// System.String TW_OAuthAPIRequest/<Request>c__IteratorA::<oauth_timestamp>__4
	String_t* ___U3Coauth_timestampU3E__4_4;
	// System.String TW_OAuthAPIRequest/<Request>c__IteratorA::<oauth_nonce>__5
	String_t* ___U3Coauth_nonceU3E__5_5;
	// System.String TW_OAuthAPIRequest/<Request>c__IteratorA::<oauth_version>__6
	String_t* ___U3Coauth_versionU3E__6_6;
	// System.String TW_OAuthAPIRequest/<Request>c__IteratorA::<baseString>__7
	String_t* ___U3CbaseStringU3E__7_7;
	// System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,System.String> TW_OAuthAPIRequest/<Request>c__IteratorA::<$s_193>__8
	Enumerator_t2667205588  ___U3CU24s_193U3E__8_8;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.String> TW_OAuthAPIRequest/<Request>c__IteratorA::<entry>__9
	KeyValuePair_2_t1701344717  ___U3CentryU3E__9_9;
	// System.String TW_OAuthAPIRequest/<Request>c__IteratorA::<consumerSecret>__10
	String_t* ___U3CconsumerSecretU3E__10_10;
	// System.String TW_OAuthAPIRequest/<Request>c__IteratorA::<oauth_token_secret>__11
	String_t* ___U3Coauth_token_secretU3E__11_11;
	// System.String TW_OAuthAPIRequest/<Request>c__IteratorA::<signingKey>__12
	String_t* ___U3CsigningKeyU3E__12_12;
	// System.Security.Cryptography.HMACSHA1 TW_OAuthAPIRequest/<Request>c__IteratorA::<hasher>__13
	HMACSHA1_t1958407246 * ___U3ChasherU3E__13_13;
	// System.String TW_OAuthAPIRequest/<Request>c__IteratorA::<signatureString>__14
	String_t* ___U3CsignatureStringU3E__14_14;
	// System.String TW_OAuthAPIRequest/<Request>c__IteratorA::<authorizationHeaderParams>__15
	String_t* ___U3CauthorizationHeaderParamsU3E__15_15;
	// UnityEngine.WWW TW_OAuthAPIRequest/<Request>c__IteratorA::<www>__16
	WWW_t2919945039 * ___U3CwwwU3E__16_16;
	// TW_APIRequstResult TW_OAuthAPIRequest/<Request>c__IteratorA::<result>__17
	TW_APIRequstResult_t455151055 * ___U3CresultU3E__17_17;
	// System.Int32 TW_OAuthAPIRequest/<Request>c__IteratorA::$PC
	int32_t ___U24PC_18;
	// System.Object TW_OAuthAPIRequest/<Request>c__IteratorA::$current
	Il2CppObject * ___U24current_19;
	// TW_OAuthAPIRequest TW_OAuthAPIRequest/<Request>c__IteratorA::<>f__this
	TW_OAuthAPIRequest_t3823919588 * ___U3CU3Ef__this_20;

public:
	inline static int32_t get_offset_of_U3CtsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3CtsU3E__0_0)); }
	inline TimeSpan_t3430258949  get_U3CtsU3E__0_0() const { return ___U3CtsU3E__0_0; }
	inline TimeSpan_t3430258949 * get_address_of_U3CtsU3E__0_0() { return &___U3CtsU3E__0_0; }
	inline void set_U3CtsU3E__0_0(TimeSpan_t3430258949  value)
	{
		___U3CtsU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Coauth_consumer_keyU3E__1_1() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3Coauth_consumer_keyU3E__1_1)); }
	inline String_t* get_U3Coauth_consumer_keyU3E__1_1() const { return ___U3Coauth_consumer_keyU3E__1_1; }
	inline String_t** get_address_of_U3Coauth_consumer_keyU3E__1_1() { return &___U3Coauth_consumer_keyU3E__1_1; }
	inline void set_U3Coauth_consumer_keyU3E__1_1(String_t* value)
	{
		___U3Coauth_consumer_keyU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3Coauth_consumer_keyU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3Coauth_tokenU3E__2_2() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3Coauth_tokenU3E__2_2)); }
	inline String_t* get_U3Coauth_tokenU3E__2_2() const { return ___U3Coauth_tokenU3E__2_2; }
	inline String_t** get_address_of_U3Coauth_tokenU3E__2_2() { return &___U3Coauth_tokenU3E__2_2; }
	inline void set_U3Coauth_tokenU3E__2_2(String_t* value)
	{
		___U3Coauth_tokenU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3Coauth_tokenU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3Coauth_signature_methodU3E__3_3() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3Coauth_signature_methodU3E__3_3)); }
	inline String_t* get_U3Coauth_signature_methodU3E__3_3() const { return ___U3Coauth_signature_methodU3E__3_3; }
	inline String_t** get_address_of_U3Coauth_signature_methodU3E__3_3() { return &___U3Coauth_signature_methodU3E__3_3; }
	inline void set_U3Coauth_signature_methodU3E__3_3(String_t* value)
	{
		___U3Coauth_signature_methodU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3Coauth_signature_methodU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3Coauth_timestampU3E__4_4() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3Coauth_timestampU3E__4_4)); }
	inline String_t* get_U3Coauth_timestampU3E__4_4() const { return ___U3Coauth_timestampU3E__4_4; }
	inline String_t** get_address_of_U3Coauth_timestampU3E__4_4() { return &___U3Coauth_timestampU3E__4_4; }
	inline void set_U3Coauth_timestampU3E__4_4(String_t* value)
	{
		___U3Coauth_timestampU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3Coauth_timestampU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3Coauth_nonceU3E__5_5() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3Coauth_nonceU3E__5_5)); }
	inline String_t* get_U3Coauth_nonceU3E__5_5() const { return ___U3Coauth_nonceU3E__5_5; }
	inline String_t** get_address_of_U3Coauth_nonceU3E__5_5() { return &___U3Coauth_nonceU3E__5_5; }
	inline void set_U3Coauth_nonceU3E__5_5(String_t* value)
	{
		___U3Coauth_nonceU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3Coauth_nonceU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3Coauth_versionU3E__6_6() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3Coauth_versionU3E__6_6)); }
	inline String_t* get_U3Coauth_versionU3E__6_6() const { return ___U3Coauth_versionU3E__6_6; }
	inline String_t** get_address_of_U3Coauth_versionU3E__6_6() { return &___U3Coauth_versionU3E__6_6; }
	inline void set_U3Coauth_versionU3E__6_6(String_t* value)
	{
		___U3Coauth_versionU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3Coauth_versionU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CbaseStringU3E__7_7() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3CbaseStringU3E__7_7)); }
	inline String_t* get_U3CbaseStringU3E__7_7() const { return ___U3CbaseStringU3E__7_7; }
	inline String_t** get_address_of_U3CbaseStringU3E__7_7() { return &___U3CbaseStringU3E__7_7; }
	inline void set_U3CbaseStringU3E__7_7(String_t* value)
	{
		___U3CbaseStringU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbaseStringU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U3CU24s_193U3E__8_8() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3CU24s_193U3E__8_8)); }
	inline Enumerator_t2667205588  get_U3CU24s_193U3E__8_8() const { return ___U3CU24s_193U3E__8_8; }
	inline Enumerator_t2667205588 * get_address_of_U3CU24s_193U3E__8_8() { return &___U3CU24s_193U3E__8_8; }
	inline void set_U3CU24s_193U3E__8_8(Enumerator_t2667205588  value)
	{
		___U3CU24s_193U3E__8_8 = value;
	}

	inline static int32_t get_offset_of_U3CentryU3E__9_9() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3CentryU3E__9_9)); }
	inline KeyValuePair_2_t1701344717  get_U3CentryU3E__9_9() const { return ___U3CentryU3E__9_9; }
	inline KeyValuePair_2_t1701344717 * get_address_of_U3CentryU3E__9_9() { return &___U3CentryU3E__9_9; }
	inline void set_U3CentryU3E__9_9(KeyValuePair_2_t1701344717  value)
	{
		___U3CentryU3E__9_9 = value;
	}

	inline static int32_t get_offset_of_U3CconsumerSecretU3E__10_10() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3CconsumerSecretU3E__10_10)); }
	inline String_t* get_U3CconsumerSecretU3E__10_10() const { return ___U3CconsumerSecretU3E__10_10; }
	inline String_t** get_address_of_U3CconsumerSecretU3E__10_10() { return &___U3CconsumerSecretU3E__10_10; }
	inline void set_U3CconsumerSecretU3E__10_10(String_t* value)
	{
		___U3CconsumerSecretU3E__10_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CconsumerSecretU3E__10_10, value);
	}

	inline static int32_t get_offset_of_U3Coauth_token_secretU3E__11_11() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3Coauth_token_secretU3E__11_11)); }
	inline String_t* get_U3Coauth_token_secretU3E__11_11() const { return ___U3Coauth_token_secretU3E__11_11; }
	inline String_t** get_address_of_U3Coauth_token_secretU3E__11_11() { return &___U3Coauth_token_secretU3E__11_11; }
	inline void set_U3Coauth_token_secretU3E__11_11(String_t* value)
	{
		___U3Coauth_token_secretU3E__11_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3Coauth_token_secretU3E__11_11, value);
	}

	inline static int32_t get_offset_of_U3CsigningKeyU3E__12_12() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3CsigningKeyU3E__12_12)); }
	inline String_t* get_U3CsigningKeyU3E__12_12() const { return ___U3CsigningKeyU3E__12_12; }
	inline String_t** get_address_of_U3CsigningKeyU3E__12_12() { return &___U3CsigningKeyU3E__12_12; }
	inline void set_U3CsigningKeyU3E__12_12(String_t* value)
	{
		___U3CsigningKeyU3E__12_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsigningKeyU3E__12_12, value);
	}

	inline static int32_t get_offset_of_U3ChasherU3E__13_13() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3ChasherU3E__13_13)); }
	inline HMACSHA1_t1958407246 * get_U3ChasherU3E__13_13() const { return ___U3ChasherU3E__13_13; }
	inline HMACSHA1_t1958407246 ** get_address_of_U3ChasherU3E__13_13() { return &___U3ChasherU3E__13_13; }
	inline void set_U3ChasherU3E__13_13(HMACSHA1_t1958407246 * value)
	{
		___U3ChasherU3E__13_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3ChasherU3E__13_13, value);
	}

	inline static int32_t get_offset_of_U3CsignatureStringU3E__14_14() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3CsignatureStringU3E__14_14)); }
	inline String_t* get_U3CsignatureStringU3E__14_14() const { return ___U3CsignatureStringU3E__14_14; }
	inline String_t** get_address_of_U3CsignatureStringU3E__14_14() { return &___U3CsignatureStringU3E__14_14; }
	inline void set_U3CsignatureStringU3E__14_14(String_t* value)
	{
		___U3CsignatureStringU3E__14_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsignatureStringU3E__14_14, value);
	}

	inline static int32_t get_offset_of_U3CauthorizationHeaderParamsU3E__15_15() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3CauthorizationHeaderParamsU3E__15_15)); }
	inline String_t* get_U3CauthorizationHeaderParamsU3E__15_15() const { return ___U3CauthorizationHeaderParamsU3E__15_15; }
	inline String_t** get_address_of_U3CauthorizationHeaderParamsU3E__15_15() { return &___U3CauthorizationHeaderParamsU3E__15_15; }
	inline void set_U3CauthorizationHeaderParamsU3E__15_15(String_t* value)
	{
		___U3CauthorizationHeaderParamsU3E__15_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CauthorizationHeaderParamsU3E__15_15, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__16_16() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3CwwwU3E__16_16)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__16_16() const { return ___U3CwwwU3E__16_16; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__16_16() { return &___U3CwwwU3E__16_16; }
	inline void set_U3CwwwU3E__16_16(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__16_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__16_16, value);
	}

	inline static int32_t get_offset_of_U3CresultU3E__17_17() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3CresultU3E__17_17)); }
	inline TW_APIRequstResult_t455151055 * get_U3CresultU3E__17_17() const { return ___U3CresultU3E__17_17; }
	inline TW_APIRequstResult_t455151055 ** get_address_of_U3CresultU3E__17_17() { return &___U3CresultU3E__17_17; }
	inline void set_U3CresultU3E__17_17(TW_APIRequstResult_t455151055 * value)
	{
		___U3CresultU3E__17_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresultU3E__17_17, value);
	}

	inline static int32_t get_offset_of_U24PC_18() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U24PC_18)); }
	inline int32_t get_U24PC_18() const { return ___U24PC_18; }
	inline int32_t* get_address_of_U24PC_18() { return &___U24PC_18; }
	inline void set_U24PC_18(int32_t value)
	{
		___U24PC_18 = value;
	}

	inline static int32_t get_offset_of_U24current_19() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U24current_19)); }
	inline Il2CppObject * get_U24current_19() const { return ___U24current_19; }
	inline Il2CppObject ** get_address_of_U24current_19() { return &___U24current_19; }
	inline void set_U24current_19(Il2CppObject * value)
	{
		___U24current_19 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_20() { return static_cast<int32_t>(offsetof(U3CRequestU3Ec__IteratorA_t1902108118, ___U3CU3Ef__this_20)); }
	inline TW_OAuthAPIRequest_t3823919588 * get_U3CU3Ef__this_20() const { return ___U3CU3Ef__this_20; }
	inline TW_OAuthAPIRequest_t3823919588 ** get_address_of_U3CU3Ef__this_20() { return &___U3CU3Ef__this_20; }
	inline void set_U3CU3Ef__this_20(TW_OAuthAPIRequest_t3823919588 * value)
	{
		___U3CU3Ef__this_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
