﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonObjectAttribute
struct JsonObjectAttribute_t162755825;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MemberSerializati687984360.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Newtonsoft.Json.JsonObjectAttribute::.ctor()
extern "C"  void JsonObjectAttribute__ctor_m3057840685 (JsonObjectAttribute_t162755825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonObjectAttribute::.ctor(Newtonsoft.Json.MemberSerialization)
extern "C"  void JsonObjectAttribute__ctor_m3385331060 (JsonObjectAttribute_t162755825 * __this, int32_t ___memberSerialization0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonObjectAttribute::.ctor(System.String)
extern "C"  void JsonObjectAttribute__ctor_m1099294459 (JsonObjectAttribute_t162755825 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.JsonObjectAttribute::get_MemberSerialization()
extern "C"  int32_t JsonObjectAttribute_get_MemberSerialization_m395518756 (JsonObjectAttribute_t162755825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonObjectAttribute::set_MemberSerialization(Newtonsoft.Json.MemberSerialization)
extern "C"  void JsonObjectAttribute_set_MemberSerialization_m72082891 (JsonObjectAttribute_t162755825 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
