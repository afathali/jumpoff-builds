﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonProperty
struct BsonProperty_t1491061775;
// Newtonsoft.Json.Bson.BsonString
struct BsonString_t3501425379;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_t3582361217;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonString3501425379.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonToken3582361217.h"

// System.Void Newtonsoft.Json.Bson.BsonProperty::.ctor()
extern "C"  void BsonProperty__ctor_m2539773957 (BsonProperty_t1491061775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonProperty::get_Name()
extern "C"  BsonString_t3501425379 * BsonProperty_get_Name_m1734025914 (BsonProperty_t1491061775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonProperty::set_Name(Newtonsoft.Json.Bson.BsonString)
extern "C"  void BsonProperty_set_Name_m1367004103 (BsonProperty_t1491061775 * __this, BsonString_t3501425379 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonProperty::get_Value()
extern "C"  BsonToken_t3582361217 * BsonProperty_get_Value_m1476537752 (BsonProperty_t1491061775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonProperty::set_Value(Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonProperty_set_Value_m235437509 (BsonProperty_t1491061775 * __this, BsonToken_t3582361217 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
