﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CreateCourseResponse
struct CreateCourseResponse_t3800683746;
// System.String
struct String_t;
// CreateCourseRequest
struct CreateCourseRequest_t4267998642;
// System.Collections.Generic.List`1<Course>
struct List_1_t2852233831;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_CreateCourseRequest4267998642.h"

// System.Void CreateCourseResponse::.ctor()
extern "C"  void CreateCourseResponse__ctor_m3459325329 (CreateCourseResponse_t3800683746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CreateCourseResponse::get_Success()
extern "C"  bool CreateCourseResponse_get_Success_m3448588643 (CreateCourseResponse_t3800683746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseResponse::set_Success(System.Boolean)
extern "C"  void CreateCourseResponse_set_Success_m3877827432 (CreateCourseResponse_t3800683746 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CreateCourseResponse::get_ErrorMessage()
extern "C"  String_t* CreateCourseResponse_get_ErrorMessage_m2661094128 (CreateCourseResponse_t3800683746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseResponse::set_ErrorMessage(System.String)
extern "C"  void CreateCourseResponse_set_ErrorMessage_m4002070213 (CreateCourseResponse_t3800683746 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CreateCourseRequest CreateCourseResponse::get_Request()
extern "C"  CreateCourseRequest_t4267998642 * CreateCourseResponse_get_Request_m2890763886 (CreateCourseResponse_t3800683746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseResponse::set_Request(CreateCourseRequest)
extern "C"  void CreateCourseResponse_set_Request_m3055530957 (CreateCourseResponse_t3800683746 * __this, CreateCourseRequest_t4267998642 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Course> CreateCourseResponse::get_Courses()
extern "C"  List_1_t2852233831 * CreateCourseResponse_get_Courses_m2506139712 (CreateCourseResponse_t3800683746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateCourseResponse::set_Courses(System.Collections.Generic.List`1<Course>)
extern "C"  void CreateCourseResponse_set_Courses_m1703298185 (CreateCourseResponse_t3800683746 * __this, List_1_t2852233831 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
