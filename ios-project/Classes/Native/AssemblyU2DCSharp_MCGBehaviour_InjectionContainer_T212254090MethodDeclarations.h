﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MCGBehaviour/InjectionContainer/TypeData
struct TypeData_t212254090;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"
#include "AssemblyU2DCSharp_MCGBehaviour_LifestyleType936921768.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MCGBehaviour/InjectionContainer/TypeData::.ctor()
extern "C"  void TypeData__ctor_m3302161183 (TypeData_t212254090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MCGBehaviour/InjectionContainer/TypeData::get_Instance()
extern "C"  Il2CppObject * TypeData_get_Instance_m2576705816 (TypeData_t212254090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour/InjectionContainer/TypeData::set_Instance(System.Object)
extern "C"  void TypeData_set_Instance_m1615518945 (TypeData_t212254090 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type MCGBehaviour/InjectionContainer/TypeData::get_ImplementedBy()
extern "C"  Type_t * TypeData_get_ImplementedBy_m2140281523 (TypeData_t212254090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour/InjectionContainer/TypeData::set_ImplementedBy(System.Type)
extern "C"  void TypeData_set_ImplementedBy_m4199684284 (TypeData_t212254090 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MCGBehaviour/LifestyleType MCGBehaviour/InjectionContainer/TypeData::get_LifestyleType()
extern "C"  int32_t TypeData_get_LifestyleType_m955252584 (TypeData_t212254090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour/InjectionContainer/TypeData::set_LifestyleType(MCGBehaviour/LifestyleType)
extern "C"  void TypeData_set_LifestyleType_m111050445 (TypeData_t212254090 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MCGBehaviour/InjectionContainer/TypeData::get_Name()
extern "C"  String_t* TypeData_get_Name_m784271350 (TypeData_t212254090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour/InjectionContainer/TypeData::set_Name(System.String)
extern "C"  void TypeData_set_Name_m2847982667 (TypeData_t212254090 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
