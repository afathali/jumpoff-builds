﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.DataContractAttribute
struct DataContractAttribute_t3332255060;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Runtime.Serialization.DataContractAttribute::.ctor()
extern "C"  void DataContractAttribute__ctor_m322966924 (DataContractAttribute_t3332255060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.DataContractAttribute::get_Name()
extern "C"  String_t* DataContractAttribute_get_Name_m3190954703 (DataContractAttribute_t3332255060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.DataContractAttribute::set_Name(System.String)
extern "C"  void DataContractAttribute_set_Name_m124198134 (DataContractAttribute_t3332255060 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.DataContractAttribute::get_Namespace()
extern "C"  String_t* DataContractAttribute_get_Namespace_m3152889531 (DataContractAttribute_t3332255060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.DataContractAttribute::set_Namespace(System.String)
extern "C"  void DataContractAttribute_set_Namespace_m3434416634 (DataContractAttribute_t3332255060 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.DataContractAttribute::get_IsReference()
extern "C"  bool DataContractAttribute_get_IsReference_m3400926920 (DataContractAttribute_t3332255060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.DataContractAttribute::set_IsReference(System.Boolean)
extern "C"  void DataContractAttribute_set_IsReference_m2424730179 (DataContractAttribute_t3332255060 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
