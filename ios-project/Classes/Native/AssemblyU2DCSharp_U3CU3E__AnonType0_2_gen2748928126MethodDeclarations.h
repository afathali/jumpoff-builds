﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType0`2<System.Int32,System.Object>
struct U3CU3E__AnonType0_2_t2748928126;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void <>__AnonType0`2<System.Int32,System.Object>::.ctor(<Count>__T,<Members>__T)
extern "C"  void U3CU3E__AnonType0_2__ctor_m3256138576_gshared (U3CU3E__AnonType0_2_t2748928126 * __this, int32_t ___Count0, Il2CppObject * ___Members1, const MethodInfo* method);
#define U3CU3E__AnonType0_2__ctor_m3256138576(__this, ___Count0, ___Members1, method) ((  void (*) (U3CU3E__AnonType0_2_t2748928126 *, int32_t, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType0_2__ctor_m3256138576_gshared)(__this, ___Count0, ___Members1, method)
// <Count>__T <>__AnonType0`2<System.Int32,System.Object>::get_Count()
extern "C"  int32_t U3CU3E__AnonType0_2_get_Count_m2307950002_gshared (U3CU3E__AnonType0_2_t2748928126 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_2_get_Count_m2307950002(__this, method) ((  int32_t (*) (U3CU3E__AnonType0_2_t2748928126 *, const MethodInfo*))U3CU3E__AnonType0_2_get_Count_m2307950002_gshared)(__this, method)
// <Members>__T <>__AnonType0`2<System.Int32,System.Object>::get_Members()
extern "C"  Il2CppObject * U3CU3E__AnonType0_2_get_Members_m2220904018_gshared (U3CU3E__AnonType0_2_t2748928126 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_2_get_Members_m2220904018(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType0_2_t2748928126 *, const MethodInfo*))U3CU3E__AnonType0_2_get_Members_m2220904018_gshared)(__this, method)
// System.Boolean <>__AnonType0`2<System.Int32,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType0_2_Equals_m3561665657_gshared (U3CU3E__AnonType0_2_t2748928126 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType0_2_Equals_m3561665657(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType0_2_t2748928126 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType0_2_Equals_m3561665657_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType0`2<System.Int32,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType0_2_GetHashCode_m595221463_gshared (U3CU3E__AnonType0_2_t2748928126 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_2_GetHashCode_m595221463(__this, method) ((  int32_t (*) (U3CU3E__AnonType0_2_t2748928126 *, const MethodInfo*))U3CU3E__AnonType0_2_GetHashCode_m595221463_gshared)(__this, method)
// System.String <>__AnonType0`2<System.Int32,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType0_2_ToString_m4103874651_gshared (U3CU3E__AnonType0_2_t2748928126 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_2_ToString_m4103874651(__this, method) ((  String_t* (*) (U3CU3E__AnonType0_2_t2748928126 *, const MethodInfo*))U3CU3E__AnonType0_2_ToString_m4103874651_gshared)(__this, method)
