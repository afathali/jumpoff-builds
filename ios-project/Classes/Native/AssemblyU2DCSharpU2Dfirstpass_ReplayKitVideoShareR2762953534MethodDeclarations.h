﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayKitVideoShareResult
struct ReplayKitVideoShareResult_t2762953534;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"

// System.Void ReplayKitVideoShareResult::.ctor(System.String[])
extern "C"  void ReplayKitVideoShareResult__ctor_m3244164629 (ReplayKitVideoShareResult_t2762953534 * __this, StringU5BU5D_t1642385972* ___sourcesArray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayKitVideoShareResult::get_Sources()
extern "C"  StringU5BU5D_t1642385972* ReplayKitVideoShareResult_get_Sources_m2854884137 (ReplayKitVideoShareResult_t2762953534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
