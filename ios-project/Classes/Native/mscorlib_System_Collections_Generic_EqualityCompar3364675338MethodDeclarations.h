﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Schema.JsonSchemaType>
struct DefaultComparer_t3364675338;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor()
extern "C"  void DefaultComparer__ctor_m3411240309_gshared (DefaultComparer_t3364675338 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3411240309(__this, method) ((  void (*) (DefaultComparer_t3364675338 *, const MethodInfo*))DefaultComparer__ctor_m3411240309_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Schema.JsonSchemaType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m980681396_gshared (DefaultComparer_t3364675338 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m980681396(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3364675338 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m980681396_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Schema.JsonSchemaType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1667543164_gshared (DefaultComparer_t3364675338 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1667543164(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3364675338 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1667543164_gshared)(__this, ___x0, ___y1, method)
