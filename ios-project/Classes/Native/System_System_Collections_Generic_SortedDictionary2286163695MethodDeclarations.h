﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t2286163695;
// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>
struct SortedDictionary_2_t2729314972;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_Collections_Generic_SortedDictionary3868088006.h"

// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m4113522426_gshared (ValueCollection_t2286163695 * __this, SortedDictionary_2_t2729314972 * ___dic0, const MethodInfo* method);
#define ValueCollection__ctor_m4113522426(__this, ___dic0, method) ((  void (*) (ValueCollection_t2286163695 *, SortedDictionary_2_t2729314972 *, const MethodInfo*))ValueCollection__ctor_m4113522426_gshared)(__this, ___dic0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3006016957_gshared (ValueCollection_t2286163695 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3006016957(__this, ___item0, method) ((  void (*) (ValueCollection_t2286163695 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3006016957_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2613910534_gshared (ValueCollection_t2286163695 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2613910534(__this, method) ((  void (*) (ValueCollection_t2286163695 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2613910534_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3650492235_gshared (ValueCollection_t2286163695 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3650492235(__this, ___item0, method) ((  bool (*) (ValueCollection_t2286163695 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3650492235_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2513187128_gshared (ValueCollection_t2286163695 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2513187128(__this, method) ((  bool (*) (ValueCollection_t2286163695 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2513187128_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m151466948_gshared (ValueCollection_t2286163695 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m151466948(__this, ___item0, method) ((  bool (*) (ValueCollection_t2286163695 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m151466948_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3404929066_gshared (ValueCollection_t2286163695 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3404929066(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2286163695 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3404929066_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1467355414_gshared (ValueCollection_t2286163695 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1467355414(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2286163695 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1467355414_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2580649730_gshared (ValueCollection_t2286163695 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2580649730(__this, method) ((  bool (*) (ValueCollection_t2286163695 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2580649730_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3508799894_gshared (ValueCollection_t2286163695 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3508799894(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2286163695 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3508799894_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1600269957_gshared (ValueCollection_t2286163695 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1600269957(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2286163695 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1600269957_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m393763126_gshared (ValueCollection_t2286163695 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ValueCollection_CopyTo_m393763126(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ValueCollection_t2286163695 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m393763126_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3211327562_gshared (ValueCollection_t2286163695 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3211327562(__this, method) ((  int32_t (*) (ValueCollection_t2286163695 *, const MethodInfo*))ValueCollection_get_Count_m3211327562_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t3868088006  ValueCollection_GetEnumerator_m4005009988_gshared (ValueCollection_t2286163695 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m4005009988(__this, method) ((  Enumerator_t3868088006  (*) (ValueCollection_t2286163695 *, const MethodInfo*))ValueCollection_GetEnumerator_m4005009988_gshared)(__this, method)
