﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Boolean SA_EditorTesting::get_IsInsideEditor()
extern "C"  bool SA_EditorTesting_get_IsInsideEditor_m216790077 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA_EditorTesting::HasFill(System.Single)
extern "C"  bool SA_EditorTesting_HasFill_m1763252407 (Il2CppObject * __this /* static, unused */, float ___fillRate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorTesting::CheckForEventSystem()
extern "C"  void SA_EditorTesting_CheckForEventSystem_m1303346307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
