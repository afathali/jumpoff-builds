﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>
struct KeyCollection_t966099451;
// System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>
struct Dictionary_2_t2777568976;
// System.Collections.Generic.IEnumerator`1<FB_ProfileImageSize>
struct IEnumerator_1_t478851957;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// FB_ProfileImageSize[]
struct FB_ProfileImageSizeU5BU5D_t4110693687;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FB_ProfileImageSize3003328130.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1172105118.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3523539860_gshared (KeyCollection_t966099451 * __this, Dictionary_2_t2777568976 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3523539860(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t966099451 *, Dictionary_2_t2777568976 *, const MethodInfo*))KeyCollection__ctor_m3523539860_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3322330018_gshared (KeyCollection_t966099451 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3322330018(__this, ___item0, method) ((  void (*) (KeyCollection_t966099451 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3322330018_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m903715163_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m903715163(__this, method) ((  void (*) (KeyCollection_t966099451 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m903715163_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m161914186_gshared (KeyCollection_t966099451 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m161914186(__this, ___item0, method) ((  bool (*) (KeyCollection_t966099451 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m161914186_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1659795907_gshared (KeyCollection_t966099451 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1659795907(__this, ___item0, method) ((  bool (*) (KeyCollection_t966099451 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1659795907_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m421845341_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m421845341(__this, method) ((  Il2CppObject* (*) (KeyCollection_t966099451 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m421845341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m355819049_gshared (KeyCollection_t966099451 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m355819049(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t966099451 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m355819049_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3039926290_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3039926290(__this, method) ((  Il2CppObject * (*) (KeyCollection_t966099451 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3039926290_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4188921679_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4188921679(__this, method) ((  bool (*) (KeyCollection_t966099451 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4188921679_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3755359973_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3755359973(__this, method) ((  bool (*) (KeyCollection_t966099451 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3755359973_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2694683641_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2694683641(__this, method) ((  Il2CppObject * (*) (KeyCollection_t966099451 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2694683641_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m285260995_gshared (KeyCollection_t966099451 * __this, FB_ProfileImageSizeU5BU5D_t4110693687* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m285260995(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t966099451 *, FB_ProfileImageSizeU5BU5D_t4110693687*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m285260995_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1172105118  KeyCollection_GetEnumerator_m1955007816_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1955007816(__this, method) ((  Enumerator_t1172105118  (*) (KeyCollection_t966099451 *, const MethodInfo*))KeyCollection_GetEnumerator_m1955007816_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1680202429_gshared (KeyCollection_t966099451 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1680202429(__this, method) ((  int32_t (*) (KeyCollection_t966099451 *, const MethodInfo*))KeyCollection_get_Count_m1680202429_gshared)(__this, method)
