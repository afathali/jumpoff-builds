﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SampleObject
struct SampleObject_t3388626405;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Guid2533601593.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void SampleObject::.ctor()
extern "C"  void SampleObject__ctor_m1286684528 (SampleObject_t3388626405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SampleObject::get_StringProperty()
extern "C"  String_t* SampleObject_get_StringProperty_m4049976458 (SampleObject_t3388626405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleObject::set_StringProperty(System.String)
extern "C"  void SampleObject_set_StringProperty_m3938089509 (SampleObject_t3388626405 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SampleObject::get_FloatProperty()
extern "C"  float SampleObject_get_FloatProperty_m4145680582 (SampleObject_t3388626405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleObject::set_FloatProperty(System.Single)
extern "C"  void SampleObject_set_FloatProperty_m664959791 (SampleObject_t3388626405 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid SampleObject::get_GuidProperty()
extern "C"  Guid_t2533601593  SampleObject_get_GuidProperty_m3188408098 (SampleObject_t3388626405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleObject::set_GuidProperty(System.Guid)
extern "C"  void SampleObject_set_GuidProperty_m2768921733 (SampleObject_t3388626405 * __this, Guid_t2533601593  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SampleObject::get_VectorProperty()
extern "C"  Vector3_t2243707580  SampleObject_get_VectorProperty_m2503551613 (SampleObject_t3388626405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleObject::set_VectorProperty(UnityEngine.Vector3)
extern "C"  void SampleObject_set_VectorProperty_m3849422950 (SampleObject_t3388626405 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
