﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2709430626;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"

// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1__ctor_m2508195542_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2709430626 * __this, const MethodInfo* method);
#define U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1__ctor_m2508195542(__this, method) ((  void (*) (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2709430626 *, const MethodInfo*))U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1__ctor_m2508195542_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<>m__106(TSource)
extern "C"  bool U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__106_m3171692391_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2709430626 * __this, KeyValuePair_2_t38854645  ___s0, const MethodInfo* method);
#define U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__106_m3171692391(__this, ___s0, method) ((  bool (*) (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2709430626 *, KeyValuePair_2_t38854645 , const MethodInfo*))U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__106_m3171692391_gshared)(__this, ___s0, method)
// System.Boolean Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<>m__107(TSource)
extern "C"  bool U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__107_m1285027138_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2709430626 * __this, KeyValuePair_2_t38854645  ___s0, const MethodInfo* method);
#define U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__107_m1285027138(__this, ___s0, method) ((  bool (*) (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2709430626 *, KeyValuePair_2_t38854645 , const MethodInfo*))U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__107_m1285027138_gshared)(__this, ___s0, method)
