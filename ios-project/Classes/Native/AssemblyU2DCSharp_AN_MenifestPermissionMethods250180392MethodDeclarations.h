﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AN_ManifestPermission1772110202.h"

// System.String AN_MenifestPermissionMethods::GetFullName(AN_ManifestPermission)
extern "C"  String_t* AN_MenifestPermissionMethods_GetFullName_m1909687534 (Il2CppObject * __this /* static, unused */, int32_t ___permission0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AN_MenifestPermissionMethods::IsNormalPermission(AN_ManifestPermission)
extern "C"  bool AN_MenifestPermissionMethods_IsNormalPermission_m2634608521 (Il2CppObject * __this /* static, unused */, int32_t ___permission0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
