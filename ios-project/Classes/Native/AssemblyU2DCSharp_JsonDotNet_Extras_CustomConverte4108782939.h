﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<Newtonsoft.Json.Linq.JProperty,System.Boolean>
struct Func_2_t100615452;
// System.Func`2<Newtonsoft.Json.Linq.JProperty,System.String>
struct Func_2_t2599228263;

#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter1964060750.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonDotNet.Extras.CustomConverters.Matrix4x4Converter
struct  Matrix4x4Converter_t4108782939  : public JsonConverter_t1964060750
{
public:

public:
};

struct Matrix4x4Converter_t4108782939_StaticFields
{
public:
	// System.Func`2<Newtonsoft.Json.Linq.JProperty,System.Boolean> JsonDotNet.Extras.CustomConverters.Matrix4x4Converter::<>f__am$cache0
	Func_2_t100615452 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<Newtonsoft.Json.Linq.JProperty,System.String> JsonDotNet.Extras.CustomConverters.Matrix4x4Converter::<>f__am$cache1
	Func_2_t2599228263 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(Matrix4x4Converter_t4108782939_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t100615452 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t100615452 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t100615452 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(Matrix4x4Converter_t4108782939_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t2599228263 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t2599228263 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t2599228263 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
