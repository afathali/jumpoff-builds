﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.EnumValues`1<System.UInt64>
struct EnumValues_1_t3738913591;
// System.String
struct String_t;
// Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>
struct EnumValue_1_t2589200904;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.EnumValues`1<System.UInt64>::.ctor()
extern "C"  void EnumValues_1__ctor_m789867523_gshared (EnumValues_1_t3738913591 * __this, const MethodInfo* method);
#define EnumValues_1__ctor_m789867523(__this, method) ((  void (*) (EnumValues_1_t3738913591 *, const MethodInfo*))EnumValues_1__ctor_m789867523_gshared)(__this, method)
// System.String Newtonsoft.Json.Utilities.EnumValues`1<System.UInt64>::GetKeyForItem(Newtonsoft.Json.Utilities.EnumValue`1<T>)
extern "C"  String_t* EnumValues_1_GetKeyForItem_m2045969929_gshared (EnumValues_1_t3738913591 * __this, EnumValue_1_t2589200904 * ___item0, const MethodInfo* method);
#define EnumValues_1_GetKeyForItem_m2045969929(__this, ___item0, method) ((  String_t* (*) (EnumValues_1_t3738913591 *, EnumValue_1_t2589200904 *, const MethodInfo*))EnumValues_1_GetKeyForItem_m2045969929_gshared)(__this, ___item0, method)
