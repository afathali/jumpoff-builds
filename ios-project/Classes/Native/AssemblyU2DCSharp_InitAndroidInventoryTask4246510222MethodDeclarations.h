﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InitAndroidInventoryTask
struct InitAndroidInventoryTask_t4246510222;
// System.Action
struct Action_t3226471752;
// BillingResult
struct BillingResult_t3511841850;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharp_BillingResult3511841850.h"

// System.Void InitAndroidInventoryTask::.ctor()
extern "C"  void InitAndroidInventoryTask__ctor_m3091015449 (InitAndroidInventoryTask_t4246510222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitAndroidInventoryTask::add_ActionComplete(System.Action)
extern "C"  void InitAndroidInventoryTask_add_ActionComplete_m1031021873 (InitAndroidInventoryTask_t4246510222 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitAndroidInventoryTask::remove_ActionComplete(System.Action)
extern "C"  void InitAndroidInventoryTask_remove_ActionComplete_m3405653068 (InitAndroidInventoryTask_t4246510222 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitAndroidInventoryTask::add_ActionFailed(System.Action)
extern "C"  void InitAndroidInventoryTask_add_ActionFailed_m1860069403 (InitAndroidInventoryTask_t4246510222 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitAndroidInventoryTask::remove_ActionFailed(System.Action)
extern "C"  void InitAndroidInventoryTask_remove_ActionFailed_m2338705318 (InitAndroidInventoryTask_t4246510222 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InitAndroidInventoryTask InitAndroidInventoryTask::Create()
extern "C"  InitAndroidInventoryTask_t4246510222 * InitAndroidInventoryTask_Create_m1814246852 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitAndroidInventoryTask::Run()
extern "C"  void InitAndroidInventoryTask_Run_m997381640 (InitAndroidInventoryTask_t4246510222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitAndroidInventoryTask::OnBillingConnected(BillingResult)
extern "C"  void InitAndroidInventoryTask_OnBillingConnected_m1292863514 (InitAndroidInventoryTask_t4246510222 * __this, BillingResult_t3511841850 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitAndroidInventoryTask::OnBillingConnectFinished()
extern "C"  void InitAndroidInventoryTask_OnBillingConnectFinished_m1290497895 (InitAndroidInventoryTask_t4246510222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitAndroidInventoryTask::OnRetrieveProductsFinised(BillingResult)
extern "C"  void InitAndroidInventoryTask_OnRetrieveProductsFinised_m2687848682 (InitAndroidInventoryTask_t4246510222 * __this, BillingResult_t3511841850 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitAndroidInventoryTask::<ActionComplete>m__14()
extern "C"  void InitAndroidInventoryTask_U3CActionCompleteU3Em__14_m3000655682 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitAndroidInventoryTask::<ActionFailed>m__15()
extern "C"  void InitAndroidInventoryTask_U3CActionFailedU3Em__15_m1426694175 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
