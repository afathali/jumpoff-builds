﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// <>__AnonType0`2<System.Int32,System.Object>
struct U3CU3E__AnonType0_2_t2748928126;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// <>__AnonType0`2<System.Object,System.Object>
struct U3CU3E__AnonType0_2_t3333162619;
// MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>
struct U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569;
// Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>
struct CustomCreationConverter_1_t4070253483;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t1973729997;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t1719617802;
// Newtonsoft.Json.JsonReader
struct JsonReader_t3154730733;
// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IJEnumerable_1_t2511655056;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerable_1_t2844771058;
// Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>
struct U3CConvertU3Ec__Iterator13_2_t1287029637;
// Newtonsoft.Json.Linq.JToken
struct JToken_t2552644013;
// Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>
struct U3CValuesU3Ec__Iterator12_2_t1507201787;
// Newtonsoft.Json.Linq.JValue
struct JValue_t300956845;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t502202687;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_t1997612730;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.Object,System.Object>
struct BidirectionalDictionary_2_t1212012318;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1902082073;
// Newtonsoft.Json.Utilities.CollectionUtils/<TryGetSingleItem>c__AnonStorey25`1<System.Object>
struct U3CTryGetSingleItemU3Ec__AnonStorey25_1_t1867941046;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>
struct CollectionWrapper_1_t840219110;
// System.Collections.IList
struct IList_t3321498491;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey2A`1<System.Object>
struct U3CTryConvertU3Ec__AnonStorey2A_1_t142049201;
// Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey2C`1<System.Object>
struct U3CTryConvertOrCastU3Ec__AnonStorey2C_1_t283520851;
// Newtonsoft.Json.Utilities.Creator`1<System.Object>
struct Creator_1_t310204047;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;
// Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>
struct DictionaryWrapper_2_t534389262;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.Generic.IDictionary`2<System.Object,System.Object>
struct IDictionary_2_t280592844;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344;
// System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>
struct IEnumerable_1_t3341002443;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t330981690;
// System.Func`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Func_2_t3251821928;
// Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>
struct EnumValue_1_t589082027;
// Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>
struct EnumValue_1_t2589200904;
// Newtonsoft.Json.Utilities.EnumValues`1<System.Int64>
struct EnumValues_1_t1738794714;
// Newtonsoft.Json.Utilities.EnumValues`1<System.UInt64>
struct EnumValues_1_t3738913591;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey30`1<System.Object>
struct U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_t1934288580;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey31`1<System.Object>
struct U3CCreateGetU3Ec__AnonStorey31_1_t881987152;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey32`1<System.Object>
struct U3CCreateGetU3Ec__AnonStorey32_1_t1719614423;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey2F`1<System.Object>
struct U3CCreateMethodCallU3Ec__AnonStorey2F_1_t4131337349;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey33`1<System.Object>
struct U3CCreateSetU3Ec__AnonStorey33_1_t2802865970;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey34`1<System.Object>
struct U3CCreateSetU3Ec__AnonStorey34_1_t3640493241;
// Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>
struct ListWrapper_1_t3921709322;
// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>
struct MethodCall_2_t1283576322;
// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2709430626;
// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Object>
struct U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t1065057980;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct ThreadSafeStore_2_t4078621129;
// System.Func`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct Func_2_t2880772247;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct ThreadSafeStore_2_t3893850024;
// System.Func`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct Func_2_t2696001142;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Object>
struct ThreadSafeStore_2_t4023353063;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// SA.Common.Pattern.NonMonoSingleton`1<System.Object>
struct NonMonoSingleton_1_t461172036;
// SA.Common.Pattern.Singleton`1<System.Object>
struct Singleton_1_t3172014400;
// SA_Singleton_OLD`1<System.Object>
struct SA_Singleton_OLD_1_t3980293213;
// Shared.Response`1<System.Object>
struct Response_1_t3572928552;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
struct ThreadSafeDictionary_2_t1463927430;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>
struct ThreadSafeDictionaryValueFactory_2_t958855109;
// System.Action`1<AN_LicenseRequestResult>
struct Action_1_t2133169943;
// System.Action`1<AndroidDialogResult>
struct Action_1_t1465846336;
// System.Action`1<GP_GamesStatusCodes>
struct Action_1_t815305555;
// System.Action`1<GPConnectionState>
struct Action_1_t449146262;
// System.Action`1<InstagramPostResult>
struct Action_1_t237882577;
// System.Action`1<IOSDialogResult>
struct Action_1_t3541040698;
// System.Action`1<ISN_SwipeDirection>
struct Action_1_t570721078;
// System.Action`1<MP_MusicPlaybackState>
struct Action_1_t2166513183;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<System.DateTime>
struct Action_1_t495005051;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<System.Single>
struct Action_1_t1878309314;
// System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>
struct Action_1_t2755832024;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_t2045506962;
// System.Action`2<GK_MatchType,System.Object>
struct Action_2_t3551550644;
// System.Action`2<System.Boolean,System.Int32>
struct Action_2_t1907880187;
// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2525452034;
// System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>
struct Action_2_t2790035381;
// System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>
struct Action_2_t1158962578;
// System.Action`2<System.Object,GK_InviteRecipientResponse>
struct Action_2_t3321460360;
// System.Action`2<System.Object,System.Boolean>
struct Action_2_t3708177276;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2572051853;
// System.Action`3<GK_MatchType,System.Object,System.Object>
struct Action_3_t3536498748;
// System.Action`3<System.Object,GK_PlayerConnectionState,System.Object>
struct Action_3_t905177775;
// System.Action`3<System.Object,System.Object,System.Boolean>
struct Action_3_t2251782606;
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_t1115657183;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t2445488949;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t4145164493;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t1254237568;
// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t2471096271;
// System.Exception
struct Exception_t1927440687;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t4170771815;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3304067486;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1864648666;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t1279844890;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1075686591;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3268689037;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType0_2_gen2748928126.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType0_2_gen2748928126MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare645512719MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare645512719.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1263084566MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1263084566.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType0_2_gen3333162619.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType0_2_gen3333162619MethodDeclarations.h"
#include "AssemblyU2DCSharp_MessageSender_U3CSendRequestCorou613674569.h"
#include "AssemblyU2DCSharp_MessageSender_U3CSendRequestCorou613674569MethodDeclarations.h"
#include "AssemblyU2DCSharp_Shared_Request3213492751MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConvert3949895659MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "mscorlib_System_Text_UTF8Encoding111055448MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "System_Core_System_Action_2_gen2525452034.h"
#include "AssemblyU2DCSharp_Shared_Request3213492751.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkReachability1092747145.h"
#include "System_Core_System_Action_2_gen2525452034MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "AssemblyU2DCSharp_MessageSender204794706.h"
#include "AssemblyU2DCSharp_MessageSender204794706MethodDeclarations.h"
#include "mscorlib_System_Text_UTF8Encoding111055448.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "mscorlib_System_Byte3683104436.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConvert3949895659.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_Custo4070253483.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_Custo4070253483MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter1964060750MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter1973729997.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer1719617802.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader3154730733.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializatio1492322735MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer1719617802MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader3154730733MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken620654565.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializatio1492322735.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JEnumerable3957742755.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JEnumerable3957742755MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Valida1621959402MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_LinqExtensio778605574MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_LinqExtensio778605574.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JEnumerable3820937473.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JEnumerable3820937473MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_LinqExtensi1287029637.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_LinqExtensi1287029637MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken2552644013.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_LinqExtensi1507201787.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_LinqExtensi1507201787MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JValue300956845.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken2552644013MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Cac666803094.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Cac666803094MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js1473969596.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js1473969596MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3334259717.h"
#include "System_Core_System_Func_2_gen3334259717MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_ThreadS237141303.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_ThreadS237141303MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Ob1997612730.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Ob1997612730MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Bidire1212012318.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Bidire1212012318MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Collec1867941046.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Collec1867941046MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Collec3679343665MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Collec3679343665.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Collect840219110.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Collect840219110MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String1768673176MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Reflec3884221258MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Convert142049201.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Convert142049201MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Conver2984810590MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Convert283520851.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Convert283520851MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Creator310204047.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Creator310204047MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Dictio2758714162.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Dictio2758714162MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Diction534389262.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Diction534389262MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "System_Core_System_Func_2_gen3251821928.h"
#include "System_Core_System_Func_2_gen3251821928MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVal589082027.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVal589082027MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVa2589200904.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVa2589200904MethodDeclarations.h"
#include "mscorlib_System_UInt642909196914.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVa1738794714.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVa1738794714MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_KeyedColle1516412003MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVa3738913591.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVa3738913591MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_KeyedColle3516530880MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo1934288580.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo1934288580MethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo2851816542MethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo2851816542.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBou881987152.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBou881987152MethodDeclarations.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo1719614423.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo1719614423MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo255040150.h"
#include "mscorlib_System_Reflection_FieldInfo255040150MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo4131337349.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo4131337349MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase904190842MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase904190842.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo2802865970.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo2802865970MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo3640493241.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo3640493241MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_ListWr3921709322.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_ListWr3921709322MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Method1283576322.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Method1283576322MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String2709430626.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String2709430626MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1405561329.h"
#include "System_Core_System_Func_2_gen1405561329MethodDeclarations.h"
#include "mscorlib_System_StringComparison2376310518.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String1065057980.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String1065057980MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2165275119.h"
#include "System_Core_System_Func_2_gen2165275119MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread4078621129.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread4078621129MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2880772247.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De3055062677.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2336777489.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2336777489MethodDeclarations.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2880772247MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread3893850024.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread3893850024MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2696001142.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Conver1788482786.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2152006384.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2152006384MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2696001142MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread4023353063.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread4023353063MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2825504181.h"
#include "System_Core_System_Func_2_gen2825504181MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Non461172036.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Non461172036MethodDeclarations.h"
#include "mscorlib_System_Activator1850728717MethodDeclarations.h"
#include "mscorlib_System_Activator1850728717.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si3172014400.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si3172014400MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3980293213.h"
#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3980293213MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "AssemblyU2DCSharp_Shared_Response_1_gen3572928552.h"
#include "AssemblyU2DCSharp_Shared_Response_1_gen3572928552MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_1463927430.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_1463927430MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_T958855109.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_T958855109MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke470039898.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va984569266.h"
#include "mscorlib_System_Action_1_gen2133169943.h"
#include "mscorlib_System_Action_1_gen2133169943MethodDeclarations.h"
#include "AssemblyU2DCSharp_AN_LicenseRequestResult2331370561.h"
#include "mscorlib_System_Action_1_gen1465846336.h"
#include "mscorlib_System_Action_1_gen1465846336MethodDeclarations.h"
#include "AssemblyU2DCSharp_AndroidDialogResult1664046954.h"
#include "mscorlib_System_Action_1_gen815305555.h"
#include "mscorlib_System_Action_1_gen815305555MethodDeclarations.h"
#include "AssemblyU2DCSharp_GP_GamesStatusCodes1013506173.h"
#include "mscorlib_System_Action_1_gen449146262.h"
#include "mscorlib_System_Action_1_gen449146262MethodDeclarations.h"
#include "AssemblyU2DCSharp_GPConnectionState647346880.h"
#include "mscorlib_System_Action_1_gen237882577.h"
#include "mscorlib_System_Action_1_gen237882577MethodDeclarations.h"
#include "AssemblyU2DCSharp_InstagramPostResult436083195.h"
#include "mscorlib_System_Action_1_gen3541040698.h"
#include "mscorlib_System_Action_1_gen3541040698MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSDialogResult3739241316.h"
#include "mscorlib_System_Action_1_gen570721078.h"
#include "mscorlib_System_Action_1_gen570721078MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_SwipeDirection768921696.h"
#include "mscorlib_System_Action_1_gen2166513183.h"
#include "mscorlib_System_Action_1_gen2166513183MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MusicPlaybackStat2364713801.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen495005051.h"
#include "mscorlib_System_Action_1_gen495005051MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Action_1_gen1873676830.h"
#include "mscorlib_System_Action_1_gen1873676830MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2491248677.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1878309314.h"
#include "mscorlib_System_Action_1_gen1878309314MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Action_1_gen2755832024.h"
#include "mscorlib_System_Action_1_gen2755832024MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "mscorlib_System_Action_1_gen2045506962.h"
#include "mscorlib_System_Action_1_gen2045506962MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "System_Core_System_Action_2_gen3551550644.h"
#include "System_Core_System_Action_2_gen3551550644MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_MatchType1493351924.h"
#include "System_Core_System_Action_2_gen1907880187.h"
#include "System_Core_System_Action_2_gen1907880187MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2790035381.h"
#include "System_Core_System_Action_2_gen2790035381MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1158962578.h"
#include "System_Core_System_Action_2_gen1158962578MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "System_Core_System_Action_2_gen3321460360.h"
#include "System_Core_System_Action_2_gen3321460360MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_InviteRecipientRe3438857802.h"
#include "System_Core_System_Action_2_gen3708177276.h"
#include "System_Core_System_Action_2_gen3708177276MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2572051853.h"
#include "System_Core_System_Action_2_gen2572051853MethodDeclarations.h"
#include "System_Core_System_Action_3_gen3536498748.h"
#include "System_Core_System_Action_3_gen3536498748MethodDeclarations.h"
#include "System_Core_System_Action_3_gen905177775.h"
#include "System_Core_System_Action_3_gen905177775MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_PlayerConnectionS2434478783.h"
#include "System_Core_System_Action_3_gen2251782606.h"
#include "System_Core_System_Action_3_gen2251782606MethodDeclarations.h"
#include "System_Core_System_Action_3_gen1115657183.h"
#include "System_Core_System_Action_3_gen1115657183MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2445488949.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2445488949MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen2471096271.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4145164493.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4145164493MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4170771815.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1254237568.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1254237568MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1279844890.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen2471096271MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4170771815MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1279844890MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2630862464.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2630862464MethodDeclarations.h"
#include "AssemblyU2DCSharp_AN_ManifestPermission1772110202.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1696953486.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1696953486MethodDeclarations.h"
#include "AssemblyU2DCSharp_AN_PermissionState838201224.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3862080392.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3862080392MethodDeclarations.h"
#include "AssemblyU2DCSharp_FB_ProfileImageSize3003328130.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2929754617.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2929754617MethodDeclarations.h"
#include "AssemblyU2DCSharp_GP_QuestsSelect2071002355.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3080482812.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3080482812MethodDeclarations.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchTurnStatus2221730550.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2870158877.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2870158877MethodDeclarations.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex2011406615.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen565169432.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen565169432MethodDeclarations.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4001384466.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3199726719.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3199726719MethodDeclarations.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2340974457.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4144585176.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4144585176MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter_State3285832914.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2166579475.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2166579475MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType1307827213.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2601497439.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2601497439MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3913814939.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3913814939MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1078953194.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1078953194MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso220200932.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2647235048.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2647235048MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3452969744.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3452969744MethodDeclarations.h"
#include "mscorlib_System_ArraySegment_1_gen2594217482.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen389359684.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen389359684MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246889402.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246889402MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen18266304.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen18266304MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3907627660.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3907627660MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1723885533.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1723885533MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li865133271.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4267761149.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4267761149MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23409008887.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1393666460.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1393666460MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_534914198.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen952874973.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen952874973MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g94122711.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen768103868.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen768103868MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24204318902.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen974663562.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen974663562MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_115911300.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3990767863.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3990767863MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"

// !!0 Newtonsoft.Json.JsonConvert::DeserializeObject<System.Object>(System.String)
extern "C"  Il2CppObject * JsonConvert_DeserializeObject_TisIl2CppObject_m3159274222_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define JsonConvert_DeserializeObject_TisIl2CppObject_m3159274222(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonConvert_DeserializeObject_TisIl2CppObject_m3159274222_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Empty<System.Object>()
extern "C"  Il2CppObject* Enumerable_Empty_TisIl2CppObject_m2244569587_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Enumerable_Empty_TisIl2CppObject_m2244569587(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Enumerable_Empty_TisIl2CppObject_m2244569587_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerable`1<!!1> Newtonsoft.Json.Linq.LinqExtensions::Values<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Object)
extern "C"  Il2CppObject* LinqExtensions_Values_TisIl2CppObject_TisIl2CppObject_m851392082_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject * p1, const MethodInfo* method);
#define LinqExtensions_Values_TisIl2CppObject_TisIl2CppObject_m851392082(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))LinqExtensions_Values_TisIl2CppObject_TisIl2CppObject_m851392082_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> Newtonsoft.Json.Linq.LinqExtensions::Values<System.Object,Newtonsoft.Json.Linq.JToken>(System.Collections.Generic.IEnumerable`1<!!0>,System.Object)
#define LinqExtensions_Values_TisIl2CppObject_TisJToken_t2552644013_m971126076(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))LinqExtensions_Values_TisIl2CppObject_TisIl2CppObject_m851392082_gshared)(__this /* static, unused */, p0, p1, method)
// !!1 Newtonsoft.Json.Linq.LinqExtensions::Convert<System.Object,System.Object>(!!0)
extern "C"  Il2CppObject * LinqExtensions_Convert_TisIl2CppObject_TisIl2CppObject_m3064098231_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define LinqExtensions_Convert_TisIl2CppObject_TisIl2CppObject_m3064098231(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))LinqExtensions_Convert_TisIl2CppObject_TisIl2CppObject_m3064098231_gshared)(__this /* static, unused */, p0, method)
// !!1 Newtonsoft.Json.Linq.LinqExtensions::Convert<Newtonsoft.Json.Linq.JToken,System.Object>(!!0)
#define LinqExtensions_Convert_TisJToken_t2552644013_TisIl2CppObject_m3198750343(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, JToken_t2552644013 *, const MethodInfo*))LinqExtensions_Convert_TisIl2CppObject_TisIl2CppObject_m3064098231_gshared)(__this /* static, unused */, p0, method)
// !!1 Newtonsoft.Json.Linq.LinqExtensions::Convert<Newtonsoft.Json.Linq.JValue,System.Object>(!!0)
#define LinqExtensions_Convert_TisJValue_t300956845_TisIl2CppObject_m624975335(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, JValue_t300956845 *, const MethodInfo*))LinqExtensions_Convert_TisIl2CppObject_TisIl2CppObject_m3064098231_gshared)(__this /* static, unused */, p0, method)
// !!0 Newtonsoft.Json.Serialization.JsonTypeReflector::GetAttribute<System.Object>(System.Reflection.ICustomAttributeProvider)
extern "C"  Il2CppObject * JsonTypeReflector_GetAttribute_TisIl2CppObject_m3722916544_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define JsonTypeReflector_GetAttribute_TisIl2CppObject_m3722916544(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))JsonTypeReflector_GetAttribute_TisIl2CppObject_m3722916544_gshared)(__this /* static, unused */, p0, method)
// !!0 Newtonsoft.Json.Utilities.CollectionUtils::GetSingleItem<System.Object>(System.Collections.Generic.IList`1<!!0>,System.Boolean)
extern "C"  Il2CppObject * CollectionUtils_GetSingleItem_TisIl2CppObject_m2013561711_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, bool p1, const MethodInfo* method);
#define CollectionUtils_GetSingleItem_TisIl2CppObject_m2013561711(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, bool, const MethodInfo*))CollectionUtils_GetSingleItem_TisIl2CppObject_m2013561711_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
extern "C"  Il2CppObject* Enumerable_Cast_TisIl2CppObject_m3596005422_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Enumerable_Cast_TisIl2CppObject_m3596005422(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Enumerable_Cast_TisIl2CppObject_m3596005422_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t2058570427 * Enumerable_ToList_TisIl2CppObject_m3361462832_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisIl2CppObject_m3361462832(__this /* static, unused */, p0, method) ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3361462832_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Collections.DictionaryEntry>(System.Collections.IEnumerable)
extern "C"  Il2CppObject* Enumerable_Cast_TisDictionaryEntry_t3048875398_m3594045651_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Enumerable_Cast_TisDictionaryEntry_t3048875398_m3594045651(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Enumerable_Cast_TisDictionaryEntry_t3048875398_m3594045651_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisDictionaryEntry_t3048875398_TisKeyValuePair_2_t38854645_m1323444655_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3251821928 * p1, const MethodInfo* method);
#define Enumerable_Select_TisDictionaryEntry_t3048875398_TisKeyValuePair_2_t38854645_m1323444655(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3251821928 *, const MethodInfo*))Enumerable_Select_TisDictionaryEntry_t3048875398_TisKeyValuePair_2_t38854645_m1323444655_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Activator::CreateInstance<System.Object>()
extern "C"  Il2CppObject * Activator_CreateInstance_TisIl2CppObject_m1928415755_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisIl2CppObject_m1928415755(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisIl2CppObject_m1928415755_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m4099813343_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m4099813343(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m4099813343_gshared)(__this, method)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2032877681_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2032877681(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2032877681_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486* p0, CustomAttributeNamedArgument_t94157543  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591* p0, CustomAttributeTypedArgument_t1498197914  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Array::InternalArray__get_Item<AN_ManifestPermission>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisAN_ManifestPermission_t1772110202_m631988758_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisAN_ManifestPermission_t1772110202_m631988758(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisAN_ManifestPermission_t1772110202_m631988758_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<AN_PermissionState>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisAN_PermissionState_t838201224_m4277541164_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisAN_PermissionState_t838201224_m4277541164(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisAN_PermissionState_t838201224_m4277541164_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<FB_ProfileImageSize>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisFB_ProfileImageSize_t3003328130_m2799170678_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisFB_ProfileImageSize_t3003328130_m2799170678(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisFB_ProfileImageSize_t3003328130_m2799170678_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<GP_QuestsSelect>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisGP_QuestsSelect_t2071002355_m3761110081_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGP_QuestsSelect_t2071002355_m3761110081(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGP_QuestsSelect_t2071002355_m3761110081_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<GP_TBM_MatchTurnStatus>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisGP_TBM_MatchTurnStatus_t2221730550_m1165309754_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGP_TBM_MatchTurnStatus_t2221730550_m1165309754(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGP_TBM_MatchTurnStatus_t2221730550_m1165309754_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C"  TableRange_t2011406615  Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977(__this, p0, method) ((  TableRange_t2011406615  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Xml2.XmlTextReader/TagName>(System.Int32)
extern "C"  TagName_t2340974457  Array_InternalArray__get_Item_TisTagName_t2340974457_m451755763_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTagName_t2340974457_m451755763(__this, p0, method) ((  TagName_t2340974457  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTagName_t2340974457_m451755763_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.JsonWriter/State>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisState_t3285832914_m1890730338_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisState_t3285832914_m1890730338(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisState_t3285832914_m1890730338_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Linq.JTokenType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisJTokenType_t1307827213_m3479920364_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisJTokenType_t1307827213_m3479920364(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisJTokenType_t1307827213_m3479920364_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Schema.JsonSchemaType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisJsonSchemaType_t1742745177_m1664607677_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisJsonSchemaType_t1742745177_m1664607677(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisJsonSchemaType_t1742745177_m1664607677_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>(System.Int32)
extern "C"  TypeNameKey_t3055062677  Array_InternalArray__get_Item_TisTypeNameKey_t3055062677_m1285324895_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeNameKey_t3055062677_m1285324895(__this, p0, method) ((  TypeNameKey_t3055062677  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeNameKey_t3055062677_m1285324895_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisPropertyPresence_t220200932_m2126384204_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPropertyPresence_t220200932_m2126384204(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPropertyPresence_t220200932_m2126384204_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>(System.Int32)
extern "C"  TypeConvertKey_t1788482786  Array_InternalArray__get_Item_TisTypeConvertKey_t1788482786_m3472545582_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeConvertKey_t1788482786_m3472545582(__this, p0, method) ((  TypeConvertKey_t1788482786  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeConvertKey_t1788482786_m3472545582_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.ArraySegment`1<System.Byte>>(System.Int32)
extern "C"  ArraySegment_1_t2594217482  Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683(__this, p0, method) ((  ArraySegment_1_t2594217482  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639(__this, p0, method) ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisByte_t3683104436_m635665873(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern "C"  Il2CppChar Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547(__this, p0, method) ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern "C"  DictionaryEntry_t3048875398  Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320(__this, p0, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern "C"  Link_t865133271  Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t865133271_m2489845481(__this, p0, method) ((  Link_t865133271  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>(System.Int32)
extern "C"  KeyValuePair_2_t3409008887  Array_InternalArray__get_Item_TisKeyValuePair_2_t3409008887_m395694521_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3409008887_m395694521(__this, p0, method) ((  KeyValuePair_2_t3409008887  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3409008887_m395694521_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t534914198  Array_InternalArray__get_Item_TisKeyValuePair_2_t534914198_m4026753073_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t534914198_m4026753073(__this, p0, method) ((  KeyValuePair_2_t534914198  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t534914198_m4026753073_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t94122711  Array_InternalArray__get_Item_TisKeyValuePair_2_t94122711_m324496728_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t94122711_m324496728(__this, p0, method) ((  KeyValuePair_2_t94122711  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t94122711_m324496728_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t4204318902  Array_InternalArray__get_Item_TisKeyValuePair_2_t4204318902_m1061941347_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t4204318902_m1061941347(__this, p0, method) ((  KeyValuePair_2_t4204318902  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t4204318902_m1061941347_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t115911300  Array_InternalArray__get_Item_TisKeyValuePair_2_t115911300_m3077433969_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t115911300_m3077433969(__this, p0, method) ((  KeyValuePair_2_t115911300  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t115911300_m3077433969_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t3132015601  Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841(__this, p0, method) ((  KeyValuePair_2_t3132015601  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void <>__AnonType0`2<System.Int32,System.Object>::.ctor(<Count>__T,<Members>__T)
extern "C"  void U3CU3E__AnonType0_2__ctor_m3256138576_gshared (U3CU3E__AnonType0_2_t2748928126 * __this, int32_t ___Count0, Il2CppObject * ___Members1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___Count0;
		__this->set_U3CCountU3E_0(L_0);
		Il2CppObject * L_1 = ___Members1;
		__this->set_U3CMembersU3E_1(L_1);
		return;
	}
}
// <Count>__T <>__AnonType0`2<System.Int32,System.Object>::get_Count()
extern "C"  int32_t U3CU3E__AnonType0_2_get_Count_m2307950002_gshared (U3CU3E__AnonType0_2_t2748928126 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CCountU3E_0();
		return L_0;
	}
}
// <Members>__T <>__AnonType0`2<System.Int32,System.Object>::get_Members()
extern "C"  Il2CppObject * U3CU3E__AnonType0_2_get_Members_m2220904018_gshared (U3CU3E__AnonType0_2_t2748928126 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CMembersU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType0`2<System.Int32,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType0_2_Equals_m3561665657_gshared (U3CU3E__AnonType0_2_t2748928126 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType0_2_t2748928126 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType0_2_t2748928126 *)((U3CU3E__AnonType0_2_t2748928126 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		U3CU3E__AnonType0_2_t2748928126 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t645512719 * L_2 = ((  EqualityComparer_1_t645512719 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_3 = (int32_t)__this->get_U3CCountU3E_0();
		U3CU3E__AnonType0_2_t2748928126 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get_U3CCountU3E_0();
		NullCheck((EqualityComparer_1_t645512719 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::Equals(!0,!0) */, (EqualityComparer_1_t645512719 *)L_2, (int32_t)L_3, (int32_t)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t1263084566 * L_7 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CMembersU3E_1();
		U3CU3E__AnonType0_2_t2748928126 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3CMembersU3E_1();
		NullCheck((EqualityComparer_1_t1263084566 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t1263084566 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType0`2<System.Int32,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType0_2_GetHashCode_m595221463_gshared (U3CU3E__AnonType0_2_t2748928126 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t645512719 * L_0 = ((  EqualityComparer_1_t645512719 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_1 = (int32_t)__this->get_U3CCountU3E_0();
		NullCheck((EqualityComparer_1_t645512719 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::GetHashCode(!0) */, (EqualityComparer_1_t645512719 *)L_0, (int32_t)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t1263084566 * L_3 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CMembersU3E_1();
		NullCheck((EqualityComparer_1_t1263084566 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t1263084566 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType0`2<System.Int32,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029399;
extern Il2CppCodeGenString* _stringLiteral2062904102;
extern Il2CppCodeGenString* _stringLiteral111738354;
extern Il2CppCodeGenString* _stringLiteral1947253693;
extern const uint32_t U3CU3E__AnonType0_2_ToString_m4103874651_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType0_2_ToString_m4103874651_gshared (U3CU3E__AnonType0_2_t2748928126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType0_2_ToString_m4103874651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029399);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029399);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral2062904102);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2062904102);
		StringU5BU5D_t1642385972* L_2 = (StringU5BU5D_t1642385972*)L_1;
		int32_t L_3 = (int32_t)__this->get_U3CCountU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
	}
	{
		int32_t L_4 = (int32_t)__this->get_U3CCountU3E_0();
		V_0 = (int32_t)L_4;
		String_t* L_5 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral111738354);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral111738354);
		StringU5BU5D_t1642385972* L_8 = (StringU5BU5D_t1642385972*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CMembersU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CMembersU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1947253693);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1947253693);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType0`2<System.Object,System.Object>::.ctor(<Count>__T,<Members>__T)
extern "C"  void U3CU3E__AnonType0_2__ctor_m1715261235_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, Il2CppObject * ___Count0, Il2CppObject * ___Members1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___Count0;
		__this->set_U3CCountU3E_0(L_0);
		Il2CppObject * L_1 = ___Members1;
		__this->set_U3CMembersU3E_1(L_1);
		return;
	}
}
// <Count>__T <>__AnonType0`2<System.Object,System.Object>::get_Count()
extern "C"  Il2CppObject * U3CU3E__AnonType0_2_get_Count_m281527827_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CCountU3E_0();
		return L_0;
	}
}
// <Members>__T <>__AnonType0`2<System.Object,System.Object>::get_Members()
extern "C"  Il2CppObject * U3CU3E__AnonType0_2_get_Members_m4072232531_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CMembersU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType0`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType0_2_Equals_m2151427210_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType0_2_t3333162619 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType0_2_t3333162619 *)((U3CU3E__AnonType0_2_t3333162619 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		U3CU3E__AnonType0_2_t3333162619 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t1263084566 * L_2 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CCountU3E_0();
		U3CU3E__AnonType0_2_t3333162619 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3CCountU3E_0();
		NullCheck((EqualityComparer_1_t1263084566 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t1263084566 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t1263084566 * L_7 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CMembersU3E_1();
		U3CU3E__AnonType0_2_t3333162619 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3CMembersU3E_1();
		NullCheck((EqualityComparer_1_t1263084566 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t1263084566 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType0`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType0_2_GetHashCode_m2678831190_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t1263084566 * L_0 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CCountU3E_0();
		NullCheck((EqualityComparer_1_t1263084566 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t1263084566 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t1263084566 * L_3 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CMembersU3E_1();
		NullCheck((EqualityComparer_1_t1263084566 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t1263084566 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType0`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029399;
extern Il2CppCodeGenString* _stringLiteral2062904102;
extern Il2CppCodeGenString* _stringLiteral111738354;
extern Il2CppCodeGenString* _stringLiteral1947253693;
extern const uint32_t U3CU3E__AnonType0_2_ToString_m2057022320_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType0_2_ToString_m2057022320_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType0_2_ToString_m2057022320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029399);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029399);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral2062904102);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2062904102);
		StringU5BU5D_t1642385972* L_2 = (StringU5BU5D_t1642385972*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CCountU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CCountU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral111738354);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral111738354);
		StringU5BU5D_t1642385972* L_8 = (StringU5BU5D_t1642385972*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CMembersU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CMembersU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1947253693);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1947253693);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CSendRequestCoroutineU3Ec__Iterator18_2__ctor_m1865567309_gshared (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendRequestCoroutineU3Ec__Iterator18_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m166941371_gshared (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_14();
		return L_0;
	}
}
// System.Object MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendRequestCoroutineU3Ec__Iterator18_2_System_Collections_IEnumerator_get_Current_m1216179635_gshared (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_14();
		return L_0;
	}
}
// System.Boolean MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonConvert_t3949895659_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* MessageSender_t204794706_il2cpp_TypeInfo_var;
extern Il2CppClass* UTF8Encoding_t111055448_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t969056901_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m760167321_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m2509825489_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m971722805_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m648921317_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1407444003_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1321351356_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2157526626_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral411082961;
extern Il2CppCodeGenString* _stringLiteral4140602317;
extern Il2CppCodeGenString* _stringLiteral1048821954;
extern Il2CppCodeGenString* _stringLiteral1308856144;
extern Il2CppCodeGenString* _stringLiteral677017058;
extern Il2CppCodeGenString* _stringLiteral2167658612;
extern Il2CppCodeGenString* _stringLiteral562836579;
extern Il2CppCodeGenString* _stringLiteral398024830;
extern const uint32_t U3CSendRequestCoroutineU3Ec__Iterator18_2_MoveNext_m2351241099_MetadataUsageId;
extern "C"  bool U3CSendRequestCoroutineU3Ec__Iterator18_2_MoveNext_m2351241099_gshared (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSendRequestCoroutineU3Ec__Iterator18_2_MoveNext_m2351241099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_13();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_13((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_017c;
		}
	}
	{
		goto IL_0336;
	}

IL_0021:
	{
		Action_2_t2525452034 * L_2 = (Action_2_t2525452034 *)__this->get_callback_0();
		__this->set_U3ClocalU3E__0_1(L_2);
		Il2CppObject ** L_3 = (Il2CppObject **)__this->get_address_of_request_2();
		NullCheck((Request_t3213492751 *)(*L_3));
		Request_SetType_m2926140511((Request_t3213492751 *)(*L_3), /*hidden argument*/NULL);
		Il2CppObject ** L_4 = (Il2CppObject **)__this->get_address_of_request_2();
		NullCheck((Il2CppObject *)(*L_4));
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)(*L_4), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral411082961, (String_t*)L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		int32_t L_8 = Application_get_internetReachability_m1191733175(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral4140602317, /*hidden argument*/NULL);
		Action_2_t2525452034 * L_9 = (Action_2_t2525452034 *)__this->get_U3ClocalU3E__0_1();
		NullCheck((Action_2_t2525452034 *)L_9);
		((  void (*) (Action_2_t2525452034 *, bool, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Action_2_t2525452034 *)L_9, (bool)0, (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		goto IL_0336;
	}

IL_008e:
	{
		Il2CppObject ** L_10 = (Il2CppObject **)__this->get_address_of_request_2();
		NullCheck((Request_t3213492751 *)(*L_10));
		Request_set_api_version_m3493102760((Request_t3213492751 *)(*L_10), (int32_t)2, /*hidden argument*/NULL);
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_request_2();
		IL2CPP_RUNTIME_CLASS_INIT(JsonConvert_t3949895659_il2cpp_TypeInfo_var);
		String_t* L_12 = JsonConvert_SerializeObject_m3256232875(NULL /*static, unused*/, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		__this->set_U3CjsonDataU3E__1_3(L_12);
		Dictionary_2_t3943999495 * L_13 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m760167321(L_13, /*hidden argument*/Dictionary_2__ctor_m760167321_MethodInfo_var);
		__this->set_U3CpostHeaderU3E__2_4(L_13);
		Dictionary_2_t3943999495 * L_14 = (Dictionary_2_t3943999495 *)__this->get_U3CpostHeaderU3E__2_4();
		NullCheck((Dictionary_2_t3943999495 *)L_14);
		Dictionary_2_Add_m2509825489((Dictionary_2_t3943999495 *)L_14, (String_t*)_stringLiteral1048821954, (String_t*)_stringLiteral1308856144, /*hidden argument*/Dictionary_2_Add_m2509825489_MethodInfo_var);
		Dictionary_2_t3943999495 * L_15 = (Dictionary_2_t3943999495 *)__this->get_U3CpostHeaderU3E__2_4();
		String_t* L_16 = (String_t*)__this->get_U3CjsonDataU3E__1_3();
		NullCheck((String_t*)L_16);
		int32_t L_17 = String_get_Length_m1606060069((String_t*)L_16, /*hidden argument*/NULL);
		V_1 = (int32_t)L_17;
		String_t* L_18 = Int32_ToString_m2960866144((int32_t*)(&V_1), /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t3943999495 *)L_15);
		Dictionary_2_Add_m2509825489((Dictionary_2_t3943999495 *)L_15, (String_t*)_stringLiteral677017058, (String_t*)L_18, /*hidden argument*/Dictionary_2_Add_m2509825489_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MessageSender_t204794706_il2cpp_TypeInfo_var);
		String_t* L_19 = ((MessageSender_t204794706_StaticFields*)MessageSender_t204794706_il2cpp_TypeInfo_var->static_fields)->get_setCookie_3();
		if (!L_19)
		{
			goto IL_012c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MessageSender_t204794706_il2cpp_TypeInfo_var);
		String_t* L_20 = ((MessageSender_t204794706_StaticFields*)MessageSender_t204794706_il2cpp_TypeInfo_var->static_fields)->get_setCookie_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_22 = String_op_Inequality_m304203149(NULL /*static, unused*/, (String_t*)L_20, (String_t*)L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_012c;
		}
	}
	{
		Dictionary_2_t3943999495 * L_23 = (Dictionary_2_t3943999495 *)__this->get_U3CpostHeaderU3E__2_4();
		IL2CPP_RUNTIME_CLASS_INIT(MessageSender_t204794706_il2cpp_TypeInfo_var);
		String_t* L_24 = ((MessageSender_t204794706_StaticFields*)MessageSender_t204794706_il2cpp_TypeInfo_var->static_fields)->get_setCookie_3();
		NullCheck((Dictionary_2_t3943999495 *)L_23);
		Dictionary_2_Add_m2509825489((Dictionary_2_t3943999495 *)L_23, (String_t*)_stringLiteral2167658612, (String_t*)L_24, /*hidden argument*/Dictionary_2_Add_m2509825489_MethodInfo_var);
	}

IL_012c:
	{
		UTF8Encoding_t111055448 * L_25 = (UTF8Encoding_t111055448 *)il2cpp_codegen_object_new(UTF8Encoding_t111055448_il2cpp_TypeInfo_var);
		UTF8Encoding__ctor_m100325490(L_25, /*hidden argument*/NULL);
		__this->set_U3CencodingU3E__3_5(L_25);
		MessageSender_t204794706 * L_26 = (MessageSender_t204794706 *)__this->get_U3CU3Ef__this_17();
		NullCheck(L_26);
		String_t* L_27 = (String_t*)L_26->get__postUrl_1();
		UTF8Encoding_t111055448 * L_28 = (UTF8Encoding_t111055448 *)__this->get_U3CencodingU3E__3_5();
		String_t* L_29 = (String_t*)__this->get_U3CjsonDataU3E__1_3();
		NullCheck((Encoding_t663144255 *)L_28);
		ByteU5BU5D_t3397334013* L_30 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, (Encoding_t663144255 *)L_28, (String_t*)L_29);
		Dictionary_2_t3943999495 * L_31 = (Dictionary_2_t3943999495 *)__this->get_U3CpostHeaderU3E__2_4();
		WWW_t2919945039 * L_32 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m1577105574(L_32, (String_t*)L_27, (ByteU5BU5D_t3397334013*)L_30, (Dictionary_2_t3943999495 *)L_31, /*hidden argument*/NULL);
		__this->set_U3CwwwRequestU3E__4_6(L_32);
		WWW_t2919945039 * L_33 = (WWW_t2919945039 *)__this->get_U3CwwwRequestU3E__4_6();
		__this->set_U24current_14(L_33);
		__this->set_U24PC_13(1);
		goto IL_0338;
	}

IL_017c:
	{
		WWW_t2919945039 * L_34 = (WWW_t2919945039 *)__this->get_U3CwwwRequestU3E__4_6();
		NullCheck((WWW_t2919945039 *)L_34);
		Dictionary_2_t3943999495 * L_35 = WWW_get_responseHeaders_m977130892((WWW_t2919945039 *)L_34, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t3943999495 *)L_35);
		Enumerator_t969056901  L_36 = Dictionary_2_GetEnumerator_m971722805((Dictionary_2_t3943999495 *)L_35, /*hidden argument*/Dictionary_2_GetEnumerator_m971722805_MethodInfo_var);
		__this->set_U3CU24s_330U3E__5_7(L_36);
	}

IL_0192:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01d7;
		}

IL_0197:
		{
			Enumerator_t969056901 * L_37 = (Enumerator_t969056901 *)__this->get_address_of_U3CU24s_330U3E__5_7();
			KeyValuePair_2_t1701344717  L_38 = Enumerator_get_Current_m648921317((Enumerator_t969056901 *)L_37, /*hidden argument*/Enumerator_get_Current_m648921317_MethodInfo_var);
			__this->set_U3CitemU3E__6_8(L_38);
			KeyValuePair_2_t1701344717 * L_39 = (KeyValuePair_2_t1701344717 *)__this->get_address_of_U3CitemU3E__6_8();
			String_t* L_40 = KeyValuePair_2_get_Key_m1407444003((KeyValuePair_2_t1701344717 *)L_39, /*hidden argument*/KeyValuePair_2_get_Key_m1407444003_MethodInfo_var);
			NullCheck((String_t*)L_40);
			String_t* L_41 = String_ToLower_m2994460523((String_t*)L_40, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_42 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_41, (String_t*)_stringLiteral562836579, /*hidden argument*/NULL);
			if (!L_42)
			{
				goto IL_01d7;
			}
		}

IL_01c7:
		{
			KeyValuePair_2_t1701344717 * L_43 = (KeyValuePair_2_t1701344717 *)__this->get_address_of_U3CitemU3E__6_8();
			String_t* L_44 = KeyValuePair_2_get_Value_m1321351356((KeyValuePair_2_t1701344717 *)L_43, /*hidden argument*/KeyValuePair_2_get_Value_m1321351356_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(MessageSender_t204794706_il2cpp_TypeInfo_var);
			((MessageSender_t204794706_StaticFields*)MessageSender_t204794706_il2cpp_TypeInfo_var->static_fields)->set_setCookie_3(L_44);
		}

IL_01d7:
		{
			Enumerator_t969056901 * L_45 = (Enumerator_t969056901 *)__this->get_address_of_U3CU24s_330U3E__5_7();
			bool L_46 = Enumerator_MoveNext_m2157526626((Enumerator_t969056901 *)L_45, /*hidden argument*/Enumerator_MoveNext_m2157526626_MethodInfo_var);
			if (L_46)
			{
				goto IL_0197;
			}
		}

IL_01e7:
		{
			IL2CPP_LEAVE(0x1FD, FINALLY_01ec);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01ec;
	}

FINALLY_01ec:
	{ // begin finally (depth: 1)
		Enumerator_t969056901  L_47 = (Enumerator_t969056901 )__this->get_U3CU24s_330U3E__5_7();
		Enumerator_t969056901  L_48 = L_47;
		Il2CppObject * L_49 = Box(Enumerator_t969056901_il2cpp_TypeInfo_var, &L_48);
		NullCheck((Il2CppObject *)L_49);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_49);
		IL2CPP_END_FINALLY(492)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(492)
	{
		IL2CPP_JUMP_TBL(0x1FD, IL_01fd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01fd:
	{
		Il2CppObject ** L_50 = (Il2CppObject **)__this->get_address_of_request_2();
		NullCheck((Il2CppObject *)(*L_50));
		Type_t * L_51 = Object_GetType_m191970594((Il2CppObject *)(*L_50), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_51);
		String_t* L_52 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_51);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral398024830, (String_t*)L_52, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_53, /*hidden argument*/NULL);
		__this->set_U3CerrorU3E__7_9((bool)0);
		WWW_t2919945039 * L_54 = (WWW_t2919945039 *)__this->get_U3CwwwRequestU3E__4_6();
		NullCheck((WWW_t2919945039 *)L_54);
		String_t* L_55 = WWW_get_error_m3092701216((WWW_t2919945039 *)L_54, /*hidden argument*/NULL);
		bool L_56 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, (String_t*)L_55, /*hidden argument*/NULL);
		if (L_56)
		{
			goto IL_0245;
		}
	}
	{
		__this->set_U3CerrorU3E__7_9((bool)1);
	}

IL_0245:
	{
		WWW_t2919945039 * L_57 = (WWW_t2919945039 *)__this->get_U3CwwwRequestU3E__4_6();
		NullCheck((WWW_t2919945039 *)L_57);
		String_t* L_58 = WWW_get_text_m1558985139((WWW_t2919945039 *)L_57, /*hidden argument*/NULL);
		NullCheck((String_t*)L_58);
		int32_t L_59 = String_IndexOf_m2358239236((String_t*)L_58, (Il2CppChar)((int32_t)123), /*hidden argument*/NULL);
		__this->set_U3CindexU3E__8_10(L_59);
		int32_t L_60 = (int32_t)__this->get_U3CindexU3E__8_10();
		if ((!(((uint32_t)L_60) == ((uint32_t)(-1)))))
		{
			goto IL_0270;
		}
	}
	{
		__this->set_U3CerrorU3E__7_9((bool)1);
	}

IL_0270:
	{
		bool L_61 = (bool)__this->get_U3CerrorU3E__7_9();
		if (L_61)
		{
			goto IL_02fd;
		}
	}
	{
		WWW_t2919945039 * L_62 = (WWW_t2919945039 *)__this->get_U3CwwwRequestU3E__4_6();
		NullCheck((WWW_t2919945039 *)L_62);
		String_t* L_63 = WWW_get_text_m1558985139((WWW_t2919945039 *)L_62, /*hidden argument*/NULL);
		int32_t L_64 = (int32_t)__this->get_U3CindexU3E__8_10();
		WWW_t2919945039 * L_65 = (WWW_t2919945039 *)__this->get_U3CwwwRequestU3E__4_6();
		NullCheck((WWW_t2919945039 *)L_65);
		String_t* L_66 = WWW_get_text_m1558985139((WWW_t2919945039 *)L_65, /*hidden argument*/NULL);
		NullCheck((String_t*)L_66);
		int32_t L_67 = String_get_Length_m1606060069((String_t*)L_66, /*hidden argument*/NULL);
		int32_t L_68 = (int32_t)__this->get_U3CindexU3E__8_10();
		NullCheck((String_t*)L_63);
		String_t* L_69 = String_Substring_m12482732((String_t*)L_63, (int32_t)L_64, (int32_t)((int32_t)((int32_t)L_67-(int32_t)L_68)), /*hidden argument*/NULL);
		__this->set_U3CtempU3E__9_11(L_69);
		String_t* L_70 = (String_t*)__this->get_U3CtempU3E__9_11();
		IL2CPP_RUNTIME_CLASS_INIT(JsonConvert_t3949895659_il2cpp_TypeInfo_var);
		Il2CppObject * L_71 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (String_t*)L_70, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		__this->set_U3CresponseU3E__10_12(L_71);
		Il2CppObject * L_72 = (Il2CppObject *)__this->get_U3CresponseU3E__10_12();
		if (L_72)
		{
			goto IL_02e6;
		}
	}
	{
		Action_2_t2525452034 * L_73 = (Action_2_t2525452034 *)__this->get_U3ClocalU3E__0_1();
		NullCheck((Action_2_t2525452034 *)L_73);
		((  void (*) (Action_2_t2525452034 *, bool, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Action_2_t2525452034 *)L_73, (bool)0, (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		goto IL_0336;
	}

IL_02e6:
	{
		Action_2_t2525452034 * L_74 = (Action_2_t2525452034 *)__this->get_U3ClocalU3E__0_1();
		Il2CppObject * L_75 = (Il2CppObject *)__this->get_U3CresponseU3E__10_12();
		NullCheck((Action_2_t2525452034 *)L_74);
		((  void (*) (Action_2_t2525452034 *, bool, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Action_2_t2525452034 *)L_74, (bool)1, (Il2CppObject *)L_75, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		goto IL_032f;
	}

IL_02fd:
	{
		WWW_t2919945039 * L_76 = (WWW_t2919945039 *)__this->get_U3CwwwRequestU3E__4_6();
		NullCheck((WWW_t2919945039 *)L_76);
		String_t* L_77 = WWW_get_error_m3092701216((WWW_t2919945039 *)L_76, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_77, /*hidden argument*/NULL);
		WWW_t2919945039 * L_78 = (WWW_t2919945039 *)__this->get_U3CwwwRequestU3E__4_6();
		NullCheck((WWW_t2919945039 *)L_78);
		String_t* L_79 = WWW_get_text_m1558985139((WWW_t2919945039 *)L_78, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)L_79, /*hidden argument*/NULL);
		Action_2_t2525452034 * L_80 = (Action_2_t2525452034 *)__this->get_U3ClocalU3E__0_1();
		NullCheck((Action_2_t2525452034 *)L_80);
		((  void (*) (Action_2_t2525452034 *, bool, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Action_2_t2525452034 *)L_80, (bool)0, (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
	}

IL_032f:
	{
		__this->set_U24PC_13((-1));
	}

IL_0336:
	{
		return (bool)0;
	}

IL_0338:
	{
		return (bool)1;
	}
	// Dead block : IL_033a: ldloc.2
}
// System.Void MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CSendRequestCoroutineU3Ec__Iterator18_2_Dispose_m329621930_gshared (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_13((-1));
		return;
	}
}
// System.Void MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendRequestCoroutineU3Ec__Iterator18_2_Reset_m1034124292_MetadataUsageId;
extern "C"  void U3CSendRequestCoroutineU3Ec__Iterator18_2_Reset_m1034124292_gshared (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSendRequestCoroutineU3Ec__Iterator18_2_Reset_m1034124292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>::.ctor()
extern "C"  void CustomCreationConverter_1__ctor_m2235784744_gshared (CustomCreationConverter_1_t4070253483 * __this, const MethodInfo* method)
{
	{
		NullCheck((JsonConverter_t1964060750 *)__this);
		JsonConverter__ctor_m3599769528((JsonConverter_t1964060750 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral718289190;
extern const uint32_t CustomCreationConverter_1_WriteJson_m466062590_MetadataUsageId;
extern "C"  void CustomCreationConverter_1_WriteJson_m466062590_gshared (CustomCreationConverter_1_t4070253483 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t1719617802 * ___serializer2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CustomCreationConverter_1_WriteJson_m466062590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral718289190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern Il2CppClass* JsonSerializationException_t1492322735_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2807571058;
extern const uint32_t CustomCreationConverter_1_ReadJson_m105648253_MetadataUsageId;
extern "C"  Il2CppObject * CustomCreationConverter_1_ReadJson_m105648253_gshared (CustomCreationConverter_1_t4070253483 * __this, JsonReader_t3154730733 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t1719617802 * ___serializer3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CustomCreationConverter_1_ReadJson_m105648253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		JsonReader_t3154730733 * L_0 = ___reader0;
		NullCheck((JsonReader_t3154730733 *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(7 /* Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::get_TokenType() */, (JsonReader_t3154730733 *)L_0);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_000f;
		}
	}
	{
		return NULL;
	}

IL_000f:
	{
		Type_t * L_2 = ___objectType1;
		NullCheck((CustomCreationConverter_1_t4070253483 *)__this);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, Type_t * >::Invoke(10 /* T Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>::Create(System.Type) */, (CustomCreationConverter_1_t4070253483 *)__this, (Type_t *)L_2);
		V_0 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_0;
		if (L_4)
		{
			goto IL_002d;
		}
	}
	{
		JsonSerializationException_t1492322735 * L_5 = (JsonSerializationException_t1492322735 *)il2cpp_codegen_object_new(JsonSerializationException_t1492322735_il2cpp_TypeInfo_var);
		JsonSerializationException__ctor_m2679515131(L_5, (String_t*)_stringLiteral2807571058, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002d:
	{
		JsonSerializer_t1719617802 * L_6 = ___serializer3;
		JsonReader_t3154730733 * L_7 = ___reader0;
		Il2CppObject * L_8 = V_0;
		NullCheck((JsonSerializer_t1719617802 *)L_6);
		JsonSerializer_Populate_m2792866844((JsonSerializer_t1719617802 *)L_6, (JsonReader_t3154730733 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		Il2CppObject * L_9 = V_0;
		return L_9;
	}
}
// System.Boolean Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>::CanConvert(System.Type)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t CustomCreationConverter_1_CanConvert_m4219581486_MetadataUsageId;
extern "C"  bool CustomCreationConverter_1_CanConvert_m4219581486_gshared (CustomCreationConverter_1_t4070253483 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CustomCreationConverter_1_CanConvert_m4219581486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		Type_t * L_1 = ___objectType0;
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		return L_2;
	}
}
// System.Boolean Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>::get_CanWrite()
extern "C"  bool CustomCreationConverter_1_get_CanWrite_m2403534380_gshared (CustomCreationConverter_1_t4070253483 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppCodeGenString* _stringLiteral1273574972;
extern const uint32_t JEnumerable_1__ctor_m1860534610_MetadataUsageId;
extern "C"  void JEnumerable_1__ctor_m1860534610_gshared (JEnumerable_1_t3957742755 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JEnumerable_1__ctor_m1860534610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___enumerable0;
		ValidationUtils_ArgumentNotNull_m3243187033(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral1273574972, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___enumerable0;
		__this->set__enumerable_1(L_1);
		return;
	}
}
extern "C"  void JEnumerable_1__ctor_m1860534610_AdjustorThunk (Il2CppObject * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	JEnumerable_1_t3957742755 * _thisAdjusted = reinterpret_cast<JEnumerable_1_t3957742755 *>(__this + 1);
	JEnumerable_1__ctor_m1860534610(_thisAdjusted, ___enumerable0, method);
}
// System.Void Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::.cctor()
extern "C"  void JEnumerable_1__cctor_m3633367596_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		JEnumerable_1_t3957742755  L_1;
		memset(&L_1, 0, sizeof(L_1));
		JEnumerable_1__ctor_m1860534610(&L_1, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((JEnumerable_1_t3957742755_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set_Empty_0(L_1);
		return;
	}
}
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m2922461236_gshared (JEnumerable_1_t3957742755 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = JEnumerable_1_GetEnumerator_m1984457945((JEnumerable_1_t3957742755 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_0;
	}
}
extern "C"  Il2CppObject * JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m2922461236_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	JEnumerable_1_t3957742755 * _thisAdjusted = reinterpret_cast<JEnumerable_1_t3957742755 *>(__this + 1);
	return JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m2922461236(_thisAdjusted, method);
}
// System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* JEnumerable_1_GetEnumerator_m1984457945_gshared (JEnumerable_1_t3957742755 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__enumerable_1();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0);
		return L_1;
	}
}
extern "C"  Il2CppObject* JEnumerable_1_GetEnumerator_m1984457945_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	JEnumerable_1_t3957742755 * _thisAdjusted = reinterpret_cast<JEnumerable_1_t3957742755 *>(__this + 1);
	return JEnumerable_1_GetEnumerator_m1984457945(_thisAdjusted, method);
}
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::get_Item(System.Object)
extern Il2CppClass* JEnumerable_1_t3820937473_il2cpp_TypeInfo_var;
extern const MethodInfo* JEnumerable_1__ctor_m1623461924_MethodInfo_var;
extern const uint32_t JEnumerable_1_get_Item_m2121380803_MetadataUsageId;
extern "C"  Il2CppObject* JEnumerable_1_get_Item_m2121380803_gshared (JEnumerable_1_t3957742755 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JEnumerable_1_get_Item_m2121380803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__enumerable_1();
		Il2CppObject * L_1 = ___key0;
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		JEnumerable_1_t3820937473  L_3;
		memset(&L_3, 0, sizeof(L_3));
		JEnumerable_1__ctor_m1623461924(&L_3, (Il2CppObject*)L_2, /*hidden argument*/JEnumerable_1__ctor_m1623461924_MethodInfo_var);
		JEnumerable_1_t3820937473  L_4 = L_3;
		Il2CppObject * L_5 = Box(JEnumerable_1_t3820937473_il2cpp_TypeInfo_var, &L_4);
		return (Il2CppObject*)L_5;
	}
}
extern "C"  Il2CppObject* JEnumerable_1_get_Item_m2121380803_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	JEnumerable_1_t3957742755 * _thisAdjusted = reinterpret_cast<JEnumerable_1_t3957742755 *>(__this + 1);
	return JEnumerable_1_get_Item_m2121380803(_thisAdjusted, ___key0, method);
}
// System.Boolean Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::Equals(System.Object)
extern "C"  bool JEnumerable_1_Equals_m1085474098_gshared (JEnumerable_1_t3957742755 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	JEnumerable_1_t3957742755  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__enumerable_1();
		Il2CppObject * L_2 = ___obj0;
		V_0 = (JEnumerable_1_t3957742755 )((*(JEnumerable_1_t3957742755 *)((JEnumerable_1_t3957742755 *)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)))));
		Il2CppObject* L_3 = (Il2CppObject*)(&V_0)->get__enumerable_1();
		NullCheck((Il2CppObject *)L_1);
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_1, (Il2CppObject *)L_3);
		return L_4;
	}

IL_0025:
	{
		return (bool)0;
	}
}
extern "C"  bool JEnumerable_1_Equals_m1085474098_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	JEnumerable_1_t3957742755 * _thisAdjusted = reinterpret_cast<JEnumerable_1_t3957742755 *>(__this + 1);
	return JEnumerable_1_Equals_m1085474098(_thisAdjusted, ___obj0, method);
}
// System.Int32 Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::GetHashCode()
extern "C"  int32_t JEnumerable_1_GetHashCode_m1140639406_gshared (JEnumerable_1_t3957742755 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__enumerable_1();
		NullCheck((Il2CppObject *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_0);
		return L_1;
	}
}
extern "C"  int32_t JEnumerable_1_GetHashCode_m1140639406_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	JEnumerable_1_t3957742755 * _thisAdjusted = reinterpret_cast<JEnumerable_1_t3957742755 *>(__this + 1);
	return JEnumerable_1_GetHashCode_m1140639406(_thisAdjusted, method);
}
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CConvertU3Ec__Iterator13_2__ctor_m2062413161_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// U Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<U>.get_Current()
extern "C"  Il2CppObject * U3CConvertU3Ec__Iterator13_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m423894566_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CConvertU3Ec__Iterator13_2_System_Collections_IEnumerator_get_Current_m2590672771_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CConvertU3Ec__Iterator13_2_System_Collections_IEnumerable_GetEnumerator_m2427907510_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CConvertU3Ec__Iterator13_2_t1287029637 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CConvertU3Ec__Iterator13_2_t1287029637 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CConvertU3Ec__Iterator13_2_t1287029637 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<U> Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<U>.GetEnumerator()
extern "C"  Il2CppObject* U3CConvertU3Ec__Iterator13_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m4188189733_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method)
{
	U3CConvertU3Ec__Iterator13_2_t1287029637 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CConvertU3Ec__Iterator13_2_t1287029637 * L_2 = (U3CConvertU3Ec__Iterator13_2_t1287029637 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CConvertU3Ec__Iterator13_2_t1287029637 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CConvertU3Ec__Iterator13_2_t1287029637 *)L_2;
		U3CConvertU3Ec__Iterator13_2_t1287029637 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_5();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CConvertU3Ec__Iterator13_2_t1287029637 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4211174801;
extern const uint32_t U3CConvertU3Ec__Iterator13_2_MoveNext_m1934812863_MetadataUsageId;
extern "C"  bool U3CConvertU3Ec__Iterator13_2_MoveNext_m1934812863_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CConvertU3Ec__Iterator13_2_MoveNext_m1934812863_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_00c2;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		ValidationUtils_ArgumentNotNull_m3243187033(NULL /*static, unused*/, (Il2CppObject *)L_2, (String_t*)_stringLiteral4211174801, /*hidden argument*/NULL);
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_3);
		__this->set_U3CU24s_250U3E__0_1(L_4);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0047:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
			{
				goto IL_008d;
			}
		}

IL_0053:
		{
			goto IL_008d;
		}

IL_0058:
		{
			Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24s_250U3E__0_1();
			NullCheck((Il2CppObject*)L_6);
			Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_6);
			__this->set_U3CtokenU3E__1_2((JToken_t2552644013 *)L_7);
			JToken_t2552644013 * L_8 = (JToken_t2552644013 *)__this->get_U3CtokenU3E__1_2();
			Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, JToken_t2552644013 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (JToken_t2552644013 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			__this->set_U24current_4(L_9);
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC4, FINALLY_00a2);
		}

IL_008d:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_250U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0058;
			}
		}

IL_009d:
		{
			IL2CPP_LEAVE(0xBB, FINALLY_00a2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00a2;
	}

FINALLY_00a2:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_00a6;
			}
		}

IL_00a5:
		{
			IL2CPP_END_FINALLY(162)
		}

IL_00a6:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_250U3E__0_1();
			if (L_13)
			{
				goto IL_00af;
			}
		}

IL_00ae:
		{
			IL2CPP_END_FINALLY(162)
		}

IL_00af:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_250U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(162)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(162)
	{
		IL2CPP_JUMP_TBL(0xC4, IL_00c4)
		IL2CPP_JUMP_TBL(0xBB, IL_00bb)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00bb:
	{
		__this->set_U24PC_3((-1));
	}

IL_00c2:
	{
		return (bool)0;
	}

IL_00c4:
	{
		return (bool)1;
	}
	// Dead block : IL_00c6: ldloc.2
}
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CConvertU3Ec__Iterator13_2_Dispose_m4160366314_MetadataUsageId;
extern "C"  void U3CConvertU3Ec__Iterator13_2_Dispose_m4160366314_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CConvertU3Ec__Iterator13_2_Dispose_m4160366314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_250U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_250U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CConvertU3Ec__Iterator13_2_Reset_m398725548_MetadataUsageId;
extern "C"  void U3CConvertU3Ec__Iterator13_2_Reset_m398725548_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CConvertU3Ec__Iterator13_2_Reset_m398725548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CValuesU3Ec__Iterator12_2__ctor_m227893975_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// U Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<U>.get_Current()
extern "C"  Il2CppObject * U3CValuesU3Ec__Iterator12_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m810599112_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_8();
		return L_0;
	}
}
// System.Object Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CValuesU3Ec__Iterator12_2_System_Collections_IEnumerator_get_Current_m20675489_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_8();
		return L_0;
	}
}
// System.Collections.IEnumerator Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CValuesU3Ec__Iterator12_2_System_Collections_IEnumerable_GetEnumerator_m2590481248_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CValuesU3Ec__Iterator12_2_t1507201787 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CValuesU3Ec__Iterator12_2_t1507201787 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CValuesU3Ec__Iterator12_2_t1507201787 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<U> Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<U>.GetEnumerator()
extern "C"  Il2CppObject* U3CValuesU3Ec__Iterator12_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m685739343_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method)
{
	U3CValuesU3Ec__Iterator12_2_t1507201787 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_7();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CValuesU3Ec__Iterator12_2_t1507201787 * L_2 = (U3CValuesU3Ec__Iterator12_2_t1507201787 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CValuesU3Ec__Iterator12_2_t1507201787 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CValuesU3Ec__Iterator12_2_t1507201787 *)L_2;
		U3CValuesU3Ec__Iterator12_2_t1507201787 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_9();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CValuesU3Ec__Iterator12_2_t1507201787 * L_5 = V_0;
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_U3CU24U3Ekey_10();
		NullCheck(L_5);
		L_5->set_key_3(L_6);
		U3CValuesU3Ec__Iterator12_2_t1507201787 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* JValue_t300956845_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t28167840_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* JEnumerable_1_GetEnumerator_m2701238769_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4211174801;
extern const uint32_t U3CValuesU3Ec__Iterator12_2_MoveNext_m3515628229_MetadataUsageId;
extern "C"  bool U3CValuesU3Ec__Iterator12_2_MoveNext_m3515628229_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CValuesU3Ec__Iterator12_2_MoveNext_m3515628229_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	JEnumerable_1_t3820937473  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_7();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_7((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_004f;
		}
		if (L_1 == 2)
		{
			goto IL_004f;
		}
		if (L_1 == 3)
		{
			goto IL_004f;
		}
	}
	{
		goto IL_01cd;
	}

IL_002b:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		ValidationUtils_ArgumentNotNull_m3243187033(NULL /*static, unused*/, (Il2CppObject *)L_2, (String_t*)_stringLiteral4211174801, /*hidden argument*/NULL);
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_3);
		__this->set_U3CU24s_248U3E__0_1(L_4);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
			{
				goto IL_00bd;
			}
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 1)
			{
				goto IL_00de;
			}
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 2)
			{
				goto IL_0193;
			}
		}

IL_0063:
		{
			goto IL_0193;
		}

IL_0068:
		{
			Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24s_248U3E__0_1();
			NullCheck((Il2CppObject*)L_6);
			Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_6);
			__this->set_U3CtokenU3E__1_2((JToken_t2552644013 *)L_7);
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_key_3();
			if (L_8)
			{
				goto IL_0152;
			}
		}

IL_0089:
		{
			JToken_t2552644013 * L_9 = (JToken_t2552644013 *)__this->get_U3CtokenU3E__1_2();
			if (!((JValue_t300956845 *)IsInst(L_9, JValue_t300956845_il2cpp_TypeInfo_var)))
			{
				goto IL_00c2;
			}
		}

IL_0099:
		{
			JToken_t2552644013 * L_10 = (JToken_t2552644013 *)__this->get_U3CtokenU3E__1_2();
			Il2CppObject * L_11 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, JValue_t300956845 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (JValue_t300956845 *)((JValue_t300956845 *)Castclass(L_10, JValue_t300956845_il2cpp_TypeInfo_var)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			__this->set_U24current_8(L_11);
			__this->set_U24PC_7(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x1CF, FINALLY_01a8);
		}

IL_00bd:
		{
			goto IL_014d;
		}

IL_00c2:
		{
			JToken_t2552644013 * L_12 = (JToken_t2552644013 *)__this->get_U3CtokenU3E__1_2();
			NullCheck((JToken_t2552644013 *)L_12);
			JEnumerable_1_t3820937473  L_13 = VirtFuncInvoker0< JEnumerable_1_t3820937473  >::Invoke(20 /* Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::Children() */, (JToken_t2552644013 *)L_12);
			V_2 = (JEnumerable_1_t3820937473 )L_13;
			Il2CppObject* L_14 = JEnumerable_1_GetEnumerator_m2701238769((JEnumerable_1_t3820937473 *)(&V_2), /*hidden argument*/JEnumerable_1_GetEnumerator_m2701238769_MethodInfo_var);
			__this->set_U3CU24s_249U3E__2_4(L_14);
			V_0 = (uint32_t)((int32_t)-3);
		}

IL_00de:
		try
		{ // begin try (depth: 2)
			{
				uint32_t L_15 = V_0;
				if (((int32_t)((int32_t)L_15-(int32_t)2)) == 0)
				{
					goto IL_011f;
				}
			}

IL_00ea:
			{
				goto IL_011f;
			}

IL_00ef:
			{
				Il2CppObject* L_16 = (Il2CppObject*)__this->get_U3CU24s_249U3E__2_4();
				NullCheck((Il2CppObject*)L_16);
				JToken_t2552644013 * L_17 = InterfaceFuncInvoker0< JToken_t2552644013 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>::get_Current() */, IEnumerator_1_t28167840_il2cpp_TypeInfo_var, (Il2CppObject*)L_16);
				__this->set_U3CtU3E__3_5(L_17);
				JToken_t2552644013 * L_18 = (JToken_t2552644013 *)__this->get_U3CtU3E__3_5();
				Il2CppObject * L_19 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, JToken_t2552644013 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (JToken_t2552644013 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
				__this->set_U24current_8(L_19);
				__this->set_U24PC_7(2);
				V_1 = (bool)1;
				IL2CPP_LEAVE(0x1CF, FINALLY_0134);
			}

IL_011f:
			{
				Il2CppObject* L_20 = (Il2CppObject*)__this->get_U3CU24s_249U3E__2_4();
				NullCheck((Il2CppObject *)L_20);
				bool L_21 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_20);
				if (L_21)
				{
					goto IL_00ef;
				}
			}

IL_012f:
			{
				IL2CPP_LEAVE(0x14D, FINALLY_0134);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0134;
		}

FINALLY_0134:
		{ // begin finally (depth: 2)
			{
				bool L_22 = V_1;
				if (!L_22)
				{
					goto IL_0138;
				}
			}

IL_0137:
			{
				IL2CPP_END_FINALLY(308)
			}

IL_0138:
			{
				Il2CppObject* L_23 = (Il2CppObject*)__this->get_U3CU24s_249U3E__2_4();
				if (L_23)
				{
					goto IL_0141;
				}
			}

IL_0140:
			{
				IL2CPP_END_FINALLY(308)
			}

IL_0141:
			{
				Il2CppObject* L_24 = (Il2CppObject*)__this->get_U3CU24s_249U3E__2_4();
				NullCheck((Il2CppObject *)L_24);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_24);
				IL2CPP_END_FINALLY(308)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(308)
		{
			IL2CPP_END_CLEANUP(0x1CF, FINALLY_01a8);
			IL2CPP_JUMP_TBL(0x14D, IL_014d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_014d:
		{
			goto IL_0193;
		}

IL_0152:
		{
			JToken_t2552644013 * L_25 = (JToken_t2552644013 *)__this->get_U3CtokenU3E__1_2();
			Il2CppObject * L_26 = (Il2CppObject *)__this->get_key_3();
			NullCheck((JToken_t2552644013 *)L_25);
			JToken_t2552644013 * L_27 = VirtFuncInvoker1< JToken_t2552644013 *, Il2CppObject * >::Invoke(15 /* Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Item(System.Object) */, (JToken_t2552644013 *)L_25, (Il2CppObject *)L_26);
			__this->set_U3CvalueU3E__4_6(L_27);
			JToken_t2552644013 * L_28 = (JToken_t2552644013 *)__this->get_U3CvalueU3E__4_6();
			if (!L_28)
			{
				goto IL_0193;
			}
		}

IL_0174:
		{
			JToken_t2552644013 * L_29 = (JToken_t2552644013 *)__this->get_U3CvalueU3E__4_6();
			Il2CppObject * L_30 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, JToken_t2552644013 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (JToken_t2552644013 *)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
			__this->set_U24current_8(L_30);
			__this->set_U24PC_7(3);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x1CF, FINALLY_01a8);
		}

IL_0193:
		{
			Il2CppObject* L_31 = (Il2CppObject*)__this->get_U3CU24s_248U3E__0_1();
			NullCheck((Il2CppObject *)L_31);
			bool L_32 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_31);
			if (L_32)
			{
				goto IL_0068;
			}
		}

IL_01a3:
		{
			IL2CPP_LEAVE(0x1C1, FINALLY_01a8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01a8;
	}

FINALLY_01a8:
	{ // begin finally (depth: 1)
		{
			bool L_33 = V_1;
			if (!L_33)
			{
				goto IL_01ac;
			}
		}

IL_01ab:
		{
			IL2CPP_END_FINALLY(424)
		}

IL_01ac:
		{
			Il2CppObject* L_34 = (Il2CppObject*)__this->get_U3CU24s_248U3E__0_1();
			if (L_34)
			{
				goto IL_01b5;
			}
		}

IL_01b4:
		{
			IL2CPP_END_FINALLY(424)
		}

IL_01b5:
		{
			Il2CppObject* L_35 = (Il2CppObject*)__this->get_U3CU24s_248U3E__0_1();
			NullCheck((Il2CppObject *)L_35);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_35);
			IL2CPP_END_FINALLY(424)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(424)
	{
		IL2CPP_JUMP_TBL(0x1CF, IL_01cf)
		IL2CPP_JUMP_TBL(0x1C1, IL_01c1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01c1:
	{
		goto IL_01cd;
	}
	// Dead block : IL_01c6: ldarg.0

IL_01cd:
	{
		return (bool)0;
	}

IL_01cf:
	{
		return (bool)1;
	}
}
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CValuesU3Ec__Iterator12_2_Dispose_m1103924608_MetadataUsageId;
extern "C"  void U3CValuesU3Ec__Iterator12_2_Dispose_m1103924608_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CValuesU3Ec__Iterator12_2_Dispose_m1103924608_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_7();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0076;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0029;
		}
		if (L_1 == 3)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_0076;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_2 = V_0;
			if (((int32_t)((int32_t)L_2-(int32_t)1)) == 0)
			{
				goto IL_005c;
			}
			if (((int32_t)((int32_t)L_2-(int32_t)1)) == 1)
			{
				goto IL_0042;
			}
			if (((int32_t)((int32_t)L_2-(int32_t)1)) == 2)
			{
				goto IL_005c;
			}
		}

IL_003d:
		{
			goto IL_005c;
		}

IL_0042:
		try
		{ // begin try (depth: 2)
			IL2CPP_LEAVE(0x5C, FINALLY_0047);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0047;
		}

FINALLY_0047:
		{ // begin finally (depth: 2)
			{
				Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_249U3E__2_4();
				if (L_3)
				{
					goto IL_0050;
				}
			}

IL_004f:
			{
				IL2CPP_END_FINALLY(71)
			}

IL_0050:
			{
				Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24s_249U3E__2_4();
				NullCheck((Il2CppObject *)L_4);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
				IL2CPP_END_FINALLY(71)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(71)
		{
			IL2CPP_JUMP_TBL(0x5C, IL_005c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_005c:
		{
			IL2CPP_LEAVE(0x76, FINALLY_0061);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0061;
	}

FINALLY_0061:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_248U3E__0_1();
			if (L_5)
			{
				goto IL_006a;
			}
		}

IL_0069:
		{
			IL2CPP_END_FINALLY(97)
		}

IL_006a:
		{
			Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24s_248U3E__0_1();
			NullCheck((Il2CppObject *)L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			IL2CPP_END_FINALLY(97)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(97)
	{
		IL2CPP_JUMP_TBL(0x76, IL_0076)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0076:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CValuesU3Ec__Iterator12_2_Reset_m2841252134_MetadataUsageId;
extern "C"  void U3CValuesU3Ec__Iterator12_2_Reset_m2841252134_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CValuesU3Ec__Iterator12_2_Reset_m2841252134_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Object>::.cctor()
extern "C"  void CachedAttributeGetter_1__cctor_m1655531471_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t3334259717 * L_1 = (Func_2_t3334259717 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Func_2_t3334259717 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		ThreadSafeStore_2_t237141303 * L_2 = (ThreadSafeStore_2_t237141303 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ThreadSafeStore_2_t237141303 *, Func_2_t3334259717 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(L_2, (Func_2_t3334259717 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((CachedAttributeGetter_1_t666803094_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->static_fields)->set_TypeAttributeCache_0(L_2);
		return;
	}
}
// T Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Object>::GetAttribute(System.Reflection.ICustomAttributeProvider)
extern "C"  Il2CppObject * CachedAttributeGetter_1_GetAttribute_m2147397194_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___type0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ThreadSafeStore_2_t237141303 * L_0 = ((CachedAttributeGetter_1_t666803094_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->static_fields)->get_TypeAttributeCache_0();
		Il2CppObject * L_1 = ___type0;
		NullCheck((ThreadSafeStore_2_t237141303 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t237141303 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)((ThreadSafeStore_2_t237141303 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ObjectConstructor_1__ctor_m2980760032_gshared (ObjectConstructor_1_t1997612730 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Object Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>::Invoke(System.Object[])
extern "C"  Il2CppObject * ObjectConstructor_1_Invoke_m1698616533_gshared (ObjectConstructor_1_t1997612730 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ObjectConstructor_1_Invoke_m1698616533((ObjectConstructor_1_t1997612730 *)__this->get_prev_9(),___args0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___args0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___args0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___args0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ObjectConstructor_1_BeginInvoke_m1266121039_gshared (ObjectConstructor_1_t1997612730 * __this, ObjectU5BU5D_t3614634134* ___args0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___args0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Object Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ObjectConstructor_1_EndInvoke_m3898774875_gshared (ObjectConstructor_1_t1997612730 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void BidirectionalDictionary_2__ctor_m1119331919_gshared (BidirectionalDictionary_2_t1212012318 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		EqualityComparer_1_t1263084566 * L_0 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		EqualityComparer_1_t1263084566 * L_1 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((BidirectionalDictionary_2_t1212012318 *)__this);
		((  void (*) (BidirectionalDictionary_2_t1212012318 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BidirectionalDictionary_2_t1212012318 *)__this, (Il2CppObject*)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirst>,System.Collections.Generic.IEqualityComparer`1<TSecond>)
extern "C"  void BidirectionalDictionary_2__ctor_m1806502961_gshared (BidirectionalDictionary_2_t1212012318 * __this, Il2CppObject* ___firstEqualityComparer0, Il2CppObject* ___secondEqualityComparer1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___firstEqualityComparer0;
		Dictionary_2_t2281509423 * L_1 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_1, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		__this->set__firstToSecond_0(L_1);
		Il2CppObject* L_2 = ___secondEqualityComparer1;
		Dictionary_2_t2281509423 * L_3 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		__this->set__secondToFirst_1(L_3);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.Object,System.Object>::Add(TFirst,TSecond)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2800155566;
extern const uint32_t BidirectionalDictionary_2_Add_m190380356_MetadataUsageId;
extern "C"  void BidirectionalDictionary_2_Add_m190380356_gshared (BidirectionalDictionary_2_t1212012318 * __this, Il2CppObject * ___first0, Il2CppObject * ___second1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BidirectionalDictionary_2_Add_m190380356_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__firstToSecond_0();
		Il2CppObject * L_1 = ___first0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::ContainsKey(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get__secondToFirst_1();
		Il2CppObject * L_4 = ___second1;
		NullCheck((Il2CppObject*)L_3);
		bool L_5 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::ContainsKey(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
		if (!L_5)
		{
			goto IL_002d;
		}
	}

IL_0022:
	{
		ArgumentException_t3259014390 * L_6 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_6, (String_t*)_stringLiteral2800155566, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002d:
	{
		Il2CppObject* L_7 = (Il2CppObject*)__this->get__firstToSecond_0();
		Il2CppObject * L_8 = ___first0;
		Il2CppObject * L_9 = ___second1;
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Add(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9);
		Il2CppObject* L_10 = (Il2CppObject*)__this->get__secondToFirst_1();
		Il2CppObject * L_11 = ___second1;
		Il2CppObject * L_12 = ___first0;
		NullCheck((Il2CppObject*)L_10);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Add(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (Il2CppObject*)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12);
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.Object,System.Object>::TryGetByFirst(TFirst,TSecond&)
extern "C"  bool BidirectionalDictionary_2_TryGetByFirst_m1495460279_gshared (BidirectionalDictionary_2_t1212012318 * __this, Il2CppObject * ___first0, Il2CppObject ** ___second1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__firstToSecond_0();
		Il2CppObject * L_1 = ___first0;
		Il2CppObject ** L_2 = ___second1;
		NullCheck((Il2CppObject*)L_0);
		bool L_3 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject **)L_2);
		return L_3;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.Object,System.Object>::TryGetBySecond(TSecond,TFirst&)
extern "C"  bool BidirectionalDictionary_2_TryGetBySecond_m1230387241_gshared (BidirectionalDictionary_2_t1212012318 * __this, Il2CppObject * ___second0, Il2CppObject ** ___first1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__secondToFirst_1();
		Il2CppObject * L_1 = ___second0;
		Il2CppObject ** L_2 = ___first1;
		NullCheck((Il2CppObject*)L_0);
		bool L_3 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject **)L_2);
		return L_3;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionUtils/<TryGetSingleItem>c__AnonStorey25`1<System.Object>::.ctor()
extern "C"  void U3CTryGetSingleItemU3Ec__AnonStorey25_1__ctor_m1640756827_gshared (U3CTryGetSingleItemU3Ec__AnonStorey25_1_t1867941046 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Newtonsoft.Json.Utilities.CollectionUtils/<TryGetSingleItem>c__AnonStorey25`1<System.Object>::<>m__E7()
extern "C"  Il2CppObject * U3CTryGetSingleItemU3Ec__AnonStorey25_1_U3CU3Em__E7_m897406577_gshared (U3CTryGetSingleItemU3Ec__AnonStorey25_1_t1867941046 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		bool L_1 = (bool)__this->get_returnDefaultIfEmpty_1();
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::.ctor(System.Collections.IList)
extern Il2CppCodeGenString* _stringLiteral1178974130;
extern const uint32_t CollectionWrapper_1__ctor_m730688311_MetadataUsageId;
extern "C"  void CollectionWrapper_1__ctor_m730688311_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1__ctor_m730688311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___list0;
		ValidationUtils_ArgumentNotNull_m3243187033(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral1178974130, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___list0;
		if (!((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
		{
			goto IL_002d;
		}
	}
	{
		Il2CppObject * L_2 = ___list0;
		__this->set__genericCollection_1(((Il2CppObject*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
		goto IL_0034;
	}

IL_002d:
	{
		Il2CppObject * L_3 = ___list0;
		__this->set__list_0(L_3);
	}

IL_0034:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::.ctor(System.Collections.Generic.ICollection`1<T>)
extern Il2CppCodeGenString* _stringLiteral1178974130;
extern const uint32_t CollectionWrapper_1__ctor_m1250166947_MetadataUsageId;
extern "C"  void CollectionWrapper_1__ctor_m1250166947_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1__ctor_m1250166947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___list0;
		ValidationUtils_ArgumentNotNull_m3243187033(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral1178974130, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___list0;
		__this->set__genericCollection_1(L_1);
		return;
	}
}
// System.Collections.IEnumerator Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_System_Collections_IEnumerable_GetEnumerator_m1635240117_MetadataUsageId;
extern "C"  Il2CppObject * CollectionWrapper_1_System_Collections_IEnumerable_GetEnumerator_m1635240117_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IEnumerable_GetEnumerator_m1635240117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		NullCheck((Il2CppObject *)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		G_B3_0 = L_3;
		goto IL_0028;
	}

IL_001d:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__list_0();
		NullCheck((Il2CppObject *)L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
	}

IL_0028:
	{
		V_0 = (Il2CppObject *)G_B3_0;
		Il2CppObject * L_6 = V_0;
		return L_6;
	}
}
// System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t CollectionWrapper_1_System_Collections_IList_Add_m3251969948_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_1 = ___value0;
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(29 /* System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Add(T) */, (CollectionWrapper_1_t840219110 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_Count() */, (CollectionWrapper_1_t840219110 *)__this);
		return ((int32_t)((int32_t)L_2-(int32_t)1));
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C"  bool CollectionWrapper_1_System_Collections_IList_Contains_m4137645066_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_2 = ___value0;
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		bool L_3 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(31 /* System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Contains(T) */, (CollectionWrapper_1_t840219110 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		return L_3;
	}

IL_0018:
	{
		return (bool)0;
	}
}
// System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4120803194;
extern const uint32_t CollectionWrapper_1_System_Collections_IList_IndexOf_m1783924686_MetadataUsageId;
extern "C"  int32_t CollectionWrapper_1_System_Collections_IList_IndexOf_m1783924686_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IList_IndexOf_m1783924686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Exception_t1927440687 * L_1 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_1, (String_t*)_stringLiteral4120803194, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___value0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__list_0();
		Il2CppObject * L_5 = ___value0;
		NullCheck((Il2CppObject *)L_4);
		int32_t L_6 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(7 /* System.Int32 System.Collections.IList::IndexOf(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		return L_6;
	}

IL_0038:
	{
		return (-1);
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.RemoveAt(System.Int32)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1890103716;
extern const uint32_t CollectionWrapper_1_System_Collections_IList_RemoveAt_m828926619_MetadataUsageId;
extern "C"  void CollectionWrapper_1_System_Collections_IList_RemoveAt_m828926619_gshared (CollectionWrapper_1_t840219110 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IList_RemoveAt_m828926619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Exception_t1927440687 * L_1 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_1, (String_t*)_stringLiteral1890103716, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__list_0();
		int32_t L_3 = ___index0;
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker1< int32_t >::Invoke(10 /* System.Void System.Collections.IList::RemoveAt(System.Int32) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (int32_t)L_3);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral209024824;
extern const uint32_t CollectionWrapper_1_System_Collections_IList_Insert_m3242781493_MetadataUsageId;
extern "C"  void CollectionWrapper_1_System_Collections_IList_Insert_m3242781493_gshared (CollectionWrapper_1_t840219110 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IList_Insert_m3242781493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Exception_t1927440687 * L_1 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_1, (String_t*)_stringLiteral209024824, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___value1;
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		int32_t L_4 = ___index0;
		Il2CppObject * L_5 = ___value1;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(8 /* System.Void System.Collections.IList::Insert(System.Int32,System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (int32_t)L_4, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_System_Collections_IList_get_IsFixedSize_m4116392417_MetadataUsageId;
extern "C"  bool CollectionWrapper_1_System_Collections_IList_get_IsFixedSize_m4116392417_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IList_get_IsFixedSize_m4116392417_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		NullCheck((Il2CppObject*)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0017:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		NullCheck((Il2CppObject *)L_3);
		bool L_4 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IList::get_IsFixedSize() */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		return L_4;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C"  void CollectionWrapper_1_System_Collections_IList_Remove_m2883738989_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_2 = ___value0;
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(35 /* System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Remove(T) */, (CollectionWrapper_1_t840219110 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
	}

IL_0018:
	{
		return;
	}
}
// System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1815973004;
extern const uint32_t CollectionWrapper_1_System_Collections_IList_get_Item_m632945545_MetadataUsageId;
extern "C"  Il2CppObject * CollectionWrapper_1_System_Collections_IList_get_Item_m632945545_gshared (CollectionWrapper_1_t840219110 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IList_get_Item_m632945545_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Exception_t1927440687 * L_1 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_1, (String_t*)_stringLiteral1815973004, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__list_0();
		int32_t L_3 = ___index0;
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (int32_t)L_3);
		return L_4;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1815973004;
extern const uint32_t CollectionWrapper_1_System_Collections_IList_set_Item_m2724715840_MetadataUsageId;
extern "C"  void CollectionWrapper_1_System_Collections_IList_set_Item_m2724715840_gshared (CollectionWrapper_1_t840219110 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IList_set_Item_m2724715840_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Exception_t1927440687 * L_1 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_1, (String_t*)_stringLiteral1815973004, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___value1;
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		int32_t L_4 = ___index0;
		Il2CppObject * L_5 = ___value1;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(3 /* System.Void System.Collections.IList::set_Item(System.Int32,System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (int32_t)L_4, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void CollectionWrapper_1_System_Collections_ICollection_CopyTo_m1279341670_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		int32_t L_1 = ___arrayIndex1;
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		VirtActionInvoker2< ObjectU5BU5D_t3614634134*, int32_t >::Invoke(32 /* System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::CopyTo(T[],System.Int32) */, (CollectionWrapper_1_t840219110 *)__this, (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9))), (int32_t)L_1);
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool CollectionWrapper_1_System_Collections_ICollection_get_IsSynchronized_m3682572004_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_System_Collections_ICollection_get_SyncRoot_m2090561030_MetadataUsageId;
extern "C"  Il2CppObject * CollectionWrapper_1_System_Collections_ICollection_get_SyncRoot_m2090561030_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_ICollection_get_SyncRoot_m2090561030_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__syncRoot_2();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of__syncRoot_2();
		Il2CppObject * L_2 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_2, /*hidden argument*/NULL);
		Interlocked_CompareExchange_m2483599240(NULL /*static, unused*/, (Il2CppObject **)L_1, (Il2CppObject *)L_2, (Il2CppObject *)NULL, /*hidden argument*/NULL);
	}

IL_001d:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__syncRoot_2();
		return L_3;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Add(T)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_Add_m3694381833_MetadataUsageId;
extern "C"  void CollectionWrapper_1_Add_m3694381833_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_Add_m3694381833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		Il2CppObject * L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		goto IL_002e;
	}

IL_001c:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		Il2CppObject * L_4 = ___item0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_4);
	}

IL_002e:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Clear()
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_Clear_m1906909381_MetadataUsageId;
extern "C"  void CollectionWrapper_1_Clear_m1906909381_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_Clear_m1906909381_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1);
		goto IL_0026;
	}

IL_001b:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__list_0();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(5 /* System.Void System.Collections.IList::Clear() */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
	}

IL_0026:
	{
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Contains(T)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_Contains_m286598843_MetadataUsageId;
extern "C"  bool CollectionWrapper_1_Contains_m286598843_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_Contains_m286598843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		Il2CppObject * L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__list_0();
		Il2CppObject * L_5 = ___item0;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IList::Contains(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		return L_6;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::CopyTo(T[],System.Int32)
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_CopyTo_m3640586785_MetadataUsageId;
extern "C"  void CollectionWrapper_1_CopyTo_m3640586785_gshared (CollectionWrapper_1_t840219110 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_CopyTo_m3640586785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		ObjectU5BU5D_t3614634134* L_2 = ___array0;
		int32_t L_3 = ___arrayIndex1;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker2< ObjectU5BU5D_t3614634134*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3);
		goto IL_002a;
	}

IL_001d:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__list_0();
		ObjectU5BU5D_t3614634134* L_5 = ___array0;
		int32_t L_6 = ___arrayIndex1;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6);
	}

IL_002a:
	{
		return;
	}
}
// System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_Count()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_get_Count_m2582336668_MetadataUsageId;
extern "C"  int32_t CollectionWrapper_1_get_Count_m2582336668_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_get_Count_m2582336668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		NullCheck((Il2CppObject*)L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0017:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		NullCheck((Il2CppObject *)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		return L_4;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_IsReadOnly()
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_get_IsReadOnly_m1401533917_MetadataUsageId;
extern "C"  bool CollectionWrapper_1_get_IsReadOnly_m1401533917_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_get_IsReadOnly_m1401533917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		NullCheck((Il2CppObject*)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0017:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		NullCheck((Il2CppObject *)L_3);
		bool L_4 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IList::get_IsReadOnly() */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		return L_4;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Remove(T)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_Remove_m673209964_MetadataUsageId;
extern "C"  bool CollectionWrapper_1_Remove_m673209964_gshared (CollectionWrapper_1_t840219110 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_Remove_m673209964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		Il2CppObject * L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__list_0();
		Il2CppObject * L_5 = ___item0;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IList::Contains(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		V_0 = (bool)L_6;
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_8 = (Il2CppObject *)__this->get__list_0();
		Il2CppObject * L_9 = ___item0;
		NullCheck((Il2CppObject *)L_8);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void System.Collections.IList::Remove(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9);
	}

IL_0041:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* CollectionWrapper_1_GetEnumerator_m1507375274_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0017:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		Il2CppObject* L_4 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject* L_5 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_4);
		return L_5;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::IsGenericCollection()
extern "C"  bool CollectionWrapper_1_IsGenericCollection_m3709139545_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		return (bool)((((int32_t)((((Il2CppObject*)(Il2CppObject*)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::VerifyValueType(System.Object)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2463332787;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t CollectionWrapper_1_VerifyValueType_m524701668_MetadataUsageId;
extern "C"  void CollectionWrapper_1_VerifyValueType_m524701668_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_VerifyValueType_m524701668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if (L_1)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_2 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		Il2CppObject * L_4 = ___value0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)L_3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)), /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		String_t* L_7 = StringUtils_FormatWith_m2705333716(NULL /*static, unused*/, (String_t*)_stringLiteral2463332787, (Il2CppObject *)L_2, (ObjectU5BU5D_t3614634134*)L_5, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_8, (String_t*)L_7, (String_t*)_stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003c:
	{
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::IsCompatibleObject(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_IsCompatibleObject_m3337514079_MetadataUsageId;
extern "C"  bool CollectionWrapper_1_IsCompatibleObject_m3337514079_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_IsCompatibleObject_m3337514079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		if (((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))
		{
			goto IL_003b;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		if (L_1)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		bool L_3 = Type_get_IsValueType_m1733572463((Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)), /*hidden argument*/NULL);
		bool L_5 = ReflectionUtils_IsNullableType_m3540253748(NULL /*static, unused*/, (Type_t *)L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003b;
		}
	}

IL_0039:
	{
		return (bool)0;
	}

IL_003b:
	{
		return (bool)1;
	}
}
// System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_UnderlyingCollection()
extern "C"  Il2CppObject * CollectionWrapper_1_get_UnderlyingCollection_m2218800867_gshared (CollectionWrapper_1_t840219110 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		return L_1;
	}

IL_0012:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__list_0();
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey2A`1<System.Object>::.ctor()
extern "C"  void U3CTryConvertU3Ec__AnonStorey2A_1__ctor_m148562382_gshared (U3CTryConvertU3Ec__AnonStorey2A_1_t142049201 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey2A`1<System.Object>::<>m__EC()
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ConvertUtils_t2984810590_il2cpp_TypeInfo_var;
extern const uint32_t U3CTryConvertU3Ec__AnonStorey2A_1_U3CU3Em__EC_m193775364_MetadataUsageId;
extern "C"  Il2CppObject * U3CTryConvertU3Ec__AnonStorey2A_1_U3CU3Em__EC_m193775364_gshared (U3CTryConvertU3Ec__AnonStorey2A_1_t142049201 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTryConvertU3Ec__AnonStorey2A_1_U3CU3Em__EC_m193775364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_initialValue_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_1 = CultureInfo_get_CurrentCulture_m711066087(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ConvertUtils_t2984810590_il2cpp_TypeInfo_var);
		ConvertUtils_TryConvert_m3341520891(NULL /*static, unused*/, (Il2CppObject *)L_0, (CultureInfo_t3500843524 *)L_1, (Type_t *)L_2, (Il2CppObject **)(&V_0), /*hidden argument*/NULL);
		Il2CppObject * L_3 = V_0;
		return ((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)));
	}
}
// System.Void Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey2C`1<System.Object>::.ctor()
extern "C"  void U3CTryConvertOrCastU3Ec__AnonStorey2C_1__ctor_m3772060474_gshared (U3CTryConvertOrCastU3Ec__AnonStorey2C_1_t283520851 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey2C`1<System.Object>::<>m__EE()
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ConvertUtils_t2984810590_il2cpp_TypeInfo_var;
extern const uint32_t U3CTryConvertOrCastU3Ec__AnonStorey2C_1_U3CU3Em__EE_m208200466_MetadataUsageId;
extern "C"  Il2CppObject * U3CTryConvertOrCastU3Ec__AnonStorey2C_1_U3CU3Em__EE_m208200466_gshared (U3CTryConvertOrCastU3Ec__AnonStorey2C_1_t283520851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTryConvertOrCastU3Ec__AnonStorey2C_1_U3CU3Em__EE_m208200466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_initialValue_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_1 = CultureInfo_get_CurrentCulture_m711066087(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ConvertUtils_t2984810590_il2cpp_TypeInfo_var);
		ConvertUtils_TryConvertOrCast_m3940889671(NULL /*static, unused*/, (Il2CppObject *)L_0, (CultureInfo_t3500843524 *)L_1, (Type_t *)L_2, (Il2CppObject **)(&V_0), /*hidden argument*/NULL);
		Il2CppObject * L_3 = V_0;
		return ((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)));
	}
}
// System.Void Newtonsoft.Json.Utilities.Creator`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Creator_1__ctor_m3589561469_gshared (Creator_1_t310204047 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T Newtonsoft.Json.Utilities.Creator`1<System.Object>::Invoke()
extern "C"  Il2CppObject * Creator_1_Invoke_m1400968972_gshared (Creator_1_t310204047 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Creator_1_Invoke_m1400968972((Creator_1_t310204047 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Newtonsoft.Json.Utilities.Creator`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Creator_1_BeginInvoke_m1842762196_gshared (Creator_1_t310204047 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T Newtonsoft.Json.Utilities.Creator`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Creator_1_EndInvoke_m3159317452_gshared (Creator_1_t310204047 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TEnumeratorKey,TEnumeratorValue>>)
extern Il2CppCodeGenString* _stringLiteral372029369;
extern const uint32_t DictionaryEnumerator_2__ctor_m2268843174_MetadataUsageId;
extern "C"  void DictionaryEnumerator_2__ctor_m2268843174_gshared (DictionaryEnumerator_2_t2758714162 * __this, Il2CppObject* ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2__ctor_m2268843174_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___e0;
		ValidationUtils_ArgumentNotNull_m3243187033(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral372029369, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___e0;
		__this->set__e_0(L_1);
		return;
	}
}
extern "C"  void DictionaryEnumerator_2__ctor_m2268843174_AdjustorThunk (Il2CppObject * __this, Il2CppObject* ___e0, const MethodInfo* method)
{
	DictionaryEnumerator_2_t2758714162 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t2758714162 *>(__this + 1);
	DictionaryEnumerator_2__ctor_m2268843174(_thisAdjusted, ___e0, method);
}
// System.Collections.DictionaryEntry Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Entry()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryEnumerator_2_get_Entry_m4176687428_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  DictionaryEnumerator_2_get_Entry_m4176687428_gshared (DictionaryEnumerator_2_t2758714162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2_get_Entry_m4176687428_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = DictionaryEnumerator_2_get_Current_m222679567((DictionaryEnumerator_2_t2758714162 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return ((*(DictionaryEntry_t3048875398 *)((DictionaryEntry_t3048875398 *)UnBox (L_0, DictionaryEntry_t3048875398_il2cpp_TypeInfo_var))));
	}
}
extern "C"  DictionaryEntry_t3048875398  DictionaryEnumerator_2_get_Entry_m4176687428_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t2758714162 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t2758714162 *>(__this + 1);
	return DictionaryEnumerator_2_get_Entry_m4176687428(_thisAdjusted, method);
}
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Key_m155013433_gshared (DictionaryEnumerator_2_t2758714162 * __this, const MethodInfo* method)
{
	DictionaryEntry_t3048875398  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DictionaryEntry_t3048875398  L_0 = DictionaryEnumerator_2_get_Entry_m4176687428((DictionaryEnumerator_2_t2758714162 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (DictionaryEntry_t3048875398 )L_0;
		Il2CppObject * L_1 = DictionaryEntry_get_Key_m3623293571((DictionaryEntry_t3048875398 *)(&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Key_m155013433_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t2758714162 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t2758714162 *>(__this + 1);
	return DictionaryEnumerator_2_get_Key_m155013433(_thisAdjusted, method);
}
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Value_m262458185_gshared (DictionaryEnumerator_2_t2758714162 * __this, const MethodInfo* method)
{
	DictionaryEntry_t3048875398  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DictionaryEntry_t3048875398  L_0 = DictionaryEnumerator_2_get_Entry_m4176687428((DictionaryEnumerator_2_t2758714162 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (DictionaryEntry_t3048875398 )L_0;
		Il2CppObject * L_1 = DictionaryEntry_get_Value_m2812883243((DictionaryEntry_t3048875398 *)(&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Value_m262458185_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t2758714162 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t2758714162 *>(__this + 1);
	return DictionaryEnumerator_2_get_Value_m262458185(_thisAdjusted, method);
}
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryEnumerator_2_get_Current_m222679567_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Current_m222679567_gshared (DictionaryEnumerator_2_t2758714162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2_get_Current_m222679567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t38854645  V_0;
	memset(&V_0, 0, sizeof(V_0));
	KeyValuePair_2_t38854645  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__e_0();
		NullCheck((Il2CppObject*)L_0);
		KeyValuePair_2_t38854645  L_1 = InterfaceFuncInvoker0< KeyValuePair_2_t38854645  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (KeyValuePair_2_t38854645 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Il2CppObject* L_3 = (Il2CppObject*)__this->get__e_0();
		NullCheck((Il2CppObject*)L_3);
		KeyValuePair_2_t38854645  L_4 = InterfaceFuncInvoker0< KeyValuePair_2_t38854645  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3);
		V_1 = (KeyValuePair_2_t38854645 )L_4;
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_2, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		DictionaryEntry_t3048875398  L_7 = L_6;
		Il2CppObject * L_8 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_7);
		return L_8;
	}
}
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Current_m222679567_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t2758714162 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t2758714162 *>(__this + 1);
	return DictionaryEnumerator_2_get_Current_m222679567(_thisAdjusted, method);
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryEnumerator_2_MoveNext_m2168681928_MetadataUsageId;
extern "C"  bool DictionaryEnumerator_2_MoveNext_m2168681928_gshared (DictionaryEnumerator_2_t2758714162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2_MoveNext_m2168681928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__e_0();
		NullCheck((Il2CppObject *)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
extern "C"  bool DictionaryEnumerator_2_MoveNext_m2168681928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t2758714162 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t2758714162 *>(__this + 1);
	return DictionaryEnumerator_2_MoveNext_m2168681928(_thisAdjusted, method);
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::Reset()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryEnumerator_2_Reset_m3518201151_MetadataUsageId;
extern "C"  void DictionaryEnumerator_2_Reset_m3518201151_gshared (DictionaryEnumerator_2_t2758714162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2_Reset_m3518201151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__e_0();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
extern "C"  void DictionaryEnumerator_2_Reset_m3518201151_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t2758714162 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t2758714162 *>(__this + 1);
	DictionaryEnumerator_2_Reset_m3518201151(_thisAdjusted, method);
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::.ctor(System.Collections.IDictionary)
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t DictionaryWrapper_2__ctor_m691830228_MetadataUsageId;
extern "C"  void DictionaryWrapper_2__ctor_m691830228_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2__ctor_m691830228_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___dictionary0;
		ValidationUtils_ArgumentNotNull_m3243187033(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___dictionary0;
		__this->set__dictionary_0(L_1);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t DictionaryWrapper_2__ctor_m1452490861_MetadataUsageId;
extern "C"  void DictionaryWrapper_2__ctor_m1452490861_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2__ctor_m1452490861_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___dictionary0;
		ValidationUtils_ArgumentNotNull_m3243187033(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___dictionary0;
		__this->set__genericDictionary_1(L_1);
		return;
	}
}
// System.Collections.IEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IEnumerable_GetEnumerator_m2017731448_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	{
		NullCheck((DictionaryWrapper_2_t534389262 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((DictionaryWrapper_2_t534389262 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_Add_m2846887253_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_System_Collections_IDictionary_Add_m2846887253_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_Add_m2846887253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_2 = ___key0;
		Il2CppObject * L_3 = ___value1;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Add(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))));
		goto IL_0034;
	}

IL_0027:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_5 = ___key0;
		Il2CppObject * L_6 = ___value1;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(6 /* System.Void System.Collections.IDictionary::Add(System.Object,System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6);
	}

IL_0034:
	{
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_Contains_m2172301529_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_System_Collections_IDictionary_Contains_m2172301529_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_Contains_m2172301529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject*)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::ContainsKey(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return L_3;
	}

IL_001d:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_5 = ___key0;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		return L_6;
	}
}
// System.Collections.IDictionaryEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1046604918_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1046604918_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1046604918_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_1);
		DictionaryEnumerator_2_t2758714162  L_3;
		memset(&L_3, 0, sizeof(L_3));
		DictionaryEnumerator_2__ctor_m2268843174(&L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		DictionaryEnumerator_2_t2758714162  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_4);
		return (Il2CppObject *)L_5;
	}

IL_0021:
	{
		Il2CppObject * L_6 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_6);
		Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(9 /* System.Collections.IDictionaryEnumerator System.Collections.IDictionary::GetEnumerator() */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
		return L_7;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_get_IsFixedSize_m3994873554_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_System_Collections_IDictionary_get_IsFixedSize_m3994873554_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_get_IsFixedSize_m3994873554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IDictionary::get_IsFixedSize() */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Collections.ICollection Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_Keys()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3567681645_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3567681645_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3567681645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Keys() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1);
		List_1_t2058570427 * L_3 = ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return L_3;
	}

IL_001c:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		return L_5;
	}
}
// System.Collections.ICollection Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_Values()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_get_Values_m3915142077_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_get_Values_m3915142077_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_get_Values_m3915142077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(7 /* System.Collections.Generic.ICollection`1<!1> System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Values() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1);
		List_1_t2058570427 * L_3 = ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_3;
	}

IL_001c:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(5 /* System.Collections.ICollection System.Collections.IDictionary::get_Values() */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		return L_5;
	}
}
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m960486311_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m960486311_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m960486311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Item(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return L_3;
	}

IL_0022:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_5 = ___key0;
		NullCheck((Il2CppObject *)L_4);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		return L_6;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m3956535670_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m3956535670_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m3956535670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_2 = ___key0;
		Il2CppObject * L_3 = ___value1;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::set_Item(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))));
		goto IL_0034;
	}

IL_0027:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_5 = ___key0;
		Il2CppObject * L_6 = ___value1;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6);
	}

IL_0034:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m2464244227_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m2464244227_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m2464244227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppArray * L_2 = ___array0;
		int32_t L_3 = ___index1;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t2854920344*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (Il2CppObject*)L_1, (KeyValuePair_2U5BU5D_t2854920344*)((KeyValuePair_2U5BU5D_t2854920344*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9))), (int32_t)L_3);
		goto IL_002f;
	}

IL_0022:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppArray *)L_5, (int32_t)L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m3832261271_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m3832261271_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m3832261271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.ICollection::get_IsSynchronized() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2251724019_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2251724019_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2251724019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__syncRoot_2();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of__syncRoot_2();
		Il2CppObject * L_2 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_2, /*hidden argument*/NULL);
		Interlocked_CompareExchange_m2483599240(NULL /*static, unused*/, (Il2CppObject **)L_1, (Il2CppObject *)L_2, (Il2CppObject *)NULL, /*hidden argument*/NULL);
	}

IL_001d:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__syncRoot_2();
		return L_3;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Add(TKey,TValue)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Add_m757996496_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_Add_m757996496_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Add_m757996496_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_2 = ___key0;
		Il2CppObject * L_3 = ___value1;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Add(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3);
		goto IL_0034;
	}

IL_001d:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_5 = ___key0;
		Il2CppObject * L_6 = ___value1;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(6 /* System.Void System.Collections.IDictionary::Add(System.Object,System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6);
	}

IL_0034:
	{
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::ContainsKey(TKey)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_ContainsKey_m1831012438_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_ContainsKey_m1831012438_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_ContainsKey_m1831012438_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject*)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::ContainsKey(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_5 = ___key0;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		return L_6;
	}
}
// System.Collections.Generic.ICollection`1<TKey> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Keys()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_get_Keys_m2092482101_MetadataUsageId;
extern "C"  Il2CppObject* DictionaryWrapper_2_get_Keys_m2092482101_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_get_Keys_m2092482101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Keys() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0017:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		Il2CppObject* L_5 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		List_1_t2058570427 * L_6 = ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return L_6;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(TKey)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Remove_m3198912934_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_Remove_m3198912934_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Remove_m3198912934_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject*)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_5 = ___key0;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_8 = ___key0;
		NullCheck((Il2CppObject *)L_7);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(10 /* System.Void System.Collections.IDictionary::Remove(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Il2CppObject *)L_8);
		return (bool)1;
	}

IL_0041:
	{
		return (bool)0;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_TryGetValue_m3700931523_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_TryGetValue_m3700931523_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_TryGetValue_m3700931523_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_2 = ___key0;
		Il2CppObject ** L_3 = ___value1;
		NullCheck((Il2CppObject*)L_1);
		bool L_4 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)L_2, (Il2CppObject **)L_3);
		return L_4;
	}

IL_0019:
	{
		Il2CppObject * L_5 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_6 = ___key0;
		NullCheck((Il2CppObject *)L_5);
		bool L_7 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_5, (Il2CppObject *)L_6);
		if (L_7)
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject ** L_8 = ___value1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_9 = V_0;
		(*(Il2CppObject **)L_8) = L_9;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_8, L_9);
		return (bool)0;
	}

IL_0040:
	{
		Il2CppObject ** L_10 = ___value1;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_12 = ___key0;
		NullCheck((Il2CppObject *)L_11);
		Il2CppObject * L_13 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_11, (Il2CppObject *)L_12);
		(*(Il2CppObject **)L_10) = ((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_10, ((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))));
		return (bool)1;
	}
}
// System.Collections.Generic.ICollection`1<TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Values()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_get_Values_m1831572781_MetadataUsageId;
extern "C"  Il2CppObject* DictionaryWrapper_2_get_Values_m1831572781_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_get_Values_m1831572781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(7 /* System.Collections.Generic.ICollection`1<!1> System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Values() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0017:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(5 /* System.Collections.ICollection System.Collections.IDictionary::get_Values() */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		Il2CppObject* L_5 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		List_1_t2058570427 * L_6 = ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_6;
	}
}
// TValue Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Item(TKey)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_get_Item_m2362944062_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryWrapper_2_get_Item_m2362944062_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_get_Item_m2362944062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Item(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_5 = ___key0;
		NullCheck((Il2CppObject *)L_4);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		return ((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_set_Item_m2531867175_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_set_Item_m2531867175_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_set_Item_m2531867175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_2 = ___key0;
		Il2CppObject * L_3 = ___value1;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::set_Item(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3);
		goto IL_0034;
	}

IL_001d:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_5 = ___key0;
		Il2CppObject * L_6 = ___value1;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6);
	}

IL_0034:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Add_m3612891991_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_Add_m3612891991_gshared (DictionaryWrapper_2_t534389262 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Add_m3612891991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		KeyValuePair_2_t38854645  L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< KeyValuePair_2_t38854645  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (Il2CppObject*)L_1, (KeyValuePair_2_t38854645 )L_2);
		goto IL_0033;
	}

IL_001c:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__dictionary_0();
		KeyValuePair_2_t38854645  L_4 = ___item0;
		KeyValuePair_2_t38854645  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13), &L_5);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_3, IList_t3321498491_il2cpp_TypeInfo_var)));
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IList_t3321498491_il2cpp_TypeInfo_var)), (Il2CppObject *)L_6);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Clear()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Clear_m3928068896_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_Clear_m3928068896_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Clear_m3928068896_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (Il2CppObject*)L_1);
		goto IL_0026;
	}

IL_001b:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(7 /* System.Void System.Collections.IDictionary::Clear() */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
	}

IL_0026:
	{
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Contains_m3569786257_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_Contains_m3569786257_gshared (DictionaryWrapper_2_t534389262 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Contains_m3569786257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		KeyValuePair_2_t38854645  L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t38854645  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (Il2CppObject*)L_1, (KeyValuePair_2_t38854645 )L_2);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		KeyValuePair_2_t38854645  L_5 = ___item0;
		KeyValuePair_2_t38854645  L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13), &L_6);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_4, IList_t3321498491_il2cpp_TypeInfo_var)));
		bool L_8 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IList::Contains(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_4, IList_t3321498491_il2cpp_TypeInfo_var)), (Il2CppObject *)L_7);
		return L_8;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_CopyTo_m2039386035_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_CopyTo_m2039386035_gshared (DictionaryWrapper_2_t534389262 * __this, KeyValuePair_2U5BU5D_t2854920344* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_CopyTo_m2039386035_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DictionaryEntry_t3048875398  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		KeyValuePair_2U5BU5D_t2854920344* L_2 = ___array0;
		int32_t L_3 = ___arrayIndex1;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t2854920344*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (Il2CppObject*)L_1, (KeyValuePair_2U5BU5D_t2854920344*)L_2, (int32_t)L_3);
		goto IL_008a;
	}

IL_001d:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(9 /* System.Collections.IDictionaryEnumerator System.Collections.IDictionary::GetEnumerator() */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		V_1 = (Il2CppObject *)L_5;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0068;
		}

IL_002e:
		{
			Il2CppObject * L_6 = V_1;
			NullCheck((Il2CppObject *)L_6);
			Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			V_0 = (DictionaryEntry_t3048875398 )((*(DictionaryEntry_t3048875398 *)((DictionaryEntry_t3048875398 *)UnBox (L_7, DictionaryEntry_t3048875398_il2cpp_TypeInfo_var))));
			KeyValuePair_2U5BU5D_t2854920344* L_8 = ___array0;
			int32_t L_9 = ___arrayIndex1;
			int32_t L_10 = (int32_t)L_9;
			___arrayIndex1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_10);
			Il2CppObject * L_11 = DictionaryEntry_get_Key_m3623293571((DictionaryEntry_t3048875398 *)(&V_0), /*hidden argument*/NULL);
			Il2CppObject * L_12 = DictionaryEntry_get_Value_m2812883243((DictionaryEntry_t3048875398 *)(&V_0), /*hidden argument*/NULL);
			KeyValuePair_2_t38854645  L_13;
			memset(&L_13, 0, sizeof(L_13));
			KeyValuePair_2__ctor_m1640124561(&L_13, (Il2CppObject *)((Il2CppObject *)Castclass(L_11, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (Il2CppObject *)((Il2CppObject *)Castclass(L_12, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			(*(KeyValuePair_2_t38854645 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)))) = L_13;
		}

IL_0068:
		{
			Il2CppObject * L_14 = V_1;
			NullCheck((Il2CppObject *)L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			if (L_15)
			{
				goto IL_002e;
			}
		}

IL_0073:
		{
			IL2CPP_LEAVE(0x8A, FINALLY_0078);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0078;
	}

FINALLY_0078:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_16 = V_1;
			V_2 = (Il2CppObject *)((Il2CppObject *)IsInst(L_16, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_17 = V_2;
			if (L_17)
			{
				goto IL_0083;
			}
		}

IL_0082:
		{
			IL2CPP_END_FINALLY(120)
		}

IL_0083:
		{
			Il2CppObject * L_18 = V_2;
			NullCheck((Il2CppObject *)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			IL2CPP_END_FINALLY(120)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(120)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008a:
	{
		return;
	}
}
// System.Int32 Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Count()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_get_Count_m2623408759_MetadataUsageId;
extern "C"  int32_t DictionaryWrapper_2_get_Count_m2623408759_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_get_Count_m2623408759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0017:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		return L_4;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_IsReadOnly()
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_get_IsReadOnly_m1745434428_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_get_IsReadOnly_m1745434428_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_get_IsReadOnly_m1745434428_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0017:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_3);
		bool L_4 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IDictionary::get_IsReadOnly() */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		return L_4;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Remove_m2930571166_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_Remove_m2930571166_gshared (DictionaryWrapper_2_t534389262 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Remove_m2930571166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		KeyValuePair_2_t38854645  L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t38854645  >::Invoke(6 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10), (Il2CppObject*)L_1, (KeyValuePair_2_t38854645 )L_2);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_5 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)(&___item0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_007e;
		}
	}
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_8 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)(&___item0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		NullCheck((Il2CppObject *)L_7);
		Il2CppObject * L_9 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Il2CppObject *)L_8);
		V_0 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_0;
		Il2CppObject * L_11 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)(&___item0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		bool L_12 = Object_Equals_m969736273(NULL /*static, unused*/, (Il2CppObject *)L_10, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007c;
		}
	}
	{
		Il2CppObject * L_13 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_14 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)(&___item0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		NullCheck((Il2CppObject *)L_13);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(10 /* System.Void System.Collections.IDictionary::Remove(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_13, (Il2CppObject *)L_14);
		return (bool)1;
	}

IL_007c:
	{
		return (bool)0;
	}

IL_007e:
	{
		return (bool)1;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::GetEnumerator()
extern const MethodInfo* Enumerable_Cast_TisDictionaryEntry_t3048875398_m3594045651_MethodInfo_var;
extern const uint32_t DictionaryWrapper_2_GetEnumerator_m2069280100_MetadataUsageId;
extern "C"  Il2CppObject* DictionaryWrapper_2_GetEnumerator_m2069280100_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_GetEnumerator_m2069280100_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* G_B4_0 = NULL;
	Il2CppObject* G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0017:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject* L_4 = Enumerable_Cast_TisDictionaryEntry_t3048875398_m3594045651(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/Enumerable_Cast_TisDictionaryEntry_t3048875398_m3594045651_MethodInfo_var);
		Func_2_t3251821928 * L_5 = ((DictionaryWrapper_2_t534389262_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		G_B3_0 = L_4;
		if (L_5)
		{
			G_B4_0 = L_4;
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		Func_2_t3251821928 * L_7 = (Func_2_t3251821928 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 19));
		((  void (*) (Func_2_t3251821928 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		((DictionaryWrapper_2_t534389262_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17)->static_fields)->set_U3CU3Ef__amU24cache3_3(L_7);
		G_B4_0 = G_B3_0;
	}

IL_003a:
	{
		Func_2_t3251821928 * L_8 = ((DictionaryWrapper_2_t534389262_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		Il2CppObject* L_9 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3251821928 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)G_B4_0, (Func_2_t3251821928 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject* L_10 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_9);
		return L_10;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(System.Object)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Remove_m743786935_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_Remove_m743786935_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Remove_m743786935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		goto IL_002e;
	}

IL_0022:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_4 = ___key0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(10 /* System.Void System.Collections.IDictionary::Remove(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_4);
	}

IL_002e:
	{
		return;
	}
}
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_UnderlyingDictionary()
extern "C"  Il2CppObject * DictionaryWrapper_2_get_UnderlyingDictionary_m4285210172_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		return L_1;
	}

IL_0012:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__dictionary_0();
		return L_2;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::<GetEnumerator>m__F0(System.Collections.DictionaryEntry)
extern "C"  KeyValuePair_2_t38854645  DictionaryWrapper_2_U3CGetEnumeratorU3Em__F0_m1182448152_gshared (Il2CppObject * __this /* static, unused */, DictionaryEntry_t3048875398  ___de0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = DictionaryEntry_get_Key_m3623293571((DictionaryEntry_t3048875398 *)(&___de0), /*hidden argument*/NULL);
		Il2CppObject * L_1 = DictionaryEntry_get_Value_m2812883243((DictionaryEntry_t3048875398 *)(&___de0), /*hidden argument*/NULL);
		KeyValuePair_2_t38854645  L_2;
		memset(&L_2, 0, sizeof(L_2));
		KeyValuePair_2__ctor_m1640124561(&L_2, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>::.ctor(System.String,T)
extern "C"  void EnumValue_1__ctor_m1800650159_gshared (EnumValue_1_t589082027 * __this, String_t* ___name0, int64_t ___value1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set__name_0(L_0);
		int64_t L_1 = ___value1;
		__this->set__value_1(L_1);
		return;
	}
}
// System.String Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>::get_Name()
extern "C"  String_t* EnumValue_1_get_Name_m2643172728_gshared (EnumValue_1_t589082027 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get__name_0();
		return L_0;
	}
}
// T Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>::get_Value()
extern "C"  int64_t EnumValue_1_get_Value_m2186583602_gshared (EnumValue_1_t589082027 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (int64_t)__this->get__value_1();
		return L_0;
	}
}
// System.Void Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>::.ctor(System.String,T)
extern "C"  void EnumValue_1__ctor_m752973208_gshared (EnumValue_1_t2589200904 * __this, String_t* ___name0, uint64_t ___value1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set__name_0(L_0);
		uint64_t L_1 = ___value1;
		__this->set__value_1(L_1);
		return;
	}
}
// System.String Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>::get_Name()
extern "C"  String_t* EnumValue_1_get_Name_m3282313175_gshared (EnumValue_1_t2589200904 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get__name_0();
		return L_0;
	}
}
// T Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>::get_Value()
extern "C"  uint64_t EnumValue_1_get_Value_m1167595673_gshared (EnumValue_1_t2589200904 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (uint64_t)__this->get__value_1();
		return L_0;
	}
}
// System.Void Newtonsoft.Json.Utilities.EnumValues`1<System.Int64>::.ctor()
extern "C"  void EnumValues_1__ctor_m4226035808_gshared (EnumValues_1_t1738794714 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyedCollection_2_t1516412003 *)__this);
		((  void (*) (KeyedCollection_2_t1516412003 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((KeyedCollection_2_t1516412003 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.String Newtonsoft.Json.Utilities.EnumValues`1<System.Int64>::GetKeyForItem(Newtonsoft.Json.Utilities.EnumValue`1<T>)
extern "C"  String_t* EnumValues_1_GetKeyForItem_m1915937120_gshared (EnumValues_1_t1738794714 * __this, EnumValue_1_t589082027 * ___item0, const MethodInfo* method)
{
	{
		EnumValue_1_t589082027 * L_0 = ___item0;
		NullCheck((EnumValue_1_t589082027 *)L_0);
		String_t* L_1 = ((  String_t* (*) (EnumValue_1_t589082027 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((EnumValue_1_t589082027 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Newtonsoft.Json.Utilities.EnumValues`1<System.UInt64>::.ctor()
extern "C"  void EnumValues_1__ctor_m789867523_gshared (EnumValues_1_t3738913591 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyedCollection_2_t3516530880 *)__this);
		((  void (*) (KeyedCollection_2_t3516530880 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((KeyedCollection_2_t3516530880 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.String Newtonsoft.Json.Utilities.EnumValues`1<System.UInt64>::GetKeyForItem(Newtonsoft.Json.Utilities.EnumValue`1<T>)
extern "C"  String_t* EnumValues_1_GetKeyForItem_m2045969929_gshared (EnumValues_1_t3738913591 * __this, EnumValue_1_t2589200904 * ___item0, const MethodInfo* method)
{
	{
		EnumValue_1_t2589200904 * L_0 = ___item0;
		NullCheck((EnumValue_1_t2589200904 *)L_0);
		String_t* L_1 = ((  String_t* (*) (EnumValue_1_t2589200904 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((EnumValue_1_t2589200904 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey30`1<System.Object>::.ctor()
extern "C"  void U3CCreateDefaultConstructorU3Ec__AnonStorey30_1__ctor_m2252876817_gshared (U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_t1934288580 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey30`1<System.Object>::<>m__F7()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_U3CU3Em__F7_m1262253726_MetadataUsageId;
extern "C"  Il2CppObject * U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_U3CU3Em__F7_m1262253726_gshared (U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_t1934288580 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_U3CU3Em__F7_m1262253726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = (Type_t *)__this->get_type_0();
		Il2CppObject * L_1 = ReflectionUtils_CreateInstance_m3027021787(NULL /*static, unused*/, (Type_t *)L_0, (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
	}
}
// T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateDefaultConstructor>c__AnonStorey30`1<System.Object>::<>m__F8()
extern "C"  Il2CppObject * U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_U3CU3Em__F8_m1840458491_gshared (U3CCreateDefaultConstructorU3Ec__AnonStorey30_1_t1934288580 * __this, const MethodInfo* method)
{
	{
		ConstructorInfo_t2851816542 * L_0 = (ConstructorInfo_t2851816542 *)__this->get_constructorInfo_1();
		NullCheck((ConstructorInfo_t2851816542 *)L_0);
		Il2CppObject * L_1 = ConstructorInfo_Invoke_m2144827141((ConstructorInfo_t2851816542 *)L_0, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey31`1<System.Object>::.ctor()
extern "C"  void U3CCreateGetU3Ec__AnonStorey31_1__ctor_m918673569_gshared (U3CCreateGetU3Ec__AnonStorey31_1_t881987152 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey31`1<System.Object>::<>m__F9(T)
extern "C"  Il2CppObject * U3CCreateGetU3Ec__AnonStorey31_1_U3CU3Em__F9_m4106919378_gshared (U3CCreateGetU3Ec__AnonStorey31_1_t881987152 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	{
		PropertyInfo_t * L_0 = (PropertyInfo_t *)__this->get_propertyInfo_0();
		Il2CppObject * L_1 = ___o0;
		NullCheck((PropertyInfo_t *)L_0);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(24 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, (PropertyInfo_t *)L_0, (Il2CppObject *)L_1, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey32`1<System.Object>::.ctor()
extern "C"  void U3CCreateGetU3Ec__AnonStorey32_1__ctor_m1450616450_gshared (U3CCreateGetU3Ec__AnonStorey32_1_t1719614423 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey32`1<System.Object>::<>m__FA(T)
extern "C"  Il2CppObject * U3CCreateGetU3Ec__AnonStorey32_1_U3CU3Em__FA_m2814438295_gshared (U3CCreateGetU3Ec__AnonStorey32_1_t1719614423 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	{
		FieldInfo_t * L_0 = (FieldInfo_t *)__this->get_fieldInfo_0();
		Il2CppObject * L_1 = ___o0;
		NullCheck((FieldInfo_t *)L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, (FieldInfo_t *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey2F`1<System.Object>::.ctor()
extern "C"  void U3CCreateMethodCallU3Ec__AnonStorey2F_1__ctor_m2292220810_gshared (U3CCreateMethodCallU3Ec__AnonStorey2F_1_t4131337349 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey2F`1<System.Object>::<>m__F5(T,System.Object[])
extern "C"  Il2CppObject * U3CCreateMethodCallU3Ec__AnonStorey2F_1_U3CU3Em__F5_m3417254765_gshared (U3CCreateMethodCallU3Ec__AnonStorey2F_1_t4131337349 * __this, Il2CppObject * ___o0, ObjectU5BU5D_t3614634134* ___a1, const MethodInfo* method)
{
	{
		ConstructorInfo_t2851816542 * L_0 = (ConstructorInfo_t2851816542 *)__this->get_c_0();
		ObjectU5BU5D_t3614634134* L_1 = ___a1;
		NullCheck((ConstructorInfo_t2851816542 *)L_0);
		Il2CppObject * L_2 = ConstructorInfo_Invoke_m2144827141((ConstructorInfo_t2851816542 *)L_0, (ObjectU5BU5D_t3614634134*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey2F`1<System.Object>::<>m__F6(T,System.Object[])
extern "C"  Il2CppObject * U3CCreateMethodCallU3Ec__AnonStorey2F_1_U3CU3Em__F6_m1046787422_gshared (U3CCreateMethodCallU3Ec__AnonStorey2F_1_t4131337349 * __this, Il2CppObject * ___o0, ObjectU5BU5D_t3614634134* ___a1, const MethodInfo* method)
{
	{
		MethodBase_t904190842 * L_0 = (MethodBase_t904190842 *)__this->get_method_1();
		Il2CppObject * L_1 = ___o0;
		ObjectU5BU5D_t3614634134* L_2 = ___a1;
		NullCheck((MethodBase_t904190842 *)L_0);
		Il2CppObject * L_3 = MethodBase_Invoke_m1075809207((MethodBase_t904190842 *)L_0, (Il2CppObject *)L_1, (ObjectU5BU5D_t3614634134*)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey33`1<System.Object>::.ctor()
extern "C"  void U3CCreateSetU3Ec__AnonStorey33_1__ctor_m634821359_gshared (U3CCreateSetU3Ec__AnonStorey33_1_t2802865970 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey33`1<System.Object>::<>m__FB(T,System.Object)
extern "C"  void U3CCreateSetU3Ec__AnonStorey33_1_U3CU3Em__FB_m2326377862_gshared (U3CCreateSetU3Ec__AnonStorey33_1_t2802865970 * __this, Il2CppObject * ___o0, Il2CppObject * ___v1, const MethodInfo* method)
{
	{
		FieldInfo_t * L_0 = (FieldInfo_t *)__this->get_fieldInfo_0();
		Il2CppObject * L_1 = ___o0;
		Il2CppObject * L_2 = ___v1;
		NullCheck((FieldInfo_t *)L_0);
		FieldInfo_SetValue_m2504255891((FieldInfo_t *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey34`1<System.Object>::.ctor()
extern "C"  void U3CCreateSetU3Ec__AnonStorey34_1__ctor_m2933444304_gshared (U3CCreateSetU3Ec__AnonStorey34_1_t3640493241 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey34`1<System.Object>::<>m__FC(T,System.Object)
extern "C"  void U3CCreateSetU3Ec__AnonStorey34_1_U3CU3Em__FC_m3304454120_gshared (U3CCreateSetU3Ec__AnonStorey34_1_t3640493241 * __this, Il2CppObject * ___o0, Il2CppObject * ___v1, const MethodInfo* method)
{
	{
		PropertyInfo_t * L_0 = (PropertyInfo_t *)__this->get_propertyInfo_0();
		Il2CppObject * L_1 = ___o0;
		Il2CppObject * L_2 = ___v1;
		NullCheck((PropertyInfo_t *)L_0);
		VirtActionInvoker3< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(26 /* System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Object[]) */, (PropertyInfo_t *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::.ctor(System.Collections.IList)
extern Il2CppCodeGenString* _stringLiteral1178974130;
extern const uint32_t ListWrapper_1__ctor_m2366675627_MetadataUsageId;
extern "C"  void ListWrapper_1__ctor_m2366675627_gshared (ListWrapper_1_t3921709322 * __this, Il2CppObject * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1__ctor_m2366675627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___list0;
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		((  void (*) (CollectionWrapper_1_t840219110 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CollectionWrapper_1_t840219110 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_1 = ___list0;
		ValidationUtils_ArgumentNotNull_m3243187033(NULL /*static, unused*/, (Il2CppObject *)L_1, (String_t*)_stringLiteral1178974130, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___list0;
		if (!((Il2CppObject*)IsInst(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))))
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_3 = ___list0;
		__this->set__genericList_3(((Il2CppObject*)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))));
	}

IL_0029:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::.ctor(System.Collections.Generic.IList`1<T>)
extern Il2CppCodeGenString* _stringLiteral1178974130;
extern const uint32_t ListWrapper_1__ctor_m2554805603_MetadataUsageId;
extern "C"  void ListWrapper_1__ctor_m2554805603_gshared (ListWrapper_1_t3921709322 * __this, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1__ctor_m2554805603_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___list0;
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		((  void (*) (CollectionWrapper_1_t840219110 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((CollectionWrapper_1_t840219110 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		Il2CppObject* L_1 = ___list0;
		ValidationUtils_ArgumentNotNull_m3243187033(NULL /*static, unused*/, (Il2CppObject *)L_1, (String_t*)_stringLiteral1178974130, /*hidden argument*/NULL);
		Il2CppObject* L_2 = ___list0;
		__this->set__genericList_3(L_2);
		return;
	}
}
// System.Int32 Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::IndexOf(T)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t ListWrapper_1_IndexOf_m1072497437_MetadataUsageId;
extern "C"  int32_t ListWrapper_1_IndexOf_m1072497437_gshared (ListWrapper_1_t3921709322 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1_IndexOf_m1072497437_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		Il2CppObject * L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		int32_t L_3 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = ___item0;
		NullCheck((Il2CppObject *)__this);
		int32_t L_5 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(7 /* System.Int32 System.Collections.IList::IndexOf(System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (Il2CppObject *)L_4);
		return L_5;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Insert(System.Int32,T)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t ListWrapper_1_Insert_m3073337676_MetadataUsageId;
extern "C"  void ListWrapper_1_Insert_m3073337676_gshared (ListWrapper_1_t3921709322 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1_Insert_m3073337676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		int32_t L_2 = ___index0;
		Il2CppObject * L_3 = ___item1;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<System.Object>::Insert(System.Int32,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_1, (int32_t)L_2, (Il2CppObject *)L_3);
		goto IL_002a;
	}

IL_001d:
	{
		int32_t L_4 = ___index0;
		Il2CppObject * L_5 = ___item1;
		NullCheck((Il2CppObject *)__this);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(8 /* System.Void System.Collections.IList::Insert(System.Int32,System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (int32_t)L_4, (Il2CppObject *)L_5);
	}

IL_002a:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::RemoveAt(System.Int32)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t ListWrapper_1_RemoveAt_m704212032_MetadataUsageId;
extern "C"  void ListWrapper_1_RemoveAt_m704212032_gshared (ListWrapper_1_t3921709322 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1_RemoveAt_m704212032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		int32_t L_2 = ___index0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Object>::RemoveAt(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_1, (int32_t)L_2);
		goto IL_0023;
	}

IL_001c:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppObject *)__this);
		InterfaceActionInvoker1< int32_t >::Invoke(10 /* System.Void System.Collections.IList::RemoveAt(System.Int32) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (int32_t)L_3);
	}

IL_0023:
	{
		return;
	}
}
// T Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t ListWrapper_1_get_Item_m745842054_MetadataUsageId;
extern "C"  Il2CppObject * ListWrapper_1_get_Item_m745842054_gshared (ListWrapper_1_t3921709322 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1_get_Item_m745842054_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		int32_t L_2 = ___index0;
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_1, (int32_t)L_2);
		return L_3;
	}

IL_0018:
	{
		int32_t L_4 = ___index0;
		NullCheck((Il2CppObject *)__this);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (int32_t)L_4);
		return ((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4)));
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::set_Item(System.Int32,T)
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern const uint32_t ListWrapper_1_set_Item_m4199764057_MetadataUsageId;
extern "C"  void ListWrapper_1_set_Item_m4199764057_gshared (ListWrapper_1_t3921709322 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListWrapper_1_set_Item_m4199764057_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		int32_t L_2 = ___index0;
		Il2CppObject * L_3 = ___value1;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Object>::set_Item(System.Int32,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_1, (int32_t)L_2, (Il2CppObject *)L_3);
		goto IL_002a;
	}

IL_001d:
	{
		int32_t L_4 = ___index0;
		Il2CppObject * L_5 = ___value1;
		NullCheck((Il2CppObject *)__this);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(3 /* System.Void System.Collections.IList::set_Item(System.Int32,System.Object) */, IList_t3321498491_il2cpp_TypeInfo_var, (Il2CppObject *)__this, (int32_t)L_4, (Il2CppObject *)L_5);
	}

IL_002a:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Add(T)
extern "C"  void ListWrapper_1_Add_m60348073_gshared (ListWrapper_1_t3921709322 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		Il2CppObject * L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		goto IL_0023;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___item0;
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		((  void (*) (CollectionWrapper_1_t840219110 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((CollectionWrapper_1_t840219110 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0023:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Clear()
extern "C"  void ListWrapper_1_Clear_m2871348141_gshared (ListWrapper_1_t3921709322 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_1);
		goto IL_0021;
	}

IL_001b:
	{
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		((  void (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((CollectionWrapper_1_t840219110 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
	}

IL_0021:
	{
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Contains(T)
extern "C"  bool ListWrapper_1_Contains_m2494154183_gshared (ListWrapper_1_t3921709322 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		Il2CppObject * L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = ___item0;
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		bool L_5 = ((  bool (*) (CollectionWrapper_1_t840219110 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((CollectionWrapper_1_t840219110 *)__this, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_5;
	}
}
// System.Void Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ListWrapper_1_CopyTo_m263509521_gshared (ListWrapper_1_t3921709322 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		ObjectU5BU5D_t3614634134* L_2 = ___array0;
		int32_t L_3 = ___arrayIndex1;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker2< ObjectU5BU5D_t3614634134*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_1, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3);
		goto IL_0025;
	}

IL_001d:
	{
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___arrayIndex1;
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		((  void (*) (CollectionWrapper_1_t840219110 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((CollectionWrapper_1_t840219110 *)__this, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
	}

IL_0025:
	{
		return;
	}
}
// System.Int32 Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::get_Count()
extern "C"  int32_t ListWrapper_1_get_Count_m822578180_gshared (ListWrapper_1_t3921709322 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		NullCheck((Il2CppObject*)L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0017:
	{
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		int32_t L_3 = ((  int32_t (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((CollectionWrapper_1_t840219110 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_3;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::get_IsReadOnly()
extern "C"  bool ListWrapper_1_get_IsReadOnly_m2983319533_gshared (ListWrapper_1_t3921709322 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		NullCheck((Il2CppObject*)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0017:
	{
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		bool L_3 = ((  bool (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((CollectionWrapper_1_t840219110 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_3;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::Remove(T)
extern "C"  bool ListWrapper_1_Remove_m225401320_gshared (ListWrapper_1_t3921709322 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		Il2CppObject * L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_0018:
	{
		Il2CppObject * L_4 = ___item0;
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		bool L_5 = ((  bool (*) (CollectionWrapper_1_t840219110 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((CollectionWrapper_1_t840219110 *)__this, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (bool)L_5;
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_002e;
		}
	}
	{
		Il2CppObject * L_7 = ___item0;
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		((  bool (*) (CollectionWrapper_1_t840219110 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CollectionWrapper_1_t840219110 *)__this, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_002e:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ListWrapper_1_GetEnumerator_m3921208890_gshared (ListWrapper_1_t3921709322 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0017:
	{
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		Il2CppObject* L_3 = ((  Il2CppObject* (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((CollectionWrapper_1_t840219110 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_3;
	}
}
// System.Object Newtonsoft.Json.Utilities.ListWrapper`1<System.Object>::get_UnderlyingList()
extern "C"  Il2CppObject * ListWrapper_1_get_UnderlyingList_m960612259_gshared (ListWrapper_1_t3921709322 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericList_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericList_3();
		return L_1;
	}

IL_0012:
	{
		NullCheck((CollectionWrapper_1_t840219110 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (CollectionWrapper_1_t840219110 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((CollectionWrapper_1_t840219110 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void MethodCall_2__ctor_m3467249549_gshared (MethodCall_2_t1283576322 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::Invoke(T,System.Object[])
extern "C"  Il2CppObject * MethodCall_2_Invoke_m3131637643_gshared (MethodCall_2_t1283576322 * __this, Il2CppObject * ___target0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		MethodCall_2_Invoke_m3131637643((MethodCall_2_t1283576322 *)__this->get_prev_9(),___target0, ___args1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___target0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___target0, ___args1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___target0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___target0, ___args1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___target0, ___args1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::BeginInvoke(T,System.Object[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MethodCall_2_BeginInvoke_m876470146_gshared (MethodCall_2_t1283576322 * __this, Il2CppObject * ___target0, ObjectU5BU5D_t3614634134* ___args1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___target0;
	__d_args[1] = ___args1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * MethodCall_2_EndInvoke_m407364021_gshared (MethodCall_2_t1283576322 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1__ctor_m2508195542_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2709430626 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<>m__106(TSource)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__106_m3171692391_MetadataUsageId;
extern "C"  bool U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__106_m3171692391_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2709430626 * __this, KeyValuePair_2_t38854645  ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__106_m3171692391_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_2_t1405561329 * L_0 = (Func_2_t1405561329 *)__this->get_valueSelector_0();
		KeyValuePair_2_t38854645  L_1 = ___s0;
		NullCheck((Func_2_t1405561329 *)L_0);
		String_t* L_2 = ((  String_t* (*) (Func_2_t1405561329 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t1405561329 *)L_0, (KeyValuePair_2_t38854645 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		String_t* L_3 = (String_t*)__this->get_testValue_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_4 = String_Compare_m3288062998(NULL /*static, unused*/, (String_t*)L_2, (String_t*)L_3, (int32_t)5, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<>m__107(TSource)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__107_m1285027138_MetadataUsageId;
extern "C"  bool U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__107_m1285027138_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2709430626 * __this, KeyValuePair_2_t38854645  ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__107_m1285027138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_2_t1405561329 * L_0 = (Func_2_t1405561329 *)__this->get_valueSelector_0();
		KeyValuePair_2_t38854645  L_1 = ___s0;
		NullCheck((Func_2_t1405561329 *)L_0);
		String_t* L_2 = ((  String_t* (*) (Func_2_t1405561329 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t1405561329 *)L_0, (KeyValuePair_2_t38854645 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		String_t* L_3 = (String_t*)__this->get_testValue_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_4 = String_Compare_m3288062998(NULL /*static, unused*/, (String_t*)L_2, (String_t*)L_3, (int32_t)4, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Object>::.ctor()
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1__ctor_m1411985081_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t1065057980 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Object>::<>m__106(TSource)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__106_m3805608978_MetadataUsageId;
extern "C"  bool U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__106_m3805608978_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t1065057980 * __this, Il2CppObject * ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__106_m3805608978_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_2_t2165275119 * L_0 = (Func_2_t2165275119 *)__this->get_valueSelector_0();
		Il2CppObject * L_1 = ___s0;
		NullCheck((Func_2_t2165275119 *)L_0);
		String_t* L_2 = ((  String_t* (*) (Func_2_t2165275119 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t2165275119 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		String_t* L_3 = (String_t*)__this->get_testValue_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_4 = String_Compare_m3288062998(NULL /*static, unused*/, (String_t*)L_2, (String_t*)L_3, (int32_t)5, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey39`1<System.Object>::<>m__107(TSource)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__107_m3804566609_MetadataUsageId;
extern "C"  bool U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__107_m3804566609_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t1065057980 * __this, Il2CppObject * ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_U3CU3Em__107_m3804566609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_2_t2165275119 * L_0 = (Func_2_t2165275119 *)__this->get_valueSelector_0();
		Il2CppObject * L_1 = ___s0;
		NullCheck((Func_2_t2165275119 *)L_0);
		String_t* L_2 = ((  String_t* (*) (Func_2_t2165275119 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t2165275119 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		String_t* L_3 = (String_t*)__this->get_testValue_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_4 = String_Compare_m3288062998(NULL /*static, unused*/, (String_t*)L_2, (String_t*)L_3, (int32_t)4, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Func`2<TKey,TValue>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral881008290;
extern const uint32_t ThreadSafeStore_2__ctor_m3678278222_MetadataUsageId;
extern "C"  void ThreadSafeStore_2__ctor_m3678278222_gshared (ThreadSafeStore_2_t4078621129 * __this, Func_2_t2880772247 * ___creator0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeStore_2__ctor_m3678278222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set__lock_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_2_t2880772247 * L_1 = ___creator0;
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t628810857 * L_2 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_2, (String_t*)_stringLiteral881008290, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Func_2_t2880772247 * L_3 = ___creator0;
		__this->set__creator_2(L_3);
		return;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_Get_m756191284_gshared (ThreadSafeStore_2_t4078621129 * __this, TypeNameKey_t3055062677  ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Dictionary_2_t2336777489 * L_0 = (Dictionary_2_t2336777489 *)__this->get__store_1();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		TypeNameKey_t3055062677  L_1 = ___key0;
		NullCheck((ThreadSafeStore_2_t4078621129 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t4078621129 *, TypeNameKey_t3055062677 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeStore_2_t4078621129 *)__this, (TypeNameKey_t3055062677 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}

IL_0013:
	{
		Dictionary_2_t2336777489 * L_3 = (Dictionary_2_t2336777489 *)__this->get__store_1();
		TypeNameKey_t3055062677  L_4 = ___key0;
		NullCheck((Dictionary_2_t2336777489 *)L_3);
		bool L_5 = ((  bool (*) (Dictionary_2_t2336777489 *, TypeNameKey_t3055062677 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2336777489 *)L_3, (TypeNameKey_t3055062677 )L_4, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (L_5)
		{
			goto IL_002e;
		}
	}
	{
		TypeNameKey_t3055062677  L_6 = ___key0;
		NullCheck((ThreadSafeStore_2_t4078621129 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t4078621129 *, TypeNameKey_t3055062677 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeStore_2_t4078621129 *)__this, (TypeNameKey_t3055062677 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_7;
	}

IL_002e:
	{
		Il2CppObject * L_8 = V_0;
		return L_8;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::AddValue(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_AddValue_m3370370496_gshared (ThreadSafeStore_2_t4078621129 * __this, TypeNameKey_t3055062677  ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Dictionary_2_t2336777489 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Func_2_t2880772247 * L_0 = (Func_2_t2880772247 *)__this->get__creator_2();
		TypeNameKey_t3055062677  L_1 = ___key0;
		NullCheck((Func_2_t2880772247 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t2880772247 *, TypeNameKey_t3055062677 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t2880772247 *)L_0, (TypeNameKey_t3055062677 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_0();
		V_1 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2336777489 * L_5 = (Dictionary_2_t2336777489 *)__this->get__store_1();
			if (L_5)
			{
				goto IL_0042;
			}
		}

IL_0025:
		{
			Dictionary_2_t2336777489 * L_6 = (Dictionary_2_t2336777489 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t2336777489 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
			__this->set__store_1(L_6);
			Dictionary_2_t2336777489 * L_7 = (Dictionary_2_t2336777489 *)__this->get__store_1();
			TypeNameKey_t3055062677  L_8 = ___key0;
			Il2CppObject * L_9 = V_0;
			NullCheck((Dictionary_2_t2336777489 *)L_7);
			((  void (*) (Dictionary_2_t2336777489 *, TypeNameKey_t3055062677 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2336777489 *)L_7, (TypeNameKey_t3055062677 )L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			goto IL_0078;
		}

IL_0042:
		{
			Dictionary_2_t2336777489 * L_10 = (Dictionary_2_t2336777489 *)__this->get__store_1();
			TypeNameKey_t3055062677  L_11 = ___key0;
			NullCheck((Dictionary_2_t2336777489 *)L_10);
			bool L_12 = ((  bool (*) (Dictionary_2_t2336777489 *, TypeNameKey_t3055062677 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2336777489 *)L_10, (TypeNameKey_t3055062677 )L_11, (Il2CppObject **)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
			if (!L_12)
			{
				goto IL_005d;
			}
		}

IL_0055:
		{
			Il2CppObject * L_13 = V_2;
			V_4 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x8C, FINALLY_0085);
		}

IL_005d:
		{
			Dictionary_2_t2336777489 * L_14 = (Dictionary_2_t2336777489 *)__this->get__store_1();
			Dictionary_2_t2336777489 * L_15 = (Dictionary_2_t2336777489 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t2336777489 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_15, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_3 = (Dictionary_2_t2336777489 *)L_15;
			Dictionary_2_t2336777489 * L_16 = V_3;
			TypeNameKey_t3055062677  L_17 = ___key0;
			Il2CppObject * L_18 = V_0;
			NullCheck((Dictionary_2_t2336777489 *)L_16);
			((  void (*) (Dictionary_2_t2336777489 *, TypeNameKey_t3055062677 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2336777489 *)L_16, (TypeNameKey_t3055062677 )L_17, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			Dictionary_2_t2336777489 * L_19 = V_3;
			__this->set__store_1(L_19);
		}

IL_0078:
		{
			Il2CppObject * L_20 = V_0;
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0x8C, FINALLY_0085);
		}

IL_0080:
		{
			; // IL_0080: leave IL_008c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0085;
	}

FINALLY_0085:
	{ // begin finally (depth: 1)
		Il2CppObject * L_21 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(133)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(133)
	{
		IL2CPP_JUMP_TBL(0x8C, IL_008c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008c:
	{
		Il2CppObject * L_22 = V_4;
		return L_22;
	}
}
// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(System.Func`2<TKey,TValue>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral881008290;
extern const uint32_t ThreadSafeStore_2__ctor_m1485686387_MetadataUsageId;
extern "C"  void ThreadSafeStore_2__ctor_m1485686387_gshared (ThreadSafeStore_2_t3893850024 * __this, Func_2_t2696001142 * ___creator0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeStore_2__ctor_m1485686387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set__lock_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_2_t2696001142 * L_1 = ___creator0;
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t628810857 * L_2 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_2, (String_t*)_stringLiteral881008290, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Func_2_t2696001142 * L_3 = ___creator0;
		__this->set__creator_2(L_3);
		return;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_Get_m1536546351_gshared (ThreadSafeStore_2_t3893850024 * __this, TypeConvertKey_t1788482786  ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Dictionary_2_t2152006384 * L_0 = (Dictionary_2_t2152006384 *)__this->get__store_1();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		TypeConvertKey_t1788482786  L_1 = ___key0;
		NullCheck((ThreadSafeStore_2_t3893850024 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t3893850024 *, TypeConvertKey_t1788482786 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeStore_2_t3893850024 *)__this, (TypeConvertKey_t1788482786 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}

IL_0013:
	{
		Dictionary_2_t2152006384 * L_3 = (Dictionary_2_t2152006384 *)__this->get__store_1();
		TypeConvertKey_t1788482786  L_4 = ___key0;
		NullCheck((Dictionary_2_t2152006384 *)L_3);
		bool L_5 = ((  bool (*) (Dictionary_2_t2152006384 *, TypeConvertKey_t1788482786 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2152006384 *)L_3, (TypeConvertKey_t1788482786 )L_4, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (L_5)
		{
			goto IL_002e;
		}
	}
	{
		TypeConvertKey_t1788482786  L_6 = ___key0;
		NullCheck((ThreadSafeStore_2_t3893850024 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t3893850024 *, TypeConvertKey_t1788482786 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeStore_2_t3893850024 *)__this, (TypeConvertKey_t1788482786 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_7;
	}

IL_002e:
	{
		Il2CppObject * L_8 = V_0;
		return L_8;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::AddValue(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_AddValue_m3015471795_gshared (ThreadSafeStore_2_t3893850024 * __this, TypeConvertKey_t1788482786  ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Dictionary_2_t2152006384 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Func_2_t2696001142 * L_0 = (Func_2_t2696001142 *)__this->get__creator_2();
		TypeConvertKey_t1788482786  L_1 = ___key0;
		NullCheck((Func_2_t2696001142 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t2696001142 *, TypeConvertKey_t1788482786 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t2696001142 *)L_0, (TypeConvertKey_t1788482786 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_0();
		V_1 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2152006384 * L_5 = (Dictionary_2_t2152006384 *)__this->get__store_1();
			if (L_5)
			{
				goto IL_0042;
			}
		}

IL_0025:
		{
			Dictionary_2_t2152006384 * L_6 = (Dictionary_2_t2152006384 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t2152006384 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
			__this->set__store_1(L_6);
			Dictionary_2_t2152006384 * L_7 = (Dictionary_2_t2152006384 *)__this->get__store_1();
			TypeConvertKey_t1788482786  L_8 = ___key0;
			Il2CppObject * L_9 = V_0;
			NullCheck((Dictionary_2_t2152006384 *)L_7);
			((  void (*) (Dictionary_2_t2152006384 *, TypeConvertKey_t1788482786 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2152006384 *)L_7, (TypeConvertKey_t1788482786 )L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			goto IL_0078;
		}

IL_0042:
		{
			Dictionary_2_t2152006384 * L_10 = (Dictionary_2_t2152006384 *)__this->get__store_1();
			TypeConvertKey_t1788482786  L_11 = ___key0;
			NullCheck((Dictionary_2_t2152006384 *)L_10);
			bool L_12 = ((  bool (*) (Dictionary_2_t2152006384 *, TypeConvertKey_t1788482786 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2152006384 *)L_10, (TypeConvertKey_t1788482786 )L_11, (Il2CppObject **)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
			if (!L_12)
			{
				goto IL_005d;
			}
		}

IL_0055:
		{
			Il2CppObject * L_13 = V_2;
			V_4 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x8C, FINALLY_0085);
		}

IL_005d:
		{
			Dictionary_2_t2152006384 * L_14 = (Dictionary_2_t2152006384 *)__this->get__store_1();
			Dictionary_2_t2152006384 * L_15 = (Dictionary_2_t2152006384 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t2152006384 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_15, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_3 = (Dictionary_2_t2152006384 *)L_15;
			Dictionary_2_t2152006384 * L_16 = V_3;
			TypeConvertKey_t1788482786  L_17 = ___key0;
			Il2CppObject * L_18 = V_0;
			NullCheck((Dictionary_2_t2152006384 *)L_16);
			((  void (*) (Dictionary_2_t2152006384 *, TypeConvertKey_t1788482786 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2152006384 *)L_16, (TypeConvertKey_t1788482786 )L_17, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			Dictionary_2_t2152006384 * L_19 = V_3;
			__this->set__store_1(L_19);
		}

IL_0078:
		{
			Il2CppObject * L_20 = V_0;
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0x8C, FINALLY_0085);
		}

IL_0080:
		{
			; // IL_0080: leave IL_008c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0085;
	}

FINALLY_0085:
	{ // begin finally (depth: 1)
		Il2CppObject * L_21 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(133)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(133)
	{
		IL2CPP_JUMP_TBL(0x8C, IL_008c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008c:
	{
		Il2CppObject * L_22 = V_4;
		return L_22;
	}
}
// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Object>::.ctor(System.Func`2<TKey,TValue>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral881008290;
extern const uint32_t ThreadSafeStore_2__ctor_m2716591129_MetadataUsageId;
extern "C"  void ThreadSafeStore_2__ctor_m2716591129_gshared (ThreadSafeStore_2_t4023353063 * __this, Func_2_t2825504181 * ___creator0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeStore_2__ctor_m2716591129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set__lock_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_2_t2825504181 * L_1 = ___creator0;
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t628810857 * L_2 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_2, (String_t*)_stringLiteral881008290, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Func_2_t2825504181 * L_3 = ___creator0;
		__this->set__creator_2(L_3);
		return;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_Get_m1768706765_gshared (ThreadSafeStore_2_t4023353063 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__store_1();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Il2CppObject * L_1 = ___key0;
		NullCheck((ThreadSafeStore_2_t4023353063 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t4023353063 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeStore_2_t4023353063 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}

IL_0013:
	{
		Dictionary_2_t2281509423 * L_3 = (Dictionary_2_t2281509423 *)__this->get__store_1();
		Il2CppObject * L_4 = ___key0;
		NullCheck((Dictionary_2_t2281509423 *)L_3);
		bool L_5 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2281509423 *)L_3, (Il2CppObject *)L_4, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (L_5)
		{
			goto IL_002e;
		}
	}
	{
		Il2CppObject * L_6 = ___key0;
		NullCheck((ThreadSafeStore_2_t4023353063 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t4023353063 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeStore_2_t4023353063 *)__this, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_7;
	}

IL_002e:
	{
		Il2CppObject * L_8 = V_0;
		return L_8;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Object>::AddValue(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_AddValue_m4069531929_gshared (ThreadSafeStore_2_t4023353063 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Dictionary_2_t2281509423 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Func_2_t2825504181 * L_0 = (Func_2_t2825504181 *)__this->get__creator_2();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Func_2_t2825504181 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t2825504181 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Func_2_t2825504181 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_0();
		V_1 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2281509423 * L_5 = (Dictionary_2_t2281509423 *)__this->get__store_1();
			if (L_5)
			{
				goto IL_0042;
			}
		}

IL_0025:
		{
			Dictionary_2_t2281509423 * L_6 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
			__this->set__store_1(L_6);
			Dictionary_2_t2281509423 * L_7 = (Dictionary_2_t2281509423 *)__this->get__store_1();
			Il2CppObject * L_8 = ___key0;
			Il2CppObject * L_9 = V_0;
			NullCheck((Dictionary_2_t2281509423 *)L_7);
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			goto IL_0078;
		}

IL_0042:
		{
			Dictionary_2_t2281509423 * L_10 = (Dictionary_2_t2281509423 *)__this->get__store_1();
			Il2CppObject * L_11 = ___key0;
			NullCheck((Dictionary_2_t2281509423 *)L_10);
			bool L_12 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2281509423 *)L_10, (Il2CppObject *)L_11, (Il2CppObject **)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
			if (!L_12)
			{
				goto IL_005d;
			}
		}

IL_0055:
		{
			Il2CppObject * L_13 = V_2;
			V_4 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x8C, FINALLY_0085);
		}

IL_005d:
		{
			Dictionary_2_t2281509423 * L_14 = (Dictionary_2_t2281509423 *)__this->get__store_1();
			Dictionary_2_t2281509423 * L_15 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_15, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_3 = (Dictionary_2_t2281509423 *)L_15;
			Dictionary_2_t2281509423 * L_16 = V_3;
			Il2CppObject * L_17 = ___key0;
			Il2CppObject * L_18 = V_0;
			NullCheck((Dictionary_2_t2281509423 *)L_16);
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_16, (Il2CppObject *)L_17, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			Dictionary_2_t2281509423 * L_19 = V_3;
			__this->set__store_1(L_19);
		}

IL_0078:
		{
			Il2CppObject * L_20 = V_0;
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0x8C, FINALLY_0085);
		}

IL_0080:
		{
			; // IL_0080: leave IL_008c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0085;
	}

FINALLY_0085:
	{ // begin finally (depth: 1)
		Il2CppObject * L_21 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(133)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(133)
	{
		IL2CPP_JUMP_TBL(0x8C, IL_008c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008c:
	{
		Il2CppObject * L_22 = V_4;
		return L_22;
	}
}
// System.Void SA.Common.Pattern.NonMonoSingleton`1<System.Object>::.ctor()
extern "C"  void NonMonoSingleton_1__ctor_m3099661804_gshared (NonMonoSingleton_1_t461172036 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA.Common.Pattern.NonMonoSingleton`1<System.Object>::.cctor()
extern "C"  void NonMonoSingleton_1__cctor_m3625282977_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// T SA.Common.Pattern.NonMonoSingleton`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * NonMonoSingleton_1_get_Instance_m2157798457_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ((NonMonoSingleton_1_t461172036_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__Instance_0();
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((NonMonoSingleton_1_t461172036_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set__Instance_0(L_1);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_2 = ((NonMonoSingleton_1_t461172036_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__Instance_0();
		return L_2;
	}
}
// System.Void SA.Common.Pattern.Singleton`1<System.Object>::.ctor()
extern "C"  void Singleton_1__ctor_m4152044218_gshared (Singleton_1_t3172014400 * __this, const MethodInfo* method)
{
	{
		NullCheck((MonoBehaviour_t1158329972 *)__this);
		MonoBehaviour__ctor_m2464341955((MonoBehaviour_t1158329972 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA.Common.Pattern.Singleton`1<System.Object>::.cctor()
extern "C"  void Singleton_1__cctor_m982417053_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// T SA.Common.Pattern.Singleton`1<System.Object>::get_Instance()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const uint32_t Singleton_1_get_Instance_m3228489301_MetadataUsageId;
extern "C"  Il2CppObject * Singleton_1_get_Instance_m3228489301_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Singleton_1_get_Instance_m3228489301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_0 = ((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_applicationIsQuitting_3();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0092;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_4 = Object_FindObjectOfType_m2330404063(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set__instance_2(((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
		Il2CppObject * L_5 = ((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__instance_2();
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0092;
		}
	}
	{
		GameObject_t1756533147 * L_7 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m498247354(L_7, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_7);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((GameObject_t1756533147 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set__instance_2(L_8);
		NullCheck((Component_t3819376471 *)(*(((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of__instance_2())));
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835((Component_t3819376471 *)(*(((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of__instance_2())), /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)(*(((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of__instance_2())));
		Type_t * L_10 = Object_GetType_m191970594((Il2CppObject *)(*(((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of__instance_2())), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_10);
		NullCheck((Object_t1021602117 *)L_9);
		Object_set_name_m4157836998((Object_t1021602117 *)L_9, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_0092:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_12 = ((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__instance_2();
		return L_12;
	}
}
// System.Boolean SA.Common.Pattern.Singleton`1<System.Object>::get_HasInstance()
extern "C"  bool Singleton_1_get_HasInstance_m2551508260_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean SA.Common.Pattern.Singleton`1<System.Object>::get_IsDestroyed()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Singleton_1_get_IsDestroyed_m4170993228_MetadataUsageId;
extern "C"  bool Singleton_1_get_IsDestroyed_m4170993228_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Singleton_1_get_IsDestroyed_m4170993228_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)1;
	}

IL_0017:
	{
		return (bool)0;
	}
}
// System.Void SA.Common.Pattern.Singleton`1<System.Object>::OnDestroy()
extern "C"  void Singleton_1_OnDestroy_m3790554761_gshared (Singleton_1_t3172014400 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->set__instance_2(((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->set_applicationIsQuitting_3((bool)1);
		return;
	}
}
// System.Void SA.Common.Pattern.Singleton`1<System.Object>::OnApplicationQuit()
extern "C"  void Singleton_1_OnApplicationQuit_m1956476828_gshared (Singleton_1_t3172014400 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->set__instance_2(((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		((Singleton_1_t3172014400_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->set_applicationIsQuitting_3((bool)1);
		return;
	}
}
// System.Void SA_Singleton_OLD`1<System.Object>::.ctor()
extern "C"  void SA_Singleton_OLD_1__ctor_m2643683696_gshared (SA_Singleton_OLD_1_t3980293213 * __this, const MethodInfo* method)
{
	{
		NullCheck((MonoBehaviour_t1158329972 *)__this);
		MonoBehaviour__ctor_m2464341955((MonoBehaviour_t1158329972 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SA_Singleton_OLD`1<System.Object>::.cctor()
extern "C"  void SA_Singleton_OLD_1__cctor_m1355192463_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// T SA_Singleton_OLD`1<System.Object>::get_instance()
extern "C"  Il2CppObject * SA_Singleton_OLD_1_get_instance_m1963118739_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
// T SA_Singleton_OLD`1<System.Object>::get_Instance()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const uint32_t SA_Singleton_OLD_1_get_Instance_m2967800051_MetadataUsageId;
extern "C"  Il2CppObject * SA_Singleton_OLD_1_get_Instance_m2967800051_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Singleton_OLD_1_get_Instance_m2967800051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_0 = ((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_applicationIsQuitting_3();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_1 = ((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get__instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0092;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_4 = Object_FindObjectOfType_m2330404063(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->set__instance_2(((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		Il2CppObject * L_5 = ((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get__instance_2();
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0092;
		}
	}
	{
		GameObject_t1756533147 * L_7 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m498247354(L_7, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_7);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((GameObject_t1756533147 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->set__instance_2(L_8);
		NullCheck((Component_t3819376471 *)(*(((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_address_of__instance_2())));
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835((Component_t3819376471 *)(*(((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_address_of__instance_2())), /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)(*(((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_address_of__instance_2())));
		Type_t * L_10 = Object_GetType_m191970594((Il2CppObject *)(*(((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_address_of__instance_2())), /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_10);
		NullCheck((Object_t1021602117 *)L_9);
		Object_set_name_m4157836998((Object_t1021602117 *)L_9, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_0092:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_12 = ((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get__instance_2();
		return L_12;
	}
}
// System.Boolean SA_Singleton_OLD`1<System.Object>::get_HasInstance()
extern "C"  bool SA_Singleton_OLD_1_get_HasInstance_m4184706798_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean SA_Singleton_OLD`1<System.Object>::get_IsDestroyed()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t SA_Singleton_OLD_1_get_IsDestroyed_m1648274602_MetadataUsageId;
extern "C"  bool SA_Singleton_OLD_1_get_IsDestroyed_m1648274602_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SA_Singleton_OLD_1_get_IsDestroyed_m1648274602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_0 = ((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get__instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)1;
	}

IL_0017:
	{
		return (bool)0;
	}
}
// System.Void SA_Singleton_OLD`1<System.Object>::OnDestroy()
extern "C"  void SA_Singleton_OLD_1_OnDestroy_m1680414419_gshared (SA_Singleton_OLD_1_t3980293213 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)->static_fields)->set__instance_2(((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))));
		((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)->static_fields)->set_applicationIsQuitting_3((bool)1);
		return;
	}
}
// System.Void SA_Singleton_OLD`1<System.Object>::OnApplicationQuit()
extern "C"  void SA_Singleton_OLD_1_OnApplicationQuit_m1864960902_gshared (SA_Singleton_OLD_1_t3980293213 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)->static_fields)->set__instance_2(((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))));
		((SA_Singleton_OLD_1_t3980293213_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)->static_fields)->set_applicationIsQuitting_3((bool)1);
		return;
	}
}
// System.Void Shared.Response`1<System.Object>::.ctor()
extern "C"  void Response_1__ctor_m920360982_gshared (Response_1_t3572928552 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2__ctor_m2422343709_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2__ctor_m2422343709_gshared (ThreadSafeDictionary_2_t1463927430 * __this, ThreadSafeDictionaryValueFactory_2_t958855109 * ___valueFactory0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2__ctor_m2422343709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set__lock_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		ThreadSafeDictionaryValueFactory_2_t958855109 * L_1 = ___valueFactory0;
		__this->set__valueFactory_1(L_1);
		return;
	}
}
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1443063812_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Enumerator_t3601534125  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_2);
		return (Il2CppObject *)L_3;
	}
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_Get_m1259355240_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Il2CppObject * L_1 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1463927430 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1463927430 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ThreadSafeDictionary_2_t1463927430 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_2;
	}

IL_0013:
	{
		Dictionary_2_t2281509423 * L_3 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		Il2CppObject * L_4 = ___key0;
		NullCheck((Dictionary_2_t2281509423 *)L_3);
		bool L_5 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t2281509423 *)L_3, (Il2CppObject *)L_4, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		if (L_5)
		{
			goto IL_002e;
		}
	}
	{
		Il2CppObject * L_6 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1463927430 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1463927430 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ThreadSafeDictionary_2_t1463927430 *)__this, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_7;
	}

IL_002e:
	{
		Il2CppObject * L_8 = V_0;
		return L_8;
	}
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::AddValue(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_AddValue_m2068149316_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Dictionary_2_t2281509423 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ThreadSafeDictionaryValueFactory_2_t958855109 * L_0 = (ThreadSafeDictionaryValueFactory_2_t958855109 *)__this->get__valueFactory_1();
		Il2CppObject * L_1 = ___key0;
		NullCheck((ThreadSafeDictionaryValueFactory_2_t958855109 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeDictionaryValueFactory_2_t958855109 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((ThreadSafeDictionaryValueFactory_2_t958855109 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_0();
		V_1 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2281509423 * L_5 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
			if (L_5)
			{
				goto IL_0042;
			}
		}

IL_0025:
		{
			Dictionary_2_t2281509423 * L_6 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
			((  void (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set__dictionary_2(L_6);
			Dictionary_2_t2281509423 * L_7 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
			Il2CppObject * L_8 = ___key0;
			Il2CppObject * L_9 = V_0;
			NullCheck((Dictionary_2_t2281509423 *)L_7);
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2281509423 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			goto IL_0078;
		}

IL_0042:
		{
			Dictionary_2_t2281509423 * L_10 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
			Il2CppObject * L_11 = ___key0;
			NullCheck((Dictionary_2_t2281509423 *)L_10);
			bool L_12 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t2281509423 *)L_10, (Il2CppObject *)L_11, (Il2CppObject **)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
			if (!L_12)
			{
				goto IL_005d;
			}
		}

IL_0055:
		{
			Il2CppObject * L_13 = V_2;
			V_4 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x86, FINALLY_007d);
		}

IL_005d:
		{
			Dictionary_2_t2281509423 * L_14 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
			Dictionary_2_t2281509423 * L_15 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_15, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
			V_3 = (Dictionary_2_t2281509423 *)L_15;
			Dictionary_2_t2281509423 * L_16 = V_3;
			Il2CppObject * L_17 = ___key0;
			Il2CppObject * L_18 = V_0;
			NullCheck((Dictionary_2_t2281509423 *)L_16);
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2281509423 *)L_16, (Il2CppObject *)L_17, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			Dictionary_2_t2281509423 * L_19 = V_3;
			__this->set__dictionary_2(L_19);
		}

IL_0078:
		{
			IL2CPP_LEAVE(0x84, FINALLY_007d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_007d;
	}

FINALLY_007d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_20 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_20, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(125)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(125)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_JUMP_TBL(0x84, IL_0084)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0084:
	{
		Il2CppObject * L_21 = V_0;
		return L_21;
	}

IL_0086:
	{
		Il2CppObject * L_22 = V_4;
		return L_22;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Add_m2282389750_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_Add_m2282389750_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Add_m2282389750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool ThreadSafeDictionary_2_ContainsKey_m379210650_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2281509423 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_2;
	}
}
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_get_Keys_m2226270717_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		KeyCollection_t470039898 * L_1 = ((  KeyCollection_t470039898 * (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_1;
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(TKey)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Remove_m2158894434_MetadataUsageId;
extern "C"  bool ThreadSafeDictionary_2_Remove_m2158894434_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Remove_m2158894434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool ThreadSafeDictionary_2_TryGetValue_m620057635_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	{
		Il2CppObject ** L_0 = ___value1;
		Il2CppObject * L_1 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1463927430 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1463927430 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((ThreadSafeDictionary_2_t1463927430 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		(*(Il2CppObject **)L_0) = L_2;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_0, L_2);
		return (bool)1;
	}
}
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Values()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_get_Values_m2761852509_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		ValueCollection_t984569266 * L_1 = ((  ValueCollection_t984569266 * (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_get_Item_m2261720138_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1463927430 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1463927430 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((ThreadSafeDictionary_2_t1463927430 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_1;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_set_Item_m2513745515_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_set_Item_m2513745515_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_set_Item_m2513745515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Add_m1306324747_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_Add_m1306324747_gshared (ThreadSafeDictionary_2_t1463927430 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Add_m1306324747_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Clear()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Clear_m3155495606_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_Clear_m3155495606_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Clear_m3155495606_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Contains_m1533726473_MetadataUsageId;
extern "C"  bool ThreadSafeDictionary_2_Contains_m1533726473_gshared (ThreadSafeDictionary_2_t1463927430 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Contains_m1533726473_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_CopyTo_m1781093487_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_CopyTo_m1781093487_gshared (ThreadSafeDictionary_2_t1463927430 * __this, KeyValuePair_2U5BU5D_t2854920344* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_CopyTo_m1781093487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t ThreadSafeDictionary_2_get_Count_m3265115815_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_1;
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_get_IsReadOnly_m1417152376_MetadataUsageId;
extern "C"  bool ThreadSafeDictionary_2_get_IsReadOnly_m1417152376_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_get_IsReadOnly_m1417152376_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Remove_m1651487354_MetadataUsageId;
extern "C"  bool ThreadSafeDictionary_2_Remove_m1651487354_gshared (ThreadSafeDictionary_2_t1463927430 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Remove_m1651487354_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_GetEnumerator_m527450770_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Enumerator_t3601534125  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_2);
		return (Il2CppObject*)L_3;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ThreadSafeDictionaryValueFactory_2__ctor_m3821979982_gshared (ThreadSafeDictionaryValueFactory_2_t958855109 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::Invoke(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_Invoke_m1040912419_gshared (ThreadSafeDictionaryValueFactory_2_t958855109 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ThreadSafeDictionaryValueFactory_2_Invoke_m1040912419((ThreadSafeDictionaryValueFactory_2_t958855109 *)__this->get_prev_9(),___key0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::BeginInvoke(TKey,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_BeginInvoke_m2175992264_gshared (ThreadSafeDictionaryValueFactory_2_t958855109 * __this, Il2CppObject * ___key0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___key0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_EndInvoke_m235081874_gshared (ThreadSafeDictionaryValueFactory_2_t958855109 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Action`1<AN_LicenseRequestResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1176810652_gshared (Action_1_t2133169943 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<AN_LicenseRequestResult>::Invoke(T)
extern "C"  void Action_1_Invoke_m1757436011_gshared (Action_1_t2133169943 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1757436011((Action_1_t2133169943 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<AN_LicenseRequestResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* AN_LicenseRequestResult_t2331370561_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2714055490_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2714055490_gshared (Action_1_t2133169943 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2714055490_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AN_LicenseRequestResult_t2331370561_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<AN_LicenseRequestResult>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2649515015_gshared (Action_1_t2133169943 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<AndroidDialogResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m4243245259_gshared (Action_1_t1465846336 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<AndroidDialogResult>::Invoke(T)
extern "C"  void Action_1_Invoke_m3968099324_gshared (Action_1_t1465846336 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3968099324((Action_1_t1465846336 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<AndroidDialogResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* AndroidDialogResult_t1664046954_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2354406687_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2354406687_gshared (Action_1_t1465846336 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2354406687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AndroidDialogResult_t1664046954_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<AndroidDialogResult>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m433669130_gshared (Action_1_t1465846336 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GP_GamesStatusCodes>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3454327224_gshared (Action_1_t815305555 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GP_GamesStatusCodes>::Invoke(T)
extern "C"  void Action_1_Invoke_m3995201175_gshared (Action_1_t815305555 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3995201175((Action_1_t815305555 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GP_GamesStatusCodes>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* GP_GamesStatusCodes_t1013506173_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1743273470_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1743273470_gshared (Action_1_t815305555 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1743273470_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(GP_GamesStatusCodes_t1013506173_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GP_GamesStatusCodes>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3336512219_gshared (Action_1_t815305555 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GPConnectionState>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3466626195_gshared (Action_1_t449146262 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GPConnectionState>::Invoke(T)
extern "C"  void Action_1_Invoke_m3111096258_gshared (Action_1_t449146262 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3111096258((Action_1_t449146262 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GPConnectionState>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* GPConnectionState_t647346880_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m375315415_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m375315415_gshared (Action_1_t449146262 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m375315415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(GPConnectionState_t647346880_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GPConnectionState>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2486922292_gshared (Action_1_t449146262 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<InstagramPostResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3434190874_gshared (Action_1_t237882577 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<InstagramPostResult>::Invoke(T)
extern "C"  void Action_1_Invoke_m3099457645_gshared (Action_1_t237882577 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3099457645((Action_1_t237882577 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<InstagramPostResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* InstagramPostResult_t436083195_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m4038205824_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m4038205824_gshared (Action_1_t237882577 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m4038205824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(InstagramPostResult_t436083195_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<InstagramPostResult>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4123268021_gshared (Action_1_t237882577 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<IOSDialogResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m4190182587_gshared (Action_1_t3541040698 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<IOSDialogResult>::Invoke(T)
extern "C"  void Action_1_Invoke_m2187606108_gshared (Action_1_t3541040698 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2187606108((Action_1_t3541040698 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<IOSDialogResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* IOSDialogResult_t3739241316_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3978838501_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3978838501_gshared (Action_1_t3541040698 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3978838501_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IOSDialogResult_t3739241316_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<IOSDialogResult>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3011300264_gshared (Action_1_t3541040698 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<ISN_SwipeDirection>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m226238497_gshared (Action_1_t570721078 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<ISN_SwipeDirection>::Invoke(T)
extern "C"  void Action_1_Invoke_m2095669440_gshared (Action_1_t570721078 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2095669440((Action_1_t570721078 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<ISN_SwipeDirection>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ISN_SwipeDirection_t768921696_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3604589487_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3604589487_gshared (Action_1_t570721078 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3604589487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ISN_SwipeDirection_t768921696_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<ISN_SwipeDirection>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1120900782_gshared (Action_1_t570721078 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<MP_MusicPlaybackState>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m482734902_gshared (Action_1_t2166513183 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<MP_MusicPlaybackState>::Invoke(T)
extern "C"  void Action_1_Invoke_m3664487689_gshared (Action_1_t2166513183 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3664487689((Action_1_t2166513183 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<MP_MusicPlaybackState>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* MP_MusicPlaybackState_t2364713801_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3272998202_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3272998202_gshared (Action_1_t2166513183 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3272998202_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(MP_MusicPlaybackState_t2364713801_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<MP_MusicPlaybackState>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1447439599_gshared (Action_1_t2166513183 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m309821356_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3662000152_gshared (Action_1_t3627374100 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3662000152((Action_1_t3627374100 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m226849422_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m226849422_gshared (Action_1_t3627374100 * __this, bool ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m226849422_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2990292511_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.DateTime>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3082271741_gshared (Action_1_t495005051 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.DateTime>::Invoke(T)
extern "C"  void Action_1_Invoke_m2972379272_gshared (Action_1_t495005051 * __this, DateTime_t693205669  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2972379272((Action_1_t495005051 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, DateTime_t693205669  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, DateTime_t693205669  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.DateTime>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m878823651_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m878823651_gshared (Action_1_t495005051 * __this, DateTime_t693205669  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m878823651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.DateTime>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m133124648_gshared (Action_1_t495005051 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m345545414_gshared (Action_1_t1873676830 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Int32>::Invoke(T)
extern "C"  void Action_1_Invoke_m3352874125_gshared (Action_1_t1873676830 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3352874125((Action_1_t1873676830 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1720726178_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1720726178_gshared (Action_1_t1873676830 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1720726178_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3128406917_gshared (Action_1_t1873676830 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m584977596_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m101203496_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m101203496((Action_1_t2491248677 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1305519803_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2057605070_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1373098066_gshared (Action_1_t1878309314 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Single>::Invoke(T)
extern "C"  void Action_1_Invoke_m1615209507_gshared (Action_1_t1878309314 * __this, float ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1615209507((Action_1_t1878309314 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1617223338_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1617223338_gshared (Action_1_t1878309314 * __this, float ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1617223338_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m249557173_gshared (Action_1_t1878309314 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2249515781_gshared (Action_1_t2755832024 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::Invoke(T)
extern "C"  void Action_1_Invoke_m2983653028_gshared (Action_1_t2755832024 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2983653028((Action_1_t2755832024 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1319991882_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1319991882_gshared (Action_1_t2755832024 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1319991882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1485676899_gshared (Action_1_t2755832024 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m978039278_gshared (Action_1_t2045506962 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void Action_1_Invoke_m181430207_gshared (Action_1_t2045506962 * __this, Vector3_t2243707580  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m181430207((Action_1_t2045506962 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m402802490_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m402802490_gshared (Action_1_t2045506962 * __this, Vector3_t2243707580  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m402802490_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4206793277_gshared (Action_1_t2045506962 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<GK_MatchType,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3696273566_gshared (Action_2_t3551550644 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<GK_MatchType,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2784585987_gshared (Action_2_t3551550644 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m2784585987((Action_2_t3551550644 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<GK_MatchType,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* GK_MatchType_t1493351924_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m2216000420_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m2216000420_gshared (Action_2_t3551550644 * __this, int32_t ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m2216000420_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(GK_MatchType_t1493351924_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<GK_MatchType,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m4029220348_gshared (Action_2_t3551550644 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m1053279240_gshared (Action_2_t1907880187 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,System.Int32>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2365036873_gshared (Action_2_t1907880187 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m2365036873((Action_2_t1907880187 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,System.Int32>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m1925898598_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1925898598_gshared (Action_2_t1907880187 * __this, bool ___arg10, int32_t ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m1925898598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m343868110_gshared (Action_2_t1907880187 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m946854823_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3842146412_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m3842146412((Action_2_t2525452034 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m3907381723_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3907381723_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m3907381723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2798191693_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3463170984_gshared (Action_2_t2790035381 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3143687639_gshared (Action_2_t2790035381 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m3143687639((Action_2_t2790035381 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m410485158_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m410485158_gshared (Action_2_t2790035381 * __this, bool ___arg10, int32_t ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m410485158_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m99462074_gshared (Action_2_t2790035381 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m1121744629_gshared (Action_2_t1158962578 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1462475090_gshared (Action_2_t1158962578 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1462475090((Action_2_t1158962578 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m772046713_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m772046713_gshared (Action_2_t1158962578 * __this, bool ___arg10, int32_t ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m772046713_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m430603547_gshared (Action_2_t1158962578 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,GK_InviteRecipientResponse>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m2260828716_gshared (Action_2_t3321460360 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,GK_InviteRecipientResponse>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m706623781_gshared (Action_2_t3321460360 * __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m706623781((Action_2_t3321460360 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,GK_InviteRecipientResponse>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* GK_InviteRecipientResponse_t3438857802_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m594672950_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m594672950_gshared (Action_2_t3321460360 * __this, Il2CppObject * ___arg10, int32_t ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m594672950_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(GK_InviteRecipientResponse_t3438857802_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,GK_InviteRecipientResponse>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3189183710_gshared (Action_2_t3321460360 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m2125582079_gshared (Action_2_t3708177276 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Boolean>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m4016543000_gshared (Action_2_t3708177276 * __this, Il2CppObject * ___arg10, bool ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m4016543000((Action_2_t3708177276 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, bool ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, bool ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m963341251_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m963341251_gshared (Action_2_t3708177276 * __this, Il2CppObject * ___arg10, bool ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m963341251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3658961145_gshared (Action_2_t3708177276 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3362391082_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2142187531_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m2142187531((Action_2_t2572051853 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1914861552_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3956733788_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<GK_MatchType,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m2995706635_gshared (Action_3_t3536498748 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<GK_MatchType,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m76985707_gshared (Action_3_t3536498748 * __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m76985707((Action_3_t3536498748 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<GK_MatchType,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* GK_MatchType_t1493351924_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m2213772654_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m2213772654_gshared (Action_3_t3536498748 * __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m2213772654_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(GK_MatchType_t1493351924_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<GK_MatchType,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m887229305_gshared (Action_3_t3536498748 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,GK_PlayerConnectionState,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m969386966_gshared (Action_3_t905177775 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,GK_PlayerConnectionState,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m4203154120_gshared (Action_3_t905177775 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m4203154120((Action_3_t905177775 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,GK_PlayerConnectionState,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* GK_PlayerConnectionState_t2434478783_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m937272357_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m937272357_gshared (Action_3_t905177775 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m937272357_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(GK_PlayerConnectionState_t2434478783_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,GK_PlayerConnectionState,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m3896340280_gshared (Action_3_t905177775 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m218933466_gshared (Action_3_t2251782606 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Boolean>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m3194942528_gshared (Action_3_t2251782606 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, bool ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m3194942528((Action_3_t2251782606 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, bool ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, bool ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, bool ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,System.Object,System.Boolean>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m2167027095_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m2167027095_gshared (Action_3_t2251782606 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, bool ___arg32, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m2167027095_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg32);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m485955472_gshared (Action_3_t2251782606 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m2182397233_gshared (Action_3_t1115657183 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m759875865_gshared (Action_3_t1115657183 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m759875865((Action_3_t1115657183 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_3_BeginInvoke_m275697784_gshared (Action_3_t1115657183 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m4265906515_gshared (Action_3_t1115657183 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1015489335_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1791706206_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2580780957_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2489948797_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t2471096271 * L_2 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t2471096271 * L_9 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1859988746_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1942816078_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m285299945_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m480171694_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		CustomAttributeNamedArgument_t94157543  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m949306872_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t4170771815 * L_2 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t4170771815 * L_9 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_10 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2403602883_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m409316647_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m988222504_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2332089385_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		CustomAttributeTypedArgument_t1498197914  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m692741405_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t1279844890 * L_2 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t1279844890 * L_9 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_10 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2201090542_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m2430810679_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m2780765696_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t2471096271 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t2471096271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t2471096271 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId;
extern "C"  Il2CppObject * ArrayReadOnlyList_1_get_Item_m176001975_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m314687476_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m962317777_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2717922212_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3970067462_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m2539474626_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1266627404_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m816115094_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m1078352793_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1537228832_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m1136669199_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m1875216835_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2701218731_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2289309720_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m691892240_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3039869667_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t4170771815 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t4170771815 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t4170771815 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t94157543  ArrayReadOnlyList_1_get_Item_m2694472846_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3536854615_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2661355086_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2189922207_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m961024239_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m1565299387_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1269788217_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m4003949395_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m634288642_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1220844927_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m2938723476_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m2325516426_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m4104441984_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2160816107_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m3778554727_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgumentU5BU5D_t1075686591* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3194679940_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t1279844890 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t1279844890 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t1279844890 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2045253203_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t1498197914  ArrayReadOnlyList_1_get_Item_m2045253203_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2045253203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m1476592004_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2272682593_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m745254596_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m592463462_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m638842154_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1984901664_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m3708038182_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgumentU5BU5D_t1075686591* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m3821693737_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1809425308_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m503707439_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m632503387_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2270349795_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2158247090_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2158247090_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2158247090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<AN_ManifestPermission>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2417370447_gshared (InternalEnumerator_1_t2630862464 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2417370447_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2630862464 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2630862464 *>(__this + 1);
	InternalEnumerator_1__ctor_m2417370447(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<AN_ManifestPermission>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3447903255_gshared (InternalEnumerator_1_t2630862464 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3447903255_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2630862464 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2630862464 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3447903255(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<AN_ManifestPermission>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2578798271_gshared (InternalEnumerator_1_t2630862464 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m1803544294((InternalEnumerator_1_t2630862464 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2578798271_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2630862464 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2630862464 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2578798271(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AN_ManifestPermission>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2224690162_gshared (InternalEnumerator_1_t2630862464 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2224690162_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2630862464 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2630862464 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2224690162(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<AN_ManifestPermission>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2898560851_gshared (InternalEnumerator_1_t2630862464 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2898560851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2630862464 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2630862464 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2898560851(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<AN_ManifestPermission>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1803544294_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1803544294_gshared (InternalEnumerator_1_t2630862464 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1803544294_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1803544294_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2630862464 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2630862464 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1803544294(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AN_PermissionState>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m768548935_gshared (InternalEnumerator_1_t1696953486 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m768548935_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1696953486 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1696953486 *>(__this + 1);
	InternalEnumerator_1__ctor_m768548935(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<AN_PermissionState>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4103474431_gshared (InternalEnumerator_1_t1696953486 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4103474431_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1696953486 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1696953486 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4103474431(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<AN_PermissionState>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m793817675_gshared (InternalEnumerator_1_t1696953486 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m242532934((InternalEnumerator_1_t1696953486 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m793817675_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1696953486 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1696953486 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m793817675(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<AN_PermissionState>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m373497536_gshared (InternalEnumerator_1_t1696953486 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m373497536_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1696953486 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1696953486 *>(__this + 1);
	InternalEnumerator_1_Dispose_m373497536(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<AN_PermissionState>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1683395259_gshared (InternalEnumerator_1_t1696953486 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1683395259_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1696953486 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1696953486 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1683395259(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<AN_PermissionState>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m242532934_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m242532934_gshared (InternalEnumerator_1_t1696953486 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m242532934_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m242532934_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1696953486 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1696953486 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m242532934(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FB_ProfileImageSize>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4177615517_gshared (InternalEnumerator_1_t3862080392 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4177615517_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3862080392 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3862080392 *>(__this + 1);
	InternalEnumerator_1__ctor_m4177615517(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<FB_ProfileImageSize>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3252548625_gshared (InternalEnumerator_1_t3862080392 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3252548625_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3862080392 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3862080392 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3252548625(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<FB_ProfileImageSize>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3399779653_gshared (InternalEnumerator_1_t3862080392 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m581384578((InternalEnumerator_1_t3862080392 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3399779653_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3862080392 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3862080392 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3399779653(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<FB_ProfileImageSize>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2528880210_gshared (InternalEnumerator_1_t3862080392 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2528880210_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3862080392 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3862080392 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2528880210(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<FB_ProfileImageSize>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3505086881_gshared (InternalEnumerator_1_t3862080392 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3505086881_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3862080392 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3862080392 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3505086881(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<FB_ProfileImageSize>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m581384578_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m581384578_gshared (InternalEnumerator_1_t3862080392 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m581384578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m581384578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3862080392 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3862080392 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m581384578(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<GP_QuestsSelect>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1809892872_gshared (InternalEnumerator_1_t2929754617 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1809892872_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2929754617 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2929754617 *>(__this + 1);
	InternalEnumerator_1__ctor_m1809892872(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<GP_QuestsSelect>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2890061920_gshared (InternalEnumerator_1_t2929754617 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2890061920_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2929754617 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2929754617 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2890061920(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<GP_QuestsSelect>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3743026934_gshared (InternalEnumerator_1_t2929754617 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m2001776303((InternalEnumerator_1_t2929754617 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3743026934_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2929754617 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2929754617 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3743026934(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<GP_QuestsSelect>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m405496395_gshared (InternalEnumerator_1_t2929754617 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m405496395_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2929754617 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2929754617 *>(__this + 1);
	InternalEnumerator_1_Dispose_m405496395(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<GP_QuestsSelect>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1137099008_gshared (InternalEnumerator_1_t2929754617 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1137099008_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2929754617 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2929754617 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1137099008(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<GP_QuestsSelect>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2001776303_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m2001776303_gshared (InternalEnumerator_1_t2929754617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2001776303_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m2001776303_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2929754617 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2929754617 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2001776303(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<GP_TBM_MatchTurnStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2830510681_gshared (InternalEnumerator_1_t3080482812 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2830510681_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3080482812 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3080482812 *>(__this + 1);
	InternalEnumerator_1__ctor_m2830510681(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<GP_TBM_MatchTurnStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4190331773_gshared (InternalEnumerator_1_t3080482812 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4190331773_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3080482812 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3080482812 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4190331773(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<GP_TBM_MatchTurnStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2187749429_gshared (InternalEnumerator_1_t3080482812 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3325508752((InternalEnumerator_1_t3080482812 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2187749429_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3080482812 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3080482812 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2187749429(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<GP_TBM_MatchTurnStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4779282_gshared (InternalEnumerator_1_t3080482812 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4779282_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3080482812 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3080482812 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4779282(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<GP_TBM_MatchTurnStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m911275493_gshared (InternalEnumerator_1_t3080482812 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m911275493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3080482812 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3080482812 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m911275493(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<GP_TBM_MatchTurnStatus>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3325508752_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3325508752_gshared (InternalEnumerator_1_t3080482812 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3325508752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3325508752_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3080482812 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3080482812 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3325508752(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2265739932_gshared (InternalEnumerator_1_t2870158877 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2265739932_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1__ctor_m2265739932(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		TableRange_t2011406615  L_0 = InternalEnumerator_1_get_Current_m2151132603((InternalEnumerator_1_t2870158877 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TableRange_t2011406615  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1050822571_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1050822571_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1050822571(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1979432532_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1979432532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1979432532(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2151132603_MetadataUsageId;
extern "C"  TableRange_t2011406615  InternalEnumerator_1_get_Current_m2151132603_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2151132603_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TableRange_t2011406615  L_8 = ((  TableRange_t2011406615  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TableRange_t2011406615  InternalEnumerator_1_get_Current_m2151132603_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2151132603(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2111763266_gshared (InternalEnumerator_1_t565169432 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2111763266_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1__ctor_m2111763266(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3847951219((InternalEnumerator_1_t565169432 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2038682075_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2038682075_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2038682075(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1182905290_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1182905290_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1182905290(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3847951219_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847951219_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3847951219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847951219_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3847951219(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1834162958_gshared (InternalEnumerator_1_t3199726719 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1834162958_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3199726719 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3199726719 *>(__this + 1);
	InternalEnumerator_1__ctor_m1834162958(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3824170762_gshared (InternalEnumerator_1_t3199726719 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3824170762_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3199726719 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3199726719 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3824170762(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2418052108_gshared (InternalEnumerator_1_t3199726719 * __this, const MethodInfo* method)
{
	{
		TagName_t2340974457  L_0 = InternalEnumerator_1_get_Current_m48002065((InternalEnumerator_1_t3199726719 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TagName_t2340974457  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2418052108_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3199726719 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3199726719 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2418052108(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1222535961_gshared (InternalEnumerator_1_t3199726719 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1222535961_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3199726719 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3199726719 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1222535961(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3920221998_gshared (InternalEnumerator_1_t3199726719 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3920221998_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3199726719 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3199726719 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3920221998(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m48002065_MetadataUsageId;
extern "C"  TagName_t2340974457  InternalEnumerator_1_get_Current_m48002065_gshared (InternalEnumerator_1_t3199726719 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m48002065_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TagName_t2340974457  L_8 = ((  TagName_t2340974457  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TagName_t2340974457  InternalEnumerator_1_get_Current_m48002065_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3199726719 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3199726719 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m48002065(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1522717567_gshared (InternalEnumerator_1_t4144585176 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1522717567_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4144585176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4144585176 *>(__this + 1);
	InternalEnumerator_1__ctor_m1522717567(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2366163887_gshared (InternalEnumerator_1_t4144585176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2366163887_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4144585176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4144585176 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2366163887(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m684868635_gshared (InternalEnumerator_1_t4144585176 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3670324128((InternalEnumerator_1_t4144585176 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m684868635_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4144585176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4144585176 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m684868635(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m655393166_gshared (InternalEnumerator_1_t4144585176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m655393166_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4144585176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4144585176 *>(__this + 1);
	InternalEnumerator_1_Dispose_m655393166(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2330916459_gshared (InternalEnumerator_1_t4144585176 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2330916459_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4144585176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4144585176 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2330916459(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3670324128_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3670324128_gshared (InternalEnumerator_1_t4144585176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3670324128_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3670324128_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4144585176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4144585176 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3670324128(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m479119441_gshared (InternalEnumerator_1_t2166579475 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m479119441_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2166579475 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2166579475 *>(__this + 1);
	InternalEnumerator_1__ctor_m479119441(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2869870821_gshared (InternalEnumerator_1_t2166579475 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2869870821_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2166579475 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2166579475 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2869870821(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1801567249_gshared (InternalEnumerator_1_t2166579475 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3816454476((InternalEnumerator_1_t2166579475 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1801567249_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2166579475 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2166579475 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1801567249(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4275179452_gshared (InternalEnumerator_1_t2166579475 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4275179452_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2166579475 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2166579475 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4275179452(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3928063661_gshared (InternalEnumerator_1_t2166579475 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3928063661_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2166579475 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2166579475 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3928063661(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3816454476_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3816454476_gshared (InternalEnumerator_1_t2166579475 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3816454476_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3816454476_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2166579475 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2166579475 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3816454476(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3831311062_gshared (InternalEnumerator_1_t2601497439 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3831311062_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2601497439 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2601497439 *>(__this + 1);
	InternalEnumerator_1__ctor_m3831311062(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4832614_gshared (InternalEnumerator_1_t2601497439 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4832614_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2601497439 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2601497439 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4832614(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810357208_gshared (InternalEnumerator_1_t2601497439 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m4208916447((InternalEnumerator_1_t2601497439 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810357208_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2601497439 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2601497439 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1810357208(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m371216459_gshared (InternalEnumerator_1_t2601497439 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m371216459_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2601497439 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2601497439 *>(__this + 1);
	InternalEnumerator_1_Dispose_m371216459(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3460721294_gshared (InternalEnumerator_1_t2601497439 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3460721294_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2601497439 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2601497439 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3460721294(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4208916447_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m4208916447_gshared (InternalEnumerator_1_t2601497439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4208916447_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m4208916447_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2601497439 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2601497439 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4208916447(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3974712790_gshared (InternalEnumerator_1_t3913814939 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3974712790_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3913814939 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3913814939 *>(__this + 1);
	InternalEnumerator_1__ctor_m3974712790(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1340860242_gshared (InternalEnumerator_1_t3913814939 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1340860242_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3913814939 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3913814939 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1340860242(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1286693498_gshared (InternalEnumerator_1_t3913814939 * __this, const MethodInfo* method)
{
	{
		TypeNameKey_t3055062677  L_0 = InternalEnumerator_1_get_Current_m3071684565((InternalEnumerator_1_t3913814939 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TypeNameKey_t3055062677  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1286693498_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3913814939 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3913814939 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1286693498(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2107590037_gshared (InternalEnumerator_1_t3913814939 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2107590037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3913814939 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3913814939 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2107590037(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2013392102_gshared (InternalEnumerator_1_t3913814939 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2013392102_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3913814939 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3913814939 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2013392102(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3071684565_MetadataUsageId;
extern "C"  TypeNameKey_t3055062677  InternalEnumerator_1_get_Current_m3071684565_gshared (InternalEnumerator_1_t3913814939 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3071684565_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TypeNameKey_t3055062677  L_8 = ((  TypeNameKey_t3055062677  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TypeNameKey_t3055062677  InternalEnumerator_1_get_Current_m3071684565_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3913814939 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3913814939 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3071684565(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2110178567_gshared (InternalEnumerator_1_t1078953194 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2110178567_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1078953194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1078953194 *>(__this + 1);
	InternalEnumerator_1__ctor_m2110178567(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m717888551_gshared (InternalEnumerator_1_t1078953194 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m717888551_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1078953194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1078953194 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m717888551(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m798649391_gshared (InternalEnumerator_1_t1078953194 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3782194712((InternalEnumerator_1_t1078953194 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m798649391_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1078953194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1078953194 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m798649391(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3389771388_gshared (InternalEnumerator_1_t1078953194 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3389771388_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1078953194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1078953194 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3389771388(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m742432835_gshared (InternalEnumerator_1_t1078953194 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m742432835_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1078953194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1078953194 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m742432835(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3782194712_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3782194712_gshared (InternalEnumerator_1_t1078953194 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3782194712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3782194712_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1078953194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1078953194 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3782194712(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2319820243_gshared (InternalEnumerator_1_t2647235048 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2319820243_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2647235048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2647235048 *>(__this + 1);
	InternalEnumerator_1__ctor_m2319820243(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m325933539_gshared (InternalEnumerator_1_t2647235048 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m325933539_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2647235048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2647235048 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m325933539(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1178970035_gshared (InternalEnumerator_1_t2647235048 * __this, const MethodInfo* method)
{
	{
		TypeConvertKey_t1788482786  L_0 = InternalEnumerator_1_get_Current_m2596744106((InternalEnumerator_1_t2647235048 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TypeConvertKey_t1788482786  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1178970035_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2647235048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2647235048 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1178970035(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1834027134_gshared (InternalEnumerator_1_t2647235048 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1834027134_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2647235048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2647235048 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1834027134(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3874674935_gshared (InternalEnumerator_1_t2647235048 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3874674935_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2647235048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2647235048 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3874674935(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2596744106_MetadataUsageId;
extern "C"  TypeConvertKey_t1788482786  InternalEnumerator_1_get_Current_m2596744106_gshared (InternalEnumerator_1_t2647235048 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2596744106_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TypeConvertKey_t1788482786  L_8 = ((  TypeConvertKey_t1788482786  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TypeConvertKey_t1788482786  InternalEnumerator_1_get_Current_m2596744106_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2647235048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2647235048 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2596744106(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1866922360_gshared (InternalEnumerator_1_t3452969744 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1866922360_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	InternalEnumerator_1__ctor_m1866922360(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	{
		ArraySegment_1_t2594217482  L_0 = InternalEnumerator_1_get_Current_m1894741129((InternalEnumerator_1_t3452969744 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ArraySegment_1_t2594217482  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m592267945_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m592267945_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	InternalEnumerator_1_Dispose_m592267945(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1460734872_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1460734872_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1460734872(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1894741129_MetadataUsageId;
extern "C"  ArraySegment_1_t2594217482  InternalEnumerator_1_get_Current_m1894741129_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1894741129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ArraySegment_1_t2594217482  L_8 = ((  ArraySegment_1_t2594217482  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ArraySegment_1_t2594217482  InternalEnumerator_1_get_Current_m1894741129_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1894741129(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4119890600_gshared (InternalEnumerator_1_t389359684 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4119890600_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1__ctor_m4119890600(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		bool L_0 = InternalEnumerator_1_get_Current_m1943362081((InternalEnumerator_1_t389359684 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1640363425_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1640363425_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1640363425(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1595676968_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1595676968_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1595676968(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1943362081_MetadataUsageId;
extern "C"  bool InternalEnumerator_1_get_Current_m1943362081_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1943362081_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		bool L_8 = ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  bool InternalEnumerator_1_get_Current_m1943362081_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1943362081(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3043733612_gshared (InternalEnumerator_1_t246889402 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3043733612_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1__ctor_m3043733612(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m4154615771((InternalEnumerator_1_t246889402 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1148506519_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1148506519_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1148506519(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2651026500_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2651026500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2651026500(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4154615771_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4154615771_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4154615771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4154615771_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4154615771(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m960275522_gshared (InternalEnumerator_1_t18266304 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m960275522_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1__ctor_m960275522(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = InternalEnumerator_1_get_Current_m2960188445((InternalEnumerator_1_t18266304 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppChar L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m811081805_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m811081805_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1_Dispose_m811081805(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m412569442_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m412569442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m412569442(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2960188445_MetadataUsageId;
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m2960188445_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2960188445_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppChar L_8 = ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m2960188445_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2960188445(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m675130983_gshared (InternalEnumerator_1_t3907627660 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m675130983_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1__ctor_m675130983(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		DictionaryEntry_t3048875398  L_0 = InternalEnumerator_1_get_Current_m2351441486((InternalEnumerator_1_t3907627660 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3597982928_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3597982928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3597982928(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1636015243_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1636015243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1636015243(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2351441486_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  InternalEnumerator_1_get_Current_m2351441486_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2351441486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DictionaryEntry_t3048875398  L_8 = ((  DictionaryEntry_t3048875398  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  InternalEnumerator_1_get_Current_m2351441486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2351441486(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2688327768_gshared (InternalEnumerator_1_t1723885533 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2688327768_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1__ctor_m2688327768(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		Link_t865133271  L_0 = InternalEnumerator_1_get_Current_m1855333455((InternalEnumerator_1_t1723885533 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t865133271  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1064404287_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1064404287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1064404287(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3585886944_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3585886944_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3585886944(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1855333455_MetadataUsageId;
extern "C"  Link_t865133271  InternalEnumerator_1_get_Current_m1855333455_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1855333455_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t865133271  L_8 = ((  Link_t865133271  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t865133271  InternalEnumerator_1_get_Current_m1855333455_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1855333455(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3708718528_gshared (InternalEnumerator_1_t4267761149 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3708718528_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4267761149 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4267761149 *>(__this + 1);
	InternalEnumerator_1__ctor_m3708718528(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963007752_gshared (InternalEnumerator_1_t4267761149 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963007752_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4267761149 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4267761149 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963007752(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3612721422_gshared (InternalEnumerator_1_t4267761149 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3409008887  L_0 = InternalEnumerator_1_get_Current_m3295893271((InternalEnumerator_1_t4267761149 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3409008887  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3612721422_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4267761149 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4267761149 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3612721422(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2496885171_gshared (InternalEnumerator_1_t4267761149 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2496885171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4267761149 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4267761149 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2496885171(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2552360808_gshared (InternalEnumerator_1_t4267761149 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2552360808_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4267761149 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4267761149 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2552360808(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3295893271_MetadataUsageId;
extern "C"  KeyValuePair_2_t3409008887  InternalEnumerator_1_get_Current_m3295893271_gshared (InternalEnumerator_1_t4267761149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3295893271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3409008887  L_8 = ((  KeyValuePair_2_t3409008887  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3409008887  InternalEnumerator_1_get_Current_m3295893271_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4267761149 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4267761149 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3295893271(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1929634852_gshared (InternalEnumerator_1_t1393666460 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1929634852_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1393666460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1393666460 *>(__this + 1);
	InternalEnumerator_1__ctor_m1929634852(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1643377252_gshared (InternalEnumerator_1_t1393666460 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1643377252_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1393666460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1393666460 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1643377252(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2927340424_gshared (InternalEnumerator_1_t1393666460 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t534914198  L_0 = InternalEnumerator_1_get_Current_m1274068803((InternalEnumerator_1_t1393666460 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t534914198  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2927340424_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1393666460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1393666460 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2927340424(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4288466531_gshared (InternalEnumerator_1_t1393666460 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4288466531_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1393666460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1393666460 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4288466531(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1668992460_gshared (InternalEnumerator_1_t1393666460 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1668992460_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1393666460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1393666460 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1668992460(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1274068803_MetadataUsageId;
extern "C"  KeyValuePair_2_t534914198  InternalEnumerator_1_get_Current_m1274068803_gshared (InternalEnumerator_1_t1393666460 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1274068803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t534914198  L_8 = ((  KeyValuePair_2_t534914198  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t534914198  InternalEnumerator_1_get_Current_m1274068803_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1393666460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1393666460 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1274068803(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3776078079_gshared (InternalEnumerator_1_t952874973 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3776078079_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t952874973 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952874973 *>(__this + 1);
	InternalEnumerator_1__ctor_m3776078079(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m544684447_gshared (InternalEnumerator_1_t952874973 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m544684447_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952874973 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952874973 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m544684447(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m246919359_gshared (InternalEnumerator_1_t952874973 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t94122711  L_0 = InternalEnumerator_1_get_Current_m2195413816((InternalEnumerator_1_t952874973 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t94122711  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m246919359_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952874973 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952874973 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m246919359(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1930060628_gshared (InternalEnumerator_1_t952874973 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1930060628_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952874973 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952874973 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1930060628(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2701085963_gshared (InternalEnumerator_1_t952874973 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2701085963_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952874973 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952874973 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2701085963(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2195413816_MetadataUsageId;
extern "C"  KeyValuePair_2_t94122711  InternalEnumerator_1_get_Current_m2195413816_gshared (InternalEnumerator_1_t952874973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2195413816_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t94122711  L_8 = ((  KeyValuePair_2_t94122711  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t94122711  InternalEnumerator_1_get_Current_m2195413816_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952874973 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952874973 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2195413816(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m421506064_gshared (InternalEnumerator_1_t768103868 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m421506064_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t768103868 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t768103868 *>(__this + 1);
	InternalEnumerator_1__ctor_m421506064(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3281551596_gshared (InternalEnumerator_1_t768103868 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3281551596_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t768103868 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t768103868 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3281551596(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4256203988_gshared (InternalEnumerator_1_t768103868 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t4204318902  L_0 = InternalEnumerator_1_get_Current_m4018610841((InternalEnumerator_1_t768103868 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t4204318902  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4256203988_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t768103868 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t768103868 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4256203988(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3968755473_gshared (InternalEnumerator_1_t768103868 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3968755473_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t768103868 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t768103868 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3968755473(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1130632672_gshared (InternalEnumerator_1_t768103868 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1130632672_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t768103868 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t768103868 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1130632672(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4018610841_MetadataUsageId;
extern "C"  KeyValuePair_2_t4204318902  InternalEnumerator_1_get_Current_m4018610841_gshared (InternalEnumerator_1_t768103868 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4018610841_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t4204318902  L_8 = ((  KeyValuePair_2_t4204318902  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t4204318902  InternalEnumerator_1_get_Current_m4018610841_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t768103868 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t768103868 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4018610841(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1812375236_gshared (InternalEnumerator_1_t974663562 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1812375236_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t974663562 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t974663562 *>(__this + 1);
	InternalEnumerator_1__ctor_m1812375236(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m878105092_gshared (InternalEnumerator_1_t974663562 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m878105092_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t974663562 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t974663562 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m878105092(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2478951816_gshared (InternalEnumerator_1_t974663562 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t115911300  L_0 = InternalEnumerator_1_get_Current_m4138513283((InternalEnumerator_1_t974663562 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t115911300  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2478951816_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t974663562 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t974663562 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2478951816(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2649412227_gshared (InternalEnumerator_1_t974663562 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2649412227_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t974663562 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t974663562 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2649412227(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2289846156_gshared (InternalEnumerator_1_t974663562 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2289846156_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t974663562 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t974663562 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2289846156(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4138513283_MetadataUsageId;
extern "C"  KeyValuePair_2_t115911300  InternalEnumerator_1_get_Current_m4138513283_gshared (InternalEnumerator_1_t974663562 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4138513283_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t115911300  L_8 = ((  KeyValuePair_2_t115911300  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t115911300  InternalEnumerator_1_get_Current_m4138513283_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t974663562 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t974663562 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4138513283(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m971224554_gshared (InternalEnumerator_1_t3990767863 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m971224554_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	InternalEnumerator_1__ctor_m971224554(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2418489778_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2418489778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2418489778(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1125095150_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3132015601  L_0 = InternalEnumerator_1_get_Current_m3903656979((InternalEnumerator_1_t3990767863 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1125095150_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1125095150(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2362056211_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2362056211_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2362056211(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m68568274_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m68568274_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m68568274(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3903656979_MetadataUsageId;
extern "C"  KeyValuePair_2_t3132015601  InternalEnumerator_1_get_Current_m3903656979_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3903656979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3132015601  L_8 = ((  KeyValuePair_2_t3132015601  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3132015601  InternalEnumerator_1_get_Current_m3903656979_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3903656979(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
