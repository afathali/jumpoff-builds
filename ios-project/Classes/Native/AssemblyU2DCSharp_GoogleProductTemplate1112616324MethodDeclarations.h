﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoogleProductTemplate
struct GoogleProductTemplate_t1112616324;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharp_AN_InAppType2513116246.h"

// System.Void GoogleProductTemplate::.ctor()
extern "C"  void GoogleProductTemplate__ctor_m1016967897 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleProductTemplate::get_originalJson()
extern "C"  String_t* GoogleProductTemplate_get_originalJson_m2274597624 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleProductTemplate::set_originalJson(System.String)
extern "C"  void GoogleProductTemplate_set_originalJson_m1716089755 (GoogleProductTemplate_t1112616324 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleProductTemplate::get_OriginalJson()
extern "C"  String_t* GoogleProductTemplate_get_OriginalJson_m2561786072 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleProductTemplate::set_OriginalJson(System.String)
extern "C"  void GoogleProductTemplate_set_OriginalJson_m3554178619 (GoogleProductTemplate_t1112616324 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GoogleProductTemplate::get_price()
extern "C"  float GoogleProductTemplate_get_price_m3284515227 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GoogleProductTemplate::get_Price()
extern "C"  float GoogleProductTemplate_get_Price_m3051684091 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GoogleProductTemplate::get_priceAmountMicros()
extern "C"  int64_t GoogleProductTemplate_get_priceAmountMicros_m3058907985 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleProductTemplate::set_priceAmountMicros(System.Int64)
extern "C"  void GoogleProductTemplate_set_priceAmountMicros_m3631247700 (GoogleProductTemplate_t1112616324 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GoogleProductTemplate::get_PriceAmountMicros()
extern "C"  int64_t GoogleProductTemplate_get_PriceAmountMicros_m858134641 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleProductTemplate::set_PriceAmountMicros(System.Int64)
extern "C"  void GoogleProductTemplate_set_PriceAmountMicros_m2962700596 (GoogleProductTemplate_t1112616324 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleProductTemplate::get_priceCurrencyCode()
extern "C"  String_t* GoogleProductTemplate_get_priceCurrencyCode_m2745530542 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleProductTemplate::set_priceCurrencyCode(System.String)
extern "C"  void GoogleProductTemplate_set_priceCurrencyCode_m3924846965 (GoogleProductTemplate_t1112616324 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleProductTemplate::get_PriceCurrencyCode()
extern "C"  String_t* GoogleProductTemplate_get_PriceCurrencyCode_m900506894 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleProductTemplate::set_PriceCurrencyCode(System.String)
extern "C"  void GoogleProductTemplate_set_PriceCurrencyCode_m1932004181 (GoogleProductTemplate_t1112616324 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleProductTemplate::get_LocalizedPrice()
extern "C"  String_t* GoogleProductTemplate_get_LocalizedPrice_m1669672751 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleProductTemplate::set_LocalizedPrice(System.String)
extern "C"  void GoogleProductTemplate_set_LocalizedPrice_m2165334654 (GoogleProductTemplate_t1112616324 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleProductTemplate::get_description()
extern "C"  String_t* GoogleProductTemplate_get_description_m2085163391 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleProductTemplate::set_description(System.String)
extern "C"  void GoogleProductTemplate_set_description_m1806109796 (GoogleProductTemplate_t1112616324 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleProductTemplate::get_Description()
extern "C"  String_t* GoogleProductTemplate_get_Description_m2781890207 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleProductTemplate::set_Description(System.String)
extern "C"  void GoogleProductTemplate_set_Description_m2375501444 (GoogleProductTemplate_t1112616324 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleProductTemplate::get_title()
extern "C"  String_t* GoogleProductTemplate_get_title_m4051738401 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleProductTemplate::set_title(System.String)
extern "C"  void GoogleProductTemplate_set_title_m1539120352 (GoogleProductTemplate_t1112616324 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleProductTemplate::get_Title()
extern "C"  String_t* GoogleProductTemplate_get_Title_m1301801857 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleProductTemplate::set_Title(System.String)
extern "C"  void GoogleProductTemplate_set_Title_m3948101952 (GoogleProductTemplate_t1112616324 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GoogleProductTemplate::get_Texture()
extern "C"  Texture2D_t3542995729 * GoogleProductTemplate_get_Texture_m2598068110 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleProductTemplate::set_Texture(UnityEngine.Texture2D)
extern "C"  void GoogleProductTemplate_set_Texture_m3712965397 (GoogleProductTemplate_t1112616324 * __this, Texture2D_t3542995729 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AN_InAppType GoogleProductTemplate::get_ProductType()
extern "C"  int32_t GoogleProductTemplate_get_ProductType_m298970098 (GoogleProductTemplate_t1112616324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleProductTemplate::set_ProductType(AN_InAppType)
extern "C"  void GoogleProductTemplate_set_ProductType_m3594267969 (GoogleProductTemplate_t1112616324 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
