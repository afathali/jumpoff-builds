﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_SoomlaProxy
struct AN_SoomlaProxy_t2595430893;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_SoomlaProxy::.ctor()
extern "C"  void AN_SoomlaProxy__ctor_m2193153734 (AN_SoomlaProxy_t2595430893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_SoomlaProxy_CallActivityFunction_m3743611639 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaProxy::Initalize(System.String,System.String)
extern "C"  void AN_SoomlaProxy_Initalize_m2477050657 (Il2CppObject * __this /* static, unused */, String_t* ___gameKey0, String_t* ___envtKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaProxy::SetBillingState(System.Boolean)
extern "C"  void AN_SoomlaProxy_SetBillingState_m3401250185 (Il2CppObject * __this /* static, unused */, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaProxy::OnMarketPurchaseStarted(System.String)
extern "C"  void AN_SoomlaProxy_OnMarketPurchaseStarted_m49363513 (Il2CppObject * __this /* static, unused */, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaProxy::OnMarketPurchaseFinished(System.String,System.Int64,System.String)
extern "C"  void AN_SoomlaProxy_OnMarketPurchaseFinished_m1937459856 (Il2CppObject * __this /* static, unused */, String_t* ___marketProductId0, int64_t ___marketPriceMicros1, String_t* ___marketCurrencyCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaProxy::OnMarketPurchaseFailed()
extern "C"  void AN_SoomlaProxy_OnMarketPurchaseFailed_m890782225 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaProxy::OnMarketPurchaseCancelled(System.String)
extern "C"  void AN_SoomlaProxy_OnMarketPurchaseCancelled_m2320176111 (Il2CppObject * __this /* static, unused */, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaProxy::OnSocialLogin(System.Int32,System.Int32)
extern "C"  void AN_SoomlaProxy_OnSocialLogin_m868871849 (Il2CppObject * __this /* static, unused */, int32_t ___eventType0, int32_t ___providerId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaProxy::OnSocialLoginFinished(System.Int32,System.String)
extern "C"  void AN_SoomlaProxy_OnSocialLoginFinished_m3112112894 (Il2CppObject * __this /* static, unused */, int32_t ___providerId0, String_t* ___profileId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaProxy::OnSocialLogout(System.Int32,System.Int32)
extern "C"  void AN_SoomlaProxy_OnSocialLogout_m2055700520 (Il2CppObject * __this /* static, unused */, int32_t ___eventType0, int32_t ___providerId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaProxy::OnSocialShare(System.Int32,System.Int32)
extern "C"  void AN_SoomlaProxy_OnSocialShare_m2682144947 (Il2CppObject * __this /* static, unused */, int32_t ___eventType0, int32_t ___providerId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_SoomlaProxy::OnFriendsRequest(System.Int32,System.Int32)
extern "C"  void AN_SoomlaProxy_OnFriendsRequest_m1997413649 (Il2CppObject * __this /* static, unused */, int32_t ___eventType0, int32_t ___providerId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
