﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>
struct Dictionary_2_t1356696369;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3043229133.h"
#include "AssemblyU2DCSharp_AN_PermissionState838201224.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m861590093_gshared (Enumerator_t3043229133 * __this, Dictionary_2_t1356696369 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m861590093(__this, ___host0, method) ((  void (*) (Enumerator_t3043229133 *, Dictionary_2_t1356696369 *, const MethodInfo*))Enumerator__ctor_m861590093_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3969925152_gshared (Enumerator_t3043229133 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3969925152(__this, method) ((  Il2CppObject * (*) (Enumerator_t3043229133 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3969925152_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1745805566_gshared (Enumerator_t3043229133 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1745805566(__this, method) ((  void (*) (Enumerator_t3043229133 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1745805566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::Dispose()
extern "C"  void Enumerator_Dispose_m182443661_gshared (Enumerator_t3043229133 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m182443661(__this, method) ((  void (*) (Enumerator_t3043229133 *, const MethodInfo*))Enumerator_Dispose_m182443661_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2821568074_gshared (Enumerator_t3043229133 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2821568074(__this, method) ((  bool (*) (Enumerator_t3043229133 *, const MethodInfo*))Enumerator_MoveNext_m2821568074_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AN_ManifestPermission,AN_PermissionState>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2621568140_gshared (Enumerator_t3043229133 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2621568140(__this, method) ((  int32_t (*) (Enumerator_t3043229133 *, const MethodInfo*))Enumerator_get_Current_m2621568140_gshared)(__this, method)
