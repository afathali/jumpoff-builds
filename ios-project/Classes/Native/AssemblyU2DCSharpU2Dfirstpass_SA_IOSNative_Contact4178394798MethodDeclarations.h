﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.IOSNative.Contacts.Contact
struct Contact_t4178394798;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.IOSNative.Contacts.Contact::.ctor()
extern "C"  void Contact__ctor_m1566119026 (Contact_t4178394798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
