﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JumpTypeCycle
struct  JumpTypeCycle_t3789588250  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text JumpTypeCycle::jump_type
	Text_t356221433 * ___jump_type_2;
	// UnityEngine.GameObject JumpTypeCycle::icon_jump1
	GameObject_t1756533147 * ___icon_jump1_3;
	// UnityEngine.GameObject JumpTypeCycle::icon_jump2
	GameObject_t1756533147 * ___icon_jump2_4;
	// UnityEngine.GameObject JumpTypeCycle::icon_waterjump
	GameObject_t1756533147 * ___icon_waterjump_5;
	// UnityEngine.GameObject JumpTypeCycle::icon_gate
	GameObject_t1756533147 * ___icon_gate_6;
	// UnityEngine.GameObject JumpTypeCycle::icon_cross
	GameObject_t1756533147 * ___icon_cross_7;
	// UnityEngine.GameObject JumpTypeCycle::icon_unicorn
	GameObject_t1756533147 * ___icon_unicorn_8;
	// System.Int32 JumpTypeCycle::type
	int32_t ___type_9;
	// System.Boolean JumpTypeCycle::inited
	bool ___inited_10;

public:
	inline static int32_t get_offset_of_jump_type_2() { return static_cast<int32_t>(offsetof(JumpTypeCycle_t3789588250, ___jump_type_2)); }
	inline Text_t356221433 * get_jump_type_2() const { return ___jump_type_2; }
	inline Text_t356221433 ** get_address_of_jump_type_2() { return &___jump_type_2; }
	inline void set_jump_type_2(Text_t356221433 * value)
	{
		___jump_type_2 = value;
		Il2CppCodeGenWriteBarrier(&___jump_type_2, value);
	}

	inline static int32_t get_offset_of_icon_jump1_3() { return static_cast<int32_t>(offsetof(JumpTypeCycle_t3789588250, ___icon_jump1_3)); }
	inline GameObject_t1756533147 * get_icon_jump1_3() const { return ___icon_jump1_3; }
	inline GameObject_t1756533147 ** get_address_of_icon_jump1_3() { return &___icon_jump1_3; }
	inline void set_icon_jump1_3(GameObject_t1756533147 * value)
	{
		___icon_jump1_3 = value;
		Il2CppCodeGenWriteBarrier(&___icon_jump1_3, value);
	}

	inline static int32_t get_offset_of_icon_jump2_4() { return static_cast<int32_t>(offsetof(JumpTypeCycle_t3789588250, ___icon_jump2_4)); }
	inline GameObject_t1756533147 * get_icon_jump2_4() const { return ___icon_jump2_4; }
	inline GameObject_t1756533147 ** get_address_of_icon_jump2_4() { return &___icon_jump2_4; }
	inline void set_icon_jump2_4(GameObject_t1756533147 * value)
	{
		___icon_jump2_4 = value;
		Il2CppCodeGenWriteBarrier(&___icon_jump2_4, value);
	}

	inline static int32_t get_offset_of_icon_waterjump_5() { return static_cast<int32_t>(offsetof(JumpTypeCycle_t3789588250, ___icon_waterjump_5)); }
	inline GameObject_t1756533147 * get_icon_waterjump_5() const { return ___icon_waterjump_5; }
	inline GameObject_t1756533147 ** get_address_of_icon_waterjump_5() { return &___icon_waterjump_5; }
	inline void set_icon_waterjump_5(GameObject_t1756533147 * value)
	{
		___icon_waterjump_5 = value;
		Il2CppCodeGenWriteBarrier(&___icon_waterjump_5, value);
	}

	inline static int32_t get_offset_of_icon_gate_6() { return static_cast<int32_t>(offsetof(JumpTypeCycle_t3789588250, ___icon_gate_6)); }
	inline GameObject_t1756533147 * get_icon_gate_6() const { return ___icon_gate_6; }
	inline GameObject_t1756533147 ** get_address_of_icon_gate_6() { return &___icon_gate_6; }
	inline void set_icon_gate_6(GameObject_t1756533147 * value)
	{
		___icon_gate_6 = value;
		Il2CppCodeGenWriteBarrier(&___icon_gate_6, value);
	}

	inline static int32_t get_offset_of_icon_cross_7() { return static_cast<int32_t>(offsetof(JumpTypeCycle_t3789588250, ___icon_cross_7)); }
	inline GameObject_t1756533147 * get_icon_cross_7() const { return ___icon_cross_7; }
	inline GameObject_t1756533147 ** get_address_of_icon_cross_7() { return &___icon_cross_7; }
	inline void set_icon_cross_7(GameObject_t1756533147 * value)
	{
		___icon_cross_7 = value;
		Il2CppCodeGenWriteBarrier(&___icon_cross_7, value);
	}

	inline static int32_t get_offset_of_icon_unicorn_8() { return static_cast<int32_t>(offsetof(JumpTypeCycle_t3789588250, ___icon_unicorn_8)); }
	inline GameObject_t1756533147 * get_icon_unicorn_8() const { return ___icon_unicorn_8; }
	inline GameObject_t1756533147 ** get_address_of_icon_unicorn_8() { return &___icon_unicorn_8; }
	inline void set_icon_unicorn_8(GameObject_t1756533147 * value)
	{
		___icon_unicorn_8 = value;
		Il2CppCodeGenWriteBarrier(&___icon_unicorn_8, value);
	}

	inline static int32_t get_offset_of_type_9() { return static_cast<int32_t>(offsetof(JumpTypeCycle_t3789588250, ___type_9)); }
	inline int32_t get_type_9() const { return ___type_9; }
	inline int32_t* get_address_of_type_9() { return &___type_9; }
	inline void set_type_9(int32_t value)
	{
		___type_9 = value;
	}

	inline static int32_t get_offset_of_inited_10() { return static_cast<int32_t>(offsetof(JumpTypeCycle_t3789588250, ___inited_10)); }
	inline bool get_inited_10() const { return ___inited_10; }
	inline bool* get_address_of_inited_10() { return &___inited_10; }
	inline void set_inited_10(bool value)
	{
		___inited_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
