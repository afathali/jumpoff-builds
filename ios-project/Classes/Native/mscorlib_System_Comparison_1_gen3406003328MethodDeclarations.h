﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3951188146MethodDeclarations.h"

// System.Void System.Comparison`1<Newtonsoft.Json.Bson.BsonReader/ContainerContext>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m1813409084(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t3406003328 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m2929820459_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<Newtonsoft.Json.Bson.BsonReader/ContainerContext>::Invoke(T,T)
#define Comparison_1_Invoke_m2906983616(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3406003328 *, ContainerContext_t2144264477 *, ContainerContext_t2144264477 *, const MethodInfo*))Comparison_1_Invoke_m2798106261_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<Newtonsoft.Json.Bson.BsonReader/ContainerContext>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m1639312669(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t3406003328 *, ContainerContext_t2144264477 *, ContainerContext_t2144264477 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m1817828810_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<Newtonsoft.Json.Bson.BsonReader/ContainerContext>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m3843123778(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t3406003328 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m1056665895_gshared)(__this, ___result0, method)
