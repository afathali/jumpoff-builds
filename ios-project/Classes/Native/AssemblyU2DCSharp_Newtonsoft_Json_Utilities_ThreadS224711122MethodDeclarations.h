﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread4023353063MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Reflection.ICustomAttributeProvider,System.Runtime.Serialization.DataMemberAttribute>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m1122223845(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t224711122 *, Func_2_t3321829536 *, const MethodInfo*))ThreadSafeStore_2__ctor_m2716591129_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Reflection.ICustomAttributeProvider,System.Runtime.Serialization.DataMemberAttribute>::Get(TKey)
#define ThreadSafeStore_2_Get_m3447115993(__this, ___key0, method) ((  DataMemberAttribute_t2677019114 * (*) (ThreadSafeStore_2_t224711122 *, Il2CppObject *, const MethodInfo*))ThreadSafeStore_2_Get_m1768706765_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Reflection.ICustomAttributeProvider,System.Runtime.Serialization.DataMemberAttribute>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m66873973(__this, ___key0, method) ((  DataMemberAttribute_t2677019114 * (*) (ThreadSafeStore_2_t224711122 *, Il2CppObject *, const MethodInfo*))ThreadSafeStore_2_AddValue_m4069531929_gshared)(__this, ___key0, method)
