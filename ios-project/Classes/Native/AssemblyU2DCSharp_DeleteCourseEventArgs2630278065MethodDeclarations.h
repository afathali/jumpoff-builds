﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeleteCourseEventArgs
struct DeleteCourseEventArgs_t2630278065;

#include "codegen/il2cpp-codegen.h"

// System.Void DeleteCourseEventArgs::.ctor()
extern "C"  void DeleteCourseEventArgs__ctor_m803501594 (DeleteCourseEventArgs_t2630278065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 DeleteCourseEventArgs::get_Id()
extern "C"  int64_t DeleteCourseEventArgs_get_Id_m2065760413 (DeleteCourseEventArgs_t2630278065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeleteCourseEventArgs::set_Id(System.Int64)
extern "C"  void DeleteCourseEventArgs_set_Id_m2537187740 (DeleteCourseEventArgs_t2630278065 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
