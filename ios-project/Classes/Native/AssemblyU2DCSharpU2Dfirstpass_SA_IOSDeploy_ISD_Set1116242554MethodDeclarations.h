﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.IOSDeploy.ISD_Settings
struct ISD_Settings_t1116242554;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SA.IOSDeploy.ISD_Settings::.ctor()
extern "C"  void ISD_Settings__ctor_m3521198905 (ISD_Settings_t1116242554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SA.IOSDeploy.ISD_Settings SA.IOSDeploy.ISD_Settings::get_Instance()
extern "C"  ISD_Settings_t1116242554 * ISD_Settings_get_Instance_m2979124640 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA.IOSDeploy.ISD_Settings::ContainsFreamworkWithName(System.String)
extern "C"  bool ISD_Settings_ContainsFreamworkWithName_m3500278981 (ISD_Settings_t1116242554 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA.IOSDeploy.ISD_Settings::ContainsPlistVarkWithName(System.String)
extern "C"  bool ISD_Settings_ContainsPlistVarkWithName_m3915340737 (ISD_Settings_t1116242554 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA.IOSDeploy.ISD_Settings::ContainsLibWithName(System.String)
extern "C"  bool ISD_Settings_ContainsLibWithName_m4074634112 (ISD_Settings_t1116242554 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
