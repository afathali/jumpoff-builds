﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2350156707(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3253614450 *, Dictionary_2_t1933589748 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2823000322(__this, method) ((  Il2CppObject * (*) (Enumerator_t3253614450 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3457982086(__this, method) ((  void (*) (Enumerator_t3253614450 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m643021857(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3253614450 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2019137628(__this, method) ((  Il2CppObject * (*) (Enumerator_t3253614450 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1694309078(__this, method) ((  Il2CppObject * (*) (Enumerator_t3253614450 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::MoveNext()
#define Enumerator_MoveNext_m3571370334(__this, method) ((  bool (*) (Enumerator_t3253614450 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::get_Current()
#define Enumerator_get_Current_m1995009302(__this, method) ((  KeyValuePair_2_t3985902266  (*) (Enumerator_t3253614450 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2432761807(__this, method) ((  int32_t (*) (Enumerator_t3253614450 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2190081391(__this, method) ((  SampleBase_t2925764113 * (*) (Enumerator_t3253614450 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::Reset()
#define Enumerator_Reset_m1331653953(__this, method) ((  void (*) (Enumerator_t3253614450 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::VerifyState()
#define Enumerator_VerifyState_m4014255978(__this, method) ((  void (*) (Enumerator_t3253614450 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2719159030(__this, method) ((  void (*) (Enumerator_t3253614450 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase>::Dispose()
#define Enumerator_Dispose_m1670262183(__this, method) ((  void (*) (Enumerator_t3253614450 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
