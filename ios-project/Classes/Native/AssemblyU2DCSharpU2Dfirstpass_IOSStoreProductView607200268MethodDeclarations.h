﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSStoreProductView
struct IOSStoreProductView_t607200268;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Action
struct Action_t3226471752;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"

// System.Void IOSStoreProductView::.ctor()
extern "C"  void IOSStoreProductView__ctor_m500836157 (IOSStoreProductView_t607200268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::.ctor(System.String[])
extern "C"  void IOSStoreProductView__ctor_m2037600361 (IOSStoreProductView_t607200268 * __this, StringU5BU5D_t1642385972* ___ids0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::add_Loaded(System.Action)
extern "C"  void IOSStoreProductView_add_Loaded_m3935276069 (IOSStoreProductView_t607200268 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::remove_Loaded(System.Action)
extern "C"  void IOSStoreProductView_remove_Loaded_m662521056 (IOSStoreProductView_t607200268 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::add_LoadFailed(System.Action)
extern "C"  void IOSStoreProductView_add_LoadFailed_m4233788599 (IOSStoreProductView_t607200268 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::remove_LoadFailed(System.Action)
extern "C"  void IOSStoreProductView_remove_LoadFailed_m3676307140 (IOSStoreProductView_t607200268 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::add_Appeared(System.Action)
extern "C"  void IOSStoreProductView_add_Appeared_m346951664 (IOSStoreProductView_t607200268 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::remove_Appeared(System.Action)
extern "C"  void IOSStoreProductView_remove_Appeared_m3434602935 (IOSStoreProductView_t607200268 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::add_Dismissed(System.Action)
extern "C"  void IOSStoreProductView_add_Dismissed_m3408188093 (IOSStoreProductView_t607200268 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::remove_Dismissed(System.Action)
extern "C"  void IOSStoreProductView_remove_Dismissed_m4184138118 (IOSStoreProductView_t607200268 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::addProductId(System.String)
extern "C"  void IOSStoreProductView_addProductId_m2902230572 (IOSStoreProductView_t607200268 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::Load()
extern "C"  void IOSStoreProductView_Load_m917880271 (IOSStoreProductView_t607200268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::Show()
extern "C"  void IOSStoreProductView_Show_m727997680 (IOSStoreProductView_t607200268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IOSStoreProductView::get_id()
extern "C"  int32_t IOSStoreProductView_get_id_m878159243 (IOSStoreProductView_t607200268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::OnProductViewAppeard()
extern "C"  void IOSStoreProductView_OnProductViewAppeard_m2894385297 (IOSStoreProductView_t607200268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::OnProductViewDismissed()
extern "C"  void IOSStoreProductView_OnProductViewDismissed_m242393957 (IOSStoreProductView_t607200268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::OnContentLoaded()
extern "C"  void IOSStoreProductView_OnContentLoaded_m2732065046 (IOSStoreProductView_t607200268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::OnContentLoadFailed()
extern "C"  void IOSStoreProductView_OnContentLoadFailed_m347414106 (IOSStoreProductView_t607200268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::SetId(System.Int32)
extern "C"  void IOSStoreProductView_SetId_m463628323 (IOSStoreProductView_t607200268 * __this, int32_t ___viewId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::<Loaded>m__51()
extern "C"  void IOSStoreProductView_U3CLoadedU3Em__51_m384656181 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::<LoadFailed>m__52()
extern "C"  void IOSStoreProductView_U3CLoadFailedU3Em__52_m2114373568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::<Appeared>m__53()
extern "C"  void IOSStoreProductView_U3CAppearedU3Em__53_m759043182 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreProductView::<Dismissed>m__54()
extern "C"  void IOSStoreProductView_U3CDismissedU3Em__54_m1994022718 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
