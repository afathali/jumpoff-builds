﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Camera[]
struct CameraU5BU5D_t3079764780;
// UnityEngine.Camera
struct Camera_t189460977;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15
struct  U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400  : public Il2CppObject
{
public:
	// UnityEngine.RenderTexture ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::<rt>__0
	RenderTexture_t2666733923 * ___U3CrtU3E__0_0;
	// UnityEngine.Texture2D ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::<screenShot>__1
	Texture2D_t3542995729 * ___U3CscreenShotU3E__1_1;
	// UnityEngine.Camera[] ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::<$s_300>__2
	CameraU5BU5D_t3079764780* ___U3CU24s_300U3E__2_2;
	// System.Int32 ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::<$s_301>__3
	int32_t ___U3CU24s_301U3E__3_3;
	// UnityEngine.Camera ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::<cam>__4
	Camera_t189460977 * ___U3CcamU3E__4_4;
	// System.Byte[] ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::<bytes>__5
	ByteU5BU5D_t3397334013* ___U3CbytesU3E__5_5;
	// System.String ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::filePath
	String_t* ___filePath_6;
	// System.Int32 ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::$PC
	int32_t ___U24PC_7;
	// System.Object ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::$current
	Il2CppObject * ___U24current_8;
	// System.String ScreenCapture/<SaveScreenshot_RenderToTexAsynch>c__Iterator15::<$>filePath
	String_t* ___U3CU24U3EfilePath_9;

public:
	inline static int32_t get_offset_of_U3CrtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400, ___U3CrtU3E__0_0)); }
	inline RenderTexture_t2666733923 * get_U3CrtU3E__0_0() const { return ___U3CrtU3E__0_0; }
	inline RenderTexture_t2666733923 ** get_address_of_U3CrtU3E__0_0() { return &___U3CrtU3E__0_0; }
	inline void set_U3CrtU3E__0_0(RenderTexture_t2666733923 * value)
	{
		___U3CrtU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrtU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CscreenShotU3E__1_1() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400, ___U3CscreenShotU3E__1_1)); }
	inline Texture2D_t3542995729 * get_U3CscreenShotU3E__1_1() const { return ___U3CscreenShotU3E__1_1; }
	inline Texture2D_t3542995729 ** get_address_of_U3CscreenShotU3E__1_1() { return &___U3CscreenShotU3E__1_1; }
	inline void set_U3CscreenShotU3E__1_1(Texture2D_t3542995729 * value)
	{
		___U3CscreenShotU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CscreenShotU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_300U3E__2_2() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400, ___U3CU24s_300U3E__2_2)); }
	inline CameraU5BU5D_t3079764780* get_U3CU24s_300U3E__2_2() const { return ___U3CU24s_300U3E__2_2; }
	inline CameraU5BU5D_t3079764780** get_address_of_U3CU24s_300U3E__2_2() { return &___U3CU24s_300U3E__2_2; }
	inline void set_U3CU24s_300U3E__2_2(CameraU5BU5D_t3079764780* value)
	{
		___U3CU24s_300U3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_300U3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_301U3E__3_3() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400, ___U3CU24s_301U3E__3_3)); }
	inline int32_t get_U3CU24s_301U3E__3_3() const { return ___U3CU24s_301U3E__3_3; }
	inline int32_t* get_address_of_U3CU24s_301U3E__3_3() { return &___U3CU24s_301U3E__3_3; }
	inline void set_U3CU24s_301U3E__3_3(int32_t value)
	{
		___U3CU24s_301U3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CcamU3E__4_4() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400, ___U3CcamU3E__4_4)); }
	inline Camera_t189460977 * get_U3CcamU3E__4_4() const { return ___U3CcamU3E__4_4; }
	inline Camera_t189460977 ** get_address_of_U3CcamU3E__4_4() { return &___U3CcamU3E__4_4; }
	inline void set_U3CcamU3E__4_4(Camera_t189460977 * value)
	{
		___U3CcamU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcamU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__5_5() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400, ___U3CbytesU3E__5_5)); }
	inline ByteU5BU5D_t3397334013* get_U3CbytesU3E__5_5() const { return ___U3CbytesU3E__5_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CbytesU3E__5_5() { return &___U3CbytesU3E__5_5; }
	inline void set_U3CbytesU3E__5_5(ByteU5BU5D_t3397334013* value)
	{
		___U3CbytesU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbytesU3E__5_5, value);
	}

	inline static int32_t get_offset_of_filePath_6() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400, ___filePath_6)); }
	inline String_t* get_filePath_6() const { return ___filePath_6; }
	inline String_t** get_address_of_filePath_6() { return &___filePath_6; }
	inline void set_filePath_6(String_t* value)
	{
		___filePath_6 = value;
		Il2CppCodeGenWriteBarrier(&___filePath_6, value);
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EfilePath_9() { return static_cast<int32_t>(offsetof(U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400, ___U3CU24U3EfilePath_9)); }
	inline String_t* get_U3CU24U3EfilePath_9() const { return ___U3CU24U3EfilePath_9; }
	inline String_t** get_address_of_U3CU24U3EfilePath_9() { return &___U3CU24U3EfilePath_9; }
	inline void set_U3CU24U3EfilePath_9(String_t* value)
	{
		___U3CU24U3EfilePath_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EfilePath_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
