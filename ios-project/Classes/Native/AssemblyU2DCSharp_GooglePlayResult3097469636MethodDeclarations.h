﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayResult
struct GooglePlayResult_t3097469636;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GP_GamesStatusCodes1013506173.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GooglePlayResult::.ctor(GP_GamesStatusCodes)
extern "C"  void GooglePlayResult__ctor_m1709556668 (GooglePlayResult_t3097469636 * __this, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayResult::.ctor(System.String)
extern "C"  void GooglePlayResult__ctor_m2796005021 (GooglePlayResult_t3097469636 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayResult::SetCode(GP_GamesStatusCodes)
extern "C"  void GooglePlayResult_SetCode_m3052159663 (GooglePlayResult_t3097469636 * __this, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_GamesStatusCodes GooglePlayResult::get_response()
extern "C"  int32_t GooglePlayResult_get_response_m1568574829 (GooglePlayResult_t3097469636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_GamesStatusCodes GooglePlayResult::get_Response()
extern "C"  int32_t GooglePlayResult_get_Response_m1613491853 (GooglePlayResult_t3097469636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayResult::get_message()
extern "C"  String_t* GooglePlayResult_get_message_m3841330144 (GooglePlayResult_t3097469636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayResult::get_Message()
extern "C"  String_t* GooglePlayResult_get_Message_m3805604544 (GooglePlayResult_t3097469636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayResult::get_isSuccess()
extern "C"  bool GooglePlayResult_get_isSuccess_m3392971659 (GooglePlayResult_t3097469636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayResult::get_IsSucceeded()
extern "C"  bool GooglePlayResult_get_IsSucceeded_m2606691471 (GooglePlayResult_t3097469636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayResult::get_isFailure()
extern "C"  bool GooglePlayResult_get_isFailure_m3276486004 (GooglePlayResult_t3097469636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayResult::get_IsFailed()
extern "C"  bool GooglePlayResult_get_IsFailed_m3392269351 (GooglePlayResult_t3097469636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
