﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator3
struct U3CPostScreenshotInstagramU3Ec__Iterator3_t3079098568;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator3::.ctor()
extern "C"  void U3CPostScreenshotInstagramU3Ec__Iterator3__ctor_m1557599923 (U3CPostScreenshotInstagramU3Ec__Iterator3_t3079098568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPostScreenshotInstagramU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1816908945 (U3CPostScreenshotInstagramU3Ec__Iterator3_t3079098568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPostScreenshotInstagramU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3642665129 (U3CPostScreenshotInstagramU3Ec__Iterator3_t3079098568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator3::MoveNext()
extern "C"  bool U3CPostScreenshotInstagramU3Ec__Iterator3_MoveNext_m1404166357 (U3CPostScreenshotInstagramU3Ec__Iterator3_t3079098568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator3::Dispose()
extern "C"  void U3CPostScreenshotInstagramU3Ec__Iterator3_Dispose_m3888832208 (U3CPostScreenshotInstagramU3Ec__Iterator3_t3079098568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSocialUseExample/<PostScreenshotInstagram>c__Iterator3::Reset()
extern "C"  void U3CPostScreenshotInstagramU3Ec__Iterator3_Reset_m3290691166 (U3CPostScreenshotInstagramU3Ec__Iterator3_t3079098568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
