﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct Dictionary_2_t2358566078;
// System.Collections.Generic.IEqualityComparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEqualityComparer_1_t2344648379;
// System.Collections.Generic.IDictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct IDictionary_2_t357649499;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct ICollection_1_t4084090906;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3023952753;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>[]
struct KeyValuePair_2U5BU5D_t433678061;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>
struct IEnumerator_1_t1886402423;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct KeyCollection_t547096553;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct ValueCollection_t1061625921;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_115911300.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3678590780.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor()
extern "C"  void Dictionary_2__ctor_m4046151498_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m4046151498(__this, method) ((  void (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2__ctor_m4046151498_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2366865588_gshared (Dictionary_2_t2358566078 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2366865588(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2358566078 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2366865588_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m2854231785_gshared (Dictionary_2_t2358566078 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m2854231785(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2358566078 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2854231785_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3086728932_gshared (Dictionary_2_t2358566078 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3086728932(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2358566078 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3086728932_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m256185396_gshared (Dictionary_2_t2358566078 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m256185396(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2358566078 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m256185396_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1299638065_gshared (Dictionary_2_t2358566078 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1299638065(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t2358566078 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1299638065_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2558740794_gshared (Dictionary_2_t2358566078 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2558740794(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2358566078 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m2558740794_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3083392263_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3083392263(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3083392263_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m745069551_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m745069551(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m745069551_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1698677217_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1698677217(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1698677217_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m1370855641_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1370855641(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1370855641_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2929008048_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2929008048(__this, method) ((  bool (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2929008048_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m4084863117_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m4084863117(__this, method) ((  bool (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m4084863117_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1636690091_gshared (Dictionary_2_t2358566078 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1636690091(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2358566078 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1636690091_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m591513390_gshared (Dictionary_2_t2358566078 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m591513390(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2358566078 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m591513390_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3952097553_gshared (Dictionary_2_t2358566078 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3952097553(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2358566078 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3952097553_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2652542581_gshared (Dictionary_2_t2358566078 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2652542581(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2358566078 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2652542581_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3725125474_gshared (Dictionary_2_t2358566078 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3725125474(__this, ___key0, method) ((  void (*) (Dictionary_2_t2358566078 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3725125474_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4067603135_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4067603135(__this, method) ((  bool (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4067603135_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m924378663_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m924378663(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m924378663_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3206735177_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3206735177(__this, method) ((  bool (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3206735177_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m729884384_gshared (Dictionary_2_t2358566078 * __this, KeyValuePair_2_t115911300  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m729884384(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2358566078 *, KeyValuePair_2_t115911300 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m729884384_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4089188492_gshared (Dictionary_2_t2358566078 * __this, KeyValuePair_2_t115911300  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4089188492(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2358566078 *, KeyValuePair_2_t115911300 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4089188492_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1779746572_gshared (Dictionary_2_t2358566078 * __this, KeyValuePair_2U5BU5D_t433678061* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1779746572(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2358566078 *, KeyValuePair_2U5BU5D_t433678061*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1779746572_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2764367171_gshared (Dictionary_2_t2358566078 * __this, KeyValuePair_2_t115911300  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2764367171(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2358566078 *, KeyValuePair_2_t115911300 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2764367171_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3278355047_gshared (Dictionary_2_t2358566078 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3278355047(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2358566078 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3278355047_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2087158946_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2087158946(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2087158946_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4275037729_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4275037729(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4275037729_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2520419580_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2520419580(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2520419580_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2336126303_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2336126303(__this, method) ((  int32_t (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_get_Count_m2336126303_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m2878787972_gshared (Dictionary_2_t2358566078 * __this, KeyValuePair_2_t3132015601  ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2878787972(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2358566078 *, KeyValuePair_2_t3132015601 , const MethodInfo*))Dictionary_2_get_Item_m2878787972_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3761844787_gshared (Dictionary_2_t2358566078 * __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3761844787(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2358566078 *, KeyValuePair_2_t3132015601 , int32_t, const MethodInfo*))Dictionary_2_set_Item_m3761844787_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3363797131_gshared (Dictionary_2_t2358566078 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3363797131(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2358566078 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3363797131_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m1990682208_gshared (Dictionary_2_t2358566078 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1990682208(__this, ___size0, method) ((  void (*) (Dictionary_2_t2358566078 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1990682208_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3743592074_gshared (Dictionary_2_t2358566078 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3743592074(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2358566078 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3743592074_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t115911300  Dictionary_2_make_pair_m3478954280_gshared (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3478954280(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t115911300  (*) (Il2CppObject * /* static, unused */, KeyValuePair_2_t3132015601 , int32_t, const MethodInfo*))Dictionary_2_make_pair_m3478954280_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::pick_key(TKey,TValue)
extern "C"  KeyValuePair_2_t3132015601  Dictionary_2_pick_key_m2780124350_gshared (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2780124350(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3132015601  (*) (Il2CppObject * /* static, unused */, KeyValuePair_2_t3132015601 , int32_t, const MethodInfo*))Dictionary_2_pick_key_m2780124350_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m2569877646_gshared (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2569877646(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, KeyValuePair_2_t3132015601 , int32_t, const MethodInfo*))Dictionary_2_pick_value_m2569877646_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3470015999_gshared (Dictionary_2_t2358566078 * __this, KeyValuePair_2U5BU5D_t433678061* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3470015999(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2358566078 *, KeyValuePair_2U5BU5D_t433678061*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3470015999_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Resize()
extern "C"  void Dictionary_2_Resize_m3601438969_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3601438969(__this, method) ((  void (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_Resize_m3601438969_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3969741058_gshared (Dictionary_2_t2358566078 * __this, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3969741058(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2358566078 *, KeyValuePair_2_t3132015601 , int32_t, const MethodInfo*))Dictionary_2_Add_m3969741058_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Clear()
extern "C"  void Dictionary_2_Clear_m686931088_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m686931088(__this, method) ((  void (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_Clear_m686931088_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m743148461_gshared (Dictionary_2_t2358566078 * __this, KeyValuePair_2_t3132015601  ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m743148461(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2358566078 *, KeyValuePair_2_t3132015601 , const MethodInfo*))Dictionary_2_ContainsKey_m743148461_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2321434028_gshared (Dictionary_2_t2358566078 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2321434028(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2358566078 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m2321434028_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1554772755_gshared (Dictionary_2_t2358566078 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1554772755(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2358566078 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m1554772755_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m677407043_gshared (Dictionary_2_t2358566078 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m677407043(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2358566078 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m677407043_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m4154866132_gshared (Dictionary_2_t2358566078 * __this, KeyValuePair_2_t3132015601  ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m4154866132(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2358566078 *, KeyValuePair_2_t3132015601 , const MethodInfo*))Dictionary_2_Remove_m4154866132_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m490837563_gshared (Dictionary_2_t2358566078 * __this, KeyValuePair_2_t3132015601  ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m490837563(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2358566078 *, KeyValuePair_2_t3132015601 , int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m490837563_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Keys()
extern "C"  KeyCollection_t547096553 * Dictionary_2_get_Keys_m2870602320_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2870602320(__this, method) ((  KeyCollection_t547096553 * (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_get_Keys_m2870602320_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Values()
extern "C"  ValueCollection_t1061625921 * Dictionary_2_get_Values_m3422651928_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3422651928(__this, method) ((  ValueCollection_t1061625921 * (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_get_Values_m3422651928_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::ToTKey(System.Object)
extern "C"  KeyValuePair_2_t3132015601  Dictionary_2_ToTKey_m1222910257_gshared (Dictionary_2_t2358566078 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1222910257(__this, ___key0, method) ((  KeyValuePair_2_t3132015601  (*) (Dictionary_2_t2358566078 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1222910257_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m2073438553_gshared (Dictionary_2_t2358566078 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m2073438553(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t2358566078 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2073438553_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1943976579_gshared (Dictionary_2_t2358566078 * __this, KeyValuePair_2_t115911300  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1943976579(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2358566078 *, KeyValuePair_2_t115911300 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1943976579_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t3678590780  Dictionary_2_GetEnumerator_m2980351304_gshared (Dictionary_2_t2358566078 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2980351304(__this, method) ((  Enumerator_t3678590780  (*) (Dictionary_2_t2358566078 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2980351304_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m3852782305_gshared (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t3132015601  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3852782305(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, KeyValuePair_2_t3132015601 , int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3852782305_gshared)(__this /* static, unused */, ___key0, ___value1, method)
