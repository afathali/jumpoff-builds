﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoogleCloudManager
struct GoogleCloudManager_t1208504825;
// System.Action`1<GoogleCloudResult>
struct Action_1_t545428089;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]>
struct Dictionary_2_t2405159648;
// GoogleCloudResult
struct GoogleCloudResult_t743628707;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GoogleCloudResult743628707.h"

// System.Void GoogleCloudManager::.ctor()
extern "C"  void GoogleCloudManager__ctor_m3003314958 (GoogleCloudManager_t1208504825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::.cctor()
extern "C"  void GoogleCloudManager__cctor_m157808523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::add_ActionStateDeleted(System.Action`1<GoogleCloudResult>)
extern "C"  void GoogleCloudManager_add_ActionStateDeleted_m1561596485 (Il2CppObject * __this /* static, unused */, Action_1_t545428089 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::remove_ActionStateDeleted(System.Action`1<GoogleCloudResult>)
extern "C"  void GoogleCloudManager_remove_ActionStateDeleted_m2248311216 (Il2CppObject * __this /* static, unused */, Action_1_t545428089 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::add_ActionStateUpdated(System.Action`1<GoogleCloudResult>)
extern "C"  void GoogleCloudManager_add_ActionStateUpdated_m3178280937 (Il2CppObject * __this /* static, unused */, Action_1_t545428089 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::remove_ActionStateUpdated(System.Action`1<GoogleCloudResult>)
extern "C"  void GoogleCloudManager_remove_ActionStateUpdated_m2741240370 (Il2CppObject * __this /* static, unused */, Action_1_t545428089 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::add_ActionStateLoaded(System.Action`1<GoogleCloudResult>)
extern "C"  void GoogleCloudManager_add_ActionStateLoaded_m1194052291 (Il2CppObject * __this /* static, unused */, Action_1_t545428089 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::remove_ActionStateLoaded(System.Action`1<GoogleCloudResult>)
extern "C"  void GoogleCloudManager_remove_ActionStateLoaded_m4143875694 (Il2CppObject * __this /* static, unused */, Action_1_t545428089 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::add_ActionStateResolved(System.Action`1<GoogleCloudResult>)
extern "C"  void GoogleCloudManager_add_ActionStateResolved_m1990063542 (Il2CppObject * __this /* static, unused */, Action_1_t545428089 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::remove_ActionStateResolved(System.Action`1<GoogleCloudResult>)
extern "C"  void GoogleCloudManager_remove_ActionStateResolved_m388320759 (Il2CppObject * __this /* static, unused */, Action_1_t545428089 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::add_ActionStateConflict(System.Action`1<GoogleCloudResult>)
extern "C"  void GoogleCloudManager_add_ActionStateConflict_m897818076 (Il2CppObject * __this /* static, unused */, Action_1_t545428089 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::remove_ActionStateConflict(System.Action`1<GoogleCloudResult>)
extern "C"  void GoogleCloudManager_remove_ActionStateConflict_m757093061 (Il2CppObject * __this /* static, unused */, Action_1_t545428089 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::add_ActionAllStatesLoaded(System.Action`1<GoogleCloudResult>)
extern "C"  void GoogleCloudManager_add_ActionAllStatesLoaded_m1860513561 (Il2CppObject * __this /* static, unused */, Action_1_t545428089 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::remove_ActionAllStatesLoaded(System.Action`1<GoogleCloudResult>)
extern "C"  void GoogleCloudManager_remove_ActionAllStatesLoaded_m1590762234 (Il2CppObject * __this /* static, unused */, Action_1_t545428089 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::Awake()
extern "C"  void GoogleCloudManager_Awake_m1112844041 (GoogleCloudManager_t1208504825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::Create()
extern "C"  void GoogleCloudManager_Create_m1325931676 (GoogleCloudManager_t1208504825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::loadAllStates()
extern "C"  void GoogleCloudManager_loadAllStates_m860503191 (GoogleCloudManager_t1208504825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::updateState(System.Int32,System.Byte[])
extern "C"  void GoogleCloudManager_updateState_m4187244392 (GoogleCloudManager_t1208504825 * __this, int32_t ___stateKey0, ByteU5BU5D_t3397334013* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::resolveState(System.Int32,System.Byte[],System.String)
extern "C"  void GoogleCloudManager_resolveState_m2862801543 (GoogleCloudManager_t1208504825 * __this, int32_t ___stateKey0, ByteU5BU5D_t3397334013* ___resolvedData1, String_t* ___resolvedVersion2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::deleteState(System.Int32)
extern "C"  void GoogleCloudManager_deleteState_m2796827683 (GoogleCloudManager_t1208504825 * __this, int32_t ___stateKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::loadState(System.Int32)
extern "C"  void GoogleCloudManager_loadState_m364569898 (GoogleCloudManager_t1208504825 * __this, int32_t ___stateKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GoogleCloudManager::GetStateData(System.Int32)
extern "C"  ByteU5BU5D_t3397334013* GoogleCloudManager_GetStateData_m3811594000 (GoogleCloudManager_t1208504825 * __this, int32_t ___stateKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GoogleCloudManager::get_maxStateSize()
extern "C"  int32_t GoogleCloudManager_get_maxStateSize_m1397726121 (GoogleCloudManager_t1208504825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GoogleCloudManager::get_maxNumKeys()
extern "C"  int32_t GoogleCloudManager_get_maxNumKeys_m656255637 (GoogleCloudManager_t1208504825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]> GoogleCloudManager::get_states()
extern "C"  Dictionary_2_t2405159648 * GoogleCloudManager_get_states_m1029132531 (GoogleCloudManager_t1208504825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::OnAllStatesLoaded(System.String)
extern "C"  void GoogleCloudManager_OnAllStatesLoaded_m2434237509 (GoogleCloudManager_t1208504825 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::OnStateConflict(System.String)
extern "C"  void GoogleCloudManager_OnStateConflict_m2834407596 (GoogleCloudManager_t1208504825 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::OnStateLoaded(System.String)
extern "C"  void GoogleCloudManager_OnStateLoaded_m2713812787 (GoogleCloudManager_t1208504825 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::OnStateResolved(System.String)
extern "C"  void GoogleCloudManager_OnStateResolved_m1229077442 (GoogleCloudManager_t1208504825 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::OnStateUpdated(System.String)
extern "C"  void GoogleCloudManager_OnStateUpdated_m3430422025 (GoogleCloudManager_t1208504825 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::OnKeyDeleted(System.String)
extern "C"  void GoogleCloudManager_OnKeyDeleted_m699031649 (GoogleCloudManager_t1208504825 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::OnCloudConnected(System.String)
extern "C"  void GoogleCloudManager_OnCloudConnected_m3629112021 (GoogleCloudManager_t1208504825 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::PushStateData(System.String,System.Byte[])
extern "C"  void GoogleCloudManager_PushStateData_m362507130 (GoogleCloudManager_t1208504825 * __this, String_t* ___stateKey0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::PushStateData(System.Int32,System.Byte[])
extern "C"  void GoogleCloudManager_PushStateData_m924816799 (GoogleCloudManager_t1208504825 * __this, int32_t ___stateKey0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GoogleCloudManager::ConvertStringToCloudData(System.String)
extern "C"  ByteU5BU5D_t3397334013* GoogleCloudManager_ConvertStringToCloudData_m3250215638 (Il2CppObject * __this /* static, unused */, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::<ActionStateDeleted>m__2C(GoogleCloudResult)
extern "C"  void GoogleCloudManager_U3CActionStateDeletedU3Em__2C_m2878026755 (Il2CppObject * __this /* static, unused */, GoogleCloudResult_t743628707 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::<ActionStateUpdated>m__2D(GoogleCloudResult)
extern "C"  void GoogleCloudManager_U3CActionStateUpdatedU3Em__2D_m2330325186 (Il2CppObject * __this /* static, unused */, GoogleCloudResult_t743628707 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::<ActionStateLoaded>m__2E(GoogleCloudResult)
extern "C"  void GoogleCloudManager_U3CActionStateLoadedU3Em__2E_m3718533319 (Il2CppObject * __this /* static, unused */, GoogleCloudResult_t743628707 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::<ActionStateResolved>m__2F(GoogleCloudResult)
extern "C"  void GoogleCloudManager_U3CActionStateResolvedU3Em__2F_m1466512643 (Il2CppObject * __this /* static, unused */, GoogleCloudResult_t743628707 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::<ActionStateConflict>m__30(GoogleCloudResult)
extern "C"  void GoogleCloudManager_U3CActionStateConflictU3Em__30_m3932927946 (Il2CppObject * __this /* static, unused */, GoogleCloudResult_t743628707 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudManager::<ActionAllStatesLoaded>m__31(GoogleCloudResult)
extern "C"  void GoogleCloudManager_U3CActionAllStatesLoadedU3Em__31_m3285858682 (Il2CppObject * __this /* static, unused */, GoogleCloudResult_t743628707 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
