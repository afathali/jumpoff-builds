﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<FB_LikeInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3725475465(__this, ___l0, method) ((  void (*) (Enumerator_t2117049884 *, List_1_t2582320210 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FB_LikeInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2766203225(__this, method) ((  void (*) (Enumerator_t2117049884 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<FB_LikeInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3848992313(__this, method) ((  Il2CppObject * (*) (Enumerator_t2117049884 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FB_LikeInfo>::Dispose()
#define Enumerator_Dispose_m1444435014(__this, method) ((  void (*) (Enumerator_t2117049884 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FB_LikeInfo>::VerifyState()
#define Enumerator_VerifyState_m408637319(__this, method) ((  void (*) (Enumerator_t2117049884 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<FB_LikeInfo>::MoveNext()
#define Enumerator_MoveNext_m2126347385(__this, method) ((  bool (*) (Enumerator_t2117049884 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<FB_LikeInfo>::get_Current()
#define Enumerator_get_Current_m1019471892(__this, method) ((  FB_LikeInfo_t3213199078 * (*) (Enumerator_t2117049884 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
