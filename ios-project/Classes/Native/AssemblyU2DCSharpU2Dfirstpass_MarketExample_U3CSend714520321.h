﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Object
struct Il2CppObject;
// MarketExample
struct MarketExample_t1603367874;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketExample/<SendRequest>c__Iterator2
struct  U3CSendRequestU3Ec__Iterator2_t714520321  : public Il2CppObject
{
public:
	// System.String MarketExample/<SendRequest>c__Iterator2::<base64string>__0
	String_t* ___U3Cbase64stringU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> MarketExample/<SendRequest>c__Iterator2::<OriginalJSON>__1
	Dictionary_2_t309261261 * ___U3COriginalJSONU3E__1_1;
	// System.String MarketExample/<SendRequest>c__Iterator2::<data>__2
	String_t* ___U3CdataU3E__2_2;
	// System.Byte[] MarketExample/<SendRequest>c__Iterator2::<binaryData>__3
	ByteU5BU5D_t3397334013* ___U3CbinaryDataU3E__3_3;
	// UnityEngine.WWW MarketExample/<SendRequest>c__Iterator2::<www>__4
	WWW_t2919945039 * ___U3CwwwU3E__4_4;
	// System.Int32 MarketExample/<SendRequest>c__Iterator2::$PC
	int32_t ___U24PC_5;
	// System.Object MarketExample/<SendRequest>c__Iterator2::$current
	Il2CppObject * ___U24current_6;
	// MarketExample MarketExample/<SendRequest>c__Iterator2::<>f__this
	MarketExample_t1603367874 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3Cbase64stringU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator2_t714520321, ___U3Cbase64stringU3E__0_0)); }
	inline String_t* get_U3Cbase64stringU3E__0_0() const { return ___U3Cbase64stringU3E__0_0; }
	inline String_t** get_address_of_U3Cbase64stringU3E__0_0() { return &___U3Cbase64stringU3E__0_0; }
	inline void set_U3Cbase64stringU3E__0_0(String_t* value)
	{
		___U3Cbase64stringU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cbase64stringU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3COriginalJSONU3E__1_1() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator2_t714520321, ___U3COriginalJSONU3E__1_1)); }
	inline Dictionary_2_t309261261 * get_U3COriginalJSONU3E__1_1() const { return ___U3COriginalJSONU3E__1_1; }
	inline Dictionary_2_t309261261 ** get_address_of_U3COriginalJSONU3E__1_1() { return &___U3COriginalJSONU3E__1_1; }
	inline void set_U3COriginalJSONU3E__1_1(Dictionary_2_t309261261 * value)
	{
		___U3COriginalJSONU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3COriginalJSONU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CdataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator2_t714520321, ___U3CdataU3E__2_2)); }
	inline String_t* get_U3CdataU3E__2_2() const { return ___U3CdataU3E__2_2; }
	inline String_t** get_address_of_U3CdataU3E__2_2() { return &___U3CdataU3E__2_2; }
	inline void set_U3CdataU3E__2_2(String_t* value)
	{
		___U3CdataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CbinaryDataU3E__3_3() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator2_t714520321, ___U3CbinaryDataU3E__3_3)); }
	inline ByteU5BU5D_t3397334013* get_U3CbinaryDataU3E__3_3() const { return ___U3CbinaryDataU3E__3_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CbinaryDataU3E__3_3() { return &___U3CbinaryDataU3E__3_3; }
	inline void set_U3CbinaryDataU3E__3_3(ByteU5BU5D_t3397334013* value)
	{
		___U3CbinaryDataU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbinaryDataU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__4_4() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator2_t714520321, ___U3CwwwU3E__4_4)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__4_4() const { return ___U3CwwwU3E__4_4; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__4_4() { return &___U3CwwwU3E__4_4; }
	inline void set_U3CwwwU3E__4_4(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator2_t714520321, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator2_t714520321, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__Iterator2_t714520321, ___U3CU3Ef__this_7)); }
	inline MarketExample_t1603367874 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline MarketExample_t1603367874 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(MarketExample_t1603367874 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
