﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidRateUsPopUp
struct AndroidRateUsPopUp_t1491925263;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AndroidRateUsPopUp::.ctor()
extern "C"  void AndroidRateUsPopUp__ctor_m3057871408 (AndroidRateUsPopUp_t1491925263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AndroidRateUsPopUp AndroidRateUsPopUp::Create(System.String,System.String,System.String)
extern "C"  AndroidRateUsPopUp_t1491925263 * AndroidRateUsPopUp_Create_m2002060404 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___url2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AndroidRateUsPopUp AndroidRateUsPopUp::Create(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  AndroidRateUsPopUp_t1491925263 * AndroidRateUsPopUp_Create_m2216977268 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___url2, String_t* ___yes3, String_t* ___later4, String_t* ___no5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidRateUsPopUp::init()
extern "C"  void AndroidRateUsPopUp_init_m883679132 (AndroidRateUsPopUp_t1491925263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidRateUsPopUp::onPopUpCallBack(System.String)
extern "C"  void AndroidRateUsPopUp_onPopUpCallBack_m1390345496 (AndroidRateUsPopUp_t1491925263 * __this, String_t* ___buttonIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
