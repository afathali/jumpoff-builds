﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.DataMemberAttribute
struct DataMemberAttribute_t2677019114;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Runtime.Serialization.DataMemberAttribute::.ctor()
extern "C"  void DataMemberAttribute__ctor_m2324739680 (DataMemberAttribute_t2677019114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.DataMemberAttribute::get_EmitDefaultValue()
extern "C"  bool DataMemberAttribute_get_EmitDefaultValue_m752213560 (DataMemberAttribute_t2677019114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.DataMemberAttribute::set_EmitDefaultValue(System.Boolean)
extern "C"  void DataMemberAttribute_set_EmitDefaultValue_m1863336097 (DataMemberAttribute_t2677019114 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.DataMemberAttribute::get_IsRequired()
extern "C"  bool DataMemberAttribute_get_IsRequired_m1439340200 (DataMemberAttribute_t2677019114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.DataMemberAttribute::set_IsRequired(System.Boolean)
extern "C"  void DataMemberAttribute_set_IsRequired_m55173287 (DataMemberAttribute_t2677019114 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.DataMemberAttribute::get_Name()
extern "C"  String_t* DataMemberAttribute_get_Name_m690493897 (DataMemberAttribute_t2677019114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.DataMemberAttribute::set_Name(System.String)
extern "C"  void DataMemberAttribute_set_Name_m3447668002 (DataMemberAttribute_t2677019114 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Serialization.DataMemberAttribute::get_Order()
extern "C"  int32_t DataMemberAttribute_get_Order_m4011927195 (DataMemberAttribute_t2677019114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.DataMemberAttribute::set_Order(System.Int32)
extern "C"  void DataMemberAttribute_set_Order_m1074180322 (DataMemberAttribute_t2677019114 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
