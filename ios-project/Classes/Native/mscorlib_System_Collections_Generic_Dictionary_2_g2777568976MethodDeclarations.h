﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>
struct Dictionary_2_t2777568976;
// System.Collections.Generic.IEqualityComparer`1<FB_ProfileImageSize>
struct IEqualityComparer_1_t2215960908;
// System.Collections.Generic.IDictionary`2<FB_ProfileImageSize,System.Object>
struct IDictionary_2_t776652397;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<FB_ProfileImageSize>
struct ICollection_1_t3955403435;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>[]
struct KeyValuePair_2U5BU5D_t2216179795;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>
struct IEnumerator_1_t2305405321;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<FB_ProfileImageSize,System.Object>
struct KeyCollection_t966099451;
// System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>
struct ValueCollection_t1480628819;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_534914198.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_FB_ProfileImageSize3003328130.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4097593678.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3820510179_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3820510179(__this, method) ((  void (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2__ctor_m3820510179_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2722400596_gshared (Dictionary_2_t2777568976 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2722400596(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2777568976 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2722400596_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m239326633_gshared (Dictionary_2_t2777568976 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m239326633(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2777568976 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m239326633_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m952744004_gshared (Dictionary_2_t2777568976 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m952744004(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2777568976 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m952744004_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1377918164_gshared (Dictionary_2_t2777568976 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1377918164(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2777568976 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1377918164_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3266560497_gshared (Dictionary_2_t2777568976 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3266560497(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t2777568976 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3266560497_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1273236090_gshared (Dictionary_2_t2777568976 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1273236090(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2777568976 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1273236090_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m295217063_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m295217063(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m295217063_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m494140623_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m494140623(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m494140623_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1903086849_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1903086849(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1903086849_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m182945305_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m182945305(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m182945305_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3049019312_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3049019312(__this, method) ((  bool (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3049019312_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1272155949_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1272155949(__this, method) ((  bool (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1272155949_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m4053760139_gshared (Dictionary_2_t2777568976 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m4053760139(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2777568976 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m4053760139_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m866044558_gshared (Dictionary_2_t2777568976 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m866044558(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2777568976 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m866044558_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3436419697_gshared (Dictionary_2_t2777568976 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3436419697(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2777568976 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3436419697_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m777454229_gshared (Dictionary_2_t2777568976 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m777454229(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2777568976 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m777454229_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3553569314_gshared (Dictionary_2_t2777568976 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3553569314(__this, ___key0, method) ((  void (*) (Dictionary_2_t2777568976 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3553569314_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1229656415_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1229656415(__this, method) ((  bool (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1229656415_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m802999303_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m802999303(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m802999303_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1453344489_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1453344489(__this, method) ((  bool (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1453344489_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3885751680_gshared (Dictionary_2_t2777568976 * __this, KeyValuePair_2_t534914198  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3885751680(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2777568976 *, KeyValuePair_2_t534914198 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3885751680_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19350924_gshared (Dictionary_2_t2777568976 * __this, KeyValuePair_2_t534914198  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19350924(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2777568976 *, KeyValuePair_2_t534914198 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19350924_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3181025708_gshared (Dictionary_2_t2777568976 * __this, KeyValuePair_2U5BU5D_t2216179795* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3181025708(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2777568976 *, KeyValuePair_2U5BU5D_t2216179795*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3181025708_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3593669827_gshared (Dictionary_2_t2777568976 * __this, KeyValuePair_2_t534914198  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3593669827(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2777568976 *, KeyValuePair_2_t534914198 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3593669827_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2763002279_gshared (Dictionary_2_t2777568976 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2763002279(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2777568976 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2763002279_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1031845602_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1031845602(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1031845602_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2220057697_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2220057697(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2220057697_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m565903548_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m565903548(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m565903548_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m205850079_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m205850079(__this, method) ((  int32_t (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_get_Count_m205850079_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m437541540_gshared (Dictionary_2_t2777568976 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m437541540(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2777568976 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m437541540_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3404479379_gshared (Dictionary_2_t2777568976 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3404479379(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2777568976 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m3404479379_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2834960683_gshared (Dictionary_2_t2777568976 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2834960683(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2777568976 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2834960683_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2982266144_gshared (Dictionary_2_t2777568976 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2982266144(__this, ___size0, method) ((  void (*) (Dictionary_2_t2777568976 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2982266144_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3494160874_gshared (Dictionary_2_t2777568976 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3494160874(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2777568976 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3494160874_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t534914198  Dictionary_2_make_pair_m1921590792_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1921590792(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t534914198  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m1921590792_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m1337642270_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1337642270(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m1337642270_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m2722242478_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2722242478(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m2722242478_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2298250047_gshared (Dictionary_2_t2777568976 * __this, KeyValuePair_2U5BU5D_t2216179795* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2298250047(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2777568976 *, KeyValuePair_2U5BU5D_t2216179795*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2298250047_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m611264601_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m611264601(__this, method) ((  void (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_Resize_m611264601_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3180975920_gshared (Dictionary_2_t2777568976 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3180975920(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2777568976 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m3180975920_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m4092921328_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m4092921328(__this, method) ((  void (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_Clear_m4092921328_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m37955324_gshared (Dictionary_2_t2777568976 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m37955324(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2777568976 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m37955324_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1784130732_gshared (Dictionary_2_t2777568976 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1784130732(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2777568976 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m1784130732_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2275178611_gshared (Dictionary_2_t2777568976 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2275178611(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2777568976 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2275178611_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1429377187_gshared (Dictionary_2_t2777568976 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1429377187(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2777568976 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1429377187_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m4125449524_gshared (Dictionary_2_t2777568976 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m4125449524(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2777568976 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m4125449524_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2476078267_gshared (Dictionary_2_t2777568976 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2476078267(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2777568976 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m2476078267_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::get_Keys()
extern "C"  KeyCollection_t966099451 * Dictionary_2_get_Keys_m2426368816_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2426368816(__this, method) ((  KeyCollection_t966099451 * (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_get_Keys_m2426368816_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::get_Values()
extern "C"  ValueCollection_t1480628819 * Dictionary_2_get_Values_m2602663928_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2602663928(__this, method) ((  ValueCollection_t1480628819 * (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_get_Values_m2602663928_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m3434537169_gshared (Dictionary_2_t2777568976 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3434537169(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2777568976 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3434537169_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m2696135993_gshared (Dictionary_2_t2777568976 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m2696135993(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t2777568976 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2696135993_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m3105491587_gshared (Dictionary_2_t2777568976 * __this, KeyValuePair_2_t534914198  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m3105491587(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2777568976 *, KeyValuePair_2_t534914198 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3105491587_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::GetEnumerator()
extern "C"  Enumerator_t4097593678  Dictionary_2_GetEnumerator_m3251490824_gshared (Dictionary_2_t2777568976 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m3251490824(__this, method) ((  Enumerator_t4097593678  (*) (Dictionary_2_t2777568976 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3251490824_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m787429345_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m787429345(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m787429345_gshared)(__this /* static, unused */, ___key0, ___value1, method)
