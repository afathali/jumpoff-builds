﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidInventory
struct AndroidInventory_t701010211;
// GooglePurchaseTemplate
struct GooglePurchaseTemplate_t2609331866;
// System.String
struct String_t;
// GoogleProductTemplate
struct GoogleProductTemplate_t1112616324;
// System.Collections.Generic.List`1<GooglePurchaseTemplate>
struct List_1_t1978452998;
// System.Collections.Generic.List`1<GoogleProductTemplate>
struct List_1_t481737456;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePurchaseTemplate2609331866.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AndroidInventory::.ctor()
extern "C"  void AndroidInventory__ctor_m711572292 (AndroidInventory_t701010211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInventory::addPurchase(GooglePurchaseTemplate)
extern "C"  void AndroidInventory_addPurchase_m3611946186 (AndroidInventory_t701010211 * __this, GooglePurchaseTemplate_t2609331866 * ___purchase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInventory::removePurchase(GooglePurchaseTemplate)
extern "C"  void AndroidInventory_removePurchase_m2716623837 (AndroidInventory_t701010211 * __this, GooglePurchaseTemplate_t2609331866 * ___purchase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidInventory::IsProductPurchased(System.String)
extern "C"  bool AndroidInventory_IsProductPurchased_m1782224050 (AndroidInventory_t701010211 * __this, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GoogleProductTemplate AndroidInventory::GetProductDetails(System.String)
extern "C"  GoogleProductTemplate_t1112616324 * AndroidInventory_GetProductDetails_m744422462 (AndroidInventory_t701010211 * __this, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePurchaseTemplate AndroidInventory::GetPurchaseDetails(System.String)
extern "C"  GooglePurchaseTemplate_t2609331866 * AndroidInventory_GetPurchaseDetails_m932301262 (AndroidInventory_t701010211 * __this, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GooglePurchaseTemplate> AndroidInventory::get_purchases()
extern "C"  List_1_t1978452998 * AndroidInventory_get_purchases_m1810340402 (AndroidInventory_t701010211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GooglePurchaseTemplate> AndroidInventory::get_Purchases()
extern "C"  List_1_t1978452998 * AndroidInventory_get_Purchases_m631682770 (AndroidInventory_t701010211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GoogleProductTemplate> AndroidInventory::get_products()
extern "C"  List_1_t481737456 * AndroidInventory_get_products_m12279028 (AndroidInventory_t701010211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GoogleProductTemplate> AndroidInventory::get_Products()
extern "C"  List_1_t481737456 * AndroidInventory_get_Products_m3907209172 (AndroidInventory_t701010211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
