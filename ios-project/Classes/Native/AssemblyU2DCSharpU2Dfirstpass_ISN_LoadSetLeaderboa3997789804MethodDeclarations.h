﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_LoadSetLeaderboardsInfoResult
struct ISN_LoadSetLeaderboardsInfoResult_t3997789804;
// GK_LeaderboardSet
struct GK_LeaderboardSet_t5314098;
// SA.Common.Models.Error
struct Error_t445207774;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_LeaderboardSet5314098.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"

// System.Void ISN_LoadSetLeaderboardsInfoResult::.ctor(GK_LeaderboardSet)
extern "C"  void ISN_LoadSetLeaderboardsInfoResult__ctor_m1849617079 (ISN_LoadSetLeaderboardsInfoResult_t3997789804 * __this, GK_LeaderboardSet_t5314098 * ___lbset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_LoadSetLeaderboardsInfoResult::.ctor(GK_LeaderboardSet,SA.Common.Models.Error)
extern "C"  void ISN_LoadSetLeaderboardsInfoResult__ctor_m728450608 (ISN_LoadSetLeaderboardsInfoResult_t3997789804 * __this, GK_LeaderboardSet_t5314098 * ___lbset0, Error_t445207774 * ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_LeaderboardSet ISN_LoadSetLeaderboardsInfoResult::get_LeaderBoardsSet()
extern "C"  GK_LeaderboardSet_t5314098 * ISN_LoadSetLeaderboardsInfoResult_get_LeaderBoardsSet_m2160372781 (ISN_LoadSetLeaderboardsInfoResult_t3997789804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
