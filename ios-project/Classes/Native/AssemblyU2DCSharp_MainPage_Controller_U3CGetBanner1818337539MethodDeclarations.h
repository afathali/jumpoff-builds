﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainPage_Controller/<GetBannerImage>c__Iterator16
struct U3CGetBannerImageU3Ec__Iterator16_t1818337539;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MainPage_Controller/<GetBannerImage>c__Iterator16::.ctor()
extern "C"  void U3CGetBannerImageU3Ec__Iterator16__ctor_m984227130 (U3CGetBannerImageU3Ec__Iterator16_t1818337539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MainPage_Controller/<GetBannerImage>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetBannerImageU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2807265460 (U3CGetBannerImageU3Ec__Iterator16_t1818337539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MainPage_Controller/<GetBannerImage>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetBannerImageU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m4157536220 (U3CGetBannerImageU3Ec__Iterator16_t1818337539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MainPage_Controller/<GetBannerImage>c__Iterator16::MoveNext()
extern "C"  bool U3CGetBannerImageU3Ec__Iterator16_MoveNext_m1494681154 (U3CGetBannerImageU3Ec__Iterator16_t1818337539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller/<GetBannerImage>c__Iterator16::Dispose()
extern "C"  void U3CGetBannerImageU3Ec__Iterator16_Dispose_m3922087413 (U3CGetBannerImageU3Ec__Iterator16_t1818337539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller/<GetBannerImage>c__Iterator16::Reset()
extern "C"  void U3CGetBannerImageU3Ec__Iterator16_Reset_m4257433927 (U3CGetBannerImageU3Ec__Iterator16_t1818337539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
