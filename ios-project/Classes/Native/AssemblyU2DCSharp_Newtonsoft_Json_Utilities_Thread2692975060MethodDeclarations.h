﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread4078621129MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m2029693697(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t2692975060 *, Func_2_t1495126178 *, const MethodInfo*))ThreadSafeStore_2__ctor_m3678278222_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>::Get(TKey)
#define ThreadSafeStore_2_Get_m381113669(__this, ___key0, method) ((  Type_t * (*) (ThreadSafeStore_2_t2692975060 *, TypeNameKey_t3055062677 , const MethodInfo*))ThreadSafeStore_2_Get_m756191284_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m3459989521(__this, ___key0, method) ((  Type_t * (*) (ThreadSafeStore_2_t2692975060 *, TypeNameKey_t3055062677 , const MethodInfo*))ThreadSafeStore_2_AddValue_m3370370496_gshared)(__this, ___key0, method)
