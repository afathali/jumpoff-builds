﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonTestScript
struct JsonTestScript_t2390343987;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void JsonTestScript::.ctor(UnityEngine.TextMesh)
extern "C"  void JsonTestScript__ctor_m3939076275 (JsonTestScript_t2390343987 * __this, TextMesh_t1641806576 * ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonTestScript::SerializeVector3()
extern "C"  void JsonTestScript_SerializeVector3_m1255397156 (JsonTestScript_t2390343987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonTestScript::GenericListSerialization()
extern "C"  void JsonTestScript_GenericListSerialization_m1570669099 (JsonTestScript_t2390343987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonTestScript::PolymorphicSerialization()
extern "C"  void JsonTestScript_PolymorphicSerialization_m77119210 (JsonTestScript_t2390343987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonTestScript::DictionarySerialization()
extern "C"  void JsonTestScript_DictionarySerialization_m2279713508 (JsonTestScript_t2390343987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonTestScript::DictionaryObjectValueSerialization()
extern "C"  void JsonTestScript_DictionaryObjectValueSerialization_m4198847434 (JsonTestScript_t2390343987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonTestScript::DictionaryObjectKeySerialization()
extern "C"  void JsonTestScript_DictionaryObjectKeySerialization_m3350162876 (JsonTestScript_t2390343987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonTestScript::DisplaySuccess(System.String)
extern "C"  void JsonTestScript_DisplaySuccess_m1510333941 (JsonTestScript_t2390343987 * __this, String_t* ___testName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonTestScript::DisplayFail(System.String,System.String)
extern "C"  void JsonTestScript_DisplayFail_m3613750684 (JsonTestScript_t2390343987 * __this, String_t* ___testName0, String_t* ___reason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonTestScript::LogStart(System.String)
extern "C"  void JsonTestScript_LogStart_m1094963050 (JsonTestScript_t2390343987 * __this, String_t* ___testName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonTestScript::LogEnd(System.Int32)
extern "C"  void JsonTestScript_LogEnd_m1022678540 (JsonTestScript_t2390343987 * __this, int32_t ___testNum0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonTestScript::Log(System.Object)
extern "C"  void JsonTestScript_Log_m368745866 (JsonTestScript_t2390343987 * __this, Il2CppObject * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonTestScript::LogSerialized(System.String)
extern "C"  void JsonTestScript_LogSerialized_m2113465322 (JsonTestScript_t2390343987 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonTestScript::LogResult(System.String,System.Object)
extern "C"  void JsonTestScript_LogResult_m2721460719 (JsonTestScript_t2390343987 * __this, String_t* ___shouldEqual0, Il2CppObject * ___actual1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
