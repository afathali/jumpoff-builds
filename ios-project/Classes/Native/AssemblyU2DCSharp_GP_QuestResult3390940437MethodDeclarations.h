﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_QuestResult
struct GP_QuestResult_t3390940437;
// System.String
struct String_t;
// GP_Quest
struct GP_Quest_t1641883470;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_QuestResult::.ctor(System.String)
extern "C"  void GP_QuestResult__ctor_m2060694812 (GP_QuestResult_t3390940437 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_Quest GP_QuestResult::GetQuest()
extern "C"  GP_Quest_t1641883470 * GP_QuestResult_GetQuest_m695504629 (GP_QuestResult_t3390940437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
