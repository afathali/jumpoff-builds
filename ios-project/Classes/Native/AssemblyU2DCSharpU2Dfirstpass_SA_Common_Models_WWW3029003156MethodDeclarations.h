﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Models.WWWTextureLoader/<LoadCoroutin>c__IteratorC
struct U3CLoadCoroutinU3Ec__IteratorC_t3029003156;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.Common.Models.WWWTextureLoader/<LoadCoroutin>c__IteratorC::.ctor()
extern "C"  void U3CLoadCoroutinU3Ec__IteratorC__ctor_m1013678707 (U3CLoadCoroutinU3Ec__IteratorC_t3029003156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Models.WWWTextureLoader/<LoadCoroutin>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadCoroutinU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4250725137 (U3CLoadCoroutinU3Ec__IteratorC_t3029003156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA.Common.Models.WWWTextureLoader/<LoadCoroutin>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadCoroutinU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m2509741321 (U3CLoadCoroutinU3Ec__IteratorC_t3029003156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA.Common.Models.WWWTextureLoader/<LoadCoroutin>c__IteratorC::MoveNext()
extern "C"  bool U3CLoadCoroutinU3Ec__IteratorC_MoveNext_m3331223593 (U3CLoadCoroutinU3Ec__IteratorC_t3029003156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.WWWTextureLoader/<LoadCoroutin>c__IteratorC::Dispose()
extern "C"  void U3CLoadCoroutinU3Ec__IteratorC_Dispose_m280667408 (U3CLoadCoroutinU3Ec__IteratorC_t3029003156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.WWWTextureLoader/<LoadCoroutin>c__IteratorC::Reset()
extern "C"  void U3CLoadCoroutinU3Ec__IteratorC_Reset_m2088169818 (U3CLoadCoroutinU3Ec__IteratorC_t3029003156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
