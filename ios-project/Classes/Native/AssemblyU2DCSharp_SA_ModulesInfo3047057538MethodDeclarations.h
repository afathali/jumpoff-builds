﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_ModulesInfo
struct SA_ModulesInfo_t3047057538;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void SA_ModulesInfo::.ctor()
extern "C"  void SA_ModulesInfo__ctor_m3055554237 (SA_ModulesInfo_t3047057538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA_ModulesInfo::get_FB_SDK_VersionCode()
extern "C"  String_t* SA_ModulesInfo_get_FB_SDK_VersionCode_m2257478156 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SA_ModulesInfo::get_FB_SDK_MajorVersionCode()
extern "C"  int32_t SA_ModulesInfo_get_FB_SDK_MajorVersionCode_m2911391242 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
