﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen3321460360MethodDeclarations.h"

// System.Void System.Action`2<GK_Player,GK_InviteRecipientResponse>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m1369802972(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t1349588917 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m2260828716_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<GK_Player,GK_InviteRecipientResponse>::Invoke(T1,T2)
#define Action_2_Invoke_m3933725083(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1349588917 *, GK_Player_t2782008294 *, int32_t, const MethodInfo*))Action_2_Invoke_m706623781_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<GK_Player,GK_InviteRecipientResponse>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m3309960986(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t1349588917 *, GK_Player_t2782008294 *, int32_t, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m594672950_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<GK_Player,GK_InviteRecipientResponse>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m290447362(__this, ___result0, method) ((  void (*) (Action_2_t1349588917 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3189183710_gshared)(__this, ___result0, method)
