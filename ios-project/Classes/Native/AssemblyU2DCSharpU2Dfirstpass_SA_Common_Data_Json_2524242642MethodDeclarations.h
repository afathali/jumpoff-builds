﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Data.Json/Serializer
struct Serializer_t2524242642;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.IList
struct IList_t3321498491;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SA.Common.Data.Json/Serializer::.ctor()
extern "C"  void Serializer__ctor_m3936685711 (Serializer_t2524242642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA.Common.Data.Json/Serializer::Serialize(System.Object)
extern "C"  String_t* Serializer_Serialize_m588422350 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Data.Json/Serializer::SerializeValue(System.Object)
extern "C"  void Serializer_SerializeValue_m32130932 (Serializer_t2524242642 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Data.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern "C"  void Serializer_SerializeObject_m1231718165 (Serializer_t2524242642 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Data.Json/Serializer::SerializeArray(System.Collections.IList)
extern "C"  void Serializer_SerializeArray_m1515841007 (Serializer_t2524242642 * __this, Il2CppObject * ___anArray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Data.Json/Serializer::SerializeString(System.String)
extern "C"  void Serializer_SerializeString_m3625905554 (Serializer_t2524242642 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Data.Json/Serializer::SerializeOther(System.Object)
extern "C"  void Serializer_SerializeOther_m1693820347 (Serializer_t2524242642 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
