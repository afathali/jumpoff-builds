﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidADBanner
struct AndroidADBanner_t886219444;
// System.Action`1<GoogleMobileAdBanner>
struct Action_1_t1125618340;
// GoogleMobileAdBanner
struct GoogleMobileAdBanner_t1323818958;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "AssemblyU2DCSharp_GADBannerSize1988135029.h"

// System.Void AndroidADBanner::.ctor(UnityEngine.TextAnchor,GADBannerSize,System.Int32)
extern "C"  void AndroidADBanner__ctor_m2859037708 (AndroidADBanner_t886219444 * __this, int32_t ___anchor0, int32_t ___size1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::.ctor(System.Int32,System.Int32,GADBannerSize,System.Int32)
extern "C"  void AndroidADBanner__ctor_m21669475 (AndroidADBanner_t886219444 * __this, int32_t ___x0, int32_t ___y1, int32_t ___size2, int32_t ___id3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::Hide()
extern "C"  void AndroidADBanner_Hide_m3965624137 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::Show()
extern "C"  void AndroidADBanner_Show_m3224694474 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::Refresh()
extern "C"  void AndroidADBanner_Refresh_m3980831044 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::Pause()
extern "C"  void AndroidADBanner_Pause_m3179819159 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::Resume()
extern "C"  void AndroidADBanner_Resume_m1992168432 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::SetBannerPosition(System.Int32,System.Int32)
extern "C"  void AndroidADBanner_SetBannerPosition_m1594312434 (AndroidADBanner_t886219444 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::SetBannerPosition(UnityEngine.TextAnchor)
extern "C"  void AndroidADBanner_SetBannerPosition_m397926685 (AndroidADBanner_t886219444 * __this, int32_t ___anchor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::DestroyAfterLoad()
extern "C"  void AndroidADBanner_DestroyAfterLoad_m3136917933 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::SetDimentions(System.Int32,System.Int32)
extern "C"  void AndroidADBanner_SetDimentions_m1433837643 (AndroidADBanner_t886219444 * __this, int32_t ___w0, int32_t ___h1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AndroidADBanner::get_id()
extern "C"  int32_t AndroidADBanner_get_id_m528680911 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AndroidADBanner::get_width()
extern "C"  int32_t AndroidADBanner_get_width_m3557273958 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AndroidADBanner::get_height()
extern "C"  int32_t AndroidADBanner_get_height_m2905292169 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GADBannerSize AndroidADBanner::get_size()
extern "C"  int32_t AndroidADBanner_get_size_m4050772873 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidADBanner::get_IsLoaded()
extern "C"  bool AndroidADBanner_get_IsLoaded_m3281503021 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidADBanner::get_IsOnScreen()
extern "C"  bool AndroidADBanner_get_IsOnScreen_m3573588939 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidADBanner::get_ShowOnLoad()
extern "C"  bool AndroidADBanner_get_ShowOnLoad_m462797696 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::set_ShowOnLoad(System.Boolean)
extern "C"  void AndroidADBanner_set_ShowOnLoad_m1076771019 (AndroidADBanner_t886219444 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextAnchor AndroidADBanner::get_anchor()
extern "C"  int32_t AndroidADBanner_get_anchor_m14226701 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AndroidADBanner::get_gravity()
extern "C"  int32_t AndroidADBanner_get_gravity_m1094968200 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<GoogleMobileAdBanner> AndroidADBanner::get_OnLoadedAction()
extern "C"  Action_1_t1125618340 * AndroidADBanner_get_OnLoadedAction_m1387360017 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::set_OnLoadedAction(System.Action`1<GoogleMobileAdBanner>)
extern "C"  void AndroidADBanner_set_OnLoadedAction_m2459362242 (AndroidADBanner_t886219444 * __this, Action_1_t1125618340 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<GoogleMobileAdBanner> AndroidADBanner::get_OnFailedLoadingAction()
extern "C"  Action_1_t1125618340 * AndroidADBanner_get_OnFailedLoadingAction_m2516431363 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::set_OnFailedLoadingAction(System.Action`1<GoogleMobileAdBanner>)
extern "C"  void AndroidADBanner_set_OnFailedLoadingAction_m2398606788 (AndroidADBanner_t886219444 * __this, Action_1_t1125618340 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<GoogleMobileAdBanner> AndroidADBanner::get_OnOpenedAction()
extern "C"  Action_1_t1125618340 * AndroidADBanner_get_OnOpenedAction_m1498042551 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::set_OnOpenedAction(System.Action`1<GoogleMobileAdBanner>)
extern "C"  void AndroidADBanner_set_OnOpenedAction_m430525046 (AndroidADBanner_t886219444 * __this, Action_1_t1125618340 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<GoogleMobileAdBanner> AndroidADBanner::get_OnClosedAction()
extern "C"  Action_1_t1125618340 * AndroidADBanner_get_OnClosedAction_m2748820258 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::set_OnClosedAction(System.Action`1<GoogleMobileAdBanner>)
extern "C"  void AndroidADBanner_set_OnClosedAction_m2229926323 (AndroidADBanner_t886219444 * __this, Action_1_t1125618340 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<GoogleMobileAdBanner> AndroidADBanner::get_OnLeftApplicationAction()
extern "C"  Action_1_t1125618340 * AndroidADBanner_get_OnLeftApplicationAction_m1438985851 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::set_OnLeftApplicationAction(System.Action`1<GoogleMobileAdBanner>)
extern "C"  void AndroidADBanner_set_OnLeftApplicationAction_m2754530566 (AndroidADBanner_t886219444 * __this, Action_1_t1125618340 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::OnBannerAdLoaded()
extern "C"  void AndroidADBanner_OnBannerAdLoaded_m2925984044 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::OnBannerAdFailedToLoad()
extern "C"  void AndroidADBanner_OnBannerAdFailedToLoad_m1279533943 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::OnBannerAdOpened()
extern "C"  void AndroidADBanner_OnBannerAdOpened_m1609121372 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::OnBannerAdClosed()
extern "C"  void AndroidADBanner_OnBannerAdClosed_m1443610259 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::OnBannerAdLeftApplication()
extern "C"  void AndroidADBanner_OnBannerAdLeftApplication_m274664156 (AndroidADBanner_t886219444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::<_OnLoadedAction>m__85(GoogleMobileAdBanner)
extern "C"  void AndroidADBanner_U3C_OnLoadedActionU3Em__85_m3151195046 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::<_OnFailedLoadingAction>m__86(GoogleMobileAdBanner)
extern "C"  void AndroidADBanner_U3C_OnFailedLoadingActionU3Em__86_m2449740837 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::<_OnOpenedAction>m__87(GoogleMobileAdBanner)
extern "C"  void AndroidADBanner_U3C_OnOpenedActionU3Em__87_m4014776084 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::<_OnClosedAction>m__88(GoogleMobileAdBanner)
extern "C"  void AndroidADBanner_U3C_OnClosedActionU3Em__88_m224828664 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidADBanner::<_OnLeftApplicationAction>m__89(GoogleMobileAdBanner)
extern "C"  void AndroidADBanner_U3C_OnLeftApplicationActionU3Em__89_m1529397702 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
