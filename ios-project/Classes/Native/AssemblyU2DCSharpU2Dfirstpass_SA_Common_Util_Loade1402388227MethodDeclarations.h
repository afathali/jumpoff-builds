﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_t1558332529;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SA.Common.Util.Loader::LoadWebTexture(System.String,System.Action`1<UnityEngine.Texture2D>)
extern "C"  void Loader_LoadWebTexture_m3316674898 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Action_1_t3344795111 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Util.Loader::LoadPrefab(System.String,System.Action`1<UnityEngine.GameObject>)
extern "C"  void Loader_LoadPrefab_m3042503449 (Il2CppObject * __this /* static, unused */, String_t* ___localPath0, Action_1_t1558332529 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
