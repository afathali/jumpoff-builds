﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GP_TBM_Match
struct GP_TBM_Match_t1275077981;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GP_TBM_MatchReceivedResult
struct  GP_TBM_MatchReceivedResult_t2394672915  : public Il2CppObject
{
public:
	// GP_TBM_Match GP_TBM_MatchReceivedResult::_Match
	GP_TBM_Match_t1275077981 * ____Match_0;

public:
	inline static int32_t get_offset_of__Match_0() { return static_cast<int32_t>(offsetof(GP_TBM_MatchReceivedResult_t2394672915, ____Match_0)); }
	inline GP_TBM_Match_t1275077981 * get__Match_0() const { return ____Match_0; }
	inline GP_TBM_Match_t1275077981 ** get_address_of__Match_0() { return &____Match_0; }
	inline void set__Match_0(GP_TBM_Match_t1275077981 * value)
	{
		____Match_0 = value;
		Il2CppCodeGenWriteBarrier(&____Match_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
