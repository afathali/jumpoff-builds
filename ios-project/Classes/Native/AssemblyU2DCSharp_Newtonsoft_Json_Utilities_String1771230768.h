﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringUtils/<NumberLines>c__AnonStorey38
struct  U3CNumberLinesU3Ec__AnonStorey38_t1771230768  : public Il2CppObject
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.StringUtils/<NumberLines>c__AnonStorey38::lineNumber
	int32_t ___lineNumber_0;

public:
	inline static int32_t get_offset_of_lineNumber_0() { return static_cast<int32_t>(offsetof(U3CNumberLinesU3Ec__AnonStorey38_t1771230768, ___lineNumber_0)); }
	inline int32_t get_lineNumber_0() const { return ___lineNumber_0; }
	inline int32_t* get_address_of_lineNumber_0() { return &___lineNumber_0; }
	inline void set_lineNumber_0(int32_t value)
	{
		___lineNumber_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
