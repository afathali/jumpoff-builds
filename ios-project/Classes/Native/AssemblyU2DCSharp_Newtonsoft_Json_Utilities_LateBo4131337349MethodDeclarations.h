﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey2F`1<System.Object>
struct U3CCreateMethodCallU3Ec__AnonStorey2F_1_t4131337349;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey2F`1<System.Object>::.ctor()
extern "C"  void U3CCreateMethodCallU3Ec__AnonStorey2F_1__ctor_m2292220810_gshared (U3CCreateMethodCallU3Ec__AnonStorey2F_1_t4131337349 * __this, const MethodInfo* method);
#define U3CCreateMethodCallU3Ec__AnonStorey2F_1__ctor_m2292220810(__this, method) ((  void (*) (U3CCreateMethodCallU3Ec__AnonStorey2F_1_t4131337349 *, const MethodInfo*))U3CCreateMethodCallU3Ec__AnonStorey2F_1__ctor_m2292220810_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey2F`1<System.Object>::<>m__F5(T,System.Object[])
extern "C"  Il2CppObject * U3CCreateMethodCallU3Ec__AnonStorey2F_1_U3CU3Em__F5_m3417254765_gshared (U3CCreateMethodCallU3Ec__AnonStorey2F_1_t4131337349 * __this, Il2CppObject * ___o0, ObjectU5BU5D_t3614634134* ___a1, const MethodInfo* method);
#define U3CCreateMethodCallU3Ec__AnonStorey2F_1_U3CU3Em__F5_m3417254765(__this, ___o0, ___a1, method) ((  Il2CppObject * (*) (U3CCreateMethodCallU3Ec__AnonStorey2F_1_t4131337349 *, Il2CppObject *, ObjectU5BU5D_t3614634134*, const MethodInfo*))U3CCreateMethodCallU3Ec__AnonStorey2F_1_U3CU3Em__F5_m3417254765_gshared)(__this, ___o0, ___a1, method)
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey2F`1<System.Object>::<>m__F6(T,System.Object[])
extern "C"  Il2CppObject * U3CCreateMethodCallU3Ec__AnonStorey2F_1_U3CU3Em__F6_m1046787422_gshared (U3CCreateMethodCallU3Ec__AnonStorey2F_1_t4131337349 * __this, Il2CppObject * ___o0, ObjectU5BU5D_t3614634134* ___a1, const MethodInfo* method);
#define U3CCreateMethodCallU3Ec__AnonStorey2F_1_U3CU3Em__F6_m1046787422(__this, ___o0, ___a1, method) ((  Il2CppObject * (*) (U3CCreateMethodCallU3Ec__AnonStorey2F_1_t4131337349 *, Il2CppObject *, ObjectU5BU5D_t3614634134*, const MethodInfo*))U3CCreateMethodCallU3Ec__AnonStorey2F_1_U3CU3Em__F6_m1046787422_gshared)(__this, ___o0, ___a1, method)
