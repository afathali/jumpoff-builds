﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FB_PostResult
struct FB_PostResult_t992884730;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FB_PostResult::.ctor(System.String,System.String)
extern "C"  void FB_PostResult__ctor_m858218605 (FB_PostResult_t992884730 * __this, String_t* ___RawData0, String_t* ___Error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_PostResult::get_PostId()
extern "C"  String_t* FB_PostResult_get_PostId_m21796132 (FB_PostResult_t992884730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
