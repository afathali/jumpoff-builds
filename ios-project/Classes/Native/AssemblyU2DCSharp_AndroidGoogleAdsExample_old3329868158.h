﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// GoogleMobileAdBanner
struct GoogleMobileAdBanner_t1323818958;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidGoogleAdsExample_old
struct  AndroidGoogleAdsExample_old_t3329868158  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GUIStyle AndroidGoogleAdsExample_old::style
	GUIStyle_t1799908754 * ___style_4;
	// UnityEngine.GUIStyle AndroidGoogleAdsExample_old::style2
	GUIStyle_t1799908754 * ___style2_5;
	// GoogleMobileAdBanner AndroidGoogleAdsExample_old::banner1
	Il2CppObject * ___banner1_6;
	// GoogleMobileAdBanner AndroidGoogleAdsExample_old::banner2
	Il2CppObject * ___banner2_7;
	// System.Boolean AndroidGoogleAdsExample_old::IsInterstisialsAdReady
	bool ___IsInterstisialsAdReady_8;

public:
	inline static int32_t get_offset_of_style_4() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_old_t3329868158, ___style_4)); }
	inline GUIStyle_t1799908754 * get_style_4() const { return ___style_4; }
	inline GUIStyle_t1799908754 ** get_address_of_style_4() { return &___style_4; }
	inline void set_style_4(GUIStyle_t1799908754 * value)
	{
		___style_4 = value;
		Il2CppCodeGenWriteBarrier(&___style_4, value);
	}

	inline static int32_t get_offset_of_style2_5() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_old_t3329868158, ___style2_5)); }
	inline GUIStyle_t1799908754 * get_style2_5() const { return ___style2_5; }
	inline GUIStyle_t1799908754 ** get_address_of_style2_5() { return &___style2_5; }
	inline void set_style2_5(GUIStyle_t1799908754 * value)
	{
		___style2_5 = value;
		Il2CppCodeGenWriteBarrier(&___style2_5, value);
	}

	inline static int32_t get_offset_of_banner1_6() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_old_t3329868158, ___banner1_6)); }
	inline Il2CppObject * get_banner1_6() const { return ___banner1_6; }
	inline Il2CppObject ** get_address_of_banner1_6() { return &___banner1_6; }
	inline void set_banner1_6(Il2CppObject * value)
	{
		___banner1_6 = value;
		Il2CppCodeGenWriteBarrier(&___banner1_6, value);
	}

	inline static int32_t get_offset_of_banner2_7() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_old_t3329868158, ___banner2_7)); }
	inline Il2CppObject * get_banner2_7() const { return ___banner2_7; }
	inline Il2CppObject ** get_address_of_banner2_7() { return &___banner2_7; }
	inline void set_banner2_7(Il2CppObject * value)
	{
		___banner2_7 = value;
		Il2CppCodeGenWriteBarrier(&___banner2_7, value);
	}

	inline static int32_t get_offset_of_IsInterstisialsAdReady_8() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_old_t3329868158, ___IsInterstisialsAdReady_8)); }
	inline bool get_IsInterstisialsAdReady_8() const { return ___IsInterstisialsAdReady_8; }
	inline bool* get_address_of_IsInterstisialsAdReady_8() { return &___IsInterstisialsAdReady_8; }
	inline void set_IsInterstisialsAdReady_8(bool value)
	{
		___IsInterstisialsAdReady_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
