﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AN_ApplicationTemplate
struct AN_ApplicationTemplate_t3835038684;
// System.Collections.Generic.List`1<AN_PropertyTemplate>
struct List_1_t1762270573;

#include "AssemblyU2DCSharp_AN_BaseTemplate2111071391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AN_ManifestTemplate
struct  AN_ManifestTemplate_t1498406345  : public AN_BaseTemplate_t2111071391
{
public:
	// AN_ApplicationTemplate AN_ManifestTemplate::_applicationTemplate
	AN_ApplicationTemplate_t3835038684 * ____applicationTemplate_2;
	// System.Collections.Generic.List`1<AN_PropertyTemplate> AN_ManifestTemplate::_permissions
	List_1_t1762270573 * ____permissions_3;

public:
	inline static int32_t get_offset_of__applicationTemplate_2() { return static_cast<int32_t>(offsetof(AN_ManifestTemplate_t1498406345, ____applicationTemplate_2)); }
	inline AN_ApplicationTemplate_t3835038684 * get__applicationTemplate_2() const { return ____applicationTemplate_2; }
	inline AN_ApplicationTemplate_t3835038684 ** get_address_of__applicationTemplate_2() { return &____applicationTemplate_2; }
	inline void set__applicationTemplate_2(AN_ApplicationTemplate_t3835038684 * value)
	{
		____applicationTemplate_2 = value;
		Il2CppCodeGenWriteBarrier(&____applicationTemplate_2, value);
	}

	inline static int32_t get_offset_of__permissions_3() { return static_cast<int32_t>(offsetof(AN_ManifestTemplate_t1498406345, ____permissions_3)); }
	inline List_1_t1762270573 * get__permissions_3() const { return ____permissions_3; }
	inline List_1_t1762270573 ** get_address_of__permissions_3() { return &____permissions_3; }
	inline void set__permissions_3(List_1_t1762270573 * value)
	{
		____permissions_3 = value;
		Il2CppCodeGenWriteBarrier(&____permissions_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
