﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonPropertyAttribute
struct JsonPropertyAttribute_t2023370155;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_NullValueHandlin3618095365.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_DefaultValueHand3457895463.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ReferenceLoopHan1017855894.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObjectCreationHa3720134651.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_TypeNameHandling1331513094.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Required2961887721.h"

// System.Void Newtonsoft.Json.JsonPropertyAttribute::.ctor()
extern "C"  void JsonPropertyAttribute__ctor_m104648295 (JsonPropertyAttribute_t2023370155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonPropertyAttribute::.ctor(System.String)
extern "C"  void JsonPropertyAttribute__ctor_m1418203749 (JsonPropertyAttribute_t2023370155 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonPropertyAttribute::get_NullValueHandling()
extern "C"  int32_t JsonPropertyAttribute_get_NullValueHandling_m1602690516 (JsonPropertyAttribute_t2023370155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonPropertyAttribute::set_NullValueHandling(Newtonsoft.Json.NullValueHandling)
extern "C"  void JsonPropertyAttribute_set_NullValueHandling_m773198545 (JsonPropertyAttribute_t2023370155 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonPropertyAttribute::get_DefaultValueHandling()
extern "C"  int32_t JsonPropertyAttribute_get_DefaultValueHandling_m3269756828 (JsonPropertyAttribute_t2023370155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonPropertyAttribute::set_DefaultValueHandling(Newtonsoft.Json.DefaultValueHandling)
extern "C"  void JsonPropertyAttribute_set_DefaultValueHandling_m3967444945 (JsonPropertyAttribute_t2023370155 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonPropertyAttribute::get_ReferenceLoopHandling()
extern "C"  int32_t JsonPropertyAttribute_get_ReferenceLoopHandling_m2070064726 (JsonPropertyAttribute_t2023370155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonPropertyAttribute::set_ReferenceLoopHandling(Newtonsoft.Json.ReferenceLoopHandling)
extern "C"  void JsonPropertyAttribute_set_ReferenceLoopHandling_m2628088433 (JsonPropertyAttribute_t2023370155 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonPropertyAttribute::get_ObjectCreationHandling()
extern "C"  int32_t JsonPropertyAttribute_get_ObjectCreationHandling_m2172694620 (JsonPropertyAttribute_t2023370155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonPropertyAttribute::set_ObjectCreationHandling(Newtonsoft.Json.ObjectCreationHandling)
extern "C"  void JsonPropertyAttribute_set_ObjectCreationHandling_m2161817761 (JsonPropertyAttribute_t2023370155 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonPropertyAttribute::get_TypeNameHandling()
extern "C"  int32_t JsonPropertyAttribute_get_TypeNameHandling_m828353820 (JsonPropertyAttribute_t2023370155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonPropertyAttribute::set_TypeNameHandling(Newtonsoft.Json.TypeNameHandling)
extern "C"  void JsonPropertyAttribute_set_TypeNameHandling_m582122217 (JsonPropertyAttribute_t2023370155 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonPropertyAttribute::get_IsReference()
extern "C"  bool JsonPropertyAttribute_get_IsReference_m436316631 (JsonPropertyAttribute_t2023370155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonPropertyAttribute::set_IsReference(System.Boolean)
extern "C"  void JsonPropertyAttribute_set_IsReference_m380536674 (JsonPropertyAttribute_t2023370155 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonPropertyAttribute::get_Order()
extern "C"  int32_t JsonPropertyAttribute_get_Order_m1625751404 (JsonPropertyAttribute_t2023370155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonPropertyAttribute::set_Order(System.Int32)
extern "C"  void JsonPropertyAttribute_set_Order_m698393925 (JsonPropertyAttribute_t2023370155 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonPropertyAttribute::get_PropertyName()
extern "C"  String_t* JsonPropertyAttribute_get_PropertyName_m657768103 (JsonPropertyAttribute_t2023370155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonPropertyAttribute::set_PropertyName(System.String)
extern "C"  void JsonPropertyAttribute_set_PropertyName_m1176243288 (JsonPropertyAttribute_t2023370155 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Required Newtonsoft.Json.JsonPropertyAttribute::get_Required()
extern "C"  int32_t JsonPropertyAttribute_get_Required_m2505966492 (JsonPropertyAttribute_t2023370155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonPropertyAttribute::set_Required(Newtonsoft.Json.Required)
extern "C"  void JsonPropertyAttribute_set_Required_m1059516429 (JsonPropertyAttribute_t2023370155 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
