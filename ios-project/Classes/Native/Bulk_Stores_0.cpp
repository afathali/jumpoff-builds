﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.Purchasing.AmazonAppStoreStoreExtensions
struct AmazonAppStoreStoreExtensions_t1518886395;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4251328308;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>
struct HashSet_1_t275936122;
// UnityEngine.Purchasing.AndroidJavaStore
struct AndroidJavaStore_t2772549594;
// System.String
struct String_t;
// UnityEngine.Purchasing.AppleStoreImpl
struct AppleStoreImpl_t1301617341;
// Uniject.IUtil
struct IUtil_t2188430191;
// UnityEngine.Purchasing.INativeAppleStore
struct INativeAppleStore_t2240226449;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<UnityEngine.Purchasing.Product>
struct Action_1_t1005487353;
// UnityEngine.Purchasing.AppleStoreImpl/<MessageCallback>c__AnonStorey0
struct U3CMessageCallbackU3Ec__AnonStorey0_t2294210682;
// UnityEngine.Purchasing.Extension.UnityUtil
struct UnityUtil_t166323129;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action
struct Action_t3226471752;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>c__Iterator0
struct U3CDelayedCoroutineU3Ec__Iterator0_t3019257939;
// UnityEngine.Purchasing.FakeAmazonExtensions
struct FakeAmazonExtensions_t2261777661;
// UnityEngine.Purchasing.FakeAppleConfiguation
struct FakeAppleConfiguation_t4052738437;
// UnityEngine.Purchasing.FakeAppleExtensions
struct FakeAppleExtensions_t4039399289;
// UnityEngine.Purchasing.FakeGooglePlayConfiguration
struct FakeGooglePlayConfiguration_t737012266;
// UnityEngine.Purchasing.FakeSamsungAppsExtensions
struct FakeSamsungAppsExtensions_t1522853249;
// UnityEngine.Purchasing.FakeStore
struct FakeStore_t3882981564;
// UnityEngine.Purchasing.Extension.IStoreCallback
struct IStoreCallback_t2691517565;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>
struct ReadOnlyCollection_1_t2128260960;
// System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>
struct Action_2_t2790035381;
// UnityEngine.Purchasing.ProductDefinition
struct ProductDefinition_t1942475268;
// System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>
struct Action_2_t1158962578;
// UnityEngine.Purchasing.FakeStore/<Purchase>c__AnonStorey1
struct U3CPurchaseU3Ec__AnonStorey1_t2000504963;
// UnityEngine.Purchasing.FakeStore/<RetrieveProducts>c__AnonStorey0
struct U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567;
// UnityEngine.Purchasing.FakeTizenStoreConfiguration
struct FakeTizenStoreConfiguration_t3055550456;
// UnityEngine.Purchasing.JavaBridge
struct JavaBridge_t44746847;
// UnityEngine.Purchasing.IUnityCallback
struct IUnityCallback_t1155931721;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.ProductDefinition>
struct IEnumerable_1_t2234602313;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Extension.ProductDescription>
struct List_1_t2687388655;
// UnityEngine.Purchasing.Extension.PurchaseFailureDescription
struct PurchaseFailureDescription_t1607114611;
// UnityEngine.Purchasing.ProductMetadata
struct ProductMetadata_t1573242544;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// UnityEngine.Purchasing.NativeJSONStore
struct NativeJSONStore_t3685388740;
// UnityEngine.Purchasing.INativeStore
struct INativeStore_t3203646079;
// UnityEngine.Purchasing.RawStoreProvider
struct RawStoreProvider_t3477922056;
// UnityEngine.Purchasing.Extension.IPurchasingBinder
struct IPurchasingBinder_t2054654539;
// UnityEngine.Purchasing.IAmazonExtensions
struct IAmazonExtensions_t3890253245;
// UnityEngine.Purchasing.IAmazonConfiguration
struct IAmazonConfiguration_t3016942165;
// UnityEngine.Purchasing.ISamsungAppsExtensions
struct ISamsungAppsExtensions_t3429739537;
// UnityEngine.Purchasing.ISamsungAppsConfiguration
struct ISamsungAppsConfiguration_t4066821689;
// UnityEngine.Purchasing.INativeTizenStore
struct INativeTizenStore_t513596045;
// UnityEngine.Purchasing.SamsungAppsStoreExtensions
struct SamsungAppsStoreExtensions_t3441062041;
// UnityEngine.Purchasing.ScriptingUnityCallback
struct ScriptingUnityCallback_t906080071;
// UnityEngine.Purchasing.StandardPurchasingModule
struct StandardPurchasingModule_t4003664591;
// UnityEngine.ILogger
struct ILogger_t1425954571;
// UnityEngine.Purchasing.IRawStoreProvider
struct IRawStoreProvider_t2441088289;
// UnityEngine.Purchasing.IAppleExtensions
struct IAppleExtensions_t1627764765;
// UnityEngine.Purchasing.IAppleConfiguration
struct IAppleConfiguration_t3277762425;
// UnityEngine.Purchasing.IMicrosoftConfiguration
struct IMicrosoftConfiguration_t1212838845;
// UnityEngine.Purchasing.IGooglePlayConfiguration
struct IGooglePlayConfiguration_t2615679878;
// UnityEngine.Purchasing.ITizenStoreConfiguration
struct ITizenStoreConfiguration_t2900348728;
// UnityEngine.Purchasing.IAndroidStoreSelection
struct IAndroidStoreSelection_t3134941501;
// UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance
struct StoreInstance_t107230755;
// UnityEngine.Purchasing.Extension.IStore
struct IStore_t3609486926;
// UnityEngine.Purchasing.StandardPurchasingModule/MicrosoftConfiguration
struct MicrosoftConfiguration_t1310600573;
// UnityEngine.Purchasing.StoreConfiguration
struct StoreConfiguration_t2466794143;
// UnityEngine.Purchasing.TizenStoreImpl
struct TizenStoreImpl_t274247241;
// UnityEngine.Purchasing.UIFakeStore
struct UIFakeStore_t3684252124;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Action`2<System.Boolean,System.Int32>
struct Action_2_t1907880187;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Purchasing.UIFakeStore/LifecycleNotifier
struct LifecycleNotifier_t1057582876;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// UnityEngine.EventSystems.StandaloneInputModule
struct StandaloneInputModule_t70867863;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.String>
struct Func_2_t2958801606;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// UnityEngine.Purchasing.UIFakeStore/DialogRequest
struct DialogRequest_t2092195449;
// UnityEngine.Purchasing.WinRTStore
struct WinRTStore_t36043095;
// UnityEngine.Purchasing.Default.IWindowsIAP
struct IWindowsIAP_t818184396;
// System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.Boolean>
struct Func_2_t460188795;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3961629604;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Default.WinProductDescription>
struct IEnumerable_1_t1367238450;
// System.Func`2<UnityEngine.Purchasing.ProductDefinition,UnityEngine.Purchasing.Default.WinProductDescription>
struct Func_2_t2004692778;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Default.WinProductDescription>
struct List_1_t444232537;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// UnityEngine.Purchasing.Default.WinProductDescription
struct WinProductDescription_t1075111405;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Stores_U3CModuleU3E3783534214.h"
#include "Stores_U3CModuleU3E3783534214MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_AmazonAppStoreStoreE1518886395.h"
#include "Stores_UnityEngine_Purchasing_AmazonAppStoreStoreE1518886395MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject4251328308.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_ge275936122.h"
#include "Stores_UnityEngine_Purchasing_JSONSerializer501879906MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject4251328308MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object2689449295.h"
#include "Stores_UnityEngine_Purchasing_AndroidJavaStore2772549594.h"
#include "Stores_UnityEngine_Purchasing_AndroidJavaStore2772549594MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_AndroidStore3203055206.h"
#include "Stores_UnityEngine_Purchasing_AndroidStore3203055206MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl1301617341.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl1301617341MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_NativeJSONStore3685388740MethodDeclarations.h"
#include "Apple_UnityEngine_Purchasing_UnityPurchasingCallba2635187846MethodDeclarations.h"
#include "Apple_UnityEngine_Purchasing_UnityPurchasingCallba2635187846.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Stores_UnityEngine_Purchasing_NativeJSONStore3685388740.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "mscorlib_System_Action_1_gen1005487353.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod3600019299MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1005487353MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod3600019299.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Action_1_gen1831019615MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1831019615.h"
#include "System_Core_System_Action3226471752MethodDeclarations.h"
#include "System_Core_System_Action3226471752.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl_U3CMe2294210682MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl_U3CMe2294210682.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710.h"
#include "mscorlib_System_Int322071877448.h"
#include "Stores_UnityEngine_Purchasing_Extension_UnityUtil166323129.h"
#include "Stores_UnityEngine_Purchasing_Extension_UnityUtil166323129MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2996495232MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2996495232.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UnityEngine_SystemInfo2353426895MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DeviceType2044541946.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2595592884MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2595592884.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1238706099MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1238706099.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_FormatException2948921286.h"
#include "Stores_UnityEngine_Purchasing_Extension_UnityUtil_3019257939MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_Extension_UnityUtil_3019257939.h"
#include "System.Core_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2531224906.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2531224906MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "Stores_UnityEngine_Purchasing_FakeAmazonExtensions2261777661.h"
#include "Stores_UnityEngine_Purchasing_FakeAmazonExtensions2261777661MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleConfiguatio4052738437.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleConfiguatio4052738437MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleExtensions4039399289.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleExtensions4039399289MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_FakeGooglePlayConfigu737012266.h"
#include "Stores_UnityEngine_Purchasing_FakeGooglePlayConfigu737012266MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_FakeSamsungAppsExten1522853249.h"
#include "Stores_UnityEngine_Purchasing_FakeSamsungAppsExten1522853249MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsMode2214306743.h"
#include "Stores_UnityEngine_Purchasing_FakeStore3882981564.h"
#include "Stores_UnityEngine_Purchasing_FakeStore3882981564MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte2787096497MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "Stores_UnityEngine_Purchasing_FakeStoreUIMode2321492887.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2128260960.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CRetriev2692708567MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2687388655MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2128260960MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1573242544MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte3318267523MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2790035381MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CRetriev2692708567.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1573242544.h"
#include "System_Core_System_Action_2_gen2790035381.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2687388655.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Decimal724701077MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte3318267523.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_DialogType1733969544.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CPurchas2000504963MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1158962578MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CPurchas2000504963.h"
#include "System_Core_System_Action_2_gen1158962578.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod2754455291.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "mscorlib_System_Guid2533601593MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte1607114611MethodDeclarations.h"
#include "mscorlib_System_Guid2533601593.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte1607114611.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_DialogType1733969544MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_FakeStoreUIMode2321492887MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_FakeTizenStoreConfig3055550456.h"
#include "Stores_UnityEngine_Purchasing_FakeTizenStoreConfig3055550456MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_JavaBridge44746847.h"
#include "Stores_UnityEngine_Purchasing_JavaBridge44746847MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaProxy4274989947MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_JSONSerializer501879906.h"
#include "Common_UnityEngine_Purchasing_MiniJson838727235MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_SerializationExtensi3531056818MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Enum2459695545MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "Stores_UnityEngine_Purchasing_RawStoreProvider3477922056.h"
#include "Stores_UnityEngine_Purchasing_RawStoreProvider3477922056MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass2973420583MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_ScriptingUnityCallbac906080071MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsStoreExte3441062041MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass2973420583.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsStoreExte3441062041.h"
#include "Stores_UnityEngine_Purchasing_ScriptingUnityCallbac906080071.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "Apple_UnityEngine_Purchasing_iOSStoreBindings2633471826MethodDeclarations.h"
#include "Apple_UnityEngine_Purchasing_OSXStoreBindings116576999MethodDeclarations.h"
#include "Apple_UnityEngine_Purchasing_iOSStoreBindings2633471826.h"
#include "Apple_UnityEngine_Purchasing_OSXStoreBindings116576999.h"
#include "Tizen_UnityEngine_Purchasing_TizenStoreBindings1951392817MethodDeclarations.h"
#include "Tizen_UnityEngine_Purchasing_TizenStoreBindings1951392817.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsMode2214306743MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_SerializationExtensi3531056818.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte4102635892MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_StoreConfiguration2466794143MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "Stores_UnityEngine_Purchasing_StoreConfiguration2466794143.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo1310600573MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMod107230755MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte4102635892.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo1310600573.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMod107230755.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2149139222MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2149139222.h"
#include "winrt_UnityEngine_Purchasing_Default_Factory1430638288MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_WinRTStore36043095MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_WinRTStore36043095.h"
#include "Stores_UnityEngine_Purchasing_TizenStoreImpl274247241MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_TizenStoreImpl274247241.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore3684252124MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore3684252124.h"
#include "Tizen_UnityEngine_Purchasing_UnityNativePurchasing3230812225MethodDeclarations.h"
#include "Tizen_UnityEngine_Purchasing_UnityNativePurchasing3230812225.h"
#include "System_Core_System_Action_2_gen1907880187.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_DialogRe2092195449MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_DialogRe2092195449.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown1985816271MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1789388632MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData2420267500MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2110227463MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_Lifecycl1057582876.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneIn70867863.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown1985816271.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1789388632.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData2420267500.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2455055323.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownEve2203087800.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2958801606MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "System_Core_System_Func_2_gen2958801606.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1907880187MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_Lifecycl1057582876MethodDeclarations.h"
#include "System_Core_System_Func_2_gen460188795MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2004692778MethodDeclarations.h"
#include "System_Core_System_Func_2_gen460188795.h"
#include "System_Core_System_Func_2_gen2004692778.h"
#include "winrt_UnityEngine_Purchasing_Default_WinProductDes1075111405.h"
#include "mscorlib_System_Collections_Generic_List_1_gen444232537.h"
#include "winrt_UnityEngine_Purchasing_Default_WinProductDes1075111405MethodDeclarations.h"

// System.Boolean UnityEngine.Purchasing.FakeStore::StartUI<UnityEngine.Purchasing.InitializationFailureReason>(System.Object,UnityEngine.Purchasing.FakeStore/DialogType,System.Action`2<System.Boolean,T>)
extern "C"  bool FakeStore_StartUI_TisInitializationFailureReason_t2954032642_m3575193201_gshared (FakeStore_t3882981564 * __this, Il2CppObject * ___model0, int32_t ___dialogType1, Action_2_t2790035381 * ___callback2, const MethodInfo* method);
#define FakeStore_StartUI_TisInitializationFailureReason_t2954032642_m3575193201(__this, ___model0, ___dialogType1, ___callback2, method) ((  bool (*) (FakeStore_t3882981564 *, Il2CppObject *, int32_t, Action_2_t2790035381 *, const MethodInfo*))FakeStore_StartUI_TisInitializationFailureReason_t2954032642_m3575193201_gshared)(__this, ___model0, ___dialogType1, ___callback2, method)
// System.Boolean UnityEngine.Purchasing.FakeStore::StartUI<UnityEngine.Purchasing.PurchaseFailureReason>(System.Object,UnityEngine.Purchasing.FakeStore/DialogType,System.Action`2<System.Boolean,T>)
extern "C"  bool FakeStore_StartUI_TisPurchaseFailureReason_t1322959839_m3432893130_gshared (FakeStore_t3882981564 * __this, Il2CppObject * ___model0, int32_t ___dialogType1, Action_2_t1158962578 * ___callback2, const MethodInfo* method);
#define FakeStore_StartUI_TisPurchaseFailureReason_t1322959839_m3432893130(__this, ___model0, ___dialogType1, ___callback2, method) ((  bool (*) (FakeStore_t3882981564 *, Il2CppObject *, int32_t, Action_2_t1158962578 *, const MethodInfo*))FakeStore_StartUI_TisPurchaseFailureReason_t1322959839_m3432893130_gshared)(__this, ___model0, ___dialogType1, ___callback2, method)
// !!0 UnityEngine.AndroidJavaObject::CallStatic<System.Object>(System.String,System.Object[])
extern "C"  Il2CppObject * AndroidJavaObject_CallStatic_TisIl2CppObject_m3665345648_gshared (AndroidJavaObject_t4251328308 * __this, String_t* p0, ObjectU5BU5D_t3614634134* p1, const MethodInfo* method);
#define AndroidJavaObject_CallStatic_TisIl2CppObject_m3665345648(__this, p0, p1, method) ((  Il2CppObject * (*) (AndroidJavaObject_t4251328308 *, String_t*, ObjectU5BU5D_t3614634134*, const MethodInfo*))AndroidJavaObject_CallStatic_TisIl2CppObject_m3665345648_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.AndroidJavaObject::CallStatic<UnityEngine.AndroidJavaObject>(System.String,System.Object[])
#define AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4251328308_m2569078019(__this, p0, p1, method) ((  AndroidJavaObject_t4251328308 * (*) (AndroidJavaObject_t4251328308 *, String_t*, ObjectU5BU5D_t3614634134*, const MethodInfo*))AndroidJavaObject_CallStatic_TisIl2CppObject_m3665345648_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Purchasing.Extension.IPurchasingBinder::RegisterExtension<System.Object>(!!0)
extern "C"  void IPurchasingBinder_RegisterExtension_TisIl2CppObject_m1972029746_gshared (Il2CppObject * __this, Il2CppObject * p0, const MethodInfo* method);
#define IPurchasingBinder_RegisterExtension_TisIl2CppObject_m1972029746(__this, p0, method) ((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IPurchasingBinder_RegisterExtension_TisIl2CppObject_m1972029746_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.IPurchasingBinder::RegisterExtension<UnityEngine.Purchasing.IAmazonExtensions>(!!0)
#define IPurchasingBinder_RegisterExtension_TisIAmazonExtensions_t3890253245_m4006255990(__this, p0, method) ((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IPurchasingBinder_RegisterExtension_TisIl2CppObject_m1972029746_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.IPurchasingBinder::RegisterConfiguration<System.Object>(!!0)
extern "C"  void IPurchasingBinder_RegisterConfiguration_TisIl2CppObject_m1643289871_gshared (Il2CppObject * __this, Il2CppObject * p0, const MethodInfo* method);
#define IPurchasingBinder_RegisterConfiguration_TisIl2CppObject_m1643289871(__this, p0, method) ((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IPurchasingBinder_RegisterConfiguration_TisIl2CppObject_m1643289871_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.IPurchasingBinder::RegisterConfiguration<UnityEngine.Purchasing.IAmazonConfiguration>(!!0)
#define IPurchasingBinder_RegisterConfiguration_TisIAmazonConfiguration_t3016942165_m2497941269(__this, p0, method) ((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IPurchasingBinder_RegisterConfiguration_TisIl2CppObject_m1643289871_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.IPurchasingBinder::RegisterExtension<UnityEngine.Purchasing.ISamsungAppsExtensions>(!!0)
#define IPurchasingBinder_RegisterExtension_TisISamsungAppsExtensions_t3429739537_m113369440(__this, p0, method) ((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IPurchasingBinder_RegisterExtension_TisIl2CppObject_m1972029746_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.IPurchasingBinder::RegisterConfiguration<UnityEngine.Purchasing.ISamsungAppsConfiguration>(!!0)
#define IPurchasingBinder_RegisterConfiguration_TisISamsungAppsConfiguration_t4066821689_m1118266513(__this, p0, method) ((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IPurchasingBinder_RegisterConfiguration_TisIl2CppObject_m1643289871_gshared)(__this, p0, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m4099813343_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m4099813343(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m4099813343_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Purchasing.Extension.UnityUtil>()
#define GameObject_AddComponent_TisUnityUtil_t166323129_m3875097307(__this, method) ((  UnityUtil_t166323129 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m4099813343_gshared)(__this, method)
// System.Void UnityEngine.Purchasing.Extension.AbstractPurchasingModule::BindExtension<System.Object>(!!0)
extern "C"  void AbstractPurchasingModule_BindExtension_TisIl2CppObject_m395336043_gshared (AbstractPurchasingModule_t4102635892 * __this, Il2CppObject * p0, const MethodInfo* method);
#define AbstractPurchasingModule_BindExtension_TisIl2CppObject_m395336043(__this, p0, method) ((  void (*) (AbstractPurchasingModule_t4102635892 *, Il2CppObject *, const MethodInfo*))AbstractPurchasingModule_BindExtension_TisIl2CppObject_m395336043_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.AbstractPurchasingModule::BindExtension<UnityEngine.Purchasing.IAppleExtensions>(!!0)
#define AbstractPurchasingModule_BindExtension_TisIAppleExtensions_t1627764765_m2766167481(__this, p0, method) ((  void (*) (AbstractPurchasingModule_t4102635892 *, Il2CppObject *, const MethodInfo*))AbstractPurchasingModule_BindExtension_TisIl2CppObject_m395336043_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.AbstractPurchasingModule::BindExtension<UnityEngine.Purchasing.IAmazonExtensions>(!!0)
#define AbstractPurchasingModule_BindExtension_TisIAmazonExtensions_t3890253245_m2976483159(__this, p0, method) ((  void (*) (AbstractPurchasingModule_t4102635892 *, Il2CppObject *, const MethodInfo*))AbstractPurchasingModule_BindExtension_TisIl2CppObject_m395336043_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.AbstractPurchasingModule::BindExtension<UnityEngine.Purchasing.ISamsungAppsExtensions>(!!0)
#define AbstractPurchasingModule_BindExtension_TisISamsungAppsExtensions_t3429739537_m2635797151(__this, p0, method) ((  void (*) (AbstractPurchasingModule_t4102635892 *, Il2CppObject *, const MethodInfo*))AbstractPurchasingModule_BindExtension_TisIl2CppObject_m395336043_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.AbstractPurchasingModule::BindConfiguration<System.Object>(!!0)
extern "C"  void AbstractPurchasingModule_BindConfiguration_TisIl2CppObject_m1650788188_gshared (AbstractPurchasingModule_t4102635892 * __this, Il2CppObject * p0, const MethodInfo* method);
#define AbstractPurchasingModule_BindConfiguration_TisIl2CppObject_m1650788188(__this, p0, method) ((  void (*) (AbstractPurchasingModule_t4102635892 *, Il2CppObject *, const MethodInfo*))AbstractPurchasingModule_BindConfiguration_TisIl2CppObject_m1650788188_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.AbstractPurchasingModule::BindConfiguration<UnityEngine.Purchasing.IAppleConfiguration>(!!0)
#define AbstractPurchasingModule_BindConfiguration_TisIAppleConfiguration_t3277762425_m3106281156(__this, p0, method) ((  void (*) (AbstractPurchasingModule_t4102635892 *, Il2CppObject *, const MethodInfo*))AbstractPurchasingModule_BindConfiguration_TisIl2CppObject_m1650788188_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.AbstractPurchasingModule::BindConfiguration<UnityEngine.Purchasing.IMicrosoftConfiguration>(!!0)
#define AbstractPurchasingModule_BindConfiguration_TisIMicrosoftConfiguration_t1212838845_m2316685840(__this, p0, method) ((  void (*) (AbstractPurchasingModule_t4102635892 *, Il2CppObject *, const MethodInfo*))AbstractPurchasingModule_BindConfiguration_TisIl2CppObject_m1650788188_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.AbstractPurchasingModule::BindConfiguration<UnityEngine.Purchasing.IGooglePlayConfiguration>(!!0)
#define AbstractPurchasingModule_BindConfiguration_TisIGooglePlayConfiguration_t2615679878_m4220697187(__this, p0, method) ((  void (*) (AbstractPurchasingModule_t4102635892 *, Il2CppObject *, const MethodInfo*))AbstractPurchasingModule_BindConfiguration_TisIl2CppObject_m1650788188_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.AbstractPurchasingModule::BindConfiguration<UnityEngine.Purchasing.IAmazonConfiguration>(!!0)
#define AbstractPurchasingModule_BindConfiguration_TisIAmazonConfiguration_t3016942165_m631587310(__this, p0, method) ((  void (*) (AbstractPurchasingModule_t4102635892 *, Il2CppObject *, const MethodInfo*))AbstractPurchasingModule_BindConfiguration_TisIl2CppObject_m1650788188_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.AbstractPurchasingModule::BindConfiguration<UnityEngine.Purchasing.ISamsungAppsConfiguration>(!!0)
#define AbstractPurchasingModule_BindConfiguration_TisISamsungAppsConfiguration_t4066821689_m610130824(__this, p0, method) ((  void (*) (AbstractPurchasingModule_t4102635892 *, Il2CppObject *, const MethodInfo*))AbstractPurchasingModule_BindConfiguration_TisIl2CppObject_m1650788188_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.AbstractPurchasingModule::BindConfiguration<UnityEngine.Purchasing.ITizenStoreConfiguration>(!!0)
#define AbstractPurchasingModule_BindConfiguration_TisITizenStoreConfiguration_t2900348728_m3915163361(__this, p0, method) ((  void (*) (AbstractPurchasingModule_t4102635892 *, Il2CppObject *, const MethodInfo*))AbstractPurchasingModule_BindConfiguration_TisIl2CppObject_m1650788188_gshared)(__this, p0, method)
// System.Void UnityEngine.Purchasing.Extension.AbstractPurchasingModule::BindConfiguration<UnityEngine.Purchasing.IAndroidStoreSelection>(!!0)
#define AbstractPurchasingModule_BindConfiguration_TisIAndroidStoreSelection_t3134941501_m905543658(__this, p0, method) ((  void (*) (AbstractPurchasingModule_t4102635892 *, Il2CppObject *, const MethodInfo*))AbstractPurchasingModule_BindConfiguration_TisIl2CppObject_m1650788188_gshared)(__this, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Canvas>()
#define GameObject_GetComponent_TisCanvas_t209405766_m195193039(__this, method) ((  Canvas_t209405766 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m447919519(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.Canvas>(!!0)
#define Object_Instantiate_TisCanvas_t209405766_m805779209(__this /* static, unused */, p0, method) ((  Canvas_t209405766 * (*) (Il2CppObject * /* static, unused */, Canvas_t209405766 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Purchasing.UIFakeStore/LifecycleNotifier>()
#define GameObject_AddComponent_TisLifecycleNotifier_t1057582876_m1564113664(__this, method) ((  LifecycleNotifier_t1057582876 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m4099813343_gshared)(__this, method)
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m1798760551_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m1798760551(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m1798760551_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<UnityEngine.EventSystems.EventSystem>()
#define Object_FindObjectOfType_TisEventSystem_t3466835263_m929139623(__this /* static, unused */, method) ((  EventSystem_t3466835263 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m1798760551_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.EventSystems.StandaloneInputModule>()
#define GameObject_AddComponent_TisStandaloneInputModule_t70867863_m4180824674(__this, method) ((  StandaloneInputModule_t70867863 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m4099813343_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m4280536079(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  Il2CppObject* Enumerable_Take_TisIl2CppObject_m2048499799_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, const MethodInfo* method);
#define Enumerable_Take_TisIl2CppObject_m2048499799(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_Take_TisIl2CppObject_m2048499799_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<UnityEngine.Purchasing.ProductDefinition>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_Take_TisProductDefinition_t1942475268_m2540339846(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_Take_TisIl2CppObject_m2048499799_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m737819943_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2825504181 * p1, const MethodInfo* method);
#define Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m737819943(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2825504181 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m737819943_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Purchasing.ProductDefinition,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisProductDefinition_t1942475268_TisString_t_m2207542709(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2958801606 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m737819943_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m3301715283(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisString_t_m1953054010(__this /* static, unused */, p0, method) ((  StringU5BU5D_t1642385972* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t2872111280_m3862106414(__this, method) ((  Button_t2872111280 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Dropdown>()
#define GameObject_GetComponent_TisDropdown_t1985816271_m1750975685(__this, method) ((  Dropdown_t1985816271 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Enumerable_Where_TisIl2CppObject_m4266917885_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3961629604 * p1, const MethodInfo* method);
#define Enumerable_Where_TisIl2CppObject_m4266917885(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3961629604 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m4266917885_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityEngine.Purchasing.ProductDefinition>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisProductDefinition_t1942475268_m96461137(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t460188795 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m4266917885_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Purchasing.ProductDefinition,UnityEngine.Purchasing.Default.WinProductDescription>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisProductDefinition_t1942475268_TisWinProductDescription_t1075111405_m2891196840(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2004692778 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m737819943_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t2058570427 * Enumerable_ToList_TisIl2CppObject_m3361462832_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisIl2CppObject_m3361462832(__this /* static, unused */, p0, method) ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3361462832_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityEngine.Purchasing.Default.WinProductDescription>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisWinProductDescription_t1075111405_m820579046(__this /* static, unused */, p0, method) ((  List_1_t444232537 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3361462832_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Purchasing.AmazonAppStoreStoreExtensions::.ctor(UnityEngine.AndroidJavaObject)
extern "C"  void AmazonAppStoreStoreExtensions__ctor_m2576379724 (AmazonAppStoreStoreExtensions_t1518886395 * __this, AndroidJavaObject_t4251328308 * ___a0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		AndroidJavaObject_t4251328308 * L_0 = ___a0;
		__this->set_android_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.AmazonAppStoreStoreExtensions::WriteSandboxJSON(System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2247694030;
extern const uint32_t AmazonAppStoreStoreExtensions_WriteSandboxJSON_m3044053010_MetadataUsageId;
extern "C"  void AmazonAppStoreStoreExtensions_WriteSandboxJSON_m3044053010 (AmazonAppStoreStoreExtensions_t1518886395 * __this, HashSet_1_t275936122 * ___products0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AmazonAppStoreStoreExtensions_WriteSandboxJSON_m3044053010_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AndroidJavaObject_t4251328308 * L_0 = __this->get_android_0();
		ObjectU5BU5D_t3614634134* L_1 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		HashSet_1_t275936122 * L_2 = ___products0;
		String_t* L_3 = JSONSerializer_SerializeProductDefs_m2515728532(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck(L_0);
		AndroidJavaObject_Call_m3681854287(L_0, _stringLiteral2247694030, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.AndroidJavaStore::.ctor(UnityEngine.AndroidJavaObject)
extern "C"  void AndroidJavaStore__ctor_m2037585869 (AndroidJavaStore_t2772549594 * __this, AndroidJavaObject_t4251328308 * ___store0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		AndroidJavaObject_t4251328308 * L_0 = ___store0;
		__this->set_m_Store_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.AndroidJavaStore::RetrieveProducts(System.String)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1551069126;
extern const uint32_t AndroidJavaStore_RetrieveProducts_m1546538144_MetadataUsageId;
extern "C"  void AndroidJavaStore_RetrieveProducts_m1546538144 (AndroidJavaStore_t2772549594 * __this, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaStore_RetrieveProducts_m1546538144_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AndroidJavaObject_t4251328308 * L_0 = __this->get_m_Store_0();
		ObjectU5BU5D_t3614634134* L_1 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_2 = ___json0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		NullCheck(L_0);
		AndroidJavaObject_Call_m3681854287(L_0, _stringLiteral1551069126, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.AndroidJavaStore::Purchase(System.String,System.String)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4093369807;
extern const uint32_t AndroidJavaStore_Purchase_m1901468879_MetadataUsageId;
extern "C"  void AndroidJavaStore_Purchase_m1901468879 (AndroidJavaStore_t2772549594 * __this, String_t* ___productJSON0, String_t* ___developerPayload1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaStore_Purchase_m1901468879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AndroidJavaObject_t4251328308 * L_0 = __this->get_m_Store_0();
		ObjectU5BU5D_t3614634134* L_1 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		String_t* L_2 = ___productJSON0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t3614634134* L_3 = L_1;
		String_t* L_4 = ___developerPayload1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		NullCheck(L_0);
		AndroidJavaObject_Call_m3681854287(L_0, _stringLiteral4093369807, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.AndroidJavaStore::FinishTransaction(System.String,System.String)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4049028819;
extern const uint32_t AndroidJavaStore_FinishTransaction_m4018781023_MetadataUsageId;
extern "C"  void AndroidJavaStore_FinishTransaction_m4018781023 (AndroidJavaStore_t2772549594 * __this, String_t* ___productJSON0, String_t* ___transactionID1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaStore_FinishTransaction_m4018781023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AndroidJavaObject_t4251328308 * L_0 = __this->get_m_Store_0();
		ObjectU5BU5D_t3614634134* L_1 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		String_t* L_2 = ___productJSON0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t3614634134* L_3 = L_1;
		String_t* L_4 = ___transactionID1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		NullCheck(L_0);
		AndroidJavaObject_Call_m3681854287(L_0, _stringLiteral4049028819, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.AppleStoreImpl::.ctor(Uniject.IUtil)
extern Il2CppClass* AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var;
extern const uint32_t AppleStoreImpl__ctor_m1783914938_MetadataUsageId;
extern "C"  void AppleStoreImpl__ctor_m1783914938 (AppleStoreImpl_t1301617341 * __this, Il2CppObject * ___util0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleStoreImpl__ctor_m1783914938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NativeJSONStore__ctor_m812188232(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___util0;
		((AppleStoreImpl_t1301617341_StaticFields*)AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var->static_fields)->set_util_7(L_0);
		((AppleStoreImpl_t1301617341_StaticFields*)AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var->static_fields)->set_instance_8(__this);
		return;
	}
}
// System.Void UnityEngine.Purchasing.AppleStoreImpl::SetNativeStore(UnityEngine.Purchasing.INativeAppleStore)
extern Il2CppClass* AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityPurchasingCallback_t2635187846_il2cpp_TypeInfo_var;
extern Il2CppClass* INativeAppleStore_t2240226449_il2cpp_TypeInfo_var;
extern const MethodInfo* AppleStoreImpl_MessageCallback_m2420291589_MethodInfo_var;
extern const uint32_t AppleStoreImpl_SetNativeStore_m4003342543_MetadataUsageId;
extern "C"  void AppleStoreImpl_SetNativeStore_m4003342543 (AppleStoreImpl_t1301617341 * __this, Il2CppObject * ___apple0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleStoreImpl_SetNativeStore_m4003342543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject * L_0 = ___apple0;
		NativeJSONStore_SetNativeStore_m1863195906(__this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___apple0;
		__this->set_m_Native_6(L_1);
		Il2CppObject * L_2 = ___apple0;
		UnityPurchasingCallback_t2635187846 * L_3 = ((AppleStoreImpl_t1301617341_StaticFields*)AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_9();
		G_B1_0 = L_2;
		if (L_3)
		{
			G_B2_0 = L_2;
			goto IL_0028;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)AppleStoreImpl_MessageCallback_m2420291589_MethodInfo_var);
		UnityPurchasingCallback_t2635187846 * L_5 = (UnityPurchasingCallback_t2635187846 *)il2cpp_codegen_object_new(UnityPurchasingCallback_t2635187846_il2cpp_TypeInfo_var);
		UnityPurchasingCallback__ctor_m1008473938(L_5, NULL, L_4, /*hidden argument*/NULL);
		((AppleStoreImpl_t1301617341_StaticFields*)AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_9(L_5);
		G_B2_0 = G_B1_0;
	}

IL_0028:
	{
		UnityPurchasingCallback_t2635187846 * L_6 = ((AppleStoreImpl_t1301617341_StaticFields*)AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_9();
		NullCheck(G_B2_0);
		InterfaceActionInvoker1< UnityPurchasingCallback_t2635187846 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.INativeAppleStore::SetUnityPurchasingCallback(UnityEngine.Purchasing.UnityPurchasingCallback) */, INativeAppleStore_t2240226449_il2cpp_TypeInfo_var, G_B2_0, L_6);
		return;
	}
}
// System.Void UnityEngine.Purchasing.AppleStoreImpl::OnProductsRetrieved(System.String)
extern Il2CppClass* INativeAppleStore_t2240226449_il2cpp_TypeInfo_var;
extern const uint32_t AppleStoreImpl_OnProductsRetrieved_m3960249760_MetadataUsageId;
extern "C"  void AppleStoreImpl_OnProductsRetrieved_m3960249760 (AppleStoreImpl_t1301617341 * __this, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleStoreImpl_OnProductsRetrieved_m3960249760_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___json0;
		NativeJSONStore_OnProductsRetrieved_m4005515159(__this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = __this->get_m_Native_6();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(2 /* System.Void UnityEngine.Purchasing.INativeAppleStore::AddTransactionObserver() */, INativeAppleStore_t2240226449_il2cpp_TypeInfo_var, L_1);
		return;
	}
}
// System.Void UnityEngine.Purchasing.AppleStoreImpl::RestoreTransactions(System.Action`1<System.Boolean>)
extern Il2CppClass* INativeAppleStore_t2240226449_il2cpp_TypeInfo_var;
extern const uint32_t AppleStoreImpl_RestoreTransactions_m3660952369_MetadataUsageId;
extern "C"  void AppleStoreImpl_RestoreTransactions_m3660952369 (AppleStoreImpl_t1301617341 * __this, Action_1_t3627374100 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleStoreImpl_RestoreTransactions_m3660952369_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = ___callback0;
		__this->set_m_RestoreCallback_5(L_0);
		Il2CppObject * L_1 = __this->get_m_Native_6();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(1 /* System.Void UnityEngine.Purchasing.INativeAppleStore::RestoreTransactions() */, INativeAppleStore_t2240226449_il2cpp_TypeInfo_var, L_1);
		return;
	}
}
// System.Void UnityEngine.Purchasing.AppleStoreImpl::RegisterPurchaseDeferredListener(System.Action`1<UnityEngine.Purchasing.Product>)
extern "C"  void AppleStoreImpl_RegisterPurchaseDeferredListener_m1897237874 (AppleStoreImpl_t1301617341 * __this, Action_1_t1005487353 * ___callback0, const MethodInfo* method)
{
	{
		Action_1_t1005487353 * L_0 = ___callback0;
		__this->set_m_DeferredCallback_2(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.AppleStoreImpl::OnPurchaseDeferred(System.String)
extern Il2CppClass* IStoreCallback_t2691517565_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2545641130_MethodInfo_var;
extern const uint32_t AppleStoreImpl_OnPurchaseDeferred_m2745090322_MetadataUsageId;
extern "C"  void AppleStoreImpl_OnPurchaseDeferred_m2745090322 (AppleStoreImpl_t1301617341 * __this, String_t* ___productId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleStoreImpl_OnPurchaseDeferred_m2745090322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Product_t1203687971 * V_0 = NULL;
	{
		Action_1_t1005487353 * L_0 = __this->get_m_DeferredCallback_2();
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		Il2CppObject * L_1 = ((NativeJSONStore_t3685388740 *)__this)->get_unity_0();
		NullCheck(L_1);
		ProductCollection_t3600019299 * L_2 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.Extension.IStoreCallback::get_products() */, IStoreCallback_t2691517565_il2cpp_TypeInfo_var, L_1);
		String_t* L_3 = ___productId0;
		NullCheck(L_2);
		Product_t1203687971 * L_4 = ProductCollection_WithStoreSpecificID_m1010132077(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Product_t1203687971 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		Action_1_t1005487353 * L_6 = __this->get_m_DeferredCallback_2();
		Product_t1203687971 * L_7 = V_0;
		NullCheck(L_6);
		Action_1_Invoke_m2545641130(L_6, L_7, /*hidden argument*/Action_1_Invoke_m2545641130_MethodInfo_var);
	}

IL_0031:
	{
	}

IL_0032:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.AppleStoreImpl::OnTransactionsRestoredSuccess()
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t AppleStoreImpl_OnTransactionsRestoredSuccess_m3905359892_MetadataUsageId;
extern "C"  void AppleStoreImpl_OnTransactionsRestoredSuccess_m3905359892 (AppleStoreImpl_t1301617341 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleStoreImpl_OnTransactionsRestoredSuccess_m3905359892_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = __this->get_m_RestoreCallback_5();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t3627374100 * L_1 = __this->get_m_RestoreCallback_5();
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, (bool)1, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.AppleStoreImpl::OnTransactionsRestoredFail(System.String)
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t AppleStoreImpl_OnTransactionsRestoredFail_m1718101857_MetadataUsageId;
extern "C"  void AppleStoreImpl_OnTransactionsRestoredFail_m1718101857 (AppleStoreImpl_t1301617341 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleStoreImpl_OnTransactionsRestoredFail_m1718101857_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = __this->get_m_RestoreCallback_5();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t3627374100 * L_1 = __this->get_m_RestoreCallback_5();
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.AppleStoreImpl::OnAppReceiptRetrieved(System.String)
extern const MethodInfo* Action_1_Invoke_m3811540703_MethodInfo_var;
extern const uint32_t AppleStoreImpl_OnAppReceiptRetrieved_m1421778187_MetadataUsageId;
extern "C"  void AppleStoreImpl_OnAppReceiptRetrieved_m1421778187 (AppleStoreImpl_t1301617341 * __this, String_t* ___receipt0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleStoreImpl_OnAppReceiptRetrieved_m1421778187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___receipt0;
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Action_1_t1831019615 * L_1 = __this->get_m_RefreshReceiptSuccess_4();
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Action_1_t1831019615 * L_2 = __this->get_m_RefreshReceiptSuccess_4();
		String_t* L_3 = ___receipt0;
		NullCheck(L_2);
		Action_1_Invoke_m3811540703(L_2, L_3, /*hidden argument*/Action_1_Invoke_m3811540703_MethodInfo_var);
	}

IL_001f:
	{
	}

IL_0020:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.AppleStoreImpl::OnAppReceiptRefreshedFailed()
extern "C"  void AppleStoreImpl_OnAppReceiptRefreshedFailed_m45452970 (AppleStoreImpl_t1301617341 * __this, const MethodInfo* method)
{
	{
		Action_t3226471752 * L_0 = __this->get_m_RefreshReceiptError_3();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_t3226471752 * L_1 = __this->get_m_RefreshReceiptError_3();
		NullCheck(L_1);
		Action_Invoke_m3801112262(L_1, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.AppleStoreImpl::MessageCallback(System.String,System.String,System.String,System.String)
extern Il2CppClass* U3CMessageCallbackU3Ec__AnonStorey0_t2294210682_il2cpp_TypeInfo_var;
extern Il2CppClass* AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern Il2CppClass* IUtil_t2188430191_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CMessageCallbackU3Ec__AnonStorey0_U3CU3Em__0_m2666828050_MethodInfo_var;
extern const uint32_t AppleStoreImpl_MessageCallback_m2420291589_MetadataUsageId;
extern "C"  void AppleStoreImpl_MessageCallback_m2420291589 (Il2CppObject * __this /* static, unused */, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleStoreImpl_MessageCallback_m2420291589_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMessageCallbackU3Ec__AnonStorey0_t2294210682 * V_0 = NULL;
	{
		U3CMessageCallbackU3Ec__AnonStorey0_t2294210682 * L_0 = (U3CMessageCallbackU3Ec__AnonStorey0_t2294210682 *)il2cpp_codegen_object_new(U3CMessageCallbackU3Ec__AnonStorey0_t2294210682_il2cpp_TypeInfo_var);
		U3CMessageCallbackU3Ec__AnonStorey0__ctor_m3816251131(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMessageCallbackU3Ec__AnonStorey0_t2294210682 * L_1 = V_0;
		String_t* L_2 = ___subject0;
		NullCheck(L_1);
		L_1->set_subject_0(L_2);
		U3CMessageCallbackU3Ec__AnonStorey0_t2294210682 * L_3 = V_0;
		String_t* L_4 = ___payload1;
		NullCheck(L_3);
		L_3->set_payload_1(L_4);
		U3CMessageCallbackU3Ec__AnonStorey0_t2294210682 * L_5 = V_0;
		String_t* L_6 = ___receipt2;
		NullCheck(L_5);
		L_5->set_receipt_2(L_6);
		U3CMessageCallbackU3Ec__AnonStorey0_t2294210682 * L_7 = V_0;
		String_t* L_8 = ___transactionId3;
		NullCheck(L_7);
		L_7->set_transactionId_3(L_8);
		Il2CppObject * L_9 = ((AppleStoreImpl_t1301617341_StaticFields*)AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var->static_fields)->get_util_7();
		U3CMessageCallbackU3Ec__AnonStorey0_t2294210682 * L_10 = V_0;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)U3CMessageCallbackU3Ec__AnonStorey0_U3CU3Em__0_m2666828050_MethodInfo_var);
		Action_t3226471752 * L_12 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_12, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		InterfaceActionInvoker1< Action_t3226471752 * >::Invoke(2 /* System.Void Uniject.IUtil::RunOnMainThread(System.Action) */, IUtil_t2188430191_il2cpp_TypeInfo_var, L_9, L_12);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_AppleStoreImpl_MessageCallback_m2420291589(char* ___subject0, char* ___payload1, char* ___receipt2, char* ___transactionId3)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___subject0' to managed representation
	String_t* ____subject0_unmarshaled = NULL;
	____subject0_unmarshaled = il2cpp_codegen_marshal_string_result(___subject0);

	// Marshaling of parameter '___payload1' to managed representation
	String_t* ____payload1_unmarshaled = NULL;
	____payload1_unmarshaled = il2cpp_codegen_marshal_string_result(___payload1);

	// Marshaling of parameter '___receipt2' to managed representation
	String_t* ____receipt2_unmarshaled = NULL;
	____receipt2_unmarshaled = il2cpp_codegen_marshal_string_result(___receipt2);

	// Marshaling of parameter '___transactionId3' to managed representation
	String_t* ____transactionId3_unmarshaled = NULL;
	____transactionId3_unmarshaled = il2cpp_codegen_marshal_string_result(___transactionId3);

	// Managed method invocation
	AppleStoreImpl_MessageCallback_m2420291589(NULL, ____subject0_unmarshaled, ____payload1_unmarshaled, ____receipt2_unmarshaled, ____transactionId3_unmarshaled, NULL);

}
// System.Void UnityEngine.Purchasing.AppleStoreImpl::ProcessMessage(System.String,System.String,System.String,System.String)
extern Il2CppClass* AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3986656710_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2118310873_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1209957957_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m2977303364_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral843374921;
extern Il2CppCodeGenString* _stringLiteral585622411;
extern Il2CppCodeGenString* _stringLiteral558302001;
extern Il2CppCodeGenString* _stringLiteral3311547177;
extern Il2CppCodeGenString* _stringLiteral2814477002;
extern Il2CppCodeGenString* _stringLiteral1222138513;
extern Il2CppCodeGenString* _stringLiteral1765947696;
extern Il2CppCodeGenString* _stringLiteral3313331908;
extern Il2CppCodeGenString* _stringLiteral2058929592;
extern const uint32_t AppleStoreImpl_ProcessMessage_m1576304923_MetadataUsageId;
extern "C"  void AppleStoreImpl_ProcessMessage_m1576304923 (AppleStoreImpl_t1301617341 * __this, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleStoreImpl_ProcessMessage_m1576304923_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t3986656710 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___subject0;
		if (!L_0)
		{
			goto IL_0139;
		}
	}
	{
		Dictionary_2_t3986656710 * L_1 = ((AppleStoreImpl_t1301617341_StaticFields*)AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_10();
		if (L_1)
		{
			goto IL_008b;
		}
	}
	{
		Dictionary_2_t3986656710 * L_2 = (Dictionary_2_t3986656710 *)il2cpp_codegen_object_new(Dictionary_2_t3986656710_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2118310873(L_2, ((int32_t)9), /*hidden argument*/Dictionary_2__ctor_m2118310873_MethodInfo_var);
		V_0 = L_2;
		Dictionary_2_t3986656710 * L_3 = V_0;
		NullCheck(L_3);
		Dictionary_2_Add_m1209957957(L_3, _stringLiteral843374921, 0, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_4 = V_0;
		NullCheck(L_4);
		Dictionary_2_Add_m1209957957(L_4, _stringLiteral585622411, 1, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_5 = V_0;
		NullCheck(L_5);
		Dictionary_2_Add_m1209957957(L_5, _stringLiteral558302001, 2, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_6 = V_0;
		NullCheck(L_6);
		Dictionary_2_Add_m1209957957(L_6, _stringLiteral3311547177, 3, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_7 = V_0;
		NullCheck(L_7);
		Dictionary_2_Add_m1209957957(L_7, _stringLiteral2814477002, 4, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_8 = V_0;
		NullCheck(L_8);
		Dictionary_2_Add_m1209957957(L_8, _stringLiteral1222138513, 5, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_9 = V_0;
		NullCheck(L_9);
		Dictionary_2_Add_m1209957957(L_9, _stringLiteral1765947696, 6, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_10 = V_0;
		NullCheck(L_10);
		Dictionary_2_Add_m1209957957(L_10, _stringLiteral3313331908, 7, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_11 = V_0;
		NullCheck(L_11);
		Dictionary_2_Add_m1209957957(L_11, _stringLiteral2058929592, 8, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_12 = V_0;
		((AppleStoreImpl_t1301617341_StaticFields*)AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map0_10(L_12);
	}

IL_008b:
	{
		Dictionary_2_t3986656710 * L_13 = ((AppleStoreImpl_t1301617341_StaticFields*)AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_10();
		String_t* L_14 = ___subject0;
		NullCheck(L_13);
		bool L_15 = Dictionary_2_TryGetValue_m2977303364(L_13, L_14, (&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m2977303364_MethodInfo_var);
		if (!L_15)
		{
			goto IL_0139;
		}
	}
	{
		int32_t L_16 = V_1;
		if (L_16 == 0)
		{
			goto IL_00cc;
		}
		if (L_16 == 1)
		{
			goto IL_00d8;
		}
		if (L_16 == 2)
		{
			goto IL_00e4;
		}
		if (L_16 == 3)
		{
			goto IL_00f3;
		}
		if (L_16 == 4)
		{
			goto IL_00ff;
		}
		if (L_16 == 5)
		{
			goto IL_010b;
		}
		if (L_16 == 6)
		{
			goto IL_0116;
		}
		if (L_16 == 7)
		{
			goto IL_0122;
		}
		if (L_16 == 8)
		{
			goto IL_012e;
		}
	}
	{
		goto IL_0139;
	}

IL_00cc:
	{
		String_t* L_17 = ___payload1;
		NativeJSONStore_OnSetupFailed_m2728050561(__this, L_17, /*hidden argument*/NULL);
		goto IL_0139;
	}

IL_00d8:
	{
		String_t* L_18 = ___payload1;
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void UnityEngine.Purchasing.NativeJSONStore::OnProductsRetrieved(System.String) */, __this, L_18);
		goto IL_0139;
	}

IL_00e4:
	{
		String_t* L_19 = ___payload1;
		String_t* L_20 = ___receipt2;
		String_t* L_21 = ___transactionId3;
		NativeJSONStore_OnPurchaseSucceeded_m363409449(__this, L_19, L_20, L_21, /*hidden argument*/NULL);
		goto IL_0139;
	}

IL_00f3:
	{
		String_t* L_22 = ___payload1;
		NativeJSONStore_OnPurchaseFailed_m4091409929(__this, L_22, /*hidden argument*/NULL);
		goto IL_0139;
	}

IL_00ff:
	{
		String_t* L_23 = ___payload1;
		AppleStoreImpl_OnPurchaseDeferred_m2745090322(__this, L_23, /*hidden argument*/NULL);
		goto IL_0139;
	}

IL_010b:
	{
		AppleStoreImpl_OnTransactionsRestoredSuccess_m3905359892(__this, /*hidden argument*/NULL);
		goto IL_0139;
	}

IL_0116:
	{
		String_t* L_24 = ___payload1;
		AppleStoreImpl_OnTransactionsRestoredFail_m1718101857(__this, L_24, /*hidden argument*/NULL);
		goto IL_0139;
	}

IL_0122:
	{
		String_t* L_25 = ___payload1;
		AppleStoreImpl_OnAppReceiptRetrieved_m1421778187(__this, L_25, /*hidden argument*/NULL);
		goto IL_0139;
	}

IL_012e:
	{
		AppleStoreImpl_OnAppReceiptRefreshedFailed_m45452970(__this, /*hidden argument*/NULL);
		goto IL_0139;
	}

IL_0139:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.AppleStoreImpl/<MessageCallback>c__AnonStorey0::.ctor()
extern "C"  void U3CMessageCallbackU3Ec__AnonStorey0__ctor_m3816251131 (U3CMessageCallbackU3Ec__AnonStorey0_t2294210682 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.AppleStoreImpl/<MessageCallback>c__AnonStorey0::<>m__0()
extern Il2CppClass* AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var;
extern const uint32_t U3CMessageCallbackU3Ec__AnonStorey0_U3CU3Em__0_m2666828050_MetadataUsageId;
extern "C"  void U3CMessageCallbackU3Ec__AnonStorey0_U3CU3Em__0_m2666828050 (U3CMessageCallbackU3Ec__AnonStorey0_t2294210682 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMessageCallbackU3Ec__AnonStorey0_U3CU3Em__0_m2666828050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AppleStoreImpl_t1301617341 * L_0 = ((AppleStoreImpl_t1301617341_StaticFields*)AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var->static_fields)->get_instance_8();
		String_t* L_1 = __this->get_subject_0();
		String_t* L_2 = __this->get_payload_1();
		String_t* L_3 = __this->get_receipt_2();
		String_t* L_4 = __this->get_transactionId_3();
		NullCheck(L_0);
		AppleStoreImpl_ProcessMessage_m1576304923(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Extension.UnityUtil::.ctor()
extern Il2CppClass* List_1_t2996495232_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m427106774_MethodInfo_var;
extern const uint32_t UnityUtil__ctor_m4064255016_MetadataUsageId;
extern "C"  void UnityUtil__ctor_m4064255016 (UnityUtil_t166323129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil__ctor_m4064255016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2996495232 * L_0 = (List_1_t2996495232 *)il2cpp_codegen_object_new(List_1_t2996495232_il2cpp_TypeInfo_var);
		List_1__ctor_m427106774(L_0, /*hidden argument*/List_1__ctor_m427106774_MethodInfo_var);
		__this->set_pauseListeners_5(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.DateTime UnityEngine.Purchasing.Extension.UnityUtil::get_currentTime()
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t UnityUtil_get_currentTime_m1270862516_MetadataUsageId;
extern "C"  DateTime_t693205669  UnityUtil_get_currentTime_m1270862516 (UnityUtil_t166323129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_get_currentTime_m1270862516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_0 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		DateTime_t693205669  L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Purchasing.Extension.UnityUtil::get_persistentDataPath()
extern "C"  String_t* UnityUtil_get_persistentDataPath_m3216924224 (UnityUtil_t166323129 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.RuntimePlatform UnityEngine.Purchasing.Extension.UnityUtil::get_platform()
extern "C"  int32_t UnityUtil_get_platform_m596824773 (UnityUtil_t166323129 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Purchasing.Extension.UnityUtil::get_isEditor()
extern "C"  bool UnityUtil_get_isEditor_m1751270940 (UnityUtil_t166323129 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Purchasing.Extension.UnityUtil::get_deviceModel()
extern "C"  String_t* UnityUtil_get_deviceModel_m1083680435 (UnityUtil_t166323129 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = SystemInfo_get_deviceModel_m3856615649(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Purchasing.Extension.UnityUtil::get_deviceName()
extern "C"  String_t* UnityUtil_get_deviceName_m2747348751 (UnityUtil_t166323129 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = SystemInfo_get_deviceName_m1285252113(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.DeviceType UnityEngine.Purchasing.Extension.UnityUtil::get_deviceType()
extern "C"  int32_t UnityUtil_get_deviceType_m4094986889 (UnityUtil_t166323129 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = SystemInfo_get_deviceType_m3581258207(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Purchasing.Extension.UnityUtil::get_operatingSystem()
extern "C"  String_t* UnityUtil_get_operatingSystem_m3655500614 (UnityUtil_t166323129 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = SystemInfo_get_operatingSystem_m2575097876(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Object UnityEngine.Purchasing.Extension.UnityUtil::Uniject.IUtil.InitiateCoroutine(System.Collections.IEnumerator)
extern "C"  Il2CppObject * UnityUtil_Uniject_IUtil_InitiateCoroutine_m465974326 (UnityUtil_t166323129 * __this, Il2CppObject * ___start0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___start0;
		Coroutine_t2299508840 * L_1 = MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Purchasing.Extension.UnityUtil::Uniject.IUtil.InitiateCoroutine(System.Collections.IEnumerator,System.Int32)
extern "C"  void UnityUtil_Uniject_IUtil_InitiateCoroutine_m1647651442 (UnityUtil_t166323129 * __this, Il2CppObject * ___start0, int32_t ___delay1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___start0;
		int32_t L_1 = ___delay1;
		UnityUtil_DelayedCoroutine_m1046729072(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Extension.UnityUtil::RunOnMainThread(System.Action)
extern Il2CppClass* UnityUtil_t166323129_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m778054002_MethodInfo_var;
extern const uint32_t UnityUtil_RunOnMainThread_m3553993608_MetadataUsageId;
extern "C"  void UnityUtil_RunOnMainThread_m3553993608 (UnityUtil_t166323129 * __this, Action_t3226471752 * ___runnable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_RunOnMainThread_m3553993608_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityUtil_t166323129_il2cpp_TypeInfo_var);
		List_1_t2595592884 * L_0 = ((UnityUtil_t166323129_StaticFields*)UnityUtil_t166323129_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(UnityUtil_t166323129_il2cpp_TypeInfo_var);
		List_1_t2595592884 * L_2 = ((UnityUtil_t166323129_StaticFields*)UnityUtil_t166323129_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_2();
		Action_t3226471752 * L_3 = ___runnable0;
		NullCheck(L_2);
		List_1_Add_m778054002(L_2, L_3, /*hidden argument*/List_1_Add_m778054002_MethodInfo_var);
		il2cpp_codegen_memory_barrier();
		((UnityUtil_t166323129_StaticFields*)UnityUtil_t166323129_il2cpp_TypeInfo_var->static_fields)->set_s_CallbacksPending_3(1);
		IL2CPP_LEAVE(0x2E, FINALLY_0027);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0027;
	}

FINALLY_0027:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(39)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(39)
	{
		IL2CPP_JUMP_TBL(0x2E, IL_002e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_002e:
	{
		return;
	}
}
// System.Object UnityEngine.Purchasing.Extension.UnityUtil::GetWaitForSeconds(System.Int32)
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t UnityUtil_GetWaitForSeconds_m2083576077_MetadataUsageId;
extern "C"  Il2CppObject * UnityUtil_GetWaitForSeconds_m2083576077 (UnityUtil_t166323129 * __this, int32_t ___seconds0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_GetWaitForSeconds_m2083576077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___seconds0;
		WaitForSeconds_t3839502067 * L_1 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_1, (((float)((float)L_0))), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Purchasing.Extension.UnityUtil::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UnityUtil_Start_m3416665072_MetadataUsageId;
extern "C"  void UnityUtil_Start_m3416665072 (UnityUtil_t166323129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_Start_m3416665072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Purchasing.Extension.UnityUtil::PcPlatform()
extern Il2CppClass* UnityUtil_t166323129_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Contains_m2326781511_MethodInfo_var;
extern const uint32_t UnityUtil_PcPlatform_m239348050_MetadataUsageId;
extern "C"  bool UnityUtil_PcPlatform_m239348050 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_PcPlatform_m239348050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityUtil_t166323129_il2cpp_TypeInfo_var);
		List_1_t1238706099 * L_0 = ((UnityUtil_t166323129_StaticFields*)UnityUtil_t166323129_il2cpp_TypeInfo_var->static_fields)->get_s_PcControlledPlatforms_4();
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m2326781511(L_0, L_1, /*hidden argument*/List_1_Contains_m2326781511_MethodInfo_var);
		V_0 = L_2;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Purchasing.Extension.UnityUtil::DebugLog(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* FormatException_t2948921286_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1054803371;
extern const uint32_t UnityUtil_DebugLog_m1910253219_MetadataUsageId;
extern "C"  void UnityUtil_DebugLog_m1910253219 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_DebugLog_m1910253219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArgumentNullException_t628810857 * V_0 = NULL;
	FormatException_t2948921286 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		String_t* L_0 = ___message0;
		ObjectU5BU5D_t3614634134* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1263743648(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_3 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1054803371, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		goto IL_003a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArgumentNullException_t628810857_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t2948921286_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002c;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.ArgumentNullException)
		V_0 = ((ArgumentNullException_t628810857 *)__exception_local);
		ArgumentNullException_t628810857 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_003a;
	} // end catch (depth: 1)

CATCH_002c:
	{ // begin catch(System.FormatException)
		V_1 = ((FormatException_t2948921286 *)__exception_local);
		FormatException_t2948921286 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		goto IL_003a;
	} // end catch (depth: 1)

IL_003a:
	{
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.Purchasing.Extension.UnityUtil::DelayedCoroutine(System.Collections.IEnumerator,System.Int32)
extern Il2CppClass* U3CDelayedCoroutineU3Ec__Iterator0_t3019257939_il2cpp_TypeInfo_var;
extern const uint32_t UnityUtil_DelayedCoroutine_m1046729072_MetadataUsageId;
extern "C"  Il2CppObject * UnityUtil_DelayedCoroutine_m1046729072 (UnityUtil_t166323129 * __this, Il2CppObject * ___coroutine0, int32_t ___delay1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_DelayedCoroutine_m1046729072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CDelayedCoroutineU3Ec__Iterator0_t3019257939 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CDelayedCoroutineU3Ec__Iterator0_t3019257939 * L_0 = (U3CDelayedCoroutineU3Ec__Iterator0_t3019257939 *)il2cpp_codegen_object_new(U3CDelayedCoroutineU3Ec__Iterator0_t3019257939_il2cpp_TypeInfo_var);
		U3CDelayedCoroutineU3Ec__Iterator0__ctor_m2110974578(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDelayedCoroutineU3Ec__Iterator0_t3019257939 * L_1 = V_0;
		int32_t L_2 = ___delay1;
		NullCheck(L_1);
		L_1->set_delay_0(L_2);
		U3CDelayedCoroutineU3Ec__Iterator0_t3019257939 * L_3 = V_0;
		Il2CppObject * L_4 = ___coroutine0;
		NullCheck(L_3);
		L_3->set_coroutine_1(L_4);
		U3CDelayedCoroutineU3Ec__Iterator0_t3019257939 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U24this_2(__this);
		U3CDelayedCoroutineU3Ec__Iterator0_t3019257939 * L_6 = V_0;
		V_1 = L_6;
		goto IL_0022;
	}

IL_0022:
	{
		Il2CppObject * L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.Purchasing.Extension.UnityUtil::Update()
extern Il2CppClass* UnityUtil_t166323129_il2cpp_TypeInfo_var;
extern Il2CppClass* ActionU5BU5D_t87223449_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m479650636_MethodInfo_var;
extern const MethodInfo* List_1_CopyTo_m183463793_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m1461442009_MethodInfo_var;
extern const uint32_t UnityUtil_Update_m281010421_MetadataUsageId;
extern "C"  void UnityUtil_Update_m281010421 (UnityUtil_t166323129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_Update_m281010421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ActionU5BU5D_t87223449* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Action_t3226471752 * V_2 = NULL;
	ActionU5BU5D_t87223449* V_3 = NULL;
	int32_t V_4 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityUtil_t166323129_il2cpp_TypeInfo_var);
		bool L_0 = ((UnityUtil_t166323129_StaticFields*)UnityUtil_t166323129_il2cpp_TypeInfo_var->static_fields)->get_s_CallbacksPending_3();
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_0093;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityUtil_t166323129_il2cpp_TypeInfo_var);
		List_1_t2595592884 * L_1 = ((UnityUtil_t166323129_StaticFields*)UnityUtil_t166323129_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_2();
		V_1 = L_1;
		Il2CppObject * L_2 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(UnityUtil_t166323129_il2cpp_TypeInfo_var);
			List_1_t2595592884 * L_3 = ((UnityUtil_t166323129_StaticFields*)UnityUtil_t166323129_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_2();
			NullCheck(L_3);
			int32_t L_4 = List_1_get_Count_m479650636(L_3, /*hidden argument*/List_1_get_Count_m479650636_MethodInfo_var);
			if (L_4)
			{
				goto IL_0033;
			}
		}

IL_002e:
		{
			IL2CPP_LEAVE(0x93, FINALLY_0066);
		}

IL_0033:
		{
			IL2CPP_RUNTIME_CLASS_INIT(UnityUtil_t166323129_il2cpp_TypeInfo_var);
			List_1_t2595592884 * L_5 = ((UnityUtil_t166323129_StaticFields*)UnityUtil_t166323129_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_2();
			NullCheck(L_5);
			int32_t L_6 = List_1_get_Count_m479650636(L_5, /*hidden argument*/List_1_get_Count_m479650636_MethodInfo_var);
			V_0 = ((ActionU5BU5D_t87223449*)SZArrayNew(ActionU5BU5D_t87223449_il2cpp_TypeInfo_var, (uint32_t)L_6));
			List_1_t2595592884 * L_7 = ((UnityUtil_t166323129_StaticFields*)UnityUtil_t166323129_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_2();
			ActionU5BU5D_t87223449* L_8 = V_0;
			NullCheck(L_7);
			List_1_CopyTo_m183463793(L_7, L_8, /*hidden argument*/List_1_CopyTo_m183463793_MethodInfo_var);
			List_1_t2595592884 * L_9 = ((UnityUtil_t166323129_StaticFields*)UnityUtil_t166323129_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_2();
			NullCheck(L_9);
			List_1_Clear_m1461442009(L_9, /*hidden argument*/List_1_Clear_m1461442009_MethodInfo_var);
			il2cpp_codegen_memory_barrier();
			((UnityUtil_t166323129_StaticFields*)UnityUtil_t166323129_il2cpp_TypeInfo_var->static_fields)->set_s_CallbacksPending_3(0);
			IL2CPP_LEAVE(0x6D, FINALLY_0066);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		Il2CppObject * L_10 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(102)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006d:
	{
		ActionU5BU5D_t87223449* L_11 = V_0;
		V_3 = L_11;
		V_4 = 0;
		goto IL_0089;
	}

IL_0078:
	{
		ActionU5BU5D_t87223449* L_12 = V_3;
		int32_t L_13 = V_4;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		Action_t3226471752 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_2 = L_15;
		Action_t3226471752 * L_16 = V_2;
		NullCheck(L_16);
		Action_Invoke_m3801112262(L_16, /*hidden argument*/NULL);
		int32_t L_17 = V_4;
		V_4 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0089:
	{
		int32_t L_18 = V_4;
		ActionU5BU5D_t87223449* L_19 = V_3;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0078;
		}
	}

IL_0093:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.Extension.UnityUtil::AddPauseListener(System.Action`1<System.Boolean>)
extern const MethodInfo* List_1_Add_m2742104618_MethodInfo_var;
extern const uint32_t UnityUtil_AddPauseListener_m633258776_MetadataUsageId;
extern "C"  void UnityUtil_AddPauseListener_m633258776 (UnityUtil_t166323129 * __this, Action_1_t3627374100 * ___runnable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_AddPauseListener_m633258776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2996495232 * L_0 = __this->get_pauseListeners_5();
		Action_1_t3627374100 * L_1 = ___runnable0;
		NullCheck(L_0);
		List_1_Add_m2742104618(L_0, L_1, /*hidden argument*/List_1_Add_m2742104618_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Extension.UnityUtil::OnApplicationPause(System.Boolean)
extern const MethodInfo* List_1_GetEnumerator_m135860085_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m646396353_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3419047393_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m61795003_MethodInfo_var;
extern const uint32_t UnityUtil_OnApplicationPause_m1143148034_MetadataUsageId;
extern "C"  void UnityUtil_OnApplicationPause_m1143148034 (UnityUtil_t166323129 * __this, bool ___paused0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil_OnApplicationPause_m1143148034_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t3627374100 * V_0 = NULL;
	Enumerator_t2531224906  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2996495232 * L_0 = __this->get_pauseListeners_5();
		NullCheck(L_0);
		Enumerator_t2531224906  L_1 = List_1_GetEnumerator_m135860085(L_0, /*hidden argument*/List_1_GetEnumerator_m135860085_MethodInfo_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0013:
		{
			Action_1_t3627374100 * L_2 = Enumerator_get_Current_m646396353((&V_1), /*hidden argument*/Enumerator_get_Current_m646396353_MethodInfo_var);
			V_0 = L_2;
			Action_1_t3627374100 * L_3 = V_0;
			bool L_4 = ___paused0;
			NullCheck(L_3);
			Action_1_Invoke_m3662000152(L_3, L_4, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		}

IL_0024:
		{
			bool L_5 = Enumerator_MoveNext_m3419047393((&V_1), /*hidden argument*/Enumerator_MoveNext_m3419047393_MethodInfo_var);
			if (L_5)
			{
				goto IL_0013;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m61795003((&V_1), /*hidden argument*/Enumerator_Dispose_m61795003_MethodInfo_var);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.Extension.UnityUtil::.cctor()
extern Il2CppClass* List_1_t2595592884_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityUtil_t166323129_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1238706099_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1261323254_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4285277593_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2233011429_MethodInfo_var;
extern const uint32_t UnityUtil__cctor_m502770183_MetadataUsageId;
extern "C"  void UnityUtil__cctor_m502770183 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityUtil__cctor_m502770183_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1238706099 * V_0 = NULL;
	{
		List_1_t2595592884 * L_0 = (List_1_t2595592884 *)il2cpp_codegen_object_new(List_1_t2595592884_il2cpp_TypeInfo_var);
		List_1__ctor_m1261323254(L_0, /*hidden argument*/List_1__ctor_m1261323254_MethodInfo_var);
		((UnityUtil_t166323129_StaticFields*)UnityUtil_t166323129_il2cpp_TypeInfo_var->static_fields)->set_s_Callbacks_2(L_0);
		List_1_t1238706099 * L_1 = (List_1_t1238706099 *)il2cpp_codegen_object_new(List_1_t1238706099_il2cpp_TypeInfo_var);
		List_1__ctor_m4285277593(L_1, /*hidden argument*/List_1__ctor_m4285277593_MethodInfo_var);
		V_0 = L_1;
		List_1_t1238706099 * L_2 = V_0;
		NullCheck(L_2);
		List_1_Add_m2233011429(L_2, ((int32_t)13), /*hidden argument*/List_1_Add_m2233011429_MethodInfo_var);
		List_1_t1238706099 * L_3 = V_0;
		NullCheck(L_3);
		List_1_Add_m2233011429(L_3, 4, /*hidden argument*/List_1_Add_m2233011429_MethodInfo_var);
		List_1_t1238706099 * L_4 = V_0;
		NullCheck(L_4);
		List_1_Add_m2233011429(L_4, 0, /*hidden argument*/List_1_Add_m2233011429_MethodInfo_var);
		List_1_t1238706099 * L_5 = V_0;
		NullCheck(L_5);
		List_1_Add_m2233011429(L_5, 1, /*hidden argument*/List_1_Add_m2233011429_MethodInfo_var);
		List_1_t1238706099 * L_6 = V_0;
		NullCheck(L_6);
		List_1_Add_m2233011429(L_6, 7, /*hidden argument*/List_1_Add_m2233011429_MethodInfo_var);
		List_1_t1238706099 * L_7 = V_0;
		NullCheck(L_7);
		List_1_Add_m2233011429(L_7, 2, /*hidden argument*/List_1_Add_m2233011429_MethodInfo_var);
		List_1_t1238706099 * L_8 = V_0;
		((UnityUtil_t166323129_StaticFields*)UnityUtil_t166323129_il2cpp_TypeInfo_var->static_fields)->set_s_PcControlledPlatforms_4(L_8);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>c__Iterator0::.ctor()
extern "C"  void U3CDelayedCoroutineU3Ec__Iterator0__ctor_m2110974578 (U3CDelayedCoroutineU3Ec__Iterator0_t3019257939 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CDelayedCoroutineU3Ec__Iterator0_MoveNext_m2244223962_MetadataUsageId;
extern "C"  bool U3CDelayedCoroutineU3Ec__Iterator0_MoveNext_m2244223962 (U3CDelayedCoroutineU3Ec__Iterator0_t3019257939 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CDelayedCoroutineU3Ec__Iterator0_MoveNext_m2244223962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0048;
		}
	}
	{
		goto IL_0061;
	}

IL_0021:
	{
		int32_t L_2 = __this->get_delay_0();
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, (((float)((float)L_2))), /*hidden argument*/NULL);
		__this->set_U24current_3(L_3);
		bool L_4 = __this->get_U24disposing_4();
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0043:
	{
		goto IL_0063;
	}

IL_0048:
	{
		UnityUtil_t166323129 * L_5 = __this->get_U24this_2();
		Il2CppObject * L_6 = __this->get_coroutine_1();
		NullCheck(L_5);
		MonoBehaviour_StartCoroutine_m2470621050(L_5, L_6, /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_0061:
	{
		return (bool)0;
	}

IL_0063:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayedCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1196699116 (U3CDelayedCoroutineU3Ec__Iterator0_t3019257939 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayedCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3367230516 (U3CDelayedCoroutineU3Ec__Iterator0_t3019257939 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>c__Iterator0::Dispose()
extern "C"  void U3CDelayedCoroutineU3Ec__Iterator0_Dispose_m1216761201 (U3CDelayedCoroutineU3Ec__Iterator0_t3019257939 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CDelayedCoroutineU3Ec__Iterator0_Reset_m1277931099_MetadataUsageId;
extern "C"  void U3CDelayedCoroutineU3Ec__Iterator0_Reset_m1277931099 (U3CDelayedCoroutineU3Ec__Iterator0_t3019257939 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CDelayedCoroutineU3Ec__Iterator0_Reset_m1277931099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.Purchasing.FakeAmazonExtensions::.ctor()
extern "C"  void FakeAmazonExtensions__ctor_m2802760721 (FakeAmazonExtensions_t2261777661 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeAmazonExtensions::WriteSandboxJSON(System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>)
extern "C"  void FakeAmazonExtensions_WriteSandboxJSON_m234044380 (FakeAmazonExtensions_t2261777661 * __this, HashSet_1_t275936122 * ___products0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeAppleConfiguation::.ctor()
extern "C"  void FakeAppleConfiguation__ctor_m217023035 (FakeAppleConfiguation_t4052738437 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeAppleExtensions::.ctor()
extern "C"  void FakeAppleExtensions__ctor_m1953807163 (FakeAppleExtensions_t4039399289 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeAppleExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t FakeAppleExtensions_RestoreTransactions_m2849330533_MetadataUsageId;
extern "C"  void FakeAppleExtensions_RestoreTransactions_m2849330533 (FakeAppleExtensions_t4039399289 * __this, Action_1_t3627374100 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FakeAppleExtensions_RestoreTransactions_m2849330533_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = ___callback0;
		NullCheck(L_0);
		Action_1_Invoke_m3662000152(L_0, (bool)1, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeAppleExtensions::RegisterPurchaseDeferredListener(System.Action`1<UnityEngine.Purchasing.Product>)
extern "C"  void FakeAppleExtensions_RegisterPurchaseDeferredListener_m4263381806 (FakeAppleExtensions_t4039399289 * __this, Action_1_t1005487353 * ___callback0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeGooglePlayConfiguration::.ctor()
extern "C"  void FakeGooglePlayConfiguration__ctor_m1997483180 (FakeGooglePlayConfiguration_t737012266 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeSamsungAppsExtensions::.ctor()
extern "C"  void FakeSamsungAppsExtensions__ctor_m3771901773 (FakeSamsungAppsExtensions_t1522853249 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeSamsungAppsExtensions::SetMode(UnityEngine.Purchasing.SamsungAppsMode)
extern "C"  void FakeSamsungAppsExtensions_SetMode_m2001257748 (FakeSamsungAppsExtensions_t1522853249 * __this, int32_t ___mode0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeSamsungAppsExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t FakeSamsungAppsExtensions_RestoreTransactions_m3018252279_MetadataUsageId;
extern "C"  void FakeSamsungAppsExtensions_RestoreTransactions_m3018252279 (FakeSamsungAppsExtensions_t1522853249 * __this, Action_1_t3627374100 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FakeSamsungAppsExtensions_RestoreTransactions_m3018252279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = ___callback0;
		NullCheck(L_0);
		Action_1_Invoke_m3662000152(L_0, (bool)1, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeStore::.ctor()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const uint32_t FakeStore__ctor_m3239256688_MetadataUsageId;
extern "C"  void FakeStore__ctor_m3239256688 (FakeStore_t3882981564 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FakeStore__ctor_m3239256688_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_m_PurchasedProducts_1(L_0);
		__this->set_UIMode_4(0);
		AbstractStore__ctor_m212291193(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Purchasing.FakeStore::get_unavailableProductId()
extern "C"  String_t* FakeStore_get_unavailableProductId_m4172264450 (FakeStore_t3882981564 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CunavailableProductIdU3Ek__BackingField_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.FakeStore::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern "C"  void FakeStore_Initialize_m2189078753 (FakeStore_t3882981564 * __this, Il2CppObject * ___biller0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___biller0;
		__this->set_m_Biller_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeStore::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern Il2CppClass* U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2687388655_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3712966391_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ProductMetadata_t1573242544_il2cpp_TypeInfo_var;
extern Il2CppClass* ProductDescription_t3318267523_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t2790035381_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4188009120_MethodInfo_var;
extern const MethodInfo* ReadOnlyCollection_1_GetEnumerator_m178725564_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3009966556_MethodInfo_var;
extern const MethodInfo* U3CRetrieveProductsU3Ec__AnonStorey0_U3CU3Em__0_m3369741789_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m3463170984_MethodInfo_var;
extern const MethodInfo* FakeStore_StartUI_TisInitializationFailureReason_t2954032642_m3575193201_MethodInfo_var;
extern const MethodInfo* Action_2_Invoke_m3143687639_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1291769943;
extern Il2CppCodeGenString* _stringLiteral1440618482;
extern Il2CppCodeGenString* _stringLiteral1914967835;
extern Il2CppCodeGenString* _stringLiteral1690816450;
extern const uint32_t FakeStore_RetrieveProducts_m3255639604_MetadataUsageId;
extern "C"  void FakeStore_RetrieveProducts_m3255639604 (FakeStore_t3882981564 * __this, ReadOnlyCollection_1_t2128260960 * ___productDefinitions0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FakeStore_RetrieveProducts_m3255639604_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567 * V_0 = NULL;
	ProductDefinition_t1942475268 * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	ProductMetadata_t1573242544 * V_3 = NULL;
	Action_2_t2790035381 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567 * L_0 = (U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567 *)il2cpp_codegen_object_new(U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567_il2cpp_TypeInfo_var);
		U3CRetrieveProductsU3Ec__AnonStorey0__ctor_m2805791778(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567 * L_2 = V_0;
		List_1_t2687388655 * L_3 = (List_1_t2687388655 *)il2cpp_codegen_object_new(List_1_t2687388655_il2cpp_TypeInfo_var);
		List_1__ctor_m4188009120(L_3, /*hidden argument*/List_1__ctor_m4188009120_MethodInfo_var);
		NullCheck(L_2);
		L_2->set_products_0(L_3);
		ReadOnlyCollection_1_t2128260960 * L_4 = ___productDefinitions0;
		NullCheck(L_4);
		Il2CppObject* L_5 = ReadOnlyCollection_1_GetEnumerator_m178725564(L_4, /*hidden argument*/ReadOnlyCollection_1_GetEnumerator_m178725564_MethodInfo_var);
		V_2 = L_5;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			goto IL_008d;
		}

IL_0026:
		{
			Il2CppObject* L_6 = V_2;
			NullCheck(L_6);
			ProductDefinition_t1942475268 * L_7 = InterfaceFuncInvoker0< ProductDefinition_t1942475268 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.ProductDefinition>::get_Current() */, IEnumerator_1_t3712966391_il2cpp_TypeInfo_var, L_6);
			V_1 = L_7;
			String_t* L_8 = FakeStore_get_unavailableProductId_m4172264450(__this, /*hidden argument*/NULL);
			ProductDefinition_t1942475268 * L_9 = V_1;
			NullCheck(L_9);
			String_t* L_10 = ProductDefinition_get_id_m264072292(L_9, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_11 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
			if (!L_11)
			{
				goto IL_008c;
			}
		}

IL_0044:
		{
			ProductDefinition_t1942475268 * L_12 = V_1;
			NullCheck(L_12);
			String_t* L_13 = ProductDefinition_get_id_m264072292(L_12, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_14 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1440618482, L_13, /*hidden argument*/NULL);
			Decimal_t724701077  L_15;
			memset(&L_15, 0, sizeof(L_15));
			Decimal__ctor_m1770144563(&L_15, 1, 0, 0, (bool)0, 2, /*hidden argument*/NULL);
			ProductMetadata_t1573242544 * L_16 = (ProductMetadata_t1573242544 *)il2cpp_codegen_object_new(ProductMetadata_t1573242544_il2cpp_TypeInfo_var);
			ProductMetadata__ctor_m3648829555(L_16, _stringLiteral1291769943, L_14, _stringLiteral1914967835, _stringLiteral1690816450, L_15, /*hidden argument*/NULL);
			V_3 = L_16;
			U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567 * L_17 = V_0;
			NullCheck(L_17);
			List_1_t2687388655 * L_18 = L_17->get_products_0();
			ProductDefinition_t1942475268 * L_19 = V_1;
			NullCheck(L_19);
			String_t* L_20 = ProductDefinition_get_storeSpecificId_m2251287741(L_19, /*hidden argument*/NULL);
			ProductMetadata_t1573242544 * L_21 = V_3;
			ProductDescription_t3318267523 * L_22 = (ProductDescription_t3318267523 *)il2cpp_codegen_object_new(ProductDescription_t3318267523_il2cpp_TypeInfo_var);
			ProductDescription__ctor_m2055358744(L_22, L_20, L_21, /*hidden argument*/NULL);
			NullCheck(L_18);
			List_1_Add_m3009966556(L_18, L_22, /*hidden argument*/List_1_Add_m3009966556_MethodInfo_var);
		}

IL_008c:
		{
		}

IL_008d:
		{
			Il2CppObject* L_23 = V_2;
			NullCheck(L_23);
			bool L_24 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_23);
			if (L_24)
			{
				goto IL_0026;
			}
		}

IL_0098:
		{
			IL2CPP_LEAVE(0xAA, FINALLY_009d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009d;
	}

FINALLY_009d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_25 = V_2;
			if (!L_25)
			{
				goto IL_00a9;
			}
		}

IL_00a3:
		{
			Il2CppObject* L_26 = V_2;
			NullCheck(L_26);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_26);
		}

IL_00a9:
		{
			IL2CPP_END_FINALLY(157)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(157)
	{
		IL2CPP_JUMP_TBL(0xAA, IL_00aa)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00aa:
	{
		U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567 * L_27 = V_0;
		IntPtr_t L_28;
		L_28.set_m_value_0((void*)(void*)U3CRetrieveProductsU3Ec__AnonStorey0_U3CU3Em__0_m3369741789_MethodInfo_var);
		Action_2_t2790035381 * L_29 = (Action_2_t2790035381 *)il2cpp_codegen_object_new(Action_2_t2790035381_il2cpp_TypeInfo_var);
		Action_2__ctor_m3463170984(L_29, L_27, L_28, /*hidden argument*/Action_2__ctor_m3463170984_MethodInfo_var);
		V_4 = L_29;
		int32_t L_30 = __this->get_UIMode_4();
		if ((!(((uint32_t)L_30) == ((uint32_t)2))))
		{
			goto IL_00d3;
		}
	}
	{
		ReadOnlyCollection_1_t2128260960 * L_31 = ___productDefinitions0;
		Action_2_t2790035381 * L_32 = V_4;
		bool L_33 = GenericVirtFuncInvoker3< bool, Il2CppObject *, int32_t, Action_2_t2790035381 * >::Invoke(FakeStore_StartUI_TisInitializationFailureReason_t2954032642_m3575193201_MethodInfo_var, __this, L_31, 1, L_32);
		if (L_33)
		{
			goto IL_00de;
		}
	}

IL_00d3:
	{
		Action_2_t2790035381 * L_34 = V_4;
		NullCheck(L_34);
		Action_2_Invoke_m3143687639(L_34, (bool)1, 2, /*hidden argument*/Action_2_Invoke_m3143687639_MethodInfo_var);
	}

IL_00de:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeStore::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern Il2CppClass* U3CPurchaseU3Ec__AnonStorey1_t2000504963_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t1158962578_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* U3CPurchaseU3Ec__AnonStorey1_U3CU3Em__0_m1215038094_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m1121744629_MethodInfo_var;
extern const MethodInfo* FakeStore_StartUI_TisPurchaseFailureReason_t1322959839_m3432893130_MethodInfo_var;
extern const MethodInfo* Action_2_Invoke_m1462475090_MethodInfo_var;
extern const uint32_t FakeStore_Purchase_m194902756_MetadataUsageId;
extern "C"  void FakeStore_Purchase_m194902756 (FakeStore_t3882981564 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___developerPayload1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FakeStore_Purchase_m194902756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPurchaseU3Ec__AnonStorey1_t2000504963 * V_0 = NULL;
	Action_2_t1158962578 * V_1 = NULL;
	{
		U3CPurchaseU3Ec__AnonStorey1_t2000504963 * L_0 = (U3CPurchaseU3Ec__AnonStorey1_t2000504963 *)il2cpp_codegen_object_new(U3CPurchaseU3Ec__AnonStorey1_t2000504963_il2cpp_TypeInfo_var);
		U3CPurchaseU3Ec__AnonStorey1__ctor_m3393892180(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPurchaseU3Ec__AnonStorey1_t2000504963 * L_1 = V_0;
		ProductDefinition_t1942475268 * L_2 = ___product0;
		NullCheck(L_1);
		L_1->set_product_0(L_2);
		U3CPurchaseU3Ec__AnonStorey1_t2000504963 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_1(__this);
		__this->set_purchaseCalled_2((bool)1);
		U3CPurchaseU3Ec__AnonStorey1_t2000504963 * L_4 = V_0;
		NullCheck(L_4);
		ProductDefinition_t1942475268 * L_5 = L_4->get_product_0();
		NullCheck(L_5);
		int32_t L_6 = ProductDefinition_get_type_m590914665(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		List_1_t1398341365 * L_7 = __this->get_m_PurchasedProducts_1();
		U3CPurchaseU3Ec__AnonStorey1_t2000504963 * L_8 = V_0;
		NullCheck(L_8);
		ProductDefinition_t1942475268 * L_9 = L_8->get_product_0();
		NullCheck(L_9);
		String_t* L_10 = ProductDefinition_get_storeSpecificId_m2251287741(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		List_1_Add_m4061286785(L_7, L_10, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
	}

IL_0042:
	{
		U3CPurchaseU3Ec__AnonStorey1_t2000504963 * L_11 = V_0;
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)U3CPurchaseU3Ec__AnonStorey1_U3CU3Em__0_m1215038094_MethodInfo_var);
		Action_2_t1158962578 * L_13 = (Action_2_t1158962578 *)il2cpp_codegen_object_new(Action_2_t1158962578_il2cpp_TypeInfo_var);
		Action_2__ctor_m1121744629(L_13, L_11, L_12, /*hidden argument*/Action_2__ctor_m1121744629_MethodInfo_var);
		V_1 = L_13;
		U3CPurchaseU3Ec__AnonStorey1_t2000504963 * L_14 = V_0;
		NullCheck(L_14);
		ProductDefinition_t1942475268 * L_15 = L_14->get_product_0();
		Action_2_t1158962578 * L_16 = V_1;
		bool L_17 = GenericVirtFuncInvoker3< bool, Il2CppObject *, int32_t, Action_2_t1158962578 * >::Invoke(FakeStore_StartUI_TisPurchaseFailureReason_t1322959839_m3432893130_MethodInfo_var, __this, L_15, 0, L_16);
		if (L_17)
		{
			goto IL_006c;
		}
	}
	{
		Action_2_t1158962578 * L_18 = V_1;
		NullCheck(L_18);
		Action_2_Invoke_m1462475090(L_18, (bool)1, 6, /*hidden argument*/Action_2_Invoke_m1462475090_MethodInfo_var);
	}

IL_006c:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeStore::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern "C"  void FakeStore_FinishTransaction_m120746640 (FakeStore_t3882981564 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___transactionId1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeStore/<Purchase>c__AnonStorey1::.ctor()
extern "C"  void U3CPurchaseU3Ec__AnonStorey1__ctor_m3393892180 (U3CPurchaseU3Ec__AnonStorey1_t2000504963 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeStore/<Purchase>c__AnonStorey1::<>m__0(System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason)
extern Il2CppClass* Guid_t2533601593_il2cpp_TypeInfo_var;
extern Il2CppClass* IStoreCallback_t2691517565_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseFailureDescription_t1607114611_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2087601344;
extern Il2CppCodeGenString* _stringLiteral33060079;
extern const uint32_t U3CPurchaseU3Ec__AnonStorey1_U3CU3Em__0_m1215038094_MetadataUsageId;
extern "C"  void U3CPurchaseU3Ec__AnonStorey1_U3CU3Em__0_m1215038094 (U3CPurchaseU3Ec__AnonStorey1_t2000504963 * __this, bool ___allow0, int32_t ___failureReason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPurchaseU3Ec__AnonStorey1_U3CU3Em__0_m1215038094_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Guid_t2533601593  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PurchaseFailureDescription_t1607114611 * V_1 = NULL;
	{
		bool L_0 = ___allow0;
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		FakeStore_t3882981564 * L_1 = __this->get_U24this_1();
		NullCheck(L_1);
		Il2CppObject * L_2 = L_1->get_m_Biller_0();
		ProductDefinition_t1942475268 * L_3 = __this->get_product_0();
		NullCheck(L_3);
		String_t* L_4 = ProductDefinition_get_storeSpecificId_m2251287741(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t2533601593_il2cpp_TypeInfo_var);
		Guid_t2533601593  L_5 = Guid_NewGuid_m3493657620(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = Guid_ToString_m3927110175((&V_0), /*hidden argument*/NULL);
		NullCheck(L_2);
		InterfaceActionInvoker3< String_t*, String_t*, String_t* >::Invoke(3 /* System.Void UnityEngine.Purchasing.Extension.IStoreCallback::OnPurchaseSucceeded(System.String,System.String,System.String) */, IStoreCallback_t2691517565_il2cpp_TypeInfo_var, L_2, L_4, _stringLiteral2087601344, L_6);
		goto IL_0077;
	}

IL_0041:
	{
		int32_t L_7 = ___failureReason1;
		if ((!(((uint32_t)L_7) == ((uint32_t)6))))
		{
			goto IL_004e;
		}
	}
	{
		___failureReason1 = 4;
	}

IL_004e:
	{
		ProductDefinition_t1942475268 * L_8 = __this->get_product_0();
		NullCheck(L_8);
		String_t* L_9 = ProductDefinition_get_id_m264072292(L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___failureReason1;
		PurchaseFailureDescription_t1607114611 * L_11 = (PurchaseFailureDescription_t1607114611 *)il2cpp_codegen_object_new(PurchaseFailureDescription_t1607114611_il2cpp_TypeInfo_var);
		PurchaseFailureDescription__ctor_m381019375(L_11, L_9, L_10, _stringLiteral33060079, /*hidden argument*/NULL);
		V_1 = L_11;
		FakeStore_t3882981564 * L_12 = __this->get_U24this_1();
		NullCheck(L_12);
		Il2CppObject * L_13 = L_12->get_m_Biller_0();
		PurchaseFailureDescription_t1607114611 * L_14 = V_1;
		NullCheck(L_13);
		InterfaceActionInvoker1< PurchaseFailureDescription_t1607114611 * >::Invoke(4 /* System.Void UnityEngine.Purchasing.Extension.IStoreCallback::OnPurchaseFailed(UnityEngine.Purchasing.Extension.PurchaseFailureDescription) */, IStoreCallback_t2691517565_il2cpp_TypeInfo_var, L_13, L_14);
	}

IL_0077:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeStore/<RetrieveProducts>c__AnonStorey0::.ctor()
extern "C"  void U3CRetrieveProductsU3Ec__AnonStorey0__ctor_m2805791778 (U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeStore/<RetrieveProducts>c__AnonStorey0::<>m__0(System.Boolean,UnityEngine.Purchasing.InitializationFailureReason)
extern Il2CppClass* IStoreCallback_t2691517565_il2cpp_TypeInfo_var;
extern const uint32_t U3CRetrieveProductsU3Ec__AnonStorey0_U3CU3Em__0_m3369741789_MetadataUsageId;
extern "C"  void U3CRetrieveProductsU3Ec__AnonStorey0_U3CU3Em__0_m3369741789 (U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567 * __this, bool ___allow0, int32_t ___failureReason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRetrieveProductsU3Ec__AnonStorey0_U3CU3Em__0_m3369741789_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___allow0;
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		FakeStore_t3882981564 * L_1 = __this->get_U24this_1();
		NullCheck(L_1);
		Il2CppObject * L_2 = L_1->get_m_Biller_0();
		List_1_t2687388655 * L_3 = __this->get_products_0();
		NullCheck(L_2);
		InterfaceActionInvoker1< List_1_t2687388655 * >::Invoke(2 /* System.Void UnityEngine.Purchasing.Extension.IStoreCallback::OnProductsRetrieved(System.Collections.Generic.List`1<UnityEngine.Purchasing.Extension.ProductDescription>) */, IStoreCallback_t2691517565_il2cpp_TypeInfo_var, L_2, L_3);
		goto IL_0037;
	}

IL_0024:
	{
		FakeStore_t3882981564 * L_4 = __this->get_U24this_1();
		NullCheck(L_4);
		Il2CppObject * L_5 = L_4->get_m_Biller_0();
		int32_t L_6 = ___failureReason1;
		NullCheck(L_5);
		InterfaceActionInvoker1< int32_t >::Invoke(1 /* System.Void UnityEngine.Purchasing.Extension.IStoreCallback::OnSetupFailed(UnityEngine.Purchasing.InitializationFailureReason) */, IStoreCallback_t2691517565_il2cpp_TypeInfo_var, L_5, L_6);
	}

IL_0037:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeTizenStoreConfiguration::.ctor()
extern "C"  void FakeTizenStoreConfiguration__ctor_m528490996 (FakeTizenStoreConfiguration_t3055550456 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.FakeTizenStoreConfiguration::SetGroupId(System.String)
extern "C"  void FakeTizenStoreConfiguration_SetGroupId_m1022538578 (FakeTizenStoreConfiguration_t3055550456 * __this, String_t* ___group0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.JavaBridge::.ctor(UnityEngine.Purchasing.IUnityCallback)
extern Il2CppCodeGenString* _stringLiteral199091546;
extern const uint32_t JavaBridge__ctor_m2145695863_MetadataUsageId;
extern "C"  void JavaBridge__ctor_m2145695863 (JavaBridge_t44746847 * __this, Il2CppObject * ___forwardTo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JavaBridge__ctor_m2145695863_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AndroidJavaProxy__ctor_m4016180768(__this, _stringLiteral199091546, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___forwardTo0;
		__this->set_forwardTo_1(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.JSONSerializer::SerializeProductDef(UnityEngine.Purchasing.ProductDefinition)
extern Il2CppClass* MiniJson_t838727235_il2cpp_TypeInfo_var;
extern const uint32_t JSONSerializer_SerializeProductDef_m3948994734_MetadataUsageId;
extern "C"  String_t* JSONSerializer_SerializeProductDef_m3948994734 (Il2CppObject * __this /* static, unused */, ProductDefinition_t1942475268 * ___product0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONSerializer_SerializeProductDef_m3948994734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		ProductDefinition_t1942475268 * L_0 = ___product0;
		Dictionary_2_t309261261 * L_1 = JSONSerializer_EncodeProductDef_m2253177235(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MiniJson_t838727235_il2cpp_TypeInfo_var);
		String_t* L_2 = MiniJson_JsonEncode_m3177863123(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.String UnityEngine.Purchasing.JSONSerializer::SerializeProductDefs(System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.ProductDefinition>)
extern Il2CppClass* List_1_t2058570427_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2234602313_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3712966391_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJson_t838727235_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1864370736_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2488140228_MethodInfo_var;
extern const uint32_t JSONSerializer_SerializeProductDefs_m2515728532_MetadataUsageId;
extern "C"  String_t* JSONSerializer_SerializeProductDefs_m2515728532 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___products0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONSerializer_SerializeProductDefs_m2515728532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2058570427 * V_0 = NULL;
	ProductDefinition_t1942475268 * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	String_t* V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)il2cpp_codegen_object_new(List_1_t2058570427_il2cpp_TypeInfo_var);
		List_1__ctor_m1864370736(L_0, /*hidden argument*/List_1__ctor_m1864370736_MethodInfo_var);
		V_0 = L_0;
		Il2CppObject* L_1 = ___products0;
		NullCheck(L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.ProductDefinition>::GetEnumerator() */, IEnumerable_1_t2234602313_il2cpp_TypeInfo_var, L_1);
		V_2 = L_2;
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0029;
		}

IL_0014:
		{
			Il2CppObject* L_3 = V_2;
			NullCheck(L_3);
			ProductDefinition_t1942475268 * L_4 = InterfaceFuncInvoker0< ProductDefinition_t1942475268 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.ProductDefinition>::get_Current() */, IEnumerator_1_t3712966391_il2cpp_TypeInfo_var, L_3);
			V_1 = L_4;
			List_1_t2058570427 * L_5 = V_0;
			ProductDefinition_t1942475268 * L_6 = V_1;
			Dictionary_2_t309261261 * L_7 = JSONSerializer_EncodeProductDef_m2253177235(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			NullCheck(L_5);
			List_1_Add_m2488140228(L_5, L_7, /*hidden argument*/List_1_Add_m2488140228_MethodInfo_var);
		}

IL_0029:
		{
			Il2CppObject* L_8 = V_2;
			NullCheck(L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_0014;
			}
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x46, FINALLY_0039);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_10 = V_2;
			if (!L_10)
			{
				goto IL_0045;
			}
		}

IL_003f:
		{
			Il2CppObject* L_11 = V_2;
			NullCheck(L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_11);
		}

IL_0045:
		{
			IL2CPP_END_FINALLY(57)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0046:
	{
		List_1_t2058570427 * L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJson_t838727235_il2cpp_TypeInfo_var);
		String_t* L_13 = MiniJson_JsonEncode_m3177863123(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		goto IL_0052;
	}

IL_0052:
	{
		String_t* L_14 = V_3;
		return L_14;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Extension.ProductDescription> UnityEngine.Purchasing.JSONSerializer::DeserializeProductDescriptions(System.String)
extern Il2CppClass* MiniJson_t838727235_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2058570427_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2687388655_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ProductDescription_t3318267523_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4188009120_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1854899495_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1222341175_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m464793699_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3009966556_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4021114635_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3736175406_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1907405137;
extern Il2CppCodeGenString* _stringLiteral667782618;
extern Il2CppCodeGenString* _stringLiteral1804875922;
extern Il2CppCodeGenString* _stringLiteral1038890517;
extern const uint32_t JSONSerializer_DeserializeProductDescriptions_m2762227127_MetadataUsageId;
extern "C"  List_1_t2687388655 * JSONSerializer_DeserializeProductDescriptions_m2762227127 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONSerializer_DeserializeProductDescriptions_m2762227127_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2058570427 * V_0 = NULL;
	List_1_t2687388655 * V_1 = NULL;
	Dictionary_2_t309261261 * V_2 = NULL;
	Enumerator_t1593300101  V_3;
	memset(&V_3, 0, sizeof(V_3));
	ProductMetadata_t1573242544 * V_4 = NULL;
	ProductDescription_t3318267523 * V_5 = NULL;
	List_1_t2687388655 * V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJson_t838727235_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = MiniJson_JsonDecode_m1067224953(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((List_1_t2058570427 *)CastclassClass(L_1, List_1_t2058570427_il2cpp_TypeInfo_var));
		List_1_t2687388655 * L_2 = (List_1_t2687388655 *)il2cpp_codegen_object_new(List_1_t2687388655_il2cpp_TypeInfo_var);
		List_1__ctor_m4188009120(L_2, /*hidden argument*/List_1__ctor_m4188009120_MethodInfo_var);
		V_1 = L_2;
		List_1_t2058570427 * L_3 = V_0;
		NullCheck(L_3);
		Enumerator_t1593300101  L_4 = List_1_GetEnumerator_m1854899495(L_3, /*hidden argument*/List_1_GetEnumerator_m1854899495_MethodInfo_var);
		V_3 = L_4;
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_007d;
		}

IL_0020:
		{
			Il2CppObject * L_5 = Enumerator_get_Current_m1222341175((&V_3), /*hidden argument*/Enumerator_get_Current_m1222341175_MethodInfo_var);
			V_2 = ((Dictionary_2_t309261261 *)CastclassClass(L_5, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
			Dictionary_2_t309261261 * L_6 = V_2;
			NullCheck(L_6);
			Il2CppObject * L_7 = Dictionary_2_get_Item_m464793699(L_6, _stringLiteral1907405137, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
			ProductMetadata_t1573242544 * L_8 = JSONSerializer_DeserializeMetadata_m540119287(NULL /*static, unused*/, ((Dictionary_2_t309261261 *)CastclassClass(L_7, Dictionary_2_t309261261_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			V_4 = L_8;
			Dictionary_2_t309261261 * L_9 = V_2;
			NullCheck(L_9);
			Il2CppObject * L_10 = Dictionary_2_get_Item_m464793699(L_9, _stringLiteral667782618, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
			ProductMetadata_t1573242544 * L_11 = V_4;
			Dictionary_2_t309261261 * L_12 = V_2;
			String_t* L_13 = SerializationExtensions_TryGetString_m1096911238(NULL /*static, unused*/, L_12, _stringLiteral1804875922, /*hidden argument*/NULL);
			Dictionary_2_t309261261 * L_14 = V_2;
			String_t* L_15 = SerializationExtensions_TryGetString_m1096911238(NULL /*static, unused*/, L_14, _stringLiteral1038890517, /*hidden argument*/NULL);
			ProductDescription_t3318267523 * L_16 = (ProductDescription_t3318267523 *)il2cpp_codegen_object_new(ProductDescription_t3318267523_il2cpp_TypeInfo_var);
			ProductDescription__ctor_m2528687064(L_16, ((String_t*)CastclassSealed(L_10, String_t_il2cpp_TypeInfo_var)), L_11, L_13, L_15, /*hidden argument*/NULL);
			V_5 = L_16;
			List_1_t2687388655 * L_17 = V_1;
			ProductDescription_t3318267523 * L_18 = V_5;
			NullCheck(L_17);
			List_1_Add_m3009966556(L_17, L_18, /*hidden argument*/List_1_Add_m3009966556_MethodInfo_var);
		}

IL_007d:
		{
			bool L_19 = Enumerator_MoveNext_m4021114635((&V_3), /*hidden argument*/Enumerator_MoveNext_m4021114635_MethodInfo_var);
			if (L_19)
			{
				goto IL_0020;
			}
		}

IL_0089:
		{
			IL2CPP_LEAVE(0x9C, FINALLY_008e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_008e;
	}

FINALLY_008e:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3736175406((&V_3), /*hidden argument*/Enumerator_Dispose_m3736175406_MethodInfo_var);
		IL2CPP_END_FINALLY(142)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(142)
	{
		IL2CPP_JUMP_TBL(0x9C, IL_009c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009c:
	{
		List_1_t2687388655 * L_20 = V_1;
		V_6 = L_20;
		goto IL_00a4;
	}

IL_00a4:
	{
		List_1_t2687388655 * L_21 = V_6;
		return L_21;
	}
}
// UnityEngine.Purchasing.Extension.PurchaseFailureDescription UnityEngine.Purchasing.JSONSerializer::DeserializeFailureReason(System.String)
extern const Il2CppType* PurchaseFailureReason_t1322959839_0_0_0_var;
extern Il2CppClass* MiniJson_t838727235_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseFailureDescription_t1607114611_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m464793699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3969962058;
extern Il2CppCodeGenString* _stringLiteral697716380;
extern Il2CppCodeGenString* _stringLiteral359429027;
extern const uint32_t JSONSerializer_DeserializeFailureReason_m1834198275_MetadataUsageId;
extern "C"  PurchaseFailureDescription_t1607114611 * JSONSerializer_DeserializeFailureReason_m1834198275 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONSerializer_DeserializeFailureReason_m1834198275_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	int32_t V_1 = 0;
	PurchaseFailureDescription_t1607114611 * V_2 = NULL;
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJson_t838727235_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = MiniJson_JsonDecode_m1067224953(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Dictionary_2_t309261261 *)CastclassClass(L_1, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(PurchaseFailureReason_t1322959839_0_0_0_var), /*hidden argument*/NULL);
		Dictionary_2_t309261261 * L_3 = V_0;
		NullCheck(L_3);
		Il2CppObject * L_4 = Dictionary_2_get_Item_m464793699(L_3, _stringLiteral3969962058, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppObject * L_5 = Enum_Parse_m2561000069(NULL /*static, unused*/, L_2, ((String_t*)CastclassSealed(L_4, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_1 = ((*(int32_t*)((int32_t*)UnBox (L_5, PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var))));
		Dictionary_2_t309261261 * L_6 = V_0;
		NullCheck(L_6);
		Il2CppObject * L_7 = Dictionary_2_get_Item_m464793699(L_6, _stringLiteral697716380, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		int32_t L_8 = V_1;
		Dictionary_2_t309261261 * L_9 = V_0;
		String_t* L_10 = SerializationExtensions_TryGetString_m1096911238(NULL /*static, unused*/, L_9, _stringLiteral359429027, /*hidden argument*/NULL);
		PurchaseFailureDescription_t1607114611 * L_11 = (PurchaseFailureDescription_t1607114611 *)il2cpp_codegen_object_new(PurchaseFailureDescription_t1607114611_il2cpp_TypeInfo_var);
		PurchaseFailureDescription__ctor_m381019375(L_11, ((String_t*)CastclassSealed(L_7, String_t_il2cpp_TypeInfo_var)), L_8, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		goto IL_0059;
	}

IL_0059:
	{
		PurchaseFailureDescription_t1607114611 * L_12 = V_2;
		return L_12;
	}
}
// UnityEngine.Purchasing.ProductMetadata UnityEngine.Purchasing.JSONSerializer::DeserializeMetadata(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern Il2CppClass* ProductMetadata_t1573242544_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m464793699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1313478283;
extern Il2CppCodeGenString* _stringLiteral2784228039;
extern Il2CppCodeGenString* _stringLiteral3201161141;
extern Il2CppCodeGenString* _stringLiteral1650955383;
extern Il2CppCodeGenString* _stringLiteral2907253904;
extern const uint32_t JSONSerializer_DeserializeMetadata_m540119287_MetadataUsageId;
extern "C"  ProductMetadata_t1573242544 * JSONSerializer_DeserializeMetadata_m540119287 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONSerializer_DeserializeMetadata_m540119287_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ProductMetadata_t1573242544 * V_0 = NULL;
	{
		Dictionary_2_t309261261 * L_0 = ___data0;
		String_t* L_1 = SerializationExtensions_TryGetString_m1096911238(NULL /*static, unused*/, L_0, _stringLiteral1313478283, /*hidden argument*/NULL);
		Dictionary_2_t309261261 * L_2 = ___data0;
		String_t* L_3 = SerializationExtensions_TryGetString_m1096911238(NULL /*static, unused*/, L_2, _stringLiteral2784228039, /*hidden argument*/NULL);
		Dictionary_2_t309261261 * L_4 = ___data0;
		String_t* L_5 = SerializationExtensions_TryGetString_m1096911238(NULL /*static, unused*/, L_4, _stringLiteral3201161141, /*hidden argument*/NULL);
		Dictionary_2_t309261261 * L_6 = ___data0;
		String_t* L_7 = SerializationExtensions_TryGetString_m1096911238(NULL /*static, unused*/, L_6, _stringLiteral1650955383, /*hidden argument*/NULL);
		Dictionary_2_t309261261 * L_8 = ___data0;
		NullCheck(L_8);
		Il2CppObject * L_9 = Dictionary_2_get_Item_m464793699(L_8, _stringLiteral2907253904, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		Decimal_t724701077  L_11 = Decimal_Parse_m672131870(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		ProductMetadata_t1573242544 * L_12 = (ProductMetadata_t1573242544 *)il2cpp_codegen_object_new(ProductMetadata_t1573242544_il2cpp_TypeInfo_var);
		ProductMetadata__ctor_m3648829555(L_12, L_1, L_3, L_5, L_7, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_004d;
	}

IL_004d:
	{
		ProductMetadata_t1573242544 * L_13 = V_0;
		return L_13;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.JSONSerializer::EncodeProductDef(UnityEngine.Purchasing.ProductDefinition)
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* ProductType_t2754455291_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3598753405_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1128763565_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral287061489;
extern Il2CppCodeGenString* _stringLiteral667782618;
extern Il2CppCodeGenString* _stringLiteral1421151742;
extern const uint32_t JSONSerializer_EncodeProductDef_m2253177235_MetadataUsageId;
extern "C"  Dictionary_2_t309261261 * JSONSerializer_EncodeProductDef_m2253177235 (Il2CppObject * __this /* static, unused */, ProductDefinition_t1942475268 * ___product0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONSerializer_EncodeProductDef_m2253177235_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	int32_t V_1 = 0;
	Dictionary_2_t309261261 * V_2 = NULL;
	{
		Dictionary_2_t309261261 * L_0 = (Dictionary_2_t309261261 *)il2cpp_codegen_object_new(Dictionary_2_t309261261_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3598753405(L_0, /*hidden argument*/Dictionary_2__ctor_m3598753405_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t309261261 * L_1 = V_0;
		ProductDefinition_t1942475268 * L_2 = ___product0;
		NullCheck(L_2);
		String_t* L_3 = ProductDefinition_get_id_m264072292(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Dictionary_2_Add_m1128763565(L_1, _stringLiteral287061489, L_3, /*hidden argument*/Dictionary_2_Add_m1128763565_MethodInfo_var);
		Dictionary_2_t309261261 * L_4 = V_0;
		ProductDefinition_t1942475268 * L_5 = ___product0;
		NullCheck(L_5);
		String_t* L_6 = ProductDefinition_get_storeSpecificId_m2251287741(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Dictionary_2_Add_m1128763565(L_4, _stringLiteral667782618, L_6, /*hidden argument*/Dictionary_2_Add_m1128763565_MethodInfo_var);
		Dictionary_2_t309261261 * L_7 = V_0;
		ProductDefinition_t1942475268 * L_8 = ___product0;
		NullCheck(L_8);
		int32_t L_9 = ProductDefinition_get_type_m590914665(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Il2CppObject * L_10 = Box(ProductType_t2754455291_il2cpp_TypeInfo_var, (&V_1));
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_7);
		Dictionary_2_Add_m1128763565(L_7, _stringLiteral1421151742, L_11, /*hidden argument*/Dictionary_2_Add_m1128763565_MethodInfo_var);
		Dictionary_2_t309261261 * L_12 = V_0;
		V_2 = L_12;
		goto IL_004f;
	}

IL_004f:
	{
		Dictionary_2_t309261261 * L_13 = V_2;
		return L_13;
	}
}
// System.Void UnityEngine.Purchasing.NativeJSONStore::.ctor()
extern "C"  void NativeJSONStore__ctor_m812188232 (NativeJSONStore_t3685388740 * __this, const MethodInfo* method)
{
	{
		AbstractStore__ctor_m212291193(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.NativeJSONStore::SetNativeStore(UnityEngine.Purchasing.INativeStore)
extern "C"  void NativeJSONStore_SetNativeStore_m1863195906 (NativeJSONStore_t3685388740 * __this, Il2CppObject * ___native0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___native0;
		__this->set_store_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.NativeJSONStore::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern "C"  void NativeJSONStore_Initialize_m4084255593 (NativeJSONStore_t3685388740 * __this, Il2CppObject * ___callback0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___callback0;
		__this->set_unity_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.NativeJSONStore::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern Il2CppClass* INativeStore_t3203646079_il2cpp_TypeInfo_var;
extern const uint32_t NativeJSONStore_RetrieveProducts_m1489118900_MetadataUsageId;
extern "C"  void NativeJSONStore_RetrieveProducts_m1489118900 (NativeJSONStore_t3685388740 * __this, ReadOnlyCollection_1_t2128260960 * ___products0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NativeJSONStore_RetrieveProducts_m1489118900_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_store_1();
		ReadOnlyCollection_1_t2128260960 * L_1 = ___products0;
		String_t* L_2 = JSONSerializer_SerializeProductDefs_m2515728532(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void UnityEngine.Purchasing.INativeStore::RetrieveProducts(System.String) */, INativeStore_t3203646079_il2cpp_TypeInfo_var, L_0, L_2);
		return;
	}
}
// System.Void UnityEngine.Purchasing.NativeJSONStore::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern Il2CppClass* INativeStore_t3203646079_il2cpp_TypeInfo_var;
extern const uint32_t NativeJSONStore_Purchase_m1927312516_MetadataUsageId;
extern "C"  void NativeJSONStore_Purchase_m1927312516 (NativeJSONStore_t3685388740 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___developerPayload1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NativeJSONStore_Purchase_m1927312516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_store_1();
		ProductDefinition_t1942475268 * L_1 = ___product0;
		String_t* L_2 = JSONSerializer_SerializeProductDef_m3948994734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_3 = ___developerPayload1;
		NullCheck(L_0);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(1 /* System.Void UnityEngine.Purchasing.INativeStore::Purchase(System.String,System.String) */, INativeStore_t3203646079_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		return;
	}
}
// System.Void UnityEngine.Purchasing.NativeJSONStore::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern Il2CppClass* INativeStore_t3203646079_il2cpp_TypeInfo_var;
extern const uint32_t NativeJSONStore_FinishTransaction_m559023640_MetadataUsageId;
extern "C"  void NativeJSONStore_FinishTransaction_m559023640 (NativeJSONStore_t3685388740 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___transactionId1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NativeJSONStore_FinishTransaction_m559023640_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		ProductDefinition_t1942475268 * L_0 = ___product0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		G_B3_0 = ((String_t*)(NULL));
		goto IL_0013;
	}

IL_000d:
	{
		ProductDefinition_t1942475268 * L_1 = ___product0;
		String_t* L_2 = JSONSerializer_SerializeProductDef_m3948994734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_0013:
	{
		V_0 = G_B3_0;
		Il2CppObject * L_3 = __this->get_store_1();
		String_t* L_4 = V_0;
		String_t* L_5 = ___transactionId1;
		NullCheck(L_3);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(2 /* System.Void UnityEngine.Purchasing.INativeStore::FinishTransaction(System.String,System.String) */, INativeStore_t3203646079_il2cpp_TypeInfo_var, L_3, L_4, L_5);
		return;
	}
}
// System.Void UnityEngine.Purchasing.NativeJSONStore::OnSetupFailed(System.String)
extern const Il2CppType* InitializationFailureReason_t2954032642_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var;
extern Il2CppClass* IStoreCallback_t2691517565_il2cpp_TypeInfo_var;
extern const uint32_t NativeJSONStore_OnSetupFailed_m2728050561_MetadataUsageId;
extern "C"  void NativeJSONStore_OnSetupFailed_m2728050561 (NativeJSONStore_t3685388740 * __this, String_t* ___reason0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NativeJSONStore_OnSetupFailed_m2728050561_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(InitializationFailureReason_t2954032642_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_1 = ___reason0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = Enum_Parse_m982704874(NULL /*static, unused*/, L_0, L_1, (bool)1, /*hidden argument*/NULL);
		V_0 = ((*(int32_t*)((int32_t*)UnBox (L_2, InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var))));
		Il2CppObject * L_3 = __this->get_unity_0();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		InterfaceActionInvoker1< int32_t >::Invoke(1 /* System.Void UnityEngine.Purchasing.Extension.IStoreCallback::OnSetupFailed(UnityEngine.Purchasing.InitializationFailureReason) */, IStoreCallback_t2691517565_il2cpp_TypeInfo_var, L_3, L_4);
		return;
	}
}
// System.Void UnityEngine.Purchasing.NativeJSONStore::OnProductsRetrieved(System.String)
extern Il2CppClass* IStoreCallback_t2691517565_il2cpp_TypeInfo_var;
extern const uint32_t NativeJSONStore_OnProductsRetrieved_m4005515159_MetadataUsageId;
extern "C"  void NativeJSONStore_OnProductsRetrieved_m4005515159 (NativeJSONStore_t3685388740 * __this, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NativeJSONStore_OnProductsRetrieved_m4005515159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_unity_0();
		String_t* L_1 = ___json0;
		List_1_t2687388655 * L_2 = JSONSerializer_DeserializeProductDescriptions_m2762227127(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< List_1_t2687388655 * >::Invoke(2 /* System.Void UnityEngine.Purchasing.Extension.IStoreCallback::OnProductsRetrieved(System.Collections.Generic.List`1<UnityEngine.Purchasing.Extension.ProductDescription>) */, IStoreCallback_t2691517565_il2cpp_TypeInfo_var, L_0, L_2);
		return;
	}
}
// System.Void UnityEngine.Purchasing.NativeJSONStore::OnPurchaseSucceeded(System.String,System.String,System.String)
extern Il2CppClass* IStoreCallback_t2691517565_il2cpp_TypeInfo_var;
extern const uint32_t NativeJSONStore_OnPurchaseSucceeded_m363409449_MetadataUsageId;
extern "C"  void NativeJSONStore_OnPurchaseSucceeded_m363409449 (NativeJSONStore_t3685388740 * __this, String_t* ___id0, String_t* ___receipt1, String_t* ___transactionID2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NativeJSONStore_OnPurchaseSucceeded_m363409449_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_unity_0();
		String_t* L_1 = ___id0;
		String_t* L_2 = ___receipt1;
		String_t* L_3 = ___transactionID2;
		NullCheck(L_0);
		InterfaceActionInvoker3< String_t*, String_t*, String_t* >::Invoke(3 /* System.Void UnityEngine.Purchasing.Extension.IStoreCallback::OnPurchaseSucceeded(System.String,System.String,System.String) */, IStoreCallback_t2691517565_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3);
		return;
	}
}
// System.Void UnityEngine.Purchasing.NativeJSONStore::OnPurchaseFailed(System.String)
extern Il2CppClass* IStoreCallback_t2691517565_il2cpp_TypeInfo_var;
extern const uint32_t NativeJSONStore_OnPurchaseFailed_m4091409929_MetadataUsageId;
extern "C"  void NativeJSONStore_OnPurchaseFailed_m4091409929 (NativeJSONStore_t3685388740 * __this, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NativeJSONStore_OnPurchaseFailed_m4091409929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_unity_0();
		String_t* L_1 = ___json0;
		PurchaseFailureDescription_t1607114611 * L_2 = JSONSerializer_DeserializeFailureReason_m1834198275(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< PurchaseFailureDescription_t1607114611 * >::Invoke(4 /* System.Void UnityEngine.Purchasing.Extension.IStoreCallback::OnPurchaseFailed(UnityEngine.Purchasing.Extension.PurchaseFailureDescription) */, IStoreCallback_t2691517565_il2cpp_TypeInfo_var, L_0, L_2);
		return;
	}
}
// System.Void UnityEngine.Purchasing.RawStoreProvider::.ctor()
extern "C"  void RawStoreProvider__ctor_m2614959374 (RawStoreProvider_t3477922056 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.RawStoreProvider::GetAndroidStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.AndroidStore,UnityEngine.Purchasing.Extension.IPurchasingBinder,Uniject.IUtil)
extern Il2CppClass* AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var;
extern Il2CppClass* ScriptingUnityCallback_t906080071_il2cpp_TypeInfo_var;
extern Il2CppClass* JavaBridge_t44746847_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* AmazonAppStoreStoreExtensions_t1518886395_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaStore_t2772549594_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* SamsungAppsStoreExtensions_t3441062041_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const MethodInfo* AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4251328308_m2569078019_MethodInfo_var;
extern const MethodInfo* IPurchasingBinder_RegisterExtension_TisIAmazonExtensions_t3890253245_m4006255990_MethodInfo_var;
extern const MethodInfo* IPurchasingBinder_RegisterConfiguration_TisIAmazonConfiguration_t3016942165_m2497941269_MethodInfo_var;
extern const MethodInfo* IPurchasingBinder_RegisterExtension_TisISamsungAppsExtensions_t3429739537_m113369440_MethodInfo_var;
extern const MethodInfo* IPurchasingBinder_RegisterConfiguration_TisISamsungAppsConfiguration_t4066821689_m1118266513_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral717246814;
extern Il2CppCodeGenString* _stringLiteral3919307841;
extern Il2CppCodeGenString* _stringLiteral2587445804;
extern Il2CppCodeGenString* _stringLiteral2570603614;
extern const uint32_t RawStoreProvider_GetAndroidStore_m2680390634_MetadataUsageId;
extern "C"  Il2CppObject * RawStoreProvider_GetAndroidStore_m2680390634 (RawStoreProvider_t3477922056 * __this, Il2CppObject * ___callback0, int32_t ___store1, Il2CppObject * ___binder2, Il2CppObject * ___util3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RawStoreProvider_GetAndroidStore_m2680390634_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AndroidJavaClass_t2973420583 * V_0 = NULL;
	JavaBridge_t44746847 * V_1 = NULL;
	AndroidJavaObject_t4251328308 * V_2 = NULL;
	AmazonAppStoreStoreExtensions_t1518886395 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	AndroidJavaClass_t2973420583 * V_5 = NULL;
	JavaBridge_t44746847 * V_6 = NULL;
	AndroidJavaObject_t4251328308 * V_7 = NULL;
	AndroidJavaClass_t2973420583 * V_8 = NULL;
	SamsungAppsStoreExtensions_t3441062041 * V_9 = NULL;
	JavaBridge_t44746847 * V_10 = NULL;
	AndroidJavaObject_t4251328308 * V_11 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___store1;
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = ___store1;
		if (!L_1)
		{
			goto IL_0079;
		}
	}
	{
		int32_t L_2 = ___store1;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_00cb;
		}
	}
	{
		goto IL_0142;
	}

IL_001a:
	{
		AndroidJavaClass_t2973420583 * L_3 = (AndroidJavaClass_t2973420583 *)il2cpp_codegen_object_new(AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m3221829804(L_3, _stringLiteral717246814, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_4 = ___callback0;
		Il2CppObject * L_5 = ___util3;
		ScriptingUnityCallback_t906080071 * L_6 = (ScriptingUnityCallback_t906080071 *)il2cpp_codegen_object_new(ScriptingUnityCallback_t906080071_il2cpp_TypeInfo_var);
		ScriptingUnityCallback__ctor_m3665273412(L_6, L_4, L_5, /*hidden argument*/NULL);
		JavaBridge_t44746847 * L_7 = (JavaBridge_t44746847 *)il2cpp_codegen_object_new(JavaBridge_t44746847_il2cpp_TypeInfo_var);
		JavaBridge__ctor_m2145695863(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		AndroidJavaClass_t2973420583 * L_8 = V_0;
		ObjectU5BU5D_t3614634134* L_9 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		JavaBridge_t44746847 * L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_10);
		NullCheck(L_8);
		AndroidJavaObject_t4251328308 * L_11 = AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4251328308_m2569078019(L_8, _stringLiteral3919307841, L_9, /*hidden argument*/AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4251328308_m2569078019_MethodInfo_var);
		V_2 = L_11;
		AndroidJavaObject_t4251328308 * L_12 = V_2;
		AmazonAppStoreStoreExtensions_t1518886395 * L_13 = (AmazonAppStoreStoreExtensions_t1518886395 *)il2cpp_codegen_object_new(AmazonAppStoreStoreExtensions_t1518886395_il2cpp_TypeInfo_var);
		AmazonAppStoreStoreExtensions__ctor_m2576379724(L_13, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		Il2CppObject * L_14 = ___binder2;
		AmazonAppStoreStoreExtensions_t1518886395 * L_15 = V_3;
		NullCheck(L_14);
		GenericInterfaceActionInvoker1< Il2CppObject * >::Invoke(IPurchasingBinder_RegisterExtension_TisIAmazonExtensions_t3890253245_m4006255990_MethodInfo_var, L_14, L_15);
		Il2CppObject * L_16 = ___binder2;
		AmazonAppStoreStoreExtensions_t1518886395 * L_17 = V_3;
		NullCheck(L_16);
		GenericInterfaceActionInvoker1< Il2CppObject * >::Invoke(IPurchasingBinder_RegisterConfiguration_TisIAmazonConfiguration_t3016942165_m2497941269_MethodInfo_var, L_16, L_17);
		AndroidJavaObject_t4251328308 * L_18 = V_2;
		AndroidJavaStore_t2772549594 * L_19 = (AndroidJavaStore_t2772549594 *)il2cpp_codegen_object_new(AndroidJavaStore_t2772549594_il2cpp_TypeInfo_var);
		AndroidJavaStore__ctor_m2037585869(L_19, L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		IL2CPP_LEAVE(0x148, FINALLY_006c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		{
			AndroidJavaClass_t2973420583 * L_20 = V_0;
			if (!L_20)
			{
				goto IL_0078;
			}
		}

IL_0072:
		{
			AndroidJavaClass_t2973420583 * L_21 = V_0;
			NullCheck(L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_21);
		}

IL_0078:
		{
			IL2CPP_END_FINALLY(108)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x148, IL_0148)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0079:
	{
		AndroidJavaClass_t2973420583 * L_22 = (AndroidJavaClass_t2973420583 *)il2cpp_codegen_object_new(AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m3221829804(L_22, _stringLiteral2587445804, /*hidden argument*/NULL);
		V_5 = L_22;
	}

IL_0085:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_23 = ___callback0;
		Il2CppObject * L_24 = ___util3;
		ScriptingUnityCallback_t906080071 * L_25 = (ScriptingUnityCallback_t906080071 *)il2cpp_codegen_object_new(ScriptingUnityCallback_t906080071_il2cpp_TypeInfo_var);
		ScriptingUnityCallback__ctor_m3665273412(L_25, L_23, L_24, /*hidden argument*/NULL);
		JavaBridge_t44746847 * L_26 = (JavaBridge_t44746847 *)il2cpp_codegen_object_new(JavaBridge_t44746847_il2cpp_TypeInfo_var);
		JavaBridge__ctor_m2145695863(L_26, L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		AndroidJavaClass_t2973420583 * L_27 = V_5;
		ObjectU5BU5D_t3614634134* L_28 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		JavaBridge_t44746847 * L_29 = V_6;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 0);
		ArrayElementTypeCheck (L_28, L_29);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_29);
		NullCheck(L_27);
		AndroidJavaObject_t4251328308 * L_30 = AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4251328308_m2569078019(L_27, _stringLiteral3919307841, L_28, /*hidden argument*/AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4251328308_m2569078019_MethodInfo_var);
		V_7 = L_30;
		AndroidJavaObject_t4251328308 * L_31 = V_7;
		AndroidJavaStore_t2772549594 * L_32 = (AndroidJavaStore_t2772549594 *)il2cpp_codegen_object_new(AndroidJavaStore_t2772549594_il2cpp_TypeInfo_var);
		AndroidJavaStore__ctor_m2037585869(L_32, L_31, /*hidden argument*/NULL);
		V_4 = L_32;
		IL2CPP_LEAVE(0x148, FINALLY_00bc);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00bc;
	}

FINALLY_00bc:
	{ // begin finally (depth: 1)
		{
			AndroidJavaClass_t2973420583 * L_33 = V_5;
			if (!L_33)
			{
				goto IL_00ca;
			}
		}

IL_00c3:
		{
			AndroidJavaClass_t2973420583 * L_34 = V_5;
			NullCheck(L_34);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_34);
		}

IL_00ca:
		{
			IL2CPP_END_FINALLY(188)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(188)
	{
		IL2CPP_JUMP_TBL(0x148, IL_0148)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00cb:
	{
		AndroidJavaClass_t2973420583 * L_35 = (AndroidJavaClass_t2973420583 *)il2cpp_codegen_object_new(AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m3221829804(L_35, _stringLiteral2570603614, /*hidden argument*/NULL);
		V_8 = L_35;
	}

IL_00d7:
	try
	{ // begin try (depth: 1)
		SamsungAppsStoreExtensions_t3441062041 * L_36 = (SamsungAppsStoreExtensions_t3441062041 *)il2cpp_codegen_object_new(SamsungAppsStoreExtensions_t3441062041_il2cpp_TypeInfo_var);
		SamsungAppsStoreExtensions__ctor_m1632218763(L_36, /*hidden argument*/NULL);
		V_9 = L_36;
		Il2CppObject * L_37 = ___callback0;
		Il2CppObject * L_38 = ___util3;
		ScriptingUnityCallback_t906080071 * L_39 = (ScriptingUnityCallback_t906080071 *)il2cpp_codegen_object_new(ScriptingUnityCallback_t906080071_il2cpp_TypeInfo_var);
		ScriptingUnityCallback__ctor_m3665273412(L_39, L_37, L_38, /*hidden argument*/NULL);
		JavaBridge_t44746847 * L_40 = (JavaBridge_t44746847 *)il2cpp_codegen_object_new(JavaBridge_t44746847_il2cpp_TypeInfo_var);
		JavaBridge__ctor_m2145695863(L_40, L_39, /*hidden argument*/NULL);
		V_10 = L_40;
		AndroidJavaClass_t2973420583 * L_41 = V_8;
		ObjectU5BU5D_t3614634134* L_42 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		JavaBridge_t44746847 * L_43 = V_10;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 0);
		ArrayElementTypeCheck (L_42, L_43);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_43);
		ObjectU5BU5D_t3614634134* L_44 = L_42;
		SamsungAppsStoreExtensions_t3441062041 * L_45 = V_9;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 1);
		ArrayElementTypeCheck (L_44, L_45);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_45);
		NullCheck(L_41);
		AndroidJavaObject_t4251328308 * L_46 = AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4251328308_m2569078019(L_41, _stringLiteral3919307841, L_44, /*hidden argument*/AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4251328308_m2569078019_MethodInfo_var);
		V_11 = L_46;
		SamsungAppsStoreExtensions_t3441062041 * L_47 = V_9;
		AndroidJavaObject_t4251328308 * L_48 = V_11;
		NullCheck(L_47);
		SamsungAppsStoreExtensions_SetAndroidJavaObject_m2653292706(L_47, L_48, /*hidden argument*/NULL);
		Il2CppObject * L_49 = ___binder2;
		SamsungAppsStoreExtensions_t3441062041 * L_50 = V_9;
		NullCheck(L_49);
		GenericInterfaceActionInvoker1< Il2CppObject * >::Invoke(IPurchasingBinder_RegisterExtension_TisISamsungAppsExtensions_t3429739537_m113369440_MethodInfo_var, L_49, L_50);
		Il2CppObject * L_51 = ___binder2;
		SamsungAppsStoreExtensions_t3441062041 * L_52 = V_9;
		NullCheck(L_51);
		GenericInterfaceActionInvoker1< Il2CppObject * >::Invoke(IPurchasingBinder_RegisterConfiguration_TisISamsungAppsConfiguration_t4066821689_m1118266513_MethodInfo_var, L_51, L_52);
		AndroidJavaObject_t4251328308 * L_53 = V_11;
		AndroidJavaStore_t2772549594 * L_54 = (AndroidJavaStore_t2772549594 *)il2cpp_codegen_object_new(AndroidJavaStore_t2772549594_il2cpp_TypeInfo_var);
		AndroidJavaStore__ctor_m2037585869(L_54, L_53, /*hidden argument*/NULL);
		V_4 = L_54;
		IL2CPP_LEAVE(0x148, FINALLY_0133);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0133;
	}

FINALLY_0133:
	{ // begin finally (depth: 1)
		{
			AndroidJavaClass_t2973420583 * L_55 = V_8;
			if (!L_55)
			{
				goto IL_0141;
			}
		}

IL_013a:
		{
			AndroidJavaClass_t2973420583 * L_56 = V_8;
			NullCheck(L_56);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_56);
		}

IL_0141:
		{
			IL2CPP_END_FINALLY(307)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(307)
	{
		IL2CPP_JUMP_TBL(0x148, IL_0148)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0142:
	{
		NotImplementedException_t2785117854 * L_57 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_57, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_57);
	}

IL_0148:
	{
		Il2CppObject * L_58 = V_4;
		return L_58;
	}
}
// UnityEngine.Purchasing.INativeAppleStore UnityEngine.Purchasing.RawStoreProvider::GetStorekit(UnityEngine.Purchasing.IUnityCallback)
extern Il2CppClass* iOSStoreBindings_t2633471826_il2cpp_TypeInfo_var;
extern Il2CppClass* OSXStoreBindings_t116576999_il2cpp_TypeInfo_var;
extern const uint32_t RawStoreProvider_GetStorekit_m1533139424_MetadataUsageId;
extern "C"  Il2CppObject * RawStoreProvider_GetStorekit_m1533139424 (RawStoreProvider_t3477922056 * __this, Il2CppObject * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RawStoreProvider_GetStorekit_m1533139424_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)8)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)31)))))
		{
			goto IL_0024;
		}
	}

IL_0018:
	{
		iOSStoreBindings_t2633471826 * L_2 = (iOSStoreBindings_t2633471826 *)il2cpp_codegen_object_new(iOSStoreBindings_t2633471826_il2cpp_TypeInfo_var);
		iOSStoreBindings__ctor_m1139108122(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_002f;
	}

IL_0024:
	{
		OSXStoreBindings_t116576999 * L_3 = (OSXStoreBindings_t116576999 *)il2cpp_codegen_object_new(OSXStoreBindings_t116576999_il2cpp_TypeInfo_var);
		OSXStoreBindings__ctor_m3914056605(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_002f;
	}

IL_002f:
	{
		Il2CppObject * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Purchasing.INativeTizenStore UnityEngine.Purchasing.RawStoreProvider::GetTizenStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.Extension.IPurchasingBinder)
extern Il2CppClass* TizenStoreBindings_t1951392817_il2cpp_TypeInfo_var;
extern const uint32_t RawStoreProvider_GetTizenStore_m900806873_MetadataUsageId;
extern "C"  Il2CppObject * RawStoreProvider_GetTizenStore_m900806873 (RawStoreProvider_t3477922056 * __this, Il2CppObject * ___callback0, Il2CppObject * ___binder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RawStoreProvider_GetTizenStore_m900806873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		TizenStoreBindings_t1951392817 * L_0 = (TizenStoreBindings_t1951392817 *)il2cpp_codegen_object_new(TizenStoreBindings_t1951392817_il2cpp_TypeInfo_var);
		TizenStoreBindings__ctor_m4223940561(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.SamsungAppsStoreExtensions::.ctor()
extern Il2CppCodeGenString* _stringLiteral3158420989;
extern const uint32_t SamsungAppsStoreExtensions__ctor_m1632218763_MetadataUsageId;
extern "C"  void SamsungAppsStoreExtensions__ctor_m1632218763 (SamsungAppsStoreExtensions_t3441062041 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SamsungAppsStoreExtensions__ctor_m1632218763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AndroidJavaProxy__ctor_m4016180768(__this, _stringLiteral3158420989, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.SamsungAppsStoreExtensions::SetAndroidJavaObject(UnityEngine.AndroidJavaObject)
extern "C"  void SamsungAppsStoreExtensions_SetAndroidJavaObject_m2653292706 (SamsungAppsStoreExtensions_t3441062041 * __this, AndroidJavaObject_t4251328308 * ___java0, const MethodInfo* method)
{
	{
		AndroidJavaObject_t4251328308 * L_0 = ___java0;
		__this->set_m_Java_2(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.SamsungAppsStoreExtensions::SetMode(UnityEngine.Purchasing.SamsungAppsMode)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* SamsungAppsMode_t2214306743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2900177237;
extern const uint32_t SamsungAppsStoreExtensions_SetMode_m430328940_MetadataUsageId;
extern "C"  void SamsungAppsStoreExtensions_SetMode_m430328940 (SamsungAppsStoreExtensions_t3441062041 * __this, int32_t ___mode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SamsungAppsStoreExtensions_SetMode_m430328940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AndroidJavaObject_t4251328308 * L_0 = __this->get_m_Java_2();
		ObjectU5BU5D_t3614634134* L_1 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_2 = Box(SamsungAppsMode_t2214306743_il2cpp_TypeInfo_var, (&___mode0));
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck(L_0);
		AndroidJavaObject_Call_m3681854287(L_0, _stringLiteral2900177237, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.SamsungAppsStoreExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral513384815;
extern const uint32_t SamsungAppsStoreExtensions_RestoreTransactions_m3530299045_MetadataUsageId;
extern "C"  void SamsungAppsStoreExtensions_RestoreTransactions_m3530299045 (SamsungAppsStoreExtensions_t3441062041 * __this, Action_1_t3627374100 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SamsungAppsStoreExtensions_RestoreTransactions_m3530299045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = ___callback0;
		__this->set_m_RestoreCallback_1(L_0);
		AndroidJavaObject_t4251328308 * L_1 = __this->get_m_Java_2();
		NullCheck(L_1);
		AndroidJavaObject_Call_m3681854287(L_1, _stringLiteral513384815, ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.ScriptingUnityCallback::.ctor(UnityEngine.Purchasing.IUnityCallback,Uniject.IUtil)
extern "C"  void ScriptingUnityCallback__ctor_m3665273412 (ScriptingUnityCallback_t906080071 * __this, Il2CppObject * ___forwardTo0, Il2CppObject * ___util1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___forwardTo0;
		__this->set_forwardTo_0(L_0);
		Il2CppObject * L_1 = ___util1;
		__this->set_util_1(L_1);
		return;
	}
}
// System.String UnityEngine.Purchasing.SerializationExtensions::TryGetString(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern const MethodInfo* Dictionary_2_ContainsKey_m1533770720_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m464793699_MethodInfo_var;
extern const uint32_t SerializationExtensions_TryGetString_m1096911238_MetadataUsageId;
extern "C"  String_t* SerializationExtensions_TryGetString_m1096911238 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SerializationExtensions_TryGetString_m1096911238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Dictionary_2_t309261261 * L_0 = ___dic0;
		String_t* L_1 = ___key1;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m1533770720(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1533770720_MethodInfo_var);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Dictionary_2_t309261261 * L_3 = ___dic0;
		String_t* L_4 = ___key1;
		NullCheck(L_3);
		Il2CppObject * L_5 = Dictionary_2_get_Item_m464793699(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		Dictionary_2_t309261261 * L_6 = ___dic0;
		String_t* L_7 = ___key1;
		NullCheck(L_6);
		Il2CppObject * L_8 = Dictionary_2_get_Item_m464793699(L_6, L_7, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		V_0 = L_9;
		goto IL_0035;
	}

IL_002d:
	{
	}

IL_002e:
	{
		V_0 = (String_t*)NULL;
		goto IL_0035;
	}

IL_0035:
	{
		String_t* L_10 = V_0;
		return L_10;
	}
}
// System.Void UnityEngine.Purchasing.StandardPurchasingModule::.ctor(Uniject.IUtil,UnityEngine.ILogger,UnityEngine.Purchasing.IRawStoreProvider,UnityEngine.RuntimePlatform,UnityEngine.Purchasing.AndroidStore)
extern "C"  void StandardPurchasingModule__ctor_m3405157511 (StandardPurchasingModule_t4003664591 * __this, Il2CppObject * ___util0, Il2CppObject * ___logger1, Il2CppObject * ___platformProvider2, int32_t ___platform3, int32_t ___android4, const MethodInfo* method)
{
	{
		AbstractPurchasingModule__ctor_m3505602562(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___util0;
		__this->set_m_Util_4(L_0);
		Il2CppObject * L_1 = ___logger1;
		__this->set_m_Logger_5(L_1);
		Il2CppObject * L_2 = ___platformProvider2;
		__this->set_m_PlatformProvider_2(L_2);
		int32_t L_3 = ___platform3;
		__this->set_m_RuntimePlatform_3(L_3);
		StandardPurchasingModule_set_useFakeStoreUIMode_m3278247934(__this, 0, /*hidden argument*/NULL);
		int32_t L_4 = ___android4;
		__this->set_m_AndroidPlatform_1(L_4);
		return;
	}
}
// UnityEngine.Purchasing.AndroidStore UnityEngine.Purchasing.StandardPurchasingModule::get_androidStore()
extern "C"  int32_t StandardPurchasingModule_get_androidStore_m1436760510 (StandardPurchasingModule_t4003664591 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_AndroidPlatform_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Purchasing.FakeStoreUIMode UnityEngine.Purchasing.StandardPurchasingModule::get_useFakeStoreUIMode()
extern "C"  int32_t StandardPurchasingModule_get_useFakeStoreUIMode_m1753364605 (StandardPurchasingModule_t4003664591 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CuseFakeStoreUIModeU3Ek__BackingField_9();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.StandardPurchasingModule::set_useFakeStoreUIMode(UnityEngine.Purchasing.FakeStoreUIMode)
extern "C"  void StandardPurchasingModule_set_useFakeStoreUIMode_m3278247934 (StandardPurchasingModule_t4003664591 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CuseFakeStoreUIModeU3Ek__BackingField_9(L_0);
		return;
	}
}
// UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule::Instance()
extern Il2CppClass* StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var;
extern const uint32_t StandardPurchasingModule_Instance_m2889845773_MetadataUsageId;
extern "C"  StandardPurchasingModule_t4003664591 * StandardPurchasingModule_Instance_m2889845773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StandardPurchasingModule_Instance_m2889845773_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StandardPurchasingModule_t4003664591 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_0 = StandardPurchasingModule_Instance_m1861703984(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		StandardPurchasingModule_t4003664591 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule::Instance(UnityEngine.Purchasing.AndroidStore)
extern Il2CppClass* StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* TextAsset_t3973159845_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* RawStoreProvider_t3477922056_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisUnityUtil_t166323129_m3875097307_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1700088252;
extern Il2CppCodeGenString* _stringLiteral378094258;
extern const uint32_t StandardPurchasingModule_Instance_m1861703984_MetadataUsageId;
extern "C"  StandardPurchasingModule_t4003664591 * StandardPurchasingModule_Instance_m1861703984 (Il2CppObject * __this /* static, unused */, int32_t ___androidStore0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StandardPurchasingModule_Instance_m1861703984_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	UnityUtil_t166323129 * V_1 = NULL;
	TextAsset_t3973159845 * V_2 = NULL;
	int32_t V_3 = 0;
	StandardPurchasingModule_t4003664591 * V_4 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_0 = ((StandardPurchasingModule_t4003664591_StaticFields*)StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var->static_fields)->get_ModuleInstance_6();
		if (L_0)
		{
			goto IL_008e;
		}
	}
	{
		GameObject_t1756533147 * L_1 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_1, _stringLiteral1700088252, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		Object_set_hideFlags_m2204253440(L_3, 3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = V_0;
		NullCheck(L_4);
		UnityUtil_t166323129 * L_5 = GameObject_AddComponent_TisUnityUtil_t166323129_m3875097307(L_4, /*hidden argument*/GameObject_AddComponent_TisUnityUtil_t166323129_m3875097307_MethodInfo_var);
		V_1 = L_5;
		int32_t L_6 = ___androidStore0;
		if ((!(((uint32_t)L_6) == ((uint32_t)3))))
		{
			goto IL_0072;
		}
	}
	{
		___androidStore0 = 0;
		Object_t1021602117 * L_7 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral378094258, /*hidden argument*/NULL);
		V_2 = ((TextAsset_t3973159845 *)IsInstClass(L_7, TextAsset_t3973159845_il2cpp_TypeInfo_var));
		TextAsset_t3973159845 * L_8 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)NULL, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0071;
		}
	}
	{
		TextAsset_t3973159845 * L_10 = V_2;
		NullCheck(L_10);
		String_t* L_11 = TextAsset_get_text_m2589865997(L_10, /*hidden argument*/NULL);
		StoreConfiguration_t2466794143 * L_12 = StoreConfiguration_Deserialize_m1161854657(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = StoreConfiguration_get_androidStore_m4060547582(L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		int32_t L_14 = V_3;
		if ((((int32_t)L_14) == ((int32_t)3)))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_15 = V_3;
		___androidStore0 = L_15;
	}

IL_0070:
	{
	}

IL_0071:
	{
	}

IL_0072:
	{
		UnityUtil_t166323129 * L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_17 = Debug_get_logger_m4173808038(NULL /*static, unused*/, /*hidden argument*/NULL);
		RawStoreProvider_t3477922056 * L_18 = (RawStoreProvider_t3477922056 *)il2cpp_codegen_object_new(RawStoreProvider_t3477922056_il2cpp_TypeInfo_var);
		RawStoreProvider__ctor_m2614959374(L_18, /*hidden argument*/NULL);
		int32_t L_19 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_20 = ___androidStore0;
		StandardPurchasingModule_t4003664591 * L_21 = (StandardPurchasingModule_t4003664591 *)il2cpp_codegen_object_new(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule__ctor_m3405157511(L_21, L_16, L_17, L_18, L_19, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		((StandardPurchasingModule_t4003664591_StaticFields*)StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var->static_fields)->set_ModuleInstance_6(L_21);
	}

IL_008e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_22 = ((StandardPurchasingModule_t4003664591_StaticFields*)StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var->static_fields)->get_ModuleInstance_6();
		V_4 = L_22;
		goto IL_009a;
	}

IL_009a:
	{
		StandardPurchasingModule_t4003664591 * L_23 = V_4;
		return L_23;
	}
}
// System.Void UnityEngine.Purchasing.StandardPurchasingModule::Configure()
extern Il2CppClass* FakeAppleExtensions_t4039399289_il2cpp_TypeInfo_var;
extern Il2CppClass* FakeAmazonExtensions_t2261777661_il2cpp_TypeInfo_var;
extern Il2CppClass* FakeSamsungAppsExtensions_t1522853249_il2cpp_TypeInfo_var;
extern Il2CppClass* FakeAppleConfiguation_t4052738437_il2cpp_TypeInfo_var;
extern Il2CppClass* MicrosoftConfiguration_t1310600573_il2cpp_TypeInfo_var;
extern Il2CppClass* FakeGooglePlayConfiguration_t737012266_il2cpp_TypeInfo_var;
extern Il2CppClass* FakeTizenStoreConfiguration_t3055550456_il2cpp_TypeInfo_var;
extern const MethodInfo* AbstractPurchasingModule_BindExtension_TisIAppleExtensions_t1627764765_m2766167481_MethodInfo_var;
extern const MethodInfo* AbstractPurchasingModule_BindExtension_TisIAmazonExtensions_t3890253245_m2976483159_MethodInfo_var;
extern const MethodInfo* AbstractPurchasingModule_BindExtension_TisISamsungAppsExtensions_t3429739537_m2635797151_MethodInfo_var;
extern const MethodInfo* AbstractPurchasingModule_BindConfiguration_TisIAppleConfiguration_t3277762425_m3106281156_MethodInfo_var;
extern const MethodInfo* AbstractPurchasingModule_BindConfiguration_TisIMicrosoftConfiguration_t1212838845_m2316685840_MethodInfo_var;
extern const MethodInfo* AbstractPurchasingModule_BindConfiguration_TisIGooglePlayConfiguration_t2615679878_m4220697187_MethodInfo_var;
extern const MethodInfo* AbstractPurchasingModule_BindConfiguration_TisIAmazonConfiguration_t3016942165_m631587310_MethodInfo_var;
extern const MethodInfo* AbstractPurchasingModule_BindConfiguration_TisISamsungAppsConfiguration_t4066821689_m610130824_MethodInfo_var;
extern const MethodInfo* AbstractPurchasingModule_BindConfiguration_TisITizenStoreConfiguration_t2900348728_m3915163361_MethodInfo_var;
extern const MethodInfo* AbstractPurchasingModule_BindConfiguration_TisIAndroidStoreSelection_t3134941501_m905543658_MethodInfo_var;
extern const uint32_t StandardPurchasingModule_Configure_m2631124963_MetadataUsageId;
extern "C"  void StandardPurchasingModule_Configure_m2631124963 (StandardPurchasingModule_t4003664591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StandardPurchasingModule_Configure_m2631124963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FakeAppleExtensions_t4039399289 * L_0 = (FakeAppleExtensions_t4039399289 *)il2cpp_codegen_object_new(FakeAppleExtensions_t4039399289_il2cpp_TypeInfo_var);
		FakeAppleExtensions__ctor_m1953807163(L_0, /*hidden argument*/NULL);
		AbstractPurchasingModule_BindExtension_TisIAppleExtensions_t1627764765_m2766167481(__this, L_0, /*hidden argument*/AbstractPurchasingModule_BindExtension_TisIAppleExtensions_t1627764765_m2766167481_MethodInfo_var);
		FakeAmazonExtensions_t2261777661 * L_1 = (FakeAmazonExtensions_t2261777661 *)il2cpp_codegen_object_new(FakeAmazonExtensions_t2261777661_il2cpp_TypeInfo_var);
		FakeAmazonExtensions__ctor_m2802760721(L_1, /*hidden argument*/NULL);
		AbstractPurchasingModule_BindExtension_TisIAmazonExtensions_t3890253245_m2976483159(__this, L_1, /*hidden argument*/AbstractPurchasingModule_BindExtension_TisIAmazonExtensions_t3890253245_m2976483159_MethodInfo_var);
		FakeSamsungAppsExtensions_t1522853249 * L_2 = (FakeSamsungAppsExtensions_t1522853249 *)il2cpp_codegen_object_new(FakeSamsungAppsExtensions_t1522853249_il2cpp_TypeInfo_var);
		FakeSamsungAppsExtensions__ctor_m3771901773(L_2, /*hidden argument*/NULL);
		AbstractPurchasingModule_BindExtension_TisISamsungAppsExtensions_t3429739537_m2635797151(__this, L_2, /*hidden argument*/AbstractPurchasingModule_BindExtension_TisISamsungAppsExtensions_t3429739537_m2635797151_MethodInfo_var);
		FakeAppleConfiguation_t4052738437 * L_3 = (FakeAppleConfiguation_t4052738437 *)il2cpp_codegen_object_new(FakeAppleConfiguation_t4052738437_il2cpp_TypeInfo_var);
		FakeAppleConfiguation__ctor_m217023035(L_3, /*hidden argument*/NULL);
		AbstractPurchasingModule_BindConfiguration_TisIAppleConfiguration_t3277762425_m3106281156(__this, L_3, /*hidden argument*/AbstractPurchasingModule_BindConfiguration_TisIAppleConfiguration_t3277762425_m3106281156_MethodInfo_var);
		MicrosoftConfiguration_t1310600573 * L_4 = (MicrosoftConfiguration_t1310600573 *)il2cpp_codegen_object_new(MicrosoftConfiguration_t1310600573_il2cpp_TypeInfo_var);
		MicrosoftConfiguration__ctor_m1885361522(L_4, __this, /*hidden argument*/NULL);
		AbstractPurchasingModule_BindConfiguration_TisIMicrosoftConfiguration_t1212838845_m2316685840(__this, L_4, /*hidden argument*/AbstractPurchasingModule_BindConfiguration_TisIMicrosoftConfiguration_t1212838845_m2316685840_MethodInfo_var);
		FakeGooglePlayConfiguration_t737012266 * L_5 = (FakeGooglePlayConfiguration_t737012266 *)il2cpp_codegen_object_new(FakeGooglePlayConfiguration_t737012266_il2cpp_TypeInfo_var);
		FakeGooglePlayConfiguration__ctor_m1997483180(L_5, /*hidden argument*/NULL);
		AbstractPurchasingModule_BindConfiguration_TisIGooglePlayConfiguration_t2615679878_m4220697187(__this, L_5, /*hidden argument*/AbstractPurchasingModule_BindConfiguration_TisIGooglePlayConfiguration_t2615679878_m4220697187_MethodInfo_var);
		FakeAmazonExtensions_t2261777661 * L_6 = (FakeAmazonExtensions_t2261777661 *)il2cpp_codegen_object_new(FakeAmazonExtensions_t2261777661_il2cpp_TypeInfo_var);
		FakeAmazonExtensions__ctor_m2802760721(L_6, /*hidden argument*/NULL);
		AbstractPurchasingModule_BindConfiguration_TisIAmazonConfiguration_t3016942165_m631587310(__this, L_6, /*hidden argument*/AbstractPurchasingModule_BindConfiguration_TisIAmazonConfiguration_t3016942165_m631587310_MethodInfo_var);
		FakeSamsungAppsExtensions_t1522853249 * L_7 = (FakeSamsungAppsExtensions_t1522853249 *)il2cpp_codegen_object_new(FakeSamsungAppsExtensions_t1522853249_il2cpp_TypeInfo_var);
		FakeSamsungAppsExtensions__ctor_m3771901773(L_7, /*hidden argument*/NULL);
		AbstractPurchasingModule_BindConfiguration_TisISamsungAppsConfiguration_t4066821689_m610130824(__this, L_7, /*hidden argument*/AbstractPurchasingModule_BindConfiguration_TisISamsungAppsConfiguration_t4066821689_m610130824_MethodInfo_var);
		FakeTizenStoreConfiguration_t3055550456 * L_8 = (FakeTizenStoreConfiguration_t3055550456 *)il2cpp_codegen_object_new(FakeTizenStoreConfiguration_t3055550456_il2cpp_TypeInfo_var);
		FakeTizenStoreConfiguration__ctor_m528490996(L_8, /*hidden argument*/NULL);
		AbstractPurchasingModule_BindConfiguration_TisITizenStoreConfiguration_t2900348728_m3915163361(__this, L_8, /*hidden argument*/AbstractPurchasingModule_BindConfiguration_TisITizenStoreConfiguration_t2900348728_m3915163361_MethodInfo_var);
		MicrosoftConfiguration_t1310600573 * L_9 = (MicrosoftConfiguration_t1310600573 *)il2cpp_codegen_object_new(MicrosoftConfiguration_t1310600573_il2cpp_TypeInfo_var);
		MicrosoftConfiguration__ctor_m1885361522(L_9, __this, /*hidden argument*/NULL);
		AbstractPurchasingModule_BindConfiguration_TisIMicrosoftConfiguration_t1212838845_m2316685840(__this, L_9, /*hidden argument*/AbstractPurchasingModule_BindConfiguration_TisIMicrosoftConfiguration_t1212838845_m2316685840_MethodInfo_var);
		AbstractPurchasingModule_BindConfiguration_TisIAndroidStoreSelection_t3134941501_m905543658(__this, __this, /*hidden argument*/AbstractPurchasingModule_BindConfiguration_TisIAndroidStoreSelection_t3134941501_m905543658_MethodInfo_var);
		StoreInstance_t107230755 * L_10 = __this->get_m_StoreInstance_7();
		if (L_10)
		{
			goto IL_0091;
		}
	}
	{
		StoreInstance_t107230755 * L_11 = StandardPurchasingModule_InstantiateStore_m84868782(__this, /*hidden argument*/NULL);
		__this->set_m_StoreInstance_7(L_11);
	}

IL_0091:
	{
		StoreInstance_t107230755 * L_12 = __this->get_m_StoreInstance_7();
		NullCheck(L_12);
		String_t* L_13 = StoreInstance_get_storeName_m2726299052(L_12, /*hidden argument*/NULL);
		StoreInstance_t107230755 * L_14 = __this->get_m_StoreInstance_7();
		NullCheck(L_14);
		Il2CppObject * L_15 = StoreInstance_get_instance_m971207499(L_14, /*hidden argument*/NULL);
		AbstractPurchasingModule_RegisterStore_m1593178344(__this, L_13, L_15, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance UnityEngine.Purchasing.StandardPurchasingModule::InstantiateStore()
extern Il2CppClass* StoreInstance_t107230755_il2cpp_TypeInfo_var;
extern Il2CppClass* StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2201107195_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2700534657;
extern Il2CppCodeGenString* _stringLiteral1570961022;
extern Il2CppCodeGenString* _stringLiteral3598065758;
extern Il2CppCodeGenString* _stringLiteral859071123;
extern Il2CppCodeGenString* _stringLiteral2328218701;
extern const uint32_t StandardPurchasingModule_InstantiateStore_m84868782_MetadataUsageId;
extern "C"  StoreInstance_t107230755 * StandardPurchasingModule_InstantiateStore_m84868782 (StandardPurchasingModule_t4003664591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StandardPurchasingModule_InstantiateStore_m84868782_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	StoreInstance_t107230755 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_m_RuntimePlatform_3();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 0)
		{
			goto IL_00a2;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 1)
		{
			goto IL_00a2;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 2)
		{
			goto IL_00a2;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 3)
		{
			goto IL_0029;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 4)
		{
			goto IL_0029;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 5)
		{
			goto IL_00b8;
		}
	}

IL_0029:
	{
		int32_t L_2 = V_0;
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 0)
		{
			goto IL_006b;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 1)
		{
			goto IL_0041;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 2)
		{
			goto IL_0041;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)8)) == 3)
		{
			goto IL_0081;
		}
	}

IL_0041:
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)31))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_00ce;
	}

IL_0055:
	{
		Il2CppObject * L_5 = StandardPurchasingModule_InstantiateApple_m1201056034(__this, /*hidden argument*/NULL);
		StoreInstance_t107230755 * L_6 = (StoreInstance_t107230755 *)il2cpp_codegen_object_new(StoreInstance_t107230755_il2cpp_TypeInfo_var);
		StoreInstance__ctor_m4140082908(L_6, _stringLiteral2700534657, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_00e4;
	}

IL_006b:
	{
		Il2CppObject * L_7 = StandardPurchasingModule_InstantiateApple_m1201056034(__this, /*hidden argument*/NULL);
		StoreInstance_t107230755 * L_8 = (StoreInstance_t107230755 *)il2cpp_codegen_object_new(StoreInstance_t107230755_il2cpp_TypeInfo_var);
		StoreInstance__ctor_m4140082908(L_8, _stringLiteral1570961022, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		goto IL_00e4;
	}

IL_0081:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		Dictionary_2_t2149139222 * L_9 = ((StandardPurchasingModule_t4003664591_StaticFields*)StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var->static_fields)->get_AndroidStoreNameMap_8();
		int32_t L_10 = __this->get_m_AndroidPlatform_1();
		NullCheck(L_9);
		String_t* L_11 = Dictionary_2_get_Item_m2201107195(L_9, L_10, /*hidden argument*/Dictionary_2_get_Item_m2201107195_MethodInfo_var);
		Il2CppObject * L_12 = StandardPurchasingModule_InstantiateAndroid_m3011531131(__this, /*hidden argument*/NULL);
		StoreInstance_t107230755 * L_13 = (StoreInstance_t107230755 *)il2cpp_codegen_object_new(StoreInstance_t107230755_il2cpp_TypeInfo_var);
		StoreInstance__ctor_m4140082908(L_13, L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		goto IL_00e4;
	}

IL_00a2:
	{
		Il2CppObject * L_14 = StandardPurchasingModule_instantiateWindowsStore_m857429766(__this, /*hidden argument*/NULL);
		StoreInstance_t107230755 * L_15 = (StoreInstance_t107230755 *)il2cpp_codegen_object_new(StoreInstance_t107230755_il2cpp_TypeInfo_var);
		StoreInstance__ctor_m4140082908(L_15, _stringLiteral3598065758, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		goto IL_00e4;
	}

IL_00b8:
	{
		Il2CppObject * L_16 = StandardPurchasingModule_InstantiateTizen_m2777272342(__this, /*hidden argument*/NULL);
		StoreInstance_t107230755 * L_17 = (StoreInstance_t107230755 *)il2cpp_codegen_object_new(StoreInstance_t107230755_il2cpp_TypeInfo_var);
		StoreInstance__ctor_m4140082908(L_17, _stringLiteral859071123, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		goto IL_00e4;
	}

IL_00ce:
	{
		Il2CppObject * L_18 = StandardPurchasingModule_InstantiateFakeStore_m1788236408(__this, /*hidden argument*/NULL);
		StoreInstance_t107230755 * L_19 = (StoreInstance_t107230755 *)il2cpp_codegen_object_new(StoreInstance_t107230755_il2cpp_TypeInfo_var);
		StoreInstance__ctor_m4140082908(L_19, _stringLiteral2328218701, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		goto IL_00e4;
	}

IL_00e4:
	{
		StoreInstance_t107230755 * L_20 = V_1;
		return L_20;
	}
}
// UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateAndroid()
extern Il2CppClass* NativeJSONStore_t3685388740_il2cpp_TypeInfo_var;
extern Il2CppClass* IRawStoreProvider_t2441088289_il2cpp_TypeInfo_var;
extern const uint32_t StandardPurchasingModule_InstantiateAndroid_m3011531131_MetadataUsageId;
extern "C"  Il2CppObject * StandardPurchasingModule_InstantiateAndroid_m3011531131 (StandardPurchasingModule_t4003664591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StandardPurchasingModule_InstantiateAndroid_m3011531131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NativeJSONStore_t3685388740 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		NativeJSONStore_t3685388740 * L_0 = (NativeJSONStore_t3685388740 *)il2cpp_codegen_object_new(NativeJSONStore_t3685388740_il2cpp_TypeInfo_var);
		NativeJSONStore__ctor_m812188232(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		NativeJSONStore_t3685388740 * L_1 = V_0;
		Il2CppObject * L_2 = __this->get_m_PlatformProvider_2();
		NativeJSONStore_t3685388740 * L_3 = V_0;
		int32_t L_4 = __this->get_m_AndroidPlatform_1();
		Il2CppObject * L_5 = ((AbstractPurchasingModule_t4102635892 *)__this)->get_m_Binder_0();
		Il2CppObject * L_6 = __this->get_m_Util_4();
		NullCheck(L_2);
		Il2CppObject * L_7 = InterfaceFuncInvoker4< Il2CppObject *, Il2CppObject *, int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.IRawStoreProvider::GetAndroidStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.AndroidStore,UnityEngine.Purchasing.Extension.IPurchasingBinder,Uniject.IUtil) */, IRawStoreProvider_t2441088289_il2cpp_TypeInfo_var, L_2, L_3, L_4, L_5, L_6);
		NullCheck(L_1);
		NativeJSONStore_SetNativeStore_m1863195906(L_1, L_7, /*hidden argument*/NULL);
		NativeJSONStore_t3685388740 * L_8 = V_0;
		V_1 = L_8;
		goto IL_0032;
	}

IL_0032:
	{
		Il2CppObject * L_9 = V_1;
		return L_9;
	}
}
// UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateApple()
extern Il2CppClass* AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var;
extern Il2CppClass* IRawStoreProvider_t2441088289_il2cpp_TypeInfo_var;
extern const MethodInfo* AbstractPurchasingModule_BindExtension_TisIAppleExtensions_t1627764765_m2766167481_MethodInfo_var;
extern const uint32_t StandardPurchasingModule_InstantiateApple_m1201056034_MetadataUsageId;
extern "C"  Il2CppObject * StandardPurchasingModule_InstantiateApple_m1201056034 (StandardPurchasingModule_t4003664591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StandardPurchasingModule_InstantiateApple_m1201056034_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AppleStoreImpl_t1301617341 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		Il2CppObject * L_0 = __this->get_m_Util_4();
		AppleStoreImpl_t1301617341 * L_1 = (AppleStoreImpl_t1301617341 *)il2cpp_codegen_object_new(AppleStoreImpl_t1301617341_il2cpp_TypeInfo_var);
		AppleStoreImpl__ctor_m1783914938(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = __this->get_m_PlatformProvider_2();
		AppleStoreImpl_t1301617341 * L_3 = V_0;
		NullCheck(L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(1 /* UnityEngine.Purchasing.INativeAppleStore UnityEngine.Purchasing.IRawStoreProvider::GetStorekit(UnityEngine.Purchasing.IUnityCallback) */, IRawStoreProvider_t2441088289_il2cpp_TypeInfo_var, L_2, L_3);
		V_1 = L_4;
		AppleStoreImpl_t1301617341 * L_5 = V_0;
		Il2CppObject * L_6 = V_1;
		NullCheck(L_5);
		AppleStoreImpl_SetNativeStore_m4003342543(L_5, L_6, /*hidden argument*/NULL);
		AppleStoreImpl_t1301617341 * L_7 = V_0;
		AbstractPurchasingModule_BindExtension_TisIAppleExtensions_t1627764765_m2766167481(__this, L_7, /*hidden argument*/AbstractPurchasingModule_BindExtension_TisIAppleExtensions_t1627764765_m2766167481_MethodInfo_var);
		AppleStoreImpl_t1301617341 * L_8 = V_0;
		V_2 = L_8;
		goto IL_002f;
	}

IL_002f:
	{
		Il2CppObject * L_9 = V_2;
		return L_9;
	}
}
// System.Void UnityEngine.Purchasing.StandardPurchasingModule::UseMockWindowsStore(System.Boolean)
extern "C"  void StandardPurchasingModule_UseMockWindowsStore_m536092621 (StandardPurchasingModule_t4003664591 * __this, bool ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		WinRTStore_t36043095 * L_0 = __this->get_windowsStore_10();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		bool L_1 = ___value0;
		Il2CppObject * L_2 = Factory_Create_m2565271259(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		WinRTStore_t36043095 * L_3 = __this->get_windowsStore_10();
		Il2CppObject * L_4 = V_0;
		NullCheck(L_3);
		WinRTStore_SetWindowsIAP_m1317379130(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::instantiateWindowsStore()
extern Il2CppClass* WinRTStore_t36043095_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern Il2CppClass* IUtil_t2188430191_il2cpp_TypeInfo_var;
extern const MethodInfo* WinRTStore_restoreTransactions_m1421431749_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m309821356_MethodInfo_var;
extern const uint32_t StandardPurchasingModule_instantiateWindowsStore_m857429766_MetadataUsageId;
extern "C"  Il2CppObject * StandardPurchasingModule_instantiateWindowsStore_m857429766 (StandardPurchasingModule_t4003664591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StandardPurchasingModule_instantiateWindowsStore_m857429766_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		Il2CppObject * L_0 = Factory_Create_m2565271259(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Il2CppObject * L_2 = __this->get_m_Util_4();
		Il2CppObject * L_3 = __this->get_m_Logger_5();
		WinRTStore_t36043095 * L_4 = (WinRTStore_t36043095 *)il2cpp_codegen_object_new(WinRTStore_t36043095_il2cpp_TypeInfo_var);
		WinRTStore__ctor_m3142178002(L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_windowsStore_10(L_4);
		Il2CppObject * L_5 = __this->get_m_Util_4();
		WinRTStore_t36043095 * L_6 = __this->get_windowsStore_10();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)WinRTStore_restoreTransactions_m1421431749_MethodInfo_var);
		Action_1_t3627374100 * L_8 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_8, L_6, L_7, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		NullCheck(L_5);
		InterfaceActionInvoker1< Action_1_t3627374100 * >::Invoke(3 /* System.Void Uniject.IUtil::AddPauseListener(System.Action`1<System.Boolean>) */, IUtil_t2188430191_il2cpp_TypeInfo_var, L_5, L_8);
		WinRTStore_t36043095 * L_9 = __this->get_windowsStore_10();
		V_1 = L_9;
		goto IL_0048;
	}

IL_0048:
	{
		Il2CppObject * L_10 = V_1;
		return L_10;
	}
}
// UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateTizen()
extern Il2CppClass* TizenStoreImpl_t274247241_il2cpp_TypeInfo_var;
extern Il2CppClass* IRawStoreProvider_t2441088289_il2cpp_TypeInfo_var;
extern const MethodInfo* AbstractPurchasingModule_BindConfiguration_TisITizenStoreConfiguration_t2900348728_m3915163361_MethodInfo_var;
extern const uint32_t StandardPurchasingModule_InstantiateTizen_m2777272342_MetadataUsageId;
extern "C"  Il2CppObject * StandardPurchasingModule_InstantiateTizen_m2777272342 (StandardPurchasingModule_t4003664591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StandardPurchasingModule_InstantiateTizen_m2777272342_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TizenStoreImpl_t274247241 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		Il2CppObject * L_0 = __this->get_m_Util_4();
		TizenStoreImpl_t274247241 * L_1 = (TizenStoreImpl_t274247241 *)il2cpp_codegen_object_new(TizenStoreImpl_t274247241_il2cpp_TypeInfo_var);
		TizenStoreImpl__ctor_m1462561694(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		TizenStoreImpl_t274247241 * L_2 = V_0;
		Il2CppObject * L_3 = __this->get_m_PlatformProvider_2();
		TizenStoreImpl_t274247241 * L_4 = V_0;
		Il2CppObject * L_5 = ((AbstractPurchasingModule_t4102635892 *)__this)->get_m_Binder_0();
		NullCheck(L_3);
		Il2CppObject * L_6 = InterfaceFuncInvoker2< Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(2 /* UnityEngine.Purchasing.INativeTizenStore UnityEngine.Purchasing.IRawStoreProvider::GetTizenStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.Extension.IPurchasingBinder) */, IRawStoreProvider_t2441088289_il2cpp_TypeInfo_var, L_3, L_4, L_5);
		NullCheck(L_2);
		TizenStoreImpl_SetNativeStore_m2574918183(L_2, L_6, /*hidden argument*/NULL);
		TizenStoreImpl_t274247241 * L_7 = V_0;
		AbstractPurchasingModule_BindConfiguration_TisITizenStoreConfiguration_t2900348728_m3915163361(__this, L_7, /*hidden argument*/AbstractPurchasingModule_BindConfiguration_TisITizenStoreConfiguration_t2900348728_m3915163361_MethodInfo_var);
		TizenStoreImpl_t274247241 * L_8 = V_0;
		V_1 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		Il2CppObject * L_9 = V_1;
		return L_9;
	}
}
// UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateFakeStore()
extern Il2CppClass* UIFakeStore_t3684252124_il2cpp_TypeInfo_var;
extern Il2CppClass* FakeStore_t3882981564_il2cpp_TypeInfo_var;
extern const uint32_t StandardPurchasingModule_InstantiateFakeStore_m1788236408_MetadataUsageId;
extern "C"  Il2CppObject * StandardPurchasingModule_InstantiateFakeStore_m1788236408 (StandardPurchasingModule_t4003664591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StandardPurchasingModule_InstantiateFakeStore_m1788236408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FakeStore_t3882981564 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = (FakeStore_t3882981564 *)NULL;
		int32_t L_0 = StandardPurchasingModule_get_useFakeStoreUIMode_m1753364605(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		UIFakeStore_t3684252124 * L_1 = (UIFakeStore_t3684252124 *)il2cpp_codegen_object_new(UIFakeStore_t3684252124_il2cpp_TypeInfo_var);
		UIFakeStore__ctor_m4281629614(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		FakeStore_t3882981564 * L_2 = V_0;
		int32_t L_3 = StandardPurchasingModule_get_useFakeStoreUIMode_m1753364605(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_UIMode_4(L_3);
	}

IL_0022:
	{
		FakeStore_t3882981564 * L_4 = V_0;
		if (L_4)
		{
			goto IL_0030;
		}
	}
	{
		FakeStore_t3882981564 * L_5 = (FakeStore_t3882981564 *)il2cpp_codegen_object_new(FakeStore_t3882981564_il2cpp_TypeInfo_var);
		FakeStore__ctor_m3239256688(L_5, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_0030:
	{
		FakeStore_t3882981564 * L_6 = V_0;
		V_1 = L_6;
		goto IL_0037;
	}

IL_0037:
	{
		Il2CppObject * L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.Purchasing.StandardPurchasingModule::.cctor()
extern Il2CppClass* Dictionary_2_t2149139222_il2cpp_TypeInfo_var;
extern Il2CppClass* StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3235132305_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3250710889_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3837887126;
extern Il2CppCodeGenString* _stringLiteral1188405057;
extern Il2CppCodeGenString* _stringLiteral3343842482;
extern const uint32_t StandardPurchasingModule__cctor_m1254625542_MetadataUsageId;
extern "C"  void StandardPurchasingModule__cctor_m1254625542 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StandardPurchasingModule__cctor_m1254625542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t2149139222 * V_0 = NULL;
	{
		Dictionary_2_t2149139222 * L_0 = (Dictionary_2_t2149139222 *)il2cpp_codegen_object_new(Dictionary_2_t2149139222_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3235132305(L_0, /*hidden argument*/Dictionary_2__ctor_m3235132305_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t2149139222 * L_1 = V_0;
		NullCheck(L_1);
		Dictionary_2_Add_m3250710889(L_1, 1, _stringLiteral3837887126, /*hidden argument*/Dictionary_2_Add_m3250710889_MethodInfo_var);
		Dictionary_2_t2149139222 * L_2 = V_0;
		NullCheck(L_2);
		Dictionary_2_Add_m3250710889(L_2, 0, _stringLiteral1188405057, /*hidden argument*/Dictionary_2_Add_m3250710889_MethodInfo_var);
		Dictionary_2_t2149139222 * L_3 = V_0;
		NullCheck(L_3);
		Dictionary_2_Add_m3250710889(L_3, 2, _stringLiteral3343842482, /*hidden argument*/Dictionary_2_Add_m3250710889_MethodInfo_var);
		Dictionary_2_t2149139222 * L_4 = V_0;
		NullCheck(L_4);
		Dictionary_2_Add_m3250710889(L_4, 3, _stringLiteral1188405057, /*hidden argument*/Dictionary_2_Add_m3250710889_MethodInfo_var);
		Dictionary_2_t2149139222 * L_5 = V_0;
		((StandardPurchasingModule_t4003664591_StaticFields*)StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var->static_fields)->set_AndroidStoreNameMap_8(L_5);
		return;
	}
}
// System.Void UnityEngine.Purchasing.StandardPurchasingModule/MicrosoftConfiguration::.ctor(UnityEngine.Purchasing.StandardPurchasingModule)
extern "C"  void MicrosoftConfiguration__ctor_m1885361522 (MicrosoftConfiguration_t1310600573 * __this, StandardPurchasingModule_t4003664591 * ___module0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		StandardPurchasingModule_t4003664591 * L_0 = ___module0;
		__this->set_module_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.StandardPurchasingModule/MicrosoftConfiguration::set_useMockBillingSystem(System.Boolean)
extern "C"  void MicrosoftConfiguration_set_useMockBillingSystem_m3265911289 (MicrosoftConfiguration_t1310600573 * __this, bool ___value0, const MethodInfo* method)
{
	{
		StandardPurchasingModule_t4003664591 * L_0 = __this->get_module_1();
		bool L_1 = ___value0;
		NullCheck(L_0);
		StandardPurchasingModule_UseMockWindowsStore_m536092621(L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = ___value0;
		__this->set_useMock_0(L_2);
		return;
	}
}
// System.Void UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance::.ctor(System.String,UnityEngine.Purchasing.Extension.IStore)
extern "C"  void StoreInstance__ctor_m4140082908 (StoreInstance_t107230755 * __this, String_t* ___name0, Il2CppObject * ___instance1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_U3CstoreNameU3Ek__BackingField_0(L_0);
		Il2CppObject * L_1 = ___instance1;
		__this->set_U3CinstanceU3Ek__BackingField_1(L_1);
		return;
	}
}
// System.String UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance::get_storeName()
extern "C"  String_t* StoreInstance_get_storeName_m2726299052 (StoreInstance_t107230755 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CstoreNameU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance::get_instance()
extern "C"  Il2CppObject * StoreInstance_get_instance_m971207499 (StoreInstance_t107230755 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U3CinstanceU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.StoreConfiguration::.ctor(UnityEngine.Purchasing.AndroidStore)
extern "C"  void StoreConfiguration__ctor_m1497335086 (StoreConfiguration_t2466794143 * __this, int32_t ___store0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___store0;
		StoreConfiguration_set_androidStore_m2843019977(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.AndroidStore UnityEngine.Purchasing.StoreConfiguration::get_androidStore()
extern "C"  int32_t StoreConfiguration_get_androidStore_m4060547582 (StoreConfiguration_t2466794143 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CandroidStoreU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.StoreConfiguration::set_androidStore(UnityEngine.Purchasing.AndroidStore)
extern "C"  void StoreConfiguration_set_androidStore_m2843019977 (StoreConfiguration_t2466794143 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CandroidStoreU3Ek__BackingField_0(L_0);
		return;
	}
}
// UnityEngine.Purchasing.StoreConfiguration UnityEngine.Purchasing.StoreConfiguration::Deserialize(System.String)
extern const Il2CppType* AndroidStore_t3203055206_0_0_0_var;
extern Il2CppClass* MiniJson_t838727235_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidStore_t3203055206_il2cpp_TypeInfo_var;
extern Il2CppClass* StoreConfiguration_t2466794143_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m464793699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral735251194;
extern const uint32_t StoreConfiguration_Deserialize_m1161854657_MetadataUsageId;
extern "C"  StoreConfiguration_t2466794143 * StoreConfiguration_Deserialize_m1161854657 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StoreConfiguration_Deserialize_m1161854657_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	int32_t V_1 = 0;
	StoreConfiguration_t2466794143 * V_2 = NULL;
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJson_t838727235_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = MiniJson_JsonDecode_m1067224953(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Dictionary_2_t309261261 *)CastclassClass(L_1, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AndroidStore_t3203055206_0_0_0_var), /*hidden argument*/NULL);
		Dictionary_2_t309261261 * L_3 = V_0;
		NullCheck(L_3);
		Il2CppObject * L_4 = Dictionary_2_get_Item_m464793699(L_3, _stringLiteral735251194, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppObject * L_5 = Enum_Parse_m982704874(NULL /*static, unused*/, L_2, ((String_t*)CastclassSealed(L_4, String_t_il2cpp_TypeInfo_var)), (bool)1, /*hidden argument*/NULL);
		V_1 = ((*(int32_t*)((int32_t*)UnBox (L_5, AndroidStore_t3203055206_il2cpp_TypeInfo_var))));
		int32_t L_6 = V_1;
		StoreConfiguration_t2466794143 * L_7 = (StoreConfiguration_t2466794143 *)il2cpp_codegen_object_new(StoreConfiguration_t2466794143_il2cpp_TypeInfo_var);
		StoreConfiguration__ctor_m1497335086(L_7, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_003f;
	}

IL_003f:
	{
		StoreConfiguration_t2466794143 * L_8 = V_2;
		return L_8;
	}
}
// System.Void UnityEngine.Purchasing.TizenStoreImpl::.ctor(Uniject.IUtil)
extern Il2CppClass* TizenStoreImpl_t274247241_il2cpp_TypeInfo_var;
extern const uint32_t TizenStoreImpl__ctor_m1462561694_MetadataUsageId;
extern "C"  void TizenStoreImpl__ctor_m1462561694 (TizenStoreImpl_t274247241 * __this, Il2CppObject * ___util0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TizenStoreImpl__ctor_m1462561694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NativeJSONStore__ctor_m812188232(__this, /*hidden argument*/NULL);
		((TizenStoreImpl_t274247241_StaticFields*)TizenStoreImpl_t274247241_il2cpp_TypeInfo_var->static_fields)->set_instance_2(__this);
		return;
	}
}
// System.Void UnityEngine.Purchasing.TizenStoreImpl::SetNativeStore(UnityEngine.Purchasing.INativeTizenStore)
extern Il2CppClass* TizenStoreImpl_t274247241_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityNativePurchasingCallback_t3230812225_il2cpp_TypeInfo_var;
extern Il2CppClass* INativeTizenStore_t513596045_il2cpp_TypeInfo_var;
extern const MethodInfo* TizenStoreImpl_MessageCallback_m2338259177_MethodInfo_var;
extern const uint32_t TizenStoreImpl_SetNativeStore_m2574918183_MetadataUsageId;
extern "C"  void TizenStoreImpl_SetNativeStore_m2574918183 (TizenStoreImpl_t274247241 * __this, Il2CppObject * ___tizen0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TizenStoreImpl_SetNativeStore_m2574918183_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject * L_0 = ___tizen0;
		NativeJSONStore_SetNativeStore_m1863195906(__this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___tizen0;
		__this->set_m_Native_3(L_1);
		Il2CppObject * L_2 = __this->get_m_Native_3();
		UnityNativePurchasingCallback_t3230812225 * L_3 = ((TizenStoreImpl_t274247241_StaticFields*)TizenStoreImpl_t274247241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_4();
		G_B1_0 = L_2;
		if (L_3)
		{
			G_B2_0 = L_2;
			goto IL_002d;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TizenStoreImpl_MessageCallback_m2338259177_MethodInfo_var);
		UnityNativePurchasingCallback_t3230812225 * L_5 = (UnityNativePurchasingCallback_t3230812225 *)il2cpp_codegen_object_new(UnityNativePurchasingCallback_t3230812225_il2cpp_TypeInfo_var);
		UnityNativePurchasingCallback__ctor_m3078820921(L_5, NULL, L_4, /*hidden argument*/NULL);
		((TizenStoreImpl_t274247241_StaticFields*)TizenStoreImpl_t274247241_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_4(L_5);
		G_B2_0 = G_B1_0;
	}

IL_002d:
	{
		UnityNativePurchasingCallback_t3230812225 * L_6 = ((TizenStoreImpl_t274247241_StaticFields*)TizenStoreImpl_t274247241_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_4();
		NullCheck(G_B2_0);
		InterfaceActionInvoker1< UnityNativePurchasingCallback_t3230812225 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.INativeTizenStore::SetUnityPurchasingCallback(UnityEngine.Purchasing.UnityNativePurchasingCallback) */, INativeTizenStore_t513596045_il2cpp_TypeInfo_var, G_B2_0, L_6);
		return;
	}
}
// System.Void UnityEngine.Purchasing.TizenStoreImpl::SetGroupId(System.String)
extern Il2CppClass* INativeTizenStore_t513596045_il2cpp_TypeInfo_var;
extern const uint32_t TizenStoreImpl_SetGroupId_m2564280605_MetadataUsageId;
extern "C"  void TizenStoreImpl_SetGroupId_m2564280605 (TizenStoreImpl_t274247241 * __this, String_t* ___group0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TizenStoreImpl_SetGroupId_m2564280605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_m_Native_3();
		String_t* L_1 = ___group0;
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(1 /* System.Void UnityEngine.Purchasing.INativeTizenStore::SetGroupId(System.String) */, INativeTizenStore_t513596045_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void UnityEngine.Purchasing.TizenStoreImpl::MessageCallback(System.String,System.String,System.String,System.String)
extern Il2CppClass* TizenStoreImpl_t274247241_il2cpp_TypeInfo_var;
extern const uint32_t TizenStoreImpl_MessageCallback_m2338259177_MetadataUsageId;
extern "C"  void TizenStoreImpl_MessageCallback_m2338259177 (Il2CppObject * __this /* static, unused */, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TizenStoreImpl_MessageCallback_m2338259177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TizenStoreImpl_t274247241 * L_0 = ((TizenStoreImpl_t274247241_StaticFields*)TizenStoreImpl_t274247241_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		String_t* L_1 = ___subject0;
		String_t* L_2 = ___payload1;
		String_t* L_3 = ___receipt2;
		String_t* L_4 = ___transactionId3;
		NullCheck(L_0);
		TizenStoreImpl_ProcessMessage_m3321258175(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_TizenStoreImpl_MessageCallback_m2338259177(char* ___subject0, char* ___payload1, char* ___receipt2, char* ___transactionId3)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___subject0' to managed representation
	String_t* ____subject0_unmarshaled = NULL;
	____subject0_unmarshaled = il2cpp_codegen_marshal_string_result(___subject0);

	// Marshaling of parameter '___payload1' to managed representation
	String_t* ____payload1_unmarshaled = NULL;
	____payload1_unmarshaled = il2cpp_codegen_marshal_string_result(___payload1);

	// Marshaling of parameter '___receipt2' to managed representation
	String_t* ____receipt2_unmarshaled = NULL;
	____receipt2_unmarshaled = il2cpp_codegen_marshal_string_result(___receipt2);

	// Marshaling of parameter '___transactionId3' to managed representation
	String_t* ____transactionId3_unmarshaled = NULL;
	____transactionId3_unmarshaled = il2cpp_codegen_marshal_string_result(___transactionId3);

	// Managed method invocation
	TizenStoreImpl_MessageCallback_m2338259177(NULL, ____subject0_unmarshaled, ____payload1_unmarshaled, ____receipt2_unmarshaled, ____transactionId3_unmarshaled, NULL);

}
// System.Void UnityEngine.Purchasing.TizenStoreImpl::ProcessMessage(System.String,System.String,System.String,System.String)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral995068015;
extern Il2CppCodeGenString* _stringLiteral3319732278;
extern Il2CppCodeGenString* _stringLiteral429730652;
extern Il2CppCodeGenString* _stringLiteral423012031;
extern Il2CppCodeGenString* _stringLiteral843374921;
extern Il2CppCodeGenString* _stringLiteral585622411;
extern Il2CppCodeGenString* _stringLiteral558302001;
extern Il2CppCodeGenString* _stringLiteral3311547177;
extern const uint32_t TizenStoreImpl_ProcessMessage_m3321258175_MetadataUsageId;
extern "C"  void TizenStoreImpl_ProcessMessage_m3321258175 (TizenStoreImpl_t274247241 * __this, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TizenStoreImpl_ProcessMessage_m3321258175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral995068015);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral995068015);
		StringU5BU5D_t1642385972* L_1 = L_0;
		String_t* L_2 = ___subject0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_2);
		StringU5BU5D_t1642385972* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral3319732278);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3319732278);
		StringU5BU5D_t1642385972* L_4 = L_3;
		String_t* L_5 = ___payload1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_5);
		StringU5BU5D_t1642385972* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral429730652);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral429730652);
		StringU5BU5D_t1642385972* L_7 = L_6;
		String_t* L_8 = ___receipt2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_8);
		StringU5BU5D_t1642385972* L_9 = L_7;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 6);
		ArrayElementTypeCheck (L_9, _stringLiteral423012031);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral423012031);
		StringU5BU5D_t1642385972* L_10 = L_9;
		String_t* L_11 = ___transactionId3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 7);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m626692867(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		String_t* L_13 = ___subject0;
		if (!L_13)
		{
			goto IL_00c0;
		}
	}
	{
		String_t* L_14 = ___subject0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_14, _stringLiteral843374921, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_16 = ___subject0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_16, _stringLiteral585622411, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0099;
		}
	}
	{
		String_t* L_18 = ___subject0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_18, _stringLiteral558302001, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00a5;
		}
	}
	{
		String_t* L_20 = ___subject0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_21 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_20, _stringLiteral3311547177, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00b4;
		}
	}
	{
		goto IL_00c0;
	}

IL_008d:
	{
		String_t* L_22 = ___payload1;
		NativeJSONStore_OnSetupFailed_m2728050561(__this, L_22, /*hidden argument*/NULL);
		goto IL_00c0;
	}

IL_0099:
	{
		String_t* L_23 = ___payload1;
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void UnityEngine.Purchasing.NativeJSONStore::OnProductsRetrieved(System.String) */, __this, L_23);
		goto IL_00c0;
	}

IL_00a5:
	{
		String_t* L_24 = ___payload1;
		String_t* L_25 = ___receipt2;
		String_t* L_26 = ___transactionId3;
		NativeJSONStore_OnPurchaseSucceeded_m363409449(__this, L_24, L_25, L_26, /*hidden argument*/NULL);
		goto IL_00c0;
	}

IL_00b4:
	{
		String_t* L_27 = ___payload1;
		NativeJSONStore_OnPurchaseFailed_m4091409929(__this, L_27, /*hidden argument*/NULL);
		goto IL_00c0;
	}

IL_00c0:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::.ctor()
extern "C"  void UIFakeStore__ctor_m4281629614 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	{
		FakeStore__ctor_m3239256688(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Purchasing.UIFakeStore::StartUI(System.String,System.String,System.String,System.Collections.Generic.List`1<System.String>,System.Action`2<System.Boolean,System.Int32>)
extern Il2CppClass* DialogRequest_t2092195449_il2cpp_TypeInfo_var;
extern const uint32_t UIFakeStore_StartUI_m2569939907_MetadataUsageId;
extern "C"  bool UIFakeStore_StartUI_m2569939907 (UIFakeStore_t3684252124 * __this, String_t* ___queryText0, String_t* ___okayButtonText1, String_t* ___cancelButtonText2, List_1_t1398341365 * ___options3, Action_2_t1907880187 * ___callback4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_StartUI_m2569939907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	DialogRequest_t2092195449 * V_1 = NULL;
	{
		bool L_0 = UIFakeStore_IsShowingDialog_m1285492143(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0053;
	}

IL_0014:
	{
		DialogRequest_t2092195449 * L_1 = (DialogRequest_t2092195449 *)il2cpp_codegen_object_new(DialogRequest_t2092195449_il2cpp_TypeInfo_var);
		DialogRequest__ctor_m3883507536(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		DialogRequest_t2092195449 * L_2 = V_1;
		String_t* L_3 = ___queryText0;
		NullCheck(L_2);
		L_2->set_QueryText_0(L_3);
		DialogRequest_t2092195449 * L_4 = V_1;
		String_t* L_5 = ___okayButtonText1;
		NullCheck(L_4);
		L_4->set_OkayButtonText_1(L_5);
		DialogRequest_t2092195449 * L_6 = V_1;
		String_t* L_7 = ___cancelButtonText2;
		NullCheck(L_6);
		L_6->set_CancelButtonText_2(L_7);
		DialogRequest_t2092195449 * L_8 = V_1;
		List_1_t1398341365 * L_9 = ___options3;
		NullCheck(L_8);
		L_8->set_Options_3(L_9);
		DialogRequest_t2092195449 * L_10 = V_1;
		Action_2_t1907880187 * L_11 = ___callback4;
		NullCheck(L_10);
		L_10->set_Callback_4(L_11);
		DialogRequest_t2092195449 * L_12 = V_1;
		__this->set_m_CurrentDialog_5(L_12);
		UIFakeStore_InstantiateDialog_m26685090(__this, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_0053;
	}

IL_0053:
	{
		bool L_13 = V_0;
		return L_13;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::InstantiateDialog()
extern const Il2CppType* EventSystem_t3466835263_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* OptionData_t2420267500_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3438463199_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCanvas_t209405766_m195193039_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisCanvas_t209405766_m805779209_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisLifecycleNotifier_t1057582876_m1564113664_MethodInfo_var;
extern const MethodInfo* UIFakeStore_U3CInstantiateDialogU3Em__0_m1757237127_MethodInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisEventSystem_t3466835263_m929139623_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisStandaloneInputModule_t70867863_m4180824674_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m1204779269_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1412753878_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2837468952_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3486116920_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1105633690_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2205157096_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m780127360_MethodInfo_var;
extern const MethodInfo* UIFakeStore_U3CInstantiateDialogU3Em__1_m3303118092_MethodInfo_var;
extern const MethodInfo* UIFakeStore_U3CInstantiateDialogU3Em__2_m2039562129_MethodInfo_var;
extern const MethodInfo* UIFakeStore_U3CInstantiateDialogU3Em__3_m322601745_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m4197675336_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m404084168_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4204482283;
extern Il2CppCodeGenString* _stringLiteral1832009962;
extern Il2CppCodeGenString* _stringLiteral3655311540;
extern Il2CppCodeGenString* _stringLiteral3628703177;
extern Il2CppCodeGenString* _stringLiteral4267440200;
extern const uint32_t UIFakeStore_InstantiateDialog_m26685090_MetadataUsageId;
extern "C"  void UIFakeStore_InstantiateDialog_m26685090 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_InstantiateDialog_m26685090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Canvas_t209405766 * V_0 = NULL;
	LifecycleNotifier_t1057582876 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	Text_t356221433 * V_3 = NULL;
	Text_t356221433 * V_4 = NULL;
	Text_t356221433 * V_5 = NULL;
	String_t* V_6 = NULL;
	Enumerator_t933071039  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		DialogRequest_t2092195449 * L_0 = __this->get_m_CurrentDialog_5();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m56707527(NULL /*static, unused*/, __this, _stringLiteral4204482283, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_029c;
	}

IL_0022:
	{
		GameObject_t1756533147 * L_2 = __this->get_UIFakeStoreCanvasPrefab_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004a;
		}
	}
	{
		Object_t1021602117 * L_4 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral1832009962, /*hidden argument*/NULL);
		__this->set_UIFakeStoreCanvasPrefab_7(((GameObject_t1756533147 *)IsInstSealed(L_4, GameObject_t1756533147_il2cpp_TypeInfo_var)));
	}

IL_004a:
	{
		GameObject_t1756533147 * L_5 = __this->get_UIFakeStoreCanvasPrefab_7();
		NullCheck(L_5);
		Canvas_t209405766 * L_6 = GameObject_GetComponent_TisCanvas_t209405766_m195193039(L_5, /*hidden argument*/GameObject_GetComponent_TisCanvas_t209405766_m195193039_MethodInfo_var);
		V_0 = L_6;
		Canvas_t209405766 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Canvas_t209405766 * L_8 = Object_Instantiate_TisCanvas_t209405766_m805779209(NULL /*static, unused*/, L_7, /*hidden argument*/Object_Instantiate_TisCanvas_t209405766_m805779209_MethodInfo_var);
		__this->set_m_Canvas_8(L_8);
		Canvas_t209405766 * L_9 = __this->get_m_Canvas_8();
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		LifecycleNotifier_t1057582876 * L_11 = GameObject_AddComponent_TisLifecycleNotifier_t1057582876_m1564113664(L_10, /*hidden argument*/GameObject_AddComponent_TisLifecycleNotifier_t1057582876_m1564113664_MethodInfo_var);
		V_1 = L_11;
		LifecycleNotifier_t1057582876 * L_12 = V_1;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)UIFakeStore_U3CInstantiateDialogU3Em__0_m1757237127_MethodInfo_var);
		Action_t3226471752 * L_14 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_14, __this, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		L_12->set_OnDestroyCallback_2(L_14);
		Canvas_t209405766 * L_15 = __this->get_m_Canvas_8();
		NullCheck(L_15);
		String_t* L_16 = Object_get_name_m2079638459(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m2596409543(NULL /*static, unused*/, L_16, _stringLiteral3655311540, /*hidden argument*/NULL);
		__this->set_m_ParentGameObjectPath_10(L_17);
		EventSystem_t3466835263 * L_18 = Object_FindObjectOfType_TisEventSystem_t3466835263_m929139623(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisEventSystem_t3466835263_m929139623_MethodInfo_var);
		bool L_19 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00fc;
		}
	}
	{
		TypeU5BU5D_t1664964607* L_20 = ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(EventSystem_t3466835263_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_21);
		GameObject_t1756533147 * L_22 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m1633632305(L_22, _stringLiteral3628703177, L_20, /*hidden argument*/NULL);
		__this->set_m_EventSystem_9(L_22);
		GameObject_t1756533147 * L_23 = __this->get_m_EventSystem_9();
		NullCheck(L_23);
		GameObject_AddComponent_TisStandaloneInputModule_t70867863_m4180824674(L_23, /*hidden argument*/GameObject_AddComponent_TisStandaloneInputModule_t70867863_m4180824674_MethodInfo_var);
		GameObject_t1756533147 * L_24 = __this->get_m_EventSystem_9();
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = GameObject_get_transform_m909382139(L_24, /*hidden argument*/NULL);
		Canvas_t209405766 * L_26 = __this->get_m_Canvas_8();
		NullCheck(L_26);
		Transform_t3275118058 * L_27 = Component_get_transform_m2697483695(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_parent_m3281327839(L_25, L_27, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		String_t* L_28 = __this->get_m_ParentGameObjectPath_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m2596409543(NULL /*static, unused*/, L_28, _stringLiteral4267440200, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_30 = GameObject_Find_m836511350(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		V_2 = L_30;
		GameObject_t1756533147 * L_31 = V_2;
		NullCheck(L_31);
		Text_t356221433 * L_32 = GameObject_GetComponent_TisText_t356221433_m4280536079(L_31, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var);
		V_3 = L_32;
		Text_t356221433 * L_33 = V_3;
		DialogRequest_t2092195449 * L_34 = __this->get_m_CurrentDialog_5();
		NullCheck(L_34);
		String_t* L_35 = L_34->get_QueryText_0();
		NullCheck(L_33);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_33, L_35);
		Text_t356221433 * L_36 = UIFakeStore_GetOkayButtonText_m6562956(__this, /*hidden argument*/NULL);
		V_4 = L_36;
		Text_t356221433 * L_37 = V_4;
		DialogRequest_t2092195449 * L_38 = __this->get_m_CurrentDialog_5();
		NullCheck(L_38);
		String_t* L_39 = L_38->get_OkayButtonText_1();
		NullCheck(L_37);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_37, L_39);
		Text_t356221433 * L_40 = UIFakeStore_GetCancelButtonText_m1587731078(__this, /*hidden argument*/NULL);
		V_5 = L_40;
		Text_t356221433 * L_41 = V_5;
		DialogRequest_t2092195449 * L_42 = __this->get_m_CurrentDialog_5();
		NullCheck(L_42);
		String_t* L_43 = L_42->get_CancelButtonText_2();
		NullCheck(L_41);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_41, L_43);
		Dropdown_t1985816271 * L_44 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		List_1_t1789388632 * L_45 = Dropdown_get_options_m2669836220(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		List_1_Clear_m1204779269(L_45, /*hidden argument*/List_1_Clear_m1204779269_MethodInfo_var);
		DialogRequest_t2092195449 * L_46 = __this->get_m_CurrentDialog_5();
		NullCheck(L_46);
		List_1_t1398341365 * L_47 = L_46->get_Options_3();
		NullCheck(L_47);
		Enumerator_t933071039  L_48 = List_1_GetEnumerator_m1412753878(L_47, /*hidden argument*/List_1_GetEnumerator_m1412753878_MethodInfo_var);
		V_7 = L_48;
	}

IL_0181:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01a8;
		}

IL_0186:
		{
			String_t* L_49 = Enumerator_get_Current_m2837468952((&V_7), /*hidden argument*/Enumerator_get_Current_m2837468952_MethodInfo_var);
			V_6 = L_49;
			Dropdown_t1985816271 * L_50 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
			NullCheck(L_50);
			List_1_t1789388632 * L_51 = Dropdown_get_options_m2669836220(L_50, /*hidden argument*/NULL);
			String_t* L_52 = V_6;
			OptionData_t2420267500 * L_53 = (OptionData_t2420267500 *)il2cpp_codegen_object_new(OptionData_t2420267500_il2cpp_TypeInfo_var);
			OptionData__ctor_m743450704(L_53, L_52, /*hidden argument*/NULL);
			NullCheck(L_51);
			List_1_Add_m3486116920(L_51, L_53, /*hidden argument*/List_1_Add_m3486116920_MethodInfo_var);
		}

IL_01a8:
		{
			bool L_54 = Enumerator_MoveNext_m1105633690((&V_7), /*hidden argument*/Enumerator_MoveNext_m1105633690_MethodInfo_var);
			if (L_54)
			{
				goto IL_0186;
			}
		}

IL_01b4:
		{
			IL2CPP_LEAVE(0x1C7, FINALLY_01b9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01b9;
	}

FINALLY_01b9:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2205157096((&V_7), /*hidden argument*/Enumerator_Dispose_m2205157096_MethodInfo_var);
		IL2CPP_END_FINALLY(441)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(441)
	{
		IL2CPP_JUMP_TBL(0x1C7, IL_01c7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01c7:
	{
		DialogRequest_t2092195449 * L_55 = __this->get_m_CurrentDialog_5();
		NullCheck(L_55);
		List_1_t1398341365 * L_56 = L_55->get_Options_3();
		NullCheck(L_56);
		int32_t L_57 = List_1_get_Count_m780127360(L_56, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		if ((((int32_t)L_57) <= ((int32_t)0)))
		{
			goto IL_01e6;
		}
	}
	{
		__this->set_m_LastSelectedDropdownIndex_6(0);
	}

IL_01e6:
	{
		Dropdown_t1985816271 * L_58 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
		NullCheck(L_58);
		Dropdown_RefreshShownValue_m3113581237(L_58, /*hidden argument*/NULL);
		Button_t2872111280 * L_59 = UIFakeStore_GetOkayButton_m2121832312(__this, /*hidden argument*/NULL);
		NullCheck(L_59);
		ButtonClickedEvent_t2455055323 * L_60 = Button_get_onClick_m1595880935(L_59, /*hidden argument*/NULL);
		IntPtr_t L_61;
		L_61.set_m_value_0((void*)(void*)UIFakeStore_U3CInstantiateDialogU3Em__1_m3303118092_MethodInfo_var);
		UnityAction_t4025899511 * L_62 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_62, __this, L_61, /*hidden argument*/NULL);
		NullCheck(L_60);
		UnityEvent_AddListener_m1596810379(L_60, L_62, /*hidden argument*/NULL);
		Button_t2872111280 * L_63 = UIFakeStore_GetCancelButton_m1851079754(__this, /*hidden argument*/NULL);
		NullCheck(L_63);
		ButtonClickedEvent_t2455055323 * L_64 = Button_get_onClick_m1595880935(L_63, /*hidden argument*/NULL);
		IntPtr_t L_65;
		L_65.set_m_value_0((void*)(void*)UIFakeStore_U3CInstantiateDialogU3Em__2_m2039562129_MethodInfo_var);
		UnityAction_t4025899511 * L_66 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_66, __this, L_65, /*hidden argument*/NULL);
		NullCheck(L_64);
		UnityEvent_AddListener_m1596810379(L_64, L_66, /*hidden argument*/NULL);
		Dropdown_t1985816271 * L_67 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
		NullCheck(L_67);
		DropdownEvent_t2203087800 * L_68 = Dropdown_get_onValueChanged_m3334401942(L_67, /*hidden argument*/NULL);
		IntPtr_t L_69;
		L_69.set_m_value_0((void*)(void*)UIFakeStore_U3CInstantiateDialogU3Em__3_m322601745_MethodInfo_var);
		UnityAction_1_t3438463199 * L_70 = (UnityAction_1_t3438463199 *)il2cpp_codegen_object_new(UnityAction_1_t3438463199_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4197675336(L_70, __this, L_69, /*hidden argument*/UnityAction_1__ctor_m4197675336_MethodInfo_var);
		NullCheck(L_68);
		UnityEvent_1_AddListener_m404084168(L_68, L_70, /*hidden argument*/UnityEvent_1_AddListener_m404084168_MethodInfo_var);
		int32_t L_71 = ((FakeStore_t3882981564 *)__this)->get_UIMode_4();
		if ((!(((uint32_t)L_71) == ((uint32_t)1))))
		{
			goto IL_0273;
		}
	}
	{
		Dropdown_t1985816271 * L_72 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
		NullCheck(L_72);
		DropdownEvent_t2203087800 * L_73 = Dropdown_get_onValueChanged_m3334401942(L_72, /*hidden argument*/NULL);
		NullCheck(L_73);
		UnityEventBase_RemoveAllListeners_m230188638(L_73, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_74 = UIFakeStore_GetDropdownContainerGameObject_m2190404260(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_74, /*hidden argument*/NULL);
		goto IL_029c;
	}

IL_0273:
	{
		int32_t L_75 = ((FakeStore_t3882981564 *)__this)->get_UIMode_4();
		if ((!(((uint32_t)L_75) == ((uint32_t)2))))
		{
			goto IL_029c;
		}
	}
	{
		Button_t2872111280 * L_76 = UIFakeStore_GetCancelButton_m1851079754(__this, /*hidden argument*/NULL);
		NullCheck(L_76);
		ButtonClickedEvent_t2455055323 * L_77 = Button_get_onClick_m1595880935(L_76, /*hidden argument*/NULL);
		NullCheck(L_77);
		UnityEventBase_RemoveAllListeners_m230188638(L_77, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_78 = UIFakeStore_GetCancelButtonGameObject_m4002152618(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
	}

IL_029c:
	{
		return;
	}
}
// System.String UnityEngine.Purchasing.UIFakeStore::CreatePurchaseQuestion(UnityEngine.Purchasing.ProductDefinition)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3368079914;
extern Il2CppCodeGenString* _stringLiteral3820628614;
extern const uint32_t UIFakeStore_CreatePurchaseQuestion_m2287137335_MetadataUsageId;
extern "C"  String_t* UIFakeStore_CreatePurchaseQuestion_m2287137335 (UIFakeStore_t3684252124 * __this, ProductDefinition_t1942475268 * ___definition0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_CreatePurchaseQuestion_m2287137335_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		ProductDefinition_t1942475268 * L_0 = ___definition0;
		NullCheck(L_0);
		String_t* L_1 = ProductDefinition_get_id_m264072292(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3368079914, L_1, _stringLiteral3820628614, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001c;
	}

IL_001c:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.String UnityEngine.Purchasing.UIFakeStore::CreateRetrieveProductsQuestion(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern Il2CppClass* UIFakeStore_t3684252124_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2958801606_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Take_TisProductDefinition_t1942475268_m2540339846_MethodInfo_var;
extern const MethodInfo* UIFakeStore_U3CCreateRetrieveProductsQuestionU3Em__4_m1781217237_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m585608501_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisProductDefinition_t1942475268_TisString_t_m2207542709_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m1953054010_MethodInfo_var;
extern const MethodInfo* ReadOnlyCollection_1_get_Count_m2134061681_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1923700425;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral3422253728;
extern Il2CppCodeGenString* _stringLiteral3741546081;
extern const uint32_t UIFakeStore_CreateRetrieveProductsQuestion_m4106290161_MetadataUsageId;
extern "C"  String_t* UIFakeStore_CreateRetrieveProductsQuestion_m4106290161 (UIFakeStore_t3684252124 * __this, ReadOnlyCollection_1_t2128260960 * ___definitions0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_CreateRetrieveProductsQuestion_m4106290161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	Il2CppObject* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	String_t* G_B2_2 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	String_t* G_B1_2 = NULL;
	{
		V_0 = _stringLiteral1923700425;
		String_t* L_0 = V_0;
		ReadOnlyCollection_1_t2128260960 * L_1 = ___definitions0;
		Il2CppObject* L_2 = Enumerable_Take_TisProductDefinition_t1942475268_m2540339846(NULL /*static, unused*/, L_1, 2, /*hidden argument*/Enumerable_Take_TisProductDefinition_t1942475268_m2540339846_MethodInfo_var);
		Func_2_t2958801606 * L_3 = ((UIFakeStore_t3684252124_StaticFields*)UIFakeStore_t3684252124_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_11();
		G_B1_0 = L_2;
		G_B1_1 = _stringLiteral811305474;
		G_B1_2 = L_0;
		if (L_3)
		{
			G_B2_0 = L_2;
			G_B2_1 = _stringLiteral811305474;
			G_B2_2 = L_0;
			goto IL_002c;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)UIFakeStore_U3CCreateRetrieveProductsQuestionU3Em__4_m1781217237_MethodInfo_var);
		Func_2_t2958801606 * L_5 = (Func_2_t2958801606 *)il2cpp_codegen_object_new(Func_2_t2958801606_il2cpp_TypeInfo_var);
		Func_2__ctor_m585608501(L_5, NULL, L_4, /*hidden argument*/Func_2__ctor_m585608501_MethodInfo_var);
		((UIFakeStore_t3684252124_StaticFields*)UIFakeStore_t3684252124_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_11(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_002c:
	{
		Func_2_t2958801606 * L_6 = ((UIFakeStore_t3684252124_StaticFields*)UIFakeStore_t3684252124_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_11();
		Il2CppObject* L_7 = Enumerable_Select_TisProductDefinition_t1942475268_TisString_t_m2207542709(NULL /*static, unused*/, G_B2_0, L_6, /*hidden argument*/Enumerable_Select_TisProductDefinition_t1942475268_TisString_t_m2207542709_MethodInfo_var);
		StringU5BU5D_t1642385972* L_8 = Enumerable_ToArray_TisString_t_m1953054010(NULL /*static, unused*/, L_7, /*hidden argument*/Enumerable_ToArray_TisString_t_m1953054010_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Join_m1966872927(NULL /*static, unused*/, G_B2_1, L_8, /*hidden argument*/NULL);
		String_t* L_10 = String_Concat_m2596409543(NULL /*static, unused*/, G_B2_2, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		ReadOnlyCollection_1_t2128260960 * L_11 = ___definitions0;
		NullCheck(L_11);
		int32_t L_12 = ReadOnlyCollection_1_get_Count_m2134061681(L_11, /*hidden argument*/ReadOnlyCollection_1_get_Count_m2134061681_MethodInfo_var);
		if ((((int32_t)L_12) <= ((int32_t)2)))
		{
			goto IL_0060;
		}
	}
	{
		String_t* L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m2596409543(NULL /*static, unused*/, L_13, _stringLiteral3422253728, /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_0060:
	{
		String_t* L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m2596409543(NULL /*static, unused*/, L_15, _stringLiteral3741546081, /*hidden argument*/NULL);
		V_0 = L_16;
		String_t* L_17 = V_0;
		V_1 = L_17;
		goto IL_0073;
	}

IL_0073:
	{
		String_t* L_18 = V_1;
		return L_18;
	}
}
// UnityEngine.UI.Button UnityEngine.Purchasing.UIFakeStore::GetOkayButton()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3889149951;
extern const uint32_t UIFakeStore_GetOkayButton_m2121832312_MetadataUsageId;
extern "C"  Button_t2872111280 * UIFakeStore_GetOkayButton_m2121832312 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetOkayButton_m2121832312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Button_t2872111280 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral3889149951, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Button_t2872111280 * L_3 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_2, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		V_0 = L_3;
		goto IL_0021;
	}

IL_0021:
	{
		Button_t2872111280 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.UI.Button UnityEngine.Purchasing.UIFakeStore::GetCancelButton()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3889149952;
extern const uint32_t UIFakeStore_GetCancelButton_m1851079754_MetadataUsageId;
extern "C"  Button_t2872111280 * UIFakeStore_GetCancelButton_m1851079754 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetCancelButton_m1851079754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Button_t2872111280 * V_1 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral3889149952, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		GameObject_t1756533147 * L_5 = V_0;
		NullCheck(L_5);
		Button_t2872111280 * L_6 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_5, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		V_1 = L_6;
		goto IL_0038;
	}

IL_0030:
	{
		V_1 = (Button_t2872111280 *)NULL;
		goto IL_0038;
	}

IL_0038:
	{
		Button_t2872111280 * L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::GetCancelButtonGameObject()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3889149952;
extern const uint32_t UIFakeStore_GetCancelButtonGameObject_m4002152618_MetadataUsageId;
extern "C"  GameObject_t1756533147 * UIFakeStore_GetCancelButtonGameObject_m4002152618 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetCancelButtonGameObject_m4002152618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral3889149952, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001c;
	}

IL_001c:
	{
		GameObject_t1756533147 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.UI.Text UnityEngine.Purchasing.UIFakeStore::GetOkayButtonText()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1636160581;
extern const uint32_t UIFakeStore_GetOkayButtonText_m6562956_MetadataUsageId;
extern "C"  Text_t356221433 * UIFakeStore_GetOkayButtonText_m6562956 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetOkayButtonText_m6562956_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Text_t356221433 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral1636160581, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Text_t356221433 * L_3 = GameObject_GetComponent_TisText_t356221433_m4280536079(L_2, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var);
		V_0 = L_3;
		goto IL_0021;
	}

IL_0021:
	{
		Text_t356221433 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.UI.Text UnityEngine.Purchasing.UIFakeStore::GetCancelButtonText()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1636166022;
extern const uint32_t UIFakeStore_GetCancelButtonText_m1587731078_MetadataUsageId;
extern "C"  Text_t356221433 * UIFakeStore_GetCancelButtonText_m1587731078 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetCancelButtonText_m1587731078_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Text_t356221433 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral1636166022, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Text_t356221433 * L_3 = GameObject_GetComponent_TisText_t356221433_m4280536079(L_2, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var);
		V_0 = L_3;
		goto IL_0021;
	}

IL_0021:
	{
		Text_t356221433 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.UI.Dropdown UnityEngine.Purchasing.UIFakeStore::GetDropdown()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisDropdown_t1985816271_m1750975685_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4076336408;
extern const uint32_t UIFakeStore_GetDropdown_m651796348_MetadataUsageId;
extern "C"  Dropdown_t1985816271 * UIFakeStore_GetDropdown_m651796348 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetDropdown_m651796348_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Dropdown_t1985816271 * V_1 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral4076336408, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		GameObject_t1756533147 * L_5 = V_0;
		NullCheck(L_5);
		Dropdown_t1985816271 * L_6 = GameObject_GetComponent_TisDropdown_t1985816271_m1750975685(L_5, /*hidden argument*/GameObject_GetComponent_TisDropdown_t1985816271_m1750975685_MethodInfo_var);
		V_1 = L_6;
		goto IL_0038;
	}

IL_0030:
	{
		V_1 = (Dropdown_t1985816271 *)NULL;
		goto IL_0038;
	}

IL_0038:
	{
		Dropdown_t1985816271 * L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::GetDropdownContainerGameObject()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral516648086;
extern const uint32_t UIFakeStore_GetDropdownContainerGameObject_m2190404260_MetadataUsageId;
extern "C"  GameObject_t1756533147 * UIFakeStore_GetDropdownContainerGameObject_m2190404260 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetDropdownContainerGameObject_m2190404260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral516648086, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001c;
	}

IL_001c:
	{
		GameObject_t1756533147 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::OkayButtonClicked()
extern const MethodInfo* Action_2_Invoke_m2365036873_MethodInfo_var;
extern const uint32_t UIFakeStore_OkayButtonClicked_m1579250743_MetadataUsageId;
extern "C"  void UIFakeStore_OkayButtonClicked_m1579250743 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_OkayButtonClicked_m1579250743_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		V_0 = (bool)0;
		int32_t L_0 = __this->get_m_LastSelectedDropdownIndex_6();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = ((FakeStore_t3882981564 *)__this)->get_UIMode_4();
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_001e;
		}
	}

IL_001a:
	{
		V_0 = (bool)1;
	}

IL_001e:
	{
		int32_t L_2 = __this->get_m_LastSelectedDropdownIndex_6();
		int32_t L_3 = Math_Max_m2671311541(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_3;
		DialogRequest_t2092195449 * L_4 = __this->get_m_CurrentDialog_5();
		NullCheck(L_4);
		Action_2_t1907880187 * L_5 = L_4->get_Callback_4();
		bool L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_5);
		Action_2_Invoke_m2365036873(L_5, L_6, L_7, /*hidden argument*/Action_2_Invoke_m2365036873_MethodInfo_var);
		UIFakeStore_CloseDialog_m4132523490(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::CancelButtonClicked()
extern const MethodInfo* Action_2_Invoke_m2365036873_MethodInfo_var;
extern const uint32_t UIFakeStore_CancelButtonClicked_m1822980185_MetadataUsageId;
extern "C"  void UIFakeStore_CancelButtonClicked_m1822980185 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_CancelButtonClicked_m1822980185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_LastSelectedDropdownIndex_6();
		int32_t L_1 = Math_Max_m2671311541(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_0-(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_1;
		DialogRequest_t2092195449 * L_2 = __this->get_m_CurrentDialog_5();
		NullCheck(L_2);
		Action_2_t1907880187 * L_3 = L_2->get_Callback_4();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Action_2_Invoke_m2365036873(L_3, (bool)0, L_4, /*hidden argument*/Action_2_Invoke_m2365036873_MethodInfo_var);
		UIFakeStore_CloseDialog_m4132523490(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::DropdownValueChanged(System.Int32)
extern "C"  void UIFakeStore_DropdownValueChanged_m3219846161 (UIFakeStore_t3684252124 * __this, int32_t ___selectedItem0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___selectedItem0;
		__this->set_m_LastSelectedDropdownIndex_6(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::CloseDialog()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UIFakeStore_CloseDialog_m4132523490_MetadataUsageId;
extern "C"  void UIFakeStore_CloseDialog_m4132523490 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_CloseDialog_m4132523490_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_CurrentDialog_5((DialogRequest_t2092195449 *)NULL);
		Button_t2872111280 * L_0 = UIFakeStore_GetOkayButton_m2121832312(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ButtonClickedEvent_t2455055323 * L_1 = Button_get_onClick_m1595880935(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		UnityEventBase_RemoveAllListeners_m230188638(L_1, /*hidden argument*/NULL);
		Button_t2872111280 * L_2 = UIFakeStore_GetCancelButton_m1851079754(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		Button_t2872111280 * L_4 = UIFakeStore_GetCancelButton_m1851079754(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		ButtonClickedEvent_t2455055323 * L_5 = Button_get_onClick_m1595880935(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		UnityEventBase_RemoveAllListeners_m230188638(L_5, /*hidden argument*/NULL);
	}

IL_003a:
	{
		Dropdown_t1985816271 * L_6 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005d;
		}
	}
	{
		Dropdown_t1985816271 * L_8 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		DropdownEvent_t2203087800 * L_9 = Dropdown_get_onValueChanged_m3334401942(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		UnityEventBase_RemoveAllListeners_m230188638(L_9, /*hidden argument*/NULL);
	}

IL_005d:
	{
		Canvas_t209405766 * L_10 = __this->get_m_Canvas_8();
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Purchasing.UIFakeStore::IsShowingDialog()
extern "C"  bool UIFakeStore_IsShowingDialog_m1285492143 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		DialogRequest_t2092195449 * L_0 = __this->get_m_CurrentDialog_5();
		V_0 = (bool)((((int32_t)((((Il2CppObject*)(DialogRequest_t2092195449 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0013;
	}

IL_0013:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>m__0()
extern "C"  void UIFakeStore_U3CInstantiateDialogU3Em__0_m1757237127 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	{
		__this->set_m_CurrentDialog_5((DialogRequest_t2092195449 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>m__1()
extern "C"  void UIFakeStore_U3CInstantiateDialogU3Em__1_m3303118092 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	{
		UIFakeStore_OkayButtonClicked_m1579250743(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>m__2()
extern "C"  void UIFakeStore_U3CInstantiateDialogU3Em__2_m2039562129 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	{
		UIFakeStore_CancelButtonClicked_m1822980185(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>m__3(System.Int32)
extern "C"  void UIFakeStore_U3CInstantiateDialogU3Em__3_m322601745 (UIFakeStore_t3684252124 * __this, int32_t ___selectedItem0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___selectedItem0;
		UIFakeStore_DropdownValueChanged_m3219846161(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Purchasing.UIFakeStore::<CreateRetrieveProductsQuestion>m__4(UnityEngine.Purchasing.ProductDefinition)
extern "C"  String_t* UIFakeStore_U3CCreateRetrieveProductsQuestionU3Em__4_m1781217237 (Il2CppObject * __this /* static, unused */, ProductDefinition_t1942475268 * ___pid0, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		ProductDefinition_t1942475268 * L_0 = ___pid0;
		NullCheck(L_0);
		String_t* L_1 = ProductDefinition_get_id_m264072292(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore/DialogRequest::.ctor()
extern "C"  void DialogRequest__ctor_m3883507536 (DialogRequest_t2092195449 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore/LifecycleNotifier::.ctor()
extern "C"  void LifecycleNotifier__ctor_m1493596751 (LifecycleNotifier_t1057582876 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore/LifecycleNotifier::OnDestroy()
extern "C"  void LifecycleNotifier_OnDestroy_m3855820152 (LifecycleNotifier_t1057582876 * __this, const MethodInfo* method)
{
	{
		Action_t3226471752 * L_0 = __this->get_OnDestroyCallback_2();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Action_t3226471752 * L_1 = __this->get_OnDestroyCallback_2();
		NullCheck(L_1);
		Action_Invoke_m3801112262(L_1, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::.ctor(UnityEngine.Purchasing.Default.IWindowsIAP,Uniject.IUtil,UnityEngine.ILogger)
extern "C"  void WinRTStore__ctor_m3142178002 (WinRTStore_t36043095 * __this, Il2CppObject * ___win80, Il2CppObject * ___util1, Il2CppObject * ___logger2, const MethodInfo* method)
{
	{
		AbstractStore__ctor_m212291193(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___win80;
		__this->set_win8_0(L_0);
		Il2CppObject * L_1 = ___util1;
		__this->set_util_2(L_1);
		Il2CppObject * L_2 = ___logger2;
		__this->set_logger_3(L_2);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::SetWindowsIAP(UnityEngine.Purchasing.Default.IWindowsIAP)
extern "C"  void WinRTStore_SetWindowsIAP_m1317379130 (WinRTStore_t36043095 * __this, Il2CppObject * ___iap0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___iap0;
		__this->set_win8_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern "C"  void WinRTStore_Initialize_m2648058662 (WinRTStore_t36043095 * __this, Il2CppObject * ___biller0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___biller0;
		__this->set_callback_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern Il2CppClass* WinRTStore_t36043095_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t460188795_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2004692778_il2cpp_TypeInfo_var;
extern Il2CppClass* IWindowsIAP_t818184396_il2cpp_TypeInfo_var;
extern const MethodInfo* WinRTStore_U3CRetrieveProductsU3Em__0_m419161337_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m4202709936_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisProductDefinition_t1942475268_m96461137_MethodInfo_var;
extern const MethodInfo* WinRTStore_U3CRetrieveProductsU3Em__1_m790770486_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m432649268_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisProductDefinition_t1942475268_TisWinProductDescription_t1075111405_m2891196840_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisWinProductDescription_t1075111405_m820579046_MethodInfo_var;
extern const uint32_t WinRTStore_RetrieveProducts_m1073973345_MetadataUsageId;
extern "C"  void WinRTStore_RetrieveProducts_m1073973345 (WinRTStore_t36043095 * __this, ReadOnlyCollection_1_t2128260960 * ___productDefs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WinRTStore_RetrieveProducts_m1073973345_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	ReadOnlyCollection_1_t2128260960 * G_B2_0 = NULL;
	ReadOnlyCollection_1_t2128260960 * G_B1_0 = NULL;
	Il2CppObject* G_B4_0 = NULL;
	Il2CppObject* G_B3_0 = NULL;
	{
		ReadOnlyCollection_1_t2128260960 * L_0 = ___productDefs0;
		Func_2_t460188795 * L_1 = ((WinRTStore_t36043095_StaticFields*)WinRTStore_t36043095_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_4();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001a;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)WinRTStore_U3CRetrieveProductsU3Em__0_m419161337_MethodInfo_var);
		Func_2_t460188795 * L_3 = (Func_2_t460188795 *)il2cpp_codegen_object_new(Func_2_t460188795_il2cpp_TypeInfo_var);
		Func_2__ctor_m4202709936(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m4202709936_MethodInfo_var);
		((WinRTStore_t36043095_StaticFields*)WinRTStore_t36043095_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_4(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001a:
	{
		Func_2_t460188795 * L_4 = ((WinRTStore_t36043095_StaticFields*)WinRTStore_t36043095_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_4();
		Il2CppObject* L_5 = Enumerable_Where_TisProductDefinition_t1942475268_m96461137(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Enumerable_Where_TisProductDefinition_t1942475268_m96461137_MethodInfo_var);
		Func_2_t2004692778 * L_6 = ((WinRTStore_t36043095_StaticFields*)WinRTStore_t36043095_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_5();
		G_B3_0 = L_5;
		if (L_6)
		{
			G_B4_0 = L_5;
			goto IL_003c;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)WinRTStore_U3CRetrieveProductsU3Em__1_m790770486_MethodInfo_var);
		Func_2_t2004692778 * L_8 = (Func_2_t2004692778 *)il2cpp_codegen_object_new(Func_2_t2004692778_il2cpp_TypeInfo_var);
		Func_2__ctor_m432649268(L_8, NULL, L_7, /*hidden argument*/Func_2__ctor_m432649268_MethodInfo_var);
		((WinRTStore_t36043095_StaticFields*)WinRTStore_t36043095_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_5(L_8);
		G_B4_0 = G_B3_0;
	}

IL_003c:
	{
		Func_2_t2004692778 * L_9 = ((WinRTStore_t36043095_StaticFields*)WinRTStore_t36043095_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_5();
		Il2CppObject* L_10 = Enumerable_Select_TisProductDefinition_t1942475268_TisWinProductDescription_t1075111405_m2891196840(NULL /*static, unused*/, G_B4_0, L_9, /*hidden argument*/Enumerable_Select_TisProductDefinition_t1942475268_TisWinProductDescription_t1075111405_m2891196840_MethodInfo_var);
		V_0 = L_10;
		Il2CppObject * L_11 = __this->get_win8_0();
		Il2CppObject* L_12 = V_0;
		List_1_t444232537 * L_13 = Enumerable_ToList_TisWinProductDescription_t1075111405_m820579046(NULL /*static, unused*/, L_12, /*hidden argument*/Enumerable_ToList_TisWinProductDescription_t1075111405_m820579046_MethodInfo_var);
		NullCheck(L_11);
		InterfaceActionInvoker1< List_1_t444232537 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.Default.IWindowsIAP::BuildDummyProducts(System.Collections.Generic.List`1<UnityEngine.Purchasing.Default.WinProductDescription>) */, IWindowsIAP_t818184396_il2cpp_TypeInfo_var, L_11, L_13);
		WinRTStore_init_m420138384(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern Il2CppClass* IWindowsIAP_t818184396_il2cpp_TypeInfo_var;
extern const uint32_t WinRTStore_FinishTransaction_m1157468965_MetadataUsageId;
extern "C"  void WinRTStore_FinishTransaction_m1157468965 (WinRTStore_t36043095 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___transactionId1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WinRTStore_FinishTransaction_m1157468965_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_win8_0();
		String_t* L_1 = ___transactionId1;
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(4 /* System.Void UnityEngine.Purchasing.Default.IWindowsIAP::FinaliseTransaction(System.String) */, IWindowsIAP_t818184396_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::init(System.Int32)
extern Il2CppClass* IWindowsIAP_t818184396_il2cpp_TypeInfo_var;
extern const uint32_t WinRTStore_init_m420138384_MetadataUsageId;
extern "C"  void WinRTStore_init_m420138384 (WinRTStore_t36043095 * __this, int32_t ___delay0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WinRTStore_init_m420138384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_win8_0();
		NullCheck(L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void UnityEngine.Purchasing.Default.IWindowsIAP::Initialize(UnityEngine.Purchasing.Default.IWindowsIAPCallback) */, IWindowsIAP_t818184396_il2cpp_TypeInfo_var, L_0, __this);
		Il2CppObject * L_1 = __this->get_win8_0();
		NullCheck(L_1);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UnityEngine.Purchasing.Default.IWindowsIAP::RetrieveProducts(System.Boolean) */, IWindowsIAP_t818184396_il2cpp_TypeInfo_var, L_1, (bool)1);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern Il2CppClass* IWindowsIAP_t818184396_il2cpp_TypeInfo_var;
extern const uint32_t WinRTStore_Purchase_m27999093_MetadataUsageId;
extern "C"  void WinRTStore_Purchase_m27999093 (WinRTStore_t36043095 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___developerPayload1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WinRTStore_Purchase_m27999093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_win8_0();
		ProductDefinition_t1942475268 * L_1 = ___product0;
		NullCheck(L_1);
		String_t* L_2 = ProductDefinition_get_storeSpecificId_m2251287741(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(3 /* System.Void UnityEngine.Purchasing.Default.IWindowsIAP::Purchase(System.String) */, IWindowsIAP_t818184396_il2cpp_TypeInfo_var, L_0, L_2);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::restoreTransactions(System.Boolean)
extern Il2CppClass* IWindowsIAP_t818184396_il2cpp_TypeInfo_var;
extern const uint32_t WinRTStore_restoreTransactions_m1421431749_MetadataUsageId;
extern "C"  void WinRTStore_restoreTransactions_m1421431749 (WinRTStore_t36043095 * __this, bool ___pausing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WinRTStore_restoreTransactions_m1421431749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___pausing0;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_win8_0();
		NullCheck(L_1);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UnityEngine.Purchasing.Default.IWindowsIAP::RetrieveProducts(System.Boolean) */, IWindowsIAP_t818184396_il2cpp_TypeInfo_var, L_1, (bool)0);
	}

IL_0015:
	{
		return;
	}
}
// System.Boolean UnityEngine.Purchasing.WinRTStore::<RetrieveProducts>m__0(UnityEngine.Purchasing.ProductDefinition)
extern "C"  bool WinRTStore_U3CRetrieveProductsU3Em__0_m419161337 (Il2CppObject * __this /* static, unused */, ProductDefinition_t1942475268 * ___def0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ProductDefinition_t1942475268 * L_0 = ___def0;
		NullCheck(L_0);
		int32_t L_1 = ProductDefinition_get_type_m590914665(L_0, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0013;
	}

IL_0013:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Purchasing.Default.WinProductDescription UnityEngine.Purchasing.WinRTStore::<RetrieveProducts>m__1(UnityEngine.Purchasing.ProductDefinition)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WinProductDescription_t1075111405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1291769943;
extern Il2CppCodeGenString* _stringLiteral956924922;
extern Il2CppCodeGenString* _stringLiteral530053810;
extern Il2CppCodeGenString* _stringLiteral1690816450;
extern const uint32_t WinRTStore_U3CRetrieveProductsU3Em__1_m790770486_MetadataUsageId;
extern "C"  WinProductDescription_t1075111405 * WinRTStore_U3CRetrieveProductsU3Em__1_m790770486 (Il2CppObject * __this /* static, unused */, ProductDefinition_t1942475268 * ___def0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WinRTStore_U3CRetrieveProductsU3Em__1_m790770486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WinProductDescription_t1075111405 * V_0 = NULL;
	{
		ProductDefinition_t1942475268 * L_0 = ___def0;
		NullCheck(L_0);
		String_t* L_1 = ProductDefinition_get_storeSpecificId_m2251287741(L_0, /*hidden argument*/NULL);
		ProductDefinition_t1942475268 * L_2 = ___def0;
		NullCheck(L_2);
		String_t* L_3 = ProductDefinition_get_storeSpecificId_m2251287741(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral956924922, L_3, /*hidden argument*/NULL);
		ProductDefinition_t1942475268 * L_5 = ___def0;
		NullCheck(L_5);
		String_t* L_6 = ProductDefinition_get_storeSpecificId_m2251287741(L_5, /*hidden argument*/NULL);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral530053810, L_6, /*hidden argument*/NULL);
		Decimal_t724701077  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Decimal__ctor_m1770144563(&L_8, 1, 0, 0, (bool)0, 2, /*hidden argument*/NULL);
		ProductDefinition_t1942475268 * L_9 = ___def0;
		NullCheck(L_9);
		int32_t L_10 = ProductDefinition_get_type_m590914665(L_9, /*hidden argument*/NULL);
		WinProductDescription_t1075111405 * L_11 = (WinProductDescription_t1075111405 *)il2cpp_codegen_object_new(WinProductDescription_t1075111405_il2cpp_TypeInfo_var);
		WinProductDescription__ctor_m3122598843(L_11, L_1, _stringLiteral1291769943, L_4, L_7, _stringLiteral1690816450, L_8, (String_t*)NULL, (String_t*)NULL, (bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_0051;
	}

IL_0051:
	{
		WinProductDescription_t1075111405 * L_12 = V_0;
		return L_12;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
