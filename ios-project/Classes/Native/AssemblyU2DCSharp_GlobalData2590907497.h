﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<Course>
struct List_1_t2852233831;
// Course
struct Course_t3483112699;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// User
struct User_t719925459;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalData
struct  GlobalData_t2590907497  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct GlobalData_t2590907497_StaticFields
{
public:
	// System.Boolean GlobalData::inapp_unlimitedCourses
	bool ___inapp_unlimitedCourses_4;
	// System.Boolean GlobalData::inapp_env_horseShow
	bool ___inapp_env_horseShow_5;
	// System.Boolean GlobalData::inapp_env_indoor
	bool ___inapp_env_indoor_6;
	// System.String GlobalData::visualizer_env
	String_t* ___visualizer_env_7;
	// System.Collections.Generic.List`1<Course> GlobalData::Courses
	List_1_t2852233831 * ___Courses_8;
	// System.Int32 GlobalData::CoursesMaxCount
	int32_t ___CoursesMaxCount_9;
	// Course GlobalData::CurrentCourse
	Course_t3483112699 * ___CurrentCourse_10;
	// System.Boolean GlobalData::SaveThroughLogin
	bool ___SaveThroughLogin_11;
	// System.Collections.Generic.List`1<System.String> GlobalData::scenes
	List_1_t1398341365 * ___scenes_12;
	// User GlobalData::<User>k__BackingField
	User_t719925459 * ___U3CUserU3Ek__BackingField_13;
	// System.Single GlobalData::<scale>k__BackingField
	float ___U3CscaleU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_inapp_unlimitedCourses_4() { return static_cast<int32_t>(offsetof(GlobalData_t2590907497_StaticFields, ___inapp_unlimitedCourses_4)); }
	inline bool get_inapp_unlimitedCourses_4() const { return ___inapp_unlimitedCourses_4; }
	inline bool* get_address_of_inapp_unlimitedCourses_4() { return &___inapp_unlimitedCourses_4; }
	inline void set_inapp_unlimitedCourses_4(bool value)
	{
		___inapp_unlimitedCourses_4 = value;
	}

	inline static int32_t get_offset_of_inapp_env_horseShow_5() { return static_cast<int32_t>(offsetof(GlobalData_t2590907497_StaticFields, ___inapp_env_horseShow_5)); }
	inline bool get_inapp_env_horseShow_5() const { return ___inapp_env_horseShow_5; }
	inline bool* get_address_of_inapp_env_horseShow_5() { return &___inapp_env_horseShow_5; }
	inline void set_inapp_env_horseShow_5(bool value)
	{
		___inapp_env_horseShow_5 = value;
	}

	inline static int32_t get_offset_of_inapp_env_indoor_6() { return static_cast<int32_t>(offsetof(GlobalData_t2590907497_StaticFields, ___inapp_env_indoor_6)); }
	inline bool get_inapp_env_indoor_6() const { return ___inapp_env_indoor_6; }
	inline bool* get_address_of_inapp_env_indoor_6() { return &___inapp_env_indoor_6; }
	inline void set_inapp_env_indoor_6(bool value)
	{
		___inapp_env_indoor_6 = value;
	}

	inline static int32_t get_offset_of_visualizer_env_7() { return static_cast<int32_t>(offsetof(GlobalData_t2590907497_StaticFields, ___visualizer_env_7)); }
	inline String_t* get_visualizer_env_7() const { return ___visualizer_env_7; }
	inline String_t** get_address_of_visualizer_env_7() { return &___visualizer_env_7; }
	inline void set_visualizer_env_7(String_t* value)
	{
		___visualizer_env_7 = value;
		Il2CppCodeGenWriteBarrier(&___visualizer_env_7, value);
	}

	inline static int32_t get_offset_of_Courses_8() { return static_cast<int32_t>(offsetof(GlobalData_t2590907497_StaticFields, ___Courses_8)); }
	inline List_1_t2852233831 * get_Courses_8() const { return ___Courses_8; }
	inline List_1_t2852233831 ** get_address_of_Courses_8() { return &___Courses_8; }
	inline void set_Courses_8(List_1_t2852233831 * value)
	{
		___Courses_8 = value;
		Il2CppCodeGenWriteBarrier(&___Courses_8, value);
	}

	inline static int32_t get_offset_of_CoursesMaxCount_9() { return static_cast<int32_t>(offsetof(GlobalData_t2590907497_StaticFields, ___CoursesMaxCount_9)); }
	inline int32_t get_CoursesMaxCount_9() const { return ___CoursesMaxCount_9; }
	inline int32_t* get_address_of_CoursesMaxCount_9() { return &___CoursesMaxCount_9; }
	inline void set_CoursesMaxCount_9(int32_t value)
	{
		___CoursesMaxCount_9 = value;
	}

	inline static int32_t get_offset_of_CurrentCourse_10() { return static_cast<int32_t>(offsetof(GlobalData_t2590907497_StaticFields, ___CurrentCourse_10)); }
	inline Course_t3483112699 * get_CurrentCourse_10() const { return ___CurrentCourse_10; }
	inline Course_t3483112699 ** get_address_of_CurrentCourse_10() { return &___CurrentCourse_10; }
	inline void set_CurrentCourse_10(Course_t3483112699 * value)
	{
		___CurrentCourse_10 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentCourse_10, value);
	}

	inline static int32_t get_offset_of_SaveThroughLogin_11() { return static_cast<int32_t>(offsetof(GlobalData_t2590907497_StaticFields, ___SaveThroughLogin_11)); }
	inline bool get_SaveThroughLogin_11() const { return ___SaveThroughLogin_11; }
	inline bool* get_address_of_SaveThroughLogin_11() { return &___SaveThroughLogin_11; }
	inline void set_SaveThroughLogin_11(bool value)
	{
		___SaveThroughLogin_11 = value;
	}

	inline static int32_t get_offset_of_scenes_12() { return static_cast<int32_t>(offsetof(GlobalData_t2590907497_StaticFields, ___scenes_12)); }
	inline List_1_t1398341365 * get_scenes_12() const { return ___scenes_12; }
	inline List_1_t1398341365 ** get_address_of_scenes_12() { return &___scenes_12; }
	inline void set_scenes_12(List_1_t1398341365 * value)
	{
		___scenes_12 = value;
		Il2CppCodeGenWriteBarrier(&___scenes_12, value);
	}

	inline static int32_t get_offset_of_U3CUserU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GlobalData_t2590907497_StaticFields, ___U3CUserU3Ek__BackingField_13)); }
	inline User_t719925459 * get_U3CUserU3Ek__BackingField_13() const { return ___U3CUserU3Ek__BackingField_13; }
	inline User_t719925459 ** get_address_of_U3CUserU3Ek__BackingField_13() { return &___U3CUserU3Ek__BackingField_13; }
	inline void set_U3CUserU3Ek__BackingField_13(User_t719925459 * value)
	{
		___U3CUserU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUserU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CscaleU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GlobalData_t2590907497_StaticFields, ___U3CscaleU3Ek__BackingField_14)); }
	inline float get_U3CscaleU3Ek__BackingField_14() const { return ___U3CscaleU3Ek__BackingField_14; }
	inline float* get_address_of_U3CscaleU3Ek__BackingField_14() { return &___U3CscaleU3Ek__BackingField_14; }
	inline void set_U3CscaleU3Ek__BackingField_14(float value)
	{
		___U3CscaleU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
