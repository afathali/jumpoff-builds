﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MCGBehaviour/InjectionContainer
struct InjectionContainer_t3223711125;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Object>>
struct Dictionary_2_t3995928324;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MCGBehaviour
struct  MCGBehaviour_t3307770884  : public MonoBehaviour_t1158329972
{
public:
	// MCGBehaviour/InjectionContainer MCGBehaviour::_container
	InjectionContainer_t3223711125 * ____container_2;

public:
	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(MCGBehaviour_t3307770884, ____container_2)); }
	inline InjectionContainer_t3223711125 * get__container_2() const { return ____container_2; }
	inline InjectionContainer_t3223711125 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(InjectionContainer_t3223711125 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier(&____container_2, value);
	}
};

struct MCGBehaviour_t3307770884_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Object>> MCGBehaviour::_subscriptions
	Dictionary_2_t3995928324 * ____subscriptions_3;

public:
	inline static int32_t get_offset_of__subscriptions_3() { return static_cast<int32_t>(offsetof(MCGBehaviour_t3307770884_StaticFields, ____subscriptions_3)); }
	inline Dictionary_2_t3995928324 * get__subscriptions_3() const { return ____subscriptions_3; }
	inline Dictionary_2_t3995928324 ** get_address_of__subscriptions_3() { return &____subscriptions_3; }
	inline void set__subscriptions_3(Dictionary_2_t3995928324 * value)
	{
		____subscriptions_3 = value;
		Il2CppCodeGenWriteBarrier(&____subscriptions_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
