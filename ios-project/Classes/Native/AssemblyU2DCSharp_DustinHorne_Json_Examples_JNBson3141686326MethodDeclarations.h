﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DustinHorne.Json.Examples.JNBsonSample
struct JNBsonSample_t3141686326;

#include "codegen/il2cpp-codegen.h"

// System.Void DustinHorne.Json.Examples.JNBsonSample::.ctor()
extern "C"  void JNBsonSample__ctor_m1205167751 (JNBsonSample_t3141686326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DustinHorne.Json.Examples.JNBsonSample::Sample()
extern "C"  void JNBsonSample_Sample_m2745892665 (JNBsonSample_t3141686326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
