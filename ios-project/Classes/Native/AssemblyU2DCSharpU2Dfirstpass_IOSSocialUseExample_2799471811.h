﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator5
struct  U3CPostTwitterScreenshotU3Ec__Iterator5_t2799471811  : public Il2CppObject
{
public:
	// System.Int32 IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator5::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator5::<height>__1
	int32_t ___U3CheightU3E__1_1;
	// UnityEngine.Texture2D IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator5::<tex>__2
	Texture2D_t3542995729 * ___U3CtexU3E__2_2;
	// System.Int32 IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator5::$PC
	int32_t ___U24PC_3;
	// System.Object IOSSocialUseExample/<PostTwitterScreenshot>c__Iterator5::$current
	Il2CppObject * ___U24current_4;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPostTwitterScreenshotU3Ec__Iterator5_t2799471811, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__1_1() { return static_cast<int32_t>(offsetof(U3CPostTwitterScreenshotU3Ec__Iterator5_t2799471811, ___U3CheightU3E__1_1)); }
	inline int32_t get_U3CheightU3E__1_1() const { return ___U3CheightU3E__1_1; }
	inline int32_t* get_address_of_U3CheightU3E__1_1() { return &___U3CheightU3E__1_1; }
	inline void set_U3CheightU3E__1_1(int32_t value)
	{
		___U3CheightU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CtexU3E__2_2() { return static_cast<int32_t>(offsetof(U3CPostTwitterScreenshotU3Ec__Iterator5_t2799471811, ___U3CtexU3E__2_2)); }
	inline Texture2D_t3542995729 * get_U3CtexU3E__2_2() const { return ___U3CtexU3E__2_2; }
	inline Texture2D_t3542995729 ** get_address_of_U3CtexU3E__2_2() { return &___U3CtexU3E__2_2; }
	inline void set_U3CtexU3E__2_2(Texture2D_t3542995729 * value)
	{
		___U3CtexU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtexU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CPostTwitterScreenshotU3Ec__Iterator5_t2799471811, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CPostTwitterScreenshotU3Ec__Iterator5_t2799471811, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
