﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_AdvertisingIdLoadResult
struct GP_AdvertisingIdLoadResult_t2783375090;

#include "codegen/il2cpp-codegen.h"

// System.Void GP_AdvertisingIdLoadResult::.ctor(System.Boolean)
extern "C"  void GP_AdvertisingIdLoadResult__ctor_m3714000596 (GP_AdvertisingIdLoadResult_t2783375090 * __this, bool ___IsResultSucceeded0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
