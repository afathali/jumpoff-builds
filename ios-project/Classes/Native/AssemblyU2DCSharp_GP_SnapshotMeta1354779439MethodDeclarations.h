﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_SnapshotMeta
struct GP_SnapshotMeta_t1354779439;

#include "codegen/il2cpp-codegen.h"

// System.Void GP_SnapshotMeta::.ctor()
extern "C"  void GP_SnapshotMeta__ctor_m896855036 (GP_SnapshotMeta_t1354779439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
