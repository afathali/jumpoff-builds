﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TempJumpInfo
struct TempJumpInfo_t2313300354;

#include "codegen/il2cpp-codegen.h"

// System.Void TempJumpInfo::.ctor()
extern "C"  void TempJumpInfo__ctor_m4273808487 (TempJumpInfo_t2313300354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TempJumpInfo::get_xPos()
extern "C"  float TempJumpInfo_get_xPos_m525103380 (TempJumpInfo_t2313300354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempJumpInfo::set_xPos(System.Single)
extern "C"  void TempJumpInfo_set_xPos_m562263327 (TempJumpInfo_t2313300354 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TempJumpInfo::get_yPos()
extern "C"  float TempJumpInfo_get_yPos_m525096021 (TempJumpInfo_t2313300354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempJumpInfo::set_yPos(System.Single)
extern "C"  void TempJumpInfo_set_yPos_m3474392448 (TempJumpInfo_t2313300354 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TempJumpInfo::get_rotation()
extern "C"  float TempJumpInfo_get_rotation_m3657160792 (TempJumpInfo_t2313300354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempJumpInfo::set_rotation(System.Single)
extern "C"  void TempJumpInfo_set_rotation_m2844980007 (TempJumpInfo_t2313300354 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TempJumpInfo::get_jumpNumber()
extern "C"  int32_t TempJumpInfo_get_jumpNumber_m3590966361 (TempJumpInfo_t2313300354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempJumpInfo::set_jumpNumber(System.Int32)
extern "C"  void TempJumpInfo_set_jumpNumber_m4241221484 (TempJumpInfo_t2313300354 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TempJumpInfo::get_jumpType()
extern "C"  int32_t TempJumpInfo_get_jumpType_m2796804662 (TempJumpInfo_t2313300354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempJumpInfo::set_jumpType(System.Int32)
extern "C"  void TempJumpInfo_set_jumpType_m4199716749 (TempJumpInfo_t2313300354 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
