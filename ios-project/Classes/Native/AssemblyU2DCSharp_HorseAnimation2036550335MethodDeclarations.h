﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HorseAnimation
struct HorseAnimation_t2036550335;

#include "codegen/il2cpp-codegen.h"

// System.Void HorseAnimation::.ctor()
extern "C"  void HorseAnimation__ctor_m2839362472 (HorseAnimation_t2036550335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HorseAnimation::Start()
extern "C"  void HorseAnimation_Start_m3917462860 (HorseAnimation_t2036550335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HorseAnimation::Jump()
extern "C"  void HorseAnimation_Jump_m298332854 (HorseAnimation_t2036550335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HorseAnimation::FixedUpdate()
extern "C"  void HorseAnimation_FixedUpdate_m1885152013 (HorseAnimation_t2036550335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
