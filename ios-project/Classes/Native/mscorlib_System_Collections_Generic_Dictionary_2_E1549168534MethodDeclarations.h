﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3877028371(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1549168534 *, Dictionary_2_t229143832 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1214948562(__this, method) ((  Il2CppObject * (*) (Enumerator_t1549168534 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1410731948(__this, method) ((  void (*) (Enumerator_t1549168534 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m798140001(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1549168534 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3906164164(__this, method) ((  Il2CppObject * (*) (Enumerator_t1549168534 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2615065550(__this, method) ((  Il2CppObject * (*) (Enumerator_t1549168534 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::MoveNext()
#define Enumerator_MoveNext_m4101982676(__this, method) ((  bool (*) (Enumerator_t1549168534 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::get_Current()
#define Enumerator_get_Current_m4164281780(__this, method) ((  KeyValuePair_2_t2281456350  (*) (Enumerator_t1549168534 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m4207179911(__this, method) ((  String_t* (*) (Enumerator_t1549168534 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m828499775(__this, method) ((  GooglePurchaseTemplate_t2609331866 * (*) (Enumerator_t1549168534 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::Reset()
#define Enumerator_Reset_m3546567753(__this, method) ((  void (*) (Enumerator_t1549168534 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::VerifyState()
#define Enumerator_VerifyState_m4170102076(__this, method) ((  void (*) (Enumerator_t1549168534 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1195780924(__this, method) ((  void (*) (Enumerator_t1549168534 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePurchaseTemplate>::Dispose()
#define Enumerator_Dispose_m2673426087(__this, method) ((  void (*) (Enumerator_t1549168534 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
