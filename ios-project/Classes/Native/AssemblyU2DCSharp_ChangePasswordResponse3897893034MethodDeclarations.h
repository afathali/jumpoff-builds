﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangePasswordResponse
struct ChangePasswordResponse_t3897893034;
// System.String
struct String_t;
// ChangePasswordRequest
struct ChangePasswordRequest_t4102061450;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ChangePasswordRequest4102061450.h"

// System.Void ChangePasswordResponse::.ctor()
extern "C"  void ChangePasswordResponse__ctor_m2106527777 (ChangePasswordResponse_t3897893034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChangePasswordResponse::get_Success()
extern "C"  bool ChangePasswordResponse_get_Success_m2164626187 (ChangePasswordResponse_t3897893034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangePasswordResponse::set_Success(System.Boolean)
extern "C"  void ChangePasswordResponse_set_Success_m902053184 (ChangePasswordResponse_t3897893034 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChangePasswordResponse::get_ErrorMessage()
extern "C"  String_t* ChangePasswordResponse_get_ErrorMessage_m50606328 (ChangePasswordResponse_t3897893034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangePasswordResponse::set_ErrorMessage(System.String)
extern "C"  void ChangePasswordResponse_set_ErrorMessage_m1715986285 (ChangePasswordResponse_t3897893034 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChangePasswordRequest ChangePasswordResponse::get_Request()
extern "C"  ChangePasswordRequest_t4102061450 * ChangePasswordResponse_get_Request_m3547187438 (ChangePasswordResponse_t3897893034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangePasswordResponse::set_Request(ChangePasswordRequest)
extern "C"  void ChangePasswordResponse_set_Request_m1581123469 (ChangePasswordResponse_t3897893034 * __this, ChangePasswordRequest_t4102061450 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
