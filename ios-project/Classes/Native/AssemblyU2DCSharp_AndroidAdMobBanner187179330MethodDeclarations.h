﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidAdMobBanner
struct AndroidAdMobBanner_t187179330;
// System.Collections.Generic.Dictionary`2<System.String,GoogleMobileAdBanner>
struct Dictionary_2_t3238598220;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidAdMobBanner::.ctor()
extern "C"  void AndroidAdMobBanner__ctor_m2761232957 (AndroidAdMobBanner_t187179330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobBanner::.cctor()
extern "C"  void AndroidAdMobBanner__cctor_m1918516668 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobBanner::Awake()
extern "C"  void AndroidAdMobBanner_Awake_m3387940914 (AndroidAdMobBanner_t187179330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobBanner::Start()
extern "C"  void AndroidAdMobBanner_Start_m950739501 (AndroidAdMobBanner_t187179330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobBanner::OnDestroy()
extern "C"  void AndroidAdMobBanner_OnDestroy_m1881053584 (AndroidAdMobBanner_t187179330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobBanner::ShowBanner()
extern "C"  void AndroidAdMobBanner_ShowBanner_m89127182 (AndroidAdMobBanner_t187179330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobBanner::HideBanner()
extern "C"  void AndroidAdMobBanner_HideBanner_m3000433853 (AndroidAdMobBanner_t187179330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,GoogleMobileAdBanner> AndroidAdMobBanner::get_registerdBanners()
extern "C"  Dictionary_2_t3238598220 * AndroidAdMobBanner_get_registerdBanners_m1388577562 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidAdMobBanner::get_sceneBannerId()
extern "C"  String_t* AndroidAdMobBanner_get_sceneBannerId_m1131113158 (AndroidAdMobBanner_t187179330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
