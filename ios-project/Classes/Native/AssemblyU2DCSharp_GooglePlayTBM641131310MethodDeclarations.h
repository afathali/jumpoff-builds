﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayTBM
struct GooglePlayTBM_t641131310;
// System.Action`1<GP_TBM_LoadMatchesResult>
struct Action_1_t643572420;
// System.Action`1<GP_TBM_MatchInitiatedResult>
struct Action_1_t3945860229;
// System.Action`1<GP_TBM_CancelMatchResult>
struct Action_1_t3681092534;
// System.Action`1<GP_TBM_LeaveMatchResult>
struct Action_1_t3459603101;
// System.Action`1<GP_TBM_LoadMatchResult>
struct Action_1_t1159218312;
// System.Action`1<GP_TBM_UpdateMatchResult>
struct Action_1_t3744805351;
// System.Action`1<GP_TBM_MatchReceivedResult>
struct Action_1_t2196472297;
// System.Action`1<GP_TBM_MatchRemovedResult>
struct Action_1_t488154502;
// System.Action`1<AndroidActivityResult>
struct Action_1_t3559310183;
// System.Action`2<System.String,GP_TBM_MatchInitiatedResult>
struct Action_2_t2054415243;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// GP_ParticipantResult[]
struct GP_ParticipantResultU5BU5D_t353789473;
// GP_TBM_MatchTurnStatus[]
struct GP_TBM_MatchTurnStatusU5BU5D_t2451090547;
// System.Collections.Generic.Dictionary`2<System.String,GP_TBM_Match>
struct Dictionary_2_t3189857243;
// GP_TBM_Match
struct GP_TBM_Match_t1275077981;
// GP_TBM_LoadMatchesResult
struct GP_TBM_LoadMatchesResult_t841773038;
// GP_TBM_MatchInitiatedResult
struct GP_TBM_MatchInitiatedResult_t4144060847;
// GP_TBM_CancelMatchResult
struct GP_TBM_CancelMatchResult_t3879293152;
// GP_TBM_LeaveMatchResult
struct GP_TBM_LeaveMatchResult_t3657803719;
// GP_TBM_LoadMatchResult
struct GP_TBM_LoadMatchResult_t1357418930;
// GP_TBM_UpdateMatchResult
struct GP_TBM_UpdateMatchResult_t3943005969;
// GP_TBM_MatchReceivedResult
struct GP_TBM_MatchReceivedResult_t2394672915;
// GP_TBM_MatchRemovedResult
struct GP_TBM_MatchRemovedResult_t686355120;
// AndroidActivityResult
struct AndroidActivityResult_t3757510801;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchesSortOrder3142286675.h"
#include "AssemblyU2DCSharp_GP_TBM_Match1275077981.h"
#include "AssemblyU2DCSharp_GP_TBM_LoadMatchesResult841773038.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchInitiatedResult4144060847.h"
#include "AssemblyU2DCSharp_GP_TBM_CancelMatchResult3879293152.h"
#include "AssemblyU2DCSharp_GP_TBM_LeaveMatchResult3657803719.h"
#include "AssemblyU2DCSharp_GP_TBM_LoadMatchResult1357418930.h"
#include "AssemblyU2DCSharp_GP_TBM_UpdateMatchResult3943005969.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchReceivedResult2394672915.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchRemovedResult686355120.h"
#include "AssemblyU2DCSharp_AndroidActivityResult3757510801.h"

// System.Void GooglePlayTBM::.ctor()
extern "C"  void GooglePlayTBM__ctor_m3791999325 (GooglePlayTBM_t641131310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::.cctor()
extern "C"  void GooglePlayTBM__cctor_m1652577608 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::add_ActionMatchesResultLoaded(System.Action`1<GP_TBM_LoadMatchesResult>)
extern "C"  void GooglePlayTBM_add_ActionMatchesResultLoaded_m394565604 (Il2CppObject * __this /* static, unused */, Action_1_t643572420 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::remove_ActionMatchesResultLoaded(System.Action`1<GP_TBM_LoadMatchesResult>)
extern "C"  void GooglePlayTBM_remove_ActionMatchesResultLoaded_m351442141 (Il2CppObject * __this /* static, unused */, Action_1_t643572420 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::add_ActionMatchInitiated(System.Action`1<GP_TBM_MatchInitiatedResult>)
extern "C"  void GooglePlayTBM_add_ActionMatchInitiated_m2163681478 (Il2CppObject * __this /* static, unused */, Action_1_t3945860229 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::remove_ActionMatchInitiated(System.Action`1<GP_TBM_MatchInitiatedResult>)
extern "C"  void GooglePlayTBM_remove_ActionMatchInitiated_m3068914745 (Il2CppObject * __this /* static, unused */, Action_1_t3945860229 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::add_ActionMatchCanceled(System.Action`1<GP_TBM_CancelMatchResult>)
extern "C"  void GooglePlayTBM_add_ActionMatchCanceled_m4119746179 (Il2CppObject * __this /* static, unused */, Action_1_t3681092534 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::remove_ActionMatchCanceled(System.Action`1<GP_TBM_CancelMatchResult>)
extern "C"  void GooglePlayTBM_remove_ActionMatchCanceled_m2971405150 (Il2CppObject * __this /* static, unused */, Action_1_t3681092534 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::add_ActionMatchLeaved(System.Action`1<GP_TBM_LeaveMatchResult>)
extern "C"  void GooglePlayTBM_add_ActionMatchLeaved_m4152414178 (Il2CppObject * __this /* static, unused */, Action_1_t3459603101 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::remove_ActionMatchLeaved(System.Action`1<GP_TBM_LeaveMatchResult>)
extern "C"  void GooglePlayTBM_remove_ActionMatchLeaved_m174449513 (Il2CppObject * __this /* static, unused */, Action_1_t3459603101 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::add_ActionMatchDataLoaded(System.Action`1<GP_TBM_LoadMatchResult>)
extern "C"  void GooglePlayTBM_add_ActionMatchDataLoaded_m1401810419 (Il2CppObject * __this /* static, unused */, Action_1_t1159218312 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::remove_ActionMatchDataLoaded(System.Action`1<GP_TBM_LoadMatchResult>)
extern "C"  void GooglePlayTBM_remove_ActionMatchDataLoaded_m1888857134 (Il2CppObject * __this /* static, unused */, Action_1_t1159218312 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::add_ActionMatchUpdated(System.Action`1<GP_TBM_UpdateMatchResult>)
extern "C"  void GooglePlayTBM_add_ActionMatchUpdated_m3624860696 (Il2CppObject * __this /* static, unused */, Action_1_t3744805351 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::remove_ActionMatchUpdated(System.Action`1<GP_TBM_UpdateMatchResult>)
extern "C"  void GooglePlayTBM_remove_ActionMatchUpdated_m3488784597 (Il2CppObject * __this /* static, unused */, Action_1_t3744805351 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::add_ActionMatchTurnFinished(System.Action`1<GP_TBM_UpdateMatchResult>)
extern "C"  void GooglePlayTBM_add_ActionMatchTurnFinished_m4163406176 (Il2CppObject * __this /* static, unused */, Action_1_t3744805351 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::remove_ActionMatchTurnFinished(System.Action`1<GP_TBM_UpdateMatchResult>)
extern "C"  void GooglePlayTBM_remove_ActionMatchTurnFinished_m2187708803 (Il2CppObject * __this /* static, unused */, Action_1_t3744805351 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::add_ActionMatchReceived(System.Action`1<GP_TBM_MatchReceivedResult>)
extern "C"  void GooglePlayTBM_add_ActionMatchReceived_m128532230 (Il2CppObject * __this /* static, unused */, Action_1_t2196472297 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::remove_ActionMatchReceived(System.Action`1<GP_TBM_MatchReceivedResult>)
extern "C"  void GooglePlayTBM_remove_ActionMatchReceived_m1503114009 (Il2CppObject * __this /* static, unused */, Action_1_t2196472297 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::add_ActionMatchRemoved(System.Action`1<GP_TBM_MatchRemovedResult>)
extern "C"  void GooglePlayTBM_add_ActionMatchRemoved_m3907709702 (Il2CppObject * __this /* static, unused */, Action_1_t488154502 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::remove_ActionMatchRemoved(System.Action`1<GP_TBM_MatchRemovedResult>)
extern "C"  void GooglePlayTBM_remove_ActionMatchRemoved_m2845868537 (Il2CppObject * __this /* static, unused */, Action_1_t488154502 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::add_ActionMatchCreationCanceled(System.Action`1<AndroidActivityResult>)
extern "C"  void GooglePlayTBM_add_ActionMatchCreationCanceled_m1315593161 (Il2CppObject * __this /* static, unused */, Action_1_t3559310183 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::remove_ActionMatchCreationCanceled(System.Action`1<AndroidActivityResult>)
extern "C"  void GooglePlayTBM_remove_ActionMatchCreationCanceled_m4241025366 (Il2CppObject * __this /* static, unused */, Action_1_t3559310183 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::add_ActionMatchInvitationAccepted(System.Action`2<System.String,GP_TBM_MatchInitiatedResult>)
extern "C"  void GooglePlayTBM_add_ActionMatchInvitationAccepted_m2721589742 (Il2CppObject * __this /* static, unused */, Action_2_t2054415243 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::remove_ActionMatchInvitationAccepted(System.Action`2<System.String,GP_TBM_MatchInitiatedResult>)
extern "C"  void GooglePlayTBM_remove_ActionMatchInvitationAccepted_m3975087671 (Il2CppObject * __this /* static, unused */, Action_2_t2054415243 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::add_ActionMatchInvitationDeclined(System.Action`1<System.String>)
extern "C"  void GooglePlayTBM_add_ActionMatchInvitationDeclined_m3212430459 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::remove_ActionMatchInvitationDeclined(System.Action`1<System.String>)
extern "C"  void GooglePlayTBM_remove_ActionMatchInvitationDeclined_m1151164426 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::Awake()
extern "C"  void GooglePlayTBM_Awake_m3430075246 (GooglePlayTBM_t641131310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::Init()
extern "C"  void GooglePlayTBM_Init_m1963824667 (GooglePlayTBM_t641131310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::AcceptInvitation(System.String)
extern "C"  void GooglePlayTBM_AcceptInvitation_m1939050080 (GooglePlayTBM_t641131310 * __this, String_t* ___invitationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::DeclineInvitation(System.String)
extern "C"  void GooglePlayTBM_DeclineInvitation_m4290103334 (GooglePlayTBM_t641131310 * __this, String_t* ___invitationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::DismissInvitation(System.String)
extern "C"  void GooglePlayTBM_DismissInvitation_m4188412308 (GooglePlayTBM_t641131310 * __this, String_t* ___invitationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::CreateMatch(System.Int32,System.Int32,System.String[])
extern "C"  void GooglePlayTBM_CreateMatch_m3215704480 (GooglePlayTBM_t641131310 * __this, int32_t ___minPlayers0, int32_t ___maxPlayers1, StringU5BU5D_t1642385972* ___playersIds2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::CancelMatch(System.String)
extern "C"  void GooglePlayTBM_CancelMatch_m2804642444 (GooglePlayTBM_t641131310 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::DismissMatch(System.String)
extern "C"  void GooglePlayTBM_DismissMatch_m3636091370 (GooglePlayTBM_t641131310 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::ConfirmhMatchFinis(System.String)
extern "C"  void GooglePlayTBM_ConfirmhMatchFinis_m2109731349 (GooglePlayTBM_t641131310 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::FinishMatch(System.String,System.Byte[],GP_ParticipantResult[])
extern "C"  void GooglePlayTBM_FinishMatch_m2269583578 (GooglePlayTBM_t641131310 * __this, String_t* ___matchId0, ByteU5BU5D_t3397334013* ___matchData1, GP_ParticipantResultU5BU5D_t353789473* ___results2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::LeaveMatch(System.String)
extern "C"  void GooglePlayTBM_LeaveMatch_m516906279 (GooglePlayTBM_t641131310 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::LeaveMatchDuringTurn(System.String,System.String)
extern "C"  void GooglePlayTBM_LeaveMatchDuringTurn_m2732466071 (GooglePlayTBM_t641131310 * __this, String_t* ___matchId0, String_t* ___pendingParticipantId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::LoadMatchInfo(System.String)
extern "C"  void GooglePlayTBM_LoadMatchInfo_m3076246346 (GooglePlayTBM_t641131310 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::LoadMatchesInfo(GP_TBM_MatchesSortOrder,GP_TBM_MatchTurnStatus[])
extern "C"  void GooglePlayTBM_LoadMatchesInfo_m2456751113 (GooglePlayTBM_t641131310 * __this, int32_t ___sortOreder0, GP_TBM_MatchTurnStatusU5BU5D_t2451090547* ___matchTurnStatuses1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::LoadAllMatchesInfo(GP_TBM_MatchesSortOrder)
extern "C"  void GooglePlayTBM_LoadAllMatchesInfo_m2587419434 (GooglePlayTBM_t641131310 * __this, int32_t ___sortOreder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::Rematch(System.String)
extern "C"  void GooglePlayTBM_Rematch_m523307155 (GooglePlayTBM_t641131310 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::RegisterMatchUpdateListener()
extern "C"  void GooglePlayTBM_RegisterMatchUpdateListener_m3910042542 (GooglePlayTBM_t641131310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::UnregisterMatchUpdateListener()
extern "C"  void GooglePlayTBM_UnregisterMatchUpdateListener_m3090224763 (GooglePlayTBM_t641131310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::TakeTrun(System.String,System.Byte[],System.String,GP_ParticipantResult[])
extern "C"  void GooglePlayTBM_TakeTrun_m2238530396 (GooglePlayTBM_t641131310 * __this, String_t* ___matchId0, ByteU5BU5D_t3397334013* ___matchData1, String_t* ___pendingParticipantId2, GP_ParticipantResultU5BU5D_t353789473* ___results3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::StartSelectOpponentsView(System.Int32,System.Int32,System.Boolean)
extern "C"  void GooglePlayTBM_StartSelectOpponentsView_m2224996989 (GooglePlayTBM_t641131310 * __this, int32_t ___minPlayers0, int32_t ___maxPlayers1, bool ___allowAutomatch2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::ShowInbox()
extern "C"  void GooglePlayTBM_ShowInbox_m1440175226 (GooglePlayTBM_t641131310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::SetVariant(System.Int32)
extern "C"  void GooglePlayTBM_SetVariant_m3320894293 (GooglePlayTBM_t641131310 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::SetExclusiveBitMask(System.Int32)
extern "C"  void GooglePlayTBM_SetExclusiveBitMask_m817330187 (GooglePlayTBM_t641131310 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,GP_TBM_Match> GooglePlayTBM::get_Matches()
extern "C"  Dictionary_2_t3189857243 * GooglePlayTBM_get_Matches_m3802609198 (GooglePlayTBM_t641131310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::OnLoadMatchesResult(System.String)
extern "C"  void GooglePlayTBM_OnLoadMatchesResult_m1744001508 (GooglePlayTBM_t641131310 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::OnMatchInitiatedCallback(System.String)
extern "C"  void GooglePlayTBM_OnMatchInitiatedCallback_m1661027235 (GooglePlayTBM_t641131310 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::OnInvitationAcceptedCallback(System.String)
extern "C"  void GooglePlayTBM_OnInvitationAcceptedCallback_m768364335 (GooglePlayTBM_t641131310 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::OnCancelMatchResult(System.String)
extern "C"  void GooglePlayTBM_OnCancelMatchResult_m1320284054 (GooglePlayTBM_t641131310 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::OnLeaveMatchResult(System.String)
extern "C"  void GooglePlayTBM_OnLeaveMatchResult_m145143335 (GooglePlayTBM_t641131310 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::OnLoadMatchResult(System.String)
extern "C"  void GooglePlayTBM_OnLoadMatchResult_m3548730796 (GooglePlayTBM_t641131310 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::OnUpdateMatchResult(System.String)
extern "C"  void GooglePlayTBM_OnUpdateMatchResult_m183726701 (GooglePlayTBM_t641131310 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::AN_OnTurnResult(System.String)
extern "C"  void GooglePlayTBM_AN_OnTurnResult_m551620676 (GooglePlayTBM_t641131310 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::OnTurnBasedMatchRemoved(System.String)
extern "C"  void GooglePlayTBM_OnTurnBasedMatchRemoved_m2723667035 (GooglePlayTBM_t641131310 * __this, String_t* ___matchId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::OnTurnBasedMatchReceived(System.String)
extern "C"  void GooglePlayTBM_OnTurnBasedMatchReceived_m2086526394 (GooglePlayTBM_t641131310 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::OnMatchCreationCanceled(System.String)
extern "C"  void GooglePlayTBM_OnMatchCreationCanceled_m936177279 (GooglePlayTBM_t641131310 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_TBM_Match GooglePlayTBM::ParceMatchInfo(System.String)
extern "C"  GP_TBM_Match_t1275077981 * GooglePlayTBM_ParceMatchInfo_m583329143 (Il2CppObject * __this /* static, unused */, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_TBM_Match GooglePlayTBM::ParceMatchInfo(System.String[],System.Int32)
extern "C"  GP_TBM_Match_t1275077981 * GooglePlayTBM_ParceMatchInfo_m2528278770 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___MatchData0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::UpdateMatchInfo(GP_TBM_Match)
extern "C"  void GooglePlayTBM_UpdateMatchInfo_m1565825334 (GooglePlayTBM_t641131310 * __this, GP_TBM_Match_t1275077981 * ___match0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::<ActionMatchesResultLoaded>m__6E(GP_TBM_LoadMatchesResult)
extern "C"  void GooglePlayTBM_U3CActionMatchesResultLoadedU3Em__6E_m1473307634 (Il2CppObject * __this /* static, unused */, GP_TBM_LoadMatchesResult_t841773038 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::<ActionMatchInitiated>m__6F(GP_TBM_MatchInitiatedResult)
extern "C"  void GooglePlayTBM_U3CActionMatchInitiatedU3Em__6F_m3890545061 (Il2CppObject * __this /* static, unused */, GP_TBM_MatchInitiatedResult_t4144060847 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::<ActionMatchCanceled>m__70(GP_TBM_CancelMatchResult)
extern "C"  void GooglePlayTBM_U3CActionMatchCanceledU3Em__70_m3818692111 (Il2CppObject * __this /* static, unused */, GP_TBM_CancelMatchResult_t3879293152 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::<ActionMatchLeaved>m__71(GP_TBM_LeaveMatchResult)
extern "C"  void GooglePlayTBM_U3CActionMatchLeavedU3Em__71_m737314995 (Il2CppObject * __this /* static, unused */, GP_TBM_LeaveMatchResult_t3657803719 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::<ActionMatchDataLoaded>m__72(GP_TBM_LoadMatchResult)
extern "C"  void GooglePlayTBM_U3CActionMatchDataLoadedU3Em__72_m3811765185 (Il2CppObject * __this /* static, unused */, GP_TBM_LoadMatchResult_t1357418930 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::<ActionMatchUpdated>m__73(GP_TBM_UpdateMatchResult)
extern "C"  void GooglePlayTBM_U3CActionMatchUpdatedU3Em__73_m1524113429 (Il2CppObject * __this /* static, unused */, GP_TBM_UpdateMatchResult_t3943005969 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::<ActionMatchTurnFinished>m__74(GP_TBM_UpdateMatchResult)
extern "C"  void GooglePlayTBM_U3CActionMatchTurnFinishedU3Em__74_m1717925400 (Il2CppObject * __this /* static, unused */, GP_TBM_UpdateMatchResult_t3943005969 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::<ActionMatchReceived>m__75(GP_TBM_MatchReceivedResult)
extern "C"  void GooglePlayTBM_U3CActionMatchReceivedU3Em__75_m2730978139 (Il2CppObject * __this /* static, unused */, GP_TBM_MatchReceivedResult_t2394672915 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::<ActionMatchRemoved>m__76(GP_TBM_MatchRemovedResult)
extern "C"  void GooglePlayTBM_U3CActionMatchRemovedU3Em__76_m1443052980 (Il2CppObject * __this /* static, unused */, GP_TBM_MatchRemovedResult_t686355120 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::<ActionMatchCreationCanceled>m__77(AndroidActivityResult)
extern "C"  void GooglePlayTBM_U3CActionMatchCreationCanceledU3Em__77_m1355543532 (Il2CppObject * __this /* static, unused */, AndroidActivityResult_t3757510801 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::<ActionMatchInvitationAccepted>m__78(System.String,GP_TBM_MatchInitiatedResult)
extern "C"  void GooglePlayTBM_U3CActionMatchInvitationAcceptedU3Em__78_m2801269231 (Il2CppObject * __this /* static, unused */, String_t* p0, GP_TBM_MatchInitiatedResult_t4144060847 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayTBM::<ActionMatchInvitationDeclined>m__79(System.String)
extern "C"  void GooglePlayTBM_U3CActionMatchInvitationDeclinedU3Em__79_m1379170982 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
