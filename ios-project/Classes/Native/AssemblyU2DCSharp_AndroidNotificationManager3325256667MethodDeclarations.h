﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidNotificationManager
struct AndroidNotificationManager_t3325256667;
// System.String
struct String_t;
// AndroidNotificationBuilder
struct AndroidNotificationBuilder_t2822362133;
// System.Collections.Generic.List`1<LocalNotificationTemplate>
struct List_1_t2250011482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AndroidNotificationBuilder2822362133.h"

// System.Void AndroidNotificationManager::.ctor()
extern "C"  void AndroidNotificationManager__ctor_m2592806298 (AndroidNotificationManager_t3325256667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationManager::Awake()
extern "C"  void AndroidNotificationManager_Awake_m3222102343 (AndroidNotificationManager_t3325256667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationManager::LocadAppLaunchNotificationId()
extern "C"  void AndroidNotificationManager_LocadAppLaunchNotificationId_m1528057831 (AndroidNotificationManager_t3325256667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationManager::HideAllNotifications()
extern "C"  void AndroidNotificationManager_HideAllNotifications_m2869338553 (AndroidNotificationManager_t3325256667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationManager::ShowToastNotification(System.String)
extern "C"  void AndroidNotificationManager_ShowToastNotification_m1428045423 (AndroidNotificationManager_t3325256667 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationManager::ShowToastNotification(System.String,System.Int32)
extern "C"  void AndroidNotificationManager_ShowToastNotification_m3718206068 (AndroidNotificationManager_t3325256667 * __this, String_t* ___text0, int32_t ___duration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AndroidNotificationManager::ScheduleLocalNotification(System.String,System.String,System.Int32)
extern "C"  int32_t AndroidNotificationManager_ScheduleLocalNotification_m3093817746 (AndroidNotificationManager_t3325256667 * __this, String_t* ___title0, String_t* ___message1, int32_t ___seconds2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AndroidNotificationManager::ScheduleLocalNotification(AndroidNotificationBuilder)
extern "C"  int32_t AndroidNotificationManager_ScheduleLocalNotification_m3953171762 (AndroidNotificationManager_t3325256667 * __this, AndroidNotificationBuilder_t2822362133 * ___builder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationManager::CancelLocalNotification(System.Int32,System.Boolean)
extern "C"  void AndroidNotificationManager_CancelLocalNotification_m321872280 (AndroidNotificationManager_t3325256667 * __this, int32_t ___id0, bool ___clearFromPrefs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationManager::CancelAllLocalNotifications()
extern "C"  void AndroidNotificationManager_CancelAllLocalNotifications_m914584962 (AndroidNotificationManager_t3325256667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AndroidNotificationManager::get_GetNextId()
extern "C"  int32_t AndroidNotificationManager_get_GetNextId_m1273085045 (AndroidNotificationManager_t3325256667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationManager::OnNotificationIdLoadedEvent(System.String)
extern "C"  void AndroidNotificationManager_OnNotificationIdLoadedEvent_m3479053034 (AndroidNotificationManager_t3325256667 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationManager::SaveNotifications(System.Collections.Generic.List`1<LocalNotificationTemplate>)
extern "C"  void AndroidNotificationManager_SaveNotifications_m3207989539 (AndroidNotificationManager_t3325256667 * __this, List_1_t2250011482 * ___notifications0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<LocalNotificationTemplate> AndroidNotificationManager::LoadPendingNotifications(System.Boolean)
extern "C"  List_1_t2250011482 * AndroidNotificationManager_LoadPendingNotifications_m435168887 (AndroidNotificationManager_t3325256667 * __this, bool ___includeAll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationManager::<OnNotificationIdLoaded>m__25(System.Int32)
extern "C"  void AndroidNotificationManager_U3COnNotificationIdLoadedU3Em__25_m2166543601 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
