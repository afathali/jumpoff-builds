﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FB_LikeInfo
struct FB_LikeInfo_t3213199078;

#include "codegen/il2cpp-codegen.h"

// System.Void FB_LikeInfo::.ctor()
extern "C"  void FB_LikeInfo__ctor_m3734850533 (FB_LikeInfo_t3213199078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
