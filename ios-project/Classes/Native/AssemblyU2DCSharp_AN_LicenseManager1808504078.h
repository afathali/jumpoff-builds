﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<AN_LicenseRequestResult>
struct Action_1_t2133169943;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3099347996.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AN_LicenseManager
struct  AN_LicenseManager_t1808504078  : public SA_Singleton_OLD_1_t3099347996
{
public:

public:
};

struct AN_LicenseManager_t1808504078_StaticFields
{
public:
	// System.Action`1<AN_LicenseRequestResult> AN_LicenseManager::OnLicenseRequestResult
	Action_1_t2133169943 * ___OnLicenseRequestResult_4;
	// System.Action`1<AN_LicenseRequestResult> AN_LicenseManager::<>f__am$cache1
	Action_1_t2133169943 * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_OnLicenseRequestResult_4() { return static_cast<int32_t>(offsetof(AN_LicenseManager_t1808504078_StaticFields, ___OnLicenseRequestResult_4)); }
	inline Action_1_t2133169943 * get_OnLicenseRequestResult_4() const { return ___OnLicenseRequestResult_4; }
	inline Action_1_t2133169943 ** get_address_of_OnLicenseRequestResult_4() { return &___OnLicenseRequestResult_4; }
	inline void set_OnLicenseRequestResult_4(Action_1_t2133169943 * value)
	{
		___OnLicenseRequestResult_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnLicenseRequestResult_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(AN_LicenseManager_t1808504078_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Action_1_t2133169943 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Action_1_t2133169943 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Action_1_t2133169943 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
