﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GoogleProductTemplate>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2228543437(__this, ___l0, method) ((  void (*) (Enumerator_t16467130 *, List_1_t481737456 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GoogleProductTemplate>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3854284637(__this, method) ((  void (*) (Enumerator_t16467130 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GoogleProductTemplate>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1015448869(__this, method) ((  Il2CppObject * (*) (Enumerator_t16467130 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GoogleProductTemplate>::Dispose()
#define Enumerator_Dispose_m2998104272(__this, method) ((  void (*) (Enumerator_t16467130 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GoogleProductTemplate>::VerifyState()
#define Enumerator_VerifyState_m869640919(__this, method) ((  void (*) (Enumerator_t16467130 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GoogleProductTemplate>::MoveNext()
#define Enumerator_MoveNext_m1565051720(__this, method) ((  bool (*) (Enumerator_t16467130 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GoogleProductTemplate>::get_Current()
#define Enumerator_get_Current_m2795500714(__this, method) ((  GoogleProductTemplate_t1112616324 * (*) (Enumerator_t16467130 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
