﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlusButtonsAPIExample
struct PlusButtonsAPIExample_t2847602409;

#include "codegen/il2cpp-codegen.h"

// System.Void PlusButtonsAPIExample::.ctor()
extern "C"  void PlusButtonsAPIExample__ctor_m4085747918 (PlusButtonsAPIExample_t2847602409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlusButtonsAPIExample::CreatePlusButtons()
extern "C"  void PlusButtonsAPIExample_CreatePlusButtons_m2405004855 (PlusButtonsAPIExample_t2847602409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlusButtonsAPIExample::HideButtons()
extern "C"  void PlusButtonsAPIExample_HideButtons_m3259670897 (PlusButtonsAPIExample_t2847602409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlusButtonsAPIExample::ShoweButtons()
extern "C"  void PlusButtonsAPIExample_ShoweButtons_m4069518855 (PlusButtonsAPIExample_t2847602409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlusButtonsAPIExample::CreateRandomPostButton()
extern "C"  void PlusButtonsAPIExample_CreateRandomPostButton_m4185440749 (PlusButtonsAPIExample_t2847602409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlusButtonsAPIExample::ChangePosPostButton()
extern "C"  void PlusButtonsAPIExample_ChangePosPostButton_m180978618 (PlusButtonsAPIExample_t2847602409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlusButtonsAPIExample::ButtonClicked()
extern "C"  void PlusButtonsAPIExample_ButtonClicked_m237433465 (PlusButtonsAPIExample_t2847602409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlusButtonsAPIExample::OnDestroy()
extern "C"  void PlusButtonsAPIExample_OnDestroy_m3049185527 (PlusButtonsAPIExample_t2847602409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
