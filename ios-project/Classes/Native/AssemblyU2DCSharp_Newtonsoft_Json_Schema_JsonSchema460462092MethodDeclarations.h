﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema
struct TypeSchema_t460462092;
// System.Type
struct Type_t;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t3772113849;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3772113849.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema::.ctor(System.Type,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void TypeSchema__ctor_m2463170637 (TypeSchema_t460462092 * __this, Type_t * ___type0, JsonSchema_t3772113849 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema::get_Type()
extern "C"  Type_t * TypeSchema_get_Type_m2093571640 (TypeSchema_t460462092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema::set_Type(System.Type)
extern "C"  void TypeSchema_set_Type_m3388245977 (TypeSchema_t460462092 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema::get_Schema()
extern "C"  JsonSchema_t3772113849 * TypeSchema_get_Schema_m3932475209 (TypeSchema_t460462092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema::set_Schema(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void TypeSchema_set_Schema_m185458202 (TypeSchema_t460462092 * __this, JsonSchema_t3772113849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
