﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ANNativeEventsExample
struct ANNativeEventsExample_t3453490363;
// AndroidActivityResult
struct AndroidActivityResult_t3757510801;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AndroidActivityResult3757510801.h"

// System.Void ANNativeEventsExample::.ctor()
extern "C"  void ANNativeEventsExample__ctor_m134288484 (ANNativeEventsExample_t3453490363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ANNativeEventsExample::Start()
extern "C"  void ANNativeEventsExample_Start_m1424707328 (ANNativeEventsExample_t3453490363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ANNativeEventsExample::OnStop()
extern "C"  void ANNativeEventsExample_OnStop_m1728690319 (ANNativeEventsExample_t3453490363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ANNativeEventsExample::OnStart()
extern "C"  void ANNativeEventsExample_OnStart_m2596070619 (ANNativeEventsExample_t3453490363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ANNativeEventsExample::OnNewIntent()
extern "C"  void ANNativeEventsExample_OnNewIntent_m973443467 (ANNativeEventsExample_t3453490363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ANNativeEventsExample::OnActivityResult(AndroidActivityResult)
extern "C"  void ANNativeEventsExample_OnActivityResult_m2554233138 (ANNativeEventsExample_t3453490363 * __this, AndroidActivityResult_t3757510801 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
