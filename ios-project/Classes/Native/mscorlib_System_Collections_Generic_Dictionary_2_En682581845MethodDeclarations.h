﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2654830007MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4119682302(__this, ___dictionary0, method) ((  void (*) (Enumerator_t682581845 *, Dictionary_2_t3657524439 *, const MethodInfo*))Enumerator__ctor_m3888930488_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m447772737(__this, method) ((  Il2CppObject * (*) (Enumerator_t682581845 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m649871021_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4161708841(__this, method) ((  void (*) (Enumerator_t682581845 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m593851829_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2187139676(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t682581845 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m218337734_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2368673523(__this, method) ((  Il2CppObject * (*) (Enumerator_t682581845 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2021025471_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2873208811(__this, method) ((  Il2CppObject * (*) (Enumerator_t682581845 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2671660943_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
#define Enumerator_MoveNext_m2444262841(__this, method) ((  bool (*) (Enumerator_t682581845 *, const MethodInfo*))Enumerator_MoveNext_m657838813_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::get_Current()
#define Enumerator_get_Current_m4116956669(__this, method) ((  KeyValuePair_2_t1414869661  (*) (Enumerator_t682581845 *, const MethodInfo*))Enumerator_get_Current_m954143681_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2706321358(__this, method) ((  String_t* (*) (Enumerator_t682581845 *, const MethodInfo*))Enumerator_get_CurrentKey_m1574894968_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2219163918(__this, method) ((  int32_t (*) (Enumerator_t682581845 *, const MethodInfo*))Enumerator_get_CurrentValue_m1369392376_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::Reset()
#define Enumerator_Reset_m3908591332(__this, method) ((  void (*) (Enumerator_t682581845 *, const MethodInfo*))Enumerator_Reset_m367248862_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::VerifyState()
#define Enumerator_VerifyState_m2862518939(__this, method) ((  void (*) (Enumerator_t682581845 *, const MethodInfo*))Enumerator_VerifyState_m1102849887_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m126506009(__this, method) ((  void (*) (Enumerator_t682581845 *, const MethodInfo*))Enumerator_VerifyCurrent_m231503237_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::Dispose()
#define Enumerator_Dispose_m50037230(__this, method) ((  void (*) (Enumerator_t682581845 *, const MethodInfo*))Enumerator_Dispose_m3267139496_gshared)(__this, method)
