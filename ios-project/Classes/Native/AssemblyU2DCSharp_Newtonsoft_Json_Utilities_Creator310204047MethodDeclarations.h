﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.Creator`1<System.Object>
struct Creator_1_t310204047;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Newtonsoft.Json.Utilities.Creator`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Creator_1__ctor_m3589561469_gshared (Creator_1_t310204047 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Creator_1__ctor_m3589561469(__this, ___object0, ___method1, method) ((  void (*) (Creator_1_t310204047 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Creator_1__ctor_m3589561469_gshared)(__this, ___object0, ___method1, method)
// T Newtonsoft.Json.Utilities.Creator`1<System.Object>::Invoke()
extern "C"  Il2CppObject * Creator_1_Invoke_m1400968972_gshared (Creator_1_t310204047 * __this, const MethodInfo* method);
#define Creator_1_Invoke_m1400968972(__this, method) ((  Il2CppObject * (*) (Creator_1_t310204047 *, const MethodInfo*))Creator_1_Invoke_m1400968972_gshared)(__this, method)
// System.IAsyncResult Newtonsoft.Json.Utilities.Creator`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Creator_1_BeginInvoke_m1842762196_gshared (Creator_1_t310204047 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method);
#define Creator_1_BeginInvoke_m1842762196(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (Creator_1_t310204047 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Creator_1_BeginInvoke_m1842762196_gshared)(__this, ___callback0, ___object1, method)
// T Newtonsoft.Json.Utilities.Creator`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Creator_1_EndInvoke_m3159317452_gshared (Creator_1_t310204047 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Creator_1_EndInvoke_m3159317452(__this, ___result0, method) ((  Il2CppObject * (*) (Creator_1_t310204047 *, Il2CppObject *, const MethodInfo*))Creator_1_EndInvoke_m3159317452_gshared)(__this, ___result0, method)
