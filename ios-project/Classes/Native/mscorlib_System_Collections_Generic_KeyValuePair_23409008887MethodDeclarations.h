﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23409008887.h"
#include "AssemblyU2DCSharp_AN_ManifestPermission1772110202.h"
#include "AssemblyU2DCSharp_AN_PermissionState838201224.h"

// System.Void System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3420454981_gshared (KeyValuePair_2_t3409008887 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3420454981(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3409008887 *, int32_t, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m3420454981_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m232446462_gshared (KeyValuePair_2_t3409008887 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m232446462(__this, method) ((  int32_t (*) (KeyValuePair_2_t3409008887 *, const MethodInfo*))KeyValuePair_2_get_Key_m232446462_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1214669006_gshared (KeyValuePair_2_t3409008887 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1214669006(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3409008887 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1214669006_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1557526483_gshared (KeyValuePair_2_t3409008887 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1557526483(__this, method) ((  int32_t (*) (KeyValuePair_2_t3409008887 *, const MethodInfo*))KeyValuePair_2_get_Value_m1557526483_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2649441830_gshared (KeyValuePair_2_t3409008887 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2649441830(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3409008887 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m2649441830_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2588186900_gshared (KeyValuePair_2_t3409008887 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2588186900(__this, method) ((  String_t* (*) (KeyValuePair_2_t3409008887 *, const MethodInfo*))KeyValuePair_2_ToString_m2588186900_gshared)(__this, method)
