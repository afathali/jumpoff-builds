﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TwitterEvents
struct TwitterEvents_t3891486642;

#include "codegen/il2cpp-codegen.h"

// System.Void TwitterEvents::.ctor()
extern "C"  void TwitterEvents__ctor_m1674942785 (TwitterEvents_t3891486642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
