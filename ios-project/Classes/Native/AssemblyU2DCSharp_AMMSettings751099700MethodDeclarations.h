﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AMMSettings
struct AMMSettings_t751099700;

#include "codegen/il2cpp-codegen.h"

// System.Void AMMSettings::.ctor()
extern "C"  void AMMSettings__ctor_m3978085995 (AMMSettings_t751099700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
