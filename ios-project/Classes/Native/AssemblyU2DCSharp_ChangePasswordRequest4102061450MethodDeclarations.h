﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangePasswordRequest
struct ChangePasswordRequest_t4102061450;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChangePasswordRequest::.ctor()
extern "C"  void ChangePasswordRequest__ctor_m2556520819 (ChangePasswordRequest_t4102061450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChangePasswordRequest::get_Type()
extern "C"  String_t* ChangePasswordRequest_get_Type_m1904835647 (ChangePasswordRequest_t4102061450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangePasswordRequest::set_Type(System.String)
extern "C"  void ChangePasswordRequest_set_Type_m3523901566 (ChangePasswordRequest_t4102061450 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ChangePasswordRequest::get_UserId()
extern "C"  int64_t ChangePasswordRequest_get_UserId_m3050861531 (ChangePasswordRequest_t4102061450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangePasswordRequest::set_UserId(System.Int64)
extern "C"  void ChangePasswordRequest_set_UserId_m4158423882 (ChangePasswordRequest_t4102061450 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChangePasswordRequest::get_NewPassword()
extern "C"  String_t* ChangePasswordRequest_get_NewPassword_m3166242780 (ChangePasswordRequest_t4102061450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangePasswordRequest::set_NewPassword(System.String)
extern "C"  void ChangePasswordRequest_set_NewPassword_m3989458251 (ChangePasswordRequest_t4102061450 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
