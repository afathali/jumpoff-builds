﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Cac666803094MethodDeclarations.h"

// System.Void Newtonsoft.Json.Serialization.CachedAttributeGetter`1<Newtonsoft.Json.JsonContainerAttribute>::.cctor()
#define CachedAttributeGetter_1__cctor_m2429507449(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))CachedAttributeGetter_1__cctor_m1655531471_gshared)(__this /* static, unused */, method)
// T Newtonsoft.Json.Serialization.CachedAttributeGetter`1<Newtonsoft.Json.JsonContainerAttribute>::GetAttribute(System.Reflection.ICustomAttributeProvider)
#define CachedAttributeGetter_1_GetAttribute_m2943353614(__this /* static, unused */, ___type0, method) ((  JsonContainerAttribute_t47210975 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))CachedAttributeGetter_1_GetAttribute_m2147397194_gshared)(__this /* static, unused */, ___type0, method)
