﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_TBM_MatchReceivedResult
struct GP_TBM_MatchReceivedResult_t2394672915;
// GP_TBM_Match
struct GP_TBM_Match_t1275077981;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GP_TBM_Match1275077981.h"

// System.Void GP_TBM_MatchReceivedResult::.ctor(GP_TBM_Match)
extern "C"  void GP_TBM_MatchReceivedResult__ctor_m1570835745 (GP_TBM_MatchReceivedResult_t2394672915 * __this, GP_TBM_Match_t1275077981 * ___match0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_TBM_Match GP_TBM_MatchReceivedResult::get_Match()
extern "C"  GP_TBM_Match_t1275077981 * GP_TBM_MatchReceivedResult_get_Match_m3164317324 (GP_TBM_MatchReceivedResult_t2394672915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
