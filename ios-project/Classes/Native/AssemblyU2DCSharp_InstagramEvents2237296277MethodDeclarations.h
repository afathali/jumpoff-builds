﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InstagramEvents
struct InstagramEvents_t2237296277;

#include "codegen/il2cpp-codegen.h"

// System.Void InstagramEvents::.ctor()
extern "C"  void InstagramEvents__ctor_m578154048 (InstagramEvents_t2237296277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
