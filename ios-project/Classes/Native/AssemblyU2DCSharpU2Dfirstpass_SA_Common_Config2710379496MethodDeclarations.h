﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Config
struct Config_t2710379496;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.Common.Config::.ctor()
extern "C"  void Config__ctor_m2495537642 (Config_t2710379496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA.Common.Config::get_FB_SDK_VersionCode()
extern "C"  String_t* Config_get_FB_SDK_VersionCode_m4043921851 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SA.Common.Config::get_FB_SDK_MajorVersionCode()
extern "C"  int32_t Config_get_FB_SDK_MajorVersionCode_m3542752943 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
