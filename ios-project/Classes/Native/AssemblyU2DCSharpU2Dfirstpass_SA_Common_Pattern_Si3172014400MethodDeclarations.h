﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Pattern.Singleton`1<System.Object>
struct Singleton_1_t3172014400;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SA.Common.Pattern.Singleton`1<System.Object>::.ctor()
extern "C"  void Singleton_1__ctor_m4152044218_gshared (Singleton_1_t3172014400 * __this, const MethodInfo* method);
#define Singleton_1__ctor_m4152044218(__this, method) ((  void (*) (Singleton_1_t3172014400 *, const MethodInfo*))Singleton_1__ctor_m4152044218_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<System.Object>::.cctor()
extern "C"  void Singleton_1__cctor_m982417053_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Singleton_1__cctor_m982417053(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1__cctor_m982417053_gshared)(__this /* static, unused */, method)
// T SA.Common.Pattern.Singleton`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * Singleton_1_get_Instance_m3228489301_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Singleton_1_get_Instance_m3228489301(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m3228489301_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<System.Object>::get_HasInstance()
extern "C"  bool Singleton_1_get_HasInstance_m2551508260_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Singleton_1_get_HasInstance_m2551508260(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_HasInstance_m2551508260_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<System.Object>::get_IsDestroyed()
extern "C"  bool Singleton_1_get_IsDestroyed_m4170993228_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Singleton_1_get_IsDestroyed_m4170993228(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_IsDestroyed_m4170993228_gshared)(__this /* static, unused */, method)
// System.Void SA.Common.Pattern.Singleton`1<System.Object>::OnDestroy()
extern "C"  void Singleton_1_OnDestroy_m3790554761_gshared (Singleton_1_t3172014400 * __this, const MethodInfo* method);
#define Singleton_1_OnDestroy_m3790554761(__this, method) ((  void (*) (Singleton_1_t3172014400 *, const MethodInfo*))Singleton_1_OnDestroy_m3790554761_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<System.Object>::OnApplicationQuit()
extern "C"  void Singleton_1_OnApplicationQuit_m1956476828_gshared (Singleton_1_t3172014400 * __this, const MethodInfo* method);
#define Singleton_1_OnApplicationQuit_m1956476828(__this, method) ((  void (*) (Singleton_1_t3172014400 *, const MethodInfo*))Singleton_1_OnApplicationQuit_m1956476828_gshared)(__this, method)
