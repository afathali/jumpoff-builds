﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_GoogleAnalyticsProxy
struct AN_GoogleAnalyticsProxy_t595315245;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_GoogleAnalyticsProxy::.ctor()
extern "C"  void AN_GoogleAnalyticsProxy__ctor_m3469471974 (AN_GoogleAnalyticsProxy_t595315245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_GoogleAnalyticsProxy_CallActivityFunction_m340414991 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::StartAnalyticsTracking()
extern "C"  void AN_GoogleAnalyticsProxy_StartAnalyticsTracking_m3284833149 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::SetTrackerID(System.String)
extern "C"  void AN_GoogleAnalyticsProxy_SetTrackerID_m504420109 (Il2CppObject * __this /* static, unused */, String_t* ___trackingID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::SendView()
extern "C"  void AN_GoogleAnalyticsProxy_SendView_m3066319769 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::SendView(System.String)
extern "C"  void AN_GoogleAnalyticsProxy_SendView_m4124366263 (Il2CppObject * __this /* static, unused */, String_t* ___appScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::SendEvent(System.String,System.String,System.String,System.String)
extern "C"  void AN_GoogleAnalyticsProxy_SendEvent_m3774287214 (Il2CppObject * __this /* static, unused */, String_t* ___category0, String_t* ___action1, String_t* ___label2, String_t* ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::SendEvent(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AN_GoogleAnalyticsProxy_SendEvent_m440594478 (Il2CppObject * __this /* static, unused */, String_t* ___category0, String_t* ___action1, String_t* ___label2, String_t* ___value3, String_t* ___key4, String_t* ___val5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::SendTiming(System.String,System.String,System.String,System.String)
extern "C"  void AN_GoogleAnalyticsProxy_SendTiming_m2274224782 (Il2CppObject * __this /* static, unused */, String_t* ___category0, String_t* ___intervalInMilliseconds1, String_t* ___name2, String_t* ___label3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::CreateTransaction(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AN_GoogleAnalyticsProxy_CreateTransaction_m4184880216 (Il2CppObject * __this /* static, unused */, String_t* ___transactionId0, String_t* ___affiliation1, String_t* ___revenue2, String_t* ___tax3, String_t* ___shipping4, String_t* ___currencyCode5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::CreateItem(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AN_GoogleAnalyticsProxy_CreateItem_m2794299575 (Il2CppObject * __this /* static, unused */, String_t* ___transactionId0, String_t* ___name1, String_t* ___sku2, String_t* ___category3, String_t* ___price4, String_t* ___quantity5, String_t* ___currencyCode6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::SetKey(System.String,System.String)
extern "C"  void AN_GoogleAnalyticsProxy_SetKey_m3217084949 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::ClearKey(System.String)
extern "C"  void AN_GoogleAnalyticsProxy_ClearKey_m2250415726 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::SetLogLevel(System.Int32)
extern "C"  void AN_GoogleAnalyticsProxy_SetLogLevel_m3145007321 (Il2CppObject * __this /* static, unused */, int32_t ___lvl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::SetDryRun(System.String)
extern "C"  void AN_GoogleAnalyticsProxy_SetDryRun_m3139639778 (Il2CppObject * __this /* static, unused */, String_t* ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAnalyticsProxy::EnableAdvertisingIdCollection(System.String)
extern "C"  void AN_GoogleAnalyticsProxy_EnableAdvertisingIdCollection_m40729558 (Il2CppObject * __this /* static, unused */, String_t* ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
