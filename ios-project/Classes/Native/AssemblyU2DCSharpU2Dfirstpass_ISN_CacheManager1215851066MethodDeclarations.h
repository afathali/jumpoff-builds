﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_CacheManager
struct ISN_CacheManager_t1215851066;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ISN_CacheManager::.ctor()
extern "C"  void ISN_CacheManager__ctor_m3170839825 (ISN_CacheManager_t1215851066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CacheManager::SaveAchievementRequest(System.String,System.Single)
extern "C"  void ISN_CacheManager_SaveAchievementRequest_m3888608577 (Il2CppObject * __this /* static, unused */, String_t* ___achievementId0, float ___percent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CacheManager::SendAchievementCachedRequest()
extern "C"  void ISN_CacheManager_SendAchievementCachedRequest_m501787491 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CacheManager::Clear()
extern "C"  void ISN_CacheManager_Clear_m2200162394 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_CacheManager::get_SavedData()
extern "C"  String_t* ISN_CacheManager_get_SavedData_m347637708 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CacheManager::set_SavedData(System.String)
extern "C"  void ISN_CacheManager_set_SavedData_m3232194607 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
