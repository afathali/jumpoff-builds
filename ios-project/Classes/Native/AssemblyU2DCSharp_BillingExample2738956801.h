﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DefaultPreviewButton
struct DefaultPreviewButton_t12674677;
// DefaultPreviewButton[]
struct DefaultPreviewButtonU5BU5D_t3800739864;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BillingExample
struct  BillingExample_t2738956801  : public MonoBehaviour_t1158329972
{
public:
	// DefaultPreviewButton BillingExample::initButton
	DefaultPreviewButton_t12674677 * ___initButton_2;
	// DefaultPreviewButton[] BillingExample::initBoundButtons
	DefaultPreviewButtonU5BU5D_t3800739864* ___initBoundButtons_3;

public:
	inline static int32_t get_offset_of_initButton_2() { return static_cast<int32_t>(offsetof(BillingExample_t2738956801, ___initButton_2)); }
	inline DefaultPreviewButton_t12674677 * get_initButton_2() const { return ___initButton_2; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_initButton_2() { return &___initButton_2; }
	inline void set_initButton_2(DefaultPreviewButton_t12674677 * value)
	{
		___initButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___initButton_2, value);
	}

	inline static int32_t get_offset_of_initBoundButtons_3() { return static_cast<int32_t>(offsetof(BillingExample_t2738956801, ___initBoundButtons_3)); }
	inline DefaultPreviewButtonU5BU5D_t3800739864* get_initBoundButtons_3() const { return ___initBoundButtons_3; }
	inline DefaultPreviewButtonU5BU5D_t3800739864** get_address_of_initBoundButtons_3() { return &___initBoundButtons_3; }
	inline void set_initBoundButtons_3(DefaultPreviewButtonU5BU5D_t3800739864* value)
	{
		___initBoundButtons_3 = value;
		Il2CppCodeGenWriteBarrier(&___initBoundButtons_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
