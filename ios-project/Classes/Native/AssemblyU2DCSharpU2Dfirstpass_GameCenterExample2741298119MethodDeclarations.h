﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameCenterExample
struct GameCenterExample_t2741298119;
// SA.Common.Models.Result
struct Result_t4287219743;
// ISN_LoadSetLeaderboardsInfoResult
struct ISN_LoadSetLeaderboardsInfoResult_t3997789804;
// GK_AchievementProgressResult
struct GK_AchievementProgressResult_t3539574352;
// GK_LeaderboardResult
struct GK_LeaderboardResult_t866080833;
// GK_PlayerSignatureResult
struct GK_PlayerSignatureResult_t13769479;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_LoadSetLeaderboa3997789804.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_AchievementProgre3539574352.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_LeaderboardResult866080833.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_PlayerSignatureResu13769479.h"

// System.Void GameCenterExample::.ctor()
extern "C"  void GameCenterExample__ctor_m4062996344 (GameCenterExample_t2741298119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::.cctor()
extern "C"  void GameCenterExample__cctor_m873598515 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::Awake()
extern "C"  void GameCenterExample_Awake_m884600457 (GameCenterExample_t2741298119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnGUI()
extern "C"  void GameCenterExample_OnGUI_m656074128 (GameCenterExample_t2741298119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnAchievementsLoaded(SA.Common.Models.Result)
extern "C"  void GameCenterExample_OnAchievementsLoaded_m3396920988 (GameCenterExample_t2741298119 * __this, Result_t4287219743 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnLeaderboardSetsInfoLoaded(SA.Common.Models.Result)
extern "C"  void GameCenterExample_OnLeaderboardSetsInfoLoaded_m3587174890 (GameCenterExample_t2741298119 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnLoaderboardsInfoLoaded(ISN_LoadSetLeaderboardsInfoResult)
extern "C"  void GameCenterExample_OnLoaderboardsInfoLoaded_m1818268186 (GameCenterExample_t2741298119 * __this, ISN_LoadSetLeaderboardsInfoResult_t3997789804 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::HandleOnAchievementsReset(SA.Common.Models.Result)
extern "C"  void GameCenterExample_HandleOnAchievementsReset_m213053404 (GameCenterExample_t2741298119 * __this, Result_t4287219743 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::HandleOnAchievementsProgress(GK_AchievementProgressResult)
extern "C"  void GameCenterExample_HandleOnAchievementsProgress_m2062659586 (GameCenterExample_t2741298119 * __this, GK_AchievementProgressResult_t3539574352 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnScoreSubmitted(GK_LeaderboardResult)
extern "C"  void GameCenterExample_OnScoreSubmitted_m2879601587 (GameCenterExample_t2741298119 * __this, GK_LeaderboardResult_t866080833 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnLeadrboardInfoLoaded(GK_LeaderboardResult)
extern "C"  void GameCenterExample_OnLeadrboardInfoLoaded_m285015811 (GameCenterExample_t2741298119 * __this, GK_LeaderboardResult_t866080833 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnScoreSubmitted(SA.Common.Models.Result)
extern "C"  void GameCenterExample_OnScoreSubmitted_m2492707366 (GameCenterExample_t2741298119 * __this, Result_t4287219743 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnAuthFinished(SA.Common.Models.Result)
extern "C"  void GameCenterExample_OnAuthFinished_m1936224023 (GameCenterExample_t2741298119 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterExample::OnPlayerSignatureRetrieveResult(GK_PlayerSignatureResult)
extern "C"  void GameCenterExample_OnPlayerSignatureRetrieveResult_m3207928356 (GameCenterExample_t2741298119 * __this, GK_PlayerSignatureResult_t13769479 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
