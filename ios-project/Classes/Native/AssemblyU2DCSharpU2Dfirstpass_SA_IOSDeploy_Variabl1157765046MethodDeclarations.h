﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.IOSDeploy.Variable
struct Variable_t1157765046;
// SA.IOSDeploy.VariableListed
struct VariableListed_t1912381035;
// System.Collections.Generic.Dictionary`2<System.String,SA.IOSDeploy.VariableListed>
struct Dictionary_2_t3827160297;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_Variabl1912381035.h"

// System.Void SA.IOSDeploy.Variable::.ctor()
extern "C"  void Variable__ctor_m1260310667 (Variable_t1157765046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSDeploy.Variable::AddVarToDictionary(SA.IOSDeploy.VariableListed)
extern "C"  void Variable_AddVarToDictionary_m3256281913 (Variable_t1157765046 * __this, VariableListed_t1912381035 * ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,SA.IOSDeploy.VariableListed> SA.IOSDeploy.Variable::get_DictionaryValue()
extern "C"  Dictionary_2_t3827160297 * Variable_get_DictionaryValue_m3058259748 (Variable_t1157765046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
