﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNativeUtility
struct IOSNativeUtility_t933355194;
// System.Action`1<ISN_Locale>
struct Action_1_t1964687467;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.String
struct String_t;
// ISN_Locale
struct ISN_Locale_t2162888085;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_Locale2162888085.h"

// System.Void IOSNativeUtility::.ctor()
extern "C"  void IOSNativeUtility__ctor_m711912209 (IOSNativeUtility_t933355194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::.cctor()
extern "C"  void IOSNativeUtility__cctor_m3056052670 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::add_OnLocaleLoaded(System.Action`1<ISN_Locale>)
extern "C"  void IOSNativeUtility_add_OnLocaleLoaded_m2749773286 (Il2CppObject * __this /* static, unused */, Action_1_t1964687467 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::remove_OnLocaleLoaded(System.Action`1<ISN_Locale>)
extern "C"  void IOSNativeUtility_remove_OnLocaleLoaded_m2542560151 (Il2CppObject * __this /* static, unused */, Action_1_t1964687467 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::add_GuidedAccessSessionRequestResult(System.Action`1<System.Boolean>)
extern "C"  void IOSNativeUtility_add_GuidedAccessSessionRequestResult_m2983397664 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::remove_GuidedAccessSessionRequestResult(System.Action`1<System.Boolean>)
extern "C"  void IOSNativeUtility_remove_GuidedAccessSessionRequestResult_m582916029 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::_ISN_RedirectToAppStoreRatingPage(System.String)
extern "C"  void IOSNativeUtility__ISN_RedirectToAppStoreRatingPage_m1524824688 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::_ISN_ShowPreloader()
extern "C"  void IOSNativeUtility__ISN_ShowPreloader_m1982641464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::_ISN_HidePreloader()
extern "C"  void IOSNativeUtility__ISN_HidePreloader_m2429978395 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::_ISN_SetApplicationBagesNumber(System.Int32)
extern "C"  void IOSNativeUtility__ISN_SetApplicationBagesNumber_m2544085489 (Il2CppObject * __this /* static, unused */, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::_ISN_GetLocale()
extern "C"  void IOSNativeUtility__ISN_GetLocale_m1572391603 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSNativeUtility::_ISN_IsGuidedAccessEnabled()
extern "C"  bool IOSNativeUtility__ISN_IsGuidedAccessEnabled_m408849028 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSNativeUtility::_ISN_IsRunningTestFlightBeta()
extern "C"  bool IOSNativeUtility__ISN_IsRunningTestFlightBeta_m3991549092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::_ISN_RequestGuidedAccessSession(System.Boolean)
extern "C"  void IOSNativeUtility__ISN_RequestGuidedAccessSession_m875286111 (Il2CppObject * __this /* static, unused */, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::Awake()
extern "C"  void IOSNativeUtility_Awake_m84460204 (IOSNativeUtility_t933355194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::GetLocale()
extern "C"  void IOSNativeUtility_GetLocale_m2456392447 (IOSNativeUtility_t933355194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::RedirectToAppStoreRatingPage()
extern "C"  void IOSNativeUtility_RedirectToAppStoreRatingPage_m4203257992 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::RedirectToAppStoreRatingPage(System.String)
extern "C"  void IOSNativeUtility_RedirectToAppStoreRatingPage_m3311935958 (Il2CppObject * __this /* static, unused */, String_t* ___appleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::SetApplicationBagesNumber(System.Int32)
extern "C"  void IOSNativeUtility_SetApplicationBagesNumber_m949802441 (Il2CppObject * __this /* static, unused */, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::ShowPreloader()
extern "C"  void IOSNativeUtility_ShowPreloader_m3617605270 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::HidePreloader()
extern "C"  void IOSNativeUtility_HidePreloader_m3441618279 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::RequestGuidedAccessSession(System.Boolean)
extern "C"  void IOSNativeUtility_RequestGuidedAccessSession_m3858918595 (IOSNativeUtility_t933355194 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSNativeUtility::get_IsGuidedAccessEnabled()
extern "C"  bool IOSNativeUtility_get_IsGuidedAccessEnabled_m4294276889 (IOSNativeUtility_t933355194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSNativeUtility::get_IsRunningTestFlightBeta()
extern "C"  bool IOSNativeUtility_get_IsRunningTestFlightBeta_m1922388545 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::OnGuidedAccessSessionRequestResult(System.String)
extern "C"  void IOSNativeUtility_OnGuidedAccessSessionRequestResult_m75140314 (IOSNativeUtility_t933355194 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::OnLocaleLoadedHandler(System.String)
extern "C"  void IOSNativeUtility_OnLocaleLoadedHandler_m3328373469 (IOSNativeUtility_t933355194 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::<OnLocaleLoaded>m__6A(ISN_Locale)
extern "C"  void IOSNativeUtility_U3COnLocaleLoadedU3Em__6A_m610762864 (Il2CppObject * __this /* static, unused */, ISN_Locale_t2162888085 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativeUtility::<GuidedAccessSessionRequestResult>m__6B(System.Boolean)
extern "C"  void IOSNativeUtility_U3CGuidedAccessSessionRequestResultU3Em__6B_m569642009 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
