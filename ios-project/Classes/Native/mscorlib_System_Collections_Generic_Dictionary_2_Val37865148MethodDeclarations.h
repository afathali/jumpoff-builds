﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>
struct ValueCollection_t37865148;
// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>
struct Dictionary_2_t1334805305;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct IEnumerator_1_t3513236300;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Schema.JsonSchemaType[]
struct JsonSchemaTypeU5BU5D_t3104176164;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3021338069.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1262928073_gshared (ValueCollection_t37865148 * __this, Dictionary_2_t1334805305 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1262928073(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t37865148 *, Dictionary_2_t1334805305 *, const MethodInfo*))ValueCollection__ctor_m1262928073_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2783403239_gshared (ValueCollection_t37865148 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2783403239(__this, ___item0, method) ((  void (*) (ValueCollection_t37865148 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2783403239_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m626751804_gshared (ValueCollection_t37865148 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m626751804(__this, method) ((  void (*) (ValueCollection_t37865148 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m626751804_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3167739569_gshared (ValueCollection_t37865148 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3167739569(__this, ___item0, method) ((  bool (*) (ValueCollection_t37865148 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3167739569_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1046023934_gshared (ValueCollection_t37865148 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1046023934(__this, ___item0, method) ((  bool (*) (ValueCollection_t37865148 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1046023934_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4248864720_gshared (ValueCollection_t37865148 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4248864720(__this, method) ((  Il2CppObject* (*) (ValueCollection_t37865148 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4248864720_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m474026232_gshared (ValueCollection_t37865148 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m474026232(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t37865148 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m474026232_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2991262103_gshared (ValueCollection_t37865148 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2991262103(__this, method) ((  Il2CppObject * (*) (ValueCollection_t37865148 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2991262103_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2005238466_gshared (ValueCollection_t37865148 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2005238466(__this, method) ((  bool (*) (ValueCollection_t37865148 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2005238466_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2166692156_gshared (ValueCollection_t37865148 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2166692156(__this, method) ((  bool (*) (ValueCollection_t37865148 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2166692156_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1170453944_gshared (ValueCollection_t37865148 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1170453944(__this, method) ((  Il2CppObject * (*) (ValueCollection_t37865148 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1170453944_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m222400720_gshared (ValueCollection_t37865148 * __this, JsonSchemaTypeU5BU5D_t3104176164* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m222400720(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t37865148 *, JsonSchemaTypeU5BU5D_t3104176164*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m222400720_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::GetEnumerator()
extern "C"  Enumerator_t3021338069  ValueCollection_GetEnumerator_m270093877_gshared (ValueCollection_t37865148 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m270093877(__this, method) ((  Enumerator_t3021338069  (*) (ValueCollection_t37865148 *, const MethodInfo*))ValueCollection_GetEnumerator_m270093877_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3989247612_gshared (ValueCollection_t37865148 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3989247612(__this, method) ((  int32_t (*) (ValueCollection_t37865148 *, const MethodInfo*))ValueCollection_get_Count_m3989247612_gshared)(__this, method)
