﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si3172014400MethodDeclarations.h"

// System.Void SA.Common.Pattern.Singleton`1<IOSSocialManager>::.ctor()
#define Singleton_1__ctor_m3666595433(__this, method) ((  void (*) (Singleton_1_t3439969068 *, const MethodInfo*))Singleton_1__ctor_m4152044218_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<IOSSocialManager>::.cctor()
#define Singleton_1__cctor_m1327248666(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1__cctor_m982417053_gshared)(__this /* static, unused */, method)
// T SA.Common.Pattern.Singleton`1<IOSSocialManager>::get_Instance()
#define Singleton_1_get_Instance_m868581838(__this /* static, unused */, method) ((  IOSSocialManager_t2957403963 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m3228489301_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<IOSSocialManager>::get_HasInstance()
#define Singleton_1_get_HasInstance_m2689189463(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_HasInstance_m2551508260_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<IOSSocialManager>::get_IsDestroyed()
#define Singleton_1_get_IsDestroyed_m718697313(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_IsDestroyed_m4170993228_gshared)(__this /* static, unused */, method)
// System.Void SA.Common.Pattern.Singleton`1<IOSSocialManager>::OnDestroy()
#define Singleton_1_OnDestroy_m2048153374(__this, method) ((  void (*) (Singleton_1_t3439969068 *, const MethodInfo*))Singleton_1_OnDestroy_m3790554761_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<IOSSocialManager>::OnApplicationQuit()
#define Singleton_1_OnApplicationQuit_m2071571371(__this, method) ((  void (*) (Singleton_1_t3439969068 *, const MethodInfo*))Singleton_1_OnApplicationQuit_m1956476828_gshared)(__this, method)
