﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CoursesNumberExceeded
struct CoursesNumberExceeded_t3039661446;

#include "codegen/il2cpp-codegen.h"

// System.Void CoursesNumberExceeded::.ctor()
extern "C"  void CoursesNumberExceeded__ctor_m1252602289 (CoursesNumberExceeded_t3039661446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoursesNumberExceeded::Start()
extern "C"  void CoursesNumberExceeded_Start_m2044885665 (CoursesNumberExceeded_t3039661446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoursesNumberExceeded::Close()
extern "C"  void CoursesNumberExceeded_Close_m4037359687 (CoursesNumberExceeded_t3039661446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoursesNumberExceeded::<Start>m__7()
extern "C"  void CoursesNumberExceeded_U3CStartU3Em__7_m1606140797 (CoursesNumberExceeded_t3039661446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoursesNumberExceeded::<Start>m__8()
extern "C"  void CoursesNumberExceeded_U3CStartU3Em__8_m1027936032 (CoursesNumberExceeded_t3039661446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoursesNumberExceeded::<Start>m__9()
extern "C"  void CoursesNumberExceeded_U3CStartU3Em__9_m3777022363 (CoursesNumberExceeded_t3039661446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
