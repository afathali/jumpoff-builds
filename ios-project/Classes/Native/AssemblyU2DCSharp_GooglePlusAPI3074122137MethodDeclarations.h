﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlusAPI
struct GooglePlusAPI_t3074122137;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlusAPI::.ctor()
extern "C"  void GooglePlusAPI__ctor_m3054901592 (GooglePlusAPI_t3074122137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlusAPI::Awake()
extern "C"  void GooglePlusAPI_Awake_m3356602265 (GooglePlusAPI_t3074122137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlusAPI::clearDefaultAccount()
extern "C"  void GooglePlusAPI_clearDefaultAccount_m1031111735 (GooglePlusAPI_t3074122137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlusAPI::ClearDefaultAccount()
extern "C"  void GooglePlusAPI_ClearDefaultAccount_m140306967 (GooglePlusAPI_t3074122137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
