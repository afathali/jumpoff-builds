﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AppEventHandlerExample
struct AppEventHandlerExample_t2905099207;

#include "codegen/il2cpp-codegen.h"

// System.Void AppEventHandlerExample::.ctor()
extern "C"  void AppEventHandlerExample__ctor_m3732077900 (AppEventHandlerExample_t2905099207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppEventHandlerExample::Awake()
extern "C"  void AppEventHandlerExample_Awake_m1848335753 (AppEventHandlerExample_t2905099207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppEventHandlerExample::HandleOnApplicationDidBecomeActive()
extern "C"  void AppEventHandlerExample_HandleOnApplicationDidBecomeActive_m328332795 (AppEventHandlerExample_t2905099207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppEventHandlerExample::OnApplicationDidReceiveMemoryWarning()
extern "C"  void AppEventHandlerExample_OnApplicationDidReceiveMemoryWarning_m2067797986 (AppEventHandlerExample_t2905099207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
