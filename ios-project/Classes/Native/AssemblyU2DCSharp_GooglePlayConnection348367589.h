﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<GooglePlayConnectionResult>
struct Action_1_t2560518106;
// System.Action`1<GPConnectionState>
struct Action_1_t449146262;
// System.Action
struct Action_t3226471752;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen1639211507.h"
#include "AssemblyU2DCSharp_GPConnectionState647346880.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayConnection
struct  GooglePlayConnection_t348367589  : public SA_Singleton_OLD_1_t1639211507
{
public:
	// System.Boolean GooglePlayConnection::_IsInitialized
	bool ____IsInitialized_4;

public:
	inline static int32_t get_offset_of__IsInitialized_4() { return static_cast<int32_t>(offsetof(GooglePlayConnection_t348367589, ____IsInitialized_4)); }
	inline bool get__IsInitialized_4() const { return ____IsInitialized_4; }
	inline bool* get_address_of__IsInitialized_4() { return &____IsInitialized_4; }
	inline void set__IsInitialized_4(bool value)
	{
		____IsInitialized_4 = value;
	}
};

struct GooglePlayConnection_t348367589_StaticFields
{
public:
	// GPConnectionState GooglePlayConnection::_State
	int32_t ____State_5;
	// System.Action`1<GooglePlayConnectionResult> GooglePlayConnection::ActionConnectionResultReceived
	Action_1_t2560518106 * ___ActionConnectionResultReceived_6;
	// System.Action`1<GPConnectionState> GooglePlayConnection::ActionConnectionStateChanged
	Action_1_t449146262 * ___ActionConnectionStateChanged_7;
	// System.Action GooglePlayConnection::ActionPlayerConnected
	Action_t3226471752 * ___ActionPlayerConnected_8;
	// System.Action GooglePlayConnection::ActionPlayerDisconnected
	Action_t3226471752 * ___ActionPlayerDisconnected_9;
	// System.Action`1<GooglePlayConnectionResult> GooglePlayConnection::<>f__am$cache6
	Action_1_t2560518106 * ___U3CU3Ef__amU24cache6_10;
	// System.Action`1<GPConnectionState> GooglePlayConnection::<>f__am$cache7
	Action_1_t449146262 * ___U3CU3Ef__amU24cache7_11;
	// System.Action GooglePlayConnection::<>f__am$cache8
	Action_t3226471752 * ___U3CU3Ef__amU24cache8_12;
	// System.Action GooglePlayConnection::<>f__am$cache9
	Action_t3226471752 * ___U3CU3Ef__amU24cache9_13;

public:
	inline static int32_t get_offset_of__State_5() { return static_cast<int32_t>(offsetof(GooglePlayConnection_t348367589_StaticFields, ____State_5)); }
	inline int32_t get__State_5() const { return ____State_5; }
	inline int32_t* get_address_of__State_5() { return &____State_5; }
	inline void set__State_5(int32_t value)
	{
		____State_5 = value;
	}

	inline static int32_t get_offset_of_ActionConnectionResultReceived_6() { return static_cast<int32_t>(offsetof(GooglePlayConnection_t348367589_StaticFields, ___ActionConnectionResultReceived_6)); }
	inline Action_1_t2560518106 * get_ActionConnectionResultReceived_6() const { return ___ActionConnectionResultReceived_6; }
	inline Action_1_t2560518106 ** get_address_of_ActionConnectionResultReceived_6() { return &___ActionConnectionResultReceived_6; }
	inline void set_ActionConnectionResultReceived_6(Action_1_t2560518106 * value)
	{
		___ActionConnectionResultReceived_6 = value;
		Il2CppCodeGenWriteBarrier(&___ActionConnectionResultReceived_6, value);
	}

	inline static int32_t get_offset_of_ActionConnectionStateChanged_7() { return static_cast<int32_t>(offsetof(GooglePlayConnection_t348367589_StaticFields, ___ActionConnectionStateChanged_7)); }
	inline Action_1_t449146262 * get_ActionConnectionStateChanged_7() const { return ___ActionConnectionStateChanged_7; }
	inline Action_1_t449146262 ** get_address_of_ActionConnectionStateChanged_7() { return &___ActionConnectionStateChanged_7; }
	inline void set_ActionConnectionStateChanged_7(Action_1_t449146262 * value)
	{
		___ActionConnectionStateChanged_7 = value;
		Il2CppCodeGenWriteBarrier(&___ActionConnectionStateChanged_7, value);
	}

	inline static int32_t get_offset_of_ActionPlayerConnected_8() { return static_cast<int32_t>(offsetof(GooglePlayConnection_t348367589_StaticFields, ___ActionPlayerConnected_8)); }
	inline Action_t3226471752 * get_ActionPlayerConnected_8() const { return ___ActionPlayerConnected_8; }
	inline Action_t3226471752 ** get_address_of_ActionPlayerConnected_8() { return &___ActionPlayerConnected_8; }
	inline void set_ActionPlayerConnected_8(Action_t3226471752 * value)
	{
		___ActionPlayerConnected_8 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPlayerConnected_8, value);
	}

	inline static int32_t get_offset_of_ActionPlayerDisconnected_9() { return static_cast<int32_t>(offsetof(GooglePlayConnection_t348367589_StaticFields, ___ActionPlayerDisconnected_9)); }
	inline Action_t3226471752 * get_ActionPlayerDisconnected_9() const { return ___ActionPlayerDisconnected_9; }
	inline Action_t3226471752 ** get_address_of_ActionPlayerDisconnected_9() { return &___ActionPlayerDisconnected_9; }
	inline void set_ActionPlayerDisconnected_9(Action_t3226471752 * value)
	{
		___ActionPlayerDisconnected_9 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPlayerDisconnected_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_10() { return static_cast<int32_t>(offsetof(GooglePlayConnection_t348367589_StaticFields, ___U3CU3Ef__amU24cache6_10)); }
	inline Action_1_t2560518106 * get_U3CU3Ef__amU24cache6_10() const { return ___U3CU3Ef__amU24cache6_10; }
	inline Action_1_t2560518106 ** get_address_of_U3CU3Ef__amU24cache6_10() { return &___U3CU3Ef__amU24cache6_10; }
	inline void set_U3CU3Ef__amU24cache6_10(Action_1_t2560518106 * value)
	{
		___U3CU3Ef__amU24cache6_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_11() { return static_cast<int32_t>(offsetof(GooglePlayConnection_t348367589_StaticFields, ___U3CU3Ef__amU24cache7_11)); }
	inline Action_1_t449146262 * get_U3CU3Ef__amU24cache7_11() const { return ___U3CU3Ef__amU24cache7_11; }
	inline Action_1_t449146262 ** get_address_of_U3CU3Ef__amU24cache7_11() { return &___U3CU3Ef__amU24cache7_11; }
	inline void set_U3CU3Ef__amU24cache7_11(Action_1_t449146262 * value)
	{
		___U3CU3Ef__amU24cache7_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_12() { return static_cast<int32_t>(offsetof(GooglePlayConnection_t348367589_StaticFields, ___U3CU3Ef__amU24cache8_12)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache8_12() const { return ___U3CU3Ef__amU24cache8_12; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache8_12() { return &___U3CU3Ef__amU24cache8_12; }
	inline void set_U3CU3Ef__amU24cache8_12(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache8_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_13() { return static_cast<int32_t>(offsetof(GooglePlayConnection_t348367589_StaticFields, ___U3CU3Ef__amU24cache9_13)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache9_13() const { return ___U3CU3Ef__amU24cache9_13; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache9_13() { return &___U3CU3Ef__amU24cache9_13; }
	inline void set_U3CU3Ef__amU24cache9_13(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache9_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
