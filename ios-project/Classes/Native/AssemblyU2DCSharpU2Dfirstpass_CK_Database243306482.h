﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,CK_Database>
struct Dictionary_2_t3546099413;
// System.Action`1<CK_RecordResult>
struct Action_1_t3864347731;
// System.Action`1<CK_RecordDeleteResult>
struct Action_1_t3050268866;
// System.Action`1<CK_QueryResult>
struct Action_1_t975866870;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CK_Database
struct  CK_Database_t243306482  : public Il2CppObject
{
public:
	// System.Int32 CK_Database::_InternalId
	int32_t ____InternalId_1;
	// System.Action`1<CK_RecordResult> CK_Database::ActionRecordSaved
	Action_1_t3864347731 * ___ActionRecordSaved_2;
	// System.Action`1<CK_RecordResult> CK_Database::ActionRecordFetchComplete
	Action_1_t3864347731 * ___ActionRecordFetchComplete_3;
	// System.Action`1<CK_RecordDeleteResult> CK_Database::ActionRecordDeleted
	Action_1_t3050268866 * ___ActionRecordDeleted_4;
	// System.Action`1<CK_QueryResult> CK_Database::ActionQueryComplete
	Action_1_t975866870 * ___ActionQueryComplete_5;

public:
	inline static int32_t get_offset_of__InternalId_1() { return static_cast<int32_t>(offsetof(CK_Database_t243306482, ____InternalId_1)); }
	inline int32_t get__InternalId_1() const { return ____InternalId_1; }
	inline int32_t* get_address_of__InternalId_1() { return &____InternalId_1; }
	inline void set__InternalId_1(int32_t value)
	{
		____InternalId_1 = value;
	}

	inline static int32_t get_offset_of_ActionRecordSaved_2() { return static_cast<int32_t>(offsetof(CK_Database_t243306482, ___ActionRecordSaved_2)); }
	inline Action_1_t3864347731 * get_ActionRecordSaved_2() const { return ___ActionRecordSaved_2; }
	inline Action_1_t3864347731 ** get_address_of_ActionRecordSaved_2() { return &___ActionRecordSaved_2; }
	inline void set_ActionRecordSaved_2(Action_1_t3864347731 * value)
	{
		___ActionRecordSaved_2 = value;
		Il2CppCodeGenWriteBarrier(&___ActionRecordSaved_2, value);
	}

	inline static int32_t get_offset_of_ActionRecordFetchComplete_3() { return static_cast<int32_t>(offsetof(CK_Database_t243306482, ___ActionRecordFetchComplete_3)); }
	inline Action_1_t3864347731 * get_ActionRecordFetchComplete_3() const { return ___ActionRecordFetchComplete_3; }
	inline Action_1_t3864347731 ** get_address_of_ActionRecordFetchComplete_3() { return &___ActionRecordFetchComplete_3; }
	inline void set_ActionRecordFetchComplete_3(Action_1_t3864347731 * value)
	{
		___ActionRecordFetchComplete_3 = value;
		Il2CppCodeGenWriteBarrier(&___ActionRecordFetchComplete_3, value);
	}

	inline static int32_t get_offset_of_ActionRecordDeleted_4() { return static_cast<int32_t>(offsetof(CK_Database_t243306482, ___ActionRecordDeleted_4)); }
	inline Action_1_t3050268866 * get_ActionRecordDeleted_4() const { return ___ActionRecordDeleted_4; }
	inline Action_1_t3050268866 ** get_address_of_ActionRecordDeleted_4() { return &___ActionRecordDeleted_4; }
	inline void set_ActionRecordDeleted_4(Action_1_t3050268866 * value)
	{
		___ActionRecordDeleted_4 = value;
		Il2CppCodeGenWriteBarrier(&___ActionRecordDeleted_4, value);
	}

	inline static int32_t get_offset_of_ActionQueryComplete_5() { return static_cast<int32_t>(offsetof(CK_Database_t243306482, ___ActionQueryComplete_5)); }
	inline Action_1_t975866870 * get_ActionQueryComplete_5() const { return ___ActionQueryComplete_5; }
	inline Action_1_t975866870 ** get_address_of_ActionQueryComplete_5() { return &___ActionQueryComplete_5; }
	inline void set_ActionQueryComplete_5(Action_1_t975866870 * value)
	{
		___ActionQueryComplete_5 = value;
		Il2CppCodeGenWriteBarrier(&___ActionQueryComplete_5, value);
	}
};

struct CK_Database_t243306482_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,CK_Database> CK_Database::_Databases
	Dictionary_2_t3546099413 * ____Databases_0;
	// System.Action`1<CK_RecordResult> CK_Database::<>f__am$cache6
	Action_1_t3864347731 * ___U3CU3Ef__amU24cache6_6;
	// System.Action`1<CK_RecordResult> CK_Database::<>f__am$cache7
	Action_1_t3864347731 * ___U3CU3Ef__amU24cache7_7;
	// System.Action`1<CK_RecordDeleteResult> CK_Database::<>f__am$cache8
	Action_1_t3050268866 * ___U3CU3Ef__amU24cache8_8;
	// System.Action`1<CK_QueryResult> CK_Database::<>f__am$cache9
	Action_1_t975866870 * ___U3CU3Ef__amU24cache9_9;

public:
	inline static int32_t get_offset_of__Databases_0() { return static_cast<int32_t>(offsetof(CK_Database_t243306482_StaticFields, ____Databases_0)); }
	inline Dictionary_2_t3546099413 * get__Databases_0() const { return ____Databases_0; }
	inline Dictionary_2_t3546099413 ** get_address_of__Databases_0() { return &____Databases_0; }
	inline void set__Databases_0(Dictionary_2_t3546099413 * value)
	{
		____Databases_0 = value;
		Il2CppCodeGenWriteBarrier(&____Databases_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(CK_Database_t243306482_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Action_1_t3864347731 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Action_1_t3864347731 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Action_1_t3864347731 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(CK_Database_t243306482_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Action_1_t3864347731 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Action_1_t3864347731 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Action_1_t3864347731 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(CK_Database_t243306482_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline Action_1_t3050268866 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline Action_1_t3050268866 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(Action_1_t3050268866 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(CK_Database_t243306482_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline Action_1_t975866870 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline Action_1_t975866870 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(Action_1_t975866870 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
