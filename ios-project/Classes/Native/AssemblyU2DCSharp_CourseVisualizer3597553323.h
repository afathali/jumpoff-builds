﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// Switch
struct Switch_t1718632940;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t390723249;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t2912116861;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t1854387467;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CourseVisualizer
struct  CourseVisualizer_t3597553323  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject CourseVisualizer::player
	GameObject_t1756533147 * ___player_3;
	// UnityEngine.Camera CourseVisualizer::mainCamera
	Camera_t189460977 * ___mainCamera_4;
	// UnityEngine.GameObject CourseVisualizer::jump1_triple
	GameObject_t1756533147 * ___jump1_triple_5;
	// UnityEngine.GameObject CourseVisualizer::jump2_with_ad
	GameObject_t1756533147 * ___jump2_with_ad_6;
	// UnityEngine.GameObject CourseVisualizer::jump3_water
	GameObject_t1756533147 * ___jump3_water_7;
	// UnityEngine.GameObject CourseVisualizer::jump4_double
	GameObject_t1756533147 * ___jump4_double_8;
	// UnityEngine.GameObject CourseVisualizer::jump5_engine_ready
	GameObject_t1756533147 * ___jump5_engine_ready_9;
	// UnityEngine.GameObject CourseVisualizer::jump6_gate
	GameObject_t1756533147 * ___jump6_gate_10;
	// UnityEngine.GameObject CourseVisualizer::jump7_cross
	GameObject_t1756533147 * ___jump7_cross_11;
	// UnityEngine.GameObject CourseVisualizer::jump8_unicorn
	GameObject_t1756533147 * ___jump8_unicorn_12;
	// UnityEngine.UI.Slider CourseVisualizer::progress
	Slider_t297367283 * ___progress_13;
	// Switch CourseVisualizer::rotateSwitch
	Switch_t1718632940 * ___rotateSwitch_14;
	// UnityEngine.UI.Text CourseVisualizer::debugText
	Text_t356221433 * ___debugText_15;
	// UnityEngine.UI.Text CourseVisualizer::debugText2
	Text_t356221433 * ___debugText2_16;
	// System.Single CourseVisualizer::worldScale
	float ___worldScale_17;
	// System.Single CourseVisualizer::totalDist
	float ___totalDist_18;
	// System.Boolean CourseVisualizer::rotate
	bool ___rotate_19;
	// UnityEngine.Quaternion CourseVisualizer::q0
	Quaternion_t4030073918  ___q0_20;
	// UnityEngine.Sprite CourseVisualizer::playSprite
	Sprite_t309593783 * ___playSprite_21;
	// UnityEngine.Sprite CourseVisualizer::pauseSprite
	Sprite_t309593783 * ___pauseSprite_22;
	// UnityEngine.UI.Button CourseVisualizer::playPauseButton
	Button_t2872111280 * ___playPauseButton_23;
	// UnityEngine.UI.Button CourseVisualizer::btn_center_view
	Button_t2872111280 * ___btn_center_view_24;
	// UnityEngine.UI.Button CourseVisualizer::btn_close
	Button_t2872111280 * ___btn_close_25;
	// UnityEngine.Quaternion CourseVisualizer::cameraBaseRotation
	Quaternion_t4030073918  ___cameraBaseRotation_26;
	// UnityEngine.GameObject CourseVisualizer::ringFence_farm
	GameObject_t1756533147 * ___ringFence_farm_27;
	// UnityEngine.GameObject CourseVisualizer::ringFence_farm_banner
	GameObject_t1756533147 * ___ringFence_farm_banner_28;
	// UnityEngine.GameObject CourseVisualizer::ringFence_show
	GameObject_t1756533147 * ___ringFence_show_29;
	// UnityEngine.GameObject CourseVisualizer::ringFence_show_banner
	GameObject_t1756533147 * ___ringFence_show_banner_30;
	// UnityEngine.GameObject CourseVisualizer::minimapPlayerMarker
	GameObject_t1756533147 * ___minimapPlayerMarker_31;
	// UnityEngine.Quaternion CourseVisualizer::minimapPlayerMarkerQ0
	Quaternion_t4030073918  ___minimapPlayerMarkerQ0_32;
	// System.Boolean CourseVisualizer::playing
	bool ___playing_33;
	// UnityEngine.GameObject CourseVisualizer::env_indoor
	GameObject_t1756533147 * ___env_indoor_34;
	// UnityEngine.GameObject CourseVisualizer::env_indoor_barn
	GameObject_t1756533147 * ___env_indoor_barn_35;
	// UnityEngine.GameObject CourseVisualizer::env_farm
	GameObject_t1756533147 * ___env_farm_36;
	// System.Collections.Generic.List`1<UnityEngine.Object> CourseVisualizer::envObjects
	List_1_t390723249 * ___envObjects_37;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CourseVisualizer::envBanners
	List_1_t1125654279 * ___envBanners_38;
	// UnityEngine.GameObject CourseVisualizer::env_show
	GameObject_t1756533147 * ___env_show_39;
	// UnityEngine.GameObject CourseVisualizer::show_tribune
	GameObject_t1756533147 * ___show_tribune_40;
	// UnityEngine.GameObject CourseVisualizer::show_tribuneWithBanner
	GameObject_t1756533147 * ___show_tribuneWithBanner_41;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CourseVisualizer::oxerBanners
	List_1_t1125654279 * ___oxerBanners_42;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> CourseVisualizer::texs
	List_1_t2912116861 * ___texs_43;
	// System.Int32 CourseVisualizer::currentPoint
	int32_t ___currentPoint_44;
	// System.Single CourseVisualizer::currSegmentTime
	float ___currSegmentTime_45;
	// System.Single CourseVisualizer::wholeSegmentsDist
	float ___wholeSegmentsDist_46;
	// UnityEngine.Quaternion CourseVisualizer::lookDestRotation
	Quaternion_t4030073918  ___lookDestRotation_47;
	// UnityEngine.Quaternion CourseVisualizer::trackDestRotation
	Quaternion_t4030073918  ___trackDestRotation_48;
	// System.Single CourseVisualizer::progressValue
	float ___progressValue_49;
	// UnityEngine.Quaternion[] CourseVisualizer::trackRotations
	QuaternionU5BU5D_t1854387467* ___trackRotations_50;
	// UnityEngine.UI.Image CourseVisualizer::minimapImage
	Image_t2042527209 * ___minimapImage_51;
	// System.Boolean CourseVisualizer::rotatePlayerNow
	bool ___rotatePlayerNow_52;
	// System.Boolean CourseVisualizer::jumping
	bool ___jumping_53;
	// System.Single CourseVisualizer::jumpTime
	float ___jumpTime_54;
	// System.Single CourseVisualizer::totalJumpTime
	float ___totalJumpTime_55;
	// System.Single CourseVisualizer::lastJumpTime
	float ___lastJumpTime_56;

public:
	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___player_3)); }
	inline GameObject_t1756533147 * get_player_3() const { return ___player_3; }
	inline GameObject_t1756533147 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(GameObject_t1756533147 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier(&___player_3, value);
	}

	inline static int32_t get_offset_of_mainCamera_4() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___mainCamera_4)); }
	inline Camera_t189460977 * get_mainCamera_4() const { return ___mainCamera_4; }
	inline Camera_t189460977 ** get_address_of_mainCamera_4() { return &___mainCamera_4; }
	inline void set_mainCamera_4(Camera_t189460977 * value)
	{
		___mainCamera_4 = value;
		Il2CppCodeGenWriteBarrier(&___mainCamera_4, value);
	}

	inline static int32_t get_offset_of_jump1_triple_5() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___jump1_triple_5)); }
	inline GameObject_t1756533147 * get_jump1_triple_5() const { return ___jump1_triple_5; }
	inline GameObject_t1756533147 ** get_address_of_jump1_triple_5() { return &___jump1_triple_5; }
	inline void set_jump1_triple_5(GameObject_t1756533147 * value)
	{
		___jump1_triple_5 = value;
		Il2CppCodeGenWriteBarrier(&___jump1_triple_5, value);
	}

	inline static int32_t get_offset_of_jump2_with_ad_6() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___jump2_with_ad_6)); }
	inline GameObject_t1756533147 * get_jump2_with_ad_6() const { return ___jump2_with_ad_6; }
	inline GameObject_t1756533147 ** get_address_of_jump2_with_ad_6() { return &___jump2_with_ad_6; }
	inline void set_jump2_with_ad_6(GameObject_t1756533147 * value)
	{
		___jump2_with_ad_6 = value;
		Il2CppCodeGenWriteBarrier(&___jump2_with_ad_6, value);
	}

	inline static int32_t get_offset_of_jump3_water_7() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___jump3_water_7)); }
	inline GameObject_t1756533147 * get_jump3_water_7() const { return ___jump3_water_7; }
	inline GameObject_t1756533147 ** get_address_of_jump3_water_7() { return &___jump3_water_7; }
	inline void set_jump3_water_7(GameObject_t1756533147 * value)
	{
		___jump3_water_7 = value;
		Il2CppCodeGenWriteBarrier(&___jump3_water_7, value);
	}

	inline static int32_t get_offset_of_jump4_double_8() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___jump4_double_8)); }
	inline GameObject_t1756533147 * get_jump4_double_8() const { return ___jump4_double_8; }
	inline GameObject_t1756533147 ** get_address_of_jump4_double_8() { return &___jump4_double_8; }
	inline void set_jump4_double_8(GameObject_t1756533147 * value)
	{
		___jump4_double_8 = value;
		Il2CppCodeGenWriteBarrier(&___jump4_double_8, value);
	}

	inline static int32_t get_offset_of_jump5_engine_ready_9() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___jump5_engine_ready_9)); }
	inline GameObject_t1756533147 * get_jump5_engine_ready_9() const { return ___jump5_engine_ready_9; }
	inline GameObject_t1756533147 ** get_address_of_jump5_engine_ready_9() { return &___jump5_engine_ready_9; }
	inline void set_jump5_engine_ready_9(GameObject_t1756533147 * value)
	{
		___jump5_engine_ready_9 = value;
		Il2CppCodeGenWriteBarrier(&___jump5_engine_ready_9, value);
	}

	inline static int32_t get_offset_of_jump6_gate_10() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___jump6_gate_10)); }
	inline GameObject_t1756533147 * get_jump6_gate_10() const { return ___jump6_gate_10; }
	inline GameObject_t1756533147 ** get_address_of_jump6_gate_10() { return &___jump6_gate_10; }
	inline void set_jump6_gate_10(GameObject_t1756533147 * value)
	{
		___jump6_gate_10 = value;
		Il2CppCodeGenWriteBarrier(&___jump6_gate_10, value);
	}

	inline static int32_t get_offset_of_jump7_cross_11() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___jump7_cross_11)); }
	inline GameObject_t1756533147 * get_jump7_cross_11() const { return ___jump7_cross_11; }
	inline GameObject_t1756533147 ** get_address_of_jump7_cross_11() { return &___jump7_cross_11; }
	inline void set_jump7_cross_11(GameObject_t1756533147 * value)
	{
		___jump7_cross_11 = value;
		Il2CppCodeGenWriteBarrier(&___jump7_cross_11, value);
	}

	inline static int32_t get_offset_of_jump8_unicorn_12() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___jump8_unicorn_12)); }
	inline GameObject_t1756533147 * get_jump8_unicorn_12() const { return ___jump8_unicorn_12; }
	inline GameObject_t1756533147 ** get_address_of_jump8_unicorn_12() { return &___jump8_unicorn_12; }
	inline void set_jump8_unicorn_12(GameObject_t1756533147 * value)
	{
		___jump8_unicorn_12 = value;
		Il2CppCodeGenWriteBarrier(&___jump8_unicorn_12, value);
	}

	inline static int32_t get_offset_of_progress_13() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___progress_13)); }
	inline Slider_t297367283 * get_progress_13() const { return ___progress_13; }
	inline Slider_t297367283 ** get_address_of_progress_13() { return &___progress_13; }
	inline void set_progress_13(Slider_t297367283 * value)
	{
		___progress_13 = value;
		Il2CppCodeGenWriteBarrier(&___progress_13, value);
	}

	inline static int32_t get_offset_of_rotateSwitch_14() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___rotateSwitch_14)); }
	inline Switch_t1718632940 * get_rotateSwitch_14() const { return ___rotateSwitch_14; }
	inline Switch_t1718632940 ** get_address_of_rotateSwitch_14() { return &___rotateSwitch_14; }
	inline void set_rotateSwitch_14(Switch_t1718632940 * value)
	{
		___rotateSwitch_14 = value;
		Il2CppCodeGenWriteBarrier(&___rotateSwitch_14, value);
	}

	inline static int32_t get_offset_of_debugText_15() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___debugText_15)); }
	inline Text_t356221433 * get_debugText_15() const { return ___debugText_15; }
	inline Text_t356221433 ** get_address_of_debugText_15() { return &___debugText_15; }
	inline void set_debugText_15(Text_t356221433 * value)
	{
		___debugText_15 = value;
		Il2CppCodeGenWriteBarrier(&___debugText_15, value);
	}

	inline static int32_t get_offset_of_debugText2_16() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___debugText2_16)); }
	inline Text_t356221433 * get_debugText2_16() const { return ___debugText2_16; }
	inline Text_t356221433 ** get_address_of_debugText2_16() { return &___debugText2_16; }
	inline void set_debugText2_16(Text_t356221433 * value)
	{
		___debugText2_16 = value;
		Il2CppCodeGenWriteBarrier(&___debugText2_16, value);
	}

	inline static int32_t get_offset_of_worldScale_17() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___worldScale_17)); }
	inline float get_worldScale_17() const { return ___worldScale_17; }
	inline float* get_address_of_worldScale_17() { return &___worldScale_17; }
	inline void set_worldScale_17(float value)
	{
		___worldScale_17 = value;
	}

	inline static int32_t get_offset_of_totalDist_18() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___totalDist_18)); }
	inline float get_totalDist_18() const { return ___totalDist_18; }
	inline float* get_address_of_totalDist_18() { return &___totalDist_18; }
	inline void set_totalDist_18(float value)
	{
		___totalDist_18 = value;
	}

	inline static int32_t get_offset_of_rotate_19() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___rotate_19)); }
	inline bool get_rotate_19() const { return ___rotate_19; }
	inline bool* get_address_of_rotate_19() { return &___rotate_19; }
	inline void set_rotate_19(bool value)
	{
		___rotate_19 = value;
	}

	inline static int32_t get_offset_of_q0_20() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___q0_20)); }
	inline Quaternion_t4030073918  get_q0_20() const { return ___q0_20; }
	inline Quaternion_t4030073918 * get_address_of_q0_20() { return &___q0_20; }
	inline void set_q0_20(Quaternion_t4030073918  value)
	{
		___q0_20 = value;
	}

	inline static int32_t get_offset_of_playSprite_21() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___playSprite_21)); }
	inline Sprite_t309593783 * get_playSprite_21() const { return ___playSprite_21; }
	inline Sprite_t309593783 ** get_address_of_playSprite_21() { return &___playSprite_21; }
	inline void set_playSprite_21(Sprite_t309593783 * value)
	{
		___playSprite_21 = value;
		Il2CppCodeGenWriteBarrier(&___playSprite_21, value);
	}

	inline static int32_t get_offset_of_pauseSprite_22() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___pauseSprite_22)); }
	inline Sprite_t309593783 * get_pauseSprite_22() const { return ___pauseSprite_22; }
	inline Sprite_t309593783 ** get_address_of_pauseSprite_22() { return &___pauseSprite_22; }
	inline void set_pauseSprite_22(Sprite_t309593783 * value)
	{
		___pauseSprite_22 = value;
		Il2CppCodeGenWriteBarrier(&___pauseSprite_22, value);
	}

	inline static int32_t get_offset_of_playPauseButton_23() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___playPauseButton_23)); }
	inline Button_t2872111280 * get_playPauseButton_23() const { return ___playPauseButton_23; }
	inline Button_t2872111280 ** get_address_of_playPauseButton_23() { return &___playPauseButton_23; }
	inline void set_playPauseButton_23(Button_t2872111280 * value)
	{
		___playPauseButton_23 = value;
		Il2CppCodeGenWriteBarrier(&___playPauseButton_23, value);
	}

	inline static int32_t get_offset_of_btn_center_view_24() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___btn_center_view_24)); }
	inline Button_t2872111280 * get_btn_center_view_24() const { return ___btn_center_view_24; }
	inline Button_t2872111280 ** get_address_of_btn_center_view_24() { return &___btn_center_view_24; }
	inline void set_btn_center_view_24(Button_t2872111280 * value)
	{
		___btn_center_view_24 = value;
		Il2CppCodeGenWriteBarrier(&___btn_center_view_24, value);
	}

	inline static int32_t get_offset_of_btn_close_25() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___btn_close_25)); }
	inline Button_t2872111280 * get_btn_close_25() const { return ___btn_close_25; }
	inline Button_t2872111280 ** get_address_of_btn_close_25() { return &___btn_close_25; }
	inline void set_btn_close_25(Button_t2872111280 * value)
	{
		___btn_close_25 = value;
		Il2CppCodeGenWriteBarrier(&___btn_close_25, value);
	}

	inline static int32_t get_offset_of_cameraBaseRotation_26() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___cameraBaseRotation_26)); }
	inline Quaternion_t4030073918  get_cameraBaseRotation_26() const { return ___cameraBaseRotation_26; }
	inline Quaternion_t4030073918 * get_address_of_cameraBaseRotation_26() { return &___cameraBaseRotation_26; }
	inline void set_cameraBaseRotation_26(Quaternion_t4030073918  value)
	{
		___cameraBaseRotation_26 = value;
	}

	inline static int32_t get_offset_of_ringFence_farm_27() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___ringFence_farm_27)); }
	inline GameObject_t1756533147 * get_ringFence_farm_27() const { return ___ringFence_farm_27; }
	inline GameObject_t1756533147 ** get_address_of_ringFence_farm_27() { return &___ringFence_farm_27; }
	inline void set_ringFence_farm_27(GameObject_t1756533147 * value)
	{
		___ringFence_farm_27 = value;
		Il2CppCodeGenWriteBarrier(&___ringFence_farm_27, value);
	}

	inline static int32_t get_offset_of_ringFence_farm_banner_28() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___ringFence_farm_banner_28)); }
	inline GameObject_t1756533147 * get_ringFence_farm_banner_28() const { return ___ringFence_farm_banner_28; }
	inline GameObject_t1756533147 ** get_address_of_ringFence_farm_banner_28() { return &___ringFence_farm_banner_28; }
	inline void set_ringFence_farm_banner_28(GameObject_t1756533147 * value)
	{
		___ringFence_farm_banner_28 = value;
		Il2CppCodeGenWriteBarrier(&___ringFence_farm_banner_28, value);
	}

	inline static int32_t get_offset_of_ringFence_show_29() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___ringFence_show_29)); }
	inline GameObject_t1756533147 * get_ringFence_show_29() const { return ___ringFence_show_29; }
	inline GameObject_t1756533147 ** get_address_of_ringFence_show_29() { return &___ringFence_show_29; }
	inline void set_ringFence_show_29(GameObject_t1756533147 * value)
	{
		___ringFence_show_29 = value;
		Il2CppCodeGenWriteBarrier(&___ringFence_show_29, value);
	}

	inline static int32_t get_offset_of_ringFence_show_banner_30() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___ringFence_show_banner_30)); }
	inline GameObject_t1756533147 * get_ringFence_show_banner_30() const { return ___ringFence_show_banner_30; }
	inline GameObject_t1756533147 ** get_address_of_ringFence_show_banner_30() { return &___ringFence_show_banner_30; }
	inline void set_ringFence_show_banner_30(GameObject_t1756533147 * value)
	{
		___ringFence_show_banner_30 = value;
		Il2CppCodeGenWriteBarrier(&___ringFence_show_banner_30, value);
	}

	inline static int32_t get_offset_of_minimapPlayerMarker_31() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___minimapPlayerMarker_31)); }
	inline GameObject_t1756533147 * get_minimapPlayerMarker_31() const { return ___minimapPlayerMarker_31; }
	inline GameObject_t1756533147 ** get_address_of_minimapPlayerMarker_31() { return &___minimapPlayerMarker_31; }
	inline void set_minimapPlayerMarker_31(GameObject_t1756533147 * value)
	{
		___minimapPlayerMarker_31 = value;
		Il2CppCodeGenWriteBarrier(&___minimapPlayerMarker_31, value);
	}

	inline static int32_t get_offset_of_minimapPlayerMarkerQ0_32() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___minimapPlayerMarkerQ0_32)); }
	inline Quaternion_t4030073918  get_minimapPlayerMarkerQ0_32() const { return ___minimapPlayerMarkerQ0_32; }
	inline Quaternion_t4030073918 * get_address_of_minimapPlayerMarkerQ0_32() { return &___minimapPlayerMarkerQ0_32; }
	inline void set_minimapPlayerMarkerQ0_32(Quaternion_t4030073918  value)
	{
		___minimapPlayerMarkerQ0_32 = value;
	}

	inline static int32_t get_offset_of_playing_33() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___playing_33)); }
	inline bool get_playing_33() const { return ___playing_33; }
	inline bool* get_address_of_playing_33() { return &___playing_33; }
	inline void set_playing_33(bool value)
	{
		___playing_33 = value;
	}

	inline static int32_t get_offset_of_env_indoor_34() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___env_indoor_34)); }
	inline GameObject_t1756533147 * get_env_indoor_34() const { return ___env_indoor_34; }
	inline GameObject_t1756533147 ** get_address_of_env_indoor_34() { return &___env_indoor_34; }
	inline void set_env_indoor_34(GameObject_t1756533147 * value)
	{
		___env_indoor_34 = value;
		Il2CppCodeGenWriteBarrier(&___env_indoor_34, value);
	}

	inline static int32_t get_offset_of_env_indoor_barn_35() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___env_indoor_barn_35)); }
	inline GameObject_t1756533147 * get_env_indoor_barn_35() const { return ___env_indoor_barn_35; }
	inline GameObject_t1756533147 ** get_address_of_env_indoor_barn_35() { return &___env_indoor_barn_35; }
	inline void set_env_indoor_barn_35(GameObject_t1756533147 * value)
	{
		___env_indoor_barn_35 = value;
		Il2CppCodeGenWriteBarrier(&___env_indoor_barn_35, value);
	}

	inline static int32_t get_offset_of_env_farm_36() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___env_farm_36)); }
	inline GameObject_t1756533147 * get_env_farm_36() const { return ___env_farm_36; }
	inline GameObject_t1756533147 ** get_address_of_env_farm_36() { return &___env_farm_36; }
	inline void set_env_farm_36(GameObject_t1756533147 * value)
	{
		___env_farm_36 = value;
		Il2CppCodeGenWriteBarrier(&___env_farm_36, value);
	}

	inline static int32_t get_offset_of_envObjects_37() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___envObjects_37)); }
	inline List_1_t390723249 * get_envObjects_37() const { return ___envObjects_37; }
	inline List_1_t390723249 ** get_address_of_envObjects_37() { return &___envObjects_37; }
	inline void set_envObjects_37(List_1_t390723249 * value)
	{
		___envObjects_37 = value;
		Il2CppCodeGenWriteBarrier(&___envObjects_37, value);
	}

	inline static int32_t get_offset_of_envBanners_38() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___envBanners_38)); }
	inline List_1_t1125654279 * get_envBanners_38() const { return ___envBanners_38; }
	inline List_1_t1125654279 ** get_address_of_envBanners_38() { return &___envBanners_38; }
	inline void set_envBanners_38(List_1_t1125654279 * value)
	{
		___envBanners_38 = value;
		Il2CppCodeGenWriteBarrier(&___envBanners_38, value);
	}

	inline static int32_t get_offset_of_env_show_39() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___env_show_39)); }
	inline GameObject_t1756533147 * get_env_show_39() const { return ___env_show_39; }
	inline GameObject_t1756533147 ** get_address_of_env_show_39() { return &___env_show_39; }
	inline void set_env_show_39(GameObject_t1756533147 * value)
	{
		___env_show_39 = value;
		Il2CppCodeGenWriteBarrier(&___env_show_39, value);
	}

	inline static int32_t get_offset_of_show_tribune_40() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___show_tribune_40)); }
	inline GameObject_t1756533147 * get_show_tribune_40() const { return ___show_tribune_40; }
	inline GameObject_t1756533147 ** get_address_of_show_tribune_40() { return &___show_tribune_40; }
	inline void set_show_tribune_40(GameObject_t1756533147 * value)
	{
		___show_tribune_40 = value;
		Il2CppCodeGenWriteBarrier(&___show_tribune_40, value);
	}

	inline static int32_t get_offset_of_show_tribuneWithBanner_41() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___show_tribuneWithBanner_41)); }
	inline GameObject_t1756533147 * get_show_tribuneWithBanner_41() const { return ___show_tribuneWithBanner_41; }
	inline GameObject_t1756533147 ** get_address_of_show_tribuneWithBanner_41() { return &___show_tribuneWithBanner_41; }
	inline void set_show_tribuneWithBanner_41(GameObject_t1756533147 * value)
	{
		___show_tribuneWithBanner_41 = value;
		Il2CppCodeGenWriteBarrier(&___show_tribuneWithBanner_41, value);
	}

	inline static int32_t get_offset_of_oxerBanners_42() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___oxerBanners_42)); }
	inline List_1_t1125654279 * get_oxerBanners_42() const { return ___oxerBanners_42; }
	inline List_1_t1125654279 ** get_address_of_oxerBanners_42() { return &___oxerBanners_42; }
	inline void set_oxerBanners_42(List_1_t1125654279 * value)
	{
		___oxerBanners_42 = value;
		Il2CppCodeGenWriteBarrier(&___oxerBanners_42, value);
	}

	inline static int32_t get_offset_of_texs_43() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___texs_43)); }
	inline List_1_t2912116861 * get_texs_43() const { return ___texs_43; }
	inline List_1_t2912116861 ** get_address_of_texs_43() { return &___texs_43; }
	inline void set_texs_43(List_1_t2912116861 * value)
	{
		___texs_43 = value;
		Il2CppCodeGenWriteBarrier(&___texs_43, value);
	}

	inline static int32_t get_offset_of_currentPoint_44() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___currentPoint_44)); }
	inline int32_t get_currentPoint_44() const { return ___currentPoint_44; }
	inline int32_t* get_address_of_currentPoint_44() { return &___currentPoint_44; }
	inline void set_currentPoint_44(int32_t value)
	{
		___currentPoint_44 = value;
	}

	inline static int32_t get_offset_of_currSegmentTime_45() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___currSegmentTime_45)); }
	inline float get_currSegmentTime_45() const { return ___currSegmentTime_45; }
	inline float* get_address_of_currSegmentTime_45() { return &___currSegmentTime_45; }
	inline void set_currSegmentTime_45(float value)
	{
		___currSegmentTime_45 = value;
	}

	inline static int32_t get_offset_of_wholeSegmentsDist_46() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___wholeSegmentsDist_46)); }
	inline float get_wholeSegmentsDist_46() const { return ___wholeSegmentsDist_46; }
	inline float* get_address_of_wholeSegmentsDist_46() { return &___wholeSegmentsDist_46; }
	inline void set_wholeSegmentsDist_46(float value)
	{
		___wholeSegmentsDist_46 = value;
	}

	inline static int32_t get_offset_of_lookDestRotation_47() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___lookDestRotation_47)); }
	inline Quaternion_t4030073918  get_lookDestRotation_47() const { return ___lookDestRotation_47; }
	inline Quaternion_t4030073918 * get_address_of_lookDestRotation_47() { return &___lookDestRotation_47; }
	inline void set_lookDestRotation_47(Quaternion_t4030073918  value)
	{
		___lookDestRotation_47 = value;
	}

	inline static int32_t get_offset_of_trackDestRotation_48() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___trackDestRotation_48)); }
	inline Quaternion_t4030073918  get_trackDestRotation_48() const { return ___trackDestRotation_48; }
	inline Quaternion_t4030073918 * get_address_of_trackDestRotation_48() { return &___trackDestRotation_48; }
	inline void set_trackDestRotation_48(Quaternion_t4030073918  value)
	{
		___trackDestRotation_48 = value;
	}

	inline static int32_t get_offset_of_progressValue_49() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___progressValue_49)); }
	inline float get_progressValue_49() const { return ___progressValue_49; }
	inline float* get_address_of_progressValue_49() { return &___progressValue_49; }
	inline void set_progressValue_49(float value)
	{
		___progressValue_49 = value;
	}

	inline static int32_t get_offset_of_trackRotations_50() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___trackRotations_50)); }
	inline QuaternionU5BU5D_t1854387467* get_trackRotations_50() const { return ___trackRotations_50; }
	inline QuaternionU5BU5D_t1854387467** get_address_of_trackRotations_50() { return &___trackRotations_50; }
	inline void set_trackRotations_50(QuaternionU5BU5D_t1854387467* value)
	{
		___trackRotations_50 = value;
		Il2CppCodeGenWriteBarrier(&___trackRotations_50, value);
	}

	inline static int32_t get_offset_of_minimapImage_51() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___minimapImage_51)); }
	inline Image_t2042527209 * get_minimapImage_51() const { return ___minimapImage_51; }
	inline Image_t2042527209 ** get_address_of_minimapImage_51() { return &___minimapImage_51; }
	inline void set_minimapImage_51(Image_t2042527209 * value)
	{
		___minimapImage_51 = value;
		Il2CppCodeGenWriteBarrier(&___minimapImage_51, value);
	}

	inline static int32_t get_offset_of_rotatePlayerNow_52() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___rotatePlayerNow_52)); }
	inline bool get_rotatePlayerNow_52() const { return ___rotatePlayerNow_52; }
	inline bool* get_address_of_rotatePlayerNow_52() { return &___rotatePlayerNow_52; }
	inline void set_rotatePlayerNow_52(bool value)
	{
		___rotatePlayerNow_52 = value;
	}

	inline static int32_t get_offset_of_jumping_53() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___jumping_53)); }
	inline bool get_jumping_53() const { return ___jumping_53; }
	inline bool* get_address_of_jumping_53() { return &___jumping_53; }
	inline void set_jumping_53(bool value)
	{
		___jumping_53 = value;
	}

	inline static int32_t get_offset_of_jumpTime_54() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___jumpTime_54)); }
	inline float get_jumpTime_54() const { return ___jumpTime_54; }
	inline float* get_address_of_jumpTime_54() { return &___jumpTime_54; }
	inline void set_jumpTime_54(float value)
	{
		___jumpTime_54 = value;
	}

	inline static int32_t get_offset_of_totalJumpTime_55() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___totalJumpTime_55)); }
	inline float get_totalJumpTime_55() const { return ___totalJumpTime_55; }
	inline float* get_address_of_totalJumpTime_55() { return &___totalJumpTime_55; }
	inline void set_totalJumpTime_55(float value)
	{
		___totalJumpTime_55 = value;
	}

	inline static int32_t get_offset_of_lastJumpTime_56() { return static_cast<int32_t>(offsetof(CourseVisualizer_t3597553323, ___lastJumpTime_56)); }
	inline float get_lastJumpTime_56() const { return ___lastJumpTime_56; }
	inline float* get_address_of_lastJumpTime_56() { return &___lastJumpTime_56; }
	inline void set_lastJumpTime_56(float value)
	{
		___lastJumpTime_56 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
