﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread4023353063MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Reflection.ICustomAttributeProvider,System.Object>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m15495176(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t237141303 *, Func_2_t3334259717 *, const MethodInfo*))ThreadSafeStore_2__ctor_m2716591129_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Reflection.ICustomAttributeProvider,System.Object>::Get(TKey)
#define ThreadSafeStore_2_Get_m3778306774(__this, ___key0, method) ((  Il2CppObject * (*) (ThreadSafeStore_2_t237141303 *, Il2CppObject *, const MethodInfo*))ThreadSafeStore_2_Get_m1768706765_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Reflection.ICustomAttributeProvider,System.Object>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m532330234(__this, ___key0, method) ((  Il2CppObject * (*) (ThreadSafeStore_2_t237141303 *, Il2CppObject *, const MethodInfo*))ThreadSafeStore_2_AddValue_m4069531929_gshared)(__this, ___key0, method)
