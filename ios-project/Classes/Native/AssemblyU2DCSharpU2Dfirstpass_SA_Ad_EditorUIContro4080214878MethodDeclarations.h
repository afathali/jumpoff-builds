﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_Ad_EditorUIController
struct SA_Ad_EditorUIController_t4080214878;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"

// System.Void SA_Ad_EditorUIController::.ctor()
extern "C"  void SA_Ad_EditorUIController__ctor_m2754299091 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::add_OnCloseVideo(System.Action`1<System.Boolean>)
extern "C"  void SA_Ad_EditorUIController_add_OnCloseVideo_m2668537072 (SA_Ad_EditorUIController_t4080214878 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::remove_OnCloseVideo(System.Action`1<System.Boolean>)
extern "C"  void SA_Ad_EditorUIController_remove_OnCloseVideo_m1838040549 (SA_Ad_EditorUIController_t4080214878 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::add_OnVideoLeftApplication(System.Action)
extern "C"  void SA_Ad_EditorUIController_add_OnVideoLeftApplication_m4048021005 (SA_Ad_EditorUIController_t4080214878 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::remove_OnVideoLeftApplication(System.Action)
extern "C"  void SA_Ad_EditorUIController_remove_OnVideoLeftApplication_m3273645198 (SA_Ad_EditorUIController_t4080214878 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::add_OnCloseInterstitial(System.Action`1<System.Boolean>)
extern "C"  void SA_Ad_EditorUIController_add_OnCloseInterstitial_m114112109 (SA_Ad_EditorUIController_t4080214878 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::remove_OnCloseInterstitial(System.Action`1<System.Boolean>)
extern "C"  void SA_Ad_EditorUIController_remove_OnCloseInterstitial_m1246761324 (SA_Ad_EditorUIController_t4080214878 * __this, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::add_OnInterstitialLeftApplication(System.Action)
extern "C"  void SA_Ad_EditorUIController_add_OnInterstitialLeftApplication_m578268554 (SA_Ad_EditorUIController_t4080214878 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::remove_OnInterstitialLeftApplication(System.Action)
extern "C"  void SA_Ad_EditorUIController_remove_OnInterstitialLeftApplication_m3223399041 (SA_Ad_EditorUIController_t4080214878 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::Awake()
extern "C"  void SA_Ad_EditorUIController_Awake_m863792080 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::Start()
extern "C"  void SA_Ad_EditorUIController_Start_m2958576415 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::InterstitialClick()
extern "C"  void SA_Ad_EditorUIController_InterstitialClick_m3665011207 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::VideoClick()
extern "C"  void SA_Ad_EditorUIController_VideoClick_m2145169358 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::ShowInterstitialAd()
extern "C"  void SA_Ad_EditorUIController_ShowInterstitialAd_m142409635 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::ShowVideoAd()
extern "C"  void SA_Ad_EditorUIController_ShowVideoAd_m1736833306 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::CloseInterstitial()
extern "C"  void SA_Ad_EditorUIController_CloseInterstitial_m1695380903 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::CloseVideo()
extern "C"  void SA_Ad_EditorUIController_CloseVideo_m962096164 (SA_Ad_EditorUIController_t4080214878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::<OnCloseVideo>m__81(System.Boolean)
extern "C"  void SA_Ad_EditorUIController_U3COnCloseVideoU3Em__81_m54967096 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::<OnVideoLeftApplication>m__82()
extern "C"  void SA_Ad_EditorUIController_U3COnVideoLeftApplicationU3Em__82_m1798040571 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::<OnCloseInterstitial>m__83(System.Boolean)
extern "C"  void SA_Ad_EditorUIController_U3COnCloseInterstitialU3Em__83_m3395544757 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Ad_EditorUIController::<OnInterstitialLeftApplication>m__84()
extern "C"  void SA_Ad_EditorUIController_U3COnInterstitialLeftApplicationU3Em__84_m164768560 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
