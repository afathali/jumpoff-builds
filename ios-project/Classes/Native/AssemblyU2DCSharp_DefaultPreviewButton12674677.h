﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.Action
struct Action_t3226471752;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultPreviewButton
struct  DefaultPreviewButton_t12674677  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture DefaultPreviewButton::normalTexture
	Texture_t2243626319 * ___normalTexture_2;
	// UnityEngine.Texture DefaultPreviewButton::pressedTexture
	Texture_t2243626319 * ___pressedTexture_3;
	// UnityEngine.Texture DefaultPreviewButton::disabledTexture
	Texture_t2243626319 * ___disabledTexture_4;
	// UnityEngine.Texture DefaultPreviewButton::selectedTexture
	Texture_t2243626319 * ___selectedTexture_5;
	// UnityEngine.Texture DefaultPreviewButton::normalTex
	Texture_t2243626319 * ___normalTex_6;
	// UnityEngine.AudioClip DefaultPreviewButton::sound
	AudioClip_t1932558630 * ___sound_7;
	// UnityEngine.AudioClip DefaultPreviewButton::disabledsound
	AudioClip_t1932558630 * ___disabledsound_8;
	// System.Boolean DefaultPreviewButton::IsDisabled
	bool ___IsDisabled_9;
	// System.Action DefaultPreviewButton::ActionClick
	Action_t3226471752 * ___ActionClick_10;

public:
	inline static int32_t get_offset_of_normalTexture_2() { return static_cast<int32_t>(offsetof(DefaultPreviewButton_t12674677, ___normalTexture_2)); }
	inline Texture_t2243626319 * get_normalTexture_2() const { return ___normalTexture_2; }
	inline Texture_t2243626319 ** get_address_of_normalTexture_2() { return &___normalTexture_2; }
	inline void set_normalTexture_2(Texture_t2243626319 * value)
	{
		___normalTexture_2 = value;
		Il2CppCodeGenWriteBarrier(&___normalTexture_2, value);
	}

	inline static int32_t get_offset_of_pressedTexture_3() { return static_cast<int32_t>(offsetof(DefaultPreviewButton_t12674677, ___pressedTexture_3)); }
	inline Texture_t2243626319 * get_pressedTexture_3() const { return ___pressedTexture_3; }
	inline Texture_t2243626319 ** get_address_of_pressedTexture_3() { return &___pressedTexture_3; }
	inline void set_pressedTexture_3(Texture_t2243626319 * value)
	{
		___pressedTexture_3 = value;
		Il2CppCodeGenWriteBarrier(&___pressedTexture_3, value);
	}

	inline static int32_t get_offset_of_disabledTexture_4() { return static_cast<int32_t>(offsetof(DefaultPreviewButton_t12674677, ___disabledTexture_4)); }
	inline Texture_t2243626319 * get_disabledTexture_4() const { return ___disabledTexture_4; }
	inline Texture_t2243626319 ** get_address_of_disabledTexture_4() { return &___disabledTexture_4; }
	inline void set_disabledTexture_4(Texture_t2243626319 * value)
	{
		___disabledTexture_4 = value;
		Il2CppCodeGenWriteBarrier(&___disabledTexture_4, value);
	}

	inline static int32_t get_offset_of_selectedTexture_5() { return static_cast<int32_t>(offsetof(DefaultPreviewButton_t12674677, ___selectedTexture_5)); }
	inline Texture_t2243626319 * get_selectedTexture_5() const { return ___selectedTexture_5; }
	inline Texture_t2243626319 ** get_address_of_selectedTexture_5() { return &___selectedTexture_5; }
	inline void set_selectedTexture_5(Texture_t2243626319 * value)
	{
		___selectedTexture_5 = value;
		Il2CppCodeGenWriteBarrier(&___selectedTexture_5, value);
	}

	inline static int32_t get_offset_of_normalTex_6() { return static_cast<int32_t>(offsetof(DefaultPreviewButton_t12674677, ___normalTex_6)); }
	inline Texture_t2243626319 * get_normalTex_6() const { return ___normalTex_6; }
	inline Texture_t2243626319 ** get_address_of_normalTex_6() { return &___normalTex_6; }
	inline void set_normalTex_6(Texture_t2243626319 * value)
	{
		___normalTex_6 = value;
		Il2CppCodeGenWriteBarrier(&___normalTex_6, value);
	}

	inline static int32_t get_offset_of_sound_7() { return static_cast<int32_t>(offsetof(DefaultPreviewButton_t12674677, ___sound_7)); }
	inline AudioClip_t1932558630 * get_sound_7() const { return ___sound_7; }
	inline AudioClip_t1932558630 ** get_address_of_sound_7() { return &___sound_7; }
	inline void set_sound_7(AudioClip_t1932558630 * value)
	{
		___sound_7 = value;
		Il2CppCodeGenWriteBarrier(&___sound_7, value);
	}

	inline static int32_t get_offset_of_disabledsound_8() { return static_cast<int32_t>(offsetof(DefaultPreviewButton_t12674677, ___disabledsound_8)); }
	inline AudioClip_t1932558630 * get_disabledsound_8() const { return ___disabledsound_8; }
	inline AudioClip_t1932558630 ** get_address_of_disabledsound_8() { return &___disabledsound_8; }
	inline void set_disabledsound_8(AudioClip_t1932558630 * value)
	{
		___disabledsound_8 = value;
		Il2CppCodeGenWriteBarrier(&___disabledsound_8, value);
	}

	inline static int32_t get_offset_of_IsDisabled_9() { return static_cast<int32_t>(offsetof(DefaultPreviewButton_t12674677, ___IsDisabled_9)); }
	inline bool get_IsDisabled_9() const { return ___IsDisabled_9; }
	inline bool* get_address_of_IsDisabled_9() { return &___IsDisabled_9; }
	inline void set_IsDisabled_9(bool value)
	{
		___IsDisabled_9 = value;
	}

	inline static int32_t get_offset_of_ActionClick_10() { return static_cast<int32_t>(offsetof(DefaultPreviewButton_t12674677, ___ActionClick_10)); }
	inline Action_t3226471752 * get_ActionClick_10() const { return ___ActionClick_10; }
	inline Action_t3226471752 ** get_address_of_ActionClick_10() { return &___ActionClick_10; }
	inline void set_ActionClick_10(Action_t3226471752 * value)
	{
		___ActionClick_10 = value;
		Il2CppCodeGenWriteBarrier(&___ActionClick_10, value);
	}
};

struct DefaultPreviewButton_t12674677_StaticFields
{
public:
	// System.Action DefaultPreviewButton::<>f__am$cache9
	Action_t3226471752 * ___U3CU3Ef__amU24cache9_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_11() { return static_cast<int32_t>(offsetof(DefaultPreviewButton_t12674677_StaticFields, ___U3CU3Ef__amU24cache9_11)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache9_11() const { return ___U3CU3Ef__amU24cache9_11; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache9_11() { return &___U3CU3Ef__amU24cache9_11; }
	inline void set_U3CU3Ef__amU24cache9_11(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
