﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JTokenEqualityComparer
struct JTokenEqualityComparer_t711291442;
// Newtonsoft.Json.Linq.JToken
struct JToken_t2552644013;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken2552644013.h"

// System.Void Newtonsoft.Json.Linq.JTokenEqualityComparer::.ctor()
extern "C"  void JTokenEqualityComparer__ctor_m1979166830 (JTokenEqualityComparer_t711291442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenEqualityComparer::Equals(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  bool JTokenEqualityComparer_Equals_m965355851 (JTokenEqualityComparer_t711291442 * __this, JToken_t2552644013 * ___x0, JToken_t2552644013 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JTokenEqualityComparer::GetHashCode(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JTokenEqualityComparer_GetHashCode_m2842059689 (JTokenEqualityComparer_t711291442 * __this, JToken_t2552644013 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
