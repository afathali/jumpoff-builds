﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_UIHightDependence
struct SA_UIHightDependence_t2303649696;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "codegen/il2cpp-codegen.h"

// System.Void SA_UIHightDependence::.ctor()
extern "C"  void SA_UIHightDependence__ctor_m878015997 (SA_UIHightDependence_t2303649696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_UIHightDependence::Awake()
extern "C"  void SA_UIHightDependence_Awake_m2762991386 (SA_UIHightDependence_t2303649696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_UIHightDependence::Update()
extern "C"  void SA_UIHightDependence_Update_m533303276 (SA_UIHightDependence_t2303649696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_UIHightDependence::ApplyTransformation()
extern "C"  void SA_UIHightDependence_ApplyTransformation_m76535612 (SA_UIHightDependence_t2303649696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform SA_UIHightDependence::get_rect()
extern "C"  RectTransform_t3349966182 * SA_UIHightDependence_get_rect_m631336958 (SA_UIHightDependence_t2303649696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_UIHightDependence::OnDetroy()
extern "C"  void SA_UIHightDependence_OnDetroy_m2620108061 (SA_UIHightDependence_t2303649696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
