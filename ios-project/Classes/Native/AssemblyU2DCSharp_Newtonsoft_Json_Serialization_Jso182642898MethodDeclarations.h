﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateAndPopulateList>c__AnonStorey24
struct U3CCreateAndPopulateListU3Ec__AnonStorey24_t182642898;
// System.Collections.IList
struct IList_t3321498491;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateAndPopulateList>c__AnonStorey24::.ctor()
extern "C"  void U3CCreateAndPopulateListU3Ec__AnonStorey24__ctor_m2081178765 (U3CCreateAndPopulateListU3Ec__AnonStorey24_t182642898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateAndPopulateList>c__AnonStorey24::<>m__E0(System.Collections.IList,System.Boolean)
extern "C"  void U3CCreateAndPopulateListU3Ec__AnonStorey24_U3CU3Em__E0_m2713661083 (U3CCreateAndPopulateListU3Ec__AnonStorey24_t182642898 * __this, Il2CppObject * ___l0, bool ___isTemporaryListReference1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
