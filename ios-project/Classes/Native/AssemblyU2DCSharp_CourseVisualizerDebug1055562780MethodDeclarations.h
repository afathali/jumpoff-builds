﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CourseVisualizerDebug
struct CourseVisualizerDebug_t1055562780;

#include "codegen/il2cpp-codegen.h"

// System.Void CourseVisualizerDebug::.ctor()
extern "C"  void CourseVisualizerDebug__ctor_m3156861915 (CourseVisualizerDebug_t1055562780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizerDebug::Start()
extern "C"  void CourseVisualizerDebug_Start_m2823694647 (CourseVisualizerDebug_t1055562780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizerDebug::Update()
extern "C"  void CourseVisualizerDebug_Update_m3750116446 (CourseVisualizerDebug_t1055562780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizerDebug::<Start>m__5()
extern "C"  void CourseVisualizerDebug_U3CStartU3Em__5_m1777809713 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseVisualizerDebug::<Start>m__6()
extern "C"  void CourseVisualizerDebug_U3CStartU3Em__6_m1636647212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
