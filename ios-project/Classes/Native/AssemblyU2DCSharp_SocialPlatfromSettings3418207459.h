﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// SocialPlatfromSettings
struct SocialPlatfromSettings_t3418207459;

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocialPlatfromSettings
struct  SocialPlatfromSettings_t3418207459  : public ScriptableObject_t1975622470
{
public:
	// System.Boolean SocialPlatfromSettings::ShowImageSharingSettings
	bool ___ShowImageSharingSettings_7;
	// System.Boolean SocialPlatfromSettings::SaveImageToGallery
	bool ___SaveImageToGallery_8;
	// System.Boolean SocialPlatfromSettings::showPermitions
	bool ___showPermitions_9;
	// System.Boolean SocialPlatfromSettings::ShowActions
	bool ___ShowActions_10;
	// System.Boolean SocialPlatfromSettings::ShowAPIS
	bool ___ShowAPIS_11;
	// System.Collections.Generic.List`1<System.String> SocialPlatfromSettings::fb_scopes_list
	List_1_t1398341365 * ___fb_scopes_list_12;
	// System.String SocialPlatfromSettings::TWITTER_CONSUMER_KEY
	String_t* ___TWITTER_CONSUMER_KEY_13;
	// System.String SocialPlatfromSettings::TWITTER_CONSUMER_SECRET
	String_t* ___TWITTER_CONSUMER_SECRET_14;
	// System.String SocialPlatfromSettings::TWITTER_ACCESS_TOKEN
	String_t* ___TWITTER_ACCESS_TOKEN_15;
	// System.String SocialPlatfromSettings::TWITTER_ACCESS_TOKEN_SECRET
	String_t* ___TWITTER_ACCESS_TOKEN_SECRET_16;
	// System.Boolean SocialPlatfromSettings::ShowEditorOauthTestingBlock
	bool ___ShowEditorOauthTestingBlock_17;
	// System.Boolean SocialPlatfromSettings::TwitterAPI
	bool ___TwitterAPI_18;
	// System.Boolean SocialPlatfromSettings::NativeSharingAPI
	bool ___NativeSharingAPI_19;
	// System.Boolean SocialPlatfromSettings::InstagramAPI
	bool ___InstagramAPI_20;
	// System.Boolean SocialPlatfromSettings::EnableImageSharing
	bool ___EnableImageSharing_21;
	// System.Boolean SocialPlatfromSettings::KeepManifestClean
	bool ___KeepManifestClean_22;

public:
	inline static int32_t get_offset_of_ShowImageSharingSettings_7() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___ShowImageSharingSettings_7)); }
	inline bool get_ShowImageSharingSettings_7() const { return ___ShowImageSharingSettings_7; }
	inline bool* get_address_of_ShowImageSharingSettings_7() { return &___ShowImageSharingSettings_7; }
	inline void set_ShowImageSharingSettings_7(bool value)
	{
		___ShowImageSharingSettings_7 = value;
	}

	inline static int32_t get_offset_of_SaveImageToGallery_8() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___SaveImageToGallery_8)); }
	inline bool get_SaveImageToGallery_8() const { return ___SaveImageToGallery_8; }
	inline bool* get_address_of_SaveImageToGallery_8() { return &___SaveImageToGallery_8; }
	inline void set_SaveImageToGallery_8(bool value)
	{
		___SaveImageToGallery_8 = value;
	}

	inline static int32_t get_offset_of_showPermitions_9() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___showPermitions_9)); }
	inline bool get_showPermitions_9() const { return ___showPermitions_9; }
	inline bool* get_address_of_showPermitions_9() { return &___showPermitions_9; }
	inline void set_showPermitions_9(bool value)
	{
		___showPermitions_9 = value;
	}

	inline static int32_t get_offset_of_ShowActions_10() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___ShowActions_10)); }
	inline bool get_ShowActions_10() const { return ___ShowActions_10; }
	inline bool* get_address_of_ShowActions_10() { return &___ShowActions_10; }
	inline void set_ShowActions_10(bool value)
	{
		___ShowActions_10 = value;
	}

	inline static int32_t get_offset_of_ShowAPIS_11() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___ShowAPIS_11)); }
	inline bool get_ShowAPIS_11() const { return ___ShowAPIS_11; }
	inline bool* get_address_of_ShowAPIS_11() { return &___ShowAPIS_11; }
	inline void set_ShowAPIS_11(bool value)
	{
		___ShowAPIS_11 = value;
	}

	inline static int32_t get_offset_of_fb_scopes_list_12() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___fb_scopes_list_12)); }
	inline List_1_t1398341365 * get_fb_scopes_list_12() const { return ___fb_scopes_list_12; }
	inline List_1_t1398341365 ** get_address_of_fb_scopes_list_12() { return &___fb_scopes_list_12; }
	inline void set_fb_scopes_list_12(List_1_t1398341365 * value)
	{
		___fb_scopes_list_12 = value;
		Il2CppCodeGenWriteBarrier(&___fb_scopes_list_12, value);
	}

	inline static int32_t get_offset_of_TWITTER_CONSUMER_KEY_13() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___TWITTER_CONSUMER_KEY_13)); }
	inline String_t* get_TWITTER_CONSUMER_KEY_13() const { return ___TWITTER_CONSUMER_KEY_13; }
	inline String_t** get_address_of_TWITTER_CONSUMER_KEY_13() { return &___TWITTER_CONSUMER_KEY_13; }
	inline void set_TWITTER_CONSUMER_KEY_13(String_t* value)
	{
		___TWITTER_CONSUMER_KEY_13 = value;
		Il2CppCodeGenWriteBarrier(&___TWITTER_CONSUMER_KEY_13, value);
	}

	inline static int32_t get_offset_of_TWITTER_CONSUMER_SECRET_14() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___TWITTER_CONSUMER_SECRET_14)); }
	inline String_t* get_TWITTER_CONSUMER_SECRET_14() const { return ___TWITTER_CONSUMER_SECRET_14; }
	inline String_t** get_address_of_TWITTER_CONSUMER_SECRET_14() { return &___TWITTER_CONSUMER_SECRET_14; }
	inline void set_TWITTER_CONSUMER_SECRET_14(String_t* value)
	{
		___TWITTER_CONSUMER_SECRET_14 = value;
		Il2CppCodeGenWriteBarrier(&___TWITTER_CONSUMER_SECRET_14, value);
	}

	inline static int32_t get_offset_of_TWITTER_ACCESS_TOKEN_15() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___TWITTER_ACCESS_TOKEN_15)); }
	inline String_t* get_TWITTER_ACCESS_TOKEN_15() const { return ___TWITTER_ACCESS_TOKEN_15; }
	inline String_t** get_address_of_TWITTER_ACCESS_TOKEN_15() { return &___TWITTER_ACCESS_TOKEN_15; }
	inline void set_TWITTER_ACCESS_TOKEN_15(String_t* value)
	{
		___TWITTER_ACCESS_TOKEN_15 = value;
		Il2CppCodeGenWriteBarrier(&___TWITTER_ACCESS_TOKEN_15, value);
	}

	inline static int32_t get_offset_of_TWITTER_ACCESS_TOKEN_SECRET_16() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___TWITTER_ACCESS_TOKEN_SECRET_16)); }
	inline String_t* get_TWITTER_ACCESS_TOKEN_SECRET_16() const { return ___TWITTER_ACCESS_TOKEN_SECRET_16; }
	inline String_t** get_address_of_TWITTER_ACCESS_TOKEN_SECRET_16() { return &___TWITTER_ACCESS_TOKEN_SECRET_16; }
	inline void set_TWITTER_ACCESS_TOKEN_SECRET_16(String_t* value)
	{
		___TWITTER_ACCESS_TOKEN_SECRET_16 = value;
		Il2CppCodeGenWriteBarrier(&___TWITTER_ACCESS_TOKEN_SECRET_16, value);
	}

	inline static int32_t get_offset_of_ShowEditorOauthTestingBlock_17() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___ShowEditorOauthTestingBlock_17)); }
	inline bool get_ShowEditorOauthTestingBlock_17() const { return ___ShowEditorOauthTestingBlock_17; }
	inline bool* get_address_of_ShowEditorOauthTestingBlock_17() { return &___ShowEditorOauthTestingBlock_17; }
	inline void set_ShowEditorOauthTestingBlock_17(bool value)
	{
		___ShowEditorOauthTestingBlock_17 = value;
	}

	inline static int32_t get_offset_of_TwitterAPI_18() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___TwitterAPI_18)); }
	inline bool get_TwitterAPI_18() const { return ___TwitterAPI_18; }
	inline bool* get_address_of_TwitterAPI_18() { return &___TwitterAPI_18; }
	inline void set_TwitterAPI_18(bool value)
	{
		___TwitterAPI_18 = value;
	}

	inline static int32_t get_offset_of_NativeSharingAPI_19() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___NativeSharingAPI_19)); }
	inline bool get_NativeSharingAPI_19() const { return ___NativeSharingAPI_19; }
	inline bool* get_address_of_NativeSharingAPI_19() { return &___NativeSharingAPI_19; }
	inline void set_NativeSharingAPI_19(bool value)
	{
		___NativeSharingAPI_19 = value;
	}

	inline static int32_t get_offset_of_InstagramAPI_20() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___InstagramAPI_20)); }
	inline bool get_InstagramAPI_20() const { return ___InstagramAPI_20; }
	inline bool* get_address_of_InstagramAPI_20() { return &___InstagramAPI_20; }
	inline void set_InstagramAPI_20(bool value)
	{
		___InstagramAPI_20 = value;
	}

	inline static int32_t get_offset_of_EnableImageSharing_21() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___EnableImageSharing_21)); }
	inline bool get_EnableImageSharing_21() const { return ___EnableImageSharing_21; }
	inline bool* get_address_of_EnableImageSharing_21() { return &___EnableImageSharing_21; }
	inline void set_EnableImageSharing_21(bool value)
	{
		___EnableImageSharing_21 = value;
	}

	inline static int32_t get_offset_of_KeepManifestClean_22() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459, ___KeepManifestClean_22)); }
	inline bool get_KeepManifestClean_22() const { return ___KeepManifestClean_22; }
	inline bool* get_address_of_KeepManifestClean_22() { return &___KeepManifestClean_22; }
	inline void set_KeepManifestClean_22(bool value)
	{
		___KeepManifestClean_22 = value;
	}
};

struct SocialPlatfromSettings_t3418207459_StaticFields
{
public:
	// SocialPlatfromSettings SocialPlatfromSettings::instance
	SocialPlatfromSettings_t3418207459 * ___instance_23;

public:
	inline static int32_t get_offset_of_instance_23() { return static_cast<int32_t>(offsetof(SocialPlatfromSettings_t3418207459_StaticFields, ___instance_23)); }
	inline SocialPlatfromSettings_t3418207459 * get_instance_23() const { return ___instance_23; }
	inline SocialPlatfromSettings_t3418207459 ** get_address_of_instance_23() { return &___instance_23; }
	inline void set_instance_23(SocialPlatfromSettings_t3418207459 * value)
	{
		___instance_23 = value;
		Il2CppCodeGenWriteBarrier(&___instance_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
