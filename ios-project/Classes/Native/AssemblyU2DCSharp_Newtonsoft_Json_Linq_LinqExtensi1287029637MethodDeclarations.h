﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>
struct U3CConvertU3Ec__Iterator13_2_t1287029637;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CConvertU3Ec__Iterator13_2__ctor_m2062413161_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator13_2__ctor_m2062413161(__this, method) ((  void (*) (U3CConvertU3Ec__Iterator13_2_t1287029637 *, const MethodInfo*))U3CConvertU3Ec__Iterator13_2__ctor_m2062413161_gshared)(__this, method)
// U Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<U>.get_Current()
extern "C"  Il2CppObject * U3CConvertU3Ec__Iterator13_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m423894566_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator13_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m423894566(__this, method) ((  Il2CppObject * (*) (U3CConvertU3Ec__Iterator13_2_t1287029637 *, const MethodInfo*))U3CConvertU3Ec__Iterator13_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m423894566_gshared)(__this, method)
// System.Object Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CConvertU3Ec__Iterator13_2_System_Collections_IEnumerator_get_Current_m2590672771_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator13_2_System_Collections_IEnumerator_get_Current_m2590672771(__this, method) ((  Il2CppObject * (*) (U3CConvertU3Ec__Iterator13_2_t1287029637 *, const MethodInfo*))U3CConvertU3Ec__Iterator13_2_System_Collections_IEnumerator_get_Current_m2590672771_gshared)(__this, method)
// System.Collections.IEnumerator Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CConvertU3Ec__Iterator13_2_System_Collections_IEnumerable_GetEnumerator_m2427907510_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator13_2_System_Collections_IEnumerable_GetEnumerator_m2427907510(__this, method) ((  Il2CppObject * (*) (U3CConvertU3Ec__Iterator13_2_t1287029637 *, const MethodInfo*))U3CConvertU3Ec__Iterator13_2_System_Collections_IEnumerable_GetEnumerator_m2427907510_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<U> Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<U>.GetEnumerator()
extern "C"  Il2CppObject* U3CConvertU3Ec__Iterator13_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m4188189733_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator13_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m4188189733(__this, method) ((  Il2CppObject* (*) (U3CConvertU3Ec__Iterator13_2_t1287029637 *, const MethodInfo*))U3CConvertU3Ec__Iterator13_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m4188189733_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::MoveNext()
extern "C"  bool U3CConvertU3Ec__Iterator13_2_MoveNext_m1934812863_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator13_2_MoveNext_m1934812863(__this, method) ((  bool (*) (U3CConvertU3Ec__Iterator13_2_t1287029637 *, const MethodInfo*))U3CConvertU3Ec__Iterator13_2_MoveNext_m1934812863_gshared)(__this, method)
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CConvertU3Ec__Iterator13_2_Dispose_m4160366314_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator13_2_Dispose_m4160366314(__this, method) ((  void (*) (U3CConvertU3Ec__Iterator13_2_t1287029637 *, const MethodInfo*))U3CConvertU3Ec__Iterator13_2_Dispose_m4160366314_gshared)(__this, method)
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Convert>c__Iterator13`2<System.Object,System.Object>::Reset()
extern "C"  void U3CConvertU3Ec__Iterator13_2_Reset_m398725548_gshared (U3CConvertU3Ec__Iterator13_2_t1287029637 * __this, const MethodInfo* method);
#define U3CConvertU3Ec__Iterator13_2_Reset_m398725548(__this, method) ((  void (*) (U3CConvertU3Ec__Iterator13_2_t1287029637 *, const MethodInfo*))U3CConvertU3Ec__Iterator13_2_Reset_m398725548_gshared)(__this, method)
