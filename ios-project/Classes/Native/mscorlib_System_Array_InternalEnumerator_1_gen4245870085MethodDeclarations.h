﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4245870085.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23387117823.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1196469849_gshared (InternalEnumerator_1_t4245870085 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1196469849(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4245870085 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1196469849_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2878584445_gshared (InternalEnumerator_1_t4245870085 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2878584445(__this, method) ((  void (*) (InternalEnumerator_1_t4245870085 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2878584445_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2664753461_gshared (InternalEnumerator_1_t4245870085 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2664753461(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4245870085 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2664753461_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1054080826_gshared (InternalEnumerator_1_t4245870085 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1054080826(__this, method) ((  void (*) (InternalEnumerator_1_t4245870085 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1054080826_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4085647429_gshared (InternalEnumerator_1_t4245870085 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4085647429(__this, method) ((  bool (*) (InternalEnumerator_1_t4245870085 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4085647429_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>>::get_Current()
extern "C"  KeyValuePair_2_t3387117823  InternalEnumerator_1_get_Current_m3296750224_gshared (InternalEnumerator_1_t4245870085 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3296750224(__this, method) ((  KeyValuePair_2_t3387117823  (*) (InternalEnumerator_1_t4245870085 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3296750224_gshared)(__this, method)
