﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Util.IdFactory/<>c__AnonStoreyD
struct U3CU3Ec__AnonStoreyD_t255967280;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SA.Common.Util.IdFactory/<>c__AnonStoreyD::.ctor()
extern "C"  void U3CU3Ec__AnonStoreyD__ctor_m3159941899 (U3CU3Ec__AnonStoreyD_t255967280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char SA.Common.Util.IdFactory/<>c__AnonStoreyD::<>m__80(System.String)
extern "C"  Il2CppChar U3CU3Ec__AnonStoreyD_U3CU3Em__80_m177739854 (U3CU3Ec__AnonStoreyD_t255967280 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
