﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iCloudData
struct iCloudData_t3080637488;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void iCloudData::.ctor(System.String,System.String)
extern "C"  void iCloudData__ctor_m1792848533 (iCloudData_t3080637488 * __this, String_t* ___k0, String_t* ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String iCloudData::get_key()
extern "C"  String_t* iCloudData_get_key_m3393039034 (iCloudData_t3080637488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String iCloudData::get_stringValue()
extern "C"  String_t* iCloudData_get_stringValue_m413251455 (iCloudData_t3080637488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single iCloudData::get_floatValue()
extern "C"  float iCloudData_get_floatValue_m1132307725 (iCloudData_t3080637488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] iCloudData::get_bytesValue()
extern "C"  ByteU5BU5D_t3397334013* iCloudData_get_bytesValue_m992852588 (iCloudData_t3080637488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iCloudData::get_IsEmpty()
extern "C"  bool iCloudData_get_IsEmpty_m3971846247 (iCloudData_t3080637488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
