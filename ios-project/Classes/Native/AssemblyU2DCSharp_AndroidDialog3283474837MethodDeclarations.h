﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidDialog
struct AndroidDialog_t3283474837;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AndroidDialog::.ctor()
extern "C"  void AndroidDialog__ctor_m2968750230 (AndroidDialog_t3283474837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AndroidDialog AndroidDialog::Create(System.String,System.String)
extern "C"  AndroidDialog_t3283474837 * AndroidDialog_Create_m1166781034 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AndroidDialog AndroidDialog::Create(System.String,System.String,System.String,System.String)
extern "C"  AndroidDialog_t3283474837 * AndroidDialog_Create_m2249185578 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___yes2, String_t* ___no3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidDialog::init()
extern "C"  void AndroidDialog_init_m529125578 (AndroidDialog_t3283474837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidDialog::onPopUpCallBack(System.String)
extern "C"  void AndroidDialog_onPopUpCallBack_m1118931470 (AndroidDialog_t3283474837 * __this, String_t* ___buttonIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
