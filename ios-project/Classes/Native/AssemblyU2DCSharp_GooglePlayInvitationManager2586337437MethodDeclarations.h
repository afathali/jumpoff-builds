﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayInvitationManager
struct GooglePlayInvitationManager_t2586337437;
// System.Action`1<GP_Invite>
struct Action_1_t428728469;
// System.Action`1<System.Collections.Generic.List`1<GP_Invite>>
struct Action_1_t4092816897;
// System.Action`1<AN_InvitationInboxCloseResult>
struct Action_1_t1085927992;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.String
struct String_t;
// GP_Invite
struct GP_Invite_t626929087;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<GP_Invite>
struct List_1_t4291017515;
// AN_InvitationInboxCloseResult
struct AN_InvitationInboxCloseResult_t1284128610;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_Invite626929087.h"
#include "AssemblyU2DCSharp_AN_InvitationInboxCloseResult1284128610.h"

// System.Void GooglePlayInvitationManager::.ctor()
extern "C"  void GooglePlayInvitationManager__ctor_m2503587968 (GooglePlayInvitationManager_t2586337437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::.cctor()
extern "C"  void GooglePlayInvitationManager__cctor_m2020194191 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::add_ActionInvitationReceived(System.Action`1<GP_Invite>)
extern "C"  void GooglePlayInvitationManager_add_ActionInvitationReceived_m2833020309 (Il2CppObject * __this /* static, unused */, Action_1_t428728469 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::remove_ActionInvitationReceived(System.Action`1<GP_Invite>)
extern "C"  void GooglePlayInvitationManager_remove_ActionInvitationReceived_m2831567112 (Il2CppObject * __this /* static, unused */, Action_1_t428728469 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::add_ActionInvitationAccepted(System.Action`1<GP_Invite>)
extern "C"  void GooglePlayInvitationManager_add_ActionInvitationAccepted_m3477725115 (Il2CppObject * __this /* static, unused */, Action_1_t428728469 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::remove_ActionInvitationAccepted(System.Action`1<GP_Invite>)
extern "C"  void GooglePlayInvitationManager_remove_ActionInvitationAccepted_m3880766226 (Il2CppObject * __this /* static, unused */, Action_1_t428728469 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::add_ActionInvitationsListLoaded(System.Action`1<System.Collections.Generic.List`1<GP_Invite>>)
extern "C"  void GooglePlayInvitationManager_add_ActionInvitationsListLoaded_m1508569170 (Il2CppObject * __this /* static, unused */, Action_1_t4092816897 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::remove_ActionInvitationsListLoaded(System.Action`1<System.Collections.Generic.List`1<GP_Invite>>)
extern "C"  void GooglePlayInvitationManager_remove_ActionInvitationsListLoaded_m1019613537 (Il2CppObject * __this /* static, unused */, Action_1_t4092816897 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::add_ActionInvitationInboxClosed(System.Action`1<AN_InvitationInboxCloseResult>)
extern "C"  void GooglePlayInvitationManager_add_ActionInvitationInboxClosed_m1331598415 (Il2CppObject * __this /* static, unused */, Action_1_t1085927992 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::remove_ActionInvitationInboxClosed(System.Action`1<AN_InvitationInboxCloseResult>)
extern "C"  void GooglePlayInvitationManager_remove_ActionInvitationInboxClosed_m2524684726 (Il2CppObject * __this /* static, unused */, Action_1_t1085927992 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::add_ActionInvitationRemoved(System.Action`1<System.String>)
extern "C"  void GooglePlayInvitationManager_add_ActionInvitationRemoved_m3281487339 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::remove_ActionInvitationRemoved(System.Action`1<System.String>)
extern "C"  void GooglePlayInvitationManager_remove_ActionInvitationRemoved_m53314308 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::Awake()
extern "C"  void GooglePlayInvitationManager_Awake_m1108650505 (GooglePlayInvitationManager_t2586337437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::Init()
extern "C"  void GooglePlayInvitationManager_Init_m871438620 (GooglePlayInvitationManager_t2586337437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::OnInvitationReceived(System.String)
extern "C"  void GooglePlayInvitationManager_OnInvitationReceived_m323213969 (GooglePlayInvitationManager_t2586337437 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::OnInvitationAccepted(System.String)
extern "C"  void GooglePlayInvitationManager_OnInvitationAccepted_m2850373935 (GooglePlayInvitationManager_t2586337437 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::OnInvitationRemoved(System.String)
extern "C"  void GooglePlayInvitationManager_OnInvitationRemoved_m3233121092 (GooglePlayInvitationManager_t2586337437 * __this, String_t* ___invId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::OnInvitationBoxUiClosed(System.String)
extern "C"  void GooglePlayInvitationManager_OnInvitationBoxUiClosed_m1872436041 (GooglePlayInvitationManager_t2586337437 * __this, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::OnLoadInvitationsResult(System.String)
extern "C"  void GooglePlayInvitationManager_OnLoadInvitationsResult_m2776020548 (GooglePlayInvitationManager_t2586337437 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_Invite GooglePlayInvitationManager::InviteFromString(System.String[],System.Int32)
extern "C"  GP_Invite_t626929087 * GooglePlayInvitationManager_InviteFromString_m127971685 (GooglePlayInvitationManager_t2586337437 * __this, StringU5BU5D_t1642385972* ___storeData0, int32_t ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::RegisterInvitationListener()
extern "C"  void GooglePlayInvitationManager_RegisterInvitationListener_m907248016 (GooglePlayInvitationManager_t2586337437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::UnregisterInvitationListener()
extern "C"  void GooglePlayInvitationManager_UnregisterInvitationListener_m3609840201 (GooglePlayInvitationManager_t2586337437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::LoadInvitations()
extern "C"  void GooglePlayInvitationManager_LoadInvitations_m923320244 (GooglePlayInvitationManager_t2586337437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::<ActionInvitationReceived>m__3C(GP_Invite)
extern "C"  void GooglePlayInvitationManager_U3CActionInvitationReceivedU3Em__3C_m4226108784 (Il2CppObject * __this /* static, unused */, GP_Invite_t626929087 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::<ActionInvitationAccepted>m__3D(GP_Invite)
extern "C"  void GooglePlayInvitationManager_U3CActionInvitationAcceptedU3Em__3D_m723985979 (Il2CppObject * __this /* static, unused */, GP_Invite_t626929087 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::<ActionInvitationsListLoaded>m__3E(System.Collections.Generic.List`1<GP_Invite>)
extern "C"  void GooglePlayInvitationManager_U3CActionInvitationsListLoadedU3Em__3E_m3035205391 (Il2CppObject * __this /* static, unused */, List_1_t4291017515 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::<ActionInvitationInboxClosed>m__3F(AN_InvitationInboxCloseResult)
extern "C"  void GooglePlayInvitationManager_U3CActionInvitationInboxClosedU3Em__3F_m2236813029 (Il2CppObject * __this /* static, unused */, AN_InvitationInboxCloseResult_t1284128610 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayInvitationManager::<ActionInvitationRemoved>m__40(System.String)
extern "C"  void GooglePlayInvitationManager_U3CActionInvitationRemovedU3Em__40_m3267125156 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
