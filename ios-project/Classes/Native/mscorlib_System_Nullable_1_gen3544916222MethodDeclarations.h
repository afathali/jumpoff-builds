﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3544916222.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Nullable`1<System.UInt16>::.ctor(T)
extern "C"  void Nullable_1__ctor_m960711409_gshared (Nullable_1_t3544916222 * __this, uint16_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m960711409(__this, ___value0, method) ((  void (*) (Nullable_1_t3544916222 *, uint16_t, const MethodInfo*))Nullable_1__ctor_m960711409_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.UInt16>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3144126760_gshared (Nullable_1_t3544916222 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m3144126760(__this, method) ((  bool (*) (Nullable_1_t3544916222 *, const MethodInfo*))Nullable_1_get_HasValue_m3144126760_gshared)(__this, method)
// T System.Nullable`1<System.UInt16>::get_Value()
extern "C"  uint16_t Nullable_1_get_Value_m3580999918_gshared (Nullable_1_t3544916222 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3580999918(__this, method) ((  uint16_t (*) (Nullable_1_t3544916222 *, const MethodInfo*))Nullable_1_get_Value_m3580999918_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.UInt16>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3624552446_gshared (Nullable_1_t3544916222 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3624552446(__this, ___other0, method) ((  bool (*) (Nullable_1_t3544916222 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3624552446_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<System.UInt16>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2954062977_gshared (Nullable_1_t3544916222 * __this, Nullable_1_t3544916222  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2954062977(__this, ___other0, method) ((  bool (*) (Nullable_1_t3544916222 *, Nullable_1_t3544916222 , const MethodInfo*))Nullable_1_Equals_m2954062977_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<System.UInt16>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3718174238_gshared (Nullable_1_t3544916222 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3718174238(__this, method) ((  int32_t (*) (Nullable_1_t3544916222 *, const MethodInfo*))Nullable_1_GetHashCode_m3718174238_gshared)(__this, method)
// T System.Nullable`1<System.UInt16>::GetValueOrDefault()
extern "C"  uint16_t Nullable_1_GetValueOrDefault_m257080427_gshared (Nullable_1_t3544916222 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m257080427(__this, method) ((  uint16_t (*) (Nullable_1_t3544916222 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m257080427_gshared)(__this, method)
// T System.Nullable`1<System.UInt16>::GetValueOrDefault(T)
extern "C"  uint16_t Nullable_1_GetValueOrDefault_m3275284163_gshared (Nullable_1_t3544916222 * __this, uint16_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3275284163(__this, ___defaultValue0, method) ((  uint16_t (*) (Nullable_1_t3544916222 *, uint16_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m3275284163_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<System.UInt16>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3771839058_gshared (Nullable_1_t3544916222 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3771839058(__this, method) ((  String_t* (*) (Nullable_1_t3544916222 *, const MethodInfo*))Nullable_1_ToString_m3771839058_gshared)(__this, method)
