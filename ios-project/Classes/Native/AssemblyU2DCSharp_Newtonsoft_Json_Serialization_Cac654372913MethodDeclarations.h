﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Cac666803094MethodDeclarations.h"

// System.Void Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Runtime.Serialization.DataMemberAttribute>::.cctor()
#define CachedAttributeGetter_1__cctor_m30050712(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))CachedAttributeGetter_1__cctor_m1655531471_gshared)(__this /* static, unused */, method)
// T Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Runtime.Serialization.DataMemberAttribute>::GetAttribute(System.Reflection.ICustomAttributeProvider)
#define CachedAttributeGetter_1_GetAttribute_m4113850803(__this /* static, unused */, ___type0, method) ((  DataMemberAttribute_t2677019114 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))CachedAttributeGetter_1_GetAttribute_m2147397194_gshared)(__this /* static, unused */, ___type0, method)
