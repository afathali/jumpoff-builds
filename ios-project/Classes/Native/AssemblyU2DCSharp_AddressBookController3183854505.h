﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<AndroidContactInfo>
struct List_1_t1487793311;
// System.Action
struct Action_t3226471752;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen179731127.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AddressBookController
struct  AddressBookController_t3183854505  : public SA_Singleton_OLD_1_t179731127
{
public:
	// System.Collections.Generic.List`1<AndroidContactInfo> AddressBookController::_contacts
	List_1_t1487793311 * ____contacts_8;

public:
	inline static int32_t get_offset_of__contacts_8() { return static_cast<int32_t>(offsetof(AddressBookController_t3183854505, ____contacts_8)); }
	inline List_1_t1487793311 * get__contacts_8() const { return ____contacts_8; }
	inline List_1_t1487793311 ** get_address_of__contacts_8() { return &____contacts_8; }
	inline void set__contacts_8(List_1_t1487793311 * value)
	{
		____contacts_8 = value;
		Il2CppCodeGenWriteBarrier(&____contacts_8, value);
	}
};

struct AddressBookController_t3183854505_StaticFields
{
public:
	// System.Boolean AddressBookController::_isLoaded
	bool ____isLoaded_7;
	// System.Action AddressBookController::OnContactsLoadedAction
	Action_t3226471752 * ___OnContactsLoadedAction_9;
	// System.Action AddressBookController::<>f__am$cache3
	Action_t3226471752 * ___U3CU3Ef__amU24cache3_10;

public:
	inline static int32_t get_offset_of__isLoaded_7() { return static_cast<int32_t>(offsetof(AddressBookController_t3183854505_StaticFields, ____isLoaded_7)); }
	inline bool get__isLoaded_7() const { return ____isLoaded_7; }
	inline bool* get_address_of__isLoaded_7() { return &____isLoaded_7; }
	inline void set__isLoaded_7(bool value)
	{
		____isLoaded_7 = value;
	}

	inline static int32_t get_offset_of_OnContactsLoadedAction_9() { return static_cast<int32_t>(offsetof(AddressBookController_t3183854505_StaticFields, ___OnContactsLoadedAction_9)); }
	inline Action_t3226471752 * get_OnContactsLoadedAction_9() const { return ___OnContactsLoadedAction_9; }
	inline Action_t3226471752 ** get_address_of_OnContactsLoadedAction_9() { return &___OnContactsLoadedAction_9; }
	inline void set_OnContactsLoadedAction_9(Action_t3226471752 * value)
	{
		___OnContactsLoadedAction_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnContactsLoadedAction_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_10() { return static_cast<int32_t>(offsetof(AddressBookController_t3183854505_StaticFields, ___U3CU3Ef__amU24cache3_10)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache3_10() const { return ___U3CU3Ef__amU24cache3_10; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache3_10() { return &___U3CU3Ef__amU24cache3_10; }
	inline void set_U3CU3Ef__amU24cache3_10(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache3_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
