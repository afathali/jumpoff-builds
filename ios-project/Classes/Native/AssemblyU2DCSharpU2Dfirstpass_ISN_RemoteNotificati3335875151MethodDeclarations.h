﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_RemoteNotificationsRegistrationResult
struct ISN_RemoteNotificationsRegistrationResult_t3335875151;
// ISN_DeviceToken
struct ISN_DeviceToken_t380973950;
// SA.Common.Models.Error
struct Error_t445207774;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_DeviceToken380973950.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"

// System.Void ISN_RemoteNotificationsRegistrationResult::.ctor(ISN_DeviceToken)
extern "C"  void ISN_RemoteNotificationsRegistrationResult__ctor_m3948303164 (ISN_RemoteNotificationsRegistrationResult_t3335875151 * __this, ISN_DeviceToken_t380973950 * ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_RemoteNotificationsRegistrationResult::.ctor(SA.Common.Models.Error)
extern "C"  void ISN_RemoteNotificationsRegistrationResult__ctor_m1151754285 (ISN_RemoteNotificationsRegistrationResult_t3335875151 * __this, Error_t445207774 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ISN_DeviceToken ISN_RemoteNotificationsRegistrationResult::get_Token()
extern "C"  ISN_DeviceToken_t380973950 * ISN_RemoteNotificationsRegistrationResult_get_Token_m2178157773 (ISN_RemoteNotificationsRegistrationResult_t3335875151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
