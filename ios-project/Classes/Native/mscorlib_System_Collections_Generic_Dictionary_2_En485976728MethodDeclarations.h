﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3482886271(__this, ___dictionary0, method) ((  void (*) (Enumerator_t485976728 *, Dictionary_2_t3460919322 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m380678766(__this, method) ((  Il2CppObject * (*) (Enumerator_t485976728 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m77096200(__this, method) ((  void (*) (Enumerator_t485976728 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1043885077(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t485976728 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2851418472(__this, method) ((  Il2CppObject * (*) (Enumerator_t485976728 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1400740738(__this, method) ((  Il2CppObject * (*) (Enumerator_t485976728 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::MoveNext()
#define Enumerator_MoveNext_m4255646040(__this, method) ((  bool (*) (Enumerator_t485976728 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::get_Current()
#define Enumerator_get_Current_m4243843120(__this, method) ((  KeyValuePair_2_t1218264544  (*) (Enumerator_t485976728 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m4008289651(__this, method) ((  int32_t (*) (Enumerator_t485976728 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1596016851(__this, method) ((  GP_LocalPlayerScoreUpdateListener_t158126391 * (*) (Enumerator_t485976728 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::Reset()
#define Enumerator_Reset_m1255322933(__this, method) ((  void (*) (Enumerator_t485976728 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::VerifyState()
#define Enumerator_VerifyState_m2425356328(__this, method) ((  void (*) (Enumerator_t485976728 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2650200448(__this, method) ((  void (*) (Enumerator_t485976728 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,GP_LocalPlayerScoreUpdateListener>::Dispose()
#define Enumerator_Dispose_m3258507027(__this, method) ((  void (*) (Enumerator_t485976728 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
