﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.IOSNative.Contacts.ContactsResult
struct ContactsResult_t1045731284;
// System.Collections.Generic.List`1<SA.IOSNative.Contacts.Contact>
struct List_1_t3547515930;
// SA.Common.Models.Error
struct Error_t445207774;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"

// System.Void SA.IOSNative.Contacts.ContactsResult::.ctor(System.Collections.Generic.List`1<SA.IOSNative.Contacts.Contact>)
extern "C"  void ContactsResult__ctor_m266668425 (ContactsResult_t1045731284 * __this, List_1_t3547515930 * ___contacts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.IOSNative.Contacts.ContactsResult::.ctor(SA.Common.Models.Error)
extern "C"  void ContactsResult__ctor_m669686757 (ContactsResult_t1045731284 * __this, Error_t445207774 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<SA.IOSNative.Contacts.Contact> SA.IOSNative.Contacts.ContactsResult::get_Contacts()
extern "C"  List_1_t3547515930 * ContactsResult_get_Contacts_m368706388 (ContactsResult_t1045731284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
