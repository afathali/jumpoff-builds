﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidActivityResult
struct AndroidActivityResult_t3757510801;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AdroidActivityResultCodes2848269969.h"

// System.Void AndroidActivityResult::.ctor(System.String,System.String)
extern "C"  void AndroidActivityResult__ctor_m2059396736 (AndroidActivityResult_t3757510801 * __this, String_t* ___rId0, String_t* ___codeString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AdroidActivityResultCodes AndroidActivityResult::get_code()
extern "C"  int32_t AndroidActivityResult_get_code_m3506721132 (AndroidActivityResult_t3757510801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AndroidActivityResult::get_requestId()
extern "C"  int32_t AndroidActivityResult_get_requestId_m3374619027 (AndroidActivityResult_t3757510801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidActivityResult::get_IsSucceeded()
extern "C"  bool AndroidActivityResult_get_IsSucceeded_m2494212990 (AndroidActivityResult_t3757510801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidActivityResult::get_IsFailed()
extern "C"  bool AndroidActivityResult_get_IsFailed_m2969319938 (AndroidActivityResult_t3757510801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
