﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLib.ExifTag
struct ExifTag_t1511511074;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// ExifLib.JpegInfo
struct JpegInfo_t3114827956;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLib_ExifTagFormat3078035447.h"
#include "AssemblyU2DCSharp_ExifLib_JpegInfo3114827956.h"
#include "AssemblyU2DCSharp_ExifLib_ExifIFD1652672661.h"

// System.Void ExifLib.ExifTag::.ctor(System.Byte[],System.Int32,System.Int32,System.Int32,System.Boolean)
extern "C"  void ExifTag__ctor_m3222885437 (ExifTag_t1511511074 * __this, ByteU5BU5D_t3397334013* ___section0, int32_t ___sectionOffset1, int32_t ___offsetBase2, int32_t ___length3, bool ___littleEndian4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLib.ExifTag::.cctor()
extern "C"  void ExifTag__cctor_m3691342259 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLib.ExifTag::get_Tag()
extern "C"  int32_t ExifTag_get_Tag_m4228053797 (ExifTag_t1511511074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLib.ExifTag::set_Tag(System.Int32)
extern "C"  void ExifTag_set_Tag_m3364972766 (ExifTag_t1511511074 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLib.ExifTagFormat ExifLib.ExifTag::get_Format()
extern "C"  int32_t ExifTag_get_Format_m2310863449 (ExifTag_t1511511074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLib.ExifTag::set_Format(ExifLib.ExifTagFormat)
extern "C"  void ExifTag_set_Format_m520561692 (ExifTag_t1511511074 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLib.ExifTag::get_Components()
extern "C"  int32_t ExifTag_get_Components_m281882391 (ExifTag_t1511511074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLib.ExifTag::set_Components(System.Int32)
extern "C"  void ExifTag_set_Components_m2881307642 (ExifTag_t1511511074 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLib.ExifTag::get_Data()
extern "C"  ByteU5BU5D_t3397334013* ExifTag_get_Data_m1429660925 (ExifTag_t1511511074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLib.ExifTag::set_Data(System.Byte[])
extern "C"  void ExifTag_set_Data_m3140696106 (ExifTag_t1511511074 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLib.ExifTag::get_LittleEndian()
extern "C"  bool ExifTag_get_LittleEndian_m3205269398 (ExifTag_t1511511074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLib.ExifTag::set_LittleEndian(System.Boolean)
extern "C"  void ExifTag_set_LittleEndian_m2265241143 (ExifTag_t1511511074 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLib.ExifTag::get_IsValid()
extern "C"  bool ExifTag_get_IsValid_m2936993149 (ExifTag_t1511511074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLib.ExifTag::set_IsValid(System.Boolean)
extern "C"  void ExifTag_set_IsValid_m3558738516 (ExifTag_t1511511074 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 ExifLib.ExifTag::ReadShort(System.Int32)
extern "C"  int16_t ExifTag_ReadShort_m3211934217 (ExifTag_t1511511074 * __this, int32_t ___offset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLib.ExifTag::ReadUShort(System.Int32)
extern "C"  uint16_t ExifTag_ReadUShort_m1920279717 (ExifTag_t1511511074 * __this, int32_t ___offset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLib.ExifTag::ReadInt(System.Int32)
extern "C"  int32_t ExifTag_ReadInt_m1901484646 (ExifTag_t1511511074 * __this, int32_t ___offset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ExifLib.ExifTag::ReadUInt(System.Int32)
extern "C"  uint32_t ExifTag_ReadUInt_m3387252388 (ExifTag_t1511511074 * __this, int32_t ___offset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExifLib.ExifTag::ReadSingle(System.Int32)
extern "C"  float ExifTag_ReadSingle_m3684166903 (ExifTag_t1511511074 * __this, int32_t ___offset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double ExifLib.ExifTag::ReadDouble(System.Int32)
extern "C"  double ExifTag_ReadDouble_m1158502199 (ExifTag_t1511511074 * __this, int32_t ___offset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLib.ExifTag::get_IsNumeric()
extern "C"  bool ExifTag_get_IsNumeric_m4045126734 (ExifTag_t1511511074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLib.ExifTag::GetInt(System.Int32)
extern "C"  int32_t ExifTag_GetInt_m3545694352 (ExifTag_t1511511074 * __this, int32_t ___componentIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double ExifLib.ExifTag::GetNumericValue(System.Int32)
extern "C"  double ExifTag_GetNumericValue_m4159319348 (ExifTag_t1511511074 * __this, int32_t ___componentIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLib.ExifTag::GetStringValue()
extern "C"  String_t* ExifTag_GetStringValue_m3552004877 (ExifTag_t1511511074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLib.ExifTag::GetStringValue(System.Int32)
extern "C"  String_t* ExifTag_GetStringValue_m2821851654 (ExifTag_t1511511074 * __this, int32_t ___componentIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLib.ExifTag::Populate(ExifLib.JpegInfo,ExifLib.ExifIFD)
extern "C"  void ExifTag_Populate_m2104675405 (ExifTag_t1511511074 * __this, JpegInfo_t3114827956 * ___info0, int32_t ___ifd1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLib.ExifTag::ToString()
extern "C"  String_t* ExifTag_ToString_m4110112535 (ExifTag_t1511511074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
