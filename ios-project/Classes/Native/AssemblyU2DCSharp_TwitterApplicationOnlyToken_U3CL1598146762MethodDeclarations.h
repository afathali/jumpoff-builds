﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TwitterApplicationOnlyToken/<Load>c__Iterator8
struct U3CLoadU3Ec__Iterator8_t1598146762;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TwitterApplicationOnlyToken/<Load>c__Iterator8::.ctor()
extern "C"  void U3CLoadU3Ec__Iterator8__ctor_m3847643777 (U3CLoadU3Ec__Iterator8_t1598146762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TwitterApplicationOnlyToken/<Load>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2239810179 (U3CLoadU3Ec__Iterator8_t1598146762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TwitterApplicationOnlyToken/<Load>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m3649566155 (U3CLoadU3Ec__Iterator8_t1598146762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TwitterApplicationOnlyToken/<Load>c__Iterator8::MoveNext()
extern "C"  bool U3CLoadU3Ec__Iterator8_MoveNext_m99360331 (U3CLoadU3Ec__Iterator8_t1598146762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterApplicationOnlyToken/<Load>c__Iterator8::Dispose()
extern "C"  void U3CLoadU3Ec__Iterator8_Dispose_m3236048172 (U3CLoadU3Ec__Iterator8_t1598146762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwitterApplicationOnlyToken/<Load>c__Iterator8::Reset()
extern "C"  void U3CLoadU3Ec__Iterator8_Reset_m352694370 (U3CLoadU3Ec__Iterator8_t1598146762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
