﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentNotNullOrEmpty(System.String,System.String)
extern "C"  void ValidationUtils_ArgumentNotNullOrEmpty_m741666233 (Il2CppObject * __this /* static, unused */, String_t* ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentNotNullOrEmptyOrWhitespace(System.String,System.String)
extern "C"  void ValidationUtils_ArgumentNotNullOrEmptyOrWhitespace_m1554622543 (Il2CppObject * __this /* static, unused */, String_t* ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentTypeIsEnum(System.Type,System.String)
extern "C"  void ValidationUtils_ArgumentTypeIsEnum_m3975384751 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentNotNullOrEmpty(System.Collections.ICollection,System.String)
extern "C"  void ValidationUtils_ArgumentNotNullOrEmpty_m1710661386 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___collection0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentNotNullOrEmpty(System.Collections.ICollection,System.String,System.String)
extern "C"  void ValidationUtils_ArgumentNotNullOrEmpty_m1395575218 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___collection0, String_t* ___parameterName1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentNotNull(System.Object,System.String)
extern "C"  void ValidationUtils_ArgumentNotNull_m3243187033 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentNotNegative(System.Int32,System.String)
extern "C"  void ValidationUtils_ArgumentNotNegative_m455657478 (Il2CppObject * __this /* static, unused */, int32_t ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentNotNegative(System.Int32,System.String,System.String)
extern "C"  void ValidationUtils_ArgumentNotNegative_m2545860134 (Il2CppObject * __this /* static, unused */, int32_t ___value0, String_t* ___parameterName1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentNotZero(System.Int32,System.String)
extern "C"  void ValidationUtils_ArgumentNotZero_m3995997295 (Il2CppObject * __this /* static, unused */, int32_t ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentNotZero(System.Int32,System.String,System.String)
extern "C"  void ValidationUtils_ArgumentNotZero_m2417032729 (Il2CppObject * __this /* static, unused */, int32_t ___value0, String_t* ___parameterName1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentIsPositive(System.Int32,System.String,System.String)
extern "C"  void ValidationUtils_ArgumentIsPositive_m1543586463 (Il2CppObject * __this /* static, unused */, int32_t ___value0, String_t* ___parameterName1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ValidationUtils::ObjectNotDisposed(System.Boolean,System.Type)
extern "C"  void ValidationUtils_ObjectNotDisposed_m4030028327 (Il2CppObject * __this /* static, unused */, bool ___disposed0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentConditionTrue(System.Boolean,System.String,System.String)
extern "C"  void ValidationUtils_ArgumentConditionTrue_m2495676405 (Il2CppObject * __this /* static, unused */, bool ___condition0, String_t* ___parameterName1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
