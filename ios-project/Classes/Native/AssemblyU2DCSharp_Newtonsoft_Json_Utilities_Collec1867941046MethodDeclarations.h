﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.CollectionUtils/<TryGetSingleItem>c__AnonStorey25`1<System.Object>
struct U3CTryGetSingleItemU3Ec__AnonStorey25_1_t1867941046;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.CollectionUtils/<TryGetSingleItem>c__AnonStorey25`1<System.Object>::.ctor()
extern "C"  void U3CTryGetSingleItemU3Ec__AnonStorey25_1__ctor_m1640756827_gshared (U3CTryGetSingleItemU3Ec__AnonStorey25_1_t1867941046 * __this, const MethodInfo* method);
#define U3CTryGetSingleItemU3Ec__AnonStorey25_1__ctor_m1640756827(__this, method) ((  void (*) (U3CTryGetSingleItemU3Ec__AnonStorey25_1_t1867941046 *, const MethodInfo*))U3CTryGetSingleItemU3Ec__AnonStorey25_1__ctor_m1640756827_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.CollectionUtils/<TryGetSingleItem>c__AnonStorey25`1<System.Object>::<>m__E7()
extern "C"  Il2CppObject * U3CTryGetSingleItemU3Ec__AnonStorey25_1_U3CU3Em__E7_m897406577_gshared (U3CTryGetSingleItemU3Ec__AnonStorey25_1_t1867941046 * __this, const MethodInfo* method);
#define U3CTryGetSingleItemU3Ec__AnonStorey25_1_U3CU3Em__E7_m897406577(__this, method) ((  Il2CppObject * (*) (U3CTryGetSingleItemU3Ec__AnonStorey25_1_t1867941046 *, const MethodInfo*))U3CTryGetSingleItemU3Ec__AnonStorey25_1_U3CU3Em__E7_m897406577_gshared)(__this, method)
