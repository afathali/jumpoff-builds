﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNativePreviewBackButton
struct IOSNativePreviewBackButton_t2513092733;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSNativePreviewBackButton::.ctor()
extern "C"  void IOSNativePreviewBackButton__ctor_m3823031450 (IOSNativePreviewBackButton_t2513092733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSNativePreviewBackButton IOSNativePreviewBackButton::Create()
extern "C"  IOSNativePreviewBackButton_t2513092733 * IOSNativePreviewBackButton_Create_m1320015116 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePreviewBackButton::Awake()
extern "C"  void IOSNativePreviewBackButton_Awake_m3193800815 (IOSNativePreviewBackButton_t2513092733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNativePreviewBackButton::OnGUI()
extern "C"  void IOSNativePreviewBackButton_OnGUI_m1685782254 (IOSNativePreviewBackButton_t2513092733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSNativePreviewBackButton::get_loadedLevelName()
extern "C"  String_t* IOSNativePreviewBackButton_get_loadedLevelName_m408469372 (IOSNativePreviewBackButton_t2513092733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
