﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_Notifications_EditorUIController
struct SA_Notifications_EditorUIController_t749267495;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorNotificatio2256397131.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Animation_2123337604.h"

// System.Void SA_Notifications_EditorUIController::.ctor()
extern "C"  void SA_Notifications_EditorUIController__ctor_m943839576 (SA_Notifications_EditorUIController_t749267495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Notifications_EditorUIController::Awake()
extern "C"  void SA_Notifications_EditorUIController_Awake_m2339683289 (SA_Notifications_EditorUIController_t749267495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Notifications_EditorUIController::ShowNotification(System.String,System.String,SA_EditorNotificationType)
extern "C"  void SA_Notifications_EditorUIController_ShowNotification_m2600411583 (SA_Notifications_EditorUIController_t749267495 * __this, String_t* ___title0, String_t* ___message1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Notifications_EditorUIController::HandleOnInTweenComplete()
extern "C"  void SA_Notifications_EditorUIController_HandleOnInTweenComplete_m1988608916 (SA_Notifications_EditorUIController_t749267495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Notifications_EditorUIController::NotificationDelayComplete()
extern "C"  void SA_Notifications_EditorUIController_NotificationDelayComplete_m884402905 (SA_Notifications_EditorUIController_t749267495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Notifications_EditorUIController::HandleOnOutTweenComplete()
extern "C"  void SA_Notifications_EditorUIController_HandleOnOutTweenComplete_m471538893 (SA_Notifications_EditorUIController_t749267495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Notifications_EditorUIController::HandleOnValueChanged(System.Single)
extern "C"  void SA_Notifications_EditorUIController_HandleOnValueChanged_m2929388259 (SA_Notifications_EditorUIController_t749267495 * __this, float ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_Notifications_EditorUIController::Animate(System.Single,System.Single,SA.Common.Animation.EaseType)
extern "C"  void SA_Notifications_EditorUIController_Animate_m2937568428 (SA_Notifications_EditorUIController_t749267495 * __this, float ___from0, float ___to1, int32_t ___easeType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
