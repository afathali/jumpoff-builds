﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleChild
struct SampleChild_t2736175310;
// System.Collections.Generic.List`1<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>
struct List_1_t1357208527;
// System.Collections.Generic.Dictionary`2<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>
struct Dictionary_2_t995913030;

#include "codegen/il2cpp-codegen.h"

// System.Void Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleChild::.ctor()
extern "C"  void SampleChild__ctor_m2128321753 (SampleChild_t2736175310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject> Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleChild::get_ObjectList()
extern "C"  List_1_t1357208527 * SampleChild_get_ObjectList_m45998037 (SampleChild_t2736175310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleChild::set_ObjectList(System.Collections.Generic.List`1<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>)
extern "C"  void SampleChild_set_ObjectList_m210822996 (SampleChild_t2736175310 * __this, List_1_t1357208527 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject> Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleChild::get_ObjectDictionary()
extern "C"  Dictionary_2_t995913030 * SampleChild_get_ObjectDictionary_m3774383691 (SampleChild_t2736175310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleChild::set_ObjectDictionary(System.Collections.Generic.Dictionary`2<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>)
extern "C"  void SampleChild_set_ObjectDictionary_m3908752940 (SampleChild_t2736175310 * __this, Dictionary_2_t995913030 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
