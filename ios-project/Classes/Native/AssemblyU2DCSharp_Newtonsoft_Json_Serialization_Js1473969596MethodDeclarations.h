﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonContainerAttribute
struct JsonContainerAttribute_t47210975;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonObjectAttribute
struct JsonObjectAttribute_t162755825;
// Newtonsoft.Json.JsonArrayAttribute
struct JsonArrayAttribute_t3639750789;
// System.Runtime.Serialization.DataContractAttribute
struct DataContractAttribute_t3332255060;
// System.Runtime.Serialization.DataMemberAttribute
struct DataMemberAttribute_t2677019114;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t502202687;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t1964060750;
// System.ComponentModel.TypeConverter
struct TypeConverter_t745995970;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct ReflectionDelegateFactory_t2294713146;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MemberSerializati687984360.h"

// System.Void Newtonsoft.Json.Serialization.JsonTypeReflector::.cctor()
extern "C"  void JsonTypeReflector__cctor_m317754257 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonContainerAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonContainerAttribute(System.Type)
extern "C"  JsonContainerAttribute_t47210975 * JsonTypeReflector_GetJsonContainerAttribute_m832501207 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonObjectAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonObjectAttribute(System.Type)
extern "C"  JsonObjectAttribute_t162755825 * JsonTypeReflector_GetJsonObjectAttribute_m2358392983 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonArrayAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonArrayAttribute(System.Type)
extern "C"  JsonArrayAttribute_t3639750789 * JsonTypeReflector_GetJsonArrayAttribute_m1940005671 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.DataContractAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetDataContractAttribute(System.Type)
extern "C"  DataContractAttribute_t3332255060 * JsonTypeReflector_GetDataContractAttribute_m1960808825 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.DataMemberAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetDataMemberAttribute(System.Reflection.MemberInfo)
extern "C"  DataMemberAttribute_t2677019114 * JsonTypeReflector_GetDataMemberAttribute_m2502336066 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonTypeReflector::GetObjectMemberSerialization(System.Type)
extern "C"  int32_t JsonTypeReflector_GetObjectMemberSerialization_m1335011806 (Il2CppObject * __this /* static, unused */, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonConverterType(System.Reflection.ICustomAttributeProvider)
extern "C"  Type_t * JsonTypeReflector_GetJsonConverterType_m1735862121 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___attributeProvider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonConverterTypeFromAttribute(System.Reflection.ICustomAttributeProvider)
extern "C"  Type_t * JsonTypeReflector_GetJsonConverterTypeFromAttribute_m131331675 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___attributeProvider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonConverter(System.Reflection.ICustomAttributeProvider,System.Type)
extern "C"  JsonConverter_t1964060750 * JsonTypeReflector_GetJsonConverter_m1218657946 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___attributeProvider0, Type_t * ___targetConvertedType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.TypeConverter Newtonsoft.Json.Serialization.JsonTypeReflector::GetTypeConverter(System.Type)
extern "C"  TypeConverter_t745995970 * JsonTypeReflector_GetTypeConverter_m438004529 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetAssociatedMetadataType(System.Type)
extern "C"  Type_t * JsonTypeReflector_GetAssociatedMetadataType_m2537466840 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetAssociateMetadataTypeFromAttribute(System.Type)
extern "C"  Type_t * JsonTypeReflector_GetAssociateMetadataTypeFromAttribute_m2405227966 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetMetadataTypeAttributeType()
extern "C"  Type_t * JsonTypeReflector_GetMetadataTypeAttributeType_m3540622633 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::get_DynamicCodeGeneration()
extern "C"  bool JsonTypeReflector_get_DynamicCodeGeneration_m1086230245 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Serialization.JsonTypeReflector::get_ReflectionDelegateFactory()
extern "C"  ReflectionDelegateFactory_t2294713146 * JsonTypeReflector_get_ReflectionDelegateFactory_m3835186815 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
