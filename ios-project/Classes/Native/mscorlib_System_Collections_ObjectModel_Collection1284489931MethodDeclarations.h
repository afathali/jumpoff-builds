﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct Collection_1_t1284489931;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Schema.JsonSchemaType[]
struct JsonSchemaTypeU5BU5D_t3104176164;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct IEnumerator_1_t3513236300;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct IList_1_t2283685778;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"

// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor()
extern "C"  void Collection_1__ctor_m385636942_gshared (Collection_1_t1284489931 * __this, const MethodInfo* method);
#define Collection_1__ctor_m385636942(__this, method) ((  void (*) (Collection_1_t1284489931 *, const MethodInfo*))Collection_1__ctor_m385636942_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1074496165_gshared (Collection_1_t1284489931 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1074496165(__this, method) ((  bool (*) (Collection_1_t1284489931 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1074496165_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2421230470_gshared (Collection_1_t1284489931 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2421230470(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1284489931 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2421230470_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m670665289_gshared (Collection_1_t1284489931 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m670665289(__this, method) ((  Il2CppObject * (*) (Collection_1_t1284489931 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m670665289_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2968013422_gshared (Collection_1_t1284489931 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2968013422(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1284489931 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2968013422_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1280568560_gshared (Collection_1_t1284489931 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1280568560(__this, ___value0, method) ((  bool (*) (Collection_1_t1284489931 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1280568560_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m4174166740_gshared (Collection_1_t1284489931 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m4174166740(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1284489931 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m4174166740_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3940469209_gshared (Collection_1_t1284489931 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3940469209(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1284489931 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3940469209_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3457606457_gshared (Collection_1_t1284489931 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3457606457(__this, ___value0, method) ((  void (*) (Collection_1_t1284489931 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3457606457_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m325836454_gshared (Collection_1_t1284489931 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m325836454(__this, method) ((  bool (*) (Collection_1_t1284489931 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m325836454_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m4117742182_gshared (Collection_1_t1284489931 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m4117742182(__this, method) ((  Il2CppObject * (*) (Collection_1_t1284489931 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m4117742182_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1419169893_gshared (Collection_1_t1284489931 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1419169893(__this, method) ((  bool (*) (Collection_1_t1284489931 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1419169893_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2459876642_gshared (Collection_1_t1284489931 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2459876642(__this, method) ((  bool (*) (Collection_1_t1284489931 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2459876642_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3634838957_gshared (Collection_1_t1284489931 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3634838957(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1284489931 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3634838957_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m566359048_gshared (Collection_1_t1284489931 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m566359048(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1284489931 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m566359048_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::Add(T)
extern "C"  void Collection_1_Add_m1928068317_gshared (Collection_1_t1284489931 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m1928068317(__this, ___item0, method) ((  void (*) (Collection_1_t1284489931 *, int32_t, const MethodInfo*))Collection_1_Add_m1928068317_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::Clear()
extern "C"  void Collection_1_Clear_m682136841_gshared (Collection_1_t1284489931 * __this, const MethodInfo* method);
#define Collection_1_Clear_m682136841(__this, method) ((  void (*) (Collection_1_t1284489931 *, const MethodInfo*))Collection_1_Clear_m682136841_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::ClearItems()
extern "C"  void Collection_1_ClearItems_m423170079_gshared (Collection_1_t1284489931 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m423170079(__this, method) ((  void (*) (Collection_1_t1284489931 *, const MethodInfo*))Collection_1_ClearItems_m423170079_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::Contains(T)
extern "C"  bool Collection_1_Contains_m2320811427_gshared (Collection_1_t1284489931 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2320811427(__this, ___item0, method) ((  bool (*) (Collection_1_t1284489931 *, int32_t, const MethodInfo*))Collection_1_Contains_m2320811427_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3369138405_gshared (Collection_1_t1284489931 * __this, JsonSchemaTypeU5BU5D_t3104176164* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3369138405(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1284489931 *, JsonSchemaTypeU5BU5D_t3104176164*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3369138405_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2275689450_gshared (Collection_1_t1284489931 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2275689450(__this, method) ((  Il2CppObject* (*) (Collection_1_t1284489931 *, const MethodInfo*))Collection_1_GetEnumerator_m2275689450_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m401334457_gshared (Collection_1_t1284489931 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m401334457(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1284489931 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m401334457_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2907169576_gshared (Collection_1_t1284489931 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2907169576(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1284489931 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m2907169576_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3844265003_gshared (Collection_1_t1284489931 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3844265003(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1284489931 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m3844265003_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m2486806275_gshared (Collection_1_t1284489931 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m2486806275(__this, method) ((  Il2CppObject* (*) (Collection_1_t1284489931 *, const MethodInfo*))Collection_1_get_Items_m2486806275_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::Remove(T)
extern "C"  bool Collection_1_Remove_m2940678422_gshared (Collection_1_t1284489931 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2940678422(__this, ___item0, method) ((  bool (*) (Collection_1_t1284489931 *, int32_t, const MethodInfo*))Collection_1_Remove_m2940678422_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1607017892_gshared (Collection_1_t1284489931 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1607017892(__this, ___index0, method) ((  void (*) (Collection_1_t1284489931 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1607017892_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3628644702_gshared (Collection_1_t1284489931 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3628644702(__this, ___index0, method) ((  void (*) (Collection_1_t1284489931 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3628644702_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1382919758_gshared (Collection_1_t1284489931 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1382919758(__this, method) ((  int32_t (*) (Collection_1_t1284489931 *, const MethodInfo*))Collection_1_get_Count_m1382919758_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m3389543226_gshared (Collection_1_t1284489931 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3389543226(__this, ___index0, method) ((  int32_t (*) (Collection_1_t1284489931 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3389543226_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1490990565_gshared (Collection_1_t1284489931 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1490990565(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1284489931 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m1490990565_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m4082248696_gshared (Collection_1_t1284489931 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m4082248696(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1284489931 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m4082248696_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3294616031_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3294616031(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3294616031_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m1414986943_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1414986943(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1414986943_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1209204803_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1209204803(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1209204803_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m427446819_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m427446819(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m427446819_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Schema.JsonSchemaType>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m883478732_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m883478732(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m883478732_gshared)(__this /* static, unused */, ___list0, method)
