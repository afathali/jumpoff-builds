﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLib.ExifReader
struct ExifReader_t3799989581;
// System.IO.Stream
struct Stream_t3255436806;
// ExifLib.JpegInfo
struct JpegInfo_t3114827956;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "AssemblyU2DCSharp_ExifLib_JpegInfo3114827956.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ExifLib_ExifIFD1652672661.h"

// System.Void ExifLib.ExifReader::.ctor(System.IO.Stream)
extern "C"  void ExifReader__ctor_m1237123040 (ExifReader_t3799989581 * __this, Stream_t3255436806 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLib.JpegInfo ExifLib.ExifReader::get_info()
extern "C"  JpegInfo_t3114827956 * ExifReader_get_info_m4257563832 (ExifReader_t3799989581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLib.ExifReader::set_info(ExifLib.JpegInfo)
extern "C"  void ExifReader_set_info_m401755613 (ExifReader_t3799989581 * __this, JpegInfo_t3114827956 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLib.JpegInfo ExifLib.ExifReader::ReadJpeg(System.Byte[],System.String)
extern "C"  JpegInfo_t3114827956 * ExifReader_ReadJpeg_m228851690 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___fiBYTES0, String_t* ___Name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLib.ExifReader::ProcessExif(System.Byte[])
extern "C"  void ExifReader_ProcessExif_m3560148297 (ExifReader_t3799989581 * __this, ByteU5BU5D_t3397334013* ___section0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLib.ExifReader::DirOffset(System.Int32,System.Int32)
extern "C"  int32_t ExifReader_DirOffset_m2469842723 (ExifReader_t3799989581 * __this, int32_t ___start0, int32_t ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLib.ExifReader::ProcessExifDir(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32,ExifLib.ExifIFD)
extern "C"  void ExifReader_ProcessExifDir_m1139636072 (ExifReader_t3799989581 * __this, ByteU5BU5D_t3397334013* ___section0, int32_t ___offsetDir1, int32_t ___offsetBase2, int32_t ___length3, int32_t ___depth4, int32_t ___ifd5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLib.ExifReader::ProcessSOF(System.Byte[],System.Int32)
extern "C"  void ExifReader_ProcessSOF_m3937678670 (ExifReader_t3799989581 * __this, ByteU5BU5D_t3397334013* ___section0, int32_t ___marker1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
