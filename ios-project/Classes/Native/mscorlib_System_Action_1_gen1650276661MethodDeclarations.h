﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"

// System.Void System.Action`1<FB_LoginResult>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m2871010814(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t1650276661 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m584977596_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<FB_LoginResult>::Invoke(T)
#define Action_1_Invoke_m836646901(__this, ___obj0, method) ((  void (*) (Action_1_t1650276661 *, FB_LoginResult_t1848477279 *, const MethodInfo*))Action_1_Invoke_m101203496_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<FB_LoginResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m1127515410(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t1650276661 *, FB_LoginResult_t1848477279 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1305519803_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<FB_LoginResult>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m3732218853(__this, ___result0, method) ((  void (*) (Action_1_t1650276661 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m2057605070_gshared)(__this, ___result0, method)
