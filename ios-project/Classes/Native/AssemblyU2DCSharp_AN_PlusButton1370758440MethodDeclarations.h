﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_PlusButton
struct AN_PlusButton_t1370758440;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AN_PlusBtnSize3788325417.h"
#include "AssemblyU2DCSharp_AN_PlusBtnAnnotation1944163283.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"

// System.Void AN_PlusButton::.ctor(System.String,AN_PlusBtnSize,AN_PlusBtnAnnotation)
extern "C"  void AN_PlusButton__ctor_m1908983855 (AN_PlusButton_t1370758440 * __this, String_t* ___url0, int32_t ___btnSize1, int32_t ___annotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButton::.cctor()
extern "C"  void AN_PlusButton__cctor_m1305887678 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButton::SetGravity(UnityEngine.TextAnchor)
extern "C"  void AN_PlusButton_SetGravity_m1814568310 (AN_PlusButton_t1370758440 * __this, int32_t ___btnAnchor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButton::SetPosition(System.Int32,System.Int32)
extern "C"  void AN_PlusButton_SetPosition_m161800808 (AN_PlusButton_t1370758440 * __this, int32_t ___btnX0, int32_t ___btnY1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButton::Show()
extern "C"  void AN_PlusButton_Show_m1985648926 (AN_PlusButton_t1370758440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButton::Hide()
extern "C"  void AN_PlusButton_Hide_m4283432123 (AN_PlusButton_t1370758440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButton::Refresh()
extern "C"  void AN_PlusButton_Refresh_m2876610624 (AN_PlusButton_t1370758440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AN_PlusButton::get_ButtonId()
extern "C"  int32_t AN_PlusButton_get_ButtonId_m3393020431 (AN_PlusButton_t1370758440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AN_PlusButton::get_x()
extern "C"  int32_t AN_PlusButton_get_x_m1822405862 (AN_PlusButton_t1370758440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AN_PlusButton::get_y()
extern "C"  int32_t AN_PlusButton_get_y_m1822405957 (AN_PlusButton_t1370758440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AN_PlusButton::get_IsShowed()
extern "C"  bool AN_PlusButton_get_IsShowed_m1184638260 (AN_PlusButton_t1370758440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextAnchor AN_PlusButton::get_anchor()
extern "C"  int32_t AN_PlusButton_get_anchor_m595789971 (AN_PlusButton_t1370758440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AN_PlusButton::get_gravity()
extern "C"  int32_t AN_PlusButton_get_gravity_m986317836 (AN_PlusButton_t1370758440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButton::FireClickAction()
extern "C"  void AN_PlusButton_FireClickAction_m3458732867 (AN_PlusButton_t1370758440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AN_PlusButton::get_nextId()
extern "C"  int32_t AN_PlusButton_get_nextId_m2499633152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PlusButton::<ButtonClicked>m__7A()
extern "C"  void AN_PlusButton_U3CButtonClickedU3Em__7A_m3275500111 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
