﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct List_1_t1111866309;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat646595983.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m627591444_gshared (Enumerator_t646595983 * __this, List_1_t1111866309 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m627591444(__this, ___l0, method) ((  void (*) (Enumerator_t646595983 *, List_1_t1111866309 *, const MethodInfo*))Enumerator__ctor_m627591444_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3792565398_gshared (Enumerator_t646595983 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3792565398(__this, method) ((  void (*) (Enumerator_t646595983 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3792565398_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2649308562_gshared (Enumerator_t646595983 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2649308562(__this, method) ((  Il2CppObject * (*) (Enumerator_t646595983 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2649308562_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::Dispose()
extern "C"  void Enumerator_Dispose_m2601704439_gshared (Enumerator_t646595983 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2601704439(__this, method) ((  void (*) (Enumerator_t646595983 *, const MethodInfo*))Enumerator_Dispose_m2601704439_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1396958330_gshared (Enumerator_t646595983 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1396958330(__this, method) ((  void (*) (Enumerator_t646595983 *, const MethodInfo*))Enumerator_VerifyState_m1396958330_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m748175054_gshared (Enumerator_t646595983 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m748175054(__this, method) ((  bool (*) (Enumerator_t646595983 *, const MethodInfo*))Enumerator_MoveNext_m748175054_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Schema.JsonSchemaType>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1769104311_gshared (Enumerator_t646595983 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1769104311(__this, method) ((  int32_t (*) (Enumerator_t646595983 *, const MethodInfo*))Enumerator_get_Current_m1769104311_gshared)(__this, method)
