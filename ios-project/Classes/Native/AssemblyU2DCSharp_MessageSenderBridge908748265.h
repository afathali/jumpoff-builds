﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MessageSenderBridge
struct MessageSenderBridge_t908748265;

#include "AssemblyU2DCSharp_MCGBehaviour3307770884.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageSenderBridge
struct  MessageSenderBridge_t908748265  : public MCGBehaviour_t3307770884
{
public:

public:
};

struct MessageSenderBridge_t908748265_StaticFields
{
public:
	// MessageSenderBridge MessageSenderBridge::_instance
	MessageSenderBridge_t908748265 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(MessageSenderBridge_t908748265_StaticFields, ____instance_4)); }
	inline MessageSenderBridge_t908748265 * get__instance_4() const { return ____instance_4; }
	inline MessageSenderBridge_t908748265 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(MessageSenderBridge_t908748265 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier(&____instance_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
