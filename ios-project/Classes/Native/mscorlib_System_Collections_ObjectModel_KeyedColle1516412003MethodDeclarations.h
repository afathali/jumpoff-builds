﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_KeyedColle1294060137MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>>::.ctor()
#define KeyedCollection_2__ctor_m2145368572(__this, method) ((  void (*) (KeyedCollection_2_t1516412003 *, const MethodInfo*))KeyedCollection_2__ctor_m3929680866_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>,System.Int32)
#define KeyedCollection_2__ctor_m997758340(__this, ___comparer0, ___dictionaryCreationThreshold1, method) ((  void (*) (KeyedCollection_2_t1516412003 *, Il2CppObject*, int32_t, const MethodInfo*))KeyedCollection_2__ctor_m1398787942_gshared)(__this, ___comparer0, ___dictionaryCreationThreshold1, method)
// System.Boolean System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>>::Contains(TKey)
#define KeyedCollection_2_Contains_m3622697214(__this, ___key0, method) ((  bool (*) (KeyedCollection_2_t1516412003 *, String_t*, const MethodInfo*))KeyedCollection_2_Contains_m3918219472_gshared)(__this, ___key0, method)
// System.Int32 System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>>::IndexOfKey(TKey)
#define KeyedCollection_2_IndexOfKey_m363339065(__this, ___key0, method) ((  int32_t (*) (KeyedCollection_2_t1516412003 *, String_t*, const MethodInfo*))KeyedCollection_2_IndexOfKey_m872387181_gshared)(__this, ___key0, method)
// TItem System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>>::get_Item(TKey)
#define KeyedCollection_2_get_Item_m3259437677(__this, ___key0, method) ((  EnumValue_1_t589082027 * (*) (KeyedCollection_2_t1516412003 *, String_t*, const MethodInfo*))KeyedCollection_2_get_Item_m1357505817_gshared)(__this, ___key0, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>>::ClearItems()
#define KeyedCollection_2_ClearItems_m2436832807(__this, method) ((  void (*) (KeyedCollection_2_t1516412003 *, const MethodInfo*))KeyedCollection_2_ClearItems_m3110573139_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>>::InsertItem(System.Int32,TItem)
#define KeyedCollection_2_InsertItem_m3225159928(__this, ___index0, ___item1, method) ((  void (*) (KeyedCollection_2_t1516412003 *, int32_t, EnumValue_1_t589082027 *, const MethodInfo*))KeyedCollection_2_InsertItem_m2740229522_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>>::RemoveItem(System.Int32)
#define KeyedCollection_2_RemoveItem_m2810479460(__this, ___index0, method) ((  void (*) (KeyedCollection_2_t1516412003 *, int32_t, const MethodInfo*))KeyedCollection_2_RemoveItem_m1708519878_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>>::SetItem(System.Int32,TItem)
#define KeyedCollection_2_SetItem_m253818723(__this, ___index0, ___item1, method) ((  void (*) (KeyedCollection_2_t1516412003 *, int32_t, EnumValue_1_t589082027 *, const MethodInfo*))KeyedCollection_2_SetItem_m4134855463_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IDictionary`2<TKey,TItem> System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>>::get_Dictionary()
#define KeyedCollection_2_get_Dictionary_m51303880(__this, method) ((  Il2CppObject* (*) (KeyedCollection_2_t1516412003 *, const MethodInfo*))KeyedCollection_2_get_Dictionary_m686852578_gshared)(__this, method)
