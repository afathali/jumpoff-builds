﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct IEnumerator_1_t3513236300;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"

// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m1749763100_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m1749763100(__this, method) ((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m1749763100_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  int32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2517370587_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2517370587(__this, method) ((  int32_t (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2517370587_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m437866568_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m437866568(__this, method) ((  Il2CppObject * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m437866568_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m2698500399_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m2698500399(__this, method) ((  Il2CppObject * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m2698500399_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1921361290_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1921361290(__this, method) ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1921361290_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m2026968792_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m2026968792(__this, method) ((  bool (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m2026968792_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::Dispose()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m2109986641_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m2109986641(__this, method) ((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m2109986641_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<Newtonsoft.Json.Schema.JsonSchemaType>::Reset()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m3466846523_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m3466846523(__this, method) ((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3175034752 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m3466846523_gshared)(__this, method)
