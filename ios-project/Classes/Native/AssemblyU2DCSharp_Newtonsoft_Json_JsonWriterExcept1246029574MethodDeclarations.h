﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonWriterException
struct JsonWriterException_t1246029574;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void Newtonsoft.Json.JsonWriterException::.ctor()
extern "C"  void JsonWriterException__ctor_m4071094552 (JsonWriterException_t1246029574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriterException::.ctor(System.String)
extern "C"  void JsonWriterException__ctor_m616060166 (JsonWriterException_t1246029574 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriterException::.ctor(System.String,System.Exception)
extern "C"  void JsonWriterException__ctor_m2108264710 (JsonWriterException_t1246029574 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
