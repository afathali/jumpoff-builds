﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct Dictionary_2_t2358566078;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4045098842.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3372355237_gshared (Enumerator_t4045098842 * __this, Dictionary_2_t2358566078 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3372355237(__this, ___host0, method) ((  void (*) (Enumerator_t4045098842 *, Dictionary_2_t2358566078 *, const MethodInfo*))Enumerator__ctor_m3372355237_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3879786394_gshared (Enumerator_t4045098842 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3879786394(__this, method) ((  Il2CppObject * (*) (Enumerator_t4045098842 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3879786394_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3351404794_gshared (Enumerator_t4045098842 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3351404794(__this, method) ((  void (*) (Enumerator_t4045098842 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3351404794_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1633197581_gshared (Enumerator_t4045098842 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1633197581(__this, method) ((  void (*) (Enumerator_t4045098842 *, const MethodInfo*))Enumerator_Dispose_m1633197581_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3329635614_gshared (Enumerator_t4045098842 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3329635614(__this, method) ((  bool (*) (Enumerator_t4045098842 *, const MethodInfo*))Enumerator_MoveNext_m3329635614_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m4235770664_gshared (Enumerator_t4045098842 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4235770664(__this, method) ((  int32_t (*) (Enumerator_t4045098842 *, const MethodInfo*))Enumerator_get_Current_m4235770664_gshared)(__this, method)
