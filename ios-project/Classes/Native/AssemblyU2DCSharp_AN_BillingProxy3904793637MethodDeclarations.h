﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_BillingProxy
struct AN_BillingProxy_t3904793637;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_BillingProxy::.ctor()
extern "C"  void AN_BillingProxy__ctor_m2815782104 (AN_BillingProxy_t3904793637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_BillingProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_BillingProxy_CallActivityFunction_m1692635655 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_BillingProxy::Connect(System.String,System.String)
extern "C"  void AN_BillingProxy_Connect_m2100675308 (Il2CppObject * __this /* static, unused */, String_t* ___ids0, String_t* ___base64EncodedPublicKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_BillingProxy::RetrieveProducDetails()
extern "C"  void AN_BillingProxy_RetrieveProducDetails_m1436759211 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_BillingProxy::Consume(System.String)
extern "C"  void AN_BillingProxy_Consume_m155193612 (Il2CppObject * __this /* static, unused */, String_t* ___SKU0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_BillingProxy::Purchase(System.String,System.String)
extern "C"  void AN_BillingProxy_Purchase_m2949213415 (Il2CppObject * __this /* static, unused */, String_t* ___SKU0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_BillingProxy::Subscribe(System.String,System.String)
extern "C"  void AN_BillingProxy_Subscribe_m282055106 (Il2CppObject * __this /* static, unused */, String_t* ___SKU0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
