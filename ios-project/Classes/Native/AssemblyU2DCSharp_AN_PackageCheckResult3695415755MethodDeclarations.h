﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_PackageCheckResult
struct AN_PackageCheckResult_t3695415755;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_PackageCheckResult::.ctor(System.String,System.Boolean)
extern "C"  void AN_PackageCheckResult__ctor_m3662115003 (AN_PackageCheckResult_t3695415755 * __this, String_t* ___packId0, bool ___IsResultSucceeded1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AN_PackageCheckResult::get_packageName()
extern "C"  String_t* AN_PackageCheckResult_get_packageName_m2111843335 (AN_PackageCheckResult_t3695415755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
