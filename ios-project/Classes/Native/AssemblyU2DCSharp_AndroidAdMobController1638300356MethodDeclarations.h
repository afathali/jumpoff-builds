﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidAdMobController
struct AndroidAdMobController_t1638300356;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// GoogleMobileAdBanner
struct GoogleMobileAdBanner_t1323818958;
// System.Collections.Generic.List`1<GoogleMobileAdBanner>
struct List_1_t692940090;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AndroidMonth1370057857.h"
#include "AssemblyU2DCSharp_GoogleGender966451998.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "AssemblyU2DCSharp_GADBannerSize1988135029.h"
#include "AssemblyU2DCSharp_GADInAppResolution612781804.h"
#include "System_Core_System_Action3226471752.h"

// System.Void AndroidAdMobController::.ctor()
extern "C"  void AndroidAdMobController__ctor_m2016049765 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::Awake()
extern "C"  void AndroidAdMobController_Awake_m1382298288 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::OnApplicationPause(System.Boolean)
extern "C"  void AndroidAdMobController_OnApplicationPause_m2288989903 (AndroidAdMobController_t1638300356 * __this, bool ___pauseStatus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::Init(System.String)
extern "C"  void AndroidAdMobController_Init_m429906069 (AndroidAdMobController_t1638300356 * __this, String_t* ___ad_unit_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::Init(System.String,System.String)
extern "C"  void AndroidAdMobController_Init_m2550596031 (AndroidAdMobController_t1638300356 * __this, String_t* ___banners_unit_id0, String_t* ___interstisial_unit_id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::SetBannersUnitID(System.String)
extern "C"  void AndroidAdMobController_SetBannersUnitID_m2601005537 (AndroidAdMobController_t1638300356 * __this, String_t* ___ad_unit_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::SetInterstisialsUnitID(System.String)
extern "C"  void AndroidAdMobController_SetInterstisialsUnitID_m3527449528 (AndroidAdMobController_t1638300356 * __this, String_t* ___ad_unit_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::AddKeyword(System.String)
extern "C"  void AndroidAdMobController_AddKeyword_m1044296503 (AndroidAdMobController_t1638300356 * __this, String_t* ___keyword0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::SetBirthday(System.Int32,AndroidMonth,System.Int32)
extern "C"  void AndroidAdMobController_SetBirthday_m1416576045 (AndroidAdMobController_t1638300356 * __this, int32_t ___year0, int32_t ___month1, int32_t ___day2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::TagForChildDirectedTreatment(System.Boolean)
extern "C"  void AndroidAdMobController_TagForChildDirectedTreatment_m1451455849 (AndroidAdMobController_t1638300356 * __this, bool ___tagForChildDirectedTreatment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::AddTestDevice(System.String)
extern "C"  void AndroidAdMobController_AddTestDevice_m2105501192 (AndroidAdMobController_t1638300356 * __this, String_t* ___deviceId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::AddTestDevices(System.String[])
extern "C"  void AndroidAdMobController_AddTestDevices_m1088619385 (AndroidAdMobController_t1638300356 * __this, StringU5BU5D_t1642385972* ___ids0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::SetGender(GoogleGender)
extern "C"  void AndroidAdMobController_SetGender_m920717008 (AndroidAdMobController_t1638300356 * __this, int32_t ___gender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GoogleMobileAdBanner AndroidAdMobController::CreateAdBanner(UnityEngine.TextAnchor,GADBannerSize)
extern "C"  Il2CppObject * AndroidAdMobController_CreateAdBanner_m1185915683 (AndroidAdMobController_t1638300356 * __this, int32_t ___anchor0, int32_t ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GoogleMobileAdBanner AndroidAdMobController::CreateAdBanner(System.Int32,System.Int32,GADBannerSize)
extern "C"  Il2CppObject * AndroidAdMobController_CreateAdBanner_m3247801230 (AndroidAdMobController_t1638300356 * __this, int32_t ___x0, int32_t ___y1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::DestroyBanner(System.Int32)
extern "C"  void AndroidAdMobController_DestroyBanner_m2917731214 (AndroidAdMobController_t1638300356 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::StartInterstitialAd()
extern "C"  void AndroidAdMobController_StartInterstitialAd_m1194025604 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::LoadInterstitialAd()
extern "C"  void AndroidAdMobController_LoadInterstitialAd_m1141120644 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::ShowInterstitialAd()
extern "C"  void AndroidAdMobController_ShowInterstitialAd_m3510764761 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::RecordInAppResolution(GADInAppResolution)
extern "C"  void AndroidAdMobController_RecordInAppResolution_m3986231404 (AndroidAdMobController_t1638300356 * __this, int32_t ___resolution0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GoogleMobileAdBanner AndroidAdMobController::GetBanner(System.Int32)
extern "C"  Il2CppObject * AndroidAdMobController_GetBanner_m3635368301 (AndroidAdMobController_t1638300356 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GoogleMobileAdBanner> AndroidAdMobController::get_banners()
extern "C"  List_1_t692940090 * AndroidAdMobController_get_banners_m3099033278 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidAdMobController::get_IsInited()
extern "C"  bool AndroidAdMobController_get_IsInited_m3259518321 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidAdMobController::get_BannersUunitId()
extern "C"  String_t* AndroidAdMobController_get_BannersUunitId_m3439478910 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidAdMobController::get_InterstisialUnitId()
extern "C"  String_t* AndroidAdMobController_get_InterstisialUnitId_m2847620291 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action AndroidAdMobController::get_OnInterstitialLoaded()
extern "C"  Action_t3226471752 * AndroidAdMobController_get_OnInterstitialLoaded_m1802565708 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::set_OnInterstitialLoaded(System.Action)
extern "C"  void AndroidAdMobController_set_OnInterstitialLoaded_m2065327077 (AndroidAdMobController_t1638300356 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action AndroidAdMobController::get_OnInterstitialFailedLoading()
extern "C"  Action_t3226471752 * AndroidAdMobController_get_OnInterstitialFailedLoading_m3745730978 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::set_OnInterstitialFailedLoading(System.Action)
extern "C"  void AndroidAdMobController_set_OnInterstitialFailedLoading_m12378083 (AndroidAdMobController_t1638300356 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action AndroidAdMobController::get_OnInterstitialOpened()
extern "C"  Action_t3226471752 * AndroidAdMobController_get_OnInterstitialOpened_m84939752 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::set_OnInterstitialOpened(System.Action)
extern "C"  void AndroidAdMobController_set_OnInterstitialOpened_m3332587947 (AndroidAdMobController_t1638300356 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action AndroidAdMobController::get_OnInterstitialClosed()
extern "C"  Action_t3226471752 * AndroidAdMobController_get_OnInterstitialClosed_m2567978063 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::set_OnInterstitialClosed(System.Action)
extern "C"  void AndroidAdMobController_set_OnInterstitialClosed_m3314238088 (AndroidAdMobController_t1638300356 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action AndroidAdMobController::get_OnInterstitialLeftApplication()
extern "C"  Action_t3226471752 * AndroidAdMobController_get_OnInterstitialLeftApplication_m3706795672 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::set_OnInterstitialLeftApplication(System.Action)
extern "C"  void AndroidAdMobController_set_OnInterstitialLeftApplication_m2567458611 (AndroidAdMobController_t1638300356 * __this, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<System.String> AndroidAdMobController::get_OnAdInAppRequest()
extern "C"  Action_1_t1831019615 * AndroidAdMobController_get_OnAdInAppRequest_m397149356 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::set_OnAdInAppRequest(System.Action`1<System.String>)
extern "C"  void AndroidAdMobController_set_OnAdInAppRequest_m1648551359 (AndroidAdMobController_t1638300356 * __this, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::OnBannerAdLoaded(System.String)
extern "C"  void AndroidAdMobController_OnBannerAdLoaded_m673650414 (AndroidAdMobController_t1638300356 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::OnBannerAdFailedToLoad(System.String)
extern "C"  void AndroidAdMobController_OnBannerAdFailedToLoad_m1756440225 (AndroidAdMobController_t1638300356 * __this, String_t* ___bannerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::OnBannerAdOpened(System.String)
extern "C"  void AndroidAdMobController_OnBannerAdOpened_m1205223922 (AndroidAdMobController_t1638300356 * __this, String_t* ___bannerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::OnBannerAdClosed(System.String)
extern "C"  void AndroidAdMobController_OnBannerAdClosed_m334685201 (AndroidAdMobController_t1638300356 * __this, String_t* ___bannerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::OnBannerAdLeftApplication(System.String)
extern "C"  void AndroidAdMobController_OnBannerAdLeftApplication_m3050516574 (AndroidAdMobController_t1638300356 * __this, String_t* ___bannerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::OnInterstitialAdLoaded()
extern "C"  void AndroidAdMobController_OnInterstitialAdLoaded_m4136001112 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::OnInterstitialAdFailedToLoad()
extern "C"  void AndroidAdMobController_OnInterstitialAdFailedToLoad_m4243429485 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::OnInterstitialAdOpened()
extern "C"  void AndroidAdMobController_OnInterstitialAdOpened_m2314960104 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::OnInterstitialAdClosed()
extern "C"  void AndroidAdMobController_OnInterstitialAdClosed_m1292017277 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::OnInterstitialAdLeftApplication()
extern "C"  void AndroidAdMobController_OnInterstitialAdLeftApplication_m3261689900 (AndroidAdMobController_t1638300356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::OnInAppPurchaseRequested(System.String)
extern "C"  void AndroidAdMobController_OnInAppPurchaseRequested_m2688523459 (AndroidAdMobController_t1638300356 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::<_OnInterstitialLoaded>m__7F()
extern "C"  void AndroidAdMobController_U3C_OnInterstitialLoadedU3Em__7F_m2943798590 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::<_OnInterstitialFailedLoading>m__80()
extern "C"  void AndroidAdMobController_U3C_OnInterstitialFailedLoadingU3Em__80_m1015147175 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::<_OnInterstitialOpened>m__81()
extern "C"  void AndroidAdMobController_U3C_OnInterstitialOpenedU3Em__81_m3519485724 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::<_OnInterstitialClosed>m__82()
extern "C"  void AndroidAdMobController_U3C_OnInterstitialClosedU3Em__82_m3923144680 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::<_OnInterstitialLeftApplication>m__83()
extern "C"  void AndroidAdMobController_U3C_OnInterstitialLeftApplicationU3Em__83_m843275062 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobController::<_OnAdInAppRequest>m__84(System.String)
extern "C"  void AndroidAdMobController_U3C_OnAdInAppRequestU3Em__84_m2011696212 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
