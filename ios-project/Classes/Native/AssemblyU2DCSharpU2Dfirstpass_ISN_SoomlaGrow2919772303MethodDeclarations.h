﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_SoomlaGrow
struct ISN_SoomlaGrow_t2919772303;
// System.Action
struct Action_t3226471752;
// IOSStoreKitVerificationResponse
struct IOSStoreKitVerificationResponse_t4263658582;
// IOSStoreKitRestoreResult
struct IOSStoreKitRestoreResult_t3305276155;
// System.String
struct String_t;
// IOSStoreKitResult
struct IOSStoreKitResult_t2359407583;
// SA.Common.Models.Result
struct Result_t4287219743;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreKitVerificat4263658582.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreKitRestoreRe3305276155.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreKitResult2359407583.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_SoomlaEvent319858442.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_SoomlaAction4294445400.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_SoomlaProvider2510794499.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"

// System.Void ISN_SoomlaGrow::.ctor()
extern "C"  void ISN_SoomlaGrow__ctor_m3489608198 (ISN_SoomlaGrow_t2919772303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::.cctor()
extern "C"  void ISN_SoomlaGrow__cctor_m2502869779 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::add_ActionInitialized(System.Action)
extern "C"  void ISN_SoomlaGrow_add_ActionInitialized_m3740854433 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::remove_ActionInitialized(System.Action)
extern "C"  void ISN_SoomlaGrow_remove_ActionInitialized_m123891206 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::add_ActionConnected(System.Action)
extern "C"  void ISN_SoomlaGrow_add_ActionConnected_m346076818 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::remove_ActionConnected(System.Action)
extern "C"  void ISN_SoomlaGrow_remove_ActionConnected_m2341735185 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::add_ActionDisconnected(System.Action)
extern "C"  void ISN_SoomlaGrow_add_ActionDisconnected_m1966954112 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::remove_ActionDisconnected(System.Action)
extern "C"  void ISN_SoomlaGrow_remove_ActionDisconnected_m704724599 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::CreateObject()
extern "C"  void ISN_SoomlaGrow_CreateObject_m568389185 (ISN_SoomlaGrow_t2919772303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::Init()
extern "C"  void ISN_SoomlaGrow_Init_m52979042 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::HandleOnVerificationComplete(IOSStoreKitVerificationResponse)
extern "C"  void ISN_SoomlaGrow_HandleOnVerificationComplete_m3008648157 (Il2CppObject * __this /* static, unused */, IOSStoreKitVerificationResponse_t4263658582 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::HandleOnRestoreComplete(IOSStoreKitRestoreResult)
extern "C"  void ISN_SoomlaGrow_HandleOnRestoreComplete_m1977907639 (Il2CppObject * __this /* static, unused */, IOSStoreKitRestoreResult_t3305276155 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::HandleOnRestoreStarted()
extern "C"  void ISN_SoomlaGrow_HandleOnRestoreStarted_m109026008 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::HandleOnTransactionStarted(System.String)
extern "C"  void ISN_SoomlaGrow_HandleOnTransactionStarted_m113890600 (Il2CppObject * __this /* static, unused */, String_t* ___prodcutId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::HandleOnTransactionComplete(IOSStoreKitResult)
extern "C"  void ISN_SoomlaGrow_HandleOnTransactionComplete_m3554152635 (Il2CppObject * __this /* static, unused */, IOSStoreKitResult_t2359407583 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::SocialAction(ISN_SoomlaEvent,ISN_SoomlaAction,ISN_SoomlaProvider)
extern "C"  void ISN_SoomlaGrow_SocialAction_m1096911894 (Il2CppObject * __this /* static, unused */, int32_t ___soomlaEvent0, int32_t ___action1, int32_t ___provider2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::PurchaseStarted(System.String)
extern "C"  void ISN_SoomlaGrow_PurchaseStarted_m3698963668 (Il2CppObject * __this /* static, unused */, String_t* ___prodcutId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::PurchaseFinished(System.String,System.String,System.String)
extern "C"  void ISN_SoomlaGrow_PurchaseFinished_m4130464135 (Il2CppObject * __this /* static, unused */, String_t* ___prodcutId0, String_t* ___priceInMicros1, String_t* ___currency2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::PurchaseCanceled(System.String)
extern "C"  void ISN_SoomlaGrow_PurchaseCanceled_m4289658834 (Il2CppObject * __this /* static, unused */, String_t* ___prodcutId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::SetPurchasesSupportedState(System.Boolean)
extern "C"  void ISN_SoomlaGrow_SetPurchasesSupportedState_m365321970 (Il2CppObject * __this /* static, unused */, bool ___isSupported0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::PurchaseError()
extern "C"  void ISN_SoomlaGrow_PurchaseError_m1185760643 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::VerificationFailed()
extern "C"  void ISN_SoomlaGrow_VerificationFailed_m2739917954 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::RestoreStarted()
extern "C"  void ISN_SoomlaGrow_RestoreStarted_m1308324641 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::RestoreFinished(System.Boolean)
extern "C"  void ISN_SoomlaGrow_RestoreFinished_m4079806645 (Il2CppObject * __this /* static, unused */, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ISN_SoomlaGrow::get_IsInitialized()
extern "C"  bool ISN_SoomlaGrow_get_IsInitialized_m366886507 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::OnHighWayInitialized()
extern "C"  void ISN_SoomlaGrow_OnHighWayInitialized_m3650849326 (ISN_SoomlaGrow_t2919772303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::OnHihgWayConnected()
extern "C"  void ISN_SoomlaGrow_OnHihgWayConnected_m1567942251 (ISN_SoomlaGrow_t2919772303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::OnHihgWayDisconnected()
extern "C"  void ISN_SoomlaGrow_OnHihgWayDisconnected_m3020832325 (ISN_SoomlaGrow_t2919772303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::HandleOnInstagramPostResult(SA.Common.Models.Result)
extern "C"  void ISN_SoomlaGrow_HandleOnInstagramPostResult_m714148694 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::HandleOnTwitterPostResult(SA.Common.Models.Result)
extern "C"  void ISN_SoomlaGrow_HandleOnTwitterPostResult_m138761963 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::HandleOnInstagramPostStart()
extern "C"  void ISN_SoomlaGrow_HandleOnInstagramPostStart_m2175084991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::HandleOnTwitterPostStart()
extern "C"  void ISN_SoomlaGrow_HandleOnTwitterPostStart_m511472268 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::HandleOnFacebookPostStart()
extern "C"  void ISN_SoomlaGrow_HandleOnFacebookPostStart_m4023624483 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::HandleOnFacebookPostResult(SA.Common.Models.Result)
extern "C"  void ISN_SoomlaGrow_HandleOnFacebookPostResult_m3449467030 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::<ActionInitialized>m__0()
extern "C"  void ISN_SoomlaGrow_U3CActionInitializedU3Em__0_m1636658637 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::<ActionConnected>m__1()
extern "C"  void ISN_SoomlaGrow_U3CActionConnectedU3Em__1_m1438116715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_SoomlaGrow::<ActionDisconnected>m__2()
extern "C"  void ISN_SoomlaGrow_U3CActionDisconnectedU3Em__2_m3723674840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
