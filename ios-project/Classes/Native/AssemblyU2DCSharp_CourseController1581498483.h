﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// Jump
struct Jump_t114869516;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Camera
struct Camera_t189460977;
// System.Collections.Generic.List`1<Jump>
struct List_1_t3778957944;

#include "AssemblyU2DCSharp_MCGBehaviour3307770884.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CourseController
struct  CourseController_t1581498483  : public MCGBehaviour_t3307770884
{
public:
	// UnityEngine.GameObject CourseController::resultImage
	GameObject_t1756533147 * ___resultImage_4;
	// UnityEngine.LineRenderer CourseController::lineRenderer
	LineRenderer_t849157671 * ___lineRenderer_5;
	// UnityEngine.GameObject CourseController::LineStartEndPoint
	GameObject_t1756533147 * ___LineStartEndPoint_6;
	// UnityEngine.GameObject CourseController::LineDotParent
	GameObject_t1756533147 * ___LineDotParent_7;
	// UnityEngine.UI.Button CourseController::btn_erase
	Button_t2872111280 * ___btn_erase_8;
	// UnityEngine.UI.Button CourseController::btn_menu
	Button_t2872111280 * ___btn_menu_9;
	// UnityEngine.GameObject CourseController::Menu
	GameObject_t1756533147 * ___Menu_10;
	// UnityEngine.UI.Button CourseController::btn_closeMenu
	Button_t2872111280 * ___btn_closeMenu_11;
	// UnityEngine.UI.Button CourseController::btn_startOver
	Button_t2872111280 * ___btn_startOver_12;
	// UnityEngine.UI.Button CourseController::btn_menu_visualize
	Button_t2872111280 * ___btn_menu_visualize_13;
	// UnityEngine.UI.Button CourseController::btn_save
	Button_t2872111280 * ___btn_save_14;
	// UnityEngine.UI.Button CourseController::btn_exit
	Button_t2872111280 * ___btn_exit_15;
	// UnityEngine.UI.Button CourseController::btn_toggleImage
	Button_t2872111280 * ___btn_toggleImage_16;
	// UnityEngine.Sprite CourseController::imageOn
	Sprite_t309593783 * ___imageOn_17;
	// UnityEngine.Sprite CourseController::imageOff
	Sprite_t309593783 * ___imageOff_18;
	// UnityEngine.UI.Button CourseController::btn_toggleCheats
	Button_t2872111280 * ___btn_toggleCheats_19;
	// UnityEngine.UI.Button CourseController::btn_drawJumps
	Button_t2872111280 * ___btn_drawJumps_20;
	// UnityEngine.Sprite CourseController::drawing
	Sprite_t309593783 * ___drawing_21;
	// UnityEngine.Sprite CourseController::jumps
	Sprite_t309593783 * ___jumps_22;
	// UnityEngine.GameObject CourseController::JumpPrefab
	GameObject_t1756533147 * ___JumpPrefab_23;
	// UnityEngine.GameObject CourseController::JumpSliderObject
	GameObject_t1756533147 * ___JumpSliderObject_24;
	// UnityEngine.GameObject CourseController::JumpSliderHandle
	GameObject_t1756533147 * ___JumpSliderHandle_25;
	// UnityEngine.UI.Slider CourseController::JumpSlider
	Slider_t297367283 * ___JumpSlider_26;
	// UnityEngine.GameObject CourseController::JumpToggleImage
	GameObject_t1756533147 * ___JumpToggleImage_27;
	// UnityEngine.GameObject CourseController::CloseMenuImage
	GameObject_t1756533147 * ___CloseMenuImage_28;
	// UnityEngine.GameObject CourseController::StartPopup
	GameObject_t1756533147 * ___StartPopup_29;
	// UnityEngine.GameObject CourseController::CompletePopup
	GameObject_t1756533147 * ___CompletePopup_30;
	// UnityEngine.GameObject CourseController::MissedPopup
	GameObject_t1756533147 * ___MissedPopup_31;
	// UnityEngine.UI.Button CourseController::btn_StartPopup
	Button_t2872111280 * ___btn_StartPopup_32;
	// UnityEngine.UI.Button CourseController::btn_CompletePopup
	Button_t2872111280 * ___btn_CompletePopup_33;
	// UnityEngine.UI.Button CourseController::btn_MissedPopup
	Button_t2872111280 * ___btn_MissedPopup_34;
	// UnityEngine.GameObject CourseController::Training_Overlay_1
	GameObject_t1756533147 * ___Training_Overlay_1_36;
	// UnityEngine.GameObject CourseController::Training_Overlay_2
	GameObject_t1756533147 * ___Training_Overlay_2_37;
	// UnityEngine.GameObject CourseController::Training_Overlay_3
	GameObject_t1756533147 * ___Training_Overlay_3_38;
	// UnityEngine.UI.Button CourseController::btn_toggleTutorial
	Button_t2872111280 * ___btn_toggleTutorial_39;
	// UnityEngine.UI.Button CourseController::btn_closeTutorial1
	Button_t2872111280 * ___btn_closeTutorial1_40;
	// UnityEngine.UI.Button CourseController::btn_closeTutorial2
	Button_t2872111280 * ___btn_closeTutorial2_41;
	// UnityEngine.UI.Button CourseController::btn_closeTutorial3
	Button_t2872111280 * ___btn_closeTutorial3_42;
	// UnityEngine.GameObject CourseController::TempCourseImage
	GameObject_t1756533147 * ___TempCourseImage_43;
	// UnityEngine.GameObject CourseController::waiting
	GameObject_t1756533147 * ___waiting_44;
	// UnityEngine.UI.Button CourseController::btn_visualize
	Button_t2872111280 * ___btn_visualize_45;
	// UnityEngine.UI.Button CourseController::btn_complete_close
	Button_t2872111280 * ___btn_complete_close_46;
	// System.Boolean CourseController::startDraw
	bool ___startDraw_47;
	// UnityEngine.Vector3 CourseController::lastPosition
	Vector3_t2243707580  ___lastPosition_48;
	// UnityEngine.Vector3 CourseController::newPosition
	Vector3_t2243707580  ___newPosition_49;
	// System.Boolean CourseController::tapped
	bool ___tapped_50;
	// UnityEngine.GameObject CourseController::startPoint
	GameObject_t1756533147 * ___startPoint_51;
	// UnityEngine.GameObject CourseController::endPoint
	GameObject_t1756533147 * ___endPoint_52;
	// System.Boolean CourseController::showingCheats
	bool ___showingCheats_53;
	// System.Boolean CourseController::isImageShown
	bool ___isImageShown_54;
	// System.Int32 CourseController::numPopups
	int32_t ___numPopups_55;
	// System.Boolean CourseController::placingJumps
	bool ___placingJumps_56;
	// Jump CourseController::jump
	Jump_t114869516 * ___jump_57;
	// System.Int32 CourseController::NextJumpNumber
	int32_t ___NextJumpNumber_58;
	// System.Int32 CourseController::lastJumpNumber
	int32_t ___lastJumpNumber_59;
	// System.Boolean CourseController::showingPopup
	bool ___showingPopup_60;
	// System.Int32 CourseController::jumpSize
	int32_t ___jumpSize_61;
	// System.Single CourseController::TIMER_MAX
	float ___TIMER_MAX_62;
	// UnityEngine.Vector3 CourseController::btn_menuLarge
	Vector3_t2243707580  ___btn_menuLarge_63;
	// UnityEngine.Vector3 CourseController::btn_jumpLarge
	Vector3_t2243707580  ___btn_jumpLarge_64;
	// UnityEngine.Vector3 CourseController::btn_eraseLarge
	Vector3_t2243707580  ___btn_eraseLarge_65;
	// UnityEngine.Vector3 CourseController::btn_menuSmall
	Vector3_t2243707580  ___btn_menuSmall_66;
	// UnityEngine.Vector3 CourseController::btn_jumpSmall
	Vector3_t2243707580  ___btn_jumpSmall_67;
	// UnityEngine.Vector3 CourseController::btn_eraseSmall
	Vector3_t2243707580  ___btn_eraseSmall_68;
	// System.Boolean CourseController::shrinkButtons
	bool ___shrinkButtons_69;
	// System.Boolean CourseController::growButtons
	bool ___growButtons_70;
	// System.Int32 CourseController::CourseScreen
	int32_t ___CourseScreen_71;
	// System.Int32 CourseController::currentJumpHitNumber
	int32_t ___currentJumpHitNumber_72;
	// System.Boolean CourseController::newJumpHit
	bool ___newJumpHit_73;
	// System.Int32 CourseController::currentJumpHit1
	int32_t ___currentJumpHit1_74;
	// System.Int32 CourseController::currentJumpHit2
	int32_t ___currentJumpHit2_75;
	// UnityEngine.Canvas CourseController::canvas
	Canvas_t209405766 * ___canvas_76;
	// UnityEngine.Camera CourseController::mainCamera
	Camera_t189460977 * ___mainCamera_77;
	// UnityEngine.GameObject CourseController::jumpEdit
	GameObject_t1756533147 * ___jumpEdit_78;
	// UnityEngine.UI.Button CourseController::btn_jumpEdit_done
	Button_t2872111280 * ___btn_jumpEdit_done_79;
	// UnityEngine.UI.Button CourseController::btn_jumpEdit_type
	Button_t2872111280 * ___btn_jumpEdit_type_80;
	// System.Collections.Generic.List`1<Jump> CourseController::jumpsList
	List_1_t3778957944 * ___jumpsList_81;
	// System.Boolean CourseController::jumpEditListeners
	bool ___jumpEditListeners_82;
	// System.Boolean CourseController::ShowStartTraining
	bool ___ShowStartTraining_83;
	// UnityEngine.GameObject CourseController::viewport
	GameObject_t1756533147 * ___viewport_84;
	// UnityEngine.GameObject CourseController::jumpsGlow
	GameObject_t1756533147 * ___jumpsGlow_85;
	// UnityEngine.GameObject CourseController::footerButtons
	GameObject_t1756533147 * ___footerButtons_86;
	// UnityEngine.Vector3 CourseController::mouseButton0Down_pos
	Vector3_t2243707580  ___mouseButton0Down_pos_87;
	// System.Single CourseController::tapMoveEpsilon
	float ___tapMoveEpsilon_88;
	// System.Boolean CourseController::wasMultitouch
	bool ___wasMultitouch_89;
	// System.Boolean CourseController::mouseInCouseArea
	bool ___mouseInCouseArea_90;
	// System.Single CourseController::touch_t0
	float ___touch_t0_91;
	// System.Boolean CourseController::tapCancelled
	bool ___tapCancelled_92;
	// UnityEngine.Vector2 CourseController::newScreenPosition
	Vector2_t2243707579  ___newScreenPosition_93;
	// UnityEngine.Vector2 CourseController::prevScreenPosition
	Vector2_t2243707579  ___prevScreenPosition_94;
	// System.Single CourseController::lastActivityTime
	float ___lastActivityTime_95;
	// System.Int32 CourseController::prevNumPopups
	int32_t ___prevNumPopups_96;
	// UnityEngine.GameObject CourseController::lastHit
	GameObject_t1756533147 * ___lastHit_97;
	// System.Boolean CourseController::haveFirstCollider
	bool ___haveFirstCollider_98;
	// System.Boolean CourseController::trackFail
	bool ___trackFail_99;
	// System.Int32 CourseController::numPointsAdded
	int32_t ___numPointsAdded_100;

public:
	inline static int32_t get_offset_of_resultImage_4() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___resultImage_4)); }
	inline GameObject_t1756533147 * get_resultImage_4() const { return ___resultImage_4; }
	inline GameObject_t1756533147 ** get_address_of_resultImage_4() { return &___resultImage_4; }
	inline void set_resultImage_4(GameObject_t1756533147 * value)
	{
		___resultImage_4 = value;
		Il2CppCodeGenWriteBarrier(&___resultImage_4, value);
	}

	inline static int32_t get_offset_of_lineRenderer_5() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___lineRenderer_5)); }
	inline LineRenderer_t849157671 * get_lineRenderer_5() const { return ___lineRenderer_5; }
	inline LineRenderer_t849157671 ** get_address_of_lineRenderer_5() { return &___lineRenderer_5; }
	inline void set_lineRenderer_5(LineRenderer_t849157671 * value)
	{
		___lineRenderer_5 = value;
		Il2CppCodeGenWriteBarrier(&___lineRenderer_5, value);
	}

	inline static int32_t get_offset_of_LineStartEndPoint_6() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___LineStartEndPoint_6)); }
	inline GameObject_t1756533147 * get_LineStartEndPoint_6() const { return ___LineStartEndPoint_6; }
	inline GameObject_t1756533147 ** get_address_of_LineStartEndPoint_6() { return &___LineStartEndPoint_6; }
	inline void set_LineStartEndPoint_6(GameObject_t1756533147 * value)
	{
		___LineStartEndPoint_6 = value;
		Il2CppCodeGenWriteBarrier(&___LineStartEndPoint_6, value);
	}

	inline static int32_t get_offset_of_LineDotParent_7() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___LineDotParent_7)); }
	inline GameObject_t1756533147 * get_LineDotParent_7() const { return ___LineDotParent_7; }
	inline GameObject_t1756533147 ** get_address_of_LineDotParent_7() { return &___LineDotParent_7; }
	inline void set_LineDotParent_7(GameObject_t1756533147 * value)
	{
		___LineDotParent_7 = value;
		Il2CppCodeGenWriteBarrier(&___LineDotParent_7, value);
	}

	inline static int32_t get_offset_of_btn_erase_8() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_erase_8)); }
	inline Button_t2872111280 * get_btn_erase_8() const { return ___btn_erase_8; }
	inline Button_t2872111280 ** get_address_of_btn_erase_8() { return &___btn_erase_8; }
	inline void set_btn_erase_8(Button_t2872111280 * value)
	{
		___btn_erase_8 = value;
		Il2CppCodeGenWriteBarrier(&___btn_erase_8, value);
	}

	inline static int32_t get_offset_of_btn_menu_9() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_menu_9)); }
	inline Button_t2872111280 * get_btn_menu_9() const { return ___btn_menu_9; }
	inline Button_t2872111280 ** get_address_of_btn_menu_9() { return &___btn_menu_9; }
	inline void set_btn_menu_9(Button_t2872111280 * value)
	{
		___btn_menu_9 = value;
		Il2CppCodeGenWriteBarrier(&___btn_menu_9, value);
	}

	inline static int32_t get_offset_of_Menu_10() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___Menu_10)); }
	inline GameObject_t1756533147 * get_Menu_10() const { return ___Menu_10; }
	inline GameObject_t1756533147 ** get_address_of_Menu_10() { return &___Menu_10; }
	inline void set_Menu_10(GameObject_t1756533147 * value)
	{
		___Menu_10 = value;
		Il2CppCodeGenWriteBarrier(&___Menu_10, value);
	}

	inline static int32_t get_offset_of_btn_closeMenu_11() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_closeMenu_11)); }
	inline Button_t2872111280 * get_btn_closeMenu_11() const { return ___btn_closeMenu_11; }
	inline Button_t2872111280 ** get_address_of_btn_closeMenu_11() { return &___btn_closeMenu_11; }
	inline void set_btn_closeMenu_11(Button_t2872111280 * value)
	{
		___btn_closeMenu_11 = value;
		Il2CppCodeGenWriteBarrier(&___btn_closeMenu_11, value);
	}

	inline static int32_t get_offset_of_btn_startOver_12() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_startOver_12)); }
	inline Button_t2872111280 * get_btn_startOver_12() const { return ___btn_startOver_12; }
	inline Button_t2872111280 ** get_address_of_btn_startOver_12() { return &___btn_startOver_12; }
	inline void set_btn_startOver_12(Button_t2872111280 * value)
	{
		___btn_startOver_12 = value;
		Il2CppCodeGenWriteBarrier(&___btn_startOver_12, value);
	}

	inline static int32_t get_offset_of_btn_menu_visualize_13() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_menu_visualize_13)); }
	inline Button_t2872111280 * get_btn_menu_visualize_13() const { return ___btn_menu_visualize_13; }
	inline Button_t2872111280 ** get_address_of_btn_menu_visualize_13() { return &___btn_menu_visualize_13; }
	inline void set_btn_menu_visualize_13(Button_t2872111280 * value)
	{
		___btn_menu_visualize_13 = value;
		Il2CppCodeGenWriteBarrier(&___btn_menu_visualize_13, value);
	}

	inline static int32_t get_offset_of_btn_save_14() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_save_14)); }
	inline Button_t2872111280 * get_btn_save_14() const { return ___btn_save_14; }
	inline Button_t2872111280 ** get_address_of_btn_save_14() { return &___btn_save_14; }
	inline void set_btn_save_14(Button_t2872111280 * value)
	{
		___btn_save_14 = value;
		Il2CppCodeGenWriteBarrier(&___btn_save_14, value);
	}

	inline static int32_t get_offset_of_btn_exit_15() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_exit_15)); }
	inline Button_t2872111280 * get_btn_exit_15() const { return ___btn_exit_15; }
	inline Button_t2872111280 ** get_address_of_btn_exit_15() { return &___btn_exit_15; }
	inline void set_btn_exit_15(Button_t2872111280 * value)
	{
		___btn_exit_15 = value;
		Il2CppCodeGenWriteBarrier(&___btn_exit_15, value);
	}

	inline static int32_t get_offset_of_btn_toggleImage_16() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_toggleImage_16)); }
	inline Button_t2872111280 * get_btn_toggleImage_16() const { return ___btn_toggleImage_16; }
	inline Button_t2872111280 ** get_address_of_btn_toggleImage_16() { return &___btn_toggleImage_16; }
	inline void set_btn_toggleImage_16(Button_t2872111280 * value)
	{
		___btn_toggleImage_16 = value;
		Il2CppCodeGenWriteBarrier(&___btn_toggleImage_16, value);
	}

	inline static int32_t get_offset_of_imageOn_17() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___imageOn_17)); }
	inline Sprite_t309593783 * get_imageOn_17() const { return ___imageOn_17; }
	inline Sprite_t309593783 ** get_address_of_imageOn_17() { return &___imageOn_17; }
	inline void set_imageOn_17(Sprite_t309593783 * value)
	{
		___imageOn_17 = value;
		Il2CppCodeGenWriteBarrier(&___imageOn_17, value);
	}

	inline static int32_t get_offset_of_imageOff_18() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___imageOff_18)); }
	inline Sprite_t309593783 * get_imageOff_18() const { return ___imageOff_18; }
	inline Sprite_t309593783 ** get_address_of_imageOff_18() { return &___imageOff_18; }
	inline void set_imageOff_18(Sprite_t309593783 * value)
	{
		___imageOff_18 = value;
		Il2CppCodeGenWriteBarrier(&___imageOff_18, value);
	}

	inline static int32_t get_offset_of_btn_toggleCheats_19() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_toggleCheats_19)); }
	inline Button_t2872111280 * get_btn_toggleCheats_19() const { return ___btn_toggleCheats_19; }
	inline Button_t2872111280 ** get_address_of_btn_toggleCheats_19() { return &___btn_toggleCheats_19; }
	inline void set_btn_toggleCheats_19(Button_t2872111280 * value)
	{
		___btn_toggleCheats_19 = value;
		Il2CppCodeGenWriteBarrier(&___btn_toggleCheats_19, value);
	}

	inline static int32_t get_offset_of_btn_drawJumps_20() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_drawJumps_20)); }
	inline Button_t2872111280 * get_btn_drawJumps_20() const { return ___btn_drawJumps_20; }
	inline Button_t2872111280 ** get_address_of_btn_drawJumps_20() { return &___btn_drawJumps_20; }
	inline void set_btn_drawJumps_20(Button_t2872111280 * value)
	{
		___btn_drawJumps_20 = value;
		Il2CppCodeGenWriteBarrier(&___btn_drawJumps_20, value);
	}

	inline static int32_t get_offset_of_drawing_21() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___drawing_21)); }
	inline Sprite_t309593783 * get_drawing_21() const { return ___drawing_21; }
	inline Sprite_t309593783 ** get_address_of_drawing_21() { return &___drawing_21; }
	inline void set_drawing_21(Sprite_t309593783 * value)
	{
		___drawing_21 = value;
		Il2CppCodeGenWriteBarrier(&___drawing_21, value);
	}

	inline static int32_t get_offset_of_jumps_22() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___jumps_22)); }
	inline Sprite_t309593783 * get_jumps_22() const { return ___jumps_22; }
	inline Sprite_t309593783 ** get_address_of_jumps_22() { return &___jumps_22; }
	inline void set_jumps_22(Sprite_t309593783 * value)
	{
		___jumps_22 = value;
		Il2CppCodeGenWriteBarrier(&___jumps_22, value);
	}

	inline static int32_t get_offset_of_JumpPrefab_23() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___JumpPrefab_23)); }
	inline GameObject_t1756533147 * get_JumpPrefab_23() const { return ___JumpPrefab_23; }
	inline GameObject_t1756533147 ** get_address_of_JumpPrefab_23() { return &___JumpPrefab_23; }
	inline void set_JumpPrefab_23(GameObject_t1756533147 * value)
	{
		___JumpPrefab_23 = value;
		Il2CppCodeGenWriteBarrier(&___JumpPrefab_23, value);
	}

	inline static int32_t get_offset_of_JumpSliderObject_24() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___JumpSliderObject_24)); }
	inline GameObject_t1756533147 * get_JumpSliderObject_24() const { return ___JumpSliderObject_24; }
	inline GameObject_t1756533147 ** get_address_of_JumpSliderObject_24() { return &___JumpSliderObject_24; }
	inline void set_JumpSliderObject_24(GameObject_t1756533147 * value)
	{
		___JumpSliderObject_24 = value;
		Il2CppCodeGenWriteBarrier(&___JumpSliderObject_24, value);
	}

	inline static int32_t get_offset_of_JumpSliderHandle_25() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___JumpSliderHandle_25)); }
	inline GameObject_t1756533147 * get_JumpSliderHandle_25() const { return ___JumpSliderHandle_25; }
	inline GameObject_t1756533147 ** get_address_of_JumpSliderHandle_25() { return &___JumpSliderHandle_25; }
	inline void set_JumpSliderHandle_25(GameObject_t1756533147 * value)
	{
		___JumpSliderHandle_25 = value;
		Il2CppCodeGenWriteBarrier(&___JumpSliderHandle_25, value);
	}

	inline static int32_t get_offset_of_JumpSlider_26() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___JumpSlider_26)); }
	inline Slider_t297367283 * get_JumpSlider_26() const { return ___JumpSlider_26; }
	inline Slider_t297367283 ** get_address_of_JumpSlider_26() { return &___JumpSlider_26; }
	inline void set_JumpSlider_26(Slider_t297367283 * value)
	{
		___JumpSlider_26 = value;
		Il2CppCodeGenWriteBarrier(&___JumpSlider_26, value);
	}

	inline static int32_t get_offset_of_JumpToggleImage_27() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___JumpToggleImage_27)); }
	inline GameObject_t1756533147 * get_JumpToggleImage_27() const { return ___JumpToggleImage_27; }
	inline GameObject_t1756533147 ** get_address_of_JumpToggleImage_27() { return &___JumpToggleImage_27; }
	inline void set_JumpToggleImage_27(GameObject_t1756533147 * value)
	{
		___JumpToggleImage_27 = value;
		Il2CppCodeGenWriteBarrier(&___JumpToggleImage_27, value);
	}

	inline static int32_t get_offset_of_CloseMenuImage_28() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___CloseMenuImage_28)); }
	inline GameObject_t1756533147 * get_CloseMenuImage_28() const { return ___CloseMenuImage_28; }
	inline GameObject_t1756533147 ** get_address_of_CloseMenuImage_28() { return &___CloseMenuImage_28; }
	inline void set_CloseMenuImage_28(GameObject_t1756533147 * value)
	{
		___CloseMenuImage_28 = value;
		Il2CppCodeGenWriteBarrier(&___CloseMenuImage_28, value);
	}

	inline static int32_t get_offset_of_StartPopup_29() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___StartPopup_29)); }
	inline GameObject_t1756533147 * get_StartPopup_29() const { return ___StartPopup_29; }
	inline GameObject_t1756533147 ** get_address_of_StartPopup_29() { return &___StartPopup_29; }
	inline void set_StartPopup_29(GameObject_t1756533147 * value)
	{
		___StartPopup_29 = value;
		Il2CppCodeGenWriteBarrier(&___StartPopup_29, value);
	}

	inline static int32_t get_offset_of_CompletePopup_30() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___CompletePopup_30)); }
	inline GameObject_t1756533147 * get_CompletePopup_30() const { return ___CompletePopup_30; }
	inline GameObject_t1756533147 ** get_address_of_CompletePopup_30() { return &___CompletePopup_30; }
	inline void set_CompletePopup_30(GameObject_t1756533147 * value)
	{
		___CompletePopup_30 = value;
		Il2CppCodeGenWriteBarrier(&___CompletePopup_30, value);
	}

	inline static int32_t get_offset_of_MissedPopup_31() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___MissedPopup_31)); }
	inline GameObject_t1756533147 * get_MissedPopup_31() const { return ___MissedPopup_31; }
	inline GameObject_t1756533147 ** get_address_of_MissedPopup_31() { return &___MissedPopup_31; }
	inline void set_MissedPopup_31(GameObject_t1756533147 * value)
	{
		___MissedPopup_31 = value;
		Il2CppCodeGenWriteBarrier(&___MissedPopup_31, value);
	}

	inline static int32_t get_offset_of_btn_StartPopup_32() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_StartPopup_32)); }
	inline Button_t2872111280 * get_btn_StartPopup_32() const { return ___btn_StartPopup_32; }
	inline Button_t2872111280 ** get_address_of_btn_StartPopup_32() { return &___btn_StartPopup_32; }
	inline void set_btn_StartPopup_32(Button_t2872111280 * value)
	{
		___btn_StartPopup_32 = value;
		Il2CppCodeGenWriteBarrier(&___btn_StartPopup_32, value);
	}

	inline static int32_t get_offset_of_btn_CompletePopup_33() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_CompletePopup_33)); }
	inline Button_t2872111280 * get_btn_CompletePopup_33() const { return ___btn_CompletePopup_33; }
	inline Button_t2872111280 ** get_address_of_btn_CompletePopup_33() { return &___btn_CompletePopup_33; }
	inline void set_btn_CompletePopup_33(Button_t2872111280 * value)
	{
		___btn_CompletePopup_33 = value;
		Il2CppCodeGenWriteBarrier(&___btn_CompletePopup_33, value);
	}

	inline static int32_t get_offset_of_btn_MissedPopup_34() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_MissedPopup_34)); }
	inline Button_t2872111280 * get_btn_MissedPopup_34() const { return ___btn_MissedPopup_34; }
	inline Button_t2872111280 ** get_address_of_btn_MissedPopup_34() { return &___btn_MissedPopup_34; }
	inline void set_btn_MissedPopup_34(Button_t2872111280 * value)
	{
		___btn_MissedPopup_34 = value;
		Il2CppCodeGenWriteBarrier(&___btn_MissedPopup_34, value);
	}

	inline static int32_t get_offset_of_Training_Overlay_1_36() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___Training_Overlay_1_36)); }
	inline GameObject_t1756533147 * get_Training_Overlay_1_36() const { return ___Training_Overlay_1_36; }
	inline GameObject_t1756533147 ** get_address_of_Training_Overlay_1_36() { return &___Training_Overlay_1_36; }
	inline void set_Training_Overlay_1_36(GameObject_t1756533147 * value)
	{
		___Training_Overlay_1_36 = value;
		Il2CppCodeGenWriteBarrier(&___Training_Overlay_1_36, value);
	}

	inline static int32_t get_offset_of_Training_Overlay_2_37() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___Training_Overlay_2_37)); }
	inline GameObject_t1756533147 * get_Training_Overlay_2_37() const { return ___Training_Overlay_2_37; }
	inline GameObject_t1756533147 ** get_address_of_Training_Overlay_2_37() { return &___Training_Overlay_2_37; }
	inline void set_Training_Overlay_2_37(GameObject_t1756533147 * value)
	{
		___Training_Overlay_2_37 = value;
		Il2CppCodeGenWriteBarrier(&___Training_Overlay_2_37, value);
	}

	inline static int32_t get_offset_of_Training_Overlay_3_38() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___Training_Overlay_3_38)); }
	inline GameObject_t1756533147 * get_Training_Overlay_3_38() const { return ___Training_Overlay_3_38; }
	inline GameObject_t1756533147 ** get_address_of_Training_Overlay_3_38() { return &___Training_Overlay_3_38; }
	inline void set_Training_Overlay_3_38(GameObject_t1756533147 * value)
	{
		___Training_Overlay_3_38 = value;
		Il2CppCodeGenWriteBarrier(&___Training_Overlay_3_38, value);
	}

	inline static int32_t get_offset_of_btn_toggleTutorial_39() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_toggleTutorial_39)); }
	inline Button_t2872111280 * get_btn_toggleTutorial_39() const { return ___btn_toggleTutorial_39; }
	inline Button_t2872111280 ** get_address_of_btn_toggleTutorial_39() { return &___btn_toggleTutorial_39; }
	inline void set_btn_toggleTutorial_39(Button_t2872111280 * value)
	{
		___btn_toggleTutorial_39 = value;
		Il2CppCodeGenWriteBarrier(&___btn_toggleTutorial_39, value);
	}

	inline static int32_t get_offset_of_btn_closeTutorial1_40() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_closeTutorial1_40)); }
	inline Button_t2872111280 * get_btn_closeTutorial1_40() const { return ___btn_closeTutorial1_40; }
	inline Button_t2872111280 ** get_address_of_btn_closeTutorial1_40() { return &___btn_closeTutorial1_40; }
	inline void set_btn_closeTutorial1_40(Button_t2872111280 * value)
	{
		___btn_closeTutorial1_40 = value;
		Il2CppCodeGenWriteBarrier(&___btn_closeTutorial1_40, value);
	}

	inline static int32_t get_offset_of_btn_closeTutorial2_41() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_closeTutorial2_41)); }
	inline Button_t2872111280 * get_btn_closeTutorial2_41() const { return ___btn_closeTutorial2_41; }
	inline Button_t2872111280 ** get_address_of_btn_closeTutorial2_41() { return &___btn_closeTutorial2_41; }
	inline void set_btn_closeTutorial2_41(Button_t2872111280 * value)
	{
		___btn_closeTutorial2_41 = value;
		Il2CppCodeGenWriteBarrier(&___btn_closeTutorial2_41, value);
	}

	inline static int32_t get_offset_of_btn_closeTutorial3_42() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_closeTutorial3_42)); }
	inline Button_t2872111280 * get_btn_closeTutorial3_42() const { return ___btn_closeTutorial3_42; }
	inline Button_t2872111280 ** get_address_of_btn_closeTutorial3_42() { return &___btn_closeTutorial3_42; }
	inline void set_btn_closeTutorial3_42(Button_t2872111280 * value)
	{
		___btn_closeTutorial3_42 = value;
		Il2CppCodeGenWriteBarrier(&___btn_closeTutorial3_42, value);
	}

	inline static int32_t get_offset_of_TempCourseImage_43() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___TempCourseImage_43)); }
	inline GameObject_t1756533147 * get_TempCourseImage_43() const { return ___TempCourseImage_43; }
	inline GameObject_t1756533147 ** get_address_of_TempCourseImage_43() { return &___TempCourseImage_43; }
	inline void set_TempCourseImage_43(GameObject_t1756533147 * value)
	{
		___TempCourseImage_43 = value;
		Il2CppCodeGenWriteBarrier(&___TempCourseImage_43, value);
	}

	inline static int32_t get_offset_of_waiting_44() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___waiting_44)); }
	inline GameObject_t1756533147 * get_waiting_44() const { return ___waiting_44; }
	inline GameObject_t1756533147 ** get_address_of_waiting_44() { return &___waiting_44; }
	inline void set_waiting_44(GameObject_t1756533147 * value)
	{
		___waiting_44 = value;
		Il2CppCodeGenWriteBarrier(&___waiting_44, value);
	}

	inline static int32_t get_offset_of_btn_visualize_45() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_visualize_45)); }
	inline Button_t2872111280 * get_btn_visualize_45() const { return ___btn_visualize_45; }
	inline Button_t2872111280 ** get_address_of_btn_visualize_45() { return &___btn_visualize_45; }
	inline void set_btn_visualize_45(Button_t2872111280 * value)
	{
		___btn_visualize_45 = value;
		Il2CppCodeGenWriteBarrier(&___btn_visualize_45, value);
	}

	inline static int32_t get_offset_of_btn_complete_close_46() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_complete_close_46)); }
	inline Button_t2872111280 * get_btn_complete_close_46() const { return ___btn_complete_close_46; }
	inline Button_t2872111280 ** get_address_of_btn_complete_close_46() { return &___btn_complete_close_46; }
	inline void set_btn_complete_close_46(Button_t2872111280 * value)
	{
		___btn_complete_close_46 = value;
		Il2CppCodeGenWriteBarrier(&___btn_complete_close_46, value);
	}

	inline static int32_t get_offset_of_startDraw_47() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___startDraw_47)); }
	inline bool get_startDraw_47() const { return ___startDraw_47; }
	inline bool* get_address_of_startDraw_47() { return &___startDraw_47; }
	inline void set_startDraw_47(bool value)
	{
		___startDraw_47 = value;
	}

	inline static int32_t get_offset_of_lastPosition_48() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___lastPosition_48)); }
	inline Vector3_t2243707580  get_lastPosition_48() const { return ___lastPosition_48; }
	inline Vector3_t2243707580 * get_address_of_lastPosition_48() { return &___lastPosition_48; }
	inline void set_lastPosition_48(Vector3_t2243707580  value)
	{
		___lastPosition_48 = value;
	}

	inline static int32_t get_offset_of_newPosition_49() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___newPosition_49)); }
	inline Vector3_t2243707580  get_newPosition_49() const { return ___newPosition_49; }
	inline Vector3_t2243707580 * get_address_of_newPosition_49() { return &___newPosition_49; }
	inline void set_newPosition_49(Vector3_t2243707580  value)
	{
		___newPosition_49 = value;
	}

	inline static int32_t get_offset_of_tapped_50() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___tapped_50)); }
	inline bool get_tapped_50() const { return ___tapped_50; }
	inline bool* get_address_of_tapped_50() { return &___tapped_50; }
	inline void set_tapped_50(bool value)
	{
		___tapped_50 = value;
	}

	inline static int32_t get_offset_of_startPoint_51() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___startPoint_51)); }
	inline GameObject_t1756533147 * get_startPoint_51() const { return ___startPoint_51; }
	inline GameObject_t1756533147 ** get_address_of_startPoint_51() { return &___startPoint_51; }
	inline void set_startPoint_51(GameObject_t1756533147 * value)
	{
		___startPoint_51 = value;
		Il2CppCodeGenWriteBarrier(&___startPoint_51, value);
	}

	inline static int32_t get_offset_of_endPoint_52() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___endPoint_52)); }
	inline GameObject_t1756533147 * get_endPoint_52() const { return ___endPoint_52; }
	inline GameObject_t1756533147 ** get_address_of_endPoint_52() { return &___endPoint_52; }
	inline void set_endPoint_52(GameObject_t1756533147 * value)
	{
		___endPoint_52 = value;
		Il2CppCodeGenWriteBarrier(&___endPoint_52, value);
	}

	inline static int32_t get_offset_of_showingCheats_53() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___showingCheats_53)); }
	inline bool get_showingCheats_53() const { return ___showingCheats_53; }
	inline bool* get_address_of_showingCheats_53() { return &___showingCheats_53; }
	inline void set_showingCheats_53(bool value)
	{
		___showingCheats_53 = value;
	}

	inline static int32_t get_offset_of_isImageShown_54() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___isImageShown_54)); }
	inline bool get_isImageShown_54() const { return ___isImageShown_54; }
	inline bool* get_address_of_isImageShown_54() { return &___isImageShown_54; }
	inline void set_isImageShown_54(bool value)
	{
		___isImageShown_54 = value;
	}

	inline static int32_t get_offset_of_numPopups_55() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___numPopups_55)); }
	inline int32_t get_numPopups_55() const { return ___numPopups_55; }
	inline int32_t* get_address_of_numPopups_55() { return &___numPopups_55; }
	inline void set_numPopups_55(int32_t value)
	{
		___numPopups_55 = value;
	}

	inline static int32_t get_offset_of_placingJumps_56() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___placingJumps_56)); }
	inline bool get_placingJumps_56() const { return ___placingJumps_56; }
	inline bool* get_address_of_placingJumps_56() { return &___placingJumps_56; }
	inline void set_placingJumps_56(bool value)
	{
		___placingJumps_56 = value;
	}

	inline static int32_t get_offset_of_jump_57() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___jump_57)); }
	inline Jump_t114869516 * get_jump_57() const { return ___jump_57; }
	inline Jump_t114869516 ** get_address_of_jump_57() { return &___jump_57; }
	inline void set_jump_57(Jump_t114869516 * value)
	{
		___jump_57 = value;
		Il2CppCodeGenWriteBarrier(&___jump_57, value);
	}

	inline static int32_t get_offset_of_NextJumpNumber_58() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___NextJumpNumber_58)); }
	inline int32_t get_NextJumpNumber_58() const { return ___NextJumpNumber_58; }
	inline int32_t* get_address_of_NextJumpNumber_58() { return &___NextJumpNumber_58; }
	inline void set_NextJumpNumber_58(int32_t value)
	{
		___NextJumpNumber_58 = value;
	}

	inline static int32_t get_offset_of_lastJumpNumber_59() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___lastJumpNumber_59)); }
	inline int32_t get_lastJumpNumber_59() const { return ___lastJumpNumber_59; }
	inline int32_t* get_address_of_lastJumpNumber_59() { return &___lastJumpNumber_59; }
	inline void set_lastJumpNumber_59(int32_t value)
	{
		___lastJumpNumber_59 = value;
	}

	inline static int32_t get_offset_of_showingPopup_60() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___showingPopup_60)); }
	inline bool get_showingPopup_60() const { return ___showingPopup_60; }
	inline bool* get_address_of_showingPopup_60() { return &___showingPopup_60; }
	inline void set_showingPopup_60(bool value)
	{
		___showingPopup_60 = value;
	}

	inline static int32_t get_offset_of_jumpSize_61() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___jumpSize_61)); }
	inline int32_t get_jumpSize_61() const { return ___jumpSize_61; }
	inline int32_t* get_address_of_jumpSize_61() { return &___jumpSize_61; }
	inline void set_jumpSize_61(int32_t value)
	{
		___jumpSize_61 = value;
	}

	inline static int32_t get_offset_of_TIMER_MAX_62() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___TIMER_MAX_62)); }
	inline float get_TIMER_MAX_62() const { return ___TIMER_MAX_62; }
	inline float* get_address_of_TIMER_MAX_62() { return &___TIMER_MAX_62; }
	inline void set_TIMER_MAX_62(float value)
	{
		___TIMER_MAX_62 = value;
	}

	inline static int32_t get_offset_of_btn_menuLarge_63() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_menuLarge_63)); }
	inline Vector3_t2243707580  get_btn_menuLarge_63() const { return ___btn_menuLarge_63; }
	inline Vector3_t2243707580 * get_address_of_btn_menuLarge_63() { return &___btn_menuLarge_63; }
	inline void set_btn_menuLarge_63(Vector3_t2243707580  value)
	{
		___btn_menuLarge_63 = value;
	}

	inline static int32_t get_offset_of_btn_jumpLarge_64() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_jumpLarge_64)); }
	inline Vector3_t2243707580  get_btn_jumpLarge_64() const { return ___btn_jumpLarge_64; }
	inline Vector3_t2243707580 * get_address_of_btn_jumpLarge_64() { return &___btn_jumpLarge_64; }
	inline void set_btn_jumpLarge_64(Vector3_t2243707580  value)
	{
		___btn_jumpLarge_64 = value;
	}

	inline static int32_t get_offset_of_btn_eraseLarge_65() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_eraseLarge_65)); }
	inline Vector3_t2243707580  get_btn_eraseLarge_65() const { return ___btn_eraseLarge_65; }
	inline Vector3_t2243707580 * get_address_of_btn_eraseLarge_65() { return &___btn_eraseLarge_65; }
	inline void set_btn_eraseLarge_65(Vector3_t2243707580  value)
	{
		___btn_eraseLarge_65 = value;
	}

	inline static int32_t get_offset_of_btn_menuSmall_66() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_menuSmall_66)); }
	inline Vector3_t2243707580  get_btn_menuSmall_66() const { return ___btn_menuSmall_66; }
	inline Vector3_t2243707580 * get_address_of_btn_menuSmall_66() { return &___btn_menuSmall_66; }
	inline void set_btn_menuSmall_66(Vector3_t2243707580  value)
	{
		___btn_menuSmall_66 = value;
	}

	inline static int32_t get_offset_of_btn_jumpSmall_67() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_jumpSmall_67)); }
	inline Vector3_t2243707580  get_btn_jumpSmall_67() const { return ___btn_jumpSmall_67; }
	inline Vector3_t2243707580 * get_address_of_btn_jumpSmall_67() { return &___btn_jumpSmall_67; }
	inline void set_btn_jumpSmall_67(Vector3_t2243707580  value)
	{
		___btn_jumpSmall_67 = value;
	}

	inline static int32_t get_offset_of_btn_eraseSmall_68() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_eraseSmall_68)); }
	inline Vector3_t2243707580  get_btn_eraseSmall_68() const { return ___btn_eraseSmall_68; }
	inline Vector3_t2243707580 * get_address_of_btn_eraseSmall_68() { return &___btn_eraseSmall_68; }
	inline void set_btn_eraseSmall_68(Vector3_t2243707580  value)
	{
		___btn_eraseSmall_68 = value;
	}

	inline static int32_t get_offset_of_shrinkButtons_69() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___shrinkButtons_69)); }
	inline bool get_shrinkButtons_69() const { return ___shrinkButtons_69; }
	inline bool* get_address_of_shrinkButtons_69() { return &___shrinkButtons_69; }
	inline void set_shrinkButtons_69(bool value)
	{
		___shrinkButtons_69 = value;
	}

	inline static int32_t get_offset_of_growButtons_70() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___growButtons_70)); }
	inline bool get_growButtons_70() const { return ___growButtons_70; }
	inline bool* get_address_of_growButtons_70() { return &___growButtons_70; }
	inline void set_growButtons_70(bool value)
	{
		___growButtons_70 = value;
	}

	inline static int32_t get_offset_of_CourseScreen_71() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___CourseScreen_71)); }
	inline int32_t get_CourseScreen_71() const { return ___CourseScreen_71; }
	inline int32_t* get_address_of_CourseScreen_71() { return &___CourseScreen_71; }
	inline void set_CourseScreen_71(int32_t value)
	{
		___CourseScreen_71 = value;
	}

	inline static int32_t get_offset_of_currentJumpHitNumber_72() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___currentJumpHitNumber_72)); }
	inline int32_t get_currentJumpHitNumber_72() const { return ___currentJumpHitNumber_72; }
	inline int32_t* get_address_of_currentJumpHitNumber_72() { return &___currentJumpHitNumber_72; }
	inline void set_currentJumpHitNumber_72(int32_t value)
	{
		___currentJumpHitNumber_72 = value;
	}

	inline static int32_t get_offset_of_newJumpHit_73() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___newJumpHit_73)); }
	inline bool get_newJumpHit_73() const { return ___newJumpHit_73; }
	inline bool* get_address_of_newJumpHit_73() { return &___newJumpHit_73; }
	inline void set_newJumpHit_73(bool value)
	{
		___newJumpHit_73 = value;
	}

	inline static int32_t get_offset_of_currentJumpHit1_74() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___currentJumpHit1_74)); }
	inline int32_t get_currentJumpHit1_74() const { return ___currentJumpHit1_74; }
	inline int32_t* get_address_of_currentJumpHit1_74() { return &___currentJumpHit1_74; }
	inline void set_currentJumpHit1_74(int32_t value)
	{
		___currentJumpHit1_74 = value;
	}

	inline static int32_t get_offset_of_currentJumpHit2_75() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___currentJumpHit2_75)); }
	inline int32_t get_currentJumpHit2_75() const { return ___currentJumpHit2_75; }
	inline int32_t* get_address_of_currentJumpHit2_75() { return &___currentJumpHit2_75; }
	inline void set_currentJumpHit2_75(int32_t value)
	{
		___currentJumpHit2_75 = value;
	}

	inline static int32_t get_offset_of_canvas_76() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___canvas_76)); }
	inline Canvas_t209405766 * get_canvas_76() const { return ___canvas_76; }
	inline Canvas_t209405766 ** get_address_of_canvas_76() { return &___canvas_76; }
	inline void set_canvas_76(Canvas_t209405766 * value)
	{
		___canvas_76 = value;
		Il2CppCodeGenWriteBarrier(&___canvas_76, value);
	}

	inline static int32_t get_offset_of_mainCamera_77() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___mainCamera_77)); }
	inline Camera_t189460977 * get_mainCamera_77() const { return ___mainCamera_77; }
	inline Camera_t189460977 ** get_address_of_mainCamera_77() { return &___mainCamera_77; }
	inline void set_mainCamera_77(Camera_t189460977 * value)
	{
		___mainCamera_77 = value;
		Il2CppCodeGenWriteBarrier(&___mainCamera_77, value);
	}

	inline static int32_t get_offset_of_jumpEdit_78() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___jumpEdit_78)); }
	inline GameObject_t1756533147 * get_jumpEdit_78() const { return ___jumpEdit_78; }
	inline GameObject_t1756533147 ** get_address_of_jumpEdit_78() { return &___jumpEdit_78; }
	inline void set_jumpEdit_78(GameObject_t1756533147 * value)
	{
		___jumpEdit_78 = value;
		Il2CppCodeGenWriteBarrier(&___jumpEdit_78, value);
	}

	inline static int32_t get_offset_of_btn_jumpEdit_done_79() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_jumpEdit_done_79)); }
	inline Button_t2872111280 * get_btn_jumpEdit_done_79() const { return ___btn_jumpEdit_done_79; }
	inline Button_t2872111280 ** get_address_of_btn_jumpEdit_done_79() { return &___btn_jumpEdit_done_79; }
	inline void set_btn_jumpEdit_done_79(Button_t2872111280 * value)
	{
		___btn_jumpEdit_done_79 = value;
		Il2CppCodeGenWriteBarrier(&___btn_jumpEdit_done_79, value);
	}

	inline static int32_t get_offset_of_btn_jumpEdit_type_80() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___btn_jumpEdit_type_80)); }
	inline Button_t2872111280 * get_btn_jumpEdit_type_80() const { return ___btn_jumpEdit_type_80; }
	inline Button_t2872111280 ** get_address_of_btn_jumpEdit_type_80() { return &___btn_jumpEdit_type_80; }
	inline void set_btn_jumpEdit_type_80(Button_t2872111280 * value)
	{
		___btn_jumpEdit_type_80 = value;
		Il2CppCodeGenWriteBarrier(&___btn_jumpEdit_type_80, value);
	}

	inline static int32_t get_offset_of_jumpsList_81() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___jumpsList_81)); }
	inline List_1_t3778957944 * get_jumpsList_81() const { return ___jumpsList_81; }
	inline List_1_t3778957944 ** get_address_of_jumpsList_81() { return &___jumpsList_81; }
	inline void set_jumpsList_81(List_1_t3778957944 * value)
	{
		___jumpsList_81 = value;
		Il2CppCodeGenWriteBarrier(&___jumpsList_81, value);
	}

	inline static int32_t get_offset_of_jumpEditListeners_82() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___jumpEditListeners_82)); }
	inline bool get_jumpEditListeners_82() const { return ___jumpEditListeners_82; }
	inline bool* get_address_of_jumpEditListeners_82() { return &___jumpEditListeners_82; }
	inline void set_jumpEditListeners_82(bool value)
	{
		___jumpEditListeners_82 = value;
	}

	inline static int32_t get_offset_of_ShowStartTraining_83() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___ShowStartTraining_83)); }
	inline bool get_ShowStartTraining_83() const { return ___ShowStartTraining_83; }
	inline bool* get_address_of_ShowStartTraining_83() { return &___ShowStartTraining_83; }
	inline void set_ShowStartTraining_83(bool value)
	{
		___ShowStartTraining_83 = value;
	}

	inline static int32_t get_offset_of_viewport_84() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___viewport_84)); }
	inline GameObject_t1756533147 * get_viewport_84() const { return ___viewport_84; }
	inline GameObject_t1756533147 ** get_address_of_viewport_84() { return &___viewport_84; }
	inline void set_viewport_84(GameObject_t1756533147 * value)
	{
		___viewport_84 = value;
		Il2CppCodeGenWriteBarrier(&___viewport_84, value);
	}

	inline static int32_t get_offset_of_jumpsGlow_85() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___jumpsGlow_85)); }
	inline GameObject_t1756533147 * get_jumpsGlow_85() const { return ___jumpsGlow_85; }
	inline GameObject_t1756533147 ** get_address_of_jumpsGlow_85() { return &___jumpsGlow_85; }
	inline void set_jumpsGlow_85(GameObject_t1756533147 * value)
	{
		___jumpsGlow_85 = value;
		Il2CppCodeGenWriteBarrier(&___jumpsGlow_85, value);
	}

	inline static int32_t get_offset_of_footerButtons_86() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___footerButtons_86)); }
	inline GameObject_t1756533147 * get_footerButtons_86() const { return ___footerButtons_86; }
	inline GameObject_t1756533147 ** get_address_of_footerButtons_86() { return &___footerButtons_86; }
	inline void set_footerButtons_86(GameObject_t1756533147 * value)
	{
		___footerButtons_86 = value;
		Il2CppCodeGenWriteBarrier(&___footerButtons_86, value);
	}

	inline static int32_t get_offset_of_mouseButton0Down_pos_87() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___mouseButton0Down_pos_87)); }
	inline Vector3_t2243707580  get_mouseButton0Down_pos_87() const { return ___mouseButton0Down_pos_87; }
	inline Vector3_t2243707580 * get_address_of_mouseButton0Down_pos_87() { return &___mouseButton0Down_pos_87; }
	inline void set_mouseButton0Down_pos_87(Vector3_t2243707580  value)
	{
		___mouseButton0Down_pos_87 = value;
	}

	inline static int32_t get_offset_of_tapMoveEpsilon_88() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___tapMoveEpsilon_88)); }
	inline float get_tapMoveEpsilon_88() const { return ___tapMoveEpsilon_88; }
	inline float* get_address_of_tapMoveEpsilon_88() { return &___tapMoveEpsilon_88; }
	inline void set_tapMoveEpsilon_88(float value)
	{
		___tapMoveEpsilon_88 = value;
	}

	inline static int32_t get_offset_of_wasMultitouch_89() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___wasMultitouch_89)); }
	inline bool get_wasMultitouch_89() const { return ___wasMultitouch_89; }
	inline bool* get_address_of_wasMultitouch_89() { return &___wasMultitouch_89; }
	inline void set_wasMultitouch_89(bool value)
	{
		___wasMultitouch_89 = value;
	}

	inline static int32_t get_offset_of_mouseInCouseArea_90() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___mouseInCouseArea_90)); }
	inline bool get_mouseInCouseArea_90() const { return ___mouseInCouseArea_90; }
	inline bool* get_address_of_mouseInCouseArea_90() { return &___mouseInCouseArea_90; }
	inline void set_mouseInCouseArea_90(bool value)
	{
		___mouseInCouseArea_90 = value;
	}

	inline static int32_t get_offset_of_touch_t0_91() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___touch_t0_91)); }
	inline float get_touch_t0_91() const { return ___touch_t0_91; }
	inline float* get_address_of_touch_t0_91() { return &___touch_t0_91; }
	inline void set_touch_t0_91(float value)
	{
		___touch_t0_91 = value;
	}

	inline static int32_t get_offset_of_tapCancelled_92() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___tapCancelled_92)); }
	inline bool get_tapCancelled_92() const { return ___tapCancelled_92; }
	inline bool* get_address_of_tapCancelled_92() { return &___tapCancelled_92; }
	inline void set_tapCancelled_92(bool value)
	{
		___tapCancelled_92 = value;
	}

	inline static int32_t get_offset_of_newScreenPosition_93() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___newScreenPosition_93)); }
	inline Vector2_t2243707579  get_newScreenPosition_93() const { return ___newScreenPosition_93; }
	inline Vector2_t2243707579 * get_address_of_newScreenPosition_93() { return &___newScreenPosition_93; }
	inline void set_newScreenPosition_93(Vector2_t2243707579  value)
	{
		___newScreenPosition_93 = value;
	}

	inline static int32_t get_offset_of_prevScreenPosition_94() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___prevScreenPosition_94)); }
	inline Vector2_t2243707579  get_prevScreenPosition_94() const { return ___prevScreenPosition_94; }
	inline Vector2_t2243707579 * get_address_of_prevScreenPosition_94() { return &___prevScreenPosition_94; }
	inline void set_prevScreenPosition_94(Vector2_t2243707579  value)
	{
		___prevScreenPosition_94 = value;
	}

	inline static int32_t get_offset_of_lastActivityTime_95() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___lastActivityTime_95)); }
	inline float get_lastActivityTime_95() const { return ___lastActivityTime_95; }
	inline float* get_address_of_lastActivityTime_95() { return &___lastActivityTime_95; }
	inline void set_lastActivityTime_95(float value)
	{
		___lastActivityTime_95 = value;
	}

	inline static int32_t get_offset_of_prevNumPopups_96() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___prevNumPopups_96)); }
	inline int32_t get_prevNumPopups_96() const { return ___prevNumPopups_96; }
	inline int32_t* get_address_of_prevNumPopups_96() { return &___prevNumPopups_96; }
	inline void set_prevNumPopups_96(int32_t value)
	{
		___prevNumPopups_96 = value;
	}

	inline static int32_t get_offset_of_lastHit_97() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___lastHit_97)); }
	inline GameObject_t1756533147 * get_lastHit_97() const { return ___lastHit_97; }
	inline GameObject_t1756533147 ** get_address_of_lastHit_97() { return &___lastHit_97; }
	inline void set_lastHit_97(GameObject_t1756533147 * value)
	{
		___lastHit_97 = value;
		Il2CppCodeGenWriteBarrier(&___lastHit_97, value);
	}

	inline static int32_t get_offset_of_haveFirstCollider_98() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___haveFirstCollider_98)); }
	inline bool get_haveFirstCollider_98() const { return ___haveFirstCollider_98; }
	inline bool* get_address_of_haveFirstCollider_98() { return &___haveFirstCollider_98; }
	inline void set_haveFirstCollider_98(bool value)
	{
		___haveFirstCollider_98 = value;
	}

	inline static int32_t get_offset_of_trackFail_99() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___trackFail_99)); }
	inline bool get_trackFail_99() const { return ___trackFail_99; }
	inline bool* get_address_of_trackFail_99() { return &___trackFail_99; }
	inline void set_trackFail_99(bool value)
	{
		___trackFail_99 = value;
	}

	inline static int32_t get_offset_of_numPointsAdded_100() { return static_cast<int32_t>(offsetof(CourseController_t1581498483, ___numPointsAdded_100)); }
	inline int32_t get_numPointsAdded_100() const { return ___numPointsAdded_100; }
	inline int32_t* get_address_of_numPointsAdded_100() { return &___numPointsAdded_100; }
	inline void set_numPointsAdded_100(int32_t value)
	{
		___numPointsAdded_100 = value;
	}
};

struct CourseController_t1581498483_StaticFields
{
public:
	// System.Int32 CourseController::courseId
	int32_t ___courseId_35;

public:
	inline static int32_t get_offset_of_courseId_35() { return static_cast<int32_t>(offsetof(CourseController_t1581498483_StaticFields, ___courseId_35)); }
	inline int32_t get_courseId_35() const { return ___courseId_35; }
	inline int32_t* get_address_of_courseId_35() { return &___courseId_35; }
	inline void set_courseId_35(int32_t value)
	{
		___courseId_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
