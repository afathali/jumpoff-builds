﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.LateBoundMetadataTypeAttribute
struct LateBoundMetadataTypeAttribute_t712380905;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Newtonsoft.Json.Serialization.LateBoundMetadataTypeAttribute::.ctor(System.Object)
extern "C"  void LateBoundMetadataTypeAttribute__ctor_m1744475013 (LateBoundMetadataTypeAttribute_t712380905 * __this, Il2CppObject * ___attribute0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.LateBoundMetadataTypeAttribute::get_MetadataClassType()
extern "C"  Type_t * LateBoundMetadataTypeAttribute_get_MetadataClassType_m3567541669 (LateBoundMetadataTypeAttribute_t712380905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
