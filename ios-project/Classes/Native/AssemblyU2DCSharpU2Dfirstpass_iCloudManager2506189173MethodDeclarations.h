﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iCloudManager
struct iCloudManager_t2506189173;
// System.Action`1<SA.Common.Models.Result>
struct Action_1_t4089019125;
// System.Action`1<iCloudData>
struct Action_1_t2882436870;
// System.Action`1<System.Collections.Generic.List`1<iCloudData>>
struct Action_1_t2251558002;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// SA.Common.Models.Result
struct Result_t4287219743;
// iCloudData
struct iCloudData_t3080637488;
// System.Collections.Generic.List`1<iCloudData>
struct List_1_t2449758620;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iCloudData3080637488.h"

// System.Void iCloudManager::.ctor()
extern "C"  void iCloudManager__ctor_m941899152 (iCloudManager_t2506189173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::.cctor()
extern "C"  void iCloudManager__cctor_m1976487349 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::add_OnCloudInitAction(System.Action`1<SA.Common.Models.Result>)
extern "C"  void iCloudManager_add_OnCloudInitAction_m3960151484 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::remove_OnCloudInitAction(System.Action`1<SA.Common.Models.Result>)
extern "C"  void iCloudManager_remove_OnCloudInitAction_m1476587429 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::add_OnCloudDataReceivedAction(System.Action`1<iCloudData>)
extern "C"  void iCloudManager_add_OnCloudDataReceivedAction_m2337728703 (Il2CppObject * __this /* static, unused */, Action_1_t2882436870 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::remove_OnCloudDataReceivedAction(System.Action`1<iCloudData>)
extern "C"  void iCloudManager_remove_OnCloudDataReceivedAction_m988687246 (Il2CppObject * __this /* static, unused */, Action_1_t2882436870 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::add_OnStoreDidChangeExternally(System.Action`1<System.Collections.Generic.List`1<iCloudData>>)
extern "C"  void iCloudManager_add_OnStoreDidChangeExternally_m25095785 (Il2CppObject * __this /* static, unused */, Action_1_t2251558002 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::remove_OnStoreDidChangeExternally(System.Action`1<System.Collections.Generic.List`1<iCloudData>>)
extern "C"  void iCloudManager_remove_OnStoreDidChangeExternally_m1800100252 (Il2CppObject * __this /* static, unused */, Action_1_t2251558002 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::_initCloud()
extern "C"  void iCloudManager__initCloud_m2622109778 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::_setString(System.String,System.String)
extern "C"  void iCloudManager__setString_m1248557738 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::_setDouble(System.String,System.Single)
extern "C"  void iCloudManager__setDouble_m11672021 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::_setData(System.String,System.String)
extern "C"  void iCloudManager__setData_m1915524043 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::_requestDataForKey(System.String)
extern "C"  void iCloudManager__requestDataForKey_m3546880464 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::Awake()
extern "C"  void iCloudManager_Awake_m3540173751 (iCloudManager_t2506189173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::setString(System.String,System.String)
extern "C"  void iCloudManager_setString_m892730585 (iCloudManager_t2506189173 * __this, String_t* ___key0, String_t* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::setFloat(System.String,System.Single)
extern "C"  void iCloudManager_setFloat_m17614627 (iCloudManager_t2506189173 * __this, String_t* ___key0, float ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::setData(System.String,System.Byte[])
extern "C"  void iCloudManager_setData_m3086659745 (iCloudManager_t2506189173 * __this, String_t* ___key0, ByteU5BU5D_t3397334013* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::requestDataForKey(System.String)
extern "C"  void iCloudManager_requestDataForKey_m1824626745 (iCloudManager_t2506189173 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::OnCloudInit()
extern "C"  void iCloudManager_OnCloudInit_m143491988 (iCloudManager_t2506189173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::OnCloudInitFail()
extern "C"  void iCloudManager_OnCloudInitFail_m1081923420 (iCloudManager_t2506189173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::OnCloudDataChanged(System.String)
extern "C"  void iCloudManager_OnCloudDataChanged_m3013226154 (iCloudManager_t2506189173 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::OnCloudData(System.String)
extern "C"  void iCloudManager_OnCloudData_m4043087988 (iCloudManager_t2506189173 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::OnCloudDataEmpty(System.String)
extern "C"  void iCloudManager_OnCloudDataEmpty_m945421189 (iCloudManager_t2506189173 * __this, String_t* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::<OnCloudInitAction>m__6C(SA.Common.Models.Result)
extern "C"  void iCloudManager_U3COnCloudInitActionU3Em__6C_m22051690 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::<OnCloudDataReceivedAction>m__6D(iCloudData)
extern "C"  void iCloudManager_U3COnCloudDataReceivedActionU3Em__6D_m728537240 (Il2CppObject * __this /* static, unused */, iCloudData_t3080637488 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iCloudManager::<OnStoreDidChangeExternally>m__6E(System.Collections.Generic.List`1<iCloudData>)
extern "C"  void iCloudManager_U3COnStoreDidChangeExternallyU3Em__6E_m2563625969 (Il2CppObject * __this /* static, unused */, List_1_t2449758620 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
