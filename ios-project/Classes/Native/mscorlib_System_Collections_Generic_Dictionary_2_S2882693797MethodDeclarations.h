﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>
struct ShimEnumerator_t2882693797;
// System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>
struct Dictionary_2_t2777568976;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3091092150_gshared (ShimEnumerator_t2882693797 * __this, Dictionary_2_t2777568976 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m3091092150(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2882693797 *, Dictionary_2_t2777568976 *, const MethodInfo*))ShimEnumerator__ctor_m3091092150_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3382097401_gshared (ShimEnumerator_t2882693797 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3382097401(__this, method) ((  bool (*) (ShimEnumerator_t2882693797 *, const MethodInfo*))ShimEnumerator_MoveNext_m3382097401_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2722709869_gshared (ShimEnumerator_t2882693797 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2722709869(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2882693797 *, const MethodInfo*))ShimEnumerator_get_Entry_m2722709869_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1988482512_gshared (ShimEnumerator_t2882693797 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1988482512(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2882693797 *, const MethodInfo*))ShimEnumerator_get_Key_m1988482512_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2201824138_gshared (ShimEnumerator_t2882693797 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2201824138(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2882693797 *, const MethodInfo*))ShimEnumerator_get_Value_m2201824138_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m56500262_gshared (ShimEnumerator_t2882693797 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m56500262(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2882693797 *, const MethodInfo*))ShimEnumerator_get_Current_m56500262_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<FB_ProfileImageSize,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3295896912_gshared (ShimEnumerator_t2882693797 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3295896912(__this, method) ((  void (*) (ShimEnumerator_t2882693797 *, const MethodInfo*))ShimEnumerator_Reset_m3295896912_gshared)(__this, method)
