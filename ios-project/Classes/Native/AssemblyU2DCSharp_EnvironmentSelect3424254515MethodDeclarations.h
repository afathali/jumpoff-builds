﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnvironmentSelect
struct EnvironmentSelect_t3424254515;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"

// System.Void EnvironmentSelect::.ctor()
extern "C"  void EnvironmentSelect__ctor_m3749883200 (EnvironmentSelect_t3424254515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnvironmentSelect::UpdateLockIcons()
extern "C"  void EnvironmentSelect_UpdateLockIcons_m2287360284 (EnvironmentSelect_t3424254515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnvironmentSelect::DisableSelectedStatus()
extern "C"  void EnvironmentSelect_DisableSelectedStatus_m1078056839 (EnvironmentSelect_t3424254515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnvironmentSelect::env1_click()
extern "C"  void EnvironmentSelect_env1_click_m3237594431 (EnvironmentSelect_t3424254515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnvironmentSelect::env2_click()
extern "C"  void EnvironmentSelect_env2_click_m3238919614 (EnvironmentSelect_t3424254515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnvironmentSelect::env3_click()
extern "C"  void EnvironmentSelect_env3_click_m3239961917 (EnvironmentSelect_t3424254515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnvironmentSelect::Start()
extern "C"  void EnvironmentSelect_Start_m1886248100 (EnvironmentSelect_t3424254515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnvironmentSelect::<Start>m__C()
extern "C"  void EnvironmentSelect_U3CStartU3Em__C_m1322176680 (EnvironmentSelect_t3424254515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnvironmentSelect::<Start>m__D(UnityEngine.EventSystems.BaseEventData)
extern "C"  void EnvironmentSelect_U3CStartU3Em__D_m2573146731 (EnvironmentSelect_t3424254515 * __this, BaseEventData_t2681005625 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnvironmentSelect::<Start>m__E(UnityEngine.EventSystems.BaseEventData)
extern "C"  void EnvironmentSelect_U3CStartU3Em__E_m4269902406 (EnvironmentSelect_t3424254515 * __this, BaseEventData_t2681005625 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnvironmentSelect::<Start>m__F(UnityEngine.EventSystems.BaseEventData)
extern "C"  void EnvironmentSelect_U3CStartU3Em__F_m3991832757 (EnvironmentSelect_t3424254515 * __this, BaseEventData_t2681005625 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
