﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SP_FB_API_v7
struct  SP_FB_API_v7_t4234733891  : public Il2CppObject
{
public:
	// System.String SP_FB_API_v7::_UserId
	String_t* ____UserId_0;
	// System.String SP_FB_API_v7::_AccessToken
	String_t* ____AccessToken_1;

public:
	inline static int32_t get_offset_of__UserId_0() { return static_cast<int32_t>(offsetof(SP_FB_API_v7_t4234733891, ____UserId_0)); }
	inline String_t* get__UserId_0() const { return ____UserId_0; }
	inline String_t** get_address_of__UserId_0() { return &____UserId_0; }
	inline void set__UserId_0(String_t* value)
	{
		____UserId_0 = value;
		Il2CppCodeGenWriteBarrier(&____UserId_0, value);
	}

	inline static int32_t get_offset_of__AccessToken_1() { return static_cast<int32_t>(offsetof(SP_FB_API_v7_t4234733891, ____AccessToken_1)); }
	inline String_t* get__AccessToken_1() const { return ____AccessToken_1; }
	inline String_t** get_address_of__AccessToken_1() { return &____AccessToken_1; }
	inline void set__AccessToken_1(String_t* value)
	{
		____AccessToken_1 = value;
		Il2CppCodeGenWriteBarrier(&____AccessToken_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
