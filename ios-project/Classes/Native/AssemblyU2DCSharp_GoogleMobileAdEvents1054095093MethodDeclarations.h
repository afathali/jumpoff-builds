﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoogleMobileAdEvents
struct GoogleMobileAdEvents_t1054095093;

#include "codegen/il2cpp-codegen.h"

// System.Void GoogleMobileAdEvents::.ctor()
extern "C"  void GoogleMobileAdEvents__ctor_m3170396468 (GoogleMobileAdEvents_t1054095093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
