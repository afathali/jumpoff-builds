﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TW_UsersLookUpRequest
struct TW_UsersLookUpRequest_t889831987;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TW_UsersLookUpRequest::.ctor()
extern "C"  void TW_UsersLookUpRequest__ctor_m2358490392 (TW_UsersLookUpRequest_t889831987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TW_UsersLookUpRequest TW_UsersLookUpRequest::Create()
extern "C"  TW_UsersLookUpRequest_t889831987 * TW_UsersLookUpRequest_Create_m417615120 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_UsersLookUpRequest::Awake()
extern "C"  void TW_UsersLookUpRequest_Awake_m863179023 (TW_UsersLookUpRequest_t889831987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_UsersLookUpRequest::OnResult(System.String)
extern "C"  void TW_UsersLookUpRequest_OnResult_m3188869084 (TW_UsersLookUpRequest_t889831987 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
