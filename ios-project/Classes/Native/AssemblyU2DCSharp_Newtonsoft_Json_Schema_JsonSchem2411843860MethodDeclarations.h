﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaGenerator
struct JsonSchemaGenerator_t2411843860;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t614887283;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t3772113849;
// Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema
struct TypeSchema_t460462092;
// System.Type
struct Type_t;
// Newtonsoft.Json.Schema.JsonSchemaResolver
struct JsonSchemaResolver_t3305548243;
// System.String
struct String_t;
// Newtonsoft.Json.Serialization.JsonObjectContract
struct JsonObjectContract_t2091736265;
// Newtonsoft.Json.Serialization.JsonISerializableContract
struct JsonISerializableContract_t122701872;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_Undefined4037327541.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460462092.h"
#include "mscorlib_System_Type1303803226.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3305548243.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Required2961887721.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_DefaultValueHand3457895463.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2091736265.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso122701872.h"
#include "mscorlib_System_Nullable_1_gen5811492.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::.ctor()
extern "C"  void JsonSchemaGenerator__ctor_m3992021037 (JsonSchemaGenerator_t2411843860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.UndefinedSchemaIdHandling Newtonsoft.Json.Schema.JsonSchemaGenerator::get_UndefinedSchemaIdHandling()
extern "C"  int32_t JsonSchemaGenerator_get_UndefinedSchemaIdHandling_m3161939855 (JsonSchemaGenerator_t2411843860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::set_UndefinedSchemaIdHandling(Newtonsoft.Json.Schema.UndefinedSchemaIdHandling)
extern "C"  void JsonSchemaGenerator_set_UndefinedSchemaIdHandling_m2348622692 (JsonSchemaGenerator_t2411843860 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Schema.JsonSchemaGenerator::get_ContractResolver()
extern "C"  Il2CppObject * JsonSchemaGenerator_get_ContractResolver_m2405880033 (JsonSchemaGenerator_t2411843860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern "C"  void JsonSchemaGenerator_set_ContractResolver_m2698350304 (JsonSchemaGenerator_t2411843860 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::get_CurrentSchema()
extern "C"  JsonSchema_t3772113849 * JsonSchemaGenerator_get_CurrentSchema_m3819898454 (JsonSchemaGenerator_t2411843860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::Push(Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema)
extern "C"  void JsonSchemaGenerator_Push_m2281646275 (JsonSchemaGenerator_t2411843860 * __this, TypeSchema_t460462092 * ___typeSchema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::Pop()
extern "C"  TypeSchema_t460462092 * JsonSchemaGenerator_Pop_m2733719685 (JsonSchemaGenerator_t2411843860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::Generate(System.Type)
extern "C"  JsonSchema_t3772113849 * JsonSchemaGenerator_Generate_m3609238343 (JsonSchemaGenerator_t2411843860 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::Generate(System.Type,Newtonsoft.Json.Schema.JsonSchemaResolver)
extern "C"  JsonSchema_t3772113849 * JsonSchemaGenerator_Generate_m718250660 (JsonSchemaGenerator_t2411843860 * __this, Type_t * ___type0, JsonSchemaResolver_t3305548243 * ___resolver1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::Generate(System.Type,System.Boolean)
extern "C"  JsonSchema_t3772113849 * JsonSchemaGenerator_Generate_m31496836 (JsonSchemaGenerator_t2411843860 * __this, Type_t * ___type0, bool ___rootSchemaNullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::Generate(System.Type,Newtonsoft.Json.Schema.JsonSchemaResolver,System.Boolean)
extern "C"  JsonSchema_t3772113849 * JsonSchemaGenerator_Generate_m1774789255 (JsonSchemaGenerator_t2411843860 * __this, Type_t * ___type0, JsonSchemaResolver_t3305548243 * ___resolver1, bool ___rootSchemaNullable2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaGenerator::GetTitle(System.Type)
extern "C"  String_t* JsonSchemaGenerator_GetTitle_m389188157 (JsonSchemaGenerator_t2411843860 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaGenerator::GetDescription(System.Type)
extern "C"  String_t* JsonSchemaGenerator_GetDescription_m3175331679 (JsonSchemaGenerator_t2411843860 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaGenerator::GetTypeId(System.Type,System.Boolean)
extern "C"  String_t* JsonSchemaGenerator_GetTypeId_m2305514287 (JsonSchemaGenerator_t2411843860 * __this, Type_t * ___type0, bool ___explicitOnly1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::GenerateInternal(System.Type,Newtonsoft.Json.Required,System.Boolean)
extern "C"  JsonSchema_t3772113849 * JsonSchemaGenerator_GenerateInternal_m3777343713 (JsonSchemaGenerator_t2411843860 * __this, Type_t * ___type0, int32_t ___valueRequired1, bool ___required2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.Schema.JsonSchemaGenerator::AddNullType(Newtonsoft.Json.Schema.JsonSchemaType,Newtonsoft.Json.Required)
extern "C"  int32_t JsonSchemaGenerator_AddNullType_m318152764 (JsonSchemaGenerator_t2411843860 * __this, int32_t ___type0, int32_t ___valueRequired1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaGenerator::HasFlag(Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern "C"  bool JsonSchemaGenerator_HasFlag_m1823475923 (JsonSchemaGenerator_t2411843860 * __this, int32_t ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::GenerateObjectSchema(System.Type,Newtonsoft.Json.Serialization.JsonObjectContract)
extern "C"  void JsonSchemaGenerator_GenerateObjectSchema_m1652102445 (JsonSchemaGenerator_t2411843860 * __this, Type_t * ___type0, JsonObjectContract_t2091736265 * ___contract1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::GenerateISerializableContract(System.Type,Newtonsoft.Json.Serialization.JsonISerializableContract)
extern "C"  void JsonSchemaGenerator_GenerateISerializableContract_m1479881424 (JsonSchemaGenerator_t2411843860 * __this, Type_t * ___type0, JsonISerializableContract_t122701872 * ___contract1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaGenerator::HasFlag(System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>,Newtonsoft.Json.Schema.JsonSchemaType)
extern "C"  bool JsonSchemaGenerator_HasFlag_m8899410 (Il2CppObject * __this /* static, unused */, Nullable_1_t5811492  ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.Schema.JsonSchemaGenerator::GetJsonSchemaType(System.Type,Newtonsoft.Json.Required)
extern "C"  int32_t JsonSchemaGenerator_GetJsonSchemaType_m579796817 (JsonSchemaGenerator_t2411843860 * __this, Type_t * ___type0, int32_t ___valueRequired1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
