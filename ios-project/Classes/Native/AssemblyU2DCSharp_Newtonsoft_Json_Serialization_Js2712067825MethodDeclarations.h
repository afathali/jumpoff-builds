﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t2712067825;
// System.String
struct String_t;
// Newtonsoft.Json.Serialization.IValueProvider
struct IValueProvider_t561708391;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t1964060750;
// System.Object
struct Il2CppObject;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2572051853;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Type1303803226.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter1964060750.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Required2961887721.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Nullable_1_gen1881161680.h"
#include "mscorlib_System_Nullable_1_gen1720961778.h"
#include "mscorlib_System_Nullable_1_gen3575889505.h"
#include "mscorlib_System_Nullable_1_gen1983200966.h"
#include "mscorlib_System_Nullable_1_gen3889546705.h"

// System.Void Newtonsoft.Json.Serialization.JsonProperty::.ctor()
extern "C"  void JsonProperty__ctor_m3264527515 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonProperty::get_PropertyName()
extern "C"  String_t* JsonProperty_get_PropertyName_m748649143 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyName(System.String)
extern "C"  void JsonProperty_set_PropertyName_m1375835106 (JsonProperty_t2712067825 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.JsonProperty::get_Order()
extern "C"  Nullable_1_t334943763  JsonProperty_get_Order_m3779327991 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Order(System.Nullable`1<System.Int32>)
extern "C"  void JsonProperty_set_Order_m3913656782 (JsonProperty_t2712067825 * __this, Nullable_1_t334943763  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonProperty::get_UnderlyingName()
extern "C"  String_t* JsonProperty_get_UnderlyingName_m773687201 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_UnderlyingName(System.String)
extern "C"  void JsonProperty_set_UnderlyingName_m840089768 (JsonProperty_t2712067825 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.JsonProperty::get_ValueProvider()
extern "C"  Il2CppObject * JsonProperty_get_ValueProvider_m1188682867 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ValueProvider(Newtonsoft.Json.Serialization.IValueProvider)
extern "C"  void JsonProperty_set_ValueProvider_m915515270 (JsonProperty_t2712067825 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonProperty::get_PropertyType()
extern "C"  Type_t * JsonProperty_get_PropertyType_m518536153 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyType(System.Type)
extern "C"  void JsonProperty_set_PropertyType_m3207683120 (JsonProperty_t2712067825 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_Converter()
extern "C"  JsonConverter_t1964060750 * JsonProperty_get_Converter_m1003716890 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Converter(Newtonsoft.Json.JsonConverter)
extern "C"  void JsonProperty_set_Converter_m4147006833 (JsonProperty_t2712067825 * __this, JsonConverter_t1964060750 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_MemberConverter()
extern "C"  JsonConverter_t1964060750 * JsonProperty_get_MemberConverter_m1667581928 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_MemberConverter(Newtonsoft.Json.JsonConverter)
extern "C"  void JsonProperty_set_MemberConverter_m1824531021 (JsonProperty_t2712067825 * __this, JsonConverter_t1964060750 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Ignored()
extern "C"  bool JsonProperty_get_Ignored_m3877332660 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Ignored(System.Boolean)
extern "C"  void JsonProperty_set_Ignored_m3791547261 (JsonProperty_t2712067825 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Readable()
extern "C"  bool JsonProperty_get_Readable_m1264567048 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Readable(System.Boolean)
extern "C"  void JsonProperty_set_Readable_m883911053 (JsonProperty_t2712067825 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Writable()
extern "C"  bool JsonProperty_get_Writable_m1483999968 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Writable(System.Boolean)
extern "C"  void JsonProperty_set_Writable_m2989551875 (JsonProperty_t2712067825 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonProperty::get_DefaultValue()
extern "C"  Il2CppObject * JsonProperty_get_DefaultValue_m2254088683 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DefaultValue(System.Object)
extern "C"  void JsonProperty_set_DefaultValue_m2771548496 (JsonProperty_t2712067825 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Required Newtonsoft.Json.Serialization.JsonProperty::get_Required()
extern "C"  int32_t JsonProperty_get_Required_m3965000432 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Required(Newtonsoft.Json.Required)
extern "C"  void JsonProperty_set_Required_m2705759593 (JsonProperty_t2712067825 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::get_IsReference()
extern "C"  Nullable_1_t2088641033  JsonProperty_get_IsReference_m459954082 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_IsReference(System.Nullable`1<System.Boolean>)
extern "C"  void JsonProperty_set_IsReference_m1515866893 (JsonProperty_t2712067825 * __this, Nullable_1_t2088641033  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.JsonProperty::get_NullValueHandling()
extern "C"  Nullable_1_t1881161680  JsonProperty_get_NullValueHandling_m2390058495 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_NullValueHandling(System.Nullable`1<Newtonsoft.Json.NullValueHandling>)
extern "C"  void JsonProperty_set_NullValueHandling_m106663718 (JsonProperty_t2712067825 * __this, Nullable_1_t1881161680  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.Serialization.JsonProperty::get_DefaultValueHandling()
extern "C"  Nullable_1_t1720961778  JsonProperty_get_DefaultValueHandling_m3292009679 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DefaultValueHandling(System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>)
extern "C"  void JsonProperty_set_DefaultValueHandling_m3289556020 (JsonProperty_t2712067825 * __this, Nullable_1_t1720961778  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ReferenceLoopHandling()
extern "C"  Nullable_1_t3575889505  JsonProperty_get_ReferenceLoopHandling_m1145823019 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern "C"  void JsonProperty_set_ReferenceLoopHandling_m1796436262 (JsonProperty_t2712067825 * __this, Nullable_1_t3575889505  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ObjectCreationHandling()
extern "C"  Nullable_1_t1983200966  JsonProperty_get_ObjectCreationHandling_m3241248271 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ObjectCreationHandling(System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>)
extern "C"  void JsonProperty_set_ObjectCreationHandling_m4228737068 (JsonProperty_t2712067825 * __this, Nullable_1_t1983200966  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::get_TypeNameHandling()
extern "C"  Nullable_1_t3889546705  JsonProperty_get_TypeNameHandling_m1862662511 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_TypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern "C"  void JsonProperty_set_TypeNameHandling_m136364166 (JsonProperty_t2712067825 * __this, Nullable_1_t3889546705  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_ShouldSerialize()
extern "C"  Predicate_1_t1132419410 * JsonProperty_get_ShouldSerialize_m2979216321 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ShouldSerialize(System.Predicate`1<System.Object>)
extern "C"  void JsonProperty_set_ShouldSerialize_m1281227982 (JsonProperty_t2712067825 * __this, Predicate_1_t1132419410 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_GetIsSpecified()
extern "C"  Predicate_1_t1132419410 * JsonProperty_get_GetIsSpecified_m2893355024 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_GetIsSpecified(System.Predicate`1<System.Object>)
extern "C"  void JsonProperty_set_GetIsSpecified_m1756747723 (JsonProperty_t2712067825 * __this, Predicate_1_t1132419410 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_SetIsSpecified()
extern "C"  Action_2_t2572051853 * JsonProperty_get_SetIsSpecified_m1542109436 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_SetIsSpecified(System.Action`2<System.Object,System.Object>)
extern "C"  void JsonProperty_set_SetIsSpecified_m2270297217 (JsonProperty_t2712067825 * __this, Action_2_t2572051853 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonProperty::ToString()
extern "C"  String_t* JsonProperty_ToString_m168426226 (JsonProperty_t2712067825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
