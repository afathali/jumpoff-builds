﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Schema.SchemaExtensions/<IsValid>c__AnonStorey20
struct  U3CIsValidU3Ec__AnonStorey20_t1322857965  : public Il2CppObject
{
public:
	// System.Boolean Newtonsoft.Json.Schema.SchemaExtensions/<IsValid>c__AnonStorey20::valid
	bool ___valid_0;

public:
	inline static int32_t get_offset_of_valid_0() { return static_cast<int32_t>(offsetof(U3CIsValidU3Ec__AnonStorey20_t1322857965, ___valid_0)); }
	inline bool get_valid_0() const { return ___valid_0; }
	inline bool* get_address_of_valid_0() { return &___valid_0; }
	inline void set_valid_0(bool value)
	{
		___valid_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
