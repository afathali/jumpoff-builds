﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen3708177276MethodDeclarations.h"

// System.Void System.Action`2<System.Collections.IList,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m3294740310(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t1205666592 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m2125582079_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.Collections.IList,System.Boolean>::Invoke(T1,T2)
#define Action_2_Invoke_m1247498483(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1205666592 *, Il2CppObject *, bool, const MethodInfo*))Action_2_Invoke_m4016543000_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.Collections.IList,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m2487029606(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t1205666592 *, Il2CppObject *, bool, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m963341251_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.Collections.IList,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m1848775546(__this, ___result0, method) ((  void (*) (Action_2_t1205666592 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3658961145_gshared)(__this, ___result0, method)
