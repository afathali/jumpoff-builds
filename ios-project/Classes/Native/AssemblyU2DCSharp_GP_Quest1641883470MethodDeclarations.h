﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_Quest
struct GP_Quest_t1641883470;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void GP_Quest::.ctor()
extern "C"  void GP_Quest__ctor_m3127225861 (GP_Quest_t1641883470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Quest::LoadIcon()
extern "C"  void GP_Quest_LoadIcon_m742722596 (GP_Quest_t1641883470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Quest::LoadBanner()
extern "C"  void GP_Quest_LoadBanner_m1035907575 (GP_Quest_t1641883470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GP_Quest::get_icon()
extern "C"  Texture2D_t3542995729 * GP_Quest_get_icon_m544110444 (GP_Quest_t1641883470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GP_Quest::get_banner()
extern "C"  Texture2D_t3542995729 * GP_Quest_get_banner_m3245427837 (GP_Quest_t1641883470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Quest::OnBannerLoaded(UnityEngine.Texture2D)
extern "C"  void GP_Quest_OnBannerLoaded_m2088246837 (GP_Quest_t1641883470 * __this, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_Quest::OnIconLoaded(UnityEngine.Texture2D)
extern "C"  void GP_Quest_OnIconLoaded_m49640302 (GP_Quest_t1641883470 * __this, Texture2D_t3542995729 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
