﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoogleCloudMessageService
struct GoogleCloudMessageService_t984991750;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<GP_GCM_RegistrationResult>
struct Action_1_t2694291500;
// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct Action_2_t2514582953;
// System.Action`3<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean>
struct Action_3_t1218892226;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// GP_GCM_RegistrationResult
struct GP_GCM_RegistrationResult_t2892492118;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_GCM_RegistrationResult2892492118.h"

// System.Void GoogleCloudMessageService::.ctor()
extern "C"  void GoogleCloudMessageService__ctor_m1653129197 (GoogleCloudMessageService_t984991750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::.cctor()
extern "C"  void GoogleCloudMessageService__cctor_m183470216 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::add_ActionCouldMessageLoaded(System.Action`1<System.String>)
extern "C"  void GoogleCloudMessageService_add_ActionCouldMessageLoaded_m3826093920 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::remove_ActionCouldMessageLoaded(System.Action`1<System.String>)
extern "C"  void GoogleCloudMessageService_remove_ActionCouldMessageLoaded_m1523775843 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::add_ActionCMDRegistrationResult(System.Action`1<GP_GCM_RegistrationResult>)
extern "C"  void GoogleCloudMessageService_add_ActionCMDRegistrationResult_m998616847 (Il2CppObject * __this /* static, unused */, Action_1_t2694291500 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::remove_ActionCMDRegistrationResult(System.Action`1<GP_GCM_RegistrationResult>)
extern "C"  void GoogleCloudMessageService_remove_ActionCMDRegistrationResult_m3993129860 (Il2CppObject * __this /* static, unused */, Action_1_t2694291500 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::add_ActionGCMPushReceived(System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern "C"  void GoogleCloudMessageService_add_ActionGCMPushReceived_m3021835689 (Il2CppObject * __this /* static, unused */, Action_2_t2514582953 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::remove_ActionGCMPushReceived(System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern "C"  void GoogleCloudMessageService_remove_ActionGCMPushReceived_m2057695596 (Il2CppObject * __this /* static, unused */, Action_2_t2514582953 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::add_ActionGameThriveNotificationReceived(System.Action`3<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean>)
extern "C"  void GoogleCloudMessageService_add_ActionGameThriveNotificationReceived_m244054971 (Il2CppObject * __this /* static, unused */, Action_3_t1218892226 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::remove_ActionGameThriveNotificationReceived(System.Action`3<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean>)
extern "C"  void GoogleCloudMessageService_remove_ActionGameThriveNotificationReceived_m555782988 (Il2CppObject * __this /* static, unused */, Action_3_t1218892226 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::add_ActionParsePushReceived(System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern "C"  void GoogleCloudMessageService_add_ActionParsePushReceived_m3921343361 (Il2CppObject * __this /* static, unused */, Action_2_t2514582953 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::remove_ActionParsePushReceived(System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern "C"  void GoogleCloudMessageService_remove_ActionParsePushReceived_m1043635114 (Il2CppObject * __this /* static, unused */, Action_2_t2514582953 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::Awake()
extern "C"  void GoogleCloudMessageService_Awake_m2157797174 (GoogleCloudMessageService_t984991750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::Init()
extern "C"  void GoogleCloudMessageService_Init_m781479043 (GoogleCloudMessageService_t984991750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::InitOneSignalNotifications()
extern "C"  void GoogleCloudMessageService_InitOneSignalNotifications_m3267969975 (GoogleCloudMessageService_t984991750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::HandleNotification(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean)
extern "C"  void GoogleCloudMessageService_HandleNotification_m1823577966 (Il2CppObject * __this /* static, unused */, String_t* ___message0, Dictionary_2_t309261261 * ___additionalData1, bool ___isActive2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::InitPushNotifications()
extern "C"  void GoogleCloudMessageService_InitPushNotifications_m1322751535 (GoogleCloudMessageService_t984991750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::InitPushNotifications(System.String,System.String,System.String,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void GoogleCloudMessageService_InitPushNotifications_m4020388624 (GoogleCloudMessageService_t984991750 * __this, String_t* ___smallIcon0, String_t* ___largeIcon1, String_t* ___sound2, bool ___enableVibrationPush3, bool ___showWhenAppForeground4, bool ___replaceOldNotificationWithNew5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::InitParsePushNotifications()
extern "C"  void GoogleCloudMessageService_InitParsePushNotifications_m2055158602 (GoogleCloudMessageService_t984991750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::RgisterDevice()
extern "C"  void GoogleCloudMessageService_RgisterDevice_m239973573 (GoogleCloudMessageService_t984991750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::LoadLastMessage()
extern "C"  void GoogleCloudMessageService_LoadLastMessage_m3139505002 (GoogleCloudMessageService_t984991750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::RemoveLastMessageInfo()
extern "C"  void GoogleCloudMessageService_RemoveLastMessageInfo_m2005043542 (GoogleCloudMessageService_t984991750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::HandleOnPushReceived(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void GoogleCloudMessageService_HandleOnPushReceived_m4237474494 (GoogleCloudMessageService_t984991750 * __this, String_t* ___stringPayload0, Dictionary_2_t309261261 * ___payload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::GCMNotificationCallback(System.String)
extern "C"  void GoogleCloudMessageService_GCMNotificationCallback_m3624246470 (GoogleCloudMessageService_t984991750 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleCloudMessageService::get_registrationId()
extern "C"  String_t* GoogleCloudMessageService_get_registrationId_m1345963845 (GoogleCloudMessageService_t984991750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleCloudMessageService::get_lastMessage()
extern "C"  String_t* GoogleCloudMessageService_get_lastMessage_m4109919540 (GoogleCloudMessageService_t984991750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::OnLastMessageLoaded(System.String)
extern "C"  void GoogleCloudMessageService_OnLastMessageLoaded_m3641207430 (GoogleCloudMessageService_t984991750 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::OnRegistrationReviced(System.String)
extern "C"  void GoogleCloudMessageService_OnRegistrationReviced_m1796947589 (GoogleCloudMessageService_t984991750 * __this, String_t* ___regId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::OnRegistrationFailed()
extern "C"  void GoogleCloudMessageService_OnRegistrationFailed_m3791992852 (GoogleCloudMessageService_t984991750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::<ActionCouldMessageLoaded>m__32(System.String)
extern "C"  void GoogleCloudMessageService_U3CActionCouldMessageLoadedU3Em__32_m3256233374 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::<ActionCMDRegistrationResult>m__33(GP_GCM_RegistrationResult)
extern "C"  void GoogleCloudMessageService_U3CActionCMDRegistrationResultU3Em__33_m1128481498 (Il2CppObject * __this /* static, unused */, GP_GCM_RegistrationResult_t2892492118 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::<ActionGCMPushReceived>m__34(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void GoogleCloudMessageService_U3CActionGCMPushReceivedU3Em__34_m2660415330 (Il2CppObject * __this /* static, unused */, String_t* p0, Dictionary_2_t309261261 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::<ActionGameThriveNotificationReceived>m__35(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean)
extern "C"  void GoogleCloudMessageService_U3CActionGameThriveNotificationReceivedU3Em__35_m3696297280 (Il2CppObject * __this /* static, unused */, String_t* p0, Dictionary_2_t309261261 * p1, bool p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudMessageService::<ActionParsePushReceived>m__36(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void GoogleCloudMessageService_U3CActionParsePushReceivedU3Em__36_m1131393802 (Il2CppObject * __this /* static, unused */, String_t* p0, Dictionary_2_t309261261 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
