﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SA.IOSDeploy.Variable>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3914650597(__this, ___l0, method) ((  void (*) (Enumerator_t61615852 *, List_1_t526886178 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SA.IOSDeploy.Variable>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m176369589(__this, method) ((  void (*) (Enumerator_t61615852 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SA.IOSDeploy.Variable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1828663637(__this, method) ((  Il2CppObject * (*) (Enumerator_t61615852 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SA.IOSDeploy.Variable>::Dispose()
#define Enumerator_Dispose_m996752434(__this, method) ((  void (*) (Enumerator_t61615852 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SA.IOSDeploy.Variable>::VerifyState()
#define Enumerator_VerifyState_m313061555(__this, method) ((  void (*) (Enumerator_t61615852 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SA.IOSDeploy.Variable>::MoveNext()
#define Enumerator_MoveNext_m3727498356(__this, method) ((  bool (*) (Enumerator_t61615852 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SA.IOSDeploy.Variable>::get_Current()
#define Enumerator_get_Current_m1560118366(__this, method) ((  Variable_t1157765046 * (*) (Enumerator_t61615852 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
