﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidGoogleAnalytics
struct AndroidGoogleAnalytics_t1297670224;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GPLogLevel1212391055.h"

// System.Void AndroidGoogleAnalytics::.ctor()
extern "C"  void AndroidGoogleAnalytics__ctor_m2872075735 (AndroidGoogleAnalytics_t1297670224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::Awake()
extern "C"  void AndroidGoogleAnalytics_Awake_m2245441372 (AndroidGoogleAnalytics_t1297670224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::StartTracking()
extern "C"  void AndroidGoogleAnalytics_StartTracking_m862358822 (AndroidGoogleAnalytics_t1297670224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::SetTrackerID(System.String)
extern "C"  void AndroidGoogleAnalytics_SetTrackerID_m1009675052 (AndroidGoogleAnalytics_t1297670224 * __this, String_t* ___trackingID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::SendView(System.String)
extern "C"  void AndroidGoogleAnalytics_SendView_m1742446566 (AndroidGoogleAnalytics_t1297670224 * __this, String_t* ___appScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::SendView()
extern "C"  void AndroidGoogleAnalytics_SendView_m89998748 (AndroidGoogleAnalytics_t1297670224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::SendEvent(System.String,System.String,System.String)
extern "C"  void AndroidGoogleAnalytics_SendEvent_m532038817 (AndroidGoogleAnalytics_t1297670224 * __this, String_t* ___category0, String_t* ___action1, String_t* ___label2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::SendEvent(System.String,System.String,System.String,System.Int64)
extern "C"  void AndroidGoogleAnalytics_SendEvent_m2674926175 (AndroidGoogleAnalytics_t1297670224 * __this, String_t* ___category0, String_t* ___action1, String_t* ___label2, int64_t ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::SendEvent(System.String,System.String,System.String,System.String,System.String)
extern "C"  void AndroidGoogleAnalytics_SendEvent_m1351654881 (AndroidGoogleAnalytics_t1297670224 * __this, String_t* ___category0, String_t* ___action1, String_t* ___label2, String_t* ___key3, String_t* ___val4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::SendEvent(System.String,System.String,System.String,System.Int64,System.String,System.String)
extern "C"  void AndroidGoogleAnalytics_SendEvent_m1375291423 (AndroidGoogleAnalytics_t1297670224 * __this, String_t* ___category0, String_t* ___action1, String_t* ___label2, int64_t ___value3, String_t* ___key4, String_t* ___val5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::SendTiming(System.String,System.Int64)
extern "C"  void AndroidGoogleAnalytics_SendTiming_m947950595 (AndroidGoogleAnalytics_t1297670224 * __this, String_t* ___category0, int64_t ___intervalInMilliseconds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::SendTiming(System.String,System.Int64,System.String)
extern "C"  void AndroidGoogleAnalytics_SendTiming_m3332298669 (AndroidGoogleAnalytics_t1297670224 * __this, String_t* ___category0, int64_t ___intervalInMilliseconds1, String_t* ___name2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::SendTiming(System.String,System.Int64,System.String,System.String)
extern "C"  void AndroidGoogleAnalytics_SendTiming_m948861379 (AndroidGoogleAnalytics_t1297670224 * __this, String_t* ___category0, int64_t ___intervalInMilliseconds1, String_t* ___name2, String_t* ___label3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::CreateTransaction(System.String,System.String,System.Single,System.Single,System.Single,System.String)
extern "C"  void AndroidGoogleAnalytics_CreateTransaction_m3460267764 (AndroidGoogleAnalytics_t1297670224 * __this, String_t* ___transactionId0, String_t* ___affiliation1, float ___revenue2, float ___tax3, float ___shipping4, String_t* ___currencyCode5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::CreateItem(System.String,System.String,System.String,System.String,System.Single,System.Int32,System.String)
extern "C"  void AndroidGoogleAnalytics_CreateItem_m1430931088 (AndroidGoogleAnalytics_t1297670224 * __this, String_t* ___transactionId0, String_t* ___name1, String_t* ___sku2, String_t* ___category3, float ___price4, int32_t ___quantity5, String_t* ___currencyCode6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::SetKey(System.String,System.String)
extern "C"  void AndroidGoogleAnalytics_SetKey_m4142412942 (AndroidGoogleAnalytics_t1297670224 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::ClearKey(System.String)
extern "C"  void AndroidGoogleAnalytics_ClearKey_m2797214699 (AndroidGoogleAnalytics_t1297670224 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::SetLogLevel(GPLogLevel)
extern "C"  void AndroidGoogleAnalytics_SetLogLevel_m2295873098 (AndroidGoogleAnalytics_t1297670224 * __this, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::SetDryRun(System.Boolean)
extern "C"  void AndroidGoogleAnalytics_SetDryRun_m1534470828 (AndroidGoogleAnalytics_t1297670224 * __this, bool ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAnalytics::EnableAdvertisingIdCollection(System.Boolean)
extern "C"  void AndroidGoogleAnalytics_EnableAdvertisingIdCollection_m265718800 (AndroidGoogleAnalytics_t1297670224 * __this, bool ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
