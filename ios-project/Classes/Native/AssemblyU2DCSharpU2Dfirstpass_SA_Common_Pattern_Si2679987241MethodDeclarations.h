﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Si3172014400MethodDeclarations.h"

// System.Void SA.Common.Pattern.Singleton`1<ISN_CloudKit>::.ctor()
#define Singleton_1__ctor_m1333940014(__this, method) ((  void (*) (Singleton_1_t2679987241 *, const MethodInfo*))Singleton_1__ctor_m4152044218_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<ISN_CloudKit>::.cctor()
#define Singleton_1__cctor_m355436883(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1__cctor_m982417053_gshared)(__this /* static, unused */, method)
// T SA.Common.Pattern.Singleton`1<ISN_CloudKit>::get_Instance()
#define Singleton_1_get_Instance_m3629706871(__this /* static, unused */, method) ((  ISN_CloudKit_t2197422136 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m3228489301_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<ISN_CloudKit>::get_HasInstance()
#define Singleton_1_get_HasInstance_m2426431956(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_HasInstance_m2551508260_gshared)(__this /* static, unused */, method)
// System.Boolean SA.Common.Pattern.Singleton`1<ISN_CloudKit>::get_IsDestroyed()
#define Singleton_1_get_IsDestroyed_m1921851192(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_IsDestroyed_m4170993228_gshared)(__this /* static, unused */, method)
// System.Void SA.Common.Pattern.Singleton`1<ISN_CloudKit>::OnDestroy()
#define Singleton_1_OnDestroy_m4217692119(__this, method) ((  void (*) (Singleton_1_t2679987241 *, const MethodInfo*))Singleton_1_OnDestroy_m3790554761_gshared)(__this, method)
// System.Void SA.Common.Pattern.Singleton`1<ISN_CloudKit>::OnApplicationQuit()
#define Singleton_1_OnApplicationQuit_m1401629524(__this, method) ((  void (*) (Singleton_1_t2679987241 *, const MethodInfo*))Singleton_1_OnApplicationQuit_m1956476828_gshared)(__this, method)
