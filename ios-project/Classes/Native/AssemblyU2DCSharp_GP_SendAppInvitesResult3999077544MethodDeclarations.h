﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_SendAppInvitesResult
struct GP_SendAppInvitesResult_t3999077544;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_SendAppInvitesResult::.ctor(System.String)
extern "C"  void GP_SendAppInvitesResult__ctor_m2541698667 (GP_SendAppInvitesResult_t3999077544 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_SendAppInvitesResult::.ctor(System.String[])
extern "C"  void GP_SendAppInvitesResult__ctor_m161147037 (GP_SendAppInvitesResult_t3999077544 * __this, StringU5BU5D_t1642385972* ___invites0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] GP_SendAppInvitesResult::get_InvitationIds()
extern "C"  StringU5BU5D_t1642385972* GP_SendAppInvitesResult_get_InvitationIds_m3427682538 (GP_SendAppInvitesResult_t3999077544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
