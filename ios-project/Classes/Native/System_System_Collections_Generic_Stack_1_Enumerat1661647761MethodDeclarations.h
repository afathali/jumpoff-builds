﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerato132208513MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<Newtonsoft.Json.JsonValidatingReader/SchemaScope>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m3244134656(__this, ___t0, method) ((  void (*) (Enumerator_t1661647761 *, Stack_1_t1011649401 *, const MethodInfo*))Enumerator__ctor_m2816143215_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<Newtonsoft.Json.JsonValidatingReader/SchemaScope>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2253490774(__this, method) ((  void (*) (Enumerator_t1661647761 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m456699159_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<Newtonsoft.Json.JsonValidatingReader/SchemaScope>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3832779574(__this, method) ((  Il2CppObject * (*) (Enumerator_t1661647761 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1270503615_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<Newtonsoft.Json.JsonValidatingReader/SchemaScope>::Dispose()
#define Enumerator_Dispose_m2666629633(__this, method) ((  void (*) (Enumerator_t1661647761 *, const MethodInfo*))Enumerator_Dispose_m1520016780_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<Newtonsoft.Json.JsonValidatingReader/SchemaScope>::MoveNext()
#define Enumerator_MoveNext_m4161516862(__this, method) ((  bool (*) (Enumerator_t1661647761 *, const MethodInfo*))Enumerator_MoveNext_m689054299_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<Newtonsoft.Json.JsonValidatingReader/SchemaScope>::get_Current()
#define Enumerator_get_Current_m3394124977(__this, method) ((  SchemaScope_t4218888543 * (*) (Enumerator_t1661647761 *, const MethodInfo*))Enumerator_get_Current_m2076859656_gshared)(__this, method)
