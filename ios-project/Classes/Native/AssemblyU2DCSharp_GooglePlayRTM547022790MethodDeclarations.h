﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayRTM
struct GooglePlayRTM_t547022790;
// GooglePlayerTemplate[]
struct GooglePlayerTemplateU5BU5D_t318995901;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// GP_RTM_Room
struct GP_RTM_Room_t851604529;
// System.Collections.Generic.List`1<GP_Invite>
struct List_1_t4291017515;
// GP_RTM_Network_Package
struct GP_RTM_Network_Package_t2050307869;
// GP_RTM_ReliableMessageSentResult
struct GP_RTM_ReliableMessageSentResult_t2743629396;
// GP_RTM_ReliableMessageDeliveredResult
struct GP_RTM_ReliableMessageDeliveredResult_t1694070004;
// GP_RTM_Result
struct GP_RTM_Result_t473289279;
// AndroidActivityResult
struct AndroidActivityResult_t3757510801;
// GP_Invite
struct GP_Invite_t626929087;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GP_RTM_PackageType1787524174.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_RTM_Network_Package2050307869.h"
#include "AssemblyU2DCSharp_GP_RTM_Room851604529.h"
#include "AssemblyU2DCSharp_GP_RTM_ReliableMessageSentResult2743629396.h"
#include "AssemblyU2DCSharp_GP_RTM_ReliableMessageDeliveredR1694070004.h"
#include "AssemblyU2DCSharp_GP_GamesStatusCodes1013506173.h"
#include "AssemblyU2DCSharp_GP_RTM_Result473289279.h"
#include "AssemblyU2DCSharp_AndroidActivityResult3757510801.h"
#include "AssemblyU2DCSharp_GP_Invite626929087.h"

// System.Void GooglePlayRTM::.ctor()
extern "C"  void GooglePlayRTM__ctor_m954948057 (GooglePlayRTM_t547022790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::.cctor()
extern "C"  void GooglePlayRTM__cctor_m535820952 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::Awake()
extern "C"  void GooglePlayRTM_Awake_m3618205890 (GooglePlayRTM_t547022790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::FindMatch(System.Int32,System.Int32)
extern "C"  void GooglePlayRTM_FindMatch_m137236355 (GooglePlayRTM_t547022790 * __this, int32_t ___minPlayers0, int32_t ___maxPlayers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::FindMatch(System.Int32,System.Int32,GooglePlayerTemplate[])
extern "C"  void GooglePlayRTM_FindMatch_m1064447911 (GooglePlayRTM_t547022790 * __this, int32_t ___minPlayers0, int32_t ___maxPlayers1, GooglePlayerTemplateU5BU5D_t318995901* ___playersToInvite2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::FindMatch(System.Int32,System.Int32,System.String[])
extern "C"  void GooglePlayRTM_FindMatch_m302701863 (GooglePlayRTM_t547022790 * __this, int32_t ___minPlayers0, int32_t ___maxPlayers1, StringU5BU5D_t1642385972* ___playersToInvite2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::FindMatch(GooglePlayerTemplate[])
extern "C"  void GooglePlayRTM_FindMatch_m3757524955 (GooglePlayRTM_t547022790 * __this, GooglePlayerTemplateU5BU5D_t318995901* ___playersToInvite0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::FindMatch(System.String[])
extern "C"  void GooglePlayRTM_FindMatch_m2071072299 (GooglePlayRTM_t547022790 * __this, StringU5BU5D_t1642385972* ___playersToInvite0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::SendDataToAll(System.Byte[],GP_RTM_PackageType)
extern "C"  void GooglePlayRTM_SendDataToAll_m1953751574 (GooglePlayRTM_t547022790 * __this, ByteU5BU5D_t3397334013* ___data0, int32_t ___sendType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::sendDataToPlayers(System.Byte[],GP_RTM_PackageType,System.String[])
extern "C"  void GooglePlayRTM_sendDataToPlayers_m3234614859 (GooglePlayRTM_t547022790 * __this, ByteU5BU5D_t3397334013* ___data0, int32_t ___sendType1, StringU5BU5D_t1642385972* ___players2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::ShowWaitingRoomIntent()
extern "C"  void GooglePlayRTM_ShowWaitingRoomIntent_m2444186260 (GooglePlayRTM_t547022790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OpenInvitationBoxUI(System.Int32,System.Int32)
extern "C"  void GooglePlayRTM_OpenInvitationBoxUI_m3136103353 (GooglePlayRTM_t547022790 * __this, int32_t ___minPlayers0, int32_t ___maxPlayers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::LeaveRoom()
extern "C"  void GooglePlayRTM_LeaveRoom_m3406708773 (GooglePlayRTM_t547022790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::AcceptInvitation(System.String)
extern "C"  void GooglePlayRTM_AcceptInvitation_m2628397616 (GooglePlayRTM_t547022790 * __this, String_t* ___invitationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::DeclineInvitation(System.String)
extern "C"  void GooglePlayRTM_DeclineInvitation_m3409360398 (GooglePlayRTM_t547022790 * __this, String_t* ___invitationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::DismissInvitation(System.String)
extern "C"  void GooglePlayRTM_DismissInvitation_m1266702272 (GooglePlayRTM_t547022790 * __this, String_t* ___invitationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OpenInvitationInBoxUI()
extern "C"  void GooglePlayRTM_OpenInvitationInBoxUI_m573686014 (GooglePlayRTM_t547022790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::SetVariant(System.Int32)
extern "C"  void GooglePlayRTM_SetVariant_m1335020025 (GooglePlayRTM_t547022790 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::SetExclusiveBitMask(System.Int32)
extern "C"  void GooglePlayRTM_SetExclusiveBitMask_m4133442427 (GooglePlayRTM_t547022790 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::ClearReliableMessageListener(System.Int32)
extern "C"  void GooglePlayRTM_ClearReliableMessageListener_m580695002 (GooglePlayRTM_t547022790 * __this, int32_t ___dataTokenId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_RTM_Room GooglePlayRTM::get_currentRoom()
extern "C"  GP_RTM_Room_t851604529 * GooglePlayRTM_get_currentRoom_m3620685382 (GooglePlayRTM_t547022790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GP_Invite> GooglePlayRTM::get_invitations()
extern "C"  List_1_t4291017515 * GooglePlayRTM_get_invitations_m3258284940 (GooglePlayRTM_t547022790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnWatingRoomIntentClosed(System.String)
extern "C"  void GooglePlayRTM_OnWatingRoomIntentClosed_m2454920349 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnRoomUpdate(System.String)
extern "C"  void GooglePlayRTM_OnRoomUpdate_m4195360372 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnReliableMessageSent(System.String)
extern "C"  void GooglePlayRTM_OnReliableMessageSent_m3511039317 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnReliableMessageDelivered(System.String)
extern "C"  void GooglePlayRTM_OnReliableMessageDelivered_m2327710193 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnMatchDataRecieved(System.String)
extern "C"  void GooglePlayRTM_OnMatchDataRecieved_m3891868934 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnWatingRoomIntentClosed()
extern "C"  void GooglePlayRTM_OnWatingRoomIntentClosed_m3023328823 (GooglePlayRTM_t547022790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnConnectedToRoom(System.String)
extern "C"  void GooglePlayRTM_OnConnectedToRoom_m3677430485 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnDisconnectedFromRoom(System.String)
extern "C"  void GooglePlayRTM_OnDisconnectedFromRoom_m1174421778 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnP2PConnected(System.String)
extern "C"  void GooglePlayRTM_OnP2PConnected_m3176761923 (GooglePlayRTM_t547022790 * __this, String_t* ___participantId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnP2PDisconnected(System.String)
extern "C"  void GooglePlayRTM_OnP2PDisconnected_m261976981 (GooglePlayRTM_t547022790 * __this, String_t* ___participantId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnPeerDeclined(System.String)
extern "C"  void GooglePlayRTM_OnPeerDeclined_m1502436620 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnPeerInvitedToRoom(System.String)
extern "C"  void GooglePlayRTM_OnPeerInvitedToRoom_m3278998553 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnPeerJoined(System.String)
extern "C"  void GooglePlayRTM_OnPeerJoined_m1593786399 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnPeerLeft(System.String)
extern "C"  void GooglePlayRTM_OnPeerLeft_m3725153119 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnPeersConnected(System.String)
extern "C"  void GooglePlayRTM_OnPeersConnected_m906552970 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnPeersDisconnected(System.String)
extern "C"  void GooglePlayRTM_OnPeersDisconnected_m2703766242 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnRoomAutoMatching(System.String)
extern "C"  void GooglePlayRTM_OnRoomAutoMatching_m3936752713 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnRoomConnecting(System.String)
extern "C"  void GooglePlayRTM_OnRoomConnecting_m1417280327 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnJoinedRoom(System.String)
extern "C"  void GooglePlayRTM_OnJoinedRoom_m616493812 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnLeftRoom(System.String)
extern "C"  void GooglePlayRTM_OnLeftRoom_m2645062988 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnRoomConnected(System.String)
extern "C"  void GooglePlayRTM_OnRoomConnected_m300739098 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnRoomCreated(System.String)
extern "C"  void GooglePlayRTM_OnRoomCreated_m2429360485 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnInvitationBoxUiClosed(System.String)
extern "C"  void GooglePlayRTM_OnInvitationBoxUiClosed_m190050946 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnInvitationReceived(System.String)
extern "C"  void GooglePlayRTM_OnInvitationReceived_m3372525690 (GooglePlayRTM_t547022790 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::OnInvitationRemoved(System.String)
extern "C"  void GooglePlayRTM_OnInvitationRemoved_m4002072813 (GooglePlayRTM_t547022790 * __this, String_t* ___invitationId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayRTM::ConvertStringToByteData(System.String)
extern "C"  ByteU5BU5D_t3397334013* GooglePlayRTM_ConvertStringToByteData_m2318147602 (Il2CppObject * __this /* static, unused */, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayRTM::ConvertByteDataToString(System.Byte[])
extern "C"  String_t* GooglePlayRTM_ConvertByteDataToString_m777172636 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionDataRecieved>m__50(GP_RTM_Network_Package)
extern "C"  void GooglePlayRTM_U3CActionDataRecievedU3Em__50_m2764412397 (Il2CppObject * __this /* static, unused */, GP_RTM_Network_Package_t2050307869 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionRoomUpdated>m__51(GP_RTM_Room)
extern "C"  void GooglePlayRTM_U3CActionRoomUpdatedU3Em__51_m2792940753 (Il2CppObject * __this /* static, unused */, GP_RTM_Room_t851604529 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionReliableMessageSent>m__52(GP_RTM_ReliableMessageSentResult)
extern "C"  void GooglePlayRTM_U3CActionReliableMessageSentU3Em__52_m1456295036 (Il2CppObject * __this /* static, unused */, GP_RTM_ReliableMessageSentResult_t2743629396 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionReliableMessageDelivered>m__53(GP_RTM_ReliableMessageDeliveredResult)
extern "C"  void GooglePlayRTM_U3CActionReliableMessageDeliveredU3Em__53_m3221682531 (Il2CppObject * __this /* static, unused */, GP_RTM_ReliableMessageDeliveredResult_t1694070004 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionConnectedToRoom>m__54()
extern "C"  void GooglePlayRTM_U3CActionConnectedToRoomU3Em__54_m4165776364 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionDisconnectedFromRoom>m__55()
extern "C"  void GooglePlayRTM_U3CActionDisconnectedFromRoomU3Em__55_m4214012126 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionP2PConnected>m__56(System.String)
extern "C"  void GooglePlayRTM_U3CActionP2PConnectedU3Em__56_m1412610512 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionP2PDisconnected>m__57(System.String)
extern "C"  void GooglePlayRTM_U3CActionP2PDisconnectedU3Em__57_m4248354359 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionPeerDeclined>m__58(System.String[])
extern "C"  void GooglePlayRTM_U3CActionPeerDeclinedU3Em__58_m3866675119 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionPeerInvitedToRoom>m__59(System.String[])
extern "C"  void GooglePlayRTM_U3CActionPeerInvitedToRoomU3Em__59_m2103592203 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionPeerJoined>m__5A(System.String[])
extern "C"  void GooglePlayRTM_U3CActionPeerJoinedU3Em__5A_m2100421657 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionPeerLeft>m__5B(System.String[])
extern "C"  void GooglePlayRTM_U3CActionPeerLeftU3Em__5B_m1765803144 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionPeersConnected>m__5C(System.String[])
extern "C"  void GooglePlayRTM_U3CActionPeersConnectedU3Em__5C_m3560779910 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionPeersDisconnected>m__5D(System.String[])
extern "C"  void GooglePlayRTM_U3CActionPeersDisconnectedU3Em__5D_m1771119549 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionRoomAutomatching>m__5E()
extern "C"  void GooglePlayRTM_U3CActionRoomAutomatchingU3Em__5E_m1825105987 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionRoomConnecting>m__5F()
extern "C"  void GooglePlayRTM_U3CActionRoomConnectingU3Em__5F_m3564601326 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionJoinedRoom>m__60(GP_GamesStatusCodes)
extern "C"  void GooglePlayRTM_U3CActionJoinedRoomU3Em__60_m541845721 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionLeftRoom>m__61(GP_RTM_Result)
extern "C"  void GooglePlayRTM_U3CActionLeftRoomU3Em__61_m3868249946 (Il2CppObject * __this /* static, unused */, GP_RTM_Result_t473289279 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionRoomConnected>m__62(GP_GamesStatusCodes)
extern "C"  void GooglePlayRTM_U3CActionRoomConnectedU3Em__62_m2718007271 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionRoomCreated>m__63(GP_GamesStatusCodes)
extern "C"  void GooglePlayRTM_U3CActionRoomCreatedU3Em__63_m320888553 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionInvitationBoxUIClosed>m__64(AndroidActivityResult)
extern "C"  void GooglePlayRTM_U3CActionInvitationBoxUIClosedU3Em__64_m2694101763 (Il2CppObject * __this /* static, unused */, AndroidActivityResult_t3757510801 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionWatingRoomIntentClosed>m__65(AndroidActivityResult)
extern "C"  void GooglePlayRTM_U3CActionWatingRoomIntentClosedU3Em__65_m1076990711 (Il2CppObject * __this /* static, unused */, AndroidActivityResult_t3757510801 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionInvitationReceived>m__66(GP_Invite)
extern "C"  void GooglePlayRTM_U3CActionInvitationReceivedU3Em__66_m3073664139 (Il2CppObject * __this /* static, unused */, GP_Invite_t626929087 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayRTM::<ActionInvitationRemoved>m__67(System.String)
extern "C"  void GooglePlayRTM_U3CActionInvitationRemovedU3Em__67_m2711010212 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
