﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GPaymnetManagerExample
struct  GPaymnetManagerExample_t841599384  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct GPaymnetManagerExample_t841599384_StaticFields
{
public:
	// System.Boolean GPaymnetManagerExample::_isInited
	bool ____isInited_6;

public:
	inline static int32_t get_offset_of__isInited_6() { return static_cast<int32_t>(offsetof(GPaymnetManagerExample_t841599384_StaticFields, ____isInited_6)); }
	inline bool get__isInited_6() const { return ____isInited_6; }
	inline bool* get_address_of__isInited_6() { return &____isInited_6; }
	inline void set__isInited_6(bool value)
	{
		____isInited_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
