﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<GP_AdvertisingIdLoadResult>
struct Action_1_t2585174472;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen2259474320.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayUtils
struct  GooglePlayUtils_t968630402  : public SA_Singleton_OLD_1_t2259474320
{
public:

public:
};

struct GooglePlayUtils_t968630402_StaticFields
{
public:
	// System.Action`1<GP_AdvertisingIdLoadResult> GooglePlayUtils::ActionAdvertisingIdLoaded
	Action_1_t2585174472 * ___ActionAdvertisingIdLoaded_4;
	// System.Action`1<GP_AdvertisingIdLoadResult> GooglePlayUtils::<>f__am$cache1
	Action_1_t2585174472 * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_ActionAdvertisingIdLoaded_4() { return static_cast<int32_t>(offsetof(GooglePlayUtils_t968630402_StaticFields, ___ActionAdvertisingIdLoaded_4)); }
	inline Action_1_t2585174472 * get_ActionAdvertisingIdLoaded_4() const { return ___ActionAdvertisingIdLoaded_4; }
	inline Action_1_t2585174472 ** get_address_of_ActionAdvertisingIdLoaded_4() { return &___ActionAdvertisingIdLoaded_4; }
	inline void set_ActionAdvertisingIdLoaded_4(Action_1_t2585174472 * value)
	{
		___ActionAdvertisingIdLoaded_4 = value;
		Il2CppCodeGenWriteBarrier(&___ActionAdvertisingIdLoaded_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(GooglePlayUtils_t968630402_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Action_1_t2585174472 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Action_1_t2585174472 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Action_1_t2585174472 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
