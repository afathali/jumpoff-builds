﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Switch/ChangeCallback
struct ChangeCallback_t1172037850;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Switch/ChangeCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ChangeCallback__ctor_m383261165 (ChangeCallback_t1172037850 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Switch/ChangeCallback::Invoke(System.Boolean)
extern "C"  void ChangeCallback_Invoke_m2181586236 (ChangeCallback_t1172037850 * __this, bool ___on0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Switch/ChangeCallback::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ChangeCallback_BeginInvoke_m4291763589 (ChangeCallback_t1172037850 * __this, bool ___on0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Switch/ChangeCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ChangeCallback_EndInvoke_m3627032627 (ChangeCallback_t1172037850 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
