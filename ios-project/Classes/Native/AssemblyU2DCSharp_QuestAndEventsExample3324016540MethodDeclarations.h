﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QuestAndEventsExample
struct QuestAndEventsExample_t3324016540;
// GP_SendAppInvitesResult
struct GP_SendAppInvitesResult_t3999077544;
// GP_RetrieveAppInviteResult
struct GP_RetrieveAppInviteResult_t18485229;
// GooglePlayResult
struct GooglePlayResult_t3097469636;
// GP_QuestResult
struct GP_QuestResult_t3390940437;
// GooglePlayConnectionResult
struct GooglePlayConnectionResult_t2758718724;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GP_SendAppInvitesResult3999077544.h"
#include "AssemblyU2DCSharp_GP_RetrieveAppInviteResult18485229.h"
#include "AssemblyU2DCSharp_GooglePlayResult3097469636.h"
#include "AssemblyU2DCSharp_GP_QuestResult3390940437.h"
#include "AssemblyU2DCSharp_GooglePlayConnectionResult2758718724.h"

// System.Void QuestAndEventsExample::.ctor()
extern "C"  void QuestAndEventsExample__ctor_m3650216897 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::Start()
extern "C"  void QuestAndEventsExample_Start_m117042913 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::ConncetButtonPress()
extern "C"  void QuestAndEventsExample_ConncetButtonPress_m53536774 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::SendInvitation()
extern "C"  void QuestAndEventsExample_SendInvitation_m3053763514 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::HandleActionAppInvitesSent(GP_SendAppInvitesResult)
extern "C"  void QuestAndEventsExample_HandleActionAppInvitesSent_m3024585324 (QuestAndEventsExample_t3324016540 * __this, GP_SendAppInvitesResult_t3999077544 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::GetInvitation()
extern "C"  void QuestAndEventsExample_GetInvitation_m201185740 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::HandleActionAppInviteRetrieved(GP_RetrieveAppInviteResult)
extern "C"  void QuestAndEventsExample_HandleActionAppInviteRetrieved_m1115275572 (QuestAndEventsExample_t3324016540 * __this, GP_RetrieveAppInviteResult_t18485229 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::FixedUpdate()
extern "C"  void QuestAndEventsExample_FixedUpdate_m2836758044 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::LoadEvents()
extern "C"  void QuestAndEventsExample_LoadEvents_m1313088470 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::IncrementEvent()
extern "C"  void QuestAndEventsExample_IncrementEvent_m2946207248 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::ShowAllQuests()
extern "C"  void QuestAndEventsExample_ShowAllQuests_m3443361988 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::ShowAcceptedQuests()
extern "C"  void QuestAndEventsExample_ShowAcceptedQuests_m1103787004 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::ShowCompletedQuests()
extern "C"  void QuestAndEventsExample_ShowCompletedQuests_m205454138 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::ShowOpenQuests()
extern "C"  void QuestAndEventsExample_ShowOpenQuests_m2299522715 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::AcceptQuest()
extern "C"  void QuestAndEventsExample_AcceptQuest_m4095342155 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::LoadQuests()
extern "C"  void QuestAndEventsExample_LoadQuests_m1881791144 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::OnEventsLoaded(GooglePlayResult)
extern "C"  void QuestAndEventsExample_OnEventsLoaded_m3834872598 (QuestAndEventsExample_t3324016540 * __this, GooglePlayResult_t3097469636 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::OnQuestsAccepted(GP_QuestResult)
extern "C"  void QuestAndEventsExample_OnQuestsAccepted_m3409306211 (QuestAndEventsExample_t3324016540 * __this, GP_QuestResult_t3390940437 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::OnQuestsCompleted(GP_QuestResult)
extern "C"  void QuestAndEventsExample_OnQuestsCompleted_m2008235443 (QuestAndEventsExample_t3324016540 * __this, GP_QuestResult_t3390940437 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::OnQuestsLoaded(GooglePlayResult)
extern "C"  void QuestAndEventsExample_OnQuestsLoaded_m2309373564 (QuestAndEventsExample_t3324016540 * __this, GooglePlayResult_t3097469636 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::OnPlayerDisconnected()
extern "C"  void QuestAndEventsExample_OnPlayerDisconnected_m2926076440 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::OnPlayerConnected()
extern "C"  void QuestAndEventsExample_OnPlayerConnected_m1439912480 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::OnConnectionResult(GooglePlayConnectionResult)
extern "C"  void QuestAndEventsExample_OnConnectionResult_m2389456549 (QuestAndEventsExample_t3324016540 * __this, GooglePlayConnectionResult_t2758718724 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestAndEventsExample::OnDestroy()
extern "C"  void QuestAndEventsExample_OnDestroy_m1677995906 (QuestAndEventsExample_t3324016540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
