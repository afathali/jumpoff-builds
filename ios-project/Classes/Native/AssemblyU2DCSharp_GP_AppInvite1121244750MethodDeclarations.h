﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_AppInvite
struct GP_AppInvite_t1121244750;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_AppInvite::.ctor(System.String,System.String,System.Boolean)
extern "C"  void GP_AppInvite__ctor_m949443112 (GP_AppInvite_t1121244750 * __this, String_t* ___id0, String_t* ___link1, bool ___isOpenedFromPlatStore2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GP_AppInvite::get_Id()
extern "C"  String_t* GP_AppInvite_get_Id_m1671630988 (GP_AppInvite_t1121244750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GP_AppInvite::get_DeepLink()
extern "C"  String_t* GP_AppInvite_get_DeepLink_m2281804407 (GP_AppInvite_t1121244750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GP_AppInvite::get_IsOpenedFromPlayStore()
extern "C"  bool GP_AppInvite_get_IsOpenedFromPlayStore_m816180156 (GP_AppInvite_t1121244750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
