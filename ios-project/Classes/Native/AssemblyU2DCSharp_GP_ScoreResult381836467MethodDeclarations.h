﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_ScoreResult
struct GP_ScoreResult_t381836467;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_ScoreResult::.ctor(System.String)
extern "C"  void GP_ScoreResult__ctor_m131273108 (GP_ScoreResult_t381836467 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
