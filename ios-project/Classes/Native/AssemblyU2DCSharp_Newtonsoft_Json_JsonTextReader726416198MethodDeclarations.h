﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonTextReader
struct JsonTextReader_t726416198;
// System.IO.TextReader
struct TextReader_t1561828458;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;
// Newtonsoft.Json.JsonReaderException
struct JsonReaderException_t2043888884;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_TextReader1561828458.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Nullable_1_gen3282734688.h"
#include "mscorlib_System_Nullable_1_gen3921022517.h"

// System.Void Newtonsoft.Json.JsonTextReader::.ctor(System.IO.TextReader)
extern "C"  void JsonTextReader__ctor_m2463575475 (JsonTextReader_t726416198 * __this, TextReader_t1561828458 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo Newtonsoft.Json.JsonTextReader::get_Culture()
extern "C"  CultureInfo_t3500843524 * JsonTextReader_get_Culture_m2026757844 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::set_Culture(System.Globalization.CultureInfo)
extern "C"  void JsonTextReader_set_Culture_m2228554607 (JsonTextReader_t726416198 * __this, CultureInfo_t3500843524 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseString(System.Char)
extern "C"  void JsonTextReader_ParseString_m3792016459 (JsonTextReader_t726416198 * __this, Il2CppChar ___quote0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ReadStringIntoBuffer(System.Char)
extern "C"  void JsonTextReader_ReadStringIntoBuffer_m4231782246 (JsonTextReader_t726416198 * __this, Il2CppChar ___quote0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonTextReader::CreateJsonReaderException(System.String,System.Object[])
extern "C"  JsonReaderException_t2043888884 * JsonTextReader_CreateJsonReaderException_m2107434322 (JsonTextReader_t726416198 * __this, String_t* ___format0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan Newtonsoft.Json.JsonTextReader::ReadOffset(System.String)
extern "C"  TimeSpan_t3430258949  JsonTextReader_ReadOffset_m3281630104 (JsonTextReader_t726416198 * __this, String_t* ___offsetText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseDate(System.String)
extern "C"  void JsonTextReader_ParseDate_m4167935103 (JsonTextReader_t726416198 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.JsonTextReader::MoveNext()
extern "C"  Il2CppChar JsonTextReader_MoveNext_m1124633982 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::HasNext()
extern "C"  bool JsonTextReader_HasNext_m1354689209 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonTextReader::PeekNext()
extern "C"  int32_t JsonTextReader_PeekNext_m3346247500 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::Read()
extern "C"  bool JsonTextReader_Read_m2412532718 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.JsonTextReader::ReadAsBytes()
extern "C"  ByteU5BU5D_t3397334013* JsonTextReader_ReadAsBytes_m3719431345 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Decimal> Newtonsoft.Json.JsonTextReader::ReadAsDecimal()
extern "C"  Nullable_1_t3282734688  JsonTextReader_ReadAsDecimal_m3075350713 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.JsonTextReader::ReadAsDateTimeOffset()
extern "C"  Nullable_1_t3921022517  JsonTextReader_ReadAsDateTimeOffset_m2154176565 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ReadInternal()
extern "C"  bool JsonTextReader_ReadInternal_m3602018725 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ParsePostValue(System.Char)
extern "C"  bool JsonTextReader_ParsePostValue_m4189940401 (JsonTextReader_t726416198 * __this, Il2CppChar ___currentChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ParseObject(System.Char)
extern "C"  bool JsonTextReader_ParseObject_m1173214631 (JsonTextReader_t726416198 * __this, Il2CppChar ___currentChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ParseProperty(System.Char)
extern "C"  bool JsonTextReader_ParseProperty_m4024934829 (JsonTextReader_t726416198 * __this, Il2CppChar ___firstChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ValidIdentifierChar(System.Char)
extern "C"  bool JsonTextReader_ValidIdentifierChar_m2574012890 (JsonTextReader_t726416198 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.JsonTextReader::ParseUnquotedProperty(System.Char)
extern "C"  Il2CppChar JsonTextReader_ParseUnquotedProperty_m3650994346 (JsonTextReader_t726416198 * __this, Il2CppChar ___firstChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::ParseValue(System.Char)
extern "C"  bool JsonTextReader_ParseValue_m2244009721 (JsonTextReader_t726416198 * __this, Il2CppChar ___currentChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::EatWhitespace(System.Char,System.Boolean,System.Char&)
extern "C"  bool JsonTextReader_EatWhitespace_m1771762572 (JsonTextReader_t726416198 * __this, Il2CppChar ___initialChar0, bool ___oneOrMore1, Il2CppChar* ___finalChar2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseConstructor()
extern "C"  void JsonTextReader_ParseConstructor_m2166241969 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseNumber(System.Char)
extern "C"  void JsonTextReader_ParseNumber_m416804605 (JsonTextReader_t726416198 * __this, Il2CppChar ___firstChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseComment()
extern "C"  void JsonTextReader_ParseComment_m1606368344 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::MatchValue(System.Char,System.String)
extern "C"  bool JsonTextReader_MatchValue_m752183729 (JsonTextReader_t726416198 * __this, Il2CppChar ___firstChar0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::MatchValue(System.Char,System.String,System.Boolean)
extern "C"  bool JsonTextReader_MatchValue_m1420992556 (JsonTextReader_t726416198 * __this, Il2CppChar ___firstChar0, String_t* ___value1, bool ___noTrailingNonSeperatorCharacters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::IsSeperator(System.Char)
extern "C"  bool JsonTextReader_IsSeperator_m2954612508 (JsonTextReader_t726416198 * __this, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseTrue()
extern "C"  void JsonTextReader_ParseTrue_m3706255175 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseNull()
extern "C"  void JsonTextReader_ParseNull_m1525818776 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseUndefined()
extern "C"  void JsonTextReader_ParseUndefined_m3229105671 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseFalse()
extern "C"  void JsonTextReader_ParseFalse_m3198558298 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseNumberNegativeInfinity()
extern "C"  void JsonTextReader_ParseNumberNegativeInfinity_m3905746359 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseNumberPositiveInfinity()
extern "C"  void JsonTextReader_ParseNumberPositiveInfinity_m995862471 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::ParseNumberNaN()
extern "C"  void JsonTextReader_ParseNumberNaN_m1545994371 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextReader::Close()
extern "C"  void JsonTextReader_Close_m72045098 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextReader::HasLineInfo()
extern "C"  bool JsonTextReader_HasLineInfo_m2697711318 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonTextReader::get_LineNumber()
extern "C"  int32_t JsonTextReader_get_LineNumber_m2854207748 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonTextReader::get_LinePosition()
extern "C"  int32_t JsonTextReader_get_LinePosition_m3515363942 (JsonTextReader_t726416198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
