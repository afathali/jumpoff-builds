﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardCustomGUIExample
struct LeaderboardCustomGUIExample_t1008136379;
// SA.Common.Models.Result
struct Result_t4287219743;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"

// System.Void LeaderboardCustomGUIExample::.ctor()
extern "C"  void LeaderboardCustomGUIExample__ctor_m2311321340 (LeaderboardCustomGUIExample_t1008136379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardCustomGUIExample::Awake()
extern "C"  void LeaderboardCustomGUIExample_Awake_m2228634717 (LeaderboardCustomGUIExample_t1008136379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardCustomGUIExample::OnGUI()
extern "C"  void LeaderboardCustomGUIExample_OnGUI_m597787820 (LeaderboardCustomGUIExample_t1008136379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardCustomGUIExample::OnScoresListLoaded(SA.Common.Models.Result)
extern "C"  void LeaderboardCustomGUIExample_OnScoresListLoaded_m1809838531 (LeaderboardCustomGUIExample_t1008136379 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardCustomGUIExample::OnAuthFinished(SA.Common.Models.Result)
extern "C"  void LeaderboardCustomGUIExample_OnAuthFinished_m938979507 (LeaderboardCustomGUIExample_t1008136379 * __this, Result_t4287219743 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
