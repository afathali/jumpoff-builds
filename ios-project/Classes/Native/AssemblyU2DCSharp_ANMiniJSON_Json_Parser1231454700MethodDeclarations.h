﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ANMiniJSON.Json/Parser
struct Parser_t1231454700;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ANMiniJSON_Json_Parser_TOKEN2018455390.h"

// System.Void ANMiniJSON.Json/Parser::.ctor(System.String)
extern "C"  void Parser__ctor_m3185171355 (Parser_t1231454700 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ANMiniJSON.Json/Parser::Parse(System.String)
extern "C"  Il2CppObject * Parser_Parse_m1803472319 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ANMiniJSON.Json/Parser::Dispose()
extern "C"  void Parser_Dispose_m4104283878 (Parser_t1231454700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> ANMiniJSON.Json/Parser::ParseObject()
extern "C"  Dictionary_2_t309261261 * Parser_ParseObject_m3937143245 (Parser_t1231454700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> ANMiniJSON.Json/Parser::ParseArray()
extern "C"  List_1_t2058570427 * Parser_ParseArray_m2715181418 (Parser_t1231454700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ANMiniJSON.Json/Parser::ParseValue()
extern "C"  Il2CppObject * Parser_ParseValue_m3778379292 (Parser_t1231454700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ANMiniJSON.Json/Parser::ParseByToken(ANMiniJSON.Json/Parser/TOKEN)
extern "C"  Il2CppObject * Parser_ParseByToken_m1735706887 (Parser_t1231454700 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ANMiniJSON.Json/Parser::ParseString()
extern "C"  String_t* Parser_ParseString_m18482054 (Parser_t1231454700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ANMiniJSON.Json/Parser::ParseNumber()
extern "C"  Il2CppObject * Parser_ParseNumber_m3036945012 (Parser_t1231454700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ANMiniJSON.Json/Parser::EatWhitespace()
extern "C"  void Parser_EatWhitespace_m1761493728 (Parser_t1231454700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ANMiniJSON.Json/Parser::get_PeekChar()
extern "C"  Il2CppChar Parser_get_PeekChar_m3731580855 (Parser_t1231454700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ANMiniJSON.Json/Parser::get_NextChar()
extern "C"  Il2CppChar Parser_get_NextChar_m2461194639 (Parser_t1231454700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ANMiniJSON.Json/Parser::get_NextWord()
extern "C"  String_t* Parser_get_NextWord_m1145529838 (Parser_t1231454700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ANMiniJSON.Json/Parser/TOKEN ANMiniJSON.Json/Parser::get_NextToken()
extern "C"  int32_t Parser_get_NextToken_m1870177949 (Parser_t1231454700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
