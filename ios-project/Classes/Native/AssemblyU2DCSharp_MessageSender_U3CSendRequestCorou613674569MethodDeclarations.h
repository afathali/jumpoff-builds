﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>
struct U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CSendRequestCoroutineU3Ec__Iterator18_2__ctor_m1865567309_gshared (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 * __this, const MethodInfo* method);
#define U3CSendRequestCoroutineU3Ec__Iterator18_2__ctor_m1865567309(__this, method) ((  void (*) (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 *, const MethodInfo*))U3CSendRequestCoroutineU3Ec__Iterator18_2__ctor_m1865567309_gshared)(__this, method)
// System.Object MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendRequestCoroutineU3Ec__Iterator18_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m166941371_gshared (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 * __this, const MethodInfo* method);
#define U3CSendRequestCoroutineU3Ec__Iterator18_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m166941371(__this, method) ((  Il2CppObject * (*) (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 *, const MethodInfo*))U3CSendRequestCoroutineU3Ec__Iterator18_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m166941371_gshared)(__this, method)
// System.Object MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendRequestCoroutineU3Ec__Iterator18_2_System_Collections_IEnumerator_get_Current_m1216179635_gshared (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 * __this, const MethodInfo* method);
#define U3CSendRequestCoroutineU3Ec__Iterator18_2_System_Collections_IEnumerator_get_Current_m1216179635(__this, method) ((  Il2CppObject * (*) (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 *, const MethodInfo*))U3CSendRequestCoroutineU3Ec__Iterator18_2_System_Collections_IEnumerator_get_Current_m1216179635_gshared)(__this, method)
// System.Boolean MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>::MoveNext()
extern "C"  bool U3CSendRequestCoroutineU3Ec__Iterator18_2_MoveNext_m2351241099_gshared (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 * __this, const MethodInfo* method);
#define U3CSendRequestCoroutineU3Ec__Iterator18_2_MoveNext_m2351241099(__this, method) ((  bool (*) (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 *, const MethodInfo*))U3CSendRequestCoroutineU3Ec__Iterator18_2_MoveNext_m2351241099_gshared)(__this, method)
// System.Void MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CSendRequestCoroutineU3Ec__Iterator18_2_Dispose_m329621930_gshared (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 * __this, const MethodInfo* method);
#define U3CSendRequestCoroutineU3Ec__Iterator18_2_Dispose_m329621930(__this, method) ((  void (*) (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 *, const MethodInfo*))U3CSendRequestCoroutineU3Ec__Iterator18_2_Dispose_m329621930_gshared)(__this, method)
// System.Void MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>::Reset()
extern "C"  void U3CSendRequestCoroutineU3Ec__Iterator18_2_Reset_m1034124292_gshared (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 * __this, const MethodInfo* method);
#define U3CSendRequestCoroutineU3Ec__Iterator18_2_Reset_m1034124292(__this, method) ((  void (*) (U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569 *, const MethodInfo*))U3CSendRequestCoroutineU3Ec__Iterator18_2_Reset_m1034124292_gshared)(__this, method)
