﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_KeyedColle1294060137MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::.ctor()
#define KeyedCollection_2__ctor_m3707544873(__this, method) ((  void (*) (KeyedCollection_2_t3639397801 *, const MethodInfo*))KeyedCollection_2__ctor_m3929680866_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>,System.Int32)
#define KeyedCollection_2__ctor_m540929632(__this, ___comparer0, ___dictionaryCreationThreshold1, method) ((  void (*) (KeyedCollection_2_t3639397801 *, Il2CppObject*, int32_t, const MethodInfo*))KeyedCollection_2__ctor_m1398787942_gshared)(__this, ___comparer0, ___dictionaryCreationThreshold1, method)
// System.Boolean System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::Contains(TKey)
#define KeyedCollection_2_Contains_m2700971723(__this, ___key0, method) ((  bool (*) (KeyedCollection_2_t3639397801 *, String_t*, const MethodInfo*))KeyedCollection_2_Contains_m3918219472_gshared)(__this, ___key0, method)
// System.Int32 System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::IndexOfKey(TKey)
#define KeyedCollection_2_IndexOfKey_m3761416143(__this, ___key0, method) ((  int32_t (*) (KeyedCollection_2_t3639397801 *, String_t*, const MethodInfo*))KeyedCollection_2_IndexOfKey_m872387181_gshared)(__this, ___key0, method)
// TItem System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::get_Item(TKey)
#define KeyedCollection_2_get_Item_m1408332995(__this, ___key0, method) ((  JsonProperty_t2712067825 * (*) (KeyedCollection_2_t3639397801 *, String_t*, const MethodInfo*))KeyedCollection_2_get_Item_m1357505817_gshared)(__this, ___key0, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::ClearItems()
#define KeyedCollection_2_ClearItems_m277518965(__this, method) ((  void (*) (KeyedCollection_2_t3639397801 *, const MethodInfo*))KeyedCollection_2_ClearItems_m3110573139_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::InsertItem(System.Int32,TItem)
#define KeyedCollection_2_InsertItem_m3312031708(__this, ___index0, ___item1, method) ((  void (*) (KeyedCollection_2_t3639397801 *, int32_t, JsonProperty_t2712067825 *, const MethodInfo*))KeyedCollection_2_InsertItem_m2740229522_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::RemoveItem(System.Int32)
#define KeyedCollection_2_RemoveItem_m1316104256(__this, ___index0, method) ((  void (*) (KeyedCollection_2_t3639397801 *, int32_t, const MethodInfo*))KeyedCollection_2_RemoveItem_m1708519878_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::SetItem(System.Int32,TItem)
#define KeyedCollection_2_SetItem_m3507623273(__this, ___index0, ___item1, method) ((  void (*) (KeyedCollection_2_t3639397801 *, int32_t, JsonProperty_t2712067825 *, const MethodInfo*))KeyedCollection_2_SetItem_m4134855463_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IDictionary`2<TKey,TItem> System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::get_Dictionary()
#define KeyedCollection_2_get_Dictionary_m1295725866(__this, method) ((  Il2CppObject* (*) (KeyedCollection_2_t3639397801 *, const MethodInfo*))KeyedCollection_2_get_Dictionary_m686852578_gshared)(__this, method)
