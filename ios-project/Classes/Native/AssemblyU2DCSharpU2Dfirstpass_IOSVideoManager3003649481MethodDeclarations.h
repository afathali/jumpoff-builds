﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSVideoManager
struct IOSVideoManager_t3003649481;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void IOSVideoManager::.ctor()
extern "C"  void IOSVideoManager__ctor_m3709777492 (IOSVideoManager_t3003649481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSVideoManager::PlayStreamingVideo(System.String)
extern "C"  void IOSVideoManager_PlayStreamingVideo_m2800443365 (IOSVideoManager_t3003649481 * __this, String_t* ___videoUrl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSVideoManager::OpenYouTubeVideo(System.String)
extern "C"  void IOSVideoManager_OpenYouTubeVideo_m954775272 (IOSVideoManager_t3003649481 * __this, String_t* ___videoUrl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
