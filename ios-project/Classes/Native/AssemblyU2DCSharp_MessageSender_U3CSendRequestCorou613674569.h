﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2525452034;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Text.UTF8Encoding
struct UTF8Encoding_t111055448;
// UnityEngine.WWW
struct WWW_t2919945039;
// MessageSender
struct MessageSender_t204794706;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageSender/<SendRequestCoroutine>c__Iterator18`2<System.Object,System.Object>
struct  U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569  : public Il2CppObject
{
public:
	// System.Action`2<System.Boolean,T2> MessageSender/<SendRequestCoroutine>c__Iterator18`2::callback
	Action_2_t2525452034 * ___callback_0;
	// System.Action`2<System.Boolean,T2> MessageSender/<SendRequestCoroutine>c__Iterator18`2::<local>__0
	Action_2_t2525452034 * ___U3ClocalU3E__0_1;
	// T1 MessageSender/<SendRequestCoroutine>c__Iterator18`2::request
	Il2CppObject * ___request_2;
	// System.String MessageSender/<SendRequestCoroutine>c__Iterator18`2::<jsonData>__1
	String_t* ___U3CjsonDataU3E__1_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> MessageSender/<SendRequestCoroutine>c__Iterator18`2::<postHeader>__2
	Dictionary_2_t3943999495 * ___U3CpostHeaderU3E__2_4;
	// System.Text.UTF8Encoding MessageSender/<SendRequestCoroutine>c__Iterator18`2::<encoding>__3
	UTF8Encoding_t111055448 * ___U3CencodingU3E__3_5;
	// UnityEngine.WWW MessageSender/<SendRequestCoroutine>c__Iterator18`2::<wwwRequest>__4
	WWW_t2919945039 * ___U3CwwwRequestU3E__4_6;
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String> MessageSender/<SendRequestCoroutine>c__Iterator18`2::<$s_330>__5
	Enumerator_t969056901  ___U3CU24s_330U3E__5_7;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.String> MessageSender/<SendRequestCoroutine>c__Iterator18`2::<item>__6
	KeyValuePair_2_t1701344717  ___U3CitemU3E__6_8;
	// System.Boolean MessageSender/<SendRequestCoroutine>c__Iterator18`2::<error>__7
	bool ___U3CerrorU3E__7_9;
	// System.Int32 MessageSender/<SendRequestCoroutine>c__Iterator18`2::<index>__8
	int32_t ___U3CindexU3E__8_10;
	// System.String MessageSender/<SendRequestCoroutine>c__Iterator18`2::<temp>__9
	String_t* ___U3CtempU3E__9_11;
	// T2 MessageSender/<SendRequestCoroutine>c__Iterator18`2::<response>__10
	Il2CppObject * ___U3CresponseU3E__10_12;
	// System.Int32 MessageSender/<SendRequestCoroutine>c__Iterator18`2::$PC
	int32_t ___U24PC_13;
	// System.Object MessageSender/<SendRequestCoroutine>c__Iterator18`2::$current
	Il2CppObject * ___U24current_14;
	// System.Action`2<System.Boolean,T2> MessageSender/<SendRequestCoroutine>c__Iterator18`2::<$>callback
	Action_2_t2525452034 * ___U3CU24U3Ecallback_15;
	// T1 MessageSender/<SendRequestCoroutine>c__Iterator18`2::<$>request
	Il2CppObject * ___U3CU24U3Erequest_16;
	// MessageSender MessageSender/<SendRequestCoroutine>c__Iterator18`2::<>f__this
	MessageSender_t204794706 * ___U3CU3Ef__this_17;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___callback_0)); }
	inline Action_2_t2525452034 * get_callback_0() const { return ___callback_0; }
	inline Action_2_t2525452034 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_2_t2525452034 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}

	inline static int32_t get_offset_of_U3ClocalU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3ClocalU3E__0_1)); }
	inline Action_2_t2525452034 * get_U3ClocalU3E__0_1() const { return ___U3ClocalU3E__0_1; }
	inline Action_2_t2525452034 ** get_address_of_U3ClocalU3E__0_1() { return &___U3ClocalU3E__0_1; }
	inline void set_U3ClocalU3E__0_1(Action_2_t2525452034 * value)
	{
		___U3ClocalU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClocalU3E__0_1, value);
	}

	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___request_2)); }
	inline Il2CppObject * get_request_2() const { return ___request_2; }
	inline Il2CppObject ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(Il2CppObject * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier(&___request_2, value);
	}

	inline static int32_t get_offset_of_U3CjsonDataU3E__1_3() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3CjsonDataU3E__1_3)); }
	inline String_t* get_U3CjsonDataU3E__1_3() const { return ___U3CjsonDataU3E__1_3; }
	inline String_t** get_address_of_U3CjsonDataU3E__1_3() { return &___U3CjsonDataU3E__1_3; }
	inline void set_U3CjsonDataU3E__1_3(String_t* value)
	{
		___U3CjsonDataU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonDataU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CpostHeaderU3E__2_4() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3CpostHeaderU3E__2_4)); }
	inline Dictionary_2_t3943999495 * get_U3CpostHeaderU3E__2_4() const { return ___U3CpostHeaderU3E__2_4; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CpostHeaderU3E__2_4() { return &___U3CpostHeaderU3E__2_4; }
	inline void set_U3CpostHeaderU3E__2_4(Dictionary_2_t3943999495 * value)
	{
		___U3CpostHeaderU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpostHeaderU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CencodingU3E__3_5() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3CencodingU3E__3_5)); }
	inline UTF8Encoding_t111055448 * get_U3CencodingU3E__3_5() const { return ___U3CencodingU3E__3_5; }
	inline UTF8Encoding_t111055448 ** get_address_of_U3CencodingU3E__3_5() { return &___U3CencodingU3E__3_5; }
	inline void set_U3CencodingU3E__3_5(UTF8Encoding_t111055448 * value)
	{
		___U3CencodingU3E__3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CencodingU3E__3_5, value);
	}

	inline static int32_t get_offset_of_U3CwwwRequestU3E__4_6() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3CwwwRequestU3E__4_6)); }
	inline WWW_t2919945039 * get_U3CwwwRequestU3E__4_6() const { return ___U3CwwwRequestU3E__4_6; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwRequestU3E__4_6() { return &___U3CwwwRequestU3E__4_6; }
	inline void set_U3CwwwRequestU3E__4_6(WWW_t2919945039 * value)
	{
		___U3CwwwRequestU3E__4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwRequestU3E__4_6, value);
	}

	inline static int32_t get_offset_of_U3CU24s_330U3E__5_7() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3CU24s_330U3E__5_7)); }
	inline Enumerator_t969056901  get_U3CU24s_330U3E__5_7() const { return ___U3CU24s_330U3E__5_7; }
	inline Enumerator_t969056901 * get_address_of_U3CU24s_330U3E__5_7() { return &___U3CU24s_330U3E__5_7; }
	inline void set_U3CU24s_330U3E__5_7(Enumerator_t969056901  value)
	{
		___U3CU24s_330U3E__5_7 = value;
	}

	inline static int32_t get_offset_of_U3CitemU3E__6_8() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3CitemU3E__6_8)); }
	inline KeyValuePair_2_t1701344717  get_U3CitemU3E__6_8() const { return ___U3CitemU3E__6_8; }
	inline KeyValuePair_2_t1701344717 * get_address_of_U3CitemU3E__6_8() { return &___U3CitemU3E__6_8; }
	inline void set_U3CitemU3E__6_8(KeyValuePair_2_t1701344717  value)
	{
		___U3CitemU3E__6_8 = value;
	}

	inline static int32_t get_offset_of_U3CerrorU3E__7_9() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3CerrorU3E__7_9)); }
	inline bool get_U3CerrorU3E__7_9() const { return ___U3CerrorU3E__7_9; }
	inline bool* get_address_of_U3CerrorU3E__7_9() { return &___U3CerrorU3E__7_9; }
	inline void set_U3CerrorU3E__7_9(bool value)
	{
		___U3CerrorU3E__7_9 = value;
	}

	inline static int32_t get_offset_of_U3CindexU3E__8_10() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3CindexU3E__8_10)); }
	inline int32_t get_U3CindexU3E__8_10() const { return ___U3CindexU3E__8_10; }
	inline int32_t* get_address_of_U3CindexU3E__8_10() { return &___U3CindexU3E__8_10; }
	inline void set_U3CindexU3E__8_10(int32_t value)
	{
		___U3CindexU3E__8_10 = value;
	}

	inline static int32_t get_offset_of_U3CtempU3E__9_11() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3CtempU3E__9_11)); }
	inline String_t* get_U3CtempU3E__9_11() const { return ___U3CtempU3E__9_11; }
	inline String_t** get_address_of_U3CtempU3E__9_11() { return &___U3CtempU3E__9_11; }
	inline void set_U3CtempU3E__9_11(String_t* value)
	{
		___U3CtempU3E__9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtempU3E__9_11, value);
	}

	inline static int32_t get_offset_of_U3CresponseU3E__10_12() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3CresponseU3E__10_12)); }
	inline Il2CppObject * get_U3CresponseU3E__10_12() const { return ___U3CresponseU3E__10_12; }
	inline Il2CppObject ** get_address_of_U3CresponseU3E__10_12() { return &___U3CresponseU3E__10_12; }
	inline void set_U3CresponseU3E__10_12(Il2CppObject * value)
	{
		___U3CresponseU3E__10_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresponseU3E__10_12, value);
	}

	inline static int32_t get_offset_of_U24PC_13() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U24PC_13)); }
	inline int32_t get_U24PC_13() const { return ___U24PC_13; }
	inline int32_t* get_address_of_U24PC_13() { return &___U24PC_13; }
	inline void set_U24PC_13(int32_t value)
	{
		___U24PC_13 = value;
	}

	inline static int32_t get_offset_of_U24current_14() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U24current_14)); }
	inline Il2CppObject * get_U24current_14() const { return ___U24current_14; }
	inline Il2CppObject ** get_address_of_U24current_14() { return &___U24current_14; }
	inline void set_U24current_14(Il2CppObject * value)
	{
		___U24current_14 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_14, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecallback_15() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3CU24U3Ecallback_15)); }
	inline Action_2_t2525452034 * get_U3CU24U3Ecallback_15() const { return ___U3CU24U3Ecallback_15; }
	inline Action_2_t2525452034 ** get_address_of_U3CU24U3Ecallback_15() { return &___U3CU24U3Ecallback_15; }
	inline void set_U3CU24U3Ecallback_15(Action_2_t2525452034 * value)
	{
		___U3CU24U3Ecallback_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecallback_15, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Erequest_16() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3CU24U3Erequest_16)); }
	inline Il2CppObject * get_U3CU24U3Erequest_16() const { return ___U3CU24U3Erequest_16; }
	inline Il2CppObject ** get_address_of_U3CU24U3Erequest_16() { return &___U3CU24U3Erequest_16; }
	inline void set_U3CU24U3Erequest_16(Il2CppObject * value)
	{
		___U3CU24U3Erequest_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Erequest_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_17() { return static_cast<int32_t>(offsetof(U3CSendRequestCoroutineU3Ec__Iterator18_2_t613674569, ___U3CU3Ef__this_17)); }
	inline MessageSender_t204794706 * get_U3CU3Ef__this_17() const { return ___U3CU3Ef__this_17; }
	inline MessageSender_t204794706 ** get_address_of_U3CU3Ef__this_17() { return &___U3CU3Ef__this_17; }
	inline void set_U3CU3Ef__this_17(MessageSender_t204794706 * value)
	{
		___U3CU3Ef__this_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
