﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_LocalPlayerScoreUpdateListener
struct GP_LocalPlayerScoreUpdateListener_t158126391;
// System.String
struct String_t;
// GPScore
struct GPScore_t3219488889;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GPScore3219488889.h"

// System.Void GP_LocalPlayerScoreUpdateListener::.ctor(System.Int32,System.String)
extern "C"  void GP_LocalPlayerScoreUpdateListener__ctor_m3905821289 (GP_LocalPlayerScoreUpdateListener_t158126391 * __this, int32_t ___requestId0, String_t* ___leaderboardId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_LocalPlayerScoreUpdateListener::ReportScoreUpdate(GPScore)
extern "C"  void GP_LocalPlayerScoreUpdateListener_ReportScoreUpdate_m1497132088 (GP_LocalPlayerScoreUpdateListener_t158126391 * __this, GPScore_t3219488889 * ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_LocalPlayerScoreUpdateListener::ReportScoreUpdateFail(System.String)
extern "C"  void GP_LocalPlayerScoreUpdateListener_ReportScoreUpdateFail_m43220385 (GP_LocalPlayerScoreUpdateListener_t158126391 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GP_LocalPlayerScoreUpdateListener::get_RequestId()
extern "C"  int32_t GP_LocalPlayerScoreUpdateListener_get_RequestId_m3376821369 (GP_LocalPlayerScoreUpdateListener_t158126391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_LocalPlayerScoreUpdateListener::DispatchUpdate()
extern "C"  void GP_LocalPlayerScoreUpdateListener_DispatchUpdate_m1015657379 (GP_LocalPlayerScoreUpdateListener_t158126391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
