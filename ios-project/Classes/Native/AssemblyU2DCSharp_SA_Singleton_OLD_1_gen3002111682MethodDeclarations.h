﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3980293213MethodDeclarations.h"

// System.Void SA_Singleton_OLD`1<AndroidCamera>::.ctor()
#define SA_Singleton_OLD_1__ctor_m3466238046(__this, method) ((  void (*) (SA_Singleton_OLD_1_t3002111682 *, const MethodInfo*))SA_Singleton_OLD_1__ctor_m2643683696_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<AndroidCamera>::.cctor()
#define SA_Singleton_OLD_1__cctor_m3970388527(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1__cctor_m1355192463_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<AndroidCamera>::get_instance()
#define SA_Singleton_OLD_1_get_instance_m4186683123(__this /* static, unused */, method) ((  AndroidCamera_t1711267764 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_instance_m1963118739_gshared)(__this /* static, unused */, method)
// T SA_Singleton_OLD`1<AndroidCamera>::get_Instance()
#define SA_Singleton_OLD_1_get_Instance_m2243347347(__this /* static, unused */, method) ((  AndroidCamera_t1711267764 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_Instance_m2967800051_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<AndroidCamera>::get_HasInstance()
#define SA_Singleton_OLD_1_get_HasInstance_m2754883072(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_HasInstance_m4184706798_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton_OLD`1<AndroidCamera>::get_IsDestroyed()
#define SA_Singleton_OLD_1_get_IsDestroyed_m2920280660(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SA_Singleton_OLD_1_get_IsDestroyed_m1648274602_gshared)(__this /* static, unused */, method)
// System.Void SA_Singleton_OLD`1<AndroidCamera>::OnDestroy()
#define SA_Singleton_OLD_1_OnDestroy_m3302405227(__this, method) ((  void (*) (SA_Singleton_OLD_1_t3002111682 *, const MethodInfo*))SA_Singleton_OLD_1_OnDestroy_m1680414419_gshared)(__this, method)
// System.Void SA_Singleton_OLD`1<AndroidCamera>::OnApplicationQuit()
#define SA_Singleton_OLD_1_OnApplicationQuit_m1471786296(__this, method) ((  void (*) (SA_Singleton_OLD_1_t3002111682 *, const MethodInfo*))SA_Singleton_OLD_1_OnApplicationQuit_m1864960902_gshared)(__this, method)
