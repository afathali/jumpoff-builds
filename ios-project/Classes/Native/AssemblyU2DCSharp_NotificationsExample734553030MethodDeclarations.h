﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationsExample
struct NotificationsExample_t734553030;
// GP_GCM_RegistrationResult
struct GP_GCM_RegistrationResult_t2892492118;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GP_GCM_RegistrationResult2892492118.h"
#include "mscorlib_System_String2029220233.h"

// System.Void NotificationsExample::.ctor()
extern "C"  void NotificationsExample__ctor_m47977325 (NotificationsExample_t734553030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsExample::Awake()
extern "C"  void NotificationsExample_Awake_m3708936642 (NotificationsExample_t734553030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsExample::Toast()
extern "C"  void NotificationsExample_Toast_m452978566 (NotificationsExample_t734553030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsExample::Local()
extern "C"  void NotificationsExample_Local_m2993622754 (NotificationsExample_t734553030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsExample::LoadLaunchNotification()
extern "C"  void NotificationsExample_LoadLaunchNotification_m83343695 (NotificationsExample_t734553030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsExample::CanselLocal()
extern "C"  void NotificationsExample_CanselLocal_m3204316372 (NotificationsExample_t734553030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsExample::CancelAll()
extern "C"  void NotificationsExample_CancelAll_m114730128 (NotificationsExample_t734553030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsExample::Reg()
extern "C"  void NotificationsExample_Reg_m2703205701 (NotificationsExample_t734553030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsExample::LoadLastMessage()
extern "C"  void NotificationsExample_LoadLastMessage_m1877994642 (NotificationsExample_t734553030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsExample::LocalNitificationsListExample()
extern "C"  void NotificationsExample_LocalNitificationsListExample_m1699067266 (NotificationsExample_t734553030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsExample::HandleActionCMDRegistrationResult(GP_GCM_RegistrationResult)
extern "C"  void NotificationsExample_HandleActionCMDRegistrationResult_m178999301 (NotificationsExample_t734553030 * __this, GP_GCM_RegistrationResult_t2892492118 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsExample::OnNotificationIdLoaded(System.Int32)
extern "C"  void NotificationsExample_OnNotificationIdLoaded_m2155508914 (NotificationsExample_t734553030 * __this, int32_t ___notificationid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsExample::OnMessageLoaded(System.String)
extern "C"  void NotificationsExample_OnMessageLoaded_m2441395318 (NotificationsExample_t734553030 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
