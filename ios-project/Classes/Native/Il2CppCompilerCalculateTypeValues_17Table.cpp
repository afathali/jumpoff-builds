﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState455716270.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope3775842435.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope2583939667.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range3455291607.h"
#include "UnityEngine_UnityEngine_PropertyAttribute2606999759.h"
#include "UnityEngine_UnityEngine_TooltipAttribute4278647215.h"
#include "UnityEngine_UnityEngine_SpaceAttribute952253354.h"
#include "UnityEngine_UnityEngine_RangeAttribute3336560921.h"
#include "UnityEngine_UnityEngine_TextAreaAttribute2454598508.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute936505999.h"
#include "UnityEngine_UnityEngine_StackTraceUtility1881293839.h"
#include "UnityEngine_UnityEngine_UnityException2687879050.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1565472209.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour2151245329.h"
#include "UnityEngine_UnityEngine_TrackedReference1045890189.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo857969000.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache4810721.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall2183506063.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3420894182.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3793436469.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup339478082.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2295673753.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime2105307154.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1808633952.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram1120735295.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1027170048.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAtt665825653.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules1810425448.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1390152093.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel3613839672.h"
#include "UnityEngine_UnityEngine_AndroidJavaRunnable3501776228.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "Apple_U3CModuleU3E3783534214.h"
#include "Apple_UnityEngine_Purchasing_UnityPurchasingCallba2635187846.h"
#include "Apple_UnityEngine_Purchasing_iOSStoreBindings2633471826.h"
#include "Apple_UnityEngine_Purchasing_OSXStoreBindings116576999.h"
#include "Tizen_U3CModuleU3E3783534214.h"
#include "Tizen_UnityEngine_Purchasing_UnityNativePurchasing3230812225.h"
#include "Tizen_UnityEngine_Purchasing_TizenStoreBindings1951392817.h"
#include "UnityEngine_Purchasing_U3CModuleU3E3783534214.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Anal3513180421.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Async423752048.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Asyn2099263868.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Asyn3541402849.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Clou3988464631.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Clou3565118814.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_IDs3808979560.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Conf1298400415.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod3600019299.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1573242544.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod2754455291.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte1607114611.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc2407199463.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc3525211160.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch728606867.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Stor2597962341.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Transa45391254.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (UserState_t455716270)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1700[6] = 
{
	UserState_t455716270::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (UserScope_t3775842435)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1705[3] = 
{
	UserScope_t3775842435::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (TimeScope_t2583939667)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1706[4] = 
{
	TimeScope_t2583939667::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (Range_t3455291607)+ sizeof (Il2CppObject), sizeof(Range_t3455291607_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1707[2] = 
{
	Range_t3455291607::get_offset_of_from_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Range_t3455291607::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (PropertyAttribute_t2606999759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (TooltipAttribute_t4278647215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1710[1] = 
{
	TooltipAttribute_t4278647215::get_offset_of_tooltip_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (SpaceAttribute_t952253354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[1] = 
{
	SpaceAttribute_t952253354::get_offset_of_height_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (RangeAttribute_t3336560921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1712[2] = 
{
	RangeAttribute_t3336560921::get_offset_of_min_0(),
	RangeAttribute_t3336560921::get_offset_of_max_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (TextAreaAttribute_t2454598508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1713[2] = 
{
	TextAreaAttribute_t2454598508::get_offset_of_minLines_0(),
	TextAreaAttribute_t2454598508::get_offset_of_maxLines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (SelectionBaseAttribute_t936505999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (StackTraceUtility_t1881293839), -1, sizeof(StackTraceUtility_t1881293839_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1715[1] = 
{
	StackTraceUtility_t1881293839_StaticFields::get_offset_of_projectFolder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (UnityException_t2687879050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[2] = 
{
	0,
	UnityException_t2687879050::get_offset_of_unityStackTrace_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (SharedBetweenAnimatorsAttribute_t1565472209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (StateMachineBehaviour_t2151245329), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (TrackedReference_t1045890189), sizeof(TrackedReference_t1045890189_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1719[1] = 
{
	TrackedReference_t1045890189::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (PersistentListenerMode_t857969000)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1720[8] = 
{
	PersistentListenerMode_t857969000::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (ArgumentCache_t4810721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[6] = 
{
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgument_0(),
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgumentAssemblyTypeName_1(),
	ArgumentCache_t4810721::get_offset_of_m_IntArgument_2(),
	ArgumentCache_t4810721::get_offset_of_m_FloatArgument_3(),
	ArgumentCache_t4810721::get_offset_of_m_StringArgument_4(),
	ArgumentCache_t4810721::get_offset_of_m_BoolArgument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (BaseInvokableCall_t2229564840), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (InvokableCall_t2183506063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[1] = 
{
	InvokableCall_t2183506063::get_offset_of_Delegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1724[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1726[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (UnityEventCallState_t3420894182)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1729[4] = 
{
	UnityEventCallState_t3420894182::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (PersistentCall_t3793436469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[5] = 
{
	PersistentCall_t3793436469::get_offset_of_m_Target_0(),
	PersistentCall_t3793436469::get_offset_of_m_MethodName_1(),
	PersistentCall_t3793436469::get_offset_of_m_Mode_2(),
	PersistentCall_t3793436469::get_offset_of_m_Arguments_3(),
	PersistentCall_t3793436469::get_offset_of_m_CallState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (PersistentCallGroup_t339478082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1731[1] = 
{
	PersistentCallGroup_t339478082::get_offset_of_m_Calls_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (InvokableCallList_t2295673753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[4] = 
{
	InvokableCallList_t2295673753::get_offset_of_m_PersistentCalls_0(),
	InvokableCallList_t2295673753::get_offset_of_m_RuntimeCalls_1(),
	InvokableCallList_t2295673753::get_offset_of_m_ExecutingCalls_2(),
	InvokableCallList_t2295673753::get_offset_of_m_NeedsUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (UnityEventBase_t828812576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[4] = 
{
	UnityEventBase_t828812576::get_offset_of_m_Calls_0(),
	UnityEventBase_t828812576::get_offset_of_m_PersistentCalls_1(),
	UnityEventBase_t828812576::get_offset_of_m_TypeName_2(),
	UnityEventBase_t828812576::get_offset_of_m_CallsDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (UnityEvent_t408735097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1734[1] = 
{
	UnityEvent_t408735097::get_offset_of_m_InvokeArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1738[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (WaitForSecondsRealtime_t2105307154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1739[1] = 
{
	WaitForSecondsRealtime_t2105307154::get_offset_of_waitTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (AnimationPlayableUtilities_t1808633952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (FrameData_t1120735295)+ sizeof (Il2CppObject), sizeof(FrameData_t1120735295_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1741[4] = 
{
	FrameData_t1120735295::get_offset_of_m_UpdateId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Time_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_LastTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_TimeScale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (DefaultValueAttribute_t1027170048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1742[1] = 
{
	DefaultValueAttribute_t1027170048::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (ExcludeFromDocsAttribute_t665825653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (Logger_t3328995178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1746[3] = 
{
	Logger_t3328995178::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t3328995178::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t3328995178::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (UsedByNativeCodeAttribute_t3212052468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (RequiredByNativeCodeAttribute_t1913052472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (FormerlySerializedAsAttribute_t3673080018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1749[1] = 
{
	FormerlySerializedAsAttribute_t3673080018::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (TypeInferenceRules_t1810425448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1750[5] = 
{
	TypeInferenceRules_t1810425448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (TypeInferenceRuleAttribute_t1390152093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1751[1] = 
{
	TypeInferenceRuleAttribute_t1390152093::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (UnityAdsDelegate_t3613839672), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (AndroidJavaRunnable_t3501776228), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (UnityAction_t4025899511), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (UnityPurchasingCallback_t2635187846), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (iOSStoreBindings_t2633471826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (OSXStoreBindings_t116576999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (UnityNativePurchasingCallback_t3230812225), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (TizenStoreBindings_t1951392817), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (AnalyticsReporter_t3513180421), -1, sizeof(AnalyticsReporter_t3513180421_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1772[2] = 
{
	AnalyticsReporter_t3513180421::get_offset_of_m_Analytics_0(),
	AnalyticsReporter_t3513180421_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (AsyncUtil_t423752048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (U3CDoInvokeU3Ec__Iterator0_t2099263868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[6] = 
{
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_delayInSeconds_0(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_a_1(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U24PC_2(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U24current_3(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U3CU24U3EdelayInSeconds_4(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U3CU24U3Ea_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (U3CProcessU3Ec__Iterator1_t3541402849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[8] = 
{
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_request_0(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_errorHandler_1(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_responseHandler_2(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U24PC_3(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U24current_4(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U3CU24U3Erequest_5(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U3CU24U3EerrorHandler_6(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U3CU24U3EresponseHandler_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (CloudCatalogManager_t3988464631), -1, sizeof(CloudCatalogManager_t3988464631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1776[7] = 
{
	CloudCatalogManager_t3988464631::get_offset_of_m_AsyncUtil_0(),
	CloudCatalogManager_t3988464631::get_offset_of_m_CacheFileName_1(),
	CloudCatalogManager_t3988464631::get_offset_of_m_Logger_2(),
	CloudCatalogManager_t3988464631::get_offset_of_m_CatalogURL_3(),
	CloudCatalogManager_t3988464631::get_offset_of_m_StoreName_4(),
	CloudCatalogManager_t3988464631_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	CloudCatalogManager_t3988464631_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (U3CFetchProductsU3Ec__AnonStorey2_t3565118814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1777[3] = 
{
	U3CFetchProductsU3Ec__AnonStorey2_t3565118814::get_offset_of_callback_0(),
	U3CFetchProductsU3Ec__AnonStorey2_t3565118814::get_offset_of_delayInSeconds_1(),
	U3CFetchProductsU3Ec__AnonStorey2_t3565118814::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (IDs_t3808979560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1778[1] = 
{
	IDs_t3808979560::get_offset_of_m_Dic_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (ConfigurationBuilder_t1298400415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1779[3] = 
{
	ConfigurationBuilder_t1298400415::get_offset_of_m_Factory_0(),
	ConfigurationBuilder_t1298400415::get_offset_of_m_Products_1(),
	ConfigurationBuilder_t1298400415::get_offset_of_U3CuseCloudCatalogU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (InitializationFailureReason_t2954032642)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1783[4] = 
{
	InitializationFailureReason_t2954032642::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (Product_t1203687971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1787[5] = 
{
	Product_t1203687971::get_offset_of_U3CdefinitionU3Ek__BackingField_0(),
	Product_t1203687971::get_offset_of_U3CmetadataU3Ek__BackingField_1(),
	Product_t1203687971::get_offset_of_U3CavailableToPurchaseU3Ek__BackingField_2(),
	Product_t1203687971::get_offset_of_U3CtransactionIDU3Ek__BackingField_3(),
	Product_t1203687971::get_offset_of_U3CreceiptU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (ProductCollection_t3600019299), -1, sizeof(ProductCollection_t3600019299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1788[6] = 
{
	ProductCollection_t3600019299::get_offset_of_m_IdToProduct_0(),
	ProductCollection_t3600019299::get_offset_of_m_StoreSpecificIdToProduct_1(),
	ProductCollection_t3600019299::get_offset_of_m_Products_2(),
	ProductCollection_t3600019299::get_offset_of_m_ProductSet_3(),
	ProductCollection_t3600019299_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	ProductCollection_t3600019299_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (ProductDefinition_t1942475268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1789[3] = 
{
	ProductDefinition_t1942475268::get_offset_of_U3CidU3Ek__BackingField_0(),
	ProductDefinition_t1942475268::get_offset_of_U3CstoreSpecificIdU3Ek__BackingField_1(),
	ProductDefinition_t1942475268::get_offset_of_U3CtypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (ProductMetadata_t1573242544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1790[5] = 
{
	ProductMetadata_t1573242544::get_offset_of_U3ClocalizedPriceStringU3Ek__BackingField_0(),
	ProductMetadata_t1573242544::get_offset_of_U3ClocalizedTitleU3Ek__BackingField_1(),
	ProductMetadata_t1573242544::get_offset_of_U3ClocalizedDescriptionU3Ek__BackingField_2(),
	ProductMetadata_t1573242544::get_offset_of_U3CisoCurrencyCodeU3Ek__BackingField_3(),
	ProductMetadata_t1573242544::get_offset_of_U3ClocalizedPriceU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (ProductType_t2754455291)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1791[4] = 
{
	ProductType_t2754455291::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (PurchaseEventArgs_t547992434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1792[1] = 
{
	PurchaseEventArgs_t547992434::get_offset_of_U3CpurchasedProductU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (PurchaseFailureDescription_t1607114611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1793[3] = 
{
	PurchaseFailureDescription_t1607114611::get_offset_of_U3CproductIdU3Ek__BackingField_0(),
	PurchaseFailureDescription_t1607114611::get_offset_of_U3CreasonU3Ek__BackingField_1(),
	PurchaseFailureDescription_t1607114611::get_offset_of_U3CmessageU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (PurchaseFailureReason_t1322959839)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1794[8] = 
{
	PurchaseFailureReason_t1322959839::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (PurchaseProcessingResult_t2407199463)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1795[3] = 
{
	PurchaseProcessingResult_t2407199463::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (PurchasingFactory_t3525211160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[4] = 
{
	PurchasingFactory_t3525211160::get_offset_of_m_ConfigMap_0(),
	PurchasingFactory_t3525211160::get_offset_of_m_ExtensionMap_1(),
	PurchasingFactory_t3525211160::get_offset_of_m_Store_2(),
	PurchasingFactory_t3525211160::get_offset_of_U3CstoreNameU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (PurchasingManager_t728606867), -1, sizeof(PurchasingManager_t728606867_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1797[11] = 
{
	PurchasingManager_t728606867::get_offset_of_m_Store_0(),
	PurchasingManager_t728606867::get_offset_of_m_Listener_1(),
	PurchasingManager_t728606867::get_offset_of_m_Logger_2(),
	PurchasingManager_t728606867::get_offset_of_m_TransactionLog_3(),
	PurchasingManager_t728606867::get_offset_of_m_StoreName_4(),
	PurchasingManager_t728606867::get_offset_of_m_AdditionalProductsCallback_5(),
	PurchasingManager_t728606867::get_offset_of_m_AdditionalProductsFailCallback_6(),
	PurchasingManager_t728606867::get_offset_of_initialized_7(),
	PurchasingManager_t728606867::get_offset_of_U3CuseTransactionLogU3Ek__BackingField_8(),
	PurchasingManager_t728606867::get_offset_of_U3CproductsU3Ek__BackingField_9(),
	PurchasingManager_t728606867_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (StoreListenerProxy_t2597962341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[3] = 
{
	StoreListenerProxy_t2597962341::get_offset_of_m_Analytics_0(),
	StoreListenerProxy_t2597962341::get_offset_of_m_ForwardTo_1(),
	StoreListenerProxy_t2597962341::get_offset_of_m_Extensions_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (TransactionLog_t45391254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[2] = 
{
	TransactionLog_t45391254::get_offset_of_logger_0(),
	TransactionLog_t45391254::get_offset_of_persistentDataPath_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
