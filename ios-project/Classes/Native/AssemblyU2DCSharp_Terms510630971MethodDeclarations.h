﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Terms
struct Terms_t510630971;

#include "codegen/il2cpp-codegen.h"

// System.Void Terms::.ctor()
extern "C"  void Terms__ctor_m3779901114 (Terms_t510630971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Terms::Start()
extern "C"  void Terms_Start_m3026716886 (Terms_t510630971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Terms::Update()
extern "C"  void Terms_Update_m676495999 (Terms_t510630971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Terms::<Start>m__15A()
extern "C"  void Terms_U3CStartU3Em__15A_m1971250726 (Terms_t510630971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Terms::<Start>m__15B()
extern "C"  void Terms_U3CStartU3Em__15B_m1547763223 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
