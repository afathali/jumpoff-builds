﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Action
struct Action_t3226471752;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen632286592.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwitterApplicationOnlyToken
struct  TwitterApplicationOnlyToken_t3636409970  : public SA_Singleton_OLD_1_t632286592
{
public:
	// System.String TwitterApplicationOnlyToken::_currentToken
	String_t* ____currentToken_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> TwitterApplicationOnlyToken::Headers
	Dictionary_2_t3943999495 * ___Headers_8;
	// System.Action TwitterApplicationOnlyToken::ActionComplete
	Action_t3226471752 * ___ActionComplete_9;

public:
	inline static int32_t get_offset_of__currentToken_7() { return static_cast<int32_t>(offsetof(TwitterApplicationOnlyToken_t3636409970, ____currentToken_7)); }
	inline String_t* get__currentToken_7() const { return ____currentToken_7; }
	inline String_t** get_address_of__currentToken_7() { return &____currentToken_7; }
	inline void set__currentToken_7(String_t* value)
	{
		____currentToken_7 = value;
		Il2CppCodeGenWriteBarrier(&____currentToken_7, value);
	}

	inline static int32_t get_offset_of_Headers_8() { return static_cast<int32_t>(offsetof(TwitterApplicationOnlyToken_t3636409970, ___Headers_8)); }
	inline Dictionary_2_t3943999495 * get_Headers_8() const { return ___Headers_8; }
	inline Dictionary_2_t3943999495 ** get_address_of_Headers_8() { return &___Headers_8; }
	inline void set_Headers_8(Dictionary_2_t3943999495 * value)
	{
		___Headers_8 = value;
		Il2CppCodeGenWriteBarrier(&___Headers_8, value);
	}

	inline static int32_t get_offset_of_ActionComplete_9() { return static_cast<int32_t>(offsetof(TwitterApplicationOnlyToken_t3636409970, ___ActionComplete_9)); }
	inline Action_t3226471752 * get_ActionComplete_9() const { return ___ActionComplete_9; }
	inline Action_t3226471752 ** get_address_of_ActionComplete_9() { return &___ActionComplete_9; }
	inline void set_ActionComplete_9(Action_t3226471752 * value)
	{
		___ActionComplete_9 = value;
		Il2CppCodeGenWriteBarrier(&___ActionComplete_9, value);
	}
};

struct TwitterApplicationOnlyToken_t3636409970_StaticFields
{
public:
	// System.Action TwitterApplicationOnlyToken::<>f__am$cache3
	Action_t3226471752 * ___U3CU3Ef__amU24cache3_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_10() { return static_cast<int32_t>(offsetof(TwitterApplicationOnlyToken_t3636409970_StaticFields, ___U3CU3Ef__amU24cache3_10)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache3_10() const { return ___U3CU3Ef__amU24cache3_10; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache3_10() { return &___U3CU3Ef__amU24cache3_10; }
	inline void set_U3CU3Ef__amU24cache3_10(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache3_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
