﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidNotificationBuilder
struct AndroidNotificationBuilder_t2822362133;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void AndroidNotificationBuilder::.ctor(System.Int32,System.String,System.String,System.Int32)
extern "C"  void AndroidNotificationBuilder__ctor_m698457602 (AndroidNotificationBuilder_t2822362133 * __this, int32_t ___id0, String_t* ___title1, String_t* ___message2, int32_t ___time3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationBuilder::SetSoundName(System.String)
extern "C"  void AndroidNotificationBuilder_SetSoundName_m4065327470 (AndroidNotificationBuilder_t2822362133 * __this, String_t* ___sound0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationBuilder::SetIconName(System.String)
extern "C"  void AndroidNotificationBuilder_SetIconName_m1164798150 (AndroidNotificationBuilder_t2822362133 * __this, String_t* ___icon0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationBuilder::SetVibration(System.Boolean)
extern "C"  void AndroidNotificationBuilder_SetVibration_m463552787 (AndroidNotificationBuilder_t2822362133 * __this, bool ___vibration0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationBuilder::SetSilentNotification()
extern "C"  void AndroidNotificationBuilder_SetSilentNotification_m1262231488 (AndroidNotificationBuilder_t2822362133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationBuilder::ShowIfAppIsForeground(System.Boolean)
extern "C"  void AndroidNotificationBuilder_ShowIfAppIsForeground_m4221726763 (AndroidNotificationBuilder_t2822362133 * __this, bool ___show0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationBuilder::SetLargeIcon(System.String)
extern "C"  void AndroidNotificationBuilder_SetLargeIcon_m932659676 (AndroidNotificationBuilder_t2822362133 * __this, String_t* ___icon0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNotificationBuilder::SetBigPicture(UnityEngine.Texture2D)
extern "C"  void AndroidNotificationBuilder_SetBigPicture_m3337607212 (AndroidNotificationBuilder_t2822362133 * __this, Texture2D_t3542995729 * ___picture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AndroidNotificationBuilder::get_Id()
extern "C"  int32_t AndroidNotificationBuilder_get_Id_m3173393014 (AndroidNotificationBuilder_t2822362133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidNotificationBuilder::get_Title()
extern "C"  String_t* AndroidNotificationBuilder_get_Title_m113936712 (AndroidNotificationBuilder_t2822362133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidNotificationBuilder::get_Message()
extern "C"  String_t* AndroidNotificationBuilder_get_Message_m683436495 (AndroidNotificationBuilder_t2822362133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AndroidNotificationBuilder::get_Time()
extern "C"  int32_t AndroidNotificationBuilder_get_Time_m2367133518 (AndroidNotificationBuilder_t2822362133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidNotificationBuilder::get_Sound()
extern "C"  String_t* AndroidNotificationBuilder_get_Sound_m233741911 (AndroidNotificationBuilder_t2822362133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidNotificationBuilder::get_Icon()
extern "C"  String_t* AndroidNotificationBuilder_get_Icon_m3779376371 (AndroidNotificationBuilder_t2822362133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidNotificationBuilder::get_Vibration()
extern "C"  bool AndroidNotificationBuilder_get_Vibration_m58369643 (AndroidNotificationBuilder_t2822362133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidNotificationBuilder::get_ShowIfAppForeground()
extern "C"  bool AndroidNotificationBuilder_get_ShowIfAppForeground_m3018763667 (AndroidNotificationBuilder_t2822362133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidNotificationBuilder::get_LargeIcon()
extern "C"  String_t* AndroidNotificationBuilder_get_LargeIcon_m1988324432 (AndroidNotificationBuilder_t2822362133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D AndroidNotificationBuilder::get_BigPicture()
extern "C"  Texture2D_t3542995729 * AndroidNotificationBuilder_get_BigPicture_m1405645520 (AndroidNotificationBuilder_t2822362133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
