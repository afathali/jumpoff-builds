﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.CollectionUtils/<CreateCollectionWrapper>c__AnonStorey26
struct U3CCreateCollectionWrapperU3Ec__AnonStorey26_t2931878223;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void Newtonsoft.Json.Utilities.CollectionUtils/<CreateCollectionWrapper>c__AnonStorey26::.ctor()
extern "C"  void U3CCreateCollectionWrapperU3Ec__AnonStorey26__ctor_m447955416 (U3CCreateCollectionWrapperU3Ec__AnonStorey26_t2931878223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.CollectionUtils/<CreateCollectionWrapper>c__AnonStorey26::<>m__E8(System.Type,System.Collections.Generic.IList`1<System.Object>)
extern "C"  Il2CppObject * U3CCreateCollectionWrapperU3Ec__AnonStorey26_U3CU3Em__E8_m1691820323 (U3CCreateCollectionWrapperU3Ec__AnonStorey26_t2931878223 * __this, Type_t * ___t0, Il2CppObject* ___a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
