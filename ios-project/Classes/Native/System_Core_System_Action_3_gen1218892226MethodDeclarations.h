﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen2251782606MethodDeclarations.h"

// System.Void System.Action`3<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m2239441425(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t1218892226 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m218933466_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m50035455(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t1218892226 *, String_t*, Dictionary_2_t309261261 *, bool, const MethodInfo*))Action_3_Invoke_m3194942528_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m68783142(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t1218892226 *, String_t*, Dictionary_2_t309261261 *, bool, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m2167027095_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m2275788531(__this, ___result0, method) ((  void (*) (Action_3_t1218892226 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m485955472_gshared)(__this, ___result0, method)
