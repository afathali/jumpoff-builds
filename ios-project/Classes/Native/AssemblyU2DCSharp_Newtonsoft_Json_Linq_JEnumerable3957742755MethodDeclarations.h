﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IJEnumerable_1_t2511655056;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JEnumerable3957742755.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void JEnumerable_1__ctor_m1860534610_gshared (JEnumerable_1_t3957742755 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define JEnumerable_1__ctor_m1860534610(__this, ___enumerable0, method) ((  void (*) (JEnumerable_1_t3957742755 *, Il2CppObject*, const MethodInfo*))JEnumerable_1__ctor_m1860534610_gshared)(__this, ___enumerable0, method)
// System.Void Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::.cctor()
extern "C"  void JEnumerable_1__cctor_m3633367596_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define JEnumerable_1__cctor_m3633367596(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))JEnumerable_1__cctor_m3633367596_gshared)(__this /* static, unused */, method)
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m2922461236_gshared (JEnumerable_1_t3957742755 * __this, const MethodInfo* method);
#define JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m2922461236(__this, method) ((  Il2CppObject * (*) (JEnumerable_1_t3957742755 *, const MethodInfo*))JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m2922461236_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* JEnumerable_1_GetEnumerator_m1984457945_gshared (JEnumerable_1_t3957742755 * __this, const MethodInfo* method);
#define JEnumerable_1_GetEnumerator_m1984457945(__this, method) ((  Il2CppObject* (*) (JEnumerable_1_t3957742755 *, const MethodInfo*))JEnumerable_1_GetEnumerator_m1984457945_gshared)(__this, method)
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::get_Item(System.Object)
extern "C"  Il2CppObject* JEnumerable_1_get_Item_m2121380803_gshared (JEnumerable_1_t3957742755 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define JEnumerable_1_get_Item_m2121380803(__this, ___key0, method) ((  Il2CppObject* (*) (JEnumerable_1_t3957742755 *, Il2CppObject *, const MethodInfo*))JEnumerable_1_get_Item_m2121380803_gshared)(__this, ___key0, method)
// System.Boolean Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::Equals(System.Object)
extern "C"  bool JEnumerable_1_Equals_m1085474098_gshared (JEnumerable_1_t3957742755 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define JEnumerable_1_Equals_m1085474098(__this, ___obj0, method) ((  bool (*) (JEnumerable_1_t3957742755 *, Il2CppObject *, const MethodInfo*))JEnumerable_1_Equals_m1085474098_gshared)(__this, ___obj0, method)
// System.Int32 Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::GetHashCode()
extern "C"  int32_t JEnumerable_1_GetHashCode_m1140639406_gshared (JEnumerable_1_t3957742755 * __this, const MethodInfo* method);
#define JEnumerable_1_GetHashCode_m1140639406(__this, method) ((  int32_t (*) (JEnumerable_1_t3957742755 *, const MethodInfo*))JEnumerable_1_GetHashCode_m1140639406_gshared)(__this, method)
