﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.D1
struct D1_t3662249053;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Newtonsoft.Json.Utilities.D1::.ctor(System.Object,System.IntPtr)
extern "C"  void D1__ctor_m1462841277 (D1_t3662249053 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.D1::Invoke()
extern "C"  Il2CppObject * D1_Invoke_m3013597384 (D1_t3662249053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Newtonsoft.Json.Utilities.D1::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * D1_BeginInvoke_m491600026 (D1_t3662249053 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.D1::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * D1_EndInvoke_m1592310992 (D1_t3662249053 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
