﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.UnityAdsInternal
struct UnityAdsInternal_t2792527316;
// UnityEngine.Advertisements.UnityAdsDelegate
struct UnityAdsDelegate_t3613839672;
// UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>
struct UnityAdsDelegate_2_t1684806294;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel3613839672.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Advertisements.UnityAdsInternal::.ctor()
extern "C"  void UnityAdsInternal__ctor_m212446971 (UnityAdsInternal_t2792527316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onCampaignsAvailable(UnityEngine.Advertisements.UnityAdsDelegate)
extern "C"  void UnityAdsInternal_add_onCampaignsAvailable_m1879828001 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onCampaignsAvailable(UnityEngine.Advertisements.UnityAdsDelegate)
extern "C"  void UnityAdsInternal_remove_onCampaignsAvailable_m4140418646 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onCampaignsFetchFailed(UnityEngine.Advertisements.UnityAdsDelegate)
extern "C"  void UnityAdsInternal_add_onCampaignsFetchFailed_m1707903351 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onCampaignsFetchFailed(UnityEngine.Advertisements.UnityAdsDelegate)
extern "C"  void UnityAdsInternal_remove_onCampaignsFetchFailed_m3632462420 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onShow(UnityEngine.Advertisements.UnityAdsDelegate)
extern "C"  void UnityAdsInternal_add_onShow_m4085422052 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onShow(UnityEngine.Advertisements.UnityAdsDelegate)
extern "C"  void UnityAdsInternal_remove_onShow_m2025989273 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onHide(UnityEngine.Advertisements.UnityAdsDelegate)
extern "C"  void UnityAdsInternal_add_onHide_m1355080865 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onHide(UnityEngine.Advertisements.UnityAdsDelegate)
extern "C"  void UnityAdsInternal_remove_onHide_m3602445128 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onVideoCompleted(UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>)
extern "C"  void UnityAdsInternal_add_onVideoCompleted_m114092648 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_2_t1684806294 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onVideoCompleted(UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>)
extern "C"  void UnityAdsInternal_remove_onVideoCompleted_m1708120711 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_2_t1684806294 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onVideoStarted(UnityEngine.Advertisements.UnityAdsDelegate)
extern "C"  void UnityAdsInternal_add_onVideoStarted_m1352216253 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onVideoStarted(UnityEngine.Advertisements.UnityAdsDelegate)
extern "C"  void UnityAdsInternal_remove_onVideoStarted_m251164176 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::RemoveAllEventHandlers()
extern "C"  void UnityAdsInternal_RemoveAllEventHandlers_m1166353867 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsCampaignsAvailable()
extern "C"  void UnityAdsInternal_CallUnityAdsCampaignsAvailable_m743841450 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsCampaignsFetchFailed()
extern "C"  void UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m2641284592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsShow()
extern "C"  void UnityAdsInternal_CallUnityAdsShow_m3749200707 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsHide()
extern "C"  void UnityAdsInternal_CallUnityAdsHide_m1451410048 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsVideoCompleted(System.String,System.Boolean)
extern "C"  void UnityAdsInternal_CallUnityAdsVideoCompleted_m2685251105 (Il2CppObject * __this /* static, unused */, String_t* ___rewardItemKey0, bool ___skipped1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsVideoStarted()
extern "C"  void UnityAdsInternal_CallUnityAdsVideoStarted_m882181564 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::RegisterNative()
extern "C"  void UnityAdsInternal_RegisterNative_m2502472975 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::Init(System.String,System.Boolean,System.Boolean,System.String)
extern "C"  void UnityAdsInternal_Init_m600965261 (Il2CppObject * __this /* static, unused */, String_t* ___gameId0, bool ___testModeEnabled1, bool ___debugModeEnabled2, String_t* ___unityVersion3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsInternal::Show(System.String,System.String,System.String)
extern "C"  bool UnityAdsInternal_Show_m3790165766 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId0, String_t* ___rewardItemKey1, String_t* ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsInternal::CanShowAds(System.String)
extern "C"  bool UnityAdsInternal_CanShowAds_m3383840268 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::SetLogLevel(System.Int32)
extern "C"  void UnityAdsInternal_SetLogLevel_m1327512352 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsInternal::SetCampaignDataURL(System.String)
extern "C"  void UnityAdsInternal_SetCampaignDataURL_m1966380984 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
