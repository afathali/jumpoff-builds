﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Details_Controller
struct Details_Controller_t3848679003;
// ShowDetailsEventArgs
struct ShowDetailsEventArgs_t1623880762;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ShowDetailsEventArgs1623880762.h"

// System.Void Details_Controller::.ctor()
extern "C"  void Details_Controller__ctor_m1007001286 (Details_Controller_t3848679003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Details_Controller::Start()
extern "C"  void Details_Controller_Start_m3927215450 (Details_Controller_t3848679003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Details_Controller::AwakeOverride()
extern "C"  void Details_Controller_AwakeOverride_m1473967167 (Details_Controller_t3848679003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Details_Controller::DestroyOverride()
extern "C"  void Details_Controller_DestroyOverride_m373839552 (Details_Controller_t3848679003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Details_Controller::HandleShowDetailsEventArgs(ShowDetailsEventArgs)
extern "C"  void Details_Controller_HandleShowDetailsEventArgs_m2075162034 (Details_Controller_t3848679003 * __this, ShowDetailsEventArgs_t1623880762 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Details_Controller::Update()
extern "C"  void Details_Controller_Update_m2852110567 (Details_Controller_t3848679003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Details_Controller::<Start>m__133()
extern "C"  void Details_Controller_U3CStartU3Em__133_m902241390 (Details_Controller_t3848679003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
