﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<SA.IOSDeploy.Framework>
struct List_1_t3392069394;
// System.Collections.Generic.List`1<SA.IOSDeploy.Lib>
struct List_1_t4024472637;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<SA.IOSDeploy.Variable>
struct List_1_t526886178;
// SA.IOSDeploy.ISD_Settings
struct ISD_Settings_t1116242554;

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.IOSDeploy.ISD_Settings
struct  ISD_Settings_t1116242554  : public ScriptableObject_t1975622470
{
public:
	// System.Boolean SA.IOSDeploy.ISD_Settings::IsfwSettingOpen
	bool ___IsfwSettingOpen_5;
	// System.Boolean SA.IOSDeploy.ISD_Settings::IsLibSettingOpen
	bool ___IsLibSettingOpen_6;
	// System.Boolean SA.IOSDeploy.ISD_Settings::IslinkerSettingOpne
	bool ___IslinkerSettingOpne_7;
	// System.Boolean SA.IOSDeploy.ISD_Settings::IscompilerSettingsOpen
	bool ___IscompilerSettingsOpen_8;
	// System.Boolean SA.IOSDeploy.ISD_Settings::IsPlistSettingsOpen
	bool ___IsPlistSettingsOpen_9;
	// System.Boolean SA.IOSDeploy.ISD_Settings::IsLanguageSettingOpen
	bool ___IsLanguageSettingOpen_10;
	// System.Collections.Generic.List`1<SA.IOSDeploy.Framework> SA.IOSDeploy.ISD_Settings::Frameworks
	List_1_t3392069394 * ___Frameworks_11;
	// System.Collections.Generic.List`1<SA.IOSDeploy.Lib> SA.IOSDeploy.ISD_Settings::Libraries
	List_1_t4024472637 * ___Libraries_12;
	// System.Collections.Generic.List`1<System.String> SA.IOSDeploy.ISD_Settings::compileFlags
	List_1_t1398341365 * ___compileFlags_13;
	// System.Collections.Generic.List`1<System.String> SA.IOSDeploy.ISD_Settings::linkFlags
	List_1_t1398341365 * ___linkFlags_14;
	// System.Collections.Generic.List`1<SA.IOSDeploy.Variable> SA.IOSDeploy.ISD_Settings::PlistVariables
	List_1_t526886178 * ___PlistVariables_15;
	// System.Collections.Generic.List`1<System.String> SA.IOSDeploy.ISD_Settings::langFolders
	List_1_t1398341365 * ___langFolders_16;

public:
	inline static int32_t get_offset_of_IsfwSettingOpen_5() { return static_cast<int32_t>(offsetof(ISD_Settings_t1116242554, ___IsfwSettingOpen_5)); }
	inline bool get_IsfwSettingOpen_5() const { return ___IsfwSettingOpen_5; }
	inline bool* get_address_of_IsfwSettingOpen_5() { return &___IsfwSettingOpen_5; }
	inline void set_IsfwSettingOpen_5(bool value)
	{
		___IsfwSettingOpen_5 = value;
	}

	inline static int32_t get_offset_of_IsLibSettingOpen_6() { return static_cast<int32_t>(offsetof(ISD_Settings_t1116242554, ___IsLibSettingOpen_6)); }
	inline bool get_IsLibSettingOpen_6() const { return ___IsLibSettingOpen_6; }
	inline bool* get_address_of_IsLibSettingOpen_6() { return &___IsLibSettingOpen_6; }
	inline void set_IsLibSettingOpen_6(bool value)
	{
		___IsLibSettingOpen_6 = value;
	}

	inline static int32_t get_offset_of_IslinkerSettingOpne_7() { return static_cast<int32_t>(offsetof(ISD_Settings_t1116242554, ___IslinkerSettingOpne_7)); }
	inline bool get_IslinkerSettingOpne_7() const { return ___IslinkerSettingOpne_7; }
	inline bool* get_address_of_IslinkerSettingOpne_7() { return &___IslinkerSettingOpne_7; }
	inline void set_IslinkerSettingOpne_7(bool value)
	{
		___IslinkerSettingOpne_7 = value;
	}

	inline static int32_t get_offset_of_IscompilerSettingsOpen_8() { return static_cast<int32_t>(offsetof(ISD_Settings_t1116242554, ___IscompilerSettingsOpen_8)); }
	inline bool get_IscompilerSettingsOpen_8() const { return ___IscompilerSettingsOpen_8; }
	inline bool* get_address_of_IscompilerSettingsOpen_8() { return &___IscompilerSettingsOpen_8; }
	inline void set_IscompilerSettingsOpen_8(bool value)
	{
		___IscompilerSettingsOpen_8 = value;
	}

	inline static int32_t get_offset_of_IsPlistSettingsOpen_9() { return static_cast<int32_t>(offsetof(ISD_Settings_t1116242554, ___IsPlistSettingsOpen_9)); }
	inline bool get_IsPlistSettingsOpen_9() const { return ___IsPlistSettingsOpen_9; }
	inline bool* get_address_of_IsPlistSettingsOpen_9() { return &___IsPlistSettingsOpen_9; }
	inline void set_IsPlistSettingsOpen_9(bool value)
	{
		___IsPlistSettingsOpen_9 = value;
	}

	inline static int32_t get_offset_of_IsLanguageSettingOpen_10() { return static_cast<int32_t>(offsetof(ISD_Settings_t1116242554, ___IsLanguageSettingOpen_10)); }
	inline bool get_IsLanguageSettingOpen_10() const { return ___IsLanguageSettingOpen_10; }
	inline bool* get_address_of_IsLanguageSettingOpen_10() { return &___IsLanguageSettingOpen_10; }
	inline void set_IsLanguageSettingOpen_10(bool value)
	{
		___IsLanguageSettingOpen_10 = value;
	}

	inline static int32_t get_offset_of_Frameworks_11() { return static_cast<int32_t>(offsetof(ISD_Settings_t1116242554, ___Frameworks_11)); }
	inline List_1_t3392069394 * get_Frameworks_11() const { return ___Frameworks_11; }
	inline List_1_t3392069394 ** get_address_of_Frameworks_11() { return &___Frameworks_11; }
	inline void set_Frameworks_11(List_1_t3392069394 * value)
	{
		___Frameworks_11 = value;
		Il2CppCodeGenWriteBarrier(&___Frameworks_11, value);
	}

	inline static int32_t get_offset_of_Libraries_12() { return static_cast<int32_t>(offsetof(ISD_Settings_t1116242554, ___Libraries_12)); }
	inline List_1_t4024472637 * get_Libraries_12() const { return ___Libraries_12; }
	inline List_1_t4024472637 ** get_address_of_Libraries_12() { return &___Libraries_12; }
	inline void set_Libraries_12(List_1_t4024472637 * value)
	{
		___Libraries_12 = value;
		Il2CppCodeGenWriteBarrier(&___Libraries_12, value);
	}

	inline static int32_t get_offset_of_compileFlags_13() { return static_cast<int32_t>(offsetof(ISD_Settings_t1116242554, ___compileFlags_13)); }
	inline List_1_t1398341365 * get_compileFlags_13() const { return ___compileFlags_13; }
	inline List_1_t1398341365 ** get_address_of_compileFlags_13() { return &___compileFlags_13; }
	inline void set_compileFlags_13(List_1_t1398341365 * value)
	{
		___compileFlags_13 = value;
		Il2CppCodeGenWriteBarrier(&___compileFlags_13, value);
	}

	inline static int32_t get_offset_of_linkFlags_14() { return static_cast<int32_t>(offsetof(ISD_Settings_t1116242554, ___linkFlags_14)); }
	inline List_1_t1398341365 * get_linkFlags_14() const { return ___linkFlags_14; }
	inline List_1_t1398341365 ** get_address_of_linkFlags_14() { return &___linkFlags_14; }
	inline void set_linkFlags_14(List_1_t1398341365 * value)
	{
		___linkFlags_14 = value;
		Il2CppCodeGenWriteBarrier(&___linkFlags_14, value);
	}

	inline static int32_t get_offset_of_PlistVariables_15() { return static_cast<int32_t>(offsetof(ISD_Settings_t1116242554, ___PlistVariables_15)); }
	inline List_1_t526886178 * get_PlistVariables_15() const { return ___PlistVariables_15; }
	inline List_1_t526886178 ** get_address_of_PlistVariables_15() { return &___PlistVariables_15; }
	inline void set_PlistVariables_15(List_1_t526886178 * value)
	{
		___PlistVariables_15 = value;
		Il2CppCodeGenWriteBarrier(&___PlistVariables_15, value);
	}

	inline static int32_t get_offset_of_langFolders_16() { return static_cast<int32_t>(offsetof(ISD_Settings_t1116242554, ___langFolders_16)); }
	inline List_1_t1398341365 * get_langFolders_16() const { return ___langFolders_16; }
	inline List_1_t1398341365 ** get_address_of_langFolders_16() { return &___langFolders_16; }
	inline void set_langFolders_16(List_1_t1398341365 * value)
	{
		___langFolders_16 = value;
		Il2CppCodeGenWriteBarrier(&___langFolders_16, value);
	}
};

struct ISD_Settings_t1116242554_StaticFields
{
public:
	// SA.IOSDeploy.ISD_Settings SA.IOSDeploy.ISD_Settings::instance
	ISD_Settings_t1116242554 * ___instance_17;

public:
	inline static int32_t get_offset_of_instance_17() { return static_cast<int32_t>(offsetof(ISD_Settings_t1116242554_StaticFields, ___instance_17)); }
	inline ISD_Settings_t1116242554 * get_instance_17() const { return ___instance_17; }
	inline ISD_Settings_t1116242554 ** get_address_of_instance_17() { return &___instance_17; }
	inline void set_instance_17(ISD_Settings_t1116242554 * value)
	{
		___instance_17 = value;
		Il2CppCodeGenWriteBarrier(&___instance_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
