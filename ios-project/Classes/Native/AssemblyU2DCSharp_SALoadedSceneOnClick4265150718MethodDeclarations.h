﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SALoadedSceneOnClick
struct SALoadedSceneOnClick_t4265150718;

#include "codegen/il2cpp-codegen.h"

// System.Void SALoadedSceneOnClick::.ctor()
extern "C"  void SALoadedSceneOnClick__ctor_m2023201347 (SALoadedSceneOnClick_t4265150718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SALoadedSceneOnClick::OnClick()
extern "C"  void SALoadedSceneOnClick_OnClick_m4054150630 (SALoadedSceneOnClick_t4265150718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
