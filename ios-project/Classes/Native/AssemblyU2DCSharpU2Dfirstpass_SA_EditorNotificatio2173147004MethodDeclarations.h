﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_EditorNotifications
struct SA_EditorNotifications_t2173147004;
// System.String
struct String_t;
// SA_Notifications_EditorUIController
struct SA_Notifications_EditorUIController_t749267495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_EditorNotificatio2256397131.h"

// System.Void SA_EditorNotifications::.ctor()
extern "C"  void SA_EditorNotifications__ctor_m992071291 (SA_EditorNotifications_t2173147004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorNotifications::.cctor()
extern "C"  void SA_EditorNotifications__cctor_m4219903688 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorNotifications::ShowNotification(System.String,System.String,SA_EditorNotificationType)
extern "C"  void SA_EditorNotifications_ShowNotification_m3379309726 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SA_Notifications_EditorUIController SA_EditorNotifications::get_EditorUI()
extern "C"  SA_Notifications_EditorUIController_t749267495 * SA_EditorNotifications_get_EditorUI_m428117509 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
