﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FB_AppRequest
struct FB_AppRequest_t501312625;
// System.String
struct String_t;
// FB_Result
struct FB_Result_t838248372;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_FB_Result838248372.h"

// System.Void FB_AppRequest::.ctor()
extern "C"  void FB_AppRequest__ctor_m899943724 (FB_AppRequest_t501312625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_AppRequest::SetCreatedTime(System.String)
extern "C"  void FB_AppRequest_SetCreatedTime_m3363509945 (FB_AppRequest_t501312625 * __this, String_t* ___time_string0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_AppRequest::Delete()
extern "C"  void FB_AppRequest_Delete_m3775013565 (FB_AppRequest_t501312625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_AppRequest::OnDeleteActionFinish(FB_Result)
extern "C"  void FB_AppRequest_OnDeleteActionFinish_m4196905955 (FB_AppRequest_t501312625 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_AppRequest::<OnDeleteRequestFinished>m__A0(FB_Result)
extern "C"  void FB_AppRequest_U3COnDeleteRequestFinishedU3Em__A0_m1417087379 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
