﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_RTM_ReliableMessageSentResult
struct GP_RTM_ReliableMessageSentResult_t2743629396;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_RTM_ReliableMessageSentResult::.ctor(System.String,System.String,System.Int32,System.Byte[])
extern "C"  void GP_RTM_ReliableMessageSentResult__ctor_m1456304939 (GP_RTM_ReliableMessageSentResult_t2743629396 * __this, String_t* ___status0, String_t* ___roomId1, int32_t ___messageTokedId2, ByteU5BU5D_t3397334013* ___data3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GP_RTM_ReliableMessageSentResult::get_MessageTokenId()
extern "C"  int32_t GP_RTM_ReliableMessageSentResult_get_MessageTokenId_m2638857603 (GP_RTM_ReliableMessageSentResult_t2743629396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GP_RTM_ReliableMessageSentResult::get_Data()
extern "C"  ByteU5BU5D_t3397334013* GP_RTM_ReliableMessageSentResult_get_Data_m455108666 (GP_RTM_ReliableMessageSentResult_t2743629396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
