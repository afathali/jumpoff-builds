﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_PropertyTemplate
struct AN_PropertyTemplate_t2393149441;
// System.String
struct String_t;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// System.Xml.XmlElement
struct XmlElement_t2877111883;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void AN_PropertyTemplate::.ctor(System.String)
extern "C"  void AN_PropertyTemplate__ctor_m3491860932 (AN_PropertyTemplate_t2393149441 * __this, String_t* ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PropertyTemplate::ToXmlElement(System.Xml.XmlDocument,System.Xml.XmlElement)
extern "C"  void AN_PropertyTemplate_ToXmlElement_m4154923109 (AN_PropertyTemplate_t2393149441 * __this, XmlDocument_t3649534162 * ___doc0, XmlElement_t2877111883 * ___parent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AN_PropertyTemplate::get_Tag()
extern "C"  String_t* AN_PropertyTemplate_get_Tag_m495149988 (AN_PropertyTemplate_t2393149441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AN_PropertyTemplate::get_Name()
extern "C"  String_t* AN_PropertyTemplate_get_Name_m679803467 (AN_PropertyTemplate_t2393149441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PropertyTemplate::set_Name(System.String)
extern "C"  void AN_PropertyTemplate_set_Name_m124908108 (AN_PropertyTemplate_t2393149441 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AN_PropertyTemplate::get_Value()
extern "C"  String_t* AN_PropertyTemplate_get_Value_m3001363979 (AN_PropertyTemplate_t2393149441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PropertyTemplate::set_Value(System.String)
extern "C"  void AN_PropertyTemplate_set_Value_m1491780818 (AN_PropertyTemplate_t2393149441 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AN_PropertyTemplate::get_Label()
extern "C"  String_t* AN_PropertyTemplate_get_Label_m2698690602 (AN_PropertyTemplate_t2393149441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PropertyTemplate::set_Label(System.String)
extern "C"  void AN_PropertyTemplate_set_Label_m2975622633 (AN_PropertyTemplate_t2393149441 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
