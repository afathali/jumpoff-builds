﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action
struct Action_t3226471752;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void SA.Common.Util.General::.cctor()
extern "C"  void General__cctor_m454697505 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Util.General::Invoke(System.Single,System.Action,System.String)
extern "C"  void General_Invoke_m1436565960 (Il2CppObject * __this /* static, unused */, float ___time0, Action_t3226471752 * ___callback1, String_t* ___name2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SA.Common.Util.General::get_CurrentTimeStamp()
extern "C"  int32_t General_get_CurrentTimeStamp_m1696844692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA.Common.Util.General::DateTimeToRfc3339(System.DateTime)
extern "C"  String_t* General_DateTimeToRfc3339_m1991261416 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime SA.Common.Util.General::ConvertFromUnixTimestamp(System.Int64)
extern "C"  DateTime_t693205669  General_ConvertFromUnixTimestamp_m3991539874 (Il2CppObject * __this /* static, unused */, int64_t ___timestamp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SA.Common.Util.General::ConvertToUnixTimestamp(System.DateTime)
extern "C"  int64_t General_ConvertToUnixTimestamp_m1005404709 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] SA.Common.Util.General::get_DateTimePatterns()
extern "C"  StringU5BU5D_t1642385972* General_get_DateTimePatterns_m3831096098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA.Common.Util.General::TryParseRfc3339(System.String,System.DateTime&)
extern "C"  bool General_TryParseRfc3339_m2077104163 (Il2CppObject * __this /* static, unused */, String_t* ___s0, DateTime_t693205669 * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA.Common.Util.General::HMAC(System.String,System.String)
extern "C"  String_t* General_HMAC_m16980664 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Util.General::CleanupInstallation()
extern "C"  void General_CleanupInstallation_m3636733562 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
