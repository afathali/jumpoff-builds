﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaModel
struct JsonSchemaModel_t708894576;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2570160834;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel>
struct IList_1_t1249835177;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>
struct IDictionary_2_t622757259;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t3093584614;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema>
struct IList_1_t18087154;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t3772113849;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen2341081996.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema708894576.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3772113849.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::.ctor()
extern "C"  void JsonSchemaModel__ctor_m1353711729 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaModel::get_Required()
extern "C"  bool JsonSchemaModel_get_Required_m1975263893 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_Required(System.Boolean)
extern "C"  void JsonSchemaModel_set_Required_m2853379916 (JsonSchemaModel_t708894576 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.Schema.JsonSchemaModel::get_Type()
extern "C"  int32_t JsonSchemaModel_get_Type_m2078416474 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_Type(Newtonsoft.Json.Schema.JsonSchemaType)
extern "C"  void JsonSchemaModel_set_Type_m687080617 (JsonSchemaModel_t708894576 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Schema.JsonSchemaModel::get_MinimumLength()
extern "C"  Nullable_1_t334943763  JsonSchemaModel_get_MinimumLength_m2534458661 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_MinimumLength(System.Nullable`1<System.Int32>)
extern "C"  void JsonSchemaModel_set_MinimumLength_m99262480 (JsonSchemaModel_t708894576 * __this, Nullable_1_t334943763  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Schema.JsonSchemaModel::get_MaximumLength()
extern "C"  Nullable_1_t334943763  JsonSchemaModel_get_MaximumLength_m364345947 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_MaximumLength(System.Nullable`1<System.Int32>)
extern "C"  void JsonSchemaModel_set_MaximumLength_m1828285166 (JsonSchemaModel_t708894576 * __this, Nullable_1_t334943763  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.Schema.JsonSchemaModel::get_DivisibleBy()
extern "C"  Nullable_1_t2341081996  JsonSchemaModel_get_DivisibleBy_m4206097966 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_DivisibleBy(System.Nullable`1<System.Double>)
extern "C"  void JsonSchemaModel_set_DivisibleBy_m1708642623 (JsonSchemaModel_t708894576 * __this, Nullable_1_t2341081996  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.Schema.JsonSchemaModel::get_Minimum()
extern "C"  Nullable_1_t2341081996  JsonSchemaModel_get_Minimum_m4006144632 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_Minimum(System.Nullable`1<System.Double>)
extern "C"  void JsonSchemaModel_set_Minimum_m3440507417 (JsonSchemaModel_t708894576 * __this, Nullable_1_t2341081996  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.Schema.JsonSchemaModel::get_Maximum()
extern "C"  Nullable_1_t2341081996  JsonSchemaModel_get_Maximum_m420603314 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_Maximum(System.Nullable`1<System.Double>)
extern "C"  void JsonSchemaModel_set_Maximum_m2475604087 (JsonSchemaModel_t708894576 * __this, Nullable_1_t2341081996  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaModel::get_ExclusiveMinimum()
extern "C"  bool JsonSchemaModel_get_ExclusiveMinimum_m725207962 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_ExclusiveMinimum(System.Boolean)
extern "C"  void JsonSchemaModel_set_ExclusiveMinimum_m321594155 (JsonSchemaModel_t708894576 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaModel::get_ExclusiveMaximum()
extern "C"  bool JsonSchemaModel_get_ExclusiveMaximum_m1509185312 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_ExclusiveMaximum(System.Boolean)
extern "C"  void JsonSchemaModel_set_ExclusiveMaximum_m611848069 (JsonSchemaModel_t708894576 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Schema.JsonSchemaModel::get_MinimumItems()
extern "C"  Nullable_1_t334943763  JsonSchemaModel_get_MinimumItems_m1769061215 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_MinimumItems(System.Nullable`1<System.Int32>)
extern "C"  void JsonSchemaModel_set_MinimumItems_m995809514 (JsonSchemaModel_t708894576 * __this, Nullable_1_t334943763  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Schema.JsonSchemaModel::get_MaximumItems()
extern "C"  Nullable_1_t334943763  JsonSchemaModel_get_MaximumItems_m2043067233 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_MaximumItems(System.Nullable`1<System.Int32>)
extern "C"  void JsonSchemaModel_set_MaximumItems_m3970410740 (JsonSchemaModel_t708894576 * __this, Nullable_1_t334943763  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.String> Newtonsoft.Json.Schema.JsonSchemaModel::get_Patterns()
extern "C"  Il2CppObject* JsonSchemaModel_get_Patterns_m1809046549 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_Patterns(System.Collections.Generic.IList`1<System.String>)
extern "C"  void JsonSchemaModel_set_Patterns_m3395307738 (JsonSchemaModel_t708894576 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.Schema.JsonSchemaModel::get_Items()
extern "C"  Il2CppObject* JsonSchemaModel_get_Items_m2914145952 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_Items(System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel>)
extern "C"  void JsonSchemaModel_set_Items_m500217811 (JsonSchemaModel_t708894576 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.Schema.JsonSchemaModel::get_Properties()
extern "C"  Il2CppObject* JsonSchemaModel_get_Properties_m2277354500 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_Properties(System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>)
extern "C"  void JsonSchemaModel_set_Properties_m6466243 (JsonSchemaModel_t708894576 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.Schema.JsonSchemaModel::get_PatternProperties()
extern "C"  Il2CppObject* JsonSchemaModel_get_PatternProperties_m377723550 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_PatternProperties(System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>)
extern "C"  void JsonSchemaModel_set_PatternProperties_m4042268823 (JsonSchemaModel_t708894576 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaModel Newtonsoft.Json.Schema.JsonSchemaModel::get_AdditionalProperties()
extern "C"  JsonSchemaModel_t708894576 * JsonSchemaModel_get_AdditionalProperties_m153802413 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_AdditionalProperties(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  void JsonSchemaModel_set_AdditionalProperties_m3632618022 (JsonSchemaModel_t708894576 * __this, JsonSchemaModel_t708894576 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaModel::get_AllowAdditionalProperties()
extern "C"  bool JsonSchemaModel_get_AllowAdditionalProperties_m117214285 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_AllowAdditionalProperties(System.Boolean)
extern "C"  void JsonSchemaModel_set_AllowAdditionalProperties_m3119184198 (JsonSchemaModel_t708894576 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Schema.JsonSchemaModel::get_Enum()
extern "C"  Il2CppObject* JsonSchemaModel_get_Enum_m220051561 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_Enum(System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>)
extern "C"  void JsonSchemaModel_set_Enum_m3360668028 (JsonSchemaModel_t708894576 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.Schema.JsonSchemaModel::get_Disallow()
extern "C"  int32_t JsonSchemaModel_get_Disallow_m447890277 (JsonSchemaModel_t708894576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::set_Disallow(Newtonsoft.Json.Schema.JsonSchemaType)
extern "C"  void JsonSchemaModel_set_Disallow_m2754229678 (JsonSchemaModel_t708894576 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaModel Newtonsoft.Json.Schema.JsonSchemaModel::Create(System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema>)
extern "C"  JsonSchemaModel_t708894576 * JsonSchemaModel_Create_m3685891838 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___schemata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModel::Combine(Newtonsoft.Json.Schema.JsonSchemaModel,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaModel_Combine_m1208486759 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t708894576 * ___model0, JsonSchema_t3772113849 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
