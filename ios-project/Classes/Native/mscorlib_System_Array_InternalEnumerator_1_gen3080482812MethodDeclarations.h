﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3080482812.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchTurnStatus2221730550.h"

// System.Void System.Array/InternalEnumerator`1<GP_TBM_MatchTurnStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2830510681_gshared (InternalEnumerator_1_t3080482812 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2830510681(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3080482812 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2830510681_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<GP_TBM_MatchTurnStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4190331773_gshared (InternalEnumerator_1_t3080482812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4190331773(__this, method) ((  void (*) (InternalEnumerator_1_t3080482812 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4190331773_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<GP_TBM_MatchTurnStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2187749429_gshared (InternalEnumerator_1_t3080482812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2187749429(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3080482812 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2187749429_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<GP_TBM_MatchTurnStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4779282_gshared (InternalEnumerator_1_t3080482812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4779282(__this, method) ((  void (*) (InternalEnumerator_1_t3080482812 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4779282_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<GP_TBM_MatchTurnStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m911275493_gshared (InternalEnumerator_1_t3080482812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m911275493(__this, method) ((  bool (*) (InternalEnumerator_1_t3080482812 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m911275493_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<GP_TBM_MatchTurnStatus>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3325508752_gshared (InternalEnumerator_1_t3080482812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3325508752(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3080482812 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3325508752_gshared)(__this, method)
