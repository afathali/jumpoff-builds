﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>
struct EnumValue_1_t2589200904;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>::.ctor(System.String,T)
extern "C"  void EnumValue_1__ctor_m752973208_gshared (EnumValue_1_t2589200904 * __this, String_t* ___name0, uint64_t ___value1, const MethodInfo* method);
#define EnumValue_1__ctor_m752973208(__this, ___name0, ___value1, method) ((  void (*) (EnumValue_1_t2589200904 *, String_t*, uint64_t, const MethodInfo*))EnumValue_1__ctor_m752973208_gshared)(__this, ___name0, ___value1, method)
// System.String Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>::get_Name()
extern "C"  String_t* EnumValue_1_get_Name_m3282313175_gshared (EnumValue_1_t2589200904 * __this, const MethodInfo* method);
#define EnumValue_1_get_Name_m3282313175(__this, method) ((  String_t* (*) (EnumValue_1_t2589200904 *, const MethodInfo*))EnumValue_1_get_Name_m3282313175_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>::get_Value()
extern "C"  uint64_t EnumValue_1_get_Value_m1167595673_gshared (EnumValue_1_t2589200904 * __this, const MethodInfo* method);
#define EnumValue_1_get_Value_m1167595673(__this, method) ((  uint64_t (*) (EnumValue_1_t2589200904 *, const MethodInfo*))EnumValue_1_get_Value_m1167595673_gshared)(__this, method)
