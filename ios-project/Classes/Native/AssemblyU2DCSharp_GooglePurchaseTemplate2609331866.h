﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_GooglePurchaseState3606541435.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePurchaseTemplate
struct  GooglePurchaseTemplate_t2609331866  : public Il2CppObject
{
public:
	// System.String GooglePurchaseTemplate::orderId
	String_t* ___orderId_0;
	// System.String GooglePurchaseTemplate::packageName
	String_t* ___packageName_1;
	// System.String GooglePurchaseTemplate::SKU
	String_t* ___SKU_2;
	// System.String GooglePurchaseTemplate::developerPayload
	String_t* ___developerPayload_3;
	// System.String GooglePurchaseTemplate::signature
	String_t* ___signature_4;
	// System.String GooglePurchaseTemplate::token
	String_t* ___token_5;
	// System.Int64 GooglePurchaseTemplate::time
	int64_t ___time_6;
	// System.String GooglePurchaseTemplate::originalJson
	String_t* ___originalJson_7;
	// GooglePurchaseState GooglePurchaseTemplate::state
	int32_t ___state_8;

public:
	inline static int32_t get_offset_of_orderId_0() { return static_cast<int32_t>(offsetof(GooglePurchaseTemplate_t2609331866, ___orderId_0)); }
	inline String_t* get_orderId_0() const { return ___orderId_0; }
	inline String_t** get_address_of_orderId_0() { return &___orderId_0; }
	inline void set_orderId_0(String_t* value)
	{
		___orderId_0 = value;
		Il2CppCodeGenWriteBarrier(&___orderId_0, value);
	}

	inline static int32_t get_offset_of_packageName_1() { return static_cast<int32_t>(offsetof(GooglePurchaseTemplate_t2609331866, ___packageName_1)); }
	inline String_t* get_packageName_1() const { return ___packageName_1; }
	inline String_t** get_address_of_packageName_1() { return &___packageName_1; }
	inline void set_packageName_1(String_t* value)
	{
		___packageName_1 = value;
		Il2CppCodeGenWriteBarrier(&___packageName_1, value);
	}

	inline static int32_t get_offset_of_SKU_2() { return static_cast<int32_t>(offsetof(GooglePurchaseTemplate_t2609331866, ___SKU_2)); }
	inline String_t* get_SKU_2() const { return ___SKU_2; }
	inline String_t** get_address_of_SKU_2() { return &___SKU_2; }
	inline void set_SKU_2(String_t* value)
	{
		___SKU_2 = value;
		Il2CppCodeGenWriteBarrier(&___SKU_2, value);
	}

	inline static int32_t get_offset_of_developerPayload_3() { return static_cast<int32_t>(offsetof(GooglePurchaseTemplate_t2609331866, ___developerPayload_3)); }
	inline String_t* get_developerPayload_3() const { return ___developerPayload_3; }
	inline String_t** get_address_of_developerPayload_3() { return &___developerPayload_3; }
	inline void set_developerPayload_3(String_t* value)
	{
		___developerPayload_3 = value;
		Il2CppCodeGenWriteBarrier(&___developerPayload_3, value);
	}

	inline static int32_t get_offset_of_signature_4() { return static_cast<int32_t>(offsetof(GooglePurchaseTemplate_t2609331866, ___signature_4)); }
	inline String_t* get_signature_4() const { return ___signature_4; }
	inline String_t** get_address_of_signature_4() { return &___signature_4; }
	inline void set_signature_4(String_t* value)
	{
		___signature_4 = value;
		Il2CppCodeGenWriteBarrier(&___signature_4, value);
	}

	inline static int32_t get_offset_of_token_5() { return static_cast<int32_t>(offsetof(GooglePurchaseTemplate_t2609331866, ___token_5)); }
	inline String_t* get_token_5() const { return ___token_5; }
	inline String_t** get_address_of_token_5() { return &___token_5; }
	inline void set_token_5(String_t* value)
	{
		___token_5 = value;
		Il2CppCodeGenWriteBarrier(&___token_5, value);
	}

	inline static int32_t get_offset_of_time_6() { return static_cast<int32_t>(offsetof(GooglePurchaseTemplate_t2609331866, ___time_6)); }
	inline int64_t get_time_6() const { return ___time_6; }
	inline int64_t* get_address_of_time_6() { return &___time_6; }
	inline void set_time_6(int64_t value)
	{
		___time_6 = value;
	}

	inline static int32_t get_offset_of_originalJson_7() { return static_cast<int32_t>(offsetof(GooglePurchaseTemplate_t2609331866, ___originalJson_7)); }
	inline String_t* get_originalJson_7() const { return ___originalJson_7; }
	inline String_t** get_address_of_originalJson_7() { return &___originalJson_7; }
	inline void set_originalJson_7(String_t* value)
	{
		___originalJson_7 = value;
		Il2CppCodeGenWriteBarrier(&___originalJson_7, value);
	}

	inline static int32_t get_offset_of_state_8() { return static_cast<int32_t>(offsetof(GooglePurchaseTemplate_t2609331866, ___state_8)); }
	inline int32_t get_state_8() const { return ___state_8; }
	inline int32_t* get_address_of_state_8() { return &___state_8; }
	inline void set_state_8(int32_t value)
	{
		___state_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
