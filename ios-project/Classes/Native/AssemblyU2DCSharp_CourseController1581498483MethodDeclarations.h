﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CourseController
struct CourseController_t1581498483;
// System.Collections.Generic.IEnumerable`1<JumpPoint>
struct IEnumerable_1_t1301987651;
// JumpPoint[]
struct JumpPointU5BU5D_t4004280907;
// CreateCourseResponse
struct CreateCourseResponse_t3800683746;
// GetCourseResponse
struct GetCourseResponse_t2659116210;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_CreateCourseResponse3800683746.h"
#include "AssemblyU2DCSharp_GetCourseResponse2659116210.h"

// System.Void CourseController::.ctor()
extern "C"  void CourseController__ctor_m2092169172 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::.cctor()
extern "C"  void CourseController__cctor_m838864453 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::fixNumbering()
extern "C"  void CourseController_fixNumbering_m2789464484 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::updateJumpsNumberText()
extern "C"  void CourseController_updateJumpsNumberText_m3235279932 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CourseController::checkNumbering(System.Int32&,System.Int32&)
extern "C"  bool CourseController_checkNumbering_m2851141019 (CourseController_t1581498483 * __this, int32_t* ___badNumber0, int32_t* ___badCombination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::registerJumpEditListeners()
extern "C"  void CourseController_registerJumpEditListeners_m3811543958 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::updateJumpFromSpinners()
extern "C"  void CourseController_updateJumpFromSpinners_m3456282337 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::UpdateCurrentCourse()
extern "C"  void CourseController_UpdateCurrentCourse_m2038596515 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::loadImage()
extern "C"  void CourseController_loadImage_m1750825227 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::visualize()
extern "C"  void CourseController_visualize_m2956057526 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::Start()
extern "C"  void CourseController_Start_m3171032256 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::CloseTutorial(System.Int32)
extern "C"  void CourseController_CloseTutorial_m1951413281 (CourseController_t1581498483 * __this, int32_t ____tutorialNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::ShowCurrentScreenTutorial()
extern "C"  void CourseController_ShowCurrentScreenTutorial_m1346111476 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::HidePopups()
extern "C"  void CourseController_HidePopups_m2067874975 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::ToggleCheats()
extern "C"  void CourseController_ToggleCheats_m224796886 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::InstantiateJumps(System.Collections.Generic.IEnumerable`1<JumpPoint>)
extern "C"  void CourseController_InstantiateJumps_m3267437274 (CourseController_t1581498483 * __this, Il2CppObject* ___jumpPoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::clearJumpsGO()
extern "C"  void CourseController_clearJumpsGO_m670398074 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::AnimateButtonsGrowing()
extern "C"  void CourseController_AnimateButtonsGrowing_m1844193389 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::AnimateButtonsShrinking()
extern "C"  void CourseController_AnimateButtonsShrinking_m3147365925 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::EnterPlacingJumpsMode()
extern "C"  void CourseController_EnterPlacingJumpsMode_m1099129956 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::EnterDrawCourseMode()
extern "C"  void CourseController_EnterDrawCourseMode_m38894490 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::ComputeJumpOrder()
extern "C"  void CourseController_ComputeJumpOrder_m1334159167 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::ResetSlider()
extern "C"  void CourseController_ResetSlider_m1230013146 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::ToggleImage()
extern "C"  void CourseController_ToggleImage_m4015016935 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::RotateJump()
extern "C"  void CourseController_RotateJump_m47189841 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CourseController::GetMousePosition()
extern "C"  Vector3_t2243707580  CourseController_GetMousePosition_m2752807028 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CourseController::getMaxJumpNumber()
extern "C"  int32_t CourseController_getMaxJumpNumber_m438575403 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CourseController::TrySelectJump()
extern "C"  bool CourseController_TrySelectJump_m2369363231 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::DeselectJump()
extern "C"  void CourseController_DeselectJump_m2301070189 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::UpdatePlaceJumps()
extern "C"  void CourseController_UpdatePlaceJumps_m1165304565 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::updateJumpCombinationSpinnerVisibility(System.Int32)
extern "C"  void CourseController_updateJumpCombinationSpinnerVisibility_m2074135346 (CourseController_t1581498483 * __this, int32_t ___jumpNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CourseController::jumpCount(System.Int32)
extern "C"  int32_t CourseController_jumpCount_m2526647548 (CourseController_t1581498483 * __this, int32_t ___jumpNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::Update_placingJumpsMode()
extern "C"  void CourseController_Update_placingJumpsMode_m2905796952 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::Update_drawCourseMode()
extern "C"  void CourseController_Update_drawCourseMode_m1328741026 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::LerpViewportReset()
extern "C"  void CourseController_LerpViewportReset_m1651408570 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::Update()
extern "C"  void CourseController_Update_m2281489075 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::placeStartPoint()
extern "C"  void CourseController_placeStartPoint_m1986434869 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::placeEndPoint()
extern "C"  void CourseController_placeEndPoint_m1095707634 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::CheckForHit(UnityEngine.Vector3)
extern "C"  void CourseController_CheckForHit_m3770842203 (CourseController_t1581498483 * __this, Vector3_t2243707580  ____point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::addMissingPoints()
extern "C"  void CourseController_addMissingPoints_m2272997520 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::EraseAttempt()
extern "C"  void CourseController_EraseAttempt_m495966987 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::OpenMenu()
extern "C"  void CourseController_OpenMenu_m1042747045 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::CloseMenu()
extern "C"  void CourseController_CloseMenu_m1706147795 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JumpPoint[] CourseController::jumpsToPoints()
extern "C"  JumpPointU5BU5D_t4004280907* CourseController_jumpsToPoints_m2779115642 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::SaveCourse()
extern "C"  void CourseController_SaveCourse_m180837750 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::HandleCreateCourseResponse(System.Boolean,CreateCourseResponse)
extern "C"  void CourseController_HandleCreateCourseResponse_m2686286347 (CourseController_t1581498483 * __this, bool ___success0, CreateCourseResponse_t3800683746 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::Exit()
extern "C"  void CourseController_Exit_m4249466994 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<registerJumpEditListeners>m__115()
extern "C"  void CourseController_U3CregisterJumpEditListenersU3Em__115_m71812890 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<registerJumpEditListeners>m__116()
extern "C"  void CourseController_U3CregisterJumpEditListenersU3Em__116_m71812729 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<registerJumpEditListeners>m__117()
extern "C"  void CourseController_U3CregisterJumpEditListenersU3Em__117_m71812824 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<registerJumpEditListeners>m__118()
extern "C"  void CourseController_U3CregisterJumpEditListenersU3Em__118_m71812415 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<registerJumpEditListeners>m__119()
extern "C"  void CourseController_U3CregisterJumpEditListenersU3Em__119_m71812510 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__11A()
extern "C"  void CourseController_U3CStartU3Em__11A_m3018547592 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__11B()
extern "C"  void CourseController_U3CStartU3Em__11B_m3018547431 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__11C()
extern "C"  void CourseController_U3CStartU3Em__11C_m3018547526 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__11D()
extern "C"  void CourseController_U3CStartU3Em__11D_m3018547629 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__11E()
extern "C"  void CourseController_U3CStartU3Em__11E_m3018547724 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__11F()
extern "C"  void CourseController_U3CStartU3Em__11F_m3018547563 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__120()
extern "C"  void CourseController_U3CStartU3Em__120_m1432261172 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__121()
extern "C"  void CourseController_U3CStartU3Em__121_m1432261139 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__122()
extern "C"  void CourseController_U3CStartU3Em__122_m1432261106 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__123()
extern "C"  void CourseController_U3CStartU3Em__123_m1432261073 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__124()
extern "C"  void CourseController_U3CStartU3Em__124_m1432261304 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__125()
extern "C"  void CourseController_U3CStartU3Em__125_m1432261271 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__126()
extern "C"  void CourseController_U3CStartU3Em__126_m1432261238 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__127()
extern "C"  void CourseController_U3CStartU3Em__127_m1432261205 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__128()
extern "C"  void CourseController_U3CStartU3Em__128_m1432261420 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__129()
extern "C"  void CourseController_U3CStartU3Em__129_m1432261387 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__12A()
extern "C"  void CourseController_U3CStartU3Em__12A_m1432260675 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__12B()
extern "C"  void CourseController_U3CStartU3Em__12B_m1432260514 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__12C()
extern "C"  void CourseController_U3CStartU3Em__12C_m1432260609 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__12D()
extern "C"  void CourseController_U3CStartU3Em__12D_m1432260712 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<Start>m__12E(System.Boolean,GetCourseResponse)
extern "C"  void CourseController_U3CStartU3Em__12E_m1765869956 (CourseController_t1581498483 * __this, bool ___success0, GetCourseResponse_t2659116210 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseController::<EnterDrawCourseMode>m__12F()
extern "C"  void CourseController_U3CEnterDrawCourseModeU3Em__12F_m1762180188 (CourseController_t1581498483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
