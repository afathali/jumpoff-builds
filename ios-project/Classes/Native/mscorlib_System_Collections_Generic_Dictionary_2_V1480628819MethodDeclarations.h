﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>
struct ValueCollection_t1480628819;
// System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>
struct Dictionary_2_t2777568976;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va169134444.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m360886506_gshared (ValueCollection_t1480628819 * __this, Dictionary_2_t2777568976 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m360886506(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1480628819 *, Dictionary_2_t2777568976 *, const MethodInfo*))ValueCollection__ctor_m360886506_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2371726736_gshared (ValueCollection_t1480628819 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2371726736(__this, ___item0, method) ((  void (*) (ValueCollection_t1480628819 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2371726736_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m31724987_gshared (ValueCollection_t1480628819 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m31724987(__this, method) ((  void (*) (ValueCollection_t1480628819 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m31724987_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m542925012_gshared (ValueCollection_t1480628819 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m542925012(__this, ___item0, method) ((  bool (*) (ValueCollection_t1480628819 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m542925012_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2399896467_gshared (ValueCollection_t1480628819 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2399896467(__this, ___item0, method) ((  bool (*) (ValueCollection_t1480628819 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2399896467_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2977964245_gshared (ValueCollection_t1480628819 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2977964245(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1480628819 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2977964245_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2352087081_gshared (ValueCollection_t1480628819 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2352087081(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1480628819 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2352087081_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1200661560_gshared (ValueCollection_t1480628819 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1200661560(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1480628819 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1200661560_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m670572463_gshared (ValueCollection_t1480628819 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m670572463(__this, method) ((  bool (*) (ValueCollection_t1480628819 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m670572463_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1165288885_gshared (ValueCollection_t1480628819 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1165288885(__this, method) ((  bool (*) (ValueCollection_t1480628819 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1165288885_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1191576105_gshared (ValueCollection_t1480628819 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1191576105(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1480628819 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1191576105_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2654148259_gshared (ValueCollection_t1480628819 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2654148259(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1480628819 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2654148259_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::GetEnumerator()
extern "C"  Enumerator_t169134444  ValueCollection_GetEnumerator_m2927199816_gshared (ValueCollection_t1480628819 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2927199816(__this, method) ((  Enumerator_t169134444  (*) (ValueCollection_t1480628819 *, const MethodInfo*))ValueCollection_GetEnumerator_m2927199816_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<FB_ProfileImageSize,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m217140693_gshared (ValueCollection_t1480628819 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m217140693(__this, method) ((  int32_t (*) (ValueCollection_t1480628819 *, const MethodInfo*))ValueCollection_get_Count_m217140693_gshared)(__this, method)
