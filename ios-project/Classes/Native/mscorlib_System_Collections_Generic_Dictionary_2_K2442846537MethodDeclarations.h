﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4147435347MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3675256126(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2442846537 *, Dictionary_2_t4254316062 *, const MethodInfo*))KeyCollection__ctor_m1627846113_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3794605024(__this, ___item0, method) ((  void (*) (KeyCollection_t2442846537 *, SampleBase_t2925764113 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3888838575_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2506014375(__this, method) ((  void (*) (KeyCollection_t2442846537 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m237922362_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4111940600(__this, ___item0, method) ((  bool (*) (KeyCollection_t2442846537 *, SampleBase_t2925764113 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1114577217_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3695347023(__this, ___item0, method) ((  bool (*) (KeyCollection_t2442846537 *, SampleBase_t2925764113 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2082552954_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1966527957(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2442846537 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4237862166_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1596730937(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2442846537 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3219210900_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1950145872(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2442846537 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3069883463_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2605714595(__this, method) ((  bool (*) (KeyCollection_t2442846537 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2823494596_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2310568301(__this, method) ((  bool (*) (KeyCollection_t2442846537 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1497186736_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1146497305(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2442846537 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3825728526_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2618745535(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2442846537 *, SampleBaseU5BU5D_t1910045324*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1942070334_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3802975942(__this, method) ((  Enumerator_t2648852204  (*) (KeyCollection_t2442846537 *, const MethodInfo*))KeyCollection_GetEnumerator_m3925297057_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::get_Count()
#define KeyCollection_get_Count_m1271356373(__this, method) ((  int32_t (*) (KeyCollection_t2442846537 *, const MethodInfo*))KeyCollection_get_Count_m3930094200_gshared)(__this, method)
