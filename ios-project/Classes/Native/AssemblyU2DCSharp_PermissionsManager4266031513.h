﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<AN_GrantPermissionsResult>
struct Action_1_t52289039;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen1261908135.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PermissionsManager
struct  PermissionsManager_t4266031513  : public SA_Singleton_OLD_1_t1261908135
{
public:

public:
};

struct PermissionsManager_t4266031513_StaticFields
{
public:
	// System.Action`1<AN_GrantPermissionsResult> PermissionsManager::ActionPermissionsRequestCompleted
	Action_1_t52289039 * ___ActionPermissionsRequestCompleted_5;
	// System.Action`1<AN_GrantPermissionsResult> PermissionsManager::<>f__am$cache1
	Action_1_t52289039 * ___U3CU3Ef__amU24cache1_6;

public:
	inline static int32_t get_offset_of_ActionPermissionsRequestCompleted_5() { return static_cast<int32_t>(offsetof(PermissionsManager_t4266031513_StaticFields, ___ActionPermissionsRequestCompleted_5)); }
	inline Action_1_t52289039 * get_ActionPermissionsRequestCompleted_5() const { return ___ActionPermissionsRequestCompleted_5; }
	inline Action_1_t52289039 ** get_address_of_ActionPermissionsRequestCompleted_5() { return &___ActionPermissionsRequestCompleted_5; }
	inline void set_ActionPermissionsRequestCompleted_5(Action_1_t52289039 * value)
	{
		___ActionPermissionsRequestCompleted_5 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPermissionsRequestCompleted_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_6() { return static_cast<int32_t>(offsetof(PermissionsManager_t4266031513_StaticFields, ___U3CU3Ef__amU24cache1_6)); }
	inline Action_1_t52289039 * get_U3CU3Ef__amU24cache1_6() const { return ___U3CU3Ef__amU24cache1_6; }
	inline Action_1_t52289039 ** get_address_of_U3CU3Ef__amU24cache1_6() { return &___U3CU3Ef__amU24cache1_6; }
	inline void set_U3CU3Ef__amU24cache1_6(Action_1_t52289039 * value)
	{
		___U3CU3Ef__amU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
