﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_BaseTemplate
struct AN_BaseTemplate_t2111071391;
// AN_PropertyTemplate
struct AN_PropertyTemplate_t2393149441;
// System.String
struct String_t;
// System.Collections.Generic.List`1<AN_PropertyTemplate>
struct List_1_t1762270573;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<AN_PropertyTemplate>>
struct Dictionary_2_t3677049835;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AN_PropertyTemplate2393149441.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"
#include "AssemblyU2DCSharp_AN_BaseTemplate2111071391.h"

// System.Void AN_BaseTemplate::.ctor()
extern "C"  void AN_BaseTemplate__ctor_m3116958898 (AN_BaseTemplate_t2111071391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AN_PropertyTemplate AN_BaseTemplate::GetOrCreateIntentFilterWithName(System.String)
extern "C"  AN_PropertyTemplate_t2393149441 * AN_BaseTemplate_GetOrCreateIntentFilterWithName_m2060288814 (AN_BaseTemplate_t2111071391 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AN_PropertyTemplate AN_BaseTemplate::GetIntentFilterWithName(System.String)
extern "C"  AN_PropertyTemplate_t2393149441 * AN_BaseTemplate_GetIntentFilterWithName_m3193205607 (AN_BaseTemplate_t2111071391 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AN_BaseTemplate::GetIntentFilterName(AN_PropertyTemplate)
extern "C"  String_t* AN_BaseTemplate_GetIntentFilterName_m4019580457 (AN_BaseTemplate_t2111071391 * __this, AN_PropertyTemplate_t2393149441 * ___intent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AN_PropertyTemplate AN_BaseTemplate::GetOrCreatePropertyWithName(System.String,System.String)
extern "C"  AN_PropertyTemplate_t2393149441 * AN_BaseTemplate_GetOrCreatePropertyWithName_m1061097335 (AN_BaseTemplate_t2111071391 * __this, String_t* ___tag0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AN_PropertyTemplate AN_BaseTemplate::GetPropertyWithName(System.String,System.String)
extern "C"  AN_PropertyTemplate_t2393149441 * AN_BaseTemplate_GetPropertyWithName_m562714734 (AN_BaseTemplate_t2111071391 * __this, String_t* ___tag0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AN_PropertyTemplate AN_BaseTemplate::GetOrCreatePropertyWithTag(System.String)
extern "C"  AN_PropertyTemplate_t2393149441 * AN_BaseTemplate_GetOrCreatePropertyWithTag_m3338475342 (AN_BaseTemplate_t2111071391 * __this, String_t* ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AN_PropertyTemplate AN_BaseTemplate::GetPropertyWithTag(System.String)
extern "C"  AN_PropertyTemplate_t2393149441 * AN_BaseTemplate_GetPropertyWithTag_m575132701 (AN_BaseTemplate_t2111071391 * __this, String_t* ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<AN_PropertyTemplate> AN_BaseTemplate::GetPropertiesWithTag(System.String)
extern "C"  List_1_t1762270573 * AN_BaseTemplate_GetPropertiesWithTag_m1346990641 (AN_BaseTemplate_t2111071391 * __this, String_t* ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_BaseTemplate::AddProperty(AN_PropertyTemplate)
extern "C"  void AN_BaseTemplate_AddProperty_m281640571 (AN_BaseTemplate_t2111071391 * __this, AN_PropertyTemplate_t2393149441 * ___property0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_BaseTemplate::AddProperty(System.String,AN_PropertyTemplate)
extern "C"  void AN_BaseTemplate_AddProperty_m3496605445 (AN_BaseTemplate_t2111071391 * __this, String_t* ___tag0, AN_PropertyTemplate_t2393149441 * ___property1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_BaseTemplate::SetValue(System.String,System.String)
extern "C"  void AN_BaseTemplate_SetValue_m1867085607 (AN_BaseTemplate_t2111071391 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AN_BaseTemplate::GetValue(System.String)
extern "C"  String_t* AN_BaseTemplate_GetValue_m1044224 (AN_BaseTemplate_t2111071391 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_BaseTemplate::RemoveProperty(AN_PropertyTemplate)
extern "C"  void AN_BaseTemplate_RemoveProperty_m1451786544 (AN_BaseTemplate_t2111071391 * __this, AN_PropertyTemplate_t2393149441 * ___property0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_BaseTemplate::RemoveValue(System.String)
extern "C"  void AN_BaseTemplate_RemoveValue_m956129637 (AN_BaseTemplate_t2111071391 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_BaseTemplate::AddPropertiesToXml(System.Xml.XmlDocument,System.Xml.XmlElement,AN_BaseTemplate)
extern "C"  void AN_BaseTemplate_AddPropertiesToXml_m241412948 (AN_BaseTemplate_t2111071391 * __this, XmlDocument_t3649534162 * ___doc0, XmlElement_t2877111883 * ___parent1, AN_BaseTemplate_t2111071391 * ___template2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_BaseTemplate::AddAttributesToXml(System.Xml.XmlDocument,System.Xml.XmlElement,AN_BaseTemplate)
extern "C"  void AN_BaseTemplate_AddAttributesToXml_m1477400732 (AN_BaseTemplate_t2111071391 * __this, XmlDocument_t3649534162 * ___doc0, XmlElement_t2877111883 * ___parent1, AN_BaseTemplate_t2111071391 * ___template2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> AN_BaseTemplate::get_Values()
extern "C"  Dictionary_2_t3943999495 * AN_BaseTemplate_get_Values_m1353020183 (AN_BaseTemplate_t2111071391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<AN_PropertyTemplate>> AN_BaseTemplate::get_Properties()
extern "C"  Dictionary_2_t3677049835 * AN_BaseTemplate_get_Properties_m2456911345 (AN_BaseTemplate_t2111071391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
