﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidSocialNativeExample/<PostScreenshot>c__Iterator2
struct U3CPostScreenshotU3Ec__Iterator2_t559519129;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidSocialNativeExample/<PostScreenshot>c__Iterator2::.ctor()
extern "C"  void U3CPostScreenshotU3Ec__Iterator2__ctor_m3335368336 (U3CPostScreenshotU3Ec__Iterator2_t559519129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AndroidSocialNativeExample/<PostScreenshot>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPostScreenshotU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3497717386 (U3CPostScreenshotU3Ec__Iterator2_t559519129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AndroidSocialNativeExample/<PostScreenshot>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPostScreenshotU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m4234589682 (U3CPostScreenshotU3Ec__Iterator2_t559519129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidSocialNativeExample/<PostScreenshot>c__Iterator2::MoveNext()
extern "C"  bool U3CPostScreenshotU3Ec__Iterator2_MoveNext_m2303835748 (U3CPostScreenshotU3Ec__Iterator2_t559519129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialNativeExample/<PostScreenshot>c__Iterator2::Dispose()
extern "C"  void U3CPostScreenshotU3Ec__Iterator2_Dispose_m2068331187 (U3CPostScreenshotU3Ec__Iterator2_t559519129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialNativeExample/<PostScreenshot>c__Iterator2::Reset()
extern "C"  void U3CPostScreenshotU3Ec__Iterator2_Reset_m3923329349 (U3CPostScreenshotU3Ec__Iterator2_t559519129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
