﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_Result
struct AN_Result_t1919128167;

#include "codegen/il2cpp-codegen.h"

// System.Void AN_Result::.ctor(System.Boolean)
extern "C"  void AN_Result__ctor_m861430717 (AN_Result_t1919128167 * __this, bool ___IsResultSucceeded0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AN_Result::get_IsSucceeded()
extern "C"  bool AN_Result_get_IsSucceeded_m3734857848 (AN_Result_t1919128167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AN_Result::get_IsFailed()
extern "C"  bool AN_Result_get_IsFailed_m962516872 (AN_Result_t1919128167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
