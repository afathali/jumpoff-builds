﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<GP_Quest>::.ctor()
#define List_1__ctor_m1694846987(__this, method) ((  void (*) (List_1_t1011004602 *, const MethodInfo*))List_1__ctor_m1864370736_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1875111867(__this, ___collection0, method) ((  void (*) (List_1_t1011004602 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2396561940_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::.ctor(System.Int32)
#define List_1__ctor_m2004896313(__this, ___capacity0, method) ((  void (*) (List_1_t1011004602 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::.cctor()
#define List_1__cctor_m18736363(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<GP_Quest>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1830251818(__this, method) ((  Il2CppObject* (*) (List_1_t1011004602 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1278935616(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1011004602 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<GP_Quest>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m141484357(__this, method) ((  Il2CppObject * (*) (List_1_t1011004602 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GP_Quest>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3246217324(__this, ___item0, method) ((  int32_t (*) (List_1_t1011004602 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<GP_Quest>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m4180971894(__this, ___item0, method) ((  bool (*) (List_1_t1011004602 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<GP_Quest>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3003273438(__this, ___item0, method) ((  int32_t (*) (List_1_t1011004602 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1834449273(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1011004602 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3133062473(__this, ___item0, method) ((  void (*) (List_1_t1011004602 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<GP_Quest>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2944211701(__this, method) ((  bool (*) (List_1_t1011004602 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GP_Quest>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m517063088(__this, method) ((  bool (*) (List_1_t1011004602 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GP_Quest>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3941303590(__this, method) ((  Il2CppObject * (*) (List_1_t1011004602 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GP_Quest>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m851970917(__this, method) ((  bool (*) (List_1_t1011004602 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GP_Quest>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1416625772(__this, method) ((  bool (*) (List_1_t1011004602 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GP_Quest>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m389005113(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1011004602 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m456723786(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1011004602 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::Add(T)
#define List_1_Add_m1283870111(__this, ___item0, method) ((  void (*) (List_1_t1011004602 *, GP_Quest_t1641883470 *, const MethodInfo*))List_1_Add_m2488140228_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m3987760690(__this, ___newCount0, method) ((  void (*) (List_1_t1011004602 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1637362050(__this, ___collection0, method) ((  void (*) (List_1_t1011004602 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1491783810(__this, ___enumerable0, method) ((  void (*) (List_1_t1011004602 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m579303977(__this, ___collection0, method) ((  void (*) (List_1_t1011004602 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<GP_Quest>::AsReadOnly()
#define List_1_AsReadOnly_m2469914388(__this, method) ((  ReadOnlyCollection_1_t1827669162 * (*) (List_1_t1011004602 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::Clear()
#define List_1_Clear_m2982468233(__this, method) ((  void (*) (List_1_t1011004602 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GP_Quest>::Contains(T)
#define List_1_Contains_m3764336619(__this, ___item0, method) ((  bool (*) (List_1_t1011004602 *, GP_Quest_t1641883470 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::CopyTo(T[])
#define List_1_CopyTo_m114733416(__this, ___array0, method) ((  void (*) (List_1_t1011004602 *, GP_QuestU5BU5D_t2503736251*, const MethodInfo*))List_1_CopyTo_m626830950_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m3651687717(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1011004602 *, GP_QuestU5BU5D_t2503736251*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<GP_Quest>::Find(System.Predicate`1<T>)
#define List_1_Find_m2994556859(__this, ___match0, method) ((  GP_Quest_t1641883470 * (*) (List_1_t1011004602 *, Predicate_1_t84853585 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2905388712(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t84853585 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<GP_Quest>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1976882995(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1011004602 *, int32_t, int32_t, Predicate_1_t84853585 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<GP_Quest>::GetEnumerator()
#define List_1_GetEnumerator_m3601999688(__this, method) ((  Enumerator_t545734276  (*) (List_1_t1011004602 *, const MethodInfo*))List_1_GetEnumerator_m1854899495_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GP_Quest>::IndexOf(T)
#define List_1_IndexOf_m4018045925(__this, ___item0, method) ((  int32_t (*) (List_1_t1011004602 *, GP_Quest_t1641883470 *, const MethodInfo*))List_1_IndexOf_m1888505567_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m2759495068(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1011004602 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m2892061185(__this, ___index0, method) ((  void (*) (List_1_t1011004602 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::Insert(System.Int32,T)
#define List_1_Insert_m3637273658(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1011004602 *, int32_t, GP_Quest_t1641883470 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2508738471(__this, ___collection0, method) ((  void (*) (List_1_t1011004602 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<GP_Quest>::Remove(T)
#define List_1_Remove_m639632196(__this, ___item0, method) ((  bool (*) (List_1_t1011004602 *, GP_Quest_t1641883470 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<GP_Quest>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1450596988(__this, ___match0, method) ((  int32_t (*) (List_1_t1011004602 *, Predicate_1_t84853585 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1515636054(__this, ___index0, method) ((  void (*) (List_1_t1011004602 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2639322385_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::Reverse()
#define List_1_Reverse_m90891342(__this, method) ((  void (*) (List_1_t1011004602 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::Sort()
#define List_1_Sort_m630409342(__this, method) ((  void (*) (List_1_t1011004602 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1549473482(__this, ___comparer0, method) ((  void (*) (List_1_t1011004602 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m4129335267(__this, ___comparison0, method) ((  void (*) (List_1_t1011004602 *, Comparison_1_t2903622321 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<GP_Quest>::ToArray()
#define List_1_ToArray_m3233932935(__this, method) ((  GP_QuestU5BU5D_t2503736251* (*) (List_1_t1011004602 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::TrimExcess()
#define List_1_TrimExcess_m415014373(__this, method) ((  void (*) (List_1_t1011004602 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GP_Quest>::get_Capacity()
#define List_1_get_Capacity_m371341003(__this, method) ((  int32_t (*) (List_1_t1011004602 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m4045323394(__this, ___value0, method) ((  void (*) (List_1_t1011004602 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<GP_Quest>::get_Count()
#define List_1_get_Count_m2027136603(__this, method) ((  int32_t (*) (List_1_t1011004602 *, const MethodInfo*))List_1_get_Count_m4253763168_gshared)(__this, method)
// T System.Collections.Generic.List`1<GP_Quest>::get_Item(System.Int32)
#define List_1_get_Item_m853496506(__this, ___index0, method) ((  GP_Quest_t1641883470 * (*) (List_1_t1011004602 *, int32_t, const MethodInfo*))List_1_get_Item_m905507485_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GP_Quest>::set_Item(System.Int32,T)
#define List_1_set_Item_m3909310821(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1011004602 *, int32_t, GP_Quest_t1641883470 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
