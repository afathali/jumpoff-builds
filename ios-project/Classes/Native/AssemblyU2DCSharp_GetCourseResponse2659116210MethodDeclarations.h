﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GetCourseResponse
struct GetCourseResponse_t2659116210;
// System.String
struct String_t;
// DeleteCourseRequest
struct DeleteCourseRequest_t1683798751;
// System.Collections.Generic.List`1<Course>
struct List_1_t2852233831;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_DeleteCourseRequest1683798751.h"

// System.Void GetCourseResponse::.ctor()
extern "C"  void GetCourseResponse__ctor_m58116547 (GetCourseResponse_t2659116210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GetCourseResponse::get_Success()
extern "C"  bool GetCourseResponse_get_Success_m4218795773 (GetCourseResponse_t2659116210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetCourseResponse::set_Success(System.Boolean)
extern "C"  void GetCourseResponse_set_Success_m2417013016 (GetCourseResponse_t2659116210 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GetCourseResponse::get_ErrorMessage()
extern "C"  String_t* GetCourseResponse_get_ErrorMessage_m1570933970 (GetCourseResponse_t2659116210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetCourseResponse::set_ErrorMessage(System.String)
extern "C"  void GetCourseResponse_set_ErrorMessage_m2842872895 (GetCourseResponse_t2659116210 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DeleteCourseRequest GetCourseResponse::get_Request()
extern "C"  DeleteCourseRequest_t1683798751 * GetCourseResponse_get_Request_m1255578133 (GetCourseResponse_t2659116210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetCourseResponse::set_Request(DeleteCourseRequest)
extern "C"  void GetCourseResponse_set_Request_m3835214314 (GetCourseResponse_t2659116210 * __this, DeleteCourseRequest_t1683798751 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Course> GetCourseResponse::get_Courses()
extern "C"  List_1_t2852233831 * GetCourseResponse_get_Courses_m1863204836 (GetCourseResponse_t2659116210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetCourseResponse::set_Courses(System.Collections.Generic.List`1<Course>)
extern "C"  void GetCourseResponse_set_Courses_m2817282223 (GetCourseResponse_t2659116210 * __this, List_1_t2852233831 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
