﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MultiplayerManagerExample
struct MultiplayerManagerExample_t3644649635;
// GK_Invite
struct GK_Invite_t22070530;
// System.String[]
struct StringU5BU5D_t1642385972;
// GK_Player[]
struct GK_PlayerU5BU5D_t1642762691;
// GK_Player
struct GK_Player_t2782008294;
// GK_RTM_Match
struct GK_RTM_Match_t873568990;
// GK_RTM_MatchStartedResult
struct GK_RTM_MatchStartedResult_t833698690;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_MatchType1493351924.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Invite22070530.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Player2782008294.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_PlayerConnectionS2434478783.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_RTM_Match873568990.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_RTM_MatchStartedRe833698690.h"

// System.Void MultiplayerManagerExample::.ctor()
extern "C"  void MultiplayerManagerExample__ctor_m2260456708 (MultiplayerManagerExample_t3644649635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplayerManagerExample::Awake()
extern "C"  void MultiplayerManagerExample_Awake_m824765741 (MultiplayerManagerExample_t3644649635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplayerManagerExample::HandleActionPlayerAcceptedInvitation(GK_MatchType,GK_Invite)
extern "C"  void MultiplayerManagerExample_HandleActionPlayerAcceptedInvitation_m2469731069 (MultiplayerManagerExample_t3644649635 * __this, int32_t ___math0, GK_Invite_t22070530 * ___invite1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplayerManagerExample::HandleActionPlayerRequestedMatchWithRecipients(GK_MatchType,System.String[],GK_Player[])
extern "C"  void MultiplayerManagerExample_HandleActionPlayerRequestedMatchWithRecipients_m2753242588 (MultiplayerManagerExample_t3644649635 * __this, int32_t ___matchType0, StringU5BU5D_t1642385972* ___recepientIds1, GK_PlayerU5BU5D_t1642762691* ___recepients2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplayerManagerExample::OnGUI()
extern "C"  void MultiplayerManagerExample_OnGUI_m958734236 (MultiplayerManagerExample_t3644649635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplayerManagerExample::HandleActionPlayerStateChanged(GK_Player,GK_PlayerConnectionState,GK_RTM_Match)
extern "C"  void MultiplayerManagerExample_HandleActionPlayerStateChanged_m1121815767 (MultiplayerManagerExample_t3644649635 * __this, GK_Player_t2782008294 * ___player0, int32_t ___state1, GK_RTM_Match_t873568990 * ___match2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplayerManagerExample::HandleActionMatchStarted(GK_RTM_MatchStartedResult)
extern "C"  void MultiplayerManagerExample_HandleActionMatchStarted_m562767932 (MultiplayerManagerExample_t3644649635 * __this, GK_RTM_MatchStartedResult_t833698690 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplayerManagerExample::HandleActionDataReceived(GK_Player,System.Byte[])
extern "C"  void MultiplayerManagerExample_HandleActionDataReceived_m3444302010 (MultiplayerManagerExample_t3644649635 * __this, GK_Player_t2782008294 * ___player0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
