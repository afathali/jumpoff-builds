﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLib.JpegInfo
struct JpegInfo_t3114827956;

#include "codegen/il2cpp-codegen.h"

// System.Void ExifLib.JpegInfo::.ctor()
extern "C"  void JpegInfo__ctor_m2982750930 (JpegInfo_t3114827956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
