﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AddressBookController
struct AddressBookController_t3183854505;
// System.Action
struct Action_t3226471752;
// System.Collections.Generic.List`1<AndroidContactInfo>
struct List_1_t1487793311;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AddressBookController::.ctor()
extern "C"  void AddressBookController__ctor_m2364926144 (AddressBookController_t3183854505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBookController::.cctor()
extern "C"  void AddressBookController__cctor_m1196602059 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBookController::add_OnContactsLoadedAction(System.Action)
extern "C"  void AddressBookController_add_OnContactsLoadedAction_m1451951944 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBookController::remove_OnContactsLoadedAction(System.Action)
extern "C"  void AddressBookController_remove_OnContactsLoadedAction_m1608756679 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBookController::Awake()
extern "C"  void AddressBookController_Awake_m2869585673 (AddressBookController_t3183854505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBookController::LoadContacts()
extern "C"  void AddressBookController_LoadContacts_m653246961 (AddressBookController_t3183854505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<AndroidContactInfo> AddressBookController::get_contacts()
extern "C"  List_1_t1487793311 * AddressBookController_get_contacts_m1438951030 (AddressBookController_t3183854505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBookController::OnContactsLoaded(System.String)
extern "C"  void AddressBookController_OnContactsLoaded_m3860653291 (AddressBookController_t3183854505 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBookController::parseContacts(System.String)
extern "C"  void AddressBookController_parseContacts_m2761720228 (AddressBookController_t3183854505 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AddressBookController::trimString(System.String,System.Int32)
extern "C"  String_t* AddressBookController_trimString_m901393537 (AddressBookController_t3183854505 * __this, String_t* ___str0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AddressBookController::isValid(System.String)
extern "C"  bool AddressBookController_isValid_m892635698 (AddressBookController_t3183854505 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AddressBookController::havePhoto(System.String)
extern "C"  bool AddressBookController_havePhoto_m808456522 (AddressBookController_t3183854505 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AddressBookController::get_isLoaded()
extern "C"  bool AddressBookController_get_isLoaded_m485543856 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBookController::<OnContactsLoadedAction>m__17()
extern "C"  void AddressBookController_U3COnContactsLoadedActionU3Em__17_m2605328632 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
