﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Action
struct Action_t3226471752;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSStoreProductView
struct  IOSStoreProductView_t607200268  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> IOSStoreProductView::_ids
	List_1_t1398341365 * ____ids_0;
	// System.Int32 IOSStoreProductView::_id
	int32_t ____id_1;
	// System.Action IOSStoreProductView::Loaded
	Action_t3226471752 * ___Loaded_2;
	// System.Action IOSStoreProductView::LoadFailed
	Action_t3226471752 * ___LoadFailed_3;
	// System.Action IOSStoreProductView::Appeared
	Action_t3226471752 * ___Appeared_4;
	// System.Action IOSStoreProductView::Dismissed
	Action_t3226471752 * ___Dismissed_5;

public:
	inline static int32_t get_offset_of__ids_0() { return static_cast<int32_t>(offsetof(IOSStoreProductView_t607200268, ____ids_0)); }
	inline List_1_t1398341365 * get__ids_0() const { return ____ids_0; }
	inline List_1_t1398341365 ** get_address_of__ids_0() { return &____ids_0; }
	inline void set__ids_0(List_1_t1398341365 * value)
	{
		____ids_0 = value;
		Il2CppCodeGenWriteBarrier(&____ids_0, value);
	}

	inline static int32_t get_offset_of__id_1() { return static_cast<int32_t>(offsetof(IOSStoreProductView_t607200268, ____id_1)); }
	inline int32_t get__id_1() const { return ____id_1; }
	inline int32_t* get_address_of__id_1() { return &____id_1; }
	inline void set__id_1(int32_t value)
	{
		____id_1 = value;
	}

	inline static int32_t get_offset_of_Loaded_2() { return static_cast<int32_t>(offsetof(IOSStoreProductView_t607200268, ___Loaded_2)); }
	inline Action_t3226471752 * get_Loaded_2() const { return ___Loaded_2; }
	inline Action_t3226471752 ** get_address_of_Loaded_2() { return &___Loaded_2; }
	inline void set_Loaded_2(Action_t3226471752 * value)
	{
		___Loaded_2 = value;
		Il2CppCodeGenWriteBarrier(&___Loaded_2, value);
	}

	inline static int32_t get_offset_of_LoadFailed_3() { return static_cast<int32_t>(offsetof(IOSStoreProductView_t607200268, ___LoadFailed_3)); }
	inline Action_t3226471752 * get_LoadFailed_3() const { return ___LoadFailed_3; }
	inline Action_t3226471752 ** get_address_of_LoadFailed_3() { return &___LoadFailed_3; }
	inline void set_LoadFailed_3(Action_t3226471752 * value)
	{
		___LoadFailed_3 = value;
		Il2CppCodeGenWriteBarrier(&___LoadFailed_3, value);
	}

	inline static int32_t get_offset_of_Appeared_4() { return static_cast<int32_t>(offsetof(IOSStoreProductView_t607200268, ___Appeared_4)); }
	inline Action_t3226471752 * get_Appeared_4() const { return ___Appeared_4; }
	inline Action_t3226471752 ** get_address_of_Appeared_4() { return &___Appeared_4; }
	inline void set_Appeared_4(Action_t3226471752 * value)
	{
		___Appeared_4 = value;
		Il2CppCodeGenWriteBarrier(&___Appeared_4, value);
	}

	inline static int32_t get_offset_of_Dismissed_5() { return static_cast<int32_t>(offsetof(IOSStoreProductView_t607200268, ___Dismissed_5)); }
	inline Action_t3226471752 * get_Dismissed_5() const { return ___Dismissed_5; }
	inline Action_t3226471752 ** get_address_of_Dismissed_5() { return &___Dismissed_5; }
	inline void set_Dismissed_5(Action_t3226471752 * value)
	{
		___Dismissed_5 = value;
		Il2CppCodeGenWriteBarrier(&___Dismissed_5, value);
	}
};

struct IOSStoreProductView_t607200268_StaticFields
{
public:
	// System.Action IOSStoreProductView::<>f__am$cache6
	Action_t3226471752 * ___U3CU3Ef__amU24cache6_6;
	// System.Action IOSStoreProductView::<>f__am$cache7
	Action_t3226471752 * ___U3CU3Ef__amU24cache7_7;
	// System.Action IOSStoreProductView::<>f__am$cache8
	Action_t3226471752 * ___U3CU3Ef__amU24cache8_8;
	// System.Action IOSStoreProductView::<>f__am$cache9
	Action_t3226471752 * ___U3CU3Ef__amU24cache9_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(IOSStoreProductView_t607200268_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(IOSStoreProductView_t607200268_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(IOSStoreProductView_t607200268_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(IOSStoreProductView_t607200268_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
