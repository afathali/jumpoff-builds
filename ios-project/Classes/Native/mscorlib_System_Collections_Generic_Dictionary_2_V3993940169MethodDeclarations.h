﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va400334773MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3510690980(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3993940169 *, Dictionary_2_t995913030 *, const MethodInfo*))ValueCollection__ctor_m882866357_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3289715962(__this, ___item0, method) ((  void (*) (ValueCollection_t3993940169 *, SimpleClassObject_t1988087395 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1903672223_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1968390493(__this, method) ((  void (*) (ValueCollection_t3993940169 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3271993638_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m824641690(__this, ___item0, method) ((  bool (*) (ValueCollection_t3993940169 *, SimpleClassObject_t1988087395 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3958350925_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1836660257(__this, ___item0, method) ((  bool (*) (ValueCollection_t3993940169 *, SimpleClassObject_t1988087395 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m98888100_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2638792143(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3993940169 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1604400448_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1479210103(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3993940169 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2627730402_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4051425406(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3993940169 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1073215119_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1007389277(__this, method) ((  bool (*) (ValueCollection_t3993940169 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1325719984_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3651767503(__this, method) ((  bool (*) (ValueCollection_t3993940169 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4041633470_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3141101419(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3993940169 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3927965720_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m4233170009(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3993940169 *, SimpleClassObjectU5BU5D_t2694722738*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1460341186_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::GetEnumerator()
#define ValueCollection_GetEnumerator_m515955524(__this, method) ((  Enumerator_t2682445794  (*) (ValueCollection_t3993940169 *, const MethodInfo*))ValueCollection_GetEnumerator_m941805197_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject>::get_Count()
#define ValueCollection_get_Count_m79642519(__this, method) ((  int32_t (*) (ValueCollection_t3993940169 *, const MethodInfo*))ValueCollection_get_Count_m90930038_gshared)(__this, method)
