﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonConverter>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3026764676(__this, ___l0, method) ((  void (*) (Enumerator_t867911556 *, List_1_t1333181882 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonConverter>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2633416286(__this, method) ((  void (*) (Enumerator_t867911556 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonConverter>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1531957614(__this, method) ((  Il2CppObject * (*) (Enumerator_t867911556 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonConverter>::Dispose()
#define Enumerator_Dispose_m1957120817(__this, method) ((  void (*) (Enumerator_t867911556 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonConverter>::VerifyState()
#define Enumerator_VerifyState_m646581014(__this, method) ((  void (*) (Enumerator_t867911556 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonConverter>::MoveNext()
#define Enumerator_MoveNext_m3879021722(__this, method) ((  bool (*) (Enumerator_t867911556 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonConverter>::get_Current()
#define Enumerator_get_Current_m1608996425(__this, method) ((  JsonConverter_t1964060750 * (*) (Enumerator_t867911556 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
