﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlDocumentWrapper
struct XmlDocumentWrapper_t559857997;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// Newtonsoft.Json.Converters.IXmlNode
struct IXmlNode_t1152344546;
// System.String
struct String_t;
// Newtonsoft.Json.Converters.IXmlElement
struct IXmlElement_t2005722770;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Newtonsoft.Json.Converters.XmlDocumentWrapper::.ctor(System.Xml.XmlDocument)
extern "C"  void XmlDocumentWrapper__ctor_m110163512 (XmlDocumentWrapper_t559857997 * __this, XmlDocument_t3649534162 * ___document0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateComment(System.String)
extern "C"  Il2CppObject * XmlDocumentWrapper_CreateComment_m4003885672 (XmlDocumentWrapper_t559857997 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateTextNode(System.String)
extern "C"  Il2CppObject * XmlDocumentWrapper_CreateTextNode_m112505910 (XmlDocumentWrapper_t559857997 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateCDataSection(System.String)
extern "C"  Il2CppObject * XmlDocumentWrapper_CreateCDataSection_m890601197 (XmlDocumentWrapper_t559857997 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateWhitespace(System.String)
extern "C"  Il2CppObject * XmlDocumentWrapper_CreateWhitespace_m1426856488 (XmlDocumentWrapper_t559857997 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateSignificantWhitespace(System.String)
extern "C"  Il2CppObject * XmlDocumentWrapper_CreateSignificantWhitespace_m337406497 (XmlDocumentWrapper_t559857997 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateXmlDeclaration(System.String,System.String,System.String)
extern "C"  Il2CppObject * XmlDocumentWrapper_CreateXmlDeclaration_m1314364426 (XmlDocumentWrapper_t559857997 * __this, String_t* ___version0, String_t* ___encoding1, String_t* ___standalone2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateProcessingInstruction(System.String,System.String)
extern "C"  Il2CppObject * XmlDocumentWrapper_CreateProcessingInstruction_m1609807528 (XmlDocumentWrapper_t559857997 * __this, String_t* ___target0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateElement(System.String)
extern "C"  Il2CppObject * XmlDocumentWrapper_CreateElement_m3248439343 (XmlDocumentWrapper_t559857997 * __this, String_t* ___elementName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateElement(System.String,System.String)
extern "C"  Il2CppObject * XmlDocumentWrapper_CreateElement_m3474815185 (XmlDocumentWrapper_t559857997 * __this, String_t* ___qualifiedName0, String_t* ___namespaceURI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateAttribute(System.String,System.String)
extern "C"  Il2CppObject * XmlDocumentWrapper_CreateAttribute_m827445145 (XmlDocumentWrapper_t559857997 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateAttribute(System.String,System.String,System.String)
extern "C"  Il2CppObject * XmlDocumentWrapper_CreateAttribute_m4261192907 (XmlDocumentWrapper_t559857997 * __this, String_t* ___qualifiedName0, String_t* ___namespaceURI1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlDocumentWrapper::get_DocumentElement()
extern "C"  Il2CppObject * XmlDocumentWrapper_get_DocumentElement_m89617847 (XmlDocumentWrapper_t559857997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
