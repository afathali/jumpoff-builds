﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonSerializerSettings
struct JsonSerializerSettings_t842388167;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>
struct IList_1_t2505001351;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t614887283;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t2044502214;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t3985864818;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_t1956922769;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ReferenceLoopHan1017855894.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MissingMemberHand367517353.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObjectCreationHa3720134651.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_NullValueHandlin3618095365.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_DefaultValueHand3457895463.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_PreserveReferenc3019117943.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_TypeNameHandling1331513094.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Fo999493661.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ConstructorHandl4150360451.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio3985864818.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Void Newtonsoft.Json.JsonSerializerSettings::.ctor()
extern "C"  void JsonSerializerSettings__ctor_m1102410465 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::.cctor()
extern "C"  void JsonSerializerSettings__cctor_m1099063184 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializerSettings::get_ReferenceLoopHandling()
extern "C"  int32_t JsonSerializerSettings_get_ReferenceLoopHandling_m3154481570 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_ReferenceLoopHandling(Newtonsoft.Json.ReferenceLoopHandling)
extern "C"  void JsonSerializerSettings_set_ReferenceLoopHandling_m334252067 (JsonSerializerSettings_t842388167 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializerSettings::get_MissingMemberHandling()
extern "C"  int32_t JsonSerializerSettings_get_MissingMemberHandling_m3494172418 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_MissingMemberHandling(Newtonsoft.Json.MissingMemberHandling)
extern "C"  void JsonSerializerSettings_set_MissingMemberHandling_m1523643971 (JsonSerializerSettings_t842388167 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializerSettings::get_ObjectCreationHandling()
extern "C"  int32_t JsonSerializerSettings_get_ObjectCreationHandling_m3371595666 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_ObjectCreationHandling(Newtonsoft.Json.ObjectCreationHandling)
extern "C"  void JsonSerializerSettings_set_ObjectCreationHandling_m717011635 (JsonSerializerSettings_t842388167 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializerSettings::get_NullValueHandling()
extern "C"  int32_t JsonSerializerSettings_get_NullValueHandling_m2026424898 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_NullValueHandling(Newtonsoft.Json.NullValueHandling)
extern "C"  void JsonSerializerSettings_set_NullValueHandling_m714511555 (JsonSerializerSettings_t842388167 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializerSettings::get_DefaultValueHandling()
extern "C"  int32_t JsonSerializerSettings_get_DefaultValueHandling_m4234303906 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_DefaultValueHandling(Newtonsoft.Json.DefaultValueHandling)
extern "C"  void JsonSerializerSettings_set_DefaultValueHandling_m3555143059 (JsonSerializerSettings_t842388167 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter> Newtonsoft.Json.JsonSerializerSettings::get_Converters()
extern "C"  Il2CppObject* JsonSerializerSettings_get_Converters_m1726295900 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_Converters(System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>)
extern "C"  void JsonSerializerSettings_set_Converters_m3851503043 (JsonSerializerSettings_t842388167 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializerSettings::get_PreserveReferencesHandling()
extern "C"  int32_t JsonSerializerSettings_get_PreserveReferencesHandling_m2058908866 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_PreserveReferencesHandling(Newtonsoft.Json.PreserveReferencesHandling)
extern "C"  void JsonSerializerSettings_set_PreserveReferencesHandling_m3767252931 (JsonSerializerSettings_t842388167 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializerSettings::get_TypeNameHandling()
extern "C"  int32_t JsonSerializerSettings_get_TypeNameHandling_m3084480952 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_TypeNameHandling(Newtonsoft.Json.TypeNameHandling)
extern "C"  void JsonSerializerSettings_set_TypeNameHandling_m4018092391 (JsonSerializerSettings_t842388167 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.JsonSerializerSettings::get_TypeNameAssemblyFormat()
extern "C"  int32_t JsonSerializerSettings_get_TypeNameAssemblyFormat_m2128219842 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_TypeNameAssemblyFormat(System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern "C"  void JsonSerializerSettings_set_TypeNameAssemblyFormat_m283433731 (JsonSerializerSettings_t842388167 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializerSettings::get_ConstructorHandling()
extern "C"  int32_t JsonSerializerSettings_get_ConstructorHandling_m816003778 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_ConstructorHandling(Newtonsoft.Json.ConstructorHandling)
extern "C"  void JsonSerializerSettings_set_ConstructorHandling_m1535944259 (JsonSerializerSettings_t842388167 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializerSettings::get_ContractResolver()
extern "C"  Il2CppObject * JsonSerializerSettings_get_ContractResolver_m3850771301 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern "C"  void JsonSerializerSettings_set_ContractResolver_m1008841372 (JsonSerializerSettings_t842388167 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializerSettings::get_ReferenceResolver()
extern "C"  Il2CppObject * JsonSerializerSettings_get_ReferenceResolver_m1113185573 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_ReferenceResolver(Newtonsoft.Json.Serialization.IReferenceResolver)
extern "C"  void JsonSerializerSettings_set_ReferenceResolver_m3013859790 (JsonSerializerSettings_t842388167 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializerSettings::get_Binder()
extern "C"  SerializationBinder_t3985864818 * JsonSerializerSettings_get_Binder_m2792135622 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_Binder(System.Runtime.Serialization.SerializationBinder)
extern "C"  void JsonSerializerSettings_set_Binder_m407598275 (JsonSerializerSettings_t842388167 * __this, SerializationBinder_t3985864818 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializerSettings::get_Error()
extern "C"  EventHandler_1_t1956922769 * JsonSerializerSettings_get_Error_m3884832591 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern "C"  void JsonSerializerSettings_set_Error_m868545334 (JsonSerializerSettings_t842388167 * __this, EventHandler_1_t1956922769 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializerSettings::get_Context()
extern "C"  StreamingContext_t1417235061  JsonSerializerSettings_get_Context_m302916676 (JsonSerializerSettings_t842388167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::set_Context(System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonSerializerSettings_set_Context_m2630457233 (JsonSerializerSettings_t842388167 * __this, StreamingContext_t1417235061  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
