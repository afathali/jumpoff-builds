﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Course>::.ctor()
#define List_1__ctor_m3369925898(__this, method) ((  void (*) (List_1_t2852233831 *, const MethodInfo*))List_1__ctor_m1864370736_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Course>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1342485760(__this, ___collection0, method) ((  void (*) (List_1_t2852233831 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2396561940_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Course>::.ctor(System.Int32)
#define List_1__ctor_m113968874(__this, ___capacity0, method) ((  void (*) (List_1_t2852233831 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Course>::.cctor()
#define List_1__cctor_m3066806090(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Course>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1387467855(__this, method) ((  Il2CppObject* (*) (List_1_t2852233831 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Course>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m4093450971(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2852233831 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Course>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3162615428(__this, method) ((  Il2CppObject * (*) (List_1_t2852233831 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Course>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m328933707(__this, ___item0, method) ((  int32_t (*) (List_1_t2852233831 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Course>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m4034775635(__this, ___item0, method) ((  bool (*) (List_1_t2852233831 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Course>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2239826017(__this, ___item0, method) ((  int32_t (*) (List_1_t2852233831 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Course>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m404925854(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2852233831 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Course>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m233779000(__this, ___item0, method) ((  void (*) (List_1_t2852233831 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Course>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m872876568(__this, method) ((  bool (*) (List_1_t2852233831 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Course>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1343845043(__this, method) ((  bool (*) (List_1_t2852233831 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Course>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2104352599(__this, method) ((  Il2CppObject * (*) (List_1_t2852233831 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Course>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m953839090(__this, method) ((  bool (*) (List_1_t2852233831 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Course>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3950112415(__this, method) ((  bool (*) (List_1_t2852233831 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Course>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2627694820(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2852233831 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Course>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1267038613(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2852233831 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Course>::Add(T)
#define List_1_Add_m2827829266(__this, ___item0, method) ((  void (*) (List_1_t2852233831 *, Course_t3483112699 *, const MethodInfo*))List_1_Add_m2488140228_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Course>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m554371591(__this, ___newCount0, method) ((  void (*) (List_1_t2852233831 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Course>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m2008308023(__this, ___collection0, method) ((  void (*) (List_1_t2852233831 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Course>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3138786135(__this, ___enumerable0, method) ((  void (*) (List_1_t2852233831 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Course>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3533944244(__this, ___collection0, method) ((  void (*) (List_1_t2852233831 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Course>::AsReadOnly()
#define List_1_AsReadOnly_m3649609463(__this, method) ((  ReadOnlyCollection_1_t3668898391 * (*) (List_1_t2852233831 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Course>::Clear()
#define List_1_Clear_m1141428462(__this, method) ((  void (*) (List_1_t2852233831 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Course>::Contains(T)
#define List_1_Contains_m1438918844(__this, ___item0, method) ((  bool (*) (List_1_t2852233831 *, Course_t3483112699 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Course>::CopyTo(T[])
#define List_1_CopyTo_m3238625847(__this, ___array0, method) ((  void (*) (List_1_t2852233831 *, CourseU5BU5D_t1357564090*, const MethodInfo*))List_1_CopyTo_m626830950_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<Course>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m186842294(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2852233831 *, CourseU5BU5D_t1357564090*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Course>::Find(System.Predicate`1<T>)
#define List_1_Find_m3371467424(__this, ___match0, method) ((  Course_t3483112699 * (*) (List_1_t2852233831 *, Predicate_1_t1926082814 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Course>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2412440211(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1926082814 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Course>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1439624694(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2852233831 *, int32_t, int32_t, Predicate_1_t1926082814 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Course>::GetEnumerator()
#define List_1_GetEnumerator_m1157433577(__this, method) ((  Enumerator_t2386963505  (*) (List_1_t2852233831 *, const MethodInfo*))List_1_GetEnumerator_m1854899495_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Course>::IndexOf(T)
#define List_1_IndexOf_m3172857560(__this, ___item0, method) ((  int32_t (*) (List_1_t2852233831 *, Course_t3483112699 *, const MethodInfo*))List_1_IndexOf_m1888505567_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Course>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3794848759(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2852233831 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Course>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1030327446(__this, ___index0, method) ((  void (*) (List_1_t2852233831 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Course>::Insert(System.Int32,T)
#define List_1_Insert_m1705030713(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2852233831 *, int32_t, Course_t3483112699 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Course>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m3836729304(__this, ___collection0, method) ((  void (*) (List_1_t2852233831 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Course>::Remove(T)
#define List_1_Remove_m3513033265(__this, ___item0, method) ((  bool (*) (List_1_t2852233831 *, Course_t3483112699 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Course>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2201400939(__this, ___match0, method) ((  int32_t (*) (List_1_t2852233831 *, Predicate_1_t1926082814 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Course>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m582745797(__this, ___index0, method) ((  void (*) (List_1_t2852233831 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2639322385_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Course>::Reverse()
#define List_1_Reverse_m2398538223(__this, method) ((  void (*) (List_1_t2852233831 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Course>::Sort()
#define List_1_Sort_m1547709945(__this, method) ((  void (*) (List_1_t2852233831 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Course>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m283975621(__this, ___comparer0, method) ((  void (*) (List_1_t2852233831 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Course>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2919800978(__this, ___comparison0, method) ((  void (*) (List_1_t2852233831 *, Comparison_1_t449884254 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Course>::ToArray()
#define List_1_ToArray_m3657445548(__this, method) ((  CourseU5BU5D_t1357564090* (*) (List_1_t2852233831 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Course>::TrimExcess()
#define List_1_TrimExcess_m3614934068(__this, method) ((  void (*) (List_1_t2852233831 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Course>::get_Capacity()
#define List_1_get_Capacity_m3987137994(__this, method) ((  int32_t (*) (List_1_t2852233831 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Course>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m110170775(__this, ___value0, method) ((  void (*) (List_1_t2852233831 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Course>::get_Count()
#define List_1_get_Count_m3373796636(__this, method) ((  int32_t (*) (List_1_t2852233831 *, const MethodInfo*))List_1_get_Count_m4253763168_gshared)(__this, method)
// T System.Collections.Generic.List`1<Course>::get_Item(System.Int32)
#define List_1_get_Item_m3276820379(__this, ___index0, method) ((  Course_t3483112699 * (*) (List_1_t2852233831 *, int32_t, const MethodInfo*))List_1_get_Item_m905507485_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Course>::set_Item(System.Int32,T)
#define List_1_set_Item_m4034219348(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2852233831 *, int32_t, Course_t3483112699 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
