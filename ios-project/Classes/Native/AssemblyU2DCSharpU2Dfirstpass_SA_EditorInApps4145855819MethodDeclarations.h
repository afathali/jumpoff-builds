﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_EditorInApps
struct SA_EditorInApps_t4145855819;
// System.String
struct String_t;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// SA_InApps_EditorUIController
struct SA_InApps_EditorUIController_t628772056;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SA_EditorInApps::.ctor()
extern "C"  void SA_EditorInApps__ctor_m2551493084 (SA_EditorInApps_t4145855819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorInApps::.cctor()
extern "C"  void SA_EditorInApps__cctor_m2469562007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_EditorInApps::ShowInAppPopup(System.String,System.String,System.String,System.Action`1<System.Boolean>)
extern "C"  void SA_EditorInApps_ShowInAppPopup_m3255254618 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___describtion1, String_t* ___price2, Action_1_t3627374100 * ___OnComplete3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SA_InApps_EditorUIController SA_EditorInApps::get_EditorUI()
extern "C"  SA_InApps_EditorUIController_t628772056 * SA_EditorInApps_get_EditorUI_m2498527461 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
