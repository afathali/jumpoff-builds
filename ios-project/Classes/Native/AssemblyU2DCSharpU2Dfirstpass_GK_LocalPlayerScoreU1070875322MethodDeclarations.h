﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_LocalPlayerScoreUpdateListener
struct GK_LocalPlayerScoreUpdateListener_t1070875322;
// System.String
struct String_t;
// GK_Score
struct GK_Score_t1529008873;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Score1529008873.h"

// System.Void GK_LocalPlayerScoreUpdateListener::.ctor(System.Int32,System.String,System.Boolean)
extern "C"  void GK_LocalPlayerScoreUpdateListener__ctor_m4165174707 (GK_LocalPlayerScoreUpdateListener_t1070875322 * __this, int32_t ___requestId0, String_t* ___leaderboardId1, bool ___isInternal2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_LocalPlayerScoreUpdateListener::ReportScoreUpdate(GK_Score)
extern "C"  void GK_LocalPlayerScoreUpdateListener_ReportScoreUpdate_m3202133371 (GK_LocalPlayerScoreUpdateListener_t1070875322 * __this, GK_Score_t1529008873 * ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_LocalPlayerScoreUpdateListener::ReportScoreUpdateFail(System.String)
extern "C"  void GK_LocalPlayerScoreUpdateListener_ReportScoreUpdateFail_m2327562900 (GK_LocalPlayerScoreUpdateListener_t1070875322 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GK_LocalPlayerScoreUpdateListener::get_RequestId()
extern "C"  int32_t GK_LocalPlayerScoreUpdateListener_get_RequestId_m3072439662 (GK_LocalPlayerScoreUpdateListener_t1070875322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_LocalPlayerScoreUpdateListener::DispatchUpdate()
extern "C"  void GK_LocalPlayerScoreUpdateListener_DispatchUpdate_m4288213940 (GK_LocalPlayerScoreUpdateListener_t1070875322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
