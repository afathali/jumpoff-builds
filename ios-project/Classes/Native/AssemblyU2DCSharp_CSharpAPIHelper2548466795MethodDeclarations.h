﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSharpAPIHelper
struct CSharpAPIHelper_t2548466795;

#include "codegen/il2cpp-codegen.h"

// System.Void CSharpAPIHelper::.ctor()
extern "C"  void CSharpAPIHelper__ctor_m2847406240 (CSharpAPIHelper_t2548466795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSharpAPIHelper::ConnectToPlaySertivce()
extern "C"  void CSharpAPIHelper_ConnectToPlaySertivce_m3203981516 (CSharpAPIHelper_t2548466795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSharpAPIHelper::OnPlayerConnected()
extern "C"  void CSharpAPIHelper_OnPlayerConnected_m2069094699 (CSharpAPIHelper_t2548466795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSharpAPIHelper::OnPlayerDisconnected()
extern "C"  void CSharpAPIHelper_OnPlayerDisconnected_m1677604041 (CSharpAPIHelper_t2548466795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
