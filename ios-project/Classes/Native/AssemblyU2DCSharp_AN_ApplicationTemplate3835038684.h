﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,AN_ActivityTemplate>
struct Dictionary_2_t2388442510;

#include "AssemblyU2DCSharp_AN_BaseTemplate2111071391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AN_ApplicationTemplate
struct  AN_ApplicationTemplate_t3835038684  : public AN_BaseTemplate_t2111071391
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,AN_ActivityTemplate> AN_ApplicationTemplate::_activities
	Dictionary_2_t2388442510 * ____activities_2;

public:
	inline static int32_t get_offset_of__activities_2() { return static_cast<int32_t>(offsetof(AN_ApplicationTemplate_t3835038684, ____activities_2)); }
	inline Dictionary_2_t2388442510 * get__activities_2() const { return ____activities_2; }
	inline Dictionary_2_t2388442510 ** get_address_of__activities_2() { return &____activities_2; }
	inline void set__activities_2(Dictionary_2_t2388442510 * value)
	{
		____activities_2 = value;
		Il2CppCodeGenWriteBarrier(&____activities_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
