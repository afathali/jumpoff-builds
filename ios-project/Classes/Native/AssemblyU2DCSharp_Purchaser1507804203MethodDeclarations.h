﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Purchaser
struct Purchaser_t1507804203;

#include "codegen/il2cpp-codegen.h"

// System.Void Purchaser::.ctor()
extern "C"  void Purchaser__ctor_m517928038 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
