﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"

// System.Void System.Action`1<GooglePlayResult>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m2058431505(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t2899269018 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m584977596_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<GooglePlayResult>::Invoke(T)
#define Action_1_Invoke_m2061120794(__this, ___obj0, method) ((  void (*) (Action_1_t2899269018 *, GooglePlayResult_t3097469636 *, const MethodInfo*))Action_1_Invoke_m101203496_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<GooglePlayResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m3850256425(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t2899269018 *, GooglePlayResult_t3097469636 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1305519803_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<GooglePlayResult>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m1516374782(__this, ___result0, method) ((  void (*) (Action_1_t2899269018 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m2057605070_gshared)(__this, ___result0, method)
