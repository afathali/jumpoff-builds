﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginResponse
struct LoginResponse_t1319408382;
// System.String
struct String_t;
// LoginRequest
struct LoginRequest_t2273172322;
// User
struct User_t719925459;
// System.Collections.Generic.List`1<Course>
struct List_1_t2852233831;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_LoginRequest2273172322.h"
#include "AssemblyU2DCSharp_User719925459.h"

// System.Void LoginResponse::.ctor()
extern "C"  void LoginResponse__ctor_m3360639251 (LoginResponse_t1319408382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoginResponse::get_Success()
extern "C"  bool LoginResponse_get_Success_m1980197201 (LoginResponse_t1319408382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginResponse::set_Success(System.Boolean)
extern "C"  void LoginResponse_set_Success_m1219986784 (LoginResponse_t1319408382 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LoginResponse::get_ErrorMessage()
extern "C"  String_t* LoginResponse_get_ErrorMessage_m822330998 (LoginResponse_t1319408382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginResponse::set_ErrorMessage(System.String)
extern "C"  void LoginResponse_set_ErrorMessage_m2470119175 (LoginResponse_t1319408382 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LoginRequest LoginResponse::get_Request()
extern "C"  LoginRequest_t2273172322 * LoginResponse_get_Request_m3983446854 (LoginResponse_t1319408382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginResponse::set_Request(LoginRequest)
extern "C"  void LoginResponse_set_Request_m3855648509 (LoginResponse_t1319408382 * __this, LoginRequest_t2273172322 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoginResponse::get_IsNewPassword()
extern "C"  bool LoginResponse_get_IsNewPassword_m1575878551 (LoginResponse_t1319408382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginResponse::set_IsNewPassword(System.Boolean)
extern "C"  void LoginResponse_set_IsNewPassword_m624112576 (LoginResponse_t1319408382 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LoginResponse::get_AutoLogin()
extern "C"  String_t* LoginResponse_get_AutoLogin_m1708474807 (LoginResponse_t1319408382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginResponse::set_AutoLogin(System.String)
extern "C"  void LoginResponse_set_AutoLogin_m727608156 (LoginResponse_t1319408382 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// User LoginResponse::get_User()
extern "C"  User_t719925459 * LoginResponse_get_User_m3611965889 (LoginResponse_t1319408382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginResponse::set_User(User)
extern "C"  void LoginResponse_set_User_m118749806 (LoginResponse_t1319408382 * __this, User_t719925459 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Course> LoginResponse::get_Courses()
extern "C"  List_1_t2852233831 * LoginResponse_get_Courses_m3796189864 (LoginResponse_t1319408382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginResponse::set_Courses(System.Collections.Generic.List`1<Course>)
extern "C"  void LoginResponse_set_Courses_m4235259303 (LoginResponse_t1319408382 * __this, List_1_t2852233831 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
