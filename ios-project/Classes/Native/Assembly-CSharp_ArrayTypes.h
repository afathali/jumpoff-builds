﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// JumpPoint
struct JumpPoint_t1009860606;
// AN_PropertyTemplate
struct AN_PropertyTemplate_t2393149441;
// AN_ActivityTemplate
struct AN_ActivityTemplate_t3380616875;
// GoogleProductTemplate
struct GoogleProductTemplate_t1112616324;
// GooglePurchaseTemplate
struct GooglePurchaseTemplate_t2609331866;
// AndroidContactInfo
struct AndroidContactInfo_t2118672179;
// LocalNotificationTemplate
struct LocalNotificationTemplate_t2880890350;
// GP_Event
struct GP_Event_t323143668;
// GP_Invite
struct GP_Invite_t626929087;
// GooglePlayerTemplate
struct GooglePlayerTemplate_t2506317812;
// GPLeaderBoard
struct GPLeaderBoard_t3649577886;
// GPAchievement
struct GPAchievement_t4279788054;
// GPGameRequest
struct GPGameRequest_t4158842942;
// GP_Participant
struct GP_Participant_t2884377673;
// GP_Quest
struct GP_Quest_t1641883470;
// GP_RTM_ReliableMessageListener
struct GP_RTM_ReliableMessageListener_t1343560679;
// GP_SnapshotMeta
struct GP_SnapshotMeta_t1354779439;
// GP_TBM_Match
struct GP_TBM_Match_t1275077981;
// GP_ParticipantResult
struct GP_ParticipantResult_t2469018720;
// AN_PlusButton
struct AN_PlusButton_t1370758440;
// GoogleMobileAdBanner
struct GoogleMobileAdBanner_t1323818958;
// DefaultPreviewButton
struct DefaultPreviewButton_t12674677;
// SA_PartisipantUI
struct SA_PartisipantUI_t3115332592;
// SA_FriendUI
struct SA_FriendUI_t3775341837;
// CustomPlayerUIRow
struct CustomPlayerUIRow_t1758969648;
// CustomLeaderboardFiledsHolder
struct CustomLeaderboardFiledsHolder_t4123707333;
// GPScore
struct GPScore_t3219488889;
// FB_AppRequest
struct FB_AppRequest_t501312625;
// FB_UserInfo
struct FB_UserInfo_t2704578078;
// FB_Score
struct FB_Score_t1450841581;
// TwitterUserInfo
struct TwitterUserInfo_t87370740;
// TweetTemplate
struct TweetTemplate_t3444491657;
// AndroidADBanner
struct AndroidADBanner_t886219444;
// GP_LocalPlayerScoreUpdateListener
struct GP_LocalPlayerScoreUpdateListener_t158126391;
// FB_LikeInfo
struct FB_LikeInfo_t3213199078;
// FB_Permission
struct FB_Permission_t1872772122;
// DustinHorne.Json.Examples.JNSimpleObjectModel
struct JNSimpleObjectModel_t1947941312;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t1964060750;
// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject
struct SimpleClassObject_t1988087395;
// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase
struct SampleBase_t2925764113;
// Newtonsoft.Json.Bson.BsonReader/ContainerContext
struct ContainerContext_t2144264477;
// Newtonsoft.Json.Bson.BsonProperty
struct BsonProperty_t1491061775;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_t3582361217;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>
struct BidirectionalDictionary_2_t2874502390;
// Newtonsoft.Json.Converters.IXmlNode
struct IXmlNode_t1152344546;
// Newtonsoft.Json.Schema.JsonSchemaModel
struct JsonSchemaModel_t708894576;
// Newtonsoft.Json.JsonValidatingReader/SchemaScope
struct SchemaScope_t4218888543;
// Newtonsoft.Json.JsonWriter/State[]
struct StateU5BU5D_t823784359;
// Newtonsoft.Json.Linq.JToken
struct JToken_t2552644013;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t3772113849;
// Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema
struct TypeSchema_t460462092;
// Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>
struct EnumValue_1_t589082027;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t2712067825;
// Newtonsoft.Json.Schema.JsonSchemaNode
struct JsonSchemaNode_t3866831117;
// Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>
struct EnumValue_1_t2589200904;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1566984540;
// Newtonsoft.Json.JsonContainerAttribute
struct JsonContainerAttribute_t47210975;
// System.Runtime.Serialization.DataContractAttribute
struct DataContractAttribute_t3332255060;
// System.Runtime.Serialization.DataMemberAttribute
struct DataMemberAttribute_t2677019114;
// MCGBehaviour/InjectionContainer/TypeData
struct TypeData_t212254090;
// Course
struct Course_t3483112699;
// Jump
struct Jump_t114869516;
// Switch/ChangeCallback
struct ChangeCallback_t1172037850;

#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_JumpPoint1009860606.h"
#include "AssemblyU2DCSharp_AN_PropertyTemplate2393149441.h"
#include "AssemblyU2DCSharp_AN_ActivityTemplate3380616875.h"
#include "AssemblyU2DCSharp_GoogleProductTemplate1112616324.h"
#include "AssemblyU2DCSharp_GooglePurchaseTemplate2609331866.h"
#include "AssemblyU2DCSharp_AndroidContactInfo2118672179.h"
#include "AssemblyU2DCSharp_AN_ManifestPermission1772110202.h"
#include "AssemblyU2DCSharp_LocalNotificationTemplate2880890350.h"
#include "AssemblyU2DCSharp_AN_PermissionState838201224.h"
#include "AssemblyU2DCSharp_GP_Event323143668.h"
#include "AssemblyU2DCSharp_GP_Invite626929087.h"
#include "AssemblyU2DCSharp_GooglePlayerTemplate2506317812.h"
#include "AssemblyU2DCSharp_GPLeaderBoard3649577886.h"
#include "AssemblyU2DCSharp_GPAchievement4279788054.h"
#include "AssemblyU2DCSharp_GPGameRequest4158842942.h"
#include "AssemblyU2DCSharp_GP_Participant2884377673.h"
#include "AssemblyU2DCSharp_GP_QuestsSelect2071002355.h"
#include "AssemblyU2DCSharp_GP_Quest1641883470.h"
#include "AssemblyU2DCSharp_GP_RTM_ReliableMessageListener1343560679.h"
#include "AssemblyU2DCSharp_GP_SnapshotMeta1354779439.h"
#include "AssemblyU2DCSharp_GP_TBM_Match1275077981.h"
#include "AssemblyU2DCSharp_GP_ParticipantResult2469018720.h"
#include "AssemblyU2DCSharp_GP_TBM_MatchTurnStatus2221730550.h"
#include "AssemblyU2DCSharp_AN_PlusButton1370758440.h"
#include "AssemblyU2DCSharp_DefaultPreviewButton12674677.h"
#include "AssemblyU2DCSharp_SA_PartisipantUI3115332592.h"
#include "AssemblyU2DCSharp_SA_FriendUI3775341837.h"
#include "AssemblyU2DCSharp_CustomPlayerUIRow1758969648.h"
#include "AssemblyU2DCSharp_CustomLeaderboardFiledsHolder4123707333.h"
#include "AssemblyU2DCSharp_GPScore3219488889.h"
#include "AssemblyU2DCSharp_FB_AppRequest501312625.h"
#include "AssemblyU2DCSharp_FB_UserInfo2704578078.h"
#include "AssemblyU2DCSharp_FB_Score1450841581.h"
#include "AssemblyU2DCSharp_TwitterUserInfo87370740.h"
#include "AssemblyU2DCSharp_TweetTemplate3444491657.h"
#include "AssemblyU2DCSharp_AndroidADBanner886219444.h"
#include "AssemblyU2DCSharp_GP_LocalPlayerScoreUpdateListener158126391.h"
#include "AssemblyU2DCSharp_FB_LikeInfo3213199078.h"
#include "AssemblyU2DCSharp_FB_Permission1872772122.h"
#include "AssemblyU2DCSharp_FB_ProfileImageSize3003328130.h"
#include "AssemblyU2DCSharp_DustinHorne_Json_Examples_JNSimp1947941312.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter1964060750.h"
#include "AssemblyU2DCSharp_Assets_DustinHorne_JsonDotNetUni1988087395.h"
#include "AssemblyU2DCSharp_Assets_DustinHorne_JsonDotNetUni2925764113.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonReader_2144264477.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonPropert1491061775.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonToken3582361217.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Bidire2874502390.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType1307827213.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema708894576.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonValidatingRe4218888543.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter_State3285832914.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken2552644013.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3772113849.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460462092.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVal589082027.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2712067825.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3866831117.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVa2589200904.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js1566984540.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De3055062677.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso220200932.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonContainerAttri47210975.h"
#include "AssemblyU2DCSharp_System_Runtime_Serialization_Dat3332255060.h"
#include "AssemblyU2DCSharp_System_Runtime_Serialization_Dat2677019114.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Conver1788482786.h"
#include "AssemblyU2DCSharp_MCGBehaviour_InjectionContainer_T212254090.h"
#include "AssemblyU2DCSharp_Course3483112699.h"
#include "AssemblyU2DCSharp_Jump114869516.h"
#include "AssemblyU2DCSharp_Switch_ChangeCallback1172037850.h"

#pragma once
// JumpPoint[]
struct JumpPointU5BU5D_t4004280907  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JumpPoint_t1009860606 * m_Items[1];

public:
	inline JumpPoint_t1009860606 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JumpPoint_t1009860606 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JumpPoint_t1009860606 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AN_PropertyTemplate[]
struct AN_PropertyTemplateU5BU5D_t3873559260  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AN_PropertyTemplate_t2393149441 * m_Items[1];

public:
	inline AN_PropertyTemplate_t2393149441 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AN_PropertyTemplate_t2393149441 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AN_PropertyTemplate_t2393149441 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AN_ActivityTemplate[]
struct AN_ActivityTemplateU5BU5D_t453014090  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AN_ActivityTemplate_t3380616875 * m_Items[1];

public:
	inline AN_ActivityTemplate_t3380616875 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AN_ActivityTemplate_t3380616875 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AN_ActivityTemplate_t3380616875 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GoogleProductTemplate[]
struct GoogleProductTemplateU5BU5D_t4247222253  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GoogleProductTemplate_t1112616324 * m_Items[1];

public:
	inline GoogleProductTemplate_t1112616324 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GoogleProductTemplate_t1112616324 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GoogleProductTemplate_t1112616324 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePurchaseTemplate[]
struct GooglePurchaseTemplateU5BU5D_t2629042623  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GooglePurchaseTemplate_t2609331866 * m_Items[1];

public:
	inline GooglePurchaseTemplate_t2609331866 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GooglePurchaseTemplate_t2609331866 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GooglePurchaseTemplate_t2609331866 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AndroidContactInfo[]
struct AndroidContactInfoU5BU5D_t1609938594  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AndroidContactInfo_t2118672179 * m_Items[1];

public:
	inline AndroidContactInfo_t2118672179 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AndroidContactInfo_t2118672179 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AndroidContactInfo_t2118672179 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AN_ManifestPermission[]
struct AN_ManifestPermissionU5BU5D_t246101855  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// LocalNotificationTemplate[]
struct LocalNotificationTemplateU5BU5D_t895700635  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LocalNotificationTemplate_t2880890350 * m_Items[1];

public:
	inline LocalNotificationTemplate_t2880890350 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LocalNotificationTemplate_t2880890350 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LocalNotificationTemplate_t2880890350 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AN_PermissionState[]
struct AN_PermissionStateU5BU5D_t700971353  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// GP_Event[]
struct GP_EventU5BU5D_t2968614333  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GP_Event_t323143668 * m_Items[1];

public:
	inline GP_Event_t323143668 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GP_Event_t323143668 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GP_Event_t323143668 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GP_Invite[]
struct GP_InviteU5BU5D_t554303590  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GP_Invite_t626929087 * m_Items[1];

public:
	inline GP_Invite_t626929087 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GP_Invite_t626929087 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GP_Invite_t626929087 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayerTemplate[]
struct GooglePlayerTemplateU5BU5D_t318995901  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GooglePlayerTemplate_t2506317812 * m_Items[1];

public:
	inline GooglePlayerTemplate_t2506317812 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GooglePlayerTemplate_t2506317812 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GooglePlayerTemplate_t2506317812 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GPLeaderBoard[]
struct GPLeaderBoardU5BU5D_t1313633323  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GPLeaderBoard_t3649577886 * m_Items[1];

public:
	inline GPLeaderBoard_t3649577886 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GPLeaderBoard_t3649577886 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GPLeaderBoard_t3649577886 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GPAchievement[]
struct GPAchievementU5BU5D_t3647591635  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GPAchievement_t4279788054 * m_Items[1];

public:
	inline GPAchievement_t4279788054 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GPAchievement_t4279788054 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GPAchievement_t4279788054 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GPGameRequest[]
struct GPGameRequestU5BU5D_t983558411  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GPGameRequest_t4158842942 * m_Items[1];

public:
	inline GPGameRequest_t4158842942 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GPGameRequest_t4158842942 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GPGameRequest_t4158842942 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GP_Participant[]
struct GP_ParticipantU5BU5D_t405562484  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GP_Participant_t2884377673 * m_Items[1];

public:
	inline GP_Participant_t2884377673 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GP_Participant_t2884377673 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GP_Participant_t2884377673 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GP_QuestsSelect[]
struct GP_QuestsSelectU5BU5D_t3003656674  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// GP_Quest[]
struct GP_QuestU5BU5D_t2503736251  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GP_Quest_t1641883470 * m_Items[1];

public:
	inline GP_Quest_t1641883470 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GP_Quest_t1641883470 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GP_Quest_t1641883470 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GP_RTM_ReliableMessageListener[]
struct GP_RTM_ReliableMessageListenerU5BU5D_t1632439454  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GP_RTM_ReliableMessageListener_t1343560679 * m_Items[1];

public:
	inline GP_RTM_ReliableMessageListener_t1343560679 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GP_RTM_ReliableMessageListener_t1343560679 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GP_RTM_ReliableMessageListener_t1343560679 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GP_SnapshotMeta[]
struct GP_SnapshotMetaU5BU5D_t1408896822  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GP_SnapshotMeta_t1354779439 * m_Items[1];

public:
	inline GP_SnapshotMeta_t1354779439 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GP_SnapshotMeta_t1354779439 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GP_SnapshotMeta_t1354779439 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GP_TBM_Match[]
struct GP_TBM_MatchU5BU5D_t3298218128  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GP_TBM_Match_t1275077981 * m_Items[1];

public:
	inline GP_TBM_Match_t1275077981 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GP_TBM_Match_t1275077981 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GP_TBM_Match_t1275077981 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GP_ParticipantResult[]
struct GP_ParticipantResultU5BU5D_t353789473  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GP_ParticipantResult_t2469018720 * m_Items[1];

public:
	inline GP_ParticipantResult_t2469018720 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GP_ParticipantResult_t2469018720 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GP_ParticipantResult_t2469018720 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GP_TBM_MatchTurnStatus[]
struct GP_TBM_MatchTurnStatusU5BU5D_t2451090547  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// AN_PlusButton[]
struct AN_PlusButtonU5BU5D_t3096985913  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AN_PlusButton_t1370758440 * m_Items[1];

public:
	inline AN_PlusButton_t1370758440 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AN_PlusButton_t1370758440 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AN_PlusButton_t1370758440 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GoogleMobileAdBanner[]
struct GoogleMobileAdBannerU5BU5D_t1395183931  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DefaultPreviewButton[]
struct DefaultPreviewButtonU5BU5D_t3800739864  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DefaultPreviewButton_t12674677 * m_Items[1];

public:
	inline DefaultPreviewButton_t12674677 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DefaultPreviewButton_t12674677 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DefaultPreviewButton_t12674677 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SA_PartisipantUI[]
struct SA_PartisipantUIU5BU5D_t1392135761  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SA_PartisipantUI_t3115332592 * m_Items[1];

public:
	inline SA_PartisipantUI_t3115332592 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SA_PartisipantUI_t3115332592 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SA_PartisipantUI_t3115332592 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SA_FriendUI[]
struct SA_FriendUIU5BU5D_t2705386528  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SA_FriendUI_t3775341837 * m_Items[1];

public:
	inline SA_FriendUI_t3775341837 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SA_FriendUI_t3775341837 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SA_FriendUI_t3775341837 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CustomPlayerUIRow[]
struct CustomPlayerUIRowU5BU5D_t745336337  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomPlayerUIRow_t1758969648 * m_Items[1];

public:
	inline CustomPlayerUIRow_t1758969648 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CustomPlayerUIRow_t1758969648 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CustomPlayerUIRow_t1758969648 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CustomLeaderboardFiledsHolder[]
struct CustomLeaderboardFiledsHolderU5BU5D_t960860040  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomLeaderboardFiledsHolder_t4123707333 * m_Items[1];

public:
	inline CustomLeaderboardFiledsHolder_t4123707333 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CustomLeaderboardFiledsHolder_t4123707333 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CustomLeaderboardFiledsHolder_t4123707333 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GPScore[]
struct GPScoreU5BU5D_t382433668  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GPScore_t3219488889 * m_Items[1];

public:
	inline GPScore_t3219488889 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GPScore_t3219488889 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GPScore_t3219488889 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FB_AppRequest[]
struct FB_AppRequestU5BU5D_t2026981036  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FB_AppRequest_t501312625 * m_Items[1];

public:
	inline FB_AppRequest_t501312625 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FB_AppRequest_t501312625 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FB_AppRequest_t501312625 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FB_UserInfo[]
struct FB_UserInfoU5BU5D_t273115051  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FB_UserInfo_t2704578078 * m_Items[1];

public:
	inline FB_UserInfo_t2704578078 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FB_UserInfo_t2704578078 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FB_UserInfo_t2704578078 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FB_Score[]
struct FB_ScoreU5BU5D_t2445458368  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FB_Score_t1450841581 * m_Items[1];

public:
	inline FB_Score_t1450841581 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FB_Score_t1450841581 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FB_Score_t1450841581 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TwitterUserInfo[]
struct TwitterUserInfoU5BU5D_t3063218621  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TwitterUserInfo_t87370740 * m_Items[1];

public:
	inline TwitterUserInfo_t87370740 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TwitterUserInfo_t87370740 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TwitterUserInfo_t87370740 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TweetTemplate[]
struct TweetTemplateU5BU5D_t1596829236  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TweetTemplate_t3444491657 * m_Items[1];

public:
	inline TweetTemplate_t3444491657 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TweetTemplate_t3444491657 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TweetTemplate_t3444491657 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AndroidADBanner[]
struct AndroidADBannerU5BU5D_t250550269  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AndroidADBanner_t886219444 * m_Items[1];

public:
	inline AndroidADBanner_t886219444 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AndroidADBanner_t886219444 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AndroidADBanner_t886219444 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GP_LocalPlayerScoreUpdateListener[]
struct GP_LocalPlayerScoreUpdateListenerU5BU5D_t1077004302  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GP_LocalPlayerScoreUpdateListener_t158126391 * m_Items[1];

public:
	inline GP_LocalPlayerScoreUpdateListener_t158126391 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GP_LocalPlayerScoreUpdateListener_t158126391 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GP_LocalPlayerScoreUpdateListener_t158126391 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FB_LikeInfo[]
struct FB_LikeInfoU5BU5D_t993260227  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FB_LikeInfo_t3213199078 * m_Items[1];

public:
	inline FB_LikeInfo_t3213199078 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FB_LikeInfo_t3213199078 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FB_LikeInfo_t3213199078 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FB_Permission[]
struct FB_PermissionU5BU5D_t1837096511  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FB_Permission_t1872772122 * m_Items[1];

public:
	inline FB_Permission_t1872772122 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FB_Permission_t1872772122 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FB_Permission_t1872772122 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FB_ProfileImageSize[]
struct FB_ProfileImageSizeU5BU5D_t4110693687  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// DustinHorne.Json.Examples.JNSimpleObjectModel[]
struct JNSimpleObjectModelU5BU5D_t4082493249  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JNSimpleObjectModel_t1947941312 * m_Items[1];

public:
	inline JNSimpleObjectModel_t1947941312 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JNSimpleObjectModel_t1947941312 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JNSimpleObjectModel_t1947941312 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t3128012475  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonConverter_t1964060750 * m_Items[1];

public:
	inline JsonConverter_t1964060750 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonConverter_t1964060750 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonConverter_t1964060750 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject[]
struct SimpleClassObjectU5BU5D_t2694722738  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SimpleClassObject_t1988087395 * m_Items[1];

public:
	inline SimpleClassObject_t1988087395 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SimpleClassObject_t1988087395 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SimpleClassObject_t1988087395 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase[]
struct SampleBaseU5BU5D_t1910045324  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SampleBase_t2925764113 * m_Items[1];

public:
	inline SampleBase_t2925764113 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SampleBase_t2925764113 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SampleBase_t2925764113 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Bson.BsonReader/ContainerContext[]
struct ContainerContextU5BU5D_t1119940048  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ContainerContext_t2144264477 * m_Items[1];

public:
	inline ContainerContext_t2144264477 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ContainerContext_t2144264477 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ContainerContext_t2144264477 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Bson.BsonProperty[]
struct BsonPropertyU5BU5D_t2436566742  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BsonProperty_t1491061775 * m_Items[1];

public:
	inline BsonProperty_t1491061775 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BsonProperty_t1491061775 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BsonProperty_t1491061775 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Bson.BsonToken[]
struct BsonTokenU5BU5D_t1813371484  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BsonToken_t3582361217 * m_Items[1];

public:
	inline BsonToken_t3582361217 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BsonToken_t3582361217 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BsonToken_t3582361217 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>[]
struct BidirectionalDictionary_2U5BU5D_t4047757939  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BidirectionalDictionary_2_t2874502390 * m_Items[1];

public:
	inline BidirectionalDictionary_2_t2874502390 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BidirectionalDictionary_2_t2874502390 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BidirectionalDictionary_2_t2874502390 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Converters.IXmlNode[]
struct IXmlNodeU5BU5D_t212713559  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t1368357152  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Schema.JsonSchemaModel[]
struct JsonSchemaModelU5BU5D_t2789475537  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonSchemaModel_t708894576 * m_Items[1];

public:
	inline JsonSchemaModel_t708894576 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonSchemaModel_t708894576 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonSchemaModel_t708894576 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.JsonValidatingReader/SchemaScope[]
struct SchemaScopeU5BU5D_t2093853254  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SchemaScope_t4218888543 * m_Items[1];

public:
	inline SchemaScope_t4218888543 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SchemaScope_t4218888543 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SchemaScope_t4218888543 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t2725846494  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StateU5BU5D_t823784359* m_Items[1];

public:
	inline StateU5BU5D_t823784359* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StateU5BU5D_t823784359** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StateU5BU5D_t823784359* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.JsonWriter/State[]
struct StateU5BU5D_t823784359  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Linq.JToken[]
struct JTokenU5BU5D_t1832626432  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JToken_t2552644013 * m_Items[1];

public:
	inline JToken_t2552644013 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JToken_t2552644013 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JToken_t2552644013 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Schema.JsonSchema[]
struct JsonSchemaU5BU5D_t1337909572  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonSchema_t3772113849 * m_Items[1];

public:
	inline JsonSchema_t3772113849 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonSchema_t3772113849 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonSchema_t3772113849 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Schema.JsonSchemaType[]
struct JsonSchemaTypeU5BU5D_t3104176164  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema[]
struct TypeSchemaU5BU5D_t2039097925  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeSchema_t460462092 * m_Items[1];

public:
	inline TypeSchema_t460462092 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeSchema_t460462092 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeSchema_t460462092 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>[]
struct EnumValue_1U5BU5D_t2087503690  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EnumValue_1_t589082027 * m_Items[1];

public:
	inline EnumValue_1_t589082027 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EnumValue_1_t589082027 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EnumValue_1_t589082027 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Serialization.JsonProperty[]
struct JsonPropertyU5BU5D_t4130290220  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonProperty_t2712067825 * m_Items[1];

public:
	inline JsonProperty_t2712067825 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonProperty_t2712067825 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonProperty_t2712067825 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Schema.JsonSchemaNode[]
struct JsonSchemaNodeU5BU5D_t2651346976  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonSchemaNode_t3866831117 * m_Items[1];

public:
	inline JsonSchemaNode_t3866831117 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonSchemaNode_t3866831117 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonSchemaNode_t3866831117 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>[]
struct EnumValue_1U5BU5D_t3039067353  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EnumValue_1_t2589200904 * m_Items[1];

public:
	inline EnumValue_1_t2589200904 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EnumValue_1_t2589200904 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EnumValue_1_t2589200904 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Serialization.JsonContract[]
struct JsonContractU5BU5D_t1422195125  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonContract_t1566984540 * m_Items[1];

public:
	inline JsonContract_t1566984540 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonContract_t1566984540 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonContract_t1566984540 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey[]
struct TypeNameKeyU5BU5D_t3669902200  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeNameKey_t3055062677  m_Items[1];

public:
	inline TypeNameKey_t3055062677  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeNameKey_t3055062677 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeNameKey_t3055062677  value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence[]
struct PropertyPresenceU5BU5D_t1324757005  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.JsonContainerAttribute[]
struct JsonContainerAttributeU5BU5D_t1039718342  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonContainerAttribute_t47210975 * m_Items[1];

public:
	inline JsonContainerAttribute_t47210975 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonContainerAttribute_t47210975 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonContainerAttribute_t47210975 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Serialization.DataContractAttribute[]
struct DataContractAttributeU5BU5D_t1980231389  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataContractAttribute_t3332255060 * m_Items[1];

public:
	inline DataContractAttribute_t3332255060 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataContractAttribute_t3332255060 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataContractAttribute_t3332255060 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Serialization.DataMemberAttribute[]
struct DataMemberAttributeU5BU5D_t3256444719  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataMemberAttribute_t2677019114 * m_Items[1];

public:
	inline DataMemberAttribute_t2677019114 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataMemberAttribute_t2677019114 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataMemberAttribute_t2677019114 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey[]
struct TypeConvertKeyU5BU5D_t3485131095  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeConvertKey_t1788482786  m_Items[1];

public:
	inline TypeConvertKey_t1788482786  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeConvertKey_t1788482786 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeConvertKey_t1788482786  value)
	{
		m_Items[index] = value;
	}
};
// MCGBehaviour/InjectionContainer/TypeData[]
struct TypeDataU5BU5D_t2270787855  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeData_t212254090 * m_Items[1];

public:
	inline TypeData_t212254090 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeData_t212254090 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeData_t212254090 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Course[]
struct CourseU5BU5D_t1357564090  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Course_t3483112699 * m_Items[1];

public:
	inline Course_t3483112699 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Course_t3483112699 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Course_t3483112699 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Jump[]
struct JumpU5BU5D_t3319408965  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Jump_t114869516 * m_Items[1];

public:
	inline Jump_t114869516 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Jump_t114869516 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Jump_t114869516 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Switch/ChangeCallback[]
struct ChangeCallbackU5BU5D_t1413959295  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ChangeCallback_t1172037850 * m_Items[1];

public:
	inline ChangeCallback_t1172037850 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ChangeCallback_t1172037850 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ChangeCallback_t1172037850 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
