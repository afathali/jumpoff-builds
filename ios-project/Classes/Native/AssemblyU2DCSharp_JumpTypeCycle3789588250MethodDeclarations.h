﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JumpTypeCycle
struct JumpTypeCycle_t3789588250;

#include "codegen/il2cpp-codegen.h"

// System.Void JumpTypeCycle::.ctor()
extern "C"  void JumpTypeCycle__ctor_m2197782397 (JumpTypeCycle_t3789588250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpTypeCycle::Start()
extern "C"  void JumpTypeCycle_Start_m1472045237 (JumpTypeCycle_t3789588250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpTypeCycle::init()
extern "C"  void JumpTypeCycle_init_m468501663 (JumpTypeCycle_t3789588250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpTypeCycle::setType(System.Int32)
extern "C"  void JumpTypeCycle_setType_m401951248 (JumpTypeCycle_t3789588250 * __this, int32_t ___itype0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpTypeCycle::Update()
extern "C"  void JumpTypeCycle_Update_m1408588376 (JumpTypeCycle_t3789588250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpTypeCycle::<init>m__108()
extern "C"  void JumpTypeCycle_U3CinitU3Em__108_m4278154489 (JumpTypeCycle_t3789588250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
