﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void Extensions::SetSize(UnityEngine.RectTransform,UnityEngine.Vector2)
extern "C"  void Extensions_SetSize_m3842241509 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___trans0, Vector2_t2243707579  ___newSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Extensions::SetHeight(UnityEngine.RectTransform,System.Single)
extern "C"  void Extensions_SetHeight_m365128674 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___trans0, float ___newSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Extensions::SetWidth(UnityEngine.RectTransform,System.Single)
extern "C"  void Extensions_SetWidth_m1636865671 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___trans0, float ___newSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
