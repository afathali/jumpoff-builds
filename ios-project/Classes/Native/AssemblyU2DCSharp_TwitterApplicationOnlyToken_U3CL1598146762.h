﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Object
struct Il2CppObject;
// TwitterApplicationOnlyToken
struct TwitterApplicationOnlyToken_t3636409970;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwitterApplicationOnlyToken/<Load>c__Iterator8
struct  U3CLoadU3Ec__Iterator8_t1598146762  : public Il2CppObject
{
public:
	// System.String TwitterApplicationOnlyToken/<Load>c__Iterator8::<url>__0
	String_t* ___U3CurlU3E__0_0;
	// System.Byte[] TwitterApplicationOnlyToken/<Load>c__Iterator8::<plainTextBytes>__1
	ByteU5BU5D_t3397334013* ___U3CplainTextBytesU3E__1_1;
	// System.String TwitterApplicationOnlyToken/<Load>c__Iterator8::<encodedAccessToken>__2
	String_t* ___U3CencodedAccessTokenU3E__2_2;
	// UnityEngine.WWWForm TwitterApplicationOnlyToken/<Load>c__Iterator8::<form>__3
	WWWForm_t3950226929 * ___U3CformU3E__3_3;
	// UnityEngine.WWW TwitterApplicationOnlyToken/<Load>c__Iterator8::<www>__4
	WWW_t2919945039 * ___U3CwwwU3E__4_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> TwitterApplicationOnlyToken/<Load>c__Iterator8::<map>__5
	Dictionary_2_t309261261 * ___U3CmapU3E__5_5;
	// System.Int32 TwitterApplicationOnlyToken/<Load>c__Iterator8::$PC
	int32_t ___U24PC_6;
	// System.Object TwitterApplicationOnlyToken/<Load>c__Iterator8::$current
	Il2CppObject * ___U24current_7;
	// TwitterApplicationOnlyToken TwitterApplicationOnlyToken/<Load>c__Iterator8::<>f__this
	TwitterApplicationOnlyToken_t3636409970 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CurlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator8_t1598146762, ___U3CurlU3E__0_0)); }
	inline String_t* get_U3CurlU3E__0_0() const { return ___U3CurlU3E__0_0; }
	inline String_t** get_address_of_U3CurlU3E__0_0() { return &___U3CurlU3E__0_0; }
	inline void set_U3CurlU3E__0_0(String_t* value)
	{
		___U3CurlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CplainTextBytesU3E__1_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator8_t1598146762, ___U3CplainTextBytesU3E__1_1)); }
	inline ByteU5BU5D_t3397334013* get_U3CplainTextBytesU3E__1_1() const { return ___U3CplainTextBytesU3E__1_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CplainTextBytesU3E__1_1() { return &___U3CplainTextBytesU3E__1_1; }
	inline void set_U3CplainTextBytesU3E__1_1(ByteU5BU5D_t3397334013* value)
	{
		___U3CplainTextBytesU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CplainTextBytesU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CencodedAccessTokenU3E__2_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator8_t1598146762, ___U3CencodedAccessTokenU3E__2_2)); }
	inline String_t* get_U3CencodedAccessTokenU3E__2_2() const { return ___U3CencodedAccessTokenU3E__2_2; }
	inline String_t** get_address_of_U3CencodedAccessTokenU3E__2_2() { return &___U3CencodedAccessTokenU3E__2_2; }
	inline void set_U3CencodedAccessTokenU3E__2_2(String_t* value)
	{
		___U3CencodedAccessTokenU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CencodedAccessTokenU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CformU3E__3_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator8_t1598146762, ___U3CformU3E__3_3)); }
	inline WWWForm_t3950226929 * get_U3CformU3E__3_3() const { return ___U3CformU3E__3_3; }
	inline WWWForm_t3950226929 ** get_address_of_U3CformU3E__3_3() { return &___U3CformU3E__3_3; }
	inline void set_U3CformU3E__3_3(WWWForm_t3950226929 * value)
	{
		___U3CformU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__4_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator8_t1598146762, ___U3CwwwU3E__4_4)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__4_4() const { return ___U3CwwwU3E__4_4; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__4_4() { return &___U3CwwwU3E__4_4; }
	inline void set_U3CwwwU3E__4_4(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CmapU3E__5_5() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator8_t1598146762, ___U3CmapU3E__5_5)); }
	inline Dictionary_2_t309261261 * get_U3CmapU3E__5_5() const { return ___U3CmapU3E__5_5; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CmapU3E__5_5() { return &___U3CmapU3E__5_5; }
	inline void set_U3CmapU3E__5_5(Dictionary_2_t309261261 * value)
	{
		___U3CmapU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmapU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator8_t1598146762, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator8_t1598146762, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__Iterator8_t1598146762, ___U3CU3Ef__this_8)); }
	inline TwitterApplicationOnlyToken_t3636409970 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline TwitterApplicationOnlyToken_t3636409970 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(TwitterApplicationOnlyToken_t3636409970 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
