﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_GooglePlayUtilsProxy
struct AN_GooglePlayUtilsProxy_t535695022;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_GooglePlayUtilsProxy::.ctor()
extern "C"  void AN_GooglePlayUtilsProxy__ctor_m3619709701 (AN_GooglePlayUtilsProxy_t535695022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GooglePlayUtilsProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_GooglePlayUtilsProxy_CallActivityFunction_m493804112 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GooglePlayUtilsProxy::GetAdvertisingId()
extern "C"  void AN_GooglePlayUtilsProxy_GetAdvertisingId_m1876962714 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
