﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_DeviceToken
struct ISN_DeviceToken_t380973950;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ISN_DeviceToken::.ctor(System.String,System.String)
extern "C"  void ISN_DeviceToken__ctor_m1225519681 (ISN_DeviceToken_t380973950 * __this, String_t* ___base64String0, String_t* ___token1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_DeviceToken::get_DeviceId()
extern "C"  String_t* ISN_DeviceToken_get_DeviceId_m3885487542 (ISN_DeviceToken_t380973950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ISN_DeviceToken::get_Bytes()
extern "C"  ByteU5BU5D_t3397334013* ISN_DeviceToken_get_Bytes_m3889827573 (ISN_DeviceToken_t380973950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_DeviceToken::get_TokenString()
extern "C"  String_t* ISN_DeviceToken_get_TokenString_m372363223 (ISN_DeviceToken_t380973950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
