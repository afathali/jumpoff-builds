﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FB_LikesRetrieveTask
struct FB_LikesRetrieveTask_t2495592094;
// System.Action`2<FB_Result,FB_LikesRetrieveTask>
struct Action_2_t3591491;
// System.String
struct String_t;
// FB_Result
struct FB_Result_t838248372;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_FB_Result838248372.h"
#include "AssemblyU2DCSharp_FB_LikesRetrieveTask2495592094.h"

// System.Void FB_LikesRetrieveTask::.ctor()
extern "C"  void FB_LikesRetrieveTask__ctor_m2463993311 (FB_LikesRetrieveTask_t2495592094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_LikesRetrieveTask::add_ActionComplete(System.Action`2<FB_Result,FB_LikesRetrieveTask>)
extern "C"  void FB_LikesRetrieveTask_add_ActionComplete_m3451973497 (FB_LikesRetrieveTask_t2495592094 * __this, Action_2_t3591491 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_LikesRetrieveTask::remove_ActionComplete(System.Action`2<FB_Result,FB_LikesRetrieveTask>)
extern "C"  void FB_LikesRetrieveTask_remove_ActionComplete_m1107241982 (FB_LikesRetrieveTask_t2495592094 * __this, Action_2_t3591491 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FB_LikesRetrieveTask FB_LikesRetrieveTask::Create()
extern "C"  FB_LikesRetrieveTask_t2495592094 * FB_LikesRetrieveTask_Create_m614535024 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_LikesRetrieveTask::LoadLikes(System.String)
extern "C"  void FB_LikesRetrieveTask_LoadLikes_m1697940063 (FB_LikesRetrieveTask_t2495592094 * __this, String_t* ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_LikesRetrieveTask::LoadLikes(System.String,System.String)
extern "C"  void FB_LikesRetrieveTask_LoadLikes_m1212047521 (FB_LikesRetrieveTask_t2495592094 * __this, String_t* ___userId0, String_t* ___pageId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_LikesRetrieveTask::get_userId()
extern "C"  String_t* FB_LikesRetrieveTask_get_userId_m2989183591 (FB_LikesRetrieveTask_t2495592094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_LikesRetrieveTask::OnUserLikesResult(FB_Result)
extern "C"  void FB_LikesRetrieveTask_OnUserLikesResult_m1660395100 (FB_LikesRetrieveTask_t2495592094 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_LikesRetrieveTask::<ActionComplete>m__A3(FB_Result,FB_LikesRetrieveTask)
extern "C"  void FB_LikesRetrieveTask_U3CActionCompleteU3Em__A3_m3201081663 (Il2CppObject * __this /* static, unused */, FB_Result_t838248372 * p0, FB_LikesRetrieveTask_t2495592094 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
