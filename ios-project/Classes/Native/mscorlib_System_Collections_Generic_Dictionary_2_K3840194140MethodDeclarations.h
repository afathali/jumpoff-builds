﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>
struct KeyCollection_t3840194140;
// System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>
struct Dictionary_2_t1356696369;
// System.Collections.Generic.IEnumerator`1<AN_ManifestPermission>
struct IEnumerator_1_t3542601325;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// AN_ManifestPermission[]
struct AN_ManifestPermissionU5BU5D_t246101855;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AN_ManifestPermission1772110202.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4046199807.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1101369442_gshared (KeyCollection_t3840194140 * __this, Dictionary_2_t1356696369 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m1101369442(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3840194140 *, Dictionary_2_t1356696369 *, const MethodInfo*))KeyCollection__ctor_m1101369442_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2930401916_gshared (KeyCollection_t3840194140 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2930401916(__this, ___item0, method) ((  void (*) (KeyCollection_t3840194140 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2930401916_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4225081399_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4225081399(__this, method) ((  void (*) (KeyCollection_t3840194140 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4225081399_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2116509612_gshared (KeyCollection_t3840194140 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2116509612(__this, ___item0, method) ((  bool (*) (KeyCollection_t3840194140 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2116509612_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3754536599_gshared (KeyCollection_t3840194140 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3754536599(__this, ___item0, method) ((  bool (*) (KeyCollection_t3840194140 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3754536599_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3753455025_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3753455025(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3840194140 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3753455025_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m267383841_gshared (KeyCollection_t3840194140 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m267383841(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3840194140 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m267383841_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3253189504_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3253189504(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3840194140 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3253189504_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m97127555_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m97127555(__this, method) ((  bool (*) (KeyCollection_t3840194140 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m97127555_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2743757997_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2743757997(__this, method) ((  bool (*) (KeyCollection_t3840194140 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2743757997_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m692264245_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m692264245(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3840194140 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m692264245_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3843875607_gshared (KeyCollection_t3840194140 * __this, AN_ManifestPermissionU5BU5D_t246101855* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3843875607(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3840194140 *, AN_ManifestPermissionU5BU5D_t246101855*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3843875607_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::GetEnumerator()
extern "C"  Enumerator_t4046199807  KeyCollection_GetEnumerator_m3089413580_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3089413580(__this, method) ((  Enumerator_t4046199807  (*) (KeyCollection_t3840194140 *, const MethodInfo*))KeyCollection_GetEnumerator_m3089413580_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3544792145_gshared (KeyCollection_t3840194140 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3544792145(__this, method) ((  int32_t (*) (KeyCollection_t3840194140 *, const MethodInfo*))KeyCollection_get_Count_m3544792145_gshared)(__this, method)
