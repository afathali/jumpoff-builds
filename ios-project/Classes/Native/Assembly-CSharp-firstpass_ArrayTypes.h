﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// SA.IOSDeploy.Framework
struct Framework_t4022948262;
// SA.IOSDeploy.Lib
struct Lib_t360384209;
// SA.IOSDeploy.Variable
struct Variable_t1157765046;
// SA.IOSDeploy.VariableListed
struct VariableListed_t1912381035;
// IOSProductTemplate
struct IOSProductTemplate_t1036598382;
// GK_Leaderboard
struct GK_Leaderboard_t156446466;
// GK_AchievementTemplate
struct GK_AchievementTemplate_t2296152240;
// GK_Player
struct GK_Player_t2782008294;
// GK_LeaderboardSet
struct GK_LeaderboardSet_t5314098;
// GK_FriendRequest
struct GK_FriendRequest_t4101620808;
// GK_TBM_Participant
struct GK_TBM_Participant_t3803955090;
// GK_TBM_Match
struct GK_TBM_Match_t132033130;
// GK_SavedGame
struct GK_SavedGame_t3320093620;
// GK_Score
struct GK_Score_t1529008873;
// GK_LocalPlayerScoreUpdateListener
struct GK_LocalPlayerScoreUpdateListener_t1070875322;
// GK_LeaderBoardInfo
struct GK_LeaderBoardInfo_t3670215494;
// MP_MediaItem
struct MP_MediaItem_t4025623029;
// ISN_LocalNotification
struct ISN_LocalNotification_t273186689;
// IOSStoreProductView
struct IOSStoreProductView_t607200268;
// SA.IOSNative.Contacts.PhoneNumber
struct PhoneNumber_t113354641;
// SA.IOSNative.Contacts.Contact
struct Contact_t4178394798;
// CK_Record
struct CK_Record_t3973541762;
// iCloudData
struct iCloudData_t3080637488;
// CK_Database
struct CK_Database_t243306482;
// CK_RecordID
struct CK_RecordID_t41838833;
// SA.Common.Animation.SA_iTween
struct SA_iTween_t1938847631;

#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_Framewo4022948262.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_Lib360384209.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_Variabl1157765046.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_Variabl1912381035.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSProductTemplate1036598382.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Leaderboard156446466.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_AchievementTempla2296152240.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Player2782008294.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_LeaderboardSet5314098.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_FriendRequest4101620808.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_Participant3803955090.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_Match132033130.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SavedGame3320093620.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Score1529008873.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_LocalPlayerScoreU1070875322.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_LeaderBoardInfo3670215494.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MediaItem4025623029.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_LocalNotification273186689.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreProductView607200268.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contacts113354641.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contact4178394798.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_Record3973541762.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iCloudData3080637488.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_Database243306482.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_RecordID41838833.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Animation_1938847631.h"

#pragma once
// SA.IOSDeploy.Framework[]
struct FrameworkU5BU5D_t1795155203  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Framework_t4022948262 * m_Items[1];

public:
	inline Framework_t4022948262 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Framework_t4022948262 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Framework_t4022948262 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SA.IOSDeploy.Lib[]
struct LibU5BU5D_t3198752460  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Lib_t360384209 * m_Items[1];

public:
	inline Lib_t360384209 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Lib_t360384209 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Lib_t360384209 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SA.IOSDeploy.Variable[]
struct VariableU5BU5D_t1804039347  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Variable_t1157765046 * m_Items[1];

public:
	inline Variable_t1157765046 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Variable_t1157765046 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Variable_t1157765046 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SA.IOSDeploy.VariableListed[]
struct VariableListedU5BU5D_t3352902538  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VariableListed_t1912381035 * m_Items[1];

public:
	inline VariableListed_t1912381035 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VariableListed_t1912381035 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VariableListed_t1912381035 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IOSProductTemplate[]
struct IOSProductTemplateU5BU5D_t1053838875  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IOSProductTemplate_t1036598382 * m_Items[1];

public:
	inline IOSProductTemplate_t1036598382 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IOSProductTemplate_t1036598382 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IOSProductTemplate_t1036598382 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GK_Leaderboard[]
struct GK_LeaderboardU5BU5D_t3185343159  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GK_Leaderboard_t156446466 * m_Items[1];

public:
	inline GK_Leaderboard_t156446466 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GK_Leaderboard_t156446466 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GK_Leaderboard_t156446466 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GK_AchievementTemplate[]
struct GK_AchievementTemplateU5BU5D_t2314303121  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GK_AchievementTemplate_t2296152240 * m_Items[1];

public:
	inline GK_AchievementTemplate_t2296152240 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GK_AchievementTemplate_t2296152240 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GK_AchievementTemplate_t2296152240 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GK_Player[]
struct GK_PlayerU5BU5D_t1642762691  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GK_Player_t2782008294 * m_Items[1];

public:
	inline GK_Player_t2782008294 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GK_Player_t2782008294 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GK_Player_t2782008294 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GK_LeaderboardSet[]
struct GK_LeaderboardSetU5BU5D_t3124601031  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GK_LeaderboardSet_t5314098 * m_Items[1];

public:
	inline GK_LeaderboardSet_t5314098 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GK_LeaderboardSet_t5314098 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GK_LeaderboardSet_t5314098 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GK_FriendRequest[]
struct GK_FriendRequestU5BU5D_t3232790937  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GK_FriendRequest_t4101620808 * m_Items[1];

public:
	inline GK_FriendRequest_t4101620808 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GK_FriendRequest_t4101620808 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GK_FriendRequest_t4101620808 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GK_TBM_Participant[]
struct GK_TBM_ParticipantU5BU5D_t3530245607  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GK_TBM_Participant_t3803955090 * m_Items[1];

public:
	inline GK_TBM_Participant_t3803955090 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GK_TBM_Participant_t3803955090 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GK_TBM_Participant_t3803955090 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GK_TBM_Match[]
struct GK_TBM_MatchU5BU5D_t2799099055  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GK_TBM_Match_t132033130 * m_Items[1];

public:
	inline GK_TBM_Match_t132033130 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GK_TBM_Match_t132033130 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GK_TBM_Match_t132033130 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GK_SavedGame[]
struct GK_SavedGameU5BU5D_t3001096957  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GK_SavedGame_t3320093620 * m_Items[1];

public:
	inline GK_SavedGame_t3320093620 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GK_SavedGame_t3320093620 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GK_SavedGame_t3320093620 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GK_Score[]
struct GK_ScoreU5BU5D_t1942740820  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GK_Score_t1529008873 * m_Items[1];

public:
	inline GK_Score_t1529008873 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GK_Score_t1529008873 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GK_Score_t1529008873 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GK_LocalPlayerScoreUpdateListener[]
struct GK_LocalPlayerScoreUpdateListenerU5BU5D_t1853016351  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GK_LocalPlayerScoreUpdateListener_t1070875322 * m_Items[1];

public:
	inline GK_LocalPlayerScoreUpdateListener_t1070875322 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GK_LocalPlayerScoreUpdateListener_t1070875322 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GK_LocalPlayerScoreUpdateListener_t1070875322 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GK_LeaderBoardInfo[]
struct GK_LeaderBoardInfoU5BU5D_t1012461795  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GK_LeaderBoardInfo_t3670215494 * m_Items[1];

public:
	inline GK_LeaderBoardInfo_t3670215494 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GK_LeaderBoardInfo_t3670215494 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GK_LeaderBoardInfo_t3670215494 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MP_MediaItem[]
struct MP_MediaItemU5BU5D_t1927419544  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MP_MediaItem_t4025623029 * m_Items[1];

public:
	inline MP_MediaItem_t4025623029 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MP_MediaItem_t4025623029 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MP_MediaItem_t4025623029 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ISN_LocalNotification[]
struct ISN_LocalNotificationU5BU5D_t1856943452  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ISN_LocalNotification_t273186689 * m_Items[1];

public:
	inline ISN_LocalNotification_t273186689 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ISN_LocalNotification_t273186689 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ISN_LocalNotification_t273186689 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IOSStoreProductView[]
struct IOSStoreProductViewU5BU5D_t2459004485  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IOSStoreProductView_t607200268 * m_Items[1];

public:
	inline IOSStoreProductView_t607200268 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IOSStoreProductView_t607200268 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IOSStoreProductView_t607200268 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SA.IOSNative.Contacts.PhoneNumber[]
struct PhoneNumberU5BU5D_t3545111308  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PhoneNumber_t113354641 * m_Items[1];

public:
	inline PhoneNumber_t113354641 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PhoneNumber_t113354641 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PhoneNumber_t113354641 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SA.IOSNative.Contacts.Contact[]
struct ContactU5BU5D_t2917841627  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Contact_t4178394798 * m_Items[1];

public:
	inline Contact_t4178394798 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Contact_t4178394798 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Contact_t4178394798 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CK_Record[]
struct CK_RecordU5BU5D_t231778359  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CK_Record_t3973541762 * m_Items[1];

public:
	inline CK_Record_t3973541762 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CK_Record_t3973541762 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CK_Record_t3973541762 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// iCloudData[]
struct iCloudDataU5BU5D_t955190545  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) iCloudData_t3080637488 * m_Items[1];

public:
	inline iCloudData_t3080637488 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline iCloudData_t3080637488 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, iCloudData_t3080637488 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CK_Database[]
struct CK_DatabaseU5BU5D_t3446032391  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CK_Database_t243306482 * m_Items[1];

public:
	inline CK_Database_t243306482 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CK_Database_t243306482 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CK_Database_t243306482 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CK_RecordID[]
struct CK_RecordIDU5BU5D_t4159064108  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CK_RecordID_t41838833 * m_Items[1];

public:
	inline CK_RecordID_t41838833 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CK_RecordID_t41838833 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CK_RecordID_t41838833 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SA.Common.Animation.SA_iTween[]
struct SA_iTweenU5BU5D_t2917451094  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SA_iTween_t1938847631 * m_Items[1];

public:
	inline SA_iTween_t1938847631 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SA_iTween_t1938847631 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SA_iTween_t1938847631 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
