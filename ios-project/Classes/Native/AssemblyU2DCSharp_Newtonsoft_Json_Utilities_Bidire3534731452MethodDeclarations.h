﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Bidire1212012318MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>::.ctor()
#define BidirectionalDictionary_2__ctor_m3373359907(__this, method) ((  void (*) (BidirectionalDictionary_2_t3534731452 *, const MethodInfo*))BidirectionalDictionary_2__ctor_m1119331919_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirst>,System.Collections.Generic.IEqualityComparer`1<TSecond>)
#define BidirectionalDictionary_2__ctor_m803454149(__this, ___firstEqualityComparer0, ___secondEqualityComparer1, method) ((  void (*) (BidirectionalDictionary_2_t3534731452 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))BidirectionalDictionary_2__ctor_m1806502961_gshared)(__this, ___firstEqualityComparer0, ___secondEqualityComparer1, method)
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>::Add(TFirst,TSecond)
#define BidirectionalDictionary_2_Add_m117214810(__this, ___first0, ___second1, method) ((  void (*) (BidirectionalDictionary_2_t3534731452 *, String_t*, Il2CppObject *, const MethodInfo*))BidirectionalDictionary_2_Add_m190380356_gshared)(__this, ___first0, ___second1, method)
// System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>::TryGetByFirst(TFirst,TSecond&)
#define BidirectionalDictionary_2_TryGetByFirst_m2531809171(__this, ___first0, ___second1, method) ((  bool (*) (BidirectionalDictionary_2_t3534731452 *, String_t*, Il2CppObject **, const MethodInfo*))BidirectionalDictionary_2_TryGetByFirst_m1495460279_gshared)(__this, ___first0, ___second1, method)
// System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>::TryGetBySecond(TSecond,TFirst&)
#define BidirectionalDictionary_2_TryGetBySecond_m3559756061(__this, ___second0, ___first1, method) ((  bool (*) (BidirectionalDictionary_2_t3534731452 *, Il2CppObject *, String_t**, const MethodInfo*))BidirectionalDictionary_2_TryGetBySecond_m1230387241_gshared)(__this, ___second0, ___first1, method)
