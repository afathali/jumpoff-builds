﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CourseObject
struct CourseObject_t1071976694;
// Course
struct Course_t3483112699;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Course3483112699.h"

// System.Void CourseObject::.ctor()
extern "C"  void CourseObject__ctor_m1670796103 (CourseObject_t1071976694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseObject::Start()
extern "C"  void CourseObject_Start_m923480131 (CourseObject_t1071976694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseObject::SetCourse(Course)
extern "C"  void CourseObject_SetCourse_m2211375509 (CourseObject_t1071976694 * __this, Course_t3483112699 * ____newCourse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Course CourseObject::GetCourse()
extern "C"  Course_t3483112699 * CourseObject_GetCourse_m477352440 (CourseObject_t1071976694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseObject::OpenCourse()
extern "C"  void CourseObject_OpenCourse_m2769521328 (CourseObject_t1071976694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseObject::OpenCourseDetails()
extern "C"  void CourseObject_OpenCourseDetails_m507278872 (CourseObject_t1071976694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseObject::DeleteCourse()
extern "C"  void CourseObject_DeleteCourse_m4096897863 (CourseObject_t1071976694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseObject::<Start>m__130()
extern "C"  void CourseObject_U3CStartU3Em__130_m695630492 (CourseObject_t1071976694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseObject::<Start>m__131()
extern "C"  void CourseObject_U3CStartU3Em__131_m695630459 (CourseObject_t1071976694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CourseObject::<Start>m__132()
extern "C"  void CourseObject_U3CStartU3Em__132_m695630426 (CourseObject_t1071976694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
