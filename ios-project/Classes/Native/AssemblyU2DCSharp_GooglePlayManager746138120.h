﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayerTemplate
struct GooglePlayerTemplate_t2506317812;
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayerTemplate>
struct Dictionary_2_t126129778;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<GPGameRequest>
struct List_1_t3527964074;
// System.String
struct String_t;
// System.Action`1<GP_LeaderboardResult>
struct Action_1_t1836014676;
// System.Action`1<GooglePlayResult>
struct Action_1_t2899269018;
// System.Action`1<GP_AchievementResult>
struct Action_1_t3772725756;
// System.Action`1<GooglePlayGiftRequestResult>
struct Action_1_t152001389;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.Collections.Generic.List`1<GPGameRequest>>
struct Action_1_t3329763456;
// System.Action`1<System.Collections.Generic.List`1<System.String>>
struct Action_1_t1200140747;
// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen2036982038.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayManager
struct  GooglePlayManager_t746138120  : public SA_Singleton_OLD_1_t2036982038
{
public:
	// GooglePlayerTemplate GooglePlayManager::_player
	GooglePlayerTemplate_t2506317812 * ____player_4;
	// System.Collections.Generic.Dictionary`2<System.String,GooglePlayerTemplate> GooglePlayManager::_players
	Dictionary_2_t126129778 * ____players_5;
	// System.Collections.Generic.List`1<System.String> GooglePlayManager::_friendsList
	List_1_t1398341365 * ____friendsList_6;
	// System.Collections.Generic.List`1<System.String> GooglePlayManager::_deviceGoogleAccountList
	List_1_t1398341365 * ____deviceGoogleAccountList_7;
	// System.Collections.Generic.List`1<GPGameRequest> GooglePlayManager::_gameRequests
	List_1_t3527964074 * ____gameRequests_8;
	// System.String GooglePlayManager::_loadedAuthToken
	String_t* ____loadedAuthToken_9;
	// System.String GooglePlayManager::_currentAccount
	String_t* ____currentAccount_10;

public:
	inline static int32_t get_offset_of__player_4() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120, ____player_4)); }
	inline GooglePlayerTemplate_t2506317812 * get__player_4() const { return ____player_4; }
	inline GooglePlayerTemplate_t2506317812 ** get_address_of__player_4() { return &____player_4; }
	inline void set__player_4(GooglePlayerTemplate_t2506317812 * value)
	{
		____player_4 = value;
		Il2CppCodeGenWriteBarrier(&____player_4, value);
	}

	inline static int32_t get_offset_of__players_5() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120, ____players_5)); }
	inline Dictionary_2_t126129778 * get__players_5() const { return ____players_5; }
	inline Dictionary_2_t126129778 ** get_address_of__players_5() { return &____players_5; }
	inline void set__players_5(Dictionary_2_t126129778 * value)
	{
		____players_5 = value;
		Il2CppCodeGenWriteBarrier(&____players_5, value);
	}

	inline static int32_t get_offset_of__friendsList_6() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120, ____friendsList_6)); }
	inline List_1_t1398341365 * get__friendsList_6() const { return ____friendsList_6; }
	inline List_1_t1398341365 ** get_address_of__friendsList_6() { return &____friendsList_6; }
	inline void set__friendsList_6(List_1_t1398341365 * value)
	{
		____friendsList_6 = value;
		Il2CppCodeGenWriteBarrier(&____friendsList_6, value);
	}

	inline static int32_t get_offset_of__deviceGoogleAccountList_7() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120, ____deviceGoogleAccountList_7)); }
	inline List_1_t1398341365 * get__deviceGoogleAccountList_7() const { return ____deviceGoogleAccountList_7; }
	inline List_1_t1398341365 ** get_address_of__deviceGoogleAccountList_7() { return &____deviceGoogleAccountList_7; }
	inline void set__deviceGoogleAccountList_7(List_1_t1398341365 * value)
	{
		____deviceGoogleAccountList_7 = value;
		Il2CppCodeGenWriteBarrier(&____deviceGoogleAccountList_7, value);
	}

	inline static int32_t get_offset_of__gameRequests_8() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120, ____gameRequests_8)); }
	inline List_1_t3527964074 * get__gameRequests_8() const { return ____gameRequests_8; }
	inline List_1_t3527964074 ** get_address_of__gameRequests_8() { return &____gameRequests_8; }
	inline void set__gameRequests_8(List_1_t3527964074 * value)
	{
		____gameRequests_8 = value;
		Il2CppCodeGenWriteBarrier(&____gameRequests_8, value);
	}

	inline static int32_t get_offset_of__loadedAuthToken_9() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120, ____loadedAuthToken_9)); }
	inline String_t* get__loadedAuthToken_9() const { return ____loadedAuthToken_9; }
	inline String_t** get_address_of__loadedAuthToken_9() { return &____loadedAuthToken_9; }
	inline void set__loadedAuthToken_9(String_t* value)
	{
		____loadedAuthToken_9 = value;
		Il2CppCodeGenWriteBarrier(&____loadedAuthToken_9, value);
	}

	inline static int32_t get_offset_of__currentAccount_10() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120, ____currentAccount_10)); }
	inline String_t* get__currentAccount_10() const { return ____currentAccount_10; }
	inline String_t** get_address_of__currentAccount_10() { return &____currentAccount_10; }
	inline void set__currentAccount_10(String_t* value)
	{
		____currentAccount_10 = value;
		Il2CppCodeGenWriteBarrier(&____currentAccount_10, value);
	}
};

struct GooglePlayManager_t746138120_StaticFields
{
public:
	// System.Boolean GooglePlayManager::_IsLeaderboardsDataLoaded
	bool ____IsLeaderboardsDataLoaded_11;
	// System.Action`1<GP_LeaderboardResult> GooglePlayManager::ActionScoreSubmited
	Action_1_t1836014676 * ___ActionScoreSubmited_12;
	// System.Action`1<GP_LeaderboardResult> GooglePlayManager::ActionScoresListLoaded
	Action_1_t1836014676 * ___ActionScoresListLoaded_13;
	// System.Action`1<GooglePlayResult> GooglePlayManager::ActionLeaderboardsLoaded
	Action_1_t2899269018 * ___ActionLeaderboardsLoaded_14;
	// System.Action`1<GP_AchievementResult> GooglePlayManager::ActionAchievementUpdated
	Action_1_t3772725756 * ___ActionAchievementUpdated_15;
	// System.Action`1<GooglePlayResult> GooglePlayManager::ActionFriendsListLoaded
	Action_1_t2899269018 * ___ActionFriendsListLoaded_16;
	// System.Action`1<GooglePlayResult> GooglePlayManager::ActionAchievementsLoaded
	Action_1_t2899269018 * ___ActionAchievementsLoaded_17;
	// System.Action`1<GooglePlayGiftRequestResult> GooglePlayManager::ActionSendGiftResultReceived
	Action_1_t152001389 * ___ActionSendGiftResultReceived_18;
	// System.Action GooglePlayManager::ActionRequestsInboxDialogDismissed
	Action_t3226471752 * ___ActionRequestsInboxDialogDismissed_19;
	// System.Action`1<System.Collections.Generic.List`1<GPGameRequest>> GooglePlayManager::ActionPendingGameRequestsDetected
	Action_1_t3329763456 * ___ActionPendingGameRequestsDetected_20;
	// System.Action`1<System.Collections.Generic.List`1<GPGameRequest>> GooglePlayManager::ActionGameRequestsAccepted
	Action_1_t3329763456 * ___ActionGameRequestsAccepted_21;
	// System.Action`1<System.Collections.Generic.List`1<System.String>> GooglePlayManager::ActionAvailableDeviceAccountsLoaded
	Action_1_t1200140747 * ___ActionAvailableDeviceAccountsLoaded_22;
	// System.Action`1<System.String> GooglePlayManager::ActionOAuthTokenLoaded
	Action_1_t1831019615 * ___ActionOAuthTokenLoaded_23;
	// System.Action`1<GP_LeaderboardResult> GooglePlayManager::<>f__am$cache14
	Action_1_t1836014676 * ___U3CU3Ef__amU24cache14_24;
	// System.Action`1<GP_LeaderboardResult> GooglePlayManager::<>f__am$cache15
	Action_1_t1836014676 * ___U3CU3Ef__amU24cache15_25;
	// System.Action`1<GooglePlayResult> GooglePlayManager::<>f__am$cache16
	Action_1_t2899269018 * ___U3CU3Ef__amU24cache16_26;
	// System.Action`1<GP_AchievementResult> GooglePlayManager::<>f__am$cache17
	Action_1_t3772725756 * ___U3CU3Ef__amU24cache17_27;
	// System.Action`1<GooglePlayResult> GooglePlayManager::<>f__am$cache18
	Action_1_t2899269018 * ___U3CU3Ef__amU24cache18_28;
	// System.Action`1<GooglePlayResult> GooglePlayManager::<>f__am$cache19
	Action_1_t2899269018 * ___U3CU3Ef__amU24cache19_29;
	// System.Action`1<GooglePlayGiftRequestResult> GooglePlayManager::<>f__am$cache1A
	Action_1_t152001389 * ___U3CU3Ef__amU24cache1A_30;
	// System.Action GooglePlayManager::<>f__am$cache1B
	Action_t3226471752 * ___U3CU3Ef__amU24cache1B_31;
	// System.Action`1<System.Collections.Generic.List`1<GPGameRequest>> GooglePlayManager::<>f__am$cache1C
	Action_1_t3329763456 * ___U3CU3Ef__amU24cache1C_32;
	// System.Action`1<System.Collections.Generic.List`1<GPGameRequest>> GooglePlayManager::<>f__am$cache1D
	Action_1_t3329763456 * ___U3CU3Ef__amU24cache1D_33;
	// System.Action`1<System.Collections.Generic.List`1<System.String>> GooglePlayManager::<>f__am$cache1E
	Action_1_t1200140747 * ___U3CU3Ef__amU24cache1E_34;
	// System.Action`1<System.String> GooglePlayManager::<>f__am$cache1F
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache1F_35;

public:
	inline static int32_t get_offset_of__IsLeaderboardsDataLoaded_11() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ____IsLeaderboardsDataLoaded_11)); }
	inline bool get__IsLeaderboardsDataLoaded_11() const { return ____IsLeaderboardsDataLoaded_11; }
	inline bool* get_address_of__IsLeaderboardsDataLoaded_11() { return &____IsLeaderboardsDataLoaded_11; }
	inline void set__IsLeaderboardsDataLoaded_11(bool value)
	{
		____IsLeaderboardsDataLoaded_11 = value;
	}

	inline static int32_t get_offset_of_ActionScoreSubmited_12() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___ActionScoreSubmited_12)); }
	inline Action_1_t1836014676 * get_ActionScoreSubmited_12() const { return ___ActionScoreSubmited_12; }
	inline Action_1_t1836014676 ** get_address_of_ActionScoreSubmited_12() { return &___ActionScoreSubmited_12; }
	inline void set_ActionScoreSubmited_12(Action_1_t1836014676 * value)
	{
		___ActionScoreSubmited_12 = value;
		Il2CppCodeGenWriteBarrier(&___ActionScoreSubmited_12, value);
	}

	inline static int32_t get_offset_of_ActionScoresListLoaded_13() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___ActionScoresListLoaded_13)); }
	inline Action_1_t1836014676 * get_ActionScoresListLoaded_13() const { return ___ActionScoresListLoaded_13; }
	inline Action_1_t1836014676 ** get_address_of_ActionScoresListLoaded_13() { return &___ActionScoresListLoaded_13; }
	inline void set_ActionScoresListLoaded_13(Action_1_t1836014676 * value)
	{
		___ActionScoresListLoaded_13 = value;
		Il2CppCodeGenWriteBarrier(&___ActionScoresListLoaded_13, value);
	}

	inline static int32_t get_offset_of_ActionLeaderboardsLoaded_14() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___ActionLeaderboardsLoaded_14)); }
	inline Action_1_t2899269018 * get_ActionLeaderboardsLoaded_14() const { return ___ActionLeaderboardsLoaded_14; }
	inline Action_1_t2899269018 ** get_address_of_ActionLeaderboardsLoaded_14() { return &___ActionLeaderboardsLoaded_14; }
	inline void set_ActionLeaderboardsLoaded_14(Action_1_t2899269018 * value)
	{
		___ActionLeaderboardsLoaded_14 = value;
		Il2CppCodeGenWriteBarrier(&___ActionLeaderboardsLoaded_14, value);
	}

	inline static int32_t get_offset_of_ActionAchievementUpdated_15() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___ActionAchievementUpdated_15)); }
	inline Action_1_t3772725756 * get_ActionAchievementUpdated_15() const { return ___ActionAchievementUpdated_15; }
	inline Action_1_t3772725756 ** get_address_of_ActionAchievementUpdated_15() { return &___ActionAchievementUpdated_15; }
	inline void set_ActionAchievementUpdated_15(Action_1_t3772725756 * value)
	{
		___ActionAchievementUpdated_15 = value;
		Il2CppCodeGenWriteBarrier(&___ActionAchievementUpdated_15, value);
	}

	inline static int32_t get_offset_of_ActionFriendsListLoaded_16() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___ActionFriendsListLoaded_16)); }
	inline Action_1_t2899269018 * get_ActionFriendsListLoaded_16() const { return ___ActionFriendsListLoaded_16; }
	inline Action_1_t2899269018 ** get_address_of_ActionFriendsListLoaded_16() { return &___ActionFriendsListLoaded_16; }
	inline void set_ActionFriendsListLoaded_16(Action_1_t2899269018 * value)
	{
		___ActionFriendsListLoaded_16 = value;
		Il2CppCodeGenWriteBarrier(&___ActionFriendsListLoaded_16, value);
	}

	inline static int32_t get_offset_of_ActionAchievementsLoaded_17() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___ActionAchievementsLoaded_17)); }
	inline Action_1_t2899269018 * get_ActionAchievementsLoaded_17() const { return ___ActionAchievementsLoaded_17; }
	inline Action_1_t2899269018 ** get_address_of_ActionAchievementsLoaded_17() { return &___ActionAchievementsLoaded_17; }
	inline void set_ActionAchievementsLoaded_17(Action_1_t2899269018 * value)
	{
		___ActionAchievementsLoaded_17 = value;
		Il2CppCodeGenWriteBarrier(&___ActionAchievementsLoaded_17, value);
	}

	inline static int32_t get_offset_of_ActionSendGiftResultReceived_18() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___ActionSendGiftResultReceived_18)); }
	inline Action_1_t152001389 * get_ActionSendGiftResultReceived_18() const { return ___ActionSendGiftResultReceived_18; }
	inline Action_1_t152001389 ** get_address_of_ActionSendGiftResultReceived_18() { return &___ActionSendGiftResultReceived_18; }
	inline void set_ActionSendGiftResultReceived_18(Action_1_t152001389 * value)
	{
		___ActionSendGiftResultReceived_18 = value;
		Il2CppCodeGenWriteBarrier(&___ActionSendGiftResultReceived_18, value);
	}

	inline static int32_t get_offset_of_ActionRequestsInboxDialogDismissed_19() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___ActionRequestsInboxDialogDismissed_19)); }
	inline Action_t3226471752 * get_ActionRequestsInboxDialogDismissed_19() const { return ___ActionRequestsInboxDialogDismissed_19; }
	inline Action_t3226471752 ** get_address_of_ActionRequestsInboxDialogDismissed_19() { return &___ActionRequestsInboxDialogDismissed_19; }
	inline void set_ActionRequestsInboxDialogDismissed_19(Action_t3226471752 * value)
	{
		___ActionRequestsInboxDialogDismissed_19 = value;
		Il2CppCodeGenWriteBarrier(&___ActionRequestsInboxDialogDismissed_19, value);
	}

	inline static int32_t get_offset_of_ActionPendingGameRequestsDetected_20() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___ActionPendingGameRequestsDetected_20)); }
	inline Action_1_t3329763456 * get_ActionPendingGameRequestsDetected_20() const { return ___ActionPendingGameRequestsDetected_20; }
	inline Action_1_t3329763456 ** get_address_of_ActionPendingGameRequestsDetected_20() { return &___ActionPendingGameRequestsDetected_20; }
	inline void set_ActionPendingGameRequestsDetected_20(Action_1_t3329763456 * value)
	{
		___ActionPendingGameRequestsDetected_20 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPendingGameRequestsDetected_20, value);
	}

	inline static int32_t get_offset_of_ActionGameRequestsAccepted_21() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___ActionGameRequestsAccepted_21)); }
	inline Action_1_t3329763456 * get_ActionGameRequestsAccepted_21() const { return ___ActionGameRequestsAccepted_21; }
	inline Action_1_t3329763456 ** get_address_of_ActionGameRequestsAccepted_21() { return &___ActionGameRequestsAccepted_21; }
	inline void set_ActionGameRequestsAccepted_21(Action_1_t3329763456 * value)
	{
		___ActionGameRequestsAccepted_21 = value;
		Il2CppCodeGenWriteBarrier(&___ActionGameRequestsAccepted_21, value);
	}

	inline static int32_t get_offset_of_ActionAvailableDeviceAccountsLoaded_22() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___ActionAvailableDeviceAccountsLoaded_22)); }
	inline Action_1_t1200140747 * get_ActionAvailableDeviceAccountsLoaded_22() const { return ___ActionAvailableDeviceAccountsLoaded_22; }
	inline Action_1_t1200140747 ** get_address_of_ActionAvailableDeviceAccountsLoaded_22() { return &___ActionAvailableDeviceAccountsLoaded_22; }
	inline void set_ActionAvailableDeviceAccountsLoaded_22(Action_1_t1200140747 * value)
	{
		___ActionAvailableDeviceAccountsLoaded_22 = value;
		Il2CppCodeGenWriteBarrier(&___ActionAvailableDeviceAccountsLoaded_22, value);
	}

	inline static int32_t get_offset_of_ActionOAuthTokenLoaded_23() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___ActionOAuthTokenLoaded_23)); }
	inline Action_1_t1831019615 * get_ActionOAuthTokenLoaded_23() const { return ___ActionOAuthTokenLoaded_23; }
	inline Action_1_t1831019615 ** get_address_of_ActionOAuthTokenLoaded_23() { return &___ActionOAuthTokenLoaded_23; }
	inline void set_ActionOAuthTokenLoaded_23(Action_1_t1831019615 * value)
	{
		___ActionOAuthTokenLoaded_23 = value;
		Il2CppCodeGenWriteBarrier(&___ActionOAuthTokenLoaded_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_24() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___U3CU3Ef__amU24cache14_24)); }
	inline Action_1_t1836014676 * get_U3CU3Ef__amU24cache14_24() const { return ___U3CU3Ef__amU24cache14_24; }
	inline Action_1_t1836014676 ** get_address_of_U3CU3Ef__amU24cache14_24() { return &___U3CU3Ef__amU24cache14_24; }
	inline void set_U3CU3Ef__amU24cache14_24(Action_1_t1836014676 * value)
	{
		___U3CU3Ef__amU24cache14_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache14_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_25() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___U3CU3Ef__amU24cache15_25)); }
	inline Action_1_t1836014676 * get_U3CU3Ef__amU24cache15_25() const { return ___U3CU3Ef__amU24cache15_25; }
	inline Action_1_t1836014676 ** get_address_of_U3CU3Ef__amU24cache15_25() { return &___U3CU3Ef__amU24cache15_25; }
	inline void set_U3CU3Ef__amU24cache15_25(Action_1_t1836014676 * value)
	{
		___U3CU3Ef__amU24cache15_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache15_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache16_26() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___U3CU3Ef__amU24cache16_26)); }
	inline Action_1_t2899269018 * get_U3CU3Ef__amU24cache16_26() const { return ___U3CU3Ef__amU24cache16_26; }
	inline Action_1_t2899269018 ** get_address_of_U3CU3Ef__amU24cache16_26() { return &___U3CU3Ef__amU24cache16_26; }
	inline void set_U3CU3Ef__amU24cache16_26(Action_1_t2899269018 * value)
	{
		___U3CU3Ef__amU24cache16_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache16_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache17_27() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___U3CU3Ef__amU24cache17_27)); }
	inline Action_1_t3772725756 * get_U3CU3Ef__amU24cache17_27() const { return ___U3CU3Ef__amU24cache17_27; }
	inline Action_1_t3772725756 ** get_address_of_U3CU3Ef__amU24cache17_27() { return &___U3CU3Ef__amU24cache17_27; }
	inline void set_U3CU3Ef__amU24cache17_27(Action_1_t3772725756 * value)
	{
		___U3CU3Ef__amU24cache17_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache17_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache18_28() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___U3CU3Ef__amU24cache18_28)); }
	inline Action_1_t2899269018 * get_U3CU3Ef__amU24cache18_28() const { return ___U3CU3Ef__amU24cache18_28; }
	inline Action_1_t2899269018 ** get_address_of_U3CU3Ef__amU24cache18_28() { return &___U3CU3Ef__amU24cache18_28; }
	inline void set_U3CU3Ef__amU24cache18_28(Action_1_t2899269018 * value)
	{
		___U3CU3Ef__amU24cache18_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache18_28, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache19_29() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___U3CU3Ef__amU24cache19_29)); }
	inline Action_1_t2899269018 * get_U3CU3Ef__amU24cache19_29() const { return ___U3CU3Ef__amU24cache19_29; }
	inline Action_1_t2899269018 ** get_address_of_U3CU3Ef__amU24cache19_29() { return &___U3CU3Ef__amU24cache19_29; }
	inline void set_U3CU3Ef__amU24cache19_29(Action_1_t2899269018 * value)
	{
		___U3CU3Ef__amU24cache19_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache19_29, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1A_30() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___U3CU3Ef__amU24cache1A_30)); }
	inline Action_1_t152001389 * get_U3CU3Ef__amU24cache1A_30() const { return ___U3CU3Ef__amU24cache1A_30; }
	inline Action_1_t152001389 ** get_address_of_U3CU3Ef__amU24cache1A_30() { return &___U3CU3Ef__amU24cache1A_30; }
	inline void set_U3CU3Ef__amU24cache1A_30(Action_1_t152001389 * value)
	{
		___U3CU3Ef__amU24cache1A_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1A_30, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1B_31() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___U3CU3Ef__amU24cache1B_31)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache1B_31() const { return ___U3CU3Ef__amU24cache1B_31; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache1B_31() { return &___U3CU3Ef__amU24cache1B_31; }
	inline void set_U3CU3Ef__amU24cache1B_31(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache1B_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1B_31, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_32() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___U3CU3Ef__amU24cache1C_32)); }
	inline Action_1_t3329763456 * get_U3CU3Ef__amU24cache1C_32() const { return ___U3CU3Ef__amU24cache1C_32; }
	inline Action_1_t3329763456 ** get_address_of_U3CU3Ef__amU24cache1C_32() { return &___U3CU3Ef__amU24cache1C_32; }
	inline void set_U3CU3Ef__amU24cache1C_32(Action_1_t3329763456 * value)
	{
		___U3CU3Ef__amU24cache1C_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1C_32, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1D_33() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___U3CU3Ef__amU24cache1D_33)); }
	inline Action_1_t3329763456 * get_U3CU3Ef__amU24cache1D_33() const { return ___U3CU3Ef__amU24cache1D_33; }
	inline Action_1_t3329763456 ** get_address_of_U3CU3Ef__amU24cache1D_33() { return &___U3CU3Ef__amU24cache1D_33; }
	inline void set_U3CU3Ef__amU24cache1D_33(Action_1_t3329763456 * value)
	{
		___U3CU3Ef__amU24cache1D_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1D_33, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1E_34() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___U3CU3Ef__amU24cache1E_34)); }
	inline Action_1_t1200140747 * get_U3CU3Ef__amU24cache1E_34() const { return ___U3CU3Ef__amU24cache1E_34; }
	inline Action_1_t1200140747 ** get_address_of_U3CU3Ef__amU24cache1E_34() { return &___U3CU3Ef__amU24cache1E_34; }
	inline void set_U3CU3Ef__amU24cache1E_34(Action_1_t1200140747 * value)
	{
		___U3CU3Ef__amU24cache1E_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1E_34, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1F_35() { return static_cast<int32_t>(offsetof(GooglePlayManager_t746138120_StaticFields, ___U3CU3Ef__amU24cache1F_35)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache1F_35() const { return ___U3CU3Ef__amU24cache1F_35; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache1F_35() { return &___U3CU3Ef__amU24cache1F_35; }
	inline void set_U3CU3Ef__amU24cache1F_35(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache1F_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1F_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
