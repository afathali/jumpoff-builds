﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeleteCourseResponse
struct DeleteCourseResponse_t1898856809;
// System.String
struct String_t;
// DeleteCourseRequest
struct DeleteCourseRequest_t1683798751;
// System.Collections.Generic.List`1<Course>
struct List_1_t2852233831;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_DeleteCourseRequest1683798751.h"

// System.Void DeleteCourseResponse::.ctor()
extern "C"  void DeleteCourseResponse__ctor_m783111924 (DeleteCourseResponse_t1898856809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DeleteCourseResponse::get_Success()
extern "C"  bool DeleteCourseResponse_get_Success_m2956226846 (DeleteCourseResponse_t1898856809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeleteCourseResponse::set_Success(System.Boolean)
extern "C"  void DeleteCourseResponse_set_Success_m3900311093 (DeleteCourseResponse_t1898856809 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DeleteCourseResponse::get_ErrorMessage()
extern "C"  String_t* DeleteCourseResponse_get_ErrorMessage_m334347959 (DeleteCourseResponse_t1898856809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeleteCourseResponse::set_ErrorMessage(System.String)
extern "C"  void DeleteCourseResponse_set_ErrorMessage_m3697376336 (DeleteCourseResponse_t1898856809 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DeleteCourseRequest DeleteCourseResponse::get_Request()
extern "C"  DeleteCourseRequest_t1683798751 * DeleteCourseResponse_get_Request_m2772169006 (DeleteCourseResponse_t1898856809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeleteCourseResponse::set_Request(DeleteCourseRequest)
extern "C"  void DeleteCourseResponse_set_Request_m2809195245 (DeleteCourseResponse_t1898856809 * __this, DeleteCourseRequest_t1683798751 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Course> DeleteCourseResponse::get_Courses()
extern "C"  List_1_t2852233831 * DeleteCourseResponse_get_Courses_m2817747591 (DeleteCourseResponse_t1898856809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeleteCourseResponse::set_Courses(System.Collections.Generic.List`1<Course>)
extern "C"  void DeleteCourseResponse_set_Courses_m4170825876 (DeleteCourseResponse_t1898856809 * __this, List_1_t2852233831 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
