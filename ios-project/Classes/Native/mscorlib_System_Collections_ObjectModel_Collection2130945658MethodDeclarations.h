﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2231194049MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::.ctor()
#define Collection_1__ctor_m3683848816(__this, method) ((  void (*) (Collection_1_t2130945658 *, const MethodInfo*))Collection_1__ctor_m3383758099_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1256707787(__this, method) ((  bool (*) (Collection_1_t2130945658 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2832435102_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m3445294572(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2130945658 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2795445359_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1848089615(__this, method) ((  Il2CppObject * (*) (Collection_1_t2130945658 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m539985258_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m3943757608(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2130945658 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m916188271_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m3830050942(__this, ___value0, method) ((  bool (*) (Collection_1_t2130945658 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3240760119_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m3679079414(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2130945658 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3460849589_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m1645790351(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2130945658 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3482199744_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m65921279(__this, ___value0, method) ((  void (*) (Collection_1_t2130945658 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1739078822_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3999057008(__this, method) ((  bool (*) (Collection_1_t2130945658 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1442644511_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2904293478(__this, method) ((  Il2CppObject * (*) (Collection_1_t2130945658 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1422512927_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2882120639(__this, method) ((  bool (*) (Collection_1_t2130945658 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2968235316_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3601113284(__this, method) ((  bool (*) (Collection_1_t2130945658 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1990189611_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m4001994851(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2130945658 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m75082808_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m3612152686(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2130945658 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m507853765_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::Add(T)
#define Collection_1_Add_m2218395195(__this, ___item0, method) ((  void (*) (Collection_1_t2130945658 *, EnumValue_1_t2589200904 *, const MethodInfo*))Collection_1_Add_m2987402052_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::Clear()
#define Collection_1_Clear_m3768267775(__this, method) ((  void (*) (Collection_1_t2130945658 *, const MethodInfo*))Collection_1_Clear_m1596645192_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::ClearItems()
#define Collection_1_ClearItems_m4172699537(__this, method) ((  void (*) (Collection_1_t2130945658 *, const MethodInfo*))Collection_1_ClearItems_m1175603758_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::Contains(T)
#define Collection_1_Contains_m2491225945(__this, ___item0, method) ((  bool (*) (Collection_1_t2130945658 *, EnumValue_1_t2589200904 *, const MethodInfo*))Collection_1_Contains_m2116635914_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m2718912807(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2130945658 *, EnumValue_1U5BU5D_t3039067353*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1578267616_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::GetEnumerator()
#define Collection_1_GetEnumerator_m1835487260(__this, method) ((  Il2CppObject* (*) (Collection_1_t2130945658 *, const MethodInfo*))Collection_1_GetEnumerator_m2963411583_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::IndexOf(T)
#define Collection_1_IndexOf_m1290392299(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2130945658 *, EnumValue_1_t2589200904 *, const MethodInfo*))Collection_1_IndexOf_m3885709710_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::Insert(System.Int32,T)
#define Collection_1_Insert_m3196801378(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2130945658 *, int32_t, EnumValue_1_t2589200904 *, const MethodInfo*))Collection_1_Insert_m2334889193_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m2015148809(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2130945658 *, int32_t, EnumValue_1_t2589200904 *, const MethodInfo*))Collection_1_InsertItem_m3611385334_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::get_Items()
#define Collection_1_get_Items_m1630216685(__this, method) ((  Il2CppObject* (*) (Collection_1_t2130945658 *, const MethodInfo*))Collection_1_get_Items_m2226526738_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::Remove(T)
#define Collection_1_Remove_m2035778924(__this, ___item0, method) ((  bool (*) (Collection_1_t2130945658 *, EnumValue_1_t2589200904 *, const MethodInfo*))Collection_1_Remove_m452558737_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m369618390(__this, ___index0, method) ((  void (*) (Collection_1_t2130945658 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1632496813_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m569075888(__this, ___index0, method) ((  void (*) (Collection_1_t2130945658 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4104600353_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::get_Count()
#define Collection_1_get_Count_m1757048520(__this, method) ((  int32_t (*) (Collection_1_t2130945658 *, const MethodInfo*))Collection_1_get_Count_m2250721247_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::get_Item(System.Int32)
#define Collection_1_get_Item_m2868597682(__this, ___index0, method) ((  EnumValue_1_t2589200904 * (*) (Collection_1_t2130945658 *, int32_t, const MethodInfo*))Collection_1_get_Item_m266052953_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m921827011(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2130945658 *, int32_t, EnumValue_1_t2589200904 *, const MethodInfo*))Collection_1_set_Item_m3489932746_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m1656051454(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2130945658 *, int32_t, EnumValue_1_t2589200904 *, const MethodInfo*))Collection_1_SetItem_m1075410277_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m1454673277(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3443424420_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m3430891329(__this /* static, unused */, ___item0, method) ((  EnumValue_1_t2589200904 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1521356246_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m44903629(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m215419136_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m604864757(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m328767958_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m1670128006(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3594284193_gshared)(__this /* static, unused */, ___list0, method)
