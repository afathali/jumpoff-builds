﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainPage_Controller/<GetBannerUrl>c__Iterator17
struct U3CGetBannerUrlU3Ec__Iterator17_t453977480;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MainPage_Controller/<GetBannerUrl>c__Iterator17::.ctor()
extern "C"  void U3CGetBannerUrlU3Ec__Iterator17__ctor_m391063983 (U3CGetBannerUrlU3Ec__Iterator17_t453977480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MainPage_Controller/<GetBannerUrl>c__Iterator17::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetBannerUrlU3Ec__Iterator17_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3562197113 (U3CGetBannerUrlU3Ec__Iterator17_t453977480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MainPage_Controller/<GetBannerUrl>c__Iterator17::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetBannerUrlU3Ec__Iterator17_System_Collections_IEnumerator_get_Current_m997269841 (U3CGetBannerUrlU3Ec__Iterator17_t453977480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MainPage_Controller/<GetBannerUrl>c__Iterator17::MoveNext()
extern "C"  bool U3CGetBannerUrlU3Ec__Iterator17_MoveNext_m3399177817 (U3CGetBannerUrlU3Ec__Iterator17_t453977480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller/<GetBannerUrl>c__Iterator17::Dispose()
extern "C"  void U3CGetBannerUrlU3Ec__Iterator17_Dispose_m735349234 (U3CGetBannerUrlU3Ec__Iterator17_t453977480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller/<GetBannerUrl>c__Iterator17::Reset()
extern "C"  void U3CGetBannerUrlU3Ec__Iterator17_Reset_m1744163764 (U3CGetBannerUrlU3Ec__Iterator17_t453977480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
