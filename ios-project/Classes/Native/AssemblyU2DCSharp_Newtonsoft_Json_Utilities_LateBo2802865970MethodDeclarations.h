﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey33`1<System.Object>
struct U3CCreateSetU3Ec__AnonStorey33_1_t2802865970;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey33`1<System.Object>::.ctor()
extern "C"  void U3CCreateSetU3Ec__AnonStorey33_1__ctor_m634821359_gshared (U3CCreateSetU3Ec__AnonStorey33_1_t2802865970 * __this, const MethodInfo* method);
#define U3CCreateSetU3Ec__AnonStorey33_1__ctor_m634821359(__this, method) ((  void (*) (U3CCreateSetU3Ec__AnonStorey33_1_t2802865970 *, const MethodInfo*))U3CCreateSetU3Ec__AnonStorey33_1__ctor_m634821359_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey33`1<System.Object>::<>m__FB(T,System.Object)
extern "C"  void U3CCreateSetU3Ec__AnonStorey33_1_U3CU3Em__FB_m2326377862_gshared (U3CCreateSetU3Ec__AnonStorey33_1_t2802865970 * __this, Il2CppObject * ___o0, Il2CppObject * ___v1, const MethodInfo* method);
#define U3CCreateSetU3Ec__AnonStorey33_1_U3CU3Em__FB_m2326377862(__this, ___o0, ___v1, method) ((  void (*) (U3CCreateSetU3Ec__AnonStorey33_1_t2802865970 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CCreateSetU3Ec__AnonStorey33_1_U3CU3Em__FB_m2326377862_gshared)(__this, ___o0, ___v1, method)
