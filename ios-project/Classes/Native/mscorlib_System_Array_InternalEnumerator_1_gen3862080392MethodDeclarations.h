﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3862080392.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_FB_ProfileImageSize3003328130.h"

// System.Void System.Array/InternalEnumerator`1<FB_ProfileImageSize>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4177615517_gshared (InternalEnumerator_1_t3862080392 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4177615517(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3862080392 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4177615517_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<FB_ProfileImageSize>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3252548625_gshared (InternalEnumerator_1_t3862080392 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3252548625(__this, method) ((  void (*) (InternalEnumerator_1_t3862080392 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3252548625_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<FB_ProfileImageSize>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3399779653_gshared (InternalEnumerator_1_t3862080392 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3399779653(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3862080392 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3399779653_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<FB_ProfileImageSize>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2528880210_gshared (InternalEnumerator_1_t3862080392 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2528880210(__this, method) ((  void (*) (InternalEnumerator_1_t3862080392 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2528880210_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<FB_ProfileImageSize>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3505086881_gshared (InternalEnumerator_1_t3862080392 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3505086881(__this, method) ((  bool (*) (InternalEnumerator_1_t3862080392 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3505086881_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<FB_ProfileImageSize>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m581384578_gshared (InternalEnumerator_1_t3862080392 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m581384578(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3862080392 *, const MethodInfo*))InternalEnumerator_1_get_Current_m581384578_gshared)(__this, method)
