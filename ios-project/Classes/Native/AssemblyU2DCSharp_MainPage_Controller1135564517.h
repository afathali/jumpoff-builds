﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData>
struct UnityAction_1_t4047591376;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;

#include "AssemblyU2DCSharp_MCGBehaviour3307770884.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainPage_Controller
struct  MainPage_Controller_t1135564517  : public MCGBehaviour_t3307770884
{
public:
	// UnityEngine.UI.Button MainPage_Controller::btn_uploadNew
	Button_t2872111280 * ___btn_uploadNew_4;
	// UnityEngine.UI.Button MainPage_Controller::btn_createBlank
	Button_t2872111280 * ___btn_createBlank_5;
	// UnityEngine.UI.Button MainPage_Controller::btn_viewCourses
	Button_t2872111280 * ___btn_viewCourses_6;
	// UnityEngine.Camera MainPage_Controller::MainCamera
	Camera_t189460977 * ___MainCamera_7;
	// UnityEngine.GameObject MainPage_Controller::SignUp
	GameObject_t1756533147 * ___SignUp_8;
	// UnityEngine.UI.Button MainPage_Controller::btn_signUp
	Button_t2872111280 * ___btn_signUp_9;
	// UnityEngine.UI.Button MainPage_Controller::btn_closeLogin
	Button_t2872111280 * ___btn_closeLogin_10;
	// UnityEngine.GameObject MainPage_Controller::AdObject
	GameObject_t1756533147 * ___AdObject_11;
	// System.Int32 MainPage_Controller::currentAdNumber
	int32_t ___currentAdNumber_12;
	// System.String MainPage_Controller::bannerImageUrl
	String_t* ___bannerImageUrl_13;
	// System.String MainPage_Controller::textFileUrl
	String_t* ___textFileUrl_14;
	// System.String MainPage_Controller::bannerUrl
	String_t* ___bannerUrl_15;
	// UnityEngine.UI.Button MainPage_Controller::btn_banner
	Button_t2872111280 * ___btn_banner_16;
	// UnityEngine.UI.Button MainPage_Controller::btn_logout
	Button_t2872111280 * ___btn_logout_17;
	// UnityEngine.GameObject MainPage_Controller::logo
	GameObject_t1756533147 * ___logo_18;
	// UnityEngine.GameObject MainPage_Controller::dlg_logout
	GameObject_t1756533147 * ___dlg_logout_19;
	// UnityEngine.UI.Button MainPage_Controller::btn_logout_yes
	Button_t2872111280 * ___btn_logout_yes_20;
	// UnityEngine.UI.Button MainPage_Controller::btn_logout_no
	Button_t2872111280 * ___btn_logout_no_21;
	// UnityEngine.GameObject MainPage_Controller::waiting
	GameObject_t1756533147 * ___waiting_22;
	// UnityEngine.GameObject MainPage_Controller::fail
	GameObject_t1756533147 * ___fail_23;
	// UnityEngine.GameObject MainPage_Controller::terms
	GameObject_t1756533147 * ___terms_24;
	// System.Boolean MainPage_Controller::destroying
	bool ___destroying_25;

public:
	inline static int32_t get_offset_of_btn_uploadNew_4() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___btn_uploadNew_4)); }
	inline Button_t2872111280 * get_btn_uploadNew_4() const { return ___btn_uploadNew_4; }
	inline Button_t2872111280 ** get_address_of_btn_uploadNew_4() { return &___btn_uploadNew_4; }
	inline void set_btn_uploadNew_4(Button_t2872111280 * value)
	{
		___btn_uploadNew_4 = value;
		Il2CppCodeGenWriteBarrier(&___btn_uploadNew_4, value);
	}

	inline static int32_t get_offset_of_btn_createBlank_5() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___btn_createBlank_5)); }
	inline Button_t2872111280 * get_btn_createBlank_5() const { return ___btn_createBlank_5; }
	inline Button_t2872111280 ** get_address_of_btn_createBlank_5() { return &___btn_createBlank_5; }
	inline void set_btn_createBlank_5(Button_t2872111280 * value)
	{
		___btn_createBlank_5 = value;
		Il2CppCodeGenWriteBarrier(&___btn_createBlank_5, value);
	}

	inline static int32_t get_offset_of_btn_viewCourses_6() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___btn_viewCourses_6)); }
	inline Button_t2872111280 * get_btn_viewCourses_6() const { return ___btn_viewCourses_6; }
	inline Button_t2872111280 ** get_address_of_btn_viewCourses_6() { return &___btn_viewCourses_6; }
	inline void set_btn_viewCourses_6(Button_t2872111280 * value)
	{
		___btn_viewCourses_6 = value;
		Il2CppCodeGenWriteBarrier(&___btn_viewCourses_6, value);
	}

	inline static int32_t get_offset_of_MainCamera_7() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___MainCamera_7)); }
	inline Camera_t189460977 * get_MainCamera_7() const { return ___MainCamera_7; }
	inline Camera_t189460977 ** get_address_of_MainCamera_7() { return &___MainCamera_7; }
	inline void set_MainCamera_7(Camera_t189460977 * value)
	{
		___MainCamera_7 = value;
		Il2CppCodeGenWriteBarrier(&___MainCamera_7, value);
	}

	inline static int32_t get_offset_of_SignUp_8() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___SignUp_8)); }
	inline GameObject_t1756533147 * get_SignUp_8() const { return ___SignUp_8; }
	inline GameObject_t1756533147 ** get_address_of_SignUp_8() { return &___SignUp_8; }
	inline void set_SignUp_8(GameObject_t1756533147 * value)
	{
		___SignUp_8 = value;
		Il2CppCodeGenWriteBarrier(&___SignUp_8, value);
	}

	inline static int32_t get_offset_of_btn_signUp_9() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___btn_signUp_9)); }
	inline Button_t2872111280 * get_btn_signUp_9() const { return ___btn_signUp_9; }
	inline Button_t2872111280 ** get_address_of_btn_signUp_9() { return &___btn_signUp_9; }
	inline void set_btn_signUp_9(Button_t2872111280 * value)
	{
		___btn_signUp_9 = value;
		Il2CppCodeGenWriteBarrier(&___btn_signUp_9, value);
	}

	inline static int32_t get_offset_of_btn_closeLogin_10() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___btn_closeLogin_10)); }
	inline Button_t2872111280 * get_btn_closeLogin_10() const { return ___btn_closeLogin_10; }
	inline Button_t2872111280 ** get_address_of_btn_closeLogin_10() { return &___btn_closeLogin_10; }
	inline void set_btn_closeLogin_10(Button_t2872111280 * value)
	{
		___btn_closeLogin_10 = value;
		Il2CppCodeGenWriteBarrier(&___btn_closeLogin_10, value);
	}

	inline static int32_t get_offset_of_AdObject_11() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___AdObject_11)); }
	inline GameObject_t1756533147 * get_AdObject_11() const { return ___AdObject_11; }
	inline GameObject_t1756533147 ** get_address_of_AdObject_11() { return &___AdObject_11; }
	inline void set_AdObject_11(GameObject_t1756533147 * value)
	{
		___AdObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___AdObject_11, value);
	}

	inline static int32_t get_offset_of_currentAdNumber_12() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___currentAdNumber_12)); }
	inline int32_t get_currentAdNumber_12() const { return ___currentAdNumber_12; }
	inline int32_t* get_address_of_currentAdNumber_12() { return &___currentAdNumber_12; }
	inline void set_currentAdNumber_12(int32_t value)
	{
		___currentAdNumber_12 = value;
	}

	inline static int32_t get_offset_of_bannerImageUrl_13() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___bannerImageUrl_13)); }
	inline String_t* get_bannerImageUrl_13() const { return ___bannerImageUrl_13; }
	inline String_t** get_address_of_bannerImageUrl_13() { return &___bannerImageUrl_13; }
	inline void set_bannerImageUrl_13(String_t* value)
	{
		___bannerImageUrl_13 = value;
		Il2CppCodeGenWriteBarrier(&___bannerImageUrl_13, value);
	}

	inline static int32_t get_offset_of_textFileUrl_14() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___textFileUrl_14)); }
	inline String_t* get_textFileUrl_14() const { return ___textFileUrl_14; }
	inline String_t** get_address_of_textFileUrl_14() { return &___textFileUrl_14; }
	inline void set_textFileUrl_14(String_t* value)
	{
		___textFileUrl_14 = value;
		Il2CppCodeGenWriteBarrier(&___textFileUrl_14, value);
	}

	inline static int32_t get_offset_of_bannerUrl_15() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___bannerUrl_15)); }
	inline String_t* get_bannerUrl_15() const { return ___bannerUrl_15; }
	inline String_t** get_address_of_bannerUrl_15() { return &___bannerUrl_15; }
	inline void set_bannerUrl_15(String_t* value)
	{
		___bannerUrl_15 = value;
		Il2CppCodeGenWriteBarrier(&___bannerUrl_15, value);
	}

	inline static int32_t get_offset_of_btn_banner_16() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___btn_banner_16)); }
	inline Button_t2872111280 * get_btn_banner_16() const { return ___btn_banner_16; }
	inline Button_t2872111280 ** get_address_of_btn_banner_16() { return &___btn_banner_16; }
	inline void set_btn_banner_16(Button_t2872111280 * value)
	{
		___btn_banner_16 = value;
		Il2CppCodeGenWriteBarrier(&___btn_banner_16, value);
	}

	inline static int32_t get_offset_of_btn_logout_17() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___btn_logout_17)); }
	inline Button_t2872111280 * get_btn_logout_17() const { return ___btn_logout_17; }
	inline Button_t2872111280 ** get_address_of_btn_logout_17() { return &___btn_logout_17; }
	inline void set_btn_logout_17(Button_t2872111280 * value)
	{
		___btn_logout_17 = value;
		Il2CppCodeGenWriteBarrier(&___btn_logout_17, value);
	}

	inline static int32_t get_offset_of_logo_18() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___logo_18)); }
	inline GameObject_t1756533147 * get_logo_18() const { return ___logo_18; }
	inline GameObject_t1756533147 ** get_address_of_logo_18() { return &___logo_18; }
	inline void set_logo_18(GameObject_t1756533147 * value)
	{
		___logo_18 = value;
		Il2CppCodeGenWriteBarrier(&___logo_18, value);
	}

	inline static int32_t get_offset_of_dlg_logout_19() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___dlg_logout_19)); }
	inline GameObject_t1756533147 * get_dlg_logout_19() const { return ___dlg_logout_19; }
	inline GameObject_t1756533147 ** get_address_of_dlg_logout_19() { return &___dlg_logout_19; }
	inline void set_dlg_logout_19(GameObject_t1756533147 * value)
	{
		___dlg_logout_19 = value;
		Il2CppCodeGenWriteBarrier(&___dlg_logout_19, value);
	}

	inline static int32_t get_offset_of_btn_logout_yes_20() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___btn_logout_yes_20)); }
	inline Button_t2872111280 * get_btn_logout_yes_20() const { return ___btn_logout_yes_20; }
	inline Button_t2872111280 ** get_address_of_btn_logout_yes_20() { return &___btn_logout_yes_20; }
	inline void set_btn_logout_yes_20(Button_t2872111280 * value)
	{
		___btn_logout_yes_20 = value;
		Il2CppCodeGenWriteBarrier(&___btn_logout_yes_20, value);
	}

	inline static int32_t get_offset_of_btn_logout_no_21() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___btn_logout_no_21)); }
	inline Button_t2872111280 * get_btn_logout_no_21() const { return ___btn_logout_no_21; }
	inline Button_t2872111280 ** get_address_of_btn_logout_no_21() { return &___btn_logout_no_21; }
	inline void set_btn_logout_no_21(Button_t2872111280 * value)
	{
		___btn_logout_no_21 = value;
		Il2CppCodeGenWriteBarrier(&___btn_logout_no_21, value);
	}

	inline static int32_t get_offset_of_waiting_22() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___waiting_22)); }
	inline GameObject_t1756533147 * get_waiting_22() const { return ___waiting_22; }
	inline GameObject_t1756533147 ** get_address_of_waiting_22() { return &___waiting_22; }
	inline void set_waiting_22(GameObject_t1756533147 * value)
	{
		___waiting_22 = value;
		Il2CppCodeGenWriteBarrier(&___waiting_22, value);
	}

	inline static int32_t get_offset_of_fail_23() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___fail_23)); }
	inline GameObject_t1756533147 * get_fail_23() const { return ___fail_23; }
	inline GameObject_t1756533147 ** get_address_of_fail_23() { return &___fail_23; }
	inline void set_fail_23(GameObject_t1756533147 * value)
	{
		___fail_23 = value;
		Il2CppCodeGenWriteBarrier(&___fail_23, value);
	}

	inline static int32_t get_offset_of_terms_24() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___terms_24)); }
	inline GameObject_t1756533147 * get_terms_24() const { return ___terms_24; }
	inline GameObject_t1756533147 ** get_address_of_terms_24() { return &___terms_24; }
	inline void set_terms_24(GameObject_t1756533147 * value)
	{
		___terms_24 = value;
		Il2CppCodeGenWriteBarrier(&___terms_24, value);
	}

	inline static int32_t get_offset_of_destroying_25() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517, ___destroying_25)); }
	inline bool get_destroying_25() const { return ___destroying_25; }
	inline bool* get_address_of_destroying_25() { return &___destroying_25; }
	inline void set_destroying_25(bool value)
	{
		___destroying_25 = value;
	}
};

struct MainPage_Controller_t1135564517_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData> MainPage_Controller::<>f__am$cache16
	UnityAction_1_t4047591376 * ___U3CU3Ef__amU24cache16_26;
	// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData> MainPage_Controller::<>f__am$cache17
	UnityAction_1_t4047591376 * ___U3CU3Ef__amU24cache17_27;
	// UnityEngine.Events.UnityAction MainPage_Controller::<>f__am$cache18
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache18_28;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache16_26() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517_StaticFields, ___U3CU3Ef__amU24cache16_26)); }
	inline UnityAction_1_t4047591376 * get_U3CU3Ef__amU24cache16_26() const { return ___U3CU3Ef__amU24cache16_26; }
	inline UnityAction_1_t4047591376 ** get_address_of_U3CU3Ef__amU24cache16_26() { return &___U3CU3Ef__amU24cache16_26; }
	inline void set_U3CU3Ef__amU24cache16_26(UnityAction_1_t4047591376 * value)
	{
		___U3CU3Ef__amU24cache16_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache16_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache17_27() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517_StaticFields, ___U3CU3Ef__amU24cache17_27)); }
	inline UnityAction_1_t4047591376 * get_U3CU3Ef__amU24cache17_27() const { return ___U3CU3Ef__amU24cache17_27; }
	inline UnityAction_1_t4047591376 ** get_address_of_U3CU3Ef__amU24cache17_27() { return &___U3CU3Ef__amU24cache17_27; }
	inline void set_U3CU3Ef__amU24cache17_27(UnityAction_1_t4047591376 * value)
	{
		___U3CU3Ef__amU24cache17_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache17_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache18_28() { return static_cast<int32_t>(offsetof(MainPage_Controller_t1135564517_StaticFields, ___U3CU3Ef__amU24cache18_28)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache18_28() const { return ___U3CU3Ef__amU24cache18_28; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache18_28() { return &___U3CU3Ef__amU24cache18_28; }
	inline void set_U3CU3Ef__amU24cache18_28(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache18_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache18_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
