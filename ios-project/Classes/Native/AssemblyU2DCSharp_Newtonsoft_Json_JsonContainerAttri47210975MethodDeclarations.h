﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonContainerAttribute
struct JsonContainerAttribute_t47210975;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Newtonsoft.Json.JsonContainerAttribute::.ctor()
extern "C"  void JsonContainerAttribute__ctor_m2591785235 (JsonContainerAttribute_t47210975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonContainerAttribute::.ctor(System.String)
extern "C"  void JsonContainerAttribute__ctor_m475344689 (JsonContainerAttribute_t47210975 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonContainerAttribute::get_Id()
extern "C"  String_t* JsonContainerAttribute_get_Id_m2363771040 (JsonContainerAttribute_t47210975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonContainerAttribute::set_Id(System.String)
extern "C"  void JsonContainerAttribute_set_Id_m4287954697 (JsonContainerAttribute_t47210975 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonContainerAttribute::get_Title()
extern "C"  String_t* JsonContainerAttribute_get_Title_m448412915 (JsonContainerAttribute_t47210975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonContainerAttribute::set_Title(System.String)
extern "C"  void JsonContainerAttribute_set_Title_m1617216066 (JsonContainerAttribute_t47210975 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonContainerAttribute::get_Description()
extern "C"  String_t* JsonContainerAttribute_get_Description_m378782197 (JsonContainerAttribute_t47210975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonContainerAttribute::set_Description(System.String)
extern "C"  void JsonContainerAttribute_set_Description_m2106041210 (JsonContainerAttribute_t47210975 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonContainerAttribute::get_IsReference()
extern "C"  bool JsonContainerAttribute_get_IsReference_m3525601875 (JsonContainerAttribute_t47210975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonContainerAttribute::set_IsReference(System.Boolean)
extern "C"  void JsonContainerAttribute_set_IsReference_m1858493602 (JsonContainerAttribute_t47210975 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
