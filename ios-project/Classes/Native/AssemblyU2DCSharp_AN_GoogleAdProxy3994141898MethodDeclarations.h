﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_GoogleAdProxy
struct AN_GoogleAdProxy_t3994141898;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_GoogleAdProxy::.ctor()
extern "C"  void AN_GoogleAdProxy__ctor_m1272926743 (AN_GoogleAdProxy_t3994141898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_GoogleAdProxy_CallActivityFunction_m3573308880 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::InitMobileAd(System.String)
extern "C"  void AN_GoogleAdProxy_InitMobileAd_m794394178 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::ChangeBannersUnitID(System.String)
extern "C"  void AN_GoogleAdProxy_ChangeBannersUnitID_m1146320299 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::ChangeInterstisialsUnitID(System.String)
extern "C"  void AN_GoogleAdProxy_ChangeInterstisialsUnitID_m3199018190 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::CreateBannerAd(System.Int32,System.Int32,System.Int32)
extern "C"  void AN_GoogleAdProxy_CreateBannerAd_m1458978339 (Il2CppObject * __this /* static, unused */, int32_t ___gravity0, int32_t ___size1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::CreateBannerAdPos(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void AN_GoogleAdProxy_CreateBannerAdPos_m171371580 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t ___size2, int32_t ___id3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::SetBannerPosition(System.Int32,System.Int32)
extern "C"  void AN_GoogleAdProxy_SetBannerPosition_m3713766912 (Il2CppObject * __this /* static, unused */, int32_t ___gravity0, int32_t ___bannerId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::SetBannerPosition(System.Int32,System.Int32,System.Int32)
extern "C"  void AN_GoogleAdProxy_SetBannerPosition_m3260754545 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t ___bannerId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::HideAd(System.Int32)
extern "C"  void AN_GoogleAdProxy_HideAd_m3161651803 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::ShowAd(System.Int32)
extern "C"  void AN_GoogleAdProxy_ShowAd_m412500428 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::RefreshAd(System.Int32)
extern "C"  void AN_GoogleAdProxy_RefreshAd_m4257126226 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::PauseAd(System.Int32)
extern "C"  void AN_GoogleAdProxy_PauseAd_m3805913821 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::ResumeAd(System.Int32)
extern "C"  void AN_GoogleAdProxy_ResumeAd_m3427443502 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::DestroyBanner(System.Int32)
extern "C"  void AN_GoogleAdProxy_DestroyBanner_m320775052 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::StartInterstitialAd()
extern "C"  void AN_GoogleAdProxy_StartInterstitialAd_m1355572898 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::LoadInterstitialAd()
extern "C"  void AN_GoogleAdProxy_LoadInterstitialAd_m3985019658 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::ShowInterstitialAd()
extern "C"  void AN_GoogleAdProxy_ShowInterstitialAd_m3290730007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::RecordInAppResolution(System.Int32)
extern "C"  void AN_GoogleAdProxy_RecordInAppResolution_m3670519107 (Il2CppObject * __this /* static, unused */, int32_t ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::AddKeyword(System.String)
extern "C"  void AN_GoogleAdProxy_AddKeyword_m1749697797 (Il2CppObject * __this /* static, unused */, String_t* ___keyword0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::SetBirthday(System.Int32,System.Int32,System.Int32)
extern "C"  void AN_GoogleAdProxy_SetBirthday_m2681560813 (Il2CppObject * __this /* static, unused */, int32_t ___year0, int32_t ___month1, int32_t ___day2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::TagForChildDirectedTreatment(System.Boolean)
extern "C"  void AN_GoogleAdProxy_TagForChildDirectedTreatment_m2797659067 (Il2CppObject * __this /* static, unused */, bool ___tagForChildDirectedTreatment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::AddTestDevice(System.String)
extern "C"  void AN_GoogleAdProxy_AddTestDevice_m176132658 (Il2CppObject * __this /* static, unused */, String_t* ___deviceId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::AddTestDevices(System.String)
extern "C"  void AN_GoogleAdProxy_AddTestDevices_m1518696105 (Il2CppObject * __this /* static, unused */, String_t* ___cvsDeviceIds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GoogleAdProxy::SetGender(System.Int32)
extern "C"  void AN_GoogleAdProxy_SetGender_m1651386671 (Il2CppObject * __this /* static, unused */, int32_t ___gender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
