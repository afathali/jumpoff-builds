﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MCGBehaviour
struct MCGBehaviour_t3307770884;

#include "codegen/il2cpp-codegen.h"

// System.Void MCGBehaviour::.ctor()
extern "C"  void MCGBehaviour__ctor_m4192805763 (MCGBehaviour_t3307770884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour::.cctor()
extern "C"  void MCGBehaviour__cctor_m1454142902 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour::Awake()
extern "C"  void MCGBehaviour_Awake_m2307194864 (MCGBehaviour_t3307770884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour::AwakeOverride()
extern "C"  void MCGBehaviour_AwakeOverride_m3296464268 (MCGBehaviour_t3307770884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour::OnDestroy()
extern "C"  void MCGBehaviour_OnDestroy_m52495746 (MCGBehaviour_t3307770884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour::DestroyOverride()
extern "C"  void MCGBehaviour_DestroyOverride_m293088137 (MCGBehaviour_t3307770884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
