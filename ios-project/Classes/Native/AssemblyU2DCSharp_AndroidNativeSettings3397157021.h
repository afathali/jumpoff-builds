﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<GoogleProductTemplate>
struct List_1_t481737456;
// System.Collections.Generic.List`1<GPLeaderBoard>
struct List_1_t3018699018;
// System.Collections.Generic.List`1<GPAchievement>
struct List_1_t3648909186;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// AndroidNativeSettings
struct AndroidNativeSettings_t3397157021;

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "AssemblyU2DCSharp_AN_PushNotificationService941379374.h"
#include "AssemblyU2DCSharp_AN_CameraCaptureType74224559.h"
#include "AssemblyU2DCSharp_AndroidCameraImageFormat4079396078.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidNativeSettings
struct  AndroidNativeSettings_t3397157021  : public ScriptableObject_t1975622470
{
public:
	// System.Boolean AndroidNativeSettings::EnablePlusAPI
	bool ___EnablePlusAPI_7;
	// System.Boolean AndroidNativeSettings::EnableGamesAPI
	bool ___EnableGamesAPI_8;
	// System.Boolean AndroidNativeSettings::EnableAppInviteAPI
	bool ___EnableAppInviteAPI_9;
	// System.Boolean AndroidNativeSettings::EnableDriveAPI
	bool ___EnableDriveAPI_10;
	// System.Boolean AndroidNativeSettings::LoadProfileIcons
	bool ___LoadProfileIcons_11;
	// System.Boolean AndroidNativeSettings::LoadProfileImages
	bool ___LoadProfileImages_12;
	// System.Boolean AndroidNativeSettings::LoadQuestsImages
	bool ___LoadQuestsImages_13;
	// System.Boolean AndroidNativeSettings::LoadQuestsIcons
	bool ___LoadQuestsIcons_14;
	// System.Boolean AndroidNativeSettings::LoadEventsIcons
	bool ___LoadEventsIcons_15;
	// System.Boolean AndroidNativeSettings::ShowConnectingPopup
	bool ___ShowConnectingPopup_16;
	// System.Boolean AndroidNativeSettings::EnableATCSupport
	bool ___EnableATCSupport_17;
	// System.Boolean AndroidNativeSettings::OneSignalEnabled
	bool ___OneSignalEnabled_18;
	// System.String AndroidNativeSettings::OneSignalAppID
	String_t* ___OneSignalAppID_19;
	// System.String AndroidNativeSettings::OneSignalDownloadLink
	String_t* ___OneSignalDownloadLink_20;
	// System.String AndroidNativeSettings::OneSignalDocLink
	String_t* ___OneSignalDocLink_21;
	// System.Boolean AndroidNativeSettings::UseParsePushNotifications
	bool ___UseParsePushNotifications_22;
	// System.String AndroidNativeSettings::ParseAppId
	String_t* ___ParseAppId_23;
	// System.String AndroidNativeSettings::DotNetKey
	String_t* ___DotNetKey_24;
	// System.String AndroidNativeSettings::ParseDocLink
	String_t* ___ParseDocLink_25;
	// System.String AndroidNativeSettings::ParseDownloadLink
	String_t* ___ParseDownloadLink_26;
	// System.Boolean AndroidNativeSettings::EnableSoomla
	bool ___EnableSoomla_27;
	// System.String AndroidNativeSettings::SoomlaDownloadLink
	String_t* ___SoomlaDownloadLink_28;
	// System.String AndroidNativeSettings::SoomlaDocsLink
	String_t* ___SoomlaDocsLink_29;
	// System.String AndroidNativeSettings::SoomlaGameKey
	String_t* ___SoomlaGameKey_30;
	// System.String AndroidNativeSettings::SoomlaEnvKey
	String_t* ___SoomlaEnvKey_31;
	// System.String AndroidNativeSettings::GCM_SenderId
	String_t* ___GCM_SenderId_32;
	// AN_PushNotificationService AndroidNativeSettings::PushService
	int32_t ___PushService_33;
	// System.Boolean AndroidNativeSettings::SaveCameraImageToGallery
	bool ___SaveCameraImageToGallery_34;
	// System.Boolean AndroidNativeSettings::UseProductNameAsFolderName
	bool ___UseProductNameAsFolderName_35;
	// System.String AndroidNativeSettings::GalleryFolderName
	String_t* ___GalleryFolderName_36;
	// System.Int32 AndroidNativeSettings::MaxImageLoadSize
	int32_t ___MaxImageLoadSize_37;
	// AN_CameraCaptureType AndroidNativeSettings::CameraCaptureMode
	int32_t ___CameraCaptureMode_38;
	// AndroidCameraImageFormat AndroidNativeSettings::ImageFormat
	int32_t ___ImageFormat_39;
	// System.Boolean AndroidNativeSettings::ShowAppPermissions
	bool ___ShowAppPermissions_40;
	// System.Boolean AndroidNativeSettings::EnableBillingAPI
	bool ___EnableBillingAPI_41;
	// System.Boolean AndroidNativeSettings::EnablePSAPI
	bool ___EnablePSAPI_42;
	// System.Boolean AndroidNativeSettings::EnableSocialAPI
	bool ___EnableSocialAPI_43;
	// System.Boolean AndroidNativeSettings::EnableCameraAPI
	bool ___EnableCameraAPI_44;
	// System.Boolean AndroidNativeSettings::ExpandNativeAPI
	bool ___ExpandNativeAPI_45;
	// System.Boolean AndroidNativeSettings::ExpandPSAPI
	bool ___ExpandPSAPI_46;
	// System.Boolean AndroidNativeSettings::ExpandBillingAPI
	bool ___ExpandBillingAPI_47;
	// System.Boolean AndroidNativeSettings::ExpandSocialAPI
	bool ___ExpandSocialAPI_48;
	// System.Boolean AndroidNativeSettings::ExpandCameraAPI
	bool ___ExpandCameraAPI_49;
	// System.Boolean AndroidNativeSettings::ThirdPartyParams
	bool ___ThirdPartyParams_50;
	// System.Boolean AndroidNativeSettings::ShowPSSettingsResources
	bool ___ShowPSSettingsResources_51;
	// System.Boolean AndroidNativeSettings::ShowActions
	bool ___ShowActions_52;
	// System.Boolean AndroidNativeSettings::GCMSettingsActinve
	bool ___GCMSettingsActinve_53;
	// System.Boolean AndroidNativeSettings::LocalNotificationsAPI
	bool ___LocalNotificationsAPI_54;
	// System.Boolean AndroidNativeSettings::ImmersiveModeAPI
	bool ___ImmersiveModeAPI_55;
	// System.Boolean AndroidNativeSettings::ApplicationInformationAPI
	bool ___ApplicationInformationAPI_56;
	// System.Boolean AndroidNativeSettings::ExternalAppsAPI
	bool ___ExternalAppsAPI_57;
	// System.Boolean AndroidNativeSettings::PoupsandPreloadersAPI
	bool ___PoupsandPreloadersAPI_58;
	// System.Boolean AndroidNativeSettings::CheckAppLicenseAPI
	bool ___CheckAppLicenseAPI_59;
	// System.Boolean AndroidNativeSettings::NetworkStateAPI
	bool ___NetworkStateAPI_60;
	// System.Boolean AndroidNativeSettings::InAppPurchasesAPI
	bool ___InAppPurchasesAPI_61;
	// System.Boolean AndroidNativeSettings::GooglePlayServicesAPI
	bool ___GooglePlayServicesAPI_62;
	// System.Boolean AndroidNativeSettings::PlayServicesAdvancedSignInAPI
	bool ___PlayServicesAdvancedSignInAPI_63;
	// System.Boolean AndroidNativeSettings::GoogleButtonAPI
	bool ___GoogleButtonAPI_64;
	// System.Boolean AndroidNativeSettings::AnalyticsAPI
	bool ___AnalyticsAPI_65;
	// System.Boolean AndroidNativeSettings::GoogleCloudSaveAPI
	bool ___GoogleCloudSaveAPI_66;
	// System.Boolean AndroidNativeSettings::PushNotificationsAPI
	bool ___PushNotificationsAPI_67;
	// System.Boolean AndroidNativeSettings::GoogleMobileAdAPI
	bool ___GoogleMobileAdAPI_68;
	// System.Boolean AndroidNativeSettings::GalleryAPI
	bool ___GalleryAPI_69;
	// System.Boolean AndroidNativeSettings::CameraAPI
	bool ___CameraAPI_70;
	// System.Boolean AndroidNativeSettings::KeepManifestClean
	bool ___KeepManifestClean_71;
	// System.String AndroidNativeSettings::GooglePlayServiceAppID
	String_t* ___GooglePlayServiceAppID_72;
	// System.Int32 AndroidNativeSettings::ToolbarSelectedIndex
	int32_t ___ToolbarSelectedIndex_73;
	// System.String AndroidNativeSettings::base64EncodedPublicKey
	String_t* ___base64EncodedPublicKey_74;
	// System.Boolean AndroidNativeSettings::ShowStoreProducts
	bool ___ShowStoreProducts_75;
	// System.Collections.Generic.List`1<GoogleProductTemplate> AndroidNativeSettings::InAppProducts
	List_1_t481737456 * ___InAppProducts_76;
	// System.Boolean AndroidNativeSettings::ShowLeaderboards
	bool ___ShowLeaderboards_77;
	// System.Collections.Generic.List`1<GPLeaderBoard> AndroidNativeSettings::Leaderboards
	List_1_t3018699018 * ___Leaderboards_78;
	// System.Boolean AndroidNativeSettings::ShowAchievements
	bool ___ShowAchievements_79;
	// System.Collections.Generic.List`1<GPAchievement> AndroidNativeSettings::Achievements
	List_1_t3648909186 * ___Achievements_80;
	// System.Boolean AndroidNativeSettings::ShowWhenAppIsForeground
	bool ___ShowWhenAppIsForeground_81;
	// System.Boolean AndroidNativeSettings::EnableVibrationLocal
	bool ___EnableVibrationLocal_82;
	// UnityEngine.Texture2D AndroidNativeSettings::LocalNotificationSmallIcon
	Texture2D_t3542995729 * ___LocalNotificationSmallIcon_83;
	// UnityEngine.Texture2D AndroidNativeSettings::LocalNotificationLargeIcon
	Texture2D_t3542995729 * ___LocalNotificationLargeIcon_84;
	// UnityEngine.AudioClip AndroidNativeSettings::LocalNotificationSound
	AudioClip_t1932558630 * ___LocalNotificationSound_85;
	// System.Boolean AndroidNativeSettings::ReplaceOldNotificationWithNew
	bool ___ReplaceOldNotificationWithNew_86;
	// System.Boolean AndroidNativeSettings::ShowPushWhenAppIsForeground
	bool ___ShowPushWhenAppIsForeground_87;
	// System.Boolean AndroidNativeSettings::EnableVibrationPush
	bool ___EnableVibrationPush_88;
	// UnityEngine.Texture2D AndroidNativeSettings::PushNotificationSmallIcon
	Texture2D_t3542995729 * ___PushNotificationSmallIcon_89;
	// UnityEngine.Texture2D AndroidNativeSettings::PushNotificationLargeIcon
	Texture2D_t3542995729 * ___PushNotificationLargeIcon_90;
	// UnityEngine.AudioClip AndroidNativeSettings::PushNotificationSound
	AudioClip_t1932558630 * ___PushNotificationSound_91;

public:
	inline static int32_t get_offset_of_EnablePlusAPI_7() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___EnablePlusAPI_7)); }
	inline bool get_EnablePlusAPI_7() const { return ___EnablePlusAPI_7; }
	inline bool* get_address_of_EnablePlusAPI_7() { return &___EnablePlusAPI_7; }
	inline void set_EnablePlusAPI_7(bool value)
	{
		___EnablePlusAPI_7 = value;
	}

	inline static int32_t get_offset_of_EnableGamesAPI_8() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___EnableGamesAPI_8)); }
	inline bool get_EnableGamesAPI_8() const { return ___EnableGamesAPI_8; }
	inline bool* get_address_of_EnableGamesAPI_8() { return &___EnableGamesAPI_8; }
	inline void set_EnableGamesAPI_8(bool value)
	{
		___EnableGamesAPI_8 = value;
	}

	inline static int32_t get_offset_of_EnableAppInviteAPI_9() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___EnableAppInviteAPI_9)); }
	inline bool get_EnableAppInviteAPI_9() const { return ___EnableAppInviteAPI_9; }
	inline bool* get_address_of_EnableAppInviteAPI_9() { return &___EnableAppInviteAPI_9; }
	inline void set_EnableAppInviteAPI_9(bool value)
	{
		___EnableAppInviteAPI_9 = value;
	}

	inline static int32_t get_offset_of_EnableDriveAPI_10() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___EnableDriveAPI_10)); }
	inline bool get_EnableDriveAPI_10() const { return ___EnableDriveAPI_10; }
	inline bool* get_address_of_EnableDriveAPI_10() { return &___EnableDriveAPI_10; }
	inline void set_EnableDriveAPI_10(bool value)
	{
		___EnableDriveAPI_10 = value;
	}

	inline static int32_t get_offset_of_LoadProfileIcons_11() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___LoadProfileIcons_11)); }
	inline bool get_LoadProfileIcons_11() const { return ___LoadProfileIcons_11; }
	inline bool* get_address_of_LoadProfileIcons_11() { return &___LoadProfileIcons_11; }
	inline void set_LoadProfileIcons_11(bool value)
	{
		___LoadProfileIcons_11 = value;
	}

	inline static int32_t get_offset_of_LoadProfileImages_12() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___LoadProfileImages_12)); }
	inline bool get_LoadProfileImages_12() const { return ___LoadProfileImages_12; }
	inline bool* get_address_of_LoadProfileImages_12() { return &___LoadProfileImages_12; }
	inline void set_LoadProfileImages_12(bool value)
	{
		___LoadProfileImages_12 = value;
	}

	inline static int32_t get_offset_of_LoadQuestsImages_13() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___LoadQuestsImages_13)); }
	inline bool get_LoadQuestsImages_13() const { return ___LoadQuestsImages_13; }
	inline bool* get_address_of_LoadQuestsImages_13() { return &___LoadQuestsImages_13; }
	inline void set_LoadQuestsImages_13(bool value)
	{
		___LoadQuestsImages_13 = value;
	}

	inline static int32_t get_offset_of_LoadQuestsIcons_14() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___LoadQuestsIcons_14)); }
	inline bool get_LoadQuestsIcons_14() const { return ___LoadQuestsIcons_14; }
	inline bool* get_address_of_LoadQuestsIcons_14() { return &___LoadQuestsIcons_14; }
	inline void set_LoadQuestsIcons_14(bool value)
	{
		___LoadQuestsIcons_14 = value;
	}

	inline static int32_t get_offset_of_LoadEventsIcons_15() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___LoadEventsIcons_15)); }
	inline bool get_LoadEventsIcons_15() const { return ___LoadEventsIcons_15; }
	inline bool* get_address_of_LoadEventsIcons_15() { return &___LoadEventsIcons_15; }
	inline void set_LoadEventsIcons_15(bool value)
	{
		___LoadEventsIcons_15 = value;
	}

	inline static int32_t get_offset_of_ShowConnectingPopup_16() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ShowConnectingPopup_16)); }
	inline bool get_ShowConnectingPopup_16() const { return ___ShowConnectingPopup_16; }
	inline bool* get_address_of_ShowConnectingPopup_16() { return &___ShowConnectingPopup_16; }
	inline void set_ShowConnectingPopup_16(bool value)
	{
		___ShowConnectingPopup_16 = value;
	}

	inline static int32_t get_offset_of_EnableATCSupport_17() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___EnableATCSupport_17)); }
	inline bool get_EnableATCSupport_17() const { return ___EnableATCSupport_17; }
	inline bool* get_address_of_EnableATCSupport_17() { return &___EnableATCSupport_17; }
	inline void set_EnableATCSupport_17(bool value)
	{
		___EnableATCSupport_17 = value;
	}

	inline static int32_t get_offset_of_OneSignalEnabled_18() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___OneSignalEnabled_18)); }
	inline bool get_OneSignalEnabled_18() const { return ___OneSignalEnabled_18; }
	inline bool* get_address_of_OneSignalEnabled_18() { return &___OneSignalEnabled_18; }
	inline void set_OneSignalEnabled_18(bool value)
	{
		___OneSignalEnabled_18 = value;
	}

	inline static int32_t get_offset_of_OneSignalAppID_19() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___OneSignalAppID_19)); }
	inline String_t* get_OneSignalAppID_19() const { return ___OneSignalAppID_19; }
	inline String_t** get_address_of_OneSignalAppID_19() { return &___OneSignalAppID_19; }
	inline void set_OneSignalAppID_19(String_t* value)
	{
		___OneSignalAppID_19 = value;
		Il2CppCodeGenWriteBarrier(&___OneSignalAppID_19, value);
	}

	inline static int32_t get_offset_of_OneSignalDownloadLink_20() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___OneSignalDownloadLink_20)); }
	inline String_t* get_OneSignalDownloadLink_20() const { return ___OneSignalDownloadLink_20; }
	inline String_t** get_address_of_OneSignalDownloadLink_20() { return &___OneSignalDownloadLink_20; }
	inline void set_OneSignalDownloadLink_20(String_t* value)
	{
		___OneSignalDownloadLink_20 = value;
		Il2CppCodeGenWriteBarrier(&___OneSignalDownloadLink_20, value);
	}

	inline static int32_t get_offset_of_OneSignalDocLink_21() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___OneSignalDocLink_21)); }
	inline String_t* get_OneSignalDocLink_21() const { return ___OneSignalDocLink_21; }
	inline String_t** get_address_of_OneSignalDocLink_21() { return &___OneSignalDocLink_21; }
	inline void set_OneSignalDocLink_21(String_t* value)
	{
		___OneSignalDocLink_21 = value;
		Il2CppCodeGenWriteBarrier(&___OneSignalDocLink_21, value);
	}

	inline static int32_t get_offset_of_UseParsePushNotifications_22() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___UseParsePushNotifications_22)); }
	inline bool get_UseParsePushNotifications_22() const { return ___UseParsePushNotifications_22; }
	inline bool* get_address_of_UseParsePushNotifications_22() { return &___UseParsePushNotifications_22; }
	inline void set_UseParsePushNotifications_22(bool value)
	{
		___UseParsePushNotifications_22 = value;
	}

	inline static int32_t get_offset_of_ParseAppId_23() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ParseAppId_23)); }
	inline String_t* get_ParseAppId_23() const { return ___ParseAppId_23; }
	inline String_t** get_address_of_ParseAppId_23() { return &___ParseAppId_23; }
	inline void set_ParseAppId_23(String_t* value)
	{
		___ParseAppId_23 = value;
		Il2CppCodeGenWriteBarrier(&___ParseAppId_23, value);
	}

	inline static int32_t get_offset_of_DotNetKey_24() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___DotNetKey_24)); }
	inline String_t* get_DotNetKey_24() const { return ___DotNetKey_24; }
	inline String_t** get_address_of_DotNetKey_24() { return &___DotNetKey_24; }
	inline void set_DotNetKey_24(String_t* value)
	{
		___DotNetKey_24 = value;
		Il2CppCodeGenWriteBarrier(&___DotNetKey_24, value);
	}

	inline static int32_t get_offset_of_ParseDocLink_25() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ParseDocLink_25)); }
	inline String_t* get_ParseDocLink_25() const { return ___ParseDocLink_25; }
	inline String_t** get_address_of_ParseDocLink_25() { return &___ParseDocLink_25; }
	inline void set_ParseDocLink_25(String_t* value)
	{
		___ParseDocLink_25 = value;
		Il2CppCodeGenWriteBarrier(&___ParseDocLink_25, value);
	}

	inline static int32_t get_offset_of_ParseDownloadLink_26() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ParseDownloadLink_26)); }
	inline String_t* get_ParseDownloadLink_26() const { return ___ParseDownloadLink_26; }
	inline String_t** get_address_of_ParseDownloadLink_26() { return &___ParseDownloadLink_26; }
	inline void set_ParseDownloadLink_26(String_t* value)
	{
		___ParseDownloadLink_26 = value;
		Il2CppCodeGenWriteBarrier(&___ParseDownloadLink_26, value);
	}

	inline static int32_t get_offset_of_EnableSoomla_27() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___EnableSoomla_27)); }
	inline bool get_EnableSoomla_27() const { return ___EnableSoomla_27; }
	inline bool* get_address_of_EnableSoomla_27() { return &___EnableSoomla_27; }
	inline void set_EnableSoomla_27(bool value)
	{
		___EnableSoomla_27 = value;
	}

	inline static int32_t get_offset_of_SoomlaDownloadLink_28() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___SoomlaDownloadLink_28)); }
	inline String_t* get_SoomlaDownloadLink_28() const { return ___SoomlaDownloadLink_28; }
	inline String_t** get_address_of_SoomlaDownloadLink_28() { return &___SoomlaDownloadLink_28; }
	inline void set_SoomlaDownloadLink_28(String_t* value)
	{
		___SoomlaDownloadLink_28 = value;
		Il2CppCodeGenWriteBarrier(&___SoomlaDownloadLink_28, value);
	}

	inline static int32_t get_offset_of_SoomlaDocsLink_29() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___SoomlaDocsLink_29)); }
	inline String_t* get_SoomlaDocsLink_29() const { return ___SoomlaDocsLink_29; }
	inline String_t** get_address_of_SoomlaDocsLink_29() { return &___SoomlaDocsLink_29; }
	inline void set_SoomlaDocsLink_29(String_t* value)
	{
		___SoomlaDocsLink_29 = value;
		Il2CppCodeGenWriteBarrier(&___SoomlaDocsLink_29, value);
	}

	inline static int32_t get_offset_of_SoomlaGameKey_30() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___SoomlaGameKey_30)); }
	inline String_t* get_SoomlaGameKey_30() const { return ___SoomlaGameKey_30; }
	inline String_t** get_address_of_SoomlaGameKey_30() { return &___SoomlaGameKey_30; }
	inline void set_SoomlaGameKey_30(String_t* value)
	{
		___SoomlaGameKey_30 = value;
		Il2CppCodeGenWriteBarrier(&___SoomlaGameKey_30, value);
	}

	inline static int32_t get_offset_of_SoomlaEnvKey_31() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___SoomlaEnvKey_31)); }
	inline String_t* get_SoomlaEnvKey_31() const { return ___SoomlaEnvKey_31; }
	inline String_t** get_address_of_SoomlaEnvKey_31() { return &___SoomlaEnvKey_31; }
	inline void set_SoomlaEnvKey_31(String_t* value)
	{
		___SoomlaEnvKey_31 = value;
		Il2CppCodeGenWriteBarrier(&___SoomlaEnvKey_31, value);
	}

	inline static int32_t get_offset_of_GCM_SenderId_32() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___GCM_SenderId_32)); }
	inline String_t* get_GCM_SenderId_32() const { return ___GCM_SenderId_32; }
	inline String_t** get_address_of_GCM_SenderId_32() { return &___GCM_SenderId_32; }
	inline void set_GCM_SenderId_32(String_t* value)
	{
		___GCM_SenderId_32 = value;
		Il2CppCodeGenWriteBarrier(&___GCM_SenderId_32, value);
	}

	inline static int32_t get_offset_of_PushService_33() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___PushService_33)); }
	inline int32_t get_PushService_33() const { return ___PushService_33; }
	inline int32_t* get_address_of_PushService_33() { return &___PushService_33; }
	inline void set_PushService_33(int32_t value)
	{
		___PushService_33 = value;
	}

	inline static int32_t get_offset_of_SaveCameraImageToGallery_34() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___SaveCameraImageToGallery_34)); }
	inline bool get_SaveCameraImageToGallery_34() const { return ___SaveCameraImageToGallery_34; }
	inline bool* get_address_of_SaveCameraImageToGallery_34() { return &___SaveCameraImageToGallery_34; }
	inline void set_SaveCameraImageToGallery_34(bool value)
	{
		___SaveCameraImageToGallery_34 = value;
	}

	inline static int32_t get_offset_of_UseProductNameAsFolderName_35() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___UseProductNameAsFolderName_35)); }
	inline bool get_UseProductNameAsFolderName_35() const { return ___UseProductNameAsFolderName_35; }
	inline bool* get_address_of_UseProductNameAsFolderName_35() { return &___UseProductNameAsFolderName_35; }
	inline void set_UseProductNameAsFolderName_35(bool value)
	{
		___UseProductNameAsFolderName_35 = value;
	}

	inline static int32_t get_offset_of_GalleryFolderName_36() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___GalleryFolderName_36)); }
	inline String_t* get_GalleryFolderName_36() const { return ___GalleryFolderName_36; }
	inline String_t** get_address_of_GalleryFolderName_36() { return &___GalleryFolderName_36; }
	inline void set_GalleryFolderName_36(String_t* value)
	{
		___GalleryFolderName_36 = value;
		Il2CppCodeGenWriteBarrier(&___GalleryFolderName_36, value);
	}

	inline static int32_t get_offset_of_MaxImageLoadSize_37() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___MaxImageLoadSize_37)); }
	inline int32_t get_MaxImageLoadSize_37() const { return ___MaxImageLoadSize_37; }
	inline int32_t* get_address_of_MaxImageLoadSize_37() { return &___MaxImageLoadSize_37; }
	inline void set_MaxImageLoadSize_37(int32_t value)
	{
		___MaxImageLoadSize_37 = value;
	}

	inline static int32_t get_offset_of_CameraCaptureMode_38() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___CameraCaptureMode_38)); }
	inline int32_t get_CameraCaptureMode_38() const { return ___CameraCaptureMode_38; }
	inline int32_t* get_address_of_CameraCaptureMode_38() { return &___CameraCaptureMode_38; }
	inline void set_CameraCaptureMode_38(int32_t value)
	{
		___CameraCaptureMode_38 = value;
	}

	inline static int32_t get_offset_of_ImageFormat_39() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ImageFormat_39)); }
	inline int32_t get_ImageFormat_39() const { return ___ImageFormat_39; }
	inline int32_t* get_address_of_ImageFormat_39() { return &___ImageFormat_39; }
	inline void set_ImageFormat_39(int32_t value)
	{
		___ImageFormat_39 = value;
	}

	inline static int32_t get_offset_of_ShowAppPermissions_40() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ShowAppPermissions_40)); }
	inline bool get_ShowAppPermissions_40() const { return ___ShowAppPermissions_40; }
	inline bool* get_address_of_ShowAppPermissions_40() { return &___ShowAppPermissions_40; }
	inline void set_ShowAppPermissions_40(bool value)
	{
		___ShowAppPermissions_40 = value;
	}

	inline static int32_t get_offset_of_EnableBillingAPI_41() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___EnableBillingAPI_41)); }
	inline bool get_EnableBillingAPI_41() const { return ___EnableBillingAPI_41; }
	inline bool* get_address_of_EnableBillingAPI_41() { return &___EnableBillingAPI_41; }
	inline void set_EnableBillingAPI_41(bool value)
	{
		___EnableBillingAPI_41 = value;
	}

	inline static int32_t get_offset_of_EnablePSAPI_42() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___EnablePSAPI_42)); }
	inline bool get_EnablePSAPI_42() const { return ___EnablePSAPI_42; }
	inline bool* get_address_of_EnablePSAPI_42() { return &___EnablePSAPI_42; }
	inline void set_EnablePSAPI_42(bool value)
	{
		___EnablePSAPI_42 = value;
	}

	inline static int32_t get_offset_of_EnableSocialAPI_43() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___EnableSocialAPI_43)); }
	inline bool get_EnableSocialAPI_43() const { return ___EnableSocialAPI_43; }
	inline bool* get_address_of_EnableSocialAPI_43() { return &___EnableSocialAPI_43; }
	inline void set_EnableSocialAPI_43(bool value)
	{
		___EnableSocialAPI_43 = value;
	}

	inline static int32_t get_offset_of_EnableCameraAPI_44() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___EnableCameraAPI_44)); }
	inline bool get_EnableCameraAPI_44() const { return ___EnableCameraAPI_44; }
	inline bool* get_address_of_EnableCameraAPI_44() { return &___EnableCameraAPI_44; }
	inline void set_EnableCameraAPI_44(bool value)
	{
		___EnableCameraAPI_44 = value;
	}

	inline static int32_t get_offset_of_ExpandNativeAPI_45() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ExpandNativeAPI_45)); }
	inline bool get_ExpandNativeAPI_45() const { return ___ExpandNativeAPI_45; }
	inline bool* get_address_of_ExpandNativeAPI_45() { return &___ExpandNativeAPI_45; }
	inline void set_ExpandNativeAPI_45(bool value)
	{
		___ExpandNativeAPI_45 = value;
	}

	inline static int32_t get_offset_of_ExpandPSAPI_46() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ExpandPSAPI_46)); }
	inline bool get_ExpandPSAPI_46() const { return ___ExpandPSAPI_46; }
	inline bool* get_address_of_ExpandPSAPI_46() { return &___ExpandPSAPI_46; }
	inline void set_ExpandPSAPI_46(bool value)
	{
		___ExpandPSAPI_46 = value;
	}

	inline static int32_t get_offset_of_ExpandBillingAPI_47() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ExpandBillingAPI_47)); }
	inline bool get_ExpandBillingAPI_47() const { return ___ExpandBillingAPI_47; }
	inline bool* get_address_of_ExpandBillingAPI_47() { return &___ExpandBillingAPI_47; }
	inline void set_ExpandBillingAPI_47(bool value)
	{
		___ExpandBillingAPI_47 = value;
	}

	inline static int32_t get_offset_of_ExpandSocialAPI_48() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ExpandSocialAPI_48)); }
	inline bool get_ExpandSocialAPI_48() const { return ___ExpandSocialAPI_48; }
	inline bool* get_address_of_ExpandSocialAPI_48() { return &___ExpandSocialAPI_48; }
	inline void set_ExpandSocialAPI_48(bool value)
	{
		___ExpandSocialAPI_48 = value;
	}

	inline static int32_t get_offset_of_ExpandCameraAPI_49() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ExpandCameraAPI_49)); }
	inline bool get_ExpandCameraAPI_49() const { return ___ExpandCameraAPI_49; }
	inline bool* get_address_of_ExpandCameraAPI_49() { return &___ExpandCameraAPI_49; }
	inline void set_ExpandCameraAPI_49(bool value)
	{
		___ExpandCameraAPI_49 = value;
	}

	inline static int32_t get_offset_of_ThirdPartyParams_50() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ThirdPartyParams_50)); }
	inline bool get_ThirdPartyParams_50() const { return ___ThirdPartyParams_50; }
	inline bool* get_address_of_ThirdPartyParams_50() { return &___ThirdPartyParams_50; }
	inline void set_ThirdPartyParams_50(bool value)
	{
		___ThirdPartyParams_50 = value;
	}

	inline static int32_t get_offset_of_ShowPSSettingsResources_51() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ShowPSSettingsResources_51)); }
	inline bool get_ShowPSSettingsResources_51() const { return ___ShowPSSettingsResources_51; }
	inline bool* get_address_of_ShowPSSettingsResources_51() { return &___ShowPSSettingsResources_51; }
	inline void set_ShowPSSettingsResources_51(bool value)
	{
		___ShowPSSettingsResources_51 = value;
	}

	inline static int32_t get_offset_of_ShowActions_52() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ShowActions_52)); }
	inline bool get_ShowActions_52() const { return ___ShowActions_52; }
	inline bool* get_address_of_ShowActions_52() { return &___ShowActions_52; }
	inline void set_ShowActions_52(bool value)
	{
		___ShowActions_52 = value;
	}

	inline static int32_t get_offset_of_GCMSettingsActinve_53() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___GCMSettingsActinve_53)); }
	inline bool get_GCMSettingsActinve_53() const { return ___GCMSettingsActinve_53; }
	inline bool* get_address_of_GCMSettingsActinve_53() { return &___GCMSettingsActinve_53; }
	inline void set_GCMSettingsActinve_53(bool value)
	{
		___GCMSettingsActinve_53 = value;
	}

	inline static int32_t get_offset_of_LocalNotificationsAPI_54() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___LocalNotificationsAPI_54)); }
	inline bool get_LocalNotificationsAPI_54() const { return ___LocalNotificationsAPI_54; }
	inline bool* get_address_of_LocalNotificationsAPI_54() { return &___LocalNotificationsAPI_54; }
	inline void set_LocalNotificationsAPI_54(bool value)
	{
		___LocalNotificationsAPI_54 = value;
	}

	inline static int32_t get_offset_of_ImmersiveModeAPI_55() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ImmersiveModeAPI_55)); }
	inline bool get_ImmersiveModeAPI_55() const { return ___ImmersiveModeAPI_55; }
	inline bool* get_address_of_ImmersiveModeAPI_55() { return &___ImmersiveModeAPI_55; }
	inline void set_ImmersiveModeAPI_55(bool value)
	{
		___ImmersiveModeAPI_55 = value;
	}

	inline static int32_t get_offset_of_ApplicationInformationAPI_56() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ApplicationInformationAPI_56)); }
	inline bool get_ApplicationInformationAPI_56() const { return ___ApplicationInformationAPI_56; }
	inline bool* get_address_of_ApplicationInformationAPI_56() { return &___ApplicationInformationAPI_56; }
	inline void set_ApplicationInformationAPI_56(bool value)
	{
		___ApplicationInformationAPI_56 = value;
	}

	inline static int32_t get_offset_of_ExternalAppsAPI_57() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ExternalAppsAPI_57)); }
	inline bool get_ExternalAppsAPI_57() const { return ___ExternalAppsAPI_57; }
	inline bool* get_address_of_ExternalAppsAPI_57() { return &___ExternalAppsAPI_57; }
	inline void set_ExternalAppsAPI_57(bool value)
	{
		___ExternalAppsAPI_57 = value;
	}

	inline static int32_t get_offset_of_PoupsandPreloadersAPI_58() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___PoupsandPreloadersAPI_58)); }
	inline bool get_PoupsandPreloadersAPI_58() const { return ___PoupsandPreloadersAPI_58; }
	inline bool* get_address_of_PoupsandPreloadersAPI_58() { return &___PoupsandPreloadersAPI_58; }
	inline void set_PoupsandPreloadersAPI_58(bool value)
	{
		___PoupsandPreloadersAPI_58 = value;
	}

	inline static int32_t get_offset_of_CheckAppLicenseAPI_59() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___CheckAppLicenseAPI_59)); }
	inline bool get_CheckAppLicenseAPI_59() const { return ___CheckAppLicenseAPI_59; }
	inline bool* get_address_of_CheckAppLicenseAPI_59() { return &___CheckAppLicenseAPI_59; }
	inline void set_CheckAppLicenseAPI_59(bool value)
	{
		___CheckAppLicenseAPI_59 = value;
	}

	inline static int32_t get_offset_of_NetworkStateAPI_60() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___NetworkStateAPI_60)); }
	inline bool get_NetworkStateAPI_60() const { return ___NetworkStateAPI_60; }
	inline bool* get_address_of_NetworkStateAPI_60() { return &___NetworkStateAPI_60; }
	inline void set_NetworkStateAPI_60(bool value)
	{
		___NetworkStateAPI_60 = value;
	}

	inline static int32_t get_offset_of_InAppPurchasesAPI_61() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___InAppPurchasesAPI_61)); }
	inline bool get_InAppPurchasesAPI_61() const { return ___InAppPurchasesAPI_61; }
	inline bool* get_address_of_InAppPurchasesAPI_61() { return &___InAppPurchasesAPI_61; }
	inline void set_InAppPurchasesAPI_61(bool value)
	{
		___InAppPurchasesAPI_61 = value;
	}

	inline static int32_t get_offset_of_GooglePlayServicesAPI_62() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___GooglePlayServicesAPI_62)); }
	inline bool get_GooglePlayServicesAPI_62() const { return ___GooglePlayServicesAPI_62; }
	inline bool* get_address_of_GooglePlayServicesAPI_62() { return &___GooglePlayServicesAPI_62; }
	inline void set_GooglePlayServicesAPI_62(bool value)
	{
		___GooglePlayServicesAPI_62 = value;
	}

	inline static int32_t get_offset_of_PlayServicesAdvancedSignInAPI_63() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___PlayServicesAdvancedSignInAPI_63)); }
	inline bool get_PlayServicesAdvancedSignInAPI_63() const { return ___PlayServicesAdvancedSignInAPI_63; }
	inline bool* get_address_of_PlayServicesAdvancedSignInAPI_63() { return &___PlayServicesAdvancedSignInAPI_63; }
	inline void set_PlayServicesAdvancedSignInAPI_63(bool value)
	{
		___PlayServicesAdvancedSignInAPI_63 = value;
	}

	inline static int32_t get_offset_of_GoogleButtonAPI_64() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___GoogleButtonAPI_64)); }
	inline bool get_GoogleButtonAPI_64() const { return ___GoogleButtonAPI_64; }
	inline bool* get_address_of_GoogleButtonAPI_64() { return &___GoogleButtonAPI_64; }
	inline void set_GoogleButtonAPI_64(bool value)
	{
		___GoogleButtonAPI_64 = value;
	}

	inline static int32_t get_offset_of_AnalyticsAPI_65() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___AnalyticsAPI_65)); }
	inline bool get_AnalyticsAPI_65() const { return ___AnalyticsAPI_65; }
	inline bool* get_address_of_AnalyticsAPI_65() { return &___AnalyticsAPI_65; }
	inline void set_AnalyticsAPI_65(bool value)
	{
		___AnalyticsAPI_65 = value;
	}

	inline static int32_t get_offset_of_GoogleCloudSaveAPI_66() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___GoogleCloudSaveAPI_66)); }
	inline bool get_GoogleCloudSaveAPI_66() const { return ___GoogleCloudSaveAPI_66; }
	inline bool* get_address_of_GoogleCloudSaveAPI_66() { return &___GoogleCloudSaveAPI_66; }
	inline void set_GoogleCloudSaveAPI_66(bool value)
	{
		___GoogleCloudSaveAPI_66 = value;
	}

	inline static int32_t get_offset_of_PushNotificationsAPI_67() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___PushNotificationsAPI_67)); }
	inline bool get_PushNotificationsAPI_67() const { return ___PushNotificationsAPI_67; }
	inline bool* get_address_of_PushNotificationsAPI_67() { return &___PushNotificationsAPI_67; }
	inline void set_PushNotificationsAPI_67(bool value)
	{
		___PushNotificationsAPI_67 = value;
	}

	inline static int32_t get_offset_of_GoogleMobileAdAPI_68() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___GoogleMobileAdAPI_68)); }
	inline bool get_GoogleMobileAdAPI_68() const { return ___GoogleMobileAdAPI_68; }
	inline bool* get_address_of_GoogleMobileAdAPI_68() { return &___GoogleMobileAdAPI_68; }
	inline void set_GoogleMobileAdAPI_68(bool value)
	{
		___GoogleMobileAdAPI_68 = value;
	}

	inline static int32_t get_offset_of_GalleryAPI_69() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___GalleryAPI_69)); }
	inline bool get_GalleryAPI_69() const { return ___GalleryAPI_69; }
	inline bool* get_address_of_GalleryAPI_69() { return &___GalleryAPI_69; }
	inline void set_GalleryAPI_69(bool value)
	{
		___GalleryAPI_69 = value;
	}

	inline static int32_t get_offset_of_CameraAPI_70() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___CameraAPI_70)); }
	inline bool get_CameraAPI_70() const { return ___CameraAPI_70; }
	inline bool* get_address_of_CameraAPI_70() { return &___CameraAPI_70; }
	inline void set_CameraAPI_70(bool value)
	{
		___CameraAPI_70 = value;
	}

	inline static int32_t get_offset_of_KeepManifestClean_71() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___KeepManifestClean_71)); }
	inline bool get_KeepManifestClean_71() const { return ___KeepManifestClean_71; }
	inline bool* get_address_of_KeepManifestClean_71() { return &___KeepManifestClean_71; }
	inline void set_KeepManifestClean_71(bool value)
	{
		___KeepManifestClean_71 = value;
	}

	inline static int32_t get_offset_of_GooglePlayServiceAppID_72() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___GooglePlayServiceAppID_72)); }
	inline String_t* get_GooglePlayServiceAppID_72() const { return ___GooglePlayServiceAppID_72; }
	inline String_t** get_address_of_GooglePlayServiceAppID_72() { return &___GooglePlayServiceAppID_72; }
	inline void set_GooglePlayServiceAppID_72(String_t* value)
	{
		___GooglePlayServiceAppID_72 = value;
		Il2CppCodeGenWriteBarrier(&___GooglePlayServiceAppID_72, value);
	}

	inline static int32_t get_offset_of_ToolbarSelectedIndex_73() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ToolbarSelectedIndex_73)); }
	inline int32_t get_ToolbarSelectedIndex_73() const { return ___ToolbarSelectedIndex_73; }
	inline int32_t* get_address_of_ToolbarSelectedIndex_73() { return &___ToolbarSelectedIndex_73; }
	inline void set_ToolbarSelectedIndex_73(int32_t value)
	{
		___ToolbarSelectedIndex_73 = value;
	}

	inline static int32_t get_offset_of_base64EncodedPublicKey_74() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___base64EncodedPublicKey_74)); }
	inline String_t* get_base64EncodedPublicKey_74() const { return ___base64EncodedPublicKey_74; }
	inline String_t** get_address_of_base64EncodedPublicKey_74() { return &___base64EncodedPublicKey_74; }
	inline void set_base64EncodedPublicKey_74(String_t* value)
	{
		___base64EncodedPublicKey_74 = value;
		Il2CppCodeGenWriteBarrier(&___base64EncodedPublicKey_74, value);
	}

	inline static int32_t get_offset_of_ShowStoreProducts_75() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ShowStoreProducts_75)); }
	inline bool get_ShowStoreProducts_75() const { return ___ShowStoreProducts_75; }
	inline bool* get_address_of_ShowStoreProducts_75() { return &___ShowStoreProducts_75; }
	inline void set_ShowStoreProducts_75(bool value)
	{
		___ShowStoreProducts_75 = value;
	}

	inline static int32_t get_offset_of_InAppProducts_76() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___InAppProducts_76)); }
	inline List_1_t481737456 * get_InAppProducts_76() const { return ___InAppProducts_76; }
	inline List_1_t481737456 ** get_address_of_InAppProducts_76() { return &___InAppProducts_76; }
	inline void set_InAppProducts_76(List_1_t481737456 * value)
	{
		___InAppProducts_76 = value;
		Il2CppCodeGenWriteBarrier(&___InAppProducts_76, value);
	}

	inline static int32_t get_offset_of_ShowLeaderboards_77() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ShowLeaderboards_77)); }
	inline bool get_ShowLeaderboards_77() const { return ___ShowLeaderboards_77; }
	inline bool* get_address_of_ShowLeaderboards_77() { return &___ShowLeaderboards_77; }
	inline void set_ShowLeaderboards_77(bool value)
	{
		___ShowLeaderboards_77 = value;
	}

	inline static int32_t get_offset_of_Leaderboards_78() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___Leaderboards_78)); }
	inline List_1_t3018699018 * get_Leaderboards_78() const { return ___Leaderboards_78; }
	inline List_1_t3018699018 ** get_address_of_Leaderboards_78() { return &___Leaderboards_78; }
	inline void set_Leaderboards_78(List_1_t3018699018 * value)
	{
		___Leaderboards_78 = value;
		Il2CppCodeGenWriteBarrier(&___Leaderboards_78, value);
	}

	inline static int32_t get_offset_of_ShowAchievements_79() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ShowAchievements_79)); }
	inline bool get_ShowAchievements_79() const { return ___ShowAchievements_79; }
	inline bool* get_address_of_ShowAchievements_79() { return &___ShowAchievements_79; }
	inline void set_ShowAchievements_79(bool value)
	{
		___ShowAchievements_79 = value;
	}

	inline static int32_t get_offset_of_Achievements_80() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___Achievements_80)); }
	inline List_1_t3648909186 * get_Achievements_80() const { return ___Achievements_80; }
	inline List_1_t3648909186 ** get_address_of_Achievements_80() { return &___Achievements_80; }
	inline void set_Achievements_80(List_1_t3648909186 * value)
	{
		___Achievements_80 = value;
		Il2CppCodeGenWriteBarrier(&___Achievements_80, value);
	}

	inline static int32_t get_offset_of_ShowWhenAppIsForeground_81() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ShowWhenAppIsForeground_81)); }
	inline bool get_ShowWhenAppIsForeground_81() const { return ___ShowWhenAppIsForeground_81; }
	inline bool* get_address_of_ShowWhenAppIsForeground_81() { return &___ShowWhenAppIsForeground_81; }
	inline void set_ShowWhenAppIsForeground_81(bool value)
	{
		___ShowWhenAppIsForeground_81 = value;
	}

	inline static int32_t get_offset_of_EnableVibrationLocal_82() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___EnableVibrationLocal_82)); }
	inline bool get_EnableVibrationLocal_82() const { return ___EnableVibrationLocal_82; }
	inline bool* get_address_of_EnableVibrationLocal_82() { return &___EnableVibrationLocal_82; }
	inline void set_EnableVibrationLocal_82(bool value)
	{
		___EnableVibrationLocal_82 = value;
	}

	inline static int32_t get_offset_of_LocalNotificationSmallIcon_83() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___LocalNotificationSmallIcon_83)); }
	inline Texture2D_t3542995729 * get_LocalNotificationSmallIcon_83() const { return ___LocalNotificationSmallIcon_83; }
	inline Texture2D_t3542995729 ** get_address_of_LocalNotificationSmallIcon_83() { return &___LocalNotificationSmallIcon_83; }
	inline void set_LocalNotificationSmallIcon_83(Texture2D_t3542995729 * value)
	{
		___LocalNotificationSmallIcon_83 = value;
		Il2CppCodeGenWriteBarrier(&___LocalNotificationSmallIcon_83, value);
	}

	inline static int32_t get_offset_of_LocalNotificationLargeIcon_84() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___LocalNotificationLargeIcon_84)); }
	inline Texture2D_t3542995729 * get_LocalNotificationLargeIcon_84() const { return ___LocalNotificationLargeIcon_84; }
	inline Texture2D_t3542995729 ** get_address_of_LocalNotificationLargeIcon_84() { return &___LocalNotificationLargeIcon_84; }
	inline void set_LocalNotificationLargeIcon_84(Texture2D_t3542995729 * value)
	{
		___LocalNotificationLargeIcon_84 = value;
		Il2CppCodeGenWriteBarrier(&___LocalNotificationLargeIcon_84, value);
	}

	inline static int32_t get_offset_of_LocalNotificationSound_85() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___LocalNotificationSound_85)); }
	inline AudioClip_t1932558630 * get_LocalNotificationSound_85() const { return ___LocalNotificationSound_85; }
	inline AudioClip_t1932558630 ** get_address_of_LocalNotificationSound_85() { return &___LocalNotificationSound_85; }
	inline void set_LocalNotificationSound_85(AudioClip_t1932558630 * value)
	{
		___LocalNotificationSound_85 = value;
		Il2CppCodeGenWriteBarrier(&___LocalNotificationSound_85, value);
	}

	inline static int32_t get_offset_of_ReplaceOldNotificationWithNew_86() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ReplaceOldNotificationWithNew_86)); }
	inline bool get_ReplaceOldNotificationWithNew_86() const { return ___ReplaceOldNotificationWithNew_86; }
	inline bool* get_address_of_ReplaceOldNotificationWithNew_86() { return &___ReplaceOldNotificationWithNew_86; }
	inline void set_ReplaceOldNotificationWithNew_86(bool value)
	{
		___ReplaceOldNotificationWithNew_86 = value;
	}

	inline static int32_t get_offset_of_ShowPushWhenAppIsForeground_87() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___ShowPushWhenAppIsForeground_87)); }
	inline bool get_ShowPushWhenAppIsForeground_87() const { return ___ShowPushWhenAppIsForeground_87; }
	inline bool* get_address_of_ShowPushWhenAppIsForeground_87() { return &___ShowPushWhenAppIsForeground_87; }
	inline void set_ShowPushWhenAppIsForeground_87(bool value)
	{
		___ShowPushWhenAppIsForeground_87 = value;
	}

	inline static int32_t get_offset_of_EnableVibrationPush_88() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___EnableVibrationPush_88)); }
	inline bool get_EnableVibrationPush_88() const { return ___EnableVibrationPush_88; }
	inline bool* get_address_of_EnableVibrationPush_88() { return &___EnableVibrationPush_88; }
	inline void set_EnableVibrationPush_88(bool value)
	{
		___EnableVibrationPush_88 = value;
	}

	inline static int32_t get_offset_of_PushNotificationSmallIcon_89() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___PushNotificationSmallIcon_89)); }
	inline Texture2D_t3542995729 * get_PushNotificationSmallIcon_89() const { return ___PushNotificationSmallIcon_89; }
	inline Texture2D_t3542995729 ** get_address_of_PushNotificationSmallIcon_89() { return &___PushNotificationSmallIcon_89; }
	inline void set_PushNotificationSmallIcon_89(Texture2D_t3542995729 * value)
	{
		___PushNotificationSmallIcon_89 = value;
		Il2CppCodeGenWriteBarrier(&___PushNotificationSmallIcon_89, value);
	}

	inline static int32_t get_offset_of_PushNotificationLargeIcon_90() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___PushNotificationLargeIcon_90)); }
	inline Texture2D_t3542995729 * get_PushNotificationLargeIcon_90() const { return ___PushNotificationLargeIcon_90; }
	inline Texture2D_t3542995729 ** get_address_of_PushNotificationLargeIcon_90() { return &___PushNotificationLargeIcon_90; }
	inline void set_PushNotificationLargeIcon_90(Texture2D_t3542995729 * value)
	{
		___PushNotificationLargeIcon_90 = value;
		Il2CppCodeGenWriteBarrier(&___PushNotificationLargeIcon_90, value);
	}

	inline static int32_t get_offset_of_PushNotificationSound_91() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021, ___PushNotificationSound_91)); }
	inline AudioClip_t1932558630 * get_PushNotificationSound_91() const { return ___PushNotificationSound_91; }
	inline AudioClip_t1932558630 ** get_address_of_PushNotificationSound_91() { return &___PushNotificationSound_91; }
	inline void set_PushNotificationSound_91(AudioClip_t1932558630 * value)
	{
		___PushNotificationSound_91 = value;
		Il2CppCodeGenWriteBarrier(&___PushNotificationSound_91, value);
	}
};

struct AndroidNativeSettings_t3397157021_StaticFields
{
public:
	// AndroidNativeSettings AndroidNativeSettings::instance
	AndroidNativeSettings_t3397157021 * ___instance_92;

public:
	inline static int32_t get_offset_of_instance_92() { return static_cast<int32_t>(offsetof(AndroidNativeSettings_t3397157021_StaticFields, ___instance_92)); }
	inline AndroidNativeSettings_t3397157021 * get_instance_92() const { return ___instance_92; }
	inline AndroidNativeSettings_t3397157021 ** get_address_of_instance_92() { return &___instance_92; }
	inline void set_instance_92(AndroidNativeSettings_t3397157021 * value)
	{
		___instance_92 = value;
		Il2CppCodeGenWriteBarrier(&___instance_92, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
