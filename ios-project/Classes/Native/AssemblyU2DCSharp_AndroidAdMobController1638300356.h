﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Int32,AndroidADBanner>
struct Dictionary_2_t4189012375;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen2929144274.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidAdMobController
struct  AndroidAdMobController_t1638300356  : public SA_Singleton_OLD_1_t2929144274
{
public:
	// System.Boolean AndroidAdMobController::_IsInited
	bool ____IsInited_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,AndroidADBanner> AndroidAdMobController::_banners
	Dictionary_2_t4189012375 * ____banners_6;
	// System.String AndroidAdMobController::_BannersUunitId
	String_t* ____BannersUunitId_7;
	// System.String AndroidAdMobController::_InterstisialUnitId
	String_t* ____InterstisialUnitId_8;
	// System.Action AndroidAdMobController::_OnInterstitialLoaded
	Action_t3226471752 * ____OnInterstitialLoaded_9;
	// System.Action AndroidAdMobController::_OnInterstitialFailedLoading
	Action_t3226471752 * ____OnInterstitialFailedLoading_10;
	// System.Action AndroidAdMobController::_OnInterstitialOpened
	Action_t3226471752 * ____OnInterstitialOpened_11;
	// System.Action AndroidAdMobController::_OnInterstitialClosed
	Action_t3226471752 * ____OnInterstitialClosed_12;
	// System.Action AndroidAdMobController::_OnInterstitialLeftApplication
	Action_t3226471752 * ____OnInterstitialLeftApplication_13;
	// System.Action`1<System.String> AndroidAdMobController::_OnAdInAppRequest
	Action_1_t1831019615 * ____OnAdInAppRequest_14;

public:
	inline static int32_t get_offset_of__IsInited_5() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356, ____IsInited_5)); }
	inline bool get__IsInited_5() const { return ____IsInited_5; }
	inline bool* get_address_of__IsInited_5() { return &____IsInited_5; }
	inline void set__IsInited_5(bool value)
	{
		____IsInited_5 = value;
	}

	inline static int32_t get_offset_of__banners_6() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356, ____banners_6)); }
	inline Dictionary_2_t4189012375 * get__banners_6() const { return ____banners_6; }
	inline Dictionary_2_t4189012375 ** get_address_of__banners_6() { return &____banners_6; }
	inline void set__banners_6(Dictionary_2_t4189012375 * value)
	{
		____banners_6 = value;
		Il2CppCodeGenWriteBarrier(&____banners_6, value);
	}

	inline static int32_t get_offset_of__BannersUunitId_7() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356, ____BannersUunitId_7)); }
	inline String_t* get__BannersUunitId_7() const { return ____BannersUunitId_7; }
	inline String_t** get_address_of__BannersUunitId_7() { return &____BannersUunitId_7; }
	inline void set__BannersUunitId_7(String_t* value)
	{
		____BannersUunitId_7 = value;
		Il2CppCodeGenWriteBarrier(&____BannersUunitId_7, value);
	}

	inline static int32_t get_offset_of__InterstisialUnitId_8() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356, ____InterstisialUnitId_8)); }
	inline String_t* get__InterstisialUnitId_8() const { return ____InterstisialUnitId_8; }
	inline String_t** get_address_of__InterstisialUnitId_8() { return &____InterstisialUnitId_8; }
	inline void set__InterstisialUnitId_8(String_t* value)
	{
		____InterstisialUnitId_8 = value;
		Il2CppCodeGenWriteBarrier(&____InterstisialUnitId_8, value);
	}

	inline static int32_t get_offset_of__OnInterstitialLoaded_9() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356, ____OnInterstitialLoaded_9)); }
	inline Action_t3226471752 * get__OnInterstitialLoaded_9() const { return ____OnInterstitialLoaded_9; }
	inline Action_t3226471752 ** get_address_of__OnInterstitialLoaded_9() { return &____OnInterstitialLoaded_9; }
	inline void set__OnInterstitialLoaded_9(Action_t3226471752 * value)
	{
		____OnInterstitialLoaded_9 = value;
		Il2CppCodeGenWriteBarrier(&____OnInterstitialLoaded_9, value);
	}

	inline static int32_t get_offset_of__OnInterstitialFailedLoading_10() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356, ____OnInterstitialFailedLoading_10)); }
	inline Action_t3226471752 * get__OnInterstitialFailedLoading_10() const { return ____OnInterstitialFailedLoading_10; }
	inline Action_t3226471752 ** get_address_of__OnInterstitialFailedLoading_10() { return &____OnInterstitialFailedLoading_10; }
	inline void set__OnInterstitialFailedLoading_10(Action_t3226471752 * value)
	{
		____OnInterstitialFailedLoading_10 = value;
		Il2CppCodeGenWriteBarrier(&____OnInterstitialFailedLoading_10, value);
	}

	inline static int32_t get_offset_of__OnInterstitialOpened_11() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356, ____OnInterstitialOpened_11)); }
	inline Action_t3226471752 * get__OnInterstitialOpened_11() const { return ____OnInterstitialOpened_11; }
	inline Action_t3226471752 ** get_address_of__OnInterstitialOpened_11() { return &____OnInterstitialOpened_11; }
	inline void set__OnInterstitialOpened_11(Action_t3226471752 * value)
	{
		____OnInterstitialOpened_11 = value;
		Il2CppCodeGenWriteBarrier(&____OnInterstitialOpened_11, value);
	}

	inline static int32_t get_offset_of__OnInterstitialClosed_12() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356, ____OnInterstitialClosed_12)); }
	inline Action_t3226471752 * get__OnInterstitialClosed_12() const { return ____OnInterstitialClosed_12; }
	inline Action_t3226471752 ** get_address_of__OnInterstitialClosed_12() { return &____OnInterstitialClosed_12; }
	inline void set__OnInterstitialClosed_12(Action_t3226471752 * value)
	{
		____OnInterstitialClosed_12 = value;
		Il2CppCodeGenWriteBarrier(&____OnInterstitialClosed_12, value);
	}

	inline static int32_t get_offset_of__OnInterstitialLeftApplication_13() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356, ____OnInterstitialLeftApplication_13)); }
	inline Action_t3226471752 * get__OnInterstitialLeftApplication_13() const { return ____OnInterstitialLeftApplication_13; }
	inline Action_t3226471752 ** get_address_of__OnInterstitialLeftApplication_13() { return &____OnInterstitialLeftApplication_13; }
	inline void set__OnInterstitialLeftApplication_13(Action_t3226471752 * value)
	{
		____OnInterstitialLeftApplication_13 = value;
		Il2CppCodeGenWriteBarrier(&____OnInterstitialLeftApplication_13, value);
	}

	inline static int32_t get_offset_of__OnAdInAppRequest_14() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356, ____OnAdInAppRequest_14)); }
	inline Action_1_t1831019615 * get__OnAdInAppRequest_14() const { return ____OnAdInAppRequest_14; }
	inline Action_1_t1831019615 ** get_address_of__OnAdInAppRequest_14() { return &____OnAdInAppRequest_14; }
	inline void set__OnAdInAppRequest_14(Action_1_t1831019615 * value)
	{
		____OnAdInAppRequest_14 = value;
		Il2CppCodeGenWriteBarrier(&____OnAdInAppRequest_14, value);
	}
};

struct AndroidAdMobController_t1638300356_StaticFields
{
public:
	// System.Action AndroidAdMobController::<>f__am$cacheA
	Action_t3226471752 * ___U3CU3Ef__amU24cacheA_15;
	// System.Action AndroidAdMobController::<>f__am$cacheB
	Action_t3226471752 * ___U3CU3Ef__amU24cacheB_16;
	// System.Action AndroidAdMobController::<>f__am$cacheC
	Action_t3226471752 * ___U3CU3Ef__amU24cacheC_17;
	// System.Action AndroidAdMobController::<>f__am$cacheD
	Action_t3226471752 * ___U3CU3Ef__amU24cacheD_18;
	// System.Action AndroidAdMobController::<>f__am$cacheE
	Action_t3226471752 * ___U3CU3Ef__amU24cacheE_19;
	// System.Action`1<System.String> AndroidAdMobController::<>f__am$cacheF
	Action_1_t1831019615 * ___U3CU3Ef__amU24cacheF_20;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_15() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356_StaticFields, ___U3CU3Ef__amU24cacheA_15)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cacheA_15() const { return ___U3CU3Ef__amU24cacheA_15; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cacheA_15() { return &___U3CU3Ef__amU24cacheA_15; }
	inline void set_U3CU3Ef__amU24cacheA_15(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cacheA_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_16() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356_StaticFields, ___U3CU3Ef__amU24cacheB_16)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cacheB_16() const { return ___U3CU3Ef__amU24cacheB_16; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cacheB_16() { return &___U3CU3Ef__amU24cacheB_16; }
	inline void set_U3CU3Ef__amU24cacheB_16(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cacheB_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_17() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356_StaticFields, ___U3CU3Ef__amU24cacheC_17)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cacheC_17() const { return ___U3CU3Ef__amU24cacheC_17; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cacheC_17() { return &___U3CU3Ef__amU24cacheC_17; }
	inline void set_U3CU3Ef__amU24cacheC_17(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cacheC_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_18() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356_StaticFields, ___U3CU3Ef__amU24cacheD_18)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cacheD_18() const { return ___U3CU3Ef__amU24cacheD_18; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cacheD_18() { return &___U3CU3Ef__amU24cacheD_18; }
	inline void set_U3CU3Ef__amU24cacheD_18(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cacheD_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_19() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356_StaticFields, ___U3CU3Ef__amU24cacheE_19)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cacheE_19() const { return ___U3CU3Ef__amU24cacheE_19; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cacheE_19() { return &___U3CU3Ef__amU24cacheE_19; }
	inline void set_U3CU3Ef__amU24cacheE_19(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cacheE_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_20() { return static_cast<int32_t>(offsetof(AndroidAdMobController_t1638300356_StaticFields, ___U3CU3Ef__amU24cacheF_20)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cacheF_20() const { return ___U3CU3Ef__amU24cacheF_20; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cacheF_20() { return &___U3CU3Ef__amU24cacheF_20; }
	inline void set_U3CU3Ef__amU24cacheF_20(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cacheF_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
