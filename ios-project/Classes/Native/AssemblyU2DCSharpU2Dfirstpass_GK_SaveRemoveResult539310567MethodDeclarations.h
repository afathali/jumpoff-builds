﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_SaveRemoveResult
struct GK_SaveRemoveResult_t539310567;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GK_SaveRemoveResult::.ctor(System.String)
extern "C"  void GK_SaveRemoveResult__ctor_m3709716178 (GK_SaveRemoveResult_t539310567 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_SaveRemoveResult::.ctor(System.String,System.String)
extern "C"  void GK_SaveRemoveResult__ctor_m3513187338 (GK_SaveRemoveResult_t539310567 * __this, String_t* ___name0, String_t* ___errorData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_SaveRemoveResult::get_SaveName()
extern "C"  String_t* GK_SaveRemoveResult_get_SaveName_m3063797326 (GK_SaveRemoveResult_t539310567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
