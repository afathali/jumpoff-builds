﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;

#include "AssemblyU2DCSharp_MCGBehaviour3307770884.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Details_Controller
struct  Details_Controller_t3848679003  : public MCGBehaviour_t3307770884
{
public:
	// UnityEngine.UI.Text Details_Controller::txt_name
	Text_t356221433 * ___txt_name_4;
	// UnityEngine.UI.Text Details_Controller::txt_location
	Text_t356221433 * ___txt_location_5;
	// UnityEngine.UI.Text Details_Controller::txt_date
	Text_t356221433 * ___txt_date_6;
	// UnityEngine.UI.Text Details_Controller::txt_notes
	Text_t356221433 * ___txt_notes_7;
	// UnityEngine.UI.Button Details_Controller::btn_done
	Button_t2872111280 * ___btn_done_8;

public:
	inline static int32_t get_offset_of_txt_name_4() { return static_cast<int32_t>(offsetof(Details_Controller_t3848679003, ___txt_name_4)); }
	inline Text_t356221433 * get_txt_name_4() const { return ___txt_name_4; }
	inline Text_t356221433 ** get_address_of_txt_name_4() { return &___txt_name_4; }
	inline void set_txt_name_4(Text_t356221433 * value)
	{
		___txt_name_4 = value;
		Il2CppCodeGenWriteBarrier(&___txt_name_4, value);
	}

	inline static int32_t get_offset_of_txt_location_5() { return static_cast<int32_t>(offsetof(Details_Controller_t3848679003, ___txt_location_5)); }
	inline Text_t356221433 * get_txt_location_5() const { return ___txt_location_5; }
	inline Text_t356221433 ** get_address_of_txt_location_5() { return &___txt_location_5; }
	inline void set_txt_location_5(Text_t356221433 * value)
	{
		___txt_location_5 = value;
		Il2CppCodeGenWriteBarrier(&___txt_location_5, value);
	}

	inline static int32_t get_offset_of_txt_date_6() { return static_cast<int32_t>(offsetof(Details_Controller_t3848679003, ___txt_date_6)); }
	inline Text_t356221433 * get_txt_date_6() const { return ___txt_date_6; }
	inline Text_t356221433 ** get_address_of_txt_date_6() { return &___txt_date_6; }
	inline void set_txt_date_6(Text_t356221433 * value)
	{
		___txt_date_6 = value;
		Il2CppCodeGenWriteBarrier(&___txt_date_6, value);
	}

	inline static int32_t get_offset_of_txt_notes_7() { return static_cast<int32_t>(offsetof(Details_Controller_t3848679003, ___txt_notes_7)); }
	inline Text_t356221433 * get_txt_notes_7() const { return ___txt_notes_7; }
	inline Text_t356221433 ** get_address_of_txt_notes_7() { return &___txt_notes_7; }
	inline void set_txt_notes_7(Text_t356221433 * value)
	{
		___txt_notes_7 = value;
		Il2CppCodeGenWriteBarrier(&___txt_notes_7, value);
	}

	inline static int32_t get_offset_of_btn_done_8() { return static_cast<int32_t>(offsetof(Details_Controller_t3848679003, ___btn_done_8)); }
	inline Button_t2872111280 * get_btn_done_8() const { return ___btn_done_8; }
	inline Button_t2872111280 ** get_address_of_btn_done_8() { return &___btn_done_8; }
	inline void set_btn_done_8(Button_t2872111280 * value)
	{
		___btn_done_8 = value;
		Il2CppCodeGenWriteBarrier(&___btn_done_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
