﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidGoogleAdsExample_old
struct AndroidGoogleAdsExample_old_t3329868158;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidGoogleAdsExample_old::.ctor()
extern "C"  void AndroidGoogleAdsExample_old__ctor_m4205037591 (AndroidGoogleAdsExample_old_t3329868158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample_old::Start()
extern "C"  void AndroidGoogleAdsExample_old_Start_m3397282435 (AndroidGoogleAdsExample_old_t3329868158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample_old::InitStyles()
extern "C"  void AndroidGoogleAdsExample_old_InitStyles_m113235569 (AndroidGoogleAdsExample_old_t3329868158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample_old::OnGUI()
extern "C"  void AndroidGoogleAdsExample_old_OnGUI_m3217578061 (AndroidGoogleAdsExample_old_t3329868158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample_old::OnInterstisialsLoaded()
extern "C"  void AndroidGoogleAdsExample_old_OnInterstisialsLoaded_m917136619 (AndroidGoogleAdsExample_old_t3329868158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidGoogleAdsExample_old::OnInterstisialsOpen()
extern "C"  void AndroidGoogleAdsExample_old_OnInterstisialsOpen_m1169628408 (AndroidGoogleAdsExample_old_t3329868158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
