﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct DefaultComparer_t458978466;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor()
extern "C"  void DefaultComparer__ctor_m2620080077_gshared (DefaultComparer_t458978466 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2620080077(__this, method) ((  void (*) (DefaultComparer_t458978466 *, const MethodInfo*))DefaultComparer__ctor_m2620080077_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1738886722_gshared (DefaultComparer_t458978466 * __this, KeyValuePair_2_t3132015601  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1738886722(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t458978466 *, KeyValuePair_2_t3132015601 , const MethodInfo*))DefaultComparer_GetHashCode_m1738886722_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2605760226_gshared (DefaultComparer_t458978466 * __this, KeyValuePair_2_t3132015601  ___x0, KeyValuePair_2_t3132015601  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2605760226(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t458978466 *, KeyValuePair_2_t3132015601 , KeyValuePair_2_t3132015601 , const MethodInfo*))DefaultComparer_Equals_m2605760226_gshared)(__this, ___x0, ___y1, method)
