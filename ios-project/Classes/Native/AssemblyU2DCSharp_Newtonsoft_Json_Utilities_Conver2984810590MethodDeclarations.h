﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.Type
struct Type_t;
// System.ComponentModel.TypeConverter
struct TypeConverter_t745995970;
// System.Object
struct Il2CppObject;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Conver1788482786.h"
#include "mscorlib_System_Type1303803226.h"
#include "System_System_ComponentModel_TypeConverter745995970.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"

// System.Void Newtonsoft.Json.Utilities.ConvertUtils::.cctor()
extern "C"  void ConvertUtils__cctor_m3194542631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ConvertUtils::CreateCastConverter(Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey)
extern "C"  Func_2_t2825504181 * ConvertUtils_CreateCastConverter_m2721240291 (Il2CppObject * __this /* static, unused */, TypeConvertKey_t1788482786  ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::CanConvertType(System.Type,System.Type,System.Boolean)
extern "C"  bool ConvertUtils_CanConvertType_m3565196946 (Il2CppObject * __this /* static, unused */, Type_t * ___initialType0, Type_t * ___targetType1, bool ___allowTypeNameToString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::IsComponentConverter(System.ComponentModel.TypeConverter)
extern "C"  bool ConvertUtils_IsComponentConverter_m1256161438 (Il2CppObject * __this /* static, unused */, TypeConverter_t745995970 * ___converter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils::Convert(System.Object,System.Globalization.CultureInfo,System.Type)
extern "C"  Il2CppObject * ConvertUtils_Convert_m2323116825 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___initialValue0, CultureInfo_t3500843524 * ___culture1, Type_t * ___targetType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryConvert(System.Object,System.Globalization.CultureInfo,System.Type,System.Object&)
extern "C"  bool ConvertUtils_TryConvert_m3341520891 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___initialValue0, CultureInfo_t3500843524 * ___culture1, Type_t * ___targetType2, Il2CppObject ** ___convertedValue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils::ConvertOrCast(System.Object,System.Globalization.CultureInfo,System.Type)
extern "C"  Il2CppObject * ConvertUtils_ConvertOrCast_m357620265 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___initialValue0, CultureInfo_t3500843524 * ___culture1, Type_t * ___targetType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryConvertOrCast(System.Object,System.Globalization.CultureInfo,System.Type,System.Object&)
extern "C"  bool ConvertUtils_TryConvertOrCast_m3940889671 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___initialValue0, CultureInfo_t3500843524 * ___culture1, Type_t * ___targetType2, Il2CppObject ** ___convertedValue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils::EnsureTypeAssignable(System.Object,System.Type,System.Type)
extern "C"  Il2CppObject * ConvertUtils_EnsureTypeAssignable_m831819232 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___initialType1, Type_t * ___targetType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.TypeConverter Newtonsoft.Json.Utilities.ConvertUtils::GetConverter(System.Type)
extern "C"  TypeConverter_t745995970 * ConvertUtils_GetConverter_m3025853363 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::IsInteger(System.Object)
extern "C"  bool ConvertUtils_IsInteger_m1115753230 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
