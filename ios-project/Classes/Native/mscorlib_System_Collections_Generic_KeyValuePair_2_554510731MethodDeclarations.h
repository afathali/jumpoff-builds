﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.JsonProperty,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1098170283(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t554510731 *, JsonProperty_t2712067825 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.JsonProperty,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m1008503074(__this, method) ((  JsonProperty_t2712067825 * (*) (KeyValuePair_2_t554510731 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.JsonProperty,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2877390630(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t554510731 *, JsonProperty_t2712067825 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.JsonProperty,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m566432757(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t554510731 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.JsonProperty,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m422657110(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t554510731 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.JsonProperty,System.Object>::ToString()
#define KeyValuePair_2_ToString_m3480171596(__this, method) ((  String_t* (*) (KeyValuePair_2_t554510731 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
