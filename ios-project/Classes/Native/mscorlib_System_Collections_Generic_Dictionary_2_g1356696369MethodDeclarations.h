﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>
struct Dictionary_2_t1356696369;
// System.Collections.Generic.IEqualityComparer`1<AN_ManifestPermission>
struct IEqualityComparer_1_t984742980;
// System.Collections.Generic.IDictionary`2<AN_ManifestPermission,AN_PermissionState>
struct IDictionary_2_t3650747086;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<AN_ManifestPermission>
struct ICollection_1_t2724185507;
// System.Collections.Generic.ICollection`1<AN_PermissionState>
struct ICollection_1_t1790276529;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>[]
struct KeyValuePair_2U5BU5D_t4072237390;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>
struct IEnumerator_1_t884532714;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<AN_ManifestPermission,AN_PermissionState>
struct KeyCollection_t3840194140;
// System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>
struct ValueCollection_t59756212;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23409008887.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_AN_PermissionState838201224.h"
#include "AssemblyU2DCSharp_AN_ManifestPermission1772110202.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2676721071.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::.ctor()
extern "C"  void Dictionary_2__ctor_m181389994_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m181389994(__this, method) ((  void (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2__ctor_m181389994_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3521078822_gshared (Dictionary_2_t1356696369 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3521078822(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1356696369 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3521078822_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m2378674661_gshared (Dictionary_2_t1356696369 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m2378674661(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t1356696369 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2378674661_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2624667818_gshared (Dictionary_2_t1356696369 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2624667818(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1356696369 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2624667818_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m4064756762_gshared (Dictionary_2_t1356696369 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m4064756762(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t1356696369 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m4064756762_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2678137237_gshared (Dictionary_2_t1356696369 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2678137237(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t1356696369 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2678137237_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m333996560_gshared (Dictionary_2_t1356696369 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m333996560(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1356696369 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m333996560_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m638735679_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m638735679(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m638735679_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m278328543_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m278328543(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m278328543_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m4023886413_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m4023886413(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4023886413_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2591527805_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2591527805(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m2591527805_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2822693914_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2822693914(__this, method) ((  bool (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2822693914_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2963324345_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2963324345(__this, method) ((  bool (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2963324345_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2774806979_gshared (Dictionary_2_t1356696369 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2774806979(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1356696369 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2774806979_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m35601732_gshared (Dictionary_2_t1356696369 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m35601732(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1356696369 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m35601732_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2422505101_gshared (Dictionary_2_t1356696369 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2422505101(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1356696369 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2422505101_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2554864617_gshared (Dictionary_2_t1356696369 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2554864617(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1356696369 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2554864617_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1358252776_gshared (Dictionary_2_t1356696369 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1358252776(__this, ___key0, method) ((  void (*) (Dictionary_2_t1356696369 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1358252776_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1681970835_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1681970835(__this, method) ((  bool (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1681970835_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1590456023_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1590456023(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1590456023_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3830123037_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3830123037(__this, method) ((  bool (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3830123037_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3026113266_gshared (Dictionary_2_t1356696369 * __this, KeyValuePair_2_t3409008887  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3026113266(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1356696369 *, KeyValuePair_2_t3409008887 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3026113266_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2177150026_gshared (Dictionary_2_t1356696369 * __this, KeyValuePair_2_t3409008887  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2177150026(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1356696369 *, KeyValuePair_2_t3409008887 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2177150026_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2374969134_gshared (Dictionary_2_t1356696369 * __this, KeyValuePair_2U5BU5D_t4072237390* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2374969134(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1356696369 *, KeyValuePair_2U5BU5D_t4072237390*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2374969134_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m893379879_gshared (Dictionary_2_t1356696369 * __this, KeyValuePair_2_t3409008887  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m893379879(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1356696369 *, KeyValuePair_2_t3409008887 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m893379879_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2885577275_gshared (Dictionary_2_t1356696369 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2885577275(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1356696369 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2885577275_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2099131524_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2099131524(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2099131524_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m300545893_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m300545893(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m300545893_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4063976050_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4063976050(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4063976050_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m364781995_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m364781995(__this, method) ((  int32_t (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_get_Count_m364781995_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m3390936934_gshared (Dictionary_2_t1356696369 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3390936934(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1356696369 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m3390936934_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1440704639_gshared (Dictionary_2_t1356696369 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1440704639(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1356696369 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m1440704639_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m891894455_gshared (Dictionary_2_t1356696369 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m891894455(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1356696369 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m891894455_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2925015870_gshared (Dictionary_2_t1356696369 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2925015870(__this, ___size0, method) ((  void (*) (Dictionary_2_t1356696369 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2925015870_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m678177520_gshared (Dictionary_2_t1356696369 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m678177520(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1356696369 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m678177520_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3409008887  Dictionary_2_make_pair_m1953007134_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1953007134(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3409008887  (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m1953007134_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m804828576_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m804828576(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m804828576_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m1169422368_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1169422368(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m1169422368_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m402979891_gshared (Dictionary_2_t1356696369 * __this, KeyValuePair_2U5BU5D_t4072237390* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m402979891(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1356696369 *, KeyValuePair_2U5BU5D_t4072237390*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m402979891_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::Resize()
extern "C"  void Dictionary_2_Resize_m3335129725_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3335129725(__this, method) ((  void (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_Resize_m3335129725_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1523741106_gshared (Dictionary_2_t1356696369 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1523741106(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1356696369 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m1523741106_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::Clear()
extern "C"  void Dictionary_2_Clear_m1160315342_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1160315342(__this, method) ((  void (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_Clear_m1160315342_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2935862406_gshared (Dictionary_2_t1356696369 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2935862406(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1356696369 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2935862406_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m493189806_gshared (Dictionary_2_t1356696369 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m493189806(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1356696369 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m493189806_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m408057471_gshared (Dictionary_2_t1356696369 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m408057471(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1356696369 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m408057471_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m310029671_gshared (Dictionary_2_t1356696369 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m310029671(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1356696369 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m310029671_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m740776026_gshared (Dictionary_2_t1356696369 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m740776026(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1356696369 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m740776026_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1806459663_gshared (Dictionary_2_t1356696369 * __this, int32_t ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1806459663(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1356696369 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m1806459663_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::get_Keys()
extern "C"  KeyCollection_t3840194140 * Dictionary_2_get_Keys_m4216590278_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m4216590278(__this, method) ((  KeyCollection_t3840194140 * (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_get_Keys_m4216590278_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::get_Values()
extern "C"  ValueCollection_t59756212 * Dictionary_2_get_Values_m869108582_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m869108582(__this, method) ((  ValueCollection_t59756212 * (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_get_Values_m869108582_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m4105932485_gshared (Dictionary_2_t1356696369 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m4105932485(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1356696369 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4105932485_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m232114021_gshared (Dictionary_2_t1356696369 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m232114021(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t1356696369 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m232114021_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m534201503_gshared (Dictionary_2_t1356696369 * __this, KeyValuePair_2_t3409008887  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m534201503(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1356696369 *, KeyValuePair_2_t3409008887 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m534201503_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::GetEnumerator()
extern "C"  Enumerator_t2676721071  Dictionary_2_GetEnumerator_m276780888_gshared (Dictionary_2_t1356696369 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m276780888(__this, method) ((  Enumerator_t2676721071  (*) (Dictionary_2_t1356696369 *, const MethodInfo*))Dictionary_2_GetEnumerator_m276780888_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m2802054557_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m2802054557(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2802054557_gshared)(__this /* static, unused */, ___key0, ___value1, method)
