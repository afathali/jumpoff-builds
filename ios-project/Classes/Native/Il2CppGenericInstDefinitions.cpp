﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType Attribute_t542643598_0_0_0;
static const Il2CppType* GenInst_Attribute_t542643598_0_0_0_Types[] = { &Attribute_t542643598_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t542643598_0_0_0 = { 1, GenInst_Attribute_t542643598_0_0_0_Types };
extern const Il2CppType _Attribute_t1557664299_0_0_0;
static const Il2CppType* GenInst__Attribute_t1557664299_0_0_0_Types[] = { &_Attribute_t1557664299_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t1557664299_0_0_0 = { 1, GenInst__Attribute_t1557664299_0_0_0_Types };
extern const Il2CppType Int32_t2071877448_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0 = { 1, GenInst_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Char_t3454481338_0_0_0;
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Types[] = { &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0 = { 1, GenInst_Char_t3454481338_0_0_0_Types };
extern const Il2CppType IConvertible_t908092482_0_0_0;
static const Il2CppType* GenInst_IConvertible_t908092482_0_0_0_Types[] = { &IConvertible_t908092482_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t908092482_0_0_0 = { 1, GenInst_IConvertible_t908092482_0_0_0_Types };
extern const Il2CppType IComparable_t1857082765_0_0_0;
static const Il2CppType* GenInst_IComparable_t1857082765_0_0_0_Types[] = { &IComparable_t1857082765_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1857082765_0_0_0 = { 1, GenInst_IComparable_t1857082765_0_0_0_Types };
extern const Il2CppType IComparable_1_t991353265_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t991353265_0_0_0_Types[] = { &IComparable_1_t991353265_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t991353265_0_0_0 = { 1, GenInst_IComparable_1_t991353265_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1363496211_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1363496211_0_0_0_Types[] = { &IEquatable_1_t1363496211_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1363496211_0_0_0 = { 1, GenInst_IEquatable_1_t1363496211_0_0_0_Types };
extern const Il2CppType ValueType_t3507792607_0_0_0;
static const Il2CppType* GenInst_ValueType_t3507792607_0_0_0_Types[] = { &ValueType_t3507792607_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t3507792607_0_0_0 = { 1, GenInst_ValueType_t3507792607_0_0_0_Types };
extern const Il2CppType Int64_t909078037_0_0_0;
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0 = { 1, GenInst_Int64_t909078037_0_0_0_Types };
extern const Il2CppType UInt32_t2149682021_0_0_0;
static const Il2CppType* GenInst_UInt32_t2149682021_0_0_0_Types[] = { &UInt32_t2149682021_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0 = { 1, GenInst_UInt32_t2149682021_0_0_0_Types };
extern const Il2CppType UInt64_t2909196914_0_0_0;
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0 = { 1, GenInst_UInt64_t2909196914_0_0_0_Types };
extern const Il2CppType Byte_t3683104436_0_0_0;
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Types[] = { &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0 = { 1, GenInst_Byte_t3683104436_0_0_0_Types };
extern const Il2CppType SByte_t454417549_0_0_0;
static const Il2CppType* GenInst_SByte_t454417549_0_0_0_Types[] = { &SByte_t454417549_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t454417549_0_0_0 = { 1, GenInst_SByte_t454417549_0_0_0_Types };
extern const Il2CppType Int16_t4041245914_0_0_0;
static const Il2CppType* GenInst_Int16_t4041245914_0_0_0_Types[] = { &Int16_t4041245914_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0 = { 1, GenInst_Int16_t4041245914_0_0_0_Types };
extern const Il2CppType UInt16_t986882611_0_0_0;
static const Il2CppType* GenInst_UInt16_t986882611_0_0_0_Types[] = { &UInt16_t986882611_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t986882611_0_0_0 = { 1, GenInst_UInt16_t986882611_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType IEnumerable_t2911409499_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t2911409499_0_0_0_Types[] = { &IEnumerable_t2911409499_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t2911409499_0_0_0 = { 1, GenInst_IEnumerable_t2911409499_0_0_0_Types };
extern const Il2CppType ICloneable_t3853279282_0_0_0;
static const Il2CppType* GenInst_ICloneable_t3853279282_0_0_0_Types[] = { &ICloneable_t3853279282_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t3853279282_0_0_0 = { 1, GenInst_ICloneable_t3853279282_0_0_0_Types };
extern const Il2CppType IComparable_1_t3861059456_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3861059456_0_0_0_Types[] = { &IComparable_1_t3861059456_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3861059456_0_0_0 = { 1, GenInst_IComparable_1_t3861059456_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4233202402_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4233202402_0_0_0_Types[] = { &IEquatable_1_t4233202402_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4233202402_0_0_0 = { 1, GenInst_IEquatable_1_t4233202402_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IReflect_t3412036974_0_0_0;
static const Il2CppType* GenInst_IReflect_t3412036974_0_0_0_Types[] = { &IReflect_t3412036974_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t3412036974_0_0_0 = { 1, GenInst_IReflect_t3412036974_0_0_0_Types };
extern const Il2CppType _Type_t102776839_0_0_0;
static const Il2CppType* GenInst__Type_t102776839_0_0_0_Types[] = { &_Type_t102776839_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t102776839_0_0_0 = { 1, GenInst__Type_t102776839_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t502202687_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types };
extern const Il2CppType _MemberInfo_t332722161_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t332722161_0_0_0_Types[] = { &_MemberInfo_t332722161_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t332722161_0_0_0 = { 1, GenInst__MemberInfo_t332722161_0_0_0_Types };
extern const Il2CppType IFormattable_t1523031934_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1523031934_0_0_0_Types[] = { &IFormattable_t1523031934_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1523031934_0_0_0 = { 1, GenInst_IFormattable_t1523031934_0_0_0_Types };
extern const Il2CppType IComparable_1_t3903716671_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3903716671_0_0_0_Types[] = { &IComparable_1_t3903716671_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3903716671_0_0_0 = { 1, GenInst_IComparable_1_t3903716671_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4275859617_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4275859617_0_0_0_Types[] = { &IEquatable_1_t4275859617_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4275859617_0_0_0 = { 1, GenInst_IEquatable_1_t4275859617_0_0_0_Types };
extern const Il2CppType Double_t4078015681_0_0_0;
static const Il2CppType* GenInst_Double_t4078015681_0_0_0_Types[] = { &Double_t4078015681_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0 = { 1, GenInst_Double_t4078015681_0_0_0_Types };
extern const Il2CppType IComparable_1_t1614887608_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1614887608_0_0_0_Types[] = { &IComparable_1_t1614887608_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1614887608_0_0_0 = { 1, GenInst_IComparable_1_t1614887608_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1987030554_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1987030554_0_0_0_Types[] = { &IEquatable_1_t1987030554_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1987030554_0_0_0 = { 1, GenInst_IEquatable_1_t1987030554_0_0_0_Types };
extern const Il2CppType IComparable_1_t3981521244_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3981521244_0_0_0_Types[] = { &IComparable_1_t3981521244_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3981521244_0_0_0 = { 1, GenInst_IComparable_1_t3981521244_0_0_0_Types };
extern const Il2CppType IEquatable_1_t58696894_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t58696894_0_0_0_Types[] = { &IEquatable_1_t58696894_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t58696894_0_0_0 = { 1, GenInst_IEquatable_1_t58696894_0_0_0_Types };
extern const Il2CppType IComparable_1_t1219976363_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1219976363_0_0_0_Types[] = { &IComparable_1_t1219976363_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1219976363_0_0_0 = { 1, GenInst_IComparable_1_t1219976363_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1592119309_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1592119309_0_0_0_Types[] = { &IEquatable_1_t1592119309_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1592119309_0_0_0 = { 1, GenInst_IEquatable_1_t1592119309_0_0_0_Types };
extern const Il2CppType Single_t2076509932_0_0_0;
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Types[] = { &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0 = { 1, GenInst_Single_t2076509932_0_0_0_Types };
extern const Il2CppType IComparable_1_t3908349155_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3908349155_0_0_0_Types[] = { &IComparable_1_t3908349155_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3908349155_0_0_0 = { 1, GenInst_IComparable_1_t3908349155_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4280492101_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4280492101_0_0_0_Types[] = { &IEquatable_1_t4280492101_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4280492101_0_0_0 = { 1, GenInst_IEquatable_1_t4280492101_0_0_0_Types };
extern const Il2CppType Decimal_t724701077_0_0_0;
static const Il2CppType* GenInst_Decimal_t724701077_0_0_0_Types[] = { &Decimal_t724701077_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0 = { 1, GenInst_Decimal_t724701077_0_0_0_Types };
extern const Il2CppType Boolean_t3825574718_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0 = { 1, GenInst_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Delegate_t3022476291_0_0_0;
static const Il2CppType* GenInst_Delegate_t3022476291_0_0_0_Types[] = { &Delegate_t3022476291_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t3022476291_0_0_0 = { 1, GenInst_Delegate_t3022476291_0_0_0_Types };
extern const Il2CppType ISerializable_t1245643778_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1245643778_0_0_0_Types[] = { &ISerializable_t1245643778_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1245643778_0_0_0 = { 1, GenInst_ISerializable_t1245643778_0_0_0_Types };
extern const Il2CppType ParameterInfo_t2249040075_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t2249040075_0_0_0_Types[] = { &ParameterInfo_t2249040075_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0 = { 1, GenInst_ParameterInfo_t2249040075_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t470209990_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t470209990_0_0_0_Types[] = { &_ParameterInfo_t470209990_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t470209990_0_0_0 = { 1, GenInst__ParameterInfo_t470209990_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1820634920_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1820634920_0_0_0_Types[] = { &ParameterModifier_t1820634920_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1820634920_0_0_0 = { 1, GenInst_ParameterModifier_t1820634920_0_0_0_Types };
extern const Il2CppType IComparable_1_t2818721834_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2818721834_0_0_0_Types[] = { &IComparable_1_t2818721834_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2818721834_0_0_0 = { 1, GenInst_IComparable_1_t2818721834_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3190864780_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3190864780_0_0_0_Types[] = { &IEquatable_1_t3190864780_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3190864780_0_0_0 = { 1, GenInst_IEquatable_1_t3190864780_0_0_0_Types };
extern const Il2CppType IComparable_1_t446068841_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t446068841_0_0_0_Types[] = { &IComparable_1_t446068841_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t446068841_0_0_0 = { 1, GenInst_IComparable_1_t446068841_0_0_0_Types };
extern const Il2CppType IEquatable_1_t818211787_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t818211787_0_0_0_Types[] = { &IEquatable_1_t818211787_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t818211787_0_0_0 = { 1, GenInst_IEquatable_1_t818211787_0_0_0_Types };
extern const Il2CppType IComparable_1_t1578117841_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1578117841_0_0_0_Types[] = { &IComparable_1_t1578117841_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1578117841_0_0_0 = { 1, GenInst_IComparable_1_t1578117841_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1950260787_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1950260787_0_0_0_Types[] = { &IEquatable_1_t1950260787_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1950260787_0_0_0 = { 1, GenInst_IEquatable_1_t1950260787_0_0_0_Types };
extern const Il2CppType IComparable_1_t2286256772_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2286256772_0_0_0_Types[] = { &IComparable_1_t2286256772_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2286256772_0_0_0 = { 1, GenInst_IComparable_1_t2286256772_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2658399718_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2658399718_0_0_0_Types[] = { &IEquatable_1_t2658399718_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2658399718_0_0_0 = { 1, GenInst_IEquatable_1_t2658399718_0_0_0_Types };
extern const Il2CppType IComparable_1_t2740917260_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2740917260_0_0_0_Types[] = { &IComparable_1_t2740917260_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2740917260_0_0_0 = { 1, GenInst_IComparable_1_t2740917260_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3113060206_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3113060206_0_0_0_Types[] = { &IEquatable_1_t3113060206_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3113060206_0_0_0 = { 1, GenInst_IEquatable_1_t3113060206_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType EventInfo_t_0_0_0;
static const Il2CppType* GenInst_EventInfo_t_0_0_0_Types[] = { &EventInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_EventInfo_t_0_0_0 = { 1, GenInst_EventInfo_t_0_0_0_Types };
extern const Il2CppType _EventInfo_t2430923913_0_0_0;
static const Il2CppType* GenInst__EventInfo_t2430923913_0_0_0_Types[] = { &_EventInfo_t2430923913_0_0_0 };
extern const Il2CppGenericInst GenInst__EventInfo_t2430923913_0_0_0 = { 1, GenInst__EventInfo_t2430923913_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2511231167_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2511231167_0_0_0_Types[] = { &_FieldInfo_t2511231167_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2511231167_0_0_0 = { 1, GenInst__FieldInfo_t2511231167_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3642518830_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3642518830_0_0_0_Types[] = { &_MethodInfo_t3642518830_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3642518830_0_0_0 = { 1, GenInst__MethodInfo_t3642518830_0_0_0_Types };
extern const Il2CppType MethodBase_t904190842_0_0_0;
static const Il2CppType* GenInst_MethodBase_t904190842_0_0_0_Types[] = { &MethodBase_t904190842_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t904190842_0_0_0 = { 1, GenInst_MethodBase_t904190842_0_0_0_Types };
extern const Il2CppType _MethodBase_t1935530873_0_0_0;
static const Il2CppType* GenInst__MethodBase_t1935530873_0_0_0_Types[] = { &_MethodBase_t1935530873_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t1935530873_0_0_0 = { 1, GenInst__MethodBase_t1935530873_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t1567586598_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t1567586598_0_0_0_Types[] = { &_PropertyInfo_t1567586598_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t1567586598_0_0_0 = { 1, GenInst__PropertyInfo_t1567586598_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t2851816542_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t2851816542_0_0_0_Types[] = { &ConstructorInfo_t2851816542_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t2851816542_0_0_0 = { 1, GenInst_ConstructorInfo_t2851816542_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3269099341_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3269099341_0_0_0_Types[] = { &_ConstructorInfo_t3269099341_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3269099341_0_0_0 = { 1, GenInst__ConstructorInfo_t3269099341_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType TableRange_t2011406615_0_0_0;
static const Il2CppType* GenInst_TableRange_t2011406615_0_0_0_Types[] = { &TableRange_t2011406615_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t2011406615_0_0_0 = { 1, GenInst_TableRange_t2011406615_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1449609243_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1449609243_0_0_0_Types[] = { &TailoringInfo_t1449609243_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1449609243_0_0_0 = { 1, GenInst_TailoringInfo_t1449609243_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3716250094_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0 = { 1, GenInst_KeyValuePair_2_t3716250094_0_0_0_Types };
extern const Il2CppType Link_t2723257478_0_0_0;
static const Il2CppType* GenInst_Link_t2723257478_0_0_0_Types[] = { &Link_t2723257478_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2723257478_0_0_0 = { 1, GenInst_Link_t2723257478_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t3048875398_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0 = { 1, GenInst_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1744001932_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1744001932_0_0_0_Types[] = { &KeyValuePair_2_t1744001932_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1744001932_0_0_0 = { 1, GenInst_KeyValuePair_2_t1744001932_0_0_0_Types };
extern const Il2CppType Contraction_t1673853792_0_0_0;
static const Il2CppType* GenInst_Contraction_t1673853792_0_0_0_Types[] = { &Contraction_t1673853792_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1673853792_0_0_0 = { 1, GenInst_Contraction_t1673853792_0_0_0_Types };
extern const Il2CppType Level2Map_t3322505726_0_0_0;
static const Il2CppType* GenInst_Level2Map_t3322505726_0_0_0_Types[] = { &Level2Map_t3322505726_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t3322505726_0_0_0 = { 1, GenInst_Level2Map_t3322505726_0_0_0_Types };
extern const Il2CppType BigInteger_t925946152_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946152_0_0_0_Types[] = { &BigInteger_t925946152_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946152_0_0_0 = { 1, GenInst_BigInteger_t925946152_0_0_0_Types };
extern const Il2CppType KeySizes_t3144736271_0_0_0;
static const Il2CppType* GenInst_KeySizes_t3144736271_0_0_0_Types[] = { &KeySizes_t3144736271_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t3144736271_0_0_0 = { 1, GenInst_KeySizes_t3144736271_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t38854645_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0 = { 1, GenInst_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
extern const Il2CppType IComparable_1_t1362446645_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1362446645_0_0_0_Types[] = { &IComparable_1_t1362446645_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1362446645_0_0_0 = { 1, GenInst_IComparable_1_t1362446645_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1734589591_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1734589591_0_0_0_Types[] = { &IEquatable_1_t1734589591_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1734589591_0_0_0 = { 1, GenInst_IEquatable_1_t1734589591_0_0_0_Types };
extern const Il2CppType Slot_t2022531261_0_0_0;
static const Il2CppType* GenInst_Slot_t2022531261_0_0_0_Types[] = { &Slot_t2022531261_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2022531261_0_0_0 = { 1, GenInst_Slot_t2022531261_0_0_0_Types };
extern const Il2CppType Slot_t2267560602_0_0_0;
static const Il2CppType* GenInst_Slot_t2267560602_0_0_0_Types[] = { &Slot_t2267560602_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2267560602_0_0_0 = { 1, GenInst_Slot_t2267560602_0_0_0_Types };
extern const Il2CppType StackFrame_t2050294881_0_0_0;
static const Il2CppType* GenInst_StackFrame_t2050294881_0_0_0_Types[] = { &StackFrame_t2050294881_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t2050294881_0_0_0 = { 1, GenInst_StackFrame_t2050294881_0_0_0_Types };
extern const Il2CppType Calendar_t585061108_0_0_0;
static const Il2CppType* GenInst_Calendar_t585061108_0_0_0_Types[] = { &Calendar_t585061108_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t585061108_0_0_0 = { 1, GenInst_Calendar_t585061108_0_0_0_Types };
extern const Il2CppType CultureInfo_t3500843524_0_0_0;
static const Il2CppType* GenInst_CultureInfo_t3500843524_0_0_0_Types[] = { &CultureInfo_t3500843524_0_0_0 };
extern const Il2CppGenericInst GenInst_CultureInfo_t3500843524_0_0_0 = { 1, GenInst_CultureInfo_t3500843524_0_0_0_Types };
extern const Il2CppType IFormatProvider_t2849799027_0_0_0;
static const Il2CppType* GenInst_IFormatProvider_t2849799027_0_0_0_Types[] = { &IFormatProvider_t2849799027_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormatProvider_t2849799027_0_0_0 = { 1, GenInst_IFormatProvider_t2849799027_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t4156028127_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t4156028127_0_0_0_Types[] = { &ModuleBuilder_t4156028127_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t4156028127_0_0_0 = { 1, GenInst_ModuleBuilder_t4156028127_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1075102050_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1075102050_0_0_0_Types[] = { &_ModuleBuilder_t1075102050_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1075102050_0_0_0 = { 1, GenInst__ModuleBuilder_t1075102050_0_0_0_Types };
extern const Il2CppType Module_t4282841206_0_0_0;
static const Il2CppType* GenInst_Module_t4282841206_0_0_0_Types[] = { &Module_t4282841206_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t4282841206_0_0_0 = { 1, GenInst_Module_t4282841206_0_0_0_Types };
extern const Il2CppType _Module_t2144668161_0_0_0;
static const Il2CppType* GenInst__Module_t2144668161_0_0_0_Types[] = { &_Module_t2144668161_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2144668161_0_0_0 = { 1, GenInst__Module_t2144668161_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t3344728474_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t3344728474_0_0_0_Types[] = { &ParameterBuilder_t3344728474_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3344728474_0_0_0 = { 1, GenInst_ParameterBuilder_t3344728474_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t2251638747_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t2251638747_0_0_0_Types[] = { &_ParameterBuilder_t2251638747_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2251638747_0_0_0 = { 1, GenInst__ParameterBuilder_t2251638747_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t1664964607_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t1664964607_0_0_0_Types[] = { &TypeU5BU5D_t1664964607_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1664964607_0_0_0 = { 1, GenInst_TypeU5BU5D_t1664964607_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t91669223_0_0_0;
static const Il2CppType* GenInst_ICollection_t91669223_0_0_0_Types[] = { &ICollection_t91669223_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t91669223_0_0_0 = { 1, GenInst_ICollection_t91669223_0_0_0_Types };
extern const Il2CppType IList_t3321498491_0_0_0;
static const Il2CppType* GenInst_IList_t3321498491_0_0_0_Types[] = { &IList_t3321498491_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t3321498491_0_0_0 = { 1, GenInst_IList_t3321498491_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t149559338_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t149559338_0_0_0_Types[] = { &ILTokenInfo_t149559338_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t149559338_0_0_0 = { 1, GenInst_ILTokenInfo_t149559338_0_0_0_Types };
extern const Il2CppType LabelData_t3712112744_0_0_0;
static const Il2CppType* GenInst_LabelData_t3712112744_0_0_0_Types[] = { &LabelData_t3712112744_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t3712112744_0_0_0 = { 1, GenInst_LabelData_t3712112744_0_0_0_Types };
extern const Il2CppType LabelFixup_t4090909514_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t4090909514_0_0_0_Types[] = { &LabelFixup_t4090909514_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t4090909514_0_0_0 = { 1, GenInst_LabelFixup_t4090909514_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1370236603_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1370236603_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types };
extern const Il2CppType TypeBuilder_t3308873219_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t3308873219_0_0_0_Types[] = { &TypeBuilder_t3308873219_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t3308873219_0_0_0 = { 1, GenInst_TypeBuilder_t3308873219_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2783404358_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2783404358_0_0_0_Types[] = { &_TypeBuilder_t2783404358_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2783404358_0_0_0 = { 1, GenInst__TypeBuilder_t2783404358_0_0_0_Types };
extern const Il2CppType MethodBuilder_t644187984_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t644187984_0_0_0_Types[] = { &MethodBuilder_t644187984_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t644187984_0_0_0 = { 1, GenInst_MethodBuilder_t644187984_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t3932949077_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t3932949077_0_0_0_Types[] = { &_MethodBuilder_t3932949077_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3932949077_0_0_0 = { 1, GenInst__MethodBuilder_t3932949077_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t700974433_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t700974433_0_0_0_Types[] = { &ConstructorBuilder_t700974433_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t700974433_0_0_0 = { 1, GenInst_ConstructorBuilder_t700974433_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t1236878896_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t1236878896_0_0_0_Types[] = { &_ConstructorBuilder_t1236878896_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1236878896_0_0_0 = { 1, GenInst__ConstructorBuilder_t1236878896_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t3694255912_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t3694255912_0_0_0_Types[] = { &PropertyBuilder_t3694255912_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t3694255912_0_0_0 = { 1, GenInst_PropertyBuilder_t3694255912_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t3341912621_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t3341912621_0_0_0_Types[] = { &_PropertyBuilder_t3341912621_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t3341912621_0_0_0 = { 1, GenInst__PropertyBuilder_t3341912621_0_0_0_Types };
extern const Il2CppType FieldBuilder_t2784804005_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t2784804005_0_0_0_Types[] = { &FieldBuilder_t2784804005_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2784804005_0_0_0 = { 1, GenInst_FieldBuilder_t2784804005_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t1895266044_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t1895266044_0_0_0_Types[] = { &_FieldBuilder_t1895266044_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t1895266044_0_0_0 = { 1, GenInst__FieldBuilder_t1895266044_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t1498197914_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t94157543_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t3093286891_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t3093286891_0_0_0_Types[] = { &CustomAttributeData_t3093286891_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t3093286891_0_0_0 = { 1, GenInst_CustomAttributeData_t3093286891_0_0_0_Types };
extern const Il2CppType ResourceInfo_t3933049236_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t3933049236_0_0_0_Types[] = { &ResourceInfo_t3933049236_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t3933049236_0_0_0 = { 1, GenInst_ResourceInfo_t3933049236_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t333236149_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t333236149_0_0_0_Types[] = { &ResourceCacheItem_t333236149_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t333236149_0_0_0 = { 1, GenInst_ResourceCacheItem_t333236149_0_0_0_Types };
extern const Il2CppType IContextProperty_t287246399_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t287246399_0_0_0_Types[] = { &IContextProperty_t287246399_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t287246399_0_0_0 = { 1, GenInst_IContextProperty_t287246399_0_0_0_Types };
extern const Il2CppType Header_t2756440555_0_0_0;
static const Il2CppType* GenInst_Header_t2756440555_0_0_0_Types[] = { &Header_t2756440555_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t2756440555_0_0_0 = { 1, GenInst_Header_t2756440555_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2759960940_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2759960940_0_0_0_Types[] = { &ITrackingHandler_t2759960940_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2759960940_0_0_0 = { 1, GenInst_ITrackingHandler_t2759960940_0_0_0_Types };
extern const Il2CppType IContextAttribute_t2439121372_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t2439121372_0_0_0_Types[] = { &IContextAttribute_t2439121372_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2439121372_0_0_0 = { 1, GenInst_IContextAttribute_t2439121372_0_0_0_Types };
extern const Il2CppType DateTime_t693205669_0_0_0;
static const Il2CppType* GenInst_DateTime_t693205669_0_0_0_Types[] = { &DateTime_t693205669_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0 = { 1, GenInst_DateTime_t693205669_0_0_0_Types };
extern const Il2CppType IComparable_1_t2525044892_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2525044892_0_0_0_Types[] = { &IComparable_1_t2525044892_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2525044892_0_0_0 = { 1, GenInst_IComparable_1_t2525044892_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2897187838_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2897187838_0_0_0_Types[] = { &IEquatable_1_t2897187838_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2897187838_0_0_0 = { 1, GenInst_IEquatable_1_t2897187838_0_0_0_Types };
extern const Il2CppType IComparable_1_t2556540300_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2556540300_0_0_0_Types[] = { &IComparable_1_t2556540300_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2556540300_0_0_0 = { 1, GenInst_IComparable_1_t2556540300_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2928683246_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2928683246_0_0_0_Types[] = { &IEquatable_1_t2928683246_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2928683246_0_0_0 = { 1, GenInst_IEquatable_1_t2928683246_0_0_0_Types };
extern const Il2CppType TimeSpan_t3430258949_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t3430258949_0_0_0_Types[] = { &TimeSpan_t3430258949_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t3430258949_0_0_0 = { 1, GenInst_TimeSpan_t3430258949_0_0_0_Types };
extern const Il2CppType IComparable_1_t967130876_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t967130876_0_0_0_Types[] = { &IComparable_1_t967130876_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t967130876_0_0_0 = { 1, GenInst_IComparable_1_t967130876_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1339273822_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1339273822_0_0_0_Types[] = { &IEquatable_1_t1339273822_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1339273822_0_0_0 = { 1, GenInst_IEquatable_1_t1339273822_0_0_0_Types };
extern const Il2CppType TypeTag_t141209596_0_0_0;
static const Il2CppType* GenInst_TypeTag_t141209596_0_0_0_Types[] = { &TypeTag_t141209596_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t141209596_0_0_0 = { 1, GenInst_TypeTag_t141209596_0_0_0_Types };
extern const Il2CppType Enum_t2459695545_0_0_0;
static const Il2CppType* GenInst_Enum_t2459695545_0_0_0_Types[] = { &Enum_t2459695545_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t2459695545_0_0_0 = { 1, GenInst_Enum_t2459695545_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType StrongName_t2988747270_0_0_0;
static const Il2CppType* GenInst_StrongName_t2988747270_0_0_0_Types[] = { &StrongName_t2988747270_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2988747270_0_0_0 = { 1, GenInst_StrongName_t2988747270_0_0_0_Types };
extern const Il2CppType Assembly_t4268412390_0_0_0;
static const Il2CppType* GenInst_Assembly_t4268412390_0_0_0_Types[] = { &Assembly_t4268412390_0_0_0 };
extern const Il2CppGenericInst GenInst_Assembly_t4268412390_0_0_0 = { 1, GenInst_Assembly_t4268412390_0_0_0_Types };
extern const Il2CppType _Assembly_t2937922309_0_0_0;
static const Il2CppType* GenInst__Assembly_t2937922309_0_0_0_Types[] = { &_Assembly_t2937922309_0_0_0 };
extern const Il2CppGenericInst GenInst__Assembly_t2937922309_0_0_0 = { 1, GenInst__Assembly_t2937922309_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1362988906_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1362988906_0_0_0_Types[] = { &DateTimeOffset_t1362988906_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1362988906_0_0_0 = { 1, GenInst_DateTimeOffset_t1362988906_0_0_0_Types };
extern const Il2CppType Guid_t2533601593_0_0_0;
static const Il2CppType* GenInst_Guid_t2533601593_0_0_0_Types[] = { &Guid_t2533601593_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t2533601593_0_0_0 = { 1, GenInst_Guid_t2533601593_0_0_0_Types };
extern const Il2CppType Version_t1755874712_0_0_0;
static const Il2CppType* GenInst_Version_t1755874712_0_0_0_Types[] = { &Version_t1755874712_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1755874712_0_0_0 = { 1, GenInst_Version_t1755874712_0_0_0_Types };
extern const Il2CppType Node_t2499136326_0_0_0;
static const Il2CppType* GenInst_Node_t2499136326_0_0_0_Types[] = { &Node_t2499136326_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t2499136326_0_0_0 = { 1, GenInst_Node_t2499136326_0_0_0_Types };
extern const Il2CppType EventDescriptor_t962731901_0_0_0;
static const Il2CppType* GenInst_EventDescriptor_t962731901_0_0_0_Types[] = { &EventDescriptor_t962731901_0_0_0 };
extern const Il2CppGenericInst GenInst_EventDescriptor_t962731901_0_0_0 = { 1, GenInst_EventDescriptor_t962731901_0_0_0_Types };
extern const Il2CppType MemberDescriptor_t3749827553_0_0_0;
static const Il2CppType* GenInst_MemberDescriptor_t3749827553_0_0_0_Types[] = { &MemberDescriptor_t3749827553_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberDescriptor_t3749827553_0_0_0 = { 1, GenInst_MemberDescriptor_t3749827553_0_0_0_Types };
extern const Il2CppType PropertyDescriptor_t4250402154_0_0_0;
static const Il2CppType* GenInst_PropertyDescriptor_t4250402154_0_0_0_Types[] = { &PropertyDescriptor_t4250402154_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyDescriptor_t4250402154_0_0_0 = { 1, GenInst_PropertyDescriptor_t4250402154_0_0_0_Types };
extern const Il2CppType LinkedList_1_t2743332604_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0 = { 2, GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_Types };
extern const Il2CppType TypeDescriptionProvider_t2438624375_0_0_0;
static const Il2CppType* GenInst_TypeDescriptionProvider_t2438624375_0_0_0_Types[] = { &TypeDescriptionProvider_t2438624375_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeDescriptionProvider_t2438624375_0_0_0 = { 1, GenInst_TypeDescriptionProvider_t2438624375_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2743332604_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_LinkedList_1_t2743332604_0_0_0_Types[] = { &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t2743332604_0_0_0 = { 1, GenInst_LinkedList_1_t2743332604_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2438035723_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2438035723_0_0_0_Types[] = { &KeyValuePair_2_t2438035723_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2438035723_0_0_0 = { 1, GenInst_KeyValuePair_2_t2438035723_0_0_0_Types };
extern const Il2CppType WeakObjectWrapper_t2012978780_0_0_0;
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0, &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0 = { 2, GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0 = { 1, GenInst_WeakObjectWrapper_t2012978780_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0, &LinkedList_1_t2743332604_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3261256129_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3261256129_0_0_0_Types[] = { &KeyValuePair_2_t3261256129_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3261256129_0_0_0 = { 1, GenInst_KeyValuePair_2_t3261256129_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t4278378721_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t4278378721_0_0_0_Types[] = { &X509ChainStatus_t4278378721_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t4278378721_0_0_0 = { 1, GenInst_X509ChainStatus_t4278378721_0_0_0_Types };
extern const Il2CppType IPAddress_t1399971723_0_0_0;
static const Il2CppType* GenInst_IPAddress_t1399971723_0_0_0_Types[] = { &IPAddress_t1399971723_0_0_0 };
extern const Il2CppGenericInst GenInst_IPAddress_t1399971723_0_0_0 = { 1, GenInst_IPAddress_t1399971723_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t2594217482_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t2594217482_0_0_0_Types[] = { &ArraySegment_1_t2594217482_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t2594217482_0_0_0 = { 1, GenInst_ArraySegment_1_t2594217482_0_0_0_Types };
extern const Il2CppType Cookie_t3154017544_0_0_0;
static const Il2CppType* GenInst_Cookie_t3154017544_0_0_0_Types[] = { &Cookie_t3154017544_0_0_0 };
extern const Il2CppGenericInst GenInst_Cookie_t3154017544_0_0_0 = { 1, GenInst_Cookie_t3154017544_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1174980068_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0 = { 1, GenInst_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3497699202_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3497699202_0_0_0_Types[] = { &KeyValuePair_2_t3497699202_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3497699202_0_0_0 = { 1, GenInst_KeyValuePair_2_t3497699202_0_0_0_Types };
extern const Il2CppType X509Certificate_t283079845_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t283079845_0_0_0_Types[] = { &X509Certificate_t283079845_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t283079845_0_0_0 = { 1, GenInst_X509Certificate_t283079845_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t327125377_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t327125377_0_0_0_Types[] = { &IDeserializationCallback_t327125377_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t327125377_0_0_0 = { 1, GenInst_IDeserializationCallback_t327125377_0_0_0_Types };
extern const Il2CppType Capture_t4157900610_0_0_0;
static const Il2CppType* GenInst_Capture_t4157900610_0_0_0_Types[] = { &Capture_t4157900610_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t4157900610_0_0_0 = { 1, GenInst_Capture_t4157900610_0_0_0_Types };
extern const Il2CppType Group_t3761430853_0_0_0;
static const Il2CppType* GenInst_Group_t3761430853_0_0_0_Types[] = { &Group_t3761430853_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t3761430853_0_0_0 = { 1, GenInst_Group_t3761430853_0_0_0_Types };
extern const Il2CppType Mark_t2724874473_0_0_0;
static const Il2CppType* GenInst_Mark_t2724874473_0_0_0_Types[] = { &Mark_t2724874473_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t2724874473_0_0_0 = { 1, GenInst_Mark_t2724874473_0_0_0_Types };
extern const Il2CppType UriScheme_t1876590943_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1876590943_0_0_0_Types[] = { &UriScheme_t1876590943_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1876590943_0_0_0 = { 1, GenInst_UriScheme_t1876590943_0_0_0_Types };
extern const Il2CppType BigInteger_t925946153_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946153_0_0_0_Types[] = { &BigInteger_t925946153_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946153_0_0_0 = { 1, GenInst_BigInteger_t925946153_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t3397334013_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t3397334013_0_0_0_Types[] = { &ByteU5BU5D_t3397334013_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3397334013_0_0_0 = { 1, GenInst_ByteU5BU5D_t3397334013_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t4001384466_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t4001384466_0_0_0_Types[] = { &ClientCertificateType_t4001384466_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t4001384466_0_0_0 = { 1, GenInst_ClientCertificateType_t4001384466_0_0_0_Types };
extern const Il2CppType Link_t865133271_0_0_0;
static const Il2CppType* GenInst_Link_t865133271_0_0_0_Types[] = { &Link_t865133271_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t865133271_0_0_0 = { 1, GenInst_Link_t865133271_0_0_0_Types };
extern const Il2CppType List_1_t2058570427_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0_Types[] = { &Il2CppObject_0_0_0, &List_1_t2058570427_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &List_1_t2058570427_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t2058570427_0_0_0_Types[] = { &List_1_t2058570427_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2058570427_0_0_0 = { 1, GenInst_List_1_t2058570427_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3702943073_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3702943073_0_0_0_Types[] = { &KeyValuePair_2_t3702943073_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3702943073_0_0_0 = { 1, GenInst_KeyValuePair_2_t3702943073_0_0_0_Types };
extern const Il2CppType IGrouping_2_t906937361_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t906937361_0_0_0_Types[] = { &IGrouping_2_t906937361_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t906937361_0_0_0 = { 1, GenInst_IGrouping_2_t906937361_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2981576340_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_IEnumerable_1_t2981576340_0_0_0_Types[] = { &Il2CppObject_0_0_0, &IEnumerable_1_t2981576340_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_IEnumerable_1_t2981576340_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_IEnumerable_1_t2981576340_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2361573779_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2361573779_0_0_0_Types[] = { &KeyValuePair_2_t2361573779_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2361573779_0_0_0 = { 1, GenInst_KeyValuePair_2_t2361573779_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1701344717_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1701344717_0_0_0_Types[] = { &KeyValuePair_2_t1701344717_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0 = { 1, GenInst_KeyValuePair_2_t1701344717_0_0_0_Types };
extern const Il2CppType Object_t1021602117_0_0_0;
static const Il2CppType* GenInst_Object_t1021602117_0_0_0_Types[] = { &Object_t1021602117_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t1021602117_0_0_0 = { 1, GenInst_Object_t1021602117_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t4083280315_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t4083280315_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t3498529102_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t3498529102_0_0_0_Types[] = { &IAchievementDescription_t3498529102_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3498529102_0_0_0 = { 1, GenInst_IAchievementDescription_t3498529102_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t2709554645_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types[] = { &IAchievementU5BU5D_t2709554645_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t2709554645_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types };
extern const Il2CppType IAchievement_t1752291260_0_0_0;
static const Il2CppType* GenInst_IAchievement_t1752291260_0_0_0_Types[] = { &IAchievement_t1752291260_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t1752291260_0_0_0 = { 1, GenInst_IAchievement_t1752291260_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t3237304636_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types[] = { &IScoreU5BU5D_t3237304636_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t3237304636_0_0_0 = { 1, GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types };
extern const Il2CppType IScore_t513966369_0_0_0;
static const Il2CppType* GenInst_IScore_t513966369_0_0_0_Types[] = { &IScore_t513966369_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t513966369_0_0_0 = { 1, GenInst_IScore_t513966369_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t3461248430_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types[] = { &IUserProfileU5BU5D_t3461248430_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3461248430_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types };
extern const Il2CppType IUserProfile_t4108565527_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t4108565527_0_0_0_Types[] = { &IUserProfile_t4108565527_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t4108565527_0_0_0 = { 1, GenInst_IUserProfile_t4108565527_0_0_0_Types };
extern const Il2CppType AchievementDescription_t3110978151_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t3110978151_0_0_0_Types[] = { &AchievementDescription_t3110978151_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3110978151_0_0_0 = { 1, GenInst_AchievementDescription_t3110978151_0_0_0_Types };
extern const Il2CppType UserProfile_t3365630962_0_0_0;
static const Il2CppType* GenInst_UserProfile_t3365630962_0_0_0_Types[] = { &UserProfile_t3365630962_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t3365630962_0_0_0 = { 1, GenInst_UserProfile_t3365630962_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t453887929_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t453887929_0_0_0_Types[] = { &GcLeaderboard_t453887929_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t453887929_0_0_0 = { 1, GenInst_GcLeaderboard_t453887929_0_0_0_Types };
extern const Il2CppType GcAchievementData_t1754866149_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t1754866149_0_0_0_Types[] = { &GcAchievementData_t1754866149_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1754866149_0_0_0 = { 1, GenInst_GcAchievementData_t1754866149_0_0_0_Types };
extern const Il2CppType Achievement_t1333316625_0_0_0;
static const Il2CppType* GenInst_Achievement_t1333316625_0_0_0_Types[] = { &Achievement_t1333316625_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t1333316625_0_0_0 = { 1, GenInst_Achievement_t1333316625_0_0_0_Types };
extern const Il2CppType GcScoreData_t3676783238_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t3676783238_0_0_0_Types[] = { &GcScoreData_t3676783238_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t3676783238_0_0_0 = { 1, GenInst_GcScoreData_t3676783238_0_0_0_Types };
extern const Il2CppType Score_t2307748940_0_0_0;
static const Il2CppType* GenInst_Score_t2307748940_0_0_0_Types[] = { &Score_t2307748940_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t2307748940_0_0_0 = { 1, GenInst_Score_t2307748940_0_0_0_Types };
extern const Il2CppType Material_t193706927_0_0_0;
static const Il2CppType* GenInst_Material_t193706927_0_0_0_Types[] = { &Material_t193706927_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t193706927_0_0_0 = { 1, GenInst_Material_t193706927_0_0_0_Types };
extern const Il2CppType Color_t2020392075_0_0_0;
static const Il2CppType* GenInst_Color_t2020392075_0_0_0_Types[] = { &Color_t2020392075_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t2020392075_0_0_0 = { 1, GenInst_Color_t2020392075_0_0_0_Types };
extern const Il2CppType Keyframe_t1449471340_0_0_0;
static const Il2CppType* GenInst_Keyframe_t1449471340_0_0_0_Types[] = { &Keyframe_t1449471340_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t1449471340_0_0_0 = { 1, GenInst_Keyframe_t1449471340_0_0_0_Types };
extern const Il2CppType Vector3_t2243707580_0_0_0;
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0 = { 1, GenInst_Vector3_t2243707580_0_0_0_Types };
extern const Il2CppType Vector4_t2243707581_0_0_0;
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0 = { 1, GenInst_Vector4_t2243707581_0_0_0_Types };
extern const Il2CppType Vector2_t2243707579_0_0_0;
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0 = { 1, GenInst_Vector2_t2243707579_0_0_0_Types };
extern const Il2CppType Color32_t874517518_0_0_0;
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0 = { 1, GenInst_Color32_t874517518_0_0_0_Types };
extern const Il2CppType Camera_t189460977_0_0_0;
static const Il2CppType* GenInst_Camera_t189460977_0_0_0_Types[] = { &Camera_t189460977_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t189460977_0_0_0 = { 1, GenInst_Camera_t189460977_0_0_0_Types };
extern const Il2CppType Behaviour_t955675639_0_0_0;
static const Il2CppType* GenInst_Behaviour_t955675639_0_0_0_Types[] = { &Behaviour_t955675639_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t955675639_0_0_0 = { 1, GenInst_Behaviour_t955675639_0_0_0_Types };
extern const Il2CppType Component_t3819376471_0_0_0;
static const Il2CppType* GenInst_Component_t3819376471_0_0_0_Types[] = { &Component_t3819376471_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3819376471_0_0_0 = { 1, GenInst_Component_t3819376471_0_0_0_Types };
extern const Il2CppType Display_t3666191348_0_0_0;
static const Il2CppType* GenInst_Display_t3666191348_0_0_0_Types[] = { &Display_t3666191348_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t3666191348_0_0_0 = { 1, GenInst_Display_t3666191348_0_0_0_Types };
extern const Il2CppType Touch_t407273883_0_0_0;
static const Il2CppType* GenInst_Touch_t407273883_0_0_0_Types[] = { &Touch_t407273883_0_0_0 };
extern const Il2CppGenericInst GenInst_Touch_t407273883_0_0_0 = { 1, GenInst_Touch_t407273883_0_0_0_Types };
extern const Il2CppType jvalue_t3412352577_0_0_0;
static const Il2CppType* GenInst_jvalue_t3412352577_0_0_0_Types[] = { &jvalue_t3412352577_0_0_0 };
extern const Il2CppGenericInst GenInst_jvalue_t3412352577_0_0_0 = { 1, GenInst_jvalue_t3412352577_0_0_0_Types };
extern const Il2CppType AndroidJavaObject_t4251328308_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject_t4251328308_0_0_0_Types[] = { &AndroidJavaObject_t4251328308_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject_t4251328308_0_0_0 = { 1, GenInst_AndroidJavaObject_t4251328308_0_0_0_Types };
extern const Il2CppType IDisposable_t2427283555_0_0_0;
static const Il2CppType* GenInst_IDisposable_t2427283555_0_0_0_Types[] = { &IDisposable_t2427283555_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t2427283555_0_0_0 = { 1, GenInst_IDisposable_t2427283555_0_0_0_Types };
extern const Il2CppType LocalNotification_t317971878_0_0_0;
static const Il2CppType* GenInst_LocalNotification_t317971878_0_0_0_Types[] = { &LocalNotification_t317971878_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalNotification_t317971878_0_0_0 = { 1, GenInst_LocalNotification_t317971878_0_0_0_Types };
extern const Il2CppType Playable_t3667545548_0_0_0;
static const Il2CppType* GenInst_Playable_t3667545548_0_0_0_Types[] = { &Playable_t3667545548_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t3667545548_0_0_0 = { 1, GenInst_Playable_t3667545548_0_0_0_Types };
extern const Il2CppType Scene_t1684909666_0_0_0;
extern const Il2CppType LoadSceneMode_t2981886439_0_0_0;
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &LoadSceneMode_t2981886439_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0 = { 1, GenInst_Scene_t1684909666_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types };
extern const Il2CppType ContactPoint_t1376425630_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t1376425630_0_0_0_Types[] = { &ContactPoint_t1376425630_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t1376425630_0_0_0 = { 1, GenInst_ContactPoint_t1376425630_0_0_0_Types };
extern const Il2CppType RaycastHit_t87180320_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t87180320_0_0_0_Types[] = { &RaycastHit_t87180320_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t87180320_0_0_0 = { 1, GenInst_RaycastHit_t87180320_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t502193897_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t502193897_0_0_0_Types[] = { &Rigidbody2D_t502193897_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t502193897_0_0_0 = { 1, GenInst_Rigidbody2D_t502193897_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t4063908774_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t4063908774_0_0_0_Types[] = { &RaycastHit2D_t4063908774_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t4063908774_0_0_0 = { 1, GenInst_RaycastHit2D_t4063908774_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t3659330976_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t3659330976_0_0_0_Types[] = { &ContactPoint2D_t3659330976_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t3659330976_0_0_0 = { 1, GenInst_ContactPoint2D_t3659330976_0_0_0_Types };
extern const Il2CppType UIVertex_t1204258818_0_0_0;
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0 = { 1, GenInst_UIVertex_t1204258818_0_0_0_Types };
extern const Il2CppType UICharInfo_t3056636800_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0 = { 1, GenInst_UICharInfo_t3056636800_0_0_0_Types };
extern const Il2CppType UILineInfo_t3621277874_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0 = { 1, GenInst_UILineInfo_t3621277874_0_0_0_Types };
extern const Il2CppType Font_t4239498691_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_Types[] = { &Font_t4239498691_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0 = { 1, GenInst_Font_t4239498691_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t4183744904_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t4183744904_0_0_0_Types[] = { &GUILayoutOption_t4183744904_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t4183744904_0_0_0 = { 1, GenInst_GUILayoutOption_t4183744904_0_0_0_Types };
extern const Il2CppType LayoutCache_t3120781045_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3749587448_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0 = { 1, GenInst_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_LayoutCache_t3120781045_0_0_0_Types[] = { &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutCache_t3120781045_0_0_0 = { 1, GenInst_LayoutCache_t3120781045_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4180919198_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4180919198_0_0_0_Types[] = { &KeyValuePair_2_t4180919198_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4180919198_0_0_0 = { 1, GenInst_KeyValuePair_2_t4180919198_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t3828586629_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t3828586629_0_0_0_Types[] = { &GUILayoutEntry_t3828586629_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3828586629_0_0_0 = { 1, GenInst_GUILayoutEntry_t3828586629_0_0_0_Types };
extern const Il2CppType GUIStyle_t1799908754_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t1799908754_0_0_0_Types[] = { &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t1799908754_0_0_0 = { 1, GenInst_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1472033238_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1472033238_0_0_0_Types[] = { &KeyValuePair_2_t1472033238_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1472033238_0_0_0 = { 1, GenInst_KeyValuePair_2_t1472033238_0_0_0_Types };
extern const Il2CppType IMultipartFormSection_t1234744138_0_0_0;
static const Il2CppType* GenInst_IMultipartFormSection_t1234744138_0_0_0_Types[] = { &IMultipartFormSection_t1234744138_0_0_0 };
extern const Il2CppGenericInst GenInst_IMultipartFormSection_t1234744138_0_0_0 = { 1, GenInst_IMultipartFormSection_t1234744138_0_0_0_Types };
extern const Il2CppType ConstructorDelegate_t3084043859_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0_Types[] = { &Type_t_0_0_0, &ConstructorDelegate_t3084043859_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0 = { 2, GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0_Types };
static const Il2CppType* GenInst_ConstructorDelegate_t3084043859_0_0_0_Types[] = { &ConstructorDelegate_t3084043859_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorDelegate_t3084043859_0_0_0 = { 1, GenInst_ConstructorDelegate_t3084043859_0_0_0_Types };
extern const Il2CppType IDictionary_2_t266144316_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t266144316_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0_Types };
extern const Il2CppType GetDelegate_t352281633_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0_Types[] = { &String_t_0_0_0, &GetDelegate_t352281633_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0 = { 2, GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0_Types };
static const Il2CppType* GenInst_GetDelegate_t352281633_0_0_0_Types[] = { &GetDelegate_t352281633_0_0_0 };
extern const Il2CppGenericInst GenInst_GetDelegate_t352281633_0_0_0 = { 1, GenInst_GetDelegate_t352281633_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t266144316_0_0_0_Types[] = { &IDictionary_2_t266144316_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t266144316_0_0_0 = { 1, GenInst_IDictionary_2_t266144316_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3814930911_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t3814930911_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3901068228_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0_Types[] = { &String_t_0_0_0, &KeyValuePair_2_t3901068228_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0 = { 2, GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0_Types };
extern const Il2CppType SetDelegate_t4206365109_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_SetDelegate_t4206365109_0_0_0_Types[] = { &Type_t_0_0_0, &SetDelegate_t4206365109_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SetDelegate_t4206365109_0_0_0 = { 2, GenInst_Type_t_0_0_0_SetDelegate_t4206365109_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3901068228_0_0_0_Types[] = { &KeyValuePair_2_t3901068228_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3901068228_0_0_0 = { 1, GenInst_KeyValuePair_2_t3901068228_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t3814930911_0_0_0_Types[] = { &IDictionary_2_t3814930911_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3814930911_0_0_0 = { 1, GenInst_IDictionary_2_t3814930911_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1683227291_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1683227291_0_0_0_Types[] = { &KeyValuePair_2_t1683227291_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1683227291_0_0_0 = { 1, GenInst_KeyValuePair_2_t1683227291_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ConstructorDelegate_t3084043859_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2778746978_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2778746978_0_0_0_Types[] = { &KeyValuePair_2_t2778746978_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2778746978_0_0_0 = { 1, GenInst_KeyValuePair_2_t2778746978_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t266144316_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4255814731_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4255814731_0_0_0_Types[] = { &KeyValuePair_2_t4255814731_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4255814731_0_0_0 = { 1, GenInst_KeyValuePair_2_t4255814731_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t3814930911_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3509634030_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3509634030_0_0_0_Types[] = { &KeyValuePair_2_t3509634030_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3509634030_0_0_0 = { 1, GenInst_KeyValuePair_2_t3509634030_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GetDelegate_t352281633_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t24406117_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t24406117_0_0_0_Types[] = { &KeyValuePair_2_t24406117_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t24406117_0_0_0 = { 1, GenInst_KeyValuePair_2_t24406117_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t1683227291_0_0_0_Types[] = { &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0, &KeyValuePair_2_t1683227291_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t1683227291_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t1683227291_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &KeyValuePair_2_t3901068228_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3573192712_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3573192712_0_0_0_Types[] = { &KeyValuePair_2_t3573192712_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3573192712_0_0_0 = { 1, GenInst_KeyValuePair_2_t3573192712_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t2656950_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types[] = { &DisallowMultipleComponent_t2656950_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2656950_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t3043633143_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types[] = { &ExecuteInEditMode_t3043633143_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3043633143_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types };
extern const Il2CppType RequireComponent_t864575032_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t864575032_0_0_0_Types[] = { &RequireComponent_t864575032_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t864575032_0_0_0 = { 1, GenInst_RequireComponent_t864575032_0_0_0_Types };
extern const Il2CppType HitInfo_t1761367055_0_0_0;
static const Il2CppType* GenInst_HitInfo_t1761367055_0_0_0_Types[] = { &HitInfo_t1761367055_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t1761367055_0_0_0 = { 1, GenInst_HitInfo_t1761367055_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType PersistentCall_t3793436469_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t3793436469_0_0_0_Types[] = { &PersistentCall_t3793436469_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t3793436469_0_0_0 = { 1, GenInst_PersistentCall_t3793436469_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t2229564840_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t2229564840_0_0_0_Types[] = { &BaseInvokableCall_t2229564840_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2229564840_0_0_0 = { 1, GenInst_BaseInvokableCall_t2229564840_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_String_t_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Int32_t2071877448_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_String_t_0_0_0 = { 3, GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType HashSet_1_t275936122_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t275936122_0_0_0_Types[] = { &HashSet_1_t275936122_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t275936122_0_0_0 = { 1, GenInst_HashSet_1_t275936122_0_0_0_Types };
extern const Il2CppType ProductDefinition_t1942475268_0_0_0;
static const Il2CppType* GenInst_ProductDefinition_t1942475268_0_0_0_Types[] = { &ProductDefinition_t1942475268_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductDefinition_t1942475268_0_0_0 = { 1, GenInst_ProductDefinition_t1942475268_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType IStoreConfiguration_t2978822016_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0_Types[] = { &Type_t_0_0_0, &IStoreConfiguration_t2978822016_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0 = { 2, GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IStoreConfiguration_t2978822016_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IStoreConfiguration_t2978822016_0_0_0_Types[] = { &IStoreConfiguration_t2978822016_0_0_0 };
extern const Il2CppGenericInst GenInst_IStoreConfiguration_t2978822016_0_0_0 = { 1, GenInst_IStoreConfiguration_t2978822016_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2673525135_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2673525135_0_0_0_Types[] = { &KeyValuePair_2_t2673525135_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2673525135_0_0_0 = { 1, GenInst_KeyValuePair_2_t2673525135_0_0_0_Types };
extern const Il2CppType IPurchasingModule_t4085676839_0_0_0;
static const Il2CppType* GenInst_IPurchasingModule_t4085676839_0_0_0_Types[] = { &IPurchasingModule_t4085676839_0_0_0 };
extern const Il2CppGenericInst GenInst_IPurchasingModule_t4085676839_0_0_0 = { 1, GenInst_IPurchasingModule_t4085676839_0_0_0_Types };
extern const Il2CppType Product_t1203687971_0_0_0;
static const Il2CppType* GenInst_Product_t1203687971_0_0_0_Types[] = { &Product_t1203687971_0_0_0 };
extern const Il2CppGenericInst GenInst_Product_t1203687971_0_0_0 = { 1, GenInst_Product_t1203687971_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Product_t1203687971_0_0_0_Types[] = { &String_t_0_0_0, &Product_t1203687971_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Product_t1203687971_0_0_0 = { 2, GenInst_String_t_0_0_0_Product_t1203687971_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Product_t1203687971_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Product_t1203687971_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Product_t1203687971_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Product_t1203687971_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t875812455_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t875812455_0_0_0_Types[] = { &KeyValuePair_2_t875812455_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t875812455_0_0_0 = { 1, GenInst_KeyValuePair_2_t875812455_0_0_0_Types };
static const Il2CppType* GenInst_Product_t1203687971_0_0_0_String_t_0_0_0_Types[] = { &Product_t1203687971_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Product_t1203687971_0_0_0_String_t_0_0_0 = { 2, GenInst_Product_t1203687971_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType IStoreExtension_t1396898229_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0_Types[] = { &Type_t_0_0_0, &IStoreExtension_t1396898229_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0 = { 2, GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IStoreExtension_t1396898229_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IStoreExtension_t1396898229_0_0_0_Types[] = { &IStoreExtension_t1396898229_0_0_0 };
extern const Il2CppGenericInst GenInst_IStoreExtension_t1396898229_0_0_0 = { 1, GenInst_IStoreExtension_t1396898229_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1091601348_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1091601348_0_0_0_Types[] = { &KeyValuePair_2_t1091601348_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1091601348_0_0_0 = { 1, GenInst_KeyValuePair_2_t1091601348_0_0_0_Types };
extern const Il2CppType InitializationFailureReason_t2954032642_0_0_0;
static const Il2CppType* GenInst_InitializationFailureReason_t2954032642_0_0_0_Types[] = { &InitializationFailureReason_t2954032642_0_0_0 };
extern const Il2CppGenericInst GenInst_InitializationFailureReason_t2954032642_0_0_0 = { 1, GenInst_InitializationFailureReason_t2954032642_0_0_0_Types };
static const Il2CppType* GenInst_ProductDefinition_t1942475268_0_0_0_Product_t1203687971_0_0_0_Types[] = { &ProductDefinition_t1942475268_0_0_0, &Product_t1203687971_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductDefinition_t1942475268_0_0_0_Product_t1203687971_0_0_0 = { 2, GenInst_ProductDefinition_t1942475268_0_0_0_Product_t1203687971_0_0_0_Types };
extern const Il2CppType ProductDescription_t3318267523_0_0_0;
static const Il2CppType* GenInst_ProductDescription_t3318267523_0_0_0_Types[] = { &ProductDescription_t3318267523_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductDescription_t3318267523_0_0_0 = { 1, GenInst_ProductDescription_t3318267523_0_0_0_Types };
extern const Il2CppType BaseInputModule_t1295781545_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t1295781545_0_0_0_Types[] = { &BaseInputModule_t1295781545_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t1295781545_0_0_0 = { 1, GenInst_BaseInputModule_t1295781545_0_0_0_Types };
extern const Il2CppType RaycastResult_t21186376_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0 = { 1, GenInst_RaycastResult_t21186376_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t3182198310_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t3182198310_0_0_0_Types[] = { &IDeselectHandler_t3182198310_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t3182198310_0_0_0 = { 1, GenInst_IDeselectHandler_t3182198310_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t2741188318_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t2741188318_0_0_0_Types[] = { &IEventSystemHandler_t2741188318_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2741188318_0_0_0 = { 1, GenInst_IEventSystemHandler_t2741188318_0_0_0_Types };
extern const Il2CppType List_1_t2110309450_0_0_0;
static const Il2CppType* GenInst_List_1_t2110309450_0_0_0_Types[] = { &List_1_t2110309450_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2110309450_0_0_0 = { 1, GenInst_List_1_t2110309450_0_0_0_Types };
extern const Il2CppType List_1_t3188497603_0_0_0;
static const Il2CppType* GenInst_List_1_t3188497603_0_0_0_Types[] = { &List_1_t3188497603_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3188497603_0_0_0 = { 1, GenInst_List_1_t3188497603_0_0_0_Types };
extern const Il2CppType ISelectHandler_t2812555161_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t2812555161_0_0_0_Types[] = { &ISelectHandler_t2812555161_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t2812555161_0_0_0 = { 1, GenInst_ISelectHandler_t2812555161_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t2336171397_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t2336171397_0_0_0_Types[] = { &BaseRaycaster_t2336171397_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2336171397_0_0_0 = { 1, GenInst_BaseRaycaster_t2336171397_0_0_0_Types };
extern const Il2CppType Entry_t3365010046_0_0_0;
static const Il2CppType* GenInst_Entry_t3365010046_0_0_0_Types[] = { &Entry_t3365010046_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t3365010046_0_0_0 = { 1, GenInst_Entry_t3365010046_0_0_0_Types };
extern const Il2CppType BaseEventData_t2681005625_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t2681005625_0_0_0_Types[] = { &BaseEventData_t2681005625_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t2681005625_0_0_0 = { 1, GenInst_BaseEventData_t2681005625_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t193164956_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t193164956_0_0_0_Types[] = { &IPointerEnterHandler_t193164956_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t193164956_0_0_0 = { 1, GenInst_IPointerEnterHandler_t193164956_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t461019860_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t461019860_0_0_0_Types[] = { &IPointerExitHandler_t461019860_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t461019860_0_0_0 = { 1, GenInst_IPointerExitHandler_t461019860_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t3929046918_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t3929046918_0_0_0_Types[] = { &IPointerDownHandler_t3929046918_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t3929046918_0_0_0 = { 1, GenInst_IPointerDownHandler_t3929046918_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t1847764461_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t1847764461_0_0_0_Types[] = { &IPointerUpHandler_t1847764461_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t1847764461_0_0_0 = { 1, GenInst_IPointerUpHandler_t1847764461_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t96169666_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t96169666_0_0_0_Types[] = { &IPointerClickHandler_t96169666_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t96169666_0_0_0 = { 1, GenInst_IPointerClickHandler_t96169666_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t3350809087_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types[] = { &IInitializePotentialDragHandler_t3350809087_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t3135127860_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t3135127860_0_0_0_Types[] = { &IBeginDragHandler_t3135127860_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t3135127860_0_0_0 = { 1, GenInst_IBeginDragHandler_t3135127860_0_0_0_Types };
extern const Il2CppType IDragHandler_t2583993319_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t2583993319_0_0_0_Types[] = { &IDragHandler_t2583993319_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t2583993319_0_0_0 = { 1, GenInst_IDragHandler_t2583993319_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t1349123600_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t1349123600_0_0_0_Types[] = { &IEndDragHandler_t1349123600_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t1349123600_0_0_0 = { 1, GenInst_IEndDragHandler_t1349123600_0_0_0_Types };
extern const Il2CppType IDropHandler_t2390101210_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t2390101210_0_0_0_Types[] = { &IDropHandler_t2390101210_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t2390101210_0_0_0 = { 1, GenInst_IDropHandler_t2390101210_0_0_0_Types };
extern const Il2CppType IScrollHandler_t3834677510_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t3834677510_0_0_0_Types[] = { &IScrollHandler_t3834677510_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t3834677510_0_0_0 = { 1, GenInst_IScrollHandler_t3834677510_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t3778909353_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types[] = { &IUpdateSelectedHandler_t3778909353_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t3778909353_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types };
extern const Il2CppType IMoveHandler_t2611925506_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t2611925506_0_0_0_Types[] = { &IMoveHandler_t2611925506_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t2611925506_0_0_0 = { 1, GenInst_IMoveHandler_t2611925506_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t525803901_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t525803901_0_0_0_Types[] = { &ISubmitHandler_t525803901_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t525803901_0_0_0 = { 1, GenInst_ISubmitHandler_t525803901_0_0_0_Types };
extern const Il2CppType ICancelHandler_t1980319651_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t1980319651_0_0_0_Types[] = { &ICancelHandler_t1980319651_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t1980319651_0_0_0 = { 1, GenInst_ICancelHandler_t1980319651_0_0_0_Types };
extern const Il2CppType Transform_t3275118058_0_0_0;
static const Il2CppType* GenInst_Transform_t3275118058_0_0_0_Types[] = { &Transform_t3275118058_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0 = { 1, GenInst_Transform_t3275118058_0_0_0_Types };
extern const Il2CppType GameObject_t1756533147_0_0_0;
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0 = { 1, GenInst_GameObject_t1756533147_0_0_0_Types };
extern const Il2CppType PointerEventData_t1599784723_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_PointerEventData_t1599784723_0_0_0_Types[] = { &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t1599784723_0_0_0 = { 1, GenInst_PointerEventData_t1599784723_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2659922876_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2659922876_0_0_0_Types[] = { &KeyValuePair_2_t2659922876_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2659922876_0_0_0 = { 1, GenInst_KeyValuePair_2_t2659922876_0_0_0_Types };
extern const Il2CppType ButtonState_t2688375492_0_0_0;
static const Il2CppType* GenInst_ButtonState_t2688375492_0_0_0_Types[] = { &ButtonState_t2688375492_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t2688375492_0_0_0 = { 1, GenInst_ButtonState_t2688375492_0_0_0_Types };
extern const Il2CppType ICanvasElement_t986520779_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0 = { 1, GenInst_ICanvasElement_t986520779_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ColorBlock_t2652774230_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t2652774230_0_0_0_Types[] = { &ColorBlock_t2652774230_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t2652774230_0_0_0 = { 1, GenInst_ColorBlock_t2652774230_0_0_0_Types };
extern const Il2CppType OptionData_t2420267500_0_0_0;
static const Il2CppType* GenInst_OptionData_t2420267500_0_0_0_Types[] = { &OptionData_t2420267500_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t2420267500_0_0_0 = { 1, GenInst_OptionData_t2420267500_0_0_0_Types };
extern const Il2CppType DropdownItem_t4139978805_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t4139978805_0_0_0_Types[] = { &DropdownItem_t4139978805_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t4139978805_0_0_0 = { 1, GenInst_DropdownItem_t4139978805_0_0_0_Types };
extern const Il2CppType FloatTween_t2986189219_0_0_0;
static const Il2CppType* GenInst_FloatTween_t2986189219_0_0_0_Types[] = { &FloatTween_t2986189219_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t2986189219_0_0_0 = { 1, GenInst_FloatTween_t2986189219_0_0_0_Types };
extern const Il2CppType Sprite_t309593783_0_0_0;
static const Il2CppType* GenInst_Sprite_t309593783_0_0_0_Types[] = { &Sprite_t309593783_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t309593783_0_0_0 = { 1, GenInst_Sprite_t309593783_0_0_0_Types };
extern const Il2CppType Canvas_t209405766_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_Types[] = { &Canvas_t209405766_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0 = { 1, GenInst_Canvas_t209405766_0_0_0_Types };
extern const Il2CppType List_1_t3873494194_0_0_0;
static const Il2CppType* GenInst_List_1_t3873494194_0_0_0_Types[] = { &List_1_t3873494194_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3873494194_0_0_0 = { 1, GenInst_List_1_t3873494194_0_0_0_Types };
extern const Il2CppType List_1_t4020309861_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_List_1_t4020309861_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &List_1_t4020309861_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_List_1_t4020309861_0_0_0 = { 2, GenInst_Font_t4239498691_0_0_0_List_1_t4020309861_0_0_0_Types };
extern const Il2CppType Text_t356221433_0_0_0;
static const Il2CppType* GenInst_Text_t356221433_0_0_0_Types[] = { &Text_t356221433_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t356221433_0_0_0 = { 1, GenInst_Text_t356221433_0_0_0_Types };
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_List_1_t4020309861_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &List_1_t4020309861_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_List_1_t4020309861_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_List_1_t4020309861_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t4020309861_0_0_0_Types[] = { &List_1_t4020309861_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t4020309861_0_0_0 = { 1, GenInst_List_1_t4020309861_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1885773127_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1885773127_0_0_0_Types[] = { &KeyValuePair_2_t1885773127_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1885773127_0_0_0 = { 1, GenInst_KeyValuePair_2_t1885773127_0_0_0_Types };
extern const Il2CppType ColorTween_t3438117476_0_0_0;
static const Il2CppType* GenInst_ColorTween_t3438117476_0_0_0_Types[] = { &ColorTween_t3438117476_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t3438117476_0_0_0 = { 1, GenInst_ColorTween_t3438117476_0_0_0_Types };
extern const Il2CppType Graphic_t2426225576_0_0_0;
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0 = { 1, GenInst_Graphic_t2426225576_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t286373651_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0 = { 2, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t286373651_0_0_0_Types[] = { &IndexedSet_1_t286373651_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t286373651_0_0_0 = { 1, GenInst_IndexedSet_1_t286373651_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2391682566_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2391682566_0_0_0_Types[] = { &KeyValuePair_2_t2391682566_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2391682566_0_0_0 = { 1, GenInst_KeyValuePair_2_t2391682566_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3010968081_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3010968081_0_0_0_Types[] = { &KeyValuePair_2_t3010968081_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3010968081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3010968081_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1912381698_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1912381698_0_0_0_Types[] = { &KeyValuePair_2_t1912381698_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1912381698_0_0_0 = { 1, GenInst_KeyValuePair_2_t1912381698_0_0_0_Types };
extern const Il2CppType ContentType_t1028629049_0_0_0;
static const Il2CppType* GenInst_ContentType_t1028629049_0_0_0_Types[] = { &ContentType_t1028629049_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t1028629049_0_0_0 = { 1, GenInst_ContentType_t1028629049_0_0_0_Types };
extern const Il2CppType Mask_t2977958238_0_0_0;
static const Il2CppType* GenInst_Mask_t2977958238_0_0_0_Types[] = { &Mask_t2977958238_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t2977958238_0_0_0 = { 1, GenInst_Mask_t2977958238_0_0_0_Types };
extern const Il2CppType List_1_t2347079370_0_0_0;
static const Il2CppType* GenInst_List_1_t2347079370_0_0_0_Types[] = { &List_1_t2347079370_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2347079370_0_0_0 = { 1, GenInst_List_1_t2347079370_0_0_0_Types };
extern const Il2CppType RectMask2D_t1156185964_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t1156185964_0_0_0_Types[] = { &RectMask2D_t1156185964_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t1156185964_0_0_0 = { 1, GenInst_RectMask2D_t1156185964_0_0_0_Types };
extern const Il2CppType List_1_t525307096_0_0_0;
static const Il2CppType* GenInst_List_1_t525307096_0_0_0_Types[] = { &List_1_t525307096_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t525307096_0_0_0 = { 1, GenInst_List_1_t525307096_0_0_0_Types };
extern const Il2CppType Navigation_t1571958496_0_0_0;
static const Il2CppType* GenInst_Navigation_t1571958496_0_0_0_Types[] = { &Navigation_t1571958496_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t1571958496_0_0_0 = { 1, GenInst_Navigation_t1571958496_0_0_0_Types };
extern const Il2CppType IClippable_t1941276057_0_0_0;
static const Il2CppType* GenInst_IClippable_t1941276057_0_0_0_Types[] = { &IClippable_t1941276057_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t1941276057_0_0_0 = { 1, GenInst_IClippable_t1941276057_0_0_0_Types };
extern const Il2CppType Selectable_t1490392188_0_0_0;
static const Il2CppType* GenInst_Selectable_t1490392188_0_0_0_Types[] = { &Selectable_t1490392188_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t1490392188_0_0_0 = { 1, GenInst_Selectable_t1490392188_0_0_0_Types };
extern const Il2CppType SpriteState_t1353336012_0_0_0;
static const Il2CppType* GenInst_SpriteState_t1353336012_0_0_0_Types[] = { &SpriteState_t1353336012_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t1353336012_0_0_0 = { 1, GenInst_SpriteState_t1353336012_0_0_0_Types };
extern const Il2CppType CanvasGroup_t3296560743_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t3296560743_0_0_0_Types[] = { &CanvasGroup_t3296560743_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t3296560743_0_0_0 = { 1, GenInst_CanvasGroup_t3296560743_0_0_0_Types };
extern const Il2CppType MatEntry_t3157325053_0_0_0;
static const Il2CppType* GenInst_MatEntry_t3157325053_0_0_0_Types[] = { &MatEntry_t3157325053_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t3157325053_0_0_0 = { 1, GenInst_MatEntry_t3157325053_0_0_0_Types };
extern const Il2CppType Toggle_t3976754468_0_0_0;
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0 = { 1, GenInst_Toggle_t3976754468_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IClipper_t900477982_0_0_0;
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Types[] = { &IClipper_t900477982_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0 = { 1, GenInst_IClipper_t900477982_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t379984643_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t379984643_0_0_0_Types[] = { &KeyValuePair_2_t379984643_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t379984643_0_0_0 = { 1, GenInst_KeyValuePair_2_t379984643_0_0_0_Types };
extern const Il2CppType RectTransform_t3349966182_0_0_0;
static const Il2CppType* GenInst_RectTransform_t3349966182_0_0_0_Types[] = { &RectTransform_t3349966182_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t3349966182_0_0_0 = { 1, GenInst_RectTransform_t3349966182_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t2155218138_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t2155218138_0_0_0_Types[] = { &LayoutRebuilder_t2155218138_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t2155218138_0_0_0 = { 1, GenInst_LayoutRebuilder_t2155218138_0_0_0_Types };
extern const Il2CppType ILayoutElement_t1975293769_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types[] = { &ILayoutElement_t1975293769_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType List_1_t1612828712_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828712_0_0_0_Types[] = { &List_1_t1612828712_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828712_0_0_0 = { 1, GenInst_List_1_t1612828712_0_0_0_Types };
extern const Il2CppType List_1_t243638650_0_0_0;
static const Il2CppType* GenInst_List_1_t243638650_0_0_0_Types[] = { &List_1_t243638650_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t243638650_0_0_0 = { 1, GenInst_List_1_t243638650_0_0_0_Types };
extern const Il2CppType List_1_t1612828711_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828711_0_0_0_Types[] = { &List_1_t1612828711_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828711_0_0_0 = { 1, GenInst_List_1_t1612828711_0_0_0_Types };
extern const Il2CppType List_1_t1612828713_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828713_0_0_0_Types[] = { &List_1_t1612828713_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828713_0_0_0 = { 1, GenInst_List_1_t1612828713_0_0_0_Types };
extern const Il2CppType List_1_t1440998580_0_0_0;
static const Il2CppType* GenInst_List_1_t1440998580_0_0_0_Types[] = { &List_1_t1440998580_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1440998580_0_0_0 = { 1, GenInst_List_1_t1440998580_0_0_0_Types };
extern const Il2CppType List_1_t573379950_0_0_0;
static const Il2CppType* GenInst_List_1_t573379950_0_0_0_Types[] = { &List_1_t573379950_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t573379950_0_0_0 = { 1, GenInst_List_1_t573379950_0_0_0_Types };
extern const Il2CppType WinProductDescription_t1075111405_0_0_0;
static const Il2CppType* GenInst_WinProductDescription_t1075111405_0_0_0_Types[] = { &WinProductDescription_t1075111405_0_0_0 };
extern const Il2CppGenericInst GenInst_WinProductDescription_t1075111405_0_0_0 = { 1, GenInst_WinProductDescription_t1075111405_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_InitializationFailureReason_t2954032642_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &InitializationFailureReason_t2954032642_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_InitializationFailureReason_t2954032642_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_InitializationFailureReason_t2954032642_0_0_0_Types };
extern const Il2CppType PurchaseFailureReason_t1322959839_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &PurchaseFailureReason_t1322959839_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_PurchaseFailureReason_t1322959839_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType AndroidStore_t3203055206_0_0_0;
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0 = { 2, GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t566713506_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t566713506_0_0_0_Types[] = { &KeyValuePair_2_t566713506_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t566713506_0_0_0 = { 1, GenInst_KeyValuePair_2_t566713506_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0 = { 1, GenInst_AndroidStore_t3203055206_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_AndroidStore_t3203055206_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &Il2CppObject_0_0_0, &AndroidStore_t3203055206_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_AndroidStore_t3203055206_0_0_0 = { 3, GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_AndroidStore_t3203055206_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t566713506_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t566713506_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t566713506_0_0_0 = { 3, GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t566713506_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4201451740_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4201451740_0_0_0_Types[] = { &KeyValuePair_2_t4201451740_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4201451740_0_0_0 = { 1, GenInst_KeyValuePair_2_t4201451740_0_0_0_Types };
static const Il2CppType* GenInst_ProductDefinition_t1942475268_0_0_0_String_t_0_0_0_Types[] = { &ProductDefinition_t1942475268_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductDefinition_t1942475268_0_0_0_String_t_0_0_0 = { 2, GenInst_ProductDefinition_t1942475268_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Action_t3226471752_0_0_0;
static const Il2CppType* GenInst_Action_t3226471752_0_0_0_Types[] = { &Action_t3226471752_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t3226471752_0_0_0 = { 1, GenInst_Action_t3226471752_0_0_0_Types };
extern const Il2CppType RuntimePlatform_t1869584967_0_0_0;
static const Il2CppType* GenInst_RuntimePlatform_t1869584967_0_0_0_Types[] = { &RuntimePlatform_t1869584967_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimePlatform_t1869584967_0_0_0 = { 1, GenInst_RuntimePlatform_t1869584967_0_0_0_Types };
extern const Il2CppType Action_1_t3627374100_0_0_0;
static const Il2CppType* GenInst_Action_1_t3627374100_0_0_0_Types[] = { &Action_1_t3627374100_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3627374100_0_0_0 = { 1, GenInst_Action_1_t3627374100_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t1158329972_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t1158329972_0_0_0_Types[] = { &MonoBehaviour_t1158329972_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t1158329972_0_0_0 = { 1, GenInst_MonoBehaviour_t1158329972_0_0_0_Types };
static const Il2CppType* GenInst_ProductDefinition_t1942475268_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &ProductDefinition_t1942475268_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductDefinition_t1942475268_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_ProductDefinition_t1942475268_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_ProductDefinition_t1942475268_0_0_0_WinProductDescription_t1075111405_0_0_0_Types[] = { &ProductDefinition_t1942475268_0_0_0, &WinProductDescription_t1075111405_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductDefinition_t1942475268_0_0_0_WinProductDescription_t1075111405_0_0_0 = { 2, GenInst_ProductDefinition_t1942475268_0_0_0_WinProductDescription_t1075111405_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3089358386_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3089358386_0_0_0_Types[] = { &KeyValuePair_2_t3089358386_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3089358386_0_0_0 = { 1, GenInst_KeyValuePair_2_t3089358386_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1430411454_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1430411454_0_0_0_Types[] = { &KeyValuePair_2_t1430411454_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1430411454_0_0_0 = { 1, GenInst_KeyValuePair_2_t1430411454_0_0_0_Types };
extern const Il2CppType DTDNode_t1758286970_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0_Types[] = { &String_t_0_0_0, &DTDNode_t1758286970_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0 = { 2, GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0_Types };
static const Il2CppType* GenInst_DTDNode_t1758286970_0_0_0_Types[] = { &DTDNode_t1758286970_0_0_0 };
extern const Il2CppGenericInst GenInst_DTDNode_t1758286970_0_0_0 = { 1, GenInst_DTDNode_t1758286970_0_0_0_Types };
extern const Il2CppType Entry_t2583369454_0_0_0;
static const Il2CppType* GenInst_Entry_t2583369454_0_0_0_Types[] = { &Entry_t2583369454_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t2583369454_0_0_0 = { 1, GenInst_Entry_t2583369454_0_0_0_Types };
extern const Il2CppType XmlNode_t616554813_0_0_0;
static const Il2CppType* GenInst_XmlNode_t616554813_0_0_0_Types[] = { &XmlNode_t616554813_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNode_t616554813_0_0_0 = { 1, GenInst_XmlNode_t616554813_0_0_0_Types };
extern const Il2CppType IXPathNavigable_t845515791_0_0_0;
static const Il2CppType* GenInst_IXPathNavigable_t845515791_0_0_0_Types[] = { &IXPathNavigable_t845515791_0_0_0 };
extern const Il2CppGenericInst GenInst_IXPathNavigable_t845515791_0_0_0 = { 1, GenInst_IXPathNavigable_t845515791_0_0_0_Types };
extern const Il2CppType NsDecl_t3210081295_0_0_0;
static const Il2CppType* GenInst_NsDecl_t3210081295_0_0_0_Types[] = { &NsDecl_t3210081295_0_0_0 };
extern const Il2CppGenericInst GenInst_NsDecl_t3210081295_0_0_0 = { 1, GenInst_NsDecl_t3210081295_0_0_0_Types };
extern const Il2CppType NsScope_t2513625351_0_0_0;
static const Il2CppType* GenInst_NsScope_t2513625351_0_0_0_Types[] = { &NsScope_t2513625351_0_0_0 };
extern const Il2CppGenericInst GenInst_NsScope_t2513625351_0_0_0 = { 1, GenInst_NsScope_t2513625351_0_0_0_Types };
extern const Il2CppType XmlAttributeTokenInfo_t3353594030_0_0_0;
static const Il2CppType* GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0_Types[] = { &XmlAttributeTokenInfo_t3353594030_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0 = { 1, GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0_Types };
extern const Il2CppType XmlTokenInfo_t254587324_0_0_0;
static const Il2CppType* GenInst_XmlTokenInfo_t254587324_0_0_0_Types[] = { &XmlTokenInfo_t254587324_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTokenInfo_t254587324_0_0_0 = { 1, GenInst_XmlTokenInfo_t254587324_0_0_0_Types };
extern const Il2CppType TagName_t2340974457_0_0_0;
static const Il2CppType* GenInst_TagName_t2340974457_0_0_0_Types[] = { &TagName_t2340974457_0_0_0 };
extern const Il2CppGenericInst GenInst_TagName_t2340974457_0_0_0 = { 1, GenInst_TagName_t2340974457_0_0_0_Types };
extern const Il2CppType XmlNodeInfo_t3709371029_0_0_0;
static const Il2CppType* GenInst_XmlNodeInfo_t3709371029_0_0_0_Types[] = { &XmlNodeInfo_t3709371029_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNodeInfo_t3709371029_0_0_0 = { 1, GenInst_XmlNodeInfo_t3709371029_0_0_0_Types };
extern const Il2CppType Framework_t4022948262_0_0_0;
static const Il2CppType* GenInst_Framework_t4022948262_0_0_0_Types[] = { &Framework_t4022948262_0_0_0 };
extern const Il2CppGenericInst GenInst_Framework_t4022948262_0_0_0 = { 1, GenInst_Framework_t4022948262_0_0_0_Types };
extern const Il2CppType Lib_t360384209_0_0_0;
static const Il2CppType* GenInst_Lib_t360384209_0_0_0_Types[] = { &Lib_t360384209_0_0_0 };
extern const Il2CppGenericInst GenInst_Lib_t360384209_0_0_0 = { 1, GenInst_Lib_t360384209_0_0_0_Types };
extern const Il2CppType Variable_t1157765046_0_0_0;
static const Il2CppType* GenInst_Variable_t1157765046_0_0_0_Types[] = { &Variable_t1157765046_0_0_0 };
extern const Il2CppGenericInst GenInst_Variable_t1157765046_0_0_0 = { 1, GenInst_Variable_t1157765046_0_0_0_Types };
extern const Il2CppType VariableListed_t1912381035_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_VariableListed_t1912381035_0_0_0_Types[] = { &String_t_0_0_0, &VariableListed_t1912381035_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VariableListed_t1912381035_0_0_0 = { 2, GenInst_String_t_0_0_0_VariableListed_t1912381035_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VariableListed_t1912381035_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &VariableListed_t1912381035_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VariableListed_t1912381035_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_VariableListed_t1912381035_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_VariableListed_t1912381035_0_0_0_Types[] = { &VariableListed_t1912381035_0_0_0 };
extern const Il2CppGenericInst GenInst_VariableListed_t1912381035_0_0_0 = { 1, GenInst_VariableListed_t1912381035_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1584505519_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1584505519_0_0_0_Types[] = { &KeyValuePair_2_t1584505519_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1584505519_0_0_0 = { 1, GenInst_KeyValuePair_2_t1584505519_0_0_0_Types };
extern const Il2CppType ISN_SoomlaGrow_t2919772303_0_0_0;
static const Il2CppType* GenInst_ISN_SoomlaGrow_t2919772303_0_0_0_Types[] = { &ISN_SoomlaGrow_t2919772303_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_SoomlaGrow_t2919772303_0_0_0 = { 1, GenInst_ISN_SoomlaGrow_t2919772303_0_0_0_Types };
extern const Il2CppType IOSInAppPurchaseManager_t644626385_0_0_0;
static const Il2CppType* GenInst_IOSInAppPurchaseManager_t644626385_0_0_0_Types[] = { &IOSInAppPurchaseManager_t644626385_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSInAppPurchaseManager_t644626385_0_0_0 = { 1, GenInst_IOSInAppPurchaseManager_t644626385_0_0_0_Types };
extern const Il2CppType IOSProductTemplate_t1036598382_0_0_0;
static const Il2CppType* GenInst_IOSProductTemplate_t1036598382_0_0_0_Types[] = { &IOSProductTemplate_t1036598382_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSProductTemplate_t1036598382_0_0_0 = { 1, GenInst_IOSProductTemplate_t1036598382_0_0_0_Types };
extern const Il2CppType GK_Leaderboard_t156446466_0_0_0;
static const Il2CppType* GenInst_GK_Leaderboard_t156446466_0_0_0_Types[] = { &GK_Leaderboard_t156446466_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_Leaderboard_t156446466_0_0_0 = { 1, GenInst_GK_Leaderboard_t156446466_0_0_0_Types };
extern const Il2CppType GK_AchievementTemplate_t2296152240_0_0_0;
static const Il2CppType* GenInst_GK_AchievementTemplate_t2296152240_0_0_0_Types[] = { &GK_AchievementTemplate_t2296152240_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_AchievementTemplate_t2296152240_0_0_0 = { 1, GenInst_GK_AchievementTemplate_t2296152240_0_0_0_Types };
extern const Il2CppType GameCenterInvitations_t2643374653_0_0_0;
static const Il2CppType* GenInst_GameCenterInvitations_t2643374653_0_0_0_Types[] = { &GameCenterInvitations_t2643374653_0_0_0 };
extern const Il2CppGenericInst GenInst_GameCenterInvitations_t2643374653_0_0_0 = { 1, GenInst_GameCenterInvitations_t2643374653_0_0_0_Types };
extern const Il2CppType GK_Player_t2782008294_0_0_0;
extern const Il2CppType GK_InviteRecipientResponse_t3438857802_0_0_0;
static const Il2CppType* GenInst_GK_Player_t2782008294_0_0_0_GK_InviteRecipientResponse_t3438857802_0_0_0_Types[] = { &GK_Player_t2782008294_0_0_0, &GK_InviteRecipientResponse_t3438857802_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_Player_t2782008294_0_0_0_GK_InviteRecipientResponse_t3438857802_0_0_0 = { 2, GenInst_GK_Player_t2782008294_0_0_0_GK_InviteRecipientResponse_t3438857802_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_GK_InviteRecipientResponse_t3438857802_0_0_0_Types[] = { &Il2CppObject_0_0_0, &GK_InviteRecipientResponse_t3438857802_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_GK_InviteRecipientResponse_t3438857802_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_GK_InviteRecipientResponse_t3438857802_0_0_0_Types };
extern const Il2CppType GK_MatchType_t1493351924_0_0_0;
extern const Il2CppType GK_Invite_t22070530_0_0_0;
static const Il2CppType* GenInst_GK_MatchType_t1493351924_0_0_0_GK_Invite_t22070530_0_0_0_Types[] = { &GK_MatchType_t1493351924_0_0_0, &GK_Invite_t22070530_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_MatchType_t1493351924_0_0_0_GK_Invite_t22070530_0_0_0 = { 2, GenInst_GK_MatchType_t1493351924_0_0_0_GK_Invite_t22070530_0_0_0_Types };
static const Il2CppType* GenInst_GK_MatchType_t1493351924_0_0_0_Il2CppObject_0_0_0_Types[] = { &GK_MatchType_t1493351924_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_MatchType_t1493351924_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_GK_MatchType_t1493351924_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType StringU5BU5D_t1642385972_0_0_0;
extern const Il2CppType GK_PlayerU5BU5D_t1642762691_0_0_0;
static const Il2CppType* GenInst_GK_MatchType_t1493351924_0_0_0_StringU5BU5D_t1642385972_0_0_0_GK_PlayerU5BU5D_t1642762691_0_0_0_Types[] = { &GK_MatchType_t1493351924_0_0_0, &StringU5BU5D_t1642385972_0_0_0, &GK_PlayerU5BU5D_t1642762691_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_MatchType_t1493351924_0_0_0_StringU5BU5D_t1642385972_0_0_0_GK_PlayerU5BU5D_t1642762691_0_0_0 = { 3, GenInst_GK_MatchType_t1493351924_0_0_0_StringU5BU5D_t1642385972_0_0_0_GK_PlayerU5BU5D_t1642762691_0_0_0_Types };
static const Il2CppType* GenInst_GK_MatchType_t1493351924_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &GK_MatchType_t1493351924_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_MatchType_t1493351924_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_GK_MatchType_t1493351924_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_GK_Player_t2782008294_0_0_0_Types[] = { &GK_Player_t2782008294_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_Player_t2782008294_0_0_0 = { 1, GenInst_GK_Player_t2782008294_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GK_Player_t2782008294_0_0_0_Types[] = { &String_t_0_0_0, &GK_Player_t2782008294_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GK_Player_t2782008294_0_0_0 = { 2, GenInst_String_t_0_0_0_GK_Player_t2782008294_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GK_Player_t2782008294_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GK_Player_t2782008294_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GK_Player_t2782008294_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GK_Player_t2782008294_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2454132778_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2454132778_0_0_0_Types[] = { &KeyValuePair_2_t2454132778_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2454132778_0_0_0 = { 1, GenInst_KeyValuePair_2_t2454132778_0_0_0_Types };
extern const Il2CppType GK_LeaderboardSet_t5314098_0_0_0;
static const Il2CppType* GenInst_GK_LeaderboardSet_t5314098_0_0_0_Types[] = { &GK_LeaderboardSet_t5314098_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_LeaderboardSet_t5314098_0_0_0 = { 1, GenInst_GK_LeaderboardSet_t5314098_0_0_0_Types };
extern const Il2CppType GK_FriendRequest_t4101620808_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GK_FriendRequest_t4101620808_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GK_FriendRequest_t4101620808_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GK_FriendRequest_t4101620808_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_GK_FriendRequest_t4101620808_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GK_FriendRequest_t4101620808_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GK_FriendRequest_t4101620808_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GK_FriendRequest_t4101620808_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_GK_FriendRequest_t4101620808_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_GK_FriendRequest_t4101620808_0_0_0_Types[] = { &GK_FriendRequest_t4101620808_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_FriendRequest_t4101620808_0_0_0 = { 1, GenInst_GK_FriendRequest_t4101620808_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t866791665_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t866791665_0_0_0_Types[] = { &KeyValuePair_2_t866791665_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t866791665_0_0_0 = { 1, GenInst_KeyValuePair_2_t866791665_0_0_0_Types };
extern const Il2CppType Result_t4287219743_0_0_0;
static const Il2CppType* GenInst_Result_t4287219743_0_0_0_Types[] = { &Result_t4287219743_0_0_0 };
extern const Il2CppGenericInst GenInst_Result_t4287219743_0_0_0 = { 1, GenInst_Result_t4287219743_0_0_0_Types };
extern const Il2CppType GK_LeaderboardResult_t866080833_0_0_0;
static const Il2CppType* GenInst_GK_LeaderboardResult_t866080833_0_0_0_Types[] = { &GK_LeaderboardResult_t866080833_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_LeaderboardResult_t866080833_0_0_0 = { 1, GenInst_GK_LeaderboardResult_t866080833_0_0_0_Types };
extern const Il2CppType GK_AchievementProgressResult_t3539574352_0_0_0;
static const Il2CppType* GenInst_GK_AchievementProgressResult_t3539574352_0_0_0_Types[] = { &GK_AchievementProgressResult_t3539574352_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_AchievementProgressResult_t3539574352_0_0_0 = { 1, GenInst_GK_AchievementProgressResult_t3539574352_0_0_0_Types };
extern const Il2CppType GK_UserInfoLoadResult_t1177841233_0_0_0;
static const Il2CppType* GenInst_GK_UserInfoLoadResult_t1177841233_0_0_0_Types[] = { &GK_UserInfoLoadResult_t1177841233_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_UserInfoLoadResult_t1177841233_0_0_0 = { 1, GenInst_GK_UserInfoLoadResult_t1177841233_0_0_0_Types };
extern const Il2CppType GK_PlayerSignatureResult_t13769479_0_0_0;
static const Il2CppType* GenInst_GK_PlayerSignatureResult_t13769479_0_0_0_Types[] = { &GK_PlayerSignatureResult_t13769479_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_PlayerSignatureResult_t13769479_0_0_0 = { 1, GenInst_GK_PlayerSignatureResult_t13769479_0_0_0_Types };
extern const Il2CppType GK_TBM_Participant_t3803955090_0_0_0;
static const Il2CppType* GenInst_GK_TBM_Participant_t3803955090_0_0_0_Types[] = { &GK_TBM_Participant_t3803955090_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_TBM_Participant_t3803955090_0_0_0 = { 1, GenInst_GK_TBM_Participant_t3803955090_0_0_0_Types };
extern const Il2CppType GameCenter_RTM_t849630631_0_0_0;
static const Il2CppType* GenInst_GameCenter_RTM_t849630631_0_0_0_Types[] = { &GameCenter_RTM_t849630631_0_0_0 };
extern const Il2CppGenericInst GenInst_GameCenter_RTM_t849630631_0_0_0 = { 1, GenInst_GameCenter_RTM_t849630631_0_0_0_Types };
extern const Il2CppType GK_RTM_MatchStartedResult_t833698690_0_0_0;
static const Il2CppType* GenInst_GK_RTM_MatchStartedResult_t833698690_0_0_0_Types[] = { &GK_RTM_MatchStartedResult_t833698690_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_RTM_MatchStartedResult_t833698690_0_0_0 = { 1, GenInst_GK_RTM_MatchStartedResult_t833698690_0_0_0_Types };
extern const Il2CppType Error_t445207774_0_0_0;
static const Il2CppType* GenInst_Error_t445207774_0_0_0_Types[] = { &Error_t445207774_0_0_0 };
extern const Il2CppGenericInst GenInst_Error_t445207774_0_0_0 = { 1, GenInst_Error_t445207774_0_0_0_Types };
static const Il2CppType* GenInst_GK_Player_t2782008294_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &GK_Player_t2782008294_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_Player_t2782008294_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_GK_Player_t2782008294_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType GK_RTM_QueryActivityResult_t2276098399_0_0_0;
static const Il2CppType* GenInst_GK_RTM_QueryActivityResult_t2276098399_0_0_0_Types[] = { &GK_RTM_QueryActivityResult_t2276098399_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_RTM_QueryActivityResult_t2276098399_0_0_0 = { 1, GenInst_GK_RTM_QueryActivityResult_t2276098399_0_0_0_Types };
static const Il2CppType* GenInst_GK_Player_t2782008294_0_0_0_ByteU5BU5D_t3397334013_0_0_0_Types[] = { &GK_Player_t2782008294_0_0_0, &ByteU5BU5D_t3397334013_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_Player_t2782008294_0_0_0_ByteU5BU5D_t3397334013_0_0_0 = { 2, GenInst_GK_Player_t2782008294_0_0_0_ByteU5BU5D_t3397334013_0_0_0_Types };
extern const Il2CppType GK_PlayerConnectionState_t2434478783_0_0_0;
extern const Il2CppType GK_RTM_Match_t873568990_0_0_0;
static const Il2CppType* GenInst_GK_Player_t2782008294_0_0_0_GK_PlayerConnectionState_t2434478783_0_0_0_GK_RTM_Match_t873568990_0_0_0_Types[] = { &GK_Player_t2782008294_0_0_0, &GK_PlayerConnectionState_t2434478783_0_0_0, &GK_RTM_Match_t873568990_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_Player_t2782008294_0_0_0_GK_PlayerConnectionState_t2434478783_0_0_0_GK_RTM_Match_t873568990_0_0_0 = { 3, GenInst_GK_Player_t2782008294_0_0_0_GK_PlayerConnectionState_t2434478783_0_0_0_GK_RTM_Match_t873568990_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_GK_PlayerConnectionState_t2434478783_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &GK_PlayerConnectionState_t2434478783_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_GK_PlayerConnectionState_t2434478783_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_GK_PlayerConnectionState_t2434478783_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType GameCenter_TBM_t3457554475_0_0_0;
static const Il2CppType* GenInst_GameCenter_TBM_t3457554475_0_0_0_Types[] = { &GameCenter_TBM_t3457554475_0_0_0 };
extern const Il2CppGenericInst GenInst_GameCenter_TBM_t3457554475_0_0_0 = { 1, GenInst_GameCenter_TBM_t3457554475_0_0_0_Types };
extern const Il2CppType GK_TBM_Match_t132033130_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GK_TBM_Match_t132033130_0_0_0_Types[] = { &String_t_0_0_0, &GK_TBM_Match_t132033130_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GK_TBM_Match_t132033130_0_0_0 = { 2, GenInst_String_t_0_0_0_GK_TBM_Match_t132033130_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GK_TBM_Match_t132033130_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GK_TBM_Match_t132033130_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GK_TBM_Match_t132033130_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GK_TBM_Match_t132033130_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_GK_TBM_Match_t132033130_0_0_0_Types[] = { &GK_TBM_Match_t132033130_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_TBM_Match_t132033130_0_0_0 = { 1, GenInst_GK_TBM_Match_t132033130_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4099124910_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4099124910_0_0_0_Types[] = { &KeyValuePair_2_t4099124910_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4099124910_0_0_0 = { 1, GenInst_KeyValuePair_2_t4099124910_0_0_0_Types };
extern const Il2CppType GK_TBM_LoadMatchResult_t1639249273_0_0_0;
static const Il2CppType* GenInst_GK_TBM_LoadMatchResult_t1639249273_0_0_0_Types[] = { &GK_TBM_LoadMatchResult_t1639249273_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_TBM_LoadMatchResult_t1639249273_0_0_0 = { 1, GenInst_GK_TBM_LoadMatchResult_t1639249273_0_0_0_Types };
extern const Il2CppType GK_TBM_LoadMatchesResult_t370491735_0_0_0;
static const Il2CppType* GenInst_GK_TBM_LoadMatchesResult_t370491735_0_0_0_Types[] = { &GK_TBM_LoadMatchesResult_t370491735_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_TBM_LoadMatchesResult_t370491735_0_0_0 = { 1, GenInst_GK_TBM_LoadMatchesResult_t370491735_0_0_0_Types };
extern const Il2CppType GK_TBM_MatchDataUpdateResult_t1356006034_0_0_0;
static const Il2CppType* GenInst_GK_TBM_MatchDataUpdateResult_t1356006034_0_0_0_Types[] = { &GK_TBM_MatchDataUpdateResult_t1356006034_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_TBM_MatchDataUpdateResult_t1356006034_0_0_0 = { 1, GenInst_GK_TBM_MatchDataUpdateResult_t1356006034_0_0_0_Types };
extern const Il2CppType GK_TBM_MatchInitResult_t3847830897_0_0_0;
static const Il2CppType* GenInst_GK_TBM_MatchInitResult_t3847830897_0_0_0_Types[] = { &GK_TBM_MatchInitResult_t3847830897_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_TBM_MatchInitResult_t3847830897_0_0_0 = { 1, GenInst_GK_TBM_MatchInitResult_t3847830897_0_0_0_Types };
extern const Il2CppType GK_TBM_MatchQuitResult_t1233820656_0_0_0;
static const Il2CppType* GenInst_GK_TBM_MatchQuitResult_t1233820656_0_0_0_Types[] = { &GK_TBM_MatchQuitResult_t1233820656_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_TBM_MatchQuitResult_t1233820656_0_0_0 = { 1, GenInst_GK_TBM_MatchQuitResult_t1233820656_0_0_0_Types };
extern const Il2CppType GK_TBM_EndTrunResult_t1517380690_0_0_0;
static const Il2CppType* GenInst_GK_TBM_EndTrunResult_t1517380690_0_0_0_Types[] = { &GK_TBM_EndTrunResult_t1517380690_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_TBM_EndTrunResult_t1517380690_0_0_0 = { 1, GenInst_GK_TBM_EndTrunResult_t1517380690_0_0_0_Types };
extern const Il2CppType GK_TBM_MatchEndResult_t3461768810_0_0_0;
static const Il2CppType* GenInst_GK_TBM_MatchEndResult_t3461768810_0_0_0_Types[] = { &GK_TBM_MatchEndResult_t3461768810_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_TBM_MatchEndResult_t3461768810_0_0_0 = { 1, GenInst_GK_TBM_MatchEndResult_t3461768810_0_0_0_Types };
extern const Il2CppType GK_TBM_RematchResult_t3159773700_0_0_0;
static const Il2CppType* GenInst_GK_TBM_RematchResult_t3159773700_0_0_0_Types[] = { &GK_TBM_RematchResult_t3159773700_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_TBM_RematchResult_t3159773700_0_0_0 = { 1, GenInst_GK_TBM_RematchResult_t3159773700_0_0_0_Types };
extern const Il2CppType GK_TBM_MatchRemovedResult_t909126313_0_0_0;
static const Il2CppType* GenInst_GK_TBM_MatchRemovedResult_t909126313_0_0_0_Types[] = { &GK_TBM_MatchRemovedResult_t909126313_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_TBM_MatchRemovedResult_t909126313_0_0_0 = { 1, GenInst_GK_TBM_MatchRemovedResult_t909126313_0_0_0_Types };
extern const Il2CppType GK_TBM_MatchTurnResult_t3583658160_0_0_0;
static const Il2CppType* GenInst_GK_TBM_MatchTurnResult_t3583658160_0_0_0_Types[] = { &GK_TBM_MatchTurnResult_t3583658160_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_TBM_MatchTurnResult_t3583658160_0_0_0 = { 1, GenInst_GK_TBM_MatchTurnResult_t3583658160_0_0_0_Types };
extern const Il2CppType ISN_GameSaves_t2236256821_0_0_0;
static const Il2CppType* GenInst_ISN_GameSaves_t2236256821_0_0_0_Types[] = { &ISN_GameSaves_t2236256821_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_GameSaves_t2236256821_0_0_0 = { 1, GenInst_ISN_GameSaves_t2236256821_0_0_0_Types };
extern const Il2CppType GK_SavedGame_t3320093620_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GK_SavedGame_t3320093620_0_0_0_Types[] = { &String_t_0_0_0, &GK_SavedGame_t3320093620_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GK_SavedGame_t3320093620_0_0_0 = { 2, GenInst_String_t_0_0_0_GK_SavedGame_t3320093620_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GK_SavedGame_t3320093620_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GK_SavedGame_t3320093620_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GK_SavedGame_t3320093620_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GK_SavedGame_t3320093620_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_GK_SavedGame_t3320093620_0_0_0_Types[] = { &GK_SavedGame_t3320093620_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_SavedGame_t3320093620_0_0_0 = { 1, GenInst_GK_SavedGame_t3320093620_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2992218104_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2992218104_0_0_0_Types[] = { &KeyValuePair_2_t2992218104_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2992218104_0_0_0 = { 1, GenInst_KeyValuePair_2_t2992218104_0_0_0_Types };
extern const Il2CppType GK_SaveRemoveResult_t539310567_0_0_0;
static const Il2CppType* GenInst_GK_SaveRemoveResult_t539310567_0_0_0_Types[] = { &GK_SaveRemoveResult_t539310567_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_SaveRemoveResult_t539310567_0_0_0 = { 1, GenInst_GK_SaveRemoveResult_t539310567_0_0_0_Types };
extern const Il2CppType GK_SaveResult_t3946576453_0_0_0;
static const Il2CppType* GenInst_GK_SaveResult_t3946576453_0_0_0_Types[] = { &GK_SaveResult_t3946576453_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_SaveResult_t3946576453_0_0_0 = { 1, GenInst_GK_SaveResult_t3946576453_0_0_0_Types };
extern const Il2CppType GK_FetchResult_t1611512656_0_0_0;
static const Il2CppType* GenInst_GK_FetchResult_t1611512656_0_0_0_Types[] = { &GK_FetchResult_t1611512656_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_FetchResult_t1611512656_0_0_0 = { 1, GenInst_GK_FetchResult_t1611512656_0_0_0_Types };
extern const Il2CppType GK_SavesResolveResult_t3508055404_0_0_0;
static const Il2CppType* GenInst_GK_SavesResolveResult_t3508055404_0_0_0_Types[] = { &GK_SavesResolveResult_t3508055404_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_SavesResolveResult_t3508055404_0_0_0 = { 1, GenInst_GK_SavesResolveResult_t3508055404_0_0_0_Types };
extern const Il2CppType GK_SaveDataLoaded_t3684688319_0_0_0;
static const Il2CppType* GenInst_GK_SaveDataLoaded_t3684688319_0_0_0_Types[] = { &GK_SaveDataLoaded_t3684688319_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_SaveDataLoaded_t3684688319_0_0_0 = { 1, GenInst_GK_SaveDataLoaded_t3684688319_0_0_0_Types };
extern const Il2CppType GK_Score_t1529008873_0_0_0;
static const Il2CppType* GenInst_GK_Score_t1529008873_0_0_0_Types[] = { &GK_Score_t1529008873_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_Score_t1529008873_0_0_0 = { 1, GenInst_GK_Score_t1529008873_0_0_0_Types };
extern const Il2CppType GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0_Types[] = { &GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0 = { 1, GenInst_GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2131013475_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2131013475_0_0_0_Types[] = { &KeyValuePair_2_t2131013475_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2131013475_0_0_0 = { 1, GenInst_KeyValuePair_2_t2131013475_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GK_Score_t1529008873_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GK_Score_t1529008873_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GK_Score_t1529008873_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_GK_Score_t1529008873_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GK_Score_t1529008873_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GK_Score_t1529008873_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GK_Score_t1529008873_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_GK_Score_t1529008873_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2589147026_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2589147026_0_0_0_Types[] = { &KeyValuePair_2_t2589147026_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2589147026_0_0_0 = { 1, GenInst_KeyValuePair_2_t2589147026_0_0_0_Types };
extern const Il2CppType GK_LeaderBoardInfo_t3670215494_0_0_0;
static const Il2CppType* GenInst_GK_LeaderBoardInfo_t3670215494_0_0_0_Types[] = { &GK_LeaderBoardInfo_t3670215494_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_LeaderBoardInfo_t3670215494_0_0_0 = { 1, GenInst_GK_LeaderBoardInfo_t3670215494_0_0_0_Types };
extern const Il2CppType ISN_LoadSetLeaderboardsInfoResult_t3997789804_0_0_0;
static const Il2CppType* GenInst_ISN_LoadSetLeaderboardsInfoResult_t3997789804_0_0_0_Types[] = { &ISN_LoadSetLeaderboardsInfoResult_t3997789804_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_LoadSetLeaderboardsInfoResult_t3997789804_0_0_0 = { 1, GenInst_ISN_LoadSetLeaderboardsInfoResult_t3997789804_0_0_0_Types };
extern const Il2CppType Texture2D_t3542995729_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Texture2D_t3542995729_0_0_0_Types[] = { &String_t_0_0_0, &Texture2D_t3542995729_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Texture2D_t3542995729_0_0_0 = { 2, GenInst_String_t_0_0_0_Texture2D_t3542995729_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Texture2D_t3542995729_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Texture2D_t3542995729_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Texture2D_t3542995729_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Texture2D_t3542995729_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Texture2D_t3542995729_0_0_0_Types[] = { &Texture2D_t3542995729_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture2D_t3542995729_0_0_0 = { 1, GenInst_Texture2D_t3542995729_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3215120213_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3215120213_0_0_0_Types[] = { &KeyValuePair_2_t3215120213_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3215120213_0_0_0 = { 1, GenInst_KeyValuePair_2_t3215120213_0_0_0_Types };
extern const Il2CppType GK_UserPhotoLoadResult_t1614198031_0_0_0;
static const Il2CppType* GenInst_GK_UserPhotoLoadResult_t1614198031_0_0_0_Types[] = { &GK_UserPhotoLoadResult_t1614198031_0_0_0 };
extern const Il2CppGenericInst GenInst_GK_UserPhotoLoadResult_t1614198031_0_0_0 = { 1, GenInst_GK_UserPhotoLoadResult_t1614198031_0_0_0_Types };
extern const Il2CppType IOSCamera_t2845108690_0_0_0;
static const Il2CppType* GenInst_IOSCamera_t2845108690_0_0_0_Types[] = { &IOSCamera_t2845108690_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSCamera_t2845108690_0_0_0 = { 1, GenInst_IOSCamera_t2845108690_0_0_0_Types };
extern const Il2CppType IOSImagePickResult_t1671334394_0_0_0;
static const Il2CppType* GenInst_IOSImagePickResult_t1671334394_0_0_0_Types[] = { &IOSImagePickResult_t1671334394_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSImagePickResult_t1671334394_0_0_0 = { 1, GenInst_IOSImagePickResult_t1671334394_0_0_0_Types };
extern const Il2CppType ISN_FilePicker_t2957423793_0_0_0;
static const Il2CppType* GenInst_ISN_FilePicker_t2957423793_0_0_0_Types[] = { &ISN_FilePicker_t2957423793_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_FilePicker_t2957423793_0_0_0 = { 1, GenInst_ISN_FilePicker_t2957423793_0_0_0_Types };
extern const Il2CppType ISN_FilePickerResult_t2234803986_0_0_0;
static const Il2CppType* GenInst_ISN_FilePickerResult_t2234803986_0_0_0_Types[] = { &ISN_FilePickerResult_t2234803986_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_FilePickerResult_t2234803986_0_0_0 = { 1, GenInst_ISN_FilePickerResult_t2234803986_0_0_0_Types };
extern const Il2CppType ISN_MediaController_t1853840745_0_0_0;
static const Il2CppType* GenInst_ISN_MediaController_t1853840745_0_0_0_Types[] = { &ISN_MediaController_t1853840745_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_MediaController_t1853840745_0_0_0 = { 1, GenInst_ISN_MediaController_t1853840745_0_0_0_Types };
extern const Il2CppType MP_MediaItem_t4025623029_0_0_0;
static const Il2CppType* GenInst_MP_MediaItem_t4025623029_0_0_0_Types[] = { &MP_MediaItem_t4025623029_0_0_0 };
extern const Il2CppGenericInst GenInst_MP_MediaItem_t4025623029_0_0_0 = { 1, GenInst_MP_MediaItem_t4025623029_0_0_0_Types };
extern const Il2CppType MP_MediaPickerResult_t2204006871_0_0_0;
static const Il2CppType* GenInst_MP_MediaPickerResult_t2204006871_0_0_0_Types[] = { &MP_MediaPickerResult_t2204006871_0_0_0 };
extern const Il2CppGenericInst GenInst_MP_MediaPickerResult_t2204006871_0_0_0 = { 1, GenInst_MP_MediaPickerResult_t2204006871_0_0_0_Types };
extern const Il2CppType MP_MusicPlaybackState_t2364713801_0_0_0;
static const Il2CppType* GenInst_MP_MusicPlaybackState_t2364713801_0_0_0_Types[] = { &MP_MusicPlaybackState_t2364713801_0_0_0 };
extern const Il2CppGenericInst GenInst_MP_MusicPlaybackState_t2364713801_0_0_0 = { 1, GenInst_MP_MusicPlaybackState_t2364713801_0_0_0_Types };
extern const Il2CppType IOSVideoManager_t3003649481_0_0_0;
static const Il2CppType* GenInst_IOSVideoManager_t3003649481_0_0_0_Types[] = { &IOSVideoManager_t3003649481_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSVideoManager_t3003649481_0_0_0 = { 1, GenInst_IOSVideoManager_t3003649481_0_0_0_Types };
extern const Il2CppType ISN_ReplayKit_t2994976952_0_0_0;
static const Il2CppType* GenInst_ISN_ReplayKit_t2994976952_0_0_0_Types[] = { &ISN_ReplayKit_t2994976952_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_ReplayKit_t2994976952_0_0_0 = { 1, GenInst_ISN_ReplayKit_t2994976952_0_0_0_Types };
extern const Il2CppType ReplayKitVideoShareResult_t2762953534_0_0_0;
static const Il2CppType* GenInst_ReplayKitVideoShareResult_t2762953534_0_0_0_Types[] = { &ReplayKitVideoShareResult_t2762953534_0_0_0 };
extern const Il2CppGenericInst GenInst_ReplayKitVideoShareResult_t2762953534_0_0_0 = { 1, GenInst_ReplayKitVideoShareResult_t2762953534_0_0_0_Types };
extern const Il2CppType ISN_LocalNotificationsController_t3778913674_0_0_0;
static const Il2CppType* GenInst_ISN_LocalNotificationsController_t3778913674_0_0_0_Types[] = { &ISN_LocalNotificationsController_t3778913674_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_LocalNotificationsController_t3778913674_0_0_0 = { 1, GenInst_ISN_LocalNotificationsController_t3778913674_0_0_0_Types };
extern const Il2CppType ISN_LocalNotification_t273186689_0_0_0;
static const Il2CppType* GenInst_ISN_LocalNotification_t273186689_0_0_0_Types[] = { &ISN_LocalNotification_t273186689_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_LocalNotification_t273186689_0_0_0 = { 1, GenInst_ISN_LocalNotification_t273186689_0_0_0_Types };
extern const Il2CppType ISN_RemoteNotificationsController_t1910006075_0_0_0;
static const Il2CppType* GenInst_ISN_RemoteNotificationsController_t1910006075_0_0_0_Types[] = { &ISN_RemoteNotificationsController_t1910006075_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_RemoteNotificationsController_t1910006075_0_0_0 = { 1, GenInst_ISN_RemoteNotificationsController_t1910006075_0_0_0_Types };
extern const Il2CppType ISN_RemoteNotificationsRegistrationResult_t3335875151_0_0_0;
static const Il2CppType* GenInst_ISN_RemoteNotificationsRegistrationResult_t3335875151_0_0_0_Types[] = { &ISN_RemoteNotificationsRegistrationResult_t3335875151_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_RemoteNotificationsRegistrationResult_t3335875151_0_0_0 = { 1, GenInst_ISN_RemoteNotificationsRegistrationResult_t3335875151_0_0_0_Types };
extern const Il2CppType ISN_DeviceToken_t380973950_0_0_0;
static const Il2CppType* GenInst_ISN_DeviceToken_t380973950_0_0_0_Types[] = { &ISN_DeviceToken_t380973950_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_DeviceToken_t380973950_0_0_0 = { 1, GenInst_ISN_DeviceToken_t380973950_0_0_0_Types };
extern const Il2CppType ISN_RemoteNotification_t1449597314_0_0_0;
static const Il2CppType* GenInst_ISN_RemoteNotification_t1449597314_0_0_0_Types[] = { &ISN_RemoteNotification_t1449597314_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_RemoteNotification_t1449597314_0_0_0 = { 1, GenInst_ISN_RemoteNotification_t1449597314_0_0_0_Types };
extern const Il2CppType IOSDialogResult_t3739241316_0_0_0;
static const Il2CppType* GenInst_IOSDialogResult_t3739241316_0_0_0_Types[] = { &IOSDialogResult_t3739241316_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSDialogResult_t3739241316_0_0_0 = { 1, GenInst_IOSDialogResult_t3739241316_0_0_0_Types };
extern const Il2CppType IOSSocialManager_t2957403963_0_0_0;
static const Il2CppType* GenInst_IOSSocialManager_t2957403963_0_0_0_Types[] = { &IOSSocialManager_t2957403963_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSSocialManager_t2957403963_0_0_0 = { 1, GenInst_IOSSocialManager_t2957403963_0_0_0_Types };
extern const Il2CppType IOSStoreProductView_t607200268_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_IOSStoreProductView_t607200268_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &IOSStoreProductView_t607200268_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_IOSStoreProductView_t607200268_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_IOSStoreProductView_t607200268_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_IOSStoreProductView_t607200268_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &IOSStoreProductView_t607200268_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_IOSStoreProductView_t607200268_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_IOSStoreProductView_t607200268_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IOSStoreProductView_t607200268_0_0_0_Types[] = { &IOSStoreProductView_t607200268_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSStoreProductView_t607200268_0_0_0 = { 1, GenInst_IOSStoreProductView_t607200268_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1667338421_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1667338421_0_0_0_Types[] = { &KeyValuePair_2_t1667338421_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1667338421_0_0_0 = { 1, GenInst_KeyValuePair_2_t1667338421_0_0_0_Types };
extern const Il2CppType IOSStoreKitResult_t2359407583_0_0_0;
static const Il2CppType* GenInst_IOSStoreKitResult_t2359407583_0_0_0_Types[] = { &IOSStoreKitResult_t2359407583_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSStoreKitResult_t2359407583_0_0_0 = { 1, GenInst_IOSStoreKitResult_t2359407583_0_0_0_Types };
extern const Il2CppType IOSStoreKitRestoreResult_t3305276155_0_0_0;
static const Il2CppType* GenInst_IOSStoreKitRestoreResult_t3305276155_0_0_0_Types[] = { &IOSStoreKitRestoreResult_t3305276155_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSStoreKitRestoreResult_t3305276155_0_0_0 = { 1, GenInst_IOSStoreKitRestoreResult_t3305276155_0_0_0_Types };
extern const Il2CppType IOSStoreKitVerificationResponse_t4263658582_0_0_0;
static const Il2CppType* GenInst_IOSStoreKitVerificationResponse_t4263658582_0_0_0_Types[] = { &IOSStoreKitVerificationResponse_t4263658582_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSStoreKitVerificationResponse_t4263658582_0_0_0 = { 1, GenInst_IOSStoreKitVerificationResponse_t4263658582_0_0_0_Types };
extern const Il2CppType SK_CloudService_t588287925_0_0_0;
static const Il2CppType* GenInst_SK_CloudService_t588287925_0_0_0_Types[] = { &SK_CloudService_t588287925_0_0_0 };
extern const Il2CppGenericInst GenInst_SK_CloudService_t588287925_0_0_0 = { 1, GenInst_SK_CloudService_t588287925_0_0_0_Types };
extern const Il2CppType SK_AuthorizationResult_t2047753871_0_0_0;
static const Il2CppType* GenInst_SK_AuthorizationResult_t2047753871_0_0_0_Types[] = { &SK_AuthorizationResult_t2047753871_0_0_0 };
extern const Il2CppGenericInst GenInst_SK_AuthorizationResult_t2047753871_0_0_0 = { 1, GenInst_SK_AuthorizationResult_t2047753871_0_0_0_Types };
extern const Il2CppType SK_RequestCapabilitieResult_t2510163844_0_0_0;
static const Il2CppType* GenInst_SK_RequestCapabilitieResult_t2510163844_0_0_0_Types[] = { &SK_RequestCapabilitieResult_t2510163844_0_0_0 };
extern const Il2CppGenericInst GenInst_SK_RequestCapabilitieResult_t2510163844_0_0_0 = { 1, GenInst_SK_RequestCapabilitieResult_t2510163844_0_0_0_Types };
extern const Il2CppType SK_RequestStorefrontIdentifierResult_t1112477162_0_0_0;
static const Il2CppType* GenInst_SK_RequestStorefrontIdentifierResult_t1112477162_0_0_0_Types[] = { &SK_RequestStorefrontIdentifierResult_t1112477162_0_0_0 };
extern const Il2CppGenericInst GenInst_SK_RequestStorefrontIdentifierResult_t1112477162_0_0_0 = { 1, GenInst_SK_RequestStorefrontIdentifierResult_t1112477162_0_0_0_Types };
extern const Il2CppType IOSNativeAppEvents_t694411788_0_0_0;
static const Il2CppType* GenInst_IOSNativeAppEvents_t694411788_0_0_0_Types[] = { &IOSNativeAppEvents_t694411788_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSNativeAppEvents_t694411788_0_0_0 = { 1, GenInst_IOSNativeAppEvents_t694411788_0_0_0_Types };
extern const Il2CppType PhoneNumber_t113354641_0_0_0;
static const Il2CppType* GenInst_PhoneNumber_t113354641_0_0_0_Types[] = { &PhoneNumber_t113354641_0_0_0 };
extern const Il2CppGenericInst GenInst_PhoneNumber_t113354641_0_0_0 = { 1, GenInst_PhoneNumber_t113354641_0_0_0_Types };
extern const Il2CppType ContactStore_t160827595_0_0_0;
static const Il2CppType* GenInst_ContactStore_t160827595_0_0_0_Types[] = { &ContactStore_t160827595_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactStore_t160827595_0_0_0 = { 1, GenInst_ContactStore_t160827595_0_0_0_Types };
extern const Il2CppType ContactsResult_t1045731284_0_0_0;
static const Il2CppType* GenInst_ContactsResult_t1045731284_0_0_0_Types[] = { &ContactsResult_t1045731284_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactsResult_t1045731284_0_0_0 = { 1, GenInst_ContactsResult_t1045731284_0_0_0_Types };
extern const Il2CppType Contact_t4178394798_0_0_0;
static const Il2CppType* GenInst_Contact_t4178394798_0_0_0_Types[] = { &Contact_t4178394798_0_0_0 };
extern const Il2CppGenericInst GenInst_Contact_t4178394798_0_0_0 = { 1, GenInst_Contact_t4178394798_0_0_0_Types };
extern const Il2CppType IOSDateTimePicker_t849222074_0_0_0;
static const Il2CppType* GenInst_IOSDateTimePicker_t849222074_0_0_0_Types[] = { &IOSDateTimePicker_t849222074_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSDateTimePicker_t849222074_0_0_0 = { 1, GenInst_IOSDateTimePicker_t849222074_0_0_0_Types };
extern const Il2CppType ISN_GestureRecognizer_t4198098372_0_0_0;
static const Il2CppType* GenInst_ISN_GestureRecognizer_t4198098372_0_0_0_Types[] = { &ISN_GestureRecognizer_t4198098372_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_GestureRecognizer_t4198098372_0_0_0 = { 1, GenInst_ISN_GestureRecognizer_t4198098372_0_0_0_Types };
extern const Il2CppType ISN_SwipeDirection_t768921696_0_0_0;
static const Il2CppType* GenInst_ISN_SwipeDirection_t768921696_0_0_0_Types[] = { &ISN_SwipeDirection_t768921696_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_SwipeDirection_t768921696_0_0_0 = { 1, GenInst_ISN_SwipeDirection_t768921696_0_0_0_Types };
extern const Il2CppType ISN_Logger_t85856405_0_0_0;
static const Il2CppType* GenInst_ISN_Logger_t85856405_0_0_0_Types[] = { &ISN_Logger_t85856405_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_Logger_t85856405_0_0_0 = { 1, GenInst_ISN_Logger_t85856405_0_0_0_Types };
extern const Il2CppType ISN_Security_t2700938347_0_0_0;
static const Il2CppType* GenInst_ISN_Security_t2700938347_0_0_0_Types[] = { &ISN_Security_t2700938347_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_Security_t2700938347_0_0_0 = { 1, GenInst_ISN_Security_t2700938347_0_0_0_Types };
extern const Il2CppType ISN_LocalReceiptResult_t3746327569_0_0_0;
static const Il2CppType* GenInst_ISN_LocalReceiptResult_t3746327569_0_0_0_Types[] = { &ISN_LocalReceiptResult_t3746327569_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_LocalReceiptResult_t3746327569_0_0_0 = { 1, GenInst_ISN_LocalReceiptResult_t3746327569_0_0_0_Types };
extern const Il2CppType IOSSharedApplication_t4065685598_0_0_0;
static const Il2CppType* GenInst_IOSSharedApplication_t4065685598_0_0_0_Types[] = { &IOSSharedApplication_t4065685598_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSSharedApplication_t4065685598_0_0_0 = { 1, GenInst_IOSSharedApplication_t4065685598_0_0_0_Types };
extern const Il2CppType ISN_CheckUrlResult_t1645724501_0_0_0;
static const Il2CppType* GenInst_ISN_CheckUrlResult_t1645724501_0_0_0_Types[] = { &ISN_CheckUrlResult_t1645724501_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_CheckUrlResult_t1645724501_0_0_0 = { 1, GenInst_ISN_CheckUrlResult_t1645724501_0_0_0_Types };
extern const Il2CppType IOSNativeUtility_t933355194_0_0_0;
static const Il2CppType* GenInst_IOSNativeUtility_t933355194_0_0_0_Types[] = { &IOSNativeUtility_t933355194_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSNativeUtility_t933355194_0_0_0 = { 1, GenInst_IOSNativeUtility_t933355194_0_0_0_Types };
extern const Il2CppType ISN_Locale_t2162888085_0_0_0;
static const Il2CppType* GenInst_ISN_Locale_t2162888085_0_0_0_Types[] = { &ISN_Locale_t2162888085_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_Locale_t2162888085_0_0_0 = { 1, GenInst_ISN_Locale_t2162888085_0_0_0_Types };
extern const Il2CppType ISN_CloudKit_t2197422136_0_0_0;
static const Il2CppType* GenInst_ISN_CloudKit_t2197422136_0_0_0_Types[] = { &ISN_CloudKit_t2197422136_0_0_0 };
extern const Il2CppGenericInst GenInst_ISN_CloudKit_t2197422136_0_0_0 = { 1, GenInst_ISN_CloudKit_t2197422136_0_0_0_Types };
extern const Il2CppType CK_Record_t3973541762_0_0_0;
static const Il2CppType* GenInst_CK_Record_t3973541762_0_0_0_Types[] = { &CK_Record_t3973541762_0_0_0 };
extern const Il2CppGenericInst GenInst_CK_Record_t3973541762_0_0_0 = { 1, GenInst_CK_Record_t3973541762_0_0_0_Types };
extern const Il2CppType iCloudManager_t2506189173_0_0_0;
static const Il2CppType* GenInst_iCloudManager_t2506189173_0_0_0_Types[] = { &iCloudManager_t2506189173_0_0_0 };
extern const Il2CppGenericInst GenInst_iCloudManager_t2506189173_0_0_0 = { 1, GenInst_iCloudManager_t2506189173_0_0_0_Types };
extern const Il2CppType iCloudData_t3080637488_0_0_0;
static const Il2CppType* GenInst_iCloudData_t3080637488_0_0_0_Types[] = { &iCloudData_t3080637488_0_0_0 };
extern const Il2CppGenericInst GenInst_iCloudData_t3080637488_0_0_0 = { 1, GenInst_iCloudData_t3080637488_0_0_0_Types };
extern const Il2CppType List_1_t2449758620_0_0_0;
static const Il2CppType* GenInst_List_1_t2449758620_0_0_0_Types[] = { &List_1_t2449758620_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2449758620_0_0_0 = { 1, GenInst_List_1_t2449758620_0_0_0_Types };
extern const Il2CppType CK_Database_t243306482_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_CK_Database_t243306482_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &CK_Database_t243306482_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_CK_Database_t243306482_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_CK_Database_t243306482_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_CK_Database_t243306482_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &CK_Database_t243306482_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_CK_Database_t243306482_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_CK_Database_t243306482_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_CK_Database_t243306482_0_0_0_Types[] = { &CK_Database_t243306482_0_0_0 };
extern const Il2CppGenericInst GenInst_CK_Database_t243306482_0_0_0 = { 1, GenInst_CK_Database_t243306482_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1303444635_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1303444635_0_0_0_Types[] = { &KeyValuePair_2_t1303444635_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1303444635_0_0_0 = { 1, GenInst_KeyValuePair_2_t1303444635_0_0_0_Types };
extern const Il2CppType CK_RecordResult_t4062548349_0_0_0;
static const Il2CppType* GenInst_CK_RecordResult_t4062548349_0_0_0_Types[] = { &CK_RecordResult_t4062548349_0_0_0 };
extern const Il2CppGenericInst GenInst_CK_RecordResult_t4062548349_0_0_0 = { 1, GenInst_CK_RecordResult_t4062548349_0_0_0_Types };
extern const Il2CppType CK_RecordDeleteResult_t3248469484_0_0_0;
static const Il2CppType* GenInst_CK_RecordDeleteResult_t3248469484_0_0_0_Types[] = { &CK_RecordDeleteResult_t3248469484_0_0_0 };
extern const Il2CppGenericInst GenInst_CK_RecordDeleteResult_t3248469484_0_0_0 = { 1, GenInst_CK_RecordDeleteResult_t3248469484_0_0_0_Types };
extern const Il2CppType CK_QueryResult_t1174067488_0_0_0;
static const Il2CppType* GenInst_CK_QueryResult_t1174067488_0_0_0_Types[] = { &CK_QueryResult_t1174067488_0_0_0 };
extern const Il2CppGenericInst GenInst_CK_QueryResult_t1174067488_0_0_0 = { 1, GenInst_CK_QueryResult_t1174067488_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_CK_Record_t3973541762_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &CK_Record_t3973541762_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_CK_Record_t3973541762_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_CK_Record_t3973541762_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_CK_Record_t3973541762_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &CK_Record_t3973541762_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_CK_Record_t3973541762_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_CK_Record_t3973541762_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t738712619_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t738712619_0_0_0_Types[] = { &KeyValuePair_2_t738712619_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t738712619_0_0_0 = { 1, GenInst_KeyValuePair_2_t738712619_0_0_0_Types };
extern const Il2CppType CK_RecordID_t41838833_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_CK_RecordID_t41838833_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &CK_RecordID_t41838833_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_CK_RecordID_t41838833_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_CK_RecordID_t41838833_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_CK_RecordID_t41838833_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &CK_RecordID_t41838833_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_CK_RecordID_t41838833_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_CK_RecordID_t41838833_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_CK_RecordID_t41838833_0_0_0_Types[] = { &CK_RecordID_t41838833_0_0_0 };
extern const Il2CppGenericInst GenInst_CK_RecordID_t41838833_0_0_0 = { 1, GenInst_CK_RecordID_t41838833_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1101976986_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1101976986_0_0_0_Types[] = { &KeyValuePair_2_t1101976986_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1101976986_0_0_0 = { 1, GenInst_KeyValuePair_2_t1101976986_0_0_0_Types };
extern const Il2CppType List_1_t2689214752_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2689214752_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2689214752_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2689214752_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t2689214752_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2689214752_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2689214752_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2689214752_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t2689214752_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t2689214752_0_0_0_Types[] = { &List_1_t2689214752_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2689214752_0_0_0 = { 1, GenInst_List_1_t2689214752_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2361339236_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2361339236_0_0_0_Types[] = { &KeyValuePair_2_t2361339236_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2361339236_0_0_0 = { 1, GenInst_KeyValuePair_2_t2361339236_0_0_0_Types };
extern const Il2CppType Hashtable_t909839986_0_0_0;
static const Il2CppType* GenInst_Hashtable_t909839986_0_0_0_Types[] = { &Hashtable_t909839986_0_0_0 };
extern const Il2CppGenericInst GenInst_Hashtable_t909839986_0_0_0 = { 1, GenInst_Hashtable_t909839986_0_0_0_Types };
extern const Il2CppType Rect_t3681755626_0_0_0;
static const Il2CppType* GenInst_Rect_t3681755626_0_0_0_Types[] = { &Rect_t3681755626_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t3681755626_0_0_0 = { 1, GenInst_Rect_t3681755626_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Char_t3454481338_0_0_0_Types[] = { &String_t_0_0_0, &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Char_t3454481338_0_0_0 = { 2, GenInst_String_t_0_0_0_Char_t3454481338_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Char_t3454481338_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Char_t3454481338_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Char_t3454481338_0_0_0_Types };
extern const Il2CppType Image_t2042527209_0_0_0;
static const Il2CppType* GenInst_Image_t2042527209_0_0_0_Types[] = { &Image_t2042527209_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t2042527209_0_0_0 = { 1, GenInst_Image_t2042527209_0_0_0_Types };
extern const Il2CppType ICanvasRaycastFilter_t1367822892_0_0_0;
static const Il2CppType* GenInst_ICanvasRaycastFilter_t1367822892_0_0_0_Types[] = { &ICanvasRaycastFilter_t1367822892_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasRaycastFilter_t1367822892_0_0_0 = { 1, GenInst_ICanvasRaycastFilter_t1367822892_0_0_0_Types };
extern const Il2CppType ISerializationCallbackReceiver_t1665913161_0_0_0;
static const Il2CppType* GenInst_ISerializationCallbackReceiver_t1665913161_0_0_0_Types[] = { &ISerializationCallbackReceiver_t1665913161_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializationCallbackReceiver_t1665913161_0_0_0 = { 1, GenInst_ISerializationCallbackReceiver_t1665913161_0_0_0_Types };
static const Il2CppType* GenInst_ILayoutElement_t1975293769_0_0_0_Types[] = { &ILayoutElement_t1975293769_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0 = { 1, GenInst_ILayoutElement_t1975293769_0_0_0_Types };
extern const Il2CppType MaskableGraphic_t540192618_0_0_0;
static const Il2CppType* GenInst_MaskableGraphic_t540192618_0_0_0_Types[] = { &MaskableGraphic_t540192618_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskableGraphic_t540192618_0_0_0 = { 1, GenInst_MaskableGraphic_t540192618_0_0_0_Types };
extern const Il2CppType IMaskable_t1431842707_0_0_0;
static const Il2CppType* GenInst_IMaskable_t1431842707_0_0_0_Types[] = { &IMaskable_t1431842707_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaskable_t1431842707_0_0_0 = { 1, GenInst_IMaskable_t1431842707_0_0_0_Types };
extern const Il2CppType IMaterialModifier_t3028564983_0_0_0;
static const Il2CppType* GenInst_IMaterialModifier_t3028564983_0_0_0_Types[] = { &IMaterialModifier_t3028564983_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaterialModifier_t3028564983_0_0_0 = { 1, GenInst_IMaterialModifier_t3028564983_0_0_0_Types };
extern const Il2CppType UIBehaviour_t3960014691_0_0_0;
static const Il2CppType* GenInst_UIBehaviour_t3960014691_0_0_0_Types[] = { &UIBehaviour_t3960014691_0_0_0 };
extern const Il2CppGenericInst GenInst_UIBehaviour_t3960014691_0_0_0 = { 1, GenInst_UIBehaviour_t3960014691_0_0_0_Types };
extern const Il2CppType SA_EditorAd_t1410159287_0_0_0;
static const Il2CppType* GenInst_SA_EditorAd_t1410159287_0_0_0_Types[] = { &SA_EditorAd_t1410159287_0_0_0 };
extern const Il2CppGenericInst GenInst_SA_EditorAd_t1410159287_0_0_0 = { 1, GenInst_SA_EditorAd_t1410159287_0_0_0_Types };
extern const Il2CppType Quaternion_t4030073918_0_0_0;
static const Il2CppType* GenInst_Quaternion_t4030073918_0_0_0_Types[] = { &Quaternion_t4030073918_0_0_0 };
extern const Il2CppGenericInst GenInst_Quaternion_t4030073918_0_0_0 = { 1, GenInst_Quaternion_t4030073918_0_0_0_Types };
extern const Il2CppType JumpPoint_t1009860606_0_0_0;
static const Il2CppType* GenInst_JumpPoint_t1009860606_0_0_0_Types[] = { &JumpPoint_t1009860606_0_0_0 };
extern const Il2CppGenericInst GenInst_JumpPoint_t1009860606_0_0_0 = { 1, GenInst_JumpPoint_t1009860606_0_0_0_Types };
extern const Il2CppType List_1_t1762270573_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1762270573_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1762270573_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1762270573_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t1762270573_0_0_0_Types };
extern const Il2CppType AN_PropertyTemplate_t2393149441_0_0_0;
static const Il2CppType* GenInst_AN_PropertyTemplate_t2393149441_0_0_0_Types[] = { &AN_PropertyTemplate_t2393149441_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_PropertyTemplate_t2393149441_0_0_0 = { 1, GenInst_AN_PropertyTemplate_t2393149441_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1762270573_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1762270573_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1762270573_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t1762270573_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t1762270573_0_0_0_Types[] = { &List_1_t1762270573_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1762270573_0_0_0 = { 1, GenInst_List_1_t1762270573_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1434395057_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1434395057_0_0_0_Types[] = { &KeyValuePair_2_t1434395057_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1434395057_0_0_0 = { 1, GenInst_KeyValuePair_2_t1434395057_0_0_0_Types };
extern const Il2CppType AN_ActivityTemplate_t3380616875_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_AN_ActivityTemplate_t3380616875_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &AN_ActivityTemplate_t3380616875_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_AN_ActivityTemplate_t3380616875_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_AN_ActivityTemplate_t3380616875_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_AN_ActivityTemplate_t3380616875_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &AN_ActivityTemplate_t3380616875_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_AN_ActivityTemplate_t3380616875_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_AN_ActivityTemplate_t3380616875_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_AN_ActivityTemplate_t3380616875_0_0_0_Types[] = { &AN_ActivityTemplate_t3380616875_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_ActivityTemplate_t3380616875_0_0_0 = { 1, GenInst_AN_ActivityTemplate_t3380616875_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t145787732_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t145787732_0_0_0_Types[] = { &KeyValuePair_2_t145787732_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t145787732_0_0_0 = { 1, GenInst_KeyValuePair_2_t145787732_0_0_0_Types };
extern const Il2CppType AndroidInAppPurchaseManager_t2155442111_0_0_0;
static const Il2CppType* GenInst_AndroidInAppPurchaseManager_t2155442111_0_0_0_Types[] = { &AndroidInAppPurchaseManager_t2155442111_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidInAppPurchaseManager_t2155442111_0_0_0 = { 1, GenInst_AndroidInAppPurchaseManager_t2155442111_0_0_0_Types };
extern const Il2CppType BillingResult_t3511841850_0_0_0;
static const Il2CppType* GenInst_BillingResult_t3511841850_0_0_0_Types[] = { &BillingResult_t3511841850_0_0_0 };
extern const Il2CppGenericInst GenInst_BillingResult_t3511841850_0_0_0 = { 1, GenInst_BillingResult_t3511841850_0_0_0_Types };
extern const Il2CppType GoogleProductTemplate_t1112616324_0_0_0;
static const Il2CppType* GenInst_GoogleProductTemplate_t1112616324_0_0_0_Types[] = { &GoogleProductTemplate_t1112616324_0_0_0 };
extern const Il2CppGenericInst GenInst_GoogleProductTemplate_t1112616324_0_0_0 = { 1, GenInst_GoogleProductTemplate_t1112616324_0_0_0_Types };
extern const Il2CppType GooglePurchaseTemplate_t2609331866_0_0_0;
static const Il2CppType* GenInst_GooglePurchaseTemplate_t2609331866_0_0_0_Types[] = { &GooglePurchaseTemplate_t2609331866_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePurchaseTemplate_t2609331866_0_0_0 = { 1, GenInst_GooglePurchaseTemplate_t2609331866_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GooglePurchaseTemplate_t2609331866_0_0_0_Types[] = { &String_t_0_0_0, &GooglePurchaseTemplate_t2609331866_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GooglePurchaseTemplate_t2609331866_0_0_0 = { 2, GenInst_String_t_0_0_0_GooglePurchaseTemplate_t2609331866_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GooglePurchaseTemplate_t2609331866_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GooglePurchaseTemplate_t2609331866_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GooglePurchaseTemplate_t2609331866_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GooglePurchaseTemplate_t2609331866_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2281456350_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2281456350_0_0_0_Types[] = { &KeyValuePair_2_t2281456350_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2281456350_0_0_0 = { 1, GenInst_KeyValuePair_2_t2281456350_0_0_0_Types };
extern const Il2CppType AndroidGoogleAnalytics_t1297670224_0_0_0;
static const Il2CppType* GenInst_AndroidGoogleAnalytics_t1297670224_0_0_0_Types[] = { &AndroidGoogleAnalytics_t1297670224_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidGoogleAnalytics_t1297670224_0_0_0 = { 1, GenInst_AndroidGoogleAnalytics_t1297670224_0_0_0_Types };
extern const Il2CppType AN_LicenseManager_t1808504078_0_0_0;
static const Il2CppType* GenInst_AN_LicenseManager_t1808504078_0_0_0_Types[] = { &AN_LicenseManager_t1808504078_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_LicenseManager_t1808504078_0_0_0 = { 1, GenInst_AN_LicenseManager_t1808504078_0_0_0_Types };
extern const Il2CppType AN_LicenseRequestResult_t2331370561_0_0_0;
static const Il2CppType* GenInst_AN_LicenseRequestResult_t2331370561_0_0_0_Types[] = { &AN_LicenseRequestResult_t2331370561_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_LicenseRequestResult_t2331370561_0_0_0 = { 1, GenInst_AN_LicenseRequestResult_t2331370561_0_0_0_Types };
extern const Il2CppType AddressBookController_t3183854505_0_0_0;
static const Il2CppType* GenInst_AddressBookController_t3183854505_0_0_0_Types[] = { &AddressBookController_t3183854505_0_0_0 };
extern const Il2CppGenericInst GenInst_AddressBookController_t3183854505_0_0_0 = { 1, GenInst_AddressBookController_t3183854505_0_0_0_Types };
extern const Il2CppType AndroidContactInfo_t2118672179_0_0_0;
static const Il2CppType* GenInst_AndroidContactInfo_t2118672179_0_0_0_Types[] = { &AndroidContactInfo_t2118672179_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidContactInfo_t2118672179_0_0_0 = { 1, GenInst_AndroidContactInfo_t2118672179_0_0_0_Types };
extern const Il2CppType AndroidAppInfoLoader_t2209520163_0_0_0;
static const Il2CppType* GenInst_AndroidAppInfoLoader_t2209520163_0_0_0_Types[] = { &AndroidAppInfoLoader_t2209520163_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidAppInfoLoader_t2209520163_0_0_0 = { 1, GenInst_AndroidAppInfoLoader_t2209520163_0_0_0_Types };
extern const Il2CppType PackageAppInfo_t260912591_0_0_0;
static const Il2CppType* GenInst_PackageAppInfo_t260912591_0_0_0_Types[] = { &PackageAppInfo_t260912591_0_0_0 };
extern const Il2CppGenericInst GenInst_PackageAppInfo_t260912591_0_0_0 = { 1, GenInst_PackageAppInfo_t260912591_0_0_0_Types };
extern const Il2CppType AndroidCamera_t1711267764_0_0_0;
static const Il2CppType* GenInst_AndroidCamera_t1711267764_0_0_0_Types[] = { &AndroidCamera_t1711267764_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidCamera_t1711267764_0_0_0 = { 1, GenInst_AndroidCamera_t1711267764_0_0_0_Types };
extern const Il2CppType AndroidImagePickResult_t1791733552_0_0_0;
static const Il2CppType* GenInst_AndroidImagePickResult_t1791733552_0_0_0_Types[] = { &AndroidImagePickResult_t1791733552_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidImagePickResult_t1791733552_0_0_0 = { 1, GenInst_AndroidImagePickResult_t1791733552_0_0_0_Types };
extern const Il2CppType GallerySaveResult_t1643856950_0_0_0;
static const Il2CppType* GenInst_GallerySaveResult_t1643856950_0_0_0_Types[] = { &GallerySaveResult_t1643856950_0_0_0 };
extern const Il2CppGenericInst GenInst_GallerySaveResult_t1643856950_0_0_0 = { 1, GenInst_GallerySaveResult_t1643856950_0_0_0_Types };
extern const Il2CppType SA_ScreenShotMaker_OLD_t795198627_0_0_0;
static const Il2CppType* GenInst_SA_ScreenShotMaker_OLD_t795198627_0_0_0_Types[] = { &SA_ScreenShotMaker_OLD_t795198627_0_0_0 };
extern const Il2CppGenericInst GenInst_SA_ScreenShotMaker_OLD_t795198627_0_0_0 = { 1, GenInst_SA_ScreenShotMaker_OLD_t795198627_0_0_0_Types };
extern const Il2CppType AndroidNativeUtility_t798160036_0_0_0;
static const Il2CppType* GenInst_AndroidNativeUtility_t798160036_0_0_0_Types[] = { &AndroidNativeUtility_t798160036_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidNativeUtility_t798160036_0_0_0 = { 1, GenInst_AndroidNativeUtility_t798160036_0_0_0_Types };
extern const Il2CppType AN_PackageCheckResult_t3695415755_0_0_0;
static const Il2CppType* GenInst_AN_PackageCheckResult_t3695415755_0_0_0_Types[] = { &AN_PackageCheckResult_t3695415755_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_PackageCheckResult_t3695415755_0_0_0 = { 1, GenInst_AN_PackageCheckResult_t3695415755_0_0_0_Types };
extern const Il2CppType AN_Locale_t121755426_0_0_0;
static const Il2CppType* GenInst_AN_Locale_t121755426_0_0_0_Types[] = { &AN_Locale_t121755426_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_Locale_t121755426_0_0_0 = { 1, GenInst_AN_Locale_t121755426_0_0_0_Types };
static const Il2CppType* GenInst_StringU5BU5D_t1642385972_0_0_0_Types[] = { &StringU5BU5D_t1642385972_0_0_0 };
extern const Il2CppGenericInst GenInst_StringU5BU5D_t1642385972_0_0_0 = { 1, GenInst_StringU5BU5D_t1642385972_0_0_0_Types };
extern const Il2CppType AN_NetworkInfo_t247793570_0_0_0;
static const Il2CppType* GenInst_AN_NetworkInfo_t247793570_0_0_0_Types[] = { &AN_NetworkInfo_t247793570_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_NetworkInfo_t247793570_0_0_0 = { 1, GenInst_AN_NetworkInfo_t247793570_0_0_0_Types };
extern const Il2CppType ImmersiveMode_t15485700_0_0_0;
static const Il2CppType* GenInst_ImmersiveMode_t15485700_0_0_0_Types[] = { &ImmersiveMode_t15485700_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmersiveMode_t15485700_0_0_0 = { 1, GenInst_ImmersiveMode_t15485700_0_0_0_Types };
extern const Il2CppType PermissionsManager_t4266031513_0_0_0;
static const Il2CppType* GenInst_PermissionsManager_t4266031513_0_0_0_Types[] = { &PermissionsManager_t4266031513_0_0_0 };
extern const Il2CppGenericInst GenInst_PermissionsManager_t4266031513_0_0_0 = { 1, GenInst_PermissionsManager_t4266031513_0_0_0_Types };
extern const Il2CppType AN_GrantPermissionsResult_t250489657_0_0_0;
static const Il2CppType* GenInst_AN_GrantPermissionsResult_t250489657_0_0_0_Types[] = { &AN_GrantPermissionsResult_t250489657_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_GrantPermissionsResult_t250489657_0_0_0 = { 1, GenInst_AN_GrantPermissionsResult_t250489657_0_0_0_Types };
extern const Il2CppType AN_ManifestPermission_t1772110202_0_0_0;
static const Il2CppType* GenInst_AN_ManifestPermission_t1772110202_0_0_0_Types[] = { &AN_ManifestPermission_t1772110202_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_ManifestPermission_t1772110202_0_0_0 = { 1, GenInst_AN_ManifestPermission_t1772110202_0_0_0_Types };
extern const Il2CppType TVAppController_t1564329309_0_0_0;
static const Il2CppType* GenInst_TVAppController_t1564329309_0_0_0_Types[] = { &TVAppController_t1564329309_0_0_0 };
extern const Il2CppGenericInst GenInst_TVAppController_t1564329309_0_0_0 = { 1, GenInst_TVAppController_t1564329309_0_0_0_Types };
extern const Il2CppType AndroidApp_t620754602_0_0_0;
static const Il2CppType* GenInst_AndroidApp_t620754602_0_0_0_Types[] = { &AndroidApp_t620754602_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidApp_t620754602_0_0_0 = { 1, GenInst_AndroidApp_t620754602_0_0_0_Types };
extern const Il2CppType AndroidActivityResult_t3757510801_0_0_0;
static const Il2CppType* GenInst_AndroidActivityResult_t3757510801_0_0_0_Types[] = { &AndroidActivityResult_t3757510801_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidActivityResult_t3757510801_0_0_0 = { 1, GenInst_AndroidActivityResult_t3757510801_0_0_0_Types };
extern const Il2CppType AndroidNotificationManager_t3325256667_0_0_0;
static const Il2CppType* GenInst_AndroidNotificationManager_t3325256667_0_0_0_Types[] = { &AndroidNotificationManager_t3325256667_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidNotificationManager_t3325256667_0_0_0 = { 1, GenInst_AndroidNotificationManager_t3325256667_0_0_0_Types };
extern const Il2CppType LocalNotificationTemplate_t2880890350_0_0_0;
static const Il2CppType* GenInst_LocalNotificationTemplate_t2880890350_0_0_0_Types[] = { &LocalNotificationTemplate_t2880890350_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalNotificationTemplate_t2880890350_0_0_0 = { 1, GenInst_LocalNotificationTemplate_t2880890350_0_0_0_Types };
extern const Il2CppType Dictionary_2_t309261261_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0_Types[] = { &String_t_0_0_0, &Dictionary_2_t309261261_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0 = { 2, GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0_Types };
extern const Il2CppType AN_PermissionState_t838201224_0_0_0;
static const Il2CppType* GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_Types[] = { &AN_ManifestPermission_t1772110202_0_0_0, &AN_PermissionState_t838201224_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0 = { 2, GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3409008887_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3409008887_0_0_0_Types[] = { &KeyValuePair_2_t3409008887_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3409008887_0_0_0 = { 1, GenInst_KeyValuePair_2_t3409008887_0_0_0_Types };
static const Il2CppType* GenInst_AN_PermissionState_t838201224_0_0_0_Types[] = { &AN_PermissionState_t838201224_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_PermissionState_t838201224_0_0_0 = { 1, GenInst_AN_PermissionState_t838201224_0_0_0_Types };
static const Il2CppType* GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_AN_ManifestPermission_t1772110202_0_0_0_Types[] = { &AN_ManifestPermission_t1772110202_0_0_0, &AN_PermissionState_t838201224_0_0_0, &AN_ManifestPermission_t1772110202_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_AN_ManifestPermission_t1772110202_0_0_0 = { 3, GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_AN_ManifestPermission_t1772110202_0_0_0_Types };
static const Il2CppType* GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_AN_PermissionState_t838201224_0_0_0_Types[] = { &AN_ManifestPermission_t1772110202_0_0_0, &AN_PermissionState_t838201224_0_0_0, &AN_PermissionState_t838201224_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_AN_PermissionState_t838201224_0_0_0 = { 3, GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_AN_PermissionState_t838201224_0_0_0_Types };
static const Il2CppType* GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &AN_ManifestPermission_t1772110202_0_0_0, &AN_PermissionState_t838201224_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_KeyValuePair_2_t3409008887_0_0_0_Types[] = { &AN_ManifestPermission_t1772110202_0_0_0, &AN_PermissionState_t838201224_0_0_0, &KeyValuePair_2_t3409008887_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_KeyValuePair_2_t3409008887_0_0_0 = { 3, GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_KeyValuePair_2_t3409008887_0_0_0_Types };
extern const Il2CppType AN_SoomlaGrow_t3055280006_0_0_0;
static const Il2CppType* GenInst_AN_SoomlaGrow_t3055280006_0_0_0_Types[] = { &AN_SoomlaGrow_t3055280006_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_SoomlaGrow_t3055280006_0_0_0 = { 1, GenInst_AN_SoomlaGrow_t3055280006_0_0_0_Types };
extern const Il2CppType AndroidTwitterManager_t3465455211_0_0_0;
static const Il2CppType* GenInst_AndroidTwitterManager_t3465455211_0_0_0_Types[] = { &AndroidTwitterManager_t3465455211_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidTwitterManager_t3465455211_0_0_0 = { 1, GenInst_AndroidTwitterManager_t3465455211_0_0_0_Types };
extern const Il2CppType TWResult_t1480791060_0_0_0;
static const Il2CppType* GenInst_TWResult_t1480791060_0_0_0_Types[] = { &TWResult_t1480791060_0_0_0 };
extern const Il2CppGenericInst GenInst_TWResult_t1480791060_0_0_0 = { 1, GenInst_TWResult_t1480791060_0_0_0_Types };
extern const Il2CppType FB_LoginResult_t1848477279_0_0_0;
static const Il2CppType* GenInst_FB_LoginResult_t1848477279_0_0_0_Types[] = { &FB_LoginResult_t1848477279_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_LoginResult_t1848477279_0_0_0 = { 1, GenInst_FB_LoginResult_t1848477279_0_0_0_Types };
extern const Il2CppType FB_PostResult_t992884730_0_0_0;
static const Il2CppType* GenInst_FB_PostResult_t992884730_0_0_0_Types[] = { &FB_PostResult_t992884730_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_PostResult_t992884730_0_0_0 = { 1, GenInst_FB_PostResult_t992884730_0_0_0_Types };
extern const Il2CppType FB_Result_t838248372_0_0_0;
static const Il2CppType* GenInst_FB_Result_t838248372_0_0_0_Types[] = { &FB_Result_t838248372_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_Result_t838248372_0_0_0 = { 1, GenInst_FB_Result_t838248372_0_0_0_Types };
extern const Il2CppType SPFacebook_t1065228369_0_0_0;
static const Il2CppType* GenInst_SPFacebook_t1065228369_0_0_0_Types[] = { &SPFacebook_t1065228369_0_0_0 };
extern const Il2CppGenericInst GenInst_SPFacebook_t1065228369_0_0_0 = { 1, GenInst_SPFacebook_t1065228369_0_0_0_Types };
extern const Il2CppType GP_AppInvitesController_t2424429497_0_0_0;
static const Il2CppType* GenInst_GP_AppInvitesController_t2424429497_0_0_0_Types[] = { &GP_AppInvitesController_t2424429497_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_AppInvitesController_t2424429497_0_0_0 = { 1, GenInst_GP_AppInvitesController_t2424429497_0_0_0_Types };
extern const Il2CppType GP_SendAppInvitesResult_t3999077544_0_0_0;
static const Il2CppType* GenInst_GP_SendAppInvitesResult_t3999077544_0_0_0_Types[] = { &GP_SendAppInvitesResult_t3999077544_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_SendAppInvitesResult_t3999077544_0_0_0 = { 1, GenInst_GP_SendAppInvitesResult_t3999077544_0_0_0_Types };
extern const Il2CppType GP_RetrieveAppInviteResult_t18485229_0_0_0;
static const Il2CppType* GenInst_GP_RetrieveAppInviteResult_t18485229_0_0_0_Types[] = { &GP_RetrieveAppInviteResult_t18485229_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_RetrieveAppInviteResult_t18485229_0_0_0 = { 1, GenInst_GP_RetrieveAppInviteResult_t18485229_0_0_0_Types };
extern const Il2CppType GoogleCloudManager_t1208504825_0_0_0;
static const Il2CppType* GenInst_GoogleCloudManager_t1208504825_0_0_0_Types[] = { &GoogleCloudManager_t1208504825_0_0_0 };
extern const Il2CppGenericInst GenInst_GoogleCloudManager_t1208504825_0_0_0 = { 1, GenInst_GoogleCloudManager_t1208504825_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_ByteU5BU5D_t3397334013_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &ByteU5BU5D_t3397334013_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_ByteU5BU5D_t3397334013_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_ByteU5BU5D_t3397334013_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_ByteU5BU5D_t3397334013_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &ByteU5BU5D_t3397334013_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_ByteU5BU5D_t3397334013_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_ByteU5BU5D_t3397334013_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t162504870_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t162504870_0_0_0_Types[] = { &KeyValuePair_2_t162504870_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t162504870_0_0_0 = { 1, GenInst_KeyValuePair_2_t162504870_0_0_0_Types };
extern const Il2CppType GoogleCloudResult_t743628707_0_0_0;
static const Il2CppType* GenInst_GoogleCloudResult_t743628707_0_0_0_Types[] = { &GoogleCloudResult_t743628707_0_0_0 };
extern const Il2CppGenericInst GenInst_GoogleCloudResult_t743628707_0_0_0 = { 1, GenInst_GoogleCloudResult_t743628707_0_0_0_Types };
extern const Il2CppType GoogleCloudMessageService_t984991750_0_0_0;
static const Il2CppType* GenInst_GoogleCloudMessageService_t984991750_0_0_0_Types[] = { &GoogleCloudMessageService_t984991750_0_0_0 };
extern const Il2CppGenericInst GenInst_GoogleCloudMessageService_t984991750_0_0_0 = { 1, GenInst_GoogleCloudMessageService_t984991750_0_0_0_Types };
extern const Il2CppType GP_GCM_RegistrationResult_t2892492118_0_0_0;
static const Il2CppType* GenInst_GP_GCM_RegistrationResult_t2892492118_0_0_0_Types[] = { &GP_GCM_RegistrationResult_t2892492118_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_GCM_RegistrationResult_t2892492118_0_0_0 = { 1, GenInst_GP_GCM_RegistrationResult_t2892492118_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &String_t_0_0_0, &Dictionary_2_t309261261_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType GooglePlayConnection_t348367589_0_0_0;
static const Il2CppType* GenInst_GooglePlayConnection_t348367589_0_0_0_Types[] = { &GooglePlayConnection_t348367589_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlayConnection_t348367589_0_0_0 = { 1, GenInst_GooglePlayConnection_t348367589_0_0_0_Types };
extern const Il2CppType GooglePlayConnectionResult_t2758718724_0_0_0;
static const Il2CppType* GenInst_GooglePlayConnectionResult_t2758718724_0_0_0_Types[] = { &GooglePlayConnectionResult_t2758718724_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlayConnectionResult_t2758718724_0_0_0 = { 1, GenInst_GooglePlayConnectionResult_t2758718724_0_0_0_Types };
extern const Il2CppType GPConnectionState_t647346880_0_0_0;
static const Il2CppType* GenInst_GPConnectionState_t647346880_0_0_0_Types[] = { &GPConnectionState_t647346880_0_0_0 };
extern const Il2CppGenericInst GenInst_GPConnectionState_t647346880_0_0_0 = { 1, GenInst_GPConnectionState_t647346880_0_0_0_Types };
extern const Il2CppType GooglePlayManager_t746138120_0_0_0;
static const Il2CppType* GenInst_GooglePlayManager_t746138120_0_0_0_Types[] = { &GooglePlayManager_t746138120_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlayManager_t746138120_0_0_0 = { 1, GenInst_GooglePlayManager_t746138120_0_0_0_Types };
extern const Il2CppType GooglePlayEvents_t1613959644_0_0_0;
static const Il2CppType* GenInst_GooglePlayEvents_t1613959644_0_0_0_Types[] = { &GooglePlayEvents_t1613959644_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlayEvents_t1613959644_0_0_0 = { 1, GenInst_GooglePlayEvents_t1613959644_0_0_0_Types };
extern const Il2CppType GP_Event_t323143668_0_0_0;
static const Il2CppType* GenInst_GP_Event_t323143668_0_0_0_Types[] = { &GP_Event_t323143668_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_Event_t323143668_0_0_0 = { 1, GenInst_GP_Event_t323143668_0_0_0_Types };
extern const Il2CppType GooglePlayResult_t3097469636_0_0_0;
static const Il2CppType* GenInst_GooglePlayResult_t3097469636_0_0_0_Types[] = { &GooglePlayResult_t3097469636_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlayResult_t3097469636_0_0_0 = { 1, GenInst_GooglePlayResult_t3097469636_0_0_0_Types };
extern const Il2CppType GooglePlayInvitationManager_t2586337437_0_0_0;
static const Il2CppType* GenInst_GooglePlayInvitationManager_t2586337437_0_0_0_Types[] = { &GooglePlayInvitationManager_t2586337437_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlayInvitationManager_t2586337437_0_0_0 = { 1, GenInst_GooglePlayInvitationManager_t2586337437_0_0_0_Types };
extern const Il2CppType GP_Invite_t626929087_0_0_0;
static const Il2CppType* GenInst_GP_Invite_t626929087_0_0_0_Types[] = { &GP_Invite_t626929087_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_Invite_t626929087_0_0_0 = { 1, GenInst_GP_Invite_t626929087_0_0_0_Types };
extern const Il2CppType List_1_t4291017515_0_0_0;
static const Il2CppType* GenInst_List_1_t4291017515_0_0_0_Types[] = { &List_1_t4291017515_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t4291017515_0_0_0 = { 1, GenInst_List_1_t4291017515_0_0_0_Types };
extern const Il2CppType AN_InvitationInboxCloseResult_t1284128610_0_0_0;
static const Il2CppType* GenInst_AN_InvitationInboxCloseResult_t1284128610_0_0_0_Types[] = { &AN_InvitationInboxCloseResult_t1284128610_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_InvitationInboxCloseResult_t1284128610_0_0_0 = { 1, GenInst_AN_InvitationInboxCloseResult_t1284128610_0_0_0_Types };
extern const Il2CppType GooglePlayerTemplate_t2506317812_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GooglePlayerTemplate_t2506317812_0_0_0_Types[] = { &String_t_0_0_0, &GooglePlayerTemplate_t2506317812_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GooglePlayerTemplate_t2506317812_0_0_0 = { 2, GenInst_String_t_0_0_0_GooglePlayerTemplate_t2506317812_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GooglePlayerTemplate_t2506317812_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GooglePlayerTemplate_t2506317812_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GooglePlayerTemplate_t2506317812_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GooglePlayerTemplate_t2506317812_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_GooglePlayerTemplate_t2506317812_0_0_0_Types[] = { &GooglePlayerTemplate_t2506317812_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlayerTemplate_t2506317812_0_0_0 = { 1, GenInst_GooglePlayerTemplate_t2506317812_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2178442296_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2178442296_0_0_0_Types[] = { &KeyValuePair_2_t2178442296_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2178442296_0_0_0 = { 1, GenInst_KeyValuePair_2_t2178442296_0_0_0_Types };
extern const Il2CppType GPLeaderBoard_t3649577886_0_0_0;
static const Il2CppType* GenInst_GPLeaderBoard_t3649577886_0_0_0_Types[] = { &GPLeaderBoard_t3649577886_0_0_0 };
extern const Il2CppGenericInst GenInst_GPLeaderBoard_t3649577886_0_0_0 = { 1, GenInst_GPLeaderBoard_t3649577886_0_0_0_Types };
extern const Il2CppType GPAchievement_t4279788054_0_0_0;
static const Il2CppType* GenInst_GPAchievement_t4279788054_0_0_0_Types[] = { &GPAchievement_t4279788054_0_0_0 };
extern const Il2CppGenericInst GenInst_GPAchievement_t4279788054_0_0_0 = { 1, GenInst_GPAchievement_t4279788054_0_0_0_Types };
extern const Il2CppType GPGameRequest_t4158842942_0_0_0;
static const Il2CppType* GenInst_GPGameRequest_t4158842942_0_0_0_Types[] = { &GPGameRequest_t4158842942_0_0_0 };
extern const Il2CppGenericInst GenInst_GPGameRequest_t4158842942_0_0_0 = { 1, GenInst_GPGameRequest_t4158842942_0_0_0_Types };
extern const Il2CppType GP_LeaderboardResult_t2034215294_0_0_0;
static const Il2CppType* GenInst_GP_LeaderboardResult_t2034215294_0_0_0_Types[] = { &GP_LeaderboardResult_t2034215294_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_LeaderboardResult_t2034215294_0_0_0 = { 1, GenInst_GP_LeaderboardResult_t2034215294_0_0_0_Types };
extern const Il2CppType GP_AchievementResult_t3970926374_0_0_0;
static const Il2CppType* GenInst_GP_AchievementResult_t3970926374_0_0_0_Types[] = { &GP_AchievementResult_t3970926374_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_AchievementResult_t3970926374_0_0_0 = { 1, GenInst_GP_AchievementResult_t3970926374_0_0_0_Types };
extern const Il2CppType GooglePlayGiftRequestResult_t350202007_0_0_0;
static const Il2CppType* GenInst_GooglePlayGiftRequestResult_t350202007_0_0_0_Types[] = { &GooglePlayGiftRequestResult_t350202007_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlayGiftRequestResult_t350202007_0_0_0 = { 1, GenInst_GooglePlayGiftRequestResult_t350202007_0_0_0_Types };
extern const Il2CppType List_1_t3527964074_0_0_0;
static const Il2CppType* GenInst_List_1_t3527964074_0_0_0_Types[] = { &List_1_t3527964074_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3527964074_0_0_0 = { 1, GenInst_List_1_t3527964074_0_0_0_Types };
extern const Il2CppType List_1_t1398341365_0_0_0;
static const Il2CppType* GenInst_List_1_t1398341365_0_0_0_Types[] = { &List_1_t1398341365_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1398341365_0_0_0 = { 1, GenInst_List_1_t1398341365_0_0_0_Types };
extern const Il2CppType GooglePlayQuests_t1618778050_0_0_0;
static const Il2CppType* GenInst_GooglePlayQuests_t1618778050_0_0_0_Types[] = { &GooglePlayQuests_t1618778050_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlayQuests_t1618778050_0_0_0 = { 1, GenInst_GooglePlayQuests_t1618778050_0_0_0_Types };
extern const Il2CppType GP_Participant_t2884377673_0_0_0;
static const Il2CppType* GenInst_GP_Participant_t2884377673_0_0_0_Types[] = { &GP_Participant_t2884377673_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_Participant_t2884377673_0_0_0 = { 1, GenInst_GP_Participant_t2884377673_0_0_0_Types };
extern const Il2CppType GP_QuestResult_t3390940437_0_0_0;
static const Il2CppType* GenInst_GP_QuestResult_t3390940437_0_0_0_Types[] = { &GP_QuestResult_t3390940437_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_QuestResult_t3390940437_0_0_0 = { 1, GenInst_GP_QuestResult_t3390940437_0_0_0_Types };
extern const Il2CppType GP_QuestsSelect_t2071002355_0_0_0;
static const Il2CppType* GenInst_GP_QuestsSelect_t2071002355_0_0_0_Types[] = { &GP_QuestsSelect_t2071002355_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_QuestsSelect_t2071002355_0_0_0 = { 1, GenInst_GP_QuestsSelect_t2071002355_0_0_0_Types };
extern const Il2CppType GP_Quest_t1641883470_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GP_Quest_t1641883470_0_0_0_Types[] = { &String_t_0_0_0, &GP_Quest_t1641883470_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GP_Quest_t1641883470_0_0_0 = { 2, GenInst_String_t_0_0_0_GP_Quest_t1641883470_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GP_Quest_t1641883470_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GP_Quest_t1641883470_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GP_Quest_t1641883470_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GP_Quest_t1641883470_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_GP_Quest_t1641883470_0_0_0_Types[] = { &GP_Quest_t1641883470_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_Quest_t1641883470_0_0_0 = { 1, GenInst_GP_Quest_t1641883470_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1314007954_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1314007954_0_0_0_Types[] = { &KeyValuePair_2_t1314007954_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1314007954_0_0_0 = { 1, GenInst_KeyValuePair_2_t1314007954_0_0_0_Types };
extern const Il2CppType GooglePlayRTM_t547022790_0_0_0;
static const Il2CppType* GenInst_GooglePlayRTM_t547022790_0_0_0_Types[] = { &GooglePlayRTM_t547022790_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlayRTM_t547022790_0_0_0 = { 1, GenInst_GooglePlayRTM_t547022790_0_0_0_Types };
extern const Il2CppType GP_RTM_Network_Package_t2050307869_0_0_0;
static const Il2CppType* GenInst_GP_RTM_Network_Package_t2050307869_0_0_0_Types[] = { &GP_RTM_Network_Package_t2050307869_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_RTM_Network_Package_t2050307869_0_0_0 = { 1, GenInst_GP_RTM_Network_Package_t2050307869_0_0_0_Types };
extern const Il2CppType GP_RTM_Room_t851604529_0_0_0;
static const Il2CppType* GenInst_GP_RTM_Room_t851604529_0_0_0_Types[] = { &GP_RTM_Room_t851604529_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_RTM_Room_t851604529_0_0_0 = { 1, GenInst_GP_RTM_Room_t851604529_0_0_0_Types };
extern const Il2CppType GP_RTM_ReliableMessageSentResult_t2743629396_0_0_0;
static const Il2CppType* GenInst_GP_RTM_ReliableMessageSentResult_t2743629396_0_0_0_Types[] = { &GP_RTM_ReliableMessageSentResult_t2743629396_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_RTM_ReliableMessageSentResult_t2743629396_0_0_0 = { 1, GenInst_GP_RTM_ReliableMessageSentResult_t2743629396_0_0_0_Types };
extern const Il2CppType GP_RTM_ReliableMessageDeliveredResult_t1694070004_0_0_0;
static const Il2CppType* GenInst_GP_RTM_ReliableMessageDeliveredResult_t1694070004_0_0_0_Types[] = { &GP_RTM_ReliableMessageDeliveredResult_t1694070004_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_RTM_ReliableMessageDeliveredResult_t1694070004_0_0_0 = { 1, GenInst_GP_RTM_ReliableMessageDeliveredResult_t1694070004_0_0_0_Types };
extern const Il2CppType GP_GamesStatusCodes_t1013506173_0_0_0;
static const Il2CppType* GenInst_GP_GamesStatusCodes_t1013506173_0_0_0_Types[] = { &GP_GamesStatusCodes_t1013506173_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_GamesStatusCodes_t1013506173_0_0_0 = { 1, GenInst_GP_GamesStatusCodes_t1013506173_0_0_0_Types };
extern const Il2CppType GP_RTM_Result_t473289279_0_0_0;
static const Il2CppType* GenInst_GP_RTM_Result_t473289279_0_0_0_Types[] = { &GP_RTM_Result_t473289279_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_RTM_Result_t473289279_0_0_0 = { 1, GenInst_GP_RTM_Result_t473289279_0_0_0_Types };
extern const Il2CppType GP_RTM_ReliableMessageListener_t1343560679_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GP_RTM_ReliableMessageListener_t1343560679_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GP_RTM_ReliableMessageListener_t1343560679_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GP_RTM_ReliableMessageListener_t1343560679_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_GP_RTM_ReliableMessageListener_t1343560679_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GP_RTM_ReliableMessageListener_t1343560679_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GP_RTM_ReliableMessageListener_t1343560679_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GP_RTM_ReliableMessageListener_t1343560679_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_GP_RTM_ReliableMessageListener_t1343560679_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_GP_RTM_ReliableMessageListener_t1343560679_0_0_0_Types[] = { &GP_RTM_ReliableMessageListener_t1343560679_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_RTM_ReliableMessageListener_t1343560679_0_0_0 = { 1, GenInst_GP_RTM_ReliableMessageListener_t1343560679_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2403698832_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2403698832_0_0_0_Types[] = { &KeyValuePair_2_t2403698832_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2403698832_0_0_0 = { 1, GenInst_KeyValuePair_2_t2403698832_0_0_0_Types };
extern const Il2CppType GooglePlaySavedGamesManager_t1475146022_0_0_0;
static const Il2CppType* GenInst_GooglePlaySavedGamesManager_t1475146022_0_0_0_Types[] = { &GooglePlaySavedGamesManager_t1475146022_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlaySavedGamesManager_t1475146022_0_0_0 = { 1, GenInst_GooglePlaySavedGamesManager_t1475146022_0_0_0_Types };
extern const Il2CppType GP_SnapshotMeta_t1354779439_0_0_0;
static const Il2CppType* GenInst_GP_SnapshotMeta_t1354779439_0_0_0_Types[] = { &GP_SnapshotMeta_t1354779439_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_SnapshotMeta_t1354779439_0_0_0 = { 1, GenInst_GP_SnapshotMeta_t1354779439_0_0_0_Types };
extern const Il2CppType GP_SpanshotLoadResult_t2263304449_0_0_0;
static const Il2CppType* GenInst_GP_SpanshotLoadResult_t2263304449_0_0_0_Types[] = { &GP_SpanshotLoadResult_t2263304449_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_SpanshotLoadResult_t2263304449_0_0_0 = { 1, GenInst_GP_SpanshotLoadResult_t2263304449_0_0_0_Types };
extern const Il2CppType GP_SnapshotConflict_t4002586770_0_0_0;
static const Il2CppType* GenInst_GP_SnapshotConflict_t4002586770_0_0_0_Types[] = { &GP_SnapshotConflict_t4002586770_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_SnapshotConflict_t4002586770_0_0_0 = { 1, GenInst_GP_SnapshotConflict_t4002586770_0_0_0_Types };
extern const Il2CppType GP_DeleteSnapshotResult_t3306790010_0_0_0;
static const Il2CppType* GenInst_GP_DeleteSnapshotResult_t3306790010_0_0_0_Types[] = { &GP_DeleteSnapshotResult_t3306790010_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_DeleteSnapshotResult_t3306790010_0_0_0 = { 1, GenInst_GP_DeleteSnapshotResult_t3306790010_0_0_0_Types };
extern const Il2CppType GooglePlayTBM_t641131310_0_0_0;
static const Il2CppType* GenInst_GooglePlayTBM_t641131310_0_0_0_Types[] = { &GooglePlayTBM_t641131310_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlayTBM_t641131310_0_0_0 = { 1, GenInst_GooglePlayTBM_t641131310_0_0_0_Types };
extern const Il2CppType GP_TBM_Match_t1275077981_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GP_TBM_Match_t1275077981_0_0_0_Types[] = { &String_t_0_0_0, &GP_TBM_Match_t1275077981_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GP_TBM_Match_t1275077981_0_0_0 = { 2, GenInst_String_t_0_0_0_GP_TBM_Match_t1275077981_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GP_TBM_Match_t1275077981_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GP_TBM_Match_t1275077981_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GP_TBM_Match_t1275077981_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GP_TBM_Match_t1275077981_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_GP_TBM_Match_t1275077981_0_0_0_Types[] = { &GP_TBM_Match_t1275077981_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_TBM_Match_t1275077981_0_0_0 = { 1, GenInst_GP_TBM_Match_t1275077981_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t947202465_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t947202465_0_0_0_Types[] = { &KeyValuePair_2_t947202465_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t947202465_0_0_0 = { 1, GenInst_KeyValuePair_2_t947202465_0_0_0_Types };
extern const Il2CppType GP_TBM_LoadMatchesResult_t841773038_0_0_0;
static const Il2CppType* GenInst_GP_TBM_LoadMatchesResult_t841773038_0_0_0_Types[] = { &GP_TBM_LoadMatchesResult_t841773038_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_TBM_LoadMatchesResult_t841773038_0_0_0 = { 1, GenInst_GP_TBM_LoadMatchesResult_t841773038_0_0_0_Types };
extern const Il2CppType GP_TBM_MatchInitiatedResult_t4144060847_0_0_0;
static const Il2CppType* GenInst_GP_TBM_MatchInitiatedResult_t4144060847_0_0_0_Types[] = { &GP_TBM_MatchInitiatedResult_t4144060847_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_TBM_MatchInitiatedResult_t4144060847_0_0_0 = { 1, GenInst_GP_TBM_MatchInitiatedResult_t4144060847_0_0_0_Types };
extern const Il2CppType GP_TBM_CancelMatchResult_t3879293152_0_0_0;
static const Il2CppType* GenInst_GP_TBM_CancelMatchResult_t3879293152_0_0_0_Types[] = { &GP_TBM_CancelMatchResult_t3879293152_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_TBM_CancelMatchResult_t3879293152_0_0_0 = { 1, GenInst_GP_TBM_CancelMatchResult_t3879293152_0_0_0_Types };
extern const Il2CppType GP_TBM_LeaveMatchResult_t3657803719_0_0_0;
static const Il2CppType* GenInst_GP_TBM_LeaveMatchResult_t3657803719_0_0_0_Types[] = { &GP_TBM_LeaveMatchResult_t3657803719_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_TBM_LeaveMatchResult_t3657803719_0_0_0 = { 1, GenInst_GP_TBM_LeaveMatchResult_t3657803719_0_0_0_Types };
extern const Il2CppType GP_TBM_LoadMatchResult_t1357418930_0_0_0;
static const Il2CppType* GenInst_GP_TBM_LoadMatchResult_t1357418930_0_0_0_Types[] = { &GP_TBM_LoadMatchResult_t1357418930_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_TBM_LoadMatchResult_t1357418930_0_0_0 = { 1, GenInst_GP_TBM_LoadMatchResult_t1357418930_0_0_0_Types };
extern const Il2CppType GP_TBM_UpdateMatchResult_t3943005969_0_0_0;
static const Il2CppType* GenInst_GP_TBM_UpdateMatchResult_t3943005969_0_0_0_Types[] = { &GP_TBM_UpdateMatchResult_t3943005969_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_TBM_UpdateMatchResult_t3943005969_0_0_0 = { 1, GenInst_GP_TBM_UpdateMatchResult_t3943005969_0_0_0_Types };
extern const Il2CppType GP_TBM_MatchReceivedResult_t2394672915_0_0_0;
static const Il2CppType* GenInst_GP_TBM_MatchReceivedResult_t2394672915_0_0_0_Types[] = { &GP_TBM_MatchReceivedResult_t2394672915_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_TBM_MatchReceivedResult_t2394672915_0_0_0 = { 1, GenInst_GP_TBM_MatchReceivedResult_t2394672915_0_0_0_Types };
extern const Il2CppType GP_TBM_MatchRemovedResult_t686355120_0_0_0;
static const Il2CppType* GenInst_GP_TBM_MatchRemovedResult_t686355120_0_0_0_Types[] = { &GP_TBM_MatchRemovedResult_t686355120_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_TBM_MatchRemovedResult_t686355120_0_0_0 = { 1, GenInst_GP_TBM_MatchRemovedResult_t686355120_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GP_TBM_MatchInitiatedResult_t4144060847_0_0_0_Types[] = { &String_t_0_0_0, &GP_TBM_MatchInitiatedResult_t4144060847_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GP_TBM_MatchInitiatedResult_t4144060847_0_0_0 = { 2, GenInst_String_t_0_0_0_GP_TBM_MatchInitiatedResult_t4144060847_0_0_0_Types };
extern const Il2CppType GP_ParticipantResult_t2469018720_0_0_0;
static const Il2CppType* GenInst_GP_ParticipantResult_t2469018720_0_0_0_Types[] = { &GP_ParticipantResult_t2469018720_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_ParticipantResult_t2469018720_0_0_0 = { 1, GenInst_GP_ParticipantResult_t2469018720_0_0_0_Types };
extern const Il2CppType GP_TBM_MatchTurnStatus_t2221730550_0_0_0;
static const Il2CppType* GenInst_GP_TBM_MatchTurnStatus_t2221730550_0_0_0_Types[] = { &GP_TBM_MatchTurnStatus_t2221730550_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_TBM_MatchTurnStatus_t2221730550_0_0_0 = { 1, GenInst_GP_TBM_MatchTurnStatus_t2221730550_0_0_0_Types };
extern const Il2CppType GooglePlusAPI_t3074122137_0_0_0;
static const Il2CppType* GenInst_GooglePlusAPI_t3074122137_0_0_0_Types[] = { &GooglePlusAPI_t3074122137_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlusAPI_t3074122137_0_0_0 = { 1, GenInst_GooglePlusAPI_t3074122137_0_0_0_Types };
extern const Il2CppType AN_PlusButtonsManager_t882255532_0_0_0;
static const Il2CppType* GenInst_AN_PlusButtonsManager_t882255532_0_0_0_Types[] = { &AN_PlusButtonsManager_t882255532_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_PlusButtonsManager_t882255532_0_0_0 = { 1, GenInst_AN_PlusButtonsManager_t882255532_0_0_0_Types };
extern const Il2CppType AN_PlusButton_t1370758440_0_0_0;
static const Il2CppType* GenInst_AN_PlusButton_t1370758440_0_0_0_Types[] = { &AN_PlusButton_t1370758440_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_PlusButton_t1370758440_0_0_0 = { 1, GenInst_AN_PlusButton_t1370758440_0_0_0_Types };
extern const Il2CppType AN_PlusShareResult_t3488688014_0_0_0;
static const Il2CppType* GenInst_AN_PlusShareResult_t3488688014_0_0_0_Types[] = { &AN_PlusShareResult_t3488688014_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_PlusShareResult_t3488688014_0_0_0 = { 1, GenInst_AN_PlusShareResult_t3488688014_0_0_0_Types };
extern const Il2CppType GooglePlayUtils_t968630402_0_0_0;
static const Il2CppType* GenInst_GooglePlayUtils_t968630402_0_0_0_Types[] = { &GooglePlayUtils_t968630402_0_0_0 };
extern const Il2CppGenericInst GenInst_GooglePlayUtils_t968630402_0_0_0 = { 1, GenInst_GooglePlayUtils_t968630402_0_0_0_Types };
extern const Il2CppType GP_AdvertisingIdLoadResult_t2783375090_0_0_0;
static const Il2CppType* GenInst_GP_AdvertisingIdLoadResult_t2783375090_0_0_0_Types[] = { &GP_AdvertisingIdLoadResult_t2783375090_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_AdvertisingIdLoadResult_t2783375090_0_0_0 = { 1, GenInst_GP_AdvertisingIdLoadResult_t2783375090_0_0_0_Types };
extern const Il2CppType AndroidDialogResult_t1664046954_0_0_0;
static const Il2CppType* GenInst_AndroidDialogResult_t1664046954_0_0_0_Types[] = { &AndroidDialogResult_t1664046954_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidDialogResult_t1664046954_0_0_0 = { 1, GenInst_AndroidDialogResult_t1664046954_0_0_0_Types };
extern const Il2CppType GoogleMobileAdBanner_t1323818958_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GoogleMobileAdBanner_t1323818958_0_0_0_Types[] = { &String_t_0_0_0, &GoogleMobileAdBanner_t1323818958_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GoogleMobileAdBanner_t1323818958_0_0_0 = { 2, GenInst_String_t_0_0_0_GoogleMobileAdBanner_t1323818958_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GoogleMobileAdBanner_t1323818958_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GoogleMobileAdBanner_t1323818958_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GoogleMobileAdBanner_t1323818958_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GoogleMobileAdBanner_t1323818958_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_GoogleMobileAdBanner_t1323818958_0_0_0_Types[] = { &GoogleMobileAdBanner_t1323818958_0_0_0 };
extern const Il2CppGenericInst GenInst_GoogleMobileAdBanner_t1323818958_0_0_0 = { 1, GenInst_GoogleMobileAdBanner_t1323818958_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t995943442_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t995943442_0_0_0_Types[] = { &KeyValuePair_2_t995943442_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t995943442_0_0_0 = { 1, GenInst_KeyValuePair_2_t995943442_0_0_0_Types };
extern const Il2CppType AndroidAdMobController_t1638300356_0_0_0;
static const Il2CppType* GenInst_AndroidAdMobController_t1638300356_0_0_0_Types[] = { &AndroidAdMobController_t1638300356_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidAdMobController_t1638300356_0_0_0 = { 1, GenInst_AndroidAdMobController_t1638300356_0_0_0_Types };
extern const Il2CppType DefaultPreviewButton_t12674677_0_0_0;
static const Il2CppType* GenInst_DefaultPreviewButton_t12674677_0_0_0_Types[] = { &DefaultPreviewButton_t12674677_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultPreviewButton_t12674677_0_0_0 = { 1, GenInst_DefaultPreviewButton_t12674677_0_0_0_Types };
extern const Il2CppType SA_PartisipantUI_t3115332592_0_0_0;
static const Il2CppType* GenInst_SA_PartisipantUI_t3115332592_0_0_0_Types[] = { &SA_PartisipantUI_t3115332592_0_0_0 };
extern const Il2CppGenericInst GenInst_SA_PartisipantUI_t3115332592_0_0_0 = { 1, GenInst_SA_PartisipantUI_t3115332592_0_0_0_Types };
extern const Il2CppType SA_FriendUI_t3775341837_0_0_0;
static const Il2CppType* GenInst_SA_FriendUI_t3775341837_0_0_0_Types[] = { &SA_FriendUI_t3775341837_0_0_0 };
extern const Il2CppGenericInst GenInst_SA_FriendUI_t3775341837_0_0_0 = { 1, GenInst_SA_FriendUI_t3775341837_0_0_0_Types };
extern const Il2CppType CustomPlayerUIRow_t1758969648_0_0_0;
static const Il2CppType* GenInst_CustomPlayerUIRow_t1758969648_0_0_0_Types[] = { &CustomPlayerUIRow_t1758969648_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomPlayerUIRow_t1758969648_0_0_0 = { 1, GenInst_CustomPlayerUIRow_t1758969648_0_0_0_Types };
extern const Il2CppType CustomLeaderboardFiledsHolder_t4123707333_0_0_0;
static const Il2CppType* GenInst_CustomLeaderboardFiledsHolder_t4123707333_0_0_0_Types[] = { &CustomLeaderboardFiledsHolder_t4123707333_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomLeaderboardFiledsHolder_t4123707333_0_0_0 = { 1, GenInst_CustomLeaderboardFiledsHolder_t4123707333_0_0_0_Types };
extern const Il2CppType GPScore_t3219488889_0_0_0;
static const Il2CppType* GenInst_GPScore_t3219488889_0_0_0_Types[] = { &GPScore_t3219488889_0_0_0 };
extern const Il2CppGenericInst GenInst_GPScore_t3219488889_0_0_0 = { 1, GenInst_GPScore_t3219488889_0_0_0_Types };
extern const Il2CppType FB_AppRequest_t501312625_0_0_0;
static const Il2CppType* GenInst_FB_AppRequest_t501312625_0_0_0_Types[] = { &FB_AppRequest_t501312625_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_AppRequest_t501312625_0_0_0 = { 1, GenInst_FB_AppRequest_t501312625_0_0_0_Types };
extern const Il2CppType FB_AppRequestResult_t2450670428_0_0_0;
static const Il2CppType* GenInst_FB_AppRequestResult_t2450670428_0_0_0_Types[] = { &FB_AppRequestResult_t2450670428_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_AppRequestResult_t2450670428_0_0_0 = { 1, GenInst_FB_AppRequestResult_t2450670428_0_0_0_Types };
extern const Il2CppType FB_UserInfo_t2704578078_0_0_0;
static const Il2CppType* GenInst_FB_UserInfo_t2704578078_0_0_0_Types[] = { &FB_UserInfo_t2704578078_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_UserInfo_t2704578078_0_0_0 = { 1, GenInst_FB_UserInfo_t2704578078_0_0_0_Types };
extern const Il2CppType FB_Score_t1450841581_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_FB_Score_t1450841581_0_0_0_Types[] = { &String_t_0_0_0, &FB_Score_t1450841581_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FB_Score_t1450841581_0_0_0 = { 2, GenInst_String_t_0_0_0_FB_Score_t1450841581_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_FB_Score_t1450841581_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &FB_Score_t1450841581_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FB_Score_t1450841581_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_FB_Score_t1450841581_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_FB_Score_t1450841581_0_0_0_Types[] = { &FB_Score_t1450841581_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_Score_t1450841581_0_0_0 = { 1, GenInst_FB_Score_t1450841581_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1122966065_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1122966065_0_0_0_Types[] = { &KeyValuePair_2_t1122966065_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1122966065_0_0_0 = { 1, GenInst_KeyValuePair_2_t1122966065_0_0_0_Types };
extern const Il2CppType TW_APIRequstResult_t455151055_0_0_0;
static const Il2CppType* GenInst_TW_APIRequstResult_t455151055_0_0_0_Types[] = { &TW_APIRequstResult_t455151055_0_0_0 };
extern const Il2CppGenericInst GenInst_TW_APIRequstResult_t455151055_0_0_0 = { 1, GenInst_TW_APIRequstResult_t455151055_0_0_0_Types };
extern const Il2CppType TwitterUserInfo_t87370740_0_0_0;
static const Il2CppType* GenInst_TwitterUserInfo_t87370740_0_0_0_Types[] = { &TwitterUserInfo_t87370740_0_0_0 };
extern const Il2CppGenericInst GenInst_TwitterUserInfo_t87370740_0_0_0 = { 1, GenInst_TwitterUserInfo_t87370740_0_0_0_Types };
extern const Il2CppType TweetTemplate_t3444491657_0_0_0;
static const Il2CppType* GenInst_TweetTemplate_t3444491657_0_0_0_Types[] = { &TweetTemplate_t3444491657_0_0_0 };
extern const Il2CppGenericInst GenInst_TweetTemplate_t3444491657_0_0_0 = { 1, GenInst_TweetTemplate_t3444491657_0_0_0_Types };
extern const Il2CppType AndroidADBanner_t886219444_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_AndroidADBanner_t886219444_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &AndroidADBanner_t886219444_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_AndroidADBanner_t886219444_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_AndroidADBanner_t886219444_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_AndroidADBanner_t886219444_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &AndroidADBanner_t886219444_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_AndroidADBanner_t886219444_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_AndroidADBanner_t886219444_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_AndroidADBanner_t886219444_0_0_0_Types[] = { &AndroidADBanner_t886219444_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidADBanner_t886219444_0_0_0 = { 1, GenInst_AndroidADBanner_t886219444_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1946357597_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1946357597_0_0_0_Types[] = { &KeyValuePair_2_t1946357597_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1946357597_0_0_0 = { 1, GenInst_KeyValuePair_2_t1946357597_0_0_0_Types };
extern const Il2CppType GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0_Types[] = { &GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0 };
extern const Il2CppGenericInst GenInst_GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0 = { 1, GenInst_GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1218264544_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1218264544_0_0_0_Types[] = { &KeyValuePair_2_t1218264544_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1218264544_0_0_0 = { 1, GenInst_KeyValuePair_2_t1218264544_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GPScore_t3219488889_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GPScore_t3219488889_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GPScore_t3219488889_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_GPScore_t3219488889_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GPScore_t3219488889_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GPScore_t3219488889_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GPScore_t3219488889_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_GPScore_t3219488889_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4279627042_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4279627042_0_0_0_Types[] = { &KeyValuePair_2_t4279627042_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4279627042_0_0_0 = { 1, GenInst_KeyValuePair_2_t4279627042_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_FB_UserInfo_t2704578078_0_0_0_Types[] = { &String_t_0_0_0, &FB_UserInfo_t2704578078_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FB_UserInfo_t2704578078_0_0_0 = { 2, GenInst_String_t_0_0_0_FB_UserInfo_t2704578078_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_FB_UserInfo_t2704578078_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &FB_UserInfo_t2704578078_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FB_UserInfo_t2704578078_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_FB_UserInfo_t2704578078_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2376702562_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2376702562_0_0_0_Types[] = { &KeyValuePair_2_t2376702562_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2376702562_0_0_0 = { 1, GenInst_KeyValuePair_2_t2376702562_0_0_0_Types };
extern const Il2CppType Dictionary_2_t833011044_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Dictionary_2_t833011044_0_0_0_Types[] = { &String_t_0_0_0, &Dictionary_2_t833011044_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t833011044_0_0_0 = { 2, GenInst_String_t_0_0_0_Dictionary_2_t833011044_0_0_0_Types };
extern const Il2CppType FB_LikeInfo_t3213199078_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_FB_LikeInfo_t3213199078_0_0_0_Types[] = { &String_t_0_0_0, &FB_LikeInfo_t3213199078_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FB_LikeInfo_t3213199078_0_0_0 = { 2, GenInst_String_t_0_0_0_FB_LikeInfo_t3213199078_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_FB_LikeInfo_t3213199078_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &FB_LikeInfo_t3213199078_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FB_LikeInfo_t3213199078_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_FB_LikeInfo_t3213199078_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_FB_LikeInfo_t3213199078_0_0_0_Types[] = { &FB_LikeInfo_t3213199078_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_LikeInfo_t3213199078_0_0_0 = { 1, GenInst_FB_LikeInfo_t3213199078_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2885323562_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2885323562_0_0_0_Types[] = { &KeyValuePair_2_t2885323562_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2885323562_0_0_0 = { 1, GenInst_KeyValuePair_2_t2885323562_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Dictionary_2_t833011044_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Dictionary_2_t833011044_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t833011044_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Dictionary_2_t833011044_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t833011044_0_0_0_Types[] = { &Dictionary_2_t833011044_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t833011044_0_0_0 = { 1, GenInst_Dictionary_2_t833011044_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t505135528_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t505135528_0_0_0_Types[] = { &KeyValuePair_2_t505135528_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t505135528_0_0_0 = { 1, GenInst_KeyValuePair_2_t505135528_0_0_0_Types };
extern const Il2CppType FB_PermissionResult_t2322011743_0_0_0;
static const Il2CppType* GenInst_FB_PermissionResult_t2322011743_0_0_0_Types[] = { &FB_PermissionResult_t2322011743_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_PermissionResult_t2322011743_0_0_0 = { 1, GenInst_FB_PermissionResult_t2322011743_0_0_0_Types };
extern const Il2CppType FB_LikesRetrieveTask_t2495592094_0_0_0;
static const Il2CppType* GenInst_FB_Result_t838248372_0_0_0_FB_LikesRetrieveTask_t2495592094_0_0_0_Types[] = { &FB_Result_t838248372_0_0_0, &FB_LikesRetrieveTask_t2495592094_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_Result_t838248372_0_0_0_FB_LikesRetrieveTask_t2495592094_0_0_0 = { 2, GenInst_FB_Result_t838248372_0_0_0_FB_LikesRetrieveTask_t2495592094_0_0_0_Types };
extern const Il2CppType FB_Permission_t1872772122_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_FB_Permission_t1872772122_0_0_0_Types[] = { &String_t_0_0_0, &FB_Permission_t1872772122_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FB_Permission_t1872772122_0_0_0 = { 2, GenInst_String_t_0_0_0_FB_Permission_t1872772122_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_FB_Permission_t1872772122_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &FB_Permission_t1872772122_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FB_Permission_t1872772122_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_FB_Permission_t1872772122_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_FB_Permission_t1872772122_0_0_0_Types[] = { &FB_Permission_t1872772122_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_Permission_t1872772122_0_0_0 = { 1, GenInst_FB_Permission_t1872772122_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1544896606_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1544896606_0_0_0_Types[] = { &KeyValuePair_2_t1544896606_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1544896606_0_0_0 = { 1, GenInst_KeyValuePair_2_t1544896606_0_0_0_Types };
extern const Il2CppType FB_ProfileImageSize_t3003328130_0_0_0;
static const Il2CppType* GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Texture2D_t3542995729_0_0_0_Types[] = { &FB_ProfileImageSize_t3003328130_0_0_0, &Texture2D_t3542995729_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Texture2D_t3542995729_0_0_0 = { 2, GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Texture2D_t3542995729_0_0_0_Types };
static const Il2CppType* GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_Types[] = { &FB_ProfileImageSize_t3003328130_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t534914198_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t534914198_0_0_0_Types[] = { &KeyValuePair_2_t534914198_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t534914198_0_0_0 = { 1, GenInst_KeyValuePair_2_t534914198_0_0_0_Types };
static const Il2CppType* GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Types[] = { &FB_ProfileImageSize_t3003328130_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_ProfileImageSize_t3003328130_0_0_0 = { 1, GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Types };
static const Il2CppType* GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_FB_ProfileImageSize_t3003328130_0_0_0_Types[] = { &FB_ProfileImageSize_t3003328130_0_0_0, &Il2CppObject_0_0_0, &FB_ProfileImageSize_t3003328130_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_FB_ProfileImageSize_t3003328130_0_0_0 = { 3, GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_FB_ProfileImageSize_t3003328130_0_0_0_Types };
static const Il2CppType* GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &FB_ProfileImageSize_t3003328130_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &FB_ProfileImageSize_t3003328130_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t534914198_0_0_0_Types[] = { &FB_ProfileImageSize_t3003328130_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t534914198_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t534914198_0_0_0 = { 3, GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t534914198_0_0_0_Types };
static const Il2CppType* GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Texture2D_t3542995729_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &FB_ProfileImageSize_t3003328130_0_0_0, &Texture2D_t3542995729_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Texture2D_t3542995729_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Texture2D_t3542995729_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1388460632_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1388460632_0_0_0_Types[] = { &KeyValuePair_2_t1388460632_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1388460632_0_0_0 = { 1, GenInst_KeyValuePair_2_t1388460632_0_0_0_Types };
extern const Il2CppType AndroidInstagramManager_t2759207936_0_0_0;
static const Il2CppType* GenInst_AndroidInstagramManager_t2759207936_0_0_0_Types[] = { &AndroidInstagramManager_t2759207936_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidInstagramManager_t2759207936_0_0_0 = { 1, GenInst_AndroidInstagramManager_t2759207936_0_0_0_Types };
extern const Il2CppType InstagramPostResult_t436083195_0_0_0;
static const Il2CppType* GenInst_InstagramPostResult_t436083195_0_0_0_Types[] = { &InstagramPostResult_t436083195_0_0_0 };
extern const Il2CppGenericInst GenInst_InstagramPostResult_t436083195_0_0_0 = { 1, GenInst_InstagramPostResult_t436083195_0_0_0_Types };
extern const Il2CppType TwitterApplicationOnlyToken_t3636409970_0_0_0;
static const Il2CppType* GenInst_TwitterApplicationOnlyToken_t3636409970_0_0_0_Types[] = { &TwitterApplicationOnlyToken_t3636409970_0_0_0 };
extern const Il2CppGenericInst GenInst_TwitterApplicationOnlyToken_t3636409970_0_0_0 = { 1, GenInst_TwitterApplicationOnlyToken_t3636409970_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_TweetTemplate_t3444491657_0_0_0_Types[] = { &String_t_0_0_0, &TweetTemplate_t3444491657_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TweetTemplate_t3444491657_0_0_0 = { 2, GenInst_String_t_0_0_0_TweetTemplate_t3444491657_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_TweetTemplate_t3444491657_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &TweetTemplate_t3444491657_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TweetTemplate_t3444491657_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_TweetTemplate_t3444491657_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3116616141_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3116616141_0_0_0_Types[] = { &KeyValuePair_2_t3116616141_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3116616141_0_0_0 = { 1, GenInst_KeyValuePair_2_t3116616141_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_TwitterUserInfo_t87370740_0_0_0_Types[] = { &String_t_0_0_0, &TwitterUserInfo_t87370740_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TwitterUserInfo_t87370740_0_0_0 = { 2, GenInst_String_t_0_0_0_TwitterUserInfo_t87370740_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_TwitterUserInfo_t87370740_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &TwitterUserInfo_t87370740_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TwitterUserInfo_t87370740_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_TwitterUserInfo_t87370740_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4054462520_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4054462520_0_0_0_Types[] = { &KeyValuePair_2_t4054462520_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4054462520_0_0_0 = { 1, GenInst_KeyValuePair_2_t4054462520_0_0_0_Types };
extern const Il2CppType TextMesh_t1641806576_0_0_0;
static const Il2CppType* GenInst_TextMesh_t1641806576_0_0_0_Types[] = { &TextMesh_t1641806576_0_0_0 };
extern const Il2CppGenericInst GenInst_TextMesh_t1641806576_0_0_0 = { 1, GenInst_TextMesh_t1641806576_0_0_0_Types };
extern const Il2CppType SALevelLoader_t4125067327_0_0_0;
static const Il2CppType* GenInst_SALevelLoader_t4125067327_0_0_0_Types[] = { &SALevelLoader_t4125067327_0_0_0 };
extern const Il2CppGenericInst GenInst_SALevelLoader_t4125067327_0_0_0 = { 1, GenInst_SALevelLoader_t4125067327_0_0_0_Types };
extern const Il2CppType JNSimpleObjectModel_t1947941312_0_0_0;
static const Il2CppType* GenInst_JNSimpleObjectModel_t1947941312_0_0_0_Types[] = { &JNSimpleObjectModel_t1947941312_0_0_0 };
extern const Il2CppGenericInst GenInst_JNSimpleObjectModel_t1947941312_0_0_0 = { 1, GenInst_JNSimpleObjectModel_t1947941312_0_0_0_Types };
extern const Il2CppType JsonConverter_t1964060750_0_0_0;
static const Il2CppType* GenInst_JsonConverter_t1964060750_0_0_0_Types[] = { &JsonConverter_t1964060750_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonConverter_t1964060750_0_0_0 = { 1, GenInst_JsonConverter_t1964060750_0_0_0_Types };
extern const Il2CppType SimpleClassObject_t1988087395_0_0_0;
static const Il2CppType* GenInst_SimpleClassObject_t1988087395_0_0_0_Types[] = { &SimpleClassObject_t1988087395_0_0_0 };
extern const Il2CppGenericInst GenInst_SimpleClassObject_t1988087395_0_0_0 = { 1, GenInst_SimpleClassObject_t1988087395_0_0_0_Types };
extern const Il2CppType SampleBase_t2925764113_0_0_0;
static const Il2CppType* GenInst_SampleBase_t2925764113_0_0_0_Types[] = { &SampleBase_t2925764113_0_0_0 };
extern const Il2CppGenericInst GenInst_SampleBase_t2925764113_0_0_0 = { 1, GenInst_SampleBase_t2925764113_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_SampleBase_t2925764113_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &SampleBase_t2925764113_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_SampleBase_t2925764113_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_SampleBase_t2925764113_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_SampleBase_t2925764113_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &SampleBase_t2925764113_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_SampleBase_t2925764113_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_SampleBase_t2925764113_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3985902266_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3985902266_0_0_0_Types[] = { &KeyValuePair_2_t3985902266_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3985902266_0_0_0 = { 1, GenInst_KeyValuePair_2_t3985902266_0_0_0_Types };
static const Il2CppType* GenInst_SampleBase_t2925764113_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &SampleBase_t2925764113_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_SampleBase_t2925764113_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_SampleBase_t2925764113_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_SampleBase_t2925764113_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &SampleBase_t2925764113_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_SampleBase_t2925764113_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_SampleBase_t2925764113_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2011661284_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2011661284_0_0_0_Types[] = { &KeyValuePair_2_t2011661284_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2011661284_0_0_0 = { 1, GenInst_KeyValuePair_2_t2011661284_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_SimpleClassObject_t1988087395_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &SimpleClassObject_t1988087395_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_SimpleClassObject_t1988087395_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_SimpleClassObject_t1988087395_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_SimpleClassObject_t1988087395_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &SimpleClassObject_t1988087395_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_SimpleClassObject_t1988087395_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_SimpleClassObject_t1988087395_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3048225548_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3048225548_0_0_0_Types[] = { &KeyValuePair_2_t3048225548_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3048225548_0_0_0 = { 1, GenInst_KeyValuePair_2_t3048225548_0_0_0_Types };
extern const Il2CppType JProperty_t2956441399_0_0_0;
static const Il2CppType* GenInst_JProperty_t2956441399_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &JProperty_t2956441399_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_JProperty_t2956441399_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_JProperty_t2956441399_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_JProperty_t2956441399_0_0_0_String_t_0_0_0_Types[] = { &JProperty_t2956441399_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_JProperty_t2956441399_0_0_0_String_t_0_0_0 = { 2, GenInst_JProperty_t2956441399_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_JProperty_t2956441399_0_0_0_Types[] = { &JProperty_t2956441399_0_0_0 };
extern const Il2CppGenericInst GenInst_JProperty_t2956441399_0_0_0 = { 1, GenInst_JProperty_t2956441399_0_0_0_Types };
extern const Il2CppType BsonProperty_t1491061775_0_0_0;
static const Il2CppType* GenInst_BsonProperty_t1491061775_0_0_0_Types[] = { &BsonProperty_t1491061775_0_0_0 };
extern const Il2CppGenericInst GenInst_BsonProperty_t1491061775_0_0_0 = { 1, GenInst_BsonProperty_t1491061775_0_0_0_Types };
extern const Il2CppType BsonToken_t3582361217_0_0_0;
static const Il2CppType* GenInst_BsonToken_t3582361217_0_0_0_Types[] = { &BsonToken_t3582361217_0_0_0 };
extern const Il2CppGenericInst GenInst_BsonToken_t3582361217_0_0_0 = { 1, GenInst_BsonToken_t3582361217_0_0_0_Types };
extern const Il2CppType ContainerContext_t2144264477_0_0_0;
static const Il2CppType* GenInst_ContainerContext_t2144264477_0_0_0_Types[] = { &ContainerContext_t2144264477_0_0_0 };
extern const Il2CppGenericInst GenInst_ContainerContext_t2144264477_0_0_0 = { 1, GenInst_ContainerContext_t2144264477_0_0_0_Types };
extern const Il2CppType BidirectionalDictionary_2_t2874502390_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t2874502390_0_0_0_Types[] = { &Type_t_0_0_0, &BidirectionalDictionary_2_t2874502390_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t2874502390_0_0_0 = { 2, GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t2874502390_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t2874502390_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &BidirectionalDictionary_2_t2874502390_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t2874502390_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t2874502390_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_BidirectionalDictionary_2_t2874502390_0_0_0_Types[] = { &BidirectionalDictionary_2_t2874502390_0_0_0 };
extern const Il2CppGenericInst GenInst_BidirectionalDictionary_2_t2874502390_0_0_0 = { 1, GenInst_BidirectionalDictionary_2_t2874502390_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2569205509_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2569205509_0_0_0_Types[] = { &KeyValuePair_2_t2569205509_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2569205509_0_0_0 = { 1, GenInst_KeyValuePair_2_t2569205509_0_0_0_Types };
extern const Il2CppType EnumMemberAttribute_t187433993_0_0_0;
static const Il2CppType* GenInst_EnumMemberAttribute_t187433993_0_0_0_String_t_0_0_0_Types[] = { &EnumMemberAttribute_t187433993_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumMemberAttribute_t187433993_0_0_0_String_t_0_0_0 = { 2, GenInst_EnumMemberAttribute_t187433993_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_EnumMemberAttribute_t187433993_0_0_0_Types[] = { &EnumMemberAttribute_t187433993_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumMemberAttribute_t187433993_0_0_0 = { 1, GenInst_EnumMemberAttribute_t187433993_0_0_0_Types };
extern const Il2CppType IXmlNode_t1152344546_0_0_0;
static const Il2CppType* GenInst_IXmlNode_t1152344546_0_0_0_Types[] = { &IXmlNode_t1152344546_0_0_0 };
extern const Il2CppGenericInst GenInst_IXmlNode_t1152344546_0_0_0 = { 1, GenInst_IXmlNode_t1152344546_0_0_0_Types };
static const Il2CppType* GenInst_XmlNode_t616554813_0_0_0_IXmlNode_t1152344546_0_0_0_Types[] = { &XmlNode_t616554813_0_0_0, &IXmlNode_t1152344546_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNode_t616554813_0_0_0_IXmlNode_t1152344546_0_0_0 = { 2, GenInst_XmlNode_t616554813_0_0_0_IXmlNode_t1152344546_0_0_0_Types };
extern const Il2CppType XmlAttribute_t175731005_0_0_0;
static const Il2CppType* GenInst_XmlAttribute_t175731005_0_0_0_Types[] = { &XmlAttribute_t175731005_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttribute_t175731005_0_0_0 = { 1, GenInst_XmlAttribute_t175731005_0_0_0_Types };
static const Il2CppType* GenInst_XmlAttribute_t175731005_0_0_0_IXmlNode_t1152344546_0_0_0_Types[] = { &XmlAttribute_t175731005_0_0_0, &IXmlNode_t1152344546_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttribute_t175731005_0_0_0_IXmlNode_t1152344546_0_0_0 = { 2, GenInst_XmlAttribute_t175731005_0_0_0_IXmlNode_t1152344546_0_0_0_Types };
static const Il2CppType* GenInst_IXmlNode_t1152344546_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &IXmlNode_t1152344546_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_IXmlNode_t1152344546_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_IXmlNode_t1152344546_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType List_1_t521465678_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t521465678_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t521465678_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t521465678_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t521465678_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t521465678_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t521465678_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t521465678_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t521465678_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t521465678_0_0_0_Types[] = { &List_1_t521465678_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t521465678_0_0_0 = { 1, GenInst_List_1_t521465678_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t193590162_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t193590162_0_0_0_Types[] = { &KeyValuePair_2_t193590162_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t193590162_0_0_0 = { 1, GenInst_KeyValuePair_2_t193590162_0_0_0_Types };
extern const Il2CppType IXmlElement_t2005722770_0_0_0;
static const Il2CppType* GenInst_IXmlElement_t2005722770_0_0_0_Types[] = { &IXmlElement_t2005722770_0_0_0 };
extern const Il2CppGenericInst GenInst_IXmlElement_t2005722770_0_0_0 = { 1, GenInst_IXmlElement_t2005722770_0_0_0_Types };
static const Il2CppType* GenInst_IXmlElement_t2005722770_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &IXmlElement_t2005722770_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_IXmlElement_t2005722770_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_IXmlElement_t2005722770_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType NullValueHandling_t3618095365_0_0_0;
static const Il2CppType* GenInst_NullValueHandling_t3618095365_0_0_0_Types[] = { &NullValueHandling_t3618095365_0_0_0 };
extern const Il2CppGenericInst GenInst_NullValueHandling_t3618095365_0_0_0 = { 1, GenInst_NullValueHandling_t3618095365_0_0_0_Types };
extern const Il2CppType DefaultValueHandling_t3457895463_0_0_0;
static const Il2CppType* GenInst_DefaultValueHandling_t3457895463_0_0_0_Types[] = { &DefaultValueHandling_t3457895463_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultValueHandling_t3457895463_0_0_0 = { 1, GenInst_DefaultValueHandling_t3457895463_0_0_0_Types };
extern const Il2CppType ReferenceLoopHandling_t1017855894_0_0_0;
static const Il2CppType* GenInst_ReferenceLoopHandling_t1017855894_0_0_0_Types[] = { &ReferenceLoopHandling_t1017855894_0_0_0 };
extern const Il2CppGenericInst GenInst_ReferenceLoopHandling_t1017855894_0_0_0 = { 1, GenInst_ReferenceLoopHandling_t1017855894_0_0_0_Types };
extern const Il2CppType ObjectCreationHandling_t3720134651_0_0_0;
static const Il2CppType* GenInst_ObjectCreationHandling_t3720134651_0_0_0_Types[] = { &ObjectCreationHandling_t3720134651_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectCreationHandling_t3720134651_0_0_0 = { 1, GenInst_ObjectCreationHandling_t3720134651_0_0_0_Types };
extern const Il2CppType TypeNameHandling_t1331513094_0_0_0;
static const Il2CppType* GenInst_TypeNameHandling_t1331513094_0_0_0_Types[] = { &TypeNameHandling_t1331513094_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameHandling_t1331513094_0_0_0 = { 1, GenInst_TypeNameHandling_t1331513094_0_0_0_Types };
extern const Il2CppType JTokenType_t1307827213_0_0_0;
static const Il2CppType* GenInst_JTokenType_t1307827213_0_0_0_Types[] = { &JTokenType_t1307827213_0_0_0 };
extern const Il2CppGenericInst GenInst_JTokenType_t1307827213_0_0_0 = { 1, GenInst_JTokenType_t1307827213_0_0_0_Types };
extern const Il2CppType ErrorEventArgs_t3365615597_0_0_0;
static const Il2CppType* GenInst_ErrorEventArgs_t3365615597_0_0_0_Types[] = { &ErrorEventArgs_t3365615597_0_0_0 };
extern const Il2CppGenericInst GenInst_ErrorEventArgs_t3365615597_0_0_0 = { 1, GenInst_ErrorEventArgs_t3365615597_0_0_0_Types };
extern const Il2CppType JsonSchemaModel_t708894576_0_0_0;
static const Il2CppType* GenInst_JsonSchemaModel_t708894576_0_0_0_Types[] = { &JsonSchemaModel_t708894576_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchemaModel_t708894576_0_0_0 = { 1, GenInst_JsonSchemaModel_t708894576_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonSchemaModel_t708894576_0_0_0_Types[] = { &String_t_0_0_0, &JsonSchemaModel_t708894576_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonSchemaModel_t708894576_0_0_0 = { 2, GenInst_String_t_0_0_0_JsonSchemaModel_t708894576_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t381019060_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t381019060_0_0_0_Types[] = { &KeyValuePair_2_t381019060_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t381019060_0_0_0 = { 1, GenInst_KeyValuePair_2_t381019060_0_0_0_Types };
extern const Il2CppType SchemaScope_t4218888543_0_0_0;
static const Il2CppType* GenInst_SchemaScope_t4218888543_0_0_0_Types[] = { &SchemaScope_t4218888543_0_0_0 };
extern const Il2CppGenericInst GenInst_SchemaScope_t4218888543_0_0_0 = { 1, GenInst_SchemaScope_t4218888543_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3497699202_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &KeyValuePair_2_t3497699202_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3497699202_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyValuePair_2_t3497699202_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3497699202_0_0_0_String_t_0_0_0_Types[] = { &KeyValuePair_2_t3497699202_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3497699202_0_0_0_String_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3497699202_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType JsonSchemaType_t1742745177_0_0_0;
static const Il2CppType* GenInst_JsonSchemaType_t1742745177_0_0_0_Types[] = { &JsonSchemaType_t1742745177_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchemaType_t1742745177_0_0_0 = { 1, GenInst_JsonSchemaType_t1742745177_0_0_0_Types };
extern const Il2CppType JToken_t2552644013_0_0_0;
static const Il2CppType* GenInst_JToken_t2552644013_0_0_0_Types[] = { &JToken_t2552644013_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t2552644013_0_0_0 = { 1, GenInst_JToken_t2552644013_0_0_0_Types };
static const Il2CppType* GenInst_JsonSchemaModel_t708894576_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &JsonSchemaModel_t708894576_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchemaModel_t708894576_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_JsonSchemaModel_t708894576_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t381019060_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &KeyValuePair_2_t381019060_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t381019060_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyValuePair_2_t381019060_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t381019060_0_0_0_String_t_0_0_0_Types[] = { &KeyValuePair_2_t381019060_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t381019060_0_0_0_String_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t381019060_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2321347278_0_0_0;
static const Il2CppType* GenInst_JsonSchemaModel_t708894576_0_0_0_IEnumerable_1_t2321347278_0_0_0_Types[] = { &JsonSchemaModel_t708894576_0_0_0, &IEnumerable_1_t2321347278_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchemaModel_t708894576_0_0_0_IEnumerable_1_t2321347278_0_0_0 = { 2, GenInst_JsonSchemaModel_t708894576_0_0_0_IEnumerable_1_t2321347278_0_0_0_Types };
extern const Il2CppType StateU5BU5D_t823784359_0_0_0;
static const Il2CppType* GenInst_StateU5BU5D_t823784359_0_0_0_Types[] = { &StateU5BU5D_t823784359_0_0_0 };
extern const Il2CppGenericInst GenInst_StateU5BU5D_t823784359_0_0_0 = { 1, GenInst_StateU5BU5D_t823784359_0_0_0_Types };
extern const Il2CppType State_t3285832914_0_0_0;
static const Il2CppType* GenInst_State_t3285832914_0_0_0_Types[] = { &State_t3285832914_0_0_0 };
extern const Il2CppGenericInst GenInst_State_t3285832914_0_0_0 = { 1, GenInst_State_t3285832914_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JToken_t2552644013_0_0_0_Types[] = { &String_t_0_0_0, &JToken_t2552644013_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JToken_t2552644013_0_0_0 = { 2, GenInst_String_t_0_0_0_JToken_t2552644013_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2224768497_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2224768497_0_0_0_Types[] = { &KeyValuePair_2_t2224768497_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2224768497_0_0_0 = { 1, GenInst_KeyValuePair_2_t2224768497_0_0_0_Types };
static const Il2CppType* GenInst_JProperty_t2956441399_0_0_0_JToken_t2552644013_0_0_0_Types[] = { &JProperty_t2956441399_0_0_0, &JToken_t2552644013_0_0_0 };
extern const Il2CppGenericInst GenInst_JProperty_t2956441399_0_0_0_JToken_t2552644013_0_0_0 = { 2, GenInst_JProperty_t2956441399_0_0_0_JToken_t2552644013_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JToken_t2552644013_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &JToken_t2552644013_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JToken_t2552644013_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_JToken_t2552644013_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType JsonToken_t620654565_0_0_0;
static const Il2CppType* GenInst_JsonToken_t620654565_0_0_0_Types[] = { &JsonToken_t620654565_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonToken_t620654565_0_0_0 = { 1, GenInst_JsonToken_t620654565_0_0_0_Types };
extern const Il2CppType JValue_t300956845_0_0_0;
static const Il2CppType* GenInst_JValue_t300956845_0_0_0_Types[] = { &JValue_t300956845_0_0_0 };
extern const Il2CppGenericInst GenInst_JValue_t300956845_0_0_0 = { 1, GenInst_JValue_t300956845_0_0_0_Types };
extern const Il2CppType JObject_t278519297_0_0_0;
extern const Il2CppType IEnumerable_1_t3248568444_0_0_0;
static const Il2CppType* GenInst_JObject_t278519297_0_0_0_IEnumerable_1_t3248568444_0_0_0_Types[] = { &JObject_t278519297_0_0_0, &IEnumerable_1_t3248568444_0_0_0 };
extern const Il2CppGenericInst GenInst_JObject_t278519297_0_0_0_IEnumerable_1_t3248568444_0_0_0 = { 2, GenInst_JObject_t278519297_0_0_0_IEnumerable_1_t3248568444_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2844771058_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_IEnumerable_1_t2844771058_0_0_0_Types[] = { &Il2CppObject_0_0_0, &IEnumerable_1_t2844771058_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_IEnumerable_1_t2844771058_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_IEnumerable_1_t2844771058_0_0_0_Types };
static const Il2CppType* GenInst_JObject_t278519297_0_0_0_Types[] = { &JObject_t278519297_0_0_0 };
extern const Il2CppGenericInst GenInst_JObject_t278519297_0_0_0 = { 1, GenInst_JObject_t278519297_0_0_0_Types };
extern const Il2CppType JsonSchema_t3772113849_0_0_0;
static const Il2CppType* GenInst_JsonSchema_t3772113849_0_0_0_Types[] = { &JsonSchema_t3772113849_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchema_t3772113849_0_0_0 = { 1, GenInst_JsonSchema_t3772113849_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonSchema_t3772113849_0_0_0_Types[] = { &String_t_0_0_0, &JsonSchema_t3772113849_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonSchema_t3772113849_0_0_0 = { 2, GenInst_String_t_0_0_0_JsonSchema_t3772113849_0_0_0_Types };
static const Il2CppType* GenInst_JToken_t2552644013_0_0_0_String_t_0_0_0_Types[] = { &JToken_t2552644013_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t2552644013_0_0_0_String_t_0_0_0 = { 2, GenInst_JToken_t2552644013_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_JToken_t2552644013_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &JToken_t2552644013_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t2552644013_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_JToken_t2552644013_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1891585177_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1891585177_0_0_0_Types[] = { &KeyValuePair_2_t1891585177_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1891585177_0_0_0 = { 1, GenInst_KeyValuePair_2_t1891585177_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonSchema_t3772113849_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &JsonSchema_t3772113849_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonSchema_t3772113849_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_JsonSchema_t3772113849_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3444238333_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3444238333_0_0_0_Types[] = { &KeyValuePair_2_t3444238333_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3444238333_0_0_0 = { 1, GenInst_KeyValuePair_2_t3444238333_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonSchemaType_t1742745177_0_0_0_Types[] = { &String_t_0_0_0, &JsonSchemaType_t1742745177_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonSchemaType_t1742745177_0_0_0 = { 2, GenInst_String_t_0_0_0_JsonSchemaType_t1742745177_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_Types[] = { &Il2CppObject_0_0_0, &JsonSchemaType_t1742745177_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3387117823_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3387117823_0_0_0_Types[] = { &KeyValuePair_2_t3387117823_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3387117823_0_0_0 = { 1, GenInst_KeyValuePair_2_t3387117823_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1414869661_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1414869661_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &KeyValuePair_2_t1414869661_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1414869661_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyValuePair_2_t1414869661_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3387117823_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &KeyValuePair_2_t3387117823_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3387117823_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyValuePair_2_t3387117823_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1414869661_0_0_0_Types[] = { &KeyValuePair_2_t1414869661_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1414869661_0_0_0 = { 1, GenInst_KeyValuePair_2_t1414869661_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &JsonSchemaType_t1742745177_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_JsonSchemaType_t1742745177_0_0_0_Types[] = { &Il2CppObject_0_0_0, &JsonSchemaType_t1742745177_0_0_0, &JsonSchemaType_t1742745177_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_JsonSchemaType_t1742745177_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_JsonSchemaType_t1742745177_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &JsonSchemaType_t1742745177_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_KeyValuePair_2_t3387117823_0_0_0_Types[] = { &Il2CppObject_0_0_0, &JsonSchemaType_t1742745177_0_0_0, &KeyValuePair_2_t3387117823_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_KeyValuePair_2_t3387117823_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_KeyValuePair_2_t3387117823_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonSchemaType_t1742745177_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &JsonSchemaType_t1742745177_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonSchemaType_t1742745177_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_JsonSchemaType_t1742745177_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType TypeSchema_t460462092_0_0_0;
static const Il2CppType* GenInst_TypeSchema_t460462092_0_0_0_Types[] = { &TypeSchema_t460462092_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeSchema_t460462092_0_0_0 = { 1, GenInst_TypeSchema_t460462092_0_0_0_Types };
extern const Il2CppType EnumValue_1_t589082027_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_EnumValue_1_t589082027_0_0_0_Types[] = { &String_t_0_0_0, &EnumValue_1_t589082027_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_EnumValue_1_t589082027_0_0_0 = { 2, GenInst_String_t_0_0_0_EnumValue_1_t589082027_0_0_0_Types };
static const Il2CppType* GenInst_EnumValue_1_t589082027_0_0_0_Types[] = { &EnumValue_1_t589082027_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumValue_1_t589082027_0_0_0 = { 1, GenInst_EnumValue_1_t589082027_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_EnumValue_1_t589082027_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &EnumValue_1_t589082027_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_EnumValue_1_t589082027_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_EnumValue_1_t589082027_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_TypeSchema_t460462092_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &TypeSchema_t460462092_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeSchema_t460462092_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_TypeSchema_t460462092_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType JsonProperty_t2712067825_0_0_0;
static const Il2CppType* GenInst_JsonProperty_t2712067825_0_0_0_Types[] = { &JsonProperty_t2712067825_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t2712067825_0_0_0 = { 1, GenInst_JsonProperty_t2712067825_0_0_0_Types };
extern const Il2CppType JsonSchemaNode_t3866831117_0_0_0;
static const Il2CppType* GenInst_JsonSchemaNode_t3866831117_0_0_0_JsonSchemaModel_t708894576_0_0_0_Types[] = { &JsonSchemaNode_t3866831117_0_0_0, &JsonSchemaModel_t708894576_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchemaNode_t3866831117_0_0_0_JsonSchemaModel_t708894576_0_0_0 = { 2, GenInst_JsonSchemaNode_t3866831117_0_0_0_JsonSchemaModel_t708894576_0_0_0_Types };
static const Il2CppType* GenInst_JsonSchemaNode_t3866831117_0_0_0_Types[] = { &JsonSchemaNode_t3866831117_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchemaNode_t3866831117_0_0_0 = { 1, GenInst_JsonSchemaNode_t3866831117_0_0_0_Types };
static const Il2CppType* GenInst_JsonSchemaNode_t3866831117_0_0_0_JsonSchemaModel_t708894576_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &JsonSchemaNode_t3866831117_0_0_0, &JsonSchemaModel_t708894576_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchemaNode_t3866831117_0_0_0_JsonSchemaModel_t708894576_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_JsonSchemaNode_t3866831117_0_0_0_JsonSchemaModel_t708894576_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1389980064_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1389980064_0_0_0_Types[] = { &KeyValuePair_2_t1389980064_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1389980064_0_0_0 = { 1, GenInst_KeyValuePair_2_t1389980064_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonSchemaNode_t3866831117_0_0_0_Types[] = { &String_t_0_0_0, &JsonSchemaNode_t3866831117_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonSchemaNode_t3866831117_0_0_0 = { 2, GenInst_String_t_0_0_0_JsonSchemaNode_t3866831117_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonSchemaNode_t3866831117_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &JsonSchemaNode_t3866831117_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonSchemaNode_t3866831117_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_JsonSchemaNode_t3866831117_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3538955601_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3538955601_0_0_0_Types[] = { &KeyValuePair_2_t3538955601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3538955601_0_0_0 = { 1, GenInst_KeyValuePair_2_t3538955601_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonSchemaModel_t708894576_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &JsonSchemaModel_t708894576_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonSchemaModel_t708894576_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_JsonSchemaModel_t708894576_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_JsonSchema_t3772113849_0_0_0_String_t_0_0_0_Types[] = { &JsonSchema_t3772113849_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchema_t3772113849_0_0_0_String_t_0_0_0 = { 2, GenInst_JsonSchema_t3772113849_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_JsonSchema_t3772113849_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &JsonSchema_t3772113849_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchema_t3772113849_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_JsonSchema_t3772113849_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_JsonSchemaType_t1742745177_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &JsonSchemaType_t1742745177_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchemaType_t1742745177_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_JsonSchemaType_t1742745177_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType EnumValue_1_t2589200904_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_EnumValue_1_t2589200904_0_0_0_Types[] = { &String_t_0_0_0, &EnumValue_1_t2589200904_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_EnumValue_1_t2589200904_0_0_0 = { 2, GenInst_String_t_0_0_0_EnumValue_1_t2589200904_0_0_0_Types };
static const Il2CppType* GenInst_EnumValue_1_t2589200904_0_0_0_Types[] = { &EnumValue_1_t2589200904_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumValue_1_t2589200904_0_0_0 = { 1, GenInst_EnumValue_1_t2589200904_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_EnumValue_1_t2589200904_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &EnumValue_1_t2589200904_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_EnumValue_1_t2589200904_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_EnumValue_1_t2589200904_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_EnumValue_1_t2589200904_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &EnumValue_1_t2589200904_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumValue_1_t2589200904_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_EnumValue_1_t2589200904_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Il2CppObject_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ResolverContractKey_t1785396551_0_0_0;
static const Il2CppType* GenInst_ResolverContractKey_t1785396551_0_0_0_Types[] = { &ResolverContractKey_t1785396551_0_0_0 };
extern const Il2CppGenericInst GenInst_ResolverContractKey_t1785396551_0_0_0 = { 1, GenInst_ResolverContractKey_t1785396551_0_0_0_Types };
extern const Il2CppType JsonContract_t1566984540_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_JsonContract_t1566984540_0_0_0_Types[] = { &Type_t_0_0_0, &JsonContract_t1566984540_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_JsonContract_t1566984540_0_0_0 = { 2, GenInst_Type_t_0_0_0_JsonContract_t1566984540_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_JsonContract_t1566984540_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &JsonContract_t1566984540_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_JsonContract_t1566984540_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_JsonContract_t1566984540_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &MemberInfo_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_MemberInfo_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_ConstructorInfo_t2851816542_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &ConstructorInfo_t2851816542_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t2851816542_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_ConstructorInfo_t2851816542_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_JsonProperty_t2712067825_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &JsonProperty_t2712067825_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t2712067825_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_JsonProperty_t2712067825_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_String_t_0_0_0_Types[] = { &Il2CppObject_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_String_t_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType TypeNameKey_t3055062677_0_0_0;
static const Il2CppType* GenInst_TypeNameKey_t3055062677_0_0_0_Type_t_0_0_0_Types[] = { &TypeNameKey_t3055062677_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t3055062677_0_0_0_Type_t_0_0_0 = { 2, GenInst_TypeNameKey_t3055062677_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_Types[] = { &TypeNameKey_t3055062677_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t94122711_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t94122711_0_0_0_Types[] = { &KeyValuePair_2_t94122711_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t94122711_0_0_0 = { 1, GenInst_KeyValuePair_2_t94122711_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t3055062677_0_0_0_Types[] = { &TypeNameKey_t3055062677_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t3055062677_0_0_0 = { 1, GenInst_TypeNameKey_t3055062677_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_TypeNameKey_t3055062677_0_0_0_Types[] = { &TypeNameKey_t3055062677_0_0_0, &Il2CppObject_0_0_0, &TypeNameKey_t3055062677_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_TypeNameKey_t3055062677_0_0_0 = { 3, GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_TypeNameKey_t3055062677_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &TypeNameKey_t3055062677_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &TypeNameKey_t3055062677_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t94122711_0_0_0_Types[] = { &TypeNameKey_t3055062677_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t94122711_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t94122711_0_0_0 = { 3, GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t94122711_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t3055062677_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &TypeNameKey_t3055062677_0_0_0, &Type_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t3055062677_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_TypeNameKey_t3055062677_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 2, GenInst_DictionaryEntry_t3048875398_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonProperty_t2712067825_0_0_0_Types[] = { &String_t_0_0_0, &JsonProperty_t2712067825_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonProperty_t2712067825_0_0_0 = { 2, GenInst_String_t_0_0_0_JsonProperty_t2712067825_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonProperty_t2712067825_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &JsonProperty_t2712067825_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonProperty_t2712067825_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_JsonProperty_t2712067825_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_ParameterInfo_t2249040075_0_0_0_ParameterInfo_t2249040075_0_0_0_Types[] = { &ParameterInfo_t2249040075_0_0_0, &ParameterInfo_t2249040075_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0_ParameterInfo_t2249040075_0_0_0 = { 2, GenInst_ParameterInfo_t2249040075_0_0_0_ParameterInfo_t2249040075_0_0_0_Types };
static const Il2CppType* GenInst_ParameterInfo_t2249040075_0_0_0_Il2CppObject_0_0_0_Types[] = { &ParameterInfo_t2249040075_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ParameterInfo_t2249040075_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2995057417_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2995057417_0_0_0_String_t_0_0_0_Types[] = { &KeyValuePair_2_t2995057417_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2995057417_0_0_0_String_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2995057417_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_JsonProperty_t2712067825_0_0_0_JsonProperty_t2712067825_0_0_0_Types[] = { &JsonProperty_t2712067825_0_0_0, &JsonProperty_t2712067825_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t2712067825_0_0_0_JsonProperty_t2712067825_0_0_0 = { 2, GenInst_JsonProperty_t2712067825_0_0_0_JsonProperty_t2712067825_0_0_0_Types };
extern const Il2CppType PropertyPresence_t220200932_0_0_0;
static const Il2CppType* GenInst_JsonProperty_t2712067825_0_0_0_PropertyPresence_t220200932_0_0_0_Types[] = { &JsonProperty_t2712067825_0_0_0, &PropertyPresence_t220200932_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t2712067825_0_0_0_PropertyPresence_t220200932_0_0_0 = { 2, GenInst_JsonProperty_t2712067825_0_0_0_PropertyPresence_t220200932_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyPresence_t220200932_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_Types };
static const Il2CppType* GenInst_IList_t3321498491_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &IList_t3321498491_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t3321498491_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_IList_t3321498491_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_JsonProperty_t2712067825_0_0_0_Il2CppObject_0_0_0_Types[] = { &JsonProperty_t2712067825_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t2712067825_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_JsonProperty_t2712067825_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t554510731_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t554510731_0_0_0_Types[] = { &KeyValuePair_2_t554510731_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t554510731_0_0_0 = { 1, GenInst_KeyValuePair_2_t554510731_0_0_0_Types };
static const Il2CppType* GenInst_ParameterInfo_t2249040075_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ParameterInfo_t2249040075_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ParameterInfo_t2249040075_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_JsonProperty_t2712067825_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &JsonProperty_t2712067825_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t2712067825_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_JsonProperty_t2712067825_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_String_t_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_String_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2995057417_0_0_0_Types[] = { &KeyValuePair_2_t2995057417_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2995057417_0_0_0 = { 1, GenInst_KeyValuePair_2_t2995057417_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1864573578_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1864573578_0_0_0_Types[] = { &KeyValuePair_2_t1864573578_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1864573578_0_0_0 = { 1, GenInst_KeyValuePair_2_t1864573578_0_0_0_Types };
static const Il2CppType* GenInst_PropertyPresence_t220200932_0_0_0_Types[] = { &PropertyPresence_t220200932_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyPresence_t220200932_0_0_0 = { 1, GenInst_PropertyPresence_t220200932_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyPresence_t220200932_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_PropertyPresence_t220200932_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyPresence_t220200932_0_0_0, &PropertyPresence_t220200932_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_PropertyPresence_t220200932_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_PropertyPresence_t220200932_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyPresence_t220200932_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_KeyValuePair_2_t1864573578_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyPresence_t220200932_0_0_0, &KeyValuePair_2_t1864573578_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_KeyValuePair_2_t1864573578_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_KeyValuePair_2_t1864573578_0_0_0_Types };
static const Il2CppType* GenInst_JsonProperty_t2712067825_0_0_0_PropertyPresence_t220200932_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &JsonProperty_t2712067825_0_0_0, &PropertyPresence_t220200932_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t2712067825_0_0_0_PropertyPresence_t220200932_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_JsonProperty_t2712067825_0_0_0_PropertyPresence_t220200932_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2380229664_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2380229664_0_0_0_Types[] = { &KeyValuePair_2_t2380229664_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2380229664_0_0_0 = { 1, GenInst_KeyValuePair_2_t2380229664_0_0_0_Types };
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Type_t_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0_Type_t_0_0_0 = { 2, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0, &Type_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Type_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &Type_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType JsonContainerAttribute_t47210975_0_0_0;
static const Il2CppType* GenInst_JsonContainerAttribute_t47210975_0_0_0_Types[] = { &JsonContainerAttribute_t47210975_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonContainerAttribute_t47210975_0_0_0 = { 1, GenInst_JsonContainerAttribute_t47210975_0_0_0_Types };
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_JsonContainerAttribute_t47210975_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0, &JsonContainerAttribute_t47210975_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0_JsonContainerAttribute_t47210975_0_0_0 = { 2, GenInst_ICustomAttributeProvider_t502202687_0_0_0_JsonContainerAttribute_t47210975_0_0_0_Types };
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_JsonContainerAttribute_t47210975_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0, &JsonContainerAttribute_t47210975_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0_JsonContainerAttribute_t47210975_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICustomAttributeProvider_t502202687_0_0_0_JsonContainerAttribute_t47210975_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType DataContractAttribute_t3332255060_0_0_0;
static const Il2CppType* GenInst_DataContractAttribute_t3332255060_0_0_0_Types[] = { &DataContractAttribute_t3332255060_0_0_0 };
extern const Il2CppGenericInst GenInst_DataContractAttribute_t3332255060_0_0_0 = { 1, GenInst_DataContractAttribute_t3332255060_0_0_0_Types };
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataContractAttribute_t3332255060_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0, &DataContractAttribute_t3332255060_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataContractAttribute_t3332255060_0_0_0 = { 2, GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataContractAttribute_t3332255060_0_0_0_Types };
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataContractAttribute_t3332255060_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0, &DataContractAttribute_t3332255060_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataContractAttribute_t3332255060_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataContractAttribute_t3332255060_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType DataMemberAttribute_t2677019114_0_0_0;
static const Il2CppType* GenInst_DataMemberAttribute_t2677019114_0_0_0_Types[] = { &DataMemberAttribute_t2677019114_0_0_0 };
extern const Il2CppGenericInst GenInst_DataMemberAttribute_t2677019114_0_0_0 = { 1, GenInst_DataMemberAttribute_t2677019114_0_0_0_Types };
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataMemberAttribute_t2677019114_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0, &DataMemberAttribute_t2677019114_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataMemberAttribute_t2677019114_0_0_0 = { 2, GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataMemberAttribute_t2677019114_0_0_0_Types };
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataMemberAttribute_t2677019114_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0, &DataMemberAttribute_t2677019114_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataMemberAttribute_t2677019114_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataMemberAttribute_t2677019114_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType IList_1_t3230389896_0_0_0;
static const Il2CppType* GenInst_IList_1_t3230389896_0_0_0_Types[] = { &IList_1_t3230389896_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3230389896_0_0_0 = { 1, GenInst_IList_1_t3230389896_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IList_1_t3230389896_0_0_0_Il2CppObject_0_0_0_Types[] = { &Type_t_0_0_0, &IList_1_t3230389896_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IList_1_t3230389896_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Type_t_0_0_0_IList_1_t3230389896_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType TypeConvertKey_t1788482786_0_0_0;
extern const Il2CppType Func_2_t2825504181_0_0_0;
static const Il2CppType* GenInst_TypeConvertKey_t1788482786_0_0_0_Func_2_t2825504181_0_0_0_Types[] = { &TypeConvertKey_t1788482786_0_0_0, &Func_2_t2825504181_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t1788482786_0_0_0_Func_2_t2825504181_0_0_0 = { 2, GenInst_TypeConvertKey_t1788482786_0_0_0_Func_2_t2825504181_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_Types[] = { &TypeConvertKey_t1788482786_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4204318902_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4204318902_0_0_0_Types[] = { &KeyValuePair_2_t4204318902_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4204318902_0_0_0 = { 1, GenInst_KeyValuePair_2_t4204318902_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t1788482786_0_0_0_Types[] = { &TypeConvertKey_t1788482786_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t1788482786_0_0_0 = { 1, GenInst_TypeConvertKey_t1788482786_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3992464955_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3992464955_0_0_0_Types[] = { &IEquatable_1_t3992464955_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3992464955_0_0_0 = { 1, GenInst_IEquatable_1_t3992464955_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_TypeConvertKey_t1788482786_0_0_0_Types[] = { &TypeConvertKey_t1788482786_0_0_0, &Il2CppObject_0_0_0, &TypeConvertKey_t1788482786_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_TypeConvertKey_t1788482786_0_0_0 = { 3, GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_TypeConvertKey_t1788482786_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &TypeConvertKey_t1788482786_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &TypeConvertKey_t1788482786_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4204318902_0_0_0_Types[] = { &TypeConvertKey_t1788482786_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t4204318902_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4204318902_0_0_0 = { 3, GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4204318902_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t1788482786_0_0_0_Func_2_t2825504181_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &TypeConvertKey_t1788482786_0_0_0, &Func_2_t2825504181_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t1788482786_0_0_0_Func_2_t2825504181_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_TypeConvertKey_t1788482786_0_0_0_Func_2_t2825504181_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &FieldInfo_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_FieldInfo_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_String_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0_String_t_0_0_0 = { 2, GenInst_MemberInfo_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType IGrouping_2_t288337164_0_0_0;
extern const Il2CppType U3CU3E__AnonType0_2_t99735840_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t288337164_0_0_0_U3CU3E__AnonType0_2_t99735840_0_0_0_Types[] = { &IGrouping_2_t288337164_0_0_0, &U3CU3E__AnonType0_2_t99735840_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t288337164_0_0_0_U3CU3E__AnonType0_2_t99735840_0_0_0 = { 2, GenInst_IGrouping_2_t288337164_0_0_0_U3CU3E__AnonType0_2_t99735840_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_Types[] = { &String_t_0_0_0, &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MemberInfo_t_0_0_0 = { 2, GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t40257009_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_IEnumerable_1_t40257009_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &IEnumerable_1_t40257009_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_IEnumerable_1_t40257009_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_IEnumerable_1_t40257009_0_0_0_Types };
static const Il2CppType* GenInst_ParameterInfo_t2249040075_0_0_0_Type_t_0_0_0_Types[] = { &ParameterInfo_t2249040075_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0_Type_t_0_0_0 = { 2, GenInst_ParameterInfo_t2249040075_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType0_2_t99735840_0_0_0_Types[] = { &U3CU3E__AnonType0_2_t99735840_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType0_2_t99735840_0_0_0 = { 1, GenInst_U3CU3E__AnonType0_2_t99735840_0_0_0_Types };
static const Il2CppType* GenInst_IGrouping_2_t288337164_0_0_0_Types[] = { &IGrouping_2_t288337164_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t288337164_0_0_0 = { 1, GenInst_IGrouping_2_t288337164_0_0_0_Types };
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &PropertyInfo_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_PropertyInfo_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_List_1_t2058570427_0_0_0_Types[] = { &Type_t_0_0_0, &List_1_t2058570427_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_List_1_t2058570427_0_0_0 = { 2, GenInst_Type_t_0_0_0_List_1_t2058570427_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &List_1_t2058570427_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1753273546_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1753273546_0_0_0_Types[] = { &KeyValuePair_2_t1753273546_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1753273546_0_0_0 = { 1, GenInst_KeyValuePair_2_t1753273546_0_0_0_Types };
extern const Il2CppType List_1_t3876342518_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_List_1_t3876342518_0_0_0_Types[] = { &Type_t_0_0_0, &List_1_t3876342518_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_List_1_t3876342518_0_0_0 = { 2, GenInst_Type_t_0_0_0_List_1_t3876342518_0_0_0_Types };
extern const Il2CppType TypeData_t212254090_0_0_0;
static const Il2CppType* GenInst_TypeData_t212254090_0_0_0_Types[] = { &TypeData_t212254090_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeData_t212254090_0_0_0 = { 1, GenInst_TypeData_t212254090_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_List_1_t3876342518_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &List_1_t3876342518_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_List_1_t3876342518_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_List_1_t3876342518_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t3876342518_0_0_0_Types[] = { &List_1_t3876342518_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3876342518_0_0_0 = { 1, GenInst_List_1_t3876342518_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3571045637_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3571045637_0_0_0_Types[] = { &KeyValuePair_2_t3571045637_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3571045637_0_0_0 = { 1, GenInst_KeyValuePair_2_t3571045637_0_0_0_Types };
static const Il2CppType* GenInst_ConstructorInfo_t2851816542_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &ConstructorInfo_t2851816542_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t2851816542_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_ConstructorInfo_t2851816542_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_TypeData_t212254090_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &TypeData_t212254090_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeData_t212254090_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_TypeData_t212254090_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType DeleteCourseEventArgs_t2630278065_0_0_0;
static const Il2CppType* GenInst_DeleteCourseEventArgs_t2630278065_0_0_0_Types[] = { &DeleteCourseEventArgs_t2630278065_0_0_0 };
extern const Il2CppGenericInst GenInst_DeleteCourseEventArgs_t2630278065_0_0_0 = { 1, GenInst_DeleteCourseEventArgs_t2630278065_0_0_0_Types };
extern const Il2CppType DeleteCourseResponse_t1898856809_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_DeleteCourseResponse_t1898856809_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &DeleteCourseResponse_t1898856809_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_DeleteCourseResponse_t1898856809_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_DeleteCourseResponse_t1898856809_0_0_0_Types };
extern const Il2CppType Course_t3483112699_0_0_0;
static const Il2CppType* GenInst_Course_t3483112699_0_0_0_Types[] = { &Course_t3483112699_0_0_0 };
extern const Il2CppGenericInst GenInst_Course_t3483112699_0_0_0 = { 1, GenInst_Course_t3483112699_0_0_0_Types };
extern const Il2CppType DeleteCourseRequest_t1683798751_0_0_0;
static const Il2CppType* GenInst_DeleteCourseRequest_t1683798751_0_0_0_Types[] = { &DeleteCourseRequest_t1683798751_0_0_0 };
extern const Il2CppGenericInst GenInst_DeleteCourseRequest_t1683798751_0_0_0 = { 1, GenInst_DeleteCourseRequest_t1683798751_0_0_0_Types };
extern const Il2CppType Jump_t114869516_0_0_0;
static const Il2CppType* GenInst_Jump_t114869516_0_0_0_Types[] = { &Jump_t114869516_0_0_0 };
extern const Il2CppGenericInst GenInst_Jump_t114869516_0_0_0 = { 1, GenInst_Jump_t114869516_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3132015601_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0 = { 1, GenInst_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t115911300_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t115911300_0_0_0_Types[] = { &KeyValuePair_2_t115911300_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t115911300_0_0_0 = { 1, GenInst_KeyValuePair_2_t115911300_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 3, GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t115911300_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t115911300_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t115911300_0_0_0 = { 3, GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t115911300_0_0_0_Types };
extern const Il2CppType GetCourseResponse_t2659116210_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_GetCourseResponse_t2659116210_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &GetCourseResponse_t2659116210_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_GetCourseResponse_t2659116210_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_GetCourseResponse_t2659116210_0_0_0_Types };
extern const Il2CppType CreateCourseResponse_t3800683746_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_CreateCourseResponse_t3800683746_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &CreateCourseResponse_t3800683746_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_CreateCourseResponse_t3800683746_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_CreateCourseResponse_t3800683746_0_0_0_Types };
extern const Il2CppType CreateCourseRequest_t4267998642_0_0_0;
static const Il2CppType* GenInst_CreateCourseRequest_t4267998642_0_0_0_Types[] = { &CreateCourseRequest_t4267998642_0_0_0 };
extern const Il2CppGenericInst GenInst_CreateCourseRequest_t4267998642_0_0_0 = { 1, GenInst_CreateCourseRequest_t4267998642_0_0_0_Types };
extern const Il2CppType GetCourseRequest_t2656580458_0_0_0;
static const Il2CppType* GenInst_GetCourseRequest_t2656580458_0_0_0_Types[] = { &GetCourseRequest_t2656580458_0_0_0 };
extern const Il2CppGenericInst GenInst_GetCourseRequest_t2656580458_0_0_0 = { 1, GenInst_GetCourseRequest_t2656580458_0_0_0_Types };
extern const Il2CppType ShowDetailsEventArgs_t1623880762_0_0_0;
static const Il2CppType* GenInst_ShowDetailsEventArgs_t1623880762_0_0_0_Types[] = { &ShowDetailsEventArgs_t1623880762_0_0_0 };
extern const Il2CppGenericInst GenInst_ShowDetailsEventArgs_t1623880762_0_0_0 = { 1, GenInst_ShowDetailsEventArgs_t1623880762_0_0_0_Types };
extern const Il2CppType ShowErrorEventArgs_t2135289798_0_0_0;
static const Il2CppType* GenInst_ShowErrorEventArgs_t2135289798_0_0_0_Types[] = { &ShowErrorEventArgs_t2135289798_0_0_0 };
extern const Il2CppGenericInst GenInst_ShowErrorEventArgs_t2135289798_0_0_0 = { 1, GenInst_ShowErrorEventArgs_t2135289798_0_0_0_Types };
extern const Il2CppType ForgotPasswordResponse_t4062332037_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_ForgotPasswordResponse_t4062332037_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &ForgotPasswordResponse_t4062332037_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_ForgotPasswordResponse_t4062332037_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_ForgotPasswordResponse_t4062332037_0_0_0_Types };
extern const Il2CppType ChangePasswordResponse_t3897893034_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_ChangePasswordResponse_t3897893034_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &ChangePasswordResponse_t3897893034_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_ChangePasswordResponse_t3897893034_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_ChangePasswordResponse_t3897893034_0_0_0_Types };
extern const Il2CppType LoginResponse_t1319408382_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_LoginResponse_t1319408382_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &LoginResponse_t1319408382_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_LoginResponse_t1319408382_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_LoginResponse_t1319408382_0_0_0_Types };
extern const Il2CppType RegisterResponse_t410466074_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_RegisterResponse_t410466074_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &RegisterResponse_t410466074_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_RegisterResponse_t410466074_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_RegisterResponse_t410466074_0_0_0_Types };
extern const Il2CppType LoginRequest_t2273172322_0_0_0;
static const Il2CppType* GenInst_LoginRequest_t2273172322_0_0_0_Types[] = { &LoginRequest_t2273172322_0_0_0 };
extern const Il2CppGenericInst GenInst_LoginRequest_t2273172322_0_0_0 = { 1, GenInst_LoginRequest_t2273172322_0_0_0_Types };
extern const Il2CppType RegisterRequest_t698226714_0_0_0;
static const Il2CppType* GenInst_RegisterRequest_t698226714_0_0_0_Types[] = { &RegisterRequest_t698226714_0_0_0 };
extern const Il2CppGenericInst GenInst_RegisterRequest_t698226714_0_0_0 = { 1, GenInst_RegisterRequest_t698226714_0_0_0_Types };
extern const Il2CppType ForgotPasswordRequest_t1886688323_0_0_0;
static const Il2CppType* GenInst_ForgotPasswordRequest_t1886688323_0_0_0_Types[] = { &ForgotPasswordRequest_t1886688323_0_0_0 };
extern const Il2CppGenericInst GenInst_ForgotPasswordRequest_t1886688323_0_0_0 = { 1, GenInst_ForgotPasswordRequest_t1886688323_0_0_0_Types };
extern const Il2CppType ChangePasswordRequest_t4102061450_0_0_0;
static const Il2CppType* GenInst_ChangePasswordRequest_t4102061450_0_0_0_Types[] = { &ChangePasswordRequest_t4102061450_0_0_0 };
extern const Il2CppGenericInst GenInst_ChangePasswordRequest_t4102061450_0_0_0 = { 1, GenInst_ChangePasswordRequest_t4102061450_0_0_0_Types };
extern const Il2CppType ChangeCallback_t1172037850_0_0_0;
static const Il2CppType* GenInst_ChangeCallback_t1172037850_0_0_0_Types[] = { &ChangeCallback_t1172037850_0_0_0 };
extern const Il2CppGenericInst GenInst_ChangeCallback_t1172037850_0_0_0 = { 1, GenInst_ChangeCallback_t1172037850_0_0_0_Types };
static const Il2CppType* GenInst_PurchaseFailureReason_t1322959839_0_0_0_Types[] = { &PurchaseFailureReason_t1322959839_0_0_0 };
extern const Il2CppGenericInst GenInst_PurchaseFailureReason_t1322959839_0_0_0 = { 1, GenInst_PurchaseFailureReason_t1322959839_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4048664256_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types[] = { &IEnumerable_1_t4048664256_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1730553742_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types[] = { &Array_Sort_m1730553742_gp_0_0_0_0, &Array_Sort_m1730553742_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3106198730_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3106198730_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types[] = { &Array_Sort_m3106198730_gp_0_0_0_0, &Array_Sort_m3106198730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2090966156_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0, &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0, &Array_Sort_m1985772939_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2736815140_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types[] = { &Array_Sort_m2736815140_gp_0_0_0_0, &Array_Sort_m2736815140_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2468799988_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2468799988_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types[] = { &Array_Sort_m2468799988_gp_0_0_0_0, &Array_Sort_m2468799988_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2587948790_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0, &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0, &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m52621935_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types[] = { &Array_Sort_m52621935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m52621935_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3546416104_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types[] = { &Array_Sort_m3546416104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3546416104_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0, &Array_qsort_m533480027_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m940423571_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m940423571_gp_0_0_0_0_Types[] = { &Array_compare_m940423571_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m940423571_gp_0_0_0_0 = { 1, GenInst_Array_compare_m940423571_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m565008110_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types[] = { &Array_qsort_m565008110_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m565008110_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m1201602141_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types[] = { &Array_Resize_m1201602141_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m1201602141_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m2783802133_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m2783802133_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m3775633118_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types[] = { &Array_ForEach_m3775633118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m3775633118_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1734974082_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1734974082_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1734974082_gp_0_0_0_0, &Array_ConvertAll_m1734974082_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m934773128_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m934773128_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m3202023711_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m3202023711_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m352384762_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m352384762_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1593955424_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1593955424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1546138173_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1546138173_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1082322798_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1082322798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m525402987_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m525402987_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3577113407_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3577113407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1033585031_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1033585031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3052238307_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3052238307_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1306290405_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1306290405_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2825795862_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2825795862_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2841140625_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2841140625_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3304283431_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3304283431_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3860096562_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3860096562_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m2100440379_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m2100440379_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m982349212_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types[] = { &Array_FindAll_m982349212_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m982349212_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m1825464757_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types[] = { &Array_Exists_m1825464757_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m1825464757_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m1258056624_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m1258056624_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m2529971459_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types[] = { &Array_Find_m2529971459_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m2529971459_gp_0_0_0_0 = { 1, GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m3929249453_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types[] = { &Array_FindLast_m3929249453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3929249453_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t3582267753_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t3582267753_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t3737699284_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t3737699284_gp_0_0_0_0_Types[] = { &IList_1_t3737699284_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3737699284_gp_0_0_0_0 = { 1, GenInst_IList_1_t3737699284_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1552160836_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types[] = { &ICollection_1_t1552160836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1552160836_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1398937014_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types[] = { &Nullable_1_t1398937014_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1398937014_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t1036860714_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types[] = { &Comparer_1_t1036860714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1036860714_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3074655092_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types[] = { &DefaultComparer_t3074655092_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3074655092_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t1787398723_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types[] = { &GenericComparer_1_t1787398723_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3180694294_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0 = { 1, GenInst_KeyValuePair_2_t3180694294_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3895203923_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3895203923_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3895203923_gp_0_0_0_0, &ShimEnumerator_t3895203923_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2089681430_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2089681430_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types[] = { &Enumerator_t2089681430_gp_0_0_0_0, &Enumerator_t2089681430_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3434615342_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3434615342_0_0_0_Types[] = { &KeyValuePair_2_t3434615342_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3434615342_0_0_0 = { 1, GenInst_KeyValuePair_2_t3434615342_0_0_0_Types };
extern const Il2CppType KeyCollection_t1229212677_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t1229212677_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t83320710_gp_0_0_0_0;
extern const Il2CppType Enumerator_t83320710_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0, &Enumerator_t83320710_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0 = { 2, GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0 = { 1, GenInst_Enumerator_t83320710_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t2262344653_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2262344653_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3111723616_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3111723616_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_0_0_0_0, &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_1_0_0_0 = { 1, GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 2, GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 2, GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2066709010_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2066709010_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t1766400012_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types[] = { &DefaultComparer_t1766400012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1766400012_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2202941003_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_1_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4174120762_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4174120762_0_0_0_Types[] = { &KeyValuePair_2_t4174120762_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4174120762_0_0_0 = { 1, GenInst_KeyValuePair_2_t4174120762_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0, &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1988958766_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1988958766_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t1988958766_gp_0_0_0_0, &KeyValuePair_2_t1988958766_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t1169184319_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t1169184319_gp_0_0_0_0_Types[] = { &List_1_t1169184319_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1169184319_gp_0_0_0_0 = { 1, GenInst_List_1_t1169184319_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1292967705_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types[] = { &Enumerator_t1292967705_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1292967705_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t686054069_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t686054069_gp_0_0_0_0_Types[] = { &Collection_1_t686054069_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t686054069_gp_0_0_0_0 = { 1, GenInst_Collection_1_t686054069_gp_0_0_0_0_Types };
extern const Il2CppType KeyedCollection_2_t3154958122_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyedCollection_2_t3154958122_gp_1_0_0_0_Types[] = { &KeyedCollection_2_t3154958122_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedCollection_2_t3154958122_gp_1_0_0_0 = { 1, GenInst_KeyedCollection_2_t3154958122_gp_1_0_0_0_Types };
extern const Il2CppType KeyedCollection_2_t3154958122_gp_0_0_0_0;
static const Il2CppType* GenInst_KeyedCollection_2_t3154958122_gp_0_0_0_0_Types[] = { &KeyedCollection_2_t3154958122_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedCollection_2_t3154958122_gp_0_0_0_0 = { 1, GenInst_KeyedCollection_2_t3154958122_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyedCollection_2_t3154958122_gp_0_0_0_0_KeyedCollection_2_t3154958122_gp_1_0_0_0_Types[] = { &KeyedCollection_2_t3154958122_gp_0_0_0_0, &KeyedCollection_2_t3154958122_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedCollection_2_t3154958122_gp_0_0_0_0_KeyedCollection_2_t3154958122_gp_1_0_0_0 = { 2, GenInst_KeyedCollection_2_t3154958122_gp_0_0_0_0_KeyedCollection_2_t3154958122_gp_1_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t3540981679_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t1001032761_gp_0_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types[] = { &ArraySegment_1_t1001032761_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0 = { 1, GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types };
extern const Il2CppType LinkedList_1_t3556217344_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types[] = { &LinkedList_1_t3556217344_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t3556217344_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4145643798_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types[] = { &Enumerator_t4145643798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4145643798_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t2172356692_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t2172356692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_Intern_m3121628056_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_Intern_m3121628056_gp_0_0_0_0_Types[] = { &RBTree_Intern_m3121628056_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_Intern_m3121628056_gp_0_0_0_0 = { 1, GenInst_RBTree_Intern_m3121628056_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_Remove_m2480195356_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_Remove_m2480195356_gp_0_0_0_0_Types[] = { &RBTree_Remove_m2480195356_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_Remove_m2480195356_gp_0_0_0_0 = { 1, GenInst_RBTree_Remove_m2480195356_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_Lookup_m3378201690_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_Lookup_m3378201690_gp_0_0_0_0_Types[] = { &RBTree_Lookup_m3378201690_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_Lookup_m3378201690_gp_0_0_0_0 = { 1, GenInst_RBTree_Lookup_m3378201690_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_find_key_m2107656200_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_find_key_m2107656200_gp_0_0_0_0_Types[] = { &RBTree_find_key_m2107656200_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_find_key_m2107656200_gp_0_0_0_0 = { 1, GenInst_RBTree_find_key_m2107656200_gp_0_0_0_0_Types };
extern const Il2CppType SortedDictionary_2_t3426860295_gp_0_0_0_0;
static const Il2CppType* GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_Types[] = { &SortedDictionary_2_t3426860295_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0 = { 1, GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_Types };
extern const Il2CppType SortedDictionary_2_t3426860295_gp_1_0_0_0;
static const Il2CppType* GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types[] = { &SortedDictionary_2_t3426860295_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0 = { 1, GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types[] = { &SortedDictionary_2_t3426860295_gp_0_0_0_0, &SortedDictionary_2_t3426860295_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0 = { 2, GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4073929546_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4073929546_0_0_0_Types[] = { &KeyValuePair_2_t4073929546_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4073929546_0_0_0 = { 1, GenInst_KeyValuePair_2_t4073929546_0_0_0_Types };
extern const Il2CppType Node_t1473068371_gp_0_0_0_0;
extern const Il2CppType Node_t1473068371_gp_1_0_0_0;
static const Il2CppType* GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0_Types[] = { &Node_t1473068371_gp_0_0_0_0, &Node_t1473068371_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0 = { 2, GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0_Types };
extern const Il2CppType NodeHelper_t357096749_gp_0_0_0_0;
static const Il2CppType* GenInst_NodeHelper_t357096749_gp_0_0_0_0_Types[] = { &NodeHelper_t357096749_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NodeHelper_t357096749_gp_0_0_0_0 = { 1, GenInst_NodeHelper_t357096749_gp_0_0_0_0_Types };
extern const Il2CppType NodeHelper_t357096749_gp_1_0_0_0;
static const Il2CppType* GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0_Types[] = { &NodeHelper_t357096749_gp_0_0_0_0, &NodeHelper_t357096749_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0 = { 2, GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0_Types };
extern const Il2CppType ValueCollection_t2735575576_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2735575576_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0_Types[] = { &ValueCollection_t2735575576_gp_0_0_0_0, &ValueCollection_t2735575576_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2735575576_gp_1_0_0_0_Types[] = { &ValueCollection_t2735575576_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2735575576_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2735575576_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1353355221_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1353355221_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0_Types[] = { &Enumerator_t1353355221_gp_0_0_0_0, &Enumerator_t1353355221_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t1353355221_gp_1_0_0_0_Types[] = { &Enumerator_t1353355221_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1353355221_gp_1_0_0_0 = { 1, GenInst_Enumerator_t1353355221_gp_1_0_0_0_Types };
extern const Il2CppType KeyCollection_t3620397270_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t3620397270_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0_Types[] = { &KeyCollection_t3620397270_gp_0_0_0_0, &KeyCollection_t3620397270_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t3620397270_gp_0_0_0_0_Types[] = { &KeyCollection_t3620397270_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3620397270_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t3620397270_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4275149413_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4275149413_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0_Types[] = { &Enumerator_t4275149413_gp_0_0_0_0, &Enumerator_t4275149413_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0 = { 2, GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t4275149413_gp_0_0_0_0_Types[] = { &Enumerator_t4275149413_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4275149413_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4275149413_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t824916987_gp_0_0_0_0;
extern const Il2CppType Enumerator_t824916987_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0_Types[] = { &Enumerator_t824916987_gp_0_0_0_0, &Enumerator_t824916987_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0 = { 2, GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t404405498_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t404405498_0_0_0_Types[] = { &KeyValuePair_2_t404405498_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t404405498_0_0_0 = { 1, GenInst_KeyValuePair_2_t404405498_0_0_0_Types };
extern const Il2CppType Stack_1_t4016656541_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types[] = { &Stack_1_t4016656541_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t4016656541_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t546412149_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t546412149_gp_0_0_0_0_Types[] = { &Enumerator_t546412149_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t546412149_gp_0_0_0_0 = { 1, GenInst_Enumerator_t546412149_gp_0_0_0_0_Types };
extern const Il2CppType HashSet_1_t2624254809_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types[] = { &HashSet_1_t2624254809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t2624254809_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2109956843_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types[] = { &Enumerator_t2109956843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2109956843_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3424417428_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types[] = { &PrimeHelper_t3424417428_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3424417428_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Aggregate_m964332100_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Types[] = { &Enumerable_Aggregate_m964332100_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0 = { 1, GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Types[] = { &Enumerable_Aggregate_m964332100_gp_0_0_0_0, &Enumerable_Aggregate_m964332100_gp_0_0_0_0, &Enumerable_Aggregate_m964332100_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0 = { 3, GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_All_m2363499768_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Types[] = { &Enumerable_All_m2363499768_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_All_m2363499768_gp_0_0_0_0 = { 1, GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_All_m2363499768_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m2739389357_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Types[] = { &Enumerable_Any_m2739389357_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Any_m2739389357_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_Cast_m2974870241_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Cast_m2974870241_gp_0_0_0_0_Types[] = { &Enumerable_Cast_m2974870241_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Cast_m2974870241_gp_0_0_0_0 = { 1, GenInst_Enumerable_Cast_m2974870241_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateCastIterator_m3836917259_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateCastIterator_m3836917259_gp_0_0_0_0_Types[] = { &Enumerable_CreateCastIterator_m3836917259_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateCastIterator_m3836917259_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateCastIterator_m3836917259_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m1284016302_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m1284016302_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m4622279_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m4622279_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Count_m1561720045_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0_Types[] = { &Enumerable_Count_m1561720045_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Distinct_m4171187007_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Distinct_m4171187007_gp_0_0_0_0_Types[] = { &Enumerable_Distinct_m4171187007_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Distinct_m4171187007_gp_0_0_0_0 = { 1, GenInst_Enumerable_Distinct_m4171187007_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Distinct_m522457978_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Distinct_m522457978_gp_0_0_0_0_Types[] = { &Enumerable_Distinct_m522457978_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Distinct_m522457978_gp_0_0_0_0 = { 1, GenInst_Enumerable_Distinct_m522457978_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0_Types[] = { &Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Empty_m2393946422_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Empty_m2393946422_gp_0_0_0_0_Types[] = { &Enumerable_Empty_m2393946422_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Empty_m2393946422_gp_0_0_0_0 = { 1, GenInst_Enumerable_Empty_m2393946422_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m1693250038_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m1693250038_gp_0_0_0_0_Types[] = { &Enumerable_First_m1693250038_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m1693250038_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m1693250038_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0;
extern const Il2CppType List_1_t2888087137_0_0_0;
static const Il2CppType* GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0_List_1_t2888087137_0_0_0_Types[] = { &Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0, &List_1_t2888087137_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0_List_1_t2888087137_0_0_0 = { 2, GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0_List_1_t2888087137_0_0_0_Types };
extern const Il2CppType Enumerable_ContainsGroup_m1146262421_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ContainsGroup_m1146262421_gp_1_0_0_0_Types[] = { &Enumerable_ContainsGroup_m1146262421_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ContainsGroup_m1146262421_gp_1_0_0_0 = { 1, GenInst_Enumerable_ContainsGroup_m1146262421_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0_Types[] = { &Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0 = { 1, GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_GroupBy_m1544473094_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Types[] = { &Enumerable_GroupBy_m1544473094_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0 = { 1, GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_GroupBy_m1544473094_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Enumerable_GroupBy_m1544473094_gp_1_0_0_0_Types[] = { &Enumerable_GroupBy_m1544473094_gp_0_0_0_0, &Enumerable_GroupBy_m1544473094_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Enumerable_GroupBy_m1544473094_gp_1_0_0_0 = { 2, GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Enumerable_GroupBy_m1544473094_gp_1_0_0_0_Types };
extern const Il2CppType IGrouping_2_t3129334088_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t3129334088_0_0_0_Types[] = { &IGrouping_2_t3129334088_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t3129334088_0_0_0 = { 1, GenInst_IGrouping_2_t3129334088_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_GroupBy_m1544473094_gp_1_0_0_0_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Types[] = { &Enumerable_GroupBy_m1544473094_gp_1_0_0_0, &Enumerable_GroupBy_m1544473094_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1544473094_gp_1_0_0_0_Enumerable_GroupBy_m1544473094_gp_0_0_0_0 = { 2, GenInst_Enumerable_GroupBy_m1544473094_gp_1_0_0_0_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_GroupBy_m1259221415_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Types[] = { &Enumerable_GroupBy_m1259221415_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0 = { 1, GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_GroupBy_m1259221415_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Types[] = { &Enumerable_GroupBy_m1259221415_gp_0_0_0_0, &Enumerable_GroupBy_m1259221415_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Enumerable_GroupBy_m1259221415_gp_1_0_0_0 = { 2, GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Types[] = { &Enumerable_GroupBy_m1259221415_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0 = { 1, GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Types };
extern const Il2CppType IGrouping_2_t3129334089_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t3129334089_0_0_0_Types[] = { &IGrouping_2_t3129334089_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t3129334089_0_0_0 = { 1, GenInst_IGrouping_2_t3129334089_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Types[] = { &Enumerable_GroupBy_m1259221415_gp_1_0_0_0, &Enumerable_GroupBy_m1259221415_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Enumerable_GroupBy_m1259221415_gp_0_0_0_0 = { 2, GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Types[] = { &Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Types[] = { &Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0, &Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Types[] = { &Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Types };
extern const Il2CppType IGrouping_2_t2588622000_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t2588622000_0_0_0_Types[] = { &IGrouping_2_t2588622000_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t2588622000_0_0_0 = { 1, GenInst_IGrouping_2_t2588622000_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Types[] = { &Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0, &Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0 = { 2, GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_LastOrDefault_m2408605338_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_LastOrDefault_m2408605338_gp_0_0_0_0_Types[] = { &Enumerable_LastOrDefault_m2408605338_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_LastOrDefault_m2408605338_gp_0_0_0_0 = { 1, GenInst_Enumerable_LastOrDefault_m2408605338_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OfType_m192007909_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OfType_m192007909_gp_0_0_0_0_Types[] = { &Enumerable_OfType_m192007909_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OfType_m192007909_gp_0_0_0_0 = { 1, GenInst_Enumerable_OfType_m192007909_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0_Types[] = { &Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m920500904_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m920500904_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m920500904_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m920500904_gp_0_0_0_0, &Enumerable_OrderBy_m920500904_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m4222740363_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m4222740363_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_0_0_0_0, &Enumerable_OrderBy_m4222740363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Repeat_m3228394296_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Repeat_m3228394296_gp_0_0_0_0_Types[] = { &Enumerable_Repeat_m3228394296_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Repeat_m3228394296_gp_0_0_0_0 = { 1, GenInst_Enumerable_Repeat_m3228394296_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateRepeatIterator_m2601674138_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateRepeatIterator_m2601674138_gp_0_0_0_0_Types[] = { &Enumerable_CreateRepeatIterator_m2601674138_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateRepeatIterator_m2601674138_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateRepeatIterator_m2601674138_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0, &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m3508258668_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Types[] = { &Enumerable_Select_m3508258668_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m3508258668_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0_Types[] = { &Enumerable_Select_m3508258668_gp_0_0_0_0, &Int32_t2071877448_0_0_0, &Enumerable_Select_m3508258668_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0 = { 3, GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m3508258668_gp_1_0_0_0_Types[] = { &Enumerable_Select_m3508258668_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m3508258668_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m3508258668_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0_Types[] = { &Enumerable_Select_m3508258668_gp_0_0_0_0, &Enumerable_Select_m3508258668_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0, &Int32_t2071877448_0_0_0, &Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0 = { 3, GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_SelectMany_m608128666_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_Types[] = { &Enumerable_SelectMany_m608128666_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0 = { 1, GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t380606227_0_0_0;
static const Il2CppType* GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_IEnumerable_1_t380606227_0_0_0_Types[] = { &Enumerable_SelectMany_m608128666_gp_0_0_0_0, &IEnumerable_1_t380606227_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_IEnumerable_1_t380606227_0_0_0 = { 2, GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_IEnumerable_1_t380606227_0_0_0_Types };
extern const Il2CppType Enumerable_SelectMany_m608128666_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_SelectMany_m608128666_gp_1_0_0_0_Types[] = { &Enumerable_SelectMany_m608128666_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m608128666_gp_1_0_0_0 = { 1, GenInst_Enumerable_SelectMany_m608128666_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_Enumerable_SelectMany_m608128666_gp_1_0_0_0_Types[] = { &Enumerable_SelectMany_m608128666_gp_0_0_0_0, &Enumerable_SelectMany_m608128666_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_Enumerable_SelectMany_m608128666_gp_1_0_0_0 = { 2, GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_Enumerable_SelectMany_m608128666_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t567845041_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_IEnumerable_1_t567845041_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0, &IEnumerable_1_t567845041_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_IEnumerable_1_t567845041_0_0_0 = { 2, GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_IEnumerable_1_t567845041_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0, &Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Single_m1979188101_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Types[] = { &Enumerable_Single_m1979188101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0 = { 1, GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Single_m1979188101_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_Single_m2776017211_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0_Types[] = { &Enumerable_Single_m2776017211_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0 = { 1, GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Single_m2776017211_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_SingleOrDefault_m760899018_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_SingleOrDefault_m760899018_gp_0_0_0_0_Types[] = { &Enumerable_SingleOrDefault_m760899018_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m760899018_gp_0_0_0_0 = { 1, GenInst_Enumerable_SingleOrDefault_m760899018_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0_Types[] = { &Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0 = { 1, GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_Take_m169782875_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Take_m169782875_gp_0_0_0_0_Types[] = { &Enumerable_Take_m169782875_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Take_m169782875_gp_0_0_0_0 = { 1, GenInst_Enumerable_Take_m169782875_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateTakeIterator_m1267606521_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateTakeIterator_m1267606521_gp_0_0_0_0_Types[] = { &Enumerable_CreateTakeIterator_m1267606521_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateTakeIterator_m1267606521_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateTakeIterator_m1267606521_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m2343256994_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m2343256994_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m3729734315_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m3729734315_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m3729734315_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Enumerable_ToDictionary_m3729734315_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m3729734315_gp_0_0_0_0, &Enumerable_ToDictionary_m3729734315_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Enumerable_ToDictionary_m3729734315_gp_1_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Enumerable_ToDictionary_m3729734315_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m3729734315_gp_2_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Enumerable_ToDictionary_m3729734315_gp_2_0_0_0_Types[] = { &Enumerable_ToDictionary_m3729734315_gp_0_0_0_0, &Enumerable_ToDictionary_m3729734315_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Enumerable_ToDictionary_m3729734315_gp_2_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Enumerable_ToDictionary_m3729734315_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3729734315_gp_1_0_0_0_Enumerable_ToDictionary_m3729734315_gp_2_0_0_0_Types[] = { &Enumerable_ToDictionary_m3729734315_gp_1_0_0_0, &Enumerable_ToDictionary_m3729734315_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3729734315_gp_1_0_0_0_Enumerable_ToDictionary_m3729734315_gp_2_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m3729734315_gp_1_0_0_0_Enumerable_ToDictionary_m3729734315_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Enumerable_ToDictionary_m3729734315_gp_1_0_0_0_Enumerable_ToDictionary_m3729734315_gp_2_0_0_0_Types[] = { &Enumerable_ToDictionary_m3729734315_gp_0_0_0_0, &Enumerable_ToDictionary_m3729734315_gp_1_0_0_0, &Enumerable_ToDictionary_m3729734315_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Enumerable_ToDictionary_m3729734315_gp_1_0_0_0_Enumerable_ToDictionary_m3729734315_gp_2_0_0_0 = { 3, GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Enumerable_ToDictionary_m3729734315_gp_1_0_0_0_Enumerable_ToDictionary_m3729734315_gp_2_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m3027976024_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m3027976024_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m3027976024_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m3027976024_gp_0_0_0_0, &Enumerable_ToDictionary_m3027976024_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m3027976024_gp_2_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0_Types[] = { &Enumerable_ToDictionary_m3027976024_gp_0_0_0_0, &Enumerable_ToDictionary_m3027976024_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m3027976024_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0_Types[] = { &Enumerable_ToDictionary_m3027976024_gp_1_0_0_0, &Enumerable_ToDictionary_m3027976024_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m2810079530_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m2810079530_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m2810079530_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m2810079530_gp_0_0_0_0, &Enumerable_ToDictionary_m2810079530_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m2810079530_gp_1_0_0_0, &Enumerable_ToDictionary_m2810079530_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m3284215677_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m3284215677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m3284215677_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m3284215677_gp_0_0_0_0, &Enumerable_ToDictionary_m3284215677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m3284215677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m3284215677_gp_1_0_0_0, &Enumerable_ToDictionary_m3284215677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m3284215677_gp_0_0_0_0, &Enumerable_ToDictionary_m3284215677_gp_1_0_0_0, &Enumerable_ToDictionary_m3284215677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0 = { 3, GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m261161385_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m261161385_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Union_m2318613702_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Union_m2318613702_gp_0_0_0_0_Types[] = { &Enumerable_Union_m2318613702_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Union_m2318613702_gp_0_0_0_0 = { 1, GenInst_Enumerable_Union_m2318613702_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Union_m2720478133_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Union_m2720478133_gp_0_0_0_0_Types[] = { &Enumerable_Union_m2720478133_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Union_m2720478133_gp_0_0_0_0 = { 1, GenInst_Enumerable_Union_m2720478133_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateUnionIterator_m1338750539_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateUnionIterator_m1338750539_gp_0_0_0_0_Types[] = { &Enumerable_CreateUnionIterator_m1338750539_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateUnionIterator_m1338750539_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateUnionIterator_m1338750539_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m2409552823_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Function_1_t1491613575_gp_0_0_0_0;
static const Il2CppType* GenInst_Function_1_t1491613575_gp_0_0_0_0_Function_1_t1491613575_gp_0_0_0_0_Types[] = { &Function_1_t1491613575_gp_0_0_0_0, &Function_1_t1491613575_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Function_1_t1491613575_gp_0_0_0_0_Function_1_t1491613575_gp_0_0_0_0 = { 2, GenInst_Function_1_t1491613575_gp_0_0_0_0_Function_1_t1491613575_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Function_1_t1491613575_gp_0_0_0_0_Types[] = { &Function_1_t1491613575_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Function_1_t1491613575_gp_0_0_0_0 = { 1, GenInst_Function_1_t1491613575_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t54777913_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t54777913_gp_0_0_0_0_Types[] = { &U3CCreateCastIteratorU3Ec__Iterator0_1_t54777913_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t54777913_gp_0_0_0_0 = { 1, GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t54777913_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0_Types[] = { &U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0 = { 1, GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0;
extern const Il2CppType U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0, &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0 = { 2, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_Types };
extern const Il2CppType IGrouping_2_t1824977620_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t1824977620_0_0_0_Types[] = { &IGrouping_2_t1824977620_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t1824977620_0_0_0 = { 1, GenInst_IGrouping_2_t1824977620_0_0_0_Types };
extern const Il2CppType List_1_t3797843219_0_0_0;
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_List_1_t3797843219_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0, &List_1_t3797843219_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_List_1_t3797843219_0_0_0 = { 2, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_List_1_t3797843219_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0 = { 1, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0, &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0 = { 2, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0 = { 1, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0_Types[] = { &U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0 = { 1, GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateRepeatIteratorU3Ec__IteratorE_1_t139808608_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateRepeatIteratorU3Ec__IteratorE_1_t139808608_gp_0_0_0_0_Types[] = { &U3CCreateRepeatIteratorU3Ec__IteratorE_1_t139808608_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateRepeatIteratorU3Ec__IteratorE_1_t139808608_gp_0_0_0_0 = { 1, GenInst_U3CCreateRepeatIteratorU3Ec__IteratorE_1_t139808608_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_Int32_t2071877448_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0, &Int32_t2071877448_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_Int32_t2071877448_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0 = { 3, GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_Int32_t2071877448_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3792789022_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_IEnumerable_1_t3792789022_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0, &IEnumerable_1_t3792789022_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_IEnumerable_1_t3792789022_0_0_0 = { 2, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_IEnumerable_1_t3792789022_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0, &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateTakeIteratorU3Ec__Iterator19_1_t4103837823_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t4103837823_gp_0_0_0_0_Types[] = { &U3CCreateTakeIteratorU3Ec__Iterator19_1_t4103837823_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t4103837823_gp_0_0_0_0 = { 1, GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t4103837823_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateUnionIteratorU3Ec__Iterator1C_1_t1794850093_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateUnionIteratorU3Ec__Iterator1C_1_t1794850093_gp_0_0_0_0_Types[] = { &U3CCreateUnionIteratorU3Ec__Iterator1C_1_t1794850093_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateUnionIteratorU3Ec__Iterator1C_1_t1794850093_gp_0_0_0_0 = { 1, GenInst_U3CCreateUnionIteratorU3Ec__Iterator1C_1_t1794850093_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Grouping_2_t288249757_gp_1_0_0_0;
static const Il2CppType* GenInst_Grouping_2_t288249757_gp_1_0_0_0_Types[] = { &Grouping_2_t288249757_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Grouping_2_t288249757_gp_1_0_0_0 = { 1, GenInst_Grouping_2_t288249757_gp_1_0_0_0_Types };
extern const Il2CppType Grouping_2_t288249757_gp_0_0_0_0;
static const Il2CppType* GenInst_Grouping_2_t288249757_gp_0_0_0_0_Grouping_2_t288249757_gp_1_0_0_0_Types[] = { &Grouping_2_t288249757_gp_0_0_0_0, &Grouping_2_t288249757_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Grouping_2_t288249757_gp_0_0_0_0_Grouping_2_t288249757_gp_1_0_0_0 = { 2, GenInst_Grouping_2_t288249757_gp_0_0_0_0_Grouping_2_t288249757_gp_1_0_0_0_Types };
extern const Il2CppType IGrouping_2_t845301090_gp_1_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t845301090_gp_1_0_0_0_Types[] = { &IGrouping_2_t845301090_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t845301090_gp_1_0_0_0 = { 1, GenInst_IGrouping_2_t845301090_gp_1_0_0_0_Types };
extern const Il2CppType IOrderedEnumerable_1_t641749975_gp_0_0_0_0;
static const Il2CppType* GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types[] = { &IOrderedEnumerable_1_t641749975_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0 = { 1, GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types };
extern const Il2CppType OrderedEnumerable_1_t753306046_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types[] = { &OrderedEnumerable_1_t753306046_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0 = { 1, GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1023848160_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1023848160_gp_1_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_0_0_0_0, &OrderedSequence_2_t1023848160_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 2, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
extern const Il2CppType QuickSort_1_t1290221672_gp_0_0_0_0;
static const Il2CppType* GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types[] = { &QuickSort_1_t1290221672_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_QuickSort_1_t1290221672_gp_0_0_0_0 = { 1, GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types };
extern const Il2CppType U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types[] = { &U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0 = { 1, GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types };
extern const Il2CppType SortContext_1_t4088581714_gp_0_0_0_0;
static const Il2CppType* GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types[] = { &SortContext_1_t4088581714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortContext_1_t4088581714_gp_0_0_0_0 = { 1, GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_0_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_1_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_0_0_0_0, &SortSequenceContext_2_t3419387730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 2, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types[] = { &Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SafeLength_m3101579087_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types[] = { &Mesh_SafeLength_m3101579087_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m3999848894_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m4171325764_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types[] = { &Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m894835059_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m894835059_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m3417738402_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m825036157_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m825036157_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m3873375864_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m1600202230_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m3990064736_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types[] = { &Component_GetComponents_m3990064736_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m2051523689_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types[] = { &Component_GetComponents_m2051523689_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types[] = { &GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m2621570726_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m2621570726_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject_Call_m1094633808_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject_Call_m1094633808_gp_0_0_0_0_Types[] = { &AndroidJavaObject_Call_m1094633808_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject_Call_m1094633808_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject_Call_m1094633808_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject_CallStatic_m946265290_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject_CallStatic_m946265290_gp_0_0_0_0_Types[] = { &AndroidJavaObject_CallStatic_m946265290_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject_CallStatic_m946265290_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject_CallStatic_m946265290_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject__Call_m4019607101_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject__Call_m4019607101_gp_0_0_0_0_Types[] = { &AndroidJavaObject__Call_m4019607101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject__Call_m4019607101_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject__Call_m4019607101_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject__CallStatic_m1525952853_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject__CallStatic_m1525952853_gp_0_0_0_0_Types[] = { &AndroidJavaObject__CallStatic_m1525952853_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject__CallStatic_m1525952853_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject__CallStatic_m1525952853_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJNIHelper_ConvertFromJNIArray_m2082383440_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJNIHelper_ConvertFromJNIArray_m2082383440_gp_0_0_0_0_Types[] = { &AndroidJNIHelper_ConvertFromJNIArray_m2082383440_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJNIHelper_ConvertFromJNIArray_m2082383440_gp_0_0_0_0 = { 1, GenInst_AndroidJNIHelper_ConvertFromJNIArray_m2082383440_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJNIHelper_GetMethodID_m2221772144_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJNIHelper_GetMethodID_m2221772144_gp_0_0_0_0_Types[] = { &AndroidJNIHelper_GetMethodID_m2221772144_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJNIHelper_GetMethodID_m2221772144_gp_0_0_0_0 = { 1, GenInst_AndroidJNIHelper_GetMethodID_m2221772144_gp_0_0_0_0_Types };
extern const Il2CppType GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0_Types[] = { &GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0 = { 1, GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0_Types };
extern const Il2CppType AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0;
static const Il2CppType* GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0_Types[] = { &AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0 = { 1, GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0_Types };
extern const Il2CppType CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0;
static const Il2CppType* GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0_Types[] = { &CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0 = { 1, GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0_Types };
extern const Il2CppType ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0;
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0_Types[] = { &ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0, &ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0 = { 2, GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0_Types[] = { &ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0 = { 1, GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0_Types[] = { &ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0 = { 1, GenInst_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1319939458_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1319939458_0_0_0_Types[] = { &KeyValuePair_2_t1319939458_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1319939458_0_0_0 = { 1, GenInst_KeyValuePair_2_t1319939458_0_0_0_Types };
extern const Il2CppType _AndroidJNIHelper_GetMethodID_m656615819_gp_0_0_0_0;
static const Il2CppType* GenInst__AndroidJNIHelper_GetMethodID_m656615819_gp_0_0_0_0_Types[] = { &_AndroidJNIHelper_GetMethodID_m656615819_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst__AndroidJNIHelper_GetMethodID_m656615819_gp_0_0_0_0 = { 1, GenInst__AndroidJNIHelper_GetMethodID_m656615819_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t476640868_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types[] = { &InvokableCall_1_t476640868_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t476640868_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t2042724809_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2042724809_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0, &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3608808750_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0, &InvokableCall_3_t3608808750_gp_1_0_0_0, &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t879925395_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0, &InvokableCall_4_t879925395_gp_1_0_0_0, &InvokableCall_4_t879925395_gp_2_0_0_0, &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t224769006_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t224769006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t4075366602_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types[] = { &UnityEvent_1_t4075366602_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t4075366599_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4075366599_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types[] = { &UnityEvent_2_t4075366599_gp_0_0_0_0, &UnityEvent_2_t4075366599_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t4075366600_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types[] = { &UnityEvent_3_t4075366600_gp_0_0_0_0, &UnityEvent_3_t4075366600_gp_1_0_0_0, &UnityEvent_3_t4075366600_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t4075366597_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types[] = { &UnityEvent_4_t4075366597_gp_0_0_0_0, &UnityEvent_4_t4075366597_gp_1_0_0_0, &UnityEvent_4_t4075366597_gp_2_0_0_0, &UnityEvent_4_t4075366597_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types };
extern const Il2CppType ConfigurationBuilder_Configure_m2155711039_gp_0_0_0_0;
static const Il2CppType* GenInst_ConfigurationBuilder_Configure_m2155711039_gp_0_0_0_0_Types[] = { &ConfigurationBuilder_Configure_m2155711039_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigurationBuilder_Configure_m2155711039_gp_0_0_0_0 = { 1, GenInst_ConfigurationBuilder_Configure_m2155711039_gp_0_0_0_0_Types };
extern const Il2CppType AbstractPurchasingModule_BindExtension_m1993884112_gp_0_0_0_0;
static const Il2CppType* GenInst_AbstractPurchasingModule_BindExtension_m1993884112_gp_0_0_0_0_Types[] = { &AbstractPurchasingModule_BindExtension_m1993884112_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AbstractPurchasingModule_BindExtension_m1993884112_gp_0_0_0_0 = { 1, GenInst_AbstractPurchasingModule_BindExtension_m1993884112_gp_0_0_0_0_Types };
extern const Il2CppType AbstractPurchasingModule_BindConfiguration_m3787044503_gp_0_0_0_0;
static const Il2CppType* GenInst_AbstractPurchasingModule_BindConfiguration_m3787044503_gp_0_0_0_0_Types[] = { &AbstractPurchasingModule_BindConfiguration_m3787044503_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AbstractPurchasingModule_BindConfiguration_m3787044503_gp_0_0_0_0 = { 1, GenInst_AbstractPurchasingModule_BindConfiguration_m3787044503_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m1961163955_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t2584777480_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types[] = { &TweenRunner_1_t2584777480_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types };
extern const Il2CppType SetPropertyUtility_SetEquatableStruct_m753578693_gp_0_0_0_0;
static const Il2CppType* GenInst_SetPropertyUtility_SetEquatableStruct_m753578693_gp_0_0_0_0_Types[] = { &SetPropertyUtility_SetEquatableStruct_m753578693_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetEquatableStruct_m753578693_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetEquatableStruct_m753578693_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t573160278_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType ListPool_1_t1984115411_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types[] = { &ListPool_1_t1984115411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t1984115411_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2000868992_0_0_0;
static const Il2CppType* GenInst_List_1_t2000868992_0_0_0_Types[] = { &List_1_t2000868992_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2000868992_0_0_0 = { 1, GenInst_List_1_t2000868992_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t4265859154_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types[] = { &ObjectPool_1_t4265859154_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types };
extern const Il2CppType FakeStore_StartUI_m935561654_gp_0_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_FakeStore_StartUI_m935561654_gp_0_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &FakeStore_StartUI_m935561654_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_FakeStore_StartUI_m935561654_gp_0_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_FakeStore_StartUI_m935561654_gp_0_0_0_0_Types };
extern const Il2CppType UIFakeStore_StartUI_m1214015556_gp_0_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &UIFakeStore_StartUI_m1214015556_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0_Types[] = { &UIFakeStore_StartUI_m1214015556_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0 = { 1, GenInst_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0_Types };
extern const Il2CppType U3CStartUIU3Ec__AnonStorey0_1_t3316954766_gp_0_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_U3CStartUIU3Ec__AnonStorey0_1_t3316954766_gp_0_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &U3CStartUIU3Ec__AnonStorey0_1_t3316954766_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_U3CStartUIU3Ec__AnonStorey0_1_t3316954766_gp_0_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_U3CStartUIU3Ec__AnonStorey0_1_t3316954766_gp_0_0_0_0_Types };
extern const Il2CppType UnityUtil_GetAnyComponentsOfType_m709219744_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityUtil_GetAnyComponentsOfType_m709219744_gp_0_0_0_0_Types[] = { &UnityUtil_GetAnyComponentsOfType_m709219744_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityUtil_GetAnyComponentsOfType_m709219744_gp_0_0_0_0 = { 1, GenInst_UnityUtil_GetAnyComponentsOfType_m709219744_gp_0_0_0_0_Types };
extern const Il2CppType UnityUtil_LoadResourceInstanceOfType_m463261387_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityUtil_LoadResourceInstanceOfType_m463261387_gp_0_0_0_0_Types[] = { &UnityUtil_LoadResourceInstanceOfType_m463261387_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityUtil_LoadResourceInstanceOfType_m463261387_gp_0_0_0_0 = { 1, GenInst_UnityUtil_LoadResourceInstanceOfType_m463261387_gp_0_0_0_0_Types };
extern const Il2CppType NonMonoSingleton_1_t1282585840_gp_0_0_0_0;
static const Il2CppType* GenInst_NonMonoSingleton_1_t1282585840_gp_0_0_0_0_Types[] = { &NonMonoSingleton_1_t1282585840_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NonMonoSingleton_1_t1282585840_gp_0_0_0_0 = { 1, GenInst_NonMonoSingleton_1_t1282585840_gp_0_0_0_0_Types };
extern const Il2CppType Singleton_1_t4004969804_gp_0_0_0_0;
static const Il2CppType* GenInst_Singleton_1_t4004969804_gp_0_0_0_0_Types[] = { &Singleton_1_t4004969804_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Singleton_1_t4004969804_gp_0_0_0_0 = { 1, GenInst_Singleton_1_t4004969804_gp_0_0_0_0_Types };
extern const Il2CppType SA_Singleton_OLD_1_t2766871217_gp_0_0_0_0;
static const Il2CppType* GenInst_SA_Singleton_OLD_1_t2766871217_gp_0_0_0_0_Types[] = { &SA_Singleton_OLD_1_t2766871217_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SA_Singleton_OLD_1_t2766871217_gp_0_0_0_0 = { 1, GenInst_SA_Singleton_OLD_1_t2766871217_gp_0_0_0_0_Types };
extern const Il2CppType EnumerationExtension_ForEach_m633171965_gp_0_0_0_0;
static const Il2CppType* GenInst_EnumerationExtension_ForEach_m633171965_gp_0_0_0_0_Types[] = { &EnumerationExtension_ForEach_m633171965_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumerationExtension_ForEach_m633171965_gp_0_0_0_0 = { 1, GenInst_EnumerationExtension_ForEach_m633171965_gp_0_0_0_0_Types };
extern const Il2CppType CustomCreationConverter_1_t1690683567_gp_0_0_0_0;
static const Il2CppType* GenInst_CustomCreationConverter_1_t1690683567_gp_0_0_0_0_Types[] = { &CustomCreationConverter_1_t1690683567_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomCreationConverter_1_t1690683567_gp_0_0_0_0 = { 1, GenInst_CustomCreationConverter_1_t1690683567_gp_0_0_0_0_Types };
extern const Il2CppType JsonConvert_DeserializeObject_m2988054426_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonConvert_DeserializeObject_m2988054426_gp_0_0_0_0_Types[] = { &JsonConvert_DeserializeObject_m2988054426_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonConvert_DeserializeObject_m2988054426_gp_0_0_0_0 = { 1, GenInst_JsonConvert_DeserializeObject_m2988054426_gp_0_0_0_0_Types };
extern const Il2CppType JsonConvert_DeserializeAnonymousType_m1519926232_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonConvert_DeserializeAnonymousType_m1519926232_gp_0_0_0_0_Types[] = { &JsonConvert_DeserializeAnonymousType_m1519926232_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonConvert_DeserializeAnonymousType_m1519926232_gp_0_0_0_0 = { 1, GenInst_JsonConvert_DeserializeAnonymousType_m1519926232_gp_0_0_0_0_Types };
extern const Il2CppType IJEnumerable_1_t75840398_gp_0_0_0_0;
static const Il2CppType* GenInst_IJEnumerable_1_t75840398_gp_0_0_0_0_Types[] = { &IJEnumerable_1_t75840398_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IJEnumerable_1_t75840398_gp_0_0_0_0 = { 1, GenInst_IJEnumerable_1_t75840398_gp_0_0_0_0_Types };
extern const Il2CppType JContainer_Values_m3671546765_gp_0_0_0_0;
static const Il2CppType* GenInst_JContainer_Values_m3671546765_gp_0_0_0_0_Types[] = { &JContainer_Values_m3671546765_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JContainer_Values_m3671546765_gp_0_0_0_0 = { 1, GenInst_JContainer_Values_m3671546765_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_JToken_t2552644013_0_0_0_JContainer_Values_m3671546765_gp_0_0_0_0_Types[] = { &JToken_t2552644013_0_0_0, &JContainer_Values_m3671546765_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t2552644013_0_0_0_JContainer_Values_m3671546765_gp_0_0_0_0 = { 2, GenInst_JToken_t2552644013_0_0_0_JContainer_Values_m3671546765_gp_0_0_0_0_Types };
extern const Il2CppType JEnumerable_1_t922559079_gp_0_0_0_0;
static const Il2CppType* GenInst_JEnumerable_1_t922559079_gp_0_0_0_0_Types[] = { &JEnumerable_1_t922559079_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JEnumerable_1_t922559079_gp_0_0_0_0 = { 1, GenInst_JEnumerable_1_t922559079_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_JEnumerable_1_t922559079_gp_0_0_0_0_JToken_t2552644013_0_0_0_Types[] = { &JEnumerable_1_t922559079_gp_0_0_0_0, &JToken_t2552644013_0_0_0 };
extern const Il2CppGenericInst GenInst_JEnumerable_1_t922559079_gp_0_0_0_0_JToken_t2552644013_0_0_0 = { 2, GenInst_JEnumerable_1_t922559079_gp_0_0_0_0_JToken_t2552644013_0_0_0_Types };
extern const Il2CppType JToken_Value_m80405919_gp_0_0_0_0;
static const Il2CppType* GenInst_JToken_t2552644013_0_0_0_JToken_Value_m80405919_gp_0_0_0_0_Types[] = { &JToken_t2552644013_0_0_0, &JToken_Value_m80405919_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t2552644013_0_0_0_JToken_Value_m80405919_gp_0_0_0_0 = { 2, GenInst_JToken_t2552644013_0_0_0_JToken_Value_m80405919_gp_0_0_0_0_Types };
extern const Il2CppType JToken_Children_m1844711917_gp_0_0_0_0;
static const Il2CppType* GenInst_JToken_Children_m1844711917_gp_0_0_0_0_Types[] = { &JToken_Children_m1844711917_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_Children_m1844711917_gp_0_0_0_0 = { 1, GenInst_JToken_Children_m1844711917_gp_0_0_0_0_Types };
extern const Il2CppType JToken_Values_m3688732645_gp_0_0_0_0;
static const Il2CppType* GenInst_JToken_Values_m3688732645_gp_0_0_0_0_Types[] = { &JToken_Values_m3688732645_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_Values_m3688732645_gp_0_0_0_0 = { 1, GenInst_JToken_Values_m3688732645_gp_0_0_0_0_Types };
extern const Il2CppType JToken_ToObject_m1720428526_gp_0_0_0_0;
static const Il2CppType* GenInst_JToken_ToObject_m1720428526_gp_0_0_0_0_Types[] = { &JToken_ToObject_m1720428526_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_ToObject_m1720428526_gp_0_0_0_0 = { 1, GenInst_JToken_ToObject_m1720428526_gp_0_0_0_0_Types };
extern const Il2CppType JToken_ToObject_m2930397507_gp_0_0_0_0;
static const Il2CppType* GenInst_JToken_ToObject_m2930397507_gp_0_0_0_0_Types[] = { &JToken_ToObject_m2930397507_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_ToObject_m2930397507_gp_0_0_0_0 = { 1, GenInst_JToken_ToObject_m2930397507_gp_0_0_0_0_Types };
extern const Il2CppType LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0;
static const Il2CppType* GenInst_LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0_Types[] = { &LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0 = { 1, GenInst_LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0_IEnumerable_1_t2844771058_0_0_0_Types[] = { &LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0, &IEnumerable_1_t2844771058_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0_IEnumerable_1_t2844771058_0_0_0 = { 2, GenInst_LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0_IEnumerable_1_t2844771058_0_0_0_Types };
static const Il2CppType* GenInst_LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0_JToken_t2552644013_0_0_0_Types[] = { &LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0, &JToken_t2552644013_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0_JToken_t2552644013_0_0_0 = { 2, GenInst_LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0_JToken_t2552644013_0_0_0_Types };
extern const Il2CppType LinqExtensions_Descendants_m2164197081_gp_0_0_0_0;
static const Il2CppType* GenInst_LinqExtensions_Descendants_m2164197081_gp_0_0_0_0_Types[] = { &LinqExtensions_Descendants_m2164197081_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Descendants_m2164197081_gp_0_0_0_0 = { 1, GenInst_LinqExtensions_Descendants_m2164197081_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_LinqExtensions_Descendants_m2164197081_gp_0_0_0_0_IEnumerable_1_t2844771058_0_0_0_Types[] = { &LinqExtensions_Descendants_m2164197081_gp_0_0_0_0, &IEnumerable_1_t2844771058_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Descendants_m2164197081_gp_0_0_0_0_IEnumerable_1_t2844771058_0_0_0 = { 2, GenInst_LinqExtensions_Descendants_m2164197081_gp_0_0_0_0_IEnumerable_1_t2844771058_0_0_0_Types };
static const Il2CppType* GenInst_LinqExtensions_Descendants_m2164197081_gp_0_0_0_0_JToken_t2552644013_0_0_0_Types[] = { &LinqExtensions_Descendants_m2164197081_gp_0_0_0_0, &JToken_t2552644013_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Descendants_m2164197081_gp_0_0_0_0_JToken_t2552644013_0_0_0 = { 2, GenInst_LinqExtensions_Descendants_m2164197081_gp_0_0_0_0_JToken_t2552644013_0_0_0_Types };
extern const Il2CppType LinqExtensions_Values_m1204940136_gp_0_0_0_0;
static const Il2CppType* GenInst_LinqExtensions_Values_m1204940136_gp_0_0_0_0_Types[] = { &LinqExtensions_Values_m1204940136_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Values_m1204940136_gp_0_0_0_0 = { 1, GenInst_LinqExtensions_Values_m1204940136_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Values_m1204940136_gp_0_0_0_0_Types[] = { &JToken_t2552644013_0_0_0, &LinqExtensions_Values_m1204940136_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Values_m1204940136_gp_0_0_0_0 = { 2, GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Values_m1204940136_gp_0_0_0_0_Types };
extern const Il2CppType LinqExtensions_Values_m24015070_gp_0_0_0_0;
static const Il2CppType* GenInst_LinqExtensions_Values_m24015070_gp_0_0_0_0_Types[] = { &LinqExtensions_Values_m24015070_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Values_m24015070_gp_0_0_0_0 = { 1, GenInst_LinqExtensions_Values_m24015070_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Values_m24015070_gp_0_0_0_0_Types[] = { &JToken_t2552644013_0_0_0, &LinqExtensions_Values_m24015070_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Values_m24015070_gp_0_0_0_0 = { 2, GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Values_m24015070_gp_0_0_0_0_Types };
extern const Il2CppType LinqExtensions_Value_m4050904768_gp_0_0_0_0;
static const Il2CppType* GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Value_m4050904768_gp_0_0_0_0_Types[] = { &JToken_t2552644013_0_0_0, &LinqExtensions_Value_m4050904768_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Value_m4050904768_gp_0_0_0_0 = { 2, GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Value_m4050904768_gp_0_0_0_0_Types };
extern const Il2CppType LinqExtensions_Value_m236465892_gp_0_0_0_0;
static const Il2CppType* GenInst_LinqExtensions_Value_m236465892_gp_0_0_0_0_Types[] = { &LinqExtensions_Value_m236465892_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Value_m236465892_gp_0_0_0_0 = { 1, GenInst_LinqExtensions_Value_m236465892_gp_0_0_0_0_Types };
extern const Il2CppType LinqExtensions_Value_m236465892_gp_1_0_0_0;
static const Il2CppType* GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Value_m236465892_gp_1_0_0_0_Types[] = { &JToken_t2552644013_0_0_0, &LinqExtensions_Value_m236465892_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Value_m236465892_gp_1_0_0_0 = { 2, GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Value_m236465892_gp_1_0_0_0_Types };
extern const Il2CppType LinqExtensions_Values_m411275432_gp_0_0_0_0;
static const Il2CppType* GenInst_LinqExtensions_Values_m411275432_gp_0_0_0_0_Types[] = { &LinqExtensions_Values_m411275432_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Values_m411275432_gp_0_0_0_0 = { 1, GenInst_LinqExtensions_Values_m411275432_gp_0_0_0_0_Types };
extern const Il2CppType LinqExtensions_Values_m411275432_gp_1_0_0_0;
static const Il2CppType* GenInst_LinqExtensions_Values_m411275432_gp_1_0_0_0_Types[] = { &LinqExtensions_Values_m411275432_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Values_m411275432_gp_1_0_0_0 = { 1, GenInst_LinqExtensions_Values_m411275432_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_LinqExtensions_Values_m411275432_gp_0_0_0_0_LinqExtensions_Values_m411275432_gp_1_0_0_0_Types[] = { &LinqExtensions_Values_m411275432_gp_0_0_0_0, &LinqExtensions_Values_m411275432_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Values_m411275432_gp_0_0_0_0_LinqExtensions_Values_m411275432_gp_1_0_0_0 = { 2, GenInst_LinqExtensions_Values_m411275432_gp_0_0_0_0_LinqExtensions_Values_m411275432_gp_1_0_0_0_Types };
extern const Il2CppType LinqExtensions_Children_m937766004_gp_0_0_0_0;
static const Il2CppType* GenInst_LinqExtensions_Children_m937766004_gp_0_0_0_0_Types[] = { &LinqExtensions_Children_m937766004_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Children_m937766004_gp_0_0_0_0 = { 1, GenInst_LinqExtensions_Children_m937766004_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_LinqExtensions_Children_m937766004_gp_0_0_0_0_JToken_t2552644013_0_0_0_Types[] = { &LinqExtensions_Children_m937766004_gp_0_0_0_0, &JToken_t2552644013_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Children_m937766004_gp_0_0_0_0_JToken_t2552644013_0_0_0 = { 2, GenInst_LinqExtensions_Children_m937766004_gp_0_0_0_0_JToken_t2552644013_0_0_0_Types };
extern const Il2CppType LinqExtensions_Children_m4091488789_gp_0_0_0_0;
static const Il2CppType* GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_Types[] = { &LinqExtensions_Children_m4091488789_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0 = { 1, GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_Types };
extern const Il2CppType LinqExtensions_Children_m4091488789_gp_1_0_0_0;
static const Il2CppType* GenInst_LinqExtensions_Children_m4091488789_gp_1_0_0_0_Types[] = { &LinqExtensions_Children_m4091488789_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Children_m4091488789_gp_1_0_0_0 = { 1, GenInst_LinqExtensions_Children_m4091488789_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_LinqExtensions_Children_m4091488789_gp_1_0_0_0_Types[] = { &LinqExtensions_Children_m4091488789_gp_0_0_0_0, &LinqExtensions_Children_m4091488789_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_LinqExtensions_Children_m4091488789_gp_1_0_0_0 = { 2, GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_LinqExtensions_Children_m4091488789_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_IEnumerable_1_t2844771058_0_0_0_Types[] = { &LinqExtensions_Children_m4091488789_gp_0_0_0_0, &IEnumerable_1_t2844771058_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_IEnumerable_1_t2844771058_0_0_0 = { 2, GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_IEnumerable_1_t2844771058_0_0_0_Types };
static const Il2CppType* GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_JToken_t2552644013_0_0_0_Types[] = { &LinqExtensions_Children_m4091488789_gp_0_0_0_0, &JToken_t2552644013_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_JToken_t2552644013_0_0_0 = { 2, GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_JToken_t2552644013_0_0_0_Types };
static const Il2CppType* GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Children_m4091488789_gp_1_0_0_0_Types[] = { &JToken_t2552644013_0_0_0, &LinqExtensions_Children_m4091488789_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Children_m4091488789_gp_1_0_0_0 = { 2, GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Children_m4091488789_gp_1_0_0_0_Types };
extern const Il2CppType LinqExtensions_Convert_m2057205221_gp_0_0_0_0;
static const Il2CppType* GenInst_LinqExtensions_Convert_m2057205221_gp_0_0_0_0_Types[] = { &LinqExtensions_Convert_m2057205221_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Convert_m2057205221_gp_0_0_0_0 = { 1, GenInst_LinqExtensions_Convert_m2057205221_gp_0_0_0_0_Types };
extern const Il2CppType LinqExtensions_Convert_m2057205221_gp_1_0_0_0;
static const Il2CppType* GenInst_LinqExtensions_Convert_m2057205221_gp_1_0_0_0_Types[] = { &LinqExtensions_Convert_m2057205221_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Convert_m2057205221_gp_1_0_0_0 = { 1, GenInst_LinqExtensions_Convert_m2057205221_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_LinqExtensions_Convert_m2057205221_gp_0_0_0_0_LinqExtensions_Convert_m2057205221_gp_1_0_0_0_Types[] = { &LinqExtensions_Convert_m2057205221_gp_0_0_0_0, &LinqExtensions_Convert_m2057205221_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_Convert_m2057205221_gp_0_0_0_0_LinqExtensions_Convert_m2057205221_gp_1_0_0_0 = { 2, GenInst_LinqExtensions_Convert_m2057205221_gp_0_0_0_0_LinqExtensions_Convert_m2057205221_gp_1_0_0_0_Types };
extern const Il2CppType LinqExtensions_AsJEnumerable_m1823102599_gp_0_0_0_0;
static const Il2CppType* GenInst_LinqExtensions_AsJEnumerable_m1823102599_gp_0_0_0_0_Types[] = { &LinqExtensions_AsJEnumerable_m1823102599_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinqExtensions_AsJEnumerable_m1823102599_gp_0_0_0_0 = { 1, GenInst_LinqExtensions_AsJEnumerable_m1823102599_gp_0_0_0_0_Types };
extern const Il2CppType U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0_Types[] = { &U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0 = { 1, GenInst_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0_Types };
extern const Il2CppType U3CValuesU3Ec__Iterator12_2_t430610032_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CValuesU3Ec__Iterator12_2_t430610032_gp_0_0_0_0_Types[] = { &U3CValuesU3Ec__Iterator12_2_t430610032_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CValuesU3Ec__Iterator12_2_t430610032_gp_0_0_0_0 = { 1, GenInst_U3CValuesU3Ec__Iterator12_2_t430610032_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CValuesU3Ec__Iterator12_2_t430610032_gp_0_0_0_0_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0_Types[] = { &U3CValuesU3Ec__Iterator12_2_t430610032_gp_0_0_0_0, &U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CValuesU3Ec__Iterator12_2_t430610032_gp_0_0_0_0_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0 = { 2, GenInst_U3CValuesU3Ec__Iterator12_2_t430610032_gp_0_0_0_0_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_JValue_t300956845_0_0_0_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0_Types[] = { &JValue_t300956845_0_0_0, &U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_JValue_t300956845_0_0_0_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0 = { 2, GenInst_JValue_t300956845_0_0_0_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_JToken_t2552644013_0_0_0_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0_Types[] = { &JToken_t2552644013_0_0_0, &U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t2552644013_0_0_0_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0 = { 2, GenInst_JToken_t2552644013_0_0_0_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0_Types };
extern const Il2CppType U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0_Types[] = { &U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0 = { 1, GenInst_U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0_Types };
extern const Il2CppType U3CConvertU3Ec__Iterator13_2_t239315294_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CConvertU3Ec__Iterator13_2_t239315294_gp_0_0_0_0_Types[] = { &U3CConvertU3Ec__Iterator13_2_t239315294_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CConvertU3Ec__Iterator13_2_t239315294_gp_0_0_0_0 = { 1, GenInst_U3CConvertU3Ec__Iterator13_2_t239315294_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CConvertU3Ec__Iterator13_2_t239315294_gp_0_0_0_0_U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0_Types[] = { &U3CConvertU3Ec__Iterator13_2_t239315294_gp_0_0_0_0, &U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CConvertU3Ec__Iterator13_2_t239315294_gp_0_0_0_0_U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0 = { 2, GenInst_U3CConvertU3Ec__Iterator13_2_t239315294_gp_0_0_0_0_U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_JToken_t2552644013_0_0_0_U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0_Types[] = { &JToken_t2552644013_0_0_0, &U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t2552644013_0_0_0_U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0 = { 2, GenInst_JToken_t2552644013_0_0_0_U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0_Types };
extern const Il2CppType CachedAttributeGetter_1_t3215015314_gp_0_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_CachedAttributeGetter_1_t3215015314_gp_0_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0, &CachedAttributeGetter_1_t3215015314_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0_CachedAttributeGetter_1_t3215015314_gp_0_0_0_0 = { 2, GenInst_ICustomAttributeProvider_t502202687_0_0_0_CachedAttributeGetter_1_t3215015314_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_CachedAttributeGetter_1_t3215015314_gp_0_0_0_0_Types[] = { &CachedAttributeGetter_1_t3215015314_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedAttributeGetter_1_t3215015314_gp_0_0_0_0 = { 1, GenInst_CachedAttributeGetter_1_t3215015314_gp_0_0_0_0_Types };
extern const Il2CppType JsonTypeReflector_GetAttribute_m3832145644_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonTypeReflector_GetAttribute_m3832145644_gp_0_0_0_0_Types[] = { &JsonTypeReflector_GetAttribute_m3832145644_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonTypeReflector_GetAttribute_m3832145644_gp_0_0_0_0 = { 1, GenInst_JsonTypeReflector_GetAttribute_m3832145644_gp_0_0_0_0_Types };
extern const Il2CppType JsonTypeReflector_GetAttribute_m2691398215_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonTypeReflector_GetAttribute_m2691398215_gp_0_0_0_0_Types[] = { &JsonTypeReflector_GetAttribute_m2691398215_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonTypeReflector_GetAttribute_m2691398215_gp_0_0_0_0 = { 1, GenInst_JsonTypeReflector_GetAttribute_m2691398215_gp_0_0_0_0_Types };
extern const Il2CppType JsonTypeReflector_GetAttribute_m2399085218_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonTypeReflector_GetAttribute_m2399085218_gp_0_0_0_0_Types[] = { &JsonTypeReflector_GetAttribute_m2399085218_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonTypeReflector_GetAttribute_m2399085218_gp_0_0_0_0 = { 1, GenInst_JsonTypeReflector_GetAttribute_m2399085218_gp_0_0_0_0_Types };
extern const Il2CppType BidirectionalDictionary_2_t2143222129_gp_0_0_0_0;
static const Il2CppType* GenInst_BidirectionalDictionary_2_t2143222129_gp_0_0_0_0_Types[] = { &BidirectionalDictionary_2_t2143222129_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BidirectionalDictionary_2_t2143222129_gp_0_0_0_0 = { 1, GenInst_BidirectionalDictionary_2_t2143222129_gp_0_0_0_0_Types };
extern const Il2CppType BidirectionalDictionary_2_t2143222129_gp_1_0_0_0;
static const Il2CppType* GenInst_BidirectionalDictionary_2_t2143222129_gp_1_0_0_0_Types[] = { &BidirectionalDictionary_2_t2143222129_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_BidirectionalDictionary_2_t2143222129_gp_1_0_0_0 = { 1, GenInst_BidirectionalDictionary_2_t2143222129_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_BidirectionalDictionary_2_t2143222129_gp_0_0_0_0_BidirectionalDictionary_2_t2143222129_gp_1_0_0_0_Types[] = { &BidirectionalDictionary_2_t2143222129_gp_0_0_0_0, &BidirectionalDictionary_2_t2143222129_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_BidirectionalDictionary_2_t2143222129_gp_0_0_0_0_BidirectionalDictionary_2_t2143222129_gp_1_0_0_0 = { 2, GenInst_BidirectionalDictionary_2_t2143222129_gp_0_0_0_0_BidirectionalDictionary_2_t2143222129_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_BidirectionalDictionary_2_t2143222129_gp_1_0_0_0_BidirectionalDictionary_2_t2143222129_gp_0_0_0_0_Types[] = { &BidirectionalDictionary_2_t2143222129_gp_1_0_0_0, &BidirectionalDictionary_2_t2143222129_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BidirectionalDictionary_2_t2143222129_gp_1_0_0_0_BidirectionalDictionary_2_t2143222129_gp_0_0_0_0 = { 2, GenInst_BidirectionalDictionary_2_t2143222129_gp_1_0_0_0_BidirectionalDictionary_2_t2143222129_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_CastValid_m3369744617_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_CastValid_m3369744617_gp_0_0_0_0_Types[] = { &CollectionUtils_CastValid_m3369744617_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_CastValid_m3369744617_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_CastValid_m3369744617_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_CreateList_m2661262288_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_CreateList_m2661262288_gp_0_0_0_0_Types[] = { &CollectionUtils_CreateList_m2661262288_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_CreateList_m2661262288_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_CreateList_m2661262288_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_IsNullOrEmpty_m4123750027_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_IsNullOrEmpty_m4123750027_gp_0_0_0_0_Types[] = { &CollectionUtils_IsNullOrEmpty_m4123750027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_IsNullOrEmpty_m4123750027_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_IsNullOrEmpty_m4123750027_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_IsNullOrEmptyOrDefault_m3426852801_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_IsNullOrEmptyOrDefault_m3426852801_gp_0_0_0_0_Types[] = { &CollectionUtils_IsNullOrEmptyOrDefault_m3426852801_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_IsNullOrEmptyOrDefault_m3426852801_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_IsNullOrEmptyOrDefault_m3426852801_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_Slice_m3827466680_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_Slice_m3827466680_gp_0_0_0_0_Types[] = { &CollectionUtils_Slice_m3827466680_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_Slice_m3827466680_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_Slice_m3827466680_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_Slice_m1108815180_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_Slice_m1108815180_gp_0_0_0_0_Types[] = { &CollectionUtils_Slice_m1108815180_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_Slice_m1108815180_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_Slice_m1108815180_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_GroupBy_m513581027_gp_1_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_GroupBy_m513581027_gp_1_0_0_0_Types[] = { &CollectionUtils_GroupBy_m513581027_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_GroupBy_m513581027_gp_1_0_0_0 = { 1, GenInst_CollectionUtils_GroupBy_m513581027_gp_1_0_0_0_Types };
extern const Il2CppType CollectionUtils_GroupBy_m513581027_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_GroupBy_m513581027_gp_1_0_0_0_CollectionUtils_GroupBy_m513581027_gp_0_0_0_0_Types[] = { &CollectionUtils_GroupBy_m513581027_gp_1_0_0_0, &CollectionUtils_GroupBy_m513581027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_GroupBy_m513581027_gp_1_0_0_0_CollectionUtils_GroupBy_m513581027_gp_0_0_0_0 = { 2, GenInst_CollectionUtils_GroupBy_m513581027_gp_1_0_0_0_CollectionUtils_GroupBy_m513581027_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t1924900830_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_GroupBy_m513581027_gp_0_0_0_0_List_1_t1924900830_0_0_0_Types[] = { &CollectionUtils_GroupBy_m513581027_gp_0_0_0_0, &List_1_t1924900830_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_GroupBy_m513581027_gp_0_0_0_0_List_1_t1924900830_0_0_0 = { 2, GenInst_CollectionUtils_GroupBy_m513581027_gp_0_0_0_0_List_1_t1924900830_0_0_0_Types };
extern const Il2CppType CollectionUtils_AddRange_m2231204599_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_AddRange_m2231204599_gp_0_0_0_0_Types[] = { &CollectionUtils_AddRange_m2231204599_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_AddRange_m2231204599_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_AddRange_m2231204599_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_Distinct_m687017250_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_Distinct_m687017250_gp_0_0_0_0_Types[] = { &CollectionUtils_Distinct_m687017250_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_Distinct_m687017250_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_Distinct_m687017250_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_Flatten_m2921316847_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_Flatten_m2921316847_gp_0_0_0_0_Types[] = { &CollectionUtils_Flatten_m2921316847_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_Flatten_m2921316847_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_Flatten_m2921316847_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t1091115121_0_0_0;
static const Il2CppType* GenInst_List_1_t1091115121_0_0_0_Types[] = { &List_1_t1091115121_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1091115121_0_0_0 = { 1, GenInst_List_1_t1091115121_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_CollectionUtils_Flatten_m2921316847_gp_0_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &CollectionUtils_Flatten_m2921316847_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_CollectionUtils_Flatten_m2921316847_gp_0_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_CollectionUtils_Flatten_m2921316847_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t2262934590_0_0_0;
static const Il2CppType* GenInst_IList_1_t2262934590_0_0_0_Types[] = { &IList_1_t2262934590_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2262934590_0_0_0 = { 1, GenInst_IList_1_t2262934590_0_0_0_Types };
extern const Il2CppType IList_1_t3947597871_0_0_0;
static const Il2CppType* GenInst_IList_1_t3947597871_0_0_0_Types[] = { &IList_1_t3947597871_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3947597871_0_0_0 = { 1, GenInst_IList_1_t3947597871_0_0_0_Types };
extern const Il2CppType CollectionUtils_Recurse_m1486496431_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_Recurse_m1486496431_gp_0_0_0_0_Types[] = { &CollectionUtils_Recurse_m1486496431_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_Recurse_m1486496431_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_Recurse_m1486496431_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_CollectionUtils_Recurse_m1486496431_gp_0_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &CollectionUtils_Recurse_m1486496431_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_CollectionUtils_Recurse_m1486496431_gp_0_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_CollectionUtils_Recurse_m1486496431_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2775778402_0_0_0;
static const Il2CppType* GenInst_List_1_t2775778402_0_0_0_Types[] = { &List_1_t2775778402_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2775778402_0_0_0 = { 1, GenInst_List_1_t2775778402_0_0_0_Types };
extern const Il2CppType CollectionUtils_CreateList_m4224726381_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_CreateList_m4224726381_gp_0_0_0_0_Types[] = { &CollectionUtils_CreateList_m4224726381_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_CreateList_m4224726381_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_CreateList_m4224726381_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_ListEquals_m2304676934_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_ListEquals_m2304676934_gp_0_0_0_0_Types[] = { &CollectionUtils_ListEquals_m2304676934_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_ListEquals_m2304676934_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_ListEquals_m2304676934_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_TryGetSingleItem_m3616950892_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_TryGetSingleItem_m3616950892_gp_0_0_0_0_Types[] = { &CollectionUtils_TryGetSingleItem_m3616950892_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_TryGetSingleItem_m3616950892_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_TryGetSingleItem_m3616950892_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_TryGetSingleItem_m936218929_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_TryGetSingleItem_m936218929_gp_0_0_0_0_Types[] = { &CollectionUtils_TryGetSingleItem_m936218929_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_TryGetSingleItem_m936218929_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_TryGetSingleItem_m936218929_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_GetSingleItem_m1986031524_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_GetSingleItem_m1986031524_gp_0_0_0_0_Types[] = { &CollectionUtils_GetSingleItem_m1986031524_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_GetSingleItem_m1986031524_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_GetSingleItem_m1986031524_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_GetSingleItem_m3328355783_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_GetSingleItem_m3328355783_gp_0_0_0_0_Types[] = { &CollectionUtils_GetSingleItem_m3328355783_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_GetSingleItem_m3328355783_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_GetSingleItem_m3328355783_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_Minus_m1749740517_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_Minus_m1749740517_gp_0_0_0_0_Types[] = { &CollectionUtils_Minus_m1749740517_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_Minus_m1749740517_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_Minus_m1749740517_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_AddDistinct_m2604642977_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_AddDistinct_m2604642977_gp_0_0_0_0_Types[] = { &CollectionUtils_AddDistinct_m2604642977_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_AddDistinct_m2604642977_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_AddDistinct_m2604642977_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_AddDistinct_m729482485_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_AddDistinct_m729482485_gp_0_0_0_0_Types[] = { &CollectionUtils_AddDistinct_m729482485_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_AddDistinct_m729482485_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_AddDistinct_m729482485_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_ContainsValue_m2334048203_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_ContainsValue_m2334048203_gp_0_0_0_0_Types[] = { &CollectionUtils_ContainsValue_m2334048203_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_ContainsValue_m2334048203_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_ContainsValue_m2334048203_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_AddRangeDistinct_m737237641_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_AddRangeDistinct_m737237641_gp_0_0_0_0_Types[] = { &CollectionUtils_AddRangeDistinct_m737237641_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_AddRangeDistinct_m737237641_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_AddRangeDistinct_m737237641_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_AddRangeDistinct_m2236996053_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_AddRangeDistinct_m2236996053_gp_0_0_0_0_Types[] = { &CollectionUtils_AddRangeDistinct_m2236996053_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_AddRangeDistinct_m2236996053_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_AddRangeDistinct_m2236996053_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_IndexOf_m1194221877_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_IndexOf_m1194221877_gp_0_0_0_0_Types[] = { &CollectionUtils_IndexOf_m1194221877_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_IndexOf_m1194221877_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_IndexOf_m1194221877_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_CollectionUtils_IndexOf_m1194221877_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &CollectionUtils_IndexOf_m1194221877_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_IndexOf_m1194221877_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_CollectionUtils_IndexOf_m1194221877_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType CollectionUtils_IndexOf_m1330017483_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_IndexOf_m1330017483_gp_0_0_0_0_Types[] = { &CollectionUtils_IndexOf_m1330017483_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_IndexOf_m1330017483_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_IndexOf_m1330017483_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_IndexOf_m1505122980_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_IndexOf_m1505122980_gp_0_0_0_0_Types[] = { &CollectionUtils_IndexOf_m1505122980_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_IndexOf_m1505122980_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_IndexOf_m1505122980_gp_0_0_0_0_Types };
extern const Il2CppType U3CTryGetSingleItemU3Ec__AnonStorey25_1_t2803002290_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CTryGetSingleItemU3Ec__AnonStorey25_1_t2803002290_gp_0_0_0_0_Types[] = { &U3CTryGetSingleItemU3Ec__AnonStorey25_1_t2803002290_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CTryGetSingleItemU3Ec__AnonStorey25_1_t2803002290_gp_0_0_0_0 = { 1, GenInst_U3CTryGetSingleItemU3Ec__AnonStorey25_1_t2803002290_gp_0_0_0_0_Types };
extern const Il2CppType CollectionWrapper_1_t318624866_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionWrapper_1_t318624866_gp_0_0_0_0_Types[] = { &CollectionWrapper_1_t318624866_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionWrapper_1_t318624866_gp_0_0_0_0 = { 1, GenInst_CollectionWrapper_1_t318624866_gp_0_0_0_0_Types };
extern const Il2CppType ConvertUtils_Convert_m1211373838_gp_0_0_0_0;
static const Il2CppType* GenInst_ConvertUtils_Convert_m1211373838_gp_0_0_0_0_Types[] = { &ConvertUtils_Convert_m1211373838_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ConvertUtils_Convert_m1211373838_gp_0_0_0_0 = { 1, GenInst_ConvertUtils_Convert_m1211373838_gp_0_0_0_0_Types };
extern const Il2CppType ConvertUtils_TryConvert_m1519373714_gp_0_0_0_0;
static const Il2CppType* GenInst_ConvertUtils_TryConvert_m1519373714_gp_0_0_0_0_Types[] = { &ConvertUtils_TryConvert_m1519373714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ConvertUtils_TryConvert_m1519373714_gp_0_0_0_0 = { 1, GenInst_ConvertUtils_TryConvert_m1519373714_gp_0_0_0_0_Types };
extern const Il2CppType ConvertUtils_TryConvert_m3257615354_gp_0_0_0_0;
static const Il2CppType* GenInst_ConvertUtils_TryConvert_m3257615354_gp_0_0_0_0_Types[] = { &ConvertUtils_TryConvert_m3257615354_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ConvertUtils_TryConvert_m3257615354_gp_0_0_0_0 = { 1, GenInst_ConvertUtils_TryConvert_m3257615354_gp_0_0_0_0_Types };
extern const Il2CppType ConvertUtils_ConvertOrCast_m219507444_gp_0_0_0_0;
static const Il2CppType* GenInst_ConvertUtils_ConvertOrCast_m219507444_gp_0_0_0_0_Types[] = { &ConvertUtils_ConvertOrCast_m219507444_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ConvertUtils_ConvertOrCast_m219507444_gp_0_0_0_0 = { 1, GenInst_ConvertUtils_ConvertOrCast_m219507444_gp_0_0_0_0_Types };
extern const Il2CppType ConvertUtils_TryConvertOrCast_m4070485328_gp_0_0_0_0;
static const Il2CppType* GenInst_ConvertUtils_TryConvertOrCast_m4070485328_gp_0_0_0_0_Types[] = { &ConvertUtils_TryConvertOrCast_m4070485328_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ConvertUtils_TryConvertOrCast_m4070485328_gp_0_0_0_0 = { 1, GenInst_ConvertUtils_TryConvertOrCast_m4070485328_gp_0_0_0_0_Types };
extern const Il2CppType ConvertUtils_TryConvertOrCast_m125199128_gp_0_0_0_0;
static const Il2CppType* GenInst_ConvertUtils_TryConvertOrCast_m125199128_gp_0_0_0_0_Types[] = { &ConvertUtils_TryConvertOrCast_m125199128_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ConvertUtils_TryConvertOrCast_m125199128_gp_0_0_0_0 = { 1, GenInst_ConvertUtils_TryConvertOrCast_m125199128_gp_0_0_0_0_Types };
extern const Il2CppType DictionaryWrapper_2_t815983041_gp_0_0_0_0;
extern const Il2CppType DictionaryWrapper_2_t815983041_gp_1_0_0_0;
static const Il2CppType* GenInst_DictionaryWrapper_2_t815983041_gp_0_0_0_0_DictionaryWrapper_2_t815983041_gp_1_0_0_0_Types[] = { &DictionaryWrapper_2_t815983041_gp_0_0_0_0, &DictionaryWrapper_2_t815983041_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryWrapper_2_t815983041_gp_0_0_0_0_DictionaryWrapper_2_t815983041_gp_1_0_0_0 = { 2, GenInst_DictionaryWrapper_2_t815983041_gp_0_0_0_0_DictionaryWrapper_2_t815983041_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryWrapper_2_t815983041_gp_0_0_0_0_Types[] = { &DictionaryWrapper_2_t815983041_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryWrapper_2_t815983041_gp_0_0_0_0 = { 1, GenInst_DictionaryWrapper_2_t815983041_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryWrapper_2_t815983041_gp_1_0_0_0_Types[] = { &DictionaryWrapper_2_t815983041_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryWrapper_2_t815983041_gp_1_0_0_0 = { 1, GenInst_DictionaryWrapper_2_t815983041_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3125239586_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3125239586_0_0_0_Types[] = { &KeyValuePair_2_t3125239586_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3125239586_0_0_0 = { 1, GenInst_KeyValuePair_2_t3125239586_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_KeyValuePair_2_t3125239586_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0, &KeyValuePair_2_t3125239586_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0_KeyValuePair_2_t3125239586_0_0_0 = { 2, GenInst_DictionaryEntry_t3048875398_0_0_0_KeyValuePair_2_t3125239586_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3432887514_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3432887514_0_0_0_Types[] = { &KeyValuePair_2_t3432887514_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3432887514_0_0_0 = { 1, GenInst_KeyValuePair_2_t3432887514_0_0_0_Types };
extern const Il2CppType DictionaryEnumerator_2_t2360669969_gp_2_0_0_0;
extern const Il2CppType DictionaryEnumerator_2_t2360669969_gp_3_0_0_0;
static const Il2CppType* GenInst_DictionaryEnumerator_2_t2360669969_gp_2_0_0_0_DictionaryEnumerator_2_t2360669969_gp_3_0_0_0_Types[] = { &DictionaryEnumerator_2_t2360669969_gp_2_0_0_0, &DictionaryEnumerator_2_t2360669969_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEnumerator_2_t2360669969_gp_2_0_0_0_DictionaryEnumerator_2_t2360669969_gp_3_0_0_0 = { 2, GenInst_DictionaryEnumerator_2_t2360669969_gp_2_0_0_0_DictionaryEnumerator_2_t2360669969_gp_3_0_0_0_Types };
extern const Il2CppType DictionaryEnumerator_2_t2360669969_gp_0_0_0_0;
extern const Il2CppType DictionaryEnumerator_2_t2360669969_gp_1_0_0_0;
static const Il2CppType* GenInst_DictionaryEnumerator_2_t2360669969_gp_0_0_0_0_DictionaryEnumerator_2_t2360669969_gp_1_0_0_0_DictionaryEnumerator_2_t2360669969_gp_2_0_0_0_DictionaryEnumerator_2_t2360669969_gp_3_0_0_0_Types[] = { &DictionaryEnumerator_2_t2360669969_gp_0_0_0_0, &DictionaryEnumerator_2_t2360669969_gp_1_0_0_0, &DictionaryEnumerator_2_t2360669969_gp_2_0_0_0, &DictionaryEnumerator_2_t2360669969_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEnumerator_2_t2360669969_gp_0_0_0_0_DictionaryEnumerator_2_t2360669969_gp_1_0_0_0_DictionaryEnumerator_2_t2360669969_gp_2_0_0_0_DictionaryEnumerator_2_t2360669969_gp_3_0_0_0 = { 4, GenInst_DictionaryEnumerator_2_t2360669969_gp_0_0_0_0_DictionaryEnumerator_2_t2360669969_gp_1_0_0_0_DictionaryEnumerator_2_t2360669969_gp_2_0_0_0_DictionaryEnumerator_2_t2360669969_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryWrapper_2_t815983041_gp_0_0_0_0_DictionaryWrapper_2_t815983041_gp_1_0_0_0_DictionaryWrapper_2_t815983041_gp_0_0_0_0_DictionaryWrapper_2_t815983041_gp_1_0_0_0_Types[] = { &DictionaryWrapper_2_t815983041_gp_0_0_0_0, &DictionaryWrapper_2_t815983041_gp_1_0_0_0, &DictionaryWrapper_2_t815983041_gp_0_0_0_0, &DictionaryWrapper_2_t815983041_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryWrapper_2_t815983041_gp_0_0_0_0_DictionaryWrapper_2_t815983041_gp_1_0_0_0_DictionaryWrapper_2_t815983041_gp_0_0_0_0_DictionaryWrapper_2_t815983041_gp_1_0_0_0 = { 4, GenInst_DictionaryWrapper_2_t815983041_gp_0_0_0_0_DictionaryWrapper_2_t815983041_gp_1_0_0_0_DictionaryWrapper_2_t815983041_gp_0_0_0_0_DictionaryWrapper_2_t815983041_gp_1_0_0_0_Types };
extern const Il2CppType EnumUtils_Parse_m3231354786_gp_0_0_0_0;
static const Il2CppType* GenInst_EnumUtils_Parse_m3231354786_gp_0_0_0_0_Types[] = { &EnumUtils_Parse_m3231354786_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumUtils_Parse_m3231354786_gp_0_0_0_0 = { 1, GenInst_EnumUtils_Parse_m3231354786_gp_0_0_0_0_Types };
extern const Il2CppType EnumUtils_TryParse_m455840625_gp_0_0_0_0;
static const Il2CppType* GenInst_EnumUtils_TryParse_m455840625_gp_0_0_0_0_Types[] = { &EnumUtils_TryParse_m455840625_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumUtils_TryParse_m455840625_gp_0_0_0_0 = { 1, GenInst_EnumUtils_TryParse_m455840625_gp_0_0_0_0_Types };
extern const Il2CppType EnumUtils_GetFlagsValues_m2938233177_gp_0_0_0_0;
static const Il2CppType* GenInst_EnumUtils_GetFlagsValues_m2938233177_gp_0_0_0_0_Types[] = { &EnumUtils_GetFlagsValues_m2938233177_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumUtils_GetFlagsValues_m2938233177_gp_0_0_0_0 = { 1, GenInst_EnumUtils_GetFlagsValues_m2938233177_gp_0_0_0_0_Types };
extern const Il2CppType EnumUtils_GetNamesAndValues_m3492261118_gp_1_0_0_0;
static const Il2CppType* GenInst_EnumUtils_GetNamesAndValues_m3492261118_gp_1_0_0_0_Types[] = { &EnumUtils_GetNamesAndValues_m3492261118_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumUtils_GetNamesAndValues_m3492261118_gp_1_0_0_0 = { 1, GenInst_EnumUtils_GetNamesAndValues_m3492261118_gp_1_0_0_0_Types };
extern const Il2CppType EnumUtils_GetNamesAndValues_m2393662447_gp_0_0_0_0;
static const Il2CppType* GenInst_EnumUtils_GetNamesAndValues_m2393662447_gp_0_0_0_0_Types[] = { &EnumUtils_GetNamesAndValues_m2393662447_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumUtils_GetNamesAndValues_m2393662447_gp_0_0_0_0 = { 1, GenInst_EnumUtils_GetNamesAndValues_m2393662447_gp_0_0_0_0_Types };
extern const Il2CppType EnumValue_1_t549532375_0_0_0;
static const Il2CppType* GenInst_EnumValue_1_t549532375_0_0_0_Types[] = { &EnumValue_1_t549532375_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumValue_1_t549532375_0_0_0 = { 1, GenInst_EnumValue_1_t549532375_0_0_0_Types };
extern const Il2CppType EnumUtils_GetValues_m1497029920_gp_0_0_0_0;
static const Il2CppType* GenInst_EnumUtils_GetValues_m1497029920_gp_0_0_0_0_Types[] = { &EnumUtils_GetValues_m1497029920_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumUtils_GetValues_m1497029920_gp_0_0_0_0 = { 1, GenInst_EnumUtils_GetValues_m1497029920_gp_0_0_0_0_Types };
extern const Il2CppType EnumUtils_GetMaximumValue_m4128337876_gp_0_0_0_0;
static const Il2CppType* GenInst_EnumUtils_GetMaximumValue_m4128337876_gp_0_0_0_0_Types[] = { &EnumUtils_GetMaximumValue_m4128337876_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumUtils_GetMaximumValue_m4128337876_gp_0_0_0_0 = { 1, GenInst_EnumUtils_GetMaximumValue_m4128337876_gp_0_0_0_0_Types };
extern const Il2CppType U3CTryParseU3Ec__AnonStorey2E_1_t1872128815_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CTryParseU3Ec__AnonStorey2E_1_t1872128815_gp_0_0_0_0_Types[] = { &U3CTryParseU3Ec__AnonStorey2E_1_t1872128815_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CTryParseU3Ec__AnonStorey2E_1_t1872128815_gp_0_0_0_0 = { 1, GenInst_U3CTryParseU3Ec__AnonStorey2E_1_t1872128815_gp_0_0_0_0_Types };
extern const Il2CppType EnumValue_1_t1952206871_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_EnumValue_1_t1952206871_0_0_0_Types[] = { &String_t_0_0_0, &EnumValue_1_t1952206871_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_EnumValue_1_t1952206871_0_0_0 = { 2, GenInst_String_t_0_0_0_EnumValue_1_t1952206871_0_0_0_Types };
extern const Il2CppType EnumValues_1_t1624570432_gp_0_0_0_0;
static const Il2CppType* GenInst_EnumValues_1_t1624570432_gp_0_0_0_0_Types[] = { &EnumValues_1_t1624570432_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumValues_1_t1624570432_gp_0_0_0_0 = { 1, GenInst_EnumValues_1_t1624570432_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_EnumValue_1_t1952206871_0_0_0_Types[] = { &EnumValue_1_t1952206871_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumValue_1_t1952206871_0_0_0 = { 1, GenInst_EnumValue_1_t1952206871_0_0_0_Types };
extern const Il2CppType LateBoundReflectionDelegateFactory_CreateMethodCall_m984568458_gp_0_0_0_0;
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m984568458_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateMethodCall_m984568458_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m984568458_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m984568458_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m984568458_gp_0_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateMethodCall_m984568458_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m984568458_gp_0_0_0_0 = { 1, GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m984568458_gp_0_0_0_0_Types };
extern const Il2CppType LateBoundReflectionDelegateFactory_CreateDefaultConstructor_m1949776623_gp_0_0_0_0;
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateDefaultConstructor_m1949776623_gp_0_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateDefaultConstructor_m1949776623_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateDefaultConstructor_m1949776623_gp_0_0_0_0 = { 1, GenInst_LateBoundReflectionDelegateFactory_CreateDefaultConstructor_m1949776623_gp_0_0_0_0_Types };
extern const Il2CppType LateBoundReflectionDelegateFactory_CreateGet_m747619007_gp_0_0_0_0;
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateGet_m747619007_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateGet_m747619007_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateGet_m747619007_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_LateBoundReflectionDelegateFactory_CreateGet_m747619007_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateGet_m747619007_gp_0_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateGet_m747619007_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateGet_m747619007_gp_0_0_0_0 = { 1, GenInst_LateBoundReflectionDelegateFactory_CreateGet_m747619007_gp_0_0_0_0_Types };
extern const Il2CppType LateBoundReflectionDelegateFactory_CreateGet_m1551745180_gp_0_0_0_0;
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1551745180_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateGet_m1551745180_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1551745180_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1551745180_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1551745180_gp_0_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateGet_m1551745180_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1551745180_gp_0_0_0_0 = { 1, GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1551745180_gp_0_0_0_0_Types };
extern const Il2CppType LateBoundReflectionDelegateFactory_CreateSet_m1737331520_gp_0_0_0_0;
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1737331520_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateSet_m1737331520_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1737331520_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1737331520_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1737331520_gp_0_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateSet_m1737331520_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1737331520_gp_0_0_0_0 = { 1, GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1737331520_gp_0_0_0_0_Types };
extern const Il2CppType LateBoundReflectionDelegateFactory_CreateSet_m1213597307_gp_0_0_0_0;
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1213597307_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateSet_m1213597307_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1213597307_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1213597307_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1213597307_gp_0_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateSet_m1213597307_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1213597307_gp_0_0_0_0 = { 1, GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1213597307_gp_0_0_0_0_Types };
extern const Il2CppType ListWrapper_1_t3036879910_gp_0_0_0_0;
static const Il2CppType* GenInst_ListWrapper_1_t3036879910_gp_0_0_0_0_Types[] = { &ListWrapper_1_t3036879910_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListWrapper_1_t3036879910_gp_0_0_0_0 = { 1, GenInst_ListWrapper_1_t3036879910_gp_0_0_0_0_Types };
extern const Il2CppType MiscellaneousUtils_TryAction_m933287344_gp_0_0_0_0;
static const Il2CppType* GenInst_MiscellaneousUtils_TryAction_m933287344_gp_0_0_0_0_Types[] = { &MiscellaneousUtils_TryAction_m933287344_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MiscellaneousUtils_TryAction_m933287344_gp_0_0_0_0 = { 1, GenInst_MiscellaneousUtils_TryAction_m933287344_gp_0_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateGet_m3576934762_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateGet_m3576934762_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateGet_m3576934762_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateGet_m3576934762_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateGet_m3576934762_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateGet_m3576934762_gp_0_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateGet_m3576934762_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateGet_m3576934762_gp_0_0_0_0 = { 1, GenInst_ReflectionDelegateFactory_CreateGet_m3576934762_gp_0_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateSet_m3835094278_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateSet_m3835094278_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateSet_m3835094278_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateSet_m3835094278_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateSet_m3835094278_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateSet_m3835094278_gp_0_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateSet_m3835094278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateSet_m3835094278_gp_0_0_0_0 = { 1, GenInst_ReflectionDelegateFactory_CreateSet_m3835094278_gp_0_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateMethodCall_m2651893450_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateMethodCall_m2651893450_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateMethodCall_m2651893450_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateMethodCall_m2651893450_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateMethodCall_m2651893450_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateDefaultConstructor_m448703917_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateDefaultConstructor_m448703917_gp_0_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateDefaultConstructor_m448703917_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateDefaultConstructor_m448703917_gp_0_0_0_0 = { 1, GenInst_ReflectionDelegateFactory_CreateDefaultConstructor_m448703917_gp_0_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateGet_m1914506157_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateGet_m1914506157_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateGet_m1914506157_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateGet_m1914506157_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateGet_m1914506157_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateGet_m638437858_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateGet_m638437858_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateGet_m638437858_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateGet_m638437858_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateGet_m638437858_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateSet_m2901308994_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateSet_m2901308994_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateSet_m2901308994_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateSet_m2901308994_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateSet_m2901308994_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateSet_m3581014573_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateSet_m3581014573_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateSet_m3581014573_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateSet_m3581014573_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateSet_m3581014573_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ReflectionUtils_ItemsUnitializedValue_m182429506_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionUtils_ItemsUnitializedValue_m182429506_gp_0_0_0_0_Types[] = { &ReflectionUtils_ItemsUnitializedValue_m182429506_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionUtils_ItemsUnitializedValue_m182429506_gp_0_0_0_0 = { 1, GenInst_ReflectionUtils_ItemsUnitializedValue_m182429506_gp_0_0_0_0_Types };
extern const Il2CppType ReflectionUtils_GetAttribute_m258851166_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionUtils_GetAttribute_m258851166_gp_0_0_0_0_Types[] = { &ReflectionUtils_GetAttribute_m258851166_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionUtils_GetAttribute_m258851166_gp_0_0_0_0 = { 1, GenInst_ReflectionUtils_GetAttribute_m258851166_gp_0_0_0_0_Types };
extern const Il2CppType ReflectionUtils_GetAttribute_m3077178463_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionUtils_GetAttribute_m3077178463_gp_0_0_0_0_Types[] = { &ReflectionUtils_GetAttribute_m3077178463_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionUtils_GetAttribute_m3077178463_gp_0_0_0_0 = { 1, GenInst_ReflectionUtils_GetAttribute_m3077178463_gp_0_0_0_0_Types };
extern const Il2CppType StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0;
static const Il2CppType* GenInst_StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0_Types[] = { &StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0 = { 1, GenInst_StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0_String_t_0_0_0_Types[] = { &StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0_String_t_0_0_0 = { 2, GenInst_StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2975701800_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2975701800_gp_0_0_0_0_String_t_0_0_0_Types[] = { &U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2975701800_gp_0_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2975701800_gp_0_0_0_0_String_t_0_0_0 = { 2, GenInst_U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2975701800_gp_0_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ThreadSafeStore_2_t855392148_gp_0_0_0_0;
extern const Il2CppType ThreadSafeStore_2_t855392148_gp_1_0_0_0;
static const Il2CppType* GenInst_ThreadSafeStore_2_t855392148_gp_0_0_0_0_ThreadSafeStore_2_t855392148_gp_1_0_0_0_Types[] = { &ThreadSafeStore_2_t855392148_gp_0_0_0_0, &ThreadSafeStore_2_t855392148_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeStore_2_t855392148_gp_0_0_0_0_ThreadSafeStore_2_t855392148_gp_1_0_0_0 = { 2, GenInst_ThreadSafeStore_2_t855392148_gp_0_0_0_0_ThreadSafeStore_2_t855392148_gp_1_0_0_0_Types };
extern const Il2CppType ValidationUtils_ArgumentNotNullOrEmpty_m291675854_gp_0_0_0_0;
static const Il2CppType* GenInst_ValidationUtils_ArgumentNotNullOrEmpty_m291675854_gp_0_0_0_0_Types[] = { &ValidationUtils_ArgumentNotNullOrEmpty_m291675854_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ValidationUtils_ArgumentNotNullOrEmpty_m291675854_gp_0_0_0_0 = { 1, GenInst_ValidationUtils_ArgumentNotNullOrEmpty_m291675854_gp_0_0_0_0_Types };
extern const Il2CppType ValidationUtils_ArgumentNotNullOrEmpty_m1289647618_gp_0_0_0_0;
static const Il2CppType* GenInst_ValidationUtils_ArgumentNotNullOrEmpty_m1289647618_gp_0_0_0_0_Types[] = { &ValidationUtils_ArgumentNotNullOrEmpty_m1289647618_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ValidationUtils_ArgumentNotNullOrEmpty_m1289647618_gp_0_0_0_0 = { 1, GenInst_ValidationUtils_ArgumentNotNullOrEmpty_m1289647618_gp_0_0_0_0_Types };
extern const Il2CppType ValidationUtils_ArgumentIsPositive_m1157249580_gp_0_0_0_0;
static const Il2CppType* GenInst_ValidationUtils_ArgumentIsPositive_m1157249580_gp_0_0_0_0_Types[] = { &ValidationUtils_ArgumentIsPositive_m1157249580_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ValidationUtils_ArgumentIsPositive_m1157249580_gp_0_0_0_0 = { 1, GenInst_ValidationUtils_ArgumentIsPositive_m1157249580_gp_0_0_0_0_Types };
extern const Il2CppType Extensions_MaxBy_m3981111180_gp_0_0_0_0;
static const Il2CppType* GenInst_Extensions_MaxBy_m3981111180_gp_0_0_0_0_Types[] = { &Extensions_MaxBy_m3981111180_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_MaxBy_m3981111180_gp_0_0_0_0 = { 1, GenInst_Extensions_MaxBy_m3981111180_gp_0_0_0_0_Types };
extern const Il2CppType Extensions_MaxBy_m3981111180_gp_1_0_0_0;
static const Il2CppType* GenInst_Extensions_MaxBy_m3981111180_gp_0_0_0_0_Extensions_MaxBy_m3981111180_gp_1_0_0_0_Types[] = { &Extensions_MaxBy_m3981111180_gp_0_0_0_0, &Extensions_MaxBy_m3981111180_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_MaxBy_m3981111180_gp_0_0_0_0_Extensions_MaxBy_m3981111180_gp_1_0_0_0 = { 2, GenInst_Extensions_MaxBy_m3981111180_gp_0_0_0_0_Extensions_MaxBy_m3981111180_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Extensions_MaxBy_m3981111180_gp_1_0_0_0_Types[] = { &Extensions_MaxBy_m3981111180_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_MaxBy_m3981111180_gp_1_0_0_0 = { 1, GenInst_Extensions_MaxBy_m3981111180_gp_1_0_0_0_Types };
extern const Il2CppType MCGBehaviour_Subscribe_m2147325413_gp_0_0_0_0;
static const Il2CppType* GenInst_MCGBehaviour_Subscribe_m2147325413_gp_0_0_0_0_Types[] = { &MCGBehaviour_Subscribe_m2147325413_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MCGBehaviour_Subscribe_m2147325413_gp_0_0_0_0 = { 1, GenInst_MCGBehaviour_Subscribe_m2147325413_gp_0_0_0_0_Types };
extern const Il2CppType MCGBehaviour_Unsubscribe_m1867511264_gp_0_0_0_0;
static const Il2CppType* GenInst_MCGBehaviour_Unsubscribe_m1867511264_gp_0_0_0_0_Types[] = { &MCGBehaviour_Unsubscribe_m1867511264_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MCGBehaviour_Unsubscribe_m1867511264_gp_0_0_0_0 = { 1, GenInst_MCGBehaviour_Unsubscribe_m1867511264_gp_0_0_0_0_Types };
extern const Il2CppType MCGBehaviour_OnNext_m647838905_gp_0_0_0_0;
static const Il2CppType* GenInst_MCGBehaviour_OnNext_m647838905_gp_0_0_0_0_Types[] = { &MCGBehaviour_OnNext_m647838905_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MCGBehaviour_OnNext_m647838905_gp_0_0_0_0 = { 1, GenInst_MCGBehaviour_OnNext_m647838905_gp_0_0_0_0_Types };
extern const Il2CppType MCGBehaviour_OnNext_m4133885633_gp_0_0_0_0;
static const Il2CppType* GenInst_MCGBehaviour_OnNext_m4133885633_gp_0_0_0_0_Types[] = { &MCGBehaviour_OnNext_m4133885633_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MCGBehaviour_OnNext_m4133885633_gp_0_0_0_0 = { 1, GenInst_MCGBehaviour_OnNext_m4133885633_gp_0_0_0_0_Types };
extern const Il2CppType MessageSender_SendRequest_m4271204661_gp_1_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_MessageSender_SendRequest_m4271204661_gp_1_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &MessageSender_SendRequest_m4271204661_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_MessageSender_SendRequest_m4271204661_gp_1_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_MessageSender_SendRequest_m4271204661_gp_1_0_0_0_Types };
extern const Il2CppType MessageSender_SendRequest_m4271204661_gp_0_0_0_0;
static const Il2CppType* GenInst_MessageSender_SendRequest_m4271204661_gp_0_0_0_0_Types[] = { &MessageSender_SendRequest_m4271204661_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageSender_SendRequest_m4271204661_gp_0_0_0_0 = { 1, GenInst_MessageSender_SendRequest_m4271204661_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_MessageSender_SendRequest_m4271204661_gp_0_0_0_0_MessageSender_SendRequest_m4271204661_gp_1_0_0_0_Types[] = { &MessageSender_SendRequest_m4271204661_gp_0_0_0_0, &MessageSender_SendRequest_m4271204661_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageSender_SendRequest_m4271204661_gp_0_0_0_0_MessageSender_SendRequest_m4271204661_gp_1_0_0_0 = { 2, GenInst_MessageSender_SendRequest_m4271204661_gp_0_0_0_0_MessageSender_SendRequest_m4271204661_gp_1_0_0_0_Types };
extern const Il2CppType MessageSender_SendRequestCoroutine_m533839419_gp_1_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_MessageSender_SendRequestCoroutine_m533839419_gp_1_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &MessageSender_SendRequestCoroutine_m533839419_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_MessageSender_SendRequestCoroutine_m533839419_gp_1_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_MessageSender_SendRequestCoroutine_m533839419_gp_1_0_0_0_Types };
extern const Il2CppType MessageSender_SendRequestCoroutine_m533839419_gp_0_0_0_0;
static const Il2CppType* GenInst_MessageSender_SendRequestCoroutine_m533839419_gp_0_0_0_0_Types[] = { &MessageSender_SendRequestCoroutine_m533839419_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageSender_SendRequestCoroutine_m533839419_gp_0_0_0_0 = { 1, GenInst_MessageSender_SendRequestCoroutine_m533839419_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_MessageSender_SendRequestCoroutine_m533839419_gp_0_0_0_0_MessageSender_SendRequestCoroutine_m533839419_gp_1_0_0_0_Types[] = { &MessageSender_SendRequestCoroutine_m533839419_gp_0_0_0_0, &MessageSender_SendRequestCoroutine_m533839419_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageSender_SendRequestCoroutine_m533839419_gp_0_0_0_0_MessageSender_SendRequestCoroutine_m533839419_gp_1_0_0_0 = { 2, GenInst_MessageSender_SendRequestCoroutine_m533839419_gp_0_0_0_0_MessageSender_SendRequestCoroutine_m533839419_gp_1_0_0_0_Types };
extern const Il2CppType U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_1_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_1_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_1_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_1_0_0_0_Types };
extern const Il2CppType U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_0_0_0_0_Types[] = { &U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_0_0_0_0 = { 1, GenInst_U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_1_0_0_0_Types[] = { &U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_1_0_0_0 = { 1, GenInst_U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_1_0_0_0_Types };
extern const Il2CppType U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0;
extern const Il2CppType U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0_Types[] = { &U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0, &U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0 = { 2, GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_Types[] = { &U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0 = { 1, GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0_Types[] = { &U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0 = { 1, GenInst_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0_Types };
extern const Il2CppType GUILayer_t3254902478_0_0_0;
static const Il2CppType* GenInst_GUILayer_t3254902478_0_0_0_Types[] = { &GUILayer_t3254902478_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t3254902478_0_0_0 = { 1, GenInst_GUILayer_t3254902478_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_String_t_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_String_t_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType AsyncUtil_t423752048_0_0_0;
static const Il2CppType* GenInst_AsyncUtil_t423752048_0_0_0_Types[] = { &AsyncUtil_t423752048_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncUtil_t423752048_0_0_0 = { 1, GenInst_AsyncUtil_t423752048_0_0_0_Types };
extern const Il2CppType EventSystem_t3466835263_0_0_0;
static const Il2CppType* GenInst_EventSystem_t3466835263_0_0_0_Types[] = { &EventSystem_t3466835263_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t3466835263_0_0_0 = { 1, GenInst_EventSystem_t3466835263_0_0_0_Types };
extern const Il2CppType AxisEventData_t1524870173_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t1524870173_0_0_0_Types[] = { &AxisEventData_t1524870173_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t1524870173_0_0_0 = { 1, GenInst_AxisEventData_t1524870173_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t1209076198_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t1209076198_0_0_0_Types[] = { &SpriteRenderer_t1209076198_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t1209076198_0_0_0 = { 1, GenInst_SpriteRenderer_t1209076198_0_0_0_Types };
extern const Il2CppType AspectMode_t1166448724_0_0_0;
static const Il2CppType* GenInst_AspectMode_t1166448724_0_0_0_Types[] = { &AspectMode_t1166448724_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t1166448724_0_0_0 = { 1, GenInst_AspectMode_t1166448724_0_0_0_Types };
extern const Il2CppType FitMode_t4030874534_0_0_0;
static const Il2CppType* GenInst_FitMode_t4030874534_0_0_0_Types[] = { &FitMode_t4030874534_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t4030874534_0_0_0 = { 1, GenInst_FitMode_t4030874534_0_0_0_Types };
extern const Il2CppType Button_t2872111280_0_0_0;
static const Il2CppType* GenInst_Button_t2872111280_0_0_0_Types[] = { &Button_t2872111280_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t2872111280_0_0_0 = { 1, GenInst_Button_t2872111280_0_0_0_Types };
extern const Il2CppType RawImage_t2749640213_0_0_0;
static const Il2CppType* GenInst_RawImage_t2749640213_0_0_0_Types[] = { &RawImage_t2749640213_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t2749640213_0_0_0 = { 1, GenInst_RawImage_t2749640213_0_0_0_Types };
extern const Il2CppType Slider_t297367283_0_0_0;
static const Il2CppType* GenInst_Slider_t297367283_0_0_0_Types[] = { &Slider_t297367283_0_0_0 };
extern const Il2CppGenericInst GenInst_Slider_t297367283_0_0_0 = { 1, GenInst_Slider_t297367283_0_0_0_Types };
extern const Il2CppType Scrollbar_t3248359358_0_0_0;
static const Il2CppType* GenInst_Scrollbar_t3248359358_0_0_0_Types[] = { &Scrollbar_t3248359358_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_t3248359358_0_0_0 = { 1, GenInst_Scrollbar_t3248359358_0_0_0_Types };
extern const Il2CppType InputField_t1631627530_0_0_0;
static const Il2CppType* GenInst_InputField_t1631627530_0_0_0_Types[] = { &InputField_t1631627530_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t1631627530_0_0_0 = { 1, GenInst_InputField_t1631627530_0_0_0_Types };
extern const Il2CppType ScrollRect_t1199013257_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t1199013257_0_0_0_Types[] = { &ScrollRect_t1199013257_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t1199013257_0_0_0 = { 1, GenInst_ScrollRect_t1199013257_0_0_0_Types };
extern const Il2CppType Dropdown_t1985816271_0_0_0;
static const Il2CppType* GenInst_Dropdown_t1985816271_0_0_0_Types[] = { &Dropdown_t1985816271_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t1985816271_0_0_0 = { 1, GenInst_Dropdown_t1985816271_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t410733016_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t410733016_0_0_0_Types[] = { &GraphicRaycaster_t410733016_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t410733016_0_0_0 = { 1, GenInst_GraphicRaycaster_t410733016_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t261436805_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t261436805_0_0_0_Types[] = { &CanvasRenderer_t261436805_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t261436805_0_0_0 = { 1, GenInst_CanvasRenderer_t261436805_0_0_0_Types };
extern const Il2CppType Corner_t1077473318_0_0_0;
static const Il2CppType* GenInst_Corner_t1077473318_0_0_0_Types[] = { &Corner_t1077473318_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t1077473318_0_0_0 = { 1, GenInst_Corner_t1077473318_0_0_0_Types };
extern const Il2CppType Axis_t1431825778_0_0_0;
static const Il2CppType* GenInst_Axis_t1431825778_0_0_0_Types[] = { &Axis_t1431825778_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t1431825778_0_0_0 = { 1, GenInst_Axis_t1431825778_0_0_0_Types };
extern const Il2CppType Constraint_t3558160636_0_0_0;
static const Il2CppType* GenInst_Constraint_t3558160636_0_0_0_Types[] = { &Constraint_t3558160636_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t3558160636_0_0_0 = { 1, GenInst_Constraint_t3558160636_0_0_0_Types };
extern const Il2CppType Type_t3352948571_0_0_0;
static const Il2CppType* GenInst_Type_t3352948571_0_0_0_Types[] = { &Type_t3352948571_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t3352948571_0_0_0 = { 1, GenInst_Type_t3352948571_0_0_0_Types };
extern const Il2CppType FillMethod_t1640962579_0_0_0;
static const Il2CppType* GenInst_FillMethod_t1640962579_0_0_0_Types[] = { &FillMethod_t1640962579_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t1640962579_0_0_0 = { 1, GenInst_FillMethod_t1640962579_0_0_0_Types };
extern const Il2CppType SubmitEvent_t907918422_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t907918422_0_0_0_Types[] = { &SubmitEvent_t907918422_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t907918422_0_0_0 = { 1, GenInst_SubmitEvent_t907918422_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t2863344003_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t2863344003_0_0_0_Types[] = { &OnChangeEvent_t2863344003_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2863344003_0_0_0 = { 1, GenInst_OnChangeEvent_t2863344003_0_0_0_Types };
extern const Il2CppType OnValidateInput_t1946318473_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t1946318473_0_0_0_Types[] = { &OnValidateInput_t1946318473_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t1946318473_0_0_0 = { 1, GenInst_OnValidateInput_t1946318473_0_0_0_Types };
extern const Il2CppType LineType_t2931319356_0_0_0;
static const Il2CppType* GenInst_LineType_t2931319356_0_0_0_Types[] = { &LineType_t2931319356_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t2931319356_0_0_0 = { 1, GenInst_LineType_t2931319356_0_0_0_Types };
extern const Il2CppType InputType_t1274231802_0_0_0;
static const Il2CppType* GenInst_InputType_t1274231802_0_0_0_Types[] = { &InputType_t1274231802_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t1274231802_0_0_0 = { 1, GenInst_InputType_t1274231802_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t875112366_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types[] = { &TouchScreenKeyboardType_t875112366_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t875112366_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types };
extern const Il2CppType CharacterValidation_t3437478890_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t3437478890_0_0_0_Types[] = { &CharacterValidation_t3437478890_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t3437478890_0_0_0 = { 1, GenInst_CharacterValidation_t3437478890_0_0_0_Types };
extern const Il2CppType LayoutElement_t2808691390_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t2808691390_0_0_0_Types[] = { &LayoutElement_t2808691390_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t2808691390_0_0_0 = { 1, GenInst_LayoutElement_t2808691390_0_0_0_Types };
extern const Il2CppType RectOffset_t3387826427_0_0_0;
static const Il2CppType* GenInst_RectOffset_t3387826427_0_0_0_Types[] = { &RectOffset_t3387826427_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t3387826427_0_0_0 = { 1, GenInst_RectOffset_t3387826427_0_0_0_Types };
extern const Il2CppType TextAnchor_t112990806_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t112990806_0_0_0_Types[] = { &TextAnchor_t112990806_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t112990806_0_0_0 = { 1, GenInst_TextAnchor_t112990806_0_0_0_Types };
extern const Il2CppType Direction_t3696775921_0_0_0;
static const Il2CppType* GenInst_Direction_t3696775921_0_0_0_Types[] = { &Direction_t3696775921_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t3696775921_0_0_0 = { 1, GenInst_Direction_t3696775921_0_0_0_Types };
extern const Il2CppType Transition_t605142169_0_0_0;
static const Il2CppType* GenInst_Transition_t605142169_0_0_0_Types[] = { &Transition_t605142169_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t605142169_0_0_0 = { 1, GenInst_Transition_t605142169_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t3244928895_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t3244928895_0_0_0_Types[] = { &AnimationTriggers_t3244928895_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t3244928895_0_0_0 = { 1, GenInst_AnimationTriggers_t3244928895_0_0_0_Types };
extern const Il2CppType Animator_t69676727_0_0_0;
static const Il2CppType* GenInst_Animator_t69676727_0_0_0_Types[] = { &Animator_t69676727_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t69676727_0_0_0 = { 1, GenInst_Animator_t69676727_0_0_0_Types };
extern const Il2CppType Direction_t1525323322_0_0_0;
static const Il2CppType* GenInst_Direction_t1525323322_0_0_0_Types[] = { &Direction_t1525323322_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t1525323322_0_0_0 = { 1, GenInst_Direction_t1525323322_0_0_0_Types };
extern const Il2CppType IAmazonExtensions_t3890253245_0_0_0;
static const Il2CppType* GenInst_IAmazonExtensions_t3890253245_0_0_0_Types[] = { &IAmazonExtensions_t3890253245_0_0_0 };
extern const Il2CppGenericInst GenInst_IAmazonExtensions_t3890253245_0_0_0 = { 1, GenInst_IAmazonExtensions_t3890253245_0_0_0_Types };
extern const Il2CppType IAmazonConfiguration_t3016942165_0_0_0;
static const Il2CppType* GenInst_IAmazonConfiguration_t3016942165_0_0_0_Types[] = { &IAmazonConfiguration_t3016942165_0_0_0 };
extern const Il2CppGenericInst GenInst_IAmazonConfiguration_t3016942165_0_0_0 = { 1, GenInst_IAmazonConfiguration_t3016942165_0_0_0_Types };
extern const Il2CppType ISamsungAppsExtensions_t3429739537_0_0_0;
static const Il2CppType* GenInst_ISamsungAppsExtensions_t3429739537_0_0_0_Types[] = { &ISamsungAppsExtensions_t3429739537_0_0_0 };
extern const Il2CppGenericInst GenInst_ISamsungAppsExtensions_t3429739537_0_0_0 = { 1, GenInst_ISamsungAppsExtensions_t3429739537_0_0_0_Types };
extern const Il2CppType ISamsungAppsConfiguration_t4066821689_0_0_0;
static const Il2CppType* GenInst_ISamsungAppsConfiguration_t4066821689_0_0_0_Types[] = { &ISamsungAppsConfiguration_t4066821689_0_0_0 };
extern const Il2CppGenericInst GenInst_ISamsungAppsConfiguration_t4066821689_0_0_0 = { 1, GenInst_ISamsungAppsConfiguration_t4066821689_0_0_0_Types };
extern const Il2CppType UnityUtil_t166323129_0_0_0;
static const Il2CppType* GenInst_UnityUtil_t166323129_0_0_0_Types[] = { &UnityUtil_t166323129_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityUtil_t166323129_0_0_0 = { 1, GenInst_UnityUtil_t166323129_0_0_0_Types };
extern const Il2CppType IAppleExtensions_t1627764765_0_0_0;
static const Il2CppType* GenInst_IAppleExtensions_t1627764765_0_0_0_Types[] = { &IAppleExtensions_t1627764765_0_0_0 };
extern const Il2CppGenericInst GenInst_IAppleExtensions_t1627764765_0_0_0 = { 1, GenInst_IAppleExtensions_t1627764765_0_0_0_Types };
extern const Il2CppType IAppleConfiguration_t3277762425_0_0_0;
static const Il2CppType* GenInst_IAppleConfiguration_t3277762425_0_0_0_Types[] = { &IAppleConfiguration_t3277762425_0_0_0 };
extern const Il2CppGenericInst GenInst_IAppleConfiguration_t3277762425_0_0_0 = { 1, GenInst_IAppleConfiguration_t3277762425_0_0_0_Types };
extern const Il2CppType IMicrosoftConfiguration_t1212838845_0_0_0;
static const Il2CppType* GenInst_IMicrosoftConfiguration_t1212838845_0_0_0_Types[] = { &IMicrosoftConfiguration_t1212838845_0_0_0 };
extern const Il2CppGenericInst GenInst_IMicrosoftConfiguration_t1212838845_0_0_0 = { 1, GenInst_IMicrosoftConfiguration_t1212838845_0_0_0_Types };
extern const Il2CppType IGooglePlayConfiguration_t2615679878_0_0_0;
static const Il2CppType* GenInst_IGooglePlayConfiguration_t2615679878_0_0_0_Types[] = { &IGooglePlayConfiguration_t2615679878_0_0_0 };
extern const Il2CppGenericInst GenInst_IGooglePlayConfiguration_t2615679878_0_0_0 = { 1, GenInst_IGooglePlayConfiguration_t2615679878_0_0_0_Types };
extern const Il2CppType ITizenStoreConfiguration_t2900348728_0_0_0;
static const Il2CppType* GenInst_ITizenStoreConfiguration_t2900348728_0_0_0_Types[] = { &ITizenStoreConfiguration_t2900348728_0_0_0 };
extern const Il2CppGenericInst GenInst_ITizenStoreConfiguration_t2900348728_0_0_0 = { 1, GenInst_ITizenStoreConfiguration_t2900348728_0_0_0_Types };
extern const Il2CppType IAndroidStoreSelection_t3134941501_0_0_0;
static const Il2CppType* GenInst_IAndroidStoreSelection_t3134941501_0_0_0_Types[] = { &IAndroidStoreSelection_t3134941501_0_0_0 };
extern const Il2CppGenericInst GenInst_IAndroidStoreSelection_t3134941501_0_0_0 = { 1, GenInst_IAndroidStoreSelection_t3134941501_0_0_0_Types };
extern const Il2CppType LifecycleNotifier_t1057582876_0_0_0;
static const Il2CppType* GenInst_LifecycleNotifier_t1057582876_0_0_0_Types[] = { &LifecycleNotifier_t1057582876_0_0_0 };
extern const Il2CppGenericInst GenInst_LifecycleNotifier_t1057582876_0_0_0 = { 1, GenInst_LifecycleNotifier_t1057582876_0_0_0_Types };
extern const Il2CppType StandaloneInputModule_t70867863_0_0_0;
static const Il2CppType* GenInst_StandaloneInputModule_t70867863_0_0_0_Types[] = { &StandaloneInputModule_t70867863_0_0_0 };
extern const Il2CppGenericInst GenInst_StandaloneInputModule_t70867863_0_0_0 = { 1, GenInst_StandaloneInputModule_t70867863_0_0_0_Types };
extern const Il2CppType GameCenterManager_t1487113918_0_0_0;
static const Il2CppType* GenInst_GameCenterManager_t1487113918_0_0_0_Types[] = { &GameCenterManager_t1487113918_0_0_0 };
extern const Il2CppGenericInst GenInst_GameCenterManager_t1487113918_0_0_0 = { 1, GenInst_GameCenterManager_t1487113918_0_0_0_Types };
extern const Il2CppType IOSDialog_t3518705031_0_0_0;
static const Il2CppType* GenInst_IOSDialog_t3518705031_0_0_0_Types[] = { &IOSDialog_t3518705031_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSDialog_t3518705031_0_0_0 = { 1, GenInst_IOSDialog_t3518705031_0_0_0_Types };
extern const Il2CppType IOSMessage_t2569463336_0_0_0;
static const Il2CppType* GenInst_IOSMessage_t2569463336_0_0_0_Types[] = { &IOSMessage_t2569463336_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSMessage_t2569463336_0_0_0 = { 1, GenInst_IOSMessage_t2569463336_0_0_0_Types };
extern const Il2CppType IOSNativePreviewBackButton_t2513092733_0_0_0;
static const Il2CppType* GenInst_IOSNativePreviewBackButton_t2513092733_0_0_0_Types[] = { &IOSNativePreviewBackButton_t2513092733_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSNativePreviewBackButton_t2513092733_0_0_0 = { 1, GenInst_IOSNativePreviewBackButton_t2513092733_0_0_0_Types };
extern const Il2CppType IOSNativeSettings_t547170227_0_0_0;
static const Il2CppType* GenInst_IOSNativeSettings_t547170227_0_0_0_Types[] = { &IOSNativeSettings_t547170227_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSNativeSettings_t547170227_0_0_0 = { 1, GenInst_IOSNativeSettings_t547170227_0_0_0_Types };
extern const Il2CppType IOSRateUsPopUp_t2222998473_0_0_0;
static const Il2CppType* GenInst_IOSRateUsPopUp_t2222998473_0_0_0_Types[] = { &IOSRateUsPopUp_t2222998473_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSRateUsPopUp_t2222998473_0_0_0 = { 1, GenInst_IOSRateUsPopUp_t2222998473_0_0_0_Types };
extern const Il2CppType ConnectionButton_t3853092004_0_0_0;
static const Il2CppType* GenInst_ConnectionButton_t3853092004_0_0_0_Types[] = { &ConnectionButton_t3853092004_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionButton_t3853092004_0_0_0 = { 1, GenInst_ConnectionButton_t3853092004_0_0_0_Types };
extern const Il2CppType DisconnectButton_t3965161390_0_0_0;
static const Il2CppType* GenInst_DisconnectButton_t3965161390_0_0_0_Types[] = { &DisconnectButton_t3965161390_0_0_0 };
extern const Il2CppGenericInst GenInst_DisconnectButton_t3965161390_0_0_0 = { 1, GenInst_DisconnectButton_t3965161390_0_0_0_Types };
extern const Il2CppType ClickManagerExample_t799098483_0_0_0;
static const Il2CppType* GenInst_ClickManagerExample_t799098483_0_0_0_Types[] = { &ClickManagerExample_t799098483_0_0_0 };
extern const Il2CppGenericInst GenInst_ClickManagerExample_t799098483_0_0_0 = { 1, GenInst_ClickManagerExample_t799098483_0_0_0_Types };
extern const Il2CppType Renderer_t257310565_0_0_0;
static const Il2CppType* GenInst_Renderer_t257310565_0_0_0_Types[] = { &Renderer_t257310565_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t257310565_0_0_0 = { 1, GenInst_Renderer_t257310565_0_0_0_Types };
extern const Il2CppType GUITexture_t1909122990_0_0_0;
static const Il2CppType* GenInst_GUITexture_t1909122990_0_0_0_Types[] = { &GUITexture_t1909122990_0_0_0 };
extern const Il2CppGenericInst GenInst_GUITexture_t1909122990_0_0_0 = { 1, GenInst_GUITexture_t1909122990_0_0_0_Types };
extern const Il2CppType GUIText_t2411476300_0_0_0;
static const Il2CppType* GenInst_GUIText_t2411476300_0_0_0_Types[] = { &GUIText_t2411476300_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIText_t2411476300_0_0_0 = { 1, GenInst_GUIText_t2411476300_0_0_0_Types };
extern const Il2CppType Light_t494725636_0_0_0;
static const Il2CppType* GenInst_Light_t494725636_0_0_0_Types[] = { &Light_t494725636_0_0_0 };
extern const Il2CppGenericInst GenInst_Light_t494725636_0_0_0 = { 1, GenInst_Light_t494725636_0_0_0_Types };
extern const Il2CppType AudioSource_t1135106623_0_0_0;
static const Il2CppType* GenInst_AudioSource_t1135106623_0_0_0_Types[] = { &AudioSource_t1135106623_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t1135106623_0_0_0 = { 1, GenInst_AudioSource_t1135106623_0_0_0_Types };
extern const Il2CppType Rigidbody_t4233889191_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t4233889191_0_0_0_Types[] = { &Rigidbody_t4233889191_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t4233889191_0_0_0 = { 1, GenInst_Rigidbody_t4233889191_0_0_0_Types };
extern const Il2CppType SA_iTween_t1938847631_0_0_0;
static const Il2CppType* GenInst_SA_iTween_t1938847631_0_0_0_Types[] = { &SA_iTween_t1938847631_0_0_0 };
extern const Il2CppGenericInst GenInst_SA_iTween_t1938847631_0_0_0 = { 1, GenInst_SA_iTween_t1938847631_0_0_0_Types };
extern const Il2CppType ValuesTween_t4181447589_0_0_0;
static const Il2CppType* GenInst_ValuesTween_t4181447589_0_0_0_Types[] = { &ValuesTween_t4181447589_0_0_0 };
extern const Il2CppGenericInst GenInst_ValuesTween_t4181447589_0_0_0 = { 1, GenInst_ValuesTween_t4181447589_0_0_0_Types };
extern const Il2CppType Invoker_t994751176_0_0_0;
static const Il2CppType* GenInst_Invoker_t994751176_0_0_0_Types[] = { &Invoker_t994751176_0_0_0 };
extern const Il2CppGenericInst GenInst_Invoker_t994751176_0_0_0 = { 1, GenInst_Invoker_t994751176_0_0_0_Types };
extern const Il2CppType PrefabAsyncLoader_t3449223737_0_0_0;
static const Il2CppType* GenInst_PrefabAsyncLoader_t3449223737_0_0_0_Types[] = { &PrefabAsyncLoader_t3449223737_0_0_0 };
extern const Il2CppGenericInst GenInst_PrefabAsyncLoader_t3449223737_0_0_0 = { 1, GenInst_PrefabAsyncLoader_t3449223737_0_0_0_Types };
extern const Il2CppType ScreenshotMaker_t4063620334_0_0_0;
static const Il2CppType* GenInst_ScreenshotMaker_t4063620334_0_0_0_Types[] = { &ScreenshotMaker_t4063620334_0_0_0 };
extern const Il2CppGenericInst GenInst_ScreenshotMaker_t4063620334_0_0_0 = { 1, GenInst_ScreenshotMaker_t4063620334_0_0_0_Types };
extern const Il2CppType WWWTextureLoader_t499142963_0_0_0;
static const Il2CppType* GenInst_WWWTextureLoader_t499142963_0_0_0_Types[] = { &WWWTextureLoader_t499142963_0_0_0 };
extern const Il2CppGenericInst GenInst_WWWTextureLoader_t499142963_0_0_0 = { 1, GenInst_WWWTextureLoader_t499142963_0_0_0_Types };
extern const Il2CppType ISD_Settings_t1116242554_0_0_0;
static const Il2CppType* GenInst_ISD_Settings_t1116242554_0_0_0_Types[] = { &ISD_Settings_t1116242554_0_0_0 };
extern const Il2CppGenericInst GenInst_ISD_Settings_t1116242554_0_0_0 = { 1, GenInst_ISD_Settings_t1116242554_0_0_0_Types };
extern const Il2CppType AN_PlusShareListener_t257800803_0_0_0;
static const Il2CppType* GenInst_AN_PlusShareListener_t257800803_0_0_0_Types[] = { &AN_PlusShareListener_t257800803_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_PlusShareListener_t257800803_0_0_0 = { 1, GenInst_AN_PlusShareListener_t257800803_0_0_0_Types };
extern const Il2CppType AndroidDialog_t3283474837_0_0_0;
static const Il2CppType* GenInst_AndroidDialog_t3283474837_0_0_0_Types[] = { &AndroidDialog_t3283474837_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidDialog_t3283474837_0_0_0 = { 1, GenInst_AndroidDialog_t3283474837_0_0_0_Types };
extern const Il2CppType AndroidMessage_t2504997174_0_0_0;
static const Il2CppType* GenInst_AndroidMessage_t2504997174_0_0_0_Types[] = { &AndroidMessage_t2504997174_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidMessage_t2504997174_0_0_0 = { 1, GenInst_AndroidMessage_t2504997174_0_0_0_Types };
extern const Il2CppType AndroidNativeSettings_t3397157021_0_0_0;
static const Il2CppType* GenInst_AndroidNativeSettings_t3397157021_0_0_0_Types[] = { &AndroidNativeSettings_t3397157021_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidNativeSettings_t3397157021_0_0_0 = { 1, GenInst_AndroidNativeSettings_t3397157021_0_0_0_Types };
extern const Il2CppType AndroidRateUsPopUp_t1491925263_0_0_0;
static const Il2CppType* GenInst_AndroidRateUsPopUp_t1491925263_0_0_0_Types[] = { &AndroidRateUsPopUp_t1491925263_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidRateUsPopUp_t1491925263_0_0_0 = { 1, GenInst_AndroidRateUsPopUp_t1491925263_0_0_0_Types };
extern const Il2CppType Spinner_t3781576355_0_0_0;
static const Il2CppType* GenInst_Spinner_t3781576355_0_0_0_Types[] = { &Spinner_t3781576355_0_0_0 };
extern const Il2CppGenericInst GenInst_Spinner_t3781576355_0_0_0 = { 1, GenInst_Spinner_t3781576355_0_0_0_Types };
extern const Il2CppType JumpTypeCycle_t3789588250_0_0_0;
static const Il2CppType* GenInst_JumpTypeCycle_t3789588250_0_0_0_Types[] = { &JumpTypeCycle_t3789588250_0_0_0 };
extern const Il2CppGenericInst GenInst_JumpTypeCycle_t3789588250_0_0_0 = { 1, GenInst_JumpTypeCycle_t3789588250_0_0_0_Types };
extern const Il2CppType Crop_t2548131222_0_0_0;
static const Il2CppType* GenInst_Crop_t2548131222_0_0_0_Types[] = { &Crop_t2548131222_0_0_0 };
extern const Il2CppGenericInst GenInst_Crop_t2548131222_0_0_0 = { 1, GenInst_Crop_t2548131222_0_0_0_Types };
static const Il2CppType* GenInst_GetCourseRequest_t2656580458_0_0_0_GetCourseResponse_t2659116210_0_0_0_Types[] = { &GetCourseRequest_t2656580458_0_0_0, &GetCourseResponse_t2659116210_0_0_0 };
extern const Il2CppGenericInst GenInst_GetCourseRequest_t2656580458_0_0_0_GetCourseResponse_t2659116210_0_0_0 = { 2, GenInst_GetCourseRequest_t2656580458_0_0_0_GetCourseResponse_t2659116210_0_0_0_Types };
extern const Il2CppType Jump_Collider_t2378236225_0_0_0;
static const Il2CppType* GenInst_Jump_Collider_t2378236225_0_0_0_Types[] = { &Jump_Collider_t2378236225_0_0_0 };
extern const Il2CppGenericInst GenInst_Jump_Collider_t2378236225_0_0_0 = { 1, GenInst_Jump_Collider_t2378236225_0_0_0_Types };
static const Il2CppType* GenInst_CreateCourseRequest_t4267998642_0_0_0_CreateCourseResponse_t3800683746_0_0_0_Types[] = { &CreateCourseRequest_t4267998642_0_0_0, &CreateCourseResponse_t3800683746_0_0_0 };
extern const Il2CppGenericInst GenInst_CreateCourseRequest_t4267998642_0_0_0_CreateCourseResponse_t3800683746_0_0_0 = { 2, GenInst_CreateCourseRequest_t4267998642_0_0_0_CreateCourseResponse_t3800683746_0_0_0_Types };
static const Il2CppType* GenInst_DeleteCourseRequest_t1683798751_0_0_0_DeleteCourseResponse_t1898856809_0_0_0_Types[] = { &DeleteCourseRequest_t1683798751_0_0_0, &DeleteCourseResponse_t1898856809_0_0_0 };
extern const Il2CppGenericInst GenInst_DeleteCourseRequest_t1683798751_0_0_0_DeleteCourseResponse_t1898856809_0_0_0 = { 2, GenInst_DeleteCourseRequest_t1683798751_0_0_0_DeleteCourseResponse_t1898856809_0_0_0_Types };
extern const Il2CppType CourseObject_t1071976694_0_0_0;
static const Il2CppType* GenInst_CourseObject_t1071976694_0_0_0_Types[] = { &CourseObject_t1071976694_0_0_0 };
extern const Il2CppGenericInst GenInst_CourseObject_t1071976694_0_0_0 = { 1, GenInst_CourseObject_t1071976694_0_0_0_Types };
extern const Il2CppType HorseAnimation_t2036550335_0_0_0;
static const Il2CppType* GenInst_HorseAnimation_t2036550335_0_0_0_Types[] = { &HorseAnimation_t2036550335_0_0_0 };
extern const Il2CppGenericInst GenInst_HorseAnimation_t2036550335_0_0_0 = { 1, GenInst_HorseAnimation_t2036550335_0_0_0_Types };
extern const Il2CppType EnvironmentSelect_t3424254515_0_0_0;
static const Il2CppType* GenInst_EnvironmentSelect_t3424254515_0_0_0_Types[] = { &EnvironmentSelect_t3424254515_0_0_0 };
extern const Il2CppGenericInst GenInst_EnvironmentSelect_t3424254515_0_0_0 = { 1, GenInst_EnvironmentSelect_t3424254515_0_0_0_Types };
extern const Il2CppType LineRenderer_t849157671_0_0_0;
static const Il2CppType* GenInst_LineRenderer_t849157671_0_0_0_Types[] = { &LineRenderer_t849157671_0_0_0 };
extern const Il2CppGenericInst GenInst_LineRenderer_t849157671_0_0_0 = { 1, GenInst_LineRenderer_t849157671_0_0_0_Types };
extern const Il2CppType List_1_t1317062444_0_0_0;
static const Il2CppType* GenInst_List_1_t1317062444_0_0_0_Types[] = { &List_1_t1317062444_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1317062444_0_0_0 = { 1, GenInst_List_1_t1317062444_0_0_0_Types };
extern const Il2CppType CourseVisualizer_t3597553323_0_0_0;
static const Il2CppType* GenInst_CourseVisualizer_t3597553323_0_0_0_Types[] = { &CourseVisualizer_t3597553323_0_0_0 };
extern const Il2CppGenericInst GenInst_CourseVisualizer_t3597553323_0_0_0 = { 1, GenInst_CourseVisualizer_t3597553323_0_0_0_Types };
extern const Il2CppType EventTrigger_t1967201810_0_0_0;
static const Il2CppType* GenInst_EventTrigger_t1967201810_0_0_0_Types[] = { &EventTrigger_t1967201810_0_0_0 };
extern const Il2CppGenericInst GenInst_EventTrigger_t1967201810_0_0_0 = { 1, GenInst_EventTrigger_t1967201810_0_0_0_Types };
static const Il2CppType* GenInst_FB_LikesRetrieveTask_t2495592094_0_0_0_Types[] = { &FB_LikesRetrieveTask_t2495592094_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_LikesRetrieveTask_t2495592094_0_0_0 = { 1, GenInst_FB_LikesRetrieveTask_t2495592094_0_0_0_Types };
extern const Il2CppType FB_PostingTask_t2068490008_0_0_0;
static const Il2CppType* GenInst_FB_PostingTask_t2068490008_0_0_0_Types[] = { &FB_PostingTask_t2068490008_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_PostingTask_t2068490008_0_0_0 = { 1, GenInst_FB_PostingTask_t2068490008_0_0_0_Types };
extern const Il2CppType CoursesNumberExceeded_t3039661446_0_0_0;
static const Il2CppType* GenInst_CoursesNumberExceeded_t3039661446_0_0_0_Types[] = { &CoursesNumberExceeded_t3039661446_0_0_0 };
extern const Il2CppGenericInst GenInst_CoursesNumberExceeded_t3039661446_0_0_0 = { 1, GenInst_CoursesNumberExceeded_t3039661446_0_0_0_Types };
extern const Il2CppType InitAndroidInventoryTask_t4246510222_0_0_0;
static const Il2CppType* GenInst_InitAndroidInventoryTask_t4246510222_0_0_0_Types[] = { &InitAndroidInventoryTask_t4246510222_0_0_0 };
extern const Il2CppGenericInst GenInst_InitAndroidInventoryTask_t4246510222_0_0_0 = { 1, GenInst_InitAndroidInventoryTask_t4246510222_0_0_0_Types };
extern const Il2CppType List_1_t1357208527_0_0_0;
static const Il2CppType* GenInst_List_1_t1357208527_0_0_0_Types[] = { &List_1_t1357208527_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1357208527_0_0_0 = { 1, GenInst_List_1_t1357208527_0_0_0_Types };
extern const Il2CppType List_1_t2294885245_0_0_0;
static const Il2CppType* GenInst_List_1_t2294885245_0_0_0_Types[] = { &List_1_t2294885245_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2294885245_0_0_0 = { 1, GenInst_List_1_t2294885245_0_0_0_Types };
extern const Il2CppType SampleExternalClass_t2192168175_0_0_0;
static const Il2CppType* GenInst_SampleExternalClass_t2192168175_0_0_0_Types[] = { &SampleExternalClass_t2192168175_0_0_0 };
extern const Il2CppGenericInst GenInst_SampleExternalClass_t2192168175_0_0_0 = { 1, GenInst_SampleExternalClass_t2192168175_0_0_0_Types };
extern const Il2CppType Dictionary_2_t1933589748_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t1933589748_0_0_0_Types[] = { &Dictionary_2_t1933589748_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1933589748_0_0_0 = { 1, GenInst_Dictionary_2_t1933589748_0_0_0_Types };
extern const Il2CppType Dictionary_2_t4254316062_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t4254316062_0_0_0_Types[] = { &Dictionary_2_t4254316062_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4254316062_0_0_0 = { 1, GenInst_Dictionary_2_t4254316062_0_0_0_Types };
static const Il2CppType* GenInst_ForgotPasswordRequest_t1886688323_0_0_0_ForgotPasswordResponse_t4062332037_0_0_0_Types[] = { &ForgotPasswordRequest_t1886688323_0_0_0, &ForgotPasswordResponse_t4062332037_0_0_0 };
extern const Il2CppGenericInst GenInst_ForgotPasswordRequest_t1886688323_0_0_0_ForgotPasswordResponse_t4062332037_0_0_0 = { 2, GenInst_ForgotPasswordRequest_t1886688323_0_0_0_ForgotPasswordResponse_t4062332037_0_0_0_Types };
static const Il2CppType* GenInst_ChangePasswordRequest_t4102061450_0_0_0_ChangePasswordResponse_t3897893034_0_0_0_Types[] = { &ChangePasswordRequest_t4102061450_0_0_0, &ChangePasswordResponse_t3897893034_0_0_0 };
extern const Il2CppGenericInst GenInst_ChangePasswordRequest_t4102061450_0_0_0_ChangePasswordResponse_t3897893034_0_0_0 = { 2, GenInst_ChangePasswordRequest_t4102061450_0_0_0_ChangePasswordResponse_t3897893034_0_0_0_Types };
static const Il2CppType* GenInst_LoginRequest_t2273172322_0_0_0_LoginResponse_t1319408382_0_0_0_Types[] = { &LoginRequest_t2273172322_0_0_0, &LoginResponse_t1319408382_0_0_0 };
extern const Il2CppGenericInst GenInst_LoginRequest_t2273172322_0_0_0_LoginResponse_t1319408382_0_0_0 = { 2, GenInst_LoginRequest_t2273172322_0_0_0_LoginResponse_t1319408382_0_0_0_Types };
extern const Il2CppType Toggle_t3489774764_0_0_0;
static const Il2CppType* GenInst_Toggle_t3489774764_0_0_0_Types[] = { &Toggle_t3489774764_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3489774764_0_0_0 = { 1, GenInst_Toggle_t3489774764_0_0_0_Types };
static const Il2CppType* GenInst_RegisterRequest_t698226714_0_0_0_RegisterResponse_t410466074_0_0_0_Types[] = { &RegisterRequest_t698226714_0_0_0, &RegisterResponse_t410466074_0_0_0 };
extern const Il2CppGenericInst GenInst_RegisterRequest_t698226714_0_0_0_RegisterResponse_t410466074_0_0_0 = { 2, GenInst_RegisterRequest_t698226714_0_0_0_RegisterResponse_t410466074_0_0_0_Types };
static const Il2CppType* GenInst_JsonSchemaModel_t708894576_0_0_0_String_t_0_0_0_Types[] = { &JsonSchemaModel_t708894576_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchemaModel_t708894576_0_0_0_String_t_0_0_0 = { 2, GenInst_JsonSchemaModel_t708894576_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_JObject_t278519297_0_0_0_JProperty_t2956441399_0_0_0_Types[] = { &JObject_t278519297_0_0_0, &JProperty_t2956441399_0_0_0 };
extern const Il2CppGenericInst GenInst_JObject_t278519297_0_0_0_JProperty_t2956441399_0_0_0 = { 2, GenInst_JObject_t278519297_0_0_0_JProperty_t2956441399_0_0_0_Types };
static const Il2CppType* GenInst_JToken_t2552644013_0_0_0_JToken_t2552644013_0_0_0_Types[] = { &JToken_t2552644013_0_0_0, &JToken_t2552644013_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t2552644013_0_0_0_JToken_t2552644013_0_0_0 = { 2, GenInst_JToken_t2552644013_0_0_0_JToken_t2552644013_0_0_0_Types };
extern const Il2CppType DescriptionAttribute_t3207779672_0_0_0;
static const Il2CppType* GenInst_DescriptionAttribute_t3207779672_0_0_0_Types[] = { &DescriptionAttribute_t3207779672_0_0_0 };
extern const Il2CppGenericInst GenInst_DescriptionAttribute_t3207779672_0_0_0 = { 1, GenInst_DescriptionAttribute_t3207779672_0_0_0_Types };
extern const Il2CppType JsonPropertyAttribute_t2023370155_0_0_0;
static const Il2CppType* GenInst_JsonPropertyAttribute_t2023370155_0_0_0_Types[] = { &JsonPropertyAttribute_t2023370155_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonPropertyAttribute_t2023370155_0_0_0 = { 1, GenInst_JsonPropertyAttribute_t2023370155_0_0_0_Types };
extern const Il2CppType JsonIgnoreAttribute_t898097550_0_0_0;
static const Il2CppType* GenInst_JsonIgnoreAttribute_t898097550_0_0_0_Types[] = { &JsonIgnoreAttribute_t898097550_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonIgnoreAttribute_t898097550_0_0_0 = { 1, GenInst_JsonIgnoreAttribute_t898097550_0_0_0_Types };
extern const Il2CppType DefaultValueAttribute_t1302720498_0_0_0;
static const Il2CppType* GenInst_DefaultValueAttribute_t1302720498_0_0_0_Types[] = { &DefaultValueAttribute_t1302720498_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultValueAttribute_t1302720498_0_0_0 = { 1, GenInst_DefaultValueAttribute_t1302720498_0_0_0_Types };
static const Il2CppType* GenInst_ParameterInfo_t2249040075_0_0_0_ParameterInfo_t2249040075_0_0_0_Il2CppObject_0_0_0_Types[] = { &ParameterInfo_t2249040075_0_0_0, &ParameterInfo_t2249040075_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0_ParameterInfo_t2249040075_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_ParameterInfo_t2249040075_0_0_0_ParameterInfo_t2249040075_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_JsonProperty_t2712067825_0_0_0_JsonProperty_t2712067825_0_0_0_PropertyPresence_t220200932_0_0_0_Types[] = { &JsonProperty_t2712067825_0_0_0, &JsonProperty_t2712067825_0_0_0, &PropertyPresence_t220200932_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t2712067825_0_0_0_JsonProperty_t2712067825_0_0_0_PropertyPresence_t220200932_0_0_0 = { 3, GenInst_JsonProperty_t2712067825_0_0_0_JsonProperty_t2712067825_0_0_0_PropertyPresence_t220200932_0_0_0_Types };
extern const Il2CppType JsonConverterAttribute_t3918837738_0_0_0;
static const Il2CppType* GenInst_JsonConverterAttribute_t3918837738_0_0_0_Types[] = { &JsonConverterAttribute_t3918837738_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonConverterAttribute_t3918837738_0_0_0 = { 1, GenInst_JsonConverterAttribute_t3918837738_0_0_0_Types };
extern const Il2CppType PreviewScreenUtil_t19016308_0_0_0;
static const Il2CppType* GenInst_PreviewScreenUtil_t19016308_0_0_0_Types[] = { &PreviewScreenUtil_t19016308_0_0_0 };
extern const Il2CppGenericInst GenInst_PreviewScreenUtil_t19016308_0_0_0 = { 1, GenInst_PreviewScreenUtil_t19016308_0_0_0_Types };
extern const Il2CppType Collider_t3497673348_0_0_0;
static const Il2CppType* GenInst_Collider_t3497673348_0_0_0_Types[] = { &Collider_t3497673348_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t3497673348_0_0_0 = { 1, GenInst_Collider_t3497673348_0_0_0_Types };
extern const Il2CppType SA_StatusBar_t1378080260_0_0_0;
static const Il2CppType* GenInst_SA_StatusBar_t1378080260_0_0_0_Types[] = { &SA_StatusBar_t1378080260_0_0_0 };
extern const Il2CppGenericInst GenInst_SA_StatusBar_t1378080260_0_0_0 = { 1, GenInst_SA_StatusBar_t1378080260_0_0_0_Types };
extern const Il2CppType SocialPlatfromSettings_t3418207459_0_0_0;
static const Il2CppType* GenInst_SocialPlatfromSettings_t3418207459_0_0_0_Types[] = { &SocialPlatfromSettings_t3418207459_0_0_0 };
extern const Il2CppGenericInst GenInst_SocialPlatfromSettings_t3418207459_0_0_0 = { 1, GenInst_SocialPlatfromSettings_t3418207459_0_0_0_Types };
extern const Il2CppType TW_FollowersIdsRequest_t261436078_0_0_0;
static const Il2CppType* GenInst_TW_FollowersIdsRequest_t261436078_0_0_0_Types[] = { &TW_FollowersIdsRequest_t261436078_0_0_0 };
extern const Il2CppGenericInst GenInst_TW_FollowersIdsRequest_t261436078_0_0_0 = { 1, GenInst_TW_FollowersIdsRequest_t261436078_0_0_0_Types };
extern const Il2CppType TW_FriendsIdsRequest_t151035516_0_0_0;
static const Il2CppType* GenInst_TW_FriendsIdsRequest_t151035516_0_0_0_Types[] = { &TW_FriendsIdsRequest_t151035516_0_0_0 };
extern const Il2CppGenericInst GenInst_TW_FriendsIdsRequest_t151035516_0_0_0 = { 1, GenInst_TW_FriendsIdsRequest_t151035516_0_0_0_Types };
extern const Il2CppType TW_OAuthAPIRequest_t3823919588_0_0_0;
static const Il2CppType* GenInst_TW_OAuthAPIRequest_t3823919588_0_0_0_Types[] = { &TW_OAuthAPIRequest_t3823919588_0_0_0 };
extern const Il2CppGenericInst GenInst_TW_OAuthAPIRequest_t3823919588_0_0_0 = { 1, GenInst_TW_OAuthAPIRequest_t3823919588_0_0_0_Types };
extern const Il2CppType TW_SearchTweetsRequest_t525743233_0_0_0;
static const Il2CppType* GenInst_TW_SearchTweetsRequest_t525743233_0_0_0_Types[] = { &TW_SearchTweetsRequest_t525743233_0_0_0 };
extern const Il2CppGenericInst GenInst_TW_SearchTweetsRequest_t525743233_0_0_0 = { 1, GenInst_TW_SearchTweetsRequest_t525743233_0_0_0_Types };
extern const Il2CppType TW_UsersLookUpRequest_t889831987_0_0_0;
static const Il2CppType* GenInst_TW_UsersLookUpRequest_t889831987_0_0_0_Types[] = { &TW_UsersLookUpRequest_t889831987_0_0_0 };
extern const Il2CppGenericInst GenInst_TW_UsersLookUpRequest_t889831987_0_0_0 = { 1, GenInst_TW_UsersLookUpRequest_t889831987_0_0_0_Types };
extern const Il2CppType TW_UserTimeLineRequest_t1907229837_0_0_0;
static const Il2CppType* GenInst_TW_UserTimeLineRequest_t1907229837_0_0_0_Types[] = { &TW_UserTimeLineRequest_t1907229837_0_0_0 };
extern const Il2CppGenericInst GenInst_TW_UserTimeLineRequest_t1907229837_0_0_0 = { 1, GenInst_TW_UserTimeLineRequest_t1907229837_0_0_0_Types };
extern const Il2CppType TwitterPostingTask_t1522896362_0_0_0;
static const Il2CppType* GenInst_TwitterPostingTask_t1522896362_0_0_0_Types[] = { &TwitterPostingTask_t1522896362_0_0_0 };
extern const Il2CppGenericInst GenInst_TwitterPostingTask_t1522896362_0_0_0 = { 1, GenInst_TwitterPostingTask_t1522896362_0_0_0_Types };
extern const Il2CppType WWWTextureLoader_t155872171_0_0_0;
static const Il2CppType* GenInst_WWWTextureLoader_t155872171_0_0_0_Types[] = { &WWWTextureLoader_t155872171_0_0_0 };
extern const Il2CppGenericInst GenInst_WWWTextureLoader_t155872171_0_0_0 = { 1, GenInst_WWWTextureLoader_t155872171_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &PropertyPresence_t220200932_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_Types };
static const Il2CppType* GenInst_JTokenType_t1307827213_0_0_0_JTokenType_t1307827213_0_0_0_Types[] = { &JTokenType_t1307827213_0_0_0, &JTokenType_t1307827213_0_0_0 };
extern const Il2CppGenericInst GenInst_JTokenType_t1307827213_0_0_0_JTokenType_t1307827213_0_0_0 = { 2, GenInst_JTokenType_t1307827213_0_0_0_JTokenType_t1307827213_0_0_0_Types };
static const Il2CppType* GenInst_JsonSchemaType_t1742745177_0_0_0_JsonSchemaType_t1742745177_0_0_0_Types[] = { &JsonSchemaType_t1742745177_0_0_0, &JsonSchemaType_t1742745177_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchemaType_t1742745177_0_0_0_JsonSchemaType_t1742745177_0_0_0 = { 2, GenInst_JsonSchemaType_t1742745177_0_0_0_JsonSchemaType_t1742745177_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types[] = { &Byte_t3683104436_0_0_0, &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0 = { 2, GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0, &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0, &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0, &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0 = { 2, GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0, &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0 = { 2, GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types };
static const Il2CppType* GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0_Types[] = { &Playable_t3667545548_0_0_0, &Playable_t3667545548_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0 = { 2, GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0_Types };
static const Il2CppType* GenInst_RuntimePlatform_t1869584967_0_0_0_RuntimePlatform_t1869584967_0_0_0_Types[] = { &RuntimePlatform_t1869584967_0_0_0, &RuntimePlatform_t1869584967_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimePlatform_t1869584967_0_0_0_RuntimePlatform_t1869584967_0_0_0 = { 2, GenInst_RuntimePlatform_t1869584967_0_0_0_RuntimePlatform_t1869584967_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0, &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0 = { 2, GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0, &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0 = { 2, GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0, &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0 = { 2, GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0, &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0 = { 2, GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0 = { 2, GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0, &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0 = { 2, GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types };
static const Il2CppType* GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_ManifestPermission_t1772110202_0_0_0_Types[] = { &AN_ManifestPermission_t1772110202_0_0_0, &AN_ManifestPermission_t1772110202_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_ManifestPermission_t1772110202_0_0_0 = { 2, GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_ManifestPermission_t1772110202_0_0_0_Types };
static const Il2CppType* GenInst_AN_ManifestPermission_t1772110202_0_0_0_Il2CppObject_0_0_0_Types[] = { &AN_ManifestPermission_t1772110202_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_ManifestPermission_t1772110202_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_AN_ManifestPermission_t1772110202_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_AN_PermissionState_t838201224_0_0_0_AN_PermissionState_t838201224_0_0_0_Types[] = { &AN_PermissionState_t838201224_0_0_0, &AN_PermissionState_t838201224_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_PermissionState_t838201224_0_0_0_AN_PermissionState_t838201224_0_0_0 = { 2, GenInst_AN_PermissionState_t838201224_0_0_0_AN_PermissionState_t838201224_0_0_0_Types };
static const Il2CppType* GenInst_AN_PermissionState_t838201224_0_0_0_Il2CppObject_0_0_0_Types[] = { &AN_PermissionState_t838201224_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_AN_PermissionState_t838201224_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_AN_PermissionState_t838201224_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3409008887_0_0_0_KeyValuePair_2_t3409008887_0_0_0_Types[] = { &KeyValuePair_2_t3409008887_0_0_0, &KeyValuePair_2_t3409008887_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3409008887_0_0_0_KeyValuePair_2_t3409008887_0_0_0 = { 2, GenInst_KeyValuePair_2_t3409008887_0_0_0_KeyValuePair_2_t3409008887_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3409008887_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3409008887_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3409008887_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3409008887_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_FB_ProfileImageSize_t3003328130_0_0_0_FB_ProfileImageSize_t3003328130_0_0_0_Types[] = { &FB_ProfileImageSize_t3003328130_0_0_0, &FB_ProfileImageSize_t3003328130_0_0_0 };
extern const Il2CppGenericInst GenInst_FB_ProfileImageSize_t3003328130_0_0_0_FB_ProfileImageSize_t3003328130_0_0_0 = { 2, GenInst_FB_ProfileImageSize_t3003328130_0_0_0_FB_ProfileImageSize_t3003328130_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t534914198_0_0_0_KeyValuePair_2_t534914198_0_0_0_Types[] = { &KeyValuePair_2_t534914198_0_0_0, &KeyValuePair_2_t534914198_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t534914198_0_0_0_KeyValuePair_2_t534914198_0_0_0 = { 2, GenInst_KeyValuePair_2_t534914198_0_0_0_KeyValuePair_2_t534914198_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t534914198_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t534914198_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t534914198_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t534914198_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t3055062677_0_0_0_TypeNameKey_t3055062677_0_0_0_Types[] = { &TypeNameKey_t3055062677_0_0_0, &TypeNameKey_t3055062677_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t3055062677_0_0_0_TypeNameKey_t3055062677_0_0_0 = { 2, GenInst_TypeNameKey_t3055062677_0_0_0_TypeNameKey_t3055062677_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t94122711_0_0_0_KeyValuePair_2_t94122711_0_0_0_Types[] = { &KeyValuePair_2_t94122711_0_0_0, &KeyValuePair_2_t94122711_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t94122711_0_0_0_KeyValuePair_2_t94122711_0_0_0 = { 2, GenInst_KeyValuePair_2_t94122711_0_0_0_KeyValuePair_2_t94122711_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t94122711_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t94122711_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t94122711_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t94122711_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t1788482786_0_0_0_TypeConvertKey_t1788482786_0_0_0_Types[] = { &TypeConvertKey_t1788482786_0_0_0, &TypeConvertKey_t1788482786_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t1788482786_0_0_0_TypeConvertKey_t1788482786_0_0_0 = { 2, GenInst_TypeConvertKey_t1788482786_0_0_0_TypeConvertKey_t1788482786_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4204318902_0_0_0_KeyValuePair_2_t4204318902_0_0_0_Types[] = { &KeyValuePair_2_t4204318902_0_0_0, &KeyValuePair_2_t4204318902_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4204318902_0_0_0_KeyValuePair_2_t4204318902_0_0_0 = { 2, GenInst_KeyValuePair_2_t4204318902_0_0_0_KeyValuePair_2_t4204318902_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4204318902_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4204318902_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4204318902_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4204318902_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t115911300_0_0_0_KeyValuePair_2_t115911300_0_0_0_Types[] = { &KeyValuePair_2_t115911300_0_0_0, &KeyValuePair_2_t115911300_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t115911300_0_0_0_KeyValuePair_2_t115911300_0_0_0 = { 2, GenInst_KeyValuePair_2_t115911300_0_0_0_KeyValuePair_2_t115911300_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t115911300_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t115911300_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t115911300_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t115911300_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_JsonSchemaType_t1742745177_0_0_0_Il2CppObject_0_0_0_Types[] = { &JsonSchemaType_t1742745177_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSchemaType_t1742745177_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_JsonSchemaType_t1742745177_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3387117823_0_0_0_KeyValuePair_2_t3387117823_0_0_0_Types[] = { &KeyValuePair_2_t3387117823_0_0_0, &KeyValuePair_2_t3387117823_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3387117823_0_0_0_KeyValuePair_2_t3387117823_0_0_0 = { 2, GenInst_KeyValuePair_2_t3387117823_0_0_0_KeyValuePair_2_t3387117823_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3387117823_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3387117823_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3387117823_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3387117823_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PropertyPresence_t220200932_0_0_0_PropertyPresence_t220200932_0_0_0_Types[] = { &PropertyPresence_t220200932_0_0_0, &PropertyPresence_t220200932_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyPresence_t220200932_0_0_0_PropertyPresence_t220200932_0_0_0 = { 2, GenInst_PropertyPresence_t220200932_0_0_0_PropertyPresence_t220200932_0_0_0_Types };
static const Il2CppType* GenInst_PropertyPresence_t220200932_0_0_0_Il2CppObject_0_0_0_Types[] = { &PropertyPresence_t220200932_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyPresence_t220200932_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_PropertyPresence_t220200932_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1864573578_0_0_0_KeyValuePair_2_t1864573578_0_0_0_Types[] = { &KeyValuePair_2_t1864573578_0_0_0, &KeyValuePair_2_t1864573578_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1864573578_0_0_0_KeyValuePair_2_t1864573578_0_0_0 = { 2, GenInst_KeyValuePair_2_t1864573578_0_0_0_KeyValuePair_2_t1864573578_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1864573578_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1864573578_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1864573578_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1864573578_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1683227291_0_0_0_KeyValuePair_2_t1683227291_0_0_0_Types[] = { &KeyValuePair_2_t1683227291_0_0_0, &KeyValuePair_2_t1683227291_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1683227291_0_0_0_KeyValuePair_2_t1683227291_0_0_0 = { 2, GenInst_KeyValuePair_2_t1683227291_0_0_0_KeyValuePair_2_t1683227291_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1683227291_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1683227291_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1683227291_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1683227291_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t566713506_0_0_0_KeyValuePair_2_t566713506_0_0_0_Types[] = { &KeyValuePair_2_t566713506_0_0_0, &KeyValuePair_2_t566713506_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t566713506_0_0_0_KeyValuePair_2_t566713506_0_0_0 = { 2, GenInst_KeyValuePair_2_t566713506_0_0_0_KeyValuePair_2_t566713506_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t566713506_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t566713506_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t566713506_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t566713506_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_AndroidStore_t3203055206_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &AndroidStore_t3203055206_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_AndroidStore_t3203055206_0_0_0 = { 2, GenInst_AndroidStore_t3203055206_0_0_0_AndroidStore_t3203055206_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[1748] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_Attribute_t542643598_0_0_0,
	&GenInst__Attribute_t1557664299_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0,
	&GenInst_Char_t3454481338_0_0_0,
	&GenInst_IConvertible_t908092482_0_0_0,
	&GenInst_IComparable_t1857082765_0_0_0,
	&GenInst_IComparable_1_t991353265_0_0_0,
	&GenInst_IEquatable_1_t1363496211_0_0_0,
	&GenInst_ValueType_t3507792607_0_0_0,
	&GenInst_Int64_t909078037_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0,
	&GenInst_SByte_t454417549_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0,
	&GenInst_UInt16_t986882611_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IEnumerable_t2911409499_0_0_0,
	&GenInst_ICloneable_t3853279282_0_0_0,
	&GenInst_IComparable_1_t3861059456_0_0_0,
	&GenInst_IEquatable_1_t4233202402_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t3412036974_0_0_0,
	&GenInst__Type_t102776839_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0,
	&GenInst__MemberInfo_t332722161_0_0_0,
	&GenInst_IFormattable_t1523031934_0_0_0,
	&GenInst_IComparable_1_t3903716671_0_0_0,
	&GenInst_IEquatable_1_t4275859617_0_0_0,
	&GenInst_Double_t4078015681_0_0_0,
	&GenInst_IComparable_1_t1614887608_0_0_0,
	&GenInst_IEquatable_1_t1987030554_0_0_0,
	&GenInst_IComparable_1_t3981521244_0_0_0,
	&GenInst_IEquatable_1_t58696894_0_0_0,
	&GenInst_IComparable_1_t1219976363_0_0_0,
	&GenInst_IEquatable_1_t1592119309_0_0_0,
	&GenInst_Single_t2076509932_0_0_0,
	&GenInst_IComparable_1_t3908349155_0_0_0,
	&GenInst_IEquatable_1_t4280492101_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0,
	&GenInst_Delegate_t3022476291_0_0_0,
	&GenInst_ISerializable_t1245643778_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0,
	&GenInst__ParameterInfo_t470209990_0_0_0,
	&GenInst_ParameterModifier_t1820634920_0_0_0,
	&GenInst_IComparable_1_t2818721834_0_0_0,
	&GenInst_IEquatable_1_t3190864780_0_0_0,
	&GenInst_IComparable_1_t446068841_0_0_0,
	&GenInst_IEquatable_1_t818211787_0_0_0,
	&GenInst_IComparable_1_t1578117841_0_0_0,
	&GenInst_IEquatable_1_t1950260787_0_0_0,
	&GenInst_IComparable_1_t2286256772_0_0_0,
	&GenInst_IEquatable_1_t2658399718_0_0_0,
	&GenInst_IComparable_1_t2740917260_0_0_0,
	&GenInst_IEquatable_1_t3113060206_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_EventInfo_t_0_0_0,
	&GenInst__EventInfo_t2430923913_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2511231167_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3642518830_0_0_0,
	&GenInst_MethodBase_t904190842_0_0_0,
	&GenInst__MethodBase_t1935530873_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t1567586598_0_0_0,
	&GenInst_ConstructorInfo_t2851816542_0_0_0,
	&GenInst__ConstructorInfo_t3269099341_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t2011406615_0_0_0,
	&GenInst_TailoringInfo_t1449609243_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_Link_t2723257478_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_Contraction_t1673853792_0_0_0,
	&GenInst_Level2Map_t3322505726_0_0_0,
	&GenInst_BigInteger_t925946152_0_0_0,
	&GenInst_KeySizes_t3144736271_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_IComparable_1_t1362446645_0_0_0,
	&GenInst_IEquatable_1_t1734589591_0_0_0,
	&GenInst_Slot_t2022531261_0_0_0,
	&GenInst_Slot_t2267560602_0_0_0,
	&GenInst_StackFrame_t2050294881_0_0_0,
	&GenInst_Calendar_t585061108_0_0_0,
	&GenInst_CultureInfo_t3500843524_0_0_0,
	&GenInst_IFormatProvider_t2849799027_0_0_0,
	&GenInst_ModuleBuilder_t4156028127_0_0_0,
	&GenInst__ModuleBuilder_t1075102050_0_0_0,
	&GenInst_Module_t4282841206_0_0_0,
	&GenInst__Module_t2144668161_0_0_0,
	&GenInst_ParameterBuilder_t3344728474_0_0_0,
	&GenInst__ParameterBuilder_t2251638747_0_0_0,
	&GenInst_TypeU5BU5D_t1664964607_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t91669223_0_0_0,
	&GenInst_IList_t3321498491_0_0_0,
	&GenInst_ILTokenInfo_t149559338_0_0_0,
	&GenInst_LabelData_t3712112744_0_0_0,
	&GenInst_LabelFixup_t4090909514_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0,
	&GenInst_TypeBuilder_t3308873219_0_0_0,
	&GenInst__TypeBuilder_t2783404358_0_0_0,
	&GenInst_MethodBuilder_t644187984_0_0_0,
	&GenInst__MethodBuilder_t3932949077_0_0_0,
	&GenInst_ConstructorBuilder_t700974433_0_0_0,
	&GenInst__ConstructorBuilder_t1236878896_0_0_0,
	&GenInst_PropertyBuilder_t3694255912_0_0_0,
	&GenInst__PropertyBuilder_t3341912621_0_0_0,
	&GenInst_FieldBuilder_t2784804005_0_0_0,
	&GenInst__FieldBuilder_t1895266044_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeData_t3093286891_0_0_0,
	&GenInst_ResourceInfo_t3933049236_0_0_0,
	&GenInst_ResourceCacheItem_t333236149_0_0_0,
	&GenInst_IContextProperty_t287246399_0_0_0,
	&GenInst_Header_t2756440555_0_0_0,
	&GenInst_ITrackingHandler_t2759960940_0_0_0,
	&GenInst_IContextAttribute_t2439121372_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0,
	&GenInst_IComparable_1_t2525044892_0_0_0,
	&GenInst_IEquatable_1_t2897187838_0_0_0,
	&GenInst_IComparable_1_t2556540300_0_0_0,
	&GenInst_IEquatable_1_t2928683246_0_0_0,
	&GenInst_TimeSpan_t3430258949_0_0_0,
	&GenInst_IComparable_1_t967130876_0_0_0,
	&GenInst_IEquatable_1_t1339273822_0_0_0,
	&GenInst_TypeTag_t141209596_0_0_0,
	&GenInst_Enum_t2459695545_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t2988747270_0_0_0,
	&GenInst_Assembly_t4268412390_0_0_0,
	&GenInst__Assembly_t2937922309_0_0_0,
	&GenInst_DateTimeOffset_t1362988906_0_0_0,
	&GenInst_Guid_t2533601593_0_0_0,
	&GenInst_Version_t1755874712_0_0_0,
	&GenInst_Node_t2499136326_0_0_0,
	&GenInst_EventDescriptor_t962731901_0_0_0,
	&GenInst_MemberDescriptor_t3749827553_0_0_0,
	&GenInst_PropertyDescriptor_t4250402154_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0,
	&GenInst_TypeDescriptionProvider_t2438624375_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_LinkedList_1_t2743332604_0_0_0,
	&GenInst_KeyValuePair_2_t2438035723_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3261256129_0_0_0,
	&GenInst_X509ChainStatus_t4278378721_0_0_0,
	&GenInst_IPAddress_t1399971723_0_0_0,
	&GenInst_ArraySegment_1_t2594217482_0_0_0,
	&GenInst_Cookie_t3154017544_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_X509Certificate_t283079845_0_0_0,
	&GenInst_IDeserializationCallback_t327125377_0_0_0,
	&GenInst_Capture_t4157900610_0_0_0,
	&GenInst_Group_t3761430853_0_0_0,
	&GenInst_Mark_t2724874473_0_0_0,
	&GenInst_UriScheme_t1876590943_0_0_0,
	&GenInst_BigInteger_t925946153_0_0_0,
	&GenInst_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_ClientCertificateType_t4001384466_0_0_0,
	&GenInst_Link_t865133271_0_0_0,
	&GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0,
	&GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t2058570427_0_0_0,
	&GenInst_KeyValuePair_2_t3702943073_0_0_0,
	&GenInst_IGrouping_2_t906937361_0_0_0,
	&GenInst_Il2CppObject_0_0_0_IEnumerable_1_t2981576340_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_Object_t1021602117_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0,
	&GenInst_IAchievementDescription_t3498529102_0_0_0,
	&GenInst_IAchievementU5BU5D_t2709554645_0_0_0,
	&GenInst_IAchievement_t1752291260_0_0_0,
	&GenInst_IScoreU5BU5D_t3237304636_0_0_0,
	&GenInst_IScore_t513966369_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3461248430_0_0_0,
	&GenInst_IUserProfile_t4108565527_0_0_0,
	&GenInst_AchievementDescription_t3110978151_0_0_0,
	&GenInst_UserProfile_t3365630962_0_0_0,
	&GenInst_GcLeaderboard_t453887929_0_0_0,
	&GenInst_GcAchievementData_t1754866149_0_0_0,
	&GenInst_Achievement_t1333316625_0_0_0,
	&GenInst_GcScoreData_t3676783238_0_0_0,
	&GenInst_Score_t2307748940_0_0_0,
	&GenInst_Material_t193706927_0_0_0,
	&GenInst_Color_t2020392075_0_0_0,
	&GenInst_Keyframe_t1449471340_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0,
	&GenInst_Color32_t874517518_0_0_0,
	&GenInst_Camera_t189460977_0_0_0,
	&GenInst_Behaviour_t955675639_0_0_0,
	&GenInst_Component_t3819376471_0_0_0,
	&GenInst_Display_t3666191348_0_0_0,
	&GenInst_Touch_t407273883_0_0_0,
	&GenInst_jvalue_t3412352577_0_0_0,
	&GenInst_AndroidJavaObject_t4251328308_0_0_0,
	&GenInst_IDisposable_t2427283555_0_0_0,
	&GenInst_LocalNotification_t317971878_0_0_0,
	&GenInst_Playable_t3667545548_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0,
	&GenInst_ContactPoint_t1376425630_0_0_0,
	&GenInst_RaycastHit_t87180320_0_0_0,
	&GenInst_Rigidbody2D_t502193897_0_0_0,
	&GenInst_RaycastHit2D_t4063908774_0_0_0,
	&GenInst_ContactPoint2D_t3659330976_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0,
	&GenInst_Font_t4239498691_0_0_0,
	&GenInst_GUILayoutOption_t4183744904_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_LayoutCache_t3120781045_0_0_0,
	&GenInst_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_GUILayoutEntry_t3828586629_0_0_0,
	&GenInst_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_IMultipartFormSection_t1234744138_0_0_0,
	&GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0,
	&GenInst_ConstructorDelegate_t3084043859_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0,
	&GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0,
	&GenInst_GetDelegate_t352281633_0_0_0,
	&GenInst_IDictionary_2_t266144316_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0,
	&GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0,
	&GenInst_Type_t_0_0_0_SetDelegate_t4206365109_0_0_0,
	&GenInst_KeyValuePair_2_t3901068228_0_0_0,
	&GenInst_IDictionary_2_t3814930911_0_0_0,
	&GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_KeyValuePair_2_t1683227291_0_0_0,
	&GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2778746978_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4255814731_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3509634030_0_0_0,
	&GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t24406117_0_0_0,
	&GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t1683227291_0_0_0,
	&GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3573192712_0_0_0,
	&GenInst_DisallowMultipleComponent_t2656950_0_0_0,
	&GenInst_ExecuteInEditMode_t3043633143_0_0_0,
	&GenInst_RequireComponent_t864575032_0_0_0,
	&GenInst_HitInfo_t1761367055_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PersistentCall_t3793436469_0_0_0,
	&GenInst_BaseInvokableCall_t2229564840_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_String_t_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0,
	&GenInst_HashSet_1_t275936122_0_0_0,
	&GenInst_ProductDefinition_t1942475268_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0,
	&GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IStoreConfiguration_t2978822016_0_0_0,
	&GenInst_KeyValuePair_2_t2673525135_0_0_0,
	&GenInst_IPurchasingModule_t4085676839_0_0_0,
	&GenInst_Product_t1203687971_0_0_0,
	&GenInst_String_t_0_0_0_Product_t1203687971_0_0_0,
	&GenInst_String_t_0_0_0_Product_t1203687971_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t875812455_0_0_0,
	&GenInst_Product_t1203687971_0_0_0_String_t_0_0_0,
	&GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0,
	&GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IStoreExtension_t1396898229_0_0_0,
	&GenInst_KeyValuePair_2_t1091601348_0_0_0,
	&GenInst_InitializationFailureReason_t2954032642_0_0_0,
	&GenInst_ProductDefinition_t1942475268_0_0_0_Product_t1203687971_0_0_0,
	&GenInst_ProductDescription_t3318267523_0_0_0,
	&GenInst_BaseInputModule_t1295781545_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0,
	&GenInst_IDeselectHandler_t3182198310_0_0_0,
	&GenInst_IEventSystemHandler_t2741188318_0_0_0,
	&GenInst_List_1_t2110309450_0_0_0,
	&GenInst_List_1_t3188497603_0_0_0,
	&GenInst_ISelectHandler_t2812555161_0_0_0,
	&GenInst_BaseRaycaster_t2336171397_0_0_0,
	&GenInst_Entry_t3365010046_0_0_0,
	&GenInst_BaseEventData_t2681005625_0_0_0,
	&GenInst_IPointerEnterHandler_t193164956_0_0_0,
	&GenInst_IPointerExitHandler_t461019860_0_0_0,
	&GenInst_IPointerDownHandler_t3929046918_0_0_0,
	&GenInst_IPointerUpHandler_t1847764461_0_0_0,
	&GenInst_IPointerClickHandler_t96169666_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0,
	&GenInst_IBeginDragHandler_t3135127860_0_0_0,
	&GenInst_IDragHandler_t2583993319_0_0_0,
	&GenInst_IEndDragHandler_t1349123600_0_0_0,
	&GenInst_IDropHandler_t2390101210_0_0_0,
	&GenInst_IScrollHandler_t3834677510_0_0_0,
	&GenInst_IUpdateSelectedHandler_t3778909353_0_0_0,
	&GenInst_IMoveHandler_t2611925506_0_0_0,
	&GenInst_ISubmitHandler_t525803901_0_0_0,
	&GenInst_ICancelHandler_t1980319651_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_PointerEventData_t1599784723_0_0_0,
	&GenInst_KeyValuePair_2_t2659922876_0_0_0,
	&GenInst_ButtonState_t2688375492_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ColorBlock_t2652774230_0_0_0,
	&GenInst_OptionData_t2420267500_0_0_0,
	&GenInst_DropdownItem_t4139978805_0_0_0,
	&GenInst_FloatTween_t2986189219_0_0_0,
	&GenInst_Sprite_t309593783_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0,
	&GenInst_List_1_t3873494194_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_List_1_t4020309861_0_0_0,
	&GenInst_Text_t356221433_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_List_1_t4020309861_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t4020309861_0_0_0,
	&GenInst_KeyValuePair_2_t1885773127_0_0_0,
	&GenInst_ColorTween_t3438117476_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IndexedSet_1_t286373651_0_0_0,
	&GenInst_KeyValuePair_2_t2391682566_0_0_0,
	&GenInst_KeyValuePair_2_t3010968081_0_0_0,
	&GenInst_KeyValuePair_2_t1912381698_0_0_0,
	&GenInst_ContentType_t1028629049_0_0_0,
	&GenInst_Mask_t2977958238_0_0_0,
	&GenInst_List_1_t2347079370_0_0_0,
	&GenInst_RectMask2D_t1156185964_0_0_0,
	&GenInst_List_1_t525307096_0_0_0,
	&GenInst_Navigation_t1571958496_0_0_0,
	&GenInst_IClippable_t1941276057_0_0_0,
	&GenInst_Selectable_t1490392188_0_0_0,
	&GenInst_SpriteState_t1353336012_0_0_0,
	&GenInst_CanvasGroup_t3296560743_0_0_0,
	&GenInst_MatEntry_t3157325053_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t379984643_0_0_0,
	&GenInst_RectTransform_t3349966182_0_0_0,
	&GenInst_LayoutRebuilder_t2155218138_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_List_1_t1612828712_0_0_0,
	&GenInst_List_1_t243638650_0_0_0,
	&GenInst_List_1_t1612828711_0_0_0,
	&GenInst_List_1_t1612828713_0_0_0,
	&GenInst_List_1_t1440998580_0_0_0,
	&GenInst_List_1_t573379950_0_0_0,
	&GenInst_WinProductDescription_t1075111405_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_InitializationFailureReason_t2954032642_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_PurchaseFailureReason_t1322959839_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t566713506_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_AndroidStore_t3203055206_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t566713506_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4201451740_0_0_0,
	&GenInst_ProductDefinition_t1942475268_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Action_t3226471752_0_0_0,
	&GenInst_RuntimePlatform_t1869584967_0_0_0,
	&GenInst_Action_1_t3627374100_0_0_0,
	&GenInst_MonoBehaviour_t1158329972_0_0_0,
	&GenInst_ProductDefinition_t1942475268_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_ProductDefinition_t1942475268_0_0_0_WinProductDescription_t1075111405_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3089358386_0_0_0,
	&GenInst_KeyValuePair_2_t1430411454_0_0_0,
	&GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0,
	&GenInst_DTDNode_t1758286970_0_0_0,
	&GenInst_Entry_t2583369454_0_0_0,
	&GenInst_XmlNode_t616554813_0_0_0,
	&GenInst_IXPathNavigable_t845515791_0_0_0,
	&GenInst_NsDecl_t3210081295_0_0_0,
	&GenInst_NsScope_t2513625351_0_0_0,
	&GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0,
	&GenInst_XmlTokenInfo_t254587324_0_0_0,
	&GenInst_TagName_t2340974457_0_0_0,
	&GenInst_XmlNodeInfo_t3709371029_0_0_0,
	&GenInst_Framework_t4022948262_0_0_0,
	&GenInst_Lib_t360384209_0_0_0,
	&GenInst_Variable_t1157765046_0_0_0,
	&GenInst_String_t_0_0_0_VariableListed_t1912381035_0_0_0,
	&GenInst_String_t_0_0_0_VariableListed_t1912381035_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_VariableListed_t1912381035_0_0_0,
	&GenInst_KeyValuePair_2_t1584505519_0_0_0,
	&GenInst_ISN_SoomlaGrow_t2919772303_0_0_0,
	&GenInst_IOSInAppPurchaseManager_t644626385_0_0_0,
	&GenInst_IOSProductTemplate_t1036598382_0_0_0,
	&GenInst_GK_Leaderboard_t156446466_0_0_0,
	&GenInst_GK_AchievementTemplate_t2296152240_0_0_0,
	&GenInst_GameCenterInvitations_t2643374653_0_0_0,
	&GenInst_GK_Player_t2782008294_0_0_0_GK_InviteRecipientResponse_t3438857802_0_0_0,
	&GenInst_Il2CppObject_0_0_0_GK_InviteRecipientResponse_t3438857802_0_0_0,
	&GenInst_GK_MatchType_t1493351924_0_0_0_GK_Invite_t22070530_0_0_0,
	&GenInst_GK_MatchType_t1493351924_0_0_0_Il2CppObject_0_0_0,
	&GenInst_GK_MatchType_t1493351924_0_0_0_StringU5BU5D_t1642385972_0_0_0_GK_PlayerU5BU5D_t1642762691_0_0_0,
	&GenInst_GK_MatchType_t1493351924_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_GK_Player_t2782008294_0_0_0,
	&GenInst_String_t_0_0_0_GK_Player_t2782008294_0_0_0,
	&GenInst_String_t_0_0_0_GK_Player_t2782008294_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2454132778_0_0_0,
	&GenInst_GK_LeaderboardSet_t5314098_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GK_FriendRequest_t4101620808_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GK_FriendRequest_t4101620808_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GK_FriendRequest_t4101620808_0_0_0,
	&GenInst_KeyValuePair_2_t866791665_0_0_0,
	&GenInst_Result_t4287219743_0_0_0,
	&GenInst_GK_LeaderboardResult_t866080833_0_0_0,
	&GenInst_GK_AchievementProgressResult_t3539574352_0_0_0,
	&GenInst_GK_UserInfoLoadResult_t1177841233_0_0_0,
	&GenInst_GK_PlayerSignatureResult_t13769479_0_0_0,
	&GenInst_GK_TBM_Participant_t3803955090_0_0_0,
	&GenInst_GameCenter_RTM_t849630631_0_0_0,
	&GenInst_GK_RTM_MatchStartedResult_t833698690_0_0_0,
	&GenInst_Error_t445207774_0_0_0,
	&GenInst_GK_Player_t2782008294_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_GK_RTM_QueryActivityResult_t2276098399_0_0_0,
	&GenInst_GK_Player_t2782008294_0_0_0_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_GK_Player_t2782008294_0_0_0_GK_PlayerConnectionState_t2434478783_0_0_0_GK_RTM_Match_t873568990_0_0_0,
	&GenInst_Il2CppObject_0_0_0_GK_PlayerConnectionState_t2434478783_0_0_0_Il2CppObject_0_0_0,
	&GenInst_GameCenter_TBM_t3457554475_0_0_0,
	&GenInst_String_t_0_0_0_GK_TBM_Match_t132033130_0_0_0,
	&GenInst_String_t_0_0_0_GK_TBM_Match_t132033130_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GK_TBM_Match_t132033130_0_0_0,
	&GenInst_KeyValuePair_2_t4099124910_0_0_0,
	&GenInst_GK_TBM_LoadMatchResult_t1639249273_0_0_0,
	&GenInst_GK_TBM_LoadMatchesResult_t370491735_0_0_0,
	&GenInst_GK_TBM_MatchDataUpdateResult_t1356006034_0_0_0,
	&GenInst_GK_TBM_MatchInitResult_t3847830897_0_0_0,
	&GenInst_GK_TBM_MatchQuitResult_t1233820656_0_0_0,
	&GenInst_GK_TBM_EndTrunResult_t1517380690_0_0_0,
	&GenInst_GK_TBM_MatchEndResult_t3461768810_0_0_0,
	&GenInst_GK_TBM_RematchResult_t3159773700_0_0_0,
	&GenInst_GK_TBM_MatchRemovedResult_t909126313_0_0_0,
	&GenInst_GK_TBM_MatchTurnResult_t3583658160_0_0_0,
	&GenInst_ISN_GameSaves_t2236256821_0_0_0,
	&GenInst_String_t_0_0_0_GK_SavedGame_t3320093620_0_0_0,
	&GenInst_String_t_0_0_0_GK_SavedGame_t3320093620_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GK_SavedGame_t3320093620_0_0_0,
	&GenInst_KeyValuePair_2_t2992218104_0_0_0,
	&GenInst_GK_SaveRemoveResult_t539310567_0_0_0,
	&GenInst_GK_SaveResult_t3946576453_0_0_0,
	&GenInst_GK_FetchResult_t1611512656_0_0_0,
	&GenInst_GK_SavesResolveResult_t3508055404_0_0_0,
	&GenInst_GK_SaveDataLoaded_t3684688319_0_0_0,
	&GenInst_GK_Score_t1529008873_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GK_LocalPlayerScoreUpdateListener_t1070875322_0_0_0,
	&GenInst_KeyValuePair_2_t2131013475_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GK_Score_t1529008873_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GK_Score_t1529008873_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2589147026_0_0_0,
	&GenInst_GK_LeaderBoardInfo_t3670215494_0_0_0,
	&GenInst_ISN_LoadSetLeaderboardsInfoResult_t3997789804_0_0_0,
	&GenInst_String_t_0_0_0_Texture2D_t3542995729_0_0_0,
	&GenInst_String_t_0_0_0_Texture2D_t3542995729_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Texture2D_t3542995729_0_0_0,
	&GenInst_KeyValuePair_2_t3215120213_0_0_0,
	&GenInst_GK_UserPhotoLoadResult_t1614198031_0_0_0,
	&GenInst_IOSCamera_t2845108690_0_0_0,
	&GenInst_IOSImagePickResult_t1671334394_0_0_0,
	&GenInst_ISN_FilePicker_t2957423793_0_0_0,
	&GenInst_ISN_FilePickerResult_t2234803986_0_0_0,
	&GenInst_ISN_MediaController_t1853840745_0_0_0,
	&GenInst_MP_MediaItem_t4025623029_0_0_0,
	&GenInst_MP_MediaPickerResult_t2204006871_0_0_0,
	&GenInst_MP_MusicPlaybackState_t2364713801_0_0_0,
	&GenInst_IOSVideoManager_t3003649481_0_0_0,
	&GenInst_ISN_ReplayKit_t2994976952_0_0_0,
	&GenInst_ReplayKitVideoShareResult_t2762953534_0_0_0,
	&GenInst_ISN_LocalNotificationsController_t3778913674_0_0_0,
	&GenInst_ISN_LocalNotification_t273186689_0_0_0,
	&GenInst_ISN_RemoteNotificationsController_t1910006075_0_0_0,
	&GenInst_ISN_RemoteNotificationsRegistrationResult_t3335875151_0_0_0,
	&GenInst_ISN_DeviceToken_t380973950_0_0_0,
	&GenInst_ISN_RemoteNotification_t1449597314_0_0_0,
	&GenInst_IOSDialogResult_t3739241316_0_0_0,
	&GenInst_IOSSocialManager_t2957403963_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_IOSStoreProductView_t607200268_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_IOSStoreProductView_t607200268_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IOSStoreProductView_t607200268_0_0_0,
	&GenInst_KeyValuePair_2_t1667338421_0_0_0,
	&GenInst_IOSStoreKitResult_t2359407583_0_0_0,
	&GenInst_IOSStoreKitRestoreResult_t3305276155_0_0_0,
	&GenInst_IOSStoreKitVerificationResponse_t4263658582_0_0_0,
	&GenInst_SK_CloudService_t588287925_0_0_0,
	&GenInst_SK_AuthorizationResult_t2047753871_0_0_0,
	&GenInst_SK_RequestCapabilitieResult_t2510163844_0_0_0,
	&GenInst_SK_RequestStorefrontIdentifierResult_t1112477162_0_0_0,
	&GenInst_IOSNativeAppEvents_t694411788_0_0_0,
	&GenInst_PhoneNumber_t113354641_0_0_0,
	&GenInst_ContactStore_t160827595_0_0_0,
	&GenInst_ContactsResult_t1045731284_0_0_0,
	&GenInst_Contact_t4178394798_0_0_0,
	&GenInst_IOSDateTimePicker_t849222074_0_0_0,
	&GenInst_ISN_GestureRecognizer_t4198098372_0_0_0,
	&GenInst_ISN_SwipeDirection_t768921696_0_0_0,
	&GenInst_ISN_Logger_t85856405_0_0_0,
	&GenInst_ISN_Security_t2700938347_0_0_0,
	&GenInst_ISN_LocalReceiptResult_t3746327569_0_0_0,
	&GenInst_IOSSharedApplication_t4065685598_0_0_0,
	&GenInst_ISN_CheckUrlResult_t1645724501_0_0_0,
	&GenInst_IOSNativeUtility_t933355194_0_0_0,
	&GenInst_ISN_Locale_t2162888085_0_0_0,
	&GenInst_ISN_CloudKit_t2197422136_0_0_0,
	&GenInst_CK_Record_t3973541762_0_0_0,
	&GenInst_iCloudManager_t2506189173_0_0_0,
	&GenInst_iCloudData_t3080637488_0_0_0,
	&GenInst_List_1_t2449758620_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_CK_Database_t243306482_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_CK_Database_t243306482_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_CK_Database_t243306482_0_0_0,
	&GenInst_KeyValuePair_2_t1303444635_0_0_0,
	&GenInst_CK_RecordResult_t4062548349_0_0_0,
	&GenInst_CK_RecordDeleteResult_t3248469484_0_0_0,
	&GenInst_CK_QueryResult_t1174067488_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_CK_Record_t3973541762_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_CK_Record_t3973541762_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t738712619_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_CK_RecordID_t41838833_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_CK_RecordID_t41838833_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_CK_RecordID_t41838833_0_0_0,
	&GenInst_KeyValuePair_2_t1101976986_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2689214752_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2689214752_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t2689214752_0_0_0,
	&GenInst_KeyValuePair_2_t2361339236_0_0_0,
	&GenInst_Hashtable_t909839986_0_0_0,
	&GenInst_Rect_t3681755626_0_0_0,
	&GenInst_String_t_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_Image_t2042527209_0_0_0,
	&GenInst_ICanvasRaycastFilter_t1367822892_0_0_0,
	&GenInst_ISerializationCallbackReceiver_t1665913161_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0,
	&GenInst_MaskableGraphic_t540192618_0_0_0,
	&GenInst_IMaskable_t1431842707_0_0_0,
	&GenInst_IMaterialModifier_t3028564983_0_0_0,
	&GenInst_UIBehaviour_t3960014691_0_0_0,
	&GenInst_SA_EditorAd_t1410159287_0_0_0,
	&GenInst_Quaternion_t4030073918_0_0_0,
	&GenInst_JumpPoint_t1009860606_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1762270573_0_0_0,
	&GenInst_AN_PropertyTemplate_t2393149441_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1762270573_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t1762270573_0_0_0,
	&GenInst_KeyValuePair_2_t1434395057_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_AN_ActivityTemplate_t3380616875_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_AN_ActivityTemplate_t3380616875_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_AN_ActivityTemplate_t3380616875_0_0_0,
	&GenInst_KeyValuePair_2_t145787732_0_0_0,
	&GenInst_AndroidInAppPurchaseManager_t2155442111_0_0_0,
	&GenInst_BillingResult_t3511841850_0_0_0,
	&GenInst_GoogleProductTemplate_t1112616324_0_0_0,
	&GenInst_GooglePurchaseTemplate_t2609331866_0_0_0,
	&GenInst_String_t_0_0_0_GooglePurchaseTemplate_t2609331866_0_0_0,
	&GenInst_String_t_0_0_0_GooglePurchaseTemplate_t2609331866_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2281456350_0_0_0,
	&GenInst_AndroidGoogleAnalytics_t1297670224_0_0_0,
	&GenInst_AN_LicenseManager_t1808504078_0_0_0,
	&GenInst_AN_LicenseRequestResult_t2331370561_0_0_0,
	&GenInst_AddressBookController_t3183854505_0_0_0,
	&GenInst_AndroidContactInfo_t2118672179_0_0_0,
	&GenInst_AndroidAppInfoLoader_t2209520163_0_0_0,
	&GenInst_PackageAppInfo_t260912591_0_0_0,
	&GenInst_AndroidCamera_t1711267764_0_0_0,
	&GenInst_AndroidImagePickResult_t1791733552_0_0_0,
	&GenInst_GallerySaveResult_t1643856950_0_0_0,
	&GenInst_SA_ScreenShotMaker_OLD_t795198627_0_0_0,
	&GenInst_AndroidNativeUtility_t798160036_0_0_0,
	&GenInst_AN_PackageCheckResult_t3695415755_0_0_0,
	&GenInst_AN_Locale_t121755426_0_0_0,
	&GenInst_StringU5BU5D_t1642385972_0_0_0,
	&GenInst_AN_NetworkInfo_t247793570_0_0_0,
	&GenInst_ImmersiveMode_t15485700_0_0_0,
	&GenInst_PermissionsManager_t4266031513_0_0_0,
	&GenInst_AN_GrantPermissionsResult_t250489657_0_0_0,
	&GenInst_AN_ManifestPermission_t1772110202_0_0_0,
	&GenInst_TVAppController_t1564329309_0_0_0,
	&GenInst_AndroidApp_t620754602_0_0_0,
	&GenInst_AndroidActivityResult_t3757510801_0_0_0,
	&GenInst_AndroidNotificationManager_t3325256667_0_0_0,
	&GenInst_LocalNotificationTemplate_t2880890350_0_0_0,
	&GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0,
	&GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0,
	&GenInst_KeyValuePair_2_t3409008887_0_0_0,
	&GenInst_AN_PermissionState_t838201224_0_0_0,
	&GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_AN_ManifestPermission_t1772110202_0_0_0,
	&GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_AN_PermissionState_t838201224_0_0_0,
	&GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_PermissionState_t838201224_0_0_0_KeyValuePair_2_t3409008887_0_0_0,
	&GenInst_AN_SoomlaGrow_t3055280006_0_0_0,
	&GenInst_AndroidTwitterManager_t3465455211_0_0_0,
	&GenInst_TWResult_t1480791060_0_0_0,
	&GenInst_FB_LoginResult_t1848477279_0_0_0,
	&GenInst_FB_PostResult_t992884730_0_0_0,
	&GenInst_FB_Result_t838248372_0_0_0,
	&GenInst_SPFacebook_t1065228369_0_0_0,
	&GenInst_GP_AppInvitesController_t2424429497_0_0_0,
	&GenInst_GP_SendAppInvitesResult_t3999077544_0_0_0,
	&GenInst_GP_RetrieveAppInviteResult_t18485229_0_0_0,
	&GenInst_GoogleCloudManager_t1208504825_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_ByteU5BU5D_t3397334013_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t162504870_0_0_0,
	&GenInst_GoogleCloudResult_t743628707_0_0_0,
	&GenInst_GoogleCloudMessageService_t984991750_0_0_0,
	&GenInst_GP_GCM_RegistrationResult_t2892492118_0_0_0,
	&GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_GooglePlayConnection_t348367589_0_0_0,
	&GenInst_GooglePlayConnectionResult_t2758718724_0_0_0,
	&GenInst_GPConnectionState_t647346880_0_0_0,
	&GenInst_GooglePlayManager_t746138120_0_0_0,
	&GenInst_GooglePlayEvents_t1613959644_0_0_0,
	&GenInst_GP_Event_t323143668_0_0_0,
	&GenInst_GooglePlayResult_t3097469636_0_0_0,
	&GenInst_GooglePlayInvitationManager_t2586337437_0_0_0,
	&GenInst_GP_Invite_t626929087_0_0_0,
	&GenInst_List_1_t4291017515_0_0_0,
	&GenInst_AN_InvitationInboxCloseResult_t1284128610_0_0_0,
	&GenInst_String_t_0_0_0_GooglePlayerTemplate_t2506317812_0_0_0,
	&GenInst_String_t_0_0_0_GooglePlayerTemplate_t2506317812_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GooglePlayerTemplate_t2506317812_0_0_0,
	&GenInst_KeyValuePair_2_t2178442296_0_0_0,
	&GenInst_GPLeaderBoard_t3649577886_0_0_0,
	&GenInst_GPAchievement_t4279788054_0_0_0,
	&GenInst_GPGameRequest_t4158842942_0_0_0,
	&GenInst_GP_LeaderboardResult_t2034215294_0_0_0,
	&GenInst_GP_AchievementResult_t3970926374_0_0_0,
	&GenInst_GooglePlayGiftRequestResult_t350202007_0_0_0,
	&GenInst_List_1_t3527964074_0_0_0,
	&GenInst_List_1_t1398341365_0_0_0,
	&GenInst_GooglePlayQuests_t1618778050_0_0_0,
	&GenInst_GP_Participant_t2884377673_0_0_0,
	&GenInst_GP_QuestResult_t3390940437_0_0_0,
	&GenInst_GP_QuestsSelect_t2071002355_0_0_0,
	&GenInst_String_t_0_0_0_GP_Quest_t1641883470_0_0_0,
	&GenInst_String_t_0_0_0_GP_Quest_t1641883470_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GP_Quest_t1641883470_0_0_0,
	&GenInst_KeyValuePair_2_t1314007954_0_0_0,
	&GenInst_GooglePlayRTM_t547022790_0_0_0,
	&GenInst_GP_RTM_Network_Package_t2050307869_0_0_0,
	&GenInst_GP_RTM_Room_t851604529_0_0_0,
	&GenInst_GP_RTM_ReliableMessageSentResult_t2743629396_0_0_0,
	&GenInst_GP_RTM_ReliableMessageDeliveredResult_t1694070004_0_0_0,
	&GenInst_GP_GamesStatusCodes_t1013506173_0_0_0,
	&GenInst_GP_RTM_Result_t473289279_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GP_RTM_ReliableMessageListener_t1343560679_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GP_RTM_ReliableMessageListener_t1343560679_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GP_RTM_ReliableMessageListener_t1343560679_0_0_0,
	&GenInst_KeyValuePair_2_t2403698832_0_0_0,
	&GenInst_GooglePlaySavedGamesManager_t1475146022_0_0_0,
	&GenInst_GP_SnapshotMeta_t1354779439_0_0_0,
	&GenInst_GP_SpanshotLoadResult_t2263304449_0_0_0,
	&GenInst_GP_SnapshotConflict_t4002586770_0_0_0,
	&GenInst_GP_DeleteSnapshotResult_t3306790010_0_0_0,
	&GenInst_GooglePlayTBM_t641131310_0_0_0,
	&GenInst_String_t_0_0_0_GP_TBM_Match_t1275077981_0_0_0,
	&GenInst_String_t_0_0_0_GP_TBM_Match_t1275077981_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GP_TBM_Match_t1275077981_0_0_0,
	&GenInst_KeyValuePair_2_t947202465_0_0_0,
	&GenInst_GP_TBM_LoadMatchesResult_t841773038_0_0_0,
	&GenInst_GP_TBM_MatchInitiatedResult_t4144060847_0_0_0,
	&GenInst_GP_TBM_CancelMatchResult_t3879293152_0_0_0,
	&GenInst_GP_TBM_LeaveMatchResult_t3657803719_0_0_0,
	&GenInst_GP_TBM_LoadMatchResult_t1357418930_0_0_0,
	&GenInst_GP_TBM_UpdateMatchResult_t3943005969_0_0_0,
	&GenInst_GP_TBM_MatchReceivedResult_t2394672915_0_0_0,
	&GenInst_GP_TBM_MatchRemovedResult_t686355120_0_0_0,
	&GenInst_String_t_0_0_0_GP_TBM_MatchInitiatedResult_t4144060847_0_0_0,
	&GenInst_GP_ParticipantResult_t2469018720_0_0_0,
	&GenInst_GP_TBM_MatchTurnStatus_t2221730550_0_0_0,
	&GenInst_GooglePlusAPI_t3074122137_0_0_0,
	&GenInst_AN_PlusButtonsManager_t882255532_0_0_0,
	&GenInst_AN_PlusButton_t1370758440_0_0_0,
	&GenInst_AN_PlusShareResult_t3488688014_0_0_0,
	&GenInst_GooglePlayUtils_t968630402_0_0_0,
	&GenInst_GP_AdvertisingIdLoadResult_t2783375090_0_0_0,
	&GenInst_AndroidDialogResult_t1664046954_0_0_0,
	&GenInst_String_t_0_0_0_GoogleMobileAdBanner_t1323818958_0_0_0,
	&GenInst_String_t_0_0_0_GoogleMobileAdBanner_t1323818958_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GoogleMobileAdBanner_t1323818958_0_0_0,
	&GenInst_KeyValuePair_2_t995943442_0_0_0,
	&GenInst_AndroidAdMobController_t1638300356_0_0_0,
	&GenInst_DefaultPreviewButton_t12674677_0_0_0,
	&GenInst_SA_PartisipantUI_t3115332592_0_0_0,
	&GenInst_SA_FriendUI_t3775341837_0_0_0,
	&GenInst_CustomPlayerUIRow_t1758969648_0_0_0,
	&GenInst_CustomLeaderboardFiledsHolder_t4123707333_0_0_0,
	&GenInst_GPScore_t3219488889_0_0_0,
	&GenInst_FB_AppRequest_t501312625_0_0_0,
	&GenInst_FB_AppRequestResult_t2450670428_0_0_0,
	&GenInst_FB_UserInfo_t2704578078_0_0_0,
	&GenInst_String_t_0_0_0_FB_Score_t1450841581_0_0_0,
	&GenInst_String_t_0_0_0_FB_Score_t1450841581_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_FB_Score_t1450841581_0_0_0,
	&GenInst_KeyValuePair_2_t1122966065_0_0_0,
	&GenInst_TW_APIRequstResult_t455151055_0_0_0,
	&GenInst_TwitterUserInfo_t87370740_0_0_0,
	&GenInst_TweetTemplate_t3444491657_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_AndroidADBanner_t886219444_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_AndroidADBanner_t886219444_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_AndroidADBanner_t886219444_0_0_0,
	&GenInst_KeyValuePair_2_t1946357597_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GP_LocalPlayerScoreUpdateListener_t158126391_0_0_0,
	&GenInst_KeyValuePair_2_t1218264544_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GPScore_t3219488889_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GPScore_t3219488889_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4279627042_0_0_0,
	&GenInst_String_t_0_0_0_FB_UserInfo_t2704578078_0_0_0,
	&GenInst_String_t_0_0_0_FB_UserInfo_t2704578078_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2376702562_0_0_0,
	&GenInst_String_t_0_0_0_Dictionary_2_t833011044_0_0_0,
	&GenInst_String_t_0_0_0_FB_LikeInfo_t3213199078_0_0_0,
	&GenInst_String_t_0_0_0_FB_LikeInfo_t3213199078_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_FB_LikeInfo_t3213199078_0_0_0,
	&GenInst_KeyValuePair_2_t2885323562_0_0_0,
	&GenInst_String_t_0_0_0_Dictionary_2_t833011044_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t833011044_0_0_0,
	&GenInst_KeyValuePair_2_t505135528_0_0_0,
	&GenInst_FB_PermissionResult_t2322011743_0_0_0,
	&GenInst_FB_Result_t838248372_0_0_0_FB_LikesRetrieveTask_t2495592094_0_0_0,
	&GenInst_String_t_0_0_0_FB_Permission_t1872772122_0_0_0,
	&GenInst_String_t_0_0_0_FB_Permission_t1872772122_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_FB_Permission_t1872772122_0_0_0,
	&GenInst_KeyValuePair_2_t1544896606_0_0_0,
	&GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Texture2D_t3542995729_0_0_0,
	&GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t534914198_0_0_0,
	&GenInst_FB_ProfileImageSize_t3003328130_0_0_0,
	&GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_FB_ProfileImageSize_t3003328130_0_0_0,
	&GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t534914198_0_0_0,
	&GenInst_FB_ProfileImageSize_t3003328130_0_0_0_Texture2D_t3542995729_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1388460632_0_0_0,
	&GenInst_AndroidInstagramManager_t2759207936_0_0_0,
	&GenInst_InstagramPostResult_t436083195_0_0_0,
	&GenInst_TwitterApplicationOnlyToken_t3636409970_0_0_0,
	&GenInst_String_t_0_0_0_TweetTemplate_t3444491657_0_0_0,
	&GenInst_String_t_0_0_0_TweetTemplate_t3444491657_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3116616141_0_0_0,
	&GenInst_String_t_0_0_0_TwitterUserInfo_t87370740_0_0_0,
	&GenInst_String_t_0_0_0_TwitterUserInfo_t87370740_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4054462520_0_0_0,
	&GenInst_TextMesh_t1641806576_0_0_0,
	&GenInst_SALevelLoader_t4125067327_0_0_0,
	&GenInst_JNSimpleObjectModel_t1947941312_0_0_0,
	&GenInst_JsonConverter_t1964060750_0_0_0,
	&GenInst_SimpleClassObject_t1988087395_0_0_0,
	&GenInst_SampleBase_t2925764113_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_SampleBase_t2925764113_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_SampleBase_t2925764113_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3985902266_0_0_0,
	&GenInst_SampleBase_t2925764113_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_SampleBase_t2925764113_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2011661284_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_SimpleClassObject_t1988087395_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_SimpleClassObject_t1988087395_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3048225548_0_0_0,
	&GenInst_JProperty_t2956441399_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_JProperty_t2956441399_0_0_0_String_t_0_0_0,
	&GenInst_JProperty_t2956441399_0_0_0,
	&GenInst_BsonProperty_t1491061775_0_0_0,
	&GenInst_BsonToken_t3582361217_0_0_0,
	&GenInst_ContainerContext_t2144264477_0_0_0,
	&GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t2874502390_0_0_0,
	&GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t2874502390_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_BidirectionalDictionary_2_t2874502390_0_0_0,
	&GenInst_KeyValuePair_2_t2569205509_0_0_0,
	&GenInst_EnumMemberAttribute_t187433993_0_0_0_String_t_0_0_0,
	&GenInst_EnumMemberAttribute_t187433993_0_0_0,
	&GenInst_IXmlNode_t1152344546_0_0_0,
	&GenInst_XmlNode_t616554813_0_0_0_IXmlNode_t1152344546_0_0_0,
	&GenInst_XmlAttribute_t175731005_0_0_0,
	&GenInst_XmlAttribute_t175731005_0_0_0_IXmlNode_t1152344546_0_0_0,
	&GenInst_IXmlNode_t1152344546_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t521465678_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t521465678_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t521465678_0_0_0,
	&GenInst_KeyValuePair_2_t193590162_0_0_0,
	&GenInst_IXmlElement_t2005722770_0_0_0,
	&GenInst_IXmlElement_t2005722770_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_NullValueHandling_t3618095365_0_0_0,
	&GenInst_DefaultValueHandling_t3457895463_0_0_0,
	&GenInst_ReferenceLoopHandling_t1017855894_0_0_0,
	&GenInst_ObjectCreationHandling_t3720134651_0_0_0,
	&GenInst_TypeNameHandling_t1331513094_0_0_0,
	&GenInst_JTokenType_t1307827213_0_0_0,
	&GenInst_ErrorEventArgs_t3365615597_0_0_0,
	&GenInst_JsonSchemaModel_t708894576_0_0_0,
	&GenInst_String_t_0_0_0_JsonSchemaModel_t708894576_0_0_0,
	&GenInst_KeyValuePair_2_t381019060_0_0_0,
	&GenInst_SchemaScope_t4218888543_0_0_0,
	&GenInst_KeyValuePair_2_t3497699202_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t3497699202_0_0_0_String_t_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0,
	&GenInst_JsonSchemaType_t1742745177_0_0_0,
	&GenInst_JToken_t2552644013_0_0_0,
	&GenInst_JsonSchemaModel_t708894576_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t381019060_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t381019060_0_0_0_String_t_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0,
	&GenInst_JsonSchemaModel_t708894576_0_0_0_IEnumerable_1_t2321347278_0_0_0,
	&GenInst_StateU5BU5D_t823784359_0_0_0,
	&GenInst_State_t3285832914_0_0_0,
	&GenInst_String_t_0_0_0_JToken_t2552644013_0_0_0,
	&GenInst_KeyValuePair_2_t2224768497_0_0_0,
	&GenInst_JProperty_t2956441399_0_0_0_JToken_t2552644013_0_0_0,
	&GenInst_String_t_0_0_0_JToken_t2552644013_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_JsonToken_t620654565_0_0_0,
	&GenInst_JValue_t300956845_0_0_0,
	&GenInst_JObject_t278519297_0_0_0_IEnumerable_1_t3248568444_0_0_0,
	&GenInst_Il2CppObject_0_0_0_IEnumerable_1_t2844771058_0_0_0,
	&GenInst_JObject_t278519297_0_0_0,
	&GenInst_JsonSchema_t3772113849_0_0_0,
	&GenInst_String_t_0_0_0_JsonSchema_t3772113849_0_0_0,
	&GenInst_JToken_t2552644013_0_0_0_String_t_0_0_0,
	&GenInst_JToken_t2552644013_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1891585177_0_0_0,
	&GenInst_String_t_0_0_0_JsonSchema_t3772113849_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3444238333_0_0_0,
	&GenInst_String_t_0_0_0_JsonSchemaType_t1742745177_0_0_0,
	&GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0,
	&GenInst_KeyValuePair_2_t3387117823_0_0_0,
	&GenInst_KeyValuePair_2_t1414869661_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t3387117823_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1414869661_0_0_0,
	&GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_JsonSchemaType_t1742745177_0_0_0,
	&GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_JsonSchemaType_t1742745177_0_0_0_KeyValuePair_2_t3387117823_0_0_0,
	&GenInst_String_t_0_0_0_JsonSchemaType_t1742745177_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_TypeSchema_t460462092_0_0_0,
	&GenInst_String_t_0_0_0_EnumValue_1_t589082027_0_0_0,
	&GenInst_EnumValue_1_t589082027_0_0_0,
	&GenInst_String_t_0_0_0_EnumValue_1_t589082027_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_TypeSchema_t460462092_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_JsonProperty_t2712067825_0_0_0,
	&GenInst_JsonSchemaNode_t3866831117_0_0_0_JsonSchemaModel_t708894576_0_0_0,
	&GenInst_JsonSchemaNode_t3866831117_0_0_0,
	&GenInst_JsonSchemaNode_t3866831117_0_0_0_JsonSchemaModel_t708894576_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1389980064_0_0_0,
	&GenInst_String_t_0_0_0_JsonSchemaNode_t3866831117_0_0_0,
	&GenInst_String_t_0_0_0_JsonSchemaNode_t3866831117_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3538955601_0_0_0,
	&GenInst_String_t_0_0_0_JsonSchemaModel_t708894576_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_JsonSchema_t3772113849_0_0_0_String_t_0_0_0,
	&GenInst_JsonSchema_t3772113849_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_JsonSchemaType_t1742745177_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_String_t_0_0_0_EnumValue_1_t2589200904_0_0_0,
	&GenInst_EnumValue_1_t2589200904_0_0_0,
	&GenInst_String_t_0_0_0_EnumValue_1_t2589200904_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_EnumValue_1_t2589200904_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ResolverContractKey_t1785396551_0_0_0,
	&GenInst_Type_t_0_0_0_JsonContract_t1566984540_0_0_0,
	&GenInst_Type_t_0_0_0_JsonContract_t1566984540_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_MemberInfo_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_ConstructorInfo_t2851816542_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_JsonProperty_t2712067825_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_String_t_0_0_0,
	&GenInst_TypeNameKey_t3055062677_0_0_0_Type_t_0_0_0,
	&GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t94122711_0_0_0,
	&GenInst_TypeNameKey_t3055062677_0_0_0,
	&GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_TypeNameKey_t3055062677_0_0_0,
	&GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_TypeNameKey_t3055062677_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t94122711_0_0_0,
	&GenInst_TypeNameKey_t3055062677_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_String_t_0_0_0_JsonProperty_t2712067825_0_0_0,
	&GenInst_String_t_0_0_0_JsonProperty_t2712067825_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0_ParameterInfo_t2249040075_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2995057417_0_0_0_String_t_0_0_0,
	&GenInst_JsonProperty_t2712067825_0_0_0_JsonProperty_t2712067825_0_0_0,
	&GenInst_JsonProperty_t2712067825_0_0_0_PropertyPresence_t220200932_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0,
	&GenInst_IList_t3321498491_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_JsonProperty_t2712067825_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t554510731_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_JsonProperty_t2712067825_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_String_t_0_0_0,
	&GenInst_KeyValuePair_2_t2995057417_0_0_0,
	&GenInst_KeyValuePair_2_t1864573578_0_0_0,
	&GenInst_PropertyPresence_t220200932_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_PropertyPresence_t220200932_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0_KeyValuePair_2_t1864573578_0_0_0,
	&GenInst_JsonProperty_t2712067825_0_0_0_PropertyPresence_t220200932_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2380229664_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0_Type_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_JsonContainerAttribute_t47210975_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0_JsonContainerAttribute_t47210975_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0_JsonContainerAttribute_t47210975_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_DataContractAttribute_t3332255060_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataContractAttribute_t3332255060_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataContractAttribute_t3332255060_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_DataMemberAttribute_t2677019114_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataMemberAttribute_t2677019114_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0_DataMemberAttribute_t2677019114_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IList_1_t3230389896_0_0_0,
	&GenInst_Type_t_0_0_0_IList_1_t3230389896_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TypeConvertKey_t1788482786_0_0_0_Func_2_t2825504181_0_0_0,
	&GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t4204318902_0_0_0,
	&GenInst_TypeConvertKey_t1788482786_0_0_0,
	&GenInst_IEquatable_1_t3992464955_0_0_0,
	&GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_TypeConvertKey_t1788482786_0_0_0,
	&GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_TypeConvertKey_t1788482786_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4204318902_0_0_0,
	&GenInst_TypeConvertKey_t1788482786_0_0_0_Func_2_t2825504181_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_FieldInfo_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_MemberInfo_t_0_0_0_String_t_0_0_0,
	&GenInst_IGrouping_2_t288337164_0_0_0_U3CU3E__AnonType0_2_t99735840_0_0_0,
	&GenInst_String_t_0_0_0_MemberInfo_t_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_IEnumerable_1_t40257009_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0_Type_t_0_0_0,
	&GenInst_U3CU3E__AnonType0_2_t99735840_0_0_0,
	&GenInst_IGrouping_2_t288337164_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Type_t_0_0_0_List_1_t2058570427_0_0_0,
	&GenInst_Type_t_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1753273546_0_0_0,
	&GenInst_Type_t_0_0_0_List_1_t3876342518_0_0_0,
	&GenInst_TypeData_t212254090_0_0_0,
	&GenInst_Type_t_0_0_0_List_1_t3876342518_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t3876342518_0_0_0,
	&GenInst_KeyValuePair_2_t3571045637_0_0_0,
	&GenInst_ConstructorInfo_t2851816542_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_TypeData_t212254090_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_DeleteCourseEventArgs_t2630278065_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_DeleteCourseResponse_t1898856809_0_0_0,
	&GenInst_Course_t3483112699_0_0_0,
	&GenInst_DeleteCourseRequest_t1683798751_0_0_0,
	&GenInst_Jump_t114869516_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t115911300_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t115911300_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_GetCourseResponse_t2659116210_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_CreateCourseResponse_t3800683746_0_0_0,
	&GenInst_CreateCourseRequest_t4267998642_0_0_0,
	&GenInst_GetCourseRequest_t2656580458_0_0_0,
	&GenInst_ShowDetailsEventArgs_t1623880762_0_0_0,
	&GenInst_ShowErrorEventArgs_t2135289798_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_ForgotPasswordResponse_t4062332037_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_ChangePasswordResponse_t3897893034_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_LoginResponse_t1319408382_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_RegisterResponse_t410466074_0_0_0,
	&GenInst_LoginRequest_t2273172322_0_0_0,
	&GenInst_RegisterRequest_t698226714_0_0_0,
	&GenInst_ForgotPasswordRequest_t1886688323_0_0_0,
	&GenInst_ChangePasswordRequest_t4102061450_0_0_0,
	&GenInst_ChangeCallback_t1172037850_0_0_0,
	&GenInst_PurchaseFailureReason_t1322959839_0_0_0,
	&GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0,
	&GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0,
	&GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0,
	&GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0,
	&GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m52621935_gp_0_0_0_0,
	&GenInst_Array_Sort_m3546416104_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0,
	&GenInst_Array_compare_m940423571_gp_0_0_0_0,
	&GenInst_Array_qsort_m565008110_gp_0_0_0_0,
	&GenInst_Array_Resize_m1201602141_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0,
	&GenInst_Array_ForEach_m3775633118_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0,
	&GenInst_Array_FindAll_m982349212_gp_0_0_0_0,
	&GenInst_Array_Exists_m1825464757_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0,
	&GenInst_Array_Find_m2529971459_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3929249453_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0,
	&GenInst_IList_1_t3737699284_gp_0_0_0_0,
	&GenInst_ICollection_1_t1552160836_gp_0_0_0_0,
	&GenInst_Nullable_1_t1398937014_gp_0_0_0_0,
	&GenInst_Comparer_1_t1036860714_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3074655092_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0,
	&GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3434615342_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1766400012_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4174120762_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0,
	&GenInst_List_1_t1169184319_gp_0_0_0_0,
	&GenInst_Enumerator_t1292967705_gp_0_0_0_0,
	&GenInst_Collection_1_t686054069_gp_0_0_0_0,
	&GenInst_KeyedCollection_2_t3154958122_gp_1_0_0_0,
	&GenInst_KeyedCollection_2_t3154958122_gp_0_0_0_0,
	&GenInst_KeyedCollection_2_t3154958122_gp_0_0_0_0_KeyedCollection_2_t3154958122_gp_1_0_0_0,
	&GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0,
	&GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0,
	&GenInst_LinkedList_1_t3556217344_gp_0_0_0_0,
	&GenInst_Enumerator_t4145643798_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0,
	&GenInst_RBTree_Intern_m3121628056_gp_0_0_0_0,
	&GenInst_RBTree_Remove_m2480195356_gp_0_0_0_0,
	&GenInst_RBTree_Lookup_m3378201690_gp_0_0_0_0,
	&GenInst_RBTree_find_key_m2107656200_gp_0_0_0_0,
	&GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0,
	&GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0,
	&GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4073929546_0_0_0,
	&GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0,
	&GenInst_NodeHelper_t357096749_gp_0_0_0_0,
	&GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0,
	&GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0,
	&GenInst_ValueCollection_t2735575576_gp_1_0_0_0,
	&GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0,
	&GenInst_Enumerator_t1353355221_gp_1_0_0_0,
	&GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0,
	&GenInst_KeyCollection_t3620397270_gp_0_0_0_0,
	&GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0,
	&GenInst_Enumerator_t4275149413_gp_0_0_0_0,
	&GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t404405498_0_0_0,
	&GenInst_Stack_1_t4016656541_gp_0_0_0_0,
	&GenInst_Enumerator_t546412149_gp_0_0_0_0,
	&GenInst_HashSet_1_t2624254809_gp_0_0_0_0,
	&GenInst_Enumerator_t2109956843_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3424417428_gp_0_0_0_0,
	&GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0,
	&GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0,
	&GenInst_Enumerable_All_m2363499768_gp_0_0_0_0,
	&GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Cast_m2974870241_gp_0_0_0_0,
	&GenInst_Enumerable_CreateCastIterator_m3836917259_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0,
	&GenInst_Enumerable_Distinct_m4171187007_gp_0_0_0_0,
	&GenInst_Enumerable_Distinct_m522457978_gp_0_0_0_0,
	&GenInst_Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0,
	&GenInst_Enumerable_Empty_m2393946422_gp_0_0_0_0,
	&GenInst_Enumerable_First_m1693250038_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0,
	&GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0_List_1_t2888087137_0_0_0,
	&GenInst_Enumerable_ContainsGroup_m1146262421_gp_1_0_0_0,
	&GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0,
	&GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0,
	&GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Enumerable_GroupBy_m1544473094_gp_1_0_0_0,
	&GenInst_IGrouping_2_t3129334088_0_0_0,
	&GenInst_Enumerable_GroupBy_m1544473094_gp_1_0_0_0_Enumerable_GroupBy_m1544473094_gp_0_0_0_0,
	&GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0,
	&GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Enumerable_GroupBy_m1259221415_gp_1_0_0_0,
	&GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0,
	&GenInst_IGrouping_2_t3129334089_0_0_0,
	&GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Enumerable_GroupBy_m1259221415_gp_0_0_0_0,
	&GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0,
	&GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0,
	&GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0,
	&GenInst_IGrouping_2_t2588622000_0_0_0,
	&GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0,
	&GenInst_Enumerable_LastOrDefault_m2408605338_gp_0_0_0_0,
	&GenInst_Enumerable_OfType_m192007909_gp_0_0_0_0,
	&GenInst_Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0,
	&GenInst_Enumerable_Repeat_m3228394296_gp_0_0_0_0,
	&GenInst_Enumerable_CreateRepeatIterator_m2601674138_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m3508258668_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0,
	&GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0,
	&GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_IEnumerable_1_t380606227_0_0_0,
	&GenInst_Enumerable_SelectMany_m608128666_gp_1_0_0_0,
	&GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_Enumerable_SelectMany_m608128666_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_IEnumerable_1_t567845041_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0,
	&GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0,
	&GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0,
	&GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m760899018_gp_0_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m2813552737_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Take_m169782875_gp_0_0_0_0,
	&GenInst_Enumerable_CreateTakeIterator_m1267606521_gp_0_0_0_0,
	&GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Enumerable_ToDictionary_m3729734315_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Enumerable_ToDictionary_m3729734315_gp_2_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3729734315_gp_1_0_0_0_Enumerable_ToDictionary_m3729734315_gp_2_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3729734315_gp_0_0_0_0_Enumerable_ToDictionary_m3729734315_gp_1_0_0_0_Enumerable_ToDictionary_m3729734315_gp_2_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0,
	&GenInst_Enumerable_Union_m2318613702_gp_0_0_0_0,
	&GenInst_Enumerable_Union_m2720478133_gp_0_0_0_0,
	&GenInst_Enumerable_CreateUnionIterator_m1338750539_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Function_1_t1491613575_gp_0_0_0_0_Function_1_t1491613575_gp_0_0_0_0,
	&GenInst_Function_1_t1491613575_gp_0_0_0_0,
	&GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t54777913_gp_0_0_0_0,
	&GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0,
	&GenInst_IGrouping_2_t1824977620_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_List_1_t3797843219_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0,
	&GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0,
	&GenInst_U3CCreateRepeatIteratorU3Ec__IteratorE_1_t139808608_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_Int32_t2071877448_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_IEnumerable_1_t3792789022_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0,
	&GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t4103837823_gp_0_0_0_0,
	&GenInst_U3CCreateUnionIteratorU3Ec__Iterator1C_1_t1794850093_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Grouping_2_t288249757_gp_1_0_0_0,
	&GenInst_Grouping_2_t288249757_gp_0_0_0_0_Grouping_2_t288249757_gp_1_0_0_0,
	&GenInst_IGrouping_2_t845301090_gp_1_0_0_0,
	&GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_QuickSort_1_t1290221672_gp_0_0_0_0,
	&GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0,
	&GenInst_SortContext_1_t4088581714_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0,
	&GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0,
	&GenInst_AndroidJavaObject_Call_m1094633808_gp_0_0_0_0,
	&GenInst_AndroidJavaObject_CallStatic_m946265290_gp_0_0_0_0,
	&GenInst_AndroidJavaObject__Call_m4019607101_gp_0_0_0_0,
	&GenInst_AndroidJavaObject__CallStatic_m1525952853_gp_0_0_0_0,
	&GenInst_AndroidJNIHelper_ConvertFromJNIArray_m2082383440_gp_0_0_0_0,
	&GenInst_AndroidJNIHelper_GetMethodID_m2221772144_gp_0_0_0_0,
	&GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0,
	&GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0,
	&GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1319939458_0_0_0,
	&GenInst__AndroidJNIHelper_GetMethodID_m656615819_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t476640868_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0,
	&GenInst_ConfigurationBuilder_Configure_m2155711039_gp_0_0_0_0,
	&GenInst_AbstractPurchasingModule_BindExtension_m1993884112_gp_0_0_0_0,
	&GenInst_AbstractPurchasingModule_BindConfiguration_m3787044503_gp_0_0_0_0,
	&GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetEquatableStruct_m753578693_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ListPool_1_t1984115411_gp_0_0_0_0,
	&GenInst_List_1_t2000868992_0_0_0,
	&GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_FakeStore_StartUI_m935561654_gp_0_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0,
	&GenInst_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_U3CStartUIU3Ec__AnonStorey0_1_t3316954766_gp_0_0_0_0,
	&GenInst_UnityUtil_GetAnyComponentsOfType_m709219744_gp_0_0_0_0,
	&GenInst_UnityUtil_LoadResourceInstanceOfType_m463261387_gp_0_0_0_0,
	&GenInst_NonMonoSingleton_1_t1282585840_gp_0_0_0_0,
	&GenInst_Singleton_1_t4004969804_gp_0_0_0_0,
	&GenInst_SA_Singleton_OLD_1_t2766871217_gp_0_0_0_0,
	&GenInst_EnumerationExtension_ForEach_m633171965_gp_0_0_0_0,
	&GenInst_CustomCreationConverter_1_t1690683567_gp_0_0_0_0,
	&GenInst_JsonConvert_DeserializeObject_m2988054426_gp_0_0_0_0,
	&GenInst_JsonConvert_DeserializeAnonymousType_m1519926232_gp_0_0_0_0,
	&GenInst_IJEnumerable_1_t75840398_gp_0_0_0_0,
	&GenInst_JContainer_Values_m3671546765_gp_0_0_0_0,
	&GenInst_JToken_t2552644013_0_0_0_JContainer_Values_m3671546765_gp_0_0_0_0,
	&GenInst_JEnumerable_1_t922559079_gp_0_0_0_0,
	&GenInst_JEnumerable_1_t922559079_gp_0_0_0_0_JToken_t2552644013_0_0_0,
	&GenInst_JToken_t2552644013_0_0_0_JToken_Value_m80405919_gp_0_0_0_0,
	&GenInst_JToken_Children_m1844711917_gp_0_0_0_0,
	&GenInst_JToken_Values_m3688732645_gp_0_0_0_0,
	&GenInst_JToken_ToObject_m1720428526_gp_0_0_0_0,
	&GenInst_JToken_ToObject_m2930397507_gp_0_0_0_0,
	&GenInst_LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0,
	&GenInst_LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0_IEnumerable_1_t2844771058_0_0_0,
	&GenInst_LinqExtensions_Ancestors_m1941908259_gp_0_0_0_0_JToken_t2552644013_0_0_0,
	&GenInst_LinqExtensions_Descendants_m2164197081_gp_0_0_0_0,
	&GenInst_LinqExtensions_Descendants_m2164197081_gp_0_0_0_0_IEnumerable_1_t2844771058_0_0_0,
	&GenInst_LinqExtensions_Descendants_m2164197081_gp_0_0_0_0_JToken_t2552644013_0_0_0,
	&GenInst_LinqExtensions_Values_m1204940136_gp_0_0_0_0,
	&GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Values_m1204940136_gp_0_0_0_0,
	&GenInst_LinqExtensions_Values_m24015070_gp_0_0_0_0,
	&GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Values_m24015070_gp_0_0_0_0,
	&GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Value_m4050904768_gp_0_0_0_0,
	&GenInst_LinqExtensions_Value_m236465892_gp_0_0_0_0,
	&GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Value_m236465892_gp_1_0_0_0,
	&GenInst_LinqExtensions_Values_m411275432_gp_0_0_0_0,
	&GenInst_LinqExtensions_Values_m411275432_gp_1_0_0_0,
	&GenInst_LinqExtensions_Values_m411275432_gp_0_0_0_0_LinqExtensions_Values_m411275432_gp_1_0_0_0,
	&GenInst_LinqExtensions_Children_m937766004_gp_0_0_0_0,
	&GenInst_LinqExtensions_Children_m937766004_gp_0_0_0_0_JToken_t2552644013_0_0_0,
	&GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0,
	&GenInst_LinqExtensions_Children_m4091488789_gp_1_0_0_0,
	&GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_LinqExtensions_Children_m4091488789_gp_1_0_0_0,
	&GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_IEnumerable_1_t2844771058_0_0_0,
	&GenInst_LinqExtensions_Children_m4091488789_gp_0_0_0_0_JToken_t2552644013_0_0_0,
	&GenInst_JToken_t2552644013_0_0_0_LinqExtensions_Children_m4091488789_gp_1_0_0_0,
	&GenInst_LinqExtensions_Convert_m2057205221_gp_0_0_0_0,
	&GenInst_LinqExtensions_Convert_m2057205221_gp_1_0_0_0,
	&GenInst_LinqExtensions_Convert_m2057205221_gp_0_0_0_0_LinqExtensions_Convert_m2057205221_gp_1_0_0_0,
	&GenInst_LinqExtensions_AsJEnumerable_m1823102599_gp_0_0_0_0,
	&GenInst_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0,
	&GenInst_U3CValuesU3Ec__Iterator12_2_t430610032_gp_0_0_0_0,
	&GenInst_U3CValuesU3Ec__Iterator12_2_t430610032_gp_0_0_0_0_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0,
	&GenInst_JValue_t300956845_0_0_0_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0,
	&GenInst_JToken_t2552644013_0_0_0_U3CValuesU3Ec__Iterator12_2_t430610032_gp_1_0_0_0,
	&GenInst_U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0,
	&GenInst_U3CConvertU3Ec__Iterator13_2_t239315294_gp_0_0_0_0,
	&GenInst_U3CConvertU3Ec__Iterator13_2_t239315294_gp_0_0_0_0_U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0,
	&GenInst_JToken_t2552644013_0_0_0_U3CConvertU3Ec__Iterator13_2_t239315294_gp_1_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0_CachedAttributeGetter_1_t3215015314_gp_0_0_0_0,
	&GenInst_CachedAttributeGetter_1_t3215015314_gp_0_0_0_0,
	&GenInst_JsonTypeReflector_GetAttribute_m3832145644_gp_0_0_0_0,
	&GenInst_JsonTypeReflector_GetAttribute_m2691398215_gp_0_0_0_0,
	&GenInst_JsonTypeReflector_GetAttribute_m2399085218_gp_0_0_0_0,
	&GenInst_BidirectionalDictionary_2_t2143222129_gp_0_0_0_0,
	&GenInst_BidirectionalDictionary_2_t2143222129_gp_1_0_0_0,
	&GenInst_BidirectionalDictionary_2_t2143222129_gp_0_0_0_0_BidirectionalDictionary_2_t2143222129_gp_1_0_0_0,
	&GenInst_BidirectionalDictionary_2_t2143222129_gp_1_0_0_0_BidirectionalDictionary_2_t2143222129_gp_0_0_0_0,
	&GenInst_CollectionUtils_CastValid_m3369744617_gp_0_0_0_0,
	&GenInst_CollectionUtils_CreateList_m2661262288_gp_0_0_0_0,
	&GenInst_CollectionUtils_IsNullOrEmpty_m4123750027_gp_0_0_0_0,
	&GenInst_CollectionUtils_IsNullOrEmptyOrDefault_m3426852801_gp_0_0_0_0,
	&GenInst_CollectionUtils_Slice_m3827466680_gp_0_0_0_0,
	&GenInst_CollectionUtils_Slice_m1108815180_gp_0_0_0_0,
	&GenInst_CollectionUtils_GroupBy_m513581027_gp_1_0_0_0,
	&GenInst_CollectionUtils_GroupBy_m513581027_gp_1_0_0_0_CollectionUtils_GroupBy_m513581027_gp_0_0_0_0,
	&GenInst_CollectionUtils_GroupBy_m513581027_gp_0_0_0_0_List_1_t1924900830_0_0_0,
	&GenInst_CollectionUtils_AddRange_m2231204599_gp_0_0_0_0,
	&GenInst_CollectionUtils_Distinct_m687017250_gp_0_0_0_0,
	&GenInst_CollectionUtils_Flatten_m2921316847_gp_0_0_0_0,
	&GenInst_List_1_t1091115121_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_CollectionUtils_Flatten_m2921316847_gp_0_0_0_0,
	&GenInst_IList_1_t2262934590_0_0_0,
	&GenInst_IList_1_t3947597871_0_0_0,
	&GenInst_CollectionUtils_Recurse_m1486496431_gp_0_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_CollectionUtils_Recurse_m1486496431_gp_0_0_0_0,
	&GenInst_List_1_t2775778402_0_0_0,
	&GenInst_CollectionUtils_CreateList_m4224726381_gp_0_0_0_0,
	&GenInst_CollectionUtils_ListEquals_m2304676934_gp_0_0_0_0,
	&GenInst_CollectionUtils_TryGetSingleItem_m3616950892_gp_0_0_0_0,
	&GenInst_CollectionUtils_TryGetSingleItem_m936218929_gp_0_0_0_0,
	&GenInst_CollectionUtils_GetSingleItem_m1986031524_gp_0_0_0_0,
	&GenInst_CollectionUtils_GetSingleItem_m3328355783_gp_0_0_0_0,
	&GenInst_CollectionUtils_Minus_m1749740517_gp_0_0_0_0,
	&GenInst_CollectionUtils_AddDistinct_m2604642977_gp_0_0_0_0,
	&GenInst_CollectionUtils_AddDistinct_m729482485_gp_0_0_0_0,
	&GenInst_CollectionUtils_ContainsValue_m2334048203_gp_0_0_0_0,
	&GenInst_CollectionUtils_AddRangeDistinct_m737237641_gp_0_0_0_0,
	&GenInst_CollectionUtils_AddRangeDistinct_m2236996053_gp_0_0_0_0,
	&GenInst_CollectionUtils_IndexOf_m1194221877_gp_0_0_0_0,
	&GenInst_CollectionUtils_IndexOf_m1194221877_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_CollectionUtils_IndexOf_m1330017483_gp_0_0_0_0,
	&GenInst_CollectionUtils_IndexOf_m1505122980_gp_0_0_0_0,
	&GenInst_U3CTryGetSingleItemU3Ec__AnonStorey25_1_t2803002290_gp_0_0_0_0,
	&GenInst_CollectionWrapper_1_t318624866_gp_0_0_0_0,
	&GenInst_ConvertUtils_Convert_m1211373838_gp_0_0_0_0,
	&GenInst_ConvertUtils_TryConvert_m1519373714_gp_0_0_0_0,
	&GenInst_ConvertUtils_TryConvert_m3257615354_gp_0_0_0_0,
	&GenInst_ConvertUtils_ConvertOrCast_m219507444_gp_0_0_0_0,
	&GenInst_ConvertUtils_TryConvertOrCast_m4070485328_gp_0_0_0_0,
	&GenInst_ConvertUtils_TryConvertOrCast_m125199128_gp_0_0_0_0,
	&GenInst_DictionaryWrapper_2_t815983041_gp_0_0_0_0_DictionaryWrapper_2_t815983041_gp_1_0_0_0,
	&GenInst_DictionaryWrapper_2_t815983041_gp_0_0_0_0,
	&GenInst_DictionaryWrapper_2_t815983041_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3125239586_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0_KeyValuePair_2_t3125239586_0_0_0,
	&GenInst_KeyValuePair_2_t3432887514_0_0_0,
	&GenInst_DictionaryEnumerator_2_t2360669969_gp_2_0_0_0_DictionaryEnumerator_2_t2360669969_gp_3_0_0_0,
	&GenInst_DictionaryEnumerator_2_t2360669969_gp_0_0_0_0_DictionaryEnumerator_2_t2360669969_gp_1_0_0_0_DictionaryEnumerator_2_t2360669969_gp_2_0_0_0_DictionaryEnumerator_2_t2360669969_gp_3_0_0_0,
	&GenInst_DictionaryWrapper_2_t815983041_gp_0_0_0_0_DictionaryWrapper_2_t815983041_gp_1_0_0_0_DictionaryWrapper_2_t815983041_gp_0_0_0_0_DictionaryWrapper_2_t815983041_gp_1_0_0_0,
	&GenInst_EnumUtils_Parse_m3231354786_gp_0_0_0_0,
	&GenInst_EnumUtils_TryParse_m455840625_gp_0_0_0_0,
	&GenInst_EnumUtils_GetFlagsValues_m2938233177_gp_0_0_0_0,
	&GenInst_EnumUtils_GetNamesAndValues_m3492261118_gp_1_0_0_0,
	&GenInst_EnumUtils_GetNamesAndValues_m2393662447_gp_0_0_0_0,
	&GenInst_EnumValue_1_t549532375_0_0_0,
	&GenInst_EnumUtils_GetValues_m1497029920_gp_0_0_0_0,
	&GenInst_EnumUtils_GetMaximumValue_m4128337876_gp_0_0_0_0,
	&GenInst_U3CTryParseU3Ec__AnonStorey2E_1_t1872128815_gp_0_0_0_0,
	&GenInst_String_t_0_0_0_EnumValue_1_t1952206871_0_0_0,
	&GenInst_EnumValues_1_t1624570432_gp_0_0_0_0,
	&GenInst_EnumValue_1_t1952206871_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m984568458_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m984568458_gp_0_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateDefaultConstructor_m1949776623_gp_0_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateGet_m747619007_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateGet_m747619007_gp_0_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1551745180_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1551745180_gp_0_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1737331520_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1737331520_gp_0_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1213597307_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateSet_m1213597307_gp_0_0_0_0,
	&GenInst_ListWrapper_1_t3036879910_gp_0_0_0_0,
	&GenInst_MiscellaneousUtils_TryAction_m933287344_gp_0_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateGet_m3576934762_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateGet_m3576934762_gp_0_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateSet_m3835094278_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateSet_m3835094278_gp_0_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateMethodCall_m2651893450_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateDefaultConstructor_m448703917_gp_0_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateGet_m1914506157_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateGet_m638437858_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateSet_m2901308994_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateSet_m3581014573_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionUtils_ItemsUnitializedValue_m182429506_gp_0_0_0_0,
	&GenInst_ReflectionUtils_GetAttribute_m258851166_gp_0_0_0_0,
	&GenInst_ReflectionUtils_GetAttribute_m3077178463_gp_0_0_0_0,
	&GenInst_StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0,
	&GenInst_StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0_String_t_0_0_0,
	&GenInst_StringUtils_ForgivingCaseSensitiveFind_m1133502830_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_U3CForgivingCaseSensitiveFindU3Ec__AnonStorey39_1_t2975701800_gp_0_0_0_0_String_t_0_0_0,
	&GenInst_ThreadSafeStore_2_t855392148_gp_0_0_0_0_ThreadSafeStore_2_t855392148_gp_1_0_0_0,
	&GenInst_ValidationUtils_ArgumentNotNullOrEmpty_m291675854_gp_0_0_0_0,
	&GenInst_ValidationUtils_ArgumentNotNullOrEmpty_m1289647618_gp_0_0_0_0,
	&GenInst_ValidationUtils_ArgumentIsPositive_m1157249580_gp_0_0_0_0,
	&GenInst_Extensions_MaxBy_m3981111180_gp_0_0_0_0,
	&GenInst_Extensions_MaxBy_m3981111180_gp_0_0_0_0_Extensions_MaxBy_m3981111180_gp_1_0_0_0,
	&GenInst_Extensions_MaxBy_m3981111180_gp_1_0_0_0,
	&GenInst_MCGBehaviour_Subscribe_m2147325413_gp_0_0_0_0,
	&GenInst_MCGBehaviour_Unsubscribe_m1867511264_gp_0_0_0_0,
	&GenInst_MCGBehaviour_OnNext_m647838905_gp_0_0_0_0,
	&GenInst_MCGBehaviour_OnNext_m4133885633_gp_0_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_MessageSender_SendRequest_m4271204661_gp_1_0_0_0,
	&GenInst_MessageSender_SendRequest_m4271204661_gp_0_0_0_0,
	&GenInst_MessageSender_SendRequest_m4271204661_gp_0_0_0_0_MessageSender_SendRequest_m4271204661_gp_1_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_MessageSender_SendRequestCoroutine_m533839419_gp_1_0_0_0,
	&GenInst_MessageSender_SendRequestCoroutine_m533839419_gp_0_0_0_0,
	&GenInst_MessageSender_SendRequestCoroutine_m533839419_gp_0_0_0_0_MessageSender_SendRequestCoroutine_m533839419_gp_1_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_1_0_0_0,
	&GenInst_U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_0_0_0_0,
	&GenInst_U3CSendRequestCoroutineU3Ec__Iterator18_2_t1279054282_gp_1_0_0_0,
	&GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0,
	&GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0,
	&GenInst_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0,
	&GenInst_GUILayer_t3254902478_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_String_t_0_0_0,
	&GenInst_AsyncUtil_t423752048_0_0_0,
	&GenInst_EventSystem_t3466835263_0_0_0,
	&GenInst_AxisEventData_t1524870173_0_0_0,
	&GenInst_SpriteRenderer_t1209076198_0_0_0,
	&GenInst_AspectMode_t1166448724_0_0_0,
	&GenInst_FitMode_t4030874534_0_0_0,
	&GenInst_Button_t2872111280_0_0_0,
	&GenInst_RawImage_t2749640213_0_0_0,
	&GenInst_Slider_t297367283_0_0_0,
	&GenInst_Scrollbar_t3248359358_0_0_0,
	&GenInst_InputField_t1631627530_0_0_0,
	&GenInst_ScrollRect_t1199013257_0_0_0,
	&GenInst_Dropdown_t1985816271_0_0_0,
	&GenInst_GraphicRaycaster_t410733016_0_0_0,
	&GenInst_CanvasRenderer_t261436805_0_0_0,
	&GenInst_Corner_t1077473318_0_0_0,
	&GenInst_Axis_t1431825778_0_0_0,
	&GenInst_Constraint_t3558160636_0_0_0,
	&GenInst_Type_t3352948571_0_0_0,
	&GenInst_FillMethod_t1640962579_0_0_0,
	&GenInst_SubmitEvent_t907918422_0_0_0,
	&GenInst_OnChangeEvent_t2863344003_0_0_0,
	&GenInst_OnValidateInput_t1946318473_0_0_0,
	&GenInst_LineType_t2931319356_0_0_0,
	&GenInst_InputType_t1274231802_0_0_0,
	&GenInst_TouchScreenKeyboardType_t875112366_0_0_0,
	&GenInst_CharacterValidation_t3437478890_0_0_0,
	&GenInst_LayoutElement_t2808691390_0_0_0,
	&GenInst_RectOffset_t3387826427_0_0_0,
	&GenInst_TextAnchor_t112990806_0_0_0,
	&GenInst_Direction_t3696775921_0_0_0,
	&GenInst_Transition_t605142169_0_0_0,
	&GenInst_AnimationTriggers_t3244928895_0_0_0,
	&GenInst_Animator_t69676727_0_0_0,
	&GenInst_Direction_t1525323322_0_0_0,
	&GenInst_IAmazonExtensions_t3890253245_0_0_0,
	&GenInst_IAmazonConfiguration_t3016942165_0_0_0,
	&GenInst_ISamsungAppsExtensions_t3429739537_0_0_0,
	&GenInst_ISamsungAppsConfiguration_t4066821689_0_0_0,
	&GenInst_UnityUtil_t166323129_0_0_0,
	&GenInst_IAppleExtensions_t1627764765_0_0_0,
	&GenInst_IAppleConfiguration_t3277762425_0_0_0,
	&GenInst_IMicrosoftConfiguration_t1212838845_0_0_0,
	&GenInst_IGooglePlayConfiguration_t2615679878_0_0_0,
	&GenInst_ITizenStoreConfiguration_t2900348728_0_0_0,
	&GenInst_IAndroidStoreSelection_t3134941501_0_0_0,
	&GenInst_LifecycleNotifier_t1057582876_0_0_0,
	&GenInst_StandaloneInputModule_t70867863_0_0_0,
	&GenInst_GameCenterManager_t1487113918_0_0_0,
	&GenInst_IOSDialog_t3518705031_0_0_0,
	&GenInst_IOSMessage_t2569463336_0_0_0,
	&GenInst_IOSNativePreviewBackButton_t2513092733_0_0_0,
	&GenInst_IOSNativeSettings_t547170227_0_0_0,
	&GenInst_IOSRateUsPopUp_t2222998473_0_0_0,
	&GenInst_ConnectionButton_t3853092004_0_0_0,
	&GenInst_DisconnectButton_t3965161390_0_0_0,
	&GenInst_ClickManagerExample_t799098483_0_0_0,
	&GenInst_Renderer_t257310565_0_0_0,
	&GenInst_GUITexture_t1909122990_0_0_0,
	&GenInst_GUIText_t2411476300_0_0_0,
	&GenInst_Light_t494725636_0_0_0,
	&GenInst_AudioSource_t1135106623_0_0_0,
	&GenInst_Rigidbody_t4233889191_0_0_0,
	&GenInst_SA_iTween_t1938847631_0_0_0,
	&GenInst_ValuesTween_t4181447589_0_0_0,
	&GenInst_Invoker_t994751176_0_0_0,
	&GenInst_PrefabAsyncLoader_t3449223737_0_0_0,
	&GenInst_ScreenshotMaker_t4063620334_0_0_0,
	&GenInst_WWWTextureLoader_t499142963_0_0_0,
	&GenInst_ISD_Settings_t1116242554_0_0_0,
	&GenInst_AN_PlusShareListener_t257800803_0_0_0,
	&GenInst_AndroidDialog_t3283474837_0_0_0,
	&GenInst_AndroidMessage_t2504997174_0_0_0,
	&GenInst_AndroidNativeSettings_t3397157021_0_0_0,
	&GenInst_AndroidRateUsPopUp_t1491925263_0_0_0,
	&GenInst_Spinner_t3781576355_0_0_0,
	&GenInst_JumpTypeCycle_t3789588250_0_0_0,
	&GenInst_Crop_t2548131222_0_0_0,
	&GenInst_GetCourseRequest_t2656580458_0_0_0_GetCourseResponse_t2659116210_0_0_0,
	&GenInst_Jump_Collider_t2378236225_0_0_0,
	&GenInst_CreateCourseRequest_t4267998642_0_0_0_CreateCourseResponse_t3800683746_0_0_0,
	&GenInst_DeleteCourseRequest_t1683798751_0_0_0_DeleteCourseResponse_t1898856809_0_0_0,
	&GenInst_CourseObject_t1071976694_0_0_0,
	&GenInst_HorseAnimation_t2036550335_0_0_0,
	&GenInst_EnvironmentSelect_t3424254515_0_0_0,
	&GenInst_LineRenderer_t849157671_0_0_0,
	&GenInst_List_1_t1317062444_0_0_0,
	&GenInst_CourseVisualizer_t3597553323_0_0_0,
	&GenInst_EventTrigger_t1967201810_0_0_0,
	&GenInst_FB_LikesRetrieveTask_t2495592094_0_0_0,
	&GenInst_FB_PostingTask_t2068490008_0_0_0,
	&GenInst_CoursesNumberExceeded_t3039661446_0_0_0,
	&GenInst_InitAndroidInventoryTask_t4246510222_0_0_0,
	&GenInst_List_1_t1357208527_0_0_0,
	&GenInst_List_1_t2294885245_0_0_0,
	&GenInst_SampleExternalClass_t2192168175_0_0_0,
	&GenInst_Dictionary_2_t1933589748_0_0_0,
	&GenInst_Dictionary_2_t4254316062_0_0_0,
	&GenInst_ForgotPasswordRequest_t1886688323_0_0_0_ForgotPasswordResponse_t4062332037_0_0_0,
	&GenInst_ChangePasswordRequest_t4102061450_0_0_0_ChangePasswordResponse_t3897893034_0_0_0,
	&GenInst_LoginRequest_t2273172322_0_0_0_LoginResponse_t1319408382_0_0_0,
	&GenInst_Toggle_t3489774764_0_0_0,
	&GenInst_RegisterRequest_t698226714_0_0_0_RegisterResponse_t410466074_0_0_0,
	&GenInst_JsonSchemaModel_t708894576_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_JObject_t278519297_0_0_0_JProperty_t2956441399_0_0_0,
	&GenInst_JToken_t2552644013_0_0_0_JToken_t2552644013_0_0_0,
	&GenInst_DescriptionAttribute_t3207779672_0_0_0,
	&GenInst_JsonPropertyAttribute_t2023370155_0_0_0,
	&GenInst_JsonIgnoreAttribute_t898097550_0_0_0,
	&GenInst_DefaultValueAttribute_t1302720498_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0_ParameterInfo_t2249040075_0_0_0_Il2CppObject_0_0_0,
	&GenInst_JsonProperty_t2712067825_0_0_0_JsonProperty_t2712067825_0_0_0_PropertyPresence_t220200932_0_0_0,
	&GenInst_JsonConverterAttribute_t3918837738_0_0_0,
	&GenInst_PreviewScreenUtil_t19016308_0_0_0,
	&GenInst_Collider_t3497673348_0_0_0,
	&GenInst_SA_StatusBar_t1378080260_0_0_0,
	&GenInst_SocialPlatfromSettings_t3418207459_0_0_0,
	&GenInst_TW_FollowersIdsRequest_t261436078_0_0_0,
	&GenInst_TW_FriendsIdsRequest_t151035516_0_0_0,
	&GenInst_TW_OAuthAPIRequest_t3823919588_0_0_0,
	&GenInst_TW_SearchTweetsRequest_t525743233_0_0_0,
	&GenInst_TW_UsersLookUpRequest_t889831987_0_0_0,
	&GenInst_TW_UserTimeLineRequest_t1907229837_0_0_0,
	&GenInst_TwitterPostingTask_t1522896362_0_0_0,
	&GenInst_WWWTextureLoader_t155872171_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_PropertyPresence_t220200932_0_0_0,
	&GenInst_JTokenType_t1307827213_0_0_0_JTokenType_t1307827213_0_0_0,
	&GenInst_JsonSchemaType_t1742745177_0_0_0_JsonSchemaType_t1742745177_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0,
	&GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0,
	&GenInst_RuntimePlatform_t1869584967_0_0_0_RuntimePlatform_t1869584967_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0,
	&GenInst_AN_ManifestPermission_t1772110202_0_0_0_AN_ManifestPermission_t1772110202_0_0_0,
	&GenInst_AN_ManifestPermission_t1772110202_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AN_PermissionState_t838201224_0_0_0_AN_PermissionState_t838201224_0_0_0,
	&GenInst_AN_PermissionState_t838201224_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3409008887_0_0_0_KeyValuePair_2_t3409008887_0_0_0,
	&GenInst_KeyValuePair_2_t3409008887_0_0_0_Il2CppObject_0_0_0,
	&GenInst_FB_ProfileImageSize_t3003328130_0_0_0_FB_ProfileImageSize_t3003328130_0_0_0,
	&GenInst_KeyValuePair_2_t534914198_0_0_0_KeyValuePair_2_t534914198_0_0_0,
	&GenInst_KeyValuePair_2_t534914198_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TypeNameKey_t3055062677_0_0_0_TypeNameKey_t3055062677_0_0_0,
	&GenInst_KeyValuePair_2_t94122711_0_0_0_KeyValuePair_2_t94122711_0_0_0,
	&GenInst_KeyValuePair_2_t94122711_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TypeConvertKey_t1788482786_0_0_0_TypeConvertKey_t1788482786_0_0_0,
	&GenInst_KeyValuePair_2_t4204318902_0_0_0_KeyValuePair_2_t4204318902_0_0_0,
	&GenInst_KeyValuePair_2_t4204318902_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t115911300_0_0_0_KeyValuePair_2_t115911300_0_0_0,
	&GenInst_KeyValuePair_2_t115911300_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_JsonSchemaType_t1742745177_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3387117823_0_0_0_KeyValuePair_2_t3387117823_0_0_0,
	&GenInst_KeyValuePair_2_t3387117823_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PropertyPresence_t220200932_0_0_0_PropertyPresence_t220200932_0_0_0,
	&GenInst_PropertyPresence_t220200932_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1864573578_0_0_0_KeyValuePair_2_t1864573578_0_0_0,
	&GenInst_KeyValuePair_2_t1864573578_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_KeyValuePair_2_t1683227291_0_0_0_KeyValuePair_2_t1683227291_0_0_0,
	&GenInst_KeyValuePair_2_t1683227291_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t566713506_0_0_0_KeyValuePair_2_t566713506_0_0_0,
	&GenInst_KeyValuePair_2_t566713506_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_AndroidStore_t3203055206_0_0_0,
};
