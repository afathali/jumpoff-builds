﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DustinHorne.Json.Examples.JNSimpleObjectModel
struct JNSimpleObjectModel_t1947941312;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_DustinHorne_Json_Examples_JNObje4041290897.h"

// System.Void DustinHorne.Json.Examples.JNSimpleObjectModel::.ctor()
extern "C"  void JNSimpleObjectModel__ctor_m2425990649 (JNSimpleObjectModel_t1947941312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DustinHorne.Json.Examples.JNSimpleObjectModel::get_IntValue()
extern "C"  int32_t JNSimpleObjectModel_get_IntValue_m4113250246 (JNSimpleObjectModel_t1947941312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DustinHorne.Json.Examples.JNSimpleObjectModel::set_IntValue(System.Int32)
extern "C"  void JNSimpleObjectModel_set_IntValue_m3014477137 (JNSimpleObjectModel_t1947941312 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DustinHorne.Json.Examples.JNSimpleObjectModel::get_FloatValue()
extern "C"  float JNSimpleObjectModel_get_FloatValue_m602054973 (JNSimpleObjectModel_t1947941312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DustinHorne.Json.Examples.JNSimpleObjectModel::set_FloatValue(System.Single)
extern "C"  void JNSimpleObjectModel_set_FloatValue_m869366990 (JNSimpleObjectModel_t1947941312 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DustinHorne.Json.Examples.JNSimpleObjectModel::get_StringValue()
extern "C"  String_t* JNSimpleObjectModel_get_StringValue_m564167711 (JNSimpleObjectModel_t1947941312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DustinHorne.Json.Examples.JNSimpleObjectModel::set_StringValue(System.String)
extern "C"  void JNSimpleObjectModel_set_StringValue_m4253707176 (JNSimpleObjectModel_t1947941312 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> DustinHorne.Json.Examples.JNSimpleObjectModel::get_IntList()
extern "C"  List_1_t1440998580 * JNSimpleObjectModel_get_IntList_m2912553041 (JNSimpleObjectModel_t1947941312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DustinHorne.Json.Examples.JNSimpleObjectModel::set_IntList(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void JNSimpleObjectModel_set_IntList_m1997964010 (JNSimpleObjectModel_t1947941312 * __this, List_1_t1440998580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DustinHorne.Json.Examples.JNObjectType DustinHorne.Json.Examples.JNSimpleObjectModel::get_ObjectType()
extern "C"  int32_t JNSimpleObjectModel_get_ObjectType_m2181715085 (JNSimpleObjectModel_t1947941312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DustinHorne.Json.Examples.JNSimpleObjectModel::set_ObjectType(DustinHorne.Json.Examples.JNObjectType)
extern "C"  void JNSimpleObjectModel_set_ObjectType_m2462902602 (JNSimpleObjectModel_t1947941312 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
