﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SK_RequestStorefrontIdentifierResult
struct SK_RequestStorefrontIdentifierResult_t1112477162;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SK_RequestStorefrontIdentifierResult::.ctor()
extern "C"  void SK_RequestStorefrontIdentifierResult__ctor_m2409072583 (SK_RequestStorefrontIdentifierResult_t1112477162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_RequestStorefrontIdentifierResult::.ctor(System.String)
extern "C"  void SK_RequestStorefrontIdentifierResult__ctor_m3372218593 (SK_RequestStorefrontIdentifierResult_t1112477162 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SK_RequestStorefrontIdentifierResult::get_StorefrontIdentifier()
extern "C"  String_t* SK_RequestStorefrontIdentifierResult_get_StorefrontIdentifier_m2139624576 (SK_RequestStorefrontIdentifierResult_t1112477162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_RequestStorefrontIdentifierResult::set_StorefrontIdentifier(System.String)
extern "C"  void SK_RequestStorefrontIdentifierResult_set_StorefrontIdentifier_m2867142185 (SK_RequestStorefrontIdentifierResult_t1112477162 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
