﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_ScreenShotMaker_OLD
struct SA_ScreenShotMaker_OLD_t795198627;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void SA_ScreenShotMaker_OLD::.ctor()
extern "C"  void SA_ScreenShotMaker_OLD__ctor_m178812220 (SA_ScreenShotMaker_OLD_t795198627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_ScreenShotMaker_OLD::GetScreenshot()
extern "C"  void SA_ScreenShotMaker_OLD_GetScreenshot_m139353964 (SA_ScreenShotMaker_OLD_t795198627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SA_ScreenShotMaker_OLD::SaveScreenshot()
extern "C"  Il2CppObject * SA_ScreenShotMaker_OLD_SaveScreenshot_m255523197 (SA_ScreenShotMaker_OLD_t795198627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
