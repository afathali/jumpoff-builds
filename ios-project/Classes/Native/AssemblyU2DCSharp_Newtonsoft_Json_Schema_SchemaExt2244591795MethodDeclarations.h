﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JToken
struct JToken_t2552644013;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t3772113849;
// Newtonsoft.Json.Schema.ValidationEventHandler
struct ValidationEventHandler_t1731902491;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken2552644013.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3772113849.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_Validatio1731902491.h"

// System.Boolean Newtonsoft.Json.Schema.SchemaExtensions::IsValid(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  bool SchemaExtensions_IsValid_m4127107871 (Il2CppObject * __this /* static, unused */, JToken_t2552644013 * ___source0, JsonSchema_t3772113849 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.SchemaExtensions::Validate(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void SchemaExtensions_Validate_m2891921785 (Il2CppObject * __this /* static, unused */, JToken_t2552644013 * ___source0, JsonSchema_t3772113849 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.SchemaExtensions::Validate(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Schema.JsonSchema,Newtonsoft.Json.Schema.ValidationEventHandler)
extern "C"  void SchemaExtensions_Validate_m4179750552 (Il2CppObject * __this /* static, unused */, JToken_t2552644013 * ___source0, JsonSchema_t3772113849 * ___schema1, ValidationEventHandler_t1731902491 * ___validationEventHandler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
