﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidApp
struct AndroidApp_t620754602;
// System.String
struct String_t;
// AndroidActivityResult
struct AndroidActivityResult_t3757510801;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AndroidActivityResult3757510801.h"

// System.Void AndroidApp::.ctor()
extern "C"  void AndroidApp__ctor_m2148680673 (AndroidApp_t620754602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidApp::Awake()
extern "C"  void AndroidApp_Awake_m2779878742 (AndroidApp_t620754602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidApp::ActivateListner()
extern "C"  void AndroidApp_ActivateListner_m16076167 (AndroidApp_t620754602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidApp::onActivityResult(System.String)
extern "C"  void AndroidApp_onActivityResult_m2192730922 (AndroidApp_t620754602 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidApp::<OnActivityResult>m__24(AndroidActivityResult)
extern "C"  void AndroidApp_U3COnActivityResultU3Em__24_m1921509352 (Il2CppObject * __this /* static, unused */, AndroidActivityResult_t3757510801 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
