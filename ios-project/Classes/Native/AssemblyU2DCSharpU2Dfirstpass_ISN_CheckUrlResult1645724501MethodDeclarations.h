﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_CheckUrlResult
struct ISN_CheckUrlResult_t1645724501;
// System.String
struct String_t;
// SA.Common.Models.Error
struct Error_t445207774;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"

// System.Void ISN_CheckUrlResult::.ctor(System.String)
extern "C"  void ISN_CheckUrlResult__ctor_m2277072686 (ISN_CheckUrlResult_t1645724501 * __this, String_t* ___checkedUrl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_CheckUrlResult::.ctor(System.String,SA.Common.Models.Error)
extern "C"  void ISN_CheckUrlResult__ctor_m3602654005 (ISN_CheckUrlResult_t1645724501 * __this, String_t* ___checkedUrl0, Error_t445207774 * ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ISN_CheckUrlResult::get_url()
extern "C"  String_t* ISN_CheckUrlResult_get_url_m1249487377 (ISN_CheckUrlResult_t1645724501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
