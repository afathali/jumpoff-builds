﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<DustinHorne.Json.Examples.JNSimpleObjectModel>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1348665933(__this, ___l0, method) ((  void (*) (Enumerator_t851792118 *, List_1_t1317062444 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DustinHorne.Json.Examples.JNSimpleObjectModel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1631193501(__this, method) ((  void (*) (Enumerator_t851792118 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<DustinHorne.Json.Examples.JNSimpleObjectModel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1765975557(__this, method) ((  Il2CppObject * (*) (Enumerator_t851792118 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DustinHorne.Json.Examples.JNSimpleObjectModel>::Dispose()
#define Enumerator_Dispose_m3007188304(__this, method) ((  void (*) (Enumerator_t851792118 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DustinHorne.Json.Examples.JNSimpleObjectModel>::VerifyState()
#define Enumerator_VerifyState_m4197761655(__this, method) ((  void (*) (Enumerator_t851792118 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<DustinHorne.Json.Examples.JNSimpleObjectModel>::MoveNext()
#define Enumerator_MoveNext_m3118730853(__this, method) ((  bool (*) (Enumerator_t851792118 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<DustinHorne.Json.Examples.JNSimpleObjectModel>::get_Current()
#define Enumerator_get_Current_m77243978(__this, method) ((  JNSimpleObjectModel_t1947941312 * (*) (Enumerator_t851792118 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
