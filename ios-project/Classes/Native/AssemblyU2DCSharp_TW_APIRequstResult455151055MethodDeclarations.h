﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TW_APIRequstResult
struct TW_APIRequstResult_t455151055;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TW_APIRequstResult::.ctor(System.Boolean,System.String)
extern "C"  void TW_APIRequstResult__ctor_m994234943 (TW_APIRequstResult_t455151055 * __this, bool ___IsResSucceeded0, String_t* ___resData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TW_APIRequstResult::get_IsSucceeded()
extern "C"  bool TW_APIRequstResult_get_IsSucceeded_m2129417210 (TW_APIRequstResult_t455151055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TW_APIRequstResult::get_responce()
extern "C"  String_t* TW_APIRequstResult_get_responce_m3836324577 (TW_APIRequstResult_t455151055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
