﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CropHints
struct CropHints_t3208752360;

#include "codegen/il2cpp-codegen.h"

// System.Void CropHints::.ctor()
extern "C"  void CropHints__ctor_m3457023785 (CropHints_t3208752360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CropHints::Start()
extern "C"  void CropHints_Start_m3875518425 (CropHints_t3208752360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CropHints::ShowHintsUI(System.Boolean)
extern "C"  void CropHints_ShowHintsUI_m17671833 (CropHints_t3208752360 * __this, bool ___show0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CropHints::<Start>m__A()
extern "C"  void CropHints_U3CStartU3Em__A_m3967085063 (CropHints_t3208752360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CropHints::<Start>m__B()
extern "C"  void CropHints_U3CStartU3Em__B_m2703529100 (CropHints_t3208752360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
