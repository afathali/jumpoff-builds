﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey2A`1<System.Object>
struct U3CTryConvertU3Ec__AnonStorey2A_1_t142049201;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey2A`1<System.Object>::.ctor()
extern "C"  void U3CTryConvertU3Ec__AnonStorey2A_1__ctor_m148562382_gshared (U3CTryConvertU3Ec__AnonStorey2A_1_t142049201 * __this, const MethodInfo* method);
#define U3CTryConvertU3Ec__AnonStorey2A_1__ctor_m148562382(__this, method) ((  void (*) (U3CTryConvertU3Ec__AnonStorey2A_1_t142049201 *, const MethodInfo*))U3CTryConvertU3Ec__AnonStorey2A_1__ctor_m148562382_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.ConvertUtils/<TryConvert>c__AnonStorey2A`1<System.Object>::<>m__EC()
extern "C"  Il2CppObject * U3CTryConvertU3Ec__AnonStorey2A_1_U3CU3Em__EC_m193775364_gshared (U3CTryConvertU3Ec__AnonStorey2A_1_t142049201 * __this, const MethodInfo* method);
#define U3CTryConvertU3Ec__AnonStorey2A_1_U3CU3Em__EC_m193775364(__this, method) ((  Il2CppObject * (*) (U3CTryConvertU3Ec__AnonStorey2A_1_t142049201 *, const MethodInfo*))U3CTryConvertU3Ec__AnonStorey2A_1_U3CU3Em__EC_m193775364_gshared)(__this, method)
