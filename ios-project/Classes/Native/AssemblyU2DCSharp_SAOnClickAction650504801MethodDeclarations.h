﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SAOnClickAction
struct SAOnClickAction_t650504801;

#include "codegen/il2cpp-codegen.h"

// System.Void SAOnClickAction::.ctor()
extern "C"  void SAOnClickAction__ctor_m1520591696 (SAOnClickAction_t650504801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SAOnClickAction::Awake()
extern "C"  void SAOnClickAction_Awake_m1413835265 (SAOnClickAction_t650504801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
