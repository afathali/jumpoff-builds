﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FB_AppRequestResult
struct FB_AppRequestResult_t2450670428;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FB_AppRequestResult::.ctor(System.String,System.String)
extern "C"  void FB_AppRequestResult__ctor_m830111169 (FB_AppRequestResult_t2450670428 * __this, String_t* ___RawData0, String_t* ___Error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_AppRequestResult::.ctor(System.String,System.Collections.Generic.List`1<System.String>,System.String)
extern "C"  void FB_AppRequestResult__ctor_m3780725797 (FB_AppRequestResult_t2450670428 * __this, String_t* ___requestId0, List_1_t1398341365 * ____recipients1, String_t* ___RawData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FB_AppRequestResult::get_ReuqestId()
extern "C"  String_t* FB_AppRequestResult_get_ReuqestId_m3614748277 (FB_AppRequestResult_t2450670428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> FB_AppRequestResult::get_Recipients()
extern "C"  List_1_t1398341365 * FB_AppRequestResult_get_Recipients_m1114739895 (FB_AppRequestResult_t2450670428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
