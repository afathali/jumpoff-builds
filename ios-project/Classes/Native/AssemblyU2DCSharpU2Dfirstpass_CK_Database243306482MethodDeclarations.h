﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CK_Database
struct CK_Database_t243306482;
// System.Action`1<CK_RecordResult>
struct Action_1_t3864347731;
// System.Action`1<CK_RecordDeleteResult>
struct Action_1_t3050268866;
// System.Action`1<CK_QueryResult>
struct Action_1_t975866870;
// CK_Record
struct CK_Record_t3973541762;
// CK_RecordID
struct CK_RecordID_t41838833;
// CK_Query
struct CK_Query_t2718018967;
// CK_RecordResult
struct CK_RecordResult_t4062548349;
// CK_RecordDeleteResult
struct CK_RecordDeleteResult_t3248469484;
// CK_QueryResult
struct CK_QueryResult_t1174067488;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_Record3973541762.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_RecordID41838833.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_Query2718018967.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_RecordResult4062548349.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_RecordDeleteResul3248469484.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_QueryResult1174067488.h"

// System.Void CK_Database::.ctor(System.Int32)
extern "C"  void CK_Database__ctor_m1030788938 (CK_Database_t243306482 * __this, int32_t ___internalId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::.cctor()
extern "C"  void CK_Database__cctor_m2659050846 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::add_ActionRecordSaved(System.Action`1<CK_RecordResult>)
extern "C"  void CK_Database_add_ActionRecordSaved_m1704560504 (CK_Database_t243306482 * __this, Action_1_t3864347731 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::remove_ActionRecordSaved(System.Action`1<CK_RecordResult>)
extern "C"  void CK_Database_remove_ActionRecordSaved_m626447513 (CK_Database_t243306482 * __this, Action_1_t3864347731 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::add_ActionRecordFetchComplete(System.Action`1<CK_RecordResult>)
extern "C"  void CK_Database_add_ActionRecordFetchComplete_m3951176800 (CK_Database_t243306482 * __this, Action_1_t3864347731 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::remove_ActionRecordFetchComplete(System.Action`1<CK_RecordResult>)
extern "C"  void CK_Database_remove_ActionRecordFetchComplete_m3463596761 (CK_Database_t243306482 * __this, Action_1_t3864347731 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::add_ActionRecordDeleted(System.Action`1<CK_RecordDeleteResult>)
extern "C"  void CK_Database_add_ActionRecordDeleted_m1796533871 (CK_Database_t243306482 * __this, Action_1_t3050268866 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::remove_ActionRecordDeleted(System.Action`1<CK_RecordDeleteResult>)
extern "C"  void CK_Database_remove_ActionRecordDeleted_m296655214 (CK_Database_t243306482 * __this, Action_1_t3050268866 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::add_ActionQueryComplete(System.Action`1<CK_QueryResult>)
extern "C"  void CK_Database_add_ActionQueryComplete_m2714308294 (CK_Database_t243306482 * __this, Action_1_t975866870 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::remove_ActionQueryComplete(System.Action`1<CK_QueryResult>)
extern "C"  void CK_Database_remove_ActionQueryComplete_m596751045 (CK_Database_t243306482 * __this, Action_1_t975866870 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::SaveRecrod(CK_Record)
extern "C"  void CK_Database_SaveRecrod_m662725693 (CK_Database_t243306482 * __this, CK_Record_t3973541762 * ___record0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::FetchRecordWithID(CK_RecordID)
extern "C"  void CK_Database_FetchRecordWithID_m3922029406 (CK_Database_t243306482 * __this, CK_RecordID_t41838833 * ___recordId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::DeleteRecordWithID(CK_RecordID)
extern "C"  void CK_Database_DeleteRecordWithID_m4284224675 (CK_Database_t243306482 * __this, CK_RecordID_t41838833 * ___recordId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::PerformQuery(CK_Query)
extern "C"  void CK_Database_PerformQuery_m822183557 (CK_Database_t243306482 * __this, CK_Query_t2718018967 * ___query0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CK_Database::get_InternalId()
extern "C"  int32_t CK_Database_get_InternalId_m346991536 (CK_Database_t243306482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CK_Database CK_Database::GetDatabaseByInternalId(System.Int32)
extern "C"  CK_Database_t243306482 * CK_Database_GetDatabaseByInternalId_m3857563445 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::FireSaveRecordResult(CK_RecordResult)
extern "C"  void CK_Database_FireSaveRecordResult_m1637185361 (CK_Database_t243306482 * __this, CK_RecordResult_t4062548349 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::FireFetchRecordResult(CK_RecordResult)
extern "C"  void CK_Database_FireFetchRecordResult_m1409788410 (CK_Database_t243306482 * __this, CK_RecordResult_t4062548349 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::FireDeleteRecordResult(CK_RecordDeleteResult)
extern "C"  void CK_Database_FireDeleteRecordResult_m4054142046 (CK_Database_t243306482 * __this, CK_RecordDeleteResult_t3248469484 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::FireQueryCompleteResult(CK_QueryResult)
extern "C"  void CK_Database_FireQueryCompleteResult_m3135379981 (CK_Database_t243306482 * __this, CK_QueryResult_t1174067488 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::<ActionRecordSaved>m__6F(CK_RecordResult)
extern "C"  void CK_Database_U3CActionRecordSavedU3Em__6F_m1386191337 (Il2CppObject * __this /* static, unused */, CK_RecordResult_t4062548349 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::<ActionRecordFetchComplete>m__70(CK_RecordResult)
extern "C"  void CK_Database_U3CActionRecordFetchCompleteU3Em__70_m3730874520 (Il2CppObject * __this /* static, unused */, CK_RecordResult_t4062548349 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::<ActionRecordDeleted>m__71(CK_RecordDeleteResult)
extern "C"  void CK_Database_U3CActionRecordDeletedU3Em__71_m1017932002 (Il2CppObject * __this /* static, unused */, CK_RecordDeleteResult_t3248469484 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_Database::<ActionQueryComplete>m__72(CK_QueryResult)
extern "C"  void CK_Database_U3CActionQueryCompleteU3Em__72_m3543471488 (Il2CppObject * __this /* static, unused */, CK_QueryResult_t1174067488 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
