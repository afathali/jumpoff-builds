﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Shared_Response_1_gen3572928552MethodDeclarations.h"

// System.Void Shared.Response`1<CreateCourseRequest>::.ctor()
#define Response_1__ctor_m1066208382(__this, method) ((  void (*) (Response_1_t856510603 *, const MethodInfo*))Response_1__ctor_m920360982_gshared)(__this, method)
