﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_AppInvitesController
struct GP_AppInvitesController_t2424429497;
// System.Action`1<GP_SendAppInvitesResult>
struct Action_1_t3800876926;
// System.Action`1<GP_RetrieveAppInviteResult>
struct Action_1_t4115251907;
// GP_AppInviteBuilder
struct GP_AppInviteBuilder_t1976720519;
// System.String
struct String_t;
// GP_SendAppInvitesResult
struct GP_SendAppInvitesResult_t3999077544;
// GP_RetrieveAppInviteResult
struct GP_RetrieveAppInviteResult_t18485229;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GP_AppInviteBuilder1976720519.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_SendAppInvitesResult3999077544.h"
#include "AssemblyU2DCSharp_GP_RetrieveAppInviteResult18485229.h"

// System.Void GP_AppInvitesController::.ctor()
extern "C"  void GP_AppInvitesController__ctor_m3379756294 (GP_AppInvitesController_t2424429497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::.cctor()
extern "C"  void GP_AppInvitesController__cctor_m2043015275 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::add_ActionAppInvitesSent(System.Action`1<GP_SendAppInvitesResult>)
extern "C"  void GP_AppInvitesController_add_ActionAppInvitesSent_m23851291 (Il2CppObject * __this /* static, unused */, Action_1_t3800876926 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::remove_ActionAppInvitesSent(System.Action`1<GP_SendAppInvitesResult>)
extern "C"  void GP_AppInvitesController_remove_ActionAppInvitesSent_m743780978 (Il2CppObject * __this /* static, unused */, Action_1_t3800876926 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::add_ActionAppInviteRetrieved(System.Action`1<GP_RetrieveAppInviteResult>)
extern "C"  void GP_AppInvitesController_add_ActionAppInviteRetrieved_m1241493703 (Il2CppObject * __this /* static, unused */, Action_1_t4115251907 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::remove_ActionAppInviteRetrieved(System.Action`1<GP_RetrieveAppInviteResult>)
extern "C"  void GP_AppInvitesController_remove_ActionAppInviteRetrieved_m1125414990 (Il2CppObject * __this /* static, unused */, Action_1_t4115251907 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::Awake()
extern "C"  void GP_AppInvitesController_Awake_m858474869 (GP_AppInvitesController_t2424429497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::StartInvitationDialog(GP_AppInviteBuilder)
extern "C"  void GP_AppInvitesController_StartInvitationDialog_m1724448342 (GP_AppInvitesController_t2424429497 * __this, GP_AppInviteBuilder_t1976720519 * ___builder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::GetInvitation(System.Boolean)
extern "C"  void GP_AppInvitesController_GetInvitation_m2377301264 (GP_AppInvitesController_t2424429497 * __this, bool ___autoLaunchDeepLink0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::OnInvitationDialogComplete(System.String)
extern "C"  void GP_AppInvitesController_OnInvitationDialogComplete_m1012639431 (GP_AppInvitesController_t2424429497 * __this, String_t* ___InvitationIds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::OnInvitationDialogFailed(System.String)
extern "C"  void GP_AppInvitesController_OnInvitationDialogFailed_m1248954629 (GP_AppInvitesController_t2424429497 * __this, String_t* ___erroCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::OnInvitationLoadFailed(System.String)
extern "C"  void GP_AppInvitesController_OnInvitationLoadFailed_m649367583 (GP_AppInvitesController_t2424429497 * __this, String_t* ___erroCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::OnInvitationLoaded(System.String)
extern "C"  void GP_AppInvitesController_OnInvitationLoaded_m413707257 (GP_AppInvitesController_t2424429497 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::<ActionAppInvitesSent>m__2A(GP_SendAppInvitesResult)
extern "C"  void GP_AppInvitesController_U3CActionAppInvitesSentU3Em__2A_m551001099 (Il2CppObject * __this /* static, unused */, GP_SendAppInvitesResult_t3999077544 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_AppInvitesController::<ActionAppInviteRetrieved>m__2B(GP_RetrieveAppInviteResult)
extern "C"  void GP_AppInvitesController_U3CActionAppInviteRetrievedU3Em__2B_m3395694708 (Il2CppObject * __this /* static, unused */, GP_RetrieveAppInviteResult_t18485229 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
