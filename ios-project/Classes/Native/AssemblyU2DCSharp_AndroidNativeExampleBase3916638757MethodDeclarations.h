﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidNativeExampleBase
struct AndroidNativeExampleBase_t3916638757;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidNativeExampleBase::.ctor()
extern "C"  void AndroidNativeExampleBase__ctor_m98878054 (AndroidNativeExampleBase_t3916638757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNativeExampleBase::Awake()
extern "C"  void AndroidNativeExampleBase_Awake_m729977313 (AndroidNativeExampleBase_t3916638757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
