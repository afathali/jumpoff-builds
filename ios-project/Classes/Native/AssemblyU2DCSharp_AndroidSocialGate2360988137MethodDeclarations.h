﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidSocialGate
struct AndroidSocialGate_t2360988137;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void AndroidSocialGate::.ctor()
extern "C"  void AndroidSocialGate__ctor_m4050262346 (AndroidSocialGate_t2360988137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialGate::StartGooglePlusShare(System.String,UnityEngine.Texture2D)
extern "C"  void AndroidSocialGate_StartGooglePlusShare_m936759292 (Il2CppObject * __this /* static, unused */, String_t* ___text0, Texture2D_t3542995729 * ___texture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialGate::StartShareIntent(System.String,System.String,System.String)
extern "C"  void AndroidSocialGate_StartShareIntent_m1971266095 (Il2CppObject * __this /* static, unused */, String_t* ___caption0, String_t* ___message1, String_t* ___packageNamePattern2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialGate::StartShareIntent(System.String,System.String,UnityEngine.Texture2D,System.String)
extern "C"  void AndroidSocialGate_StartShareIntent_m3609281005 (Il2CppObject * __this /* static, unused */, String_t* ___caption0, String_t* ___message1, Texture2D_t3542995729 * ___texture2, String_t* ___packageNamePattern3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialGate::StartShareIntentWithSubject(System.String,System.String,System.String,System.String)
extern "C"  void AndroidSocialGate_StartShareIntentWithSubject_m1921836143 (Il2CppObject * __this /* static, unused */, String_t* ___caption0, String_t* ___message1, String_t* ___subject2, String_t* ___packageNamePattern3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialGate::StartShareIntentWithSubject(System.String,System.String,System.String,UnityEngine.Texture2D,System.String)
extern "C"  void AndroidSocialGate_StartShareIntentWithSubject_m3230935345 (Il2CppObject * __this /* static, unused */, String_t* ___caption0, String_t* ___message1, String_t* ___subject2, Texture2D_t3542995729 * ___texture3, String_t* ___packageNamePattern4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidSocialGate::SendMail(System.String,System.String,System.String,System.String,UnityEngine.Texture2D)
extern "C"  void AndroidSocialGate_SendMail_m2173869347 (Il2CppObject * __this /* static, unused */, String_t* ___caption0, String_t* ___message1, String_t* ___subject2, String_t* ___recipients3, Texture2D_t3542995729 * ___texture4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
