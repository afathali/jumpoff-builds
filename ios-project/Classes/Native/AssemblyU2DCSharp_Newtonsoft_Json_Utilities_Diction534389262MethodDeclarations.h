﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>
struct DictionaryWrapper_2_t534389262;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.Generic.IDictionary`2<System.Object,System.Object>
struct IDictionary_2_t280592844;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::.ctor(System.Collections.IDictionary)
extern "C"  void DictionaryWrapper_2__ctor_m691830228_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___dictionary0, const MethodInfo* method);
#define DictionaryWrapper_2__ctor_m691830228(__this, ___dictionary0, method) ((  void (*) (DictionaryWrapper_2_t534389262 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2__ctor_m691830228_gshared)(__this, ___dictionary0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void DictionaryWrapper_2__ctor_m1452490861_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define DictionaryWrapper_2__ctor_m1452490861(__this, ___dictionary0, method) ((  void (*) (DictionaryWrapper_2_t534389262 *, Il2CppObject*, const MethodInfo*))DictionaryWrapper_2__ctor_m1452490861_gshared)(__this, ___dictionary0, method)
// System.Collections.IEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IEnumerable_GetEnumerator_m2017731448_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IEnumerable_GetEnumerator_m2017731448(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IEnumerable_GetEnumerator_m2017731448_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void DictionaryWrapper_2_System_Collections_IDictionary_Add_m2846887253_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_Add_m2846887253(__this, ___key0, ___value1, method) ((  void (*) (DictionaryWrapper_2_t534389262 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_Add_m2846887253_gshared)(__this, ___key0, ___value1, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool DictionaryWrapper_2_System_Collections_IDictionary_Contains_m2172301529_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_Contains_m2172301529(__this, ___key0, method) ((  bool (*) (DictionaryWrapper_2_t534389262 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_Contains_m2172301529_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1046604918_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1046604918(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1046604918_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool DictionaryWrapper_2_System_Collections_IDictionary_get_IsFixedSize_m3994873554_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_get_IsFixedSize_m3994873554(__this, method) ((  bool (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_get_IsFixedSize_m3994873554_gshared)(__this, method)
// System.Collections.ICollection Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3567681645_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3567681645(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3567681645_gshared)(__this, method)
// System.Collections.ICollection Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_get_Values_m3915142077_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_get_Values_m3915142077(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_get_Values_m3915142077_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m960486311_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m960486311(__this, ___key0, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t534389262 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m960486311_gshared)(__this, ___key0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m3956535670_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m3956535670(__this, ___key0, ___value1, method) ((  void (*) (DictionaryWrapper_2_t534389262 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m3956535670_gshared)(__this, ___key0, ___value1, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m2464244227_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m2464244227(__this, ___array0, ___index1, method) ((  void (*) (DictionaryWrapper_2_t534389262 *, Il2CppArray *, int32_t, const MethodInfo*))DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m2464244227_gshared)(__this, ___array0, ___index1, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m3832261271_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m3832261271(__this, method) ((  bool (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m3832261271_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2251724019_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2251724019(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2251724019_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void DictionaryWrapper_2_Add_m757996496_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_Add_m757996496(__this, ___key0, ___value1, method) ((  void (*) (DictionaryWrapper_2_t534389262 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_Add_m757996496_gshared)(__this, ___key0, ___value1, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool DictionaryWrapper_2_ContainsKey_m1831012438_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_ContainsKey_m1831012438(__this, ___key0, method) ((  bool (*) (DictionaryWrapper_2_t534389262 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_ContainsKey_m1831012438_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* DictionaryWrapper_2_get_Keys_m2092482101_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_Keys_m2092482101(__this, method) ((  Il2CppObject* (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_get_Keys_m2092482101_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(TKey)
extern "C"  bool DictionaryWrapper_2_Remove_m3198912934_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_Remove_m3198912934(__this, ___key0, method) ((  bool (*) (DictionaryWrapper_2_t534389262 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_Remove_m3198912934_gshared)(__this, ___key0, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool DictionaryWrapper_2_TryGetValue_m3700931523_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_TryGetValue_m3700931523(__this, ___key0, ___value1, method) ((  bool (*) (DictionaryWrapper_2_t534389262 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))DictionaryWrapper_2_TryGetValue_m3700931523_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.ICollection`1<TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Values()
extern "C"  Il2CppObject* DictionaryWrapper_2_get_Values_m1831572781_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_Values_m1831572781(__this, method) ((  Il2CppObject* (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_get_Values_m1831572781_gshared)(__this, method)
// TValue Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * DictionaryWrapper_2_get_Item_m2362944062_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_get_Item_m2362944062(__this, ___key0, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t534389262 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_get_Item_m2362944062_gshared)(__this, ___key0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void DictionaryWrapper_2_set_Item_m2531867175_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_set_Item_m2531867175(__this, ___key0, ___value1, method) ((  void (*) (DictionaryWrapper_2_t534389262 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_set_Item_m2531867175_gshared)(__this, ___key0, ___value1, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void DictionaryWrapper_2_Add_m3612891991_gshared (DictionaryWrapper_2_t534389262 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method);
#define DictionaryWrapper_2_Add_m3612891991(__this, ___item0, method) ((  void (*) (DictionaryWrapper_2_t534389262 *, KeyValuePair_2_t38854645 , const MethodInfo*))DictionaryWrapper_2_Add_m3612891991_gshared)(__this, ___item0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Clear()
extern "C"  void DictionaryWrapper_2_Clear_m3928068896_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_Clear_m3928068896(__this, method) ((  void (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_Clear_m3928068896_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool DictionaryWrapper_2_Contains_m3569786257_gshared (DictionaryWrapper_2_t534389262 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method);
#define DictionaryWrapper_2_Contains_m3569786257(__this, ___item0, method) ((  bool (*) (DictionaryWrapper_2_t534389262 *, KeyValuePair_2_t38854645 , const MethodInfo*))DictionaryWrapper_2_Contains_m3569786257_gshared)(__this, ___item0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void DictionaryWrapper_2_CopyTo_m2039386035_gshared (DictionaryWrapper_2_t534389262 * __this, KeyValuePair_2U5BU5D_t2854920344* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define DictionaryWrapper_2_CopyTo_m2039386035(__this, ___array0, ___arrayIndex1, method) ((  void (*) (DictionaryWrapper_2_t534389262 *, KeyValuePair_2U5BU5D_t2854920344*, int32_t, const MethodInfo*))DictionaryWrapper_2_CopyTo_m2039386035_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t DictionaryWrapper_2_get_Count_m2623408759_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_Count_m2623408759(__this, method) ((  int32_t (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_get_Count_m2623408759_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool DictionaryWrapper_2_get_IsReadOnly_m1745434428_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_IsReadOnly_m1745434428(__this, method) ((  bool (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_get_IsReadOnly_m1745434428_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool DictionaryWrapper_2_Remove_m2930571166_gshared (DictionaryWrapper_2_t534389262 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method);
#define DictionaryWrapper_2_Remove_m2930571166(__this, ___item0, method) ((  bool (*) (DictionaryWrapper_2_t534389262 *, KeyValuePair_2_t38854645 , const MethodInfo*))DictionaryWrapper_2_Remove_m2930571166_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* DictionaryWrapper_2_GetEnumerator_m2069280100_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_GetEnumerator_m2069280100(__this, method) ((  Il2CppObject* (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_GetEnumerator_m2069280100_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(System.Object)
extern "C"  void DictionaryWrapper_2_Remove_m743786935_gshared (DictionaryWrapper_2_t534389262 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_Remove_m743786935(__this, ___key0, method) ((  void (*) (DictionaryWrapper_2_t534389262 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_Remove_m743786935_gshared)(__this, ___key0, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_UnderlyingDictionary()
extern "C"  Il2CppObject * DictionaryWrapper_2_get_UnderlyingDictionary_m4285210172_gshared (DictionaryWrapper_2_t534389262 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_UnderlyingDictionary_m4285210172(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t534389262 *, const MethodInfo*))DictionaryWrapper_2_get_UnderlyingDictionary_m4285210172_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::<GetEnumerator>m__F0(System.Collections.DictionaryEntry)
extern "C"  KeyValuePair_2_t38854645  DictionaryWrapper_2_U3CGetEnumeratorU3Em__F0_m1182448152_gshared (Il2CppObject * __this /* static, unused */, DictionaryEntry_t3048875398  ___de0, const MethodInfo* method);
#define DictionaryWrapper_2_U3CGetEnumeratorU3Em__F0_m1182448152(__this /* static, unused */, ___de0, method) ((  KeyValuePair_2_t38854645  (*) (Il2CppObject * /* static, unused */, DictionaryEntry_t3048875398 , const MethodInfo*))DictionaryWrapper_2_U3CGetEnumeratorU3Em__F0_m1182448152_gshared)(__this /* static, unused */, ___de0, method)
