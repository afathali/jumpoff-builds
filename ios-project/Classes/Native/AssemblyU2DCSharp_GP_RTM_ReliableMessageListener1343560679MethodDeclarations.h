﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_RTM_ReliableMessageListener
struct GP_RTM_ReliableMessageListener_t1343560679;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.Void GP_RTM_ReliableMessageListener::.ctor(System.Int32,System.Byte[])
extern "C"  void GP_RTM_ReliableMessageListener__ctor_m675539894 (GP_RTM_ReliableMessageListener_t1343560679 * __this, int32_t ___dataTokenId0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_RTM_ReliableMessageListener::ReportSentMessage()
extern "C"  void GP_RTM_ReliableMessageListener_ReportSentMessage_m1827263835 (GP_RTM_ReliableMessageListener_t1343560679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_RTM_ReliableMessageListener::ReportDeliveredMessage()
extern "C"  void GP_RTM_ReliableMessageListener_ReportDeliveredMessage_m2114471847 (GP_RTM_ReliableMessageListener_t1343560679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GP_RTM_ReliableMessageListener::get_DataTokenId()
extern "C"  int32_t GP_RTM_ReliableMessageListener_get_DataTokenId_m3408456155 (GP_RTM_ReliableMessageListener_t1343560679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GP_RTM_ReliableMessageListener::get_Data()
extern "C"  ByteU5BU5D_t3397334013* GP_RTM_ReliableMessageListener_get_Data_m2264979399 (GP_RTM_ReliableMessageListener_t1343560679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
