﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TwitterStatus
struct TwitterStatus_t2765035499;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void TwitterStatus::.ctor(System.Collections.IDictionary)
extern "C"  void TwitterStatus__ctor_m1609543363 (TwitterStatus_t2765035499 * __this, Il2CppObject * ___JSON0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterStatus::get_rawJSON()
extern "C"  String_t* TwitterStatus_get_rawJSON_m895623078 (TwitterStatus_t2765035499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterStatus::get_text()
extern "C"  String_t* TwitterStatus_get_text_m1975256347 (TwitterStatus_t2765035499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TwitterStatus::get_geo()
extern "C"  String_t* TwitterStatus_get_geo_m2786132087 (TwitterStatus_t2765035499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
