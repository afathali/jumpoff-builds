﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>
struct Dictionary_2_t1334805305;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4024308743.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1888479228_gshared (Enumerator_t4024308743 * __this, Dictionary_2_t1334805305 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1888479228(__this, ___host0, method) ((  void (*) (Enumerator_t4024308743 *, Dictionary_2_t1334805305 *, const MethodInfo*))Enumerator__ctor_m1888479228_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1103411995_gshared (Enumerator_t4024308743 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1103411995(__this, method) ((  Il2CppObject * (*) (Enumerator_t4024308743 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1103411995_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1868764239_gshared (Enumerator_t4024308743 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1868764239(__this, method) ((  void (*) (Enumerator_t4024308743 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1868764239_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::Dispose()
extern "C"  void Enumerator_Dispose_m900475408_gshared (Enumerator_t4024308743 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m900475408(__this, method) ((  void (*) (Enumerator_t4024308743 *, const MethodInfo*))Enumerator_Dispose_m900475408_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1448737131_gshared (Enumerator_t4024308743 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1448737131(__this, method) ((  bool (*) (Enumerator_t4024308743 *, const MethodInfo*))Enumerator_MoveNext_m1448737131_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m4186407577_gshared (Enumerator_t4024308743 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4186407577(__this, method) ((  Il2CppObject * (*) (Enumerator_t4024308743 *, const MethodInfo*))Enumerator_get_Current_m4186407577_gshared)(__this, method)
