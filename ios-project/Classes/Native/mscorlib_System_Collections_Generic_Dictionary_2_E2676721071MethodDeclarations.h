﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>
struct Dictionary_2_t1356696369;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2676721071.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23409008887.h"
#include "AssemblyU2DCSharp_AN_ManifestPermission1772110202.h"
#include "AssemblyU2DCSharp_AN_PermissionState838201224.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3639829663_gshared (Enumerator_t2676721071 * __this, Dictionary_2_t1356696369 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3639829663(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2676721071 *, Dictionary_2_t1356696369 *, const MethodInfo*))Enumerator__ctor_m3639829663_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m707319646_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m707319646(__this, method) ((  Il2CppObject * (*) (Enumerator_t2676721071 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m707319646_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m79201504_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m79201504(__this, method) ((  void (*) (Enumerator_t2676721071 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m79201504_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1803753565_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1803753565(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2676721071 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1803753565_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3208901104_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3208901104(__this, method) ((  Il2CppObject * (*) (Enumerator_t2676721071 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3208901104_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3415387586_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3415387586(__this, method) ((  Il2CppObject * (*) (Enumerator_t2676721071 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3415387586_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1965426417_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1965426417(__this, method) ((  bool (*) (Enumerator_t2676721071 *, const MethodInfo*))Enumerator_MoveNext_m1965426417_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::get_Current()
extern "C"  KeyValuePair_2_t3409008887  Enumerator_get_Current_m4125464454_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4125464454(__this, method) ((  KeyValuePair_2_t3409008887  (*) (Enumerator_t2676721071 *, const MethodInfo*))Enumerator_get_Current_m4125464454_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m4071352643_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m4071352643(__this, method) ((  int32_t (*) (Enumerator_t2676721071 *, const MethodInfo*))Enumerator_get_CurrentKey_m4071352643_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m3254424323_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3254424323(__this, method) ((  int32_t (*) (Enumerator_t2676721071 *, const MethodInfo*))Enumerator_get_CurrentValue_m3254424323_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::Reset()
extern "C"  void Enumerator_Reset_m3948831101_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3948831101(__this, method) ((  void (*) (Enumerator_t2676721071 *, const MethodInfo*))Enumerator_Reset_m3948831101_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3727973704_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3727973704(__this, method) ((  void (*) (Enumerator_t2676721071 *, const MethodInfo*))Enumerator_VerifyState_m3727973704_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2881515912_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2881515912(__this, method) ((  void (*) (Enumerator_t2676721071 *, const MethodInfo*))Enumerator_VerifyCurrent_m2881515912_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AN_ManifestPermission,AN_PermissionState>::Dispose()
extern "C"  void Enumerator_Dispose_m2041202203_gshared (Enumerator_t2676721071 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2041202203(__this, method) ((  void (*) (Enumerator_t2676721071 *, const MethodInfo*))Enumerator_Dispose_m2041202203_gshared)(__this, method)
