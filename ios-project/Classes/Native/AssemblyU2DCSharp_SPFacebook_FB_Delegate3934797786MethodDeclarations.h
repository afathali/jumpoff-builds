﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SPFacebook/FB_Delegate
struct FB_Delegate_t3934797786;
// System.Object
struct Il2CppObject;
// FB_Result
struct FB_Result_t838248372;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_FB_Result838248372.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void SPFacebook/FB_Delegate::.ctor(System.Object,System.IntPtr)
extern "C"  void FB_Delegate__ctor_m3704190307 (FB_Delegate_t3934797786 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook/FB_Delegate::Invoke(FB_Result)
extern "C"  void FB_Delegate_Invoke_m2983414413 (FB_Delegate_t3934797786 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult SPFacebook/FB_Delegate::BeginInvoke(FB_Result,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FB_Delegate_BeginInvoke_m3336004708 (FB_Delegate_t3934797786 * __this, FB_Result_t838248372 * ___result0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebook/FB_Delegate::EndInvoke(System.IAsyncResult)
extern "C"  void FB_Delegate_EndInvoke_m3495042741 (FB_Delegate_t3934797786 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
