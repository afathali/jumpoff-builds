﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonReaderException
struct JsonReaderException_t2043888884;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void Newtonsoft.Json.JsonReaderException::.ctor()
extern "C"  void JsonReaderException__ctor_m672731048 (JsonReaderException_t2043888884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonReaderException::.ctor(System.String)
extern "C"  void JsonReaderException__ctor_m2887038574 (JsonReaderException_t2043888884 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonReaderException::.ctor(System.String,System.Exception)
extern "C"  void JsonReaderException__ctor_m801596782 (JsonReaderException_t2043888884 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonReaderException::.ctor(System.String,System.Exception,System.Int32,System.Int32)
extern "C"  void JsonReaderException__ctor_m287726616 (JsonReaderException_t2043888884 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, int32_t ___lineNumber2, int32_t ___linePosition3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonReaderException::get_LineNumber()
extern "C"  int32_t JsonReaderException_get_LineNumber_m99133434 (JsonReaderException_t2043888884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonReaderException::set_LineNumber(System.Int32)
extern "C"  void JsonReaderException_set_LineNumber_m1583373767 (JsonReaderException_t2043888884 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonReaderException::get_LinePosition()
extern "C"  int32_t JsonReaderException_get_LinePosition_m1999773828 (JsonReaderException_t2043888884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonReaderException::set_LinePosition(System.Int32)
extern "C"  void JsonReaderException_set_LinePosition_m3243425045 (JsonReaderException_t2043888884 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
