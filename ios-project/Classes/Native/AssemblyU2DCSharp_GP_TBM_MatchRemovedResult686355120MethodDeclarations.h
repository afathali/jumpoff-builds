﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_TBM_MatchRemovedResult
struct GP_TBM_MatchRemovedResult_t686355120;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_TBM_MatchRemovedResult::.ctor(System.String)
extern "C"  void GP_TBM_MatchRemovedResult__ctor_m1289843609 (GP_TBM_MatchRemovedResult_t686355120 * __this, String_t* ___mId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GP_TBM_MatchRemovedResult::get_MatchId()
extern "C"  String_t* GP_TBM_MatchRemovedResult_get_MatchId_m619157033 (GP_TBM_MatchRemovedResult_t686355120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
