﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::.ctor()
#define List_1__ctor_m3666578036(__this, method) ((  void (*) (List_1_t1427691559 *, const MethodInfo*))List_1__ctor_m1864370736_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m3075990573(__this, ___collection0, method) ((  void (*) (List_1_t1427691559 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2396561940_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::.ctor(System.Int32)
#define List_1__ctor_m1701848463(__this, ___capacity0, method) ((  void (*) (List_1_t1427691559 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::.cctor()
#define List_1__cctor_m506105221(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4234458250(__this, method) ((  Il2CppObject* (*) (List_1_t1427691559 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m4140598424(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1427691559 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1585734075(__this, method) ((  Il2CppObject * (*) (List_1_t1427691559 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1477297756(__this, ___item0, method) ((  int32_t (*) (List_1_t1427691559 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m527961466(__this, ___item0, method) ((  bool (*) (List_1_t1427691559 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1370167242(__this, ___item0, method) ((  int32_t (*) (List_1_t1427691559 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m2650842515(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1427691559 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1393606779(__this, ___item0, method) ((  void (*) (List_1_t1427691559 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3583259183(__this, method) ((  bool (*) (List_1_t1427691559 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2029850228(__this, method) ((  bool (*) (List_1_t1427691559 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2478012898(__this, method) ((  Il2CppObject * (*) (List_1_t1427691559 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m2097139219(__this, method) ((  bool (*) (List_1_t1427691559 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3506528792(__this, method) ((  bool (*) (List_1_t1427691559 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2244020527(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1427691559 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2544492538(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1427691559 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::Add(T)
#define List_1_Add_m2251919039(__this, ___item0, method) ((  void (*) (List_1_t1427691559 *, List_1_t2058570427 *, const MethodInfo*))List_1_Add_m2488140228_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2426056922(__this, ___newCount0, method) ((  void (*) (List_1_t1427691559 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3849044578(__this, ___collection0, method) ((  void (*) (List_1_t1427691559 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2839870482(__this, ___enumerable0, method) ((  void (*) (List_1_t1427691559 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1798979927(__this, ___collection0, method) ((  void (*) (List_1_t1427691559 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::AsReadOnly()
#define List_1_AsReadOnly_m3738547672(__this, method) ((  ReadOnlyCollection_1_t2244356119 * (*) (List_1_t1427691559 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::Clear()
#define List_1_Clear_m3011483403(__this, method) ((  void (*) (List_1_t1427691559 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::Contains(T)
#define List_1_Contains_m2454838869(__this, ___item0, method) ((  bool (*) (List_1_t1427691559 *, List_1_t2058570427 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::CopyTo(T[])
#define List_1_CopyTo_m196996620(__this, ___array0, method) ((  void (*) (List_1_t1427691559 *, List_1U5BU5D_t3679011834*, const MethodInfo*))List_1_CopyTo_m626830950_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m2354426499(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1427691559 *, List_1U5BU5D_t3679011834*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::Find(System.Predicate`1<T>)
#define List_1_Find_m2550846709(__this, ___match0, method) ((  List_1_t2058570427 * (*) (List_1_t1427691559 *, Predicate_1_t501540542 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1126571832(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t501540542 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3798184533(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1427691559 *, int32_t, int32_t, Predicate_1_t501540542 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::GetEnumerator()
#define List_1_GetEnumerator_m2143355300(__this, method) ((  Enumerator_t962421233  (*) (List_1_t1427691559 *, const MethodInfo*))List_1_GetEnumerator_m1854899495_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::IndexOf(T)
#define List_1_IndexOf_m3704072343(__this, ___item0, method) ((  int32_t (*) (List_1_t1427691559 *, List_1_t2058570427 *, const MethodInfo*))List_1_IndexOf_m1888505567_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m4265009524(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1427691559 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3214702683(__this, ___index0, method) ((  void (*) (List_1_t1427691559 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::Insert(System.Int32,T)
#define List_1_Insert_m3064455222(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1427691559 *, int32_t, List_1_t2058570427 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2942090589(__this, ___collection0, method) ((  void (*) (List_1_t1427691559 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::Remove(T)
#define List_1_Remove_m1339155960(__this, ___item0, method) ((  bool (*) (List_1_t1427691559 *, List_1_t2058570427 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m466550820(__this, ___match0, method) ((  int32_t (*) (List_1_t1427691559 *, Predicate_1_t501540542 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m921306722(__this, ___index0, method) ((  void (*) (List_1_t1427691559 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2639322385_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::Reverse()
#define List_1_Reverse_m3277812578(__this, method) ((  void (*) (List_1_t1427691559 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::Sort()
#define List_1_Sort_m3470726670(__this, method) ((  void (*) (List_1_t1427691559 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3175267938(__this, ___comparer0, method) ((  void (*) (List_1_t1427691559 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2549464061(__this, ___comparison0, method) ((  void (*) (List_1_t1427691559 *, Comparison_1_t3320309278 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::ToArray()
#define List_1_ToArray_m1895035353(__this, method) ((  List_1U5BU5D_t3679011834* (*) (List_1_t1427691559 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::TrimExcess()
#define List_1_TrimExcess_m206734615(__this, method) ((  void (*) (List_1_t1427691559 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::get_Capacity()
#define List_1_get_Capacity_m1545494113(__this, method) ((  int32_t (*) (List_1_t1427691559 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m2198547074(__this, ___value0, method) ((  void (*) (List_1_t1427691559 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::get_Count()
#define List_1_get_Count_m2358194092(__this, method) ((  int32_t (*) (List_1_t1427691559 *, const MethodInfo*))List_1_get_Count_m4253763168_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::get_Item(System.Int32)
#define List_1_get_Item_m763257902(__this, ___index0, method) ((  List_1_t2058570427 * (*) (List_1_t1427691559 *, int32_t, const MethodInfo*))List_1_get_Item_m905507485_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Object>>::set_Item(System.Int32,T)
#define List_1_set_Item_m859361895(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1427691559 *, int32_t, List_1_t2058570427 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
