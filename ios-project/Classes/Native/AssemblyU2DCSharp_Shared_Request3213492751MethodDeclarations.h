﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Shared.Request
struct Request_t3213492751;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Shared.Request::.ctor()
extern "C"  void Request__ctor_m649389877 (Request_t3213492751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Shared.Request::get_api_version()
extern "C"  int32_t Request_get_api_version_m3902875407 (Request_t3213492751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shared.Request::set_api_version(System.Int32)
extern "C"  void Request_set_api_version_m3493102760 (Request_t3213492751 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Shared.Request::get_BaseType()
extern "C"  String_t* Request_get_BaseType_m4123397572 (Request_t3213492751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shared.Request::set_BaseType(System.String)
extern "C"  void Request_set_BaseType_m762259739 (Request_t3213492751 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shared.Request::SetType()
extern "C"  void Request_SetType_m2926140511 (Request_t3213492751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
