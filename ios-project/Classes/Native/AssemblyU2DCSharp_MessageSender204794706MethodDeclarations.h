﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageSender
struct MessageSender_t204794706;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageSender::.ctor()
extern "C"  void MessageSender__ctor_m1351608363 (MessageSender_t204794706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSender::.cctor()
extern "C"  void MessageSender__cctor_m580910784 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSender::GetBridge()
extern "C"  void MessageSender_GetBridge_m2282086224 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MessageSender::GetPostUrl()
extern "C"  String_t* MessageSender_GetPostUrl_m751670255 (MessageSender_t204794706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
