﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DustinHorne.Json.Examples.JNPolymorphismSample
struct JNPolymorphismSample_t2287370235;
// DustinHorne.Json.Examples.JNSimpleObjectModel
struct JNSimpleObjectModel_t1947941312;
// DustinHorne.Json.Examples.JNSubClassModel
struct JNSubClassModel_t2237730893;

#include "codegen/il2cpp-codegen.h"

// System.Void DustinHorne.Json.Examples.JNPolymorphismSample::.ctor()
extern "C"  void JNPolymorphismSample__ctor_m821885986 (JNPolymorphismSample_t2287370235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DustinHorne.Json.Examples.JNPolymorphismSample::Sample()
extern "C"  void JNPolymorphismSample_Sample_m2758709782 (JNPolymorphismSample_t2287370235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DustinHorne.Json.Examples.JNSimpleObjectModel DustinHorne.Json.Examples.JNPolymorphismSample::GetBaseModel()
extern "C"  JNSimpleObjectModel_t1947941312 * JNPolymorphismSample_GetBaseModel_m2078657151 (JNPolymorphismSample_t2287370235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DustinHorne.Json.Examples.JNSubClassModel DustinHorne.Json.Examples.JNPolymorphismSample::GetSubClassModel()
extern "C"  JNSubClassModel_t2237730893 * JNPolymorphismSample_GetSubClassModel_m3775589405 (JNPolymorphismSample_t2287370235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
