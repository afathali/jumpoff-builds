﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.Action`1<System.Object>
struct Action_1_t2491248677;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Aot.EnumerationExtension::ForEach(System.Collections.IEnumerable,System.Action`1<System.Object>)
extern "C"  void EnumerationExtension_ForEach_m1359024743 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___enumerable0, Action_1_t2491248677 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
