﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_RTM_Network_Package
struct GP_RTM_Network_Package_t2050307869;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_RTM_Network_Package::.ctor(System.String,System.String)
extern "C"  void GP_RTM_Network_Package__ctor_m3914955016 (GP_RTM_Network_Package_t2050307869 * __this, String_t* ___player0, String_t* ___recievedData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GP_RTM_Network_Package::get_participantId()
extern "C"  String_t* GP_RTM_Network_Package_get_participantId_m2216390638 (GP_RTM_Network_Package_t2050307869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GP_RTM_Network_Package::get_buffer()
extern "C"  ByteU5BU5D_t3397334013* GP_RTM_Network_Package_get_buffer_m1528026021 (GP_RTM_Network_Package_t2050307869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GP_RTM_Network_Package::ConvertStringToByteData(System.String)
extern "C"  ByteU5BU5D_t3397334013* GP_RTM_Network_Package_ConvertStringToByteData_m2113478205 (Il2CppObject * __this /* static, unused */, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
