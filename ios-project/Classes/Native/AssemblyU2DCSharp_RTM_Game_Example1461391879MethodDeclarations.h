﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RTM_Game_Example
struct RTM_Game_Example_t1461391879;
// System.String
struct String_t;
// GooglePlayResult
struct GooglePlayResult_t3097469636;
// GooglePlayConnectionResult
struct GooglePlayConnectionResult_t2758718724;
// GP_Invite
struct GP_Invite_t626929087;
// GP_RTM_Network_Package
struct GP_RTM_Network_Package_t2050307869;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GooglePlayResult3097469636.h"
#include "AssemblyU2DCSharp_GooglePlayConnectionResult2758718724.h"
#include "AssemblyU2DCSharp_GP_Invite626929087.h"
#include "AssemblyU2DCSharp_GP_GamesStatusCodes1013506173.h"
#include "AssemblyU2DCSharp_AndroidDialogResult1664046954.h"
#include "AssemblyU2DCSharp_GP_RTM_Network_Package2050307869.h"

// System.Void RTM_Game_Example::.ctor()
extern "C"  void RTM_Game_Example__ctor_m1395171160 (RTM_Game_Example_t1461391879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::Start()
extern "C"  void RTM_Game_Example_Start_m1673718100 (RTM_Game_Example_t1461391879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::ConncetButtonPress()
extern "C"  void RTM_Game_Example_ConncetButtonPress_m1933184733 (RTM_Game_Example_t1461391879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::ShowWatingRoom()
extern "C"  void RTM_Game_Example_ShowWatingRoom_m3714602648 (RTM_Game_Example_t1461391879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::findMatch()
extern "C"  void RTM_Game_Example_findMatch_m3142949336 (RTM_Game_Example_t1461391879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::InviteFriends()
extern "C"  void RTM_Game_Example_InviteFriends_m2923456686 (RTM_Game_Example_t1461391879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::SendHello()
extern "C"  void RTM_Game_Example_SendHello_m1802520570 (RTM_Game_Example_t1461391879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::LeaveRoom()
extern "C"  void RTM_Game_Example_LeaveRoom_m3874901968 (RTM_Game_Example_t1461391879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::DrawParticipants()
extern "C"  void RTM_Game_Example_DrawParticipants_m4191126914 (RTM_Game_Example_t1461391879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::UpdateGameState(System.String)
extern "C"  void RTM_Game_Example_UpdateGameState_m1299987902 (RTM_Game_Example_t1461391879 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::FixedUpdate()
extern "C"  void RTM_Game_Example_FixedUpdate_m2912872557 (RTM_Game_Example_t1461391879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::OnPlayerDisconnected()
extern "C"  void RTM_Game_Example_OnPlayerDisconnected_m1143018105 (RTM_Game_Example_t1461391879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::OnPlayerConnected()
extern "C"  void RTM_Game_Example_OnPlayerConnected_m3630385575 (RTM_Game_Example_t1461391879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::OnFriendListLoaded(GooglePlayResult)
extern "C"  void RTM_Game_Example_OnFriendListLoaded_m3942370498 (RTM_Game_Example_t1461391879 * __this, GooglePlayResult_t3097469636 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::OnConnectionResult(GooglePlayConnectionResult)
extern "C"  void RTM_Game_Example_OnConnectionResult_m2836470362 (RTM_Game_Example_t1461391879 * __this, GooglePlayConnectionResult_t2758718724 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::OnInvite(GP_Invite)
extern "C"  void RTM_Game_Example_OnInvite_m1554357803 (RTM_Game_Example_t1461391879 * __this, GP_Invite_t626929087 * ___invitation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::ActionInvitationAccepted(GP_Invite)
extern "C"  void RTM_Game_Example_ActionInvitationAccepted_m1911898645 (RTM_Game_Example_t1461391879 * __this, GP_Invite_t626929087 * ___invitation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::OnRoomCreated(GP_GamesStatusCodes)
extern "C"  void RTM_Game_Example_OnRoomCreated_m740040127 (RTM_Game_Example_t1461391879 * __this, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::OnInvDialogComplete(AndroidDialogResult)
extern "C"  void RTM_Game_Example_OnInvDialogComplete_m721264277 (RTM_Game_Example_t1461391879 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::OnInvManageDialogComplete(AndroidDialogResult)
extern "C"  void RTM_Game_Example_OnInvManageDialogComplete_m1797121572 (RTM_Game_Example_t1461391879 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RTM_Game_Example::OnGCDataReceived(GP_RTM_Network_Package)
extern "C"  void RTM_Game_Example_OnGCDataReceived_m2308185683 (RTM_Game_Example_t1461391879 * __this, GP_RTM_Network_Package_t2050307869 * ___package0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
