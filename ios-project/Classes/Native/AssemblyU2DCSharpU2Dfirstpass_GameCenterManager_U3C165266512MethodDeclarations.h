﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0
struct U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t165266512;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0::.ctor()
extern "C"  void U3CLoadLeaderboardInfoLocalU3Ec__Iterator0__ctor_m1632567863 (U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t165266512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m597938381 (U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t165266512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m485413253 (U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t165266512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_MoveNext_m1372829117 (U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t165266512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0::Dispose()
extern "C"  void U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_Dispose_m2539316748 (U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t165266512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterManager/<LoadLeaderboardInfoLocal>c__Iterator0::Reset()
extern "C"  void U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_Reset_m187509590 (U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t165266512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
