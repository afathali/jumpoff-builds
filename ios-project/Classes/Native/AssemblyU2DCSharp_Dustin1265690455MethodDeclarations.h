﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Dustin
struct Dustin_t1265690455;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Dustin::.ctor()
extern "C"  void Dustin__ctor_m3029406782 (Dustin_t1265690455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Dustin::get_Name()
extern "C"  String_t* Dustin_get_Name_m190883585 (Dustin_t1265690455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dustin::set_Name(System.String)
extern "C"  void Dustin_set_Name_m1571271852 (Dustin_t1265690455 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Dustin::get_Age()
extern "C"  int32_t Dustin_get_Age_m2270403876 (Dustin_t1265690455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dustin::set_Age(System.Int32)
extern "C"  void Dustin_set_Age_m2232502805 (Dustin_t1265690455 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Dustin::get_Url()
extern "C"  String_t* Dustin_get_Url_m314263765 (Dustin_t1265690455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dustin::set_Url(System.String)
extern "C"  void Dustin_set_Url_m3945455578 (Dustin_t1265690455 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
