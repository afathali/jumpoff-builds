﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JProperty2956441399.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JPropertyDes544573776.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JRaw3423748388.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken2552644013.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken_U3CAn445779108.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken_U3CA1950608209.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken_U3CB3432444035.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenEquali711291442.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenReade3330885370.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType1307827213.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenWrite3631426868.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JValue300956845.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_LinqExtensio778605574.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MemberSerializati687984360.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MissingMemberHand367517353.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_NullValueHandlin3618095365.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObjectCreationHa3720134651.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObservableSuppor1197149700.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObservableSuppor2380857217.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObservableSuppor2707150522.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObservableSuppor2999891911.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_PreserveReferenc3019117943.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ReferenceLoopHan1017855894.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Required2961887721.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3772113849.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1268927014.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3943075005.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3437911980.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3555037192.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2411843860.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460462092.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3389842206.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema708894576.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3556688399.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3866831117.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2800080063.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3305548243.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1048337091.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema860460200.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_SchemaExt2244591795.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_SchemaExt1322857965.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_Undefined4037327541.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_Validation130261338.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Ca2702496155.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Re1785396551.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De1136768961.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Defa55730933.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De1797502741.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Def269506396.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De1062508680.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De2461993261.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De3055062677.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Err615697659.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Er3365615597.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2625589241.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js1566984540.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js3573211912.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js3196859494.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso122701872.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js3556659186.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2091736265.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2604782005.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2712067825.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js3302934105.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso795582376.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2304948129.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js3254279720.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso220200932.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso182642898.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js3234521618.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Json27144642.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2979008531.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js1473969596.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Lat712380905.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_On1208384365.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Ref879419187.h"
#include "AssemblyU2DCSharp_System_Runtime_Serialization_Dat3332255060.h"
#include "AssemblyU2DCSharp_System_Runtime_Serialization_Dat2677019114.h"
#include "AssemblyU2DCSharp_System_Runtime_Serialization_Enum187433993.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_TypeNameHandling1331513094.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Base642682770749.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Collec3679343665.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Collec2931878223.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Collect463578570.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Collec4083342187.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Conver2984810590.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (JProperty_t2956441399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2900[2] = 
{
	JProperty_t2956441399::get_offset_of__content_8(),
	JProperty_t2956441399::get_offset_of__name_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (JPropertyDescriptor_t544573776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2901[1] = 
{
	JPropertyDescriptor_t544573776::get_offset_of__propertyType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (JRaw_t3423748388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (JToken_t2552644013), -1, sizeof(JToken_t2552644013_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2903[6] = 
{
	JToken_t2552644013::get_offset_of__parent_0(),
	JToken_t2552644013::get_offset_of__previous_1(),
	JToken_t2552644013::get_offset_of__next_2(),
	JToken_t2552644013_StaticFields::get_offset_of__equalityComparer_3(),
	JToken_t2552644013::get_offset_of__lineNumber_4(),
	JToken_t2552644013::get_offset_of__linePosition_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (U3CAncestorsU3Ec__IteratorD_t445779108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2904[4] = 
{
	U3CAncestorsU3Ec__IteratorD_t445779108::get_offset_of_U3CparentU3E__0_0(),
	U3CAncestorsU3Ec__IteratorD_t445779108::get_offset_of_U24PC_1(),
	U3CAncestorsU3Ec__IteratorD_t445779108::get_offset_of_U24current_2(),
	U3CAncestorsU3Ec__IteratorD_t445779108::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (U3CAfterSelfU3Ec__IteratorE_t1950608209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2905[4] = 
{
	U3CAfterSelfU3Ec__IteratorE_t1950608209::get_offset_of_U3CoU3E__0_0(),
	U3CAfterSelfU3Ec__IteratorE_t1950608209::get_offset_of_U24PC_1(),
	U3CAfterSelfU3Ec__IteratorE_t1950608209::get_offset_of_U24current_2(),
	U3CAfterSelfU3Ec__IteratorE_t1950608209::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (U3CBeforeSelfU3Ec__IteratorF_t3432444035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2906[4] = 
{
	U3CBeforeSelfU3Ec__IteratorF_t3432444035::get_offset_of_U3CoU3E__0_0(),
	U3CBeforeSelfU3Ec__IteratorF_t3432444035::get_offset_of_U24PC_1(),
	U3CBeforeSelfU3Ec__IteratorF_t3432444035::get_offset_of_U24current_2(),
	U3CBeforeSelfU3Ec__IteratorF_t3432444035::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (JTokenEqualityComparer_t711291442), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (JTokenReader_t3330885370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[3] = 
{
	JTokenReader_t3330885370::get_offset_of__root_9(),
	JTokenReader_t3330885370::get_offset_of__parent_10(),
	JTokenReader_t3330885370::get_offset_of__current_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (JTokenType_t1307827213)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2909[19] = 
{
	JTokenType_t1307827213::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (JTokenWriter_t3631426868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2910[3] = 
{
	JTokenWriter_t3631426868::get_offset_of__token_6(),
	JTokenWriter_t3631426868::get_offset_of__parent_7(),
	JTokenWriter_t3631426868::get_offset_of__value_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (JValue_t300956845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2911[2] = 
{
	JValue_t300956845::get_offset_of__valueType_6(),
	JValue_t300956845::get_offset_of__value_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (LinqExtensions_t778605574), -1, sizeof(LinqExtensions_t778605574_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2912[1] = 
{
	LinqExtensions_t778605574_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2913[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (MemberSerialization_t687984360)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2915[3] = 
{
	MemberSerialization_t687984360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (MissingMemberHandling_t367517353)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2916[3] = 
{
	MissingMemberHandling_t367517353::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (NullValueHandling_t3618095365)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2917[3] = 
{
	NullValueHandling_t3618095365::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (ObjectCreationHandling_t3720134651)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2918[4] = 
{
	ObjectCreationHandling_t3720134651::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (AddingNewEventArgs_t1197149700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[1] = 
{
	AddingNewEventArgs_t1197149700::get_offset_of_U3CNewObjectU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (NotifyCollectionChangedAction_t2380857217)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2922[6] = 
{
	NotifyCollectionChangedAction_t2380857217::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (NotifyCollectionChangedEventArgs_t2707150522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[5] = 
{
	NotifyCollectionChangedEventArgs_t2707150522::get_offset_of_U3CActionU3Ek__BackingField_0(),
	NotifyCollectionChangedEventArgs_t2707150522::get_offset_of_U3CNewItemsU3Ek__BackingField_1(),
	NotifyCollectionChangedEventArgs_t2707150522::get_offset_of_U3CNewStartingIndexU3Ek__BackingField_2(),
	NotifyCollectionChangedEventArgs_t2707150522::get_offset_of_U3COldItemsU3Ek__BackingField_3(),
	NotifyCollectionChangedEventArgs_t2707150522::get_offset_of_U3COldStartingIndexU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (PropertyChangingEventArgs_t2999891911), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[1] = 
{
	PropertyChangingEventArgs_t2999891911::get_offset_of_U3CPropertyNameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (PreserveReferencesHandling_t3019117943)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2925[5] = 
{
	PreserveReferencesHandling_t3019117943::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (ReferenceLoopHandling_t1017855894)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2926[4] = 
{
	ReferenceLoopHandling_t1017855894::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (Required_t2961887721)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2927[4] = 
{
	Required_t2961887721::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (JsonSchema_t3772113849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2928[32] = 
{
	JsonSchema_t3772113849::get_offset_of__internalId_0(),
	JsonSchema_t3772113849::get_offset_of_U3CIdU3Ek__BackingField_1(),
	JsonSchema_t3772113849::get_offset_of_U3CTitleU3Ek__BackingField_2(),
	JsonSchema_t3772113849::get_offset_of_U3CRequiredU3Ek__BackingField_3(),
	JsonSchema_t3772113849::get_offset_of_U3CReadOnlyU3Ek__BackingField_4(),
	JsonSchema_t3772113849::get_offset_of_U3CHiddenU3Ek__BackingField_5(),
	JsonSchema_t3772113849::get_offset_of_U3CTransientU3Ek__BackingField_6(),
	JsonSchema_t3772113849::get_offset_of_U3CDescriptionU3Ek__BackingField_7(),
	JsonSchema_t3772113849::get_offset_of_U3CTypeU3Ek__BackingField_8(),
	JsonSchema_t3772113849::get_offset_of_U3CPatternU3Ek__BackingField_9(),
	JsonSchema_t3772113849::get_offset_of_U3CMinimumLengthU3Ek__BackingField_10(),
	JsonSchema_t3772113849::get_offset_of_U3CMaximumLengthU3Ek__BackingField_11(),
	JsonSchema_t3772113849::get_offset_of_U3CDivisibleByU3Ek__BackingField_12(),
	JsonSchema_t3772113849::get_offset_of_U3CMinimumU3Ek__BackingField_13(),
	JsonSchema_t3772113849::get_offset_of_U3CMaximumU3Ek__BackingField_14(),
	JsonSchema_t3772113849::get_offset_of_U3CExclusiveMinimumU3Ek__BackingField_15(),
	JsonSchema_t3772113849::get_offset_of_U3CExclusiveMaximumU3Ek__BackingField_16(),
	JsonSchema_t3772113849::get_offset_of_U3CMinimumItemsU3Ek__BackingField_17(),
	JsonSchema_t3772113849::get_offset_of_U3CMaximumItemsU3Ek__BackingField_18(),
	JsonSchema_t3772113849::get_offset_of_U3CItemsU3Ek__BackingField_19(),
	JsonSchema_t3772113849::get_offset_of_U3CPropertiesU3Ek__BackingField_20(),
	JsonSchema_t3772113849::get_offset_of_U3CAdditionalPropertiesU3Ek__BackingField_21(),
	JsonSchema_t3772113849::get_offset_of_U3CPatternPropertiesU3Ek__BackingField_22(),
	JsonSchema_t3772113849::get_offset_of_U3CAllowAdditionalPropertiesU3Ek__BackingField_23(),
	JsonSchema_t3772113849::get_offset_of_U3CRequiresU3Ek__BackingField_24(),
	JsonSchema_t3772113849::get_offset_of_U3CIdentityU3Ek__BackingField_25(),
	JsonSchema_t3772113849::get_offset_of_U3CEnumU3Ek__BackingField_26(),
	JsonSchema_t3772113849::get_offset_of_U3COptionsU3Ek__BackingField_27(),
	JsonSchema_t3772113849::get_offset_of_U3CDisallowU3Ek__BackingField_28(),
	JsonSchema_t3772113849::get_offset_of_U3CDefaultU3Ek__BackingField_29(),
	JsonSchema_t3772113849::get_offset_of_U3CExtendsU3Ek__BackingField_30(),
	JsonSchema_t3772113849::get_offset_of_U3CFormatU3Ek__BackingField_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (JsonSchemaBuilder_t1268927014), -1, sizeof(JsonSchemaBuilder_t1268927014_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2929[6] = 
{
	JsonSchemaBuilder_t1268927014::get_offset_of__reader_0(),
	JsonSchemaBuilder_t1268927014::get_offset_of__stack_1(),
	JsonSchemaBuilder_t1268927014::get_offset_of__resolver_2(),
	JsonSchemaBuilder_t1268927014::get_offset_of__currentSchema_3(),
	JsonSchemaBuilder_t1268927014_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_4(),
	JsonSchemaBuilder_t1268927014_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (U3CMapTypeU3Ec__AnonStorey1D_t3943075005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2930[1] = 
{
	U3CMapTypeU3Ec__AnonStorey1D_t3943075005::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (JsonSchemaConstants_t3437911980), -1, sizeof(JsonSchemaConstants_t3437911980_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2931[34] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	JsonSchemaConstants_t3437911980_StaticFields::get_offset_of_JsonSchemaTypeMapping_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (JsonSchemaException_t3555037192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2932[2] = 
{
	JsonSchemaException_t3555037192::get_offset_of_U3CLineNumberU3Ek__BackingField_11(),
	JsonSchemaException_t3555037192::get_offset_of_U3CLinePositionU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (JsonSchemaGenerator_t2411843860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2933[5] = 
{
	JsonSchemaGenerator_t2411843860::get_offset_of__contractResolver_0(),
	JsonSchemaGenerator_t2411843860::get_offset_of__resolver_1(),
	JsonSchemaGenerator_t2411843860::get_offset_of__stack_2(),
	JsonSchemaGenerator_t2411843860::get_offset_of__currentSchema_3(),
	JsonSchemaGenerator_t2411843860::get_offset_of_U3CUndefinedSchemaIdHandlingU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (TypeSchema_t460462092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2934[2] = 
{
	TypeSchema_t460462092::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	TypeSchema_t460462092::get_offset_of_U3CSchemaU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (U3CGenerateInternalU3Ec__AnonStorey1E_t3389842206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2935[1] = 
{
	U3CGenerateInternalU3Ec__AnonStorey1E_t3389842206::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (JsonSchemaModel_t708894576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2936[19] = 
{
	JsonSchemaModel_t708894576::get_offset_of_U3CRequiredU3Ek__BackingField_0(),
	JsonSchemaModel_t708894576::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	JsonSchemaModel_t708894576::get_offset_of_U3CMinimumLengthU3Ek__BackingField_2(),
	JsonSchemaModel_t708894576::get_offset_of_U3CMaximumLengthU3Ek__BackingField_3(),
	JsonSchemaModel_t708894576::get_offset_of_U3CDivisibleByU3Ek__BackingField_4(),
	JsonSchemaModel_t708894576::get_offset_of_U3CMinimumU3Ek__BackingField_5(),
	JsonSchemaModel_t708894576::get_offset_of_U3CMaximumU3Ek__BackingField_6(),
	JsonSchemaModel_t708894576::get_offset_of_U3CExclusiveMinimumU3Ek__BackingField_7(),
	JsonSchemaModel_t708894576::get_offset_of_U3CExclusiveMaximumU3Ek__BackingField_8(),
	JsonSchemaModel_t708894576::get_offset_of_U3CMinimumItemsU3Ek__BackingField_9(),
	JsonSchemaModel_t708894576::get_offset_of_U3CMaximumItemsU3Ek__BackingField_10(),
	JsonSchemaModel_t708894576::get_offset_of_U3CPatternsU3Ek__BackingField_11(),
	JsonSchemaModel_t708894576::get_offset_of_U3CItemsU3Ek__BackingField_12(),
	JsonSchemaModel_t708894576::get_offset_of_U3CPropertiesU3Ek__BackingField_13(),
	JsonSchemaModel_t708894576::get_offset_of_U3CPatternPropertiesU3Ek__BackingField_14(),
	JsonSchemaModel_t708894576::get_offset_of_U3CAdditionalPropertiesU3Ek__BackingField_15(),
	JsonSchemaModel_t708894576::get_offset_of_U3CAllowAdditionalPropertiesU3Ek__BackingField_16(),
	JsonSchemaModel_t708894576::get_offset_of_U3CEnumU3Ek__BackingField_17(),
	JsonSchemaModel_t708894576::get_offset_of_U3CDisallowU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (JsonSchemaModelBuilder_t3556688399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2937[3] = 
{
	JsonSchemaModelBuilder_t3556688399::get_offset_of__nodes_0(),
	JsonSchemaModelBuilder_t3556688399::get_offset_of__nodeModels_1(),
	JsonSchemaModelBuilder_t3556688399::get_offset_of__node_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (JsonSchemaNode_t3866831117), -1, sizeof(JsonSchemaNode_t3866831117_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2938[8] = 
{
	JsonSchemaNode_t3866831117::get_offset_of_U3CIdU3Ek__BackingField_0(),
	JsonSchemaNode_t3866831117::get_offset_of_U3CSchemasU3Ek__BackingField_1(),
	JsonSchemaNode_t3866831117::get_offset_of_U3CPropertiesU3Ek__BackingField_2(),
	JsonSchemaNode_t3866831117::get_offset_of_U3CPatternPropertiesU3Ek__BackingField_3(),
	JsonSchemaNode_t3866831117::get_offset_of_U3CItemsU3Ek__BackingField_4(),
	JsonSchemaNode_t3866831117::get_offset_of_U3CAdditionalPropertiesU3Ek__BackingField_5(),
	JsonSchemaNode_t3866831117_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	JsonSchemaNode_t3866831117_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (JsonSchemaNodeCollection_t2800080063), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (JsonSchemaResolver_t3305548243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2940[1] = 
{
	JsonSchemaResolver_t3305548243::get_offset_of_U3CLoadedSchemasU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (U3CGetSchemaU3Ec__AnonStorey1F_t1048337091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2941[1] = 
{
	U3CGetSchemaU3Ec__AnonStorey1F_t1048337091::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (JsonSchemaType_t1742745177)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2942[10] = 
{
	JsonSchemaType_t1742745177::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (JsonSchemaWriter_t860460200), -1, sizeof(JsonSchemaWriter_t860460200_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2943[3] = 
{
	JsonSchemaWriter_t860460200::get_offset_of__writer_0(),
	JsonSchemaWriter_t860460200::get_offset_of__resolver_1(),
	JsonSchemaWriter_t860460200_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (SchemaExtensions_t2244591795), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (U3CIsValidU3Ec__AnonStorey20_t1322857965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2945[1] = 
{
	U3CIsValidU3Ec__AnonStorey20_t1322857965::get_offset_of_valid_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (UndefinedSchemaIdHandling_t4037327541)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2946[4] = 
{
	UndefinedSchemaIdHandling_t4037327541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (ValidationEventArgs_t130261338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2947[1] = 
{
	ValidationEventArgs_t130261338::get_offset_of__ex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2948[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (CamelCasePropertyNamesContractResolver_t2702496155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (ResolverContractKey_t1785396551)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2950[2] = 
{
	ResolverContractKey_t1785396551::get_offset_of__resolverType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ResolverContractKey_t1785396551::get_offset_of__contractType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (DefaultContractResolver_t1136768961), -1, sizeof(DefaultContractResolver_t1136768961_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2951[10] = 
{
	DefaultContractResolver_t1136768961_StaticFields::get_offset_of__instance_0(),
	DefaultContractResolver_t1136768961_StaticFields::get_offset_of_BuiltInConverters_1(),
	DefaultContractResolver_t1136768961::get_offset_of__typeContractCache_2(),
	DefaultContractResolver_t1136768961::get_offset_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_3(),
	DefaultContractResolver_t1136768961::get_offset_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_4(),
	DefaultContractResolver_t1136768961_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	DefaultContractResolver_t1136768961_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	DefaultContractResolver_t1136768961_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
	DefaultContractResolver_t1136768961_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
	DefaultContractResolver_t1136768961_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (U3CCreateISerializableContractU3Ec__AnonStorey21_t55730933), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2952[1] = 
{
	U3CCreateISerializableContractU3Ec__AnonStorey21_t55730933::get_offset_of_methodCall_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (U3CCreateShouldSerializeTestU3Ec__AnonStorey22_t1797502741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2953[1] = 
{
	U3CCreateShouldSerializeTestU3Ec__AnonStorey22_t1797502741::get_offset_of_shouldSerializeCall_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (U3CSetIsSpecifiedActionsU3Ec__AnonStorey23_t269506396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2954[1] = 
{
	U3CSetIsSpecifiedActionsU3Ec__AnonStorey23_t269506396::get_offset_of_specifiedPropertyGet_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (DefaultReferenceResolver_t1062508680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2955[1] = 
{
	DefaultReferenceResolver_t1062508680::get_offset_of__referenceCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (DefaultSerializationBinder_t2461993261), -1, sizeof(DefaultSerializationBinder_t2461993261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2956[2] = 
{
	DefaultSerializationBinder_t2461993261_StaticFields::get_offset_of_Instance_0(),
	DefaultSerializationBinder_t2461993261::get_offset_of__typeCache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (TypeNameKey_t3055062677)+ sizeof (Il2CppObject), sizeof(TypeNameKey_t3055062677_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2957[2] = 
{
	TypeNameKey_t3055062677::get_offset_of_AssemblyName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TypeNameKey_t3055062677::get_offset_of_TypeName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (ErrorContext_t615697659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[4] = 
{
	ErrorContext_t615697659::get_offset_of_U3CErrorU3Ek__BackingField_0(),
	ErrorContext_t615697659::get_offset_of_U3COriginalObjectU3Ek__BackingField_1(),
	ErrorContext_t615697659::get_offset_of_U3CMemberU3Ek__BackingField_2(),
	ErrorContext_t615697659::get_offset_of_U3CHandledU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (ErrorEventArgs_t3365615597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2959[2] = 
{
	ErrorEventArgs_t3365615597::get_offset_of_U3CCurrentObjectU3Ek__BackingField_1(),
	ErrorEventArgs_t3365615597::get_offset_of_U3CErrorContextU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (JsonArrayContract_t2625589241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2963[6] = 
{
	JsonArrayContract_t2625589241::get_offset_of__isCollectionItemTypeNullableType_12(),
	JsonArrayContract_t2625589241::get_offset_of__genericCollectionDefinitionType_13(),
	JsonArrayContract_t2625589241::get_offset_of__genericWrapperType_14(),
	JsonArrayContract_t2625589241::get_offset_of__genericWrapperCreator_15(),
	JsonArrayContract_t2625589241::get_offset_of_U3CCollectionItemTypeU3Ek__BackingField_16(),
	JsonArrayContract_t2625589241::get_offset_of_U3CIsMultidimensionalArrayU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (JsonContract_t1566984540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2964[12] = 
{
	JsonContract_t1566984540::get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_0(),
	JsonContract_t1566984540::get_offset_of_U3CCreatedTypeU3Ek__BackingField_1(),
	JsonContract_t1566984540::get_offset_of_U3CIsReferenceU3Ek__BackingField_2(),
	JsonContract_t1566984540::get_offset_of_U3CConverterU3Ek__BackingField_3(),
	JsonContract_t1566984540::get_offset_of_U3CInternalConverterU3Ek__BackingField_4(),
	JsonContract_t1566984540::get_offset_of_U3COnDeserializedU3Ek__BackingField_5(),
	JsonContract_t1566984540::get_offset_of_U3COnDeserializingU3Ek__BackingField_6(),
	JsonContract_t1566984540::get_offset_of_U3COnSerializedU3Ek__BackingField_7(),
	JsonContract_t1566984540::get_offset_of_U3COnSerializingU3Ek__BackingField_8(),
	JsonContract_t1566984540::get_offset_of_U3CDefaultCreatorU3Ek__BackingField_9(),
	JsonContract_t1566984540::get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_10(),
	JsonContract_t1566984540::get_offset_of_U3COnErrorU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (JsonDictionaryContract_t3573211912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2965[7] = 
{
	JsonDictionaryContract_t3573211912::get_offset_of__isDictionaryValueTypeNullableType_12(),
	JsonDictionaryContract_t3573211912::get_offset_of__genericCollectionDefinitionType_13(),
	JsonDictionaryContract_t3573211912::get_offset_of__genericWrapperType_14(),
	JsonDictionaryContract_t3573211912::get_offset_of__genericWrapperCreator_15(),
	JsonDictionaryContract_t3573211912::get_offset_of_U3CPropertyNameResolverU3Ek__BackingField_16(),
	JsonDictionaryContract_t3573211912::get_offset_of_U3CDictionaryKeyTypeU3Ek__BackingField_17(),
	JsonDictionaryContract_t3573211912::get_offset_of_U3CDictionaryValueTypeU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (JsonFormatterConverter_t3196859494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2966[1] = 
{
	JsonFormatterConverter_t3196859494::get_offset_of__serializer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (JsonISerializableContract_t122701872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2967[1] = 
{
	JsonISerializableContract_t122701872::get_offset_of_U3CISerializableCreatorU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (JsonLinqContract_t3556659186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (JsonObjectContract_t2091736265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2969[5] = 
{
	JsonObjectContract_t2091736265::get_offset_of_U3CMemberSerializationU3Ek__BackingField_12(),
	JsonObjectContract_t2091736265::get_offset_of_U3CPropertiesU3Ek__BackingField_13(),
	JsonObjectContract_t2091736265::get_offset_of_U3CConstructorParametersU3Ek__BackingField_14(),
	JsonObjectContract_t2091736265::get_offset_of_U3COverrideConstructorU3Ek__BackingField_15(),
	JsonObjectContract_t2091736265::get_offset_of_U3CParametrizedConstructorU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (JsonPrimitiveContract_t2604782005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (JsonProperty_t2712067825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2971[21] = 
{
	JsonProperty_t2712067825::get_offset_of_U3CPropertyNameU3Ek__BackingField_0(),
	JsonProperty_t2712067825::get_offset_of_U3COrderU3Ek__BackingField_1(),
	JsonProperty_t2712067825::get_offset_of_U3CUnderlyingNameU3Ek__BackingField_2(),
	JsonProperty_t2712067825::get_offset_of_U3CValueProviderU3Ek__BackingField_3(),
	JsonProperty_t2712067825::get_offset_of_U3CPropertyTypeU3Ek__BackingField_4(),
	JsonProperty_t2712067825::get_offset_of_U3CConverterU3Ek__BackingField_5(),
	JsonProperty_t2712067825::get_offset_of_U3CMemberConverterU3Ek__BackingField_6(),
	JsonProperty_t2712067825::get_offset_of_U3CIgnoredU3Ek__BackingField_7(),
	JsonProperty_t2712067825::get_offset_of_U3CReadableU3Ek__BackingField_8(),
	JsonProperty_t2712067825::get_offset_of_U3CWritableU3Ek__BackingField_9(),
	JsonProperty_t2712067825::get_offset_of_U3CDefaultValueU3Ek__BackingField_10(),
	JsonProperty_t2712067825::get_offset_of_U3CRequiredU3Ek__BackingField_11(),
	JsonProperty_t2712067825::get_offset_of_U3CIsReferenceU3Ek__BackingField_12(),
	JsonProperty_t2712067825::get_offset_of_U3CNullValueHandlingU3Ek__BackingField_13(),
	JsonProperty_t2712067825::get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_14(),
	JsonProperty_t2712067825::get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_15(),
	JsonProperty_t2712067825::get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_16(),
	JsonProperty_t2712067825::get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_17(),
	JsonProperty_t2712067825::get_offset_of_U3CShouldSerializeU3Ek__BackingField_18(),
	JsonProperty_t2712067825::get_offset_of_U3CGetIsSpecifiedU3Ek__BackingField_19(),
	JsonProperty_t2712067825::get_offset_of_U3CSetIsSpecifiedU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (JsonPropertyCollection_t3302934105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2972[1] = 
{
	JsonPropertyCollection_t3302934105::get_offset_of__type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (JsonSerializerInternalBase_t795582376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2973[3] = 
{
	JsonSerializerInternalBase_t795582376::get_offset_of__currentErrorContext_0(),
	JsonSerializerInternalBase_t795582376::get_offset_of__mappings_1(),
	JsonSerializerInternalBase_t795582376::get_offset_of_U3CSerializerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (ReferenceEqualsEqualityComparer_t2304948129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (JsonSerializerInternalReader_t3254279720), -1, sizeof(JsonSerializerInternalReader_t3254279720_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2975[7] = 
{
	JsonSerializerInternalReader_t3254279720::get_offset_of__internalSerializer_3(),
	JsonSerializerInternalReader_t3254279720::get_offset_of__formatterConverter_4(),
	JsonSerializerInternalReader_t3254279720_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_5(),
	JsonSerializerInternalReader_t3254279720_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_6(),
	JsonSerializerInternalReader_t3254279720_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_7(),
	JsonSerializerInternalReader_t3254279720_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_8(),
	JsonSerializerInternalReader_t3254279720_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (PropertyPresence_t220200932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2976[4] = 
{
	PropertyPresence_t220200932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (U3CCreateAndPopulateListU3Ec__AnonStorey24_t182642898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2977[4] = 
{
	U3CCreateAndPopulateListU3Ec__AnonStorey24_t182642898::get_offset_of_reference_0(),
	U3CCreateAndPopulateListU3Ec__AnonStorey24_t182642898::get_offset_of_contract_1(),
	U3CCreateAndPopulateListU3Ec__AnonStorey24_t182642898::get_offset_of_reader_2(),
	U3CCreateAndPopulateListU3Ec__AnonStorey24_t182642898::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (JsonSerializerInternalWriter_t3234521618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2978[2] = 
{
	JsonSerializerInternalWriter_t3234521618::get_offset_of__internalSerializer_3(),
	JsonSerializerInternalWriter_t3234521618::get_offset_of__serializeStack_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (JsonSerializerProxy_t27144642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2979[3] = 
{
	JsonSerializerProxy_t27144642::get_offset_of__serializerReader_15(),
	JsonSerializerProxy_t27144642::get_offset_of__serializerWriter_16(),
	JsonSerializerProxy_t27144642::get_offset_of__serializer_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (JsonStringContract_t2979008531), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (JsonTypeReflector_t1473969596), -1, sizeof(JsonTypeReflector_t1473969596_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2982[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	JsonTypeReflector_t1473969596_StaticFields::get_offset_of_JsonConverterTypeCache_8(),
	JsonTypeReflector_t1473969596_StaticFields::get_offset_of_AssociatedMetadataTypesCache_9(),
	JsonTypeReflector_t1473969596_StaticFields::get_offset_of__cachedMetadataTypeAttributeType_10(),
	JsonTypeReflector_t1473969596_StaticFields::get_offset_of__dynamicCodeGeneration_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (LateBoundMetadataTypeAttribute_t712380905), -1, sizeof(LateBoundMetadataTypeAttribute_t712380905_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2983[2] = 
{
	LateBoundMetadataTypeAttribute_t712380905_StaticFields::get_offset_of__metadataClassTypeProperty_0(),
	LateBoundMetadataTypeAttribute_t712380905::get_offset_of__attribute_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (OnErrorAttribute_t1208384365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (ReflectionValueProvider_t879419187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2985[1] = 
{
	ReflectionValueProvider_t879419187::get_offset_of__memberInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (DataContractAttribute_t3332255060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2986[3] = 
{
	DataContractAttribute_t3332255060::get_offset_of_name_0(),
	DataContractAttribute_t3332255060::get_offset_of_ns_1(),
	DataContractAttribute_t3332255060::get_offset_of_U3CIsReferenceU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (DataMemberAttribute_t2677019114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2987[4] = 
{
	DataMemberAttribute_t2677019114::get_offset_of_is_required_0(),
	DataMemberAttribute_t2677019114::get_offset_of_emit_default_1(),
	DataMemberAttribute_t2677019114::get_offset_of_name_2(),
	DataMemberAttribute_t2677019114::get_offset_of_order_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (EnumMemberAttribute_t187433993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2988[1] = 
{
	EnumMemberAttribute_t187433993::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (TypeNameHandling_t1331513094)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2989[6] = 
{
	TypeNameHandling_t1331513094::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (Base64Encoder_t2682770749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2990[6] = 
{
	0,
	0,
	Base64Encoder_t2682770749::get_offset_of__charsLine_2(),
	Base64Encoder_t2682770749::get_offset_of__writer_3(),
	Base64Encoder_t2682770749::get_offset_of__leftOverBytes_4(),
	Base64Encoder_t2682770749::get_offset_of__leftOverBytesCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2991[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (CollectionUtils_t3679343665), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2993[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (U3CCreateCollectionWrapperU3Ec__AnonStorey26_t2931878223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2994[2] = 
{
	U3CCreateCollectionWrapperU3Ec__AnonStorey26_t2931878223::get_offset_of_collectionDefinition_0(),
	U3CCreateCollectionWrapperU3Ec__AnonStorey26_t2931878223::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (U3CCreateListWrapperU3Ec__AnonStorey27_t463578570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2995[2] = 
{
	U3CCreateListWrapperU3Ec__AnonStorey27_t463578570::get_offset_of_listDefinition_0(),
	U3CCreateListWrapperU3Ec__AnonStorey27_t463578570::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (U3CCreateDictionaryWrapperU3Ec__AnonStorey28_t4083342187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2996[2] = 
{
	U3CCreateDictionaryWrapperU3Ec__AnonStorey28_t4083342187::get_offset_of_dictionaryDefinition_0(),
	U3CCreateDictionaryWrapperU3Ec__AnonStorey28_t4083342187::get_offset_of_dictionary_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2998[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (ConvertUtils_t2984810590), -1, sizeof(ConvertUtils_t2984810590_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2999[1] = 
{
	ConvertUtils_t2984810590_StaticFields::get_offset_of_CastConverters_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
