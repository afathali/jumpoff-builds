﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3201915814MethodDeclarations.h"

// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2867195720(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2854776331 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m809665740_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m3826803644(__this, ___arg10, method) ((  bool (*) (Func_2_t2854776331 *, KeyValuePair_2_t381019060 , const MethodInfo*))Func_2_Invoke_m174842950_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m4250372635(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2854776331 *, KeyValuePair_2_t381019060 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2337603003_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1800245342(__this, ___result0, method) ((  bool (*) (Func_2_t2854776331 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4101312048_gshared)(__this, ___result0, method)
