﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23387117823MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1911957618(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1414869661 *, String_t*, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m3533396040_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::get_Key()
#define KeyValuePair_2_get_Key_m3624486385(__this, method) ((  String_t* (*) (KeyValuePair_2_t1414869661 *, const MethodInfo*))KeyValuePair_2_get_Key_m372290202_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2120490371(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1414869661 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m2809222207_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::get_Value()
#define KeyValuePair_2_get_Value_m1144486502(__this, method) ((  int32_t (*) (KeyValuePair_2_t1414869661 *, const MethodInfo*))KeyValuePair_2_get_Value_m3683399194_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1177194355(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1414869661 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1219699439_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>::ToString()
#define KeyValuePair_2_ToString_m2550623485(__this, method) ((  String_t* (*) (KeyValuePair_2_t1414869661 *, const MethodInfo*))KeyValuePair_2_ToString_m3225376289_gshared)(__this, method)
