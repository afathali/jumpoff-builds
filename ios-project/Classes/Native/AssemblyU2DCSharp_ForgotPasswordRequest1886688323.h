﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Shared_Request3213492751.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForgotPasswordRequest
struct  ForgotPasswordRequest_t1886688323  : public Request_t3213492751
{
public:
	// System.String ForgotPasswordRequest::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_2;
	// System.String ForgotPasswordRequest::<Email>k__BackingField
	String_t* ___U3CEmailU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ForgotPasswordRequest_t1886688323, ___U3CTypeU3Ek__BackingField_2)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTypeU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CEmailU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ForgotPasswordRequest_t1886688323, ___U3CEmailU3Ek__BackingField_3)); }
	inline String_t* get_U3CEmailU3Ek__BackingField_3() const { return ___U3CEmailU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CEmailU3Ek__BackingField_3() { return &___U3CEmailU3Ek__BackingField_3; }
	inline void set_U3CEmailU3Ek__BackingField_3(String_t* value)
	{
		___U3CEmailU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEmailU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
