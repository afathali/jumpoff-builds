﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Shared_Request3213492751.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginRequest
struct  LoginRequest_t2273172322  : public Request_t3213492751
{
public:
	// System.String LoginRequest::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_2;
	// System.String LoginRequest::<Email>k__BackingField
	String_t* ___U3CEmailU3Ek__BackingField_3;
	// System.String LoginRequest::<Password>k__BackingField
	String_t* ___U3CPasswordU3Ek__BackingField_4;
	// System.String LoginRequest::<AutoLogin>k__BackingField
	String_t* ___U3CAutoLoginU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LoginRequest_t2273172322, ___U3CTypeU3Ek__BackingField_2)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTypeU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CEmailU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LoginRequest_t2273172322, ___U3CEmailU3Ek__BackingField_3)); }
	inline String_t* get_U3CEmailU3Ek__BackingField_3() const { return ___U3CEmailU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CEmailU3Ek__BackingField_3() { return &___U3CEmailU3Ek__BackingField_3; }
	inline void set_U3CEmailU3Ek__BackingField_3(String_t* value)
	{
		___U3CEmailU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEmailU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CPasswordU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LoginRequest_t2273172322, ___U3CPasswordU3Ek__BackingField_4)); }
	inline String_t* get_U3CPasswordU3Ek__BackingField_4() const { return ___U3CPasswordU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CPasswordU3Ek__BackingField_4() { return &___U3CPasswordU3Ek__BackingField_4; }
	inline void set_U3CPasswordU3Ek__BackingField_4(String_t* value)
	{
		___U3CPasswordU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPasswordU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CAutoLoginU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LoginRequest_t2273172322, ___U3CAutoLoginU3Ek__BackingField_5)); }
	inline String_t* get_U3CAutoLoginU3Ek__BackingField_5() const { return ___U3CAutoLoginU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CAutoLoginU3Ek__BackingField_5() { return &___U3CAutoLoginU3Ek__BackingField_5; }
	inline void set_U3CAutoLoginU3Ek__BackingField_5(String_t* value)
	{
		___U3CAutoLoginU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAutoLoginU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
