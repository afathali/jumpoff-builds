﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSStoreKitResult
struct IOSStoreKitResult_t2359407583;
// System.String
struct String_t;
// SA.Common.Models.Error
struct Error_t445207774;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Erro445207774.h"
#include "AssemblyU2DCSharpU2Dfirstpass_InAppPurchaseState3414001346.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSTransactionErrorC1822631322.h"

// System.Void IOSStoreKitResult::.ctor(System.String,SA.Common.Models.Error)
extern "C"  void IOSStoreKitResult__ctor_m2270256867 (IOSStoreKitResult_t2359407583 * __this, String_t* ___productIdentifier0, Error_t445207774 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSStoreKitResult::.ctor(System.String,InAppPurchaseState,System.String,System.String,System.String)
extern "C"  void IOSStoreKitResult__ctor_m3542282502 (IOSStoreKitResult_t2359407583 * __this, String_t* ___productIdentifier0, int32_t ___state1, String_t* ___applicationUsername2, String_t* ___receipt3, String_t* ___transactionIdentifier4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IOSTransactionErrorCode IOSStoreKitResult::get_TransactionErrorCode()
extern "C"  int32_t IOSStoreKitResult_get_TransactionErrorCode_m165265547 (IOSStoreKitResult_t2359407583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InAppPurchaseState IOSStoreKitResult::get_State()
extern "C"  int32_t IOSStoreKitResult_get_State_m2548174511 (IOSStoreKitResult_t2359407583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSStoreKitResult::get_ProductIdentifier()
extern "C"  String_t* IOSStoreKitResult_get_ProductIdentifier_m2153110210 (IOSStoreKitResult_t2359407583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSStoreKitResult::get_ApplicationUsername()
extern "C"  String_t* IOSStoreKitResult_get_ApplicationUsername_m2209337088 (IOSStoreKitResult_t2359407583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSStoreKitResult::get_Receipt()
extern "C"  String_t* IOSStoreKitResult_get_Receipt_m807313434 (IOSStoreKitResult_t2359407583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSStoreKitResult::get_TransactionIdentifier()
extern "C"  String_t* IOSStoreKitResult_get_TransactionIdentifier_m4260598555 (IOSStoreKitResult_t2359407583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
