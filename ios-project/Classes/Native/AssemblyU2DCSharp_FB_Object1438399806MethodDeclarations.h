﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FB_Object
struct FB_Object_t1438399806;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FB_Object::.ctor()
extern "C"  void FB_Object__ctor_m2590345249 (FB_Object_t1438399806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_Object::SetCreatedTime(System.String)
extern "C"  void FB_Object_SetCreatedTime_m3352531622 (FB_Object_t1438399806 * __this, String_t* ___time_string0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_Object::AddImageUrl(System.String)
extern "C"  void FB_Object_AddImageUrl_m490584358 (FB_Object_t1438399806 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
