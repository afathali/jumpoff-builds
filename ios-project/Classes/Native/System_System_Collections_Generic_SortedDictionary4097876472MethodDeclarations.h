﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>
struct SortedDictionary_2_t2729314972;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Int32>
struct Node_t1311643904;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary4097876472.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1005713785_gshared (Enumerator_t4097876472 * __this, SortedDictionary_2_t2729314972 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m1005713785(__this, ___dic0, method) ((  void (*) (Enumerator_t4097876472 *, SortedDictionary_2_t2729314972 *, const MethodInfo*))Enumerator__ctor_m1005713785_gshared)(__this, ___dic0, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2123593350_gshared (Enumerator_t4097876472 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2123593350(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t4097876472 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2123593350_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2967233137_gshared (Enumerator_t4097876472 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2967233137(__this, method) ((  Il2CppObject * (*) (Enumerator_t4097876472 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2967233137_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3942140953_gshared (Enumerator_t4097876472 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3942140953(__this, method) ((  Il2CppObject * (*) (Enumerator_t4097876472 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3942140953_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m327078147_gshared (Enumerator_t4097876472 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m327078147(__this, method) ((  Il2CppObject * (*) (Enumerator_t4097876472 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m327078147_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m532613647_gshared (Enumerator_t4097876472 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m532613647(__this, method) ((  void (*) (Enumerator_t4097876472 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m532613647_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t3132015601  Enumerator_get_Current_m3565051023_gshared (Enumerator_t4097876472 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3565051023(__this, method) ((  KeyValuePair_2_t3132015601  (*) (Enumerator_t4097876472 *, const MethodInfo*))Enumerator_get_Current_m3565051023_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m675340678_gshared (Enumerator_t4097876472 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m675340678(__this, method) ((  bool (*) (Enumerator_t4097876472 *, const MethodInfo*))Enumerator_MoveNext_m675340678_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m4272760800_gshared (Enumerator_t4097876472 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4272760800(__this, method) ((  void (*) (Enumerator_t4097876472 *, const MethodInfo*))Enumerator_Dispose_m4272760800_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/Node<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentNode()
extern "C"  Node_t1311643904 * Enumerator_get_CurrentNode_m60562601_gshared (Enumerator_t4097876472 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentNode_m60562601(__this, method) ((  Node_t1311643904 * (*) (Enumerator_t4097876472 *, const MethodInfo*))Enumerator_get_CurrentNode_m60562601_gshared)(__this, method)
