﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_RTM_Room
struct GP_RTM_Room_t851604529;
// GP_Participant
struct GP_Participant_t2884377673;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GP_Participant2884377673.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_RTM_Room::.ctor()
extern "C"  void GP_RTM_Room__ctor_m1709476928 (GP_RTM_Room_t851604529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP_RTM_Room::AddParticipant(GP_Participant)
extern "C"  void GP_RTM_Room_AddParticipant_m3509556045 (GP_RTM_Room_t851604529 * __this, GP_Participant_t2884377673 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_Participant GP_RTM_Room::GetParticipantById(System.String)
extern "C"  GP_Participant_t2884377673 * GP_RTM_Room_GetParticipantById_m905851117 (GP_RTM_Room_t851604529 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
