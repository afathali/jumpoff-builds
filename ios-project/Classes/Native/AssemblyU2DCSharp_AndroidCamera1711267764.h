﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<AndroidImagePickResult>
struct Action_1_t1593532934;
// System.Action`1<GallerySaveResult>
struct Action_1_t1445656332;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3002111682.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidCamera
struct  AndroidCamera_t1711267764  : public SA_Singleton_OLD_1_t3002111682
{
public:
	// System.Action`1<AndroidImagePickResult> AndroidCamera::OnImagePicked
	Action_1_t1593532934 * ___OnImagePicked_4;
	// System.Action`1<GallerySaveResult> AndroidCamera::OnImageSaved
	Action_1_t1445656332 * ___OnImageSaved_5;

public:
	inline static int32_t get_offset_of_OnImagePicked_4() { return static_cast<int32_t>(offsetof(AndroidCamera_t1711267764, ___OnImagePicked_4)); }
	inline Action_1_t1593532934 * get_OnImagePicked_4() const { return ___OnImagePicked_4; }
	inline Action_1_t1593532934 ** get_address_of_OnImagePicked_4() { return &___OnImagePicked_4; }
	inline void set_OnImagePicked_4(Action_1_t1593532934 * value)
	{
		___OnImagePicked_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnImagePicked_4, value);
	}

	inline static int32_t get_offset_of_OnImageSaved_5() { return static_cast<int32_t>(offsetof(AndroidCamera_t1711267764, ___OnImageSaved_5)); }
	inline Action_1_t1445656332 * get_OnImageSaved_5() const { return ___OnImageSaved_5; }
	inline Action_1_t1445656332 ** get_address_of_OnImageSaved_5() { return &___OnImageSaved_5; }
	inline void set_OnImageSaved_5(Action_1_t1445656332 * value)
	{
		___OnImageSaved_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnImageSaved_5, value);
	}
};

struct AndroidCamera_t1711267764_StaticFields
{
public:
	// System.String AndroidCamera::_lastImageName
	String_t* ____lastImageName_6;
	// System.Action`1<AndroidImagePickResult> AndroidCamera::<>f__am$cache3
	Action_1_t1593532934 * ___U3CU3Ef__amU24cache3_7;
	// System.Action`1<GallerySaveResult> AndroidCamera::<>f__am$cache4
	Action_1_t1445656332 * ___U3CU3Ef__amU24cache4_8;

public:
	inline static int32_t get_offset_of__lastImageName_6() { return static_cast<int32_t>(offsetof(AndroidCamera_t1711267764_StaticFields, ____lastImageName_6)); }
	inline String_t* get__lastImageName_6() const { return ____lastImageName_6; }
	inline String_t** get_address_of__lastImageName_6() { return &____lastImageName_6; }
	inline void set__lastImageName_6(String_t* value)
	{
		____lastImageName_6 = value;
		Il2CppCodeGenWriteBarrier(&____lastImageName_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(AndroidCamera_t1711267764_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline Action_1_t1593532934 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline Action_1_t1593532934 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(Action_1_t1593532934 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(AndroidCamera_t1711267764_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline Action_1_t1445656332 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline Action_1_t1445656332 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(Action_1_t1445656332 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
