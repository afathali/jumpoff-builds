﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_GMSInvitationProxy
struct AN_GMSInvitationProxy_t2905962638;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_GMSInvitationProxy::.ctor()
extern "C"  void AN_GMSInvitationProxy__ctor_m3531854727 (AN_GMSInvitationProxy_t2905962638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSInvitationProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_GMSInvitationProxy_CallActivityFunction_m2222255640 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSInvitationProxy::registerInvitationListener()
extern "C"  void AN_GMSInvitationProxy_registerInvitationListener_m1832717913 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSInvitationProxy::unregisterInvitationListener()
extern "C"  void AN_GMSInvitationProxy_unregisterInvitationListener_m708963472 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_GMSInvitationProxy::LoadInvitations()
extern "C"  void AN_GMSInvitationProxy_LoadInvitations_m4138160007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
