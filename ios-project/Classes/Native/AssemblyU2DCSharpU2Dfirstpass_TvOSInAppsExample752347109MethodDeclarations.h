﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TvOSInAppsExample
struct TvOSInAppsExample_t752347109;

#include "codegen/il2cpp-codegen.h"

// System.Void TvOSInAppsExample::.ctor()
extern "C"  void TvOSInAppsExample__ctor_m1961606928 (TvOSInAppsExample_t752347109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TvOSInAppsExample::Init()
extern "C"  void TvOSInAppsExample_Init_m403571852 (TvOSInAppsExample_t752347109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TvOSInAppsExample::Buy()
extern "C"  void TvOSInAppsExample_Buy_m1752436844 (TvOSInAppsExample_t752347109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
