﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainPage_Controller
struct MainPage_Controller_t1135564517;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// LoginResponse
struct LoginResponse_t1319408382;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LoginResponse1319408382.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"

// System.Void MainPage_Controller::.ctor()
extern "C"  void MainPage_Controller__ctor_m3983267122 (MainPage_Controller_t1135564517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::Start()
extern "C"  void MainPage_Controller_Start_m2660204394 (MainPage_Controller_t1135564517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MainPage_Controller::GetBannerImage()
extern "C"  Il2CppObject * MainPage_Controller_GetBannerImage_m2505671159 (MainPage_Controller_t1135564517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MainPage_Controller::GetBannerUrl()
extern "C"  Il2CppObject * MainPage_Controller_GetBannerUrl_m3080031179 (MainPage_Controller_t1135564517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::Logout()
extern "C"  void MainPage_Controller_Logout_m2490826298 (MainPage_Controller_t1135564517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::OnDestroy()
extern "C"  void MainPage_Controller_OnDestroy_m3379324155 (MainPage_Controller_t1135564517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::HandleLoginResponse(System.Boolean,LoginResponse)
extern "C"  void MainPage_Controller_HandleLoginResponse_m2057042749 (MainPage_Controller_t1135564517 * __this, bool ___success0, LoginResponse_t1319408382 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::ViewCourses()
extern "C"  void MainPage_Controller_ViewCourses_m1764177307 (MainPage_Controller_t1135564517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::<Start>m__14E()
extern "C"  void MainPage_Controller_U3CStartU3Em__14E_m3772071327 (MainPage_Controller_t1135564517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::<Start>m__14F()
extern "C"  void MainPage_Controller_U3CStartU3Em__14F_m740659994 (MainPage_Controller_t1135564517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::<Start>m__150()
extern "C"  void MainPage_Controller_U3CStartU3Em__150_m996392039 (MainPage_Controller_t1135564517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::<Start>m__151()
extern "C"  void MainPage_Controller_U3CStartU3Em__151_m855229538 (MainPage_Controller_t1135564517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::<Start>m__152(UnityEngine.EventSystems.BaseEventData)
extern "C"  void MainPage_Controller_U3CStartU3Em__152_m1388876891 (Il2CppObject * __this /* static, unused */, BaseEventData_t2681005625 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::<Start>m__153(UnityEngine.EventSystems.BaseEventData)
extern "C"  void MainPage_Controller_U3CStartU3Em__153_m2098253440 (Il2CppObject * __this /* static, unused */, BaseEventData_t2681005625 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::<Start>m__154()
extern "C"  void MainPage_Controller_U3CStartU3Em__154_m431742035 (MainPage_Controller_t1135564517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::<Start>m__155()
extern "C"  void MainPage_Controller_U3CStartU3Em__155_m290579534 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainPage_Controller::<Start>m__156()
extern "C"  void MainPage_Controller_U3CStartU3Em__156_m149417033 (MainPage_Controller_t1135564517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
