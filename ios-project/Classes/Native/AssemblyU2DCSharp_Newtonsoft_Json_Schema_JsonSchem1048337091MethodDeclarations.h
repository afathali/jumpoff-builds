﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaResolver/<GetSchema>c__AnonStorey1F
struct U3CGetSchemaU3Ec__AnonStorey1F_t1048337091;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t3772113849;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3772113849.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaResolver/<GetSchema>c__AnonStorey1F::.ctor()
extern "C"  void U3CGetSchemaU3Ec__AnonStorey1F__ctor_m3600499902 (U3CGetSchemaU3Ec__AnonStorey1F_t1048337091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaResolver/<GetSchema>c__AnonStorey1F::<>m__D5(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  bool U3CGetSchemaU3Ec__AnonStorey1F_U3CU3Em__D5_m2569183537 (U3CGetSchemaU3Ec__AnonStorey1F_t1048337091 * __this, JsonSchema_t3772113849 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
