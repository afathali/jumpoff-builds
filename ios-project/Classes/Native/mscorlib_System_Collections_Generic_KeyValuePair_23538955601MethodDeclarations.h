﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m826951578(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3538955601 *, String_t*, JsonSchemaNode_t3866831117 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::get_Key()
#define KeyValuePair_2_get_Key_m1041672805(__this, method) ((  String_t* (*) (KeyValuePair_2_t3538955601 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1567585587(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3538955601 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::get_Value()
#define KeyValuePair_2_get_Value_m3770689410(__this, method) ((  JsonSchemaNode_t3866831117 * (*) (KeyValuePair_2_t3538955601 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1218371155(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3538955601 *, JsonSchemaNode_t3866831117 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::ToString()
#define KeyValuePair_2_ToString_m2658679713(__this, method) ((  String_t* (*) (KeyValuePair_2_t3538955601 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
