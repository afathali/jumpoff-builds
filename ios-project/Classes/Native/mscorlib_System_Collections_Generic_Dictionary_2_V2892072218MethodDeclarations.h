﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va400334773MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2709569801(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2892072218 *, Dictionary_2_t4189012375 *, const MethodInfo*))ValueCollection__ctor_m882866357_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1066051231(__this, ___item0, method) ((  void (*) (ValueCollection_t2892072218 *, AndroidADBanner_t886219444 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1903672223_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4012842808(__this, method) ((  void (*) (ValueCollection_t2892072218 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3271993638_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2743085017(__this, ___item0, method) ((  bool (*) (ValueCollection_t2892072218 *, AndroidADBanner_t886219444 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3958350925_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2580838338(__this, ___item0, method) ((  bool (*) (ValueCollection_t2892072218 *, AndroidADBanner_t886219444 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m98888100_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1511952302(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2892072218 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1604400448_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3593614012(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2892072218 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2627730402_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3836404223(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2892072218 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1073215119_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m745626206(__this, method) ((  bool (*) (ValueCollection_t2892072218 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1325719984_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1765953424(__this, method) ((  bool (*) (ValueCollection_t2892072218 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4041633470_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m851903622(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2892072218 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3927965720_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3160696660(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2892072218 *, AndroidADBannerU5BU5D_t250550269*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1460341186_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2409059209(__this, method) ((  Enumerator_t1580577843  (*) (ValueCollection_t2892072218 *, const MethodInfo*))ValueCollection_GetEnumerator_m941805197_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,AndroidADBanner>::get_Count()
#define ValueCollection_get_Count_m218245592(__this, method) ((  int32_t (*) (ValueCollection_t2892072218 *, const MethodInfo*))ValueCollection_get_Count_m90930038_gshared)(__this, method)
