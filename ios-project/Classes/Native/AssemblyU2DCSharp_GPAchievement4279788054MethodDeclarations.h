﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPAchievement
struct GPAchievement_t4279788054;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GPAchievementType785407574.h"
#include "AssemblyU2DCSharp_GPAchievementState819112501.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void GPAchievement::.ctor(System.String,System.String)
extern "C"  void GPAchievement__ctor_m2516332219 (GPAchievement_t4279788054 * __this, String_t* ___id0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPAchievement::.ctor(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void GPAchievement__ctor_m1299866961 (GPAchievement_t4279788054 * __this, String_t* ___aId0, String_t* ___aName1, String_t* ___aDescr2, String_t* ___aCurentSteps3, String_t* ___aTotalSteps4, String_t* ___aState5, String_t* ___aType6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPAchievement::get_id()
extern "C"  String_t* GPAchievement_get_id_m2663740710 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPAchievement::get_Id()
extern "C"  String_t* GPAchievement_get_Id_m1440849030 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPAchievement::set_Id(System.String)
extern "C"  void GPAchievement_set_Id_m1996613593 (GPAchievement_t4279788054 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPAchievement::get_name()
extern "C"  String_t* GPAchievement_get_name_m3689077752 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPAchievement::get_Name()
extern "C"  String_t* GPAchievement_get_Name_m2321095448 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPAchievement::set_Name(System.String)
extern "C"  void GPAchievement_set_Name_m2899377911 (GPAchievement_t4279788054 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPAchievement::get_description()
extern "C"  String_t* GPAchievement_get_description_m797465721 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPAchievement::get_Description()
extern "C"  String_t* GPAchievement_get_Description_m548032921 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPAchievement::set_Description(System.String)
extern "C"  void GPAchievement_set_Description_m2010946078 (GPAchievement_t4279788054 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GPAchievement::get_currentSteps()
extern "C"  int32_t GPAchievement_get_currentSteps_m1803897482 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GPAchievement::get_CurrentSteps()
extern "C"  int32_t GPAchievement_get_CurrentSteps_m2054376618 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GPAchievement::get_totalSteps()
extern "C"  int32_t GPAchievement_get_totalSteps_m297430519 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GPAchievement::get_TotalSteps()
extern "C"  int32_t GPAchievement_get_TotalSteps_m1476096599 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPAchievementType GPAchievement::get_type()
extern "C"  int32_t GPAchievement_get_type_m3446911993 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPAchievementType GPAchievement::get_Type()
extern "C"  int32_t GPAchievement_get_Type_m1924645465 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPAchievementState GPAchievement::get_state()
extern "C"  int32_t GPAchievement_get_state_m1698438693 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPAchievementState GPAchievement::get_State()
extern "C"  int32_t GPAchievement_get_State_m1697364805 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GPAchievement::get_Texture()
extern "C"  Texture2D_t3542995729 * GPAchievement_get_Texture_m339453736 (GPAchievement_t4279788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPAchievement::set_Texture(UnityEngine.Texture2D)
extern "C"  void GPAchievement_set_Texture_m3260211727 (GPAchievement_t4279788054 * __this, Texture2D_t3542995729 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
