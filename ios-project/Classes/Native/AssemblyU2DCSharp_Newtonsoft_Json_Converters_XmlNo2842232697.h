﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Func`2<Newtonsoft.Json.Converters.IXmlNode,System.Boolean>
struct Func_2_t559709029;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter1964060750.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlNodeConverter
struct  XmlNodeConverter_t2842232697  : public JsonConverter_t1964060750
{
public:
	// System.String Newtonsoft.Json.Converters.XmlNodeConverter::<DeserializeRootElementName>k__BackingField
	String_t* ___U3CDeserializeRootElementNameU3Ek__BackingField_7;
	// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<WriteArrayAttribute>k__BackingField
	bool ___U3CWriteArrayAttributeU3Ek__BackingField_8;
	// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<OmitRootObject>k__BackingField
	bool ___U3COmitRootObjectU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CDeserializeRootElementNameU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t2842232697, ___U3CDeserializeRootElementNameU3Ek__BackingField_7)); }
	inline String_t* get_U3CDeserializeRootElementNameU3Ek__BackingField_7() const { return ___U3CDeserializeRootElementNameU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CDeserializeRootElementNameU3Ek__BackingField_7() { return &___U3CDeserializeRootElementNameU3Ek__BackingField_7; }
	inline void set_U3CDeserializeRootElementNameU3Ek__BackingField_7(String_t* value)
	{
		___U3CDeserializeRootElementNameU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDeserializeRootElementNameU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CWriteArrayAttributeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t2842232697, ___U3CWriteArrayAttributeU3Ek__BackingField_8)); }
	inline bool get_U3CWriteArrayAttributeU3Ek__BackingField_8() const { return ___U3CWriteArrayAttributeU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CWriteArrayAttributeU3Ek__BackingField_8() { return &___U3CWriteArrayAttributeU3Ek__BackingField_8; }
	inline void set_U3CWriteArrayAttributeU3Ek__BackingField_8(bool value)
	{
		___U3CWriteArrayAttributeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3COmitRootObjectU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t2842232697, ___U3COmitRootObjectU3Ek__BackingField_9)); }
	inline bool get_U3COmitRootObjectU3Ek__BackingField_9() const { return ___U3COmitRootObjectU3Ek__BackingField_9; }
	inline bool* get_address_of_U3COmitRootObjectU3Ek__BackingField_9() { return &___U3COmitRootObjectU3Ek__BackingField_9; }
	inline void set_U3COmitRootObjectU3Ek__BackingField_9(bool value)
	{
		___U3COmitRootObjectU3Ek__BackingField_9 = value;
	}
};

struct XmlNodeConverter_t2842232697_StaticFields
{
public:
	// System.Func`2<Newtonsoft.Json.Converters.IXmlNode,System.Boolean> Newtonsoft.Json.Converters.XmlNodeConverter::<>f__am$cache3
	Func_2_t559709029 * ___U3CU3Ef__amU24cache3_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Newtonsoft.Json.Converters.XmlNodeConverter::<>f__switch$map4
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map4_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Newtonsoft.Json.Converters.XmlNodeConverter::<>f__switch$map5
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map5_12;
	// System.Func`2<Newtonsoft.Json.Converters.IXmlNode,System.Boolean> Newtonsoft.Json.Converters.XmlNodeConverter::<>f__am$cache6
	Func_2_t559709029 * ___U3CU3Ef__amU24cache6_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_10() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t2842232697_StaticFields, ___U3CU3Ef__amU24cache3_10)); }
	inline Func_2_t559709029 * get_U3CU3Ef__amU24cache3_10() const { return ___U3CU3Ef__amU24cache3_10; }
	inline Func_2_t559709029 ** get_address_of_U3CU3Ef__amU24cache3_10() { return &___U3CU3Ef__amU24cache3_10; }
	inline void set_U3CU3Ef__amU24cache3_10(Func_2_t559709029 * value)
	{
		___U3CU3Ef__amU24cache3_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_11() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t2842232697_StaticFields, ___U3CU3Ef__switchU24map4_11)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map4_11() const { return ___U3CU3Ef__switchU24map4_11; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map4_11() { return &___U3CU3Ef__switchU24map4_11; }
	inline void set_U3CU3Ef__switchU24map4_11(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map4_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map4_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_12() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t2842232697_StaticFields, ___U3CU3Ef__switchU24map5_12)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map5_12() const { return ___U3CU3Ef__switchU24map5_12; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map5_12() { return &___U3CU3Ef__switchU24map5_12; }
	inline void set_U3CU3Ef__switchU24map5_12(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map5_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map5_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_13() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t2842232697_StaticFields, ___U3CU3Ef__amU24cache6_13)); }
	inline Func_2_t559709029 * get_U3CU3Ef__amU24cache6_13() const { return ___U3CU3Ef__amU24cache6_13; }
	inline Func_2_t559709029 ** get_address_of_U3CU3Ef__amU24cache6_13() { return &___U3CU3Ef__amU24cache6_13; }
	inline void set_U3CU3Ef__amU24cache6_13(Func_2_t559709029 * value)
	{
		___U3CU3Ef__amU24cache6_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
