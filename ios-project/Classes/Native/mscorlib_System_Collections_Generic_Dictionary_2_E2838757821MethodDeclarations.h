﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3457670512(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2838757821 *, Dictionary_2_t1518733119 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2601408623(__this, method) ((  Il2CppObject * (*) (Enumerator_t2838757821 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1319417571(__this, method) ((  void (*) (Enumerator_t2838757821 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m937254286(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2838757821 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1060875757(__this, method) ((  Il2CppObject * (*) (Enumerator_t2838757821 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1492693165(__this, method) ((  Il2CppObject * (*) (Enumerator_t2838757821 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::MoveNext()
#define Enumerator_MoveNext_m3636706791(__this, method) ((  bool (*) (Enumerator_t2838757821 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::get_Current()
#define Enumerator_get_Current_m599463631(__this, method) ((  KeyValuePair_2_t3571045637  (*) (Enumerator_t2838757821 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1907579160(__this, method) ((  Type_t * (*) (Enumerator_t2838757821 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m47958104(__this, method) ((  List_1_t3876342518 * (*) (Enumerator_t2838757821 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::Reset()
#define Enumerator_Reset_m310292714(__this, method) ((  void (*) (Enumerator_t2838757821 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::VerifyState()
#define Enumerator_VerifyState_m1168468041(__this, method) ((  void (*) (Enumerator_t2838757821 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m474343123(__this, method) ((  void (*) (Enumerator_t2838757821 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<MCGBehaviour/InjectionContainer/TypeData>>::Dispose()
#define Enumerator_Dispose_m3332869092(__this, method) ((  void (*) (Enumerator_t2838757821 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
