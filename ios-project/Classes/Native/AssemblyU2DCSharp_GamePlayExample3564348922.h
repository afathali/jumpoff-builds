﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// DefaultPreviewButton[]
struct DefaultPreviewButtonU5BU5D_t3800739864;
// SA_Label
struct SA_Label_t226960149;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GamePlayExample
struct  GamePlayExample_t3564348922  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] GamePlayExample::objectToEnbaleOnInit
	GameObjectU5BU5D_t3057952154* ___objectToEnbaleOnInit_2;
	// UnityEngine.GameObject[] GamePlayExample::objectToDisableOnInit
	GameObjectU5BU5D_t3057952154* ___objectToDisableOnInit_3;
	// DefaultPreviewButton[] GamePlayExample::initBoundButtons
	DefaultPreviewButtonU5BU5D_t3800739864* ___initBoundButtons_4;
	// SA_Label GamePlayExample::coinsLable
	SA_Label_t226960149 * ___coinsLable_5;
	// SA_Label GamePlayExample::boostLabel
	SA_Label_t226960149 * ___boostLabel_6;

public:
	inline static int32_t get_offset_of_objectToEnbaleOnInit_2() { return static_cast<int32_t>(offsetof(GamePlayExample_t3564348922, ___objectToEnbaleOnInit_2)); }
	inline GameObjectU5BU5D_t3057952154* get_objectToEnbaleOnInit_2() const { return ___objectToEnbaleOnInit_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_objectToEnbaleOnInit_2() { return &___objectToEnbaleOnInit_2; }
	inline void set_objectToEnbaleOnInit_2(GameObjectU5BU5D_t3057952154* value)
	{
		___objectToEnbaleOnInit_2 = value;
		Il2CppCodeGenWriteBarrier(&___objectToEnbaleOnInit_2, value);
	}

	inline static int32_t get_offset_of_objectToDisableOnInit_3() { return static_cast<int32_t>(offsetof(GamePlayExample_t3564348922, ___objectToDisableOnInit_3)); }
	inline GameObjectU5BU5D_t3057952154* get_objectToDisableOnInit_3() const { return ___objectToDisableOnInit_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_objectToDisableOnInit_3() { return &___objectToDisableOnInit_3; }
	inline void set_objectToDisableOnInit_3(GameObjectU5BU5D_t3057952154* value)
	{
		___objectToDisableOnInit_3 = value;
		Il2CppCodeGenWriteBarrier(&___objectToDisableOnInit_3, value);
	}

	inline static int32_t get_offset_of_initBoundButtons_4() { return static_cast<int32_t>(offsetof(GamePlayExample_t3564348922, ___initBoundButtons_4)); }
	inline DefaultPreviewButtonU5BU5D_t3800739864* get_initBoundButtons_4() const { return ___initBoundButtons_4; }
	inline DefaultPreviewButtonU5BU5D_t3800739864** get_address_of_initBoundButtons_4() { return &___initBoundButtons_4; }
	inline void set_initBoundButtons_4(DefaultPreviewButtonU5BU5D_t3800739864* value)
	{
		___initBoundButtons_4 = value;
		Il2CppCodeGenWriteBarrier(&___initBoundButtons_4, value);
	}

	inline static int32_t get_offset_of_coinsLable_5() { return static_cast<int32_t>(offsetof(GamePlayExample_t3564348922, ___coinsLable_5)); }
	inline SA_Label_t226960149 * get_coinsLable_5() const { return ___coinsLable_5; }
	inline SA_Label_t226960149 ** get_address_of_coinsLable_5() { return &___coinsLable_5; }
	inline void set_coinsLable_5(SA_Label_t226960149 * value)
	{
		___coinsLable_5 = value;
		Il2CppCodeGenWriteBarrier(&___coinsLable_5, value);
	}

	inline static int32_t get_offset_of_boostLabel_6() { return static_cast<int32_t>(offsetof(GamePlayExample_t3564348922, ___boostLabel_6)); }
	inline SA_Label_t226960149 * get_boostLabel_6() const { return ___boostLabel_6; }
	inline SA_Label_t226960149 ** get_address_of_boostLabel_6() { return &___boostLabel_6; }
	inline void set_boostLabel_6(SA_Label_t226960149 * value)
	{
		___boostLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___boostLabel_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
