﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TW_OAuthAPIRequest/<Request>c__IteratorA
struct U3CRequestU3Ec__IteratorA_t1902108118;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TW_OAuthAPIRequest/<Request>c__IteratorA::.ctor()
extern "C"  void U3CRequestU3Ec__IteratorA__ctor_m1619192785 (U3CRequestU3Ec__IteratorA_t1902108118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TW_OAuthAPIRequest/<Request>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRequestU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1533873599 (U3CRequestU3Ec__IteratorA_t1902108118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TW_OAuthAPIRequest/<Request>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRequestU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3774115271 (U3CRequestU3Ec__IteratorA_t1902108118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TW_OAuthAPIRequest/<Request>c__IteratorA::MoveNext()
extern "C"  bool U3CRequestU3Ec__IteratorA_MoveNext_m1203766479 (U3CRequestU3Ec__IteratorA_t1902108118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_OAuthAPIRequest/<Request>c__IteratorA::Dispose()
extern "C"  void U3CRequestU3Ec__IteratorA_Dispose_m1839714976 (U3CRequestU3Ec__IteratorA_t1902108118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TW_OAuthAPIRequest/<Request>c__IteratorA::Reset()
extern "C"  void U3CRequestU3Ec__IteratorA_Reset_m3953181610 (U3CRequestU3Ec__IteratorA_t1902108118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
