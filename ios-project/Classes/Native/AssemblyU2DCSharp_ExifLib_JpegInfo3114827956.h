﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Double[]
struct DoubleU5BU5D_t1889952540;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ExifLib_ExifOrientation1205752066.h"
#include "AssemblyU2DCSharp_ExifLib_ExifUnit2084677226.h"
#include "AssemblyU2DCSharp_ExifLib_ExifFlash1670535696.h"
#include "AssemblyU2DCSharp_ExifLib_ExifGpsLatitudeRef1514933379.h"
#include "AssemblyU2DCSharp_ExifLib_ExifGpsLongitudeRef1142847722.h"
#include "mscorlib_System_TimeSpan3430258949.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExifLib.JpegInfo
struct  JpegInfo_t3114827956  : public Il2CppObject
{
public:
	// System.String ExifLib.JpegInfo::FileName
	String_t* ___FileName_0;
	// System.Int32 ExifLib.JpegInfo::FileSize
	int32_t ___FileSize_1;
	// System.Boolean ExifLib.JpegInfo::IsValid
	bool ___IsValid_2;
	// System.Int32 ExifLib.JpegInfo::Width
	int32_t ___Width_3;
	// System.Int32 ExifLib.JpegInfo::Height
	int32_t ___Height_4;
	// System.Boolean ExifLib.JpegInfo::IsColor
	bool ___IsColor_5;
	// ExifLib.ExifOrientation ExifLib.JpegInfo::Orientation
	int32_t ___Orientation_6;
	// System.Double ExifLib.JpegInfo::XResolution
	double ___XResolution_7;
	// System.Double ExifLib.JpegInfo::YResolution
	double ___YResolution_8;
	// ExifLib.ExifUnit ExifLib.JpegInfo::ResolutionUnit
	int32_t ___ResolutionUnit_9;
	// System.String ExifLib.JpegInfo::DateTime
	String_t* ___DateTime_10;
	// System.String ExifLib.JpegInfo::Description
	String_t* ___Description_11;
	// System.String ExifLib.JpegInfo::Make
	String_t* ___Make_12;
	// System.String ExifLib.JpegInfo::Model
	String_t* ___Model_13;
	// System.String ExifLib.JpegInfo::Software
	String_t* ___Software_14;
	// System.String ExifLib.JpegInfo::Artist
	String_t* ___Artist_15;
	// System.String ExifLib.JpegInfo::Copyright
	String_t* ___Copyright_16;
	// System.String ExifLib.JpegInfo::UserComment
	String_t* ___UserComment_17;
	// System.Double ExifLib.JpegInfo::ExposureTime
	double ___ExposureTime_18;
	// System.Double ExifLib.JpegInfo::FNumber
	double ___FNumber_19;
	// ExifLib.ExifFlash ExifLib.JpegInfo::Flash
	int32_t ___Flash_20;
	// ExifLib.ExifGpsLatitudeRef ExifLib.JpegInfo::GpsLatitudeRef
	int32_t ___GpsLatitudeRef_21;
	// System.Double[] ExifLib.JpegInfo::GpsLatitude
	DoubleU5BU5D_t1889952540* ___GpsLatitude_22;
	// ExifLib.ExifGpsLongitudeRef ExifLib.JpegInfo::GpsLongitudeRef
	int32_t ___GpsLongitudeRef_23;
	// System.Double[] ExifLib.JpegInfo::GpsLongitude
	DoubleU5BU5D_t1889952540* ___GpsLongitude_24;
	// System.Int32 ExifLib.JpegInfo::ThumbnailOffset
	int32_t ___ThumbnailOffset_25;
	// System.Int32 ExifLib.JpegInfo::ThumbnailSize
	int32_t ___ThumbnailSize_26;
	// System.Byte[] ExifLib.JpegInfo::ThumbnailData
	ByteU5BU5D_t3397334013* ___ThumbnailData_27;
	// System.TimeSpan ExifLib.JpegInfo::LoadTime
	TimeSpan_t3430258949  ___LoadTime_28;

public:
	inline static int32_t get_offset_of_FileName_0() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___FileName_0)); }
	inline String_t* get_FileName_0() const { return ___FileName_0; }
	inline String_t** get_address_of_FileName_0() { return &___FileName_0; }
	inline void set_FileName_0(String_t* value)
	{
		___FileName_0 = value;
		Il2CppCodeGenWriteBarrier(&___FileName_0, value);
	}

	inline static int32_t get_offset_of_FileSize_1() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___FileSize_1)); }
	inline int32_t get_FileSize_1() const { return ___FileSize_1; }
	inline int32_t* get_address_of_FileSize_1() { return &___FileSize_1; }
	inline void set_FileSize_1(int32_t value)
	{
		___FileSize_1 = value;
	}

	inline static int32_t get_offset_of_IsValid_2() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___IsValid_2)); }
	inline bool get_IsValid_2() const { return ___IsValid_2; }
	inline bool* get_address_of_IsValid_2() { return &___IsValid_2; }
	inline void set_IsValid_2(bool value)
	{
		___IsValid_2 = value;
	}

	inline static int32_t get_offset_of_Width_3() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___Width_3)); }
	inline int32_t get_Width_3() const { return ___Width_3; }
	inline int32_t* get_address_of_Width_3() { return &___Width_3; }
	inline void set_Width_3(int32_t value)
	{
		___Width_3 = value;
	}

	inline static int32_t get_offset_of_Height_4() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___Height_4)); }
	inline int32_t get_Height_4() const { return ___Height_4; }
	inline int32_t* get_address_of_Height_4() { return &___Height_4; }
	inline void set_Height_4(int32_t value)
	{
		___Height_4 = value;
	}

	inline static int32_t get_offset_of_IsColor_5() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___IsColor_5)); }
	inline bool get_IsColor_5() const { return ___IsColor_5; }
	inline bool* get_address_of_IsColor_5() { return &___IsColor_5; }
	inline void set_IsColor_5(bool value)
	{
		___IsColor_5 = value;
	}

	inline static int32_t get_offset_of_Orientation_6() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___Orientation_6)); }
	inline int32_t get_Orientation_6() const { return ___Orientation_6; }
	inline int32_t* get_address_of_Orientation_6() { return &___Orientation_6; }
	inline void set_Orientation_6(int32_t value)
	{
		___Orientation_6 = value;
	}

	inline static int32_t get_offset_of_XResolution_7() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___XResolution_7)); }
	inline double get_XResolution_7() const { return ___XResolution_7; }
	inline double* get_address_of_XResolution_7() { return &___XResolution_7; }
	inline void set_XResolution_7(double value)
	{
		___XResolution_7 = value;
	}

	inline static int32_t get_offset_of_YResolution_8() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___YResolution_8)); }
	inline double get_YResolution_8() const { return ___YResolution_8; }
	inline double* get_address_of_YResolution_8() { return &___YResolution_8; }
	inline void set_YResolution_8(double value)
	{
		___YResolution_8 = value;
	}

	inline static int32_t get_offset_of_ResolutionUnit_9() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___ResolutionUnit_9)); }
	inline int32_t get_ResolutionUnit_9() const { return ___ResolutionUnit_9; }
	inline int32_t* get_address_of_ResolutionUnit_9() { return &___ResolutionUnit_9; }
	inline void set_ResolutionUnit_9(int32_t value)
	{
		___ResolutionUnit_9 = value;
	}

	inline static int32_t get_offset_of_DateTime_10() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___DateTime_10)); }
	inline String_t* get_DateTime_10() const { return ___DateTime_10; }
	inline String_t** get_address_of_DateTime_10() { return &___DateTime_10; }
	inline void set_DateTime_10(String_t* value)
	{
		___DateTime_10 = value;
		Il2CppCodeGenWriteBarrier(&___DateTime_10, value);
	}

	inline static int32_t get_offset_of_Description_11() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___Description_11)); }
	inline String_t* get_Description_11() const { return ___Description_11; }
	inline String_t** get_address_of_Description_11() { return &___Description_11; }
	inline void set_Description_11(String_t* value)
	{
		___Description_11 = value;
		Il2CppCodeGenWriteBarrier(&___Description_11, value);
	}

	inline static int32_t get_offset_of_Make_12() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___Make_12)); }
	inline String_t* get_Make_12() const { return ___Make_12; }
	inline String_t** get_address_of_Make_12() { return &___Make_12; }
	inline void set_Make_12(String_t* value)
	{
		___Make_12 = value;
		Il2CppCodeGenWriteBarrier(&___Make_12, value);
	}

	inline static int32_t get_offset_of_Model_13() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___Model_13)); }
	inline String_t* get_Model_13() const { return ___Model_13; }
	inline String_t** get_address_of_Model_13() { return &___Model_13; }
	inline void set_Model_13(String_t* value)
	{
		___Model_13 = value;
		Il2CppCodeGenWriteBarrier(&___Model_13, value);
	}

	inline static int32_t get_offset_of_Software_14() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___Software_14)); }
	inline String_t* get_Software_14() const { return ___Software_14; }
	inline String_t** get_address_of_Software_14() { return &___Software_14; }
	inline void set_Software_14(String_t* value)
	{
		___Software_14 = value;
		Il2CppCodeGenWriteBarrier(&___Software_14, value);
	}

	inline static int32_t get_offset_of_Artist_15() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___Artist_15)); }
	inline String_t* get_Artist_15() const { return ___Artist_15; }
	inline String_t** get_address_of_Artist_15() { return &___Artist_15; }
	inline void set_Artist_15(String_t* value)
	{
		___Artist_15 = value;
		Il2CppCodeGenWriteBarrier(&___Artist_15, value);
	}

	inline static int32_t get_offset_of_Copyright_16() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___Copyright_16)); }
	inline String_t* get_Copyright_16() const { return ___Copyright_16; }
	inline String_t** get_address_of_Copyright_16() { return &___Copyright_16; }
	inline void set_Copyright_16(String_t* value)
	{
		___Copyright_16 = value;
		Il2CppCodeGenWriteBarrier(&___Copyright_16, value);
	}

	inline static int32_t get_offset_of_UserComment_17() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___UserComment_17)); }
	inline String_t* get_UserComment_17() const { return ___UserComment_17; }
	inline String_t** get_address_of_UserComment_17() { return &___UserComment_17; }
	inline void set_UserComment_17(String_t* value)
	{
		___UserComment_17 = value;
		Il2CppCodeGenWriteBarrier(&___UserComment_17, value);
	}

	inline static int32_t get_offset_of_ExposureTime_18() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___ExposureTime_18)); }
	inline double get_ExposureTime_18() const { return ___ExposureTime_18; }
	inline double* get_address_of_ExposureTime_18() { return &___ExposureTime_18; }
	inline void set_ExposureTime_18(double value)
	{
		___ExposureTime_18 = value;
	}

	inline static int32_t get_offset_of_FNumber_19() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___FNumber_19)); }
	inline double get_FNumber_19() const { return ___FNumber_19; }
	inline double* get_address_of_FNumber_19() { return &___FNumber_19; }
	inline void set_FNumber_19(double value)
	{
		___FNumber_19 = value;
	}

	inline static int32_t get_offset_of_Flash_20() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___Flash_20)); }
	inline int32_t get_Flash_20() const { return ___Flash_20; }
	inline int32_t* get_address_of_Flash_20() { return &___Flash_20; }
	inline void set_Flash_20(int32_t value)
	{
		___Flash_20 = value;
	}

	inline static int32_t get_offset_of_GpsLatitudeRef_21() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___GpsLatitudeRef_21)); }
	inline int32_t get_GpsLatitudeRef_21() const { return ___GpsLatitudeRef_21; }
	inline int32_t* get_address_of_GpsLatitudeRef_21() { return &___GpsLatitudeRef_21; }
	inline void set_GpsLatitudeRef_21(int32_t value)
	{
		___GpsLatitudeRef_21 = value;
	}

	inline static int32_t get_offset_of_GpsLatitude_22() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___GpsLatitude_22)); }
	inline DoubleU5BU5D_t1889952540* get_GpsLatitude_22() const { return ___GpsLatitude_22; }
	inline DoubleU5BU5D_t1889952540** get_address_of_GpsLatitude_22() { return &___GpsLatitude_22; }
	inline void set_GpsLatitude_22(DoubleU5BU5D_t1889952540* value)
	{
		___GpsLatitude_22 = value;
		Il2CppCodeGenWriteBarrier(&___GpsLatitude_22, value);
	}

	inline static int32_t get_offset_of_GpsLongitudeRef_23() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___GpsLongitudeRef_23)); }
	inline int32_t get_GpsLongitudeRef_23() const { return ___GpsLongitudeRef_23; }
	inline int32_t* get_address_of_GpsLongitudeRef_23() { return &___GpsLongitudeRef_23; }
	inline void set_GpsLongitudeRef_23(int32_t value)
	{
		___GpsLongitudeRef_23 = value;
	}

	inline static int32_t get_offset_of_GpsLongitude_24() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___GpsLongitude_24)); }
	inline DoubleU5BU5D_t1889952540* get_GpsLongitude_24() const { return ___GpsLongitude_24; }
	inline DoubleU5BU5D_t1889952540** get_address_of_GpsLongitude_24() { return &___GpsLongitude_24; }
	inline void set_GpsLongitude_24(DoubleU5BU5D_t1889952540* value)
	{
		___GpsLongitude_24 = value;
		Il2CppCodeGenWriteBarrier(&___GpsLongitude_24, value);
	}

	inline static int32_t get_offset_of_ThumbnailOffset_25() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___ThumbnailOffset_25)); }
	inline int32_t get_ThumbnailOffset_25() const { return ___ThumbnailOffset_25; }
	inline int32_t* get_address_of_ThumbnailOffset_25() { return &___ThumbnailOffset_25; }
	inline void set_ThumbnailOffset_25(int32_t value)
	{
		___ThumbnailOffset_25 = value;
	}

	inline static int32_t get_offset_of_ThumbnailSize_26() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___ThumbnailSize_26)); }
	inline int32_t get_ThumbnailSize_26() const { return ___ThumbnailSize_26; }
	inline int32_t* get_address_of_ThumbnailSize_26() { return &___ThumbnailSize_26; }
	inline void set_ThumbnailSize_26(int32_t value)
	{
		___ThumbnailSize_26 = value;
	}

	inline static int32_t get_offset_of_ThumbnailData_27() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___ThumbnailData_27)); }
	inline ByteU5BU5D_t3397334013* get_ThumbnailData_27() const { return ___ThumbnailData_27; }
	inline ByteU5BU5D_t3397334013** get_address_of_ThumbnailData_27() { return &___ThumbnailData_27; }
	inline void set_ThumbnailData_27(ByteU5BU5D_t3397334013* value)
	{
		___ThumbnailData_27 = value;
		Il2CppCodeGenWriteBarrier(&___ThumbnailData_27, value);
	}

	inline static int32_t get_offset_of_LoadTime_28() { return static_cast<int32_t>(offsetof(JpegInfo_t3114827956, ___LoadTime_28)); }
	inline TimeSpan_t3430258949  get_LoadTime_28() const { return ___LoadTime_28; }
	inline TimeSpan_t3430258949 * get_address_of_LoadTime_28() { return &___LoadTime_28; }
	inline void set_LoadTime_28(TimeSpan_t3430258949  value)
	{
		___LoadTime_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
