﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SK_RequestCapabilitieResult
struct SK_RequestCapabilitieResult_t2510163844;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_CloudServiceCapabi714629801.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SK_RequestCapabilitieResult::.ctor(SK_CloudServiceCapability)
extern "C"  void SK_RequestCapabilitieResult__ctor_m2861536598 (SK_RequestCapabilitieResult_t2510163844 * __this, int32_t ___capability0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SK_RequestCapabilitieResult::.ctor(System.String)
extern "C"  void SK_RequestCapabilitieResult__ctor_m3528146553 (SK_RequestCapabilitieResult_t2510163844 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SK_CloudServiceCapability SK_RequestCapabilitieResult::get_Capability()
extern "C"  int32_t SK_RequestCapabilitieResult_get_Capability_m2135374280 (SK_RequestCapabilitieResult_t2510163844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
