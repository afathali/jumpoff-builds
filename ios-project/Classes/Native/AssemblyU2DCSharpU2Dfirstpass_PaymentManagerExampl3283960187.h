﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PaymentManagerExample
struct  PaymentManagerExample_t3283960187  : public Il2CppObject
{
public:

public:
};

struct PaymentManagerExample_t3283960187_StaticFields
{
public:
	// System.Boolean PaymentManagerExample::IsInitialized
	bool ___IsInitialized_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PaymentManagerExample::<>f__switch$map0
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map0_3;

public:
	inline static int32_t get_offset_of_IsInitialized_2() { return static_cast<int32_t>(offsetof(PaymentManagerExample_t3283960187_StaticFields, ___IsInitialized_2)); }
	inline bool get_IsInitialized_2() const { return ___IsInitialized_2; }
	inline bool* get_address_of_IsInitialized_2() { return &___IsInitialized_2; }
	inline void set_IsInitialized_2(bool value)
	{
		___IsInitialized_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_3() { return static_cast<int32_t>(offsetof(PaymentManagerExample_t3283960187_StaticFields, ___U3CU3Ef__switchU24map0_3)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map0_3() const { return ___U3CU3Ef__switchU24map0_3; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map0_3() { return &___U3CU3Ef__switchU24map0_3; }
	inline void set_U3CU3Ef__switchU24map0_3(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
