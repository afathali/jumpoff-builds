﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WWWTextureLoader
struct WWWTextureLoader_t155872171;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void WWWTextureLoader::.ctor()
extern "C"  void WWWTextureLoader__ctor_m2514922492 (WWWTextureLoader_t155872171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WWWTextureLoader::add_OnLoad(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void WWWTextureLoader_add_OnLoad_m1127950879 (WWWTextureLoader_t155872171 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WWWTextureLoader::remove_OnLoad(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void WWWTextureLoader_remove_OnLoad_m3003222534 (WWWTextureLoader_t155872171 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WWWTextureLoader WWWTextureLoader::Create()
extern "C"  WWWTextureLoader_t155872171 * WWWTextureLoader_Create_m2845297014 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WWWTextureLoader::LoadTexture(System.String)
extern "C"  void WWWTextureLoader_LoadTexture_m783656337 (WWWTextureLoader_t155872171 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WWWTextureLoader::LoadCoroutin()
extern "C"  Il2CppObject * WWWTextureLoader_LoadCoroutin_m3068864101 (WWWTextureLoader_t155872171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WWWTextureLoader::<OnLoad>m__B5(UnityEngine.Texture2D)
extern "C"  void WWWTextureLoader_U3COnLoadU3Em__B5_m66899739 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
