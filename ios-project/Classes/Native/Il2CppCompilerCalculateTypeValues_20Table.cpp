﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Stores_UnityEngine_Purchasing_AndroidJavaStore2772549594.h"
#include "Stores_UnityEngine_Purchasing_SerializationExtensi3531056818.h"
#include "Stores_UnityEngine_Purchasing_JSONSerializer501879906.h"
#include "Stores_UnityEngine_Purchasing_JavaBridge44746847.h"
#include "Stores_UnityEngine_Purchasing_NativeJSONStore3685388740.h"
#include "Stores_UnityEngine_Purchasing_ScriptingUnityCallbac906080071.h"
#include "Stores_UnityEngine_Purchasing_AmazonAppStoreStoreE1518886395.h"
#include "Stores_UnityEngine_Purchasing_FakeAmazonExtensions2261777661.h"
#include "Stores_UnityEngine_Purchasing_FakeGooglePlayConfigu737012266.h"
#include "Stores_UnityEngine_Purchasing_FakeSamsungAppsExten1522853249.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsMode2214306743.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsStoreExte3441062041.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl1301617341.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl_U3CMe2294210682.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleConfiguatio4052738437.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleExtensions4039399289.h"
#include "Stores_UnityEngine_Purchasing_AndroidStore3203055206.h"
#include "Stores_UnityEngine_Purchasing_FakeStoreUIMode2321492887.h"
#include "Stores_UnityEngine_Purchasing_FakeStore3882981564.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_DialogType1733969544.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CRetriev2692708567.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CPurchas2000504963.h"
#include "Stores_UnityEngine_Purchasing_RawStoreProvider3477922056.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMod107230755.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo1310600573.h"
#include "Stores_UnityEngine_Purchasing_StoreConfiguration2466794143.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore3684252124.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_DialogRe2092195449.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_Lifecycl1057582876.h"
#include "Stores_UnityEngine_Purchasing_Extension_UnityUtil166323129.h"
#include "Stores_UnityEngine_Purchasing_Extension_UnityUtil_3019257939.h"
#include "Stores_UnityEngine_Purchasing_WinRTStore36043095.h"
#include "Stores_UnityEngine_Purchasing_TizenStoreImpl274247241.h"
#include "Stores_UnityEngine_Purchasing_FakeTizenStoreConfig3055550456.h"
#include "SampleClassLibrary_U3CModuleU3E3783534214.h"
#include "SampleClassLibrary_SampleClassLibrary_SampleExtern2192168175.h"
#include "System_Xml_U3CModuleU3E3783534214.h"
#include "System_Xml_System_MonoTODOAttribute3487514019.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet2758097827.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType1096449895.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType3359210273.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic2904699188.h"
#include "System_Xml_Mono_Xml_Schema_XsdString263933896.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString2132216291.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken2902215462.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage3897851897.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken547135877.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens1753890866.h"
#include "System_Xml_Mono_Xml_Schema_XsdName1409945702.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName414304927.h"
#include "System_Xml_Mono_Xml_Schema_XsdID1067193160.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef1377311049.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs763119546.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity2664305022.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities1103053540.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation720379093.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal2932251550.h"
#include "System_Xml_Mono_Xml_Schema_XsdInteger1330502157.h"
#include "System_Xml_Mono_Xml_Schema_XsdLong4179235589.h"
#include "System_Xml_Mono_Xml_Schema_XsdInt1488443894.h"
#include "System_Xml_Mono_Xml_Schema_XsdShort1778530041.h"
#include "System_Xml_Mono_Xml_Schema_XsdByte1120972221.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeInteger3587933853.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLong137890294.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedInt2956447959.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShort3693774826.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedByte3216355454.h"
#include "System_Xml_Mono_Xml_Schema_XsdPositiveInteger1896481288.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveInteger409343009.h"
#include "System_Xml_Mono_Xml_Schema_XsdNegativeInteger399444136.h"
#include "System_Xml_Mono_Xml_Schema_XsdFloat386143221.h"
#include "System_Xml_Mono_Xml_Schema_XsdDouble2510112208.h"
#include "System_Xml_Mono_Xml_Schema_XsdBase64Binary1094704629.h"
#include "System_Xml_Mono_Xml_Schema_XsdHexBinary3496718151.h"
#include "System_Xml_Mono_Xml_Schema_XsdQName930779123.h"
#include "System_Xml_Mono_Xml_Schema_XsdBoolean4126353587.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyURI2527983239.h"
#include "System_Xml_Mono_Xml_Schema_XsdDuration1605638443.h"
#include "System_Xml_Mono_Xml_Schema_XdtDayTimeDuration2797717973.h"
#include "System_Xml_Mono_Xml_Schema_XdtYearMonthDuration1764328599.h"
#include "System_Xml_Mono_Xml_Schema_XsdDateTime1344468684.h"
#include "System_Xml_Mono_Xml_Schema_XsdDate919459387.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (AndroidJavaStore_t2772549594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[1] = 
{
	AndroidJavaStore_t2772549594::get_offset_of_m_Store_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (SerializationExtensions_t3531056818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (JSONSerializer_t501879906), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (JavaBridge_t44746847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[1] = 
{
	JavaBridge_t44746847::get_offset_of_forwardTo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (NativeJSONStore_t3685388740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[2] = 
{
	NativeJSONStore_t3685388740::get_offset_of_unity_0(),
	NativeJSONStore_t3685388740::get_offset_of_store_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (ScriptingUnityCallback_t906080071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[2] = 
{
	ScriptingUnityCallback_t906080071::get_offset_of_forwardTo_0(),
	ScriptingUnityCallback_t906080071::get_offset_of_util_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (AmazonAppStoreStoreExtensions_t1518886395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[1] = 
{
	AmazonAppStoreStoreExtensions_t1518886395::get_offset_of_android_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (FakeAmazonExtensions_t2261777661), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (FakeGooglePlayConfiguration_t737012266), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (FakeSamsungAppsExtensions_t1522853249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (SamsungAppsMode_t2214306743)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2018[4] = 
{
	SamsungAppsMode_t2214306743::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (SamsungAppsStoreExtensions_t3441062041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2019[2] = 
{
	SamsungAppsStoreExtensions_t3441062041::get_offset_of_m_RestoreCallback_1(),
	SamsungAppsStoreExtensions_t3441062041::get_offset_of_m_Java_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (AppleStoreImpl_t1301617341), -1, sizeof(AppleStoreImpl_t1301617341_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2020[9] = 
{
	AppleStoreImpl_t1301617341::get_offset_of_m_DeferredCallback_2(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RefreshReceiptError_3(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RefreshReceiptSuccess_4(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RestoreCallback_5(),
	AppleStoreImpl_t1301617341::get_offset_of_m_Native_6(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_util_7(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_instance_8(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_9(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (U3CMessageCallbackU3Ec__AnonStorey0_t2294210682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[4] = 
{
	U3CMessageCallbackU3Ec__AnonStorey0_t2294210682::get_offset_of_subject_0(),
	U3CMessageCallbackU3Ec__AnonStorey0_t2294210682::get_offset_of_payload_1(),
	U3CMessageCallbackU3Ec__AnonStorey0_t2294210682::get_offset_of_receipt_2(),
	U3CMessageCallbackU3Ec__AnonStorey0_t2294210682::get_offset_of_transactionId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (FakeAppleConfiguation_t4052738437), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (FakeAppleExtensions_t4039399289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (AndroidStore_t3203055206)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2026[5] = 
{
	AndroidStore_t3203055206::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (FakeStoreUIMode_t2321492887)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2027[4] = 
{
	FakeStoreUIMode_t2321492887::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (FakeStore_t3882981564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[5] = 
{
	FakeStore_t3882981564::get_offset_of_m_Biller_0(),
	FakeStore_t3882981564::get_offset_of_m_PurchasedProducts_1(),
	FakeStore_t3882981564::get_offset_of_purchaseCalled_2(),
	FakeStore_t3882981564::get_offset_of_U3CunavailableProductIdU3Ek__BackingField_3(),
	FakeStore_t3882981564::get_offset_of_UIMode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (DialogType_t1733969544)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2029[3] = 
{
	DialogType_t1733969544::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[2] = 
{
	U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567::get_offset_of_products_0(),
	U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (U3CPurchaseU3Ec__AnonStorey1_t2000504963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[2] = 
{
	U3CPurchaseU3Ec__AnonStorey1_t2000504963::get_offset_of_product_0(),
	U3CPurchaseU3Ec__AnonStorey1_t2000504963::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (RawStoreProvider_t3477922056), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (StandardPurchasingModule_t4003664591), -1, sizeof(StandardPurchasingModule_t4003664591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2036[10] = 
{
	StandardPurchasingModule_t4003664591::get_offset_of_m_AndroidPlatform_1(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_PlatformProvider_2(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_RuntimePlatform_3(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_Util_4(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_Logger_5(),
	StandardPurchasingModule_t4003664591_StaticFields::get_offset_of_ModuleInstance_6(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_StoreInstance_7(),
	StandardPurchasingModule_t4003664591_StaticFields::get_offset_of_AndroidStoreNameMap_8(),
	StandardPurchasingModule_t4003664591::get_offset_of_U3CuseFakeStoreUIModeU3Ek__BackingField_9(),
	StandardPurchasingModule_t4003664591::get_offset_of_windowsStore_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (StoreInstance_t107230755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[2] = 
{
	StoreInstance_t107230755::get_offset_of_U3CstoreNameU3Ek__BackingField_0(),
	StoreInstance_t107230755::get_offset_of_U3CinstanceU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (MicrosoftConfiguration_t1310600573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[2] = 
{
	MicrosoftConfiguration_t1310600573::get_offset_of_useMock_0(),
	MicrosoftConfiguration_t1310600573::get_offset_of_module_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (StoreConfiguration_t2466794143), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2039[1] = 
{
	StoreConfiguration_t2466794143::get_offset_of_U3CandroidStoreU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (UIFakeStore_t3684252124), -1, sizeof(UIFakeStore_t3684252124_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2040[7] = 
{
	UIFakeStore_t3684252124::get_offset_of_m_CurrentDialog_5(),
	UIFakeStore_t3684252124::get_offset_of_m_LastSelectedDropdownIndex_6(),
	UIFakeStore_t3684252124::get_offset_of_UIFakeStoreCanvasPrefab_7(),
	UIFakeStore_t3684252124::get_offset_of_m_Canvas_8(),
	UIFakeStore_t3684252124::get_offset_of_m_EventSystem_9(),
	UIFakeStore_t3684252124::get_offset_of_m_ParentGameObjectPath_10(),
	UIFakeStore_t3684252124_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (DialogRequest_t2092195449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[5] = 
{
	DialogRequest_t2092195449::get_offset_of_QueryText_0(),
	DialogRequest_t2092195449::get_offset_of_OkayButtonText_1(),
	DialogRequest_t2092195449::get_offset_of_CancelButtonText_2(),
	DialogRequest_t2092195449::get_offset_of_Options_3(),
	DialogRequest_t2092195449::get_offset_of_Callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (LifecycleNotifier_t1057582876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[1] = 
{
	LifecycleNotifier_t1057582876::get_offset_of_OnDestroyCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (UnityUtil_t166323129), -1, sizeof(UnityUtil_t166323129_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2044[4] = 
{
	UnityUtil_t166323129_StaticFields::get_offset_of_s_Callbacks_2(),
	UnityUtil_t166323129_StaticFields::get_offset_of_s_CallbacksPending_3(),
	UnityUtil_t166323129_StaticFields::get_offset_of_s_PcControlledPlatforms_4(),
	UnityUtil_t166323129::get_offset_of_pauseListeners_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (U3CDelayedCoroutineU3Ec__Iterator0_t3019257939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[6] = 
{
	U3CDelayedCoroutineU3Ec__Iterator0_t3019257939::get_offset_of_delay_0(),
	U3CDelayedCoroutineU3Ec__Iterator0_t3019257939::get_offset_of_coroutine_1(),
	U3CDelayedCoroutineU3Ec__Iterator0_t3019257939::get_offset_of_U24this_2(),
	U3CDelayedCoroutineU3Ec__Iterator0_t3019257939::get_offset_of_U24current_3(),
	U3CDelayedCoroutineU3Ec__Iterator0_t3019257939::get_offset_of_U24disposing_4(),
	U3CDelayedCoroutineU3Ec__Iterator0_t3019257939::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (WinRTStore_t36043095), -1, sizeof(WinRTStore_t36043095_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2047[6] = 
{
	WinRTStore_t36043095::get_offset_of_win8_0(),
	WinRTStore_t36043095::get_offset_of_callback_1(),
	WinRTStore_t36043095::get_offset_of_util_2(),
	WinRTStore_t36043095::get_offset_of_logger_3(),
	WinRTStore_t36043095_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	WinRTStore_t36043095_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (TizenStoreImpl_t274247241), -1, sizeof(TizenStoreImpl_t274247241_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2049[3] = 
{
	TizenStoreImpl_t274247241_StaticFields::get_offset_of_instance_2(),
	TizenStoreImpl_t274247241::get_offset_of_m_Native_3(),
	TizenStoreImpl_t274247241_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (FakeTizenStoreConfiguration_t3055550456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (U3CModuleU3E_t3783534227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (SampleExternalClass_t2192168175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2052[2] = 
{
	SampleExternalClass_t2192168175::get_offset_of_U3CSampleStringU3Ek__BackingField_0(),
	SampleExternalClass_t2192168175::get_offset_of_U3CSampleDictionaryU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (U3CModuleU3E_t3783534228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (MonoTODOAttribute_t3487514022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (XsdWhitespaceFacet_t2758097827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2056[4] = 
{
	XsdWhitespaceFacet_t2758097827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (XsdAnySimpleType_t1096449895), -1, sizeof(XsdAnySimpleType_t1096449895_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2057[6] = 
{
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (XdtAnyAtomicType_t3359210273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (XdtUntypedAtomic_t2904699188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (XsdString_t263933896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (XsdNormalizedString_t2132216291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (XsdToken_t2902215462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (XsdLanguage_t3897851897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (XsdNMToken_t547135877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (XsdNMTokens_t1753890866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (XsdName_t1409945702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (XsdNCName_t414304927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (XsdID_t1067193160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (XsdIDRef_t1377311049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (XsdIDRefs_t763119546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (XsdEntity_t2664305022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (XsdEntities_t1103053540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (XsdNotation_t720379093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (XsdDecimal_t2932251550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (XsdInteger_t1330502157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (XsdLong_t4179235589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (XsdInt_t1488443894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (XsdShort_t1778530041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (XsdByte_t1120972221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (XsdNonNegativeInteger_t3587933853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (XsdUnsignedLong_t137890294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (XsdUnsignedInt_t2956447959), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (XsdUnsignedShort_t3693774826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (XsdUnsignedByte_t3216355454), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (XsdPositiveInteger_t1896481288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (XsdNonPositiveInteger_t409343009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (XsdNegativeInteger_t399444136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (XsdFloat_t386143221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (XsdDouble_t2510112208), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (XsdBase64Binary_t1094704629), -1, sizeof(XsdBase64Binary_t1094704629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2090[2] = 
{
	XsdBase64Binary_t1094704629_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t1094704629_StaticFields::get_offset_of_decodeTable_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (XsdHexBinary_t3496718151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (XsdQName_t930779123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (XsdBoolean_t4126353587), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (XsdAnyURI_t2527983239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (XsdDuration_t1605638443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (XdtDayTimeDuration_t2797717973), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (XdtYearMonthDuration_t1764328599), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (XsdDateTime_t1344468684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (XsdDate_t919459387), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
