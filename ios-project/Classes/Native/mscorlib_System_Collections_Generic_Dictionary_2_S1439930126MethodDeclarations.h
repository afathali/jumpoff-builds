﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>
struct ShimEnumerator_t1439930126;
// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>
struct Dictionary_2_t1334805305;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m999822191_gshared (ShimEnumerator_t1439930126 * __this, Dictionary_2_t1334805305 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m999822191(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1439930126 *, Dictionary_2_t1334805305 *, const MethodInfo*))ShimEnumerator__ctor_m999822191_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3177276602_gshared (ShimEnumerator_t1439930126 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3177276602(__this, method) ((  bool (*) (ShimEnumerator_t1439930126 *, const MethodInfo*))ShimEnumerator_MoveNext_m3177276602_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3548669866_gshared (ShimEnumerator_t1439930126 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3548669866(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1439930126 *, const MethodInfo*))ShimEnumerator_get_Entry_m3548669866_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2569292831_gshared (ShimEnumerator_t1439930126 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2569292831(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1439930126 *, const MethodInfo*))ShimEnumerator_get_Key_m2569292831_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m954032039_gshared (ShimEnumerator_t1439930126 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m954032039(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1439930126 *, const MethodInfo*))ShimEnumerator_get_Value_m954032039_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1572723561_gshared (ShimEnumerator_t1439930126 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1572723561(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1439930126 *, const MethodInfo*))ShimEnumerator_get_Current_m1572723561_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::Reset()
extern "C"  void ShimEnumerator_Reset_m3751950453_gshared (ShimEnumerator_t1439930126 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3751950453(__this, method) ((  void (*) (ShimEnumerator_t1439930126 *, const MethodInfo*))ShimEnumerator_Reset_m3751950453_gshared)(__this, method)
