﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TWResult
struct TWResult_t1480791060;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TWResult::.ctor(System.Boolean,System.String)
extern "C"  void TWResult__ctor_m1336289862 (TWResult_t1480791060 * __this, bool ___IsResSucceeded0, String_t* ___resData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TWResult::get_IsSucceeded()
extern "C"  bool TWResult_get_IsSucceeded_m3208623953 (TWResult_t1480791060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TWResult::get_data()
extern "C"  String_t* TWResult_get_data_m1312261893 (TWResult_t1480791060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
