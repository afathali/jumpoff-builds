﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GPScore>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2870213002(__this, ___l0, method) ((  void (*) (Enumerator_t2123339695 *, List_1_t2588610021 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GPScore>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1626498168(__this, method) ((  void (*) (Enumerator_t2123339695 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GPScore>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2684818544(__this, method) ((  Il2CppObject * (*) (Enumerator_t2123339695 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GPScore>::Dispose()
#define Enumerator_Dispose_m4074452397(__this, method) ((  void (*) (Enumerator_t2123339695 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GPScore>::VerifyState()
#define Enumerator_VerifyState_m4014782372(__this, method) ((  void (*) (Enumerator_t2123339695 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GPScore>::MoveNext()
#define Enumerator_MoveNext_m182995299(__this, method) ((  bool (*) (Enumerator_t2123339695 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GPScore>::get_Current()
#define Enumerator_get_Current_m2199446863(__this, method) ((  GPScore_t3219488889 * (*) (Enumerator_t2123339695 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
