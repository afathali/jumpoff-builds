﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkManagerExample
struct NetworkManagerExample_t1502234955;
// BasePackage
struct BasePackage_t3541548393;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BasePackage3541548393.h"

// System.Void NetworkManagerExample::.ctor()
extern "C"  void NetworkManagerExample__ctor_m3470440278 (NetworkManagerExample_t1502234955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkManagerExample::send(BasePackage)
extern "C"  void NetworkManagerExample_send_m1324145143 (Il2CppObject * __this /* static, unused */, BasePackage_t3541548393 * ___pack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
