﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPGameRequest
struct GPGameRequest_t4158842942;

#include "codegen/il2cpp-codegen.h"

// System.Void GPGameRequest::.ctor()
extern "C"  void GPGameRequest__ctor_m2040925387 (GPGameRequest_t4158842942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
