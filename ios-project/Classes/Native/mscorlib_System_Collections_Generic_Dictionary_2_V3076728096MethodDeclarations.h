﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va400334773MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m4257107093(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3076728096 *, Dictionary_2_t78700957 *, const MethodInfo*))ValueCollection__ctor_m882866357_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3984399483(__this, ___item0, method) ((  void (*) (ValueCollection_t3076728096 *, GK_LocalPlayerScoreUpdateListener_t1070875322 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1903672223_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m429023838(__this, method) ((  void (*) (ValueCollection_t3076728096 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3271993638_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2931849013(__this, ___item0, method) ((  bool (*) (ValueCollection_t3076728096 *, GK_LocalPlayerScoreUpdateListener_t1070875322 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3958350925_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2476402332(__this, ___item0, method) ((  bool (*) (ValueCollection_t3076728096 *, GK_LocalPlayerScoreUpdateListener_t1070875322 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m98888100_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2895416980(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3076728096 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1604400448_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3640952086(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3076728096 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2627730402_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m29884259(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3076728096 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1073215119_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2736518984(__this, method) ((  bool (*) (ValueCollection_t3076728096 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1325719984_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3158715370(__this, method) ((  bool (*) (ValueCollection_t3076728096 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4041633470_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m688439692(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3076728096 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3927965720_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2698508202(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3076728096 *, GK_LocalPlayerScoreUpdateListenerU5BU5D_t1853016351*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1460341186_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::GetEnumerator()
#define ValueCollection_GetEnumerator_m694734997(__this, method) ((  Enumerator_t1765233721  (*) (ValueCollection_t3076728096 *, const MethodInfo*))ValueCollection_GetEnumerator_m941805197_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GK_LocalPlayerScoreUpdateListener>::get_Count()
#define ValueCollection_get_Count_m3110749778(__this, method) ((  int32_t (*) (ValueCollection_t3076728096 *, const MethodInfo*))ValueCollection_get_Count_m90930038_gshared)(__this, method)
