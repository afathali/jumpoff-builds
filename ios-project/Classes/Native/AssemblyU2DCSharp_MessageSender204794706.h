﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MCGBehaviour
struct MCGBehaviour_t3307770884;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageSender
struct  MessageSender_t204794706  : public Il2CppObject
{
public:
	// System.String MessageSender::_postUrl
	String_t* ____postUrl_1;

public:
	inline static int32_t get_offset_of__postUrl_1() { return static_cast<int32_t>(offsetof(MessageSender_t204794706, ____postUrl_1)); }
	inline String_t* get__postUrl_1() const { return ____postUrl_1; }
	inline String_t** get_address_of__postUrl_1() { return &____postUrl_1; }
	inline void set__postUrl_1(String_t* value)
	{
		____postUrl_1 = value;
		Il2CppCodeGenWriteBarrier(&____postUrl_1, value);
	}
};

struct MessageSender_t204794706_StaticFields
{
public:
	// MCGBehaviour MessageSender::_bridge
	MCGBehaviour_t3307770884 * ____bridge_2;
	// System.String MessageSender::setCookie
	String_t* ___setCookie_3;

public:
	inline static int32_t get_offset_of__bridge_2() { return static_cast<int32_t>(offsetof(MessageSender_t204794706_StaticFields, ____bridge_2)); }
	inline MCGBehaviour_t3307770884 * get__bridge_2() const { return ____bridge_2; }
	inline MCGBehaviour_t3307770884 ** get_address_of__bridge_2() { return &____bridge_2; }
	inline void set__bridge_2(MCGBehaviour_t3307770884 * value)
	{
		____bridge_2 = value;
		Il2CppCodeGenWriteBarrier(&____bridge_2, value);
	}

	inline static int32_t get_offset_of_setCookie_3() { return static_cast<int32_t>(offsetof(MessageSender_t204794706_StaticFields, ___setCookie_3)); }
	inline String_t* get_setCookie_3() const { return ___setCookie_3; }
	inline String_t** get_address_of_setCookie_3() { return &___setCookie_3; }
	inline void set_setCookie_3(String_t* value)
	{
		___setCookie_3 = value;
		Il2CppCodeGenWriteBarrier(&___setCookie_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
