﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3792524923(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2482832397 *, Dictionary_2_t1162807695 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1481280044(__this, method) ((  Il2CppObject * (*) (Enumerator_t2482832397 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3423044352(__this, method) ((  void (*) (Enumerator_t2482832397 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1430546041(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2482832397 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2207735422(__this, method) ((  Il2CppObject * (*) (Enumerator_t2482832397 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m327417484(__this, method) ((  Il2CppObject * (*) (Enumerator_t2482832397 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::MoveNext()
#define Enumerator_MoveNext_m3284994784(__this, method) ((  bool (*) (Enumerator_t2482832397 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::get_Current()
#define Enumerator_get_Current_m3055403080(__this, method) ((  KeyValuePair_2_t3215120213  (*) (Enumerator_t2482832397 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m403265863(__this, method) ((  String_t* (*) (Enumerator_t2482832397 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m434952743(__this, method) ((  Texture2D_t3542995729 * (*) (Enumerator_t2482832397 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::Reset()
#define Enumerator_Reset_m3052723353(__this, method) ((  void (*) (Enumerator_t2482832397 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::VerifyState()
#define Enumerator_VerifyState_m2577058936(__this, method) ((  void (*) (Enumerator_t2482832397 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m72511608(__this, method) ((  void (*) (Enumerator_t2482832397 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.Texture2D>::Dispose()
#define Enumerator_Dispose_m1323814319(__this, method) ((  void (*) (Enumerator_t2482832397 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
