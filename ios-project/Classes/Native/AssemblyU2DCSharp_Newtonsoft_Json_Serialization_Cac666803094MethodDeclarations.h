﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t502202687;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Object>::.cctor()
extern "C"  void CachedAttributeGetter_1__cctor_m1655531471_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define CachedAttributeGetter_1__cctor_m1655531471(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))CachedAttributeGetter_1__cctor_m1655531471_gshared)(__this /* static, unused */, method)
// T Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Object>::GetAttribute(System.Reflection.ICustomAttributeProvider)
extern "C"  Il2CppObject * CachedAttributeGetter_1_GetAttribute_m2147397194_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___type0, const MethodInfo* method);
#define CachedAttributeGetter_1_GetAttribute_m2147397194(__this /* static, unused */, ___type0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))CachedAttributeGetter_1_GetAttribute_m2147397194_gshared)(__this /* static, unused */, ___type0, method)
