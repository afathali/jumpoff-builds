﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_MCGBehaviour_LifestyleType936921768.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MCGBehaviour/InjectionContainer/TypeData
struct  TypeData_t212254090  : public Il2CppObject
{
public:
	// System.Object MCGBehaviour/InjectionContainer/TypeData::<Instance>k__BackingField
	Il2CppObject * ___U3CInstanceU3Ek__BackingField_0;
	// System.Type MCGBehaviour/InjectionContainer/TypeData::<ImplementedBy>k__BackingField
	Type_t * ___U3CImplementedByU3Ek__BackingField_1;
	// MCGBehaviour/LifestyleType MCGBehaviour/InjectionContainer/TypeData::<LifestyleType>k__BackingField
	int32_t ___U3CLifestyleTypeU3Ek__BackingField_2;
	// System.String MCGBehaviour/InjectionContainer/TypeData::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TypeData_t212254090, ___U3CInstanceU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CInstanceU3Ek__BackingField_0() const { return ___U3CInstanceU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CInstanceU3Ek__BackingField_0() { return &___U3CInstanceU3Ek__BackingField_0; }
	inline void set_U3CInstanceU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CInstanceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CImplementedByU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TypeData_t212254090, ___U3CImplementedByU3Ek__BackingField_1)); }
	inline Type_t * get_U3CImplementedByU3Ek__BackingField_1() const { return ___U3CImplementedByU3Ek__BackingField_1; }
	inline Type_t ** get_address_of_U3CImplementedByU3Ek__BackingField_1() { return &___U3CImplementedByU3Ek__BackingField_1; }
	inline void set_U3CImplementedByU3Ek__BackingField_1(Type_t * value)
	{
		___U3CImplementedByU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CImplementedByU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CLifestyleTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TypeData_t212254090, ___U3CLifestyleTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CLifestyleTypeU3Ek__BackingField_2() const { return ___U3CLifestyleTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CLifestyleTypeU3Ek__BackingField_2() { return &___U3CLifestyleTypeU3Ek__BackingField_2; }
	inline void set_U3CLifestyleTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CLifestyleTypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TypeData_t212254090, ___U3CNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CNameU3Ek__BackingField_3() const { return ___U3CNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_3() { return &___U3CNameU3Ek__BackingField_3; }
	inline void set_U3CNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNameU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
