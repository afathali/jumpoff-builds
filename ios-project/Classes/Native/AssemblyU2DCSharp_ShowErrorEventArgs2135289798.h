﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ShowErrorEventArgs/del
struct del_t1625342804;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowErrorEventArgs
struct  ShowErrorEventArgs_t2135289798  : public Il2CppObject
{
public:
	// System.String ShowErrorEventArgs::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_0;
	// ShowErrorEventArgs/del ShowErrorEventArgs::<onDismiss>k__BackingField
	del_t1625342804 * ___U3ConDismissU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ShowErrorEventArgs_t2135289798, ___U3CMessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_0() const { return ___U3CMessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_0() { return &___U3CMessageU3Ek__BackingField_0; }
	inline void set_U3CMessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMessageU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3ConDismissU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ShowErrorEventArgs_t2135289798, ___U3ConDismissU3Ek__BackingField_1)); }
	inline del_t1625342804 * get_U3ConDismissU3Ek__BackingField_1() const { return ___U3ConDismissU3Ek__BackingField_1; }
	inline del_t1625342804 ** get_address_of_U3ConDismissU3Ek__BackingField_1() { return &___U3ConDismissU3Ek__BackingField_1; }
	inline void set_U3ConDismissU3Ek__BackingField_1(del_t1625342804 * value)
	{
		___U3ConDismissU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3ConDismissU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
