﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayServiceExample
struct PlayServiceExample_t1159077439;
// GP_AdvertisingIdLoadResult
struct GP_AdvertisingIdLoadResult_t2783375090;
// GooglePlayResult
struct GooglePlayResult_t3097469636;
// GP_AchievementResult
struct GP_AchievementResult_t3970926374;
// GP_LeaderboardResult
struct GP_LeaderboardResult_t2034215294;
// GooglePlayConnectionResult
struct GooglePlayConnectionResult_t2758718724;
// GooglePlayGiftRequestResult
struct GooglePlayGiftRequestResult_t350202007;
// System.Collections.Generic.List`1<GPGameRequest>
struct List_1_t3527964074;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GP_AdvertisingIdLoadResult2783375090.h"
#include "AssemblyU2DCSharp_GooglePlayResult3097469636.h"
#include "AssemblyU2DCSharp_GP_AchievementResult3970926374.h"
#include "AssemblyU2DCSharp_GP_LeaderboardResult2034215294.h"
#include "AssemblyU2DCSharp_GooglePlayConnectionResult2758718724.h"
#include "AssemblyU2DCSharp_GooglePlayGiftRequestResult350202007.h"
#include "AssemblyU2DCSharp_AndroidDialogResult1664046954.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PlayServiceExample::.ctor()
extern "C"  void PlayServiceExample__ctor_m1677848556 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::Start()
extern "C"  void PlayServiceExample_Start_m1026670624 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OnDestroy()
extern "C"  void PlayServiceExample_OnDestroy_m1762769805 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::ConncetButtonPress()
extern "C"  void PlayServiceExample_ConncetButtonPress_m2823495517 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::GetAccs()
extern "C"  void PlayServiceExample_GetAccs_m1700102782 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::RetrieveToken()
extern "C"  void PlayServiceExample_RetrieveToken_m3635586487 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::showLeaderBoardsUI()
extern "C"  void PlayServiceExample_showLeaderBoardsUI_m4136635739 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::loadLeaderBoards()
extern "C"  void PlayServiceExample_loadLeaderBoards_m3013187406 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::showLeaderBoard()
extern "C"  void PlayServiceExample_showLeaderBoard_m800240136 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::submitScore()
extern "C"  void PlayServiceExample_submitScore_m3352073454 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::ResetBoard()
extern "C"  void PlayServiceExample_ResetBoard_m2225468557 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::showAchievementsUI()
extern "C"  void PlayServiceExample_showAchievementsUI_m785311165 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::loadAchievements()
extern "C"  void PlayServiceExample_loadAchievements_m3211761362 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::reportAchievement()
extern "C"  void PlayServiceExample_reportAchievement_m109877395 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::incrementAchievement()
extern "C"  void PlayServiceExample_incrementAchievement_m3621079974 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::revealAchievement()
extern "C"  void PlayServiceExample_revealAchievement_m2206222200 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::ResetAchievement()
extern "C"  void PlayServiceExample_ResetAchievement_m2399402632 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::ResetAllAchievements()
extern "C"  void PlayServiceExample_ResetAllAchievements_m1088054512 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::SendGiftRequest()
extern "C"  void PlayServiceExample_SendGiftRequest_m3423086459 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OpenInbox()
extern "C"  void PlayServiceExample_OpenInbox_m1650891280 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::clearDefaultAccount()
extern "C"  void PlayServiceExample_clearDefaultAccount_m3689449429 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::FixedUpdate()
extern "C"  void PlayServiceExample_FixedUpdate_m2966736117 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::RequestAdvertisingId()
extern "C"  void PlayServiceExample_RequestAdvertisingId_m268729418 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::ActionAdvertisingIdLoaded(GP_AdvertisingIdLoadResult)
extern "C"  void PlayServiceExample_ActionAdvertisingIdLoaded_m134722226 (PlayServiceExample_t1159077439 * __this, GP_AdvertisingIdLoadResult_t2783375090 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OnAchievmnetsLoadedInfoListner(GooglePlayResult)
extern "C"  void PlayServiceExample_OnAchievmnetsLoadedInfoListner_m1604730836 (PlayServiceExample_t1159077439 * __this, GooglePlayResult_t3097469636 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OnAchievementsLoaded(GooglePlayResult)
extern "C"  void PlayServiceExample_OnAchievementsLoaded_m980958666 (PlayServiceExample_t1159077439 * __this, GooglePlayResult_t3097469636 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OnAchievementUpdated(GP_AchievementResult)
extern "C"  void PlayServiceExample_OnAchievementUpdated_m930991375 (PlayServiceExample_t1159077439 * __this, GP_AchievementResult_t3970926374 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OnLeaderBoardsLoaded(GooglePlayResult)
extern "C"  void PlayServiceExample_OnLeaderBoardsLoaded_m2585868482 (PlayServiceExample_t1159077439 * __this, GooglePlayResult_t3097469636 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::UpdateBoardInfo()
extern "C"  void PlayServiceExample_UpdateBoardInfo_m2227346403 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OnScoreSubmited(GP_LeaderboardResult)
extern "C"  void PlayServiceExample_OnScoreSubmited_m789891070 (PlayServiceExample_t1159077439 * __this, GP_LeaderboardResult_t2034215294 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OnScoreUpdated(GooglePlayResult)
extern "C"  void PlayServiceExample_OnScoreUpdated_m1456520786 (PlayServiceExample_t1159077439 * __this, GooglePlayResult_t3097469636 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OnPlayerDisconnected()
extern "C"  void PlayServiceExample_OnPlayerDisconnected_m1326395081 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OnPlayerConnected()
extern "C"  void PlayServiceExample_OnPlayerConnected_m3077910987 (PlayServiceExample_t1159077439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::ActionConnectionResultReceived(GooglePlayConnectionResult)
extern "C"  void PlayServiceExample_ActionConnectionResultReceived_m24468974 (PlayServiceExample_t1159077439 * __this, GooglePlayConnectionResult_t2758718724 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OnGiftResult(GooglePlayGiftRequestResult)
extern "C"  void PlayServiceExample_OnGiftResult_m2075951279 (PlayServiceExample_t1159077439 * __this, GooglePlayGiftRequestResult_t350202007 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OnPendingGiftsDetected(System.Collections.Generic.List`1<GPGameRequest>)
extern "C"  void PlayServiceExample_OnPendingGiftsDetected_m2599860615 (PlayServiceExample_t1159077439 * __this, List_1_t3527964074 * ___gifts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OnPromtGiftDialogClose(AndroidDialogResult)
extern "C"  void PlayServiceExample_OnPromtGiftDialogClose_m2970433623 (PlayServiceExample_t1159077439 * __this, int32_t ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::OnGameRequestAccepted(System.Collections.Generic.List`1<GPGameRequest>)
extern "C"  void PlayServiceExample_OnGameRequestAccepted_m2517632439 (PlayServiceExample_t1159077439 * __this, List_1_t3527964074 * ___gifts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::ActionAvailableDeviceAccountsLoaded(System.Collections.Generic.List`1<System.String>)
extern "C"  void PlayServiceExample_ActionAvailableDeviceAccountsLoaded_m352037486 (PlayServiceExample_t1159077439 * __this, List_1_t1398341365 * ___accounts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::SighDialogComplete(AndroidDialogResult)
extern "C"  void PlayServiceExample_SighDialogComplete_m3144212354 (PlayServiceExample_t1159077439 * __this, int32_t ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayServiceExample::ActionOAuthTokenLoaded(System.String)
extern "C"  void PlayServiceExample_ActionOAuthTokenLoaded_m2355880185 (PlayServiceExample_t1159077439 * __this, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
