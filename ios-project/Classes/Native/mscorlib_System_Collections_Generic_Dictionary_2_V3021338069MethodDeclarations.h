﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>
struct Dictionary_2_t1334805305;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3021338069.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2203297066_gshared (Enumerator_t3021338069 * __this, Dictionary_2_t1334805305 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2203297066(__this, ___host0, method) ((  void (*) (Enumerator_t3021338069 *, Dictionary_2_t1334805305 *, const MethodInfo*))Enumerator__ctor_m2203297066_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3535300875_gshared (Enumerator_t3021338069 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3535300875(__this, method) ((  Il2CppObject * (*) (Enumerator_t3021338069 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3535300875_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3523724823_gshared (Enumerator_t3021338069 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3523724823(__this, method) ((  void (*) (Enumerator_t3021338069 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3523724823_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::Dispose()
extern "C"  void Enumerator_Dispose_m2331264822_gshared (Enumerator_t3021338069 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2331264822(__this, method) ((  void (*) (Enumerator_t3021338069 *, const MethodInfo*))Enumerator_Dispose_m2331264822_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1501822819_gshared (Enumerator_t3021338069 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1501822819(__this, method) ((  bool (*) (Enumerator_t3021338069 *, const MethodInfo*))Enumerator_MoveNext_m1501822819_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Newtonsoft.Json.Schema.JsonSchemaType>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1944260665_gshared (Enumerator_t3021338069 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1944260665(__this, method) ((  int32_t (*) (Enumerator_t3021338069 *, const MethodInfo*))Enumerator_get_Current_m1944260665_gshared)(__this, method)
