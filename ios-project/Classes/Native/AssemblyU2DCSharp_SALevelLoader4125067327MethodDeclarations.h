﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SALevelLoader
struct SALevelLoader_t4125067327;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SALevelLoader::.ctor()
extern "C"  void SALevelLoader__ctor_m2458354136 (SALevelLoader_t4125067327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SALevelLoader::Awake()
extern "C"  void SALevelLoader_Awake_m524854591 (SALevelLoader_t4125067327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SALevelLoader::LoadLevel(System.String)
extern "C"  void SALevelLoader_LoadLevel_m1931319716 (SALevelLoader_t4125067327 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SALevelLoader::Restart()
extern "C"  void SALevelLoader_Restart_m3907253197 (SALevelLoader_t4125067327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
