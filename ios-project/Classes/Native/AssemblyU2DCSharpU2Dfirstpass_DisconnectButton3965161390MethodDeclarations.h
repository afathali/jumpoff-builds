﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DisconnectButton
struct DisconnectButton_t3965161390;

#include "codegen/il2cpp-codegen.h"

// System.Void DisconnectButton::.ctor()
extern "C"  void DisconnectButton__ctor_m4109790019 (DisconnectButton_t3965161390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisconnectButton::Start()
extern "C"  void DisconnectButton_Start_m2536483975 (DisconnectButton_t3965161390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisconnectButton::OnGUI()
extern "C"  void DisconnectButton_OnGUI_m1654885505 (DisconnectButton_t3965161390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
