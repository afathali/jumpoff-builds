﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSSharedApplication
struct IOSSharedApplication_t4065685598;
// System.Action`1<ISN_CheckUrlResult>
struct Action_1_t1447523883;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.String
struct String_t;
// ISN_CheckUrlResult
struct ISN_CheckUrlResult_t1645724501;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_CheckUrlResult1645724501.h"

// System.Void IOSSharedApplication::.ctor()
extern "C"  void IOSSharedApplication__ctor_m1216226707 (IOSSharedApplication_t4065685598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::.cctor()
extern "C"  void IOSSharedApplication__cctor_m466276614 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::add_OnUrlCheckResultAction(System.Action`1<ISN_CheckUrlResult>)
extern "C"  void IOSSharedApplication_add_OnUrlCheckResultAction_m3131400575 (Il2CppObject * __this /* static, unused */, Action_1_t1447523883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::remove_OnUrlCheckResultAction(System.Action`1<ISN_CheckUrlResult>)
extern "C"  void IOSSharedApplication_remove_OnUrlCheckResultAction_m1214961594 (Il2CppObject * __this /* static, unused */, Action_1_t1447523883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::add_OnAdvertisingIdentifierLoadedAction(System.Action`1<System.String>)
extern "C"  void IOSSharedApplication_add_OnAdvertisingIdentifierLoadedAction_m1696225044 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::remove_OnAdvertisingIdentifierLoadedAction(System.Action`1<System.String>)
extern "C"  void IOSSharedApplication_remove_OnAdvertisingIdentifierLoadedAction_m1523913211 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::_ISN_checkUrl(System.String)
extern "C"  void IOSSharedApplication__ISN_checkUrl_m971835306 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::_ISN_openUrl(System.String)
extern "C"  void IOSSharedApplication__ISN_openUrl_m459573886 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::_ISN_GetIFA()
extern "C"  void IOSSharedApplication__ISN_GetIFA_m890200851 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::Awake()
extern "C"  void IOSSharedApplication_Awake_m584705440 (IOSSharedApplication_t4065685598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::CheckUrl(System.String)
extern "C"  void IOSSharedApplication_CheckUrl_m887724064 (IOSSharedApplication_t4065685598 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::OpenUrl(System.String)
extern "C"  void IOSSharedApplication_OpenUrl_m1973172376 (IOSSharedApplication_t4065685598 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::GetAdvertisingIdentifier()
extern "C"  void IOSSharedApplication_GetAdvertisingIdentifier_m3139101138 (IOSSharedApplication_t4065685598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::UrlCheckSuccess(System.String)
extern "C"  void IOSSharedApplication_UrlCheckSuccess_m2770762151 (IOSSharedApplication_t4065685598 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::UrlCheckFailed(System.String)
extern "C"  void IOSSharedApplication_UrlCheckFailed_m4211839355 (IOSSharedApplication_t4065685598 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::OnAdvertisingIdentifierLoaded(System.String)
extern "C"  void IOSSharedApplication_OnAdvertisingIdentifierLoaded_m1813365650 (IOSSharedApplication_t4065685598 * __this, String_t* ___Identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::<OnUrlCheckResultAction>m__68(ISN_CheckUrlResult)
extern "C"  void IOSSharedApplication_U3COnUrlCheckResultActionU3Em__68_m3413355238 (Il2CppObject * __this /* static, unused */, ISN_CheckUrlResult_t1645724501 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSSharedApplication::<OnAdvertisingIdentifierLoadedAction>m__69(System.String)
extern "C"  void IOSSharedApplication_U3COnAdvertisingIdentifierLoadedActionU3Em__69_m3995736302 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
