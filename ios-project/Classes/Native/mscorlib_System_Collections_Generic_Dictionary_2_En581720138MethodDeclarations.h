﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2850182031(__this, ___dictionary0, method) ((  void (*) (Enumerator_t581720138 *, Dictionary_2_t3556662732 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3194960622(__this, method) ((  Il2CppObject * (*) (Enumerator_t581720138 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1101360744(__this, method) ((  void (*) (Enumerator_t581720138 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1191859857(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t581720138 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m789011948(__this, method) ((  Il2CppObject * (*) (Enumerator_t581720138 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2282494230(__this, method) ((  Il2CppObject * (*) (Enumerator_t581720138 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::MoveNext()
#define Enumerator_MoveNext_m1638131293(__this, method) ((  bool (*) (Enumerator_t581720138 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::get_Current()
#define Enumerator_get_Current_m269449202(__this, method) ((  KeyValuePair_2_t1314007954  (*) (Enumerator_t581720138 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2792320163(__this, method) ((  String_t* (*) (Enumerator_t581720138 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1010903443(__this, method) ((  GP_Quest_t1641883470 * (*) (Enumerator_t581720138 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::Reset()
#define Enumerator_Reset_m1257979953(__this, method) ((  void (*) (Enumerator_t581720138 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::VerifyState()
#define Enumerator_VerifyState_m1040248100(__this, method) ((  void (*) (Enumerator_t581720138 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m4147222272(__this, method) ((  void (*) (Enumerator_t581720138 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GP_Quest>::Dispose()
#define Enumerator_Dispose_m1319004195(__this, method) ((  void (*) (Enumerator_t581720138 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
