﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchEndResul3461768810.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchInitResu3847830897.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchQuitResu1233820656.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchRemovedRe909126313.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchTurnResu3583658160.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_RematchResult3159773700.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_Match132033130.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_Participant3803955090.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSCamera2845108690.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSCamera_U3CSaveScr2834900354.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_ImageSource2773896895.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSImagePickResult1671334394.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_FilePicker2957423793.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSGalleryLoadImageFo518894765.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_FilePickerResult2234803986.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_MediaController1853840745.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MusicPlaybackStat2364713801.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MusicRepeatMode445284825.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MusicShuffleMode2068355741.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MediaItem4025623029.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MediaPickerResult2204006871.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSVideoManager3003649481.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_ReplayKit2994976952.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ReplayKitVideoShareR2762953534.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_LocalNotificatio3778913674.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_RemoteNotificati1910006075.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_NotificationType2960395864.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_DeviceToken380973950.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_LocalNotification273186689.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_RemoteNotificati1449597314.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_RemoteNotificati3335875151.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSNativePopUpManage3689761895.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BaseIOSPopup3320937364.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSDialog3518705031.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSMessage2569463336.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSRateUsPopUp2222998473.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSDialogResult3739241316.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSSocialManager2957403963.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSBillingInitChecke4166635083.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSBillingInitChecke4162012659.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSInAppPurchaseManag644626385.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSNativeMarketBridg1553898649.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreResultCode365779630.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSTransactionErrorC1822631322.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_InAppPriceTier570012276.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_InAppType3300503503.h"
#include "AssemblyU2DCSharpU2Dfirstpass_InAppPurchaseState3414001346.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreProductView607200268.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_CloudService588287925.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_CloudServiceAutho3247563438.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_CloudServiceCapabi714629801.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_AuthorizationResu2047753871.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_RequestCapabiliti2510163844.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_RequestStorefront1112477162.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSProductTemplate1036598382.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreKitRestoreRe3305276155.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreKitResult2359407583.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSStoreKitVerificat4263658582.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSNativeAppEvents694411788.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contact4178394798.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contacts160827595.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contact1045731284.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSNative_Contacts113354641.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSDateTimePicker849222074.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSDateTimePickerMod3227591715.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_Device2325020285.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_DeviceGUID1787531836.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_InterfaceIdiom3178386526.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_GestureRecognize4198098372.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_SwipeDirection768921696.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_Logger85856405.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_LocalReceiptResu3746327569.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_Security2700938347.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSSharedApplication4065685598.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_CheckUrlResult1645724501.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSNativeUtility933355194.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_Locale2162888085.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_CloudKit2197422136.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iCloudManager2506189173.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_Database243306482.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_Query2718018967.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_Record3973541762.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_RecordID41838833.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_QueryResult1174067488.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_RecordDeleteResul3248469484.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_RecordResult4062548349.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iCloudData3080637488.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BaseIOSFeaturePrevie3055692840.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MarketExample1603367874.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MarketExample_U3CSend714520321.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PaymentManagerExampl3283960187.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GameCenterExample2741298119.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GameCenterFriendLoad2699600019.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LeaderboardCustomGUI1008136379.h"
#include "AssemblyU2DCSharpU2Dfirstpass_JSHelper2046037197.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MultiplayerManagerEx3644649635.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TBM_Multiplayer_Exam1159631235.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ConnectionButton3853092004.h"
#include "AssemblyU2DCSharpU2Dfirstpass_DisconnectButton3965161390.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ByteBuffer1012950836.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (GK_TBM_MatchEndResult_t3461768810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[1] = 
{
	GK_TBM_MatchEndResult_t3461768810::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (GK_TBM_MatchInitResult_t3847830897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[1] = 
{
	GK_TBM_MatchInitResult_t3847830897::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (GK_TBM_MatchQuitResult_t1233820656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[1] = 
{
	GK_TBM_MatchQuitResult_t1233820656::get_offset_of__MatchId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (GK_TBM_MatchRemovedResult_t909126313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[1] = 
{
	GK_TBM_MatchRemovedResult_t909126313::get_offset_of__MatchId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (GK_TBM_MatchTurnResult_t3583658160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[1] = 
{
	GK_TBM_MatchTurnResult_t3583658160::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (GK_TBM_RematchResult_t3159773700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[1] = 
{
	GK_TBM_RematchResult_t3159773700::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (GK_TBM_Match_t132033130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[7] = 
{
	GK_TBM_Match_t132033130::get_offset_of_Id_0(),
	GK_TBM_Match_t132033130::get_offset_of_Message_1(),
	GK_TBM_Match_t132033130::get_offset_of_CurrentParticipant_2(),
	GK_TBM_Match_t132033130::get_offset_of_CreationTimestamp_3(),
	GK_TBM_Match_t132033130::get_offset_of_Data_4(),
	GK_TBM_Match_t132033130::get_offset_of_Status_5(),
	GK_TBM_Match_t132033130::get_offset_of_Participants_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (GK_TBM_Participant_t3803955090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[6] = 
{
	GK_TBM_Participant_t3803955090::get_offset_of__PlayerId_0(),
	GK_TBM_Participant_t3803955090::get_offset_of__MatchId_1(),
	GK_TBM_Participant_t3803955090::get_offset_of__TimeoutDate_2(),
	GK_TBM_Participant_t3803955090::get_offset_of__LastTurnDate_3(),
	GK_TBM_Participant_t3803955090::get_offset_of__Status_4(),
	GK_TBM_Participant_t3803955090::get_offset_of__MatchOutcome_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (IOSCamera_t2845108690), -1, sizeof(IOSCamera_t2845108690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2308[8] = 
{
	IOSCamera_t2845108690::get_offset_of__IsWaitngForResponce_4(),
	IOSCamera_t2845108690::get_offset_of__IsInitialized_5(),
	IOSCamera_t2845108690_StaticFields::get_offset_of_OnImagePicked_6(),
	IOSCamera_t2845108690_StaticFields::get_offset_of_OnImageSaved_7(),
	IOSCamera_t2845108690_StaticFields::get_offset_of_OnVideoPathPicked_8(),
	IOSCamera_t2845108690_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
	IOSCamera_t2845108690_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_10(),
	IOSCamera_t2845108690_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (U3CSaveScreenshotU3Ec__Iterator1_t2834900354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[6] = 
{
	U3CSaveScreenshotU3Ec__Iterator1_t2834900354::get_offset_of_U3CwidthU3E__0_0(),
	U3CSaveScreenshotU3Ec__Iterator1_t2834900354::get_offset_of_U3CheightU3E__1_1(),
	U3CSaveScreenshotU3Ec__Iterator1_t2834900354::get_offset_of_U3CtexU3E__2_2(),
	U3CSaveScreenshotU3Ec__Iterator1_t2834900354::get_offset_of_U24PC_3(),
	U3CSaveScreenshotU3Ec__Iterator1_t2834900354::get_offset_of_U24current_4(),
	U3CSaveScreenshotU3Ec__Iterator1_t2834900354::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (ISN_ImageSource_t2773896895)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2310[4] = 
{
	ISN_ImageSource_t2773896895::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (IOSImagePickResult_t1671334394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[1] = 
{
	IOSImagePickResult_t1671334394::get_offset_of__image_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (ISN_FilePicker_t2957423793), -1, sizeof(ISN_FilePicker_t2957423793_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2312[2] = 
{
	ISN_FilePicker_t2957423793_StaticFields::get_offset_of_MediaPickFinished_4(),
	ISN_FilePicker_t2957423793_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (IOSGalleryLoadImageFormat_t518894765)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2313[3] = 
{
	IOSGalleryLoadImageFormat_t518894765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (ISN_FilePickerResult_t2234803986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[1] = 
{
	ISN_FilePickerResult_t2234803986::get_offset_of_PickedImages_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (ISN_MediaController_t1853840745), -1, sizeof(ISN_MediaController_t1853840745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2315[11] = 
{
	ISN_MediaController_t1853840745::get_offset_of__NowPlayingItem_4(),
	ISN_MediaController_t1853840745::get_offset_of__State_5(),
	ISN_MediaController_t1853840745::get_offset_of__CurrentQueue_6(),
	ISN_MediaController_t1853840745_StaticFields::get_offset_of_ActionMediaPickerResult_7(),
	ISN_MediaController_t1853840745_StaticFields::get_offset_of_ActionQueueUpdated_8(),
	ISN_MediaController_t1853840745_StaticFields::get_offset_of_ActionNowPlayingItemChanged_9(),
	ISN_MediaController_t1853840745_StaticFields::get_offset_of_ActionPlaybackStateChanged_10(),
	ISN_MediaController_t1853840745_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
	ISN_MediaController_t1853840745_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_12(),
	ISN_MediaController_t1853840745_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_13(),
	ISN_MediaController_t1853840745_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (MP_MusicPlaybackState_t2364713801)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2316[7] = 
{
	MP_MusicPlaybackState_t2364713801::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (MP_MusicRepeatMode_t445284825)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2317[5] = 
{
	MP_MusicRepeatMode_t445284825::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (MP_MusicShuffleMode_t2068355741)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2318[5] = 
{
	MP_MusicShuffleMode_t2068355741::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (MP_MediaItem_t4025623029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[8] = 
{
	MP_MediaItem_t4025623029::get_offset_of__Id_0(),
	MP_MediaItem_t4025623029::get_offset_of__Title_1(),
	MP_MediaItem_t4025623029::get_offset_of__Artist_2(),
	MP_MediaItem_t4025623029::get_offset_of__AlbumTitle_3(),
	MP_MediaItem_t4025623029::get_offset_of__AlbumArtist_4(),
	MP_MediaItem_t4025623029::get_offset_of__Genre_5(),
	MP_MediaItem_t4025623029::get_offset_of__PlaybackDuration_6(),
	MP_MediaItem_t4025623029::get_offset_of__Composer_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (MP_MediaPickerResult_t2204006871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[1] = 
{
	MP_MediaPickerResult_t2204006871::get_offset_of__SelectedmediaItems_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (IOSVideoManager_t3003649481), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (ISN_ReplayKit_t2994976952), -1, sizeof(ISN_ReplayKit_t2994976952_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2322[13] = 
{
	ISN_ReplayKit_t2994976952::get_offset_of__IsRecodingAvailableToShare_4(),
	ISN_ReplayKit_t2994976952_StaticFields::get_offset_of_ActionRecordStarted_5(),
	ISN_ReplayKit_t2994976952_StaticFields::get_offset_of_ActionRecordStoped_6(),
	ISN_ReplayKit_t2994976952_StaticFields::get_offset_of_ActionShareDialogFinished_7(),
	ISN_ReplayKit_t2994976952_StaticFields::get_offset_of_ActionRecordInterrupted_8(),
	ISN_ReplayKit_t2994976952_StaticFields::get_offset_of_ActionRecorderDidChangeAvailability_9(),
	ISN_ReplayKit_t2994976952_StaticFields::get_offset_of_ActionRecordDiscard_10(),
	ISN_ReplayKit_t2994976952_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
	ISN_ReplayKit_t2994976952_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_12(),
	ISN_ReplayKit_t2994976952_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_13(),
	ISN_ReplayKit_t2994976952_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_14(),
	ISN_ReplayKit_t2994976952_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_15(),
	ISN_ReplayKit_t2994976952_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (ReplayKitVideoShareResult_t2762953534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[1] = 
{
	ReplayKitVideoShareResult_t2762953534::get_offset_of__Sources_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (ISN_LocalNotificationsController_t3778913674), -1, sizeof(ISN_LocalNotificationsController_t3778913674_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2324[7] = 
{
	0,
	0,
	ISN_LocalNotificationsController_t3778913674::get_offset_of__LaunchNotification_6(),
	ISN_LocalNotificationsController_t3778913674_StaticFields::get_offset_of_OnNotificationScheduleResult_7(),
	ISN_LocalNotificationsController_t3778913674_StaticFields::get_offset_of_OnLocalNotificationReceived_8(),
	ISN_LocalNotificationsController_t3778913674_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_9(),
	ISN_LocalNotificationsController_t3778913674_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (ISN_RemoteNotificationsController_t1910006075), -1, sizeof(ISN_RemoteNotificationsController_t1910006075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2325[6] = 
{
	ISN_RemoteNotificationsController_t1910006075_StaticFields::get_offset_of__RegistrationCallback_4(),
	ISN_RemoteNotificationsController_t1910006075::get_offset_of__LaunchNotification_5(),
	ISN_RemoteNotificationsController_t1910006075_StaticFields::get_offset_of_OnDeviceTokenReceived_6(),
	ISN_RemoteNotificationsController_t1910006075_StaticFields::get_offset_of_OnRemoteNotificationReceived_7(),
	ISN_RemoteNotificationsController_t1910006075_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
	ISN_RemoteNotificationsController_t1910006075_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (ISN_NotificationType_t2960395864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (ISN_DeviceToken_t380973950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[2] = 
{
	ISN_DeviceToken_t380973950::get_offset_of__tokenString_0(),
	ISN_DeviceToken_t380973950::get_offset_of__tokenBytes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (ISN_LocalNotification_t273186689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[8] = 
{
	0,
	ISN_LocalNotification_t273186689::get_offset_of__Id_1(),
	ISN_LocalNotification_t273186689::get_offset_of__Date_2(),
	ISN_LocalNotification_t273186689::get_offset_of__Message_3(),
	ISN_LocalNotification_t273186689::get_offset_of__UseSound_4(),
	ISN_LocalNotification_t273186689::get_offset_of__Badges_5(),
	ISN_LocalNotification_t273186689::get_offset_of__Data_6(),
	ISN_LocalNotification_t273186689::get_offset_of__SoundName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (ISN_RemoteNotification_t1449597314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[1] = 
{
	ISN_RemoteNotification_t1449597314::get_offset_of__Body_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (ISN_RemoteNotificationsRegistrationResult_t3335875151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[1] = 
{
	ISN_RemoteNotificationsRegistrationResult_t3335875151::get_offset_of__Token_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (IOSNativePopUpManager_t3689761895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (BaseIOSPopup_t3320937364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[2] = 
{
	BaseIOSPopup_t3320937364::get_offset_of_title_2(),
	BaseIOSPopup_t3320937364::get_offset_of_message_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (IOSDialog_t3518705031), -1, sizeof(IOSDialog_t3518705031_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2333[4] = 
{
	IOSDialog_t3518705031::get_offset_of_yes_4(),
	IOSDialog_t3518705031::get_offset_of_no_5(),
	IOSDialog_t3518705031::get_offset_of_OnComplete_6(),
	IOSDialog_t3518705031_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (IOSMessage_t2569463336), -1, sizeof(IOSMessage_t2569463336_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2334[3] = 
{
	IOSMessage_t2569463336::get_offset_of_ok_4(),
	IOSMessage_t2569463336::get_offset_of_OnComplete_5(),
	IOSMessage_t2569463336_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (IOSRateUsPopUp_t2222998473), -1, sizeof(IOSRateUsPopUp_t2222998473_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2335[5] = 
{
	IOSRateUsPopUp_t2222998473::get_offset_of_rate_4(),
	IOSRateUsPopUp_t2222998473::get_offset_of_remind_5(),
	IOSRateUsPopUp_t2222998473::get_offset_of_declined_6(),
	IOSRateUsPopUp_t2222998473::get_offset_of_OnComplete_7(),
	IOSRateUsPopUp_t2222998473_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (IOSDialogResult_t3739241316)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2336[6] = 
{
	IOSDialogResult_t3739241316::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (IOSSocialManager_t2957403963), -1, sizeof(IOSSocialManager_t2957403963_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2337[14] = 
{
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_OnFacebookPostStart_4(),
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_OnTwitterPostStart_5(),
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_OnInstagramPostStart_6(),
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_OnFacebookPostResult_7(),
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_OnTwitterPostResult_8(),
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_OnInstagramPostResult_9(),
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_OnMailResult_10(),
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_12(),
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_13(),
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_14(),
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_15(),
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_16(),
	IOSSocialManager_t2957403963_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (IOSBillingInitChecker_t4166635083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[1] = 
{
	IOSBillingInitChecker_t4166635083::get_offset_of__listener_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (BillingInitListener_t4162012659), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (IOSInAppPurchaseManager_t644626385), -1, sizeof(IOSInAppPurchaseManager_t644626385_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2340[21] = 
{
	0,
	0,
	IOSInAppPurchaseManager_t644626385::get_offset_of__IsStoreLoaded_6(),
	IOSInAppPurchaseManager_t644626385::get_offset_of__IsWaitingLoadResult_7(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of__nextId_8(),
	IOSInAppPurchaseManager_t644626385::get_offset_of__productsView_9(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_lastPurchasedProduct_10(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_OnRestoreStarted_11(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_OnTransactionStarted_12(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_OnTransactionComplete_13(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_OnRestoreComplete_14(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_OnStoreKitInitComplete_15(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_OnPurchasesStateSettingsLoaded_16(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_OnVerificationComplete_17(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_18(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_19(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_20(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_21(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_22(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_23(),
	IOSInAppPurchaseManager_t644626385_StaticFields::get_offset_of_U3CU3Ef__amU24cache12_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (IOSNativeMarketBridge_t1553898649), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (IOSStoreResultCode_t365779630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (IOSTransactionErrorCode_t1822631322)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2343[10] = 
{
	IOSTransactionErrorCode_t1822631322::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (ISN_InAppPriceTier_t570012276)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2344[88] = 
{
	ISN_InAppPriceTier_t570012276::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (ISN_InAppType_t3300503503)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2345[3] = 
{
	ISN_InAppType_t3300503503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (InAppPurchaseState_t3414001346)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2346[5] = 
{
	InAppPurchaseState_t3414001346::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (IOSStoreProductView_t607200268), -1, sizeof(IOSStoreProductView_t607200268_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2347[10] = 
{
	IOSStoreProductView_t607200268::get_offset_of__ids_0(),
	IOSStoreProductView_t607200268::get_offset_of__id_1(),
	IOSStoreProductView_t607200268::get_offset_of_Loaded_2(),
	IOSStoreProductView_t607200268::get_offset_of_LoadFailed_3(),
	IOSStoreProductView_t607200268::get_offset_of_Appeared_4(),
	IOSStoreProductView_t607200268::get_offset_of_Dismissed_5(),
	IOSStoreProductView_t607200268_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	IOSStoreProductView_t607200268_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
	IOSStoreProductView_t607200268_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
	IOSStoreProductView_t607200268_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (SK_CloudService_t588287925), -1, sizeof(SK_CloudService_t588287925_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2348[6] = 
{
	SK_CloudService_t588287925_StaticFields::get_offset_of_OnAuthorizationFinished_4(),
	SK_CloudService_t588287925_StaticFields::get_offset_of_OnCapabilitiesRequestFinished_5(),
	SK_CloudService_t588287925_StaticFields::get_offset_of_OnStorefrontIdentifierRequestFinished_6(),
	SK_CloudService_t588287925_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	SK_CloudService_t588287925_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
	SK_CloudService_t588287925_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (SK_CloudServiceAuthorizationStatus_t3247563438)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2349[5] = 
{
	SK_CloudServiceAuthorizationStatus_t3247563438::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (SK_CloudServiceCapability_t714629801)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2350[4] = 
{
	SK_CloudServiceCapability_t714629801::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (SK_AuthorizationResult_t2047753871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[1] = 
{
	SK_AuthorizationResult_t2047753871::get_offset_of__AuthorizationStatus_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (SK_RequestCapabilitieResult_t2510163844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[1] = 
{
	SK_RequestCapabilitieResult_t2510163844::get_offset_of__Capability_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (SK_RequestStorefrontIdentifierResult_t1112477162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[1] = 
{
	SK_RequestStorefrontIdentifierResult_t1112477162::get_offset_of__StorefrontIdentifier_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (IOSProductTemplate_t1036598382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[12] = 
{
	IOSProductTemplate_t1036598382::get_offset_of_IsOpen_0(),
	IOSProductTemplate_t1036598382::get_offset_of__IsAvaliable_1(),
	IOSProductTemplate_t1036598382::get_offset_of__Id_2(),
	IOSProductTemplate_t1036598382::get_offset_of__DisplayName_3(),
	IOSProductTemplate_t1036598382::get_offset_of__Description_4(),
	IOSProductTemplate_t1036598382::get_offset_of__Price_5(),
	IOSProductTemplate_t1036598382::get_offset_of__LocalizedPrice_6(),
	IOSProductTemplate_t1036598382::get_offset_of__CurrencySymbol_7(),
	IOSProductTemplate_t1036598382::get_offset_of__CurrencyCode_8(),
	IOSProductTemplate_t1036598382::get_offset_of__Texture_9(),
	IOSProductTemplate_t1036598382::get_offset_of__ProductType_10(),
	IOSProductTemplate_t1036598382::get_offset_of__PriceTier_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (IOSStoreKitRestoreResult_t3305276155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (IOSStoreKitResult_t2359407583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2356[5] = 
{
	IOSStoreKitResult_t2359407583::get_offset_of__ProductIdentifier_1(),
	IOSStoreKitResult_t2359407583::get_offset_of__State_2(),
	IOSStoreKitResult_t2359407583::get_offset_of__Receipt_3(),
	IOSStoreKitResult_t2359407583::get_offset_of__TransactionIdentifier_4(),
	IOSStoreKitResult_t2359407583::get_offset_of__ApplicationUsername_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (IOSStoreKitVerificationResponse_t4263658582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[4] = 
{
	IOSStoreKitVerificationResponse_t4263658582::get_offset_of_status_0(),
	IOSStoreKitVerificationResponse_t4263658582::get_offset_of_receipt_1(),
	IOSStoreKitVerificationResponse_t4263658582::get_offset_of_productIdentifier_2(),
	IOSStoreKitVerificationResponse_t4263658582::get_offset_of_originalJSON_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (IOSNativeAppEvents_t694411788), -1, sizeof(IOSNativeAppEvents_t694411788_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2358[10] = 
{
	IOSNativeAppEvents_t694411788_StaticFields::get_offset_of_OnApplicationDidEnterBackground_4(),
	IOSNativeAppEvents_t694411788_StaticFields::get_offset_of_OnApplicationDidBecomeActive_5(),
	IOSNativeAppEvents_t694411788_StaticFields::get_offset_of_OnApplicationDidReceiveMemoryWarning_6(),
	IOSNativeAppEvents_t694411788_StaticFields::get_offset_of_OnApplicationWillResignActive_7(),
	IOSNativeAppEvents_t694411788_StaticFields::get_offset_of_OnApplicationWillTerminate_8(),
	IOSNativeAppEvents_t694411788_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
	IOSNativeAppEvents_t694411788_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_10(),
	IOSNativeAppEvents_t694411788_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
	IOSNativeAppEvents_t694411788_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_12(),
	IOSNativeAppEvents_t694411788_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (Contact_t4178394798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[4] = 
{
	Contact_t4178394798::get_offset_of_GivenName_0(),
	Contact_t4178394798::get_offset_of_FamilyName_1(),
	Contact_t4178394798::get_offset_of_Emails_2(),
	Contact_t4178394798::get_offset_of_PhoneNumbers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (ContactStore_t160827595), -1, sizeof(ContactStore_t160827595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2360[8] = 
{
	ContactStore_t160827595::get_offset_of_ContactsLoadResult_4(),
	ContactStore_t160827595::get_offset_of_ContactsPickResult_5(),
	ContactStore_t160827595_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	ContactStore_t160827595_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	ContactStore_t160827595_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
	ContactStore_t160827595_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
	ContactStore_t160827595_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_10(),
	ContactStore_t160827595_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (ContactsResult_t1045731284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[1] = 
{
	ContactsResult_t1045731284::get_offset_of__Contacts_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (PhoneNumber_t113354641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[2] = 
{
	PhoneNumber_t113354641::get_offset_of_CountryCode_0(),
	PhoneNumber_t113354641::get_offset_of_Digits_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (IOSDateTimePicker_t849222074), -1, sizeof(IOSDateTimePicker_t849222074_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2363[4] = 
{
	IOSDateTimePicker_t849222074::get_offset_of_OnDateChanged_4(),
	IOSDateTimePicker_t849222074::get_offset_of_OnPickerClosed_5(),
	IOSDateTimePicker_t849222074_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	IOSDateTimePicker_t849222074_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (IOSDateTimePickerMode_t3227591715)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2364[5] = 
{
	IOSDateTimePickerMode_t3227591715::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (ISN_Device_t2325020285), -1, sizeof(ISN_Device_t2325020285_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2365[9] = 
{
	ISN_Device_t2325020285_StaticFields::get_offset_of__CurrentDevice_0(),
	ISN_Device_t2325020285::get_offset_of__Name_1(),
	ISN_Device_t2325020285::get_offset_of__SystemName_2(),
	ISN_Device_t2325020285::get_offset_of__Model_3(),
	ISN_Device_t2325020285::get_offset_of__LocalizedModel_4(),
	ISN_Device_t2325020285::get_offset_of__SystemVersion_5(),
	ISN_Device_t2325020285::get_offset_of__MajorSystemVersion_6(),
	ISN_Device_t2325020285::get_offset_of__InterfaceIdiom_7(),
	ISN_Device_t2325020285::get_offset_of__GUID_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (ISN_DeviceGUID_t1787531836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[2] = 
{
	ISN_DeviceGUID_t1787531836::get_offset_of__Bytes_0(),
	ISN_DeviceGUID_t1787531836::get_offset_of__Base64_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (ISN_InterfaceIdiom_t3178386526)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2367[4] = 
{
	ISN_InterfaceIdiom_t3178386526::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (ISN_GestureRecognizer_t4198098372), -1, sizeof(ISN_GestureRecognizer_t4198098372_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2368[2] = 
{
	ISN_GestureRecognizer_t4198098372::get_offset_of_OnSwipe_4(),
	ISN_GestureRecognizer_t4198098372_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (ISN_SwipeDirection_t768921696)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2369[5] = 
{
	ISN_SwipeDirection_t768921696::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (ISN_Logger_t85856405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (ISN_LocalReceiptResult_t3746327569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2371[2] = 
{
	ISN_LocalReceiptResult_t3746327569::get_offset_of__Receipt_0(),
	ISN_LocalReceiptResult_t3746327569::get_offset_of__ReceiptString_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (ISN_Security_t2700938347), -1, sizeof(ISN_Security_t2700938347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2372[4] = 
{
	ISN_Security_t2700938347_StaticFields::get_offset_of_OnReceiptLoaded_4(),
	ISN_Security_t2700938347_StaticFields::get_offset_of_OnReceiptRefreshComplete_5(),
	ISN_Security_t2700938347_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	ISN_Security_t2700938347_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (IOSSharedApplication_t4065685598), -1, sizeof(IOSSharedApplication_t4065685598_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2373[6] = 
{
	0,
	0,
	IOSSharedApplication_t4065685598_StaticFields::get_offset_of_OnUrlCheckResultAction_6(),
	IOSSharedApplication_t4065685598_StaticFields::get_offset_of_OnAdvertisingIdentifierLoadedAction_7(),
	IOSSharedApplication_t4065685598_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_8(),
	IOSSharedApplication_t4065685598_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (ISN_CheckUrlResult_t1645724501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[1] = 
{
	ISN_CheckUrlResult_t1645724501::get_offset_of__url_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (IOSNativeUtility_t933355194), -1, sizeof(IOSNativeUtility_t933355194_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2375[4] = 
{
	IOSNativeUtility_t933355194_StaticFields::get_offset_of_OnLocaleLoaded_4(),
	IOSNativeUtility_t933355194_StaticFields::get_offset_of_GuidedAccessSessionRequestResult_5(),
	IOSNativeUtility_t933355194_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	IOSNativeUtility_t933355194_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (ISN_Locale_t2162888085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[4] = 
{
	ISN_Locale_t2162888085::get_offset_of__CountryCode_0(),
	ISN_Locale_t2162888085::get_offset_of__DisplayCountry_1(),
	ISN_Locale_t2162888085::get_offset_of__LanguageCode_2(),
	ISN_Locale_t2162888085::get_offset_of__DisplayLanguage_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (ISN_CloudKit_t2197422136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[4] = 
{
	0,
	0,
	ISN_CloudKit_t2197422136::get_offset_of__PrivateDB_6(),
	ISN_CloudKit_t2197422136::get_offset_of__PublicDB_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (iCloudManager_t2506189173), -1, sizeof(iCloudManager_t2506189173_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2378[6] = 
{
	iCloudManager_t2506189173_StaticFields::get_offset_of_OnCloudInitAction_4(),
	iCloudManager_t2506189173_StaticFields::get_offset_of_OnCloudDataReceivedAction_5(),
	iCloudManager_t2506189173_StaticFields::get_offset_of_OnStoreDidChangeExternally_6(),
	iCloudManager_t2506189173_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	iCloudManager_t2506189173_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
	iCloudManager_t2506189173_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (CK_Database_t243306482), -1, sizeof(CK_Database_t243306482_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2379[10] = 
{
	CK_Database_t243306482_StaticFields::get_offset_of__Databases_0(),
	CK_Database_t243306482::get_offset_of__InternalId_1(),
	CK_Database_t243306482::get_offset_of_ActionRecordSaved_2(),
	CK_Database_t243306482::get_offset_of_ActionRecordFetchComplete_3(),
	CK_Database_t243306482::get_offset_of_ActionRecordDeleted_4(),
	CK_Database_t243306482::get_offset_of_ActionQueryComplete_5(),
	CK_Database_t243306482_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	CK_Database_t243306482_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
	CK_Database_t243306482_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
	CK_Database_t243306482_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (CK_Query_t2718018967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[2] = 
{
	CK_Query_t2718018967::get_offset_of__Predicate_0(),
	CK_Query_t2718018967::get_offset_of__RecordType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (CK_Record_t3973541762), -1, sizeof(CK_Record_t3973541762_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2381[5] = 
{
	CK_Record_t3973541762_StaticFields::get_offset_of__Records_0(),
	CK_Record_t3973541762::get_offset_of__Id_1(),
	CK_Record_t3973541762::get_offset_of__Type_2(),
	CK_Record_t3973541762::get_offset_of__Data_3(),
	CK_Record_t3973541762::get_offset_of__internalId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (CK_RecordID_t41838833), -1, sizeof(CK_RecordID_t41838833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2382[3] = 
{
	CK_RecordID_t41838833::get_offset_of__internalId_0(),
	CK_RecordID_t41838833::get_offset_of__Name_1(),
	CK_RecordID_t41838833_StaticFields::get_offset_of__Ids_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (CK_QueryResult_t1174067488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[2] = 
{
	CK_QueryResult_t1174067488::get_offset_of__Database_1(),
	CK_QueryResult_t1174067488::get_offset_of__Records_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (CK_RecordDeleteResult_t3248469484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[2] = 
{
	CK_RecordDeleteResult_t3248469484::get_offset_of__RecordID_1(),
	CK_RecordDeleteResult_t3248469484::get_offset_of__Database_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (CK_RecordResult_t4062548349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[2] = 
{
	CK_RecordResult_t4062548349::get_offset_of__Record_1(),
	CK_RecordResult_t4062548349::get_offset_of__Database_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (iCloudData_t3080637488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[3] = 
{
	iCloudData_t3080637488::get_offset_of__key_0(),
	iCloudData_t3080637488::get_offset_of__val_1(),
	iCloudData_t3080637488::get_offset_of__IsEmpty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (BaseIOSFeaturePreview_t3055692840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[10] = 
{
	BaseIOSFeaturePreview_t3055692840::get_offset_of_style_2(),
	BaseIOSFeaturePreview_t3055692840::get_offset_of_buttonWidth_3(),
	BaseIOSFeaturePreview_t3055692840::get_offset_of_buttonHeight_4(),
	BaseIOSFeaturePreview_t3055692840::get_offset_of_StartY_5(),
	BaseIOSFeaturePreview_t3055692840::get_offset_of_StartX_6(),
	BaseIOSFeaturePreview_t3055692840::get_offset_of_XStartPos_7(),
	BaseIOSFeaturePreview_t3055692840::get_offset_of_YStartPos_8(),
	BaseIOSFeaturePreview_t3055692840::get_offset_of_XButtonStep_9(),
	BaseIOSFeaturePreview_t3055692840::get_offset_of_YButtonStep_10(),
	BaseIOSFeaturePreview_t3055692840::get_offset_of_YLableStep_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (MarketExample_t1603367874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[1] = 
{
	MarketExample_t1603367874::get_offset_of_ReceiptData_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (U3CSendRequestU3Ec__Iterator2_t714520321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[8] = 
{
	U3CSendRequestU3Ec__Iterator2_t714520321::get_offset_of_U3Cbase64stringU3E__0_0(),
	U3CSendRequestU3Ec__Iterator2_t714520321::get_offset_of_U3COriginalJSONU3E__1_1(),
	U3CSendRequestU3Ec__Iterator2_t714520321::get_offset_of_U3CdataU3E__2_2(),
	U3CSendRequestU3Ec__Iterator2_t714520321::get_offset_of_U3CbinaryDataU3E__3_3(),
	U3CSendRequestU3Ec__Iterator2_t714520321::get_offset_of_U3CwwwU3E__4_4(),
	U3CSendRequestU3Ec__Iterator2_t714520321::get_offset_of_U24PC_5(),
	U3CSendRequestU3Ec__Iterator2_t714520321::get_offset_of_U24current_6(),
	U3CSendRequestU3Ec__Iterator2_t714520321::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (PaymentManagerExample_t3283960187), -1, sizeof(PaymentManagerExample_t3283960187_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2390[4] = 
{
	0,
	0,
	PaymentManagerExample_t3283960187_StaticFields::get_offset_of_IsInitialized_2(),
	PaymentManagerExample_t3283960187_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (GameCenterExample_t2741298119), -1, sizeof(GameCenterExample_t2741298119_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2391[7] = 
{
	GameCenterExample_t2741298119::get_offset_of_hiScore_12(),
	GameCenterExample_t2741298119::get_offset_of_TEST_LEADERBOARD_1_13(),
	GameCenterExample_t2741298119::get_offset_of_TEST_LEADERBOARD_2_14(),
	GameCenterExample_t2741298119::get_offset_of_TEST_ACHIEVEMENT_1_ID_15(),
	GameCenterExample_t2741298119::get_offset_of_TEST_ACHIEVEMENT_2_ID_16(),
	GameCenterExample_t2741298119_StaticFields::get_offset_of_IsInitialized_17(),
	GameCenterExample_t2741298119_StaticFields::get_offset_of_LB2BestScores_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (GameCenterFriendLoadExample_t2699600019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[5] = 
{
	GameCenterFriendLoadExample_t2699600019::get_offset_of_ChallengeLeaderboard_2(),
	GameCenterFriendLoadExample_t2699600019::get_offset_of_ChallengeAchievement_3(),
	GameCenterFriendLoadExample_t2699600019::get_offset_of_headerStyle_4(),
	GameCenterFriendLoadExample_t2699600019::get_offset_of_boardStyle_5(),
	GameCenterFriendLoadExample_t2699600019::get_offset_of_renderFriendsList_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (LeaderboardCustomGUIExample_t1008136379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[6] = 
{
	LeaderboardCustomGUIExample_t1008136379::get_offset_of_leaderboardId_1_2(),
	LeaderboardCustomGUIExample_t1008136379::get_offset_of_hiScore_3(),
	LeaderboardCustomGUIExample_t1008136379::get_offset_of_headerStyle_4(),
	LeaderboardCustomGUIExample_t1008136379::get_offset_of_boardStyle_5(),
	LeaderboardCustomGUIExample_t1008136379::get_offset_of_loadedLeaderboard_6(),
	LeaderboardCustomGUIExample_t1008136379::get_offset_of_displayCollection_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (JSHelper_t2046037197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[3] = 
{
	JSHelper_t2046037197::get_offset_of_leaderboardId_2(),
	JSHelper_t2046037197::get_offset_of_TEST_ACHIEVEMENT_1_ID_3(),
	JSHelper_t2046037197::get_offset_of_TEST_ACHIEVEMENT_2_ID_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (MultiplayerManagerExample_t3644649635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (TBM_Multiplayer_Example_t1159631235), -1, sizeof(TBM_Multiplayer_Example_t1159631235_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2396[1] = 
{
	TBM_Multiplayer_Example_t1159631235_StaticFields::get_offset_of_IsInitialized_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (ConnectionButton_t3853092004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[3] = 
{
	ConnectionButton_t3853092004::get_offset_of_w_2(),
	ConnectionButton_t3853092004::get_offset_of_h_3(),
	ConnectionButton_t3853092004::get_offset_of_r_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (DisconnectButton_t3965161390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[3] = 
{
	DisconnectButton_t3965161390::get_offset_of_w_2(),
	DisconnectButton_t3965161390::get_offset_of_h_3(),
	DisconnectButton_t3965161390::get_offset_of_r_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (ByteBuffer_t1012950836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[2] = 
{
	ByteBuffer_t1012950836::get_offset_of_buffer_0(),
	ByteBuffer_t1012950836::get_offset_of_pointer_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
