﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageSenderBridge
struct MessageSenderBridge_t908748265;

#include "codegen/il2cpp-codegen.h"

// System.Void MessageSenderBridge::.ctor()
extern "C"  void MessageSenderBridge__ctor_m1972359798 (MessageSenderBridge_t908748265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MessageSenderBridge MessageSenderBridge::GetInstance()
extern "C"  MessageSenderBridge_t908748265 * MessageSenderBridge_GetInstance_m496211055 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSenderBridge::AwakeOverride()
extern "C"  void MessageSenderBridge_AwakeOverride_m3713606593 (MessageSenderBridge_t908748265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageSenderBridge::Start()
extern "C"  void MessageSenderBridge_Start_m1744728174 (MessageSenderBridge_t908748265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
