﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnvironmentListItem
struct EnvironmentListItem_t1261052282;

#include "codegen/il2cpp-codegen.h"

// System.Void EnvironmentListItem::.ctor()
extern "C"  void EnvironmentListItem__ctor_m1106686309 (EnvironmentListItem_t1261052282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnvironmentListItem::SetLocked(System.Boolean)
extern "C"  void EnvironmentListItem_SetLocked_m2379447198 (EnvironmentListItem_t1261052282 * __this, bool ___locked0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnvironmentListItem::SetSelected(System.Boolean)
extern "C"  void EnvironmentListItem_SetSelected_m2453520481 (EnvironmentListItem_t1261052282 * __this, bool ___selected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
