﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen3777177449MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>::.ctor()
#define Stack_1__ctor_m2667244830(__this, method) ((  void (*) (Stack_1_t3586864480 *, const MethodInfo*))Stack_1__ctor_m1041657164_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m1295335830(__this, method) ((  bool (*) (Stack_1_t3586864480 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m2076161108_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m2239419258(__this, method) ((  Il2CppObject * (*) (Stack_1_t3586864480 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m3151629354_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m1661821530(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t3586864480 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m2104527616_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3199464464(__this, method) ((  Il2CppObject* (*) (Stack_1_t3586864480 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m680979874_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m1560984627(__this, method) ((  Il2CppObject * (*) (Stack_1_t3586864480 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m3875192475_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>::Contains(T)
#define Stack_1_Contains_m4257867169(__this, ___t0, method) ((  bool (*) (Stack_1_t3586864480 *, Node_t2499136326 *, const MethodInfo*))Stack_1_Contains_m973625077_gshared)(__this, ___t0, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>::Peek()
#define Stack_1_Peek_m4196035282(__this, method) ((  Node_t2499136326 * (*) (Stack_1_t3586864480 *, const MethodInfo*))Stack_1_Peek_m1548778538_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>::Pop()
#define Stack_1_Pop_m3059123758(__this, method) ((  Node_t2499136326 * (*) (Stack_1_t3586864480 *, const MethodInfo*))Stack_1_Pop_m535185982_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>::Push(T)
#define Stack_1_Push_m147339882(__this, ___t0, method) ((  void (*) (Stack_1_t3586864480 *, Node_t2499136326 *, const MethodInfo*))Stack_1_Push_m2122392216_gshared)(__this, ___t0, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>::get_Count()
#define Stack_1_get_Count_m4266702390(__this, method) ((  int32_t (*) (Stack_1_t3586864480 *, const MethodInfo*))Stack_1_get_Count_m4101767244_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>::GetEnumerator()
#define Stack_1_GetEnumerator_m3779619808(__this, method) ((  Enumerator_t4236862840  (*) (Stack_1_t3586864480 *, const MethodInfo*))Stack_1_GetEnumerator_m287848754_gshared)(__this, method)
