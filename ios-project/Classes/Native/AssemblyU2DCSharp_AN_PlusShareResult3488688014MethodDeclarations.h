﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_PlusShareResult
struct AN_PlusShareResult_t3488688014;

#include "codegen/il2cpp-codegen.h"

// System.Void AN_PlusShareResult::.ctor(System.Boolean)
extern "C"  void AN_PlusShareResult__ctor_m2110572500 (AN_PlusShareResult_t3488688014 * __this, bool ___IsResultSucceeded0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
