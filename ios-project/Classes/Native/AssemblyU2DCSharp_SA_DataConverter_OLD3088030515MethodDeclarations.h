﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA_DataConverter_OLD
struct SA_DataConverter_OLD_t3088030515;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SA_DataConverter_OLD::.ctor()
extern "C"  void SA_DataConverter_OLD__ctor_m2655794136 (SA_DataConverter_OLD_t3088030515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA_DataConverter_OLD::SerializeArray(System.String[],System.String)
extern "C"  String_t* SA_DataConverter_OLD_SerializeArray_m134231298 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___array0, String_t* ___splitter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] SA_DataConverter_OLD::ParseArray(System.String,System.String)
extern "C"  StringU5BU5D_t1642385972* SA_DataConverter_OLD_ParseArray_m2894199541 (Il2CppObject * __this /* static, unused */, String_t* ___arrayData0, String_t* ___splitter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
