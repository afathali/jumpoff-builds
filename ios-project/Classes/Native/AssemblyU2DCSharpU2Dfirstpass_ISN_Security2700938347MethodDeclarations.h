﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_Security
struct ISN_Security_t2700938347;
// System.Action`1<ISN_LocalReceiptResult>
struct Action_1_t3548126951;
// System.Action`1<SA.Common.Models.Result>
struct Action_1_t4089019125;
// System.String
struct String_t;
// ISN_LocalReceiptResult
struct ISN_LocalReceiptResult_t3746327569;
// SA.Common.Models.Result
struct Result_t4287219743;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_LocalReceiptResu3746327569.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"

// System.Void ISN_Security::.ctor()
extern "C"  void ISN_Security__ctor_m2176444882 (ISN_Security_t2700938347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::.cctor()
extern "C"  void ISN_Security__cctor_m3400570707 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::add_OnReceiptLoaded(System.Action`1<ISN_LocalReceiptResult>)
extern "C"  void ISN_Security_add_OnReceiptLoaded_m2771649037 (Il2CppObject * __this /* static, unused */, Action_1_t3548126951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::remove_OnReceiptLoaded(System.Action`1<ISN_LocalReceiptResult>)
extern "C"  void ISN_Security_remove_OnReceiptLoaded_m2650196956 (Il2CppObject * __this /* static, unused */, Action_1_t3548126951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::add_OnReceiptRefreshComplete(System.Action`1<SA.Common.Models.Result>)
extern "C"  void ISN_Security_add_OnReceiptRefreshComplete_m855636605 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::remove_OnReceiptRefreshComplete(System.Action`1<SA.Common.Models.Result>)
extern "C"  void ISN_Security_remove_OnReceiptRefreshComplete_m3266669288 (Il2CppObject * __this /* static, unused */, Action_1_t4089019125 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::Awake()
extern "C"  void ISN_Security_Awake_m1544824285 (ISN_Security_t2700938347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::RetrieveLocalReceipt()
extern "C"  void ISN_Security_RetrieveLocalReceipt_m3685899563 (ISN_Security_t2700938347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::StartReceiptRefreshRequest()
extern "C"  void ISN_Security_StartReceiptRefreshRequest_m3979201376 (ISN_Security_t2700938347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::Event_ReceiptLoaded(System.String)
extern "C"  void ISN_Security_Event_ReceiptLoaded_m3133503438 (ISN_Security_t2700938347 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::Event_ReceiptRefreshRequestReceived(System.String)
extern "C"  void ISN_Security_Event_ReceiptRefreshRequestReceived_m2711032368 (ISN_Security_t2700938347 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::<OnReceiptLoaded>m__66(ISN_LocalReceiptResult)
extern "C"  void ISN_Security_U3COnReceiptLoadedU3Em__66_m4232820580 (Il2CppObject * __this /* static, unused */, ISN_LocalReceiptResult_t3746327569 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_Security::<OnReceiptRefreshComplete>m__67(SA.Common.Models.Result)
extern "C"  void ISN_Security_U3COnReceiptRefreshCompleteU3Em__67_m2299014035 (Il2CppObject * __this /* static, unused */, Result_t4287219743 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
