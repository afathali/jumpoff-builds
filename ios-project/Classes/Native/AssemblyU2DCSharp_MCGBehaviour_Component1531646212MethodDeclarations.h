﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MCGBehaviour/Component
struct Component_t1531646212;
// System.Type
struct Type_t;
// System.String
struct String_t;
// MCGBehaviour/Component/ComponentLifestyle
struct ComponentLifestyle_t4170640987;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_MCGBehaviour_Component_Component4170640987.h"
#include "AssemblyU2DCSharp_MCGBehaviour_LifestyleType936921768.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void MCGBehaviour/Component::.ctor(System.Type)
extern "C"  void Component__ctor_m348519504 (Component_t1531646212 * __this, Type_t * ___forType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type MCGBehaviour/Component::get_ForType()
extern "C"  Type_t * Component_get_ForType_m3369861705 (Component_t1531646212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour/Component::set_ForType(System.Type)
extern "C"  void Component_set_ForType_m3913999068 (Component_t1531646212 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type MCGBehaviour/Component::get_ImplementedByType()
extern "C"  Type_t * Component_get_ImplementedByType_m3265086631 (Component_t1531646212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour/Component::set_ImplementedByType(System.Type)
extern "C"  void Component_set_ImplementedByType_m2270669486 (Component_t1531646212 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MCGBehaviour/Component::get_Name()
extern "C"  String_t* Component_get_Name_m2381749228 (Component_t1531646212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour/Component::set_Name(System.String)
extern "C"  void Component_set_Name_m2404732999 (Component_t1531646212 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MCGBehaviour/Component/ComponentLifestyle MCGBehaviour/Component::get_Lifestyle()
extern "C"  ComponentLifestyle_t4170640987 * Component_get_Lifestyle_m3510089189 (Component_t1531646212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour/Component::set_Lifestyle(MCGBehaviour/Component/ComponentLifestyle)
extern "C"  void Component_set_Lifestyle_m2468050790 (Component_t1531646212 * __this, ComponentLifestyle_t4170640987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MCGBehaviour/LifestyleType MCGBehaviour/Component::get_LifestyleType()
extern "C"  int32_t Component_get_LifestyleType_m3142371058 (Component_t1531646212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour/Component::set_LifestyleType(MCGBehaviour/LifestyleType)
extern "C"  void Component_set_LifestyleType_m1277571837 (Component_t1531646212 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MCGBehaviour/Component::get_Instance()
extern "C"  Il2CppObject * Component_get_Instance_m399813170 (Component_t1531646212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MCGBehaviour/Component::set_Instance(System.Object)
extern "C"  void Component_set_Instance_m372560121 (Component_t1531646212 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MCGBehaviour/Component MCGBehaviour/Component::Named(System.String)
extern "C"  Component_t1531646212 * Component_Named_m52368289 (Component_t1531646212 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MCGBehaviour/Component MCGBehaviour/Component::SetLifestyle(MCGBehaviour/LifestyleType)
extern "C"  Component_t1531646212 * Component_SetLifestyle_m2612535293 (Component_t1531646212 * __this, int32_t ___lifestyleType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
