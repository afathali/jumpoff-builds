﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vector3Converter
struct Vector3Converter_t3052510548;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t1973729997;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t1719617802;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonReader
struct JsonReader_t3154730733;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter1973729997.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer1719617802.h"
#include "mscorlib_System_Type1303803226.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader3154730733.h"

// System.Void Vector3Converter::.ctor()
extern "C"  void Vector3Converter__ctor_m653849197 (Vector3Converter_t3052510548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vector3Converter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void Vector3Converter_WriteJson_m814190525 (Vector3Converter_t3052510548 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t1719617802 * ___serializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vector3Converter::CanConvert(System.Type)
extern "C"  bool Vector3Converter_CanConvert_m2730517481 (Vector3Converter_t3052510548 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Vector3Converter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * Vector3Converter_ReadJson_m1133402306 (Vector3Converter_t3052510548 * __this, JsonReader_t3154730733 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t1719617802 * ___serializer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vector3Converter::get_CanRead()
extern "C"  bool Vector3Converter_get_CanRead_m4040637396 (Vector3Converter_t3052510548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
