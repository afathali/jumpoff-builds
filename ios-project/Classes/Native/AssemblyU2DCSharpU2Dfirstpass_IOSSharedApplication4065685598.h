﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<ISN_CheckUrlResult>
struct Action_1_t1447523883;
// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Pattern_Sin253283407.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IOSSharedApplication
struct  IOSSharedApplication_t4065685598  : public Singleton_1_t253283407
{
public:

public:
};

struct IOSSharedApplication_t4065685598_StaticFields
{
public:
	// System.Action`1<ISN_CheckUrlResult> IOSSharedApplication::OnUrlCheckResultAction
	Action_1_t1447523883 * ___OnUrlCheckResultAction_6;
	// System.Action`1<System.String> IOSSharedApplication::OnAdvertisingIdentifierLoadedAction
	Action_1_t1831019615 * ___OnAdvertisingIdentifierLoadedAction_7;
	// System.Action`1<ISN_CheckUrlResult> IOSSharedApplication::<>f__am$cache2
	Action_1_t1447523883 * ___U3CU3Ef__amU24cache2_8;
	// System.Action`1<System.String> IOSSharedApplication::<>f__am$cache3
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache3_9;

public:
	inline static int32_t get_offset_of_OnUrlCheckResultAction_6() { return static_cast<int32_t>(offsetof(IOSSharedApplication_t4065685598_StaticFields, ___OnUrlCheckResultAction_6)); }
	inline Action_1_t1447523883 * get_OnUrlCheckResultAction_6() const { return ___OnUrlCheckResultAction_6; }
	inline Action_1_t1447523883 ** get_address_of_OnUrlCheckResultAction_6() { return &___OnUrlCheckResultAction_6; }
	inline void set_OnUrlCheckResultAction_6(Action_1_t1447523883 * value)
	{
		___OnUrlCheckResultAction_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnUrlCheckResultAction_6, value);
	}

	inline static int32_t get_offset_of_OnAdvertisingIdentifierLoadedAction_7() { return static_cast<int32_t>(offsetof(IOSSharedApplication_t4065685598_StaticFields, ___OnAdvertisingIdentifierLoadedAction_7)); }
	inline Action_1_t1831019615 * get_OnAdvertisingIdentifierLoadedAction_7() const { return ___OnAdvertisingIdentifierLoadedAction_7; }
	inline Action_1_t1831019615 ** get_address_of_OnAdvertisingIdentifierLoadedAction_7() { return &___OnAdvertisingIdentifierLoadedAction_7; }
	inline void set_OnAdvertisingIdentifierLoadedAction_7(Action_1_t1831019615 * value)
	{
		___OnAdvertisingIdentifierLoadedAction_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnAdvertisingIdentifierLoadedAction_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_8() { return static_cast<int32_t>(offsetof(IOSSharedApplication_t4065685598_StaticFields, ___U3CU3Ef__amU24cache2_8)); }
	inline Action_1_t1447523883 * get_U3CU3Ef__amU24cache2_8() const { return ___U3CU3Ef__amU24cache2_8; }
	inline Action_1_t1447523883 ** get_address_of_U3CU3Ef__amU24cache2_8() { return &___U3CU3Ef__amU24cache2_8; }
	inline void set_U3CU3Ef__amU24cache2_8(Action_1_t1447523883 * value)
	{
		___U3CU3Ef__amU24cache2_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_9() { return static_cast<int32_t>(offsetof(IOSSharedApplication_t4065685598_StaticFields, ___U3CU3Ef__amU24cache3_9)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache3_9() const { return ___U3CU3Ef__amU24cache3_9; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache3_9() { return &___U3CU3Ef__amU24cache3_9; }
	inline void set_U3CU3Ef__amU24cache3_9(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache3_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
