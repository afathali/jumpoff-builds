﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// DefaultPreviewButton
struct DefaultPreviewButton_t12674677;
// SA_Label
struct SA_Label_t226960149;
// DefaultPreviewButton[]
struct DefaultPreviewButtonU5BU5D_t3800739864;

#include "AssemblyU2DCSharp_AndroidNativeExampleBase3916638757.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayServiceExample
struct  PlayServiceExample_t1159077439  : public AndroidNativeExampleBase_t3916638757
{
public:
	// System.Int32 PlayServiceExample::score
	int32_t ___score_6;
	// UnityEngine.GameObject PlayServiceExample::avatar
	GameObject_t1756533147 * ___avatar_7;
	// UnityEngine.Texture PlayServiceExample::defaulttexture
	Texture_t2243626319 * ___defaulttexture_8;
	// UnityEngine.Texture2D PlayServiceExample::pieIcon
	Texture2D_t3542995729 * ___pieIcon_9;
	// DefaultPreviewButton PlayServiceExample::connectButton
	DefaultPreviewButton_t12674677 * ___connectButton_10;
	// DefaultPreviewButton PlayServiceExample::scoreSubmit
	DefaultPreviewButton_t12674677 * ___scoreSubmit_11;
	// SA_Label PlayServiceExample::playerLabel
	SA_Label_t226960149 * ___playerLabel_12;
	// DefaultPreviewButton[] PlayServiceExample::ConnectionDependedntButtons
	DefaultPreviewButtonU5BU5D_t3800739864* ___ConnectionDependedntButtons_13;
	// SA_Label PlayServiceExample::a_id
	SA_Label_t226960149 * ___a_id_14;
	// SA_Label PlayServiceExample::a_name
	SA_Label_t226960149 * ___a_name_15;
	// SA_Label PlayServiceExample::a_descr
	SA_Label_t226960149 * ___a_descr_16;
	// SA_Label PlayServiceExample::a_type
	SA_Label_t226960149 * ___a_type_17;
	// SA_Label PlayServiceExample::a_state
	SA_Label_t226960149 * ___a_state_18;
	// SA_Label PlayServiceExample::a_steps
	SA_Label_t226960149 * ___a_steps_19;
	// SA_Label PlayServiceExample::a_total
	SA_Label_t226960149 * ___a_total_20;
	// SA_Label PlayServiceExample::b_id
	SA_Label_t226960149 * ___b_id_21;
	// SA_Label PlayServiceExample::b_name
	SA_Label_t226960149 * ___b_name_22;
	// SA_Label PlayServiceExample::b_all_time
	SA_Label_t226960149 * ___b_all_time_23;

public:
	inline static int32_t get_offset_of_score_6() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___score_6)); }
	inline int32_t get_score_6() const { return ___score_6; }
	inline int32_t* get_address_of_score_6() { return &___score_6; }
	inline void set_score_6(int32_t value)
	{
		___score_6 = value;
	}

	inline static int32_t get_offset_of_avatar_7() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___avatar_7)); }
	inline GameObject_t1756533147 * get_avatar_7() const { return ___avatar_7; }
	inline GameObject_t1756533147 ** get_address_of_avatar_7() { return &___avatar_7; }
	inline void set_avatar_7(GameObject_t1756533147 * value)
	{
		___avatar_7 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_7, value);
	}

	inline static int32_t get_offset_of_defaulttexture_8() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___defaulttexture_8)); }
	inline Texture_t2243626319 * get_defaulttexture_8() const { return ___defaulttexture_8; }
	inline Texture_t2243626319 ** get_address_of_defaulttexture_8() { return &___defaulttexture_8; }
	inline void set_defaulttexture_8(Texture_t2243626319 * value)
	{
		___defaulttexture_8 = value;
		Il2CppCodeGenWriteBarrier(&___defaulttexture_8, value);
	}

	inline static int32_t get_offset_of_pieIcon_9() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___pieIcon_9)); }
	inline Texture2D_t3542995729 * get_pieIcon_9() const { return ___pieIcon_9; }
	inline Texture2D_t3542995729 ** get_address_of_pieIcon_9() { return &___pieIcon_9; }
	inline void set_pieIcon_9(Texture2D_t3542995729 * value)
	{
		___pieIcon_9 = value;
		Il2CppCodeGenWriteBarrier(&___pieIcon_9, value);
	}

	inline static int32_t get_offset_of_connectButton_10() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___connectButton_10)); }
	inline DefaultPreviewButton_t12674677 * get_connectButton_10() const { return ___connectButton_10; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_connectButton_10() { return &___connectButton_10; }
	inline void set_connectButton_10(DefaultPreviewButton_t12674677 * value)
	{
		___connectButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___connectButton_10, value);
	}

	inline static int32_t get_offset_of_scoreSubmit_11() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___scoreSubmit_11)); }
	inline DefaultPreviewButton_t12674677 * get_scoreSubmit_11() const { return ___scoreSubmit_11; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_scoreSubmit_11() { return &___scoreSubmit_11; }
	inline void set_scoreSubmit_11(DefaultPreviewButton_t12674677 * value)
	{
		___scoreSubmit_11 = value;
		Il2CppCodeGenWriteBarrier(&___scoreSubmit_11, value);
	}

	inline static int32_t get_offset_of_playerLabel_12() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___playerLabel_12)); }
	inline SA_Label_t226960149 * get_playerLabel_12() const { return ___playerLabel_12; }
	inline SA_Label_t226960149 ** get_address_of_playerLabel_12() { return &___playerLabel_12; }
	inline void set_playerLabel_12(SA_Label_t226960149 * value)
	{
		___playerLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___playerLabel_12, value);
	}

	inline static int32_t get_offset_of_ConnectionDependedntButtons_13() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___ConnectionDependedntButtons_13)); }
	inline DefaultPreviewButtonU5BU5D_t3800739864* get_ConnectionDependedntButtons_13() const { return ___ConnectionDependedntButtons_13; }
	inline DefaultPreviewButtonU5BU5D_t3800739864** get_address_of_ConnectionDependedntButtons_13() { return &___ConnectionDependedntButtons_13; }
	inline void set_ConnectionDependedntButtons_13(DefaultPreviewButtonU5BU5D_t3800739864* value)
	{
		___ConnectionDependedntButtons_13 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionDependedntButtons_13, value);
	}

	inline static int32_t get_offset_of_a_id_14() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___a_id_14)); }
	inline SA_Label_t226960149 * get_a_id_14() const { return ___a_id_14; }
	inline SA_Label_t226960149 ** get_address_of_a_id_14() { return &___a_id_14; }
	inline void set_a_id_14(SA_Label_t226960149 * value)
	{
		___a_id_14 = value;
		Il2CppCodeGenWriteBarrier(&___a_id_14, value);
	}

	inline static int32_t get_offset_of_a_name_15() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___a_name_15)); }
	inline SA_Label_t226960149 * get_a_name_15() const { return ___a_name_15; }
	inline SA_Label_t226960149 ** get_address_of_a_name_15() { return &___a_name_15; }
	inline void set_a_name_15(SA_Label_t226960149 * value)
	{
		___a_name_15 = value;
		Il2CppCodeGenWriteBarrier(&___a_name_15, value);
	}

	inline static int32_t get_offset_of_a_descr_16() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___a_descr_16)); }
	inline SA_Label_t226960149 * get_a_descr_16() const { return ___a_descr_16; }
	inline SA_Label_t226960149 ** get_address_of_a_descr_16() { return &___a_descr_16; }
	inline void set_a_descr_16(SA_Label_t226960149 * value)
	{
		___a_descr_16 = value;
		Il2CppCodeGenWriteBarrier(&___a_descr_16, value);
	}

	inline static int32_t get_offset_of_a_type_17() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___a_type_17)); }
	inline SA_Label_t226960149 * get_a_type_17() const { return ___a_type_17; }
	inline SA_Label_t226960149 ** get_address_of_a_type_17() { return &___a_type_17; }
	inline void set_a_type_17(SA_Label_t226960149 * value)
	{
		___a_type_17 = value;
		Il2CppCodeGenWriteBarrier(&___a_type_17, value);
	}

	inline static int32_t get_offset_of_a_state_18() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___a_state_18)); }
	inline SA_Label_t226960149 * get_a_state_18() const { return ___a_state_18; }
	inline SA_Label_t226960149 ** get_address_of_a_state_18() { return &___a_state_18; }
	inline void set_a_state_18(SA_Label_t226960149 * value)
	{
		___a_state_18 = value;
		Il2CppCodeGenWriteBarrier(&___a_state_18, value);
	}

	inline static int32_t get_offset_of_a_steps_19() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___a_steps_19)); }
	inline SA_Label_t226960149 * get_a_steps_19() const { return ___a_steps_19; }
	inline SA_Label_t226960149 ** get_address_of_a_steps_19() { return &___a_steps_19; }
	inline void set_a_steps_19(SA_Label_t226960149 * value)
	{
		___a_steps_19 = value;
		Il2CppCodeGenWriteBarrier(&___a_steps_19, value);
	}

	inline static int32_t get_offset_of_a_total_20() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___a_total_20)); }
	inline SA_Label_t226960149 * get_a_total_20() const { return ___a_total_20; }
	inline SA_Label_t226960149 ** get_address_of_a_total_20() { return &___a_total_20; }
	inline void set_a_total_20(SA_Label_t226960149 * value)
	{
		___a_total_20 = value;
		Il2CppCodeGenWriteBarrier(&___a_total_20, value);
	}

	inline static int32_t get_offset_of_b_id_21() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___b_id_21)); }
	inline SA_Label_t226960149 * get_b_id_21() const { return ___b_id_21; }
	inline SA_Label_t226960149 ** get_address_of_b_id_21() { return &___b_id_21; }
	inline void set_b_id_21(SA_Label_t226960149 * value)
	{
		___b_id_21 = value;
		Il2CppCodeGenWriteBarrier(&___b_id_21, value);
	}

	inline static int32_t get_offset_of_b_name_22() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___b_name_22)); }
	inline SA_Label_t226960149 * get_b_name_22() const { return ___b_name_22; }
	inline SA_Label_t226960149 ** get_address_of_b_name_22() { return &___b_name_22; }
	inline void set_b_name_22(SA_Label_t226960149 * value)
	{
		___b_name_22 = value;
		Il2CppCodeGenWriteBarrier(&___b_name_22, value);
	}

	inline static int32_t get_offset_of_b_all_time_23() { return static_cast<int32_t>(offsetof(PlayServiceExample_t1159077439, ___b_all_time_23)); }
	inline SA_Label_t226960149 * get_b_all_time_23() const { return ___b_all_time_23; }
	inline SA_Label_t226960149 ** get_address_of_b_all_time_23() { return &___b_all_time_23; }
	inline void set_b_all_time_23(SA_Label_t226960149 * value)
	{
		___b_all_time_23 = value;
		Il2CppCodeGenWriteBarrier(&___b_all_time_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
