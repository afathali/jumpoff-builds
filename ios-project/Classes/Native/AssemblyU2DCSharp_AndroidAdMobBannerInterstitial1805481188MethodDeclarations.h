﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidAdMobBannerInterstitial
struct AndroidAdMobBannerInterstitial_t1805481188;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidAdMobBannerInterstitial::.ctor()
extern "C"  void AndroidAdMobBannerInterstitial__ctor_m2369087011 (AndroidAdMobBannerInterstitial_t1805481188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobBannerInterstitial::Awake()
extern "C"  void AndroidAdMobBannerInterstitial_Awake_m1735375472 (AndroidAdMobBannerInterstitial_t1805481188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobBannerInterstitial::Start()
extern "C"  void AndroidAdMobBannerInterstitial_Start_m3815239055 (AndroidAdMobBannerInterstitial_t1805481188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidAdMobBannerInterstitial::ShowBanner()
extern "C"  void AndroidAdMobBannerInterstitial_ShowBanner_m1187971780 (AndroidAdMobBannerInterstitial_t1805481188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidAdMobBannerInterstitial::get_sceneBannerId()
extern "C"  String_t* AndroidAdMobBannerInterstitial_get_sceneBannerId_m1567046060 (AndroidAdMobBannerInterstitial_t1805481188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
