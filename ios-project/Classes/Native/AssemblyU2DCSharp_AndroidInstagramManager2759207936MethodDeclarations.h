﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidInstagramManager
struct AndroidInstagramManager_t2759207936;
// System.Action`1<InstagramPostResult>
struct Action_1_t237882577;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_InstagramPostResult436083195.h"

// System.Void AndroidInstagramManager::.ctor()
extern "C"  void AndroidInstagramManager__ctor_m3531791095 (AndroidInstagramManager_t2759207936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInstagramManager::.cctor()
extern "C"  void AndroidInstagramManager__cctor_m2874624918 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInstagramManager::add_OnPostingCompleteAction(System.Action`1<InstagramPostResult>)
extern "C"  void AndroidInstagramManager_add_OnPostingCompleteAction_m2993774626 (Il2CppObject * __this /* static, unused */, Action_1_t237882577 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInstagramManager::remove_OnPostingCompleteAction(System.Action`1<InstagramPostResult>)
extern "C"  void AndroidInstagramManager_remove_OnPostingCompleteAction_m2101366827 (Il2CppObject * __this /* static, unused */, Action_1_t237882577 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInstagramManager::Share(UnityEngine.Texture2D)
extern "C"  void AndroidInstagramManager_Share_m749758534 (AndroidInstagramManager_t2759207936 * __this, Texture2D_t3542995729 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInstagramManager::Share(UnityEngine.Texture2D,System.String)
extern "C"  void AndroidInstagramManager_Share_m474246354 (AndroidInstagramManager_t2759207936 * __this, Texture2D_t3542995729 * ___texture0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInstagramManager::OnPostSuccess()
extern "C"  void AndroidInstagramManager_OnPostSuccess_m4042140141 (AndroidInstagramManager_t2759207936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInstagramManager::OnPostFailed(System.String)
extern "C"  void AndroidInstagramManager_OnPostFailed_m314738987 (AndroidInstagramManager_t2759207936 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidInstagramManager::<OnPostingCompleteAction>m__A4(InstagramPostResult)
extern "C"  void AndroidInstagramManager_U3COnPostingCompleteActionU3Em__A4_m2969250212 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
