﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GoogleMobileAdBanner
struct GoogleMobileAdBanner_t1323818958;
// DefaultPreviewButton
struct DefaultPreviewButton_t12674677;
// DefaultPreviewButton[]
struct DefaultPreviewButtonU5BU5D_t3800739864;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidGoogleAdsExample
struct  AndroidGoogleAdsExample_t2727429144  : public MonoBehaviour_t1158329972
{
public:
	// GoogleMobileAdBanner AndroidGoogleAdsExample::banner1
	Il2CppObject * ___banner1_4;
	// GoogleMobileAdBanner AndroidGoogleAdsExample::banner2
	Il2CppObject * ___banner2_5;
	// System.Boolean AndroidGoogleAdsExample::IsInterstisialsAdReady
	bool ___IsInterstisialsAdReady_6;
	// DefaultPreviewButton AndroidGoogleAdsExample::ShowIntersButton
	DefaultPreviewButton_t12674677 * ___ShowIntersButton_7;
	// DefaultPreviewButton[] AndroidGoogleAdsExample::b1CreateButtons
	DefaultPreviewButtonU5BU5D_t3800739864* ___b1CreateButtons_8;
	// DefaultPreviewButton AndroidGoogleAdsExample::b1Hide
	DefaultPreviewButton_t12674677 * ___b1Hide_9;
	// DefaultPreviewButton AndroidGoogleAdsExample::b1Show
	DefaultPreviewButton_t12674677 * ___b1Show_10;
	// DefaultPreviewButton AndroidGoogleAdsExample::b1Refresh
	DefaultPreviewButton_t12674677 * ___b1Refresh_11;
	// DefaultPreviewButton AndroidGoogleAdsExample::ChangePost1
	DefaultPreviewButton_t12674677 * ___ChangePost1_12;
	// DefaultPreviewButton AndroidGoogleAdsExample::ChangePost2
	DefaultPreviewButton_t12674677 * ___ChangePost2_13;
	// DefaultPreviewButton AndroidGoogleAdsExample::b1Destroy
	DefaultPreviewButton_t12674677 * ___b1Destroy_14;
	// DefaultPreviewButton[] AndroidGoogleAdsExample::b2CreateButtons
	DefaultPreviewButtonU5BU5D_t3800739864* ___b2CreateButtons_15;
	// DefaultPreviewButton AndroidGoogleAdsExample::b2Hide
	DefaultPreviewButton_t12674677 * ___b2Hide_16;
	// DefaultPreviewButton AndroidGoogleAdsExample::b2Show
	DefaultPreviewButton_t12674677 * ___b2Show_17;
	// DefaultPreviewButton AndroidGoogleAdsExample::b2Refresh
	DefaultPreviewButton_t12674677 * ___b2Refresh_18;
	// DefaultPreviewButton AndroidGoogleAdsExample::b2Destroy
	DefaultPreviewButton_t12674677 * ___b2Destroy_19;

public:
	inline static int32_t get_offset_of_banner1_4() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___banner1_4)); }
	inline Il2CppObject * get_banner1_4() const { return ___banner1_4; }
	inline Il2CppObject ** get_address_of_banner1_4() { return &___banner1_4; }
	inline void set_banner1_4(Il2CppObject * value)
	{
		___banner1_4 = value;
		Il2CppCodeGenWriteBarrier(&___banner1_4, value);
	}

	inline static int32_t get_offset_of_banner2_5() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___banner2_5)); }
	inline Il2CppObject * get_banner2_5() const { return ___banner2_5; }
	inline Il2CppObject ** get_address_of_banner2_5() { return &___banner2_5; }
	inline void set_banner2_5(Il2CppObject * value)
	{
		___banner2_5 = value;
		Il2CppCodeGenWriteBarrier(&___banner2_5, value);
	}

	inline static int32_t get_offset_of_IsInterstisialsAdReady_6() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___IsInterstisialsAdReady_6)); }
	inline bool get_IsInterstisialsAdReady_6() const { return ___IsInterstisialsAdReady_6; }
	inline bool* get_address_of_IsInterstisialsAdReady_6() { return &___IsInterstisialsAdReady_6; }
	inline void set_IsInterstisialsAdReady_6(bool value)
	{
		___IsInterstisialsAdReady_6 = value;
	}

	inline static int32_t get_offset_of_ShowIntersButton_7() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___ShowIntersButton_7)); }
	inline DefaultPreviewButton_t12674677 * get_ShowIntersButton_7() const { return ___ShowIntersButton_7; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_ShowIntersButton_7() { return &___ShowIntersButton_7; }
	inline void set_ShowIntersButton_7(DefaultPreviewButton_t12674677 * value)
	{
		___ShowIntersButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___ShowIntersButton_7, value);
	}

	inline static int32_t get_offset_of_b1CreateButtons_8() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___b1CreateButtons_8)); }
	inline DefaultPreviewButtonU5BU5D_t3800739864* get_b1CreateButtons_8() const { return ___b1CreateButtons_8; }
	inline DefaultPreviewButtonU5BU5D_t3800739864** get_address_of_b1CreateButtons_8() { return &___b1CreateButtons_8; }
	inline void set_b1CreateButtons_8(DefaultPreviewButtonU5BU5D_t3800739864* value)
	{
		___b1CreateButtons_8 = value;
		Il2CppCodeGenWriteBarrier(&___b1CreateButtons_8, value);
	}

	inline static int32_t get_offset_of_b1Hide_9() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___b1Hide_9)); }
	inline DefaultPreviewButton_t12674677 * get_b1Hide_9() const { return ___b1Hide_9; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_b1Hide_9() { return &___b1Hide_9; }
	inline void set_b1Hide_9(DefaultPreviewButton_t12674677 * value)
	{
		___b1Hide_9 = value;
		Il2CppCodeGenWriteBarrier(&___b1Hide_9, value);
	}

	inline static int32_t get_offset_of_b1Show_10() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___b1Show_10)); }
	inline DefaultPreviewButton_t12674677 * get_b1Show_10() const { return ___b1Show_10; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_b1Show_10() { return &___b1Show_10; }
	inline void set_b1Show_10(DefaultPreviewButton_t12674677 * value)
	{
		___b1Show_10 = value;
		Il2CppCodeGenWriteBarrier(&___b1Show_10, value);
	}

	inline static int32_t get_offset_of_b1Refresh_11() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___b1Refresh_11)); }
	inline DefaultPreviewButton_t12674677 * get_b1Refresh_11() const { return ___b1Refresh_11; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_b1Refresh_11() { return &___b1Refresh_11; }
	inline void set_b1Refresh_11(DefaultPreviewButton_t12674677 * value)
	{
		___b1Refresh_11 = value;
		Il2CppCodeGenWriteBarrier(&___b1Refresh_11, value);
	}

	inline static int32_t get_offset_of_ChangePost1_12() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___ChangePost1_12)); }
	inline DefaultPreviewButton_t12674677 * get_ChangePost1_12() const { return ___ChangePost1_12; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_ChangePost1_12() { return &___ChangePost1_12; }
	inline void set_ChangePost1_12(DefaultPreviewButton_t12674677 * value)
	{
		___ChangePost1_12 = value;
		Il2CppCodeGenWriteBarrier(&___ChangePost1_12, value);
	}

	inline static int32_t get_offset_of_ChangePost2_13() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___ChangePost2_13)); }
	inline DefaultPreviewButton_t12674677 * get_ChangePost2_13() const { return ___ChangePost2_13; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_ChangePost2_13() { return &___ChangePost2_13; }
	inline void set_ChangePost2_13(DefaultPreviewButton_t12674677 * value)
	{
		___ChangePost2_13 = value;
		Il2CppCodeGenWriteBarrier(&___ChangePost2_13, value);
	}

	inline static int32_t get_offset_of_b1Destroy_14() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___b1Destroy_14)); }
	inline DefaultPreviewButton_t12674677 * get_b1Destroy_14() const { return ___b1Destroy_14; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_b1Destroy_14() { return &___b1Destroy_14; }
	inline void set_b1Destroy_14(DefaultPreviewButton_t12674677 * value)
	{
		___b1Destroy_14 = value;
		Il2CppCodeGenWriteBarrier(&___b1Destroy_14, value);
	}

	inline static int32_t get_offset_of_b2CreateButtons_15() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___b2CreateButtons_15)); }
	inline DefaultPreviewButtonU5BU5D_t3800739864* get_b2CreateButtons_15() const { return ___b2CreateButtons_15; }
	inline DefaultPreviewButtonU5BU5D_t3800739864** get_address_of_b2CreateButtons_15() { return &___b2CreateButtons_15; }
	inline void set_b2CreateButtons_15(DefaultPreviewButtonU5BU5D_t3800739864* value)
	{
		___b2CreateButtons_15 = value;
		Il2CppCodeGenWriteBarrier(&___b2CreateButtons_15, value);
	}

	inline static int32_t get_offset_of_b2Hide_16() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___b2Hide_16)); }
	inline DefaultPreviewButton_t12674677 * get_b2Hide_16() const { return ___b2Hide_16; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_b2Hide_16() { return &___b2Hide_16; }
	inline void set_b2Hide_16(DefaultPreviewButton_t12674677 * value)
	{
		___b2Hide_16 = value;
		Il2CppCodeGenWriteBarrier(&___b2Hide_16, value);
	}

	inline static int32_t get_offset_of_b2Show_17() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___b2Show_17)); }
	inline DefaultPreviewButton_t12674677 * get_b2Show_17() const { return ___b2Show_17; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_b2Show_17() { return &___b2Show_17; }
	inline void set_b2Show_17(DefaultPreviewButton_t12674677 * value)
	{
		___b2Show_17 = value;
		Il2CppCodeGenWriteBarrier(&___b2Show_17, value);
	}

	inline static int32_t get_offset_of_b2Refresh_18() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___b2Refresh_18)); }
	inline DefaultPreviewButton_t12674677 * get_b2Refresh_18() const { return ___b2Refresh_18; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_b2Refresh_18() { return &___b2Refresh_18; }
	inline void set_b2Refresh_18(DefaultPreviewButton_t12674677 * value)
	{
		___b2Refresh_18 = value;
		Il2CppCodeGenWriteBarrier(&___b2Refresh_18, value);
	}

	inline static int32_t get_offset_of_b2Destroy_19() { return static_cast<int32_t>(offsetof(AndroidGoogleAdsExample_t2727429144, ___b2Destroy_19)); }
	inline DefaultPreviewButton_t12674677 * get_b2Destroy_19() const { return ___b2Destroy_19; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_b2Destroy_19() { return &___b2Destroy_19; }
	inline void set_b2Destroy_19(DefaultPreviewButton_t12674677 * value)
	{
		___b2Destroy_19 = value;
		Il2CppCodeGenWriteBarrier(&___b2Destroy_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
