﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_KeyedColle1294060137MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::.ctor()
#define KeyedCollection_2__ctor_m2957831018(__this, method) ((  void (*) (KeyedCollection_2_t499193797 *, const MethodInfo*))KeyedCollection_2__ctor_m3929680866_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>,System.Int32)
#define KeyedCollection_2__ctor_m1861318457(__this, ___comparer0, ___dictionaryCreationThreshold1, method) ((  void (*) (KeyedCollection_2_t499193797 *, Il2CppObject*, int32_t, const MethodInfo*))KeyedCollection_2__ctor_m1398787942_gshared)(__this, ___comparer0, ___dictionaryCreationThreshold1, method)
// System.Boolean System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::Contains(TKey)
#define KeyedCollection_2_Contains_m4246941140(__this, ___key0, method) ((  bool (*) (KeyedCollection_2_t499193797 *, String_t*, const MethodInfo*))KeyedCollection_2_Contains_m3918219472_gshared)(__this, ___key0, method)
// System.Int32 System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::IndexOfKey(TKey)
#define KeyedCollection_2_IndexOfKey_m550406278(__this, ___key0, method) ((  int32_t (*) (KeyedCollection_2_t499193797 *, String_t*, const MethodInfo*))KeyedCollection_2_IndexOfKey_m872387181_gshared)(__this, ___key0, method)
// TItem System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::get_Item(TKey)
#define KeyedCollection_2_get_Item_m4147753758(__this, ___key0, method) ((  JsonSchemaNode_t3866831117 * (*) (KeyedCollection_2_t499193797 *, String_t*, const MethodInfo*))KeyedCollection_2_get_Item_m1357505817_gshared)(__this, ___key0, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::ClearItems()
#define KeyedCollection_2_ClearItems_m2332730392(__this, method) ((  void (*) (KeyedCollection_2_t499193797 *, const MethodInfo*))KeyedCollection_2_ClearItems_m3110573139_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::InsertItem(System.Int32,TItem)
#define KeyedCollection_2_InsertItem_m1023505653(__this, ___index0, ___item1, method) ((  void (*) (KeyedCollection_2_t499193797 *, int32_t, JsonSchemaNode_t3866831117 *, const MethodInfo*))KeyedCollection_2_InsertItem_m2740229522_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::RemoveItem(System.Int32)
#define KeyedCollection_2_RemoveItem_m2472916953(__this, ___index0, method) ((  void (*) (KeyedCollection_2_t499193797 *, int32_t, const MethodInfo*))KeyedCollection_2_RemoveItem_m1708519878_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::SetItem(System.Int32,TItem)
#define KeyedCollection_2_SetItem_m2100101244(__this, ___index0, ___item1, method) ((  void (*) (KeyedCollection_2_t499193797 *, int32_t, JsonSchemaNode_t3866831117 *, const MethodInfo*))KeyedCollection_2_SetItem_m4134855463_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IDictionary`2<TKey,TItem> System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>::get_Dictionary()
#define KeyedCollection_2_get_Dictionary_m1290523325(__this, method) ((  Il2CppObject* (*) (KeyedCollection_2_t499193797 *, const MethodInfo*))KeyedCollection_2_get_Dictionary_m686852578_gshared)(__this, method)
