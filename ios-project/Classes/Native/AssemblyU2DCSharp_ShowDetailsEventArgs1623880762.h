﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Course
struct Course_t3483112699;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowDetailsEventArgs
struct  ShowDetailsEventArgs_t1623880762  : public Il2CppObject
{
public:
	// Course ShowDetailsEventArgs::<Course>k__BackingField
	Course_t3483112699 * ___U3CCourseU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCourseU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ShowDetailsEventArgs_t1623880762, ___U3CCourseU3Ek__BackingField_0)); }
	inline Course_t3483112699 * get_U3CCourseU3Ek__BackingField_0() const { return ___U3CCourseU3Ek__BackingField_0; }
	inline Course_t3483112699 ** get_address_of_U3CCourseU3Ek__BackingField_0() { return &___U3CCourseU3Ek__BackingField_0; }
	inline void set_U3CCourseU3Ek__BackingField_0(Course_t3483112699 * value)
	{
		___U3CCourseU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCourseU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
