﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// SA_Label
struct SA_Label_t226960149;
// DefaultPreviewButton
struct DefaultPreviewButton_t12674677;
// UnityEngine.Texture
struct Texture_t2243626319;
// DefaultPreviewButton[]
struct DefaultPreviewButtonU5BU5D_t3800739864;
// CustomPlayerUIRow[]
struct CustomPlayerUIRowU5BU5D_t745336337;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayServicFridnsLoadExample_New
struct  PlayServicFridnsLoadExample_New_t1740499043  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PlayServicFridnsLoadExample_New::avatar
	GameObject_t1756533147 * ___avatar_2;
	// SA_Label PlayServicFridnsLoadExample_New::playerLabel
	SA_Label_t226960149 * ___playerLabel_3;
	// DefaultPreviewButton PlayServicFridnsLoadExample_New::connectButton
	DefaultPreviewButton_t12674677 * ___connectButton_4;
	// UnityEngine.Texture PlayServicFridnsLoadExample_New::defaulttexture
	Texture_t2243626319 * ___defaulttexture_5;
	// DefaultPreviewButton[] PlayServicFridnsLoadExample_New::ConnectionDependedntButtons
	DefaultPreviewButtonU5BU5D_t3800739864* ___ConnectionDependedntButtons_6;
	// CustomPlayerUIRow[] PlayServicFridnsLoadExample_New::rows
	CustomPlayerUIRowU5BU5D_t745336337* ___rows_7;

public:
	inline static int32_t get_offset_of_avatar_2() { return static_cast<int32_t>(offsetof(PlayServicFridnsLoadExample_New_t1740499043, ___avatar_2)); }
	inline GameObject_t1756533147 * get_avatar_2() const { return ___avatar_2; }
	inline GameObject_t1756533147 ** get_address_of_avatar_2() { return &___avatar_2; }
	inline void set_avatar_2(GameObject_t1756533147 * value)
	{
		___avatar_2 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_2, value);
	}

	inline static int32_t get_offset_of_playerLabel_3() { return static_cast<int32_t>(offsetof(PlayServicFridnsLoadExample_New_t1740499043, ___playerLabel_3)); }
	inline SA_Label_t226960149 * get_playerLabel_3() const { return ___playerLabel_3; }
	inline SA_Label_t226960149 ** get_address_of_playerLabel_3() { return &___playerLabel_3; }
	inline void set_playerLabel_3(SA_Label_t226960149 * value)
	{
		___playerLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___playerLabel_3, value);
	}

	inline static int32_t get_offset_of_connectButton_4() { return static_cast<int32_t>(offsetof(PlayServicFridnsLoadExample_New_t1740499043, ___connectButton_4)); }
	inline DefaultPreviewButton_t12674677 * get_connectButton_4() const { return ___connectButton_4; }
	inline DefaultPreviewButton_t12674677 ** get_address_of_connectButton_4() { return &___connectButton_4; }
	inline void set_connectButton_4(DefaultPreviewButton_t12674677 * value)
	{
		___connectButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___connectButton_4, value);
	}

	inline static int32_t get_offset_of_defaulttexture_5() { return static_cast<int32_t>(offsetof(PlayServicFridnsLoadExample_New_t1740499043, ___defaulttexture_5)); }
	inline Texture_t2243626319 * get_defaulttexture_5() const { return ___defaulttexture_5; }
	inline Texture_t2243626319 ** get_address_of_defaulttexture_5() { return &___defaulttexture_5; }
	inline void set_defaulttexture_5(Texture_t2243626319 * value)
	{
		___defaulttexture_5 = value;
		Il2CppCodeGenWriteBarrier(&___defaulttexture_5, value);
	}

	inline static int32_t get_offset_of_ConnectionDependedntButtons_6() { return static_cast<int32_t>(offsetof(PlayServicFridnsLoadExample_New_t1740499043, ___ConnectionDependedntButtons_6)); }
	inline DefaultPreviewButtonU5BU5D_t3800739864* get_ConnectionDependedntButtons_6() const { return ___ConnectionDependedntButtons_6; }
	inline DefaultPreviewButtonU5BU5D_t3800739864** get_address_of_ConnectionDependedntButtons_6() { return &___ConnectionDependedntButtons_6; }
	inline void set_ConnectionDependedntButtons_6(DefaultPreviewButtonU5BU5D_t3800739864* value)
	{
		___ConnectionDependedntButtons_6 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionDependedntButtons_6, value);
	}

	inline static int32_t get_offset_of_rows_7() { return static_cast<int32_t>(offsetof(PlayServicFridnsLoadExample_New_t1740499043, ___rows_7)); }
	inline CustomPlayerUIRowU5BU5D_t745336337* get_rows_7() const { return ___rows_7; }
	inline CustomPlayerUIRowU5BU5D_t745336337** get_address_of_rows_7() { return &___rows_7; }
	inline void set_rows_7(CustomPlayerUIRowU5BU5D_t745336337* value)
	{
		___rows_7 = value;
		Il2CppCodeGenWriteBarrier(&___rows_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
