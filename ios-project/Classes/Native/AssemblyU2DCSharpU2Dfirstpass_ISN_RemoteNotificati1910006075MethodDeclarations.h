﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_RemoteNotificationsController
struct ISN_RemoteNotificationsController_t1910006075;
// System.Action`1<ISN_DeviceToken>
struct Action_1_t182773332;
// System.Action`1<ISN_RemoteNotification>
struct Action_1_t1251396696;
// System.Action`1<ISN_RemoteNotificationsRegistrationResult>
struct Action_1_t3137674533;
// ISN_RemoteNotification
struct ISN_RemoteNotification_t1449597314;
// System.String
struct String_t;
// ISN_DeviceToken
struct ISN_DeviceToken_t380973950;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_DeviceToken380973950.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_RemoteNotificati1449597314.h"

// System.Void ISN_RemoteNotificationsController::.ctor()
extern "C"  void ISN_RemoteNotificationsController__ctor_m3600199404 (ISN_RemoteNotificationsController_t1910006075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_RemoteNotificationsController::.cctor()
extern "C"  void ISN_RemoteNotificationsController__cctor_m51811771 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_RemoteNotificationsController::add_OnDeviceTokenReceived(System.Action`1<ISN_DeviceToken>)
extern "C"  void ISN_RemoteNotificationsController_add_OnDeviceTokenReceived_m1225533833 (Il2CppObject * __this /* static, unused */, Action_1_t182773332 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_RemoteNotificationsController::remove_OnDeviceTokenReceived(System.Action`1<ISN_DeviceToken>)
extern "C"  void ISN_RemoteNotificationsController_remove_OnDeviceTokenReceived_m3317188802 (Il2CppObject * __this /* static, unused */, Action_1_t182773332 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_RemoteNotificationsController::add_OnRemoteNotificationReceived(System.Action`1<ISN_RemoteNotification>)
extern "C"  void ISN_RemoteNotificationsController_add_OnRemoteNotificationReceived_m598678693 (Il2CppObject * __this /* static, unused */, Action_1_t1251396696 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_RemoteNotificationsController::remove_OnRemoteNotificationReceived(System.Action`1<ISN_RemoteNotification>)
extern "C"  void ISN_RemoteNotificationsController_remove_OnRemoteNotificationReceived_m4222388368 (Il2CppObject * __this /* static, unused */, Action_1_t1251396696 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_RemoteNotificationsController::Awake()
extern "C"  void ISN_RemoteNotificationsController_Awake_m3134927421 (ISN_RemoteNotificationsController_t1910006075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_RemoteNotificationsController::RegisterForRemoteNotifications(System.Action`1<ISN_RemoteNotificationsRegistrationResult>)
extern "C"  void ISN_RemoteNotificationsController_RegisterForRemoteNotifications_m635727023 (ISN_RemoteNotificationsController_t1910006075 * __this, Action_1_t3137674533 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ISN_RemoteNotification ISN_RemoteNotificationsController::get_LaunchNotification()
extern "C"  ISN_RemoteNotification_t1449597314 * ISN_RemoteNotificationsController_get_LaunchNotification_m3592580796 (ISN_RemoteNotificationsController_t1910006075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_RemoteNotificationsController::DidFailToRegisterForRemoteNotifications(System.String)
extern "C"  void ISN_RemoteNotificationsController_DidFailToRegisterForRemoteNotifications_m836361948 (ISN_RemoteNotificationsController_t1910006075 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_RemoteNotificationsController::DidRegisterForRemoteNotifications(System.String)
extern "C"  void ISN_RemoteNotificationsController_DidRegisterForRemoteNotifications_m2779759339 (ISN_RemoteNotificationsController_t1910006075 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_RemoteNotificationsController::DidReceiveRemoteNotification(System.String)
extern "C"  void ISN_RemoteNotificationsController_DidReceiveRemoteNotification_m3851245753 (ISN_RemoteNotificationsController_t1910006075 * __this, String_t* ___notificationBody0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_RemoteNotificationsController::<OnDeviceTokenReceived>m__3E(ISN_DeviceToken)
extern "C"  void ISN_RemoteNotificationsController_U3COnDeviceTokenReceivedU3Em__3E_m3924045818 (Il2CppObject * __this /* static, unused */, ISN_DeviceToken_t380973950 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_RemoteNotificationsController::<OnRemoteNotificationReceived>m__3F(ISN_RemoteNotification)
extern "C"  void ISN_RemoteNotificationsController_U3COnRemoteNotificationReceivedU3Em__3F_m1523370143 (Il2CppObject * __this /* static, unused */, ISN_RemoteNotification_t1449597314 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
