﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_GrantPermissionsResult
struct AN_GrantPermissionsResult_t250489657;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>
struct Dictionary_2_t1356696369;

#include "codegen/il2cpp-codegen.h"

// System.Void AN_GrantPermissionsResult::.ctor(System.String[],System.String[])
extern "C"  void AN_GrantPermissionsResult__ctor_m1620402916 (AN_GrantPermissionsResult_t250489657 * __this, StringU5BU5D_t1642385972* ___permissionsList0, StringU5BU5D_t1642385972* ___resultsList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState> AN_GrantPermissionsResult::get_RequestedPermissionsState()
extern "C"  Dictionary_2_t1356696369 * AN_GrantPermissionsResult_get_RequestedPermissionsState_m1678731228 (AN_GrantPermissionsResult_t250489657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
