﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4267761149.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23409008887.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3708718528_gshared (InternalEnumerator_1_t4267761149 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3708718528(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4267761149 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3708718528_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963007752_gshared (InternalEnumerator_1_t4267761149 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963007752(__this, method) ((  void (*) (InternalEnumerator_1_t4267761149 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963007752_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3612721422_gshared (InternalEnumerator_1_t4267761149 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3612721422(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4267761149 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3612721422_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2496885171_gshared (InternalEnumerator_1_t4267761149 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2496885171(__this, method) ((  void (*) (InternalEnumerator_1_t4267761149 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2496885171_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2552360808_gshared (InternalEnumerator_1_t4267761149 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2552360808(__this, method) ((  bool (*) (InternalEnumerator_1_t4267761149 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2552360808_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<AN_ManifestPermission,AN_PermissionState>>::get_Current()
extern "C"  KeyValuePair_2_t3409008887  InternalEnumerator_1_get_Current_m3295893271_gshared (InternalEnumerator_1_t4267761149 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3295893271(__this, method) ((  KeyValuePair_2_t3409008887  (*) (InternalEnumerator_1_t4267761149 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3295893271_gshared)(__this, method)
