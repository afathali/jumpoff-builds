﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3590536224MethodDeclarations.h"

// System.Void System.Func`2<System.String,System.Char>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2245180313(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1618288062 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3245784135_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.String,System.Char>::Invoke(T)
#define Func_2_Invoke_m1497206965(__this, ___arg10, method) ((  Il2CppChar (*) (Func_2_t1618288062 *, String_t*, const MethodInfo*))Func_2_Invoke_m2083325041_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.String,System.Char>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2254540426(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1618288062 *, String_t*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2687241844_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.String,System.Char>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3007320331(__this, ___result0, method) ((  Il2CppChar (*) (Func_2_t1618288062 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1113294503_gshared)(__this, ___result0, method)
