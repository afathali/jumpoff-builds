﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<AN_PackageCheckResult>
struct Action_1_t3497215137;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<AN_Locale>
struct Action_1_t4218522104;
// System.Action`1<System.String[]>
struct Action_1_t1444185354;
// System.Action`1<AN_NetworkInfo>
struct Action_1_t49592952;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen2089003954.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidNativeUtility
struct  AndroidNativeUtility_t798160036  : public SA_Singleton_OLD_1_t2089003954
{
public:

public:
};

struct AndroidNativeUtility_t798160036_StaticFields
{
public:
	// System.Action`1<AN_PackageCheckResult> AndroidNativeUtility::OnPackageCheckResult
	Action_1_t3497215137 * ___OnPackageCheckResult_4;
	// System.Action`1<System.String> AndroidNativeUtility::OnAndroidIdLoaded
	Action_1_t1831019615 * ___OnAndroidIdLoaded_5;
	// System.Action`1<System.String> AndroidNativeUtility::InternalStoragePathLoaded
	Action_1_t1831019615 * ___InternalStoragePathLoaded_6;
	// System.Action`1<System.String> AndroidNativeUtility::ExternalStoragePathLoaded
	Action_1_t1831019615 * ___ExternalStoragePathLoaded_7;
	// System.Action`1<AN_Locale> AndroidNativeUtility::LocaleInfoLoaded
	Action_1_t4218522104 * ___LocaleInfoLoaded_8;
	// System.Action`1<System.String[]> AndroidNativeUtility::ActionDevicePackagesListLoaded
	Action_1_t1444185354 * ___ActionDevicePackagesListLoaded_9;
	// System.Action`1<AN_NetworkInfo> AndroidNativeUtility::ActionNetworkInfoLoaded
	Action_1_t49592952 * ___ActionNetworkInfoLoaded_10;
	// System.Action`1<AN_PackageCheckResult> AndroidNativeUtility::<>f__am$cache7
	Action_1_t3497215137 * ___U3CU3Ef__amU24cache7_11;
	// System.Action`1<System.String> AndroidNativeUtility::<>f__am$cache8
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache8_12;
	// System.Action`1<System.String> AndroidNativeUtility::<>f__am$cache9
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache9_13;
	// System.Action`1<System.String> AndroidNativeUtility::<>f__am$cacheA
	Action_1_t1831019615 * ___U3CU3Ef__amU24cacheA_14;
	// System.Action`1<AN_Locale> AndroidNativeUtility::<>f__am$cacheB
	Action_1_t4218522104 * ___U3CU3Ef__amU24cacheB_15;
	// System.Action`1<System.String[]> AndroidNativeUtility::<>f__am$cacheC
	Action_1_t1444185354 * ___U3CU3Ef__amU24cacheC_16;
	// System.Action`1<AN_NetworkInfo> AndroidNativeUtility::<>f__am$cacheD
	Action_1_t49592952 * ___U3CU3Ef__amU24cacheD_17;

public:
	inline static int32_t get_offset_of_OnPackageCheckResult_4() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___OnPackageCheckResult_4)); }
	inline Action_1_t3497215137 * get_OnPackageCheckResult_4() const { return ___OnPackageCheckResult_4; }
	inline Action_1_t3497215137 ** get_address_of_OnPackageCheckResult_4() { return &___OnPackageCheckResult_4; }
	inline void set_OnPackageCheckResult_4(Action_1_t3497215137 * value)
	{
		___OnPackageCheckResult_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnPackageCheckResult_4, value);
	}

	inline static int32_t get_offset_of_OnAndroidIdLoaded_5() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___OnAndroidIdLoaded_5)); }
	inline Action_1_t1831019615 * get_OnAndroidIdLoaded_5() const { return ___OnAndroidIdLoaded_5; }
	inline Action_1_t1831019615 ** get_address_of_OnAndroidIdLoaded_5() { return &___OnAndroidIdLoaded_5; }
	inline void set_OnAndroidIdLoaded_5(Action_1_t1831019615 * value)
	{
		___OnAndroidIdLoaded_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnAndroidIdLoaded_5, value);
	}

	inline static int32_t get_offset_of_InternalStoragePathLoaded_6() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___InternalStoragePathLoaded_6)); }
	inline Action_1_t1831019615 * get_InternalStoragePathLoaded_6() const { return ___InternalStoragePathLoaded_6; }
	inline Action_1_t1831019615 ** get_address_of_InternalStoragePathLoaded_6() { return &___InternalStoragePathLoaded_6; }
	inline void set_InternalStoragePathLoaded_6(Action_1_t1831019615 * value)
	{
		___InternalStoragePathLoaded_6 = value;
		Il2CppCodeGenWriteBarrier(&___InternalStoragePathLoaded_6, value);
	}

	inline static int32_t get_offset_of_ExternalStoragePathLoaded_7() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___ExternalStoragePathLoaded_7)); }
	inline Action_1_t1831019615 * get_ExternalStoragePathLoaded_7() const { return ___ExternalStoragePathLoaded_7; }
	inline Action_1_t1831019615 ** get_address_of_ExternalStoragePathLoaded_7() { return &___ExternalStoragePathLoaded_7; }
	inline void set_ExternalStoragePathLoaded_7(Action_1_t1831019615 * value)
	{
		___ExternalStoragePathLoaded_7 = value;
		Il2CppCodeGenWriteBarrier(&___ExternalStoragePathLoaded_7, value);
	}

	inline static int32_t get_offset_of_LocaleInfoLoaded_8() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___LocaleInfoLoaded_8)); }
	inline Action_1_t4218522104 * get_LocaleInfoLoaded_8() const { return ___LocaleInfoLoaded_8; }
	inline Action_1_t4218522104 ** get_address_of_LocaleInfoLoaded_8() { return &___LocaleInfoLoaded_8; }
	inline void set_LocaleInfoLoaded_8(Action_1_t4218522104 * value)
	{
		___LocaleInfoLoaded_8 = value;
		Il2CppCodeGenWriteBarrier(&___LocaleInfoLoaded_8, value);
	}

	inline static int32_t get_offset_of_ActionDevicePackagesListLoaded_9() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___ActionDevicePackagesListLoaded_9)); }
	inline Action_1_t1444185354 * get_ActionDevicePackagesListLoaded_9() const { return ___ActionDevicePackagesListLoaded_9; }
	inline Action_1_t1444185354 ** get_address_of_ActionDevicePackagesListLoaded_9() { return &___ActionDevicePackagesListLoaded_9; }
	inline void set_ActionDevicePackagesListLoaded_9(Action_1_t1444185354 * value)
	{
		___ActionDevicePackagesListLoaded_9 = value;
		Il2CppCodeGenWriteBarrier(&___ActionDevicePackagesListLoaded_9, value);
	}

	inline static int32_t get_offset_of_ActionNetworkInfoLoaded_10() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___ActionNetworkInfoLoaded_10)); }
	inline Action_1_t49592952 * get_ActionNetworkInfoLoaded_10() const { return ___ActionNetworkInfoLoaded_10; }
	inline Action_1_t49592952 ** get_address_of_ActionNetworkInfoLoaded_10() { return &___ActionNetworkInfoLoaded_10; }
	inline void set_ActionNetworkInfoLoaded_10(Action_1_t49592952 * value)
	{
		___ActionNetworkInfoLoaded_10 = value;
		Il2CppCodeGenWriteBarrier(&___ActionNetworkInfoLoaded_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_11() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___U3CU3Ef__amU24cache7_11)); }
	inline Action_1_t3497215137 * get_U3CU3Ef__amU24cache7_11() const { return ___U3CU3Ef__amU24cache7_11; }
	inline Action_1_t3497215137 ** get_address_of_U3CU3Ef__amU24cache7_11() { return &___U3CU3Ef__amU24cache7_11; }
	inline void set_U3CU3Ef__amU24cache7_11(Action_1_t3497215137 * value)
	{
		___U3CU3Ef__amU24cache7_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_12() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___U3CU3Ef__amU24cache8_12)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache8_12() const { return ___U3CU3Ef__amU24cache8_12; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache8_12() { return &___U3CU3Ef__amU24cache8_12; }
	inline void set_U3CU3Ef__amU24cache8_12(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache8_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_13() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___U3CU3Ef__amU24cache9_13)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache9_13() const { return ___U3CU3Ef__amU24cache9_13; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache9_13() { return &___U3CU3Ef__amU24cache9_13; }
	inline void set_U3CU3Ef__amU24cache9_13(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache9_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_14() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___U3CU3Ef__amU24cacheA_14)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cacheA_14() const { return ___U3CU3Ef__amU24cacheA_14; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cacheA_14() { return &___U3CU3Ef__amU24cacheA_14; }
	inline void set_U3CU3Ef__amU24cacheA_14(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cacheA_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Action_1_t4218522104 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Action_1_t4218522104 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Action_1_t4218522104 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_16() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___U3CU3Ef__amU24cacheC_16)); }
	inline Action_1_t1444185354 * get_U3CU3Ef__amU24cacheC_16() const { return ___U3CU3Ef__amU24cacheC_16; }
	inline Action_1_t1444185354 ** get_address_of_U3CU3Ef__amU24cacheC_16() { return &___U3CU3Ef__amU24cacheC_16; }
	inline void set_U3CU3Ef__amU24cacheC_16(Action_1_t1444185354 * value)
	{
		___U3CU3Ef__amU24cacheC_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_17() { return static_cast<int32_t>(offsetof(AndroidNativeUtility_t798160036_StaticFields, ___U3CU3Ef__amU24cacheD_17)); }
	inline Action_1_t49592952 * get_U3CU3Ef__amU24cacheD_17() const { return ___U3CU3Ef__amU24cacheD_17; }
	inline Action_1_t49592952 ** get_address_of_U3CU3Ef__amU24cacheD_17() { return &___U3CU3Ef__amU24cacheD_17; }
	inline void set_U3CU3Ef__amU24cacheD_17(Action_1_t49592952 * value)
	{
		___U3CU3Ef__amU24cacheD_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
