﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Models.Invoker
struct Invoker_t994751176;
// System.String
struct String_t;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Core_System_Action3226471752.h"

// System.Void SA.Common.Models.Invoker::.ctor()
extern "C"  void Invoker__ctor_m1432752622 (Invoker_t994751176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SA.Common.Models.Invoker SA.Common.Models.Invoker::Create(System.String)
extern "C"  Invoker_t994751176 * Invoker_Create_m1650333862 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.Invoker::Awake()
extern "C"  void Invoker_Awake_m2061895843 (Invoker_t994751176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.Invoker::StartInvoke(System.Action,System.Single)
extern "C"  void Invoker_StartInvoke_m1716477762 (Invoker_t994751176 * __this, Action_t3226471752 * ___callback0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.Invoker::TimeOut()
extern "C"  void Invoker_TimeOut_m3772682717 (Invoker_t994751176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
