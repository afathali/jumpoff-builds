﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_MediaController
struct ISN_MediaController_t1853840745;
// System.Action`1<MP_MediaPickerResult>
struct Action_1_t2005806253;
// System.Action`1<MP_MediaItem>
struct Action_1_t3827422411;
// System.Action`1<MP_MusicPlaybackState>
struct Action_1_t2166513183;
// MP_MediaItem[]
struct MP_MediaItemU5BU5D_t1927419544;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// MP_MediaItem
struct MP_MediaItem_t4025623029;
// System.Collections.Generic.List`1<MP_MediaItem>
struct List_1_t3394744161;
// MP_MediaPickerResult
struct MP_MediaPickerResult_t2204006871;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MusicRepeatMode445284825.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MusicShuffleMode2068355741.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MusicPlaybackStat2364713801.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MediaPickerResult2204006871.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MP_MediaItem4025623029.h"

// System.Void ISN_MediaController::.ctor()
extern "C"  void ISN_MediaController__ctor_m1284344906 (ISN_MediaController_t1853840745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::.cctor()
extern "C"  void ISN_MediaController__cctor_m729114933 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::add_ActionMediaPickerResult(System.Action`1<MP_MediaPickerResult>)
extern "C"  void ISN_MediaController_add_ActionMediaPickerResult_m622250592 (Il2CppObject * __this /* static, unused */, Action_1_t2005806253 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::remove_ActionMediaPickerResult(System.Action`1<MP_MediaPickerResult>)
extern "C"  void ISN_MediaController_remove_ActionMediaPickerResult_m2507031273 (Il2CppObject * __this /* static, unused */, Action_1_t2005806253 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::add_ActionQueueUpdated(System.Action`1<MP_MediaPickerResult>)
extern "C"  void ISN_MediaController_add_ActionQueueUpdated_m1057086953 (Il2CppObject * __this /* static, unused */, Action_1_t2005806253 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::remove_ActionQueueUpdated(System.Action`1<MP_MediaPickerResult>)
extern "C"  void ISN_MediaController_remove_ActionQueueUpdated_m2214730968 (Il2CppObject * __this /* static, unused */, Action_1_t2005806253 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::add_ActionNowPlayingItemChanged(System.Action`1<MP_MediaItem>)
extern "C"  void ISN_MediaController_add_ActionNowPlayingItemChanged_m4003906300 (Il2CppObject * __this /* static, unused */, Action_1_t3827422411 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::remove_ActionNowPlayingItemChanged(System.Action`1<MP_MediaItem>)
extern "C"  void ISN_MediaController_remove_ActionNowPlayingItemChanged_m1648857661 (Il2CppObject * __this /* static, unused */, Action_1_t3827422411 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::add_ActionPlaybackStateChanged(System.Action`1<MP_MusicPlaybackState>)
extern "C"  void ISN_MediaController_add_ActionPlaybackStateChanged_m1417391773 (Il2CppObject * __this /* static, unused */, Action_1_t2166513183 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::remove_ActionPlaybackStateChanged(System.Action`1<MP_MusicPlaybackState>)
extern "C"  void ISN_MediaController_remove_ActionPlaybackStateChanged_m2383267308 (Il2CppObject * __this /* static, unused */, Action_1_t2166513183 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::Awake()
extern "C"  void ISN_MediaController_Awake_m2424951275 (ISN_MediaController_t1853840745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::SetRepeatMode(MP_MusicRepeatMode)
extern "C"  void ISN_MediaController_SetRepeatMode_m366731267 (ISN_MediaController_t1853840745 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::SetShuffleMode(MP_MusicShuffleMode)
extern "C"  void ISN_MediaController_SetShuffleMode_m1538343511 (ISN_MediaController_t1853840745 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::Play()
extern "C"  void ISN_MediaController_Play_m2370245722 (ISN_MediaController_t1853840745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::Pause()
extern "C"  void ISN_MediaController_Pause_m150737936 (ISN_MediaController_t1853840745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::SkipToNextItem()
extern "C"  void ISN_MediaController_SkipToNextItem_m3937407612 (ISN_MediaController_t1853840745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::SkipToBeginning()
extern "C"  void ISN_MediaController_SkipToBeginning_m2415059929 (ISN_MediaController_t1853840745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::SkipToPreviousItem()
extern "C"  void ISN_MediaController_SkipToPreviousItem_m1192030312 (ISN_MediaController_t1853840745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::ShowMediaPicker()
extern "C"  void ISN_MediaController_ShowMediaPicker_m3765292431 (ISN_MediaController_t1853840745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::SetCollection(MP_MediaItem[])
extern "C"  void ISN_MediaController_SetCollection_m2051547827 (ISN_MediaController_t1853840745 * __this, MP_MediaItemU5BU5D_t1927419544* ___items0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::AddItemWithProductID(System.String)
extern "C"  void ISN_MediaController_AddItemWithProductID_m139981360 (ISN_MediaController_t1853840745 * __this, String_t* ___productID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::SetCollection(System.String[])
extern "C"  void ISN_MediaController_SetCollection_m432998464 (ISN_MediaController_t1853840745 * __this, StringU5BU5D_t1642385972* ___itemIds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MP_MediaItem ISN_MediaController::get_NowPlayingItem()
extern "C"  MP_MediaItem_t4025623029 * ISN_MediaController_get_NowPlayingItem_m2414007924 (ISN_MediaController_t1853840745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<MP_MediaItem> ISN_MediaController::get_CurrentQueue()
extern "C"  List_1_t3394744161 * ISN_MediaController_get_CurrentQueue_m856816453 (ISN_MediaController_t1853840745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MP_MusicPlaybackState ISN_MediaController::get_State()
extern "C"  int32_t ISN_MediaController_get_State_m2726766526 (ISN_MediaController_t1853840745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<MP_MediaItem> ISN_MediaController::ParseMediaItemsList(System.String[],System.Int32)
extern "C"  List_1_t3394744161 * ISN_MediaController_ParseMediaItemsList_m2316562632 (ISN_MediaController_t1853840745 * __this, StringU5BU5D_t1642385972* ___data0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MP_MediaItem ISN_MediaController::ParseMediaItemData(System.String[],System.Int32)
extern "C"  MP_MediaItem_t4025623029 * ISN_MediaController_ParseMediaItemData_m3800105731 (ISN_MediaController_t1853840745 * __this, StringU5BU5D_t1642385972* ___data0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::OnQueueUpdate(System.String)
extern "C"  void ISN_MediaController_OnQueueUpdate_m366120489 (ISN_MediaController_t1853840745 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::OnQueueUpdateFailed(System.String)
extern "C"  void ISN_MediaController_OnQueueUpdateFailed_m1865703120 (ISN_MediaController_t1853840745 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::OnMediaPickerResult(System.String)
extern "C"  void ISN_MediaController_OnMediaPickerResult_m2006350840 (ISN_MediaController_t1853840745 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::OnMediaPickerFailed(System.String)
extern "C"  void ISN_MediaController_OnMediaPickerFailed_m845530048 (ISN_MediaController_t1853840745 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::OnNowPlayingItemchanged(System.String)
extern "C"  void ISN_MediaController_OnNowPlayingItemchanged_m2339670808 (ISN_MediaController_t1853840745 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::OnPlaybackStateChanged(System.String)
extern "C"  void ISN_MediaController_OnPlaybackStateChanged_m2065078179 (ISN_MediaController_t1853840745 * __this, String_t* ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::<ActionMediaPickerResult>m__32(MP_MediaPickerResult)
extern "C"  void ISN_MediaController_U3CActionMediaPickerResultU3Em__32_m1697753872 (Il2CppObject * __this /* static, unused */, MP_MediaPickerResult_t2204006871 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::<ActionQueueUpdated>m__33(MP_MediaPickerResult)
extern "C"  void ISN_MediaController_U3CActionQueueUpdatedU3Em__33_m2459260496 (Il2CppObject * __this /* static, unused */, MP_MediaPickerResult_t2204006871 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::<ActionNowPlayingItemChanged>m__34(MP_MediaItem)
extern "C"  void ISN_MediaController_U3CActionNowPlayingItemChangedU3Em__34_m1685044222 (Il2CppObject * __this /* static, unused */, MP_MediaItem_t4025623029 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_MediaController::<ActionPlaybackStateChanged>m__35(MP_MusicPlaybackState)
extern "C"  void ISN_MediaController_U3CActionPlaybackStateChangedU3Em__35_m4054741234 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
