﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Conver1788482786.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Conver3985667581.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Conver1202049151.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Conver3160111325.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_DateTim919483584.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumUt1099402118.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_JavaSc4013793858.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_LateBo3208546116.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_MathUti722929707.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Miscel2828154915.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Reflec2294713146.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Reflec3884221258.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Reflec1085039004.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Reflec1041854794.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String2484172789.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String1768673176.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String3198721076.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_StringU409797621.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_String1771230768.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Valida1621959402.h"
#include "AssemblyU2DCSharp_Jump3D438928399.h"
#include "AssemblyU2DCSharp_JumpTypeCycle3789588250.h"
#include "AssemblyU2DCSharp_Purchaser1507804203.h"
#include "AssemblyU2DCSharp_CaptureMethod3448917501.h"
#include "AssemblyU2DCSharp_ScreenCapture1787384414.h"
#include "AssemblyU2DCSharp_ScreenCapture_U3CSaveScreenshot_1581677774.h"
#include "AssemblyU2DCSharp_ScreenCapture_U3CSaveScreenshot_1429752400.h"
#include "AssemblyU2DCSharp_Extensions612262650.h"
#include "AssemblyU2DCSharp_MCGBehaviour3307770884.h"
#include "AssemblyU2DCSharp_MCGBehaviour_LifestyleType936921768.h"
#include "AssemblyU2DCSharp_MCGBehaviour_Component1531646212.h"
#include "AssemblyU2DCSharp_MCGBehaviour_Component_Component4170640987.h"
#include "AssemblyU2DCSharp_MCGBehaviour_InjectionContainer3223711125.h"
#include "AssemblyU2DCSharp_MCGBehaviour_InjectionContainer_T212254090.h"
#include "AssemblyU2DCSharp_MCGBehaviour_InjectionContainer_3929677068.h"
#include "AssemblyU2DCSharp_MCGBehaviour_InjectionContainer_3839060284.h"
#include "AssemblyU2DCSharp_CanvasOverlay3181174292.h"
#include "AssemblyU2DCSharp_CourseListController1183148997.h"
#include "AssemblyU2DCSharp_DeleteCourseRequest1683798751.h"
#include "AssemblyU2DCSharp_DeleteCourseResponse1898856809.h"
#include "AssemblyU2DCSharp_CourseController1581498483.h"
#include "AssemblyU2DCSharp_CreateCourseRequest4267998642.h"
#include "AssemblyU2DCSharp_JumpPoint1009860606.h"
#include "AssemblyU2DCSharp_CreateCourseResponse3800683746.h"
#include "AssemblyU2DCSharp_TempJumpInfo2313300354.h"
#include "AssemblyU2DCSharp_GetCourseRequest2656580458.h"
#include "AssemblyU2DCSharp_GetCourseResponse2659116210.h"
#include "AssemblyU2DCSharp_CourseObject1071976694.h"
#include "AssemblyU2DCSharp_DeleteCourseEventArgs2630278065.h"
#include "AssemblyU2DCSharp_Jump114869516.h"
#include "AssemblyU2DCSharp_Jump_Collider2378236225.h"
#include "AssemblyU2DCSharp_CropSprite3868165965.h"
#include "AssemblyU2DCSharp_Details_Controller3848679003.h"
#include "AssemblyU2DCSharp_ShowDetailsEventArgs1623880762.h"
#include "AssemblyU2DCSharp_ErrorMessage521535845.h"
#include "AssemblyU2DCSharp_ShowErrorEventArgs2135289798.h"
#include "AssemblyU2DCSharp_ShowErrorEventArgs_del1625342804.h"
#include "AssemblyU2DCSharp_ExifLib_ExifIO1291478686.h"
#include "AssemblyU2DCSharp_ExifLib_JpegId1946960615.h"
#include "AssemblyU2DCSharp_ExifLib_ExifIFD1652672661.h"
#include "AssemblyU2DCSharp_ExifLib_ExifId343747131.h"
#include "AssemblyU2DCSharp_ExifLib_ExifGps3527933044.h"
#include "AssemblyU2DCSharp_ExifLib_ExifOrientation1205752066.h"
#include "AssemblyU2DCSharp_ExifLib_ExifUnit2084677226.h"
#include "AssemblyU2DCSharp_ExifLib_ExifFlash1670535696.h"
#include "AssemblyU2DCSharp_ExifLib_ExifGpsLatitudeRef1514933379.h"
#include "AssemblyU2DCSharp_ExifLib_ExifGpsLongitudeRef1142847722.h"
#include "AssemblyU2DCSharp_ExifLib_ExifReader3799989581.h"
#include "AssemblyU2DCSharp_ExifLib_ExifTagFormat3078035447.h"
#include "AssemblyU2DCSharp_ExifLib_ExifTag1511511074.h"
#include "AssemblyU2DCSharp_ExifLib_JpegInfo3114827956.h"
#include "AssemblyU2DCSharp_GlobalData2590907497.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "AssemblyU2DCSharp_Course3483112699.h"
#include "AssemblyU2DCSharp_Crop2548131222.h"
#include "AssemblyU2DCSharp_ImageUploader3573860837.h"
#include "AssemblyU2DCSharp_ImageUploaderPageManager1025178313.h"
#include "AssemblyU2DCSharp_Login_Controller1381689646.h"
#include "AssemblyU2DCSharp_LoginRequest2273172322.h"
#include "AssemblyU2DCSharp_LoginResponse1319408382.h"
#include "AssemblyU2DCSharp_RegisterRequest698226714.h"
#include "AssemblyU2DCSharp_RegisterResponse410466074.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (TypeConvertKey_t1788482786)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3000[2] = 
{
	TypeConvertKey_t1788482786::get_offset_of__initialType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TypeConvertKey_t1788482786::get_offset_of__targetType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (U3CCreateCastConverterU3Ec__AnonStorey29_t3985667581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3001[1] = 
{
	U3CCreateCastConverterU3Ec__AnonStorey29_t3985667581::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3002[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (U3CTryConvertU3Ec__AnonStorey2B_t1202049151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3003[3] = 
{
	U3CTryConvertU3Ec__AnonStorey2B_t1202049151::get_offset_of_initialValue_0(),
	U3CTryConvertU3Ec__AnonStorey2B_t1202049151::get_offset_of_culture_1(),
	U3CTryConvertU3Ec__AnonStorey2B_t1202049151::get_offset_of_targetType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3004[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (U3CTryConvertOrCastU3Ec__AnonStorey2D_t3160111325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3005[3] = 
{
	U3CTryConvertOrCastU3Ec__AnonStorey2D_t3160111325::get_offset_of_initialValue_0(),
	U3CTryConvertOrCastU3Ec__AnonStorey2D_t3160111325::get_offset_of_culture_1(),
	U3CTryConvertOrCastU3Ec__AnonStorey2D_t3160111325::get_offset_of_targetType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (DateTimeUtils_t919483584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3008[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3009[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (EnumUtils_t1099402118), -1, sizeof(EnumUtils_t1099402118_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3010[2] = 
{
	EnumUtils_t1099402118_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	EnumUtils_t1099402118_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3011[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3012[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (JavaScriptUtils_t4013793858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (LateBoundReflectionDelegateFactory_t3208546116), -1, sizeof(LateBoundReflectionDelegateFactory_t3208546116_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3015[1] = 
{
	LateBoundReflectionDelegateFactory_t3208546116_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3016[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3017[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3018[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3019[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3020[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3021[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3023[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (MathUtils_t722929707), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (MiscellaneousUtils_t2828154915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (ReflectionDelegateFactory_t2294713146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (ReflectionUtils_t3884221258), -1, sizeof(ReflectionUtils_t3884221258_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3027[5] = 
{
	ReflectionUtils_t3884221258_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	ReflectionUtils_t3884221258_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	ReflectionUtils_t3884221258_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	ReflectionUtils_t3884221258_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	ReflectionUtils_t3884221258_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (U3CGetFieldsAndPropertiesU3Ec__AnonStorey35_t1085039004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3028[1] = 
{
	U3CGetFieldsAndPropertiesU3Ec__AnonStorey35_t1085039004::get_offset_of_bindingAttr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (U3CGetChildPrivatePropertiesU3Ec__AnonStorey36_t1041854794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3029[1] = 
{
	U3CGetChildPrivatePropertiesU3Ec__AnonStorey36_t1041854794::get_offset_of_nonPublicProperty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (StringBuffer_t2484172789), -1, sizeof(StringBuffer_t2484172789_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3030[3] = 
{
	StringBuffer_t2484172789::get_offset_of__buffer_0(),
	StringBuffer_t2484172789::get_offset_of__position_1(),
	StringBuffer_t2484172789_StaticFields::get_offset_of__emptyBuffer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (StringUtils_t1768673176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3031[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (ActionLine_t3198721076), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (U3CIndentU3Ec__AnonStorey37_t409797621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3033[2] = 
{
	U3CIndentU3Ec__AnonStorey37_t409797621::get_offset_of_indentChar_0(),
	U3CIndentU3Ec__AnonStorey37_t409797621::get_offset_of_indentation_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (U3CNumberLinesU3Ec__AnonStorey38_t1771230768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3034[1] = 
{
	U3CNumberLinesU3Ec__AnonStorey38_t1771230768::get_offset_of_lineNumber_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3035[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3036[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (ValidationUtils_t1621959402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3037[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (Jump3D_t438928399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3038[1] = 
{
	Jump3D_t438928399::get_offset_of_rotCanvas_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (JumpTypeCycle_t3789588250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3039[9] = 
{
	JumpTypeCycle_t3789588250::get_offset_of_jump_type_2(),
	JumpTypeCycle_t3789588250::get_offset_of_icon_jump1_3(),
	JumpTypeCycle_t3789588250::get_offset_of_icon_jump2_4(),
	JumpTypeCycle_t3789588250::get_offset_of_icon_waterjump_5(),
	JumpTypeCycle_t3789588250::get_offset_of_icon_gate_6(),
	JumpTypeCycle_t3789588250::get_offset_of_icon_cross_7(),
	JumpTypeCycle_t3789588250::get_offset_of_icon_unicorn_8(),
	JumpTypeCycle_t3789588250::get_offset_of_type_9(),
	JumpTypeCycle_t3789588250::get_offset_of_inited_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (Purchaser_t1507804203), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (CaptureMethod_t3448917501)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3041[7] = 
{
	CaptureMethod_t3448917501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (ScreenCapture_t1787384414), -1, sizeof(ScreenCapture_t1787384414_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3042[1] = 
{
	ScreenCapture_t1787384414_StaticFields::get_offset_of_tempFileCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3043[6] = 
{
	U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774::get_offset_of_U3CtextureU3E__0_0(),
	U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774::get_offset_of_U3CbytesU3E__1_1(),
	U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774::get_offset_of_filePath_2(),
	U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774::get_offset_of_U24PC_3(),
	U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774::get_offset_of_U24current_4(),
	U3CSaveScreenshot_ReadPixelsAsynchU3Ec__Iterator14_t1581677774::get_offset_of_U3CU24U3EfilePath_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3044[10] = 
{
	U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400::get_offset_of_U3CrtU3E__0_0(),
	U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400::get_offset_of_U3CscreenShotU3E__1_1(),
	U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400::get_offset_of_U3CU24s_300U3E__2_2(),
	U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400::get_offset_of_U3CU24s_301U3E__3_3(),
	U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400::get_offset_of_U3CcamU3E__4_4(),
	U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400::get_offset_of_U3CbytesU3E__5_5(),
	U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400::get_offset_of_filePath_6(),
	U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400::get_offset_of_U24PC_7(),
	U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400::get_offset_of_U24current_8(),
	U3CSaveScreenshot_RenderToTexAsynchU3Ec__Iterator15_t1429752400::get_offset_of_U3CU24U3EfilePath_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (Extensions_t612262650), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (MCGBehaviour_t3307770884), -1, sizeof(MCGBehaviour_t3307770884_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3046[2] = 
{
	MCGBehaviour_t3307770884::get_offset_of__container_2(),
	MCGBehaviour_t3307770884_StaticFields::get_offset_of__subscriptions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (LifestyleType_t936921768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3047[3] = 
{
	LifestyleType_t936921768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (Component_t1531646212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3048[6] = 
{
	Component_t1531646212::get_offset_of_U3CForTypeU3Ek__BackingField_0(),
	Component_t1531646212::get_offset_of_U3CImplementedByTypeU3Ek__BackingField_1(),
	Component_t1531646212::get_offset_of_U3CNameU3Ek__BackingField_2(),
	Component_t1531646212::get_offset_of_U3CLifestyleU3Ek__BackingField_3(),
	Component_t1531646212::get_offset_of_U3CLifestyleTypeU3Ek__BackingField_4(),
	Component_t1531646212::get_offset_of_U3CInstanceU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (ComponentLifestyle_t4170640987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3049[1] = 
{
	ComponentLifestyle_t4170640987::get_offset_of__component_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (InjectionContainer_t3223711125), -1, sizeof(InjectionContainer_t3223711125_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3050[2] = 
{
	InjectionContainer_t3223711125_StaticFields::get_offset_of__map_0(),
	InjectionContainer_t3223711125_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (TypeData_t212254090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3051[4] = 
{
	TypeData_t212254090::get_offset_of_U3CInstanceU3Ek__BackingField_0(),
	TypeData_t212254090::get_offset_of_U3CImplementedByU3Ek__BackingField_1(),
	TypeData_t212254090::get_offset_of_U3CLifestyleTypeU3Ek__BackingField_2(),
	TypeData_t212254090::get_offset_of_U3CNameU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (U3CRegisterU3Ec__AnonStorey3A_t3929677068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3052[1] = 
{
	U3CRegisterU3Ec__AnonStorey3A_t3929677068::get_offset_of_component_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (U3CResolveU3Ec__AnonStorey3B_t3839060284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3053[1] = 
{
	U3CResolveU3Ec__AnonStorey3B_t3839060284::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (CanvasOverlay_t3181174292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (CourseListController_t1183148997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3055[22] = 
{
	CourseListController_t1183148997::get_offset_of_btn_close_4(),
	CourseListController_t1183148997::get_offset_of_btn_addNew_5(),
	CourseListController_t1183148997::get_offset_of_btn_backToList_6(),
	CourseListController_t1183148997::get_offset_of_btn_backToImageSelect_7(),
	CourseListController_t1183148997::get_offset_of_btn_continue_8(),
	CourseListController_t1183148997::get_offset_of_btn_finish_9(),
	CourseListController_t1183148997::get_offset_of_btn_skip_10(),
	CourseListController_t1183148997::get_offset_of_ListContainer_11(),
	CourseListController_t1183148997::get_offset_of_DetailsPage_12(),
	CourseListController_t1183148997::get_offset_of_Course_13(),
	CourseListController_t1183148997::get_offset_of_MainCamera_14(),
	CourseListController_t1183148997::get_offset_of_input_name_15(),
	CourseListController_t1183148997::get_offset_of_input_show_16(),
	CourseListController_t1183148997::get_offset_of_input_notes_17(),
	CourseListController_t1183148997::get_offset_of_DeleteMessage_18(),
	CourseListController_t1183148997::get_offset_of_btn_cancelDelete_19(),
	CourseListController_t1183148997::get_offset_of_btn_confirmDelete_20(),
	CourseListController_t1183148997::get_offset_of_DeleteCourseId_21(),
	CourseListController_t1183148997::get_offset_of_CourseObjects_22(),
	CourseListController_t1183148997::get_offset_of_slider_courseImageScale_23(),
	CourseListController_t1183148997::get_offset_of_waiting_24(),
	CourseListController_t1183148997::get_offset_of_fail_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (DeleteCourseRequest_t1683798751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3056[3] = 
{
	DeleteCourseRequest_t1683798751::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	DeleteCourseRequest_t1683798751::get_offset_of_U3CUserIdU3Ek__BackingField_3(),
	DeleteCourseRequest_t1683798751::get_offset_of_U3CIdU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (DeleteCourseResponse_t1898856809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3057[4] = 
{
	DeleteCourseResponse_t1898856809::get_offset_of_U3CSuccessU3Ek__BackingField_0(),
	DeleteCourseResponse_t1898856809::get_offset_of_U3CErrorMessageU3Ek__BackingField_1(),
	DeleteCourseResponse_t1898856809::get_offset_of_U3CRequestU3Ek__BackingField_2(),
	DeleteCourseResponse_t1898856809::get_offset_of_U3CCoursesU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (CourseController_t1581498483), -1, sizeof(CourseController_t1581498483_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3058[97] = 
{
	CourseController_t1581498483::get_offset_of_resultImage_4(),
	CourseController_t1581498483::get_offset_of_lineRenderer_5(),
	CourseController_t1581498483::get_offset_of_LineStartEndPoint_6(),
	CourseController_t1581498483::get_offset_of_LineDotParent_7(),
	CourseController_t1581498483::get_offset_of_btn_erase_8(),
	CourseController_t1581498483::get_offset_of_btn_menu_9(),
	CourseController_t1581498483::get_offset_of_Menu_10(),
	CourseController_t1581498483::get_offset_of_btn_closeMenu_11(),
	CourseController_t1581498483::get_offset_of_btn_startOver_12(),
	CourseController_t1581498483::get_offset_of_btn_menu_visualize_13(),
	CourseController_t1581498483::get_offset_of_btn_save_14(),
	CourseController_t1581498483::get_offset_of_btn_exit_15(),
	CourseController_t1581498483::get_offset_of_btn_toggleImage_16(),
	CourseController_t1581498483::get_offset_of_imageOn_17(),
	CourseController_t1581498483::get_offset_of_imageOff_18(),
	CourseController_t1581498483::get_offset_of_btn_toggleCheats_19(),
	CourseController_t1581498483::get_offset_of_btn_drawJumps_20(),
	CourseController_t1581498483::get_offset_of_drawing_21(),
	CourseController_t1581498483::get_offset_of_jumps_22(),
	CourseController_t1581498483::get_offset_of_JumpPrefab_23(),
	CourseController_t1581498483::get_offset_of_JumpSliderObject_24(),
	CourseController_t1581498483::get_offset_of_JumpSliderHandle_25(),
	CourseController_t1581498483::get_offset_of_JumpSlider_26(),
	CourseController_t1581498483::get_offset_of_JumpToggleImage_27(),
	CourseController_t1581498483::get_offset_of_CloseMenuImage_28(),
	CourseController_t1581498483::get_offset_of_StartPopup_29(),
	CourseController_t1581498483::get_offset_of_CompletePopup_30(),
	CourseController_t1581498483::get_offset_of_MissedPopup_31(),
	CourseController_t1581498483::get_offset_of_btn_StartPopup_32(),
	CourseController_t1581498483::get_offset_of_btn_CompletePopup_33(),
	CourseController_t1581498483::get_offset_of_btn_MissedPopup_34(),
	CourseController_t1581498483_StaticFields::get_offset_of_courseId_35(),
	CourseController_t1581498483::get_offset_of_Training_Overlay_1_36(),
	CourseController_t1581498483::get_offset_of_Training_Overlay_2_37(),
	CourseController_t1581498483::get_offset_of_Training_Overlay_3_38(),
	CourseController_t1581498483::get_offset_of_btn_toggleTutorial_39(),
	CourseController_t1581498483::get_offset_of_btn_closeTutorial1_40(),
	CourseController_t1581498483::get_offset_of_btn_closeTutorial2_41(),
	CourseController_t1581498483::get_offset_of_btn_closeTutorial3_42(),
	CourseController_t1581498483::get_offset_of_TempCourseImage_43(),
	CourseController_t1581498483::get_offset_of_waiting_44(),
	CourseController_t1581498483::get_offset_of_btn_visualize_45(),
	CourseController_t1581498483::get_offset_of_btn_complete_close_46(),
	CourseController_t1581498483::get_offset_of_startDraw_47(),
	CourseController_t1581498483::get_offset_of_lastPosition_48(),
	CourseController_t1581498483::get_offset_of_newPosition_49(),
	CourseController_t1581498483::get_offset_of_tapped_50(),
	CourseController_t1581498483::get_offset_of_startPoint_51(),
	CourseController_t1581498483::get_offset_of_endPoint_52(),
	CourseController_t1581498483::get_offset_of_showingCheats_53(),
	CourseController_t1581498483::get_offset_of_isImageShown_54(),
	CourseController_t1581498483::get_offset_of_numPopups_55(),
	CourseController_t1581498483::get_offset_of_placingJumps_56(),
	CourseController_t1581498483::get_offset_of_jump_57(),
	CourseController_t1581498483::get_offset_of_NextJumpNumber_58(),
	CourseController_t1581498483::get_offset_of_lastJumpNumber_59(),
	CourseController_t1581498483::get_offset_of_showingPopup_60(),
	CourseController_t1581498483::get_offset_of_jumpSize_61(),
	CourseController_t1581498483::get_offset_of_TIMER_MAX_62(),
	CourseController_t1581498483::get_offset_of_btn_menuLarge_63(),
	CourseController_t1581498483::get_offset_of_btn_jumpLarge_64(),
	CourseController_t1581498483::get_offset_of_btn_eraseLarge_65(),
	CourseController_t1581498483::get_offset_of_btn_menuSmall_66(),
	CourseController_t1581498483::get_offset_of_btn_jumpSmall_67(),
	CourseController_t1581498483::get_offset_of_btn_eraseSmall_68(),
	CourseController_t1581498483::get_offset_of_shrinkButtons_69(),
	CourseController_t1581498483::get_offset_of_growButtons_70(),
	CourseController_t1581498483::get_offset_of_CourseScreen_71(),
	CourseController_t1581498483::get_offset_of_currentJumpHitNumber_72(),
	CourseController_t1581498483::get_offset_of_newJumpHit_73(),
	CourseController_t1581498483::get_offset_of_currentJumpHit1_74(),
	CourseController_t1581498483::get_offset_of_currentJumpHit2_75(),
	CourseController_t1581498483::get_offset_of_canvas_76(),
	CourseController_t1581498483::get_offset_of_mainCamera_77(),
	CourseController_t1581498483::get_offset_of_jumpEdit_78(),
	CourseController_t1581498483::get_offset_of_btn_jumpEdit_done_79(),
	CourseController_t1581498483::get_offset_of_btn_jumpEdit_type_80(),
	CourseController_t1581498483::get_offset_of_jumpsList_81(),
	CourseController_t1581498483::get_offset_of_jumpEditListeners_82(),
	CourseController_t1581498483::get_offset_of_ShowStartTraining_83(),
	CourseController_t1581498483::get_offset_of_viewport_84(),
	CourseController_t1581498483::get_offset_of_jumpsGlow_85(),
	CourseController_t1581498483::get_offset_of_footerButtons_86(),
	CourseController_t1581498483::get_offset_of_mouseButton0Down_pos_87(),
	CourseController_t1581498483::get_offset_of_tapMoveEpsilon_88(),
	CourseController_t1581498483::get_offset_of_wasMultitouch_89(),
	CourseController_t1581498483::get_offset_of_mouseInCouseArea_90(),
	CourseController_t1581498483::get_offset_of_touch_t0_91(),
	CourseController_t1581498483::get_offset_of_tapCancelled_92(),
	CourseController_t1581498483::get_offset_of_newScreenPosition_93(),
	CourseController_t1581498483::get_offset_of_prevScreenPosition_94(),
	CourseController_t1581498483::get_offset_of_lastActivityTime_95(),
	CourseController_t1581498483::get_offset_of_prevNumPopups_96(),
	CourseController_t1581498483::get_offset_of_lastHit_97(),
	CourseController_t1581498483::get_offset_of_haveFirstCollider_98(),
	CourseController_t1581498483::get_offset_of_trackFail_99(),
	CourseController_t1581498483::get_offset_of_numPointsAdded_100(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (CreateCourseRequest_t4267998642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3059[13] = 
{
	CreateCourseRequest_t4267998642::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	CreateCourseRequest_t4267998642::get_offset_of_U3CIdU3Ek__BackingField_3(),
	CreateCourseRequest_t4267998642::get_offset_of_U3CUserIdU3Ek__BackingField_4(),
	CreateCourseRequest_t4267998642::get_offset_of_U3CNameU3Ek__BackingField_5(),
	CreateCourseRequest_t4267998642::get_offset_of_U3CShowU3Ek__BackingField_6(),
	CreateCourseRequest_t4267998642::get_offset_of_U3CDateU3Ek__BackingField_7(),
	CreateCourseRequest_t4267998642::get_offset_of_U3CNotesU3Ek__BackingField_8(),
	CreateCourseRequest_t4267998642::get_offset_of_U3CImageU3Ek__BackingField_9(),
	CreateCourseRequest_t4267998642::get_offset_of_U3CImagePositionU3Ek__BackingField_10(),
	CreateCourseRequest_t4267998642::get_offset_of_U3CImageRotationU3Ek__BackingField_11(),
	CreateCourseRequest_t4267998642::get_offset_of_U3CImageScaleU3Ek__BackingField_12(),
	CreateCourseRequest_t4267998642::get_offset_of_U3CJumpsU3Ek__BackingField_13(),
	CreateCourseRequest_t4267998642::get_offset_of_U3CScaleU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (JumpPoint_t1009860606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3060[6] = 
{
	JumpPoint_t1009860606::get_offset_of_U3CxPosU3Ek__BackingField_0(),
	JumpPoint_t1009860606::get_offset_of_U3CyPosU3Ek__BackingField_1(),
	JumpPoint_t1009860606::get_offset_of_U3CrotationU3Ek__BackingField_2(),
	JumpPoint_t1009860606::get_offset_of_U3CjumpNumberU3Ek__BackingField_3(),
	JumpPoint_t1009860606::get_offset_of_U3CjumpTypeU3Ek__BackingField_4(),
	JumpPoint_t1009860606::get_offset_of_U3CtempNumberTextU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (CreateCourseResponse_t3800683746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3061[4] = 
{
	CreateCourseResponse_t3800683746::get_offset_of_U3CSuccessU3Ek__BackingField_0(),
	CreateCourseResponse_t3800683746::get_offset_of_U3CErrorMessageU3Ek__BackingField_1(),
	CreateCourseResponse_t3800683746::get_offset_of_U3CRequestU3Ek__BackingField_2(),
	CreateCourseResponse_t3800683746::get_offset_of_U3CCoursesU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (TempJumpInfo_t2313300354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3062[5] = 
{
	TempJumpInfo_t2313300354::get_offset_of_U3CxPosU3Ek__BackingField_0(),
	TempJumpInfo_t2313300354::get_offset_of_U3CyPosU3Ek__BackingField_1(),
	TempJumpInfo_t2313300354::get_offset_of_U3CrotationU3Ek__BackingField_2(),
	TempJumpInfo_t2313300354::get_offset_of_U3CjumpNumberU3Ek__BackingField_3(),
	TempJumpInfo_t2313300354::get_offset_of_U3CjumpTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (GetCourseRequest_t2656580458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3063[3] = 
{
	GetCourseRequest_t2656580458::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	GetCourseRequest_t2656580458::get_offset_of_U3CUserIdU3Ek__BackingField_3(),
	GetCourseRequest_t2656580458::get_offset_of_U3CIdU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (GetCourseResponse_t2659116210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3064[4] = 
{
	GetCourseResponse_t2659116210::get_offset_of_U3CSuccessU3Ek__BackingField_0(),
	GetCourseResponse_t2659116210::get_offset_of_U3CErrorMessageU3Ek__BackingField_1(),
	GetCourseResponse_t2659116210::get_offset_of_U3CRequestU3Ek__BackingField_2(),
	GetCourseResponse_t2659116210::get_offset_of_U3CCoursesU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (CourseObject_t1071976694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3065[7] = 
{
	CourseObject_t1071976694::get_offset_of_CourseName_4(),
	CourseObject_t1071976694::get_offset_of_CourseLocation_5(),
	CourseObject_t1071976694::get_offset_of_CourseDate_6(),
	CourseObject_t1071976694::get_offset_of_btn_openCourse_7(),
	CourseObject_t1071976694::get_offset_of_btn_details_8(),
	CourseObject_t1071976694::get_offset_of_btn_delete_9(),
	CourseObject_t1071976694::get_offset_of_MyCourse_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (DeleteCourseEventArgs_t2630278065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3066[1] = 
{
	DeleteCourseEventArgs_t2630278065::get_offset_of_U3CIdU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (Jump_t114869516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3067[23] = 
{
	Jump_t114869516::get_offset_of_img_arrow_4(),
	Jump_t114869516::get_offset_of_img_x_5(),
	Jump_t114869516::get_offset_of_rotation_6(),
	Jump_t114869516::get_offset_of__jumpNumber_7(),
	Jump_t114869516::get_offset_of__jumpCombination_8(),
	Jump_t114869516::get_offset_of_Collider1_9(),
	Jump_t114869516::get_offset_of_Collider2_10(),
	Jump_t114869516::get_offset_of_Circle_11(),
	Jump_t114869516::get_offset_of_Select_12(),
	Jump_t114869516::get_offset_of_number_13(),
	Jump_t114869516::get_offset_of_jumpNumberText_14(),
	Jump_t114869516::get_offset_of_underline_15(),
	Jump_t114869516::get_offset_of_RotataableItems_16(),
	Jump_t114869516::get_offset_of_Jump_1_17(),
	Jump_t114869516::get_offset_of_Jump_2_18(),
	Jump_t114869516::get_offset_of_Jump_3_19(),
	Jump_t114869516::get_offset_of_Jump_6_20(),
	Jump_t114869516::get_offset_of_Jump_7_21(),
	Jump_t114869516::get_offset_of_Jump_8_22(),
	Jump_t114869516::get_offset_of_JumpTypeImage_23(),
	Jump_t114869516::get_offset_of_numberPanel_24(),
	Jump_t114869516::get_offset_of_U3CjumpOrderU3Ek__BackingField_25(),
	Jump_t114869516::get_offset_of_U3CjumpTypeU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (Jump_Collider_t2378236225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3068[1] = 
{
	Jump_Collider_t2378236225::get_offset_of_Parent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (CropSprite_t3868165965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3069[8] = 
{
	CropSprite_t3868165965::get_offset_of_spriteToCrop_2(),
	CropSprite_t3868165965::get_offset_of_startPoint_3(),
	CropSprite_t3868165965::get_offset_of_endPoint_4(),
	CropSprite_t3868165965::get_offset_of_isMousePressed_5(),
	CropSprite_t3868165965::get_offset_of_leftLine_6(),
	CropSprite_t3868165965::get_offset_of_rightLine_7(),
	CropSprite_t3868165965::get_offset_of_topLine_8(),
	CropSprite_t3868165965::get_offset_of_bottomLine_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (Details_Controller_t3848679003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[5] = 
{
	Details_Controller_t3848679003::get_offset_of_txt_name_4(),
	Details_Controller_t3848679003::get_offset_of_txt_location_5(),
	Details_Controller_t3848679003::get_offset_of_txt_date_6(),
	Details_Controller_t3848679003::get_offset_of_txt_notes_7(),
	Details_Controller_t3848679003::get_offset_of_btn_done_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (ShowDetailsEventArgs_t1623880762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[1] = 
{
	ShowDetailsEventArgs_t1623880762::get_offset_of_U3CCourseU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (ErrorMessage_t521535845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3072[3] = 
{
	ErrorMessage_t521535845::get_offset_of_Message_4(),
	ErrorMessage_t521535845::get_offset_of_btn_close_5(),
	ErrorMessage_t521535845::get_offset_of_onDismiss_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (ShowErrorEventArgs_t2135289798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3073[2] = 
{
	ShowErrorEventArgs_t2135289798::get_offset_of_U3CMessageU3Ek__BackingField_0(),
	ShowErrorEventArgs_t2135289798::get_offset_of_U3ConDismissU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (del_t1625342804), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (ExifIO_t1291478686), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (JpegId_t1946960615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3076[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (ExifIFD_t1652672661)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3077[3] = 
{
	ExifIFD_t1652672661::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (ExifId_t343747131)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3078[21] = 
{
	ExifId_t343747131::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (ExifGps_t3527933044)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3079[32] = 
{
	ExifGps_t3527933044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (ExifOrientation_t1205752066)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3080[6] = 
{
	ExifOrientation_t1205752066::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (ExifUnit_t2084677226)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3081[4] = 
{
	ExifUnit_t2084677226::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (ExifFlash_t1670535696)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3082[9] = 
{
	ExifFlash_t1670535696::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (ExifGpsLatitudeRef_t1514933379)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3083[4] = 
{
	ExifGpsLatitudeRef_t1514933379::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (ExifGpsLongitudeRef_t1142847722)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3084[4] = 
{
	ExifGpsLongitudeRef_t1142847722::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (ExifReader_t3799989581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3085[2] = 
{
	ExifReader_t3799989581::get_offset_of_littleEndian_0(),
	ExifReader_t3799989581::get_offset_of_U3CinfoU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (ExifTagFormat_t3078035447)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3086[14] = 
{
	ExifTagFormat_t3078035447::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (ExifTag_t1511511074), -1, sizeof(ExifTag_t1511511074_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3087[7] = 
{
	ExifTag_t1511511074_StaticFields::get_offset_of_BytesPerFormat_0(),
	ExifTag_t1511511074::get_offset_of_U3CTagU3Ek__BackingField_1(),
	ExifTag_t1511511074::get_offset_of_U3CFormatU3Ek__BackingField_2(),
	ExifTag_t1511511074::get_offset_of_U3CComponentsU3Ek__BackingField_3(),
	ExifTag_t1511511074::get_offset_of_U3CDataU3Ek__BackingField_4(),
	ExifTag_t1511511074::get_offset_of_U3CLittleEndianU3Ek__BackingField_5(),
	ExifTag_t1511511074::get_offset_of_U3CIsValidU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (JpegInfo_t3114827956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3088[29] = 
{
	JpegInfo_t3114827956::get_offset_of_FileName_0(),
	JpegInfo_t3114827956::get_offset_of_FileSize_1(),
	JpegInfo_t3114827956::get_offset_of_IsValid_2(),
	JpegInfo_t3114827956::get_offset_of_Width_3(),
	JpegInfo_t3114827956::get_offset_of_Height_4(),
	JpegInfo_t3114827956::get_offset_of_IsColor_5(),
	JpegInfo_t3114827956::get_offset_of_Orientation_6(),
	JpegInfo_t3114827956::get_offset_of_XResolution_7(),
	JpegInfo_t3114827956::get_offset_of_YResolution_8(),
	JpegInfo_t3114827956::get_offset_of_ResolutionUnit_9(),
	JpegInfo_t3114827956::get_offset_of_DateTime_10(),
	JpegInfo_t3114827956::get_offset_of_Description_11(),
	JpegInfo_t3114827956::get_offset_of_Make_12(),
	JpegInfo_t3114827956::get_offset_of_Model_13(),
	JpegInfo_t3114827956::get_offset_of_Software_14(),
	JpegInfo_t3114827956::get_offset_of_Artist_15(),
	JpegInfo_t3114827956::get_offset_of_Copyright_16(),
	JpegInfo_t3114827956::get_offset_of_UserComment_17(),
	JpegInfo_t3114827956::get_offset_of_ExposureTime_18(),
	JpegInfo_t3114827956::get_offset_of_FNumber_19(),
	JpegInfo_t3114827956::get_offset_of_Flash_20(),
	JpegInfo_t3114827956::get_offset_of_GpsLatitudeRef_21(),
	JpegInfo_t3114827956::get_offset_of_GpsLatitude_22(),
	JpegInfo_t3114827956::get_offset_of_GpsLongitudeRef_23(),
	JpegInfo_t3114827956::get_offset_of_GpsLongitude_24(),
	JpegInfo_t3114827956::get_offset_of_ThumbnailOffset_25(),
	JpegInfo_t3114827956::get_offset_of_ThumbnailSize_26(),
	JpegInfo_t3114827956::get_offset_of_ThumbnailData_27(),
	JpegInfo_t3114827956::get_offset_of_LoadTime_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (GlobalData_t2590907497), -1, sizeof(GlobalData_t2590907497_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3089[13] = 
{
	0,
	0,
	GlobalData_t2590907497_StaticFields::get_offset_of_inapp_unlimitedCourses_4(),
	GlobalData_t2590907497_StaticFields::get_offset_of_inapp_env_horseShow_5(),
	GlobalData_t2590907497_StaticFields::get_offset_of_inapp_env_indoor_6(),
	GlobalData_t2590907497_StaticFields::get_offset_of_visualizer_env_7(),
	GlobalData_t2590907497_StaticFields::get_offset_of_Courses_8(),
	GlobalData_t2590907497_StaticFields::get_offset_of_CoursesMaxCount_9(),
	GlobalData_t2590907497_StaticFields::get_offset_of_CurrentCourse_10(),
	GlobalData_t2590907497_StaticFields::get_offset_of_SaveThroughLogin_11(),
	GlobalData_t2590907497_StaticFields::get_offset_of_scenes_12(),
	GlobalData_t2590907497_StaticFields::get_offset_of_U3CUserU3Ek__BackingField_13(),
	GlobalData_t2590907497_StaticFields::get_offset_of_U3CscaleU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (User_t719925459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3090[3] = 
{
	User_t719925459::get_offset_of_U3CIdU3Ek__BackingField_0(),
	User_t719925459::get_offset_of_U3CFullNameU3Ek__BackingField_1(),
	User_t719925459::get_offset_of_U3CEmailU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (Course_t3483112699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3091[14] = 
{
	Course_t3483112699::get_offset_of_ImagePosition_0(),
	Course_t3483112699::get_offset_of_ImageRotation_1(),
	Course_t3483112699::get_offset_of_ImageScale_2(),
	Course_t3483112699::get_offset_of_course_show_cheats_3(),
	Course_t3483112699::get_offset_of_course_show_image_4(),
	Course_t3483112699::get_offset_of_linePoints_5(),
	Course_t3483112699::get_offset_of_U3CIdU3Ek__BackingField_6(),
	Course_t3483112699::get_offset_of_U3CImageU3Ek__BackingField_7(),
	Course_t3483112699::get_offset_of_U3CNameU3Ek__BackingField_8(),
	Course_t3483112699::get_offset_of_U3CShowU3Ek__BackingField_9(),
	Course_t3483112699::get_offset_of_U3CDateU3Ek__BackingField_10(),
	Course_t3483112699::get_offset_of_U3CNotesU3Ek__BackingField_11(),
	Course_t3483112699::get_offset_of_U3CJumpsU3Ek__BackingField_12(),
	Course_t3483112699::get_offset_of_U3CScaleU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (Crop_t2548131222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3092[15] = 
{
	Crop_t2548131222::get_offset_of_resultImage_2(),
	Crop_t2548131222::get_offset_of_action_1finger_3(),
	Crop_t2548131222::get_offset_of_action_1finger_waitForEpsilon_4(),
	Crop_t2548131222::get_offset_of_waitForEpsilon_v0_5(),
	Crop_t2548131222::get_offset_of_move1Epsilon_6(),
	Crop_t2548131222::get_offset_of_action_2fingers_7(),
	Crop_t2548131222::get_offset_of_v0_8(),
	Crop_t2548131222::get_offset_of_v1_9(),
	Crop_t2548131222::get_offset_of_v_10(),
	Crop_t2548131222::get_offset_of_v_angle_11(),
	Crop_t2548131222::get_offset_of_currentTouchCancelled_12(),
	Crop_t2548131222::get_offset_of_anyTouches_13(),
	Crop_t2548131222::get_offset_of_touchInArea_14(),
	Crop_t2548131222::get_offset_of_lastTouchCount_15(),
	Crop_t2548131222::get_offset_of_touchCount_once_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (ImageUploader_t3573860837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3093[15] = 
{
	ImageUploader_t3573860837::get_offset_of_resultImagePlane_4(),
	ImageUploader_t3573860837::get_offset_of_resultFail_5(),
	ImageUploader_t3573860837::get_offset_of_waiting_6(),
	ImageUploader_t3573860837::get_offset_of_btn_skip_7(),
	ImageUploader_t3573860837::get_offset_of_btn_continue_8(),
	ImageUploader_t3573860837::get_offset_of_btn_closeImageFail_9(),
	ImageUploader_t3573860837::get_offset_of_input_name_10(),
	ImageUploader_t3573860837::get_offset_of_input_location_11(),
	ImageUploader_t3573860837::get_offset_of_input_notes_12(),
	ImageUploader_t3573860837::get_offset_of_resultImage_13(),
	ImageUploader_t3573860837::get_offset_of_debug_14(),
	ImageUploader_t3573860837::get_offset_of_btn_rotate_15(),
	ImageUploader_t3573860837::get_offset_of_localPosition0_16(),
	ImageUploader_t3573860837::get_offset_of_localRotation0_17(),
	ImageUploader_t3573860837::get_offset_of_localScale0_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (ImageUploaderPageManager_t1025178313), -1, sizeof(ImageUploaderPageManager_t1025178313_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3094[18] = 
{
	ImageUploaderPageManager_t1025178313::get_offset_of_btn_crop_2(),
	ImageUploaderPageManager_t1025178313::get_offset_of_btn_details_3(),
	ImageUploaderPageManager_t1025178313::get_offset_of_btn_back_4(),
	ImageUploaderPageManager_t1025178313::get_offset_of_btn_info_5(),
	ImageUploaderPageManager_t1025178313::get_offset_of_btn_skip_6(),
	ImageUploaderPageManager_t1025178313::get_offset_of_btn_rotate_container_7(),
	ImageUploaderPageManager_t1025178313::get_offset_of_cropSelected_8(),
	ImageUploaderPageManager_t1025178313::get_offset_of_cropUnselected_9(),
	ImageUploaderPageManager_t1025178313::get_offset_of_detailsSelected_10(),
	ImageUploaderPageManager_t1025178313::get_offset_of_detailsUnselected_11(),
	ImageUploaderPageManager_t1025178313::get_offset_of_selected_12(),
	ImageUploaderPageManager_t1025178313::get_offset_of_cropArea_13(),
	ImageUploaderPageManager_t1025178313::get_offset_of_detailsArea_14(),
	ImageUploaderPageManager_t1025178313::get_offset_of_details_glow_15(),
	ImageUploaderPageManager_t1025178313::get_offset_of_title_16(),
	ImageUploaderPageManager_t1025178313::get_offset_of_detailsSelectedOnce_17(),
	ImageUploaderPageManager_t1025178313::get_offset_of_lastActivityTime_18(),
	ImageUploaderPageManager_t1025178313_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (Login_Controller_t1381689646), -1, sizeof(Login_Controller_t1381689646_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3095[54] = 
{
	Login_Controller_t1381689646::get_offset_of_LoginWait_4(),
	Login_Controller_t1381689646::get_offset_of_MainCamera_5(),
	Login_Controller_t1381689646::get_offset_of_ErrorMessage_6(),
	Login_Controller_t1381689646::get_offset_of_btn_signIn_7(),
	Login_Controller_t1381689646::get_offset_of_btn_signUp_8(),
	Login_Controller_t1381689646::get_offset_of_btn_signCancel_9(),
	Login_Controller_t1381689646::get_offset_of_btn_signSubmit_10(),
	Login_Controller_t1381689646::get_offset_of_btn_close1_11(),
	Login_Controller_t1381689646::get_offset_of_btn_close2_12(),
	Login_Controller_t1381689646::get_offset_of_btn_close3_13(),
	Login_Controller_t1381689646::get_offset_of_btn_close4_14(),
	Login_Controller_t1381689646::get_offset_of_btn_close5_15(),
	Login_Controller_t1381689646::get_offset_of_UserEmail_16(),
	Login_Controller_t1381689646::get_offset_of_UserPassword_17(),
	Login_Controller_t1381689646::get_offset_of_btn_forgotPassword_18(),
	Login_Controller_t1381689646::get_offset_of_SignInPage_19(),
	Login_Controller_t1381689646::get_offset_of_SignUpPage_20(),
	Login_Controller_t1381689646::get_offset_of_MainPage_21(),
	Login_Controller_t1381689646::get_offset_of_ForgotPasswordPage_22(),
	Login_Controller_t1381689646::get_offset_of_ChangePasswordPage_23(),
	Login_Controller_t1381689646::get_offset_of_forgotEmail_24(),
	Login_Controller_t1381689646::get_offset_of_btn_forgotCancel_25(),
	Login_Controller_t1381689646::get_offset_of_btn_forgotSubmit_26(),
	Login_Controller_t1381689646::get_offset_of_changePassword_27(),
	Login_Controller_t1381689646::get_offset_of_changeReenterPassword_28(),
	Login_Controller_t1381689646::get_offset_of_btn_changeCancel_29(),
	Login_Controller_t1381689646::get_offset_of_btn_changeSubmit_30(),
	Login_Controller_t1381689646::get_offset_of_FullName_31(),
	Login_Controller_t1381689646::get_offset_of_Email_32(),
	Login_Controller_t1381689646::get_offset_of_Password_33(),
	Login_Controller_t1381689646::get_offset_of_ConfirmPassword_34(),
	Login_Controller_t1381689646::get_offset_of_Zone1_35(),
	Login_Controller_t1381689646::get_offset_of_Zone2_36(),
	Login_Controller_t1381689646::get_offset_of_Zone3_37(),
	Login_Controller_t1381689646::get_offset_of_Zone4_38(),
	Login_Controller_t1381689646::get_offset_of_Zone5_39(),
	Login_Controller_t1381689646::get_offset_of_Zone6_40(),
	Login_Controller_t1381689646::get_offset_of_Zone7_41(),
	Login_Controller_t1381689646::get_offset_of_Zone8_42(),
	Login_Controller_t1381689646::get_offset_of_Zone9_43(),
	Login_Controller_t1381689646::get_offset_of_Zone10_44(),
	Login_Controller_t1381689646::get_offset_of_Zone11_45(),
	Login_Controller_t1381689646::get_offset_of_Zone12_46(),
	Login_Controller_t1381689646::get_offset_of_Discipline1_47(),
	Login_Controller_t1381689646::get_offset_of_Discipline2_48(),
	Login_Controller_t1381689646::get_offset_of_Discipline3_49(),
	Login_Controller_t1381689646::get_offset_of_selectedZones_50(),
	Login_Controller_t1381689646::get_offset_of_selectedDisciplines_51(),
	Login_Controller_t1381689646::get_offset_of_btn_cancel_52(),
	Login_Controller_t1381689646::get_offset_of_btn_join_53(),
	Login_Controller_t1381689646::get_offset_of_Terms_54(),
	Login_Controller_t1381689646::get_offset_of_btn_terms_55(),
	Login_Controller_t1381689646::get_offset_of_CurrentScreen_56(),
	Login_Controller_t1381689646_StaticFields::get_offset_of_U3CU3Ef__amU24cache35_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (LoginRequest_t2273172322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3096[4] = 
{
	LoginRequest_t2273172322::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	LoginRequest_t2273172322::get_offset_of_U3CEmailU3Ek__BackingField_3(),
	LoginRequest_t2273172322::get_offset_of_U3CPasswordU3Ek__BackingField_4(),
	LoginRequest_t2273172322::get_offset_of_U3CAutoLoginU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (LoginResponse_t1319408382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3097[7] = 
{
	LoginResponse_t1319408382::get_offset_of_U3CSuccessU3Ek__BackingField_0(),
	LoginResponse_t1319408382::get_offset_of_U3CErrorMessageU3Ek__BackingField_1(),
	LoginResponse_t1319408382::get_offset_of_U3CRequestU3Ek__BackingField_2(),
	LoginResponse_t1319408382::get_offset_of_U3CIsNewPasswordU3Ek__BackingField_3(),
	LoginResponse_t1319408382::get_offset_of_U3CAutoLoginU3Ek__BackingField_4(),
	LoginResponse_t1319408382::get_offset_of_U3CUserU3Ek__BackingField_5(),
	LoginResponse_t1319408382::get_offset_of_U3CCoursesU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (RegisterRequest_t698226714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3098[6] = 
{
	RegisterRequest_t698226714::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	RegisterRequest_t698226714::get_offset_of_U3CFullNameU3Ek__BackingField_3(),
	RegisterRequest_t698226714::get_offset_of_U3CEmailU3Ek__BackingField_4(),
	RegisterRequest_t698226714::get_offset_of_U3CPasswordU3Ek__BackingField_5(),
	RegisterRequest_t698226714::get_offset_of_U3CZonesU3Ek__BackingField_6(),
	RegisterRequest_t698226714::get_offset_of_U3CDisiplineU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (RegisterResponse_t410466074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3099[5] = 
{
	RegisterResponse_t410466074::get_offset_of_U3CSuccessU3Ek__BackingField_0(),
	RegisterResponse_t410466074::get_offset_of_U3CErrorMessageU3Ek__BackingField_1(),
	RegisterResponse_t410466074::get_offset_of_U3CRequestU3Ek__BackingField_2(),
	RegisterResponse_t410466074::get_offset_of_U3CAutoLoginU3Ek__BackingField_3(),
	RegisterResponse_t410466074::get_offset_of_U3CUserU3Ek__BackingField_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
