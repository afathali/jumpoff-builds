﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"

// System.Void System.Predicate`1<FB_UserInfo>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m567008325(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t1147548193 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2910895094_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<FB_UserInfo>::Invoke(T)
#define Predicate_1_Invoke_m3422670689(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1147548193 *, FB_UserInfo_t2704578078 *, const MethodInfo*))Predicate_1_Invoke_m921875257_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<FB_UserInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1944606442(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t1147548193 *, FB_UserInfo_t2704578078 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3556950370_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<FB_UserInfo>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m1226967815(__this, ___result0, method) ((  bool (*) (Predicate_1_t1147548193 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3656575065_gshared)(__this, ___result0, method)
