﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1393666460.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_534914198.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1929634852_gshared (InternalEnumerator_1_t1393666460 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1929634852(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1393666460 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1929634852_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1643377252_gshared (InternalEnumerator_1_t1393666460 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1643377252(__this, method) ((  void (*) (InternalEnumerator_1_t1393666460 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1643377252_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2927340424_gshared (InternalEnumerator_1_t1393666460 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2927340424(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1393666460 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2927340424_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4288466531_gshared (InternalEnumerator_1_t1393666460 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4288466531(__this, method) ((  void (*) (InternalEnumerator_1_t1393666460 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4288466531_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1668992460_gshared (InternalEnumerator_1_t1393666460 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1668992460(__this, method) ((  bool (*) (InternalEnumerator_1_t1393666460 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1668992460_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<FB_ProfileImageSize,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t534914198  InternalEnumerator_1_get_Current_m1274068803_gshared (InternalEnumerator_1_t1393666460 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1274068803(__this, method) ((  KeyValuePair_2_t534914198  (*) (InternalEnumerator_1_t1393666460 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1274068803_gshared)(__this, method)
