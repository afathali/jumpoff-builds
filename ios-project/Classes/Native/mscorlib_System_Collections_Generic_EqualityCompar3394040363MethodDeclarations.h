﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<AN_ManifestPermission>
struct DefaultComparer_t3394040363;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AN_ManifestPermission1772110202.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<AN_ManifestPermission>::.ctor()
extern "C"  void DefaultComparer__ctor_m2616793564_gshared (DefaultComparer_t3394040363 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2616793564(__this, method) ((  void (*) (DefaultComparer_t3394040363 *, const MethodInfo*))DefaultComparer__ctor_m2616793564_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<AN_ManifestPermission>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1940578447_gshared (DefaultComparer_t3394040363 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1940578447(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3394040363 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1940578447_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<AN_ManifestPermission>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3041668447_gshared (DefaultComparer_t3394040363 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3041668447(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3394040363 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3041668447_gshared)(__this, ___x0, ___y1, method)
