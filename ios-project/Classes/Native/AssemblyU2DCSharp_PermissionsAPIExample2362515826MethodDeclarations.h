﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PermissionsAPIExample
struct PermissionsAPIExample_t2362515826;
// AN_GrantPermissionsResult
struct AN_GrantPermissionsResult_t250489657;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AN_GrantPermissionsResult250489657.h"

// System.Void PermissionsAPIExample::.ctor()
extern "C"  void PermissionsAPIExample__ctor_m1653889219 (PermissionsAPIExample_t2362515826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PermissionsAPIExample::Awake()
extern "C"  void PermissionsAPIExample_Awake_m3141285858 (PermissionsAPIExample_t2362515826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PermissionsAPIExample::CheckPermission()
extern "C"  void PermissionsAPIExample_CheckPermission_m2300361382 (PermissionsAPIExample_t2362515826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PermissionsAPIExample::RequestPermission()
extern "C"  void PermissionsAPIExample_RequestPermission_m3558457121 (PermissionsAPIExample_t2362515826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PermissionsAPIExample::HandleActionPermissionsRequestCompleted(AN_GrantPermissionsResult)
extern "C"  void PermissionsAPIExample_HandleActionPermissionsRequestCompleted_m1525624646 (PermissionsAPIExample_t2362515826 * __this, AN_GrantPermissionsResult_t250489657 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
