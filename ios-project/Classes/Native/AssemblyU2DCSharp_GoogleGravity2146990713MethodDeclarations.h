﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoogleGravity
struct GoogleGravity_t2146990713;

#include "codegen/il2cpp-codegen.h"

// System.Void GoogleGravity::.ctor()
extern "C"  void GoogleGravity__ctor_m2743283334 (GoogleGravity_t2146990713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
