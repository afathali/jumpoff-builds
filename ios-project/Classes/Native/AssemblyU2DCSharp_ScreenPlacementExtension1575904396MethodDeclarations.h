﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_ScreenPosition2449785599.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void ScreenPlacementExtension::ScreenPlacement(UnityEngine.GameObject,ScreenPosition)
extern "C"  void ScreenPlacementExtension_ScreenPlacement_m4209059795 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, int32_t ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenPlacementExtension::ScreenPlacement(UnityEngine.GameObject,ScreenPosition,UnityEngine.Camera)
extern "C"  void ScreenPlacementExtension_ScreenPlacement_m3316474781 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, int32_t ___position1, Camera_t189460977 * ___renderingCamera2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenPlacementExtension::ScreenPlacement(UnityEngine.GameObject,ScreenPosition,UnityEngine.Vector2)
extern "C"  void ScreenPlacementExtension_ScreenPlacement_m1981176495 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, int32_t ___position1, Vector2_t2243707579  ___pixelsFromEdge2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenPlacementExtension::ScreenPlacement(UnityEngine.GameObject,ScreenPosition,UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  void ScreenPlacementExtension_ScreenPlacement_m2539623861 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, int32_t ___position1, Vector2_t2243707579  ___pixelsFromEdge2, Camera_t189460977 * ___renderingCamera3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenPlacementExtension::ScreenPlacement(UnityEngine.Transform,ScreenPosition)
extern "C"  void ScreenPlacementExtension_ScreenPlacement_m2347308792 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, int32_t ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenPlacementExtension::ScreenPlacement(UnityEngine.Transform,ScreenPosition,UnityEngine.Camera)
extern "C"  void ScreenPlacementExtension_ScreenPlacement_m2673153818 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, int32_t ___position1, Camera_t189460977 * ___renderingCamera2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenPlacementExtension::ScreenPlacement(UnityEngine.Transform,ScreenPosition,UnityEngine.Vector2)
extern "C"  void ScreenPlacementExtension_ScreenPlacement_m3781730490 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, int32_t ___position1, Vector2_t2243707579  ___pixelsFromEdge2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenPlacementExtension::ScreenPlacement(UnityEngine.Transform,ScreenPosition,UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  void ScreenPlacementExtension_ScreenPlacement_m2336767112 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, int32_t ___position1, Vector2_t2243707579  ___pixelsFromEdge2, Camera_t189460977 * ___renderingCamera3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenPlacementExtension::DoScreenPlacement(UnityEngine.Transform,ScreenPosition,UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  void ScreenPlacementExtension_DoScreenPlacement_m150853069 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, int32_t ___position1, Vector2_t2243707579  ___pixelsFromEdge2, Camera_t189460977 * ___renderingCamera3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
