﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSDateTimePicker
struct IOSDateTimePicker_t849222074;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSDateTimePickerMod3227591715.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_String2029220233.h"

// System.Void IOSDateTimePicker::.ctor()
extern "C"  void IOSDateTimePicker__ctor_m1994472859 (IOSDateTimePicker_t849222074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::_ISN_ShowDP(System.Int32)
extern "C"  void IOSDateTimePicker__ISN_ShowDP_m1329582153 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::_ISN_ShowDPWithTime(System.Int32,System.Double)
extern "C"  void IOSDateTimePicker__ISN_ShowDPWithTime_m3020280372 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, double ___seconds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::Awake()
extern "C"  void IOSDateTimePicker_Awake_m386806804 (IOSDateTimePicker_t849222074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::Show(IOSDateTimePickerMode)
extern "C"  void IOSDateTimePicker_Show_m1036054265 (IOSDateTimePicker_t849222074 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::Show(IOSDateTimePickerMode,System.DateTime)
extern "C"  void IOSDateTimePicker_Show_m3965559211 (IOSDateTimePicker_t849222074 * __this, int32_t ___mode0, DateTime_t693205669  ___dateTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::DateChangedEvent(System.String)
extern "C"  void IOSDateTimePicker_DateChangedEvent_m678486827 (IOSDateTimePicker_t849222074 * __this, String_t* ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::PickerClosed(System.String)
extern "C"  void IOSDateTimePicker_PickerClosed_m4067280813 (IOSDateTimePicker_t849222074 * __this, String_t* ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::<OnDateChanged>m__63(System.DateTime)
extern "C"  void IOSDateTimePicker_U3COnDateChangedU3Em__63_m3916265866 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSDateTimePicker::<OnPickerClosed>m__64(System.DateTime)
extern "C"  void IOSDateTimePicker_U3COnPickerClosedU3Em__64_m155752137 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
