﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SK_AuthorizationResult
struct SK_AuthorizationResult_t2047753871;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SK_CloudServiceAutho3247563438.h"

// System.Void SK_AuthorizationResult::.ctor(SK_CloudServiceAuthorizationStatus)
extern "C"  void SK_AuthorizationResult__ctor_m1369504372 (SK_AuthorizationResult_t2047753871 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SK_CloudServiceAuthorizationStatus SK_AuthorizationResult::get_AuthorizationStatus()
extern "C"  int32_t SK_AuthorizationResult_get_AuthorizationStatus_m4090126557 (SK_AuthorizationResult_t2047753871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
