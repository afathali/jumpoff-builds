﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>
struct U3CValuesU3Ec__Iterator12_2_t1507201787;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CValuesU3Ec__Iterator12_2__ctor_m227893975_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator12_2__ctor_m227893975(__this, method) ((  void (*) (U3CValuesU3Ec__Iterator12_2_t1507201787 *, const MethodInfo*))U3CValuesU3Ec__Iterator12_2__ctor_m227893975_gshared)(__this, method)
// U Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<U>.get_Current()
extern "C"  Il2CppObject * U3CValuesU3Ec__Iterator12_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m810599112_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator12_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m810599112(__this, method) ((  Il2CppObject * (*) (U3CValuesU3Ec__Iterator12_2_t1507201787 *, const MethodInfo*))U3CValuesU3Ec__Iterator12_2_System_Collections_Generic_IEnumeratorU3CUU3E_get_Current_m810599112_gshared)(__this, method)
// System.Object Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CValuesU3Ec__Iterator12_2_System_Collections_IEnumerator_get_Current_m20675489_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator12_2_System_Collections_IEnumerator_get_Current_m20675489(__this, method) ((  Il2CppObject * (*) (U3CValuesU3Ec__Iterator12_2_t1507201787 *, const MethodInfo*))U3CValuesU3Ec__Iterator12_2_System_Collections_IEnumerator_get_Current_m20675489_gshared)(__this, method)
// System.Collections.IEnumerator Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CValuesU3Ec__Iterator12_2_System_Collections_IEnumerable_GetEnumerator_m2590481248_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator12_2_System_Collections_IEnumerable_GetEnumerator_m2590481248(__this, method) ((  Il2CppObject * (*) (U3CValuesU3Ec__Iterator12_2_t1507201787 *, const MethodInfo*))U3CValuesU3Ec__Iterator12_2_System_Collections_IEnumerable_GetEnumerator_m2590481248_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<U> Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<U>.GetEnumerator()
extern "C"  Il2CppObject* U3CValuesU3Ec__Iterator12_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m685739343_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator12_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m685739343(__this, method) ((  Il2CppObject* (*) (U3CValuesU3Ec__Iterator12_2_t1507201787 *, const MethodInfo*))U3CValuesU3Ec__Iterator12_2_System_Collections_Generic_IEnumerableU3CUU3E_GetEnumerator_m685739343_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::MoveNext()
extern "C"  bool U3CValuesU3Ec__Iterator12_2_MoveNext_m3515628229_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator12_2_MoveNext_m3515628229(__this, method) ((  bool (*) (U3CValuesU3Ec__Iterator12_2_t1507201787 *, const MethodInfo*))U3CValuesU3Ec__Iterator12_2_MoveNext_m3515628229_gshared)(__this, method)
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CValuesU3Ec__Iterator12_2_Dispose_m1103924608_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator12_2_Dispose_m1103924608(__this, method) ((  void (*) (U3CValuesU3Ec__Iterator12_2_t1507201787 *, const MethodInfo*))U3CValuesU3Ec__Iterator12_2_Dispose_m1103924608_gshared)(__this, method)
// System.Void Newtonsoft.Json.Linq.LinqExtensions/<Values>c__Iterator12`2<System.Object,System.Object>::Reset()
extern "C"  void U3CValuesU3Ec__Iterator12_2_Reset_m2841252134_gshared (U3CValuesU3Ec__Iterator12_2_t1507201787 * __this, const MethodInfo* method);
#define U3CValuesU3Ec__Iterator12_2_Reset_m2841252134(__this, method) ((  void (*) (U3CValuesU3Ec__Iterator12_2_t1507201787 *, const MethodInfo*))U3CValuesU3Ec__Iterator12_2_Reset_m2841252134_gshared)(__this, method)
