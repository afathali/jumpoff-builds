﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnalyticsUseExample
struct AnalyticsUseExample_t751986731;

#include "codegen/il2cpp-codegen.h"

// System.Void AnalyticsUseExample::.ctor()
extern "C"  void AnalyticsUseExample__ctor_m3963016146 (AnalyticsUseExample_t751986731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsUseExample::Awake()
extern "C"  void AnalyticsUseExample_Awake_m852819643 (AnalyticsUseExample_t751986731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsUseExample::Start()
extern "C"  void AnalyticsUseExample_Start_m3315430662 (AnalyticsUseExample_t751986731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsUseExample::PurchaseTackingExample()
extern "C"  void AnalyticsUseExample_PurchaseTackingExample_m659034968 (AnalyticsUseExample_t751986731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
