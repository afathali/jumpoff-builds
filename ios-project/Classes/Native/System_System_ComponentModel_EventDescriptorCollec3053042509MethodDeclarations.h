﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.EventDescriptorCollection
struct EventDescriptorCollection_t3053042509;
// System.ComponentModel.EventDescriptor[]
struct EventDescriptorU5BU5D_t2599422448;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Array
struct Il2CppArray;
// System.ComponentModel.EventDescriptor
struct EventDescriptor_t962731901;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_ComponentModel_EventDescriptor962731901.h"

// System.Void System.ComponentModel.EventDescriptorCollection::.ctor(System.ComponentModel.EventDescriptor[],System.Boolean)
extern "C"  void EventDescriptorCollection__ctor_m3188039553 (EventDescriptorCollection_t3053042509 * __this, EventDescriptorU5BU5D_t2599422448* ___events0, bool ___readOnly1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EventDescriptorCollection::.cctor()
extern "C"  void EventDescriptorCollection__cctor_m4270642981 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EventDescriptorCollection::System.Collections.IList.Clear()
extern "C"  void EventDescriptorCollection_System_Collections_IList_Clear_m2748019132 (EventDescriptorCollection_t3053042509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.ComponentModel.EventDescriptorCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * EventDescriptorCollection_System_Collections_IEnumerable_GetEnumerator_m2721463831 (EventDescriptorCollection_t3053042509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EventDescriptorCollection::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void EventDescriptorCollection_System_Collections_IList_RemoveAt_m3022137793 (EventDescriptorCollection_t3053042509 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.EventDescriptorCollection::System.Collections.ICollection.get_Count()
extern "C"  int32_t EventDescriptorCollection_System_Collections_ICollection_get_Count_m2014796415 (EventDescriptorCollection_t3053042509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.EventDescriptorCollection::System.Collections.IList.Add(System.Object)
extern "C"  int32_t EventDescriptorCollection_System_Collections_IList_Add_m3160558838 (EventDescriptorCollection_t3053042509 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.EventDescriptorCollection::System.Collections.IList.Contains(System.Object)
extern "C"  bool EventDescriptorCollection_System_Collections_IList_Contains_m960541688 (EventDescriptorCollection_t3053042509 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.EventDescriptorCollection::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t EventDescriptorCollection_System_Collections_IList_IndexOf_m3950327564 (EventDescriptorCollection_t3053042509 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EventDescriptorCollection::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void EventDescriptorCollection_System_Collections_IList_Insert_m1530970263 (EventDescriptorCollection_t3053042509 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EventDescriptorCollection::System.Collections.IList.Remove(System.Object)
extern "C"  void EventDescriptorCollection_System_Collections_IList_Remove_m2344817903 (EventDescriptorCollection_t3053042509 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.EventDescriptorCollection::System.Collections.IList.get_IsFixedSize()
extern "C"  bool EventDescriptorCollection_System_Collections_IList_get_IsFixedSize_m4243861183 (EventDescriptorCollection_t3053042509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.EventDescriptorCollection::System.Collections.IList.get_IsReadOnly()
extern "C"  bool EventDescriptorCollection_System_Collections_IList_get_IsReadOnly_m3626269354 (EventDescriptorCollection_t3053042509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.EventDescriptorCollection::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * EventDescriptorCollection_System_Collections_IList_get_Item_m1399118971 (EventDescriptorCollection_t3053042509 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EventDescriptorCollection::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void EventDescriptorCollection_System_Collections_IList_set_Item_m661286160 (EventDescriptorCollection_t3053042509 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EventDescriptorCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void EventDescriptorCollection_System_Collections_ICollection_CopyTo_m3746463922 (EventDescriptorCollection_t3053042509 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.EventDescriptorCollection::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool EventDescriptorCollection_System_Collections_ICollection_get_IsSynchronized_m1277634958 (EventDescriptorCollection_t3053042509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.EventDescriptorCollection::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * EventDescriptorCollection_System_Collections_ICollection_get_SyncRoot_m1184452744 (EventDescriptorCollection_t3053042509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.EventDescriptorCollection::Add(System.ComponentModel.EventDescriptor)
extern "C"  int32_t EventDescriptorCollection_Add_m348090267 (EventDescriptorCollection_t3053042509 * __this, EventDescriptor_t962731901 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EventDescriptorCollection::Clear()
extern "C"  void EventDescriptorCollection_Clear_m1155557215 (EventDescriptorCollection_t3053042509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.EventDescriptorCollection::Contains(System.ComponentModel.EventDescriptor)
extern "C"  bool EventDescriptorCollection_Contains_m3455944259 (EventDescriptorCollection_t3053042509 * __this, EventDescriptor_t962731901 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.ComponentModel.EventDescriptorCollection::GetEnumerator()
extern "C"  Il2CppObject * EventDescriptorCollection_GetEnumerator_m2636733690 (EventDescriptorCollection_t3053042509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.EventDescriptorCollection::IndexOf(System.ComponentModel.EventDescriptor)
extern "C"  int32_t EventDescriptorCollection_IndexOf_m3026506957 (EventDescriptorCollection_t3053042509 * __this, EventDescriptor_t962731901 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EventDescriptorCollection::Insert(System.Int32,System.ComponentModel.EventDescriptor)
extern "C"  void EventDescriptorCollection_Insert_m1803656474 (EventDescriptorCollection_t3053042509 * __this, int32_t ___index0, EventDescriptor_t962731901 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EventDescriptorCollection::Remove(System.ComponentModel.EventDescriptor)
extern "C"  void EventDescriptorCollection_Remove_m2508467324 (EventDescriptorCollection_t3053042509 * __this, EventDescriptor_t962731901 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EventDescriptorCollection::RemoveAt(System.Int32)
extern "C"  void EventDescriptorCollection_RemoveAt_m1854698440 (EventDescriptorCollection_t3053042509 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.EventDescriptorCollection::get_Count()
extern "C"  int32_t EventDescriptorCollection_get_Count_m3337938086 (EventDescriptorCollection_t3053042509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
