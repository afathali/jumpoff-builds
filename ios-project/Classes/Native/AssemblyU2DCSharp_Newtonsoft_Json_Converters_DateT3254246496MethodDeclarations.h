﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.DateTimeConverterBase
struct DateTimeConverterBase_t3254246496;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void Newtonsoft.Json.Converters.DateTimeConverterBase::.ctor()
extern "C"  void DateTimeConverterBase__ctor_m2968235417 (DateTimeConverterBase_t3254246496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.DateTimeConverterBase::CanConvert(System.Type)
extern "C"  bool DateTimeConverterBase_CanConvert_m4255527901 (DateTimeConverterBase_t3254246496 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
