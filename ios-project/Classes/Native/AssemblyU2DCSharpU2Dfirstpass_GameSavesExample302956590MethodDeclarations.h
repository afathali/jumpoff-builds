﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameSavesExample
struct GameSavesExample_t302956590;
// System.String
struct String_t;
// GK_SavedGame
struct GK_SavedGame_t3320093620;
// System.Collections.Generic.List`1<GK_SavedGame>
struct List_1_t2689214752;
// SA.Common.Models.Result
struct Result_t4287219743;
// GK_SaveResult
struct GK_SaveResult_t3946576453;
// GK_SaveRemoveResult
struct GK_SaveRemoveResult_t539310567;
// GK_SaveDataLoaded
struct GK_SaveDataLoaded_t3684688319;
// GK_FetchResult
struct GK_FetchResult_t1611512656;
// GK_SavesResolveResult
struct GK_SavesResolveResult_t3508055404;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_Common_Models_Res4287219743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SaveResult3946576453.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SaveRemoveResult539310567.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SaveDataLoaded3684688319.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_FetchResult1611512656.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SavesResolveResul3508055404.h"

// System.Void GameSavesExample::.ctor()
extern "C"  void GameSavesExample__ctor_m3184856389 (GameSavesExample_t302956590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::Awake()
extern "C"  void GameSavesExample_Awake_m3812234536 (GameSavesExample_t302956590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::OnGUI()
extern "C"  void GameSavesExample_OnGUI_m3873023503 (GameSavesExample_t302956590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::Save(System.String)
extern "C"  void GameSavesExample_Save_m3396078164 (GameSavesExample_t302956590 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::Fetch()
extern "C"  void GameSavesExample_Fetch_m3083375609 (GameSavesExample_t302956590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::Delete(System.String)
extern "C"  void GameSavesExample_Delete_m3480079398 (GameSavesExample_t302956590 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::Load()
extern "C"  void GameSavesExample_Load_m2063509023 (GameSavesExample_t302956590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::ResolveConflicts()
extern "C"  void GameSavesExample_ResolveConflicts_m1181646864 (GameSavesExample_t302956590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_SavedGame GameSavesExample::GetLoadedSave(System.String)
extern "C"  GK_SavedGame_t3320093620 * GameSavesExample_GetLoadedSave_m1023939704 (GameSavesExample_t302956590 * __this, String_t* ___saveGameName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GK_SavedGame> GameSavesExample::GetConflict()
extern "C"  List_1_t2689214752 * GameSavesExample_GetConflict_m572641386 (GameSavesExample_t302956590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameSavesExample::GetConflictsCount()
extern "C"  int32_t GameSavesExample_GetConflictsCount_m2023478039 (GameSavesExample_t302956590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::CheckSavesOnDuplicates()
extern "C"  void GameSavesExample_CheckSavesOnDuplicates_m710797282 (GameSavesExample_t302956590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::HandleOnAuthFinished(SA.Common.Models.Result)
extern "C"  void GameSavesExample_HandleOnAuthFinished_m1711062826 (GameSavesExample_t302956590 * __this, Result_t4287219743 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::HandleActionGameSaved(GK_SaveResult)
extern "C"  void GameSavesExample_HandleActionGameSaved_m212629713 (GameSavesExample_t302956590 * __this, GK_SaveResult_t3946576453 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::HandleActionSaveRemoved(GK_SaveRemoveResult)
extern "C"  void GameSavesExample_HandleActionSaveRemoved_m1695331003 (GameSavesExample_t302956590 * __this, GK_SaveRemoveResult_t539310567 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::HandleActionDataLoaded(GK_SaveDataLoaded)
extern "C"  void GameSavesExample_HandleActionDataLoaded_m3728603495 (GameSavesExample_t302956590 * __this, GK_SaveDataLoaded_t3684688319 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::HandleActionSavesFetched(GK_FetchResult)
extern "C"  void GameSavesExample_HandleActionSavesFetched_m1434365772 (GameSavesExample_t302956590 * __this, GK_FetchResult_t1611512656 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSavesExample::HandleActionSavesResolved(GK_SavesResolveResult)
extern "C"  void GameSavesExample_HandleActionSavesResolved_m1073006271 (GameSavesExample_t302956590 * __this, GK_SavesResolveResult_t3508055404 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
