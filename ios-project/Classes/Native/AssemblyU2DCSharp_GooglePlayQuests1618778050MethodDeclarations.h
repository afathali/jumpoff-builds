﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayQuests
struct GooglePlayQuests_t1618778050;
// GP_QuestsSelect[]
struct GP_QuestsSelectU5BU5D_t3003656674;
// System.String
struct String_t;
// GP_Quest
struct GP_Quest_t1641883470;
// System.Collections.Generic.List`1<GP_Quest>
struct List_1_t1011004602;
// GP_QuestResult
struct GP_QuestResult_t3390940437;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GP_QuestSortOrder4026176042.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_QuestState1072114879.h"
#include "AssemblyU2DCSharp_GP_QuestResult3390940437.h"

// System.Void GooglePlayQuests::.ctor()
extern "C"  void GooglePlayQuests__ctor_m709724767 (GooglePlayQuests_t1618778050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::.cctor()
extern "C"  void GooglePlayQuests__cctor_m2513526524 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::Awake()
extern "C"  void GooglePlayQuests_Awake_m1344011138 (GooglePlayQuests_t1618778050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::Init()
extern "C"  void GooglePlayQuests_Init_m3960239465 (GooglePlayQuests_t1618778050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::LoadQuests(GP_QuestSortOrder)
extern "C"  void GooglePlayQuests_LoadQuests_m4172875930 (GooglePlayQuests_t1618778050 * __this, int32_t ___sortOrder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::LoadQuests(GP_QuestSortOrder,GP_QuestsSelect[])
extern "C"  void GooglePlayQuests_LoadQuests_m2789075181 (GooglePlayQuests_t1618778050 * __this, int32_t ___sortOrder0, GP_QuestsSelectU5BU5D_t3003656674* ___selectors1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::ShowQuests()
extern "C"  void GooglePlayQuests_ShowQuests_m3907459725 (GooglePlayQuests_t1618778050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::ShowQuests(GP_QuestsSelect[])
extern "C"  void GooglePlayQuests_ShowQuests_m4256297574 (GooglePlayQuests_t1618778050 * __this, GP_QuestsSelectU5BU5D_t3003656674* ___selectors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::AcceptQuest(System.String)
extern "C"  void GooglePlayQuests_AcceptQuest_m2185350035 (GooglePlayQuests_t1618778050 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_Quest GooglePlayQuests::GetQuestById(System.String)
extern "C"  GP_Quest_t1641883470 * GooglePlayQuests_GetQuestById_m1211324768 (GooglePlayQuests_t1618778050 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GP_Quest> GooglePlayQuests::GetQuests()
extern "C"  List_1_t1011004602 * GooglePlayQuests_GetQuests_m3857394835 (GooglePlayQuests_t1618778050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GP_Quest> GooglePlayQuests::GetQuestsByState(GP_QuestState)
extern "C"  List_1_t1011004602 * GooglePlayQuests_GetQuestsByState_m2379191376 (GooglePlayQuests_t1618778050 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::OnGPQuestAccepted(System.String)
extern "C"  void GooglePlayQuests_OnGPQuestAccepted_m3769193578 (GooglePlayQuests_t1618778050 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::OnGPQuestCompleted(System.String)
extern "C"  void GooglePlayQuests_OnGPQuestCompleted_m189087906 (GooglePlayQuests_t1618778050 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::OnGPQuestUpdated(System.String)
extern "C"  void GooglePlayQuests_OnGPQuestUpdated_m2219706600 (GooglePlayQuests_t1618778050 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::OnGPQuestsLoaded(System.String)
extern "C"  void GooglePlayQuests_OnGPQuestsLoaded_m2147603461 (GooglePlayQuests_t1618778050 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::UpdateQuestInfo(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void GooglePlayQuests_UpdateQuestInfo_m1325862430 (GooglePlayQuests_t1618778050 * __this, String_t* ___id0, String_t* ___name1, String_t* ___descr2, String_t* ___icon3, String_t* ___banner4, String_t* ___state5, String_t* ___timeUpdated6, String_t* ___timeAccepted7, String_t* ___timeEnded8, String_t* ___rewardData9, String_t* ___currentProgress10, String_t* ___targetProgress11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::<OnQuestsLoaded>m__4D(GP_QuestResult)
extern "C"  void GooglePlayQuests_U3COnQuestsLoadedU3Em__4D_m994054444 (Il2CppObject * __this /* static, unused */, GP_QuestResult_t3390940437 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::<OnQuestsAccepted>m__4E(GP_QuestResult)
extern "C"  void GooglePlayQuests_U3COnQuestsAcceptedU3Em__4E_m2200616139 (Il2CppObject * __this /* static, unused */, GP_QuestResult_t3390940437 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayQuests::<OnQuestsCompleted>m__4F(GP_QuestResult)
extern "C"  void GooglePlayQuests_U3COnQuestsCompletedU3Em__4F_m43336258 (Il2CppObject * __this /* static, unused */, GP_QuestResult_t3390940437 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
