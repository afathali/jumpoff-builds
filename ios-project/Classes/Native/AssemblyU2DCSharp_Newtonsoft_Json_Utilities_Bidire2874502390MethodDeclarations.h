﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Bidire1212012318MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>::.ctor()
#define BidirectionalDictionary_2__ctor_m1251844367(__this, method) ((  void (*) (BidirectionalDictionary_2_t2874502390 *, const MethodInfo*))BidirectionalDictionary_2__ctor_m1119331919_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirst>,System.Collections.Generic.IEqualityComparer`1<TSecond>)
#define BidirectionalDictionary_2__ctor_m1708423537(__this, ___firstEqualityComparer0, ___secondEqualityComparer1, method) ((  void (*) (BidirectionalDictionary_2_t2874502390 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))BidirectionalDictionary_2__ctor_m1806502961_gshared)(__this, ___firstEqualityComparer0, ___secondEqualityComparer1, method)
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>::Add(TFirst,TSecond)
#define BidirectionalDictionary_2_Add_m830649796(__this, ___first0, ___second1, method) ((  void (*) (BidirectionalDictionary_2_t2874502390 *, String_t*, String_t*, const MethodInfo*))BidirectionalDictionary_2_Add_m190380356_gshared)(__this, ___first0, ___second1, method)
// System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>::TryGetByFirst(TFirst,TSecond&)
#define BidirectionalDictionary_2_TryGetByFirst_m3432118839(__this, ___first0, ___second1, method) ((  bool (*) (BidirectionalDictionary_2_t2874502390 *, String_t*, String_t**, const MethodInfo*))BidirectionalDictionary_2_TryGetByFirst_m1495460279_gshared)(__this, ___first0, ___second1, method)
// System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>::TryGetBySecond(TSecond,TFirst&)
#define BidirectionalDictionary_2_TryGetBySecond_m1434003049(__this, ___second0, ___first1, method) ((  bool (*) (BidirectionalDictionary_2_t2874502390 *, String_t*, String_t**, const MethodInfo*))BidirectionalDictionary_2_TryGetBySecond_m1230387241_gshared)(__this, ___second0, ___first1, method)
