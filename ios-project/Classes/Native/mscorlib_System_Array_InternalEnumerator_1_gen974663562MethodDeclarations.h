﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen974663562.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_115911300.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1812375236_gshared (InternalEnumerator_1_t974663562 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1812375236(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t974663562 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1812375236_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m878105092_gshared (InternalEnumerator_1_t974663562 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m878105092(__this, method) ((  void (*) (InternalEnumerator_1_t974663562 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m878105092_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2478951816_gshared (InternalEnumerator_1_t974663562 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2478951816(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t974663562 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2478951816_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2649412227_gshared (InternalEnumerator_1_t974663562 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2649412227(__this, method) ((  void (*) (InternalEnumerator_1_t974663562 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2649412227_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2289846156_gshared (InternalEnumerator_1_t974663562 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2289846156(__this, method) ((  bool (*) (InternalEnumerator_1_t974663562 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2289846156_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t115911300  InternalEnumerator_1_get_Current_m4138513283_gshared (InternalEnumerator_1_t974663562 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4138513283(__this, method) ((  KeyValuePair_2_t115911300  (*) (InternalEnumerator_1_t974663562 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4138513283_gshared)(__this, method)
