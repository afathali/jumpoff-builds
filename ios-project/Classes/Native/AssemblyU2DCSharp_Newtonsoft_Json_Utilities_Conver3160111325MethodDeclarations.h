﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey2D
struct U3CTryConvertOrCastU3Ec__AnonStorey2D_t3160111325;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey2D::.ctor()
extern "C"  void U3CTryConvertOrCastU3Ec__AnonStorey2D__ctor_m297450722 (U3CTryConvertOrCastU3Ec__AnonStorey2D_t3160111325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey2D::<>m__EF()
extern "C"  Il2CppObject * U3CTryConvertOrCastU3Ec__AnonStorey2D_U3CU3Em__EF_m4227204997 (U3CTryConvertOrCastU3Ec__AnonStorey2D_t3160111325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
