﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Object
struct Il2CppObject;
// SA_ScreenShotMaker_OLD
struct SA_ScreenShotMaker_OLD_t795198627;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA_ScreenShotMaker_OLD/<SaveScreenshot>c__IteratorB
struct  U3CSaveScreenshotU3Ec__IteratorB_t479799330  : public Il2CppObject
{
public:
	// System.Int32 SA_ScreenShotMaker_OLD/<SaveScreenshot>c__IteratorB::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 SA_ScreenShotMaker_OLD/<SaveScreenshot>c__IteratorB::<height>__1
	int32_t ___U3CheightU3E__1_1;
	// UnityEngine.Texture2D SA_ScreenShotMaker_OLD/<SaveScreenshot>c__IteratorB::<tex>__2
	Texture2D_t3542995729 * ___U3CtexU3E__2_2;
	// System.Int32 SA_ScreenShotMaker_OLD/<SaveScreenshot>c__IteratorB::$PC
	int32_t ___U24PC_3;
	// System.Object SA_ScreenShotMaker_OLD/<SaveScreenshot>c__IteratorB::$current
	Il2CppObject * ___U24current_4;
	// SA_ScreenShotMaker_OLD SA_ScreenShotMaker_OLD/<SaveScreenshot>c__IteratorB::<>f__this
	SA_ScreenShotMaker_OLD_t795198627 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__IteratorB_t479799330, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__1_1() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__IteratorB_t479799330, ___U3CheightU3E__1_1)); }
	inline int32_t get_U3CheightU3E__1_1() const { return ___U3CheightU3E__1_1; }
	inline int32_t* get_address_of_U3CheightU3E__1_1() { return &___U3CheightU3E__1_1; }
	inline void set_U3CheightU3E__1_1(int32_t value)
	{
		___U3CheightU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CtexU3E__2_2() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__IteratorB_t479799330, ___U3CtexU3E__2_2)); }
	inline Texture2D_t3542995729 * get_U3CtexU3E__2_2() const { return ___U3CtexU3E__2_2; }
	inline Texture2D_t3542995729 ** get_address_of_U3CtexU3E__2_2() { return &___U3CtexU3E__2_2; }
	inline void set_U3CtexU3E__2_2(Texture2D_t3542995729 * value)
	{
		___U3CtexU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtexU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__IteratorB_t479799330, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__IteratorB_t479799330, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CSaveScreenshotU3Ec__IteratorB_t479799330, ___U3CU3Ef__this_5)); }
	inline SA_ScreenShotMaker_OLD_t795198627 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline SA_ScreenShotMaker_OLD_t795198627 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(SA_ScreenShotMaker_OLD_t795198627 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
