﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISN_GameSaves
struct ISN_GameSaves_t2236256821;
// System.Action`1<GK_SaveRemoveResult>
struct Action_1_t341109949;
// System.Action`1<GK_SaveResult>
struct Action_1_t3748375835;
// System.Action`1<GK_FetchResult>
struct Action_1_t1413312038;
// System.Action`1<GK_SavesResolveResult>
struct Action_1_t3309854786;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// System.Collections.Generic.List`1<GK_SavedGame>
struct List_1_t2689214752;
// GK_SavedGame
struct GK_SavedGame_t3320093620;
// GK_SaveRemoveResult
struct GK_SaveRemoveResult_t539310567;
// GK_SaveResult
struct GK_SaveResult_t3946576453;
// GK_FetchResult
struct GK_FetchResult_t1611512656;
// GK_SavesResolveResult
struct GK_SavesResolveResult_t3508055404;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SavedGame3320093620.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SaveRemoveResult539310567.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SaveResult3946576453.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_FetchResult1611512656.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SavesResolveResul3508055404.h"

// System.Void ISN_GameSaves::.ctor()
extern "C"  void ISN_GameSaves__ctor_m3997679606 (ISN_GameSaves_t2236256821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::.cctor()
extern "C"  void ISN_GameSaves__cctor_m1973926485 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::add_ActionSaveRemoved(System.Action`1<GK_SaveRemoveResult>)
extern "C"  void ISN_GameSaves_add_ActionSaveRemoved_m1073781162 (Il2CppObject * __this /* static, unused */, Action_1_t341109949 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::remove_ActionSaveRemoved(System.Action`1<GK_SaveRemoveResult>)
extern "C"  void ISN_GameSaves_remove_ActionSaveRemoved_m609073433 (Il2CppObject * __this /* static, unused */, Action_1_t341109949 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::add_ActionGameSaved(System.Action`1<GK_SaveResult>)
extern "C"  void ISN_GameSaves_add_ActionGameSaved_m1684001984 (Il2CppObject * __this /* static, unused */, Action_1_t3748375835 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::remove_ActionGameSaved(System.Action`1<GK_SaveResult>)
extern "C"  void ISN_GameSaves_remove_ActionGameSaved_m209935363 (Il2CppObject * __this /* static, unused */, Action_1_t3748375835 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::add_ActionSavesFetched(System.Action`1<GK_FetchResult>)
extern "C"  void ISN_GameSaves_add_ActionSavesFetched_m1362846433 (Il2CppObject * __this /* static, unused */, Action_1_t1413312038 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::remove_ActionSavesFetched(System.Action`1<GK_FetchResult>)
extern "C"  void ISN_GameSaves_remove_ActionSavesFetched_m810521826 (Il2CppObject * __this /* static, unused */, Action_1_t1413312038 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::add_ActionSavesResolved(System.Action`1<GK_SavesResolveResult>)
extern "C"  void ISN_GameSaves_add_ActionSavesResolved_m4160979856 (Il2CppObject * __this /* static, unused */, Action_1_t3309854786 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::remove_ActionSavesResolved(System.Action`1<GK_SavesResolveResult>)
extern "C"  void ISN_GameSaves_remove_ActionSavesResolved_m270433981 (Il2CppObject * __this /* static, unused */, Action_1_t3309854786 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::Awake()
extern "C"  void ISN_GameSaves_Awake_m2637933959 (ISN_GameSaves_t2236256821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::SaveGame(System.Byte[],System.String)
extern "C"  void ISN_GameSaves_SaveGame_m2012345134 (ISN_GameSaves_t2236256821 * __this, ByteU5BU5D_t3397334013* ___data0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::FetchSavedGames()
extern "C"  void ISN_GameSaves_FetchSavedGames_m3856295940 (ISN_GameSaves_t2236256821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::DeleteSavedGame(System.String)
extern "C"  void ISN_GameSaves_DeleteSavedGame_m2470706820 (ISN_GameSaves_t2236256821 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::ResolveConflictingSavedGames(System.Collections.Generic.List`1<GK_SavedGame>,System.Byte[])
extern "C"  void ISN_GameSaves_ResolveConflictingSavedGames_m248593113 (ISN_GameSaves_t2236256821 * __this, List_1_t2689214752 * ___conflicts0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::LoadSaveData(GK_SavedGame)
extern "C"  void ISN_GameSaves_LoadSaveData_m1364880317 (ISN_GameSaves_t2236256821 * __this, GK_SavedGame_t3320093620 * ___save0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::OnSaveSuccess(System.String)
extern "C"  void ISN_GameSaves_OnSaveSuccess_m3797569599 (ISN_GameSaves_t2236256821 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::OnSaveFailed(System.String)
extern "C"  void ISN_GameSaves_OnSaveFailed_m3190308875 (ISN_GameSaves_t2236256821 * __this, String_t* ___erroData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::OnFetchSuccess(System.String)
extern "C"  void ISN_GameSaves_OnFetchSuccess_m669682984 (ISN_GameSaves_t2236256821 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::OnFetchFailed(System.String)
extern "C"  void ISN_GameSaves_OnFetchFailed_m3667567034 (ISN_GameSaves_t2236256821 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::OnResolveSuccess(System.String)
extern "C"  void ISN_GameSaves_OnResolveSuccess_m1227332312 (ISN_GameSaves_t2236256821 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::OnResolveFailed(System.String)
extern "C"  void ISN_GameSaves_OnResolveFailed_m2332066170 (ISN_GameSaves_t2236256821 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::OnDeleteSuccess(System.String)
extern "C"  void ISN_GameSaves_OnDeleteSuccess_m2016237291 (ISN_GameSaves_t2236256821 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::OnDeleteFailed(System.String)
extern "C"  void ISN_GameSaves_OnDeleteFailed_m2952612399 (ISN_GameSaves_t2236256821 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::OnSaveDataLoaded(System.String)
extern "C"  void ISN_GameSaves_OnSaveDataLoaded_m760458551 (ISN_GameSaves_t2236256821 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::OnSaveDataLoadFailed(System.String)
extern "C"  void ISN_GameSaves_OnSaveDataLoadFailed_m888535749 (ISN_GameSaves_t2236256821 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GK_SavedGame ISN_GameSaves::DeserializeGameSave(System.String)
extern "C"  GK_SavedGame_t3320093620 * ISN_GameSaves_DeserializeGameSave_m3753369287 (ISN_GameSaves_t2236256821 * __this, String_t* ___serializedData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::<ActionSaveRemoved>m__27(GK_SaveRemoveResult)
extern "C"  void ISN_GameSaves_U3CActionSaveRemovedU3Em__27_m4004688964 (Il2CppObject * __this /* static, unused */, GK_SaveRemoveResult_t539310567 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::<ActionGameSaved>m__28(GK_SaveResult)
extern "C"  void ISN_GameSaves_U3CActionGameSavedU3Em__28_m219810713 (Il2CppObject * __this /* static, unused */, GK_SaveResult_t3946576453 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::<ActionSavesFetched>m__29(GK_FetchResult)
extern "C"  void ISN_GameSaves_U3CActionSavesFetchedU3Em__29_m3189962317 (Il2CppObject * __this /* static, unused */, GK_FetchResult_t1611512656 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISN_GameSaves::<ActionSavesResolved>m__2A(GK_SavesResolveResult)
extern "C"  void ISN_GameSaves_U3CActionSavesResolvedU3Em__2A_m283309020 (Il2CppObject * __this /* static, unused */, GK_SavesResolveResult_t3508055404 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
