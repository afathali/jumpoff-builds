﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidNative
struct AndroidNative_t876365532;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AndroidNative::.ctor()
extern "C"  void AndroidNative__ctor_m841075585 (AndroidNative_t876365532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] AndroidNative::StringToArray(System.String)
extern "C"  StringU5BU5D_t1642385972* AndroidNative_StringToArray_m1250275219 (Il2CppObject * __this /* static, unused */, String_t* ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidNative::ArrayToString(System.String[])
extern "C"  String_t* AndroidNative_ArrayToString_m945220035 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::TwitterInit(System.String,System.String)
extern "C"  void AndroidNative_TwitterInit_m1483978648 (Il2CppObject * __this /* static, unused */, String_t* ___consumer_key0, String_t* ___consumer_secret1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::AuthificateUser()
extern "C"  void AndroidNative_AuthificateUser_m3313956809 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::LoadUserData()
extern "C"  void AndroidNative_LoadUserData_m2641024532 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::TwitterPost(System.String)
extern "C"  void AndroidNative_TwitterPost_m606361192 (Il2CppObject * __this /* static, unused */, String_t* ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::TwitterPostWithImage(System.String,System.String)
extern "C"  void AndroidNative_TwitterPostWithImage_m2516114833 (Il2CppObject * __this /* static, unused */, String_t* ___status0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::LogoutFromTwitter()
extern "C"  void AndroidNative_LogoutFromTwitter_m617838614 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::InitCameraAPI(System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void AndroidNative_InitCameraAPI_m1215263057 (Il2CppObject * __this /* static, unused */, String_t* ___folderName0, int32_t ___maxSize1, int32_t ___mode2, int32_t ___format3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::SaveToGalalry(System.String,System.String)
extern "C"  void AndroidNative_SaveToGalalry_m1055866647 (Il2CppObject * __this /* static, unused */, String_t* ___ImageData0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::GetImageFromGallery()
extern "C"  void AndroidNative_GetImageFromGallery_m2759772734 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::GetImageFromCamera(System.Boolean)
extern "C"  void AndroidNative_GetImageFromCamera_m413260340 (Il2CppObject * __this /* static, unused */, bool ___bSaveToGallery0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::isPackageInstalled(System.String)
extern "C"  void AndroidNative_isPackageInstalled_m3064153907 (Il2CppObject * __this /* static, unused */, String_t* ___packagename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::runPackage(System.String)
extern "C"  void AndroidNative_runPackage_m715458662 (Il2CppObject * __this /* static, unused */, String_t* ___packagename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::LoadAndroidId()
extern "C"  void AndroidNative_LoadAndroidId_m4262394957 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::LoadPackagesList()
extern "C"  void AndroidNative_LoadPackagesList_m1290592410 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::LoadNetworkInfo()
extern "C"  void AndroidNative_LoadNetworkInfo_m3445865915 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::OpenSettingsPage(System.String)
extern "C"  void AndroidNative_OpenSettingsPage_m214927041 (Il2CppObject * __this /* static, unused */, String_t* ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::LaunchApplication(System.String,System.String)
extern "C"  void AndroidNative_LaunchApplication_m3525685162 (Il2CppObject * __this /* static, unused */, String_t* ___bundle0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::LoadContacts()
extern "C"  void AndroidNative_LoadContacts_m852945570 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::LoadPackageInfo()
extern "C"  void AndroidNative_LoadPackageInfo_m2568487219 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::GetInternalStoragePath()
extern "C"  void AndroidNative_GetInternalStoragePath_m2056792606 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::GetExternalStoragePath()
extern "C"  void AndroidNative_GetExternalStoragePath_m3310871500 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::LoadLocaleInfo()
extern "C"  void AndroidNative_LoadLocaleInfo_m3073033981 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::StartLockTask()
extern "C"  void AndroidNative_StartLockTask_m4197794083 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::StopLockTask()
extern "C"  void AndroidNative_StopLockTask_m756377991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::OpenAppInStore(System.String)
extern "C"  void AndroidNative_OpenAppInStore_m3596191496 (Il2CppObject * __this /* static, unused */, String_t* ___appPackageName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::CallUtility(System.String,System.Object[])
extern "C"  void AndroidNative_CallUtility_m2402381849 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::CallAndroidNativeBridge(System.String,System.Object[])
extern "C"  void AndroidNative_CallAndroidNativeBridge_m466426902 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidNative::CallStatic(System.String,System.String,System.Object[])
extern "C"  void AndroidNative_CallStatic_m1587091547 (Il2CppObject * __this /* static, unused */, String_t* ___className0, String_t* ___methodName1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
