﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultContractResolver/<CreateISerializableContract>c__AnonStorey21
struct U3CCreateISerializableContractU3Ec__AnonStorey21_t55730933;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<CreateISerializableContract>c__AnonStorey21::.ctor()
extern "C"  void U3CCreateISerializableContractU3Ec__AnonStorey21__ctor_m1060051796 (U3CCreateISerializableContractU3Ec__AnonStorey21_t55730933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.DefaultContractResolver/<CreateISerializableContract>c__AnonStorey21::<>m__DC(System.Object[])
extern "C"  Il2CppObject * U3CCreateISerializableContractU3Ec__AnonStorey21_U3CU3Em__DC_m418294321 (U3CCreateISerializableContractU3Ec__AnonStorey21_t55730933 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
