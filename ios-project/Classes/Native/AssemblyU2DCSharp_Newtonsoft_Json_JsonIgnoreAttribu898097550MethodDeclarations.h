﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonIgnoreAttribute
struct JsonIgnoreAttribute_t898097550;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.JsonIgnoreAttribute::.ctor()
extern "C"  void JsonIgnoreAttribute__ctor_m3245478182 (JsonIgnoreAttribute_t898097550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
