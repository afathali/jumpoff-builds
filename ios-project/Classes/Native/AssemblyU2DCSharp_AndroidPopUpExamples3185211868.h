﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidPopUpExamples
struct  AndroidPopUpExamples_t3185211868  : public MonoBehaviour_t1158329972
{
public:
	// System.String AndroidPopUpExamples::rateText
	String_t* ___rateText_2;
	// System.String AndroidPopUpExamples::rateUrl
	String_t* ___rateUrl_3;

public:
	inline static int32_t get_offset_of_rateText_2() { return static_cast<int32_t>(offsetof(AndroidPopUpExamples_t3185211868, ___rateText_2)); }
	inline String_t* get_rateText_2() const { return ___rateText_2; }
	inline String_t** get_address_of_rateText_2() { return &___rateText_2; }
	inline void set_rateText_2(String_t* value)
	{
		___rateText_2 = value;
		Il2CppCodeGenWriteBarrier(&___rateText_2, value);
	}

	inline static int32_t get_offset_of_rateUrl_3() { return static_cast<int32_t>(offsetof(AndroidPopUpExamples_t3185211868, ___rateUrl_3)); }
	inline String_t* get_rateUrl_3() const { return ___rateUrl_3; }
	inline String_t** get_address_of_rateUrl_3() { return &___rateUrl_3; }
	inline void set_rateUrl_3(String_t* value)
	{
		___rateUrl_3 = value;
		Il2CppCodeGenWriteBarrier(&___rateUrl_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
