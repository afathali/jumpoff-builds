﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoogleCloudUseExample
struct GoogleCloudUseExample_t2949985613;
// GoogleCloudResult
struct GoogleCloudResult_t743628707;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GoogleCloudResult743628707.h"

// System.Void GoogleCloudUseExample::.ctor()
extern "C"  void GoogleCloudUseExample__ctor_m3072674506 (GoogleCloudUseExample_t2949985613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudUseExample::Awake()
extern "C"  void GoogleCloudUseExample_Awake_m3081584105 (GoogleCloudUseExample_t2949985613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudUseExample::FixedUpdate()
extern "C"  void GoogleCloudUseExample_FixedUpdate_m2569193123 (GoogleCloudUseExample_t2949985613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudUseExample::LoadAllStates()
extern "C"  void GoogleCloudUseExample_LoadAllStates_m3358908515 (GoogleCloudUseExample_t2949985613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudUseExample::LoadState()
extern "C"  void GoogleCloudUseExample_LoadState_m4277238825 (GoogleCloudUseExample_t2949985613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudUseExample::UpdateState()
extern "C"  void GoogleCloudUseExample_UpdateState_m975185956 (GoogleCloudUseExample_t2949985613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudUseExample::DeleteState()
extern "C"  void GoogleCloudUseExample_DeleteState_m2835094390 (GoogleCloudUseExample_t2949985613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudUseExample::OnStateConflict(GoogleCloudResult)
extern "C"  void GoogleCloudUseExample_OnStateConflict_m3018650689 (GoogleCloudUseExample_t2949985613 * __this, GoogleCloudResult_t743628707 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudUseExample::OnStateUpdated(GoogleCloudResult)
extern "C"  void GoogleCloudUseExample_OnStateUpdated_m2384616098 (GoogleCloudUseExample_t2949985613 * __this, GoogleCloudResult_t743628707 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudUseExample::OnAllLoaded(GoogleCloudResult)
extern "C"  void GoogleCloudUseExample_OnAllLoaded_m2539867436 (GoogleCloudUseExample_t2949985613 * __this, GoogleCloudResult_t743628707 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleCloudUseExample::OnStateDeleted(GoogleCloudResult)
extern "C"  void GoogleCloudUseExample_OnStateDeleted_m4294294576 (GoogleCloudUseExample_t2949985613 * __this, GoogleCloudResult_t743628707 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
