﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject
struct SimpleClassObject_t1988087395;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject::.ctor()
extern "C"  void SimpleClassObject__ctor_m1480126140 (SimpleClassObject_t1988087395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject::get_TextValue()
extern "C"  String_t* SimpleClassObject_get_TextValue_m123645878 (SimpleClassObject_t1988087395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject::set_TextValue(System.String)
extern "C"  void SimpleClassObject_set_TextValue_m1721099901 (SimpleClassObject_t1988087395 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject::get_NumberValue()
extern "C"  int32_t SimpleClassObject_get_NumberValue_m1213073187 (SimpleClassObject_t1988087395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject::set_NumberValue(System.Int32)
extern "C"  void SimpleClassObject_set_NumberValue_m3537866932 (SimpleClassObject_t1988087395 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject::get_VectorValue()
extern "C"  Vector3_t2243707580  SimpleClassObject_get_VectorValue_m4267998781 (SimpleClassObject_t1988087395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SimpleClassObject::set_VectorValue(UnityEngine.Vector3)
extern "C"  void SimpleClassObject_set_VectorValue_m1912363400 (SimpleClassObject_t1988087395 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
