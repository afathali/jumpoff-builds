﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaNodeCollection
struct JsonSchemaNodeCollection_t2800080063;
// System.String
struct String_t;
// Newtonsoft.Json.Schema.JsonSchemaNode
struct JsonSchemaNode_t3866831117;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3866831117.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaNodeCollection::.ctor()
extern "C"  void JsonSchemaNodeCollection__ctor_m452607832 (JsonSchemaNodeCollection_t2800080063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaNodeCollection::GetKeyForItem(Newtonsoft.Json.Schema.JsonSchemaNode)
extern "C"  String_t* JsonSchemaNodeCollection_GetKeyForItem_m2928127491 (JsonSchemaNodeCollection_t2800080063 * __this, JsonSchemaNode_t3866831117 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
