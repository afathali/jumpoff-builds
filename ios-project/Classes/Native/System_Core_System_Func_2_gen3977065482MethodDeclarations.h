﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2825504181MethodDeclarations.h"

// System.Void System.Func`2<System.Reflection.ICustomAttributeProvider,System.Runtime.Serialization.DataContractAttribute>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3768879590(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3977065482 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3134197304_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Reflection.ICustomAttributeProvider,System.Runtime.Serialization.DataContractAttribute>::Invoke(T)
#define Func_2_Invoke_m1909145580(__this, ___arg10, method) ((  DataContractAttribute_t3332255060 * (*) (Func_2_t3977065482 *, Il2CppObject *, const MethodInfo*))Func_2_Invoke_m815118996_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Reflection.ICustomAttributeProvider,System.Runtime.Serialization.DataContractAttribute>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m4133084759(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3977065482 *, Il2CppObject *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4034295761_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Reflection.ICustomAttributeProvider,System.Runtime.Serialization.DataContractAttribute>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3061753570(__this, ___result0, method) ((  DataContractAttribute_t3332255060 * (*) (Func_2_t3977065482 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1674435418_gshared)(__this, ___result0, method)
