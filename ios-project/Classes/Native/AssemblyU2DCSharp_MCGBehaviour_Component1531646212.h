﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.String
struct String_t;
// MCGBehaviour/Component/ComponentLifestyle
struct ComponentLifestyle_t4170640987;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_MCGBehaviour_LifestyleType936921768.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MCGBehaviour/Component
struct  Component_t1531646212  : public Il2CppObject
{
public:
	// System.Type MCGBehaviour/Component::<ForType>k__BackingField
	Type_t * ___U3CForTypeU3Ek__BackingField_0;
	// System.Type MCGBehaviour/Component::<ImplementedByType>k__BackingField
	Type_t * ___U3CImplementedByTypeU3Ek__BackingField_1;
	// System.String MCGBehaviour/Component::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_2;
	// MCGBehaviour/Component/ComponentLifestyle MCGBehaviour/Component::<Lifestyle>k__BackingField
	ComponentLifestyle_t4170640987 * ___U3CLifestyleU3Ek__BackingField_3;
	// MCGBehaviour/LifestyleType MCGBehaviour/Component::<LifestyleType>k__BackingField
	int32_t ___U3CLifestyleTypeU3Ek__BackingField_4;
	// System.Object MCGBehaviour/Component::<Instance>k__BackingField
	Il2CppObject * ___U3CInstanceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CForTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Component_t1531646212, ___U3CForTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CForTypeU3Ek__BackingField_0() const { return ___U3CForTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CForTypeU3Ek__BackingField_0() { return &___U3CForTypeU3Ek__BackingField_0; }
	inline void set_U3CForTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CForTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CForTypeU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CImplementedByTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Component_t1531646212, ___U3CImplementedByTypeU3Ek__BackingField_1)); }
	inline Type_t * get_U3CImplementedByTypeU3Ek__BackingField_1() const { return ___U3CImplementedByTypeU3Ek__BackingField_1; }
	inline Type_t ** get_address_of_U3CImplementedByTypeU3Ek__BackingField_1() { return &___U3CImplementedByTypeU3Ek__BackingField_1; }
	inline void set_U3CImplementedByTypeU3Ek__BackingField_1(Type_t * value)
	{
		___U3CImplementedByTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CImplementedByTypeU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Component_t1531646212, ___U3CNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CNameU3Ek__BackingField_2() const { return ___U3CNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_2() { return &___U3CNameU3Ek__BackingField_2; }
	inline void set_U3CNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CLifestyleU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Component_t1531646212, ___U3CLifestyleU3Ek__BackingField_3)); }
	inline ComponentLifestyle_t4170640987 * get_U3CLifestyleU3Ek__BackingField_3() const { return ___U3CLifestyleU3Ek__BackingField_3; }
	inline ComponentLifestyle_t4170640987 ** get_address_of_U3CLifestyleU3Ek__BackingField_3() { return &___U3CLifestyleU3Ek__BackingField_3; }
	inline void set_U3CLifestyleU3Ek__BackingField_3(ComponentLifestyle_t4170640987 * value)
	{
		___U3CLifestyleU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLifestyleU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CLifestyleTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Component_t1531646212, ___U3CLifestyleTypeU3Ek__BackingField_4)); }
	inline int32_t get_U3CLifestyleTypeU3Ek__BackingField_4() const { return ___U3CLifestyleTypeU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CLifestyleTypeU3Ek__BackingField_4() { return &___U3CLifestyleTypeU3Ek__BackingField_4; }
	inline void set_U3CLifestyleTypeU3Ek__BackingField_4(int32_t value)
	{
		___U3CLifestyleTypeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Component_t1531646212, ___U3CInstanceU3Ek__BackingField_5)); }
	inline Il2CppObject * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline Il2CppObject ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(Il2CppObject * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
