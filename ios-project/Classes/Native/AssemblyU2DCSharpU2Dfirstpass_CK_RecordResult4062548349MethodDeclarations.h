﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CK_RecordResult
struct CK_RecordResult_t4062548349;
// System.String
struct String_t;
// CK_Database
struct CK_Database_t243306482;
// CK_Record
struct CK_Record_t3973541762;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CK_Database243306482.h"

// System.Void CK_RecordResult::.ctor(System.Int32)
extern "C"  void CK_RecordResult__ctor_m856170895 (CK_RecordResult_t4062548349 * __this, int32_t ___recordId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_RecordResult::.ctor(System.String)
extern "C"  void CK_RecordResult__ctor_m173708914 (CK_RecordResult_t4062548349 * __this, String_t* ___errorData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CK_RecordResult::SetDatabase(CK_Database)
extern "C"  void CK_RecordResult_SetDatabase_m1986087279 (CK_RecordResult_t4062548349 * __this, CK_Database_t243306482 * ___database0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CK_Record CK_RecordResult::get_Record()
extern "C"  CK_Record_t3973541762 * CK_RecordResult_get_Record_m3124080979 (CK_RecordResult_t4062548349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CK_Database CK_RecordResult::get_Database()
extern "C"  CK_Database_t243306482 * CK_RecordResult_get_Database_m1446361587 (CK_RecordResult_t4062548349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
