﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SPFacebookAnalytics::ActivateApp()
extern "C"  void SPFacebookAnalytics_ActivateApp_m912142804 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::AchievedLevel(System.Int32)
extern "C"  void SPFacebookAnalytics_AchievedLevel_m2271487752 (Il2CppObject * __this /* static, unused */, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::AddedPaymentInfo(System.Boolean)
extern "C"  void SPFacebookAnalytics_AddedPaymentInfo_m2198302737 (Il2CppObject * __this /* static, unused */, bool ___IsPaymentInfoAvailable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::AddedToCart(System.Single,System.String,System.String,System.String)
extern "C"  void SPFacebookAnalytics_AddedToCart_m2715313570 (Il2CppObject * __this /* static, unused */, float ___price0, String_t* ___id1, String_t* ___type2, String_t* ___currency3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::AddedToWishlist(System.Single,System.String,System.String,System.String)
extern "C"  void SPFacebookAnalytics_AddedToWishlist_m325061849 (Il2CppObject * __this /* static, unused */, float ___price0, String_t* ___id1, String_t* ___type2, String_t* ___currency3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::CompletedRegistration(System.String)
extern "C"  void SPFacebookAnalytics_CompletedRegistration_m3476369314 (Il2CppObject * __this /* static, unused */, String_t* ___RegistrationMethod0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::CompletedTutorial(System.Boolean,System.String)
extern "C"  void SPFacebookAnalytics_CompletedTutorial_m1281321322 (Il2CppObject * __this /* static, unused */, bool ___IsIsSuccsessed0, String_t* ___ContentId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::InitiatedCheckout(System.Single,System.Int32,System.String,System.String,System.Boolean,System.String)
extern "C"  void SPFacebookAnalytics_InitiatedCheckout_m3866739380 (Il2CppObject * __this /* static, unused */, float ___price0, int32_t ___itemsCount1, String_t* ___ContentType2, String_t* ___ContentId3, bool ___IsPaymentInfoAvailable4, String_t* ___Currency5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::Purchased(System.Single,System.Int32,System.String,System.String,System.String)
extern "C"  void SPFacebookAnalytics_Purchased_m3417432715 (Il2CppObject * __this /* static, unused */, float ___price0, int32_t ___itemsCount1, String_t* ___ContentType2, String_t* ___ContentId3, String_t* ___Currency4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::Rated(System.Int32,System.String,System.String,System.Int32)
extern "C"  void SPFacebookAnalytics_Rated_m787934614 (Il2CppObject * __this /* static, unused */, int32_t ___Rating0, String_t* ___ContentType1, String_t* ___ContentId2, int32_t ___MaxRating3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::Searched(System.String,System.String,System.Boolean)
extern "C"  void SPFacebookAnalytics_Searched_m3743077142 (Il2CppObject * __this /* static, unused */, String_t* ___ContentType0, String_t* ___SearchString1, bool ___IsIsSuccsessed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::SpentCredits(System.Single,System.String,System.String)
extern "C"  void SPFacebookAnalytics_SpentCredits_m3244547549 (Il2CppObject * __this /* static, unused */, float ___credit0, String_t* ___ContentType1, String_t* ___ContentId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::UnlockedAchievement(System.String)
extern "C"  void SPFacebookAnalytics_UnlockedAchievement_m698943534 (Il2CppObject * __this /* static, unused */, String_t* ___Describtion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::LogEvent()
extern "C"  void SPFacebookAnalytics_LogEvent_m2108360692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SPFacebookAnalytics::ViewedContent(System.Single,System.String,System.String,System.String)
extern "C"  void SPFacebookAnalytics_ViewedContent_m3329837314 (Il2CppObject * __this /* static, unused */, float ___price0, String_t* ___ContentType1, String_t* ___ContentId2, String_t* ___Currency3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
