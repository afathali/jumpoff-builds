﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CanvasOverlay
struct CanvasOverlay_t3181174292;

#include "codegen/il2cpp-codegen.h"

// System.Void CanvasOverlay::.ctor()
extern "C"  void CanvasOverlay__ctor_m3763657737 (CanvasOverlay_t3181174292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CanvasOverlay::Start()
extern "C"  void CanvasOverlay_Start_m3508609369 (CanvasOverlay_t3181174292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CanvasOverlay::Update()
extern "C"  void CanvasOverlay_Update_m2372827838 (CanvasOverlay_t3181174292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
