﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_ActivityTemplate
struct AN_ActivityTemplate_t3380616875;
// System.String
struct String_t;
// AN_PropertyTemplate
struct AN_PropertyTemplate_t2393149441;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// System.Xml.XmlElement
struct XmlElement_t2877111883;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AN_PropertyTemplate2393149441.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void AN_ActivityTemplate::.ctor(System.Boolean,System.String)
extern "C"  void AN_ActivityTemplate__ctor_m2938538671 (AN_ActivityTemplate_t3380616875 * __this, bool ___isLauncher0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ActivityTemplate::SetName(System.String)
extern "C"  void AN_ActivityTemplate_SetName_m2531693541 (AN_ActivityTemplate_t3380616875 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ActivityTemplate::SetAsLauncher(System.Boolean)
extern "C"  void AN_ActivityTemplate_SetAsLauncher_m3604251579 (AN_ActivityTemplate_t3380616875 * __this, bool ___isLauncher0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AN_PropertyTemplate AN_ActivityTemplate::GetLauncherPropertyTemplate()
extern "C"  AN_PropertyTemplate_t2393149441 * AN_ActivityTemplate_GetLauncherPropertyTemplate_m1569030365 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AN_ActivityTemplate::IsLauncherProperty(AN_PropertyTemplate)
extern "C"  bool AN_ActivityTemplate_IsLauncherProperty_m2398768798 (AN_ActivityTemplate_t3380616875 * __this, AN_PropertyTemplate_t2393149441 * ___property0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ActivityTemplate::ToXmlElement(System.Xml.XmlDocument,System.Xml.XmlElement)
extern "C"  void AN_ActivityTemplate_ToXmlElement_m147071123 (AN_ActivityTemplate_t3380616875 * __this, XmlDocument_t3649534162 * ___doc0, XmlElement_t2877111883 * ___parent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AN_ActivityTemplate::get_IsLauncher()
extern "C"  bool AN_ActivityTemplate_get_IsLauncher_m1303512743 (AN_ActivityTemplate_t3380616875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AN_ActivityTemplate::get_Name()
extern "C"  String_t* AN_ActivityTemplate_get_Name_m3126956021 (AN_ActivityTemplate_t3380616875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AN_ActivityTemplate::get_Id()
extern "C"  int32_t AN_ActivityTemplate_get_Id_m848270814 (AN_ActivityTemplate_t3380616875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
