﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jump3D
struct  Jump3D_t438928399  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Jump3D::rotCanvas
	GameObject_t1756533147 * ___rotCanvas_2;

public:
	inline static int32_t get_offset_of_rotCanvas_2() { return static_cast<int32_t>(offsetof(Jump3D_t438928399, ___rotCanvas_2)); }
	inline GameObject_t1756533147 * get_rotCanvas_2() const { return ___rotCanvas_2; }
	inline GameObject_t1756533147 ** get_address_of_rotCanvas_2() { return &___rotCanvas_2; }
	inline void set_rotCanvas_2(GameObject_t1756533147 * value)
	{
		___rotCanvas_2 = value;
		Il2CppCodeGenWriteBarrier(&___rotCanvas_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
