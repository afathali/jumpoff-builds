﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Int32>
struct Node_t1311643904;
// System.Collections.Generic.RBTree/Node
struct Node_t2499136326;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_RBTree_Node2499136326.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Int32>::.ctor(TKey)
extern "C"  void Node__ctor_m528884334_gshared (Node_t1311643904 * __this, int32_t ___key0, const MethodInfo* method);
#define Node__ctor_m528884334(__this, ___key0, method) ((  void (*) (Node_t1311643904 *, int32_t, const MethodInfo*))Node__ctor_m528884334_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Int32>::.ctor(TKey,TValue)
extern "C"  void Node__ctor_m2089501525_gshared (Node_t1311643904 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Node__ctor_m2089501525(__this, ___key0, ___value1, method) ((  void (*) (Node_t1311643904 *, int32_t, int32_t, const MethodInfo*))Node__ctor_m2089501525_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Int32>::SwapValue(System.Collections.Generic.RBTree/Node)
extern "C"  void Node_SwapValue_m975546121_gshared (Node_t1311643904 * __this, Node_t2499136326 * ___other0, const MethodInfo* method);
#define Node_SwapValue_m975546121(__this, ___other0, method) ((  void (*) (Node_t1311643904 *, Node_t2499136326 *, const MethodInfo*))Node_SwapValue_m975546121_gshared)(__this, ___other0, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Int32>::AsKV()
extern "C"  KeyValuePair_2_t3132015601  Node_AsKV_m3269694326_gshared (Node_t1311643904 * __this, const MethodInfo* method);
#define Node_AsKV_m3269694326(__this, method) ((  KeyValuePair_2_t3132015601  (*) (Node_t1311643904 *, const MethodInfo*))Node_AsKV_m3269694326_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Int32>::AsDE()
extern "C"  DictionaryEntry_t3048875398  Node_AsDE_m4169323149_gshared (Node_t1311643904 * __this, const MethodInfo* method);
#define Node_AsDE_m4169323149(__this, method) ((  DictionaryEntry_t3048875398  (*) (Node_t1311643904 *, const MethodInfo*))Node_AsDE_m4169323149_gshared)(__this, method)
