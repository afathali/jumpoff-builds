﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNodeListChildren_Enumerato569056069.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_XmlNotation206561061.h"
#include "System_Xml_System_Xml_XmlParserContext2728039553.h"
#include "System_Xml_System_Xml_XmlParserContext_ContextItem1262420678.h"
#include "System_Xml_System_Xml_XmlParserInput2366782760.h"
#include "System_Xml_System_Xml_XmlParserInput_XmlParserInputS25800784.h"
#include "System_Xml_System_Xml_XmlProcessingInstruction431557540.h"
#include "System_Xml_System_Xml_XmlQualifiedName1944712516.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport1548133672.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_Comma1644897369.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_CharG1955031820.h"
#include "System_Xml_System_Xml_XmlReaderSettings1578612233.h"
#include "System_Xml_System_Xml_XmlResolver2024571559.h"
#include "System_Xml_System_Xml_XmlSignificantWhitespace1224054391.h"
#include "System_Xml_System_Xml_XmlSpace2880376877.h"
#include "System_Xml_System_Xml_XmlText4111601336.h"
#include "System_Xml_Mono_Xml2_XmlTextReader511376973.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlTokenInfo254587324.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlAttributeTok3353594030.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2340974457.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputState3313602765.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputStateSt3023928423.h"
#include "System_Xml_System_Xml_XmlTextReader3514170725.h"
#include "System_Xml_System_Xml_XmlTextWriter2527250655.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlNodeInfo3709371029.h"
#include "System_Xml_System_Xml_XmlTextWriter_StringUtil2068578019.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlDeclState3530111136.h"
#include "System_Xml_System_Xml_XmlTokenizedType1619571710.h"
#include "System_Xml_System_Xml_XmlUrlResolver896669594.h"
#include "System_Xml_System_Xml_XmlWhitespace2557770518.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventHandler2964483403.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3672778802.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1957337327.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2038352954.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar628910058.h"
#include "UnityScript_Lang_U3CModuleU3E3783534214.h"
#include "UnityScript_Lang_UnityScript_Lang_Array1396575355.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_ISD_Set1116242554.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_PlistVa3782075076.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_Framewo4022948262.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_Lib360384209.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_Variabl1157765046.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SA_IOSDeploy_Variabl1912381035.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_SoomlaGrow2919772303.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_SoomlaAction4294445400.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_SoomlaEvent319858442.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_SoomlaProvider2510794499.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSNativeSettings547170227.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GameCenterInvitation2643374653.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GameCenterManager1487113918.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GameCenterManager_U3C165266512.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GameCenter_RTM849630631.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GameCenter_TBM3457554475.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_CacheManager1215851066.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_GameSaves2236256821.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SavedGame3320093620.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_FetchResult1611512656.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SaveDataLoaded3684688319.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SaveRemoveResult539310567.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SaveResult3946576453.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SavesResolveResul3508055404.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_CollectionType3353981271.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_InviteRecipientRe3438857802.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_MatchType1493351924.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_PhotoSize3124681388.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TimeSpan1050271570.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_FriendRequest4101620808.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_AchievementProgre3539574352.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_LeaderboardResult866080833.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_PlayerSignatureResu13769479.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_UserInfoLoadResul1177841233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_UserPhotoLoadResu1614198031.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ISN_LoadSetLeaderboa3997789804.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_AchievementTempla2296152240.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Invite22070530.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_LeaderBoardInfo3670215494.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Leaderboard156446466.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_LeaderboardSet5314098.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_LocalPlayerScoreU1070875322.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Player2782008294.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Score1529008873.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_ScoreCollection4092547735.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_MatchSendDataMode3659269993.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_PlayerConnectionS2434478783.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_RTM_MatchStartedRe833698690.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_RTM_QueryActivity2276098399.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_RTM_Match873568990.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TurnBasedMatchOut2242380984.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TurnBasedMatchSta2382635170.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TurnBasedParticip2126479626.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_EndTrunResult1517380690.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_LoadMatchResu1639249273.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_LoadMatchesRes370491735.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_TBM_MatchDataUpda1356006034.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (Enumerator_t569056069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[3] = 
{
	Enumerator_t569056069::get_offset_of_parent_0(),
	Enumerator_t569056069::get_offset_of_currentChild_1(),
	Enumerator_t569056069::get_offset_of_passedLastNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (XmlNodeType_t739504597)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2201[19] = 
{
	XmlNodeType_t739504597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (XmlNotation_t206561061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[4] = 
{
	XmlNotation_t206561061::get_offset_of_localName_6(),
	XmlNotation_t206561061::get_offset_of_publicId_7(),
	XmlNotation_t206561061::get_offset_of_systemId_8(),
	XmlNotation_t206561061::get_offset_of_prefix_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (XmlParserContext_t2728039553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[13] = 
{
	XmlParserContext_t2728039553::get_offset_of_baseURI_0(),
	XmlParserContext_t2728039553::get_offset_of_docTypeName_1(),
	XmlParserContext_t2728039553::get_offset_of_encoding_2(),
	XmlParserContext_t2728039553::get_offset_of_internalSubset_3(),
	XmlParserContext_t2728039553::get_offset_of_namespaceManager_4(),
	XmlParserContext_t2728039553::get_offset_of_nameTable_5(),
	XmlParserContext_t2728039553::get_offset_of_publicID_6(),
	XmlParserContext_t2728039553::get_offset_of_systemID_7(),
	XmlParserContext_t2728039553::get_offset_of_xmlLang_8(),
	XmlParserContext_t2728039553::get_offset_of_xmlSpace_9(),
	XmlParserContext_t2728039553::get_offset_of_contextItems_10(),
	XmlParserContext_t2728039553::get_offset_of_contextItemCount_11(),
	XmlParserContext_t2728039553::get_offset_of_dtd_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (ContextItem_t1262420678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[3] = 
{
	ContextItem_t1262420678::get_offset_of_BaseURI_0(),
	ContextItem_t1262420678::get_offset_of_XmlLang_1(),
	ContextItem_t1262420678::get_offset_of_XmlSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (XmlParserInput_t2366782760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[5] = 
{
	XmlParserInput_t2366782760::get_offset_of_sourceStack_0(),
	XmlParserInput_t2366782760::get_offset_of_source_1(),
	XmlParserInput_t2366782760::get_offset_of_has_peek_2(),
	XmlParserInput_t2366782760::get_offset_of_peek_char_3(),
	XmlParserInput_t2366782760::get_offset_of_allowTextDecl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (XmlParserInputSource_t25800784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[6] = 
{
	XmlParserInputSource_t25800784::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t25800784::get_offset_of_reader_1(),
	XmlParserInputSource_t25800784::get_offset_of_state_2(),
	XmlParserInputSource_t25800784::get_offset_of_isPE_3(),
	XmlParserInputSource_t25800784::get_offset_of_line_4(),
	XmlParserInputSource_t25800784::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (XmlProcessingInstruction_t431557540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[2] = 
{
	XmlProcessingInstruction_t431557540::get_offset_of_target_7(),
	XmlProcessingInstruction_t431557540::get_offset_of_data_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (XmlQualifiedName_t1944712516), -1, sizeof(XmlQualifiedName_t1944712516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2208[4] = 
{
	XmlQualifiedName_t1944712516_StaticFields::get_offset_of_Empty_0(),
	XmlQualifiedName_t1944712516::get_offset_of_name_1(),
	XmlQualifiedName_t1944712516::get_offset_of_ns_2(),
	XmlQualifiedName_t1944712516::get_offset_of_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (XmlReader_t3675626668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[2] = 
{
	XmlReader_t3675626668::get_offset_of_binary_0(),
	XmlReader_t3675626668::get_offset_of_settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (XmlReaderBinarySupport_t1548133672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[5] = 
{
	XmlReaderBinarySupport_t1548133672::get_offset_of_reader_0(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_base64CacheStartsAt_1(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_state_2(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_hasCache_3(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_dontReset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (CommandState_t1644897369)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2211[6] = 
{
	CommandState_t1644897369::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (CharGetter_t1955031820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (XmlReaderSettings_t1578612233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[2] = 
{
	XmlReaderSettings_t1578612233::get_offset_of_checkCharacters_0(),
	XmlReaderSettings_t1578612233::get_offset_of_conformance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (XmlResolver_t2024571559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (XmlSignificantWhitespace_t1224054391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (XmlSpace_t2880376877)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2216[4] = 
{
	XmlSpace_t2880376877::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (XmlText_t4111601336), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (XmlTextReader_t511376973), -1, sizeof(XmlTextReader_t511376973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2218[54] = 
{
	XmlTextReader_t511376973::get_offset_of_cursorToken_2(),
	XmlTextReader_t511376973::get_offset_of_currentToken_3(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeToken_4(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeValueToken_5(),
	XmlTextReader_t511376973::get_offset_of_attributeTokens_6(),
	XmlTextReader_t511376973::get_offset_of_attributeValueTokens_7(),
	XmlTextReader_t511376973::get_offset_of_currentAttribute_8(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeValue_9(),
	XmlTextReader_t511376973::get_offset_of_attributeCount_10(),
	XmlTextReader_t511376973::get_offset_of_parserContext_11(),
	XmlTextReader_t511376973::get_offset_of_nameTable_12(),
	XmlTextReader_t511376973::get_offset_of_nsmgr_13(),
	XmlTextReader_t511376973::get_offset_of_readState_14(),
	XmlTextReader_t511376973::get_offset_of_disallowReset_15(),
	XmlTextReader_t511376973::get_offset_of_depth_16(),
	XmlTextReader_t511376973::get_offset_of_elementDepth_17(),
	XmlTextReader_t511376973::get_offset_of_depthUp_18(),
	XmlTextReader_t511376973::get_offset_of_popScope_19(),
	XmlTextReader_t511376973::get_offset_of_elementNames_20(),
	XmlTextReader_t511376973::get_offset_of_elementNameStackPos_21(),
	XmlTextReader_t511376973::get_offset_of_allowMultipleRoot_22(),
	XmlTextReader_t511376973::get_offset_of_isStandalone_23(),
	XmlTextReader_t511376973::get_offset_of_returnEntityReference_24(),
	XmlTextReader_t511376973::get_offset_of_entityReferenceName_25(),
	XmlTextReader_t511376973::get_offset_of_valueBuffer_26(),
	XmlTextReader_t511376973::get_offset_of_reader_27(),
	XmlTextReader_t511376973::get_offset_of_peekChars_28(),
	XmlTextReader_t511376973::get_offset_of_peekCharsIndex_29(),
	XmlTextReader_t511376973::get_offset_of_peekCharsLength_30(),
	XmlTextReader_t511376973::get_offset_of_curNodePeekIndex_31(),
	XmlTextReader_t511376973::get_offset_of_preserveCurrentTag_32(),
	XmlTextReader_t511376973::get_offset_of_line_33(),
	XmlTextReader_t511376973::get_offset_of_column_34(),
	XmlTextReader_t511376973::get_offset_of_currentLinkedNodeLineNumber_35(),
	XmlTextReader_t511376973::get_offset_of_currentLinkedNodeLinePosition_36(),
	XmlTextReader_t511376973::get_offset_of_useProceedingLineInfo_37(),
	XmlTextReader_t511376973::get_offset_of_startNodeType_38(),
	XmlTextReader_t511376973::get_offset_of_currentState_39(),
	XmlTextReader_t511376973::get_offset_of_nestLevel_40(),
	XmlTextReader_t511376973::get_offset_of_readCharsInProgress_41(),
	XmlTextReader_t511376973::get_offset_of_binaryCharGetter_42(),
	XmlTextReader_t511376973::get_offset_of_namespaces_43(),
	XmlTextReader_t511376973::get_offset_of_whitespaceHandling_44(),
	XmlTextReader_t511376973::get_offset_of_resolver_45(),
	XmlTextReader_t511376973::get_offset_of_normalization_46(),
	XmlTextReader_t511376973::get_offset_of_checkCharacters_47(),
	XmlTextReader_t511376973::get_offset_of_prohibitDtd_48(),
	XmlTextReader_t511376973::get_offset_of_closeInput_49(),
	XmlTextReader_t511376973::get_offset_of_entityHandling_50(),
	XmlTextReader_t511376973::get_offset_of_whitespacePool_51(),
	XmlTextReader_t511376973::get_offset_of_whitespaceCache_52(),
	XmlTextReader_t511376973::get_offset_of_stateStack_53(),
	XmlTextReader_t511376973_StaticFields::get_offset_of_U3CU3Ef__switchU24map51_54(),
	XmlTextReader_t511376973_StaticFields::get_offset_of_U3CU3Ef__switchU24map52_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (XmlTokenInfo_t254587324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[13] = 
{
	XmlTokenInfo_t254587324::get_offset_of_valueCache_0(),
	XmlTokenInfo_t254587324::get_offset_of_Reader_1(),
	XmlTokenInfo_t254587324::get_offset_of_Name_2(),
	XmlTokenInfo_t254587324::get_offset_of_LocalName_3(),
	XmlTokenInfo_t254587324::get_offset_of_Prefix_4(),
	XmlTokenInfo_t254587324::get_offset_of_NamespaceURI_5(),
	XmlTokenInfo_t254587324::get_offset_of_IsEmptyElement_6(),
	XmlTokenInfo_t254587324::get_offset_of_QuoteChar_7(),
	XmlTokenInfo_t254587324::get_offset_of_LineNumber_8(),
	XmlTokenInfo_t254587324::get_offset_of_LinePosition_9(),
	XmlTokenInfo_t254587324::get_offset_of_ValueBufferStart_10(),
	XmlTokenInfo_t254587324::get_offset_of_ValueBufferEnd_11(),
	XmlTokenInfo_t254587324::get_offset_of_NodeType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (XmlAttributeTokenInfo_t3353594030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[4] = 
{
	XmlAttributeTokenInfo_t3353594030::get_offset_of_ValueTokenStartIndex_13(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_ValueTokenEndIndex_14(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_valueCache_15(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_tmpBuilder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (TagName_t2340974457)+ sizeof (Il2CppObject), sizeof(TagName_t2340974457_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2221[3] = 
{
	TagName_t2340974457::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2340974457::get_offset_of_LocalName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2340974457::get_offset_of_Prefix_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (DtdInputState_t3313602765)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2222[10] = 
{
	DtdInputState_t3313602765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (DtdInputStateStack_t3023928423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[1] = 
{
	DtdInputStateStack_t3023928423::get_offset_of_intern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (XmlTextReader_t3514170725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[5] = 
{
	XmlTextReader_t3514170725::get_offset_of_entity_2(),
	XmlTextReader_t3514170725::get_offset_of_source_3(),
	XmlTextReader_t3514170725::get_offset_of_entityInsideAttribute_4(),
	XmlTextReader_t3514170725::get_offset_of_insideAttribute_5(),
	XmlTextReader_t3514170725::get_offset_of_entityNameStack_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (XmlTextWriter_t2527250655), -1, sizeof(XmlTextWriter_t2527250655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2225[35] = 
{
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_unmarked_utf8encoding_0(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_text_chars_1(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_attr_chars_2(),
	XmlTextWriter_t2527250655::get_offset_of_base_stream_3(),
	XmlTextWriter_t2527250655::get_offset_of_source_4(),
	XmlTextWriter_t2527250655::get_offset_of_writer_5(),
	XmlTextWriter_t2527250655::get_offset_of_preserver_6(),
	XmlTextWriter_t2527250655::get_offset_of_preserved_name_7(),
	XmlTextWriter_t2527250655::get_offset_of_is_preserved_xmlns_8(),
	XmlTextWriter_t2527250655::get_offset_of_allow_doc_fragment_9(),
	XmlTextWriter_t2527250655::get_offset_of_close_output_stream_10(),
	XmlTextWriter_t2527250655::get_offset_of_ignore_encoding_11(),
	XmlTextWriter_t2527250655::get_offset_of_namespaces_12(),
	XmlTextWriter_t2527250655::get_offset_of_xmldecl_state_13(),
	XmlTextWriter_t2527250655::get_offset_of_check_character_validity_14(),
	XmlTextWriter_t2527250655::get_offset_of_newline_handling_15(),
	XmlTextWriter_t2527250655::get_offset_of_is_document_entity_16(),
	XmlTextWriter_t2527250655::get_offset_of_state_17(),
	XmlTextWriter_t2527250655::get_offset_of_node_state_18(),
	XmlTextWriter_t2527250655::get_offset_of_nsmanager_19(),
	XmlTextWriter_t2527250655::get_offset_of_open_count_20(),
	XmlTextWriter_t2527250655::get_offset_of_elements_21(),
	XmlTextWriter_t2527250655::get_offset_of_new_local_namespaces_22(),
	XmlTextWriter_t2527250655::get_offset_of_explicit_nsdecls_23(),
	XmlTextWriter_t2527250655::get_offset_of_namespace_handling_24(),
	XmlTextWriter_t2527250655::get_offset_of_indent_25(),
	XmlTextWriter_t2527250655::get_offset_of_indent_count_26(),
	XmlTextWriter_t2527250655::get_offset_of_indent_char_27(),
	XmlTextWriter_t2527250655::get_offset_of_indent_string_28(),
	XmlTextWriter_t2527250655::get_offset_of_newline_29(),
	XmlTextWriter_t2527250655::get_offset_of_indent_attributes_30(),
	XmlTextWriter_t2527250655::get_offset_of_quote_char_31(),
	XmlTextWriter_t2527250655::get_offset_of_v2_32(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map53_33(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map54_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (XmlNodeInfo_t3709371029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[7] = 
{
	XmlNodeInfo_t3709371029::get_offset_of_Prefix_0(),
	XmlNodeInfo_t3709371029::get_offset_of_LocalName_1(),
	XmlNodeInfo_t3709371029::get_offset_of_NS_2(),
	XmlNodeInfo_t3709371029::get_offset_of_HasSimple_3(),
	XmlNodeInfo_t3709371029::get_offset_of_HasElements_4(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlLang_5(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (StringUtil_t2068578019), -1, sizeof(StringUtil_t2068578019_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2227[2] = 
{
	StringUtil_t2068578019_StaticFields::get_offset_of_cul_0(),
	StringUtil_t2068578019_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (XmlDeclState_t3530111136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2228[5] = 
{
	XmlDeclState_t3530111136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (XmlTokenizedType_t1619571710)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2229[14] = 
{
	XmlTokenizedType_t1619571710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (XmlUrlResolver_t896669594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[1] = 
{
	XmlUrlResolver_t896669594::get_offset_of_credential_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (XmlWhitespace_t2557770518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (XmlWriter_t1048088568), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (XmlNodeChangedEventHandler_t2964483403), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2234[7] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24U24fieldU2D23_0(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24U24fieldU2D26_1(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24U24fieldU2D27_2(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24U24fieldU2D28_3(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24U24fieldU2D29_4(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24U24fieldU2D43_5(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24U24fieldU2D44_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (U24ArrayTypeU2412_t3672778808)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778808_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (U24ArrayTypeU248_t1957337328)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t1957337328_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (U24ArrayTypeU24256_t2038352957)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352957_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (U24ArrayTypeU241280_t628910058)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241280_t628910058_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (U3CModuleU3E_t3783534229), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (Array_t1396575355), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (U3CModuleU3E_t3783534230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (U3CModuleU3E_t3783534231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (ISD_Settings_t1116242554), -1, sizeof(ISD_Settings_t1116242554_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2243[16] = 
{
	0,
	0,
	0,
	ISD_Settings_t1116242554::get_offset_of_IsfwSettingOpen_5(),
	ISD_Settings_t1116242554::get_offset_of_IsLibSettingOpen_6(),
	ISD_Settings_t1116242554::get_offset_of_IslinkerSettingOpne_7(),
	ISD_Settings_t1116242554::get_offset_of_IscompilerSettingsOpen_8(),
	ISD_Settings_t1116242554::get_offset_of_IsPlistSettingsOpen_9(),
	ISD_Settings_t1116242554::get_offset_of_IsLanguageSettingOpen_10(),
	ISD_Settings_t1116242554::get_offset_of_Frameworks_11(),
	ISD_Settings_t1116242554::get_offset_of_Libraries_12(),
	ISD_Settings_t1116242554::get_offset_of_compileFlags_13(),
	ISD_Settings_t1116242554::get_offset_of_linkFlags_14(),
	ISD_Settings_t1116242554::get_offset_of_PlistVariables_15(),
	ISD_Settings_t1116242554::get_offset_of_langFolders_16(),
	ISD_Settings_t1116242554_StaticFields::get_offset_of_instance_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (PlistValueTypes_t3782075076)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2244[7] = 
{
	PlistValueTypes_t3782075076::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (Framework_t4022948262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[3] = 
{
	Framework_t4022948262::get_offset_of_IsOpen_0(),
	Framework_t4022948262::get_offset_of_Name_1(),
	Framework_t4022948262::get_offset_of_IsOptional_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (Lib_t360384209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[3] = 
{
	Lib_t360384209::get_offset_of_IsOpen_0(),
	Lib_t360384209::get_offset_of_Name_1(),
	Lib_t360384209::get_offset_of_IsOptional_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (Variable_t1157765046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[11] = 
{
	Variable_t1157765046::get_offset_of_IsOpen_0(),
	Variable_t1157765046::get_offset_of_IsListOpen_1(),
	Variable_t1157765046::get_offset_of_Name_2(),
	Variable_t1157765046::get_offset_of_Type_3(),
	Variable_t1157765046::get_offset_of_ArrayType_4(),
	Variable_t1157765046::get_offset_of_StringValue_5(),
	Variable_t1157765046::get_offset_of_IntegerValue_6(),
	Variable_t1157765046::get_offset_of_FloatValue_7(),
	Variable_t1157765046::get_offset_of_BooleanValue_8(),
	Variable_t1157765046::get_offset_of_ArrayValue_9(),
	Variable_t1157765046::get_offset_of_DictValues_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (VariableListed_t1912381035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[7] = 
{
	VariableListed_t1912381035::get_offset_of_IsOpen_0(),
	VariableListed_t1912381035::get_offset_of_DictKey_1(),
	VariableListed_t1912381035::get_offset_of_StringValue_2(),
	VariableListed_t1912381035::get_offset_of_IntegerValue_3(),
	VariableListed_t1912381035::get_offset_of_FloatValue_4(),
	VariableListed_t1912381035::get_offset_of_BooleanValue_5(),
	VariableListed_t1912381035::get_offset_of_Type_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (ISN_SoomlaGrow_t2919772303), -1, sizeof(ISN_SoomlaGrow_t2919772303_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2249[7] = 
{
	ISN_SoomlaGrow_t2919772303_StaticFields::get_offset_of__IsInitialized_4(),
	ISN_SoomlaGrow_t2919772303_StaticFields::get_offset_of_ActionInitialized_5(),
	ISN_SoomlaGrow_t2919772303_StaticFields::get_offset_of_ActionConnected_6(),
	ISN_SoomlaGrow_t2919772303_StaticFields::get_offset_of_ActionDisconnected_7(),
	ISN_SoomlaGrow_t2919772303_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
	ISN_SoomlaGrow_t2919772303_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
	ISN_SoomlaGrow_t2919772303_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (ISN_SoomlaAction_t4294445400)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2250[6] = 
{
	ISN_SoomlaAction_t4294445400::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (ISN_SoomlaEvent_t319858442)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2251[5] = 
{
	ISN_SoomlaEvent_t319858442::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (ISN_SoomlaProvider_t2510794499)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2252[13] = 
{
	ISN_SoomlaProvider_t2510794499::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (IOSNativeSettings_t547170227), -1, sizeof(IOSNativeSettings_t547170227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2253[53] = 
{
	0,
	0,
	0,
	IOSNativeSettings_t547170227::get_offset_of_AppleId_5(),
	IOSNativeSettings_t547170227::get_offset_of_ToolbarIndex_6(),
	IOSNativeSettings_t547170227::get_offset_of_SendFakeEventsInEditor_7(),
	IOSNativeSettings_t547170227::get_offset_of_DefaultStoreProductsView_8(),
	IOSNativeSettings_t547170227::get_offset_of_InAppProducts_9(),
	IOSNativeSettings_t547170227::get_offset_of_Leaderboards_10(),
	IOSNativeSettings_t547170227::get_offset_of_Achievements_11(),
	IOSNativeSettings_t547170227::get_offset_of_checkInternetBeforeLoadRequest_12(),
	IOSNativeSettings_t547170227::get_offset_of_ShowStoreKitProducts_13(),
	IOSNativeSettings_t547170227::get_offset_of_ShowLeaderboards_14(),
	IOSNativeSettings_t547170227::get_offset_of_ShowAchievementsParams_15(),
	IOSNativeSettings_t547170227::get_offset_of_ShowUsersParams_16(),
	IOSNativeSettings_t547170227::get_offset_of_ShowOtherParams_17(),
	IOSNativeSettings_t547170227::get_offset_of_ShowRPKParams_18(),
	IOSNativeSettings_t547170227::get_offset_of_ExpandAPISettings_19(),
	IOSNativeSettings_t547170227::get_offset_of_EnableGameCenterAPI_20(),
	IOSNativeSettings_t547170227::get_offset_of_EnableInAppsAPI_21(),
	IOSNativeSettings_t547170227::get_offset_of_EnableCameraAPI_22(),
	IOSNativeSettings_t547170227::get_offset_of_EnableSocialSharingAPI_23(),
	IOSNativeSettings_t547170227::get_offset_of_EnablePickerAPI_24(),
	IOSNativeSettings_t547170227::get_offset_of_EnableMediaPlayerAPI_25(),
	IOSNativeSettings_t547170227::get_offset_of_EnableReplayKit_26(),
	IOSNativeSettings_t547170227::get_offset_of_EnableCloudKit_27(),
	IOSNativeSettings_t547170227::get_offset_of_EnableSoomla_28(),
	IOSNativeSettings_t547170227::get_offset_of_EnableGestureAPI_29(),
	IOSNativeSettings_t547170227::get_offset_of_EnablePushNotificationsAPI_30(),
	IOSNativeSettings_t547170227::get_offset_of_EnableContactsAPI_31(),
	IOSNativeSettings_t547170227::get_offset_of_EditorFillRateIndex_32(),
	IOSNativeSettings_t547170227::get_offset_of_EditorFillRate_33(),
	IOSNativeSettings_t547170227::get_offset_of_IsEditorTestingEnabled_34(),
	IOSNativeSettings_t547170227::get_offset_of_DisablePluginLogs_35(),
	IOSNativeSettings_t547170227::get_offset_of_UseGCRequestCaching_36(),
	IOSNativeSettings_t547170227::get_offset_of_UsePPForAchievements_37(),
	IOSNativeSettings_t547170227::get_offset_of_AutoLoadUsersSmallImages_38(),
	IOSNativeSettings_t547170227::get_offset_of_AutoLoadUsersBigImages_39(),
	IOSNativeSettings_t547170227::get_offset_of_MaxImageLoadSize_40(),
	IOSNativeSettings_t547170227::get_offset_of_JPegCompressionRate_41(),
	IOSNativeSettings_t547170227::get_offset_of_GalleryImageFormat_42(),
	IOSNativeSettings_t547170227::get_offset_of_RPK_iPadViewType_43(),
	IOSNativeSettings_t547170227::get_offset_of_CameraUsageDescription_44(),
	IOSNativeSettings_t547170227::get_offset_of_PhotoLibraryUsageDescription_45(),
	IOSNativeSettings_t547170227::get_offset_of_AppleMusicUsageDescription_46(),
	IOSNativeSettings_t547170227::get_offset_of_ContactsUsageDescription_47(),
	IOSNativeSettings_t547170227::get_offset_of_SoomlaDownloadLink_48(),
	IOSNativeSettings_t547170227::get_offset_of_SoomlaDocsLink_49(),
	IOSNativeSettings_t547170227::get_offset_of_SoomlaGameKey_50(),
	IOSNativeSettings_t547170227::get_offset_of_SoomlaEnvKey_51(),
	IOSNativeSettings_t547170227::get_offset_of_OneSignalEnabled_52(),
	IOSNativeSettings_t547170227::get_offset_of_OneSignalDocsLink_53(),
	IOSNativeSettings_t547170227_StaticFields::get_offset_of_instance_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (GameCenterInvitations_t2643374653), -1, sizeof(GameCenterInvitations_t2643374653_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2254[6] = 
{
	GameCenterInvitations_t2643374653_StaticFields::get_offset_of_ActionInviteeResponse_4(),
	GameCenterInvitations_t2643374653_StaticFields::get_offset_of_ActionPlayerAcceptedInvitation_5(),
	GameCenterInvitations_t2643374653_StaticFields::get_offset_of_ActionPlayerRequestedMatchWithRecipients_6(),
	GameCenterInvitations_t2643374653_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	GameCenterInvitations_t2643374653_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
	GameCenterInvitations_t2643374653_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (GameCenterManager_t1487113918), -1, sizeof(GameCenterManager_t1487113918_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2255[32] = 
{
	0,
	GameCenterManager_t1487113918_StaticFields::get_offset_of__IsInitialized_3(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of__IsAchievementsInfoLoaded_4(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of__players_5(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of__friendsList_6(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of__LeaderboardSets_7(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of__FriendRequests_8(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of__player_9(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_OnAuthFinished_10(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_OnScoreSubmitted_11(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_OnScoresListLoaded_12(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_OnLeadrboardInfoLoaded_13(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_OnLeaderboardSetsInfoLoaded_14(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_OnAchievementsReset_15(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_OnAchievementsLoaded_16(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_OnAchievementsProgress_17(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_OnGameCenterViewDismissed_18(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_OnFriendsListLoaded_19(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_OnUserInfoLoaded_20(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_OnPlayerSignatureRetrieveResult_21(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_22(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_U3CU3Ef__amU24cache14_23(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_U3CU3Ef__amU24cache15_24(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_U3CU3Ef__amU24cache16_25(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_U3CU3Ef__amU24cache17_26(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_U3CU3Ef__amU24cache18_27(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_U3CU3Ef__amU24cache19_28(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_U3CU3Ef__amU24cache1A_29(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_U3CU3Ef__amU24cache1B_30(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_31(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_U3CU3Ef__amU24cache1D_32(),
	GameCenterManager_t1487113918_StaticFields::get_offset_of_U3CU3Ef__amU24cache1E_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t165266512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[2] = 
{
	U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t165266512::get_offset_of_U24PC_0(),
	U3CLoadLeaderboardInfoLocalU3Ec__Iterator0_t165266512::get_offset_of_U24current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (GameCenter_RTM_t849630631), -1, sizeof(GameCenter_RTM_t849630631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2257[18] = 
{
	GameCenter_RTM_t849630631::get_offset_of__CurrentMatch_4(),
	GameCenter_RTM_t849630631::get_offset_of__NearbyPlayers_5(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_ActionMatchStarted_6(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_ActionMatchFailed_7(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_ActionNearbyPlayerStateUpdated_8(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_ActionActivityResultReceived_9(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_ActionDataSendError_10(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_ActionDataReceived_11(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_ActionPlayerStateChanged_12(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_ActionDiconnectedPlayerReinvited_13(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_14(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_15(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_16(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_17(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_18(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_19(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_20(),
	GameCenter_RTM_t849630631_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (GameCenter_TBM_t3457554475), -1, sizeof(GameCenter_TBM_t3457554475_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2258[27] = 
{
	GameCenter_TBM_t3457554475::get_offset_of__Matches_4(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_ActionMatchInfoLoaded_5(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_ActionMatchesInfoLoaded_6(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_ActionMatchDataUpdated_7(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_ActionMatchFound_8(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_ActionMatchQuit_9(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_ActionTrunEnded_10(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_ActionMacthEnded_11(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_ActionRematched_12(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_ActionMatchRemoved_13(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_ActionMatchInvitationAccepted_14(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_ActionMatchInvitationDeclined_15(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_ActionPlayerQuitForMatch_16(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_ActionTrunReceived_17(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_18(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_19(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_20(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_21(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_U3CU3Ef__amU24cache12_22(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_23(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_U3CU3Ef__amU24cache14_24(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_U3CU3Ef__amU24cache15_25(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_U3CU3Ef__amU24cache16_26(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_U3CU3Ef__amU24cache17_27(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_U3CU3Ef__amU24cache18_28(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_U3CU3Ef__amU24cache19_29(),
	GameCenter_TBM_t3457554475_StaticFields::get_offset_of_U3CU3Ef__amU24cache1A_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (ISN_CacheManager_t1215851066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (ISN_GameSaves_t2236256821), -1, sizeof(ISN_GameSaves_t2236256821_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2260[9] = 
{
	ISN_GameSaves_t2236256821_StaticFields::get_offset_of__CachedGameSaves_4(),
	ISN_GameSaves_t2236256821_StaticFields::get_offset_of_ActionSaveRemoved_5(),
	ISN_GameSaves_t2236256821_StaticFields::get_offset_of_ActionGameSaved_6(),
	ISN_GameSaves_t2236256821_StaticFields::get_offset_of_ActionSavesFetched_7(),
	ISN_GameSaves_t2236256821_StaticFields::get_offset_of_ActionSavesResolved_8(),
	ISN_GameSaves_t2236256821_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
	ISN_GameSaves_t2236256821_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_10(),
	ISN_GameSaves_t2236256821_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
	ISN_GameSaves_t2236256821_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (GK_SavedGame_t3320093620), -1, sizeof(GK_SavedGame_t3320093620_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2261[8] = 
{
	GK_SavedGame_t3320093620::get_offset_of__Id_0(),
	GK_SavedGame_t3320093620::get_offset_of__Name_1(),
	GK_SavedGame_t3320093620::get_offset_of__DeviceName_2(),
	GK_SavedGame_t3320093620::get_offset_of__ModificationDate_3(),
	GK_SavedGame_t3320093620::get_offset_of__Data_4(),
	GK_SavedGame_t3320093620::get_offset_of__IsDataLoaded_5(),
	GK_SavedGame_t3320093620::get_offset_of_ActionDataLoaded_6(),
	GK_SavedGame_t3320093620_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (GK_FetchResult_t1611512656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[1] = 
{
	GK_FetchResult_t1611512656::get_offset_of__SavedGames_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (GK_SaveDataLoaded_t3684688319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2263[1] = 
{
	GK_SaveDataLoaded_t3684688319::get_offset_of__SavedGame_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (GK_SaveRemoveResult_t539310567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[1] = 
{
	GK_SaveRemoveResult_t539310567::get_offset_of__SaveName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (GK_SaveResult_t3946576453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[1] = 
{
	GK_SaveResult_t3946576453::get_offset_of__SavedGame_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (GK_SavesResolveResult_t3508055404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[1] = 
{
	GK_SavesResolveResult_t3508055404::get_offset_of__ResolvedSaves_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (GK_CollectionType_t3353981271)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2267[3] = 
{
	GK_CollectionType_t3353981271::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (GK_InviteRecipientResponse_t3438857802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2268[7] = 
{
	GK_InviteRecipientResponse_t3438857802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (GK_MatchType_t1493351924)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2269[3] = 
{
	GK_MatchType_t1493351924::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (GK_PhotoSize_t3124681388)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2270[3] = 
{
	GK_PhotoSize_t3124681388::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (GK_TimeSpan_t1050271570)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2271[4] = 
{
	GK_TimeSpan_t1050271570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (GK_FriendRequest_t4101620808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[3] = 
{
	GK_FriendRequest_t4101620808::get_offset_of__Id_0(),
	GK_FriendRequest_t4101620808::get_offset_of__PlayersIds_1(),
	GK_FriendRequest_t4101620808::get_offset_of__Emails_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (GK_AchievementProgressResult_t3539574352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[1] = 
{
	GK_AchievementProgressResult_t3539574352::get_offset_of__tpl_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (GK_LeaderboardResult_t866080833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[1] = 
{
	GK_LeaderboardResult_t866080833::get_offset_of__Leaderboard_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (GK_PlayerSignatureResult_t13769479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[4] = 
{
	GK_PlayerSignatureResult_t13769479::get_offset_of__PublicKeyUrl_1(),
	GK_PlayerSignatureResult_t13769479::get_offset_of__Signature_2(),
	GK_PlayerSignatureResult_t13769479::get_offset_of__Salt_3(),
	GK_PlayerSignatureResult_t13769479::get_offset_of__Timestamp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (GK_UserInfoLoadResult_t1177841233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[2] = 
{
	GK_UserInfoLoadResult_t1177841233::get_offset_of__playerId_1(),
	GK_UserInfoLoadResult_t1177841233::get_offset_of__tpl_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (GK_UserPhotoLoadResult_t1614198031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[2] = 
{
	GK_UserPhotoLoadResult_t1614198031::get_offset_of__Photo_1(),
	GK_UserPhotoLoadResult_t1614198031::get_offset_of__Size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (ISN_LoadSetLeaderboardsInfoResult_t3997789804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[1] = 
{
	ISN_LoadSetLeaderboardsInfoResult_t3997789804::get_offset_of__LeaderBoardsSet_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (GK_AchievementTemplate_t2296152240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[6] = 
{
	GK_AchievementTemplate_t2296152240::get_offset_of_IsOpen_0(),
	GK_AchievementTemplate_t2296152240::get_offset_of_Id_1(),
	GK_AchievementTemplate_t2296152240::get_offset_of_Title_2(),
	GK_AchievementTemplate_t2296152240::get_offset_of_Description_3(),
	GK_AchievementTemplate_t2296152240::get_offset_of__progress_4(),
	GK_AchievementTemplate_t2296152240::get_offset_of_Texture_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (GK_Invite_t22070530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[4] = 
{
	GK_Invite_t22070530::get_offset_of__Id_0(),
	GK_Invite_t22070530::get_offset_of__Sender_1(),
	GK_Invite_t22070530::get_offset_of__PlayerGroup_2(),
	GK_Invite_t22070530::get_offset_of__PlayerAttributes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (GK_LeaderBoardInfo_t3670215494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[5] = 
{
	GK_LeaderBoardInfo_t3670215494::get_offset_of_Title_0(),
	GK_LeaderBoardInfo_t3670215494::get_offset_of_Description_1(),
	GK_LeaderBoardInfo_t3670215494::get_offset_of_Identifier_2(),
	GK_LeaderBoardInfo_t3670215494::get_offset_of_Texture_3(),
	GK_LeaderBoardInfo_t3670215494::get_offset_of_MaxRange_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (GK_Leaderboard_t156446466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[7] = 
{
	GK_Leaderboard_t156446466::get_offset_of_IsOpen_0(),
	GK_Leaderboard_t156446466::get_offset_of__CurrentPlayerScoreLoaded_1(),
	GK_Leaderboard_t156446466::get_offset_of_SocsialCollection_2(),
	GK_Leaderboard_t156446466::get_offset_of_GlobalCollection_3(),
	GK_Leaderboard_t156446466::get_offset_of_CurrentPlayerScore_4(),
	GK_Leaderboard_t156446466::get_offset_of_ScoreUpdateListners_5(),
	GK_Leaderboard_t156446466::get_offset_of__info_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (GK_LeaderboardSet_t5314098), -1, sizeof(GK_LeaderboardSet_t5314098_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2283[6] = 
{
	GK_LeaderboardSet_t5314098::get_offset_of_Title_0(),
	GK_LeaderboardSet_t5314098::get_offset_of_Identifier_1(),
	GK_LeaderboardSet_t5314098::get_offset_of_GroupIdentifier_2(),
	GK_LeaderboardSet_t5314098::get_offset_of__BoardsInfo_3(),
	GK_LeaderboardSet_t5314098::get_offset_of_OnLoaderboardsInfoLoaded_4(),
	GK_LeaderboardSet_t5314098_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (GK_LocalPlayerScoreUpdateListener_t1070875322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[5] = 
{
	GK_LocalPlayerScoreUpdateListener_t1070875322::get_offset_of__RequestId_0(),
	GK_LocalPlayerScoreUpdateListener_t1070875322::get_offset_of__IsInternal_1(),
	GK_LocalPlayerScoreUpdateListener_t1070875322::get_offset_of__leaderboardId_2(),
	GK_LocalPlayerScoreUpdateListener_t1070875322::get_offset_of__ErrorData_3(),
	GK_LocalPlayerScoreUpdateListener_t1070875322::get_offset_of_Scores_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (GK_Player_t2782008294), -1, sizeof(GK_Player_t2782008294_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2285[8] = 
{
	GK_Player_t2782008294::get_offset_of__PlayerId_0(),
	GK_Player_t2782008294::get_offset_of__DisplayName_1(),
	GK_Player_t2782008294::get_offset_of__Alias_2(),
	GK_Player_t2782008294::get_offset_of__SmallPhoto_3(),
	GK_Player_t2782008294::get_offset_of__BigPhoto_4(),
	GK_Player_t2782008294_StaticFields::get_offset_of_LocalPhotosCache_5(),
	GK_Player_t2782008294::get_offset_of_OnPlayerPhotoLoaded_6(),
	GK_Player_t2782008294_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (GK_Score_t1529008873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[7] = 
{
	GK_Score_t1529008873::get_offset_of__Rank_0(),
	GK_Score_t1529008873::get_offset_of__Score_1(),
	GK_Score_t1529008873::get_offset_of__Context_2(),
	GK_Score_t1529008873::get_offset_of__PlayerId_3(),
	GK_Score_t1529008873::get_offset_of__LeaderboardId_4(),
	GK_Score_t1529008873::get_offset_of__Collection_5(),
	GK_Score_t1529008873::get_offset_of__TimeSpan_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (GK_ScoreCollection_t4092547735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[3] = 
{
	GK_ScoreCollection_t4092547735::get_offset_of_AllTimeScores_0(),
	GK_ScoreCollection_t4092547735::get_offset_of_WeekScores_1(),
	GK_ScoreCollection_t4092547735::get_offset_of_TodayScores_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (GK_MatchSendDataMode_t3659269993)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2288[3] = 
{
	GK_MatchSendDataMode_t3659269993::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (GK_PlayerConnectionState_t2434478783)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2289[4] = 
{
	GK_PlayerConnectionState_t2434478783::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (GK_RTM_MatchStartedResult_t833698690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[1] = 
{
	GK_RTM_MatchStartedResult_t833698690::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (GK_RTM_QueryActivityResult_t2276098399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[1] = 
{
	GK_RTM_QueryActivityResult_t2276098399::get_offset_of__Activity_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (GK_RTM_Match_t873568990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[2] = 
{
	GK_RTM_Match_t873568990::get_offset_of__ExpectedPlayerCount_0(),
	GK_RTM_Match_t873568990::get_offset_of__Players_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (GK_TurnBasedMatchOutcome_t2242380984)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2293[11] = 
{
	GK_TurnBasedMatchOutcome_t2242380984::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (GK_TurnBasedMatchStatus_t2382635170)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2294[5] = 
{
	GK_TurnBasedMatchStatus_t2382635170::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (GK_TurnBasedParticipantStatus_t2126479626)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2295[7] = 
{
	GK_TurnBasedParticipantStatus_t2126479626::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (GK_TBM_EndTrunResult_t1517380690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[1] = 
{
	GK_TBM_EndTrunResult_t1517380690::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (GK_TBM_LoadMatchResult_t1639249273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[1] = 
{
	GK_TBM_LoadMatchResult_t1639249273::get_offset_of__Match_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (GK_TBM_LoadMatchesResult_t370491735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[1] = 
{
	GK_TBM_LoadMatchesResult_t370491735::get_offset_of_LoadedMatches_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (GK_TBM_MatchDataUpdateResult_t1356006034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2299[1] = 
{
	GK_TBM_MatchDataUpdateResult_t1356006034::get_offset_of__Match_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
