﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExifLib.JpegInfo
struct JpegInfo_t3114827956;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExifLib.ExifReader
struct  ExifReader_t3799989581  : public Il2CppObject
{
public:
	// System.Boolean ExifLib.ExifReader::littleEndian
	bool ___littleEndian_0;
	// ExifLib.JpegInfo ExifLib.ExifReader::<info>k__BackingField
	JpegInfo_t3114827956 * ___U3CinfoU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_littleEndian_0() { return static_cast<int32_t>(offsetof(ExifReader_t3799989581, ___littleEndian_0)); }
	inline bool get_littleEndian_0() const { return ___littleEndian_0; }
	inline bool* get_address_of_littleEndian_0() { return &___littleEndian_0; }
	inline void set_littleEndian_0(bool value)
	{
		___littleEndian_0 = value;
	}

	inline static int32_t get_offset_of_U3CinfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExifReader_t3799989581, ___U3CinfoU3Ek__BackingField_1)); }
	inline JpegInfo_t3114827956 * get_U3CinfoU3Ek__BackingField_1() const { return ___U3CinfoU3Ek__BackingField_1; }
	inline JpegInfo_t3114827956 ** get_address_of_U3CinfoU3Ek__BackingField_1() { return &___U3CinfoU3Ek__BackingField_1; }
	inline void set_U3CinfoU3Ek__BackingField_1(JpegInfo_t3114827956 * value)
	{
		___U3CinfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CinfoU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
