﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Crop
struct  Crop_t2548131222  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Crop::resultImage
	GameObject_t1756533147 * ___resultImage_2;
	// System.Boolean Crop::action_1finger
	bool ___action_1finger_3;
	// System.Boolean Crop::action_1finger_waitForEpsilon
	bool ___action_1finger_waitForEpsilon_4;
	// UnityEngine.Vector2 Crop::waitForEpsilon_v0
	Vector2_t2243707579  ___waitForEpsilon_v0_5;
	// System.Single Crop::move1Epsilon
	float ___move1Epsilon_6;
	// System.Boolean Crop::action_2fingers
	bool ___action_2fingers_7;
	// UnityEngine.Vector2 Crop::v0
	Vector2_t2243707579  ___v0_8;
	// UnityEngine.Vector2 Crop::v1
	Vector2_t2243707579  ___v1_9;
	// UnityEngine.Vector2 Crop::v
	Vector2_t2243707579  ___v_10;
	// System.Single Crop::v_angle
	float ___v_angle_11;
	// System.Boolean Crop::currentTouchCancelled
	bool ___currentTouchCancelled_12;
	// System.Boolean Crop::anyTouches
	bool ___anyTouches_13;
	// System.Boolean Crop::touchInArea
	bool ___touchInArea_14;
	// System.Int32 Crop::lastTouchCount
	int32_t ___lastTouchCount_15;
	// System.Int32 Crop::touchCount_once
	int32_t ___touchCount_once_16;

public:
	inline static int32_t get_offset_of_resultImage_2() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___resultImage_2)); }
	inline GameObject_t1756533147 * get_resultImage_2() const { return ___resultImage_2; }
	inline GameObject_t1756533147 ** get_address_of_resultImage_2() { return &___resultImage_2; }
	inline void set_resultImage_2(GameObject_t1756533147 * value)
	{
		___resultImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___resultImage_2, value);
	}

	inline static int32_t get_offset_of_action_1finger_3() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___action_1finger_3)); }
	inline bool get_action_1finger_3() const { return ___action_1finger_3; }
	inline bool* get_address_of_action_1finger_3() { return &___action_1finger_3; }
	inline void set_action_1finger_3(bool value)
	{
		___action_1finger_3 = value;
	}

	inline static int32_t get_offset_of_action_1finger_waitForEpsilon_4() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___action_1finger_waitForEpsilon_4)); }
	inline bool get_action_1finger_waitForEpsilon_4() const { return ___action_1finger_waitForEpsilon_4; }
	inline bool* get_address_of_action_1finger_waitForEpsilon_4() { return &___action_1finger_waitForEpsilon_4; }
	inline void set_action_1finger_waitForEpsilon_4(bool value)
	{
		___action_1finger_waitForEpsilon_4 = value;
	}

	inline static int32_t get_offset_of_waitForEpsilon_v0_5() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___waitForEpsilon_v0_5)); }
	inline Vector2_t2243707579  get_waitForEpsilon_v0_5() const { return ___waitForEpsilon_v0_5; }
	inline Vector2_t2243707579 * get_address_of_waitForEpsilon_v0_5() { return &___waitForEpsilon_v0_5; }
	inline void set_waitForEpsilon_v0_5(Vector2_t2243707579  value)
	{
		___waitForEpsilon_v0_5 = value;
	}

	inline static int32_t get_offset_of_move1Epsilon_6() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___move1Epsilon_6)); }
	inline float get_move1Epsilon_6() const { return ___move1Epsilon_6; }
	inline float* get_address_of_move1Epsilon_6() { return &___move1Epsilon_6; }
	inline void set_move1Epsilon_6(float value)
	{
		___move1Epsilon_6 = value;
	}

	inline static int32_t get_offset_of_action_2fingers_7() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___action_2fingers_7)); }
	inline bool get_action_2fingers_7() const { return ___action_2fingers_7; }
	inline bool* get_address_of_action_2fingers_7() { return &___action_2fingers_7; }
	inline void set_action_2fingers_7(bool value)
	{
		___action_2fingers_7 = value;
	}

	inline static int32_t get_offset_of_v0_8() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___v0_8)); }
	inline Vector2_t2243707579  get_v0_8() const { return ___v0_8; }
	inline Vector2_t2243707579 * get_address_of_v0_8() { return &___v0_8; }
	inline void set_v0_8(Vector2_t2243707579  value)
	{
		___v0_8 = value;
	}

	inline static int32_t get_offset_of_v1_9() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___v1_9)); }
	inline Vector2_t2243707579  get_v1_9() const { return ___v1_9; }
	inline Vector2_t2243707579 * get_address_of_v1_9() { return &___v1_9; }
	inline void set_v1_9(Vector2_t2243707579  value)
	{
		___v1_9 = value;
	}

	inline static int32_t get_offset_of_v_10() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___v_10)); }
	inline Vector2_t2243707579  get_v_10() const { return ___v_10; }
	inline Vector2_t2243707579 * get_address_of_v_10() { return &___v_10; }
	inline void set_v_10(Vector2_t2243707579  value)
	{
		___v_10 = value;
	}

	inline static int32_t get_offset_of_v_angle_11() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___v_angle_11)); }
	inline float get_v_angle_11() const { return ___v_angle_11; }
	inline float* get_address_of_v_angle_11() { return &___v_angle_11; }
	inline void set_v_angle_11(float value)
	{
		___v_angle_11 = value;
	}

	inline static int32_t get_offset_of_currentTouchCancelled_12() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___currentTouchCancelled_12)); }
	inline bool get_currentTouchCancelled_12() const { return ___currentTouchCancelled_12; }
	inline bool* get_address_of_currentTouchCancelled_12() { return &___currentTouchCancelled_12; }
	inline void set_currentTouchCancelled_12(bool value)
	{
		___currentTouchCancelled_12 = value;
	}

	inline static int32_t get_offset_of_anyTouches_13() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___anyTouches_13)); }
	inline bool get_anyTouches_13() const { return ___anyTouches_13; }
	inline bool* get_address_of_anyTouches_13() { return &___anyTouches_13; }
	inline void set_anyTouches_13(bool value)
	{
		___anyTouches_13 = value;
	}

	inline static int32_t get_offset_of_touchInArea_14() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___touchInArea_14)); }
	inline bool get_touchInArea_14() const { return ___touchInArea_14; }
	inline bool* get_address_of_touchInArea_14() { return &___touchInArea_14; }
	inline void set_touchInArea_14(bool value)
	{
		___touchInArea_14 = value;
	}

	inline static int32_t get_offset_of_lastTouchCount_15() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___lastTouchCount_15)); }
	inline int32_t get_lastTouchCount_15() const { return ___lastTouchCount_15; }
	inline int32_t* get_address_of_lastTouchCount_15() { return &___lastTouchCount_15; }
	inline void set_lastTouchCount_15(int32_t value)
	{
		___lastTouchCount_15 = value;
	}

	inline static int32_t get_offset_of_touchCount_once_16() { return static_cast<int32_t>(offsetof(Crop_t2548131222, ___touchCount_once_16)); }
	inline int32_t get_touchCount_once_16() const { return ___touchCount_once_16; }
	inline int32_t* get_address_of_touchCount_once_16() { return &___touchCount_once_16; }
	inline void set_touchCount_once_16(int32_t value)
	{
		___touchCount_once_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
