﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Animation.SA_iTween
struct SA_iTween_t1938847631;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SA.Common.Animation.SA_iTween::.ctor(System.Collections.Hashtable)
extern "C"  void SA_iTween__ctor_m3192894451 (SA_iTween_t1938847631 * __this, Hashtable_t909839986 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::.cctor()
extern "C"  void SA_iTween__cctor_m4037473410 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Init(UnityEngine.GameObject)
extern "C"  void SA_iTween_Init_m1578351877 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::CameraFadeFrom(System.Single,System.Single)
extern "C"  void SA_iTween_CameraFadeFrom_m3832967076 (Il2CppObject * __this /* static, unused */, float ___amount0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::CameraFadeFrom(System.Collections.Hashtable)
extern "C"  void SA_iTween_CameraFadeFrom_m4096554326 (Il2CppObject * __this /* static, unused */, Hashtable_t909839986 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::CameraFadeTo(System.Single,System.Single)
extern "C"  void SA_iTween_CameraFadeTo_m3293121011 (Il2CppObject * __this /* static, unused */, float ___amount0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::CameraFadeTo(System.Collections.Hashtable)
extern "C"  void SA_iTween_CameraFadeTo_m922141511 (Il2CppObject * __this /* static, unused */, Hashtable_t909839986 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ValueTo(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_ValueTo_m2879304615 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::FadeFrom(UnityEngine.GameObject,System.Single,System.Single)
extern "C"  void SA_iTween_FadeFrom_m830706187 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, float ___alpha1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::FadeFrom(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_FadeFrom_m144959823 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::FadeTo(UnityEngine.GameObject,System.Single,System.Single)
extern "C"  void SA_iTween_FadeTo_m2229091362 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, float ___alpha1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::FadeTo(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_FadeTo_m597822936 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ColorFrom(UnityEngine.GameObject,UnityEngine.Color,System.Single)
extern "C"  void SA_iTween_ColorFrom_m2056666639 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Color_t2020392075  ___color1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ColorFrom(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_ColorFrom_m3829225828 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ColorTo(UnityEngine.GameObject,UnityEngine.Color,System.Single)
extern "C"  void SA_iTween_ColorTo_m2734052538 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Color_t2020392075  ___color1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ColorTo(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_ColorTo_m2171011273 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::AudioFrom(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern "C"  void SA_iTween_AudioFrom_m857093872 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, float ___volume1, float ___pitch2, float ___time3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::AudioFrom(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_AudioFrom_m3927188171 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::AudioTo(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern "C"  void SA_iTween_AudioTo_m2357568019 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, float ___volume1, float ___pitch2, float ___time3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::AudioTo(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_AudioTo_m1530715592 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Stab(UnityEngine.GameObject,UnityEngine.AudioClip,System.Single)
extern "C"  void SA_iTween_Stab_m3114122853 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, AudioClip_t1932558630 * ___audioclip1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Stab(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_Stab_m347338311 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::LookFrom(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_LookFrom_m428302642 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___looktarget1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::LookFrom(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_LookFrom_m3764332564 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::LookTo(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_LookTo_m244488155 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___looktarget1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::LookTo(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_LookTo_m468867827 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::MoveTo(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_MoveTo_m2803301429 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___position1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::MoveTo(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_MoveTo_m1360690965 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::MoveFrom(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_MoveFrom_m491600156 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___position1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::MoveFrom(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_MoveFrom_m1518322758 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::MoveAdd(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_MoveAdd_m2795598879 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::MoveAdd(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_MoveAdd_m1627185583 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::MoveBy(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_MoveBy_m2211937593 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::MoveBy(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_MoveBy_m687393233 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ScaleTo(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_ScaleTo_m3629596018 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___scale1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ScaleTo(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_ScaleTo_m3186008920 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ScaleFrom(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_ScaleFrom_m2928206325 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___scale1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ScaleFrom(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_ScaleFrom_m1263719197 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ScaleAdd(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_ScaleAdd_m2170980624 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ScaleAdd(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_ScaleAdd_m58004550 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ScaleBy(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_ScaleBy_m4101093998 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ScaleBy(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_ScaleBy_m2337442748 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::RotateTo(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_RotateTo_m1180959177 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___rotation1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::RotateTo(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_RotateTo_m4207414097 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::RotateFrom(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_RotateFrom_m358328586 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___rotation1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::RotateFrom(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_RotateFrom_m208567560 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::RotateAdd(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_RotateAdd_m580146451 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::RotateAdd(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_RotateAdd_m968907531 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::RotateBy(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_RotateBy_m2855689677 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::RotateBy(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_RotateBy_m389583885 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ShakePosition(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_ShakePosition_m3202377932 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ShakePosition(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_ShakePosition_m4111327066 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ShakeScale(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_ShakeScale_m3014191535 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ShakeScale(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_ShakeScale_m1030078711 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ShakeRotation(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_ShakeRotation_m3472096719 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ShakeRotation(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_ShakeRotation_m2634596319 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::PunchPosition(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_PunchPosition_m1976171954 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::PunchPosition(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_PunchPosition_m3319106860 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::PunchRotation(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_PunchRotation_m2056711679 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::PunchRotation(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_PunchRotation_m1044480631 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::PunchScale(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_PunchScale_m665227335 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___amount1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::PunchScale(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_PunchScale_m3567505823 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateTargets()
extern "C"  void SA_iTween_GenerateTargets_m627633464 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateRectTargets()
extern "C"  void SA_iTween_GenerateRectTargets_m3130251858 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateColorTargets()
extern "C"  void SA_iTween_GenerateColorTargets_m2244811963 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateVector3Targets()
extern "C"  void SA_iTween_GenerateVector3Targets_m3427556634 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateVector2Targets()
extern "C"  void SA_iTween_GenerateVector2Targets_m2126672629 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateFloatTargets()
extern "C"  void SA_iTween_GenerateFloatTargets_m2335990808 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateColorToTargets()
extern "C"  void SA_iTween_GenerateColorToTargets_m1247068308 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateAudioToTargets()
extern "C"  void SA_iTween_GenerateAudioToTargets_m1225159499 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateStabTargets()
extern "C"  void SA_iTween_GenerateStabTargets_m2383760356 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateLookToTargets()
extern "C"  void SA_iTween_GenerateLookToTargets_m1080068150 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateMoveToPathTargets()
extern "C"  void SA_iTween_GenerateMoveToPathTargets_m2997793425 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateMoveToTargets()
extern "C"  void SA_iTween_GenerateMoveToTargets_m2406215148 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateMoveByTargets()
extern "C"  void SA_iTween_GenerateMoveByTargets_m2808734760 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateScaleToTargets()
extern "C"  void SA_iTween_GenerateScaleToTargets_m565065805 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateScaleByTargets()
extern "C"  void SA_iTween_GenerateScaleByTargets_m1751471097 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateScaleAddTargets()
extern "C"  void SA_iTween_GenerateScaleAddTargets_m2911821827 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateRotateToTargets()
extern "C"  void SA_iTween_GenerateRotateToTargets_m2434445978 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateRotateAddTargets()
extern "C"  void SA_iTween_GenerateRotateAddTargets_m3269930008 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateRotateByTargets()
extern "C"  void SA_iTween_GenerateRotateByTargets_m2846165462 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateShakePositionTargets()
extern "C"  void SA_iTween_GenerateShakePositionTargets_m353849087 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateShakeScaleTargets()
extern "C"  void SA_iTween_GenerateShakeScaleTargets_m34679892 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GenerateShakeRotationTargets()
extern "C"  void SA_iTween_GenerateShakeRotationTargets_m232064378 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GeneratePunchPositionTargets()
extern "C"  void SA_iTween_GeneratePunchPositionTargets_m2202545847 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GeneratePunchRotationTargets()
extern "C"  void SA_iTween_GeneratePunchRotationTargets_m145554284 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GeneratePunchScaleTargets()
extern "C"  void SA_iTween_GeneratePunchScaleTargets_m1817258458 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyRectTargets()
extern "C"  void SA_iTween_ApplyRectTargets_m2771845965 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyColorTargets()
extern "C"  void SA_iTween_ApplyColorTargets_m1048894260 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyVector3Targets()
extern "C"  void SA_iTween_ApplyVector3Targets_m4155219835 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyVector2Targets()
extern "C"  void SA_iTween_ApplyVector2Targets_m4154177596 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyFloatTargets()
extern "C"  void SA_iTween_ApplyFloatTargets_m2917330173 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyColorToTargets()
extern "C"  void SA_iTween_ApplyColorToTargets_m2561027363 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyAudioToTargets()
extern "C"  void SA_iTween_ApplyAudioToTargets_m647130100 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyStabTargets()
extern "C"  void SA_iTween_ApplyStabTargets_m1028611785 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyMoveToPathTargets()
extern "C"  void SA_iTween_ApplyMoveToPathTargets_m1825969120 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyMoveToTargets()
extern "C"  void SA_iTween_ApplyMoveToTargets_m4264909479 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyMoveByTargets()
extern "C"  void SA_iTween_ApplyMoveByTargets_m2295636607 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyScaleToTargets()
extern "C"  void SA_iTween_ApplyScaleToTargets_m1707687700 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyLookToTargets()
extern "C"  void SA_iTween_ApplyLookToTargets_m4029914085 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyRotateToTargets()
extern "C"  void SA_iTween_ApplyRotateToTargets_m2423783107 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyRotateAddTargets()
extern "C"  void SA_iTween_ApplyRotateAddTargets_m2349860489 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyShakePositionTargets()
extern "C"  void SA_iTween_ApplyShakePositionTargets_m2083578990 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyShakeScaleTargets()
extern "C"  void SA_iTween_ApplyShakeScaleTargets_m2874823685 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyShakeRotationTargets()
extern "C"  void SA_iTween_ApplyShakeRotationTargets_m1706596893 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyPunchPositionTargets()
extern "C"  void SA_iTween_ApplyPunchPositionTargets_m1981973572 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyPunchRotationTargets()
extern "C"  void SA_iTween_ApplyPunchRotationTargets_m2267174949 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ApplyPunchScaleTargets()
extern "C"  void SA_iTween_ApplyPunchScaleTargets_m1837421285 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SA.Common.Animation.SA_iTween::TweenDelay()
extern "C"  Il2CppObject * SA_iTween_TweenDelay_m3808409289 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::TweenStart()
extern "C"  void SA_iTween_TweenStart_m1611344366 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SA.Common.Animation.SA_iTween::TweenRestart()
extern "C"  Il2CppObject * SA_iTween_TweenRestart_m3511830439 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::TweenUpdate()
extern "C"  void SA_iTween_TweenUpdate_m1204932763 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::TweenComplete()
extern "C"  void SA_iTween_TweenComplete_m601793927 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::TweenLoop()
extern "C"  void SA_iTween_TweenLoop_m2867885978 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect SA.Common.Animation.SA_iTween::RectUpdate(UnityEngine.Rect,UnityEngine.Rect,System.Single)
extern "C"  Rect_t3681755626  SA_iTween_RectUpdate_m3446209199 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___currentValue0, Rect_t3681755626  ___targetValue1, float ___speed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SA.Common.Animation.SA_iTween::Vector3Update(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  SA_iTween_Vector3Update_m266864619 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___currentValue0, Vector3_t2243707580  ___targetValue1, float ___speed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SA.Common.Animation.SA_iTween::Vector2Update(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2243707579  SA_iTween_Vector2Update_m3981322959 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___currentValue0, Vector2_t2243707579  ___targetValue1, float ___speed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::FloatUpdate(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_FloatUpdate_m593285381 (Il2CppObject * __this /* static, unused */, float ___currentValue0, float ___targetValue1, float ___speed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::FadeUpdate(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_FadeUpdate_m4264101176 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::FadeUpdate(UnityEngine.GameObject,System.Single,System.Single)
extern "C"  void SA_iTween_FadeUpdate_m3257478882 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, float ___alpha1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ColorUpdate(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_ColorUpdate_m4133841215 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ColorUpdate(UnityEngine.GameObject,UnityEngine.Color,System.Single)
extern "C"  void SA_iTween_ColorUpdate_m2802118174 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Color_t2020392075  ___color1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::AudioUpdate(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_AudioUpdate_m1517585248 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::AudioUpdate(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern "C"  void SA_iTween_AudioUpdate_m3864898497 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, float ___volume1, float ___pitch2, float ___time3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::RotateUpdate(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_RotateUpdate_m638131655 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::RotateUpdate(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_RotateUpdate_m3244451111 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___rotation1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ScaleUpdate(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_ScaleUpdate_m3409073836 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ScaleUpdate(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_ScaleUpdate_m4036465946 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___scale1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::MoveUpdate(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_MoveUpdate_m1367207379 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::MoveUpdate(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_MoveUpdate_m1037565323 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___position1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::LookUpdate(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_LookUpdate_m3111457373 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::LookUpdate(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void SA_iTween_LookUpdate_m1045342685 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3_t2243707580  ___looktarget1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::PathLength(UnityEngine.Transform[])
extern "C"  float SA_iTween_PathLength_m1398040149 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::PathLength(UnityEngine.Vector3[])
extern "C"  float SA_iTween_PathLength_m1517192463 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D SA.Common.Animation.SA_iTween::CameraTexture(UnityEngine.Color)
extern "C"  Texture2D_t3542995729 * SA_iTween_CameraTexture_m355143412 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::PutOnPath(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern "C"  void SA_iTween_PutOnPath_m413820042 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Vector3U5BU5D_t1172311765* ___path1, float ___percent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::PutOnPath(UnityEngine.Transform,UnityEngine.Vector3[],System.Single)
extern "C"  void SA_iTween_PutOnPath_m2652547207 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, Vector3U5BU5D_t1172311765* ___path1, float ___percent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::PutOnPath(UnityEngine.GameObject,UnityEngine.Transform[],System.Single)
extern "C"  void SA_iTween_PutOnPath_m2380958630 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, TransformU5BU5D_t3764228911* ___path1, float ___percent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::PutOnPath(UnityEngine.Transform,UnityEngine.Transform[],System.Single)
extern "C"  void SA_iTween_PutOnPath_m2440183365 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, TransformU5BU5D_t3764228911* ___path1, float ___percent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SA.Common.Animation.SA_iTween::PointOnPath(UnityEngine.Transform[],System.Single)
extern "C"  Vector3_t2243707580  SA_iTween_PointOnPath_m3673809329 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___path0, float ___percent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawLine(UnityEngine.Vector3[])
extern "C"  void SA_iTween_DrawLine_m347025132 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawLine(UnityEngine.Vector3[],UnityEngine.Color)
extern "C"  void SA_iTween_DrawLine_m4007863862 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___line0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawLine(UnityEngine.Transform[])
extern "C"  void SA_iTween_DrawLine_m2766294140 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawLine(UnityEngine.Transform[],UnityEngine.Color)
extern "C"  void SA_iTween_DrawLine_m2776008022 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___line0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawLineGizmos(UnityEngine.Vector3[])
extern "C"  void SA_iTween_DrawLineGizmos_m4146730447 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawLineGizmos(UnityEngine.Vector3[],UnityEngine.Color)
extern "C"  void SA_iTween_DrawLineGizmos_m2800490493 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___line0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawLineGizmos(UnityEngine.Transform[])
extern "C"  void SA_iTween_DrawLineGizmos_m2991846645 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawLineGizmos(UnityEngine.Transform[],UnityEngine.Color)
extern "C"  void SA_iTween_DrawLineGizmos_m3862851507 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___line0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawLineHandles(UnityEngine.Vector3[])
extern "C"  void SA_iTween_DrawLineHandles_m1895620695 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawLineHandles(UnityEngine.Vector3[],UnityEngine.Color)
extern "C"  void SA_iTween_DrawLineHandles_m1470537585 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___line0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawLineHandles(UnityEngine.Transform[])
extern "C"  void SA_iTween_DrawLineHandles_m3260284913 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawLineHandles(UnityEngine.Transform[],UnityEngine.Color)
extern "C"  void SA_iTween_DrawLineHandles_m4129859131 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___line0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SA.Common.Animation.SA_iTween::PointOnPath(UnityEngine.Vector3[],System.Single)
extern "C"  Vector3_t2243707580  SA_iTween_PointOnPath_m1316411447 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___path0, float ___percent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawPath(UnityEngine.Vector3[])
extern "C"  void SA_iTween_DrawPath_m734577509 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawPath(UnityEngine.Vector3[],UnityEngine.Color)
extern "C"  void SA_iTween_DrawPath_m880712231 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___path0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawPath(UnityEngine.Transform[])
extern "C"  void SA_iTween_DrawPath_m2834608843 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawPath(UnityEngine.Transform[],UnityEngine.Color)
extern "C"  void SA_iTween_DrawPath_m1169934493 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___path0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawPathGizmos(UnityEngine.Vector3[])
extern "C"  void SA_iTween_DrawPathGizmos_m320414430 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawPathGizmos(UnityEngine.Vector3[],UnityEngine.Color)
extern "C"  void SA_iTween_DrawPathGizmos_m3162984836 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___path0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawPathGizmos(UnityEngine.Transform[])
extern "C"  void SA_iTween_DrawPathGizmos_m3251598830 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawPathGizmos(UnityEngine.Transform[],UnityEngine.Color)
extern "C"  void SA_iTween_DrawPathGizmos_m2308642340 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___path0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawPathHandles(UnityEngine.Vector3[])
extern "C"  void SA_iTween_DrawPathHandles_m1226949116 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawPathHandles(UnityEngine.Vector3[],UnityEngine.Color)
extern "C"  void SA_iTween_DrawPathHandles_m2716933010 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___path0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawPathHandles(UnityEngine.Transform[])
extern "C"  void SA_iTween_DrawPathHandles_m1017381776 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawPathHandles(UnityEngine.Transform[],UnityEngine.Color)
extern "C"  void SA_iTween_DrawPathHandles_m3215319446 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___path0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::CameraFadeDepth(System.Int32)
extern "C"  void SA_iTween_CameraFadeDepth_m4089899340 (Il2CppObject * __this /* static, unused */, int32_t ___depth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::CameraFadeDestroy()
extern "C"  void SA_iTween_CameraFadeDestroy_m2628821638 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::CameraFadeSwap(UnityEngine.Texture2D)
extern "C"  void SA_iTween_CameraFadeSwap_m1768679507 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject SA.Common.Animation.SA_iTween::CameraFadeAdd(UnityEngine.Texture2D,System.Int32)
extern "C"  GameObject_t1756533147 * SA_iTween_CameraFadeAdd_m1850939625 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, int32_t ___depth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject SA.Common.Animation.SA_iTween::CameraFadeAdd(UnityEngine.Texture2D)
extern "C"  GameObject_t1756533147 * SA_iTween_CameraFadeAdd_m3695101744 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject SA.Common.Animation.SA_iTween::CameraFadeAdd()
extern "C"  GameObject_t1756533147 * SA_iTween_CameraFadeAdd_m2279921726 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Resume(UnityEngine.GameObject)
extern "C"  void SA_iTween_Resume_m1710796166 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Resume(UnityEngine.GameObject,System.Boolean)
extern "C"  void SA_iTween_Resume_m757895141 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, bool ___includechildren1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Resume(UnityEngine.GameObject,System.String)
extern "C"  void SA_iTween_Resume_m898266650 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Resume(UnityEngine.GameObject,System.String,System.Boolean)
extern "C"  void SA_iTween_Resume_m2108762715 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, String_t* ___type1, bool ___includechildren2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Resume()
extern "C"  void SA_iTween_Resume_m3709470416 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Resume(System.String)
extern "C"  void SA_iTween_Resume_m141154306 (Il2CppObject * __this /* static, unused */, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Pause(UnityEngine.GameObject)
extern "C"  void SA_iTween_Pause_m3443159121 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Pause(UnityEngine.GameObject,System.Boolean)
extern "C"  void SA_iTween_Pause_m2144867894 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, bool ___includechildren1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Pause(UnityEngine.GameObject,System.String)
extern "C"  void SA_iTween_Pause_m587340903 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Pause(UnityEngine.GameObject,System.String,System.Boolean)
extern "C"  void SA_iTween_Pause_m4196587810 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, String_t* ___type1, bool ___includechildren2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Pause()
extern "C"  void SA_iTween_Pause_m3609487741 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Pause(System.String)
extern "C"  void SA_iTween_Pause_m3664647279 (Il2CppObject * __this /* static, unused */, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SA.Common.Animation.SA_iTween::Count()
extern "C"  int32_t SA_iTween_Count_m2195169550 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SA.Common.Animation.SA_iTween::Count(System.String)
extern "C"  int32_t SA_iTween_Count_m1294807940 (Il2CppObject * __this /* static, unused */, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SA.Common.Animation.SA_iTween::Count(UnityEngine.GameObject)
extern "C"  int32_t SA_iTween_Count_m3602746032 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SA.Common.Animation.SA_iTween::Count(UnityEngine.GameObject,System.String)
extern "C"  int32_t SA_iTween_Count_m14414492 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Stop()
extern "C"  void SA_iTween_Stop_m1921775247 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Stop(System.String)
extern "C"  void SA_iTween_Stop_m1457109537 (Il2CppObject * __this /* static, unused */, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::StopByName(System.String)
extern "C"  void SA_iTween_StopByName_m2485620249 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Stop(UnityEngine.GameObject)
extern "C"  void SA_iTween_Stop_m1323456935 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Stop(UnityEngine.GameObject,System.Boolean)
extern "C"  void SA_iTween_Stop_m133554404 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, bool ___includechildren1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Stop(UnityEngine.GameObject,System.String)
extern "C"  void SA_iTween_Stop_m3973129785 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::StopByName(UnityEngine.GameObject,System.String)
extern "C"  void SA_iTween_StopByName_m387976241 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Stop(UnityEngine.GameObject,System.String,System.Boolean)
extern "C"  void SA_iTween_Stop_m1934066972 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, String_t* ___type1, bool ___includechildren2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::StopByName(UnityEngine.GameObject,System.String,System.Boolean)
extern "C"  void SA_iTween_StopByName_m797302642 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, String_t* ___name1, bool ___includechildren2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable SA.Common.Animation.SA_iTween::Hash(System.Object[])
extern "C"  Hashtable_t909839986 * SA_iTween_Hash_m880650498 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Awake()
extern "C"  void SA_iTween_Awake_m691864096 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SA.Common.Animation.SA_iTween::Start()
extern "C"  Il2CppObject * SA_iTween_Start_m4151783257 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Update()
extern "C"  void SA_iTween_Update_m4284935010 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::FixedUpdate()
extern "C"  void SA_iTween_FixedUpdate_m2956088332 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::LateUpdate()
extern "C"  void SA_iTween_LateUpdate_m1341682046 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::OnEnable()
extern "C"  void SA_iTween_OnEnable_m298039051 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::OnDisable()
extern "C"  void SA_iTween_OnDisable_m3111240516 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawLineHelper(UnityEngine.Vector3[],UnityEngine.Color,System.String)
extern "C"  void SA_iTween_DrawLineHelper_m3860548654 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___line0, Color_t2020392075  ___color1, String_t* ___method2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DrawPathHelper(UnityEngine.Vector3[],UnityEngine.Color,System.String)
extern "C"  void SA_iTween_DrawPathHelper_m4042239827 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___path0, Color_t2020392075  ___color1, String_t* ___method2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] SA.Common.Animation.SA_iTween::PathControlPointGenerator(UnityEngine.Vector3[])
extern "C"  Vector3U5BU5D_t1172311765* SA_iTween_PathControlPointGenerator_m1513832785 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SA.Common.Animation.SA_iTween::Interp(UnityEngine.Vector3[],System.Single)
extern "C"  Vector3_t2243707580  SA_iTween_Interp_m2776040315 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___pts0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Launch(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void SA_iTween_Launch_m2132934016 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___target0, Hashtable_t909839986 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable SA.Common.Animation.SA_iTween::CleanArgs(System.Collections.Hashtable)
extern "C"  Hashtable_t909839986 * SA_iTween_CleanArgs_m2634697084 (Il2CppObject * __this /* static, unused */, Hashtable_t909839986 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA.Common.Animation.SA_iTween::GenerateID()
extern "C"  String_t* SA_iTween_GenerateID_m346958322 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::RetrieveArgs()
extern "C"  void SA_iTween_RetrieveArgs_m2028537266 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::GetEasingFunction()
extern "C"  void SA_iTween_GetEasingFunction_m3629542522 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::UpdatePercentage()
extern "C"  void SA_iTween_UpdatePercentage_m1452102754 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::CallBack(System.String)
extern "C"  void SA_iTween_CallBack_m82463742 (SA_iTween_t1938847631 * __this, String_t* ___callbackType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::Dispose()
extern "C"  void SA_iTween_Dispose_m1235158446 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ConflictCheck()
extern "C"  void SA_iTween_ConflictCheck_m659227455 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::EnableKinematic()
extern "C"  void SA_iTween_EnableKinematic_m1250462197 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::DisableKinematic()
extern "C"  void SA_iTween_DisableKinematic_m554767638 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Animation.SA_iTween::ResumeDelay()
extern "C"  void SA_iTween_ResumeDelay_m507010467 (SA_iTween_t1938847631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::linear(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_linear_m1851579029 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::clerp(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_clerp_m2199089758 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::spring(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_spring_m3996914705 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInQuad(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInQuad_m1212992980 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeOutQuad(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeOutQuad_m3165579493 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInOutQuad(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInOutQuad_m1667055454 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInCubic(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInCubic_m1586688399 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeOutCubic(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeOutCubic_m3804296886 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInOutCubic(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInOutCubic_m3549281893 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInQuart(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInQuart_m7561562 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeOutQuart(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeOutQuart_m966124759 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInOutQuart(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInOutQuart_m2974872184 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInQuint(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInQuint_m1224411198 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeOutQuint(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeOutQuint_m4222883739 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInOutQuint(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInOutQuint_m1775483772 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInSine(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInSine_m3686660898 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeOutSine(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeOutSine_m634710393 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInOutSine(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInOutSine_m2424277956 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInExpo(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInExpo_m2200379199 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeOutExpo(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeOutExpo_m1254755448 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInOutExpo(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInOutExpo_m2942290489 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInCirc(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInCirc_m1574016928 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeOutCirc(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeOutCirc_m1807989779 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInOutCirc(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInOutCirc_m4079750758 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInBounce(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInBounce_m1309315309 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeOutBounce(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeOutBounce_m365562936 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInOutBounce(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInOutBounce_m2144841571 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInBack(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInBack_m3819708368 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeOutBack(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeOutBack_m1994723715 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInOutBack(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInOutBack_m4258765014 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::punch(System.Single,System.Single)
extern "C"  float SA_iTween_punch_m1427798641 (SA_iTween_t1938847631 * __this, float ___amplitude0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInElastic(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInElastic_m510769706 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeOutElastic(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeOutElastic_m854497465 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SA.Common.Animation.SA_iTween::easeInOutElastic(System.Single,System.Single,System.Single)
extern "C"  float SA_iTween_easeInOutElastic_m2072700984 (SA_iTween_t1938847631 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
