﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlDocument
struct XmlDocument_t3649534162;

#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_XmlNo2035079622.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlDocumentWrapper
struct  XmlDocumentWrapper_t559857997  : public XmlNodeWrapper_t2035079622
{
public:
	// System.Xml.XmlDocument Newtonsoft.Json.Converters.XmlDocumentWrapper::_document
	XmlDocument_t3649534162 * ____document_1;

public:
	inline static int32_t get_offset_of__document_1() { return static_cast<int32_t>(offsetof(XmlDocumentWrapper_t559857997, ____document_1)); }
	inline XmlDocument_t3649534162 * get__document_1() const { return ____document_1; }
	inline XmlDocument_t3649534162 ** get_address_of__document_1() { return &____document_1; }
	inline void set__document_1(XmlDocument_t3649534162 * value)
	{
		____document_1 = value;
		Il2CppCodeGenWriteBarrier(&____document_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
