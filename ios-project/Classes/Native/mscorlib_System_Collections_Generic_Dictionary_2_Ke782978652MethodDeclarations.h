﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key91811072MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,AN_ActivityTemplate>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2943833481(__this, ___host0, method) ((  void (*) (Enumerator_t782978652 *, Dictionary_2_t2388442510 *, const MethodInfo*))Enumerator__ctor_m2975000652_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,AN_ActivityTemplate>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2987563924(__this, method) ((  Il2CppObject * (*) (Enumerator_t782978652 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1197115851_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,AN_ActivityTemplate>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m336905610(__this, method) ((  void (*) (Enumerator_t782978652 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1291229611_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,AN_ActivityTemplate>::Dispose()
#define Enumerator_Dispose_m3050194801(__this, method) ((  void (*) (Enumerator_t782978652 *, const MethodInfo*))Enumerator_Dispose_m3298895664_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,AN_ActivityTemplate>::MoveNext()
#define Enumerator_MoveNext_m3564653351(__this, method) ((  bool (*) (Enumerator_t782978652 *, const MethodInfo*))Enumerator_MoveNext_m1388125215_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,AN_ActivityTemplate>::get_Current()
#define Enumerator_get_Current_m3531324883(__this, method) ((  int32_t (*) (Enumerator_t782978652 *, const MethodInfo*))Enumerator_get_Current_m862874469_gshared)(__this, method)
