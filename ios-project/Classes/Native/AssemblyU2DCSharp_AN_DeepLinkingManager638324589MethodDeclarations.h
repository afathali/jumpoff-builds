﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_DeepLinkingManager
struct AN_DeepLinkingManager_t638324589;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_DeepLinkingManager::.ctor()
extern "C"  void AN_DeepLinkingManager__ctor_m58952676 (AN_DeepLinkingManager_t638324589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_DeepLinkingManager::add_OnDeepLinkReceived(System.Action`1<System.String>)
extern "C"  void AN_DeepLinkingManager_add_OnDeepLinkReceived_m3307488928 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_DeepLinkingManager::remove_OnDeepLinkReceived(System.Action`1<System.String>)
extern "C"  void AN_DeepLinkingManager_remove_OnDeepLinkReceived_m3068898559 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_DeepLinkingManager::GetLaunchDeepLinkId()
extern "C"  void AN_DeepLinkingManager_GetLaunchDeepLinkId_m549735980 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_DeepLinkingManager::DeepLinkReceived(System.String)
extern "C"  void AN_DeepLinkingManager_DeepLinkReceived_m4054199305 (AN_DeepLinkingManager_t638324589 * __this, String_t* ___linkId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
