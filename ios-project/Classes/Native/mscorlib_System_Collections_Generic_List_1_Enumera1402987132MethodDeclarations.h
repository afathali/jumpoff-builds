﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.RBTree/Node>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m332521321(__this, ___l0, method) ((  void (*) (Enumerator_t1402987132 *, List_1_t1868257458 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.RBTree/Node>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3284093145(__this, method) ((  void (*) (Enumerator_t1402987132 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.RBTree/Node>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2189400349(__this, method) ((  Il2CppObject * (*) (Enumerator_t1402987132 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.RBTree/Node>::Dispose()
#define Enumerator_Dispose_m4290904354(__this, method) ((  void (*) (Enumerator_t1402987132 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.RBTree/Node>::VerifyState()
#define Enumerator_VerifyState_m3148594019(__this, method) ((  void (*) (Enumerator_t1402987132 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.RBTree/Node>::MoveNext()
#define Enumerator_MoveNext_m1653949065(__this, method) ((  bool (*) (Enumerator_t1402987132 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.RBTree/Node>::get_Current()
#define Enumerator_get_Current_m736232306(__this, method) ((  Node_t2499136326 * (*) (Enumerator_t1402987132 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
