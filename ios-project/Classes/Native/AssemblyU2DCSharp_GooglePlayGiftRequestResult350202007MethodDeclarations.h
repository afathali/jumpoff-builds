﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGiftRequestResult
struct GooglePlayGiftRequestResult_t350202007;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GP_GamesActivityResultCodes70181657.h"

// System.Void GooglePlayGiftRequestResult::.ctor(System.String)
extern "C"  void GooglePlayGiftRequestResult__ctor_m384602130 (GooglePlayGiftRequestResult_t350202007 * __this, String_t* ___r_code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP_GamesActivityResultCodes GooglePlayGiftRequestResult::get_code()
extern "C"  int32_t GooglePlayGiftRequestResult_get_code_m1869200910 (GooglePlayGiftRequestResult_t350202007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGiftRequestResult::get_isSuccess()
extern "C"  bool GooglePlayGiftRequestResult_get_isSuccess_m4183927498 (GooglePlayGiftRequestResult_t350202007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGiftRequestResult::get_isFailure()
extern "C"  bool GooglePlayGiftRequestResult_get_isFailure_m3817143329 (GooglePlayGiftRequestResult_t350202007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
