﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>
struct ValueCollection_t59756212;
// System.Collections.Generic.Dictionary`2<AN_ManifestPermission,AN_PermissionState>
struct Dictionary_2_t1356696369;
// System.Collections.Generic.IEnumerator`1<AN_PermissionState>
struct IEnumerator_1_t2608692347;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// AN_PermissionState[]
struct AN_PermissionStateU5BU5D_t700971353;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AN_PermissionState838201224.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3043229133.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m4275294348_gshared (ValueCollection_t59756212 * __this, Dictionary_2_t1356696369 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m4275294348(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t59756212 *, Dictionary_2_t1356696369 *, const MethodInfo*))ValueCollection__ctor_m4275294348_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m829180566_gshared (ValueCollection_t59756212 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m829180566(__this, ___item0, method) ((  void (*) (ValueCollection_t59756212 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m829180566_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m611967095_gshared (ValueCollection_t59756212 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m611967095(__this, method) ((  void (*) (ValueCollection_t59756212 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m611967095_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3454008338_gshared (ValueCollection_t59756212 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3454008338(__this, ___item0, method) ((  bool (*) (ValueCollection_t59756212 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3454008338_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m488686055_gshared (ValueCollection_t59756212 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m488686055(__this, ___item0, method) ((  bool (*) (ValueCollection_t59756212 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m488686055_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3413318601_gshared (ValueCollection_t59756212 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3413318601(__this, method) ((  Il2CppObject* (*) (ValueCollection_t59756212 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3413318601_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2428068441_gshared (ValueCollection_t59756212 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2428068441(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t59756212 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2428068441_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1765461050_gshared (ValueCollection_t59756212 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1765461050(__this, method) ((  Il2CppObject * (*) (ValueCollection_t59756212 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1765461050_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1755191787_gshared (ValueCollection_t59756212 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1755191787(__this, method) ((  bool (*) (ValueCollection_t59756212 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1755191787_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1364733021_gshared (ValueCollection_t59756212 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1364733021(__this, method) ((  bool (*) (ValueCollection_t59756212 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1364733021_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m519940405_gshared (ValueCollection_t59756212 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m519940405(__this, method) ((  Il2CppObject * (*) (ValueCollection_t59756212 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m519940405_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3593585895_gshared (ValueCollection_t59756212 * __this, AN_PermissionStateU5BU5D_t700971353* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3593585895(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t59756212 *, AN_PermissionStateU5BU5D_t700971353*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3593585895_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::GetEnumerator()
extern "C"  Enumerator_t3043229133  ValueCollection_GetEnumerator_m2599920140_gshared (ValueCollection_t59756212 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2599920140(__this, method) ((  Enumerator_t3043229133  (*) (ValueCollection_t59756212 *, const MethodInfo*))ValueCollection_GetEnumerator_m2599920140_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<AN_ManifestPermission,AN_PermissionState>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3490722113_gshared (ValueCollection_t59756212 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3490722113(__this, method) ((  int32_t (*) (ValueCollection_t59756212 *, const MethodInfo*))ValueCollection_get_Count_m3490722113_gshared)(__this, method)
