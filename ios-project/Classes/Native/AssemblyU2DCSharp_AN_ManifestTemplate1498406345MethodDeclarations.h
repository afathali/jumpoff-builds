﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_ManifestTemplate
struct AN_ManifestTemplate_t1498406345;
// System.String
struct String_t;
// AN_PropertyTemplate
struct AN_PropertyTemplate_t2393149441;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// AN_ApplicationTemplate
struct AN_ApplicationTemplate_t3835038684;
// System.Collections.Generic.List`1<AN_PropertyTemplate>
struct List_1_t1762270573;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AN_PropertyTemplate2393149441.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void AN_ManifestTemplate::.ctor()
extern "C"  void AN_ManifestTemplate__ctor_m1023123694 (AN_ManifestTemplate_t1498406345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AN_ManifestTemplate::HasPermission(System.String)
extern "C"  bool AN_ManifestTemplate_HasPermission_m2414760291 (AN_ManifestTemplate_t1498406345 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ManifestTemplate::RemovePermission(System.String)
extern "C"  void AN_ManifestTemplate_RemovePermission_m1553435071 (AN_ManifestTemplate_t1498406345 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ManifestTemplate::RemovePermission(AN_PropertyTemplate)
extern "C"  void AN_ManifestTemplate_RemovePermission_m3937395150 (AN_ManifestTemplate_t1498406345 * __this, AN_PropertyTemplate_t2393149441 * ___permission0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ManifestTemplate::AddPermission(System.String)
extern "C"  void AN_ManifestTemplate_AddPermission_m3824980760 (AN_ManifestTemplate_t1498406345 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ManifestTemplate::AddPermission(AN_PropertyTemplate)
extern "C"  void AN_ManifestTemplate_AddPermission_m1414951705 (AN_ManifestTemplate_t1498406345 * __this, AN_PropertyTemplate_t2393149441 * ___permission0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_ManifestTemplate::ToXmlElement(System.Xml.XmlDocument,System.Xml.XmlElement)
extern "C"  void AN_ManifestTemplate_ToXmlElement_m2252386413 (AN_ManifestTemplate_t1498406345 * __this, XmlDocument_t3649534162 * ___doc0, XmlElement_t2877111883 * ___parent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AN_ApplicationTemplate AN_ManifestTemplate::get_ApplicationTemplate()
extern "C"  AN_ApplicationTemplate_t3835038684 * AN_ManifestTemplate_get_ApplicationTemplate_m3744464010 (AN_ManifestTemplate_t1498406345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<AN_PropertyTemplate> AN_ManifestTemplate::get_Permissions()
extern "C"  List_1_t1762270573 * AN_ManifestTemplate_get_Permissions_m2217372235 (AN_ManifestTemplate_t1498406345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
