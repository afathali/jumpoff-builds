﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "AssemblyU2DCSharp_Shared_Request3213492751.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegisterRequest
struct  RegisterRequest_t698226714  : public Request_t3213492751
{
public:
	// System.String RegisterRequest::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_2;
	// System.String RegisterRequest::<FullName>k__BackingField
	String_t* ___U3CFullNameU3Ek__BackingField_3;
	// System.String RegisterRequest::<Email>k__BackingField
	String_t* ___U3CEmailU3Ek__BackingField_4;
	// System.String RegisterRequest::<Password>k__BackingField
	String_t* ___U3CPasswordU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.Int32> RegisterRequest::<Zones>k__BackingField
	List_1_t1440998580 * ___U3CZonesU3Ek__BackingField_6;
	// System.Collections.Generic.List`1<System.String> RegisterRequest::<Disipline>k__BackingField
	List_1_t1398341365 * ___U3CDisiplineU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RegisterRequest_t698226714, ___U3CTypeU3Ek__BackingField_2)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTypeU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CFullNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RegisterRequest_t698226714, ___U3CFullNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CFullNameU3Ek__BackingField_3() const { return ___U3CFullNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CFullNameU3Ek__BackingField_3() { return &___U3CFullNameU3Ek__BackingField_3; }
	inline void set_U3CFullNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CFullNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFullNameU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CEmailU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RegisterRequest_t698226714, ___U3CEmailU3Ek__BackingField_4)); }
	inline String_t* get_U3CEmailU3Ek__BackingField_4() const { return ___U3CEmailU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CEmailU3Ek__BackingField_4() { return &___U3CEmailU3Ek__BackingField_4; }
	inline void set_U3CEmailU3Ek__BackingField_4(String_t* value)
	{
		___U3CEmailU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEmailU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CPasswordU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RegisterRequest_t698226714, ___U3CPasswordU3Ek__BackingField_5)); }
	inline String_t* get_U3CPasswordU3Ek__BackingField_5() const { return ___U3CPasswordU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CPasswordU3Ek__BackingField_5() { return &___U3CPasswordU3Ek__BackingField_5; }
	inline void set_U3CPasswordU3Ek__BackingField_5(String_t* value)
	{
		___U3CPasswordU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPasswordU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CZonesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RegisterRequest_t698226714, ___U3CZonesU3Ek__BackingField_6)); }
	inline List_1_t1440998580 * get_U3CZonesU3Ek__BackingField_6() const { return ___U3CZonesU3Ek__BackingField_6; }
	inline List_1_t1440998580 ** get_address_of_U3CZonesU3Ek__BackingField_6() { return &___U3CZonesU3Ek__BackingField_6; }
	inline void set_U3CZonesU3Ek__BackingField_6(List_1_t1440998580 * value)
	{
		___U3CZonesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CZonesU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CDisiplineU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RegisterRequest_t698226714, ___U3CDisiplineU3Ek__BackingField_7)); }
	inline List_1_t1398341365 * get_U3CDisiplineU3Ek__BackingField_7() const { return ___U3CDisiplineU3Ek__BackingField_7; }
	inline List_1_t1398341365 ** get_address_of_U3CDisiplineU3Ek__BackingField_7() { return &___U3CDisiplineU3Ek__BackingField_7; }
	inline void set_U3CDisiplineU3Ek__BackingField_7(List_1_t1398341365 * value)
	{
		___U3CDisiplineU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDisiplineU3Ek__BackingField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
