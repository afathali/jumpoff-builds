﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AN_PoupsProxy
struct AN_PoupsProxy_t2977617695;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AN_PoupsProxy::.ctor()
extern "C"  void AN_PoupsProxy__ctor_m2031673514 (AN_PoupsProxy_t2977617695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PoupsProxy::CallActivityFunction(System.String,System.Object[])
extern "C"  void AN_PoupsProxy_CallActivityFunction_m3180070345 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PoupsProxy::showDialog(System.String,System.String)
extern "C"  void AN_PoupsProxy_showDialog_m2456144209 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PoupsProxy::showDialog(System.String,System.String,System.String,System.String)
extern "C"  void AN_PoupsProxy_showDialog_m2911425809 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___yes2, String_t* ___no3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PoupsProxy::showMessage(System.String,System.String)
extern "C"  void AN_PoupsProxy_showMessage_m1405428576 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PoupsProxy::showMessage(System.String,System.String,System.String)
extern "C"  void AN_PoupsProxy_showMessage_m960506408 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___ok2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PoupsProxy::OpenAppRatePage(System.String)
extern "C"  void AN_PoupsProxy_OpenAppRatePage_m793343634 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PoupsProxy::showRateDialog(System.String,System.String,System.String,System.String,System.String)
extern "C"  void AN_PoupsProxy_showRateDialog_m72007901 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___yes2, String_t* ___laiter3, String_t* ___no4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PoupsProxy::ShowPreloader(System.String,System.String)
extern "C"  void AN_PoupsProxy_ShowPreloader_m1229013467 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PoupsProxy::HidePreloader()
extern "C"  void AN_PoupsProxy_HidePreloader_m2455961868 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AN_PoupsProxy::HideCurrentPopup()
extern "C"  void AN_PoupsProxy_HideCurrentPopup_m297791593 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
