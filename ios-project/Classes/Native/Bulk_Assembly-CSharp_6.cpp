﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// TwitterUserInfo
struct TwitterUserInfo_t87370740;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t3344795111;
// TwitterStatus
struct TwitterStatus_t2765035499;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// TWResult
struct TWResult_t1480791060;
// User
struct User_t719925459;
// Vector2Converter
struct Vector2Converter_t3051461781;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t1973729997;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t1719617802;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonReader
struct JsonReader_t3154730733;
// Vector3Converter
struct Vector3Converter_t3052510548;
// Vector4Converter
struct Vector4Converter_t3044633495;
// WWWTextureLoader
struct WWWTextureLoader_t155872171;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// WWWTextureLoader/<LoadCoroutin>c__IteratorC
struct U3CLoadCoroutinU3Ec__IteratorC_t1223036233;
// XScaleModifayer
struct XScaleModifayer_t1656594896;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_TwitterUserInfo87370740.h"
#include "AssemblyU2DCSharp_TwitterUserInfo87370740MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3344795111MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AssemblyU2DCSharp_ANMiniJSON_Json181042048MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "AssemblyU2DCSharp_TwitterStatus2765035499MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Action_1_gen3344795111.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_TwitterStatus2765035499.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "AssemblyU2DCSharp_WWWTextureLoader155872171MethodDeclarations.h"
#include "AssemblyU2DCSharp_WWWTextureLoader155872171.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Globalization_NumberStyles3408984435.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "AssemblyU2DCSharp_TWResult1480791060.h"
#include "AssemblyU2DCSharp_TWResult1480791060MethodDeclarations.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "AssemblyU2DCSharp_User719925459MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "AssemblyU2DCSharp_Vector2Converter3051461781.h"
#include "AssemblyU2DCSharp_Vector2Converter3051461781MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter1964060750MethodDeclarations.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter1973729997.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer1719617802.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter1973729997MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader3154730733.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "AssemblyU2DCSharp_Vector3Converter3052510548.h"
#include "AssemblyU2DCSharp_Vector3Converter3052510548MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_Vector4Converter3044633495.h"
#include "AssemblyU2DCSharp_Vector4Converter3044633495MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_WWWTextureLoader_U3CLoadCoroutin1223036233MethodDeclarations.h"
#include "AssemblyU2DCSharp_WWWTextureLoader_U3CLoadCoroutin1223036233.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_XScaleModifayer1656594896.h"
#include "AssemblyU2DCSharp_XScaleModifayer1656594896MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "AssemblyU2DCSharp_PreviewScreenUtil19016308MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m4099813343_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m4099813343(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m4099813343_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<WWWTextureLoader>()
#define GameObject_AddComponent_TisWWWTextureLoader_t155872171_m2812646813(__this, method) ((  WWWTextureLoader_t155872171 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m4099813343_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TwitterUserInfo::.ctor(System.String)
extern Il2CppClass* TwitterUserInfo_t87370740_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3344795111_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* TwitterStatus_t2765035499_il2cpp_TypeInfo_var;
extern const MethodInfo* TwitterUserInfo_U3CActionProfileImageLoadedU3Em__AD_m3003816570_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3899261817_MethodInfo_var;
extern const MethodInfo* TwitterUserInfo_U3CActionProfileBackgroundImageLoadedU3Em__AE_m3213365547_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral287061489;
extern Il2CppCodeGenString* _stringLiteral2328218955;
extern Il2CppCodeGenString* _stringLiteral3686446540;
extern Il2CppCodeGenString* _stringLiteral2812880832;
extern Il2CppCodeGenString* _stringLiteral1165419350;
extern Il2CppCodeGenString* _stringLiteral2323915341;
extern Il2CppCodeGenString* _stringLiteral1628651131;
extern Il2CppCodeGenString* _stringLiteral4271139811;
extern Il2CppCodeGenString* _stringLiteral4272488836;
extern Il2CppCodeGenString* _stringLiteral3225320706;
extern Il2CppCodeGenString* _stringLiteral1022718173;
extern Il2CppCodeGenString* _stringLiteral2567023836;
extern Il2CppCodeGenString* _stringLiteral4134113675;
extern Il2CppCodeGenString* _stringLiteral1403120956;
extern Il2CppCodeGenString* _stringLiteral1959740304;
extern const uint32_t TwitterUserInfo__ctor_m1842084383_MetadataUsageId;
extern "C"  void TwitterUserInfo__ctor_m1842084383 (TwitterUserInfo_t87370740 * __this, String_t* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TwitterUserInfo__ctor_m1842084383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	TwitterUserInfo_t87370740 * G_B2_0 = NULL;
	TwitterUserInfo_t87370740 * G_B1_0 = NULL;
	TwitterUserInfo_t87370740 * G_B4_0 = NULL;
	TwitterUserInfo_t87370740 * G_B3_0 = NULL;
	{
		Color_t2020392075  L_0 = Color_get_clear_m1469108305(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__profile_background_color_13(L_0);
		Color_t2020392075  L_1 = Color_get_clear_m1469108305(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__profile_text_color_14(L_1);
		Action_1_t3344795111 * L_2 = ((TwitterUserInfo_t87370740_StaticFields*)TwitterUserInfo_t87370740_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache14_20();
		G_B1_0 = __this;
		if (L_2)
		{
			G_B2_0 = __this;
			goto IL_002f;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)TwitterUserInfo_U3CActionProfileImageLoadedU3Em__AD_m3003816570_MethodInfo_var);
		Action_1_t3344795111 * L_4 = (Action_1_t3344795111 *)il2cpp_codegen_object_new(Action_1_t3344795111_il2cpp_TypeInfo_var);
		Action_1__ctor_m3899261817(L_4, NULL, L_3, /*hidden argument*/Action_1__ctor_m3899261817_MethodInfo_var);
		((TwitterUserInfo_t87370740_StaticFields*)TwitterUserInfo_t87370740_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache14_20(L_4);
		G_B2_0 = G_B1_0;
	}

IL_002f:
	{
		Action_1_t3344795111 * L_5 = ((TwitterUserInfo_t87370740_StaticFields*)TwitterUserInfo_t87370740_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache14_20();
		NullCheck(G_B2_0);
		G_B2_0->set_ActionProfileImageLoaded_18(L_5);
		Action_1_t3344795111 * L_6 = ((TwitterUserInfo_t87370740_StaticFields*)TwitterUserInfo_t87370740_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache15_21();
		G_B3_0 = __this;
		if (L_6)
		{
			G_B4_0 = __this;
			goto IL_0052;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)TwitterUserInfo_U3CActionProfileBackgroundImageLoadedU3Em__AE_m3213365547_MethodInfo_var);
		Action_1_t3344795111 * L_8 = (Action_1_t3344795111 *)il2cpp_codegen_object_new(Action_1_t3344795111_il2cpp_TypeInfo_var);
		Action_1__ctor_m3899261817(L_8, NULL, L_7, /*hidden argument*/Action_1__ctor_m3899261817_MethodInfo_var);
		((TwitterUserInfo_t87370740_StaticFields*)TwitterUserInfo_t87370740_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache15_21(L_8);
		G_B4_0 = G_B3_0;
	}

IL_0052:
	{
		Action_1_t3344795111 * L_9 = ((TwitterUserInfo_t87370740_StaticFields*)TwitterUserInfo_t87370740_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache15_21();
		NullCheck(G_B4_0);
		G_B4_0->set_ActionProfileBackgroundImageLoaded_19(L_9);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_10 = ___data0;
		__this->set__rawJSON_6(L_10);
		String_t* L_11 = __this->get__rawJSON_6();
		Il2CppObject * L_12 = Json_Deserialize_m989925873(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_0 = ((Il2CppObject *)IsInst(L_12, IDictionary_t596158605_il2cpp_TypeInfo_var));
		Il2CppObject * L_13 = V_0;
		NullCheck(L_13);
		Il2CppObject * L_14 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_13, _stringLiteral287061489);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_15 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		__this->set__id_0(L_15);
		Il2CppObject * L_16 = V_0;
		NullCheck(L_16);
		Il2CppObject * L_17 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_16, _stringLiteral2328218955);
		String_t* L_18 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		__this->set__name_2(L_18);
		Il2CppObject * L_19 = V_0;
		NullCheck(L_19);
		Il2CppObject * L_20 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_19, _stringLiteral3686446540);
		String_t* L_21 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		__this->set__description_1(L_21);
		Il2CppObject * L_22 = V_0;
		NullCheck(L_22);
		Il2CppObject * L_23 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_22, _stringLiteral2812880832);
		String_t* L_24 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		__this->set__screen_name_3(L_24);
		Il2CppObject * L_25 = V_0;
		NullCheck(L_25);
		Il2CppObject * L_26 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_25, _stringLiteral1165419350);
		String_t* L_27 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		__this->set__lang_5(L_27);
		Il2CppObject * L_28 = V_0;
		NullCheck(L_28);
		Il2CppObject * L_29 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_28, _stringLiteral2323915341);
		String_t* L_30 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		__this->set__location_4(L_30);
		Il2CppObject * L_31 = V_0;
		NullCheck(L_31);
		Il2CppObject * L_32 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_31, _stringLiteral1628651131);
		String_t* L_33 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		__this->set__profile_image_url_7(L_33);
		Il2CppObject * L_34 = V_0;
		NullCheck(L_34);
		Il2CppObject * L_35 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_34, _stringLiteral4271139811);
		String_t* L_36 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		__this->set__profile_image_url_https_8(L_36);
		Il2CppObject * L_37 = V_0;
		NullCheck(L_37);
		Il2CppObject * L_38 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_37, _stringLiteral4272488836);
		String_t* L_39 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		__this->set__profile_background_image_url_9(L_39);
		Il2CppObject * L_40 = V_0;
		NullCheck(L_40);
		Il2CppObject * L_41 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_40, _stringLiteral3225320706);
		String_t* L_42 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		__this->set__profile_background_image_url_https_10(L_42);
		Il2CppObject * L_43 = V_0;
		NullCheck(L_43);
		Il2CppObject * L_44 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_43, _stringLiteral1022718173);
		int32_t L_45 = Convert_ToInt32_m366159805(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		__this->set__friends_count_15(L_45);
		Il2CppObject * L_46 = V_0;
		NullCheck(L_46);
		Il2CppObject * L_47 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_46, _stringLiteral2567023836);
		int32_t L_48 = Convert_ToInt32_m366159805(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		__this->set__statuses_count_16(L_48);
		Il2CppObject * L_49 = V_0;
		NullCheck(L_49);
		Il2CppObject * L_50 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_49, _stringLiteral4134113675);
		String_t* L_51 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Color_t2020392075  L_52 = TwitterUserInfo_HexToColor_m538248085(__this, L_51, /*hidden argument*/NULL);
		__this->set__profile_text_color_14(L_52);
		Il2CppObject * L_53 = V_0;
		NullCheck(L_53);
		Il2CppObject * L_54 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_53, _stringLiteral1403120956);
		String_t* L_55 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		Color_t2020392075  L_56 = TwitterUserInfo_HexToColor_m538248085(__this, L_55, /*hidden argument*/NULL);
		__this->set__profile_background_color_13(L_56);
		Il2CppObject * L_57 = V_0;
		NullCheck(L_57);
		Il2CppObject * L_58 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_57, _stringLiteral1959740304);
		TwitterStatus_t2765035499 * L_59 = (TwitterStatus_t2765035499 *)il2cpp_codegen_object_new(TwitterStatus_t2765035499_il2cpp_TypeInfo_var);
		TwitterStatus__ctor_m1609543363(L_59, ((Il2CppObject *)IsInst(L_58, IDictionary_t596158605_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		__this->set__status_17(L_59);
		return;
	}
}
// System.Void TwitterUserInfo::.ctor(System.Collections.IDictionary)
extern Il2CppClass* TwitterUserInfo_t87370740_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3344795111_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const MethodInfo* TwitterUserInfo_U3CActionProfileImageLoadedU3Em__AD_m3003816570_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3899261817_MethodInfo_var;
extern const MethodInfo* TwitterUserInfo_U3CActionProfileBackgroundImageLoadedU3Em__AE_m3213365547_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral287061489;
extern Il2CppCodeGenString* _stringLiteral2328218955;
extern Il2CppCodeGenString* _stringLiteral3686446540;
extern Il2CppCodeGenString* _stringLiteral2812880832;
extern Il2CppCodeGenString* _stringLiteral1165419350;
extern Il2CppCodeGenString* _stringLiteral2323915341;
extern Il2CppCodeGenString* _stringLiteral1628651131;
extern Il2CppCodeGenString* _stringLiteral4271139811;
extern Il2CppCodeGenString* _stringLiteral4272488836;
extern Il2CppCodeGenString* _stringLiteral3225320706;
extern Il2CppCodeGenString* _stringLiteral1022718173;
extern Il2CppCodeGenString* _stringLiteral2567023836;
extern Il2CppCodeGenString* _stringLiteral4134113675;
extern Il2CppCodeGenString* _stringLiteral1403120956;
extern const uint32_t TwitterUserInfo__ctor_m2734525946_MetadataUsageId;
extern "C"  void TwitterUserInfo__ctor_m2734525946 (TwitterUserInfo_t87370740 * __this, Il2CppObject * ___JSON0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TwitterUserInfo__ctor_m2734525946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TwitterUserInfo_t87370740 * G_B2_0 = NULL;
	TwitterUserInfo_t87370740 * G_B1_0 = NULL;
	TwitterUserInfo_t87370740 * G_B4_0 = NULL;
	TwitterUserInfo_t87370740 * G_B3_0 = NULL;
	{
		Color_t2020392075  L_0 = Color_get_clear_m1469108305(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__profile_background_color_13(L_0);
		Color_t2020392075  L_1 = Color_get_clear_m1469108305(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__profile_text_color_14(L_1);
		Action_1_t3344795111 * L_2 = ((TwitterUserInfo_t87370740_StaticFields*)TwitterUserInfo_t87370740_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache14_20();
		G_B1_0 = __this;
		if (L_2)
		{
			G_B2_0 = __this;
			goto IL_002f;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)TwitterUserInfo_U3CActionProfileImageLoadedU3Em__AD_m3003816570_MethodInfo_var);
		Action_1_t3344795111 * L_4 = (Action_1_t3344795111 *)il2cpp_codegen_object_new(Action_1_t3344795111_il2cpp_TypeInfo_var);
		Action_1__ctor_m3899261817(L_4, NULL, L_3, /*hidden argument*/Action_1__ctor_m3899261817_MethodInfo_var);
		((TwitterUserInfo_t87370740_StaticFields*)TwitterUserInfo_t87370740_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache14_20(L_4);
		G_B2_0 = G_B1_0;
	}

IL_002f:
	{
		Action_1_t3344795111 * L_5 = ((TwitterUserInfo_t87370740_StaticFields*)TwitterUserInfo_t87370740_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache14_20();
		NullCheck(G_B2_0);
		G_B2_0->set_ActionProfileImageLoaded_18(L_5);
		Action_1_t3344795111 * L_6 = ((TwitterUserInfo_t87370740_StaticFields*)TwitterUserInfo_t87370740_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache15_21();
		G_B3_0 = __this;
		if (L_6)
		{
			G_B4_0 = __this;
			goto IL_0052;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)TwitterUserInfo_U3CActionProfileBackgroundImageLoadedU3Em__AE_m3213365547_MethodInfo_var);
		Action_1_t3344795111 * L_8 = (Action_1_t3344795111 *)il2cpp_codegen_object_new(Action_1_t3344795111_il2cpp_TypeInfo_var);
		Action_1__ctor_m3899261817(L_8, NULL, L_7, /*hidden argument*/Action_1__ctor_m3899261817_MethodInfo_var);
		((TwitterUserInfo_t87370740_StaticFields*)TwitterUserInfo_t87370740_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache15_21(L_8);
		G_B4_0 = G_B3_0;
	}

IL_0052:
	{
		Action_1_t3344795111 * L_9 = ((TwitterUserInfo_t87370740_StaticFields*)TwitterUserInfo_t87370740_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache15_21();
		NullCheck(G_B4_0);
		G_B4_0->set_ActionProfileBackgroundImageLoaded_19(L_9);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Il2CppObject * L_10 = ___JSON0;
		NullCheck(L_10);
		Il2CppObject * L_11 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_10, _stringLiteral287061489);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_12 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		__this->set__id_0(L_12);
		Il2CppObject * L_13 = ___JSON0;
		NullCheck(L_13);
		Il2CppObject * L_14 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_13, _stringLiteral2328218955);
		String_t* L_15 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		__this->set__name_2(L_15);
		Il2CppObject * L_16 = ___JSON0;
		NullCheck(L_16);
		Il2CppObject * L_17 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_16, _stringLiteral3686446540);
		String_t* L_18 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		__this->set__description_1(L_18);
		Il2CppObject * L_19 = ___JSON0;
		NullCheck(L_19);
		Il2CppObject * L_20 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_19, _stringLiteral2812880832);
		String_t* L_21 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		__this->set__screen_name_3(L_21);
		Il2CppObject * L_22 = ___JSON0;
		NullCheck(L_22);
		Il2CppObject * L_23 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_22, _stringLiteral1165419350);
		String_t* L_24 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		__this->set__lang_5(L_24);
		Il2CppObject * L_25 = ___JSON0;
		NullCheck(L_25);
		Il2CppObject * L_26 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_25, _stringLiteral2323915341);
		String_t* L_27 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		__this->set__location_4(L_27);
		Il2CppObject * L_28 = ___JSON0;
		NullCheck(L_28);
		Il2CppObject * L_29 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_28, _stringLiteral1628651131);
		String_t* L_30 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		__this->set__profile_image_url_7(L_30);
		Il2CppObject * L_31 = ___JSON0;
		NullCheck(L_31);
		Il2CppObject * L_32 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_31, _stringLiteral4271139811);
		String_t* L_33 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		__this->set__profile_image_url_https_8(L_33);
		Il2CppObject * L_34 = ___JSON0;
		NullCheck(L_34);
		Il2CppObject * L_35 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_34, _stringLiteral4272488836);
		String_t* L_36 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		__this->set__profile_background_image_url_9(L_36);
		Il2CppObject * L_37 = ___JSON0;
		NullCheck(L_37);
		Il2CppObject * L_38 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_37, _stringLiteral3225320706);
		String_t* L_39 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		__this->set__profile_background_image_url_https_10(L_39);
		Il2CppObject * L_40 = ___JSON0;
		NullCheck(L_40);
		Il2CppObject * L_41 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_40, _stringLiteral1022718173);
		int32_t L_42 = Convert_ToInt32_m366159805(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		__this->set__friends_count_15(L_42);
		Il2CppObject * L_43 = ___JSON0;
		NullCheck(L_43);
		Il2CppObject * L_44 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_43, _stringLiteral2567023836);
		int32_t L_45 = Convert_ToInt32_m366159805(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		__this->set__statuses_count_16(L_45);
		Il2CppObject * L_46 = ___JSON0;
		NullCheck(L_46);
		Il2CppObject * L_47 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_46, _stringLiteral4134113675);
		String_t* L_48 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		Color_t2020392075  L_49 = TwitterUserInfo_HexToColor_m538248085(__this, L_48, /*hidden argument*/NULL);
		__this->set__profile_text_color_14(L_49);
		Il2CppObject * L_50 = ___JSON0;
		NullCheck(L_50);
		Il2CppObject * L_51 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_50, _stringLiteral1403120956);
		String_t* L_52 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Color_t2020392075  L_53 = TwitterUserInfo_HexToColor_m538248085(__this, L_52, /*hidden argument*/NULL);
		__this->set__profile_background_color_13(L_53);
		return;
	}
}
// System.Void TwitterUserInfo::add_ActionProfileImageLoaded(System.Action`1<UnityEngine.Texture2D>)
extern Il2CppClass* Action_1_t3344795111_il2cpp_TypeInfo_var;
extern const uint32_t TwitterUserInfo_add_ActionProfileImageLoaded_m3050009664_MetadataUsageId;
extern "C"  void TwitterUserInfo_add_ActionProfileImageLoaded_m3050009664 (TwitterUserInfo_t87370740 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TwitterUserInfo_add_ActionProfileImageLoaded_m3050009664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3344795111 * L_0 = __this->get_ActionProfileImageLoaded_18();
		Action_1_t3344795111 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ActionProfileImageLoaded_18(((Action_1_t3344795111 *)CastclassSealed(L_2, Action_1_t3344795111_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void TwitterUserInfo::remove_ActionProfileImageLoaded(System.Action`1<UnityEngine.Texture2D>)
extern Il2CppClass* Action_1_t3344795111_il2cpp_TypeInfo_var;
extern const uint32_t TwitterUserInfo_remove_ActionProfileImageLoaded_m1769448413_MetadataUsageId;
extern "C"  void TwitterUserInfo_remove_ActionProfileImageLoaded_m1769448413 (TwitterUserInfo_t87370740 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TwitterUserInfo_remove_ActionProfileImageLoaded_m1769448413_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3344795111 * L_0 = __this->get_ActionProfileImageLoaded_18();
		Action_1_t3344795111 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ActionProfileImageLoaded_18(((Action_1_t3344795111 *)CastclassSealed(L_2, Action_1_t3344795111_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void TwitterUserInfo::add_ActionProfileBackgroundImageLoaded(System.Action`1<UnityEngine.Texture2D>)
extern Il2CppClass* Action_1_t3344795111_il2cpp_TypeInfo_var;
extern const uint32_t TwitterUserInfo_add_ActionProfileBackgroundImageLoaded_m2026856352_MetadataUsageId;
extern "C"  void TwitterUserInfo_add_ActionProfileBackgroundImageLoaded_m2026856352 (TwitterUserInfo_t87370740 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TwitterUserInfo_add_ActionProfileBackgroundImageLoaded_m2026856352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3344795111 * L_0 = __this->get_ActionProfileBackgroundImageLoaded_19();
		Action_1_t3344795111 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ActionProfileBackgroundImageLoaded_19(((Action_1_t3344795111 *)CastclassSealed(L_2, Action_1_t3344795111_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void TwitterUserInfo::remove_ActionProfileBackgroundImageLoaded(System.Action`1<UnityEngine.Texture2D>)
extern Il2CppClass* Action_1_t3344795111_il2cpp_TypeInfo_var;
extern const uint32_t TwitterUserInfo_remove_ActionProfileBackgroundImageLoaded_m270843863_MetadataUsageId;
extern "C"  void TwitterUserInfo_remove_ActionProfileBackgroundImageLoaded_m270843863 (TwitterUserInfo_t87370740 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TwitterUserInfo_remove_ActionProfileBackgroundImageLoaded_m270843863_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3344795111 * L_0 = __this->get_ActionProfileBackgroundImageLoaded_19();
		Action_1_t3344795111 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ActionProfileBackgroundImageLoaded_19(((Action_1_t3344795111 *)CastclassSealed(L_2, Action_1_t3344795111_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void TwitterUserInfo::LoadProfileImage()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3344795111_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m4165075220_MethodInfo_var;
extern const MethodInfo* TwitterUserInfo_OnProfileImageLoaded_m4232127673_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3899261817_MethodInfo_var;
extern const uint32_t TwitterUserInfo_LoadProfileImage_m1762410403_MetadataUsageId;
extern "C"  void TwitterUserInfo_LoadProfileImage_m1762410403 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TwitterUserInfo_LoadProfileImage_m1762410403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WWWTextureLoader_t155872171 * V_0 = NULL;
	{
		Texture2D_t3542995729 * L_0 = __this->get__profile_image_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		Action_1_t3344795111 * L_2 = __this->get_ActionProfileImageLoaded_18();
		Texture2D_t3542995729 * L_3 = __this->get__profile_image_11();
		NullCheck(L_2);
		Action_1_Invoke_m4165075220(L_2, L_3, /*hidden argument*/Action_1_Invoke_m4165075220_MethodInfo_var);
		return;
	}

IL_0023:
	{
		WWWTextureLoader_t155872171 * L_4 = WWWTextureLoader_Create_m2845297014(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_4;
		WWWTextureLoader_t155872171 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)TwitterUserInfo_OnProfileImageLoaded_m4232127673_MethodInfo_var);
		Action_1_t3344795111 * L_7 = (Action_1_t3344795111 *)il2cpp_codegen_object_new(Action_1_t3344795111_il2cpp_TypeInfo_var);
		Action_1__ctor_m3899261817(L_7, __this, L_6, /*hidden argument*/Action_1__ctor_m3899261817_MethodInfo_var);
		NullCheck(L_5);
		WWWTextureLoader_add_OnLoad_m1127950879(L_5, L_7, /*hidden argument*/NULL);
		WWWTextureLoader_t155872171 * L_8 = V_0;
		String_t* L_9 = __this->get__profile_image_url_https_8();
		NullCheck(L_8);
		WWWTextureLoader_LoadTexture_m783656337(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwitterUserInfo::LoadBackgroundImage()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3344795111_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m4165075220_MethodInfo_var;
extern const MethodInfo* TwitterUserInfo_OnProfileBackgroundLoaded_m303523722_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3899261817_MethodInfo_var;
extern const uint32_t TwitterUserInfo_LoadBackgroundImage_m2500582438_MetadataUsageId;
extern "C"  void TwitterUserInfo_LoadBackgroundImage_m2500582438 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TwitterUserInfo_LoadBackgroundImage_m2500582438_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WWWTextureLoader_t155872171 * V_0 = NULL;
	{
		Texture2D_t3542995729 * L_0 = __this->get__profile_background_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		Action_1_t3344795111 * L_2 = __this->get_ActionProfileBackgroundImageLoaded_19();
		Texture2D_t3542995729 * L_3 = __this->get__profile_background_12();
		NullCheck(L_2);
		Action_1_Invoke_m4165075220(L_2, L_3, /*hidden argument*/Action_1_Invoke_m4165075220_MethodInfo_var);
		return;
	}

IL_0023:
	{
		WWWTextureLoader_t155872171 * L_4 = WWWTextureLoader_Create_m2845297014(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_4;
		WWWTextureLoader_t155872171 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)TwitterUserInfo_OnProfileBackgroundLoaded_m303523722_MethodInfo_var);
		Action_1_t3344795111 * L_7 = (Action_1_t3344795111 *)il2cpp_codegen_object_new(Action_1_t3344795111_il2cpp_TypeInfo_var);
		Action_1__ctor_m3899261817(L_7, __this, L_6, /*hidden argument*/Action_1__ctor_m3899261817_MethodInfo_var);
		NullCheck(L_5);
		WWWTextureLoader_add_OnLoad_m1127950879(L_5, L_7, /*hidden argument*/NULL);
		WWWTextureLoader_t155872171 * L_8 = V_0;
		String_t* L_9 = __this->get__profile_background_image_url_https_10();
		NullCheck(L_8);
		WWWTextureLoader_LoadTexture_m783656337(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.String TwitterUserInfo::get_rawJSON()
extern "C"  String_t* TwitterUserInfo_get_rawJSON_m3069431681 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__rawJSON_6();
		return L_0;
	}
}
// System.String TwitterUserInfo::get_id()
extern "C"  String_t* TwitterUserInfo_get_id_m3137512336 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__id_0();
		return L_0;
	}
}
// System.String TwitterUserInfo::get_name()
extern "C"  String_t* TwitterUserInfo_get_name_m1911747022 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__name_2();
		return L_0;
	}
}
// System.String TwitterUserInfo::get_description()
extern "C"  String_t* TwitterUserInfo_get_description_m969481391 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__description_1();
		return L_0;
	}
}
// System.String TwitterUserInfo::get_screen_name()
extern "C"  String_t* TwitterUserInfo_get_screen_name_m4136318513 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__screen_name_3();
		return L_0;
	}
}
// System.String TwitterUserInfo::get_location()
extern "C"  String_t* TwitterUserInfo_get_location_m1042764134 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__location_4();
		return L_0;
	}
}
// System.String TwitterUserInfo::get_lang()
extern "C"  String_t* TwitterUserInfo_get_lang_m536462993 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__lang_5();
		return L_0;
	}
}
// System.String TwitterUserInfo::get_profile_image_url()
extern "C"  String_t* TwitterUserInfo_get_profile_image_url_m1660135716 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__profile_image_url_7();
		return L_0;
	}
}
// System.String TwitterUserInfo::get_profile_image_url_https()
extern "C"  String_t* TwitterUserInfo_get_profile_image_url_https_m887630722 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__profile_image_url_https_8();
		return L_0;
	}
}
// System.String TwitterUserInfo::get_profile_background_image_url()
extern "C"  String_t* TwitterUserInfo_get_profile_background_image_url_m490475547 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__profile_background_image_url_9();
		return L_0;
	}
}
// System.String TwitterUserInfo::get_profile_background_image_url_https()
extern "C"  String_t* TwitterUserInfo_get_profile_background_image_url_https_m1640120291 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__profile_background_image_url_https_10();
		return L_0;
	}
}
// System.Int32 TwitterUserInfo::get_friends_count()
extern "C"  int32_t TwitterUserInfo_get_friends_count_m1473011967 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__friends_count_15();
		return L_0;
	}
}
// System.Int32 TwitterUserInfo::get_statuses_count()
extern "C"  int32_t TwitterUserInfo_get_statuses_count_m919889122 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__statuses_count_16();
		return L_0;
	}
}
// TwitterStatus TwitterUserInfo::get_status()
extern "C"  TwitterStatus_t2765035499 * TwitterUserInfo_get_status_m2930417650 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		TwitterStatus_t2765035499 * L_0 = __this->get__status_17();
		return L_0;
	}
}
// UnityEngine.Texture2D TwitterUserInfo::get_profile_image()
extern "C"  Texture2D_t3542995729 * TwitterUserInfo_get_profile_image_m3292256762 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		Texture2D_t3542995729 * L_0 = __this->get__profile_image_11();
		return L_0;
	}
}
// UnityEngine.Texture2D TwitterUserInfo::get_profile_background()
extern "C"  Texture2D_t3542995729 * TwitterUserInfo_get_profile_background_m835540159 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		Texture2D_t3542995729 * L_0 = __this->get__profile_background_12();
		return L_0;
	}
}
// UnityEngine.Color TwitterUserInfo::get_profile_background_color()
extern "C"  Color_t2020392075  TwitterUserInfo_get_profile_background_color_m935814061 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = __this->get__profile_background_color_13();
		return L_0;
	}
}
// UnityEngine.Color TwitterUserInfo::get_profile_text_color()
extern "C"  Color_t2020392075  TwitterUserInfo_get_profile_text_color_m4138052686 (TwitterUserInfo_t87370740 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = __this->get__profile_text_color_14();
		return L_0;
	}
}
// System.Void TwitterUserInfo::OnProfileImageLoaded(UnityEngine.Texture2D)
extern const MethodInfo* Action_1_Invoke_m4165075220_MethodInfo_var;
extern const uint32_t TwitterUserInfo_OnProfileImageLoaded_m4232127673_MetadataUsageId;
extern "C"  void TwitterUserInfo_OnProfileImageLoaded_m4232127673 (TwitterUserInfo_t87370740 * __this, Texture2D_t3542995729 * ___img0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TwitterUserInfo_OnProfileImageLoaded_m4232127673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture2D_t3542995729 * L_0 = ___img0;
		__this->set__profile_image_11(L_0);
		Action_1_t3344795111 * L_1 = __this->get_ActionProfileImageLoaded_18();
		Texture2D_t3542995729 * L_2 = __this->get__profile_image_11();
		NullCheck(L_1);
		Action_1_Invoke_m4165075220(L_1, L_2, /*hidden argument*/Action_1_Invoke_m4165075220_MethodInfo_var);
		return;
	}
}
// System.Void TwitterUserInfo::OnProfileBackgroundLoaded(UnityEngine.Texture2D)
extern const MethodInfo* Action_1_Invoke_m4165075220_MethodInfo_var;
extern const uint32_t TwitterUserInfo_OnProfileBackgroundLoaded_m303523722_MetadataUsageId;
extern "C"  void TwitterUserInfo_OnProfileBackgroundLoaded_m303523722 (TwitterUserInfo_t87370740 * __this, Texture2D_t3542995729 * ___img0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TwitterUserInfo_OnProfileBackgroundLoaded_m303523722_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture2D_t3542995729 * L_0 = ___img0;
		__this->set__profile_background_12(L_0);
		Action_1_t3344795111 * L_1 = __this->get_ActionProfileBackgroundImageLoaded_19();
		Texture2D_t3542995729 * L_2 = __this->get__profile_background_12();
		NullCheck(L_1);
		Action_1_Invoke_m4165075220(L_1, L_2, /*hidden argument*/Action_1_Invoke_m4165075220_MethodInfo_var);
		return;
	}
}
// UnityEngine.Color TwitterUserInfo::HexToColor(System.String)
extern "C"  Color_t2020392075  TwitterUserInfo_HexToColor_m538248085 (TwitterUserInfo_t87370740 * __this, String_t* ___hex0, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	uint8_t V_1 = 0x0;
	uint8_t V_2 = 0x0;
	{
		String_t* L_0 = ___hex0;
		NullCheck(L_0);
		String_t* L_1 = String_Substring_m12482732(L_0, 0, 2, /*hidden argument*/NULL);
		uint8_t L_2 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_1, ((int32_t)515), /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = ___hex0;
		NullCheck(L_3);
		String_t* L_4 = String_Substring_m12482732(L_3, 2, 2, /*hidden argument*/NULL);
		uint8_t L_5 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_4, ((int32_t)515), /*hidden argument*/NULL);
		V_1 = L_5;
		String_t* L_6 = ___hex0;
		NullCheck(L_6);
		String_t* L_7 = String_Substring_m12482732(L_6, 4, 2, /*hidden argument*/NULL);
		uint8_t L_8 = Byte_Parse_m4137294155(NULL /*static, unused*/, L_7, ((int32_t)515), /*hidden argument*/NULL);
		V_2 = L_8;
		uint8_t L_9 = V_0;
		uint8_t L_10 = V_1;
		uint8_t L_11 = V_2;
		Color32_t874517518  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Color32__ctor_m1932627809(&L_12, L_9, L_10, L_11, ((int32_t)255), /*hidden argument*/NULL);
		Color_t2020392075  L_13 = Color32_op_Implicit_m889975790(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void TwitterUserInfo::<ActionProfileImageLoaded>m__AD(UnityEngine.Texture2D)
extern "C"  void TwitterUserInfo_U3CActionProfileImageLoadedU3Em__AD_m3003816570 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TwitterUserInfo::<ActionProfileBackgroundImageLoaded>m__AE(UnityEngine.Texture2D)
extern "C"  void TwitterUserInfo_U3CActionProfileBackgroundImageLoadedU3Em__AE_m3213365547 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TWResult::.ctor(System.Boolean,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TWResult__ctor_m1336289862_MetadataUsageId;
extern "C"  void TWResult__ctor_m1336289862 (TWResult_t1480791060 * __this, bool ___IsResSucceeded0, String_t* ___resData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TWResult__ctor_m1336289862_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__data_1(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		bool L_1 = ___IsResSucceeded0;
		__this->set__IsSucceeded_0(L_1);
		String_t* L_2 = ___resData1;
		__this->set__data_1(L_2);
		return;
	}
}
// System.Boolean TWResult::get_IsSucceeded()
extern "C"  bool TWResult_get_IsSucceeded_m3208623953 (TWResult_t1480791060 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__IsSucceeded_0();
		return L_0;
	}
}
// System.String TWResult::get_data()
extern "C"  String_t* TWResult_get_data_m1312261893 (TWResult_t1480791060 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__data_1();
		return L_0;
	}
}
// System.Void User::.ctor()
extern "C"  void User__ctor_m2333596438 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int64 User::get_Id()
extern "C"  int64_t User_get_Id_m1163330207 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_U3CIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void User::set_Id(System.Int64)
extern "C"  void User_set_Id_m3907032336 (User_t719925459 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_U3CIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String User::get_FullName()
extern "C"  String_t* User_get_FullName_m18201382 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CFullNameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void User::set_FullName(System.String)
extern "C"  void User_set_FullName_m3337776817 (User_t719925459 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFullNameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String User::get_Email()
extern "C"  String_t* User_get_Email_m2844587358 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CEmailU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void User::set_Email(System.String)
extern "C"  void User_set_Email_m2895411485 (User_t719925459 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CEmailU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void Vector2Converter::.ctor()
extern "C"  void Vector2Converter__ctor_m3823559564 (Vector2Converter_t3051461781 * __this, const MethodInfo* method)
{
	{
		JsonConverter__ctor_m3599769528(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vector2Converter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern const uint32_t Vector2Converter_WriteJson_m3268326654_MetadataUsageId;
extern "C"  void Vector2Converter_WriteJson_m3268326654 (Vector2Converter_t3051461781 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t1719617802 * ___serializer2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2Converter_WriteJson_m3268326654_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___value1;
		V_0 = ((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_0, Vector2_t2243707579_il2cpp_TypeInfo_var))));
		JsonWriter_t1973729997 * L_1 = ___writer0;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(7 /* System.Void Newtonsoft.Json.JsonWriter::WriteStartObject() */, L_1);
		JsonWriter_t1973729997 * L_2 = ___writer0;
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String) */, L_2, _stringLiteral372029398);
		JsonWriter_t1973729997 * L_3 = ___writer0;
		float L_4 = (&V_0)->get_x_1();
		NullCheck(L_3);
		VirtActionInvoker1< float >::Invoke(28 /* System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Single) */, L_3, L_4);
		JsonWriter_t1973729997 * L_5 = ___writer0;
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String) */, L_5, _stringLiteral372029397);
		JsonWriter_t1973729997 * L_6 = ___writer0;
		float L_7 = (&V_0)->get_y_2();
		NullCheck(L_6);
		VirtActionInvoker1< float >::Invoke(28 /* System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Single) */, L_6, L_7);
		JsonWriter_t1973729997 * L_8 = ___writer0;
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(8 /* System.Void Newtonsoft.Json.JsonWriter::WriteEndObject() */, L_8);
		return;
	}
}
// System.Boolean Vector2Converter::CanConvert(System.Type)
extern const Il2CppType* Vector2_t2243707579_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Vector2Converter_CanConvert_m3401152238_MetadataUsageId;
extern "C"  bool Vector2Converter_CanConvert_m3401152238 (Vector2Converter_t3051461781 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2Converter_CanConvert_m3401152238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___objectType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Vector2_t2243707579_0_0_0_var), /*hidden argument*/NULL);
		return (bool)((((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))? 1 : 0);
	}
}
// System.Object Vector2Converter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1984862004;
extern const uint32_t Vector2Converter_ReadJson_m3547897987_MetadataUsageId;
extern "C"  Il2CppObject * Vector2Converter_ReadJson_m3547897987 (Vector2Converter_t3051461781 * __this, JsonReader_t3154730733 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t1719617802 * ___serializer3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2Converter_ReadJson_m3547897987_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_0, _stringLiteral1984862004, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean Vector2Converter::get_CanRead()
extern "C"  bool Vector2Converter_get_CanRead_m1629505327 (Vector2Converter_t3051461781 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void Vector3Converter::.ctor()
extern "C"  void Vector3Converter__ctor_m653849197 (Vector3Converter_t3052510548 * __this, const MethodInfo* method)
{
	{
		JsonConverter__ctor_m3599769528(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vector3Converter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern const uint32_t Vector3Converter_WriteJson_m814190525_MetadataUsageId;
extern "C"  void Vector3Converter_WriteJson_m814190525 (Vector3Converter_t3052510548 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t1719617802 * ___serializer2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3Converter_WriteJson_m814190525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___value1;
		V_0 = ((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_0, Vector3_t2243707580_il2cpp_TypeInfo_var))));
		JsonWriter_t1973729997 * L_1 = ___writer0;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(7 /* System.Void Newtonsoft.Json.JsonWriter::WriteStartObject() */, L_1);
		JsonWriter_t1973729997 * L_2 = ___writer0;
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String) */, L_2, _stringLiteral372029398);
		JsonWriter_t1973729997 * L_3 = ___writer0;
		float L_4 = (&V_0)->get_x_1();
		NullCheck(L_3);
		VirtActionInvoker1< float >::Invoke(28 /* System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Single) */, L_3, L_4);
		JsonWriter_t1973729997 * L_5 = ___writer0;
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String) */, L_5, _stringLiteral372029397);
		JsonWriter_t1973729997 * L_6 = ___writer0;
		float L_7 = (&V_0)->get_y_2();
		NullCheck(L_6);
		VirtActionInvoker1< float >::Invoke(28 /* System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Single) */, L_6, L_7);
		JsonWriter_t1973729997 * L_8 = ___writer0;
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String) */, L_8, _stringLiteral372029400);
		JsonWriter_t1973729997 * L_9 = ___writer0;
		float L_10 = (&V_0)->get_z_3();
		NullCheck(L_9);
		VirtActionInvoker1< float >::Invoke(28 /* System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Single) */, L_9, L_10);
		JsonWriter_t1973729997 * L_11 = ___writer0;
		NullCheck(L_11);
		VirtActionInvoker0::Invoke(8 /* System.Void Newtonsoft.Json.JsonWriter::WriteEndObject() */, L_11);
		return;
	}
}
// System.Boolean Vector3Converter::CanConvert(System.Type)
extern const Il2CppType* Vector3_t2243707580_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Vector3Converter_CanConvert_m2730517481_MetadataUsageId;
extern "C"  bool Vector3Converter_CanConvert_m2730517481 (Vector3Converter_t3052510548 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3Converter_CanConvert_m2730517481_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___objectType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Vector3_t2243707580_0_0_0_var), /*hidden argument*/NULL);
		return (bool)((((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))? 1 : 0);
	}
}
// System.Object Vector3Converter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1984862004;
extern const uint32_t Vector3Converter_ReadJson_m1133402306_MetadataUsageId;
extern "C"  Il2CppObject * Vector3Converter_ReadJson_m1133402306 (Vector3Converter_t3052510548 * __this, JsonReader_t3154730733 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t1719617802 * ___serializer3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3Converter_ReadJson_m1133402306_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_0, _stringLiteral1984862004, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean Vector3Converter::get_CanRead()
extern "C"  bool Vector3Converter_get_CanRead_m4040637396 (Vector3Converter_t3052510548 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void Vector4Converter::.ctor()
extern "C"  void Vector4Converter__ctor_m2449945418 (Vector4Converter_t3044633495 * __this, const MethodInfo* method)
{
	{
		JsonConverter__ctor_m3599769528(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vector4Converter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029387;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern Il2CppCodeGenString* _stringLiteral372029400;
extern const uint32_t Vector4Converter_WriteJson_m3794964100_MetadataUsageId;
extern "C"  void Vector4Converter_WriteJson_m3794964100 (Vector4Converter_t3044633495 * __this, JsonWriter_t1973729997 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t1719617802 * ___serializer2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4Converter_WriteJson_m3794964100_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___value1;
		V_0 = ((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_0, Vector4_t2243707581_il2cpp_TypeInfo_var))));
		JsonWriter_t1973729997 * L_1 = ___writer0;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(7 /* System.Void Newtonsoft.Json.JsonWriter::WriteStartObject() */, L_1);
		JsonWriter_t1973729997 * L_2 = ___writer0;
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String) */, L_2, _stringLiteral372029387);
		JsonWriter_t1973729997 * L_3 = ___writer0;
		float L_4 = (&V_0)->get_w_4();
		NullCheck(L_3);
		VirtActionInvoker1< float >::Invoke(28 /* System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Single) */, L_3, L_4);
		JsonWriter_t1973729997 * L_5 = ___writer0;
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String) */, L_5, _stringLiteral372029398);
		JsonWriter_t1973729997 * L_6 = ___writer0;
		float L_7 = (&V_0)->get_x_1();
		NullCheck(L_6);
		VirtActionInvoker1< float >::Invoke(28 /* System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Single) */, L_6, L_7);
		JsonWriter_t1973729997 * L_8 = ___writer0;
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String) */, L_8, _stringLiteral372029397);
		JsonWriter_t1973729997 * L_9 = ___writer0;
		float L_10 = (&V_0)->get_y_2();
		NullCheck(L_9);
		VirtActionInvoker1< float >::Invoke(28 /* System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Single) */, L_9, L_10);
		JsonWriter_t1973729997 * L_11 = ___writer0;
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String) */, L_11, _stringLiteral372029400);
		JsonWriter_t1973729997 * L_12 = ___writer0;
		float L_13 = (&V_0)->get_z_3();
		NullCheck(L_12);
		VirtActionInvoker1< float >::Invoke(28 /* System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Single) */, L_12, L_13);
		JsonWriter_t1973729997 * L_14 = ___writer0;
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(8 /* System.Void Newtonsoft.Json.JsonWriter::WriteEndObject() */, L_14);
		return;
	}
}
// System.Boolean Vector4Converter::CanConvert(System.Type)
extern const Il2CppType* Vector4_t2243707581_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Vector4Converter_CanConvert_m2683792996_MetadataUsageId;
extern "C"  bool Vector4Converter_CanConvert_m2683792996 (Vector4Converter_t3044633495 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4Converter_CanConvert_m2683792996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___objectType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Vector4_t2243707581_0_0_0_var), /*hidden argument*/NULL);
		return (bool)((((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))? 1 : 0);
	}
}
// System.Object Vector4Converter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1984862004;
extern const uint32_t Vector4Converter_ReadJson_m791286017_MetadataUsageId;
extern "C"  Il2CppObject * Vector4Converter_ReadJson_m791286017 (Vector4Converter_t3044633495 * __this, JsonReader_t3154730733 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t1719617802 * ___serializer3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4Converter_ReadJson_m791286017_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_0, _stringLiteral1984862004, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean Vector4Converter::get_CanRead()
extern "C"  bool Vector4Converter_get_CanRead_m3085923085 (Vector4Converter_t3044633495 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void WWWTextureLoader::.ctor()
extern Il2CppClass* WWWTextureLoader_t155872171_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3344795111_il2cpp_TypeInfo_var;
extern const MethodInfo* WWWTextureLoader_U3COnLoadU3Em__B5_m66899739_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3899261817_MethodInfo_var;
extern const uint32_t WWWTextureLoader__ctor_m2514922492_MetadataUsageId;
extern "C"  void WWWTextureLoader__ctor_m2514922492 (WWWTextureLoader_t155872171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTextureLoader__ctor_m2514922492_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WWWTextureLoader_t155872171 * G_B2_0 = NULL;
	WWWTextureLoader_t155872171 * G_B1_0 = NULL;
	{
		Action_1_t3344795111 * L_0 = ((WWWTextureLoader_t155872171_StaticFields*)WWWTextureLoader_t155872171_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_4();
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)WWWTextureLoader_U3COnLoadU3Em__B5_m66899739_MethodInfo_var);
		Action_1_t3344795111 * L_2 = (Action_1_t3344795111 *)il2cpp_codegen_object_new(Action_1_t3344795111_il2cpp_TypeInfo_var);
		Action_1__ctor_m3899261817(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m3899261817_MethodInfo_var);
		((WWWTextureLoader_t155872171_StaticFields*)WWWTextureLoader_t155872171_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_4(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Action_1_t3344795111 * L_3 = ((WWWTextureLoader_t155872171_StaticFields*)WWWTextureLoader_t155872171_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_4();
		NullCheck(G_B2_0);
		G_B2_0->set_OnLoad_3(L_3);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WWWTextureLoader::add_OnLoad(System.Action`1<UnityEngine.Texture2D>)
extern Il2CppClass* Action_1_t3344795111_il2cpp_TypeInfo_var;
extern const uint32_t WWWTextureLoader_add_OnLoad_m1127950879_MetadataUsageId;
extern "C"  void WWWTextureLoader_add_OnLoad_m1127950879 (WWWTextureLoader_t155872171 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTextureLoader_add_OnLoad_m1127950879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3344795111 * L_0 = __this->get_OnLoad_3();
		Action_1_t3344795111 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnLoad_3(((Action_1_t3344795111 *)CastclassSealed(L_2, Action_1_t3344795111_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WWWTextureLoader::remove_OnLoad(System.Action`1<UnityEngine.Texture2D>)
extern Il2CppClass* Action_1_t3344795111_il2cpp_TypeInfo_var;
extern const uint32_t WWWTextureLoader_remove_OnLoad_m3003222534_MetadataUsageId;
extern "C"  void WWWTextureLoader_remove_OnLoad_m3003222534 (WWWTextureLoader_t155872171 * __this, Action_1_t3344795111 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTextureLoader_remove_OnLoad_m3003222534_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3344795111 * L_0 = __this->get_OnLoad_3();
		Action_1_t3344795111 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnLoad_3(((Action_1_t3344795111 *)CastclassSealed(L_2, Action_1_t3344795111_il2cpp_TypeInfo_var)));
		return;
	}
}
// WWWTextureLoader WWWTextureLoader::Create()
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisWWWTextureLoader_t155872171_m2812646813_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral883950689;
extern const uint32_t WWWTextureLoader_Create_m2845297014_MetadataUsageId;
extern "C"  WWWTextureLoader_t155872171 * WWWTextureLoader_Create_m2845297014 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTextureLoader_Create_m2845297014_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_0, _stringLiteral883950689, /*hidden argument*/NULL);
		NullCheck(L_0);
		WWWTextureLoader_t155872171 * L_1 = GameObject_AddComponent_TisWWWTextureLoader_t155872171_m2812646813(L_0, /*hidden argument*/GameObject_AddComponent_TisWWWTextureLoader_t155872171_m2812646813_MethodInfo_var);
		return L_1;
	}
}
// System.Void WWWTextureLoader::LoadTexture(System.String)
extern "C"  void WWWTextureLoader_LoadTexture_m783656337 (WWWTextureLoader_t155872171 * __this, String_t* ___url0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___url0;
		__this->set__url_2(L_0);
		Il2CppObject * L_1 = WWWTextureLoader_LoadCoroutin_m3068864101(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator WWWTextureLoader::LoadCoroutin()
extern Il2CppClass* U3CLoadCoroutinU3Ec__IteratorC_t1223036233_il2cpp_TypeInfo_var;
extern const uint32_t WWWTextureLoader_LoadCoroutin_m3068864101_MetadataUsageId;
extern "C"  Il2CppObject * WWWTextureLoader_LoadCoroutin_m3068864101 (WWWTextureLoader_t155872171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTextureLoader_LoadCoroutin_m3068864101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * V_0 = NULL;
	{
		U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * L_0 = (U3CLoadCoroutinU3Ec__IteratorC_t1223036233 *)il2cpp_codegen_object_new(U3CLoadCoroutinU3Ec__IteratorC_t1223036233_il2cpp_TypeInfo_var);
		U3CLoadCoroutinU3Ec__IteratorC__ctor_m1891961986(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * L_2 = V_0;
		return L_2;
	}
}
// System.Void WWWTextureLoader::<OnLoad>m__B5(UnityEngine.Texture2D)
extern "C"  void WWWTextureLoader_U3COnLoadU3Em__B5_m66899739 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void WWWTextureLoader/<LoadCoroutin>c__IteratorC::.ctor()
extern "C"  void U3CLoadCoroutinU3Ec__IteratorC__ctor_m1891961986 (U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object WWWTextureLoader/<LoadCoroutin>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadCoroutinU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1601072068 (U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object WWWTextureLoader/<LoadCoroutin>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadCoroutinU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m952981804 (U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean WWWTextureLoader/<LoadCoroutin>c__IteratorC::MoveNext()
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m4165075220_MethodInfo_var;
extern const uint32_t U3CLoadCoroutinU3Ec__IteratorC_MoveNext_m735453058_MetadataUsageId;
extern "C"  bool U3CLoadCoroutinU3Ec__IteratorC_MoveNext_m735453058 (U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadCoroutinU3Ec__IteratorC_MoveNext_m735453058_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004f;
		}
	}
	{
		goto IL_0097;
	}

IL_0021:
	{
		WWWTextureLoader_t155872171 * L_2 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		String_t* L_3 = L_2->get__url_2();
		WWW_t2919945039 * L_4 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_0(L_4);
		WWW_t2919945039 * L_5 = __this->get_U3CwwwU3E__0_0();
		__this->set_U24current_2(L_5);
		__this->set_U24PC_1(1);
		goto IL_0099;
	}

IL_004f:
	{
		WWW_t2919945039 * L_6 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_6);
		String_t* L_7 = WWW_get_error_m3092701216(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_007f;
		}
	}
	{
		WWWTextureLoader_t155872171 * L_8 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_8);
		Action_1_t3344795111 * L_9 = L_8->get_OnLoad_3();
		WWW_t2919945039 * L_10 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_10);
		Texture2D_t3542995729 * L_11 = WWW_get_texture_m1121178301(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Action_1_Invoke_m4165075220(L_9, L_11, /*hidden argument*/Action_1_Invoke_m4165075220_MethodInfo_var);
		goto IL_0090;
	}

IL_007f:
	{
		WWWTextureLoader_t155872171 * L_12 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_12);
		Action_1_t3344795111 * L_13 = L_12->get_OnLoad_3();
		NullCheck(L_13);
		Action_1_Invoke_m4165075220(L_13, (Texture2D_t3542995729 *)NULL, /*hidden argument*/Action_1_Invoke_m4165075220_MethodInfo_var);
	}

IL_0090:
	{
		__this->set_U24PC_1((-1));
	}

IL_0097:
	{
		return (bool)0;
	}

IL_0099:
	{
		return (bool)1;
	}
	// Dead block : IL_009b: ldloc.1
}
// System.Void WWWTextureLoader/<LoadCoroutin>c__IteratorC::Dispose()
extern "C"  void U3CLoadCoroutinU3Ec__IteratorC_Dispose_m1183995683 (U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void WWWTextureLoader/<LoadCoroutin>c__IteratorC::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadCoroutinU3Ec__IteratorC_Reset_m786866165_MetadataUsageId;
extern "C"  void U3CLoadCoroutinU3Ec__IteratorC_Reset_m786866165 (U3CLoadCoroutinU3Ec__IteratorC_t1223036233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadCoroutinU3Ec__IteratorC_Reset_m786866165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void XScaleModifayer::.ctor()
extern "C"  void XScaleModifayer__ctor_m2070622509 (XScaleModifayer_t1656594896 * __this, const MethodInfo* method)
{
	{
		__this->set_XMaxSize_2((10.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void XScaleModifayer::Awake()
extern "C"  void XScaleModifayer_Awake_m2321640540 (XScaleModifayer_t1656594896 * __this, const MethodInfo* method)
{
	{
		XScaleModifayer_Calculate_m3092807809(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void XScaleModifayer::FixedUpdate()
extern "C"  void XScaleModifayer_FixedUpdate_m2659588320 (XScaleModifayer_t1656594896 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_calulateStartOnly_4();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		XScaleModifayer_Calculate_m3092807809(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void XScaleModifayer::Calculate()
extern Il2CppClass* PreviewScreenUtil_t19016308_il2cpp_TypeInfo_var;
extern const uint32_t XScaleModifayer_Calculate_m3092807809_MetadataUsageId;
extern "C"  void XScaleModifayer_Calculate_m3092807809 (XScaleModifayer_t1656594896 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XScaleModifayer_Calculate_m3092807809_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PreviewScreenUtil_t19016308_il2cpp_TypeInfo_var);
		Rect_t3681755626  L_1 = PreviewScreenUtil_getObjectBounds_m3150637395(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = __this->get_XMaxSize_2();
		V_1 = ((float)((float)((float)((float)(((float)((float)L_2)))/(float)(100.0f)))*(float)L_3));
		float L_4 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		float L_5 = V_1;
		if ((!(((float)L_4) < ((float)L_5))))
		{
			goto IL_0039;
		}
	}
	{
		bool L_6 = __this->get_scaleDownOnly_3();
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		return;
	}

IL_0039:
	{
		float L_7 = V_1;
		float L_8 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		V_2 = ((float)((float)L_7/(float)L_8));
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_localScale_m3074381503(L_10, /*hidden argument*/NULL);
		float L_12 = V_2;
		Vector3_t2243707580  L_13 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localScale_m2325460848(L_9, L_13, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
