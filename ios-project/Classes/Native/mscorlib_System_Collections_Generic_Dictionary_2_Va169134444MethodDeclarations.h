﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<FB_ProfileImageSize,System.Object>
struct Dictionary_2_t2777568976;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va169134444.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<FB_ProfileImageSize,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2320414789_gshared (Enumerator_t169134444 * __this, Dictionary_2_t2777568976 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2320414789(__this, ___host0, method) ((  void (*) (Enumerator_t169134444 *, Dictionary_2_t2777568976 *, const MethodInfo*))Enumerator__ctor_m2320414789_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1381660858_gshared (Enumerator_t169134444 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1381660858(__this, method) ((  Il2CppObject * (*) (Enumerator_t169134444 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1381660858_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<FB_ProfileImageSize,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m817655290_gshared (Enumerator_t169134444 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m817655290(__this, method) ((  void (*) (Enumerator_t169134444 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m817655290_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<FB_ProfileImageSize,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1512935053_gshared (Enumerator_t169134444 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1512935053(__this, method) ((  void (*) (Enumerator_t169134444 *, const MethodInfo*))Enumerator_Dispose_m1512935053_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<FB_ProfileImageSize,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m152879166_gshared (Enumerator_t169134444 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m152879166(__this, method) ((  bool (*) (Enumerator_t169134444 *, const MethodInfo*))Enumerator_MoveNext_m152879166_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<FB_ProfileImageSize,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1112377992_gshared (Enumerator_t169134444 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1112377992(__this, method) ((  Il2CppObject * (*) (Enumerator_t169134444 *, const MethodInfo*))Enumerator_get_Current_m1112377992_gshared)(__this, method)
