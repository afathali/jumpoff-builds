﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ScreenScaler358654512.h"
#include "AssemblyU2DCSharp_XScaleModifayer1656594896.h"
#include "AssemblyU2DCSharp_Glow4087105517.h"
#include "AssemblyU2DCSharp_HorseSkinSelect2411928240.h"
#include "AssemblyU2DCSharp_DustinHorne_Json_Examples_JNSimp2759437545.h"
#include "AssemblyU2DCSharp_DustinHorne_Json_Examples_JNPoly2287370235.h"
#include "AssemblyU2DCSharp_DustinHorne_Json_Examples_JNBson3141686326.h"
#include "AssemblyU2DCSharp_DustinHorne_Json_Examples_JNObje4041290897.h"
#include "AssemblyU2DCSharp_DustinHorne_Json_Examples_JNSimp1947941312.h"
#include "AssemblyU2DCSharp_DustinHorne_Json_Examples_JNSubC2237730893.h"
#include "AssemblyU2DCSharp_JsonTestScript2390343987.h"
#include "AssemblyU2DCSharp_Assets_DustinHorne_JsonDotNetUnit375836235.h"
#include "AssemblyU2DCSharp_Assets_DustinHorne_JsonDotNetUni2925764113.h"
#include "AssemblyU2DCSharp_Assets_DustinHorne_JsonDotNetUni2736175310.h"
#include "AssemblyU2DCSharp_SampleObject3388626405.h"
#include "AssemblyU2DCSharp_Dustin1265690455.h"
#include "AssemblyU2DCSharp_Assets_DustinHorne_JsonDotNetUni1988087395.h"
#include "AssemblyU2DCSharp_TextTest2178089809.h"
#include "AssemblyU2DCSharp_JsonDotNet_Extras_CustomConverte4108782939.h"
#include "AssemblyU2DCSharp_Vector2Converter3051461781.h"
#include "AssemblyU2DCSharp_Vector3Converter3052510548.h"
#include "AssemblyU2DCSharp_Vector4Converter3044633495.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Aot_EnumerationEx539530610.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonBinaryT2009671055.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonBinaryW2169798354.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonObjectI1719565518.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonReader2352164445.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonReader_1749779967.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonReader_2144264477.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonToken3582361217.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonObject2841166125.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonArray2790707601.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonValue309805921.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonString3501425379.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonRegex3386721543.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonPropert1491061775.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonType2055433366.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonWriter101776461.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ConstructorHandl4150360451.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_BsonO2711362450.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_DateT3254246496.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_IsoDa1450148078.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_JavaSc847478424.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_JsonD1248356504.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_KeyVal575953290.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_Regex3589249209.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_StringE54762202.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_XmlDoc559857997.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_XmlEle832553344.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_XmlDe3510733548.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_XmlNo2035079622.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_XmlNo2842232697.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_XmlNo1249742045.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_XmlNod913630666.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_XmlNo3970047118.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_DefaultValueHand3457895463.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonArrayAttribu3639750789.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConstructorA4031199386.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonContainerAttri47210975.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConvert3949895659.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter1964060750.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverterAtt3918837738.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverterCol3315164788.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonIgnoreAttribu898097550.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonObjectAttribu162755825.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonPropertyAttr2023370155.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader3154730733.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader_State2444238258.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReaderExcept2043888884.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializatio1492322735.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer1719617802.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializerSet842388167.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonTextReader726416198.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonTextReader_R2517660684.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonTextWriter2524035668.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken620654565.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonValidatingRe2264892260.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonValidatingRe4218888543.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_WriteState1009238728.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Formatting4009318759.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter1973729997.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter_State3285832914.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriterExcept1246029574.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JArray1483708661.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JConstructo3123819808.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3538280255.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer_4173827918.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer_3911541127.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject278519297.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject_JPr2398227289.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject_U3C2136724618.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JPath1663498301.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (ScreenScaler_t358654512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[4] = 
{
	ScreenScaler_t358654512::get_offset_of_calulateStartOnly_2(),
	ScreenScaler_t358654512::get_offset_of_persentsY_3(),
	ScreenScaler_t358654512::get_offset_of__scaleFactorY_4(),
	ScreenScaler_t358654512::get_offset_of__xScaleDiff_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (XScaleModifayer_t1656594896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[3] = 
{
	XScaleModifayer_t1656594896::get_offset_of_XMaxSize_2(),
	XScaleModifayer_t1656594896::get_offset_of_scaleDownOnly_3(),
	XScaleModifayer_t1656594896::get_offset_of_calulateStartOnly_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (Glow_t4087105517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[3] = 
{
	Glow_t4087105517::get_offset_of_minAlpha_2(),
	Glow_t4087105517::get_offset_of_maxAlpha_3(),
	Glow_t4087105517::get_offset_of_speed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (HorseSkinSelect_t2411928240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2803[11] = 
{
	HorseSkinSelect_t2411928240::get_offset_of_environmentUI_2(),
	HorseSkinSelect_t2411928240::get_offset_of_horseSkin0_button_3(),
	HorseSkinSelect_t2411928240::get_offset_of_horseSkin1_button_4(),
	HorseSkinSelect_t2411928240::get_offset_of_horseSkin2_button_5(),
	HorseSkinSelect_t2411928240::get_offset_of_horseSkin3_button_6(),
	HorseSkinSelect_t2411928240::get_offset_of_horseSkin0_7(),
	HorseSkinSelect_t2411928240::get_offset_of_horseSkin1_8(),
	HorseSkinSelect_t2411928240::get_offset_of_horseSkin2_9(),
	HorseSkinSelect_t2411928240::get_offset_of_horseSkin3_10(),
	HorseSkinSelect_t2411928240::get_offset_of_horseModel_11(),
	HorseSkinSelect_t2411928240::get_offset_of_unicornHorn_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (JNSimpleObjectSample_t2759437545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (JNPolymorphismSample_t2287370235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[1] = 
{
	JNPolymorphismSample_t2287370235::get_offset_of__rnd_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (JNBsonSample_t3141686326), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (JNObjectType_t4041290897)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2807[3] = 
{
	JNObjectType_t4041290897::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (JNSimpleObjectModel_t1947941312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2808[5] = 
{
	JNSimpleObjectModel_t1947941312::get_offset_of_U3CIntValueU3Ek__BackingField_0(),
	JNSimpleObjectModel_t1947941312::get_offset_of_U3CFloatValueU3Ek__BackingField_1(),
	JNSimpleObjectModel_t1947941312::get_offset_of_U3CStringValueU3Ek__BackingField_2(),
	JNSimpleObjectModel_t1947941312::get_offset_of_U3CIntListU3Ek__BackingField_3(),
	JNSimpleObjectModel_t1947941312::get_offset_of_U3CObjectTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (JNSubClassModel_t2237730893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[1] = 
{
	JNSubClassModel_t2237730893::get_offset_of_U3CSubClassStringValueU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (JsonTestScript_t2390343987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[2] = 
{
	0,
	JsonTestScript_t2390343987::get_offset_of__text_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (TestCaseUtils_t375836235), -1, sizeof(TestCaseUtils_t375836235_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2811[1] = 
{
	TestCaseUtils_t375836235_StaticFields::get_offset_of__rnd_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (SampleBase_t2925764113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2812[3] = 
{
	SampleBase_t2925764113::get_offset_of_U3CTextValueU3Ek__BackingField_0(),
	SampleBase_t2925764113::get_offset_of_U3CNumberValueU3Ek__BackingField_1(),
	SampleBase_t2925764113::get_offset_of_U3CVectorValueU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (SampleChild_t2736175310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[2] = 
{
	SampleChild_t2736175310::get_offset_of_U3CObjectListU3Ek__BackingField_3(),
	SampleChild_t2736175310::get_offset_of_U3CObjectDictionaryU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (SampleObject_t3388626405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[4] = 
{
	SampleObject_t3388626405::get_offset_of_U3CStringPropertyU3Ek__BackingField_0(),
	SampleObject_t3388626405::get_offset_of_U3CFloatPropertyU3Ek__BackingField_1(),
	SampleObject_t3388626405::get_offset_of_U3CGuidPropertyU3Ek__BackingField_2(),
	SampleObject_t3388626405::get_offset_of_U3CVectorPropertyU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (Dustin_t1265690455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2815[3] = 
{
	Dustin_t1265690455::get_offset_of_U3CNameU3Ek__BackingField_0(),
	Dustin_t1265690455::get_offset_of_U3CAgeU3Ek__BackingField_1(),
	Dustin_t1265690455::get_offset_of_U3CUrlU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (SimpleClassObject_t1988087395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[3] = 
{
	SimpleClassObject_t1988087395::get_offset_of_U3CTextValueU3Ek__BackingField_0(),
	SimpleClassObject_t1988087395::get_offset_of_U3CNumberValueU3Ek__BackingField_1(),
	SimpleClassObject_t1988087395::get_offset_of_U3CVectorValueU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (TextTest_t2178089809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2817[6] = 
{
	TextTest_t2178089809::get_offset_of_TextObject_2(),
	TextTest_t2178089809::get_offset_of__statusText_3(),
	TextTest_t2178089809::get_offset_of__refTime_4(),
	TextTest_t2178089809::get_offset_of__testNum_5(),
	TextTest_t2178089809::get_offset_of__tester_6(),
	TextTest_t2178089809::get_offset_of__complete_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (Matrix4x4Converter_t4108782939), -1, sizeof(Matrix4x4Converter_t4108782939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2818[2] = 
{
	Matrix4x4Converter_t4108782939_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	Matrix4x4Converter_t4108782939_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (Vector2Converter_t3051461781), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (Vector3Converter_t3052510548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (Vector4Converter_t3044633495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (EnumerationExtension_t539530610), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (BsonBinaryType_t2009671055)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2823[7] = 
{
	BsonBinaryType_t2009671055::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (BsonBinaryWriter_t2169798354), -1, sizeof(BsonBinaryWriter_t2169798354_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2824[5] = 
{
	BsonBinaryWriter_t2169798354_StaticFields::get_offset_of_Encoding_0(),
	BsonBinaryWriter_t2169798354::get_offset_of__writer_1(),
	BsonBinaryWriter_t2169798354::get_offset_of__largeByteBuffer_2(),
	BsonBinaryWriter_t2169798354::get_offset_of__maxChars_3(),
	BsonBinaryWriter_t2169798354::get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (BsonObjectId_t1719565518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2825[1] = 
{
	BsonObjectId_t1719565518::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (BsonReader_t2352164445), -1, sizeof(BsonReader_t2352164445_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2826[15] = 
{
	0,
	BsonReader_t2352164445_StaticFields::get_offset_of__seqRange1_10(),
	BsonReader_t2352164445_StaticFields::get_offset_of__seqRange2_11(),
	BsonReader_t2352164445_StaticFields::get_offset_of__seqRange3_12(),
	BsonReader_t2352164445_StaticFields::get_offset_of__seqRange4_13(),
	BsonReader_t2352164445::get_offset_of__reader_14(),
	BsonReader_t2352164445::get_offset_of__stack_15(),
	BsonReader_t2352164445::get_offset_of__byteBuffer_16(),
	BsonReader_t2352164445::get_offset_of__charBuffer_17(),
	BsonReader_t2352164445::get_offset_of__currentElementType_18(),
	BsonReader_t2352164445::get_offset_of__bsonReaderState_19(),
	BsonReader_t2352164445::get_offset_of__currentContext_20(),
	BsonReader_t2352164445::get_offset_of__readRootValueAsArray_21(),
	BsonReader_t2352164445::get_offset_of__jsonNet35BinaryCompatibility_22(),
	BsonReader_t2352164445::get_offset_of__dateTimeKindHandling_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (BsonReaderState_t1749779967)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2827[10] = 
{
	BsonReaderState_t1749779967::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (ContainerContext_t2144264477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[3] = 
{
	ContainerContext_t2144264477::get_offset_of_Type_0(),
	ContainerContext_t2144264477::get_offset_of_Length_1(),
	ContainerContext_t2144264477::get_offset_of_Position_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (BsonToken_t3582361217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2829[2] = 
{
	BsonToken_t3582361217::get_offset_of_U3CParentU3Ek__BackingField_0(),
	BsonToken_t3582361217::get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (BsonObject_t2841166125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[1] = 
{
	BsonObject_t2841166125::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (BsonArray_t2790707601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2831[1] = 
{
	BsonArray_t2790707601::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (BsonValue_t309805921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2832[2] = 
{
	BsonValue_t309805921::get_offset_of__value_2(),
	BsonValue_t309805921::get_offset_of__type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (BsonString_t3501425379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[2] = 
{
	BsonString_t3501425379::get_offset_of_U3CByteCountU3Ek__BackingField_4(),
	BsonString_t3501425379::get_offset_of_U3CIncludeLengthU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (BsonRegex_t3386721543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2834[2] = 
{
	BsonRegex_t3386721543::get_offset_of_U3CPatternU3Ek__BackingField_2(),
	BsonRegex_t3386721543::get_offset_of_U3COptionsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (BsonProperty_t1491061775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2835[2] = 
{
	BsonProperty_t1491061775::get_offset_of_U3CNameU3Ek__BackingField_0(),
	BsonProperty_t1491061775::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (BsonType_t2055433366)+ sizeof (Il2CppObject), sizeof(int8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2836[21] = 
{
	BsonType_t2055433366::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (BsonWriter_t101776461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[4] = 
{
	BsonWriter_t101776461::get_offset_of__writer_6(),
	BsonWriter_t101776461::get_offset_of__root_7(),
	BsonWriter_t101776461::get_offset_of__parent_8(),
	BsonWriter_t101776461::get_offset_of__propertyName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (ConstructorHandling_t4150360451)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2838[3] = 
{
	ConstructorHandling_t4150360451::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (BsonObjectIdConverter_t2711362450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (DateTimeConverterBase_t3254246496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (IsoDateTimeConverter_t1450148078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2842[4] = 
{
	0,
	IsoDateTimeConverter_t1450148078::get_offset_of__dateTimeStyles_1(),
	IsoDateTimeConverter_t1450148078::get_offset_of__dateTimeFormat_2(),
	IsoDateTimeConverter_t1450148078::get_offset_of__culture_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (JavaScriptDateTimeConverter_t847478424), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (JsonDateTimeSerializationMode_t1248356504)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2844[5] = 
{
	JsonDateTimeSerializationMode_t1248356504::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (KeyValuePairConverter_t575953290), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (RegexConverter_t3589249209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (StringEnumConverter_t54762202), -1, sizeof(StringEnumConverter_t54762202_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2847[3] = 
{
	StringEnumConverter_t54762202::get_offset_of__enumMemberNamesPerType_0(),
	StringEnumConverter_t54762202::get_offset_of_U3CCamelCaseTextU3Ek__BackingField_1(),
	StringEnumConverter_t54762202_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (XmlDocumentWrapper_t559857997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2848[1] = 
{
	XmlDocumentWrapper_t559857997::get_offset_of__document_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (XmlElementWrapper_t832553344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2849[1] = 
{
	XmlElementWrapper_t832553344::get_offset_of__element_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (XmlDeclarationWrapper_t3510733548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2850[1] = 
{
	XmlDeclarationWrapper_t3510733548::get_offset_of__declaration_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (XmlNodeWrapper_t2035079622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2851[1] = 
{
	XmlNodeWrapper_t2035079622::get_offset_of__node_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (XmlNodeConverter_t2842232697), -1, sizeof(XmlNodeConverter_t2842232697_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2856[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	XmlNodeConverter_t2842232697::get_offset_of_U3CDeserializeRootElementNameU3Ek__BackingField_7(),
	XmlNodeConverter_t2842232697::get_offset_of_U3CWriteArrayAttributeU3Ek__BackingField_8(),
	XmlNodeConverter_t2842232697::get_offset_of_U3COmitRootObjectU3Ek__BackingField_9(),
	XmlNodeConverter_t2842232697_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_10(),
	XmlNodeConverter_t2842232697_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_11(),
	XmlNodeConverter_t2842232697_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_12(),
	XmlNodeConverter_t2842232697_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (U3CSerializeNodeU3Ec__AnonStorey1A_t1249742045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2857[1] = 
{
	U3CSerializeNodeU3Ec__AnonStorey1A_t1249742045::get_offset_of_node_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (U3CReadArrayElementsU3Ec__AnonStorey1B_t913630666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2858[1] = 
{
	U3CReadArrayElementsU3Ec__AnonStorey1B_t913630666::get_offset_of_propertyName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (U3CDeserializeNodeU3Ec__AnonStorey1C_t3970047118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2859[1] = 
{
	U3CDeserializeNodeU3Ec__AnonStorey1C_t3970047118::get_offset_of_propertyName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (DefaultValueHandling_t3457895463)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2860[5] = 
{
	DefaultValueHandling_t3457895463::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (JsonArrayAttribute_t3639750789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[1] = 
{
	JsonArrayAttribute_t3639750789::get_offset_of__allowNullItems_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (JsonConstructorAttribute_t4031199386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (JsonContainerAttribute_t47210975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[4] = 
{
	JsonContainerAttribute_t47210975::get_offset_of__isReference_0(),
	JsonContainerAttribute_t47210975::get_offset_of_U3CIdU3Ek__BackingField_1(),
	JsonContainerAttribute_t47210975::get_offset_of_U3CTitleU3Ek__BackingField_2(),
	JsonContainerAttribute_t47210975::get_offset_of_U3CDescriptionU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (JsonConvert_t3949895659), -1, sizeof(JsonConvert_t3949895659_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2865[8] = 
{
	JsonConvert_t3949895659_StaticFields::get_offset_of_True_0(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_False_1(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_Null_2(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_Undefined_3(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_PositiveInfinity_4(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_NegativeInfinity_5(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_NaN_6(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_InitialJavaScriptDateTicks_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (JsonConverter_t1964060750), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (JsonConverterAttribute_t3918837738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[1] = 
{
	JsonConverterAttribute_t3918837738::get_offset_of__converterType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (JsonConverterCollection_t3315164788), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (JsonIgnoreAttribute_t898097550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (JsonObjectAttribute_t162755825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2870[1] = 
{
	JsonObjectAttribute_t162755825::get_offset_of__memberSerialization_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (JsonPropertyAttribute_t2023370155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[9] = 
{
	JsonPropertyAttribute_t2023370155::get_offset_of__nullValueHandling_0(),
	JsonPropertyAttribute_t2023370155::get_offset_of__defaultValueHandling_1(),
	JsonPropertyAttribute_t2023370155::get_offset_of__referenceLoopHandling_2(),
	JsonPropertyAttribute_t2023370155::get_offset_of__objectCreationHandling_3(),
	JsonPropertyAttribute_t2023370155::get_offset_of__typeNameHandling_4(),
	JsonPropertyAttribute_t2023370155::get_offset_of__isReference_5(),
	JsonPropertyAttribute_t2023370155::get_offset_of__order_6(),
	JsonPropertyAttribute_t2023370155::get_offset_of_U3CPropertyNameU3Ek__BackingField_7(),
	JsonPropertyAttribute_t2023370155::get_offset_of_U3CRequiredU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (JsonReader_t3154730733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[9] = 
{
	JsonReader_t3154730733::get_offset_of__token_0(),
	JsonReader_t3154730733::get_offset_of__value_1(),
	JsonReader_t3154730733::get_offset_of__valueType_2(),
	JsonReader_t3154730733::get_offset_of__quoteChar_3(),
	JsonReader_t3154730733::get_offset_of__currentState_4(),
	JsonReader_t3154730733::get_offset_of__currentTypeContext_5(),
	JsonReader_t3154730733::get_offset_of__top_6(),
	JsonReader_t3154730733::get_offset_of__stack_7(),
	JsonReader_t3154730733::get_offset_of_U3CCloseInputU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (State_t2444238258)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2873[14] = 
{
	State_t2444238258::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (JsonReaderException_t2043888884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[2] = 
{
	JsonReaderException_t2043888884::get_offset_of_U3CLineNumberU3Ek__BackingField_11(),
	JsonReaderException_t2043888884::get_offset_of_U3CLinePositionU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (JsonSerializationException_t1492322735), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (JsonSerializer_t1719617802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2876[15] = 
{
	JsonSerializer_t1719617802::get_offset_of__typeNameHandling_0(),
	JsonSerializer_t1719617802::get_offset_of__typeNameAssemblyFormat_1(),
	JsonSerializer_t1719617802::get_offset_of__preserveReferencesHandling_2(),
	JsonSerializer_t1719617802::get_offset_of__referenceLoopHandling_3(),
	JsonSerializer_t1719617802::get_offset_of__missingMemberHandling_4(),
	JsonSerializer_t1719617802::get_offset_of__objectCreationHandling_5(),
	JsonSerializer_t1719617802::get_offset_of__nullValueHandling_6(),
	JsonSerializer_t1719617802::get_offset_of__defaultValueHandling_7(),
	JsonSerializer_t1719617802::get_offset_of__constructorHandling_8(),
	JsonSerializer_t1719617802::get_offset_of__converters_9(),
	JsonSerializer_t1719617802::get_offset_of__contractResolver_10(),
	JsonSerializer_t1719617802::get_offset_of__referenceResolver_11(),
	JsonSerializer_t1719617802::get_offset_of__binder_12(),
	JsonSerializer_t1719617802::get_offset_of__context_13(),
	JsonSerializer_t1719617802::get_offset_of_Error_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (JsonSerializerSettings_t842388167), -1, sizeof(JsonSerializerSettings_t842388167_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2877[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	JsonSerializerSettings_t842388167_StaticFields::get_offset_of_DefaultContext_9(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_10(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CMissingMemberHandlingU3Ek__BackingField_11(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_12(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CNullValueHandlingU3Ek__BackingField_13(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_14(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CConvertersU3Ek__BackingField_15(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CPreserveReferencesHandlingU3Ek__BackingField_16(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_17(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CTypeNameAssemblyFormatU3Ek__BackingField_18(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CConstructorHandlingU3Ek__BackingField_19(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CContractResolverU3Ek__BackingField_20(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CReferenceResolverU3Ek__BackingField_21(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CBinderU3Ek__BackingField_22(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CErrorU3Ek__BackingField_23(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CContextU3Ek__BackingField_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (JsonTextReader_t726416198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2878[10] = 
{
	0,
	0,
	JsonTextReader_t726416198::get_offset_of__reader_11(),
	JsonTextReader_t726416198::get_offset_of__buffer_12(),
	JsonTextReader_t726416198::get_offset_of__lastChar_13(),
	JsonTextReader_t726416198::get_offset_of__currentLinePosition_14(),
	JsonTextReader_t726416198::get_offset_of__currentLineNumber_15(),
	JsonTextReader_t726416198::get_offset_of__end_16(),
	JsonTextReader_t726416198::get_offset_of__readType_17(),
	JsonTextReader_t726416198::get_offset_of__culture_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (ReadType_t2517660684)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2879[5] = 
{
	ReadType_t2517660684::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (JsonTextWriter_t2524035668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2880[6] = 
{
	JsonTextWriter_t2524035668::get_offset_of__writer_6(),
	JsonTextWriter_t2524035668::get_offset_of__base64Encoder_7(),
	JsonTextWriter_t2524035668::get_offset_of__indentChar_8(),
	JsonTextWriter_t2524035668::get_offset_of__indentation_9(),
	JsonTextWriter_t2524035668::get_offset_of__quoteChar_10(),
	JsonTextWriter_t2524035668::get_offset_of__quoteName_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (JsonToken_t620654565)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2881[19] = 
{
	JsonToken_t620654565::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (JsonValidatingReader_t2264892260), -1, sizeof(JsonValidatingReader_t2264892260_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2882[8] = 
{
	JsonValidatingReader_t2264892260::get_offset_of__reader_9(),
	JsonValidatingReader_t2264892260::get_offset_of__stack_10(),
	JsonValidatingReader_t2264892260::get_offset_of__schema_11(),
	JsonValidatingReader_t2264892260::get_offset_of__model_12(),
	JsonValidatingReader_t2264892260::get_offset_of__currentScope_13(),
	JsonValidatingReader_t2264892260::get_offset_of_ValidationEventHandler_14(),
	JsonValidatingReader_t2264892260_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_15(),
	JsonValidatingReader_t2264892260_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (SchemaScope_t4218888543), -1, sizeof(SchemaScope_t4218888543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2883[9] = 
{
	SchemaScope_t4218888543::get_offset_of__tokenType_0(),
	SchemaScope_t4218888543::get_offset_of__schemas_1(),
	SchemaScope_t4218888543::get_offset_of__requiredProperties_2(),
	SchemaScope_t4218888543::get_offset_of_U3CCurrentPropertyNameU3Ek__BackingField_3(),
	SchemaScope_t4218888543::get_offset_of_U3CArrayItemCountU3Ek__BackingField_4(),
	SchemaScope_t4218888543_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	SchemaScope_t4218888543_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	SchemaScope_t4218888543_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
	SchemaScope_t4218888543_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (WriteState_t1009238728)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2884[8] = 
{
	WriteState_t1009238728::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (Formatting_t4009318759)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2885[3] = 
{
	Formatting_t4009318759::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (JsonWriter_t1973729997), -1, sizeof(JsonWriter_t1973729997_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2886[6] = 
{
	JsonWriter_t1973729997_StaticFields::get_offset_of_stateArray_0(),
	JsonWriter_t1973729997::get_offset_of__top_1(),
	JsonWriter_t1973729997::get_offset_of__stack_2(),
	JsonWriter_t1973729997::get_offset_of__currentState_3(),
	JsonWriter_t1973729997::get_offset_of__formatting_4(),
	JsonWriter_t1973729997::get_offset_of_U3CCloseOutputU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (State_t3285832914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2887[12] = 
{
	State_t3285832914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (JsonWriterException_t1246029574), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (JArray_t1483708661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[1] = 
{
	JArray_t1483708661::get_offset_of__values_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (JConstructor_t3123819808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2891[2] = 
{
	JConstructor_t3123819808::get_offset_of__name_8(),
	JConstructor_t3123819808::get_offset_of__values_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (JContainer_t3538280255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2892[2] = 
{
	JContainer_t3538280255::get_offset_of__syncRoot_6(),
	JContainer_t3538280255::get_offset_of__busy_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (JTokenReferenceEqualityComparer_t4173827918), -1, sizeof(JTokenReferenceEqualityComparer_t4173827918_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2893[1] = 
{
	JTokenReferenceEqualityComparer_t4173827918_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (U3CDescendantsU3Ec__Iterator10_t3911541127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2894[8] = 
{
	U3CDescendantsU3Ec__Iterator10_t3911541127::get_offset_of_U3CU24s_235U3E__0_0(),
	U3CDescendantsU3Ec__Iterator10_t3911541127::get_offset_of_U3CoU3E__1_1(),
	U3CDescendantsU3Ec__Iterator10_t3911541127::get_offset_of_U3CcU3E__2_2(),
	U3CDescendantsU3Ec__Iterator10_t3911541127::get_offset_of_U3CU24s_236U3E__3_3(),
	U3CDescendantsU3Ec__Iterator10_t3911541127::get_offset_of_U3CdU3E__4_4(),
	U3CDescendantsU3Ec__Iterator10_t3911541127::get_offset_of_U24PC_5(),
	U3CDescendantsU3Ec__Iterator10_t3911541127::get_offset_of_U24current_6(),
	U3CDescendantsU3Ec__Iterator10_t3911541127::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2895[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (JObject_t278519297), -1, sizeof(JObject_t278519297_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2896[3] = 
{
	JObject_t278519297::get_offset_of__properties_8(),
	JObject_t278519297::get_offset_of_PropertyChanged_9(),
	JObject_t278519297_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (JPropertKeyedCollection_t2398227289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (U3CGetEnumeratorU3Ec__Iterator11_t2136724618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2898[5] = 
{
	U3CGetEnumeratorU3Ec__Iterator11_t2136724618::get_offset_of_U3CU24s_246U3E__0_0(),
	U3CGetEnumeratorU3Ec__Iterator11_t2136724618::get_offset_of_U3CpropertyU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator11_t2136724618::get_offset_of_U24PC_2(),
	U3CGetEnumeratorU3Ec__Iterator11_t2136724618::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator11_t2136724618::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (JPath_t1663498301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2899[3] = 
{
	JPath_t1663498301::get_offset_of__expression_0(),
	JPath_t1663498301::get_offset_of__currentIndex_1(),
	JPath_t1663498301::get_offset_of_U3CPartsU3Ek__BackingField_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
