﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GK_SavedGame
struct GK_SavedGame_t3320093620;
// System.String
struct String_t;
// System.Action`1<GK_SaveDataLoaded>
struct Action_1_t3486487701;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// GK_SaveDataLoaded
struct GK_SaveDataLoaded_t3684688319;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_SaveDataLoaded3684688319.h"

// System.Void GK_SavedGame::.ctor(System.String,System.String,System.String,System.String)
extern "C"  void GK_SavedGame__ctor_m3885958145 (GK_SavedGame_t3320093620 * __this, String_t* ___id0, String_t* ___name1, String_t* ___device2, String_t* ___dateString3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_SavedGame::add_ActionDataLoaded(System.Action`1<GK_SaveDataLoaded>)
extern "C"  void GK_SavedGame_add_ActionDataLoaded_m1130332849 (GK_SavedGame_t3320093620 * __this, Action_1_t3486487701 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_SavedGame::remove_ActionDataLoaded(System.Action`1<GK_SaveDataLoaded>)
extern "C"  void GK_SavedGame_remove_ActionDataLoaded_m4073317998 (GK_SavedGame_t3320093620 * __this, Action_1_t3486487701 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_SavedGame::LoadData()
extern "C"  void GK_SavedGame_LoadData_m300625807 (GK_SavedGame_t3320093620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_SavedGame::GenerateDataLoadEvent(System.String)
extern "C"  void GK_SavedGame_GenerateDataLoadEvent_m3810918634 (GK_SavedGame_t3320093620 * __this, String_t* ___base64Data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_SavedGame::GenerateDataLoadFailedEvent(System.String)
extern "C"  void GK_SavedGame_GenerateDataLoadFailedEvent_m3684063617 (GK_SavedGame_t3320093620 * __this, String_t* ___erorrData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_SavedGame::get_Id()
extern "C"  String_t* GK_SavedGame_get_Id_m3033690352 (GK_SavedGame_t3320093620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_SavedGame::get_Name()
extern "C"  String_t* GK_SavedGame_get_Name_m4035959050 (GK_SavedGame_t3320093620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GK_SavedGame::get_DeviceName()
extern "C"  String_t* GK_SavedGame_get_DeviceName_m2329665192 (GK_SavedGame_t3320093620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime GK_SavedGame::get_ModificationDate()
extern "C"  DateTime_t693205669  GK_SavedGame_get_ModificationDate_m43166615 (GK_SavedGame_t3320093620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GK_SavedGame::get_Data()
extern "C"  ByteU5BU5D_t3397334013* GK_SavedGame_get_Data_m3222674024 (GK_SavedGame_t3320093620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GK_SavedGame::get_IsDataLoaded()
extern "C"  bool GK_SavedGame_get_IsDataLoaded_m2589071113 (GK_SavedGame_t3320093620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GK_SavedGame::<ActionDataLoaded>m__2B(GK_SaveDataLoaded)
extern "C"  void GK_SavedGame_U3CActionDataLoadedU3Em__2B_m1291767908 (Il2CppObject * __this /* static, unused */, GK_SaveDataLoaded_t3684688319 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
