﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct ReadOnlyCollection_1_t1928530869;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct IList_1_t2283685778;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Schema.JsonSchemaType[]
struct JsonSchemaTypeU5BU5D_t3104176164;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Schema.JsonSchemaType>
struct IEnumerator_1_t3513236300;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem1742745177.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1928559417_gshared (ReadOnlyCollection_1_t1928530869 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1928559417(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1928530869 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1928559417_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m919250619_gshared (ReadOnlyCollection_1_t1928530869 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m919250619(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1928530869 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m919250619_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m551738727_gshared (ReadOnlyCollection_1_t1928530869 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m551738727(__this, method) ((  void (*) (ReadOnlyCollection_1_t1928530869 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m551738727_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2833780734_gshared (ReadOnlyCollection_1_t1928530869 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2833780734(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1928530869 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2833780734_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3703174504_gshared (ReadOnlyCollection_1_t1928530869 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3703174504(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1928530869 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3703174504_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3194765010_gshared (ReadOnlyCollection_1_t1928530869 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3194765010(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1928530869 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3194765010_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3001354868_gshared (ReadOnlyCollection_1_t1928530869 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3001354868(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1928530869 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3001354868_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2554219975_gshared (ReadOnlyCollection_1_t1928530869 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2554219975(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1928530869 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2554219975_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m568162027_gshared (ReadOnlyCollection_1_t1928530869 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m568162027(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1928530869 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m568162027_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m923548168_gshared (ReadOnlyCollection_1_t1928530869 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m923548168(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1928530869 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m923548168_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3770912039_gshared (ReadOnlyCollection_1_t1928530869 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3770912039(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1928530869 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3770912039_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1283285612_gshared (ReadOnlyCollection_1_t1928530869 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1283285612(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1928530869 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1283285612_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2036061190_gshared (ReadOnlyCollection_1_t1928530869 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2036061190(__this, method) ((  void (*) (ReadOnlyCollection_1_t1928530869 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2036061190_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m416166538_gshared (ReadOnlyCollection_1_t1928530869 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m416166538(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1928530869 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m416166538_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3655454386_gshared (ReadOnlyCollection_1_t1928530869 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3655454386(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1928530869 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3655454386_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1697834295_gshared (ReadOnlyCollection_1_t1928530869 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1697834295(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1928530869 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1697834295_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3284158831_gshared (ReadOnlyCollection_1_t1928530869 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3284158831(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1928530869 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3284158831_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3264413593_gshared (ReadOnlyCollection_1_t1928530869 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3264413593(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1928530869 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3264413593_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3440875180_gshared (ReadOnlyCollection_1_t1928530869 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3440875180(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1928530869 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3440875180_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4109239176_gshared (ReadOnlyCollection_1_t1928530869 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4109239176(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1928530869 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4109239176_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2283714343_gshared (ReadOnlyCollection_1_t1928530869 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2283714343(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1928530869 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2283714343_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m46042760_gshared (ReadOnlyCollection_1_t1928530869 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m46042760(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1928530869 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m46042760_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3882970799_gshared (ReadOnlyCollection_1_t1928530869 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3882970799(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1928530869 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3882970799_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m686554590_gshared (ReadOnlyCollection_1_t1928530869 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m686554590(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1928530869 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m686554590_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1470388029_gshared (ReadOnlyCollection_1_t1928530869 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1470388029(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1928530869 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m1470388029_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3448448071_gshared (ReadOnlyCollection_1_t1928530869 * __this, JsonSchemaTypeU5BU5D_t3104176164* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3448448071(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1928530869 *, JsonSchemaTypeU5BU5D_t3104176164*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3448448071_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3979475208_gshared (ReadOnlyCollection_1_t1928530869 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3979475208(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1928530869 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3979475208_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m539629339_gshared (ReadOnlyCollection_1_t1928530869 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m539629339(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1928530869 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m539629339_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2304934572_gshared (ReadOnlyCollection_1_t1928530869 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2304934572(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1928530869 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2304934572_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchemaType>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m316399760_gshared (ReadOnlyCollection_1_t1928530869 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m316399760(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1928530869 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m316399760_gshared)(__this, ___index0, method)
