﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<GP_SendAppInvitesResult>
struct Action_1_t3800876926;
// System.Action`1<GP_RetrieveAppInviteResult>
struct Action_1_t4115251907;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen3715273415.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GP_AppInvitesController
struct  GP_AppInvitesController_t2424429497  : public SA_Singleton_OLD_1_t3715273415
{
public:

public:
};

struct GP_AppInvitesController_t2424429497_StaticFields
{
public:
	// System.Action`1<GP_SendAppInvitesResult> GP_AppInvitesController::ActionAppInvitesSent
	Action_1_t3800876926 * ___ActionAppInvitesSent_4;
	// System.Action`1<GP_RetrieveAppInviteResult> GP_AppInvitesController::ActionAppInviteRetrieved
	Action_1_t4115251907 * ___ActionAppInviteRetrieved_5;
	// System.Action`1<GP_SendAppInvitesResult> GP_AppInvitesController::<>f__am$cache2
	Action_1_t3800876926 * ___U3CU3Ef__amU24cache2_6;
	// System.Action`1<GP_RetrieveAppInviteResult> GP_AppInvitesController::<>f__am$cache3
	Action_1_t4115251907 * ___U3CU3Ef__amU24cache3_7;

public:
	inline static int32_t get_offset_of_ActionAppInvitesSent_4() { return static_cast<int32_t>(offsetof(GP_AppInvitesController_t2424429497_StaticFields, ___ActionAppInvitesSent_4)); }
	inline Action_1_t3800876926 * get_ActionAppInvitesSent_4() const { return ___ActionAppInvitesSent_4; }
	inline Action_1_t3800876926 ** get_address_of_ActionAppInvitesSent_4() { return &___ActionAppInvitesSent_4; }
	inline void set_ActionAppInvitesSent_4(Action_1_t3800876926 * value)
	{
		___ActionAppInvitesSent_4 = value;
		Il2CppCodeGenWriteBarrier(&___ActionAppInvitesSent_4, value);
	}

	inline static int32_t get_offset_of_ActionAppInviteRetrieved_5() { return static_cast<int32_t>(offsetof(GP_AppInvitesController_t2424429497_StaticFields, ___ActionAppInviteRetrieved_5)); }
	inline Action_1_t4115251907 * get_ActionAppInviteRetrieved_5() const { return ___ActionAppInviteRetrieved_5; }
	inline Action_1_t4115251907 ** get_address_of_ActionAppInviteRetrieved_5() { return &___ActionAppInviteRetrieved_5; }
	inline void set_ActionAppInviteRetrieved_5(Action_1_t4115251907 * value)
	{
		___ActionAppInviteRetrieved_5 = value;
		Il2CppCodeGenWriteBarrier(&___ActionAppInviteRetrieved_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(GP_AppInvitesController_t2424429497_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline Action_1_t3800876926 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline Action_1_t3800876926 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(Action_1_t3800876926 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(GP_AppInvitesController_t2424429497_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline Action_1_t4115251907 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline Action_1_t4115251907 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(Action_1_t4115251907 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
