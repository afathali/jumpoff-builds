﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SA.Common.Models.PrefabAsyncLoader
struct PrefabAsyncLoader_t3449223737;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_t1558332529;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void SA.Common.Models.PrefabAsyncLoader::.ctor()
extern "C"  void PrefabAsyncLoader__ctor_m3495415449 (PrefabAsyncLoader_t3449223737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.PrefabAsyncLoader::add_ObjectLoadedAction(System.Action`1<UnityEngine.GameObject>)
extern "C"  void PrefabAsyncLoader_add_ObjectLoadedAction_m3266632541 (PrefabAsyncLoader_t3449223737 * __this, Action_1_t1558332529 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.PrefabAsyncLoader::remove_ObjectLoadedAction(System.Action`1<UnityEngine.GameObject>)
extern "C"  void PrefabAsyncLoader_remove_ObjectLoadedAction_m651202704 (PrefabAsyncLoader_t3449223737 * __this, Action_1_t1558332529 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SA.Common.Models.PrefabAsyncLoader SA.Common.Models.PrefabAsyncLoader::Create()
extern "C"  PrefabAsyncLoader_t3449223737 * PrefabAsyncLoader_Create_m2869374078 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.PrefabAsyncLoader::Awake()
extern "C"  void PrefabAsyncLoader_Awake_m1083193332 (PrefabAsyncLoader_t3449223737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.PrefabAsyncLoader::LoadAsync(System.String)
extern "C"  void PrefabAsyncLoader_LoadAsync_m689025947 (PrefabAsyncLoader_t3449223737 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SA.Common.Models.PrefabAsyncLoader::Load()
extern "C"  Il2CppObject * PrefabAsyncLoader_Load_m1681057677 (PrefabAsyncLoader_t3449223737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA.Common.Models.PrefabAsyncLoader::<ObjectLoadedAction>m__7D(UnityEngine.GameObject)
extern "C"  void PrefabAsyncLoader_U3CObjectLoadedActionU3Em__7D_m3276664909 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
