﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key58473718MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3118906029(__this, ___host0, method) ((  void (*) (Enumerator_t2648852204 *, Dictionary_2_t4254316062 *, const MethodInfo*))Enumerator__ctor_m1642475044_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1660571386(__this, method) ((  Il2CppObject * (*) (Enumerator_t2648852204 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m610493107_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1875009522(__this, method) ((  void (*) (Enumerator_t2648852204 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1684647011_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::Dispose()
#define Enumerator_Dispose_m868630493(__this, method) ((  void (*) (Enumerator_t2648852204 *, const MethodInfo*))Enumerator_Dispose_m688620792_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m554526695(__this, method) ((  bool (*) (Enumerator_t2648852204 *, const MethodInfo*))Enumerator_MoveNext_m639383991_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Assets.DustinHorne.JsonDotNetUnity.TestCases.TestModels.SampleBase,System.Int32>::get_Current()
#define Enumerator_get_Current_m2611803835(__this, method) ((  SampleBase_t2925764113 * (*) (Enumerator_t2648852204 *, const MethodInfo*))Enumerator_get_Current_m1529104153_gshared)(__this, method)
