﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClickManagerExample
struct ClickManagerExample_t799098483;
// GK_Player
struct GK_Player_t2782008294;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GK_Player2782008294.h"

// System.Void ClickManagerExample::.ctor()
extern "C"  void ClickManagerExample__ctor_m2723865428 (ClickManagerExample_t799098483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickManagerExample::Awake()
extern "C"  void ClickManagerExample_Awake_m3093757037 (ClickManagerExample_t799098483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickManagerExample::Update()
extern "C"  void ClickManagerExample_Update_m3137933889 (ClickManagerExample_t799098483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickManagerExample::HandleActionDataReceived(GK_Player,System.Byte[])
extern "C"  void ClickManagerExample_HandleActionDataReceived_m599260778 (ClickManagerExample_t799098483 * __this, GK_Player_t2782008294 * ___player0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
