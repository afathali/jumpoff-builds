﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookAndroidUseExample/<PostFBScreenshot>c__Iterator4
struct U3CPostFBScreenshotU3Ec__Iterator4_t3249146850;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FacebookAndroidUseExample/<PostFBScreenshot>c__Iterator4::.ctor()
extern "C"  void U3CPostFBScreenshotU3Ec__Iterator4__ctor_m2232171047 (U3CPostFBScreenshotU3Ec__Iterator4_t3249146850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FacebookAndroidUseExample/<PostFBScreenshot>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPostFBScreenshotU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1048862089 (U3CPostFBScreenshotU3Ec__Iterator4_t3249146850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FacebookAndroidUseExample/<PostFBScreenshot>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPostFBScreenshotU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3088023297 (U3CPostFBScreenshotU3Ec__Iterator4_t3249146850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FacebookAndroidUseExample/<PostFBScreenshot>c__Iterator4::MoveNext()
extern "C"  bool U3CPostFBScreenshotU3Ec__Iterator4_MoveNext_m3529217909 (U3CPostFBScreenshotU3Ec__Iterator4_t3249146850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample/<PostFBScreenshot>c__Iterator4::Dispose()
extern "C"  void U3CPostFBScreenshotU3Ec__Iterator4_Dispose_m716687408 (U3CPostFBScreenshotU3Ec__Iterator4_t3249146850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookAndroidUseExample/<PostFBScreenshot>c__Iterator4::Reset()
extern "C"  void U3CPostFBScreenshotU3Ec__Iterator4_Reset_m3673134774 (U3CPostFBScreenshotU3Ec__Iterator4_t3249146850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
