﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FB_PostingTask
struct FB_PostingTask_t2068490008;
// System.String
struct String_t;
// FB_Result
struct FB_Result_t838248372;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_FB_Result838248372.h"

// System.Void FB_PostingTask::.ctor()
extern "C"  void FB_PostingTask__ctor_m2891948649 (FB_PostingTask_t2068490008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FB_PostingTask FB_PostingTask::Cretae()
extern "C"  FB_PostingTask_t2068490008 * FB_PostingTask_Cretae_m1520325986 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_PostingTask::FeedShare(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void FB_PostingTask_FeedShare_m1117387810 (FB_PostingTask_t2068490008 * __this, String_t* ___toId0, String_t* ___link1, String_t* ___linkName2, String_t* ___linkCaption3, String_t* ___linkDescription4, String_t* ___picture5, String_t* ___actionName6, String_t* ___actionLink7, String_t* ___reference8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_PostingTask::OnFBInited()
extern "C"  void FB_PostingTask_OnFBInited_m386733047 (FB_PostingTask_t2068490008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FB_PostingTask::OnFBAuth(FB_Result)
extern "C"  void FB_PostingTask_OnFBAuth_m2457414770 (FB_PostingTask_t2068490008 * __this, FB_Result_t838248372 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
