﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayConnection
struct GooglePlayConnection_t348367589;
// System.Action`1<GooglePlayConnectionResult>
struct Action_1_t2560518106;
// System.Action`1<GPConnectionState>
struct Action_1_t449146262;
// System.Action
struct Action_t3226471752;
// System.String
struct String_t;
// GooglePlayConnectionResult
struct GooglePlayConnectionResult_t2758718724;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_GPConnectionState647346880.h"
#include "AssemblyU2DCSharp_GooglePlayConnectionResult2758718724.h"

// System.Void GooglePlayConnection::.ctor()
extern "C"  void GooglePlayConnection__ctor_m101105884 (GooglePlayConnection_t348367589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::.cctor()
extern "C"  void GooglePlayConnection__cctor_m3691537471 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::add_ActionConnectionResultReceived(System.Action`1<GooglePlayConnectionResult>)
extern "C"  void GooglePlayConnection_add_ActionConnectionResultReceived_m972150702 (Il2CppObject * __this /* static, unused */, Action_1_t2560518106 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::remove_ActionConnectionResultReceived(System.Action`1<GooglePlayConnectionResult>)
extern "C"  void GooglePlayConnection_remove_ActionConnectionResultReceived_m3275946515 (Il2CppObject * __this /* static, unused */, Action_1_t2560518106 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::add_ActionConnectionStateChanged(System.Action`1<GPConnectionState>)
extern "C"  void GooglePlayConnection_add_ActionConnectionStateChanged_m148191927 (Il2CppObject * __this /* static, unused */, Action_1_t449146262 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::remove_ActionConnectionStateChanged(System.Action`1<GPConnectionState>)
extern "C"  void GooglePlayConnection_remove_ActionConnectionStateChanged_m1507028638 (Il2CppObject * __this /* static, unused */, Action_1_t449146262 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::add_ActionPlayerConnected(System.Action)
extern "C"  void GooglePlayConnection_add_ActionPlayerConnected_m3801355297 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::remove_ActionPlayerConnected(System.Action)
extern "C"  void GooglePlayConnection_remove_ActionPlayerConnected_m4258758830 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::add_ActionPlayerDisconnected(System.Action)
extern "C"  void GooglePlayConnection_add_ActionPlayerDisconnected_m3962954439 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::remove_ActionPlayerDisconnected(System.Action)
extern "C"  void GooglePlayConnection_remove_ActionPlayerDisconnected_m1549908572 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::Awake()
extern "C"  void GooglePlayConnection_Awake_m1989099425 (GooglePlayConnection_t348367589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::Init()
extern "C"  void GooglePlayConnection_Init_m1413053416 (GooglePlayConnection_t348367589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::connect()
extern "C"  void GooglePlayConnection_connect_m1091014638 (GooglePlayConnection_t348367589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::Connect()
extern "C"  void GooglePlayConnection_Connect_m1055297486 (GooglePlayConnection_t348367589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::connect(System.String)
extern "C"  void GooglePlayConnection_connect_m4269794872 (GooglePlayConnection_t348367589 * __this, String_t* ___accountName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::Connect(System.String)
extern "C"  void GooglePlayConnection_Connect_m3058087896 (GooglePlayConnection_t348367589 * __this, String_t* ___accountName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::disconnect()
extern "C"  void GooglePlayConnection_disconnect_m3237200920 (GooglePlayConnection_t348367589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::Disconnect()
extern "C"  void GooglePlayConnection_Disconnect_m1754947512 (GooglePlayConnection_t348367589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayConnection::CheckState()
extern "C"  bool GooglePlayConnection_CheckState_m3729261449 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayConnection::get_IsConnected()
extern "C"  bool GooglePlayConnection_get_IsConnected_m1824323050 (GooglePlayConnection_t348367589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPConnectionState GooglePlayConnection::get_state()
extern "C"  int32_t GooglePlayConnection_get_state_m1932340671 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPConnectionState GooglePlayConnection::get_State()
extern "C"  int32_t GooglePlayConnection_get_State_m1933414687 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayConnection::get_isInitialized()
extern "C"  bool GooglePlayConnection_get_isInitialized_m2795706591 (GooglePlayConnection_t348367589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayConnection::get_IsInitialized()
extern "C"  bool GooglePlayConnection_get_IsInitialized_m188146175 (GooglePlayConnection_t348367589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::OnApplicationPause(System.Boolean)
extern "C"  void GooglePlayConnection_OnApplicationPause_m3751944854 (GooglePlayConnection_t348367589 * __this, bool ___pauseStatus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::OnPlayServiceDisconnected(System.String)
extern "C"  void GooglePlayConnection_OnPlayServiceDisconnected_m4206271587 (GooglePlayConnection_t348367589 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::OnConnectionResult(System.String)
extern "C"  void GooglePlayConnection_OnConnectionResult_m2708091268 (GooglePlayConnection_t348367589 * __this, String_t* ___resultCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::OnStateChange(GPConnectionState)
extern "C"  void GooglePlayConnection_OnStateChange_m3456633740 (GooglePlayConnection_t348367589 * __this, int32_t ___connectionState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::<ActionConnectionResultReceived>m__37(GooglePlayConnectionResult)
extern "C"  void GooglePlayConnection_U3CActionConnectionResultReceivedU3Em__37_m3739674759 (Il2CppObject * __this /* static, unused */, GooglePlayConnectionResult_t2758718724 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::<ActionConnectionStateChanged>m__38(GPConnectionState)
extern "C"  void GooglePlayConnection_U3CActionConnectionStateChangedU3Em__38_m2314240707 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::<ActionPlayerConnected>m__39()
extern "C"  void GooglePlayConnection_U3CActionPlayerConnectedU3Em__39_m1141029839 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayConnection::<ActionPlayerDisconnected>m__3A()
extern "C"  void GooglePlayConnection_U3CActionPlayerDisconnectedU3Em__3A_m3322744969 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
