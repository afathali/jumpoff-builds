﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SP_FB_API_v6
struct SP_FB_API_v6_t1505850536;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// SPFacebook/FB_Delegate
struct FB_Delegate_t3934797786;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_FB_HttpMethod1543704700.h"
#include "AssemblyU2DCSharp_SPFacebook_FB_Delegate3934797786.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929.h"
#include "AssemblyU2DCSharp_FB_RequestActionType2205998668.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void SP_FB_API_v6::.ctor()
extern "C"  void SP_FB_API_v6__ctor_m2516295019 (SP_FB_API_v6_t1505850536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SP_FB_API_v6::Init()
extern "C"  void SP_FB_API_v6_Init_m3660930053 (SP_FB_API_v6_t1505850536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SP_FB_API_v6::Login(System.String[])
extern "C"  void SP_FB_API_v6_Login_m3116291172 (SP_FB_API_v6_t1505850536 * __this, StringU5BU5D_t1642385972* ___scopes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SP_FB_API_v6::Logout()
extern "C"  void SP_FB_API_v6_Logout_m1988255873 (SP_FB_API_v6_t1505850536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SP_FB_API_v6::API(System.String,FB_HttpMethod,SPFacebook/FB_Delegate)
extern "C"  void SP_FB_API_v6_API_m3434272041 (SP_FB_API_v6_t1505850536 * __this, String_t* ___query0, int32_t ___method1, FB_Delegate_t3934797786 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SP_FB_API_v6::API(System.String,FB_HttpMethod,SPFacebook/FB_Delegate,UnityEngine.WWWForm)
extern "C"  void SP_FB_API_v6_API_m404136811 (SP_FB_API_v6_t1505850536 * __this, String_t* ___query0, int32_t ___method1, FB_Delegate_t3934797786 * ___callback2, WWWForm_t3950226929 * ___form3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SP_FB_API_v6::AppRequest(System.String,FB_RequestActionType,System.String,System.String[],System.String,System.String)
extern "C"  void SP_FB_API_v6_AppRequest_m2218758627 (SP_FB_API_v6_t1505850536 * __this, String_t* ___message0, int32_t ___actionType1, String_t* ___objectId2, StringU5BU5D_t1642385972* ___to3, String_t* ___data4, String_t* ___title5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SP_FB_API_v6::AppRequest(System.String,FB_RequestActionType,System.String,System.Collections.Generic.List`1<System.Object>,System.String[],System.Nullable`1<System.Int32>,System.String,System.String)
extern "C"  void SP_FB_API_v6_AppRequest_m1857662443 (SP_FB_API_v6_t1505850536 * __this, String_t* ___message0, int32_t ___actionType1, String_t* ___objectId2, List_1_t2058570427 * ___filters3, StringU5BU5D_t1642385972* ___excludeIds4, Nullable_1_t334943763  ___maxRecipients5, String_t* ___data6, String_t* ___title7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SP_FB_API_v6::AppRequest(System.String,System.String[],System.Collections.Generic.List`1<System.Object>,System.String[],System.Nullable`1<System.Int32>,System.String,System.String)
extern "C"  void SP_FB_API_v6_AppRequest_m105259817 (SP_FB_API_v6_t1505850536 * __this, String_t* ___message0, StringU5BU5D_t1642385972* ___to1, List_1_t2058570427 * ___filters2, StringU5BU5D_t1642385972* ___excludeIds3, Nullable_1_t334943763  ___maxRecipients4, String_t* ___data5, String_t* ___title6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SP_FB_API_v6::FeedShare(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void SP_FB_API_v6_FeedShare_m2191401178 (SP_FB_API_v6_t1505850536 * __this, String_t* ___toId0, String_t* ___link1, String_t* ___linkName2, String_t* ___linkCaption3, String_t* ___linkDescription4, String_t* ___picture5, String_t* ___actionName6, String_t* ___actionLink7, String_t* ___reference8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SP_FB_API_v6::get_IsLoggedIn()
extern "C"  bool SP_FB_API_v6_get_IsLoggedIn_m2036322617 (SP_FB_API_v6_t1505850536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SP_FB_API_v6::get_UserId()
extern "C"  String_t* SP_FB_API_v6_get_UserId_m4179094755 (SP_FB_API_v6_t1505850536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SP_FB_API_v6::get_AccessToken()
extern "C"  String_t* SP_FB_API_v6_get_AccessToken_m3639447204 (SP_FB_API_v6_t1505850536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SP_FB_API_v6::get_AppId()
extern "C"  String_t* SP_FB_API_v6_get_AppId_m3434703631 (SP_FB_API_v6_t1505850536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SP_FB_API_v6::get_IsAPIEnabled()
extern "C"  bool SP_FB_API_v6_get_IsAPIEnabled_m4288156237 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
