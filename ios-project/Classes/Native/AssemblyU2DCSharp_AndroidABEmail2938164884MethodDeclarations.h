﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidABEmail
struct AndroidABEmail_t2938164884;

#include "codegen/il2cpp-codegen.h"

// System.Void AndroidABEmail::.ctor()
extern "C"  void AndroidABEmail__ctor_m2848911415 (AndroidABEmail_t2938164884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
