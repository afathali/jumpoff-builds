﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MCGBehaviour/InjectionContainer/<Register>c__AnonStorey3A
struct U3CRegisterU3Ec__AnonStorey3A_t3929677068;
// MCGBehaviour/InjectionContainer/TypeData
struct TypeData_t212254090;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MCGBehaviour_InjectionContainer_T212254090.h"

// System.Void MCGBehaviour/InjectionContainer/<Register>c__AnonStorey3A::.ctor()
extern "C"  void U3CRegisterU3Ec__AnonStorey3A__ctor_m1931231333 (U3CRegisterU3Ec__AnonStorey3A_t3929677068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MCGBehaviour/InjectionContainer/<Register>c__AnonStorey3A::<>m__109(MCGBehaviour/InjectionContainer/TypeData)
extern "C"  bool U3CRegisterU3Ec__AnonStorey3A_U3CU3Em__109_m1397794878 (U3CRegisterU3Ec__AnonStorey3A_t3929677068 * __this, TypeData_t212254090 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
