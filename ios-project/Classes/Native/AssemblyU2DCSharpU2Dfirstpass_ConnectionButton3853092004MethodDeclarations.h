﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConnectionButton
struct ConnectionButton_t3853092004;

#include "codegen/il2cpp-codegen.h"

// System.Void ConnectionButton::.ctor()
extern "C"  void ConnectionButton__ctor_m1971640657 (ConnectionButton_t3853092004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionButton::Start()
extern "C"  void ConnectionButton_Start_m2252121561 (ConnectionButton_t3853092004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionButton::OnGUI()
extern "C"  void ConnectionButton_OnGUI_m3520659951 (ConnectionButton_t3853092004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
