﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t3898244613 ();
extern "C" void DelegatePInvokeWrapper_Swapper_t2637371637 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3184826381 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t489908132 ();
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t754146990 ();
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t362827733 ();
extern "C" void DelegatePInvokeWrapper_ThreadStart_t3437517264 ();
extern "C" void DelegatePInvokeWrapper_ReadMethod_t3362229488 ();
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1990215745 ();
extern "C" void DelegatePInvokeWrapper_WriteMethod_t1894833619 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t1559754630 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t888270799 ();
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t3737776727 ();
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1824458113 ();
extern "C" void DelegatePInvokeWrapper_Action_t3226471752 ();
extern "C" void DelegatePInvokeWrapper_UnityAdsDelegate_t3613839672 ();
extern "C" void DelegatePInvokeWrapper_AndroidJavaRunnable_t3501776228 ();
extern "C" void DelegatePInvokeWrapper_LogCallback_t1867914413 ();
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t3007145346 ();
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554 ();
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033 ();
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3522132132 ();
extern "C" void DelegatePInvokeWrapper_StateChanged_t2480912210 ();
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815 ();
extern "C" void DelegatePInvokeWrapper_UnityAction_t4025899511 ();
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033 ();
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3486805455 ();
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336 ();
extern "C" void DelegatePInvokeWrapper_BannerFailedToLoadDelegate_t1733255396 ();
extern "C" void DelegatePInvokeWrapper_BannerWasClickedDelegate_t2267846590 ();
extern "C" void DelegatePInvokeWrapper_BannerWasLoadedDelegate_t2542964478 ();
extern "C" void DelegatePInvokeWrapper_InterstitialWasLoadedDelegate_t1835954262 ();
extern "C" void DelegatePInvokeWrapper_InterstitialWasViewedDelegate_t365858839 ();
extern "C" void DelegatePInvokeWrapper_UnityPurchasingCallback_t2635187846 ();
extern "C" void DelegatePInvokeWrapper_UnityNativePurchasingCallback_t3230812225 ();
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t1946318473 ();
extern "C" void DelegatePInvokeWrapper_CharGetter_t1955031820 ();
extern "C" void DelegatePInvokeWrapper_BillingInitListener_t4162012659 ();
extern "C" void DelegatePInvokeWrapper_ApplyTween_t1085725044 ();
extern "C" void DelegatePInvokeWrapper_EasingFunction_t1024616186 ();
extern "C" void DelegatePInvokeWrapper_del_t1625342804 ();
extern "C" void DelegatePInvokeWrapper_ChangeCallback_t1172037850 ();
extern const Il2CppMethodPointer g_DelegateWrappersManagedToNative[42] = 
{
	DelegatePInvokeWrapper_AppDomainInitializer_t3898244613,
	DelegatePInvokeWrapper_Swapper_t2637371637,
	DelegatePInvokeWrapper_ReadDelegate_t3184826381,
	DelegatePInvokeWrapper_WriteDelegate_t489908132,
	DelegatePInvokeWrapper_CrossContextDelegate_t754146990,
	DelegatePInvokeWrapper_CallbackHandler_t362827733,
	DelegatePInvokeWrapper_ThreadStart_t3437517264,
	DelegatePInvokeWrapper_ReadMethod_t3362229488,
	DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1990215745,
	DelegatePInvokeWrapper_WriteMethod_t1894833619,
	DelegatePInvokeWrapper_ReadDelegate_t1559754630,
	DelegatePInvokeWrapper_WriteDelegate_t888270799,
	DelegatePInvokeWrapper_SocketAsyncCall_t3737776727,
	DelegatePInvokeWrapper_CostDelegate_t1824458113,
	DelegatePInvokeWrapper_Action_t3226471752,
	DelegatePInvokeWrapper_UnityAdsDelegate_t3613839672,
	DelegatePInvokeWrapper_AndroidJavaRunnable_t3501776228,
	DelegatePInvokeWrapper_LogCallback_t1867914413,
	DelegatePInvokeWrapper_PCMReaderCallback_t3007145346,
	DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554,
	DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033,
	DelegatePInvokeWrapper_WillRenderCanvases_t3522132132,
	DelegatePInvokeWrapper_StateChanged_t2480912210,
	DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815,
	DelegatePInvokeWrapper_UnityAction_t4025899511,
	DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033,
	DelegatePInvokeWrapper_WindowFunction_t3486805455,
	DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336,
	DelegatePInvokeWrapper_BannerFailedToLoadDelegate_t1733255396,
	DelegatePInvokeWrapper_BannerWasClickedDelegate_t2267846590,
	DelegatePInvokeWrapper_BannerWasLoadedDelegate_t2542964478,
	DelegatePInvokeWrapper_InterstitialWasLoadedDelegate_t1835954262,
	DelegatePInvokeWrapper_InterstitialWasViewedDelegate_t365858839,
	DelegatePInvokeWrapper_UnityPurchasingCallback_t2635187846,
	DelegatePInvokeWrapper_UnityNativePurchasingCallback_t3230812225,
	DelegatePInvokeWrapper_OnValidateInput_t1946318473,
	DelegatePInvokeWrapper_CharGetter_t1955031820,
	DelegatePInvokeWrapper_BillingInitListener_t4162012659,
	DelegatePInvokeWrapper_ApplyTween_t1085725044,
	DelegatePInvokeWrapper_EasingFunction_t1024616186,
	DelegatePInvokeWrapper_del_t1625342804,
	DelegatePInvokeWrapper_ChangeCallback_t1172037850,
};
