﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TextTest
struct TextTest_t2178089809;

#include "codegen/il2cpp-codegen.h"

// System.Void TextTest::.ctor()
extern "C"  void TextTest__ctor_m3682155682 (TextTest_t2178089809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextTest::Start()
extern "C"  void TextTest_Start_m2954191026 (TextTest_t2178089809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextTest::Update()
extern "C"  void TextTest_Update_m1985933425 (TextTest_t2178089809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextTest::RunNextTest()
extern "C"  void TextTest_RunNextTest_m3770888878 (TextTest_t2178089809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
