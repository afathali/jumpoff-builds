﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FB_UserInfo
struct FB_UserInfo_t2704578078;
// System.Collections.Generic.Dictionary`2<System.String,FB_UserInfo>
struct Dictionary_2_t324390044;
// System.Collections.Generic.Dictionary`2<System.String,FB_Score>
struct Dictionary_2_t3365620843;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,FB_LikeInfo>>
struct Dictionary_2_t2747790306;
// System.Collections.Generic.List`1<FB_AppRequest>
struct List_1_t4165401053;
// SP_FB_API
struct SP_FB_API_t498797273;
// System.Action
struct Action_t3226471752;
// System.Action`1<FB_PostResult>
struct Action_1_t794684112;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<FB_LoginResult>
struct Action_1_t1650276661;
// System.Action`1<FB_Result>
struct Action_1_t640047754;
// System.Action`1<FB_AppRequestResult>
struct Action_1_t2252469810;
// System.Action`1<FB_PermissionResult>
struct Action_1_t2123811125;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "AssemblyU2DCSharp_SA_Singleton_OLD_1_gen2356072287.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SPFacebook
struct  SPFacebook_t1065228369  : public SA_Singleton_OLD_1_t2356072287
{
public:
	// FB_UserInfo SPFacebook::_userInfo
	FB_UserInfo_t2704578078 * ____userInfo_4;
	// System.Collections.Generic.Dictionary`2<System.String,FB_UserInfo> SPFacebook::_friends
	Dictionary_2_t324390044 * ____friends_5;
	// System.Collections.Generic.Dictionary`2<System.String,FB_UserInfo> SPFacebook::_invitableFriends
	Dictionary_2_t324390044 * ____invitableFriends_6;
	// System.Boolean SPFacebook::_IsInited
	bool ____IsInited_7;
	// System.Collections.Generic.Dictionary`2<System.String,FB_Score> SPFacebook::_userScores
	Dictionary_2_t3365620843 * ____userScores_8;
	// System.Collections.Generic.Dictionary`2<System.String,FB_Score> SPFacebook::_appScores
	Dictionary_2_t3365620843 * ____appScores_9;
	// System.Int32 SPFacebook::lastSubmitedScore
	int32_t ___lastSubmitedScore_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,FB_LikeInfo>> SPFacebook::_likes
	Dictionary_2_t2747790306 * ____likes_11;
	// System.Collections.Generic.List`1<FB_AppRequest> SPFacebook::_AppRequests
	List_1_t4165401053 * ____AppRequests_12;
	// SP_FB_API SPFacebook::_FB
	Il2CppObject * ____FB_13;
	// System.Boolean SPFacebook::IsLoginRequestInProgress
	bool ___IsLoginRequestInProgress_14;

public:
	inline static int32_t get_offset_of__userInfo_4() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369, ____userInfo_4)); }
	inline FB_UserInfo_t2704578078 * get__userInfo_4() const { return ____userInfo_4; }
	inline FB_UserInfo_t2704578078 ** get_address_of__userInfo_4() { return &____userInfo_4; }
	inline void set__userInfo_4(FB_UserInfo_t2704578078 * value)
	{
		____userInfo_4 = value;
		Il2CppCodeGenWriteBarrier(&____userInfo_4, value);
	}

	inline static int32_t get_offset_of__friends_5() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369, ____friends_5)); }
	inline Dictionary_2_t324390044 * get__friends_5() const { return ____friends_5; }
	inline Dictionary_2_t324390044 ** get_address_of__friends_5() { return &____friends_5; }
	inline void set__friends_5(Dictionary_2_t324390044 * value)
	{
		____friends_5 = value;
		Il2CppCodeGenWriteBarrier(&____friends_5, value);
	}

	inline static int32_t get_offset_of__invitableFriends_6() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369, ____invitableFriends_6)); }
	inline Dictionary_2_t324390044 * get__invitableFriends_6() const { return ____invitableFriends_6; }
	inline Dictionary_2_t324390044 ** get_address_of__invitableFriends_6() { return &____invitableFriends_6; }
	inline void set__invitableFriends_6(Dictionary_2_t324390044 * value)
	{
		____invitableFriends_6 = value;
		Il2CppCodeGenWriteBarrier(&____invitableFriends_6, value);
	}

	inline static int32_t get_offset_of__IsInited_7() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369, ____IsInited_7)); }
	inline bool get__IsInited_7() const { return ____IsInited_7; }
	inline bool* get_address_of__IsInited_7() { return &____IsInited_7; }
	inline void set__IsInited_7(bool value)
	{
		____IsInited_7 = value;
	}

	inline static int32_t get_offset_of__userScores_8() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369, ____userScores_8)); }
	inline Dictionary_2_t3365620843 * get__userScores_8() const { return ____userScores_8; }
	inline Dictionary_2_t3365620843 ** get_address_of__userScores_8() { return &____userScores_8; }
	inline void set__userScores_8(Dictionary_2_t3365620843 * value)
	{
		____userScores_8 = value;
		Il2CppCodeGenWriteBarrier(&____userScores_8, value);
	}

	inline static int32_t get_offset_of__appScores_9() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369, ____appScores_9)); }
	inline Dictionary_2_t3365620843 * get__appScores_9() const { return ____appScores_9; }
	inline Dictionary_2_t3365620843 ** get_address_of__appScores_9() { return &____appScores_9; }
	inline void set__appScores_9(Dictionary_2_t3365620843 * value)
	{
		____appScores_9 = value;
		Il2CppCodeGenWriteBarrier(&____appScores_9, value);
	}

	inline static int32_t get_offset_of_lastSubmitedScore_10() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369, ___lastSubmitedScore_10)); }
	inline int32_t get_lastSubmitedScore_10() const { return ___lastSubmitedScore_10; }
	inline int32_t* get_address_of_lastSubmitedScore_10() { return &___lastSubmitedScore_10; }
	inline void set_lastSubmitedScore_10(int32_t value)
	{
		___lastSubmitedScore_10 = value;
	}

	inline static int32_t get_offset_of__likes_11() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369, ____likes_11)); }
	inline Dictionary_2_t2747790306 * get__likes_11() const { return ____likes_11; }
	inline Dictionary_2_t2747790306 ** get_address_of__likes_11() { return &____likes_11; }
	inline void set__likes_11(Dictionary_2_t2747790306 * value)
	{
		____likes_11 = value;
		Il2CppCodeGenWriteBarrier(&____likes_11, value);
	}

	inline static int32_t get_offset_of__AppRequests_12() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369, ____AppRequests_12)); }
	inline List_1_t4165401053 * get__AppRequests_12() const { return ____AppRequests_12; }
	inline List_1_t4165401053 ** get_address_of__AppRequests_12() { return &____AppRequests_12; }
	inline void set__AppRequests_12(List_1_t4165401053 * value)
	{
		____AppRequests_12 = value;
		Il2CppCodeGenWriteBarrier(&____AppRequests_12, value);
	}

	inline static int32_t get_offset_of__FB_13() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369, ____FB_13)); }
	inline Il2CppObject * get__FB_13() const { return ____FB_13; }
	inline Il2CppObject ** get_address_of__FB_13() { return &____FB_13; }
	inline void set__FB_13(Il2CppObject * value)
	{
		____FB_13 = value;
		Il2CppCodeGenWriteBarrier(&____FB_13, value);
	}

	inline static int32_t get_offset_of_IsLoginRequestInProgress_14() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369, ___IsLoginRequestInProgress_14)); }
	inline bool get_IsLoginRequestInProgress_14() const { return ___IsLoginRequestInProgress_14; }
	inline bool* get_address_of_IsLoginRequestInProgress_14() { return &___IsLoginRequestInProgress_14; }
	inline void set_IsLoginRequestInProgress_14(bool value)
	{
		___IsLoginRequestInProgress_14 = value;
	}
};

struct SPFacebook_t1065228369_StaticFields
{
public:
	// System.Action SPFacebook::OnPostStarted
	Action_t3226471752 * ___OnPostStarted_15;
	// System.Action SPFacebook::OnLoginStarted
	Action_t3226471752 * ___OnLoginStarted_16;
	// System.Action SPFacebook::OnLogOut
	Action_t3226471752 * ___OnLogOut_17;
	// System.Action SPFacebook::OnFriendsRequestStarted
	Action_t3226471752 * ___OnFriendsRequestStarted_18;
	// System.Action SPFacebook::OnInitCompleteAction
	Action_t3226471752 * ___OnInitCompleteAction_19;
	// System.Action`1<FB_PostResult> SPFacebook::OnPostingCompleteAction
	Action_1_t794684112 * ___OnPostingCompleteAction_20;
	// System.Action`1<System.Boolean> SPFacebook::OnFocusChangedAction
	Action_1_t3627374100 * ___OnFocusChangedAction_21;
	// System.Action`1<FB_LoginResult> SPFacebook::OnAuthCompleteAction
	Action_1_t1650276661 * ___OnAuthCompleteAction_22;
	// System.Action`1<FB_Result> SPFacebook::OnUserDataRequestCompleteAction
	Action_1_t640047754 * ___OnUserDataRequestCompleteAction_23;
	// System.Action`1<FB_Result> SPFacebook::OnFriendsDataRequestCompleteAction
	Action_1_t640047754 * ___OnFriendsDataRequestCompleteAction_24;
	// System.Action`1<FB_Result> SPFacebook::OnInvitableFriendsDataRequestCompleteAction
	Action_1_t640047754 * ___OnInvitableFriendsDataRequestCompleteAction_25;
	// System.Action`1<FB_AppRequestResult> SPFacebook::OnAppRequestCompleteAction
	Action_1_t2252469810 * ___OnAppRequestCompleteAction_26;
	// System.Action`1<FB_Result> SPFacebook::OnAppRequestsLoaded
	Action_1_t640047754 * ___OnAppRequestsLoaded_27;
	// System.Action`1<FB_PermissionResult> SPFacebook::OnPermissionsLoaded
	Action_1_t2123811125 * ___OnPermissionsLoaded_28;
	// System.Action`1<FB_Result> SPFacebook::OnRevokePermission
	Action_1_t640047754 * ___OnRevokePermission_29;
	// System.Action`1<FB_Result> SPFacebook::OnAppScoresRequestCompleteAction
	Action_1_t640047754 * ___OnAppScoresRequestCompleteAction_30;
	// System.Action`1<FB_Result> SPFacebook::OnPlayerScoresRequestCompleteAction
	Action_1_t640047754 * ___OnPlayerScoresRequestCompleteAction_31;
	// System.Action`1<FB_Result> SPFacebook::OnSubmitScoreRequestCompleteAction
	Action_1_t640047754 * ___OnSubmitScoreRequestCompleteAction_32;
	// System.Action`1<FB_Result> SPFacebook::OnDeleteScoresRequestCompleteAction
	Action_1_t640047754 * ___OnDeleteScoresRequestCompleteAction_33;
	// System.Action`1<FB_Result> SPFacebook::OnLikesListLoadedAction
	Action_1_t640047754 * ___OnLikesListLoadedAction_34;
	// System.Action SPFacebook::<>f__am$cache1F
	Action_t3226471752 * ___U3CU3Ef__amU24cache1F_35;
	// System.Action SPFacebook::<>f__am$cache20
	Action_t3226471752 * ___U3CU3Ef__amU24cache20_36;
	// System.Action SPFacebook::<>f__am$cache21
	Action_t3226471752 * ___U3CU3Ef__amU24cache21_37;
	// System.Action SPFacebook::<>f__am$cache22
	Action_t3226471752 * ___U3CU3Ef__amU24cache22_38;
	// System.Action SPFacebook::<>f__am$cache23
	Action_t3226471752 * ___U3CU3Ef__amU24cache23_39;
	// System.Action`1<FB_PostResult> SPFacebook::<>f__am$cache24
	Action_1_t794684112 * ___U3CU3Ef__amU24cache24_40;
	// System.Action`1<System.Boolean> SPFacebook::<>f__am$cache25
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache25_41;
	// System.Action`1<FB_LoginResult> SPFacebook::<>f__am$cache26
	Action_1_t1650276661 * ___U3CU3Ef__amU24cache26_42;
	// System.Action`1<FB_Result> SPFacebook::<>f__am$cache27
	Action_1_t640047754 * ___U3CU3Ef__amU24cache27_43;
	// System.Action`1<FB_Result> SPFacebook::<>f__am$cache28
	Action_1_t640047754 * ___U3CU3Ef__amU24cache28_44;
	// System.Action`1<FB_Result> SPFacebook::<>f__am$cache29
	Action_1_t640047754 * ___U3CU3Ef__amU24cache29_45;
	// System.Action`1<FB_AppRequestResult> SPFacebook::<>f__am$cache2A
	Action_1_t2252469810 * ___U3CU3Ef__amU24cache2A_46;
	// System.Action`1<FB_Result> SPFacebook::<>f__am$cache2B
	Action_1_t640047754 * ___U3CU3Ef__amU24cache2B_47;
	// System.Action`1<FB_PermissionResult> SPFacebook::<>f__am$cache2C
	Action_1_t2123811125 * ___U3CU3Ef__amU24cache2C_48;
	// System.Action`1<FB_Result> SPFacebook::<>f__am$cache2D
	Action_1_t640047754 * ___U3CU3Ef__amU24cache2D_49;
	// System.Action`1<FB_Result> SPFacebook::<>f__am$cache2E
	Action_1_t640047754 * ___U3CU3Ef__amU24cache2E_50;
	// System.Action`1<FB_Result> SPFacebook::<>f__am$cache2F
	Action_1_t640047754 * ___U3CU3Ef__amU24cache2F_51;
	// System.Action`1<FB_Result> SPFacebook::<>f__am$cache30
	Action_1_t640047754 * ___U3CU3Ef__amU24cache30_52;
	// System.Action`1<FB_Result> SPFacebook::<>f__am$cache31
	Action_1_t640047754 * ___U3CU3Ef__amU24cache31_53;
	// System.Action`1<FB_Result> SPFacebook::<>f__am$cache32
	Action_1_t640047754 * ___U3CU3Ef__amU24cache32_54;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> SPFacebook::<>f__switch$map3
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map3_55;

public:
	inline static int32_t get_offset_of_OnPostStarted_15() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnPostStarted_15)); }
	inline Action_t3226471752 * get_OnPostStarted_15() const { return ___OnPostStarted_15; }
	inline Action_t3226471752 ** get_address_of_OnPostStarted_15() { return &___OnPostStarted_15; }
	inline void set_OnPostStarted_15(Action_t3226471752 * value)
	{
		___OnPostStarted_15 = value;
		Il2CppCodeGenWriteBarrier(&___OnPostStarted_15, value);
	}

	inline static int32_t get_offset_of_OnLoginStarted_16() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnLoginStarted_16)); }
	inline Action_t3226471752 * get_OnLoginStarted_16() const { return ___OnLoginStarted_16; }
	inline Action_t3226471752 ** get_address_of_OnLoginStarted_16() { return &___OnLoginStarted_16; }
	inline void set_OnLoginStarted_16(Action_t3226471752 * value)
	{
		___OnLoginStarted_16 = value;
		Il2CppCodeGenWriteBarrier(&___OnLoginStarted_16, value);
	}

	inline static int32_t get_offset_of_OnLogOut_17() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnLogOut_17)); }
	inline Action_t3226471752 * get_OnLogOut_17() const { return ___OnLogOut_17; }
	inline Action_t3226471752 ** get_address_of_OnLogOut_17() { return &___OnLogOut_17; }
	inline void set_OnLogOut_17(Action_t3226471752 * value)
	{
		___OnLogOut_17 = value;
		Il2CppCodeGenWriteBarrier(&___OnLogOut_17, value);
	}

	inline static int32_t get_offset_of_OnFriendsRequestStarted_18() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnFriendsRequestStarted_18)); }
	inline Action_t3226471752 * get_OnFriendsRequestStarted_18() const { return ___OnFriendsRequestStarted_18; }
	inline Action_t3226471752 ** get_address_of_OnFriendsRequestStarted_18() { return &___OnFriendsRequestStarted_18; }
	inline void set_OnFriendsRequestStarted_18(Action_t3226471752 * value)
	{
		___OnFriendsRequestStarted_18 = value;
		Il2CppCodeGenWriteBarrier(&___OnFriendsRequestStarted_18, value);
	}

	inline static int32_t get_offset_of_OnInitCompleteAction_19() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnInitCompleteAction_19)); }
	inline Action_t3226471752 * get_OnInitCompleteAction_19() const { return ___OnInitCompleteAction_19; }
	inline Action_t3226471752 ** get_address_of_OnInitCompleteAction_19() { return &___OnInitCompleteAction_19; }
	inline void set_OnInitCompleteAction_19(Action_t3226471752 * value)
	{
		___OnInitCompleteAction_19 = value;
		Il2CppCodeGenWriteBarrier(&___OnInitCompleteAction_19, value);
	}

	inline static int32_t get_offset_of_OnPostingCompleteAction_20() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnPostingCompleteAction_20)); }
	inline Action_1_t794684112 * get_OnPostingCompleteAction_20() const { return ___OnPostingCompleteAction_20; }
	inline Action_1_t794684112 ** get_address_of_OnPostingCompleteAction_20() { return &___OnPostingCompleteAction_20; }
	inline void set_OnPostingCompleteAction_20(Action_1_t794684112 * value)
	{
		___OnPostingCompleteAction_20 = value;
		Il2CppCodeGenWriteBarrier(&___OnPostingCompleteAction_20, value);
	}

	inline static int32_t get_offset_of_OnFocusChangedAction_21() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnFocusChangedAction_21)); }
	inline Action_1_t3627374100 * get_OnFocusChangedAction_21() const { return ___OnFocusChangedAction_21; }
	inline Action_1_t3627374100 ** get_address_of_OnFocusChangedAction_21() { return &___OnFocusChangedAction_21; }
	inline void set_OnFocusChangedAction_21(Action_1_t3627374100 * value)
	{
		___OnFocusChangedAction_21 = value;
		Il2CppCodeGenWriteBarrier(&___OnFocusChangedAction_21, value);
	}

	inline static int32_t get_offset_of_OnAuthCompleteAction_22() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnAuthCompleteAction_22)); }
	inline Action_1_t1650276661 * get_OnAuthCompleteAction_22() const { return ___OnAuthCompleteAction_22; }
	inline Action_1_t1650276661 ** get_address_of_OnAuthCompleteAction_22() { return &___OnAuthCompleteAction_22; }
	inline void set_OnAuthCompleteAction_22(Action_1_t1650276661 * value)
	{
		___OnAuthCompleteAction_22 = value;
		Il2CppCodeGenWriteBarrier(&___OnAuthCompleteAction_22, value);
	}

	inline static int32_t get_offset_of_OnUserDataRequestCompleteAction_23() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnUserDataRequestCompleteAction_23)); }
	inline Action_1_t640047754 * get_OnUserDataRequestCompleteAction_23() const { return ___OnUserDataRequestCompleteAction_23; }
	inline Action_1_t640047754 ** get_address_of_OnUserDataRequestCompleteAction_23() { return &___OnUserDataRequestCompleteAction_23; }
	inline void set_OnUserDataRequestCompleteAction_23(Action_1_t640047754 * value)
	{
		___OnUserDataRequestCompleteAction_23 = value;
		Il2CppCodeGenWriteBarrier(&___OnUserDataRequestCompleteAction_23, value);
	}

	inline static int32_t get_offset_of_OnFriendsDataRequestCompleteAction_24() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnFriendsDataRequestCompleteAction_24)); }
	inline Action_1_t640047754 * get_OnFriendsDataRequestCompleteAction_24() const { return ___OnFriendsDataRequestCompleteAction_24; }
	inline Action_1_t640047754 ** get_address_of_OnFriendsDataRequestCompleteAction_24() { return &___OnFriendsDataRequestCompleteAction_24; }
	inline void set_OnFriendsDataRequestCompleteAction_24(Action_1_t640047754 * value)
	{
		___OnFriendsDataRequestCompleteAction_24 = value;
		Il2CppCodeGenWriteBarrier(&___OnFriendsDataRequestCompleteAction_24, value);
	}

	inline static int32_t get_offset_of_OnInvitableFriendsDataRequestCompleteAction_25() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnInvitableFriendsDataRequestCompleteAction_25)); }
	inline Action_1_t640047754 * get_OnInvitableFriendsDataRequestCompleteAction_25() const { return ___OnInvitableFriendsDataRequestCompleteAction_25; }
	inline Action_1_t640047754 ** get_address_of_OnInvitableFriendsDataRequestCompleteAction_25() { return &___OnInvitableFriendsDataRequestCompleteAction_25; }
	inline void set_OnInvitableFriendsDataRequestCompleteAction_25(Action_1_t640047754 * value)
	{
		___OnInvitableFriendsDataRequestCompleteAction_25 = value;
		Il2CppCodeGenWriteBarrier(&___OnInvitableFriendsDataRequestCompleteAction_25, value);
	}

	inline static int32_t get_offset_of_OnAppRequestCompleteAction_26() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnAppRequestCompleteAction_26)); }
	inline Action_1_t2252469810 * get_OnAppRequestCompleteAction_26() const { return ___OnAppRequestCompleteAction_26; }
	inline Action_1_t2252469810 ** get_address_of_OnAppRequestCompleteAction_26() { return &___OnAppRequestCompleteAction_26; }
	inline void set_OnAppRequestCompleteAction_26(Action_1_t2252469810 * value)
	{
		___OnAppRequestCompleteAction_26 = value;
		Il2CppCodeGenWriteBarrier(&___OnAppRequestCompleteAction_26, value);
	}

	inline static int32_t get_offset_of_OnAppRequestsLoaded_27() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnAppRequestsLoaded_27)); }
	inline Action_1_t640047754 * get_OnAppRequestsLoaded_27() const { return ___OnAppRequestsLoaded_27; }
	inline Action_1_t640047754 ** get_address_of_OnAppRequestsLoaded_27() { return &___OnAppRequestsLoaded_27; }
	inline void set_OnAppRequestsLoaded_27(Action_1_t640047754 * value)
	{
		___OnAppRequestsLoaded_27 = value;
		Il2CppCodeGenWriteBarrier(&___OnAppRequestsLoaded_27, value);
	}

	inline static int32_t get_offset_of_OnPermissionsLoaded_28() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnPermissionsLoaded_28)); }
	inline Action_1_t2123811125 * get_OnPermissionsLoaded_28() const { return ___OnPermissionsLoaded_28; }
	inline Action_1_t2123811125 ** get_address_of_OnPermissionsLoaded_28() { return &___OnPermissionsLoaded_28; }
	inline void set_OnPermissionsLoaded_28(Action_1_t2123811125 * value)
	{
		___OnPermissionsLoaded_28 = value;
		Il2CppCodeGenWriteBarrier(&___OnPermissionsLoaded_28, value);
	}

	inline static int32_t get_offset_of_OnRevokePermission_29() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnRevokePermission_29)); }
	inline Action_1_t640047754 * get_OnRevokePermission_29() const { return ___OnRevokePermission_29; }
	inline Action_1_t640047754 ** get_address_of_OnRevokePermission_29() { return &___OnRevokePermission_29; }
	inline void set_OnRevokePermission_29(Action_1_t640047754 * value)
	{
		___OnRevokePermission_29 = value;
		Il2CppCodeGenWriteBarrier(&___OnRevokePermission_29, value);
	}

	inline static int32_t get_offset_of_OnAppScoresRequestCompleteAction_30() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnAppScoresRequestCompleteAction_30)); }
	inline Action_1_t640047754 * get_OnAppScoresRequestCompleteAction_30() const { return ___OnAppScoresRequestCompleteAction_30; }
	inline Action_1_t640047754 ** get_address_of_OnAppScoresRequestCompleteAction_30() { return &___OnAppScoresRequestCompleteAction_30; }
	inline void set_OnAppScoresRequestCompleteAction_30(Action_1_t640047754 * value)
	{
		___OnAppScoresRequestCompleteAction_30 = value;
		Il2CppCodeGenWriteBarrier(&___OnAppScoresRequestCompleteAction_30, value);
	}

	inline static int32_t get_offset_of_OnPlayerScoresRequestCompleteAction_31() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnPlayerScoresRequestCompleteAction_31)); }
	inline Action_1_t640047754 * get_OnPlayerScoresRequestCompleteAction_31() const { return ___OnPlayerScoresRequestCompleteAction_31; }
	inline Action_1_t640047754 ** get_address_of_OnPlayerScoresRequestCompleteAction_31() { return &___OnPlayerScoresRequestCompleteAction_31; }
	inline void set_OnPlayerScoresRequestCompleteAction_31(Action_1_t640047754 * value)
	{
		___OnPlayerScoresRequestCompleteAction_31 = value;
		Il2CppCodeGenWriteBarrier(&___OnPlayerScoresRequestCompleteAction_31, value);
	}

	inline static int32_t get_offset_of_OnSubmitScoreRequestCompleteAction_32() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnSubmitScoreRequestCompleteAction_32)); }
	inline Action_1_t640047754 * get_OnSubmitScoreRequestCompleteAction_32() const { return ___OnSubmitScoreRequestCompleteAction_32; }
	inline Action_1_t640047754 ** get_address_of_OnSubmitScoreRequestCompleteAction_32() { return &___OnSubmitScoreRequestCompleteAction_32; }
	inline void set_OnSubmitScoreRequestCompleteAction_32(Action_1_t640047754 * value)
	{
		___OnSubmitScoreRequestCompleteAction_32 = value;
		Il2CppCodeGenWriteBarrier(&___OnSubmitScoreRequestCompleteAction_32, value);
	}

	inline static int32_t get_offset_of_OnDeleteScoresRequestCompleteAction_33() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnDeleteScoresRequestCompleteAction_33)); }
	inline Action_1_t640047754 * get_OnDeleteScoresRequestCompleteAction_33() const { return ___OnDeleteScoresRequestCompleteAction_33; }
	inline Action_1_t640047754 ** get_address_of_OnDeleteScoresRequestCompleteAction_33() { return &___OnDeleteScoresRequestCompleteAction_33; }
	inline void set_OnDeleteScoresRequestCompleteAction_33(Action_1_t640047754 * value)
	{
		___OnDeleteScoresRequestCompleteAction_33 = value;
		Il2CppCodeGenWriteBarrier(&___OnDeleteScoresRequestCompleteAction_33, value);
	}

	inline static int32_t get_offset_of_OnLikesListLoadedAction_34() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___OnLikesListLoadedAction_34)); }
	inline Action_1_t640047754 * get_OnLikesListLoadedAction_34() const { return ___OnLikesListLoadedAction_34; }
	inline Action_1_t640047754 ** get_address_of_OnLikesListLoadedAction_34() { return &___OnLikesListLoadedAction_34; }
	inline void set_OnLikesListLoadedAction_34(Action_1_t640047754 * value)
	{
		___OnLikesListLoadedAction_34 = value;
		Il2CppCodeGenWriteBarrier(&___OnLikesListLoadedAction_34, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1F_35() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache1F_35)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache1F_35() const { return ___U3CU3Ef__amU24cache1F_35; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache1F_35() { return &___U3CU3Ef__amU24cache1F_35; }
	inline void set_U3CU3Ef__amU24cache1F_35(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache1F_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1F_35, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache20_36() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache20_36)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache20_36() const { return ___U3CU3Ef__amU24cache20_36; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache20_36() { return &___U3CU3Ef__amU24cache20_36; }
	inline void set_U3CU3Ef__amU24cache20_36(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache20_36 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache20_36, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache21_37() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache21_37)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache21_37() const { return ___U3CU3Ef__amU24cache21_37; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache21_37() { return &___U3CU3Ef__amU24cache21_37; }
	inline void set_U3CU3Ef__amU24cache21_37(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache21_37 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache21_37, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache22_38() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache22_38)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache22_38() const { return ___U3CU3Ef__amU24cache22_38; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache22_38() { return &___U3CU3Ef__amU24cache22_38; }
	inline void set_U3CU3Ef__amU24cache22_38(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache22_38 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache22_38, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache23_39() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache23_39)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache23_39() const { return ___U3CU3Ef__amU24cache23_39; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache23_39() { return &___U3CU3Ef__amU24cache23_39; }
	inline void set_U3CU3Ef__amU24cache23_39(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache23_39 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache23_39, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache24_40() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache24_40)); }
	inline Action_1_t794684112 * get_U3CU3Ef__amU24cache24_40() const { return ___U3CU3Ef__amU24cache24_40; }
	inline Action_1_t794684112 ** get_address_of_U3CU3Ef__amU24cache24_40() { return &___U3CU3Ef__amU24cache24_40; }
	inline void set_U3CU3Ef__amU24cache24_40(Action_1_t794684112 * value)
	{
		___U3CU3Ef__amU24cache24_40 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache24_40, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache25_41() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache25_41)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache25_41() const { return ___U3CU3Ef__amU24cache25_41; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache25_41() { return &___U3CU3Ef__amU24cache25_41; }
	inline void set_U3CU3Ef__amU24cache25_41(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache25_41 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache25_41, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache26_42() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache26_42)); }
	inline Action_1_t1650276661 * get_U3CU3Ef__amU24cache26_42() const { return ___U3CU3Ef__amU24cache26_42; }
	inline Action_1_t1650276661 ** get_address_of_U3CU3Ef__amU24cache26_42() { return &___U3CU3Ef__amU24cache26_42; }
	inline void set_U3CU3Ef__amU24cache26_42(Action_1_t1650276661 * value)
	{
		___U3CU3Ef__amU24cache26_42 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache26_42, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache27_43() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache27_43)); }
	inline Action_1_t640047754 * get_U3CU3Ef__amU24cache27_43() const { return ___U3CU3Ef__amU24cache27_43; }
	inline Action_1_t640047754 ** get_address_of_U3CU3Ef__amU24cache27_43() { return &___U3CU3Ef__amU24cache27_43; }
	inline void set_U3CU3Ef__amU24cache27_43(Action_1_t640047754 * value)
	{
		___U3CU3Ef__amU24cache27_43 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache27_43, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache28_44() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache28_44)); }
	inline Action_1_t640047754 * get_U3CU3Ef__amU24cache28_44() const { return ___U3CU3Ef__amU24cache28_44; }
	inline Action_1_t640047754 ** get_address_of_U3CU3Ef__amU24cache28_44() { return &___U3CU3Ef__amU24cache28_44; }
	inline void set_U3CU3Ef__amU24cache28_44(Action_1_t640047754 * value)
	{
		___U3CU3Ef__amU24cache28_44 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache28_44, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache29_45() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache29_45)); }
	inline Action_1_t640047754 * get_U3CU3Ef__amU24cache29_45() const { return ___U3CU3Ef__amU24cache29_45; }
	inline Action_1_t640047754 ** get_address_of_U3CU3Ef__amU24cache29_45() { return &___U3CU3Ef__amU24cache29_45; }
	inline void set_U3CU3Ef__amU24cache29_45(Action_1_t640047754 * value)
	{
		___U3CU3Ef__amU24cache29_45 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache29_45, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2A_46() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache2A_46)); }
	inline Action_1_t2252469810 * get_U3CU3Ef__amU24cache2A_46() const { return ___U3CU3Ef__amU24cache2A_46; }
	inline Action_1_t2252469810 ** get_address_of_U3CU3Ef__amU24cache2A_46() { return &___U3CU3Ef__amU24cache2A_46; }
	inline void set_U3CU3Ef__amU24cache2A_46(Action_1_t2252469810 * value)
	{
		___U3CU3Ef__amU24cache2A_46 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2A_46, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2B_47() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache2B_47)); }
	inline Action_1_t640047754 * get_U3CU3Ef__amU24cache2B_47() const { return ___U3CU3Ef__amU24cache2B_47; }
	inline Action_1_t640047754 ** get_address_of_U3CU3Ef__amU24cache2B_47() { return &___U3CU3Ef__amU24cache2B_47; }
	inline void set_U3CU3Ef__amU24cache2B_47(Action_1_t640047754 * value)
	{
		___U3CU3Ef__amU24cache2B_47 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2B_47, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2C_48() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache2C_48)); }
	inline Action_1_t2123811125 * get_U3CU3Ef__amU24cache2C_48() const { return ___U3CU3Ef__amU24cache2C_48; }
	inline Action_1_t2123811125 ** get_address_of_U3CU3Ef__amU24cache2C_48() { return &___U3CU3Ef__amU24cache2C_48; }
	inline void set_U3CU3Ef__amU24cache2C_48(Action_1_t2123811125 * value)
	{
		___U3CU3Ef__amU24cache2C_48 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2C_48, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2D_49() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache2D_49)); }
	inline Action_1_t640047754 * get_U3CU3Ef__amU24cache2D_49() const { return ___U3CU3Ef__amU24cache2D_49; }
	inline Action_1_t640047754 ** get_address_of_U3CU3Ef__amU24cache2D_49() { return &___U3CU3Ef__amU24cache2D_49; }
	inline void set_U3CU3Ef__amU24cache2D_49(Action_1_t640047754 * value)
	{
		___U3CU3Ef__amU24cache2D_49 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2D_49, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2E_50() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache2E_50)); }
	inline Action_1_t640047754 * get_U3CU3Ef__amU24cache2E_50() const { return ___U3CU3Ef__amU24cache2E_50; }
	inline Action_1_t640047754 ** get_address_of_U3CU3Ef__amU24cache2E_50() { return &___U3CU3Ef__amU24cache2E_50; }
	inline void set_U3CU3Ef__amU24cache2E_50(Action_1_t640047754 * value)
	{
		___U3CU3Ef__amU24cache2E_50 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2E_50, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2F_51() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache2F_51)); }
	inline Action_1_t640047754 * get_U3CU3Ef__amU24cache2F_51() const { return ___U3CU3Ef__amU24cache2F_51; }
	inline Action_1_t640047754 ** get_address_of_U3CU3Ef__amU24cache2F_51() { return &___U3CU3Ef__amU24cache2F_51; }
	inline void set_U3CU3Ef__amU24cache2F_51(Action_1_t640047754 * value)
	{
		___U3CU3Ef__amU24cache2F_51 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2F_51, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache30_52() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache30_52)); }
	inline Action_1_t640047754 * get_U3CU3Ef__amU24cache30_52() const { return ___U3CU3Ef__amU24cache30_52; }
	inline Action_1_t640047754 ** get_address_of_U3CU3Ef__amU24cache30_52() { return &___U3CU3Ef__amU24cache30_52; }
	inline void set_U3CU3Ef__amU24cache30_52(Action_1_t640047754 * value)
	{
		___U3CU3Ef__amU24cache30_52 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache30_52, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache31_53() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache31_53)); }
	inline Action_1_t640047754 * get_U3CU3Ef__amU24cache31_53() const { return ___U3CU3Ef__amU24cache31_53; }
	inline Action_1_t640047754 ** get_address_of_U3CU3Ef__amU24cache31_53() { return &___U3CU3Ef__amU24cache31_53; }
	inline void set_U3CU3Ef__amU24cache31_53(Action_1_t640047754 * value)
	{
		___U3CU3Ef__amU24cache31_53 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache31_53, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache32_54() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__amU24cache32_54)); }
	inline Action_1_t640047754 * get_U3CU3Ef__amU24cache32_54() const { return ___U3CU3Ef__amU24cache32_54; }
	inline Action_1_t640047754 ** get_address_of_U3CU3Ef__amU24cache32_54() { return &___U3CU3Ef__amU24cache32_54; }
	inline void set_U3CU3Ef__amU24cache32_54(Action_1_t640047754 * value)
	{
		___U3CU3Ef__amU24cache32_54 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache32_54, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_55() { return static_cast<int32_t>(offsetof(SPFacebook_t1065228369_StaticFields, ___U3CU3Ef__switchU24map3_55)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map3_55() const { return ___U3CU3Ef__switchU24map3_55; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map3_55() { return &___U3CU3Ef__switchU24map3_55; }
	inline void set_U3CU3Ef__switchU24map3_55(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map3_55 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map3_55, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
