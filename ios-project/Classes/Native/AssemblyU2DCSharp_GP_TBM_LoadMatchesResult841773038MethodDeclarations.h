﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP_TBM_LoadMatchesResult
struct GP_TBM_LoadMatchesResult_t841773038;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GP_TBM_LoadMatchesResult::.ctor(System.String)
extern "C"  void GP_TBM_LoadMatchesResult__ctor_m3665956419 (GP_TBM_LoadMatchesResult_t841773038 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
